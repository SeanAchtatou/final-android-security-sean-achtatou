import math
import numpy as np
import time
import os
import pyjadx

from xml.dom import minidom
from apk_analyzer import APK_analyze
from pathlib import Path
from shutil import copy

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

if os.name == 'nt':
    print("\033[31m[WARNING]\033[00m The application has been developped to work on \033[04m\033[96mLinux\033[00m, the environment detected is \033[41m\033[5mWindows\033[00m. The application might not work correctly.")
    input("Press enter to continue...")
    print("")


print("\033[47m\033[30m----------------------------Analyze APK 1.0-------------------------\033[00m \n")

apk_tool = 'jadx/build/jadx/bin/jadx'
apk_tool_gui = 'jadx/build/jadx/bin/jadx-gui'

while True:
    print('')
    try:
        path_analyze = 'Applications_APK-Android/Analyze'
        existing_apk = os.listdir(path_analyze)

    except:
        print("\033[31m[ERROR]\033[00m The path <Applications_APK-Android/Analyze> does not exist.")
        exit()

    print("\n\033[32m\033[04mAndroid applications analyzable:\033[00m")
    for i in existing_apk:
        if i[-4:] == '.apk':
            print(f"\033[01m{i}\033[00m")

    while True:
        apk = str(input("\n\033[93m[SYSTEM]\033[00m Please, enter one of the APK file listed above to analyze (i.e. 'PUBG.apk'), or 'exit' to quit, or 'info' for information about the application  >>"))
        if apk.lower() == "exit":
            exit()

        if apk.lower() == 'info':
            print("")
            print("""This application has been developped by as student in the Bachelor In Computer Science [BICS] in Luxembourg. 
It allows to detect if a possible Android application contains malwares activities inside. 
Moreover, you have the possibility to manually analyze the code of the application with the help of a GUI. 
The application should be used in the \033[04m\033[96mLinux Environment\033[00m.

How to use the application :
1. Enter the application(s) to analyze in the path \033[04m\033[96m./Applications_APK-Android/Analyze\033[00m
2. Launch the \033[04m\033[96mmain_android.py\033[00m
3. Select the application to analyze
4. Choose manual (GUI) or automatic analysis.
5. Follow the instructions.""")
            input("Press enter to return...")

        if apk[-4:] != ".apk" or (apk not in existing_apk) and apk != 'info':
            print("\033[33m--[WARNING]\033[00m The name of the file should finish with \033[04m\033[96m.apk\033[00m, or the file specified does not exist.")
        else:
            break

    if " " in apk:
        print(f'\033[33m--[WARNING]\033[00m The name of the file contains spaces, the file has been renamed from \033[04m\033[96m{apk}\033[00m to \033[04m\033[96m{apk.replace(" ","")}\033[00m ')

        copy(f'{path_analyze}/{apk}',f'{path_analyze}/{apk.replace(" ","")}')
        os.remove(f'{path_analyze}/{apk}')
        apk = apk.replace(" ","")

    manually = input("\033[93m[SYSTEM]\033[00m Would you like to manually analyze the APK ? [Y/N] >>").lower()
    if manually == "y":
        print(f'\033[93m[SYSTEM]\033[00m Opening of the \033[1;32mGUI\033[00m.')
        print(f'\033[93m[SYSTEM]\033[00m Do \033[31mNOT\033[00m close the console.')

        os.system(apk_tool_gui + " " + "./" + os.path.join(path_analyze,apk))
        exit()

    try:
        print(f'\033[93m[SYSTEM]\033[00m APK \033[1;32mSUCCESSFULLY\033[00m found. Creation of the folder containing the decompiled application in path \033[04m\033[96m<{path_analyze}/Decompiled_{apk}_folder>\033[00m')
        os.mkdir(f'{path_analyze}/Decompiled_{apk}_folder')
    except:
        print(f"\033[33m--[WARNING]\033[00m Folder \033[04m\033[96m<{path_analyze}/Decompiled_{apk}_folder>\033[00m already exists.")

    if not os.listdir(f'{path_analyze}/Decompiled_{apk}_folder'):
        print(f'\033[93m[SYSTEM]\033[00m Start of the reverse Engineering on the APK \033[42m\033[5m{apk}\033[00m running ... Please wait and do not exit the application.')
        os.system(apk_tool + " " + f"-d {path_analyze}/Decompiled_{apk}_folder"+ " " + "./" +os.path.join(path_analyze,apk))
        print(f'\033[93m[SYSTEM]\033[00m APK has been \033[1;32mSUCCESSFULLY\033[00m decompiled.')
    else:
        print(f'\033[33m--[WARNING]\033[00m Folder \033[04m\033[96m<{path_analyze}/Decompiled_{apk}_folder>\033[00m is not empty, please empty it if you want to redo reverse engineering. ')

    #try:

    result = APK_analyze(f'{path_analyze}/Decompiled_{apk}_folder')
    if result == -1:
        print(f'\033[31m[ALERT]\033[00m The APK has too much missing files, analyze \033[31m\033[4mIMPOSSIBLE\033[00m !')
    else:
        if result >= 55:
            print(f'            \033[31m[ALERT]\033[00m \033[41m\033[5mThe APK analyzed is at {result}% a Malware !\033[00m \033[31m[ALERT]\033[00m')
        elif 45 < result < 55:
            print(f'            \033[33m[ALERT]\033[00m \033[30m\033[43m\033[5mThe APK is only safe at {100-result}%, so be careful !\033[00m \033[33m[ALERT]\033[00m ')
        else:
            print(f'            \033[33m[ALERT]\033[00m \033[42m\033[5mThe APK analyzed is safe !\033[00m \033[33m[ALERT]\033[00m ')

    input("\033[93m[SYSTEM]\033[00m Press enter to continue...")

    #except:
        #print("\033[31m[ERROR]\033[00m Analyze of the APK has failed.")









