package com.google.android.vending.licensing;

public class StrictPolicy implements Policy {
    private int mLastResponse = Policy.RETRY;

    public void processServerResponse(int i, ResponseData responseData) {
        this.mLastResponse = i;
    }

    public boolean allowAccess() {
        return this.mLastResponse == 256;
    }
}
