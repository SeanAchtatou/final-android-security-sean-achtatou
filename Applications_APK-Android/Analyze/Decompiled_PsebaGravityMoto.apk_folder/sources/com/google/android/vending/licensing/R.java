package com.google.android.vending.licensing;

public final class R {
    private R() {
    }

    public static final class string {
        public static final int yyg_gpl_ext_buy = 2131361890;
        public static final int yyg_gpl_ext_check_network = 2131361891;
        public static final int yyg_gpl_ext_license_fail = 2131361892;
        public static final int yyg_gpl_ext_retry = 2131361893;
        public static final int yyg_gpl_ext_retry_needed = 2131361894;

        private string() {
        }
    }
}
