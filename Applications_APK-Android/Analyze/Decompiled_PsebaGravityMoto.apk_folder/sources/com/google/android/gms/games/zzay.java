package com.google.android.gms.games;

import androidx.annotation.NonNull;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zzq;

final class zzay implements zzq<Players.LoadPlayersResult> {
    zzay() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        Players.LoadPlayersResult loadPlayersResult = (Players.LoadPlayersResult) obj;
        if (loadPlayersResult.getPlayers() != null) {
            loadPlayersResult.getPlayers().release();
        }
    }
}
