package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzwt;
import java.io.IOException;

public final class zzxk extends zzdrr<zzxk> {
    public String zzcep = null;
    private zzwt.zza[] zzceq = new zzwt.zza[0];
    private zzwx zzcer = null;
    private zzwx zzces = null;
    private zzwx zzcet = null;

    public zzxk() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        String str = this.zzcep;
        if (str != null) {
            zzdrp.zzf(1, str);
        }
        zzwt.zza[] zzaArr = this.zzceq;
        if (zzaArr != null && zzaArr.length > 0) {
            int i = 0;
            while (true) {
                zzwt.zza[] zzaArr2 = this.zzceq;
                if (i >= zzaArr2.length) {
                    break;
                }
                zzwt.zza zza = zzaArr2[i];
                if (zza != null) {
                    zzdrp.zze(2, zza);
                }
                i++;
            }
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        String str = this.zzcep;
        if (str != null) {
            zzor += zzdrp.zzg(1, str);
        }
        zzwt.zza[] zzaArr = this.zzceq;
        if (zzaArr != null && zzaArr.length > 0) {
            int i = 0;
            while (true) {
                zzwt.zza[] zzaArr2 = this.zzceq;
                if (i >= zzaArr2.length) {
                    break;
                }
                zzwt.zza zza = zzaArr2[i];
                if (zza != null) {
                    zzor += zzdni.zzc(2, zza);
                }
                i++;
            }
        }
        return zzor;
    }
}
