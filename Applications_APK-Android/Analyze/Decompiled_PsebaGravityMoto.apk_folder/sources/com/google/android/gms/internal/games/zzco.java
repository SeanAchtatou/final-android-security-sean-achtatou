package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.snapshot.Snapshots;

abstract class zzco extends Games.zza<Snapshots.CommitSnapshotResult> {
    private zzco(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzco(GoogleApiClient googleApiClient, zzcj zzcj) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzcp(this, status);
    }
}
