package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class zzdjj implements zzdlk {
    private final SecretKeySpec zzgxn;
    private final int zzgxo;
    private final int zzgxp = zzdkx.zzhap.zzgt("AES/CTR/NoPadding").getBlockSize();

    public zzdjj(byte[] bArr, int i) throws GeneralSecurityException {
        zzdlx.zzfg(bArr.length);
        this.zzgxn = new SecretKeySpec(bArr, "AES");
        if (i < 12 || i > this.zzgxp) {
            throw new GeneralSecurityException("invalid IV size");
        }
        this.zzgxo = i;
    }

    public final byte[] zzo(byte[] bArr) throws GeneralSecurityException {
        int length = bArr.length;
        int i = this.zzgxo;
        if (length <= Integer.MAX_VALUE - i) {
            byte[] bArr2 = new byte[(bArr.length + i)];
            byte[] zzff = zzdlo.zzff(i);
            System.arraycopy(zzff, 0, bArr2, 0, this.zzgxo);
            int length2 = bArr.length;
            int i2 = this.zzgxo;
            Cipher zzgt = zzdkx.zzhap.zzgt("AES/CTR/NoPadding");
            byte[] bArr3 = new byte[this.zzgxp];
            System.arraycopy(zzff, 0, bArr3, 0, this.zzgxo);
            zzgt.init(1, this.zzgxn, new IvParameterSpec(bArr3));
            if (zzgt.doFinal(bArr, 0, length2, bArr2, i2) == length2) {
                return bArr2;
            }
            throw new GeneralSecurityException("stored output's length does not match input's length");
        }
        int i3 = Integer.MAX_VALUE - i;
        StringBuilder sb = new StringBuilder(43);
        sb.append("plaintext length can not exceed ");
        sb.append(i3);
        throw new GeneralSecurityException(sb.toString());
    }
}
