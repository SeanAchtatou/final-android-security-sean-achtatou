package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.List;

final class zzdng implements zzdqa {
    private int tag;
    private final zzdnd zzhdj;
    private int zzhdk;
    private int zzhdl = 0;

    public static zzdng zza(zzdnd zzdnd) {
        if (zzdnd.zzhdc != null) {
            return zzdnd.zzhdc;
        }
        return new zzdng(zzdnd);
    }

    private zzdng(zzdnd zzdnd) {
        this.zzhdj = (zzdnd) zzdod.zza(zzdnd, "input");
        this.zzhdj.zzhdc = this;
    }

    public final int zzaws() throws IOException {
        int i = this.zzhdl;
        if (i != 0) {
            this.tag = i;
            this.zzhdl = 0;
        } else {
            this.tag = this.zzhdj.zzavu();
        }
        int i2 = this.tag;
        if (i2 == 0 || i2 == this.zzhdk) {
            return Integer.MAX_VALUE;
        }
        return i2 >>> 3;
    }

    public final int getTag() {
        return this.tag;
    }

    public final boolean zzawt() throws IOException {
        int i;
        if (this.zzhdj.zzawk() || (i = this.tag) == this.zzhdk) {
            return false;
        }
        return this.zzhdj.zzfq(i);
    }

    private final void zzfv(int i) throws IOException {
        if ((this.tag & 7) != i) {
            throw zzdok.zzayi();
        }
    }

    public final double readDouble() throws IOException {
        zzfv(1);
        return this.zzhdj.readDouble();
    }

    public final float readFloat() throws IOException {
        zzfv(5);
        return this.zzhdj.readFloat();
    }

    public final long zzavv() throws IOException {
        zzfv(0);
        return this.zzhdj.zzavv();
    }

    public final long zzavw() throws IOException {
        zzfv(0);
        return this.zzhdj.zzavw();
    }

    public final int zzavx() throws IOException {
        zzfv(0);
        return this.zzhdj.zzavx();
    }

    public final long zzavy() throws IOException {
        zzfv(1);
        return this.zzhdj.zzavy();
    }

    public final int zzavz() throws IOException {
        zzfv(5);
        return this.zzhdj.zzavz();
    }

    public final boolean zzawa() throws IOException {
        zzfv(0);
        return this.zzhdj.zzawa();
    }

    public final String readString() throws IOException {
        zzfv(2);
        return this.zzhdj.readString();
    }

    public final String zzawb() throws IOException {
        zzfv(2);
        return this.zzhdj.zzawb();
    }

    public final <T> T zza(zzdqb<T> zzdqb, zzdno zzdno) throws IOException {
        zzfv(2);
        return zzc(zzdqb, zzdno);
    }

    public final <T> T zzb(zzdqb<T> zzdqb, zzdno zzdno) throws IOException {
        zzfv(3);
        return zzd(zzdqb, zzdno);
    }

    private final <T> T zzc(zzdqb<T> zzdqb, zzdno zzdno) throws IOException {
        int zzawd = this.zzhdj.zzawd();
        if (this.zzhdj.zzhcz < this.zzhdj.zzhda) {
            int zzfr = this.zzhdj.zzfr(zzawd);
            T newInstance = zzdqb.newInstance();
            this.zzhdj.zzhcz++;
            zzdqb.zza(newInstance, this, zzdno);
            zzdqb.zzaa(newInstance);
            this.zzhdj.zzfp(0);
            zzdnd zzdnd = this.zzhdj;
            zzdnd.zzhcz--;
            this.zzhdj.zzfs(zzfr);
            return newInstance;
        }
        throw new zzdok("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    private final <T> T zzd(zzdqb<T> zzdqb, zzdno zzdno) throws IOException {
        int i = this.zzhdk;
        this.zzhdk = ((this.tag >>> 3) << 3) | 4;
        try {
            T newInstance = zzdqb.newInstance();
            zzdqb.zza(newInstance, this, zzdno);
            zzdqb.zzaa(newInstance);
            if (this.tag == this.zzhdk) {
                return newInstance;
            }
            throw zzdok.zzayj();
        } finally {
            this.zzhdk = i;
        }
    }

    public final zzdmr zzawc() throws IOException {
        zzfv(2);
        return this.zzhdj.zzawc();
    }

    public final int zzawd() throws IOException {
        zzfv(0);
        return this.zzhdj.zzawd();
    }

    public final int zzawe() throws IOException {
        zzfv(0);
        return this.zzhdj.zzawe();
    }

    public final int zzawf() throws IOException {
        zzfv(5);
        return this.zzhdj.zzawf();
    }

    public final long zzawg() throws IOException {
        zzfv(1);
        return this.zzhdj.zzawg();
    }

    public final int zzawh() throws IOException {
        zzfv(0);
        return this.zzhdj.zzawh();
    }

    public final long zzawi() throws IOException {
        zzfv(0);
        return this.zzhdj.zzawi();
    }

    public final void zzi(List<Double> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdnl) {
            zzdnl zzdnl = (zzdnl) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzdnl.zzd(this.zzhdj.readDouble());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawd = this.zzhdj.zzawd();
                zzfw(zzawd);
                int zzawl = this.zzhdj.zzawl() + zzawd;
                do {
                    zzdnl.zzd(this.zzhdj.readDouble());
                } while (this.zzhdj.zzawl() < zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Double.valueOf(this.zzhdj.readDouble()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawd2 = this.zzhdj.zzawd();
                zzfw(zzawd2);
                int zzawl2 = this.zzhdj.zzawl() + zzawd2;
                do {
                    list.add(Double.valueOf(this.zzhdj.readDouble()));
                } while (this.zzhdj.zzawl() < zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzj(List<Float> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdny) {
            zzdny zzdny = (zzdny) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzawd = this.zzhdj.zzawd();
                zzfx(zzawd);
                int zzawl = this.zzhdj.zzawl() + zzawd;
                do {
                    zzdny.zzi(this.zzhdj.readFloat());
                } while (this.zzhdj.zzawl() < zzawl);
            } else if (i == 5) {
                do {
                    zzdny.zzi(this.zzhdj.readFloat());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzawd2 = this.zzhdj.zzawd();
                zzfx(zzawd2);
                int zzawl2 = this.zzhdj.zzawl() + zzawd2;
                do {
                    list.add(Float.valueOf(this.zzhdj.readFloat()));
                } while (this.zzhdj.zzawl() < zzawl2);
            } else if (i2 == 5) {
                do {
                    list.add(Float.valueOf(this.zzhdj.readFloat()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzk(List<Long> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoy) {
            zzdoy zzdoy = (zzdoy) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdoy.zzfu(this.zzhdj.zzavv());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdoy.zzfu(this.zzhdj.zzavv());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzhdj.zzavv()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Long.valueOf(this.zzhdj.zzavv()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzl(List<Long> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoy) {
            zzdoy zzdoy = (zzdoy) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdoy.zzfu(this.zzhdj.zzavw());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdoy.zzfu(this.zzhdj.zzavw());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzhdj.zzavw()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Long.valueOf(this.zzhdj.zzavw()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzm(List<Integer> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoc) {
            zzdoc zzdoc = (zzdoc) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdoc.zzgp(this.zzhdj.zzavx());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdoc.zzgp(this.zzhdj.zzavx());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzavx()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzavx()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzn(List<Long> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoy) {
            zzdoy zzdoy = (zzdoy) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzdoy.zzfu(this.zzhdj.zzavy());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawd = this.zzhdj.zzawd();
                zzfw(zzawd);
                int zzawl = this.zzhdj.zzawl() + zzawd;
                do {
                    zzdoy.zzfu(this.zzhdj.zzavy());
                } while (this.zzhdj.zzawl() < zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.zzhdj.zzavy()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawd2 = this.zzhdj.zzawd();
                zzfw(zzawd2);
                int zzawl2 = this.zzhdj.zzawl() + zzawd2;
                do {
                    list.add(Long.valueOf(this.zzhdj.zzavy()));
                } while (this.zzhdj.zzawl() < zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzo(List<Integer> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoc) {
            zzdoc zzdoc = (zzdoc) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzawd = this.zzhdj.zzawd();
                zzfx(zzawd);
                int zzawl = this.zzhdj.zzawl() + zzawd;
                do {
                    zzdoc.zzgp(this.zzhdj.zzavz());
                } while (this.zzhdj.zzawl() < zzawl);
            } else if (i == 5) {
                do {
                    zzdoc.zzgp(this.zzhdj.zzavz());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzawd2 = this.zzhdj.zzawd();
                zzfx(zzawd2);
                int zzawl2 = this.zzhdj.zzawl() + zzawd2;
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzavz()));
                } while (this.zzhdj.zzawl() < zzawl2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzavz()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzp(List<Boolean> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdmp) {
            zzdmp zzdmp = (zzdmp) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdmp.addBoolean(this.zzhdj.zzawa());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdmp.addBoolean(this.zzhdj.zzawa());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Boolean.valueOf(this.zzhdj.zzawa()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Boolean.valueOf(this.zzhdj.zzawa()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdng.zza(java.util.List<java.lang.String>, boolean):void
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.ads.zzdng.zza(com.google.android.gms.internal.ads.zzdqb, com.google.android.gms.internal.ads.zzdno):T
      com.google.android.gms.internal.ads.zzdqa.zza(com.google.android.gms.internal.ads.zzdqb, com.google.android.gms.internal.ads.zzdno):T
      com.google.android.gms.internal.ads.zzdng.zza(java.util.List<java.lang.String>, boolean):void */
    public final void readStringList(List<String> list) throws IOException {
        zza(list, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdng.zza(java.util.List<java.lang.String>, boolean):void
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.ads.zzdng.zza(com.google.android.gms.internal.ads.zzdqb, com.google.android.gms.internal.ads.zzdno):T
      com.google.android.gms.internal.ads.zzdqa.zza(com.google.android.gms.internal.ads.zzdqb, com.google.android.gms.internal.ads.zzdno):T
      com.google.android.gms.internal.ads.zzdng.zza(java.util.List<java.lang.String>, boolean):void */
    public final void zzq(List<String> list) throws IOException {
        zza(list, true);
    }

    private final void zza(List<String> list, boolean z) throws IOException {
        int zzavu;
        int zzavu2;
        if ((this.tag & 7) != 2) {
            throw zzdok.zzayi();
        } else if (!(list instanceof zzdot) || z) {
            do {
                list.add(z ? zzawb() : readString());
                if (!this.zzhdj.zzawk()) {
                    zzavu = this.zzhdj.zzavu();
                } else {
                    return;
                }
            } while (zzavu == this.tag);
            this.zzhdl = zzavu;
        } else {
            zzdot zzdot = (zzdot) list;
            do {
                zzdot.zzdb(zzawc());
                if (!this.zzhdj.zzawk()) {
                    zzavu2 = this.zzhdj.zzavu();
                } else {
                    return;
                }
            } while (zzavu2 == this.tag);
            this.zzhdl = zzavu2;
        }
    }

    public final <T> void zza(List<T> list, zzdqb<T> zzdqb, zzdno zzdno) throws IOException {
        int zzavu;
        int i = this.tag;
        if ((i & 7) == 2) {
            do {
                list.add(zzc(zzdqb, zzdno));
                if (!this.zzhdj.zzawk() && this.zzhdl == 0) {
                    zzavu = this.zzhdj.zzavu();
                } else {
                    return;
                }
            } while (zzavu == i);
            this.zzhdl = zzavu;
            return;
        }
        throw zzdok.zzayi();
    }

    public final <T> void zzb(List<T> list, zzdqb<T> zzdqb, zzdno zzdno) throws IOException {
        int zzavu;
        int i = this.tag;
        if ((i & 7) == 3) {
            do {
                list.add(zzd(zzdqb, zzdno));
                if (!this.zzhdj.zzawk() && this.zzhdl == 0) {
                    zzavu = this.zzhdj.zzavu();
                } else {
                    return;
                }
            } while (zzavu == i);
            this.zzhdl = zzavu;
            return;
        }
        throw zzdok.zzayi();
    }

    public final void zzr(List<zzdmr> list) throws IOException {
        int zzavu;
        if ((this.tag & 7) == 2) {
            do {
                list.add(zzawc());
                if (!this.zzhdj.zzawk()) {
                    zzavu = this.zzhdj.zzavu();
                } else {
                    return;
                }
            } while (zzavu == this.tag);
            this.zzhdl = zzavu;
            return;
        }
        throw zzdok.zzayi();
    }

    public final void zzs(List<Integer> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoc) {
            zzdoc zzdoc = (zzdoc) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdoc.zzgp(this.zzhdj.zzawd());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdoc.zzgp(this.zzhdj.zzawd());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawd()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawd()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzt(List<Integer> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoc) {
            zzdoc zzdoc = (zzdoc) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdoc.zzgp(this.zzhdj.zzawe());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdoc.zzgp(this.zzhdj.zzawe());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawe()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawe()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzu(List<Integer> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoc) {
            zzdoc zzdoc = (zzdoc) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzawd = this.zzhdj.zzawd();
                zzfx(zzawd);
                int zzawl = this.zzhdj.zzawl() + zzawd;
                do {
                    zzdoc.zzgp(this.zzhdj.zzawf());
                } while (this.zzhdj.zzawl() < zzawl);
            } else if (i == 5) {
                do {
                    zzdoc.zzgp(this.zzhdj.zzawf());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzawd2 = this.zzhdj.zzawd();
                zzfx(zzawd2);
                int zzawl2 = this.zzhdj.zzawl() + zzawd2;
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawf()));
                } while (this.zzhdj.zzawl() < zzawl2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawf()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzv(List<Long> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoy) {
            zzdoy zzdoy = (zzdoy) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzdoy.zzfu(this.zzhdj.zzawg());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawd = this.zzhdj.zzawd();
                zzfw(zzawd);
                int zzawl = this.zzhdj.zzawl() + zzawd;
                do {
                    zzdoy.zzfu(this.zzhdj.zzawg());
                } while (this.zzhdj.zzawl() < zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.zzhdj.zzawg()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawd2 = this.zzhdj.zzawd();
                zzfw(zzawd2);
                int zzawl2 = this.zzhdj.zzawl() + zzawd2;
                do {
                    list.add(Long.valueOf(this.zzhdj.zzawg()));
                } while (this.zzhdj.zzawl() < zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzw(List<Integer> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoc) {
            zzdoc zzdoc = (zzdoc) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdoc.zzgp(this.zzhdj.zzawh());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdoc.zzgp(this.zzhdj.zzawh());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawh()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Integer.valueOf(this.zzhdj.zzawh()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    public final void zzx(List<Long> list) throws IOException {
        int zzavu;
        int zzavu2;
        if (list instanceof zzdoy) {
            zzdoy zzdoy = (zzdoy) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdoy.zzfu(this.zzhdj.zzawi());
                    if (!this.zzhdj.zzawk()) {
                        zzavu2 = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu2 == this.tag);
                this.zzhdl = zzavu2;
            } else if (i == 2) {
                int zzawl = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    zzdoy.zzfu(this.zzhdj.zzawi());
                } while (this.zzhdj.zzawl() < zzawl);
                zzfy(zzawl);
            } else {
                throw zzdok.zzayi();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzhdj.zzawi()));
                    if (!this.zzhdj.zzawk()) {
                        zzavu = this.zzhdj.zzavu();
                    } else {
                        return;
                    }
                } while (zzavu == this.tag);
                this.zzhdl = zzavu;
            } else if (i2 == 2) {
                int zzawl2 = this.zzhdj.zzawl() + this.zzhdj.zzawd();
                do {
                    list.add(Long.valueOf(this.zzhdj.zzawi()));
                } while (this.zzhdj.zzawl() < zzawl2);
                zzfy(zzawl2);
            } else {
                throw zzdok.zzayi();
            }
        }
    }

    private static void zzfw(int i) throws IOException {
        if ((i & 7) != 0) {
            throw zzdok.zzayj();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0052, code lost:
        if (zzawt() != false) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        throw new com.google.android.gms.internal.ads.zzdok("Unable to parse map entry.");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <K, V> void zza(java.util.Map<K, V> r8, com.google.android.gms.internal.ads.zzdpd<K, V> r9, com.google.android.gms.internal.ads.zzdno r10) throws java.io.IOException {
        /*
            r7 = this;
            r0 = 2
            r7.zzfv(r0)
            com.google.android.gms.internal.ads.zzdnd r1 = r7.zzhdj
            int r1 = r1.zzawd()
            com.google.android.gms.internal.ads.zzdnd r2 = r7.zzhdj
            int r1 = r2.zzfr(r1)
            K r2 = r9.zzhjj
            V r3 = r9.zzckh
        L_0x0014:
            int r4 = r7.zzaws()     // Catch:{ all -> 0x0064 }
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r4 == r5) goto L_0x005b
            com.google.android.gms.internal.ads.zzdnd r5 = r7.zzhdj     // Catch:{ all -> 0x0064 }
            boolean r5 = r5.zzawk()     // Catch:{ all -> 0x0064 }
            if (r5 != 0) goto L_0x005b
            r5 = 1
            java.lang.String r6 = "Unable to parse map entry."
            if (r4 == r5) goto L_0x0046
            if (r4 == r0) goto L_0x0039
            boolean r4 = r7.zzawt()     // Catch:{ zzdol -> 0x004e }
            if (r4 == 0) goto L_0x0033
            goto L_0x0014
        L_0x0033:
            com.google.android.gms.internal.ads.zzdok r4 = new com.google.android.gms.internal.ads.zzdok     // Catch:{ zzdol -> 0x004e }
            r4.<init>(r6)     // Catch:{ zzdol -> 0x004e }
            throw r4     // Catch:{ zzdol -> 0x004e }
        L_0x0039:
            com.google.android.gms.internal.ads.zzdri r4 = r9.zzhjk     // Catch:{ zzdol -> 0x004e }
            V r5 = r9.zzckh     // Catch:{ zzdol -> 0x004e }
            java.lang.Class r5 = r5.getClass()     // Catch:{ zzdol -> 0x004e }
            java.lang.Object r3 = r7.zza(r4, r5, r10)     // Catch:{ zzdol -> 0x004e }
            goto L_0x0014
        L_0x0046:
            com.google.android.gms.internal.ads.zzdri r4 = r9.zzhji     // Catch:{ zzdol -> 0x004e }
            r5 = 0
            java.lang.Object r2 = r7.zza(r4, r5, r5)     // Catch:{ zzdol -> 0x004e }
            goto L_0x0014
        L_0x004e:
            boolean r4 = r7.zzawt()     // Catch:{ all -> 0x0064 }
            if (r4 == 0) goto L_0x0055
            goto L_0x0014
        L_0x0055:
            com.google.android.gms.internal.ads.zzdok r8 = new com.google.android.gms.internal.ads.zzdok     // Catch:{ all -> 0x0064 }
            r8.<init>(r6)     // Catch:{ all -> 0x0064 }
            throw r8     // Catch:{ all -> 0x0064 }
        L_0x005b:
            r8.put(r2, r3)     // Catch:{ all -> 0x0064 }
            com.google.android.gms.internal.ads.zzdnd r8 = r7.zzhdj
            r8.zzfs(r1)
            return
        L_0x0064:
            r8 = move-exception
            com.google.android.gms.internal.ads.zzdnd r9 = r7.zzhdj
            r9.zzfs(r1)
            goto L_0x006c
        L_0x006b:
            throw r8
        L_0x006c:
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdng.zza(java.util.Map, com.google.android.gms.internal.ads.zzdpd, com.google.android.gms.internal.ads.zzdno):void");
    }

    private final Object zza(zzdri zzdri, Class<?> cls, zzdno zzdno) throws IOException {
        switch (zzdnh.zzhdm[zzdri.ordinal()]) {
            case 1:
                return Boolean.valueOf(zzawa());
            case 2:
                return zzawc();
            case 3:
                return Double.valueOf(readDouble());
            case 4:
                return Integer.valueOf(zzawe());
            case 5:
                return Integer.valueOf(zzavz());
            case 6:
                return Long.valueOf(zzavy());
            case 7:
                return Float.valueOf(readFloat());
            case 8:
                return Integer.valueOf(zzavx());
            case 9:
                return Long.valueOf(zzavw());
            case 10:
                zzfv(2);
                return zzc(zzdpx.zzazg().zzg(cls), zzdno);
            case 11:
                return Integer.valueOf(zzawf());
            case 12:
                return Long.valueOf(zzawg());
            case 13:
                return Integer.valueOf(zzawh());
            case 14:
                return Long.valueOf(zzawi());
            case 15:
                return zzawb();
            case 16:
                return Integer.valueOf(zzawd());
            case 17:
                return Long.valueOf(zzavv());
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    private static void zzfx(int i) throws IOException {
        if ((i & 3) != 0) {
            throw zzdok.zzayj();
        }
    }

    private final void zzfy(int i) throws IOException {
        if (this.zzhdj.zzawl() != i) {
            throw zzdok.zzayd();
        }
    }
}
