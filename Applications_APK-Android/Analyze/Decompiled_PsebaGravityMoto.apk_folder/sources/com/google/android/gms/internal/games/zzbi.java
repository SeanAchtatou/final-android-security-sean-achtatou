package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zze;

final class zzbi extends zzbm {
    private final /* synthetic */ int zzjs;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbi(zzbe zzbe, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient);
        this.zzjs = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.Players$LoadPlayersResult>, int, boolean, boolean):void
     arg types: [com.google.android.gms.internal.games.zzbi, int, int, int]
     candidates:
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.ListenerHolder<com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer$ReliableMessageSentCallback>, byte[], java.lang.String, java.lang.String):int
      com.google.android.gms.games.internal.zze.zza(java.lang.String, boolean, boolean, int):android.content.Intent
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.request.Requests$LoadRequestsResult>, int, int, int):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.leaderboard.Leaderboards$LoadScoresResult>, com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer, int, int):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.leaderboard.Leaderboards$SubmitScoreResult>, java.lang.String, long, java.lang.String):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.snapshot.Snapshots$OpenSnapshotResult>, java.lang.String, boolean, int):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$UpdateMatchResult>, java.lang.String, byte[], com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.quest.Quests$LoadQuestsResult>, int[], int, boolean):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.ListenerHolder<? extends com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener>, com.google.android.gms.common.api.internal.ListenerHolder<? extends com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener>, com.google.android.gms.common.api.internal.ListenerHolder<? extends com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener>, com.google.android.gms.games.multiplayer.realtime.RoomConfig):void
      com.google.android.gms.common.internal.BaseGmsClient.zza(com.google.android.gms.common.internal.BaseGmsClient, int, int, android.os.IInterface):boolean
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.Players$LoadPlayersResult>, int, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza((BaseImplementation.ResultHolder<Players.LoadPlayersResult>) this, this.zzjs, true, false);
    }
}
