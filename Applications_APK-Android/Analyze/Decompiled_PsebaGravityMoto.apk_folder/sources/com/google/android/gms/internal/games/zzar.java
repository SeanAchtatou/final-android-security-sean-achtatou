package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzar extends zzay {
    private final /* synthetic */ String zzbq;
    private final /* synthetic */ boolean zzjg;
    private final /* synthetic */ int zzjm;
    private final /* synthetic */ int zzjn;
    private final /* synthetic */ int zzjo;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzar(zzam zzam, GoogleApiClient googleApiClient, String str, int i, int i2, int i3, boolean z) {
        super(googleApiClient, null);
        this.zzbq = str;
        this.zzjm = i;
        this.zzjn = i2;
        this.zzjo = i3;
        this.zzjg = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzbq, this.zzjm, this.zzjn, this.zzjo, this.zzjg);
    }
}
