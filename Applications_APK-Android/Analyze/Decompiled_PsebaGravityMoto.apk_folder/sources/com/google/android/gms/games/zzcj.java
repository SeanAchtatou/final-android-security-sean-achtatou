package com.google.android.gms.games;

import androidx.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcj implements PendingResultUtil.ResultConverter<TurnBasedMultiplayer.LeaveMatchResult, Void> {
    zzcj() {
    }

    public final /* bridge */ /* synthetic */ Object convert(@Nullable Result result) {
        return null;
    }
}
