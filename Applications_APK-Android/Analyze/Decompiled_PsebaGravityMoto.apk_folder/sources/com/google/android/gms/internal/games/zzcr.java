package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzcr implements Snapshots.DeleteSnapshotResult {
    private final /* synthetic */ Status zzbc;

    zzcr(zzcq zzcq, Status status) {
        this.zzbc = status;
    }

    public final String getSnapshotId() {
        return null;
    }

    public final Status getStatus() {
        return this.zzbc;
    }
}
