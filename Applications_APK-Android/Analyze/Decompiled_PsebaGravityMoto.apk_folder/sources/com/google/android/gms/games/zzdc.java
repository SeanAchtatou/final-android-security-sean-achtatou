package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;

final class zzdc implements PendingResultUtil.ResultConverter<Videos.CaptureCapabilitiesResult, VideoCapabilities> {
    zzdc() {
    }

    public final /* synthetic */ Object convert(Result result) {
        Videos.CaptureCapabilitiesResult captureCapabilitiesResult = (Videos.CaptureCapabilitiesResult) result;
        if (captureCapabilitiesResult == null) {
            return null;
        }
        return captureCapabilitiesResult.getCapabilities();
    }
}
