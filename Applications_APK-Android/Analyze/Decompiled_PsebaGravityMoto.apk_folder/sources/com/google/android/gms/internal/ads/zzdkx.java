package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdky;
import com.google.android.gms.security.ProviderInstaller;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;

public final class zzdkx<T_WRAPPER extends zzdky<T_ENGINE>, T_ENGINE> {
    private static final Logger logger = Logger.getLogger(zzdkx.class.getName());
    private static final List<Provider> zzhao;
    public static final zzdkx<zzdkz, Cipher> zzhap = new zzdkx<>(new zzdkz());
    public static final zzdkx<zzdld, Mac> zzhaq = new zzdkx<>(new zzdld());
    public static final zzdkx<zzdlf, Signature> zzhar = new zzdkx<>(new zzdlf());
    public static final zzdkx<zzdle, MessageDigest> zzhas = new zzdkx<>(new zzdle());
    public static final zzdkx<zzdla, KeyAgreement> zzhat = new zzdkx<>(new zzdla());
    public static final zzdkx<zzdlc, KeyPairGenerator> zzhau = new zzdkx<>(new zzdlc());
    public static final zzdkx<zzdlb, KeyFactory> zzhav = new zzdkx<>(new zzdlb());
    private T_WRAPPER zzhaw;
    private List<Provider> zzhax = zzhao;
    private boolean zzhay = true;

    private zzdkx(T_WRAPPER t_wrapper) {
        this.zzhaw = t_wrapper;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T_WRAPPER
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final T_ENGINE zzgt(java.lang.String r4) throws java.security.GeneralSecurityException {
        /*
            r3 = this;
            java.util.List<java.security.Provider> r0 = r3.zzhax
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x001f
            java.lang.Object r1 = r0.next()
            java.security.Provider r1 = (java.security.Provider) r1
            boolean r2 = r3.zza(r4, r1)
            if (r2 == 0) goto L_0x0006
            T_WRAPPER r0 = r3.zzhaw
            java.lang.Object r4 = r0.zzb(r4, r1)
            return r4
        L_0x001f:
            boolean r0 = r3.zzhay
            if (r0 == 0) goto L_0x002b
            T_WRAPPER r0 = r3.zzhaw
            r1 = 0
            java.lang.Object r4 = r0.zzb(r4, r1)
            return r4
        L_0x002b:
            java.security.GeneralSecurityException r4 = new java.security.GeneralSecurityException
            java.lang.String r0 = "No good Provider found."
            r4.<init>(r0)
            goto L_0x0034
        L_0x0033:
            throw r4
        L_0x0034:
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdkx.zzgt(java.lang.String):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T_WRAPPER
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final boolean zza(java.lang.String r2, java.security.Provider r3) {
        /*
            r1 = this;
            T_WRAPPER r0 = r1.zzhaw     // Catch:{ Exception -> 0x0007 }
            r0.zzb(r2, r3)     // Catch:{ Exception -> 0x0007 }
            r2 = 1
            return r2
        L_0x0007:
            r2 = move-exception
            com.google.android.gms.internal.ads.zzdmb.zzg(r2)
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdkx.zza(java.lang.String, java.security.Provider):boolean");
    }

    static {
        if (zzdlv.zzavd()) {
            String[] strArr = {ProviderInstaller.PROVIDER_NAME, "AndroidOpenSSL"};
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 2; i++) {
                String str = strArr[i];
                Provider provider = Security.getProvider(str);
                if (provider != null) {
                    arrayList.add(provider);
                } else {
                    logger.logp(Level.INFO, "com.google.crypto.tink.subtle.EngineFactory", "toProviderList", String.format("Provider %s not available", str));
                }
            }
            zzhao = arrayList;
        } else {
            zzhao = new ArrayList();
        }
    }
}
