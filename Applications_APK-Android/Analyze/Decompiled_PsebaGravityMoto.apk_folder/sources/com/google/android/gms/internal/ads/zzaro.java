package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzaro extends zzfm implements zzarm {
    zzaro(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.request.IAdRequestService");
    }

    public final zzari zza(zzarg zzarg) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzfo.zza(obtainAndWriteInterfaceToken, zzarg);
        Parcel transactAndReadException = transactAndReadException(1, obtainAndWriteInterfaceToken);
        zzari zzari = (zzari) zzfo.zza(transactAndReadException, zzari.CREATOR);
        transactAndReadException.recycle();
        return zzari;
    }

    public final void zza(zzarg zzarg, zzarp zzarp) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzfo.zza(obtainAndWriteInterfaceToken, zzarg);
        zzfo.zza(obtainAndWriteInterfaceToken, zzarp);
        zza(2, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzarx zzarx, zzarr zzarr) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzfo.zza(obtainAndWriteInterfaceToken, zzarx);
        zzfo.zza(obtainAndWriteInterfaceToken, zzarr);
        zza(4, obtainAndWriteInterfaceToken);
    }

    public final void zzb(zzarx zzarx, zzarr zzarr) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzfo.zza(obtainAndWriteInterfaceToken, zzarx);
        zzfo.zza(obtainAndWriteInterfaceToken, zzarr);
        zza(5, obtainAndWriteInterfaceToken);
    }
}
