package com.google.android.gms.games;

import androidx.annotation.NonNull;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.internal.zzq;

final class zzw implements zzq<GamesMetadata.LoadGamesResult> {
    zzw() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        GamesMetadata.LoadGamesResult loadGamesResult = (GamesMetadata.LoadGamesResult) obj;
        if (loadGamesResult.getGames() != null) {
            loadGamesResult.getGames().release();
        }
    }
}
