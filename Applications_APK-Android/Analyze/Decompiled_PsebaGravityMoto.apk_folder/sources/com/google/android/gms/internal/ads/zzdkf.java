package com.google.android.gms.internal.ads;

public final class zzdkf {
    private final zzdlj zzgyw;
    private final zzdlj zzgyx;

    public zzdkf(byte[] bArr, byte[] bArr2) {
        this.zzgyw = zzdlj.zzv(bArr);
        this.zzgyx = zzdlj.zzv(bArr2);
    }

    public final byte[] zzauw() {
        zzdlj zzdlj = this.zzgyw;
        if (zzdlj == null) {
            return null;
        }
        return zzdlj.getBytes();
    }

    public final byte[] zzaux() {
        zzdlj zzdlj = this.zzgyx;
        if (zzdlj == null) {
            return null;
        }
        return zzdlj.getBytes();
    }
}
