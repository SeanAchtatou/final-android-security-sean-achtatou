package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzuo;
import com.google.android.gms.internal.measurement.zzuo.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class zzuo<MessageType extends zzuo<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzsx<MessageType, BuilderType> {
    private static Map<Object, zzuo<?, ?>> zzbyh = new ConcurrentHashMap();
    protected zzxe zzbyf = zzxe.zzyl();
    private int zzbyg = -1;

    public static abstract class zzc<MessageType extends zzc<MessageType, BuilderType>, BuilderType> extends zzuo<MessageType, BuilderType> implements zzvx {
        protected zzuf<Object> zzbyl = zzuf.zzvw();
    }

    public static class zzd<ContainingType extends zzvv, Type> extends zztz<ContainingType, Type> {
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class zze {
        public static final int zzbym = 1;
        public static final int zzbyn = 2;
        public static final int zzbyo = 3;
        public static final int zzbyp = 4;
        public static final int zzbyq = 5;
        public static final int zzbyr = 6;
        public static final int zzbys = 7;
        private static final /* synthetic */ int[] zzbyt = {zzbym, zzbyn, zzbyo, zzbyp, zzbyq, zzbyr, zzbys};
        public static final int zzbyu = 1;
        public static final int zzbyv = 2;
        private static final /* synthetic */ int[] zzbyw = {zzbyu, zzbyv};
        public static final int zzbyx = 1;
        public static final int zzbyy = 2;
        private static final /* synthetic */ int[] zzbyz = {zzbyx, zzbyy};

        public static int[] zzwp() {
            return (int[]) zzbyt.clone();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    public static class zzb<T extends zzuo<T, ?>> extends zzsz<T> {
        private final T zzbyi;

        public zzb(T t) {
            this.zzbyi = t;
        }

        public final /* synthetic */ Object zza(zztq zztq, zzub zzub) throws zzuv {
            return zzuo.zza(this.zzbyi, zztq, zzub);
        }
    }

    public String toString() {
        return zzvy.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzbtk != 0) {
            return this.zzbtk;
        }
        this.zzbtk = zzwh.zzxt().zzak(this).hashCode(this);
        return this.zzbtk;
    }

    public static abstract class zza<MessageType extends zzuo<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzsy<MessageType, BuilderType> {
        private final MessageType zzbyi;
        protected MessageType zzbyj;
        private boolean zzbyk = false;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected zza(MessageType r3) {
            /*
                r2 = this;
                r2.<init>()
                r2.zzbyi = r3
                int r0 = com.google.android.gms.internal.measurement.zzuo.zze.zzbyp
                r1 = 0
                java.lang.Object r3 = r3.zza(r0, r1, r1)
                com.google.android.gms.internal.measurement.zzuo r3 = (com.google.android.gms.internal.measurement.zzuo) r3
                r2.zzbyj = r3
                r3 = 0
                r2.zzbyk = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzuo.zza.<init>(com.google.android.gms.internal.measurement.zzuo):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected final void zzwk() {
            /*
                r3 = this;
                boolean r0 = r3.zzbyk
                if (r0 == 0) goto L_0x0019
                MessageType r0 = r3.zzbyj
                int r1 = com.google.android.gms.internal.measurement.zzuo.zze.zzbyp
                r2 = 0
                java.lang.Object r0 = r0.zza(r1, r2, r2)
                com.google.android.gms.internal.measurement.zzuo r0 = (com.google.android.gms.internal.measurement.zzuo) r0
                MessageType r1 = r3.zzbyj
                zza(r0, r1)
                r3.zzbyj = r0
                r0 = 0
                r3.zzbyk = r0
            L_0x0019:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzuo.zza.zzwk():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.zzuo.zza(com.google.android.gms.internal.measurement.zzuo, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          com.google.android.gms.internal.measurement.zzuo.zza(java.lang.Class, com.google.android.gms.internal.measurement.zzuo):void
          com.google.android.gms.internal.measurement.zzuo.zza(com.google.android.gms.internal.measurement.zzuo, boolean):boolean */
        public final boolean isInitialized() {
            return zzuo.zza((zzuo) this.zzbyj, false);
        }

        /* renamed from: zzwl */
        public MessageType zzwn() {
            if (this.zzbyk) {
                return this.zzbyj;
            }
            MessageType messagetype = this.zzbyj;
            zzwh.zzxt().zzak(messagetype).zzy(messagetype);
            this.zzbyk = true;
            return this.zzbyj;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        /* renamed from: zzwm */
        public final MessageType zzwo() {
            /*
                r5 = this;
                com.google.android.gms.internal.measurement.zzvv r0 = r5.zzwn()
                com.google.android.gms.internal.measurement.zzuo r0 = (com.google.android.gms.internal.measurement.zzuo) r0
                java.lang.Boolean r1 = java.lang.Boolean.TRUE
                boolean r1 = r1.booleanValue()
                int r2 = com.google.android.gms.internal.measurement.zzuo.zze.zzbym
                r3 = 0
                java.lang.Object r2 = r0.zza(r2, r3, r3)
                java.lang.Byte r2 = (java.lang.Byte) r2
                byte r2 = r2.byteValue()
                r4 = 1
                if (r2 != r4) goto L_0x001d
                goto L_0x0039
            L_0x001d:
                if (r2 != 0) goto L_0x0021
                r4 = 0
                goto L_0x0039
            L_0x0021:
                com.google.android.gms.internal.measurement.zzwh r2 = com.google.android.gms.internal.measurement.zzwh.zzxt()
                com.google.android.gms.internal.measurement.zzwl r2 = r2.zzak(r0)
                boolean r4 = r2.zzaj(r0)
                if (r1 == 0) goto L_0x0039
                int r1 = com.google.android.gms.internal.measurement.zzuo.zze.zzbyn
                if (r4 == 0) goto L_0x0035
                r2 = r0
                goto L_0x0036
            L_0x0035:
                r2 = r3
            L_0x0036:
                r0.zza(r1, r2, r3)
            L_0x0039:
                if (r4 == 0) goto L_0x003c
                return r0
            L_0x003c:
                com.google.android.gms.internal.measurement.zzxc r1 = new com.google.android.gms.internal.measurement.zzxc
                r1.<init>(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzuo.zza.zzwo():com.google.android.gms.internal.measurement.zzuo");
        }

        public final BuilderType zza(MessageType messagetype) {
            zzwk();
            zza(this.zzbyj, messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzwh.zzxt().zzak(messagetype).zzd(messagetype, messagetype2);
        }

        public final /* synthetic */ zzsy zzty() {
            return (zza) clone();
        }

        public final /* synthetic */ zzvv zzwj() {
            return this.zzbyi;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zza zza = (zza) ((zzuo) this.zzbyi).zza(zze.zzbyq, (Object) null, (Object) null);
            zza.zza((zzuo) zzwn());
            return zza;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzuo) zza(zze.zzbyr, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return zzwh.zzxt().zzak(this).equals(this, (zzuo) obj);
    }

    public final boolean isInitialized() {
        boolean booleanValue = Boolean.TRUE.booleanValue();
        byte byteValue = ((Byte) zza(zze.zzbym, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzaj = zzwh.zzxt().zzak(this).zzaj(this);
        if (booleanValue) {
            zza(zze.zzbyn, zzaj ? this : null, (Object) null);
        }
        return zzaj;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: BuilderType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final BuilderType zzwf() {
        /*
            r2 = this;
            int r0 = com.google.android.gms.internal.measurement.zzuo.zze.zzbyq
            r1 = 0
            java.lang.Object r0 = r2.zza(r0, r1, r1)
            com.google.android.gms.internal.measurement.zzuo$zza r0 = (com.google.android.gms.internal.measurement.zzuo.zza) r0
            r0.zza(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzuo.zzwf():com.google.android.gms.internal.measurement.zzuo$zza");
    }

    /* access modifiers changed from: package-private */
    public final int zztx() {
        return this.zzbyg;
    }

    /* access modifiers changed from: package-private */
    public final void zzai(int i) {
        this.zzbyg = i;
    }

    public final void zzb(zztv zztv) throws IOException {
        zzwh.zzxt().zzi(getClass()).zza(this, zztx.zza(zztv));
    }

    public final int zzvx() {
        if (this.zzbyg == -1) {
            this.zzbyg = zzwh.zzxt().zzak(this).zzai(this);
        }
        return this.zzbyg;
    }

    static <T extends zzuo<?, ?>> T zzg(Class<T> cls) {
        T t = (zzuo) zzbyh.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzuo) zzbyh.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzuo) ((zzuo) zzxj.zzk(cls)).zza(zze.zzbyr, (Object) null, (Object) null);
            if (t != null) {
                zzbyh.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends zzuo<?, ?>> void zza(Class<T> cls, T t) {
        zzbyh.put(cls, t);
    }

    protected static Object zza(zzvv zzvv, String str, Object[] objArr) {
        return new zzwj(zzvv, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static final <T extends com.google.android.gms.internal.measurement.zzuo<T, ?>> boolean zza(T r1, boolean r2) {
        /*
            int r2 = com.google.android.gms.internal.measurement.zzuo.zze.zzbym
            r0 = 0
            java.lang.Object r2 = r1.zza(r2, r0, r0)
            java.lang.Byte r2 = (java.lang.Byte) r2
            byte r2 = r2.byteValue()
            r0 = 1
            if (r2 != r0) goto L_0x0011
            return r0
        L_0x0011:
            if (r2 != 0) goto L_0x0015
            r1 = 0
            return r1
        L_0x0015:
            com.google.android.gms.internal.measurement.zzwh r2 = com.google.android.gms.internal.measurement.zzwh.zzxt()
            com.google.android.gms.internal.measurement.zzwl r2 = r2.zzak(r1)
            boolean r1 = r2.zzaj(r1)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzuo.zza(com.google.android.gms.internal.measurement.zzuo, boolean):boolean");
    }

    protected static <E> zzuu<E> zzwg() {
        return zzwi.zzxu();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    static <T extends com.google.android.gms.internal.measurement.zzuo<T, ?>> T zza(T r2, com.google.android.gms.internal.measurement.zztq r3, com.google.android.gms.internal.measurement.zzub r4) throws com.google.android.gms.internal.measurement.zzuv {
        /*
            int r0 = com.google.android.gms.internal.measurement.zzuo.zze.zzbyp
            r1 = 0
            java.lang.Object r2 = r2.zza(r0, r1, r1)
            com.google.android.gms.internal.measurement.zzuo r2 = (com.google.android.gms.internal.measurement.zzuo) r2
            com.google.android.gms.internal.measurement.zzwh r0 = com.google.android.gms.internal.measurement.zzwh.zzxt()     // Catch:{ IOException -> 0x0035, RuntimeException -> 0x0024 }
            com.google.android.gms.internal.measurement.zzwl r0 = r0.zzak(r2)     // Catch:{ IOException -> 0x0035, RuntimeException -> 0x0024 }
            com.google.android.gms.internal.measurement.zztt r3 = com.google.android.gms.internal.measurement.zztt.zza(r3)     // Catch:{ IOException -> 0x0035, RuntimeException -> 0x0024 }
            r0.zza(r2, r3, r4)     // Catch:{ IOException -> 0x0035, RuntimeException -> 0x0024 }
            com.google.android.gms.internal.measurement.zzwh r3 = com.google.android.gms.internal.measurement.zzwh.zzxt()     // Catch:{ IOException -> 0x0035, RuntimeException -> 0x0024 }
            com.google.android.gms.internal.measurement.zzwl r3 = r3.zzak(r2)     // Catch:{ IOException -> 0x0035, RuntimeException -> 0x0024 }
            r3.zzy(r2)     // Catch:{ IOException -> 0x0035, RuntimeException -> 0x0024 }
            return r2
        L_0x0024:
            r2 = move-exception
            java.lang.Throwable r3 = r2.getCause()
            boolean r3 = r3 instanceof com.google.android.gms.internal.measurement.zzuv
            if (r3 == 0) goto L_0x0034
            java.lang.Throwable r2 = r2.getCause()
            com.google.android.gms.internal.measurement.zzuv r2 = (com.google.android.gms.internal.measurement.zzuv) r2
            throw r2
        L_0x0034:
            throw r2
        L_0x0035:
            r3 = move-exception
            java.lang.Throwable r4 = r3.getCause()
            boolean r4 = r4 instanceof com.google.android.gms.internal.measurement.zzuv
            if (r4 == 0) goto L_0x0045
            java.lang.Throwable r2 = r3.getCause()
            com.google.android.gms.internal.measurement.zzuv r2 = (com.google.android.gms.internal.measurement.zzuv) r2
            throw r2
        L_0x0045:
            com.google.android.gms.internal.measurement.zzuv r4 = new com.google.android.gms.internal.measurement.zzuv
            java.lang.String r3 = r3.getMessage()
            r4.<init>(r3)
            com.google.android.gms.internal.measurement.zzuv r2 = r4.zzg(r2)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzuo.zza(com.google.android.gms.internal.measurement.zzuo, com.google.android.gms.internal.measurement.zztq, com.google.android.gms.internal.measurement.zzub):com.google.android.gms.internal.measurement.zzuo");
    }

    public final /* synthetic */ zzvw zzwh() {
        zza zza2 = (zza) zza(zze.zzbyq, (Object) null, (Object) null);
        zza2.zza(this);
        return zza2;
    }

    public final /* synthetic */ zzvw zzwi() {
        return (zza) zza(zze.zzbyq, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzvv zzwj() {
        return (zzuo) zza(zze.zzbyr, (Object) null, (Object) null);
    }
}
