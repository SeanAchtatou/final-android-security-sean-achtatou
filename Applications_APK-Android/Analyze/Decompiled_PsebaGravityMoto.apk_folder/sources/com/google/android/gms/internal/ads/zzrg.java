package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicReference;

public final class zzrg extends zzrj {
    private static final int[] zzblb = new int[0];
    private final zzrn zzblc;
    private final AtomicReference<zzrh> zzbld;

    public zzrg() {
        this(null);
    }

    private static int zzf(int i, int i2) {
        if (i == -1) {
            return i2 == -1 ? 0 : -1;
        }
        if (i2 == -1) {
            return 1;
        }
        return i - i2;
    }

    private static boolean zzf(int i, boolean z) {
        int i2 = i & 3;
        if (i2 != 3) {
            return z && i2 == 2;
        }
        return true;
    }

    private zzrg(zzrn zzrn) {
        this.zzblc = null;
        this.zzbld = new AtomicReference<>(new zzrh());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzrg.zzf(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.ads.zzrg.zzf(int, int):int
      com.google.android.gms.internal.ads.zzrg.zzf(int, boolean):boolean */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0185, code lost:
        if (r9.zzatm <= r15) goto L_0x018a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01df  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01b3  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01bc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzrm[] zza(com.google.android.gms.internal.ads.zzlp[] r36, com.google.android.gms.internal.ads.zzrb[] r37, int[][][] r38) throws com.google.android.gms.internal.ads.zzku {
        /*
            r35 = this;
            r0 = r36
            int r1 = r0.length
            com.google.android.gms.internal.ads.zzrm[] r2 = new com.google.android.gms.internal.ads.zzrm[r1]
            r3 = r35
            java.util.concurrent.atomic.AtomicReference<com.google.android.gms.internal.ads.zzrh> r4 = r3.zzbld
            java.lang.Object r4 = r4.get()
            com.google.android.gms.internal.ads.zzrh r4 = (com.google.android.gms.internal.ads.zzrh) r4
            r6 = 0
            r7 = 0
        L_0x0011:
            r8 = 2
            if (r6 >= r1) goto L_0x0262
            r12 = r0[r6]
            int r12 = r12.getTrackType()
            if (r8 != r12) goto L_0x024c
            if (r7 != 0) goto L_0x023f
            r7 = r37[r6]
            r12 = r38[r6]
            int r13 = r4.zzbli
            int r14 = r4.zzblj
            int r15 = r4.zzblk
            int r10 = r4.viewportWidth
            int r8 = r4.viewportHeight
            boolean r5 = r4.zzbln
            boolean r9 = r4.zzbll
            boolean r11 = r4.zzblm
            r24 = r1
            r20 = r4
            r0 = 0
            r3 = 0
            r4 = 0
            r21 = 0
            r22 = -1
            r23 = -1
        L_0x003f:
            int r1 = r7.length
            if (r3 >= r1) goto L_0x021d
            com.google.android.gms.internal.ads.zzra r1 = r7.zzbg(r3)
            r25 = r7
            java.util.ArrayList r7 = new java.util.ArrayList
            r26 = r2
            int r2 = r1.length
            r7.<init>(r2)
            r27 = r6
            r2 = 0
        L_0x0055:
            int r6 = r1.length
            if (r2 >= r6) goto L_0x0063
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)
            r7.add(r6)
            int r2 = r2 + 1
            goto L_0x0055
        L_0x0063:
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r10 == r2) goto L_0x0139
            if (r8 != r2) goto L_0x006c
            goto L_0x0139
        L_0x006c:
            r28 = r0
            r6 = 0
        L_0x006f:
            int r0 = r1.length
            if (r6 >= r0) goto L_0x0103
            com.google.android.gms.internal.ads.zzlh r0 = r1.zzbf(r6)
            r29 = r4
            int r4 = r0.width
            if (r4 <= 0) goto L_0x00e9
            int r4 = r0.height
            if (r4 <= 0) goto L_0x00e9
            int r4 = r0.width
            r30 = r9
            int r9 = r0.height
            if (r5 == 0) goto L_0x00a3
            if (r4 <= r9) goto L_0x008f
            r31 = r5
            r5 = 1
            goto L_0x0092
        L_0x008f:
            r31 = r5
            r5 = 0
        L_0x0092:
            if (r10 <= r8) goto L_0x0098
            r32 = r8
            r8 = 1
            goto L_0x009b
        L_0x0098:
            r32 = r8
            r8 = 0
        L_0x009b:
            if (r5 == r8) goto L_0x00a7
            r5 = r10
            r33 = r5
            r8 = r32
            goto L_0x00ac
        L_0x00a3:
            r31 = r5
            r32 = r8
        L_0x00a7:
            r8 = r10
            r33 = r8
            r5 = r32
        L_0x00ac:
            int r10 = r4 * r5
            r34 = r15
            int r15 = r9 * r8
            if (r10 < r15) goto L_0x00bf
            android.graphics.Point r5 = new android.graphics.Point
            int r4 = com.google.android.gms.internal.ads.zzsy.zzb(r15, r4)
            r5.<init>(r8, r4)
            r4 = r5
            goto L_0x00c8
        L_0x00bf:
            android.graphics.Point r4 = new android.graphics.Point
            int r8 = com.google.android.gms.internal.ads.zzsy.zzb(r10, r9)
            r4.<init>(r8, r5)
        L_0x00c8:
            int r5 = r0.width
            int r8 = r0.height
            int r5 = r5 * r8
            int r8 = r0.width
            int r9 = r4.x
            float r9 = (float) r9
            r10 = 1065017672(0x3f7ae148, float:0.98)
            float r9 = r9 * r10
            int r9 = (int) r9
            if (r8 < r9) goto L_0x00f3
            int r0 = r0.height
            int r4 = r4.y
            float r4 = (float) r4
            float r4 = r4 * r10
            int r4 = (int) r4
            if (r0 < r4) goto L_0x00f3
            if (r5 >= r2) goto L_0x00f3
            r2 = r5
            goto L_0x00f3
        L_0x00e9:
            r31 = r5
            r32 = r8
            r30 = r9
            r33 = r10
            r34 = r15
        L_0x00f3:
            int r6 = r6 + 1
            r4 = r29
            r9 = r30
            r5 = r31
            r8 = r32
            r10 = r33
            r15 = r34
            goto L_0x006f
        L_0x0103:
            r29 = r4
            r31 = r5
            r32 = r8
            r30 = r9
            r33 = r10
            r34 = r15
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r2 == r0) goto L_0x0147
            int r0 = r7.size()
            r4 = 1
            int r0 = r0 - r4
        L_0x011a:
            if (r0 < 0) goto L_0x0147
            java.lang.Object r4 = r7.get(r0)
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            com.google.android.gms.internal.ads.zzlh r4 = r1.zzbf(r4)
            int r4 = r4.zzhc()
            r5 = -1
            if (r4 == r5) goto L_0x0133
            if (r4 <= r2) goto L_0x0136
        L_0x0133:
            r7.remove(r0)
        L_0x0136:
            int r0 = r0 + -1
            goto L_0x011a
        L_0x0139:
            r28 = r0
            r29 = r4
            r31 = r5
            r32 = r8
            r30 = r9
            r33 = r10
            r34 = r15
        L_0x0147:
            r0 = r12[r3]
            r5 = r21
            r6 = r22
            r8 = r23
            r4 = r29
            r2 = 0
        L_0x0152:
            int r9 = r1.length
            if (r2 >= r9) goto L_0x01ff
            r9 = r0[r2]
            boolean r9 = zzf(r9, r11)
            if (r9 == 0) goto L_0x01eb
            com.google.android.gms.internal.ads.zzlh r9 = r1.zzbf(r2)
            java.lang.Integer r10 = java.lang.Integer.valueOf(r2)
            boolean r10 = r7.contains(r10)
            if (r10 == 0) goto L_0x018c
            int r10 = r9.width
            r15 = -1
            if (r10 == r15) goto L_0x0175
            int r10 = r9.width
            if (r10 > r13) goto L_0x018c
        L_0x0175:
            int r10 = r9.height
            if (r10 == r15) goto L_0x017d
            int r10 = r9.height
            if (r10 > r14) goto L_0x018c
        L_0x017d:
            int r10 = r9.zzatm
            if (r10 == r15) goto L_0x0188
            int r10 = r9.zzatm
            r15 = r34
            if (r10 > r15) goto L_0x018e
            goto L_0x018a
        L_0x0188:
            r15 = r34
        L_0x018a:
            r10 = 1
            goto L_0x018f
        L_0x018c:
            r15 = r34
        L_0x018e:
            r10 = 0
        L_0x018f:
            if (r10 != 0) goto L_0x019b
            if (r30 == 0) goto L_0x0194
            goto L_0x019b
        L_0x0194:
            r23 = r0
            r21 = r1
            r22 = r4
            goto L_0x01f3
        L_0x019b:
            if (r10 == 0) goto L_0x01a3
            r21 = r1
            r22 = r4
            r1 = 2
            goto L_0x01a8
        L_0x01a3:
            r21 = r1
            r22 = r4
            r1 = 1
        L_0x01a8:
            r4 = r0[r2]
            r23 = r0
            r0 = 0
            boolean r4 = zzf(r4, r0)
            if (r4 == 0) goto L_0x01b5
            int r1 = r1 + 1000
        L_0x01b5:
            if (r1 <= r5) goto L_0x01b9
            r0 = 1
            goto L_0x01ba
        L_0x01b9:
            r0 = 0
        L_0x01ba:
            if (r1 != r5) goto L_0x01dd
            int r0 = r9.zzhc()
            if (r0 == r6) goto L_0x01cb
            int r0 = r9.zzhc()
            int r0 = zzf(r0, r6)
            goto L_0x01d1
        L_0x01cb:
            int r0 = r9.zzatm
            int r0 = zzf(r0, r8)
        L_0x01d1:
            if (r4 == 0) goto L_0x01d8
            if (r10 == 0) goto L_0x01d8
            if (r0 <= 0) goto L_0x01dc
            goto L_0x01da
        L_0x01d8:
            if (r0 >= 0) goto L_0x01dc
        L_0x01da:
            r0 = 1
            goto L_0x01dd
        L_0x01dc:
            r0 = 0
        L_0x01dd:
            if (r0 == 0) goto L_0x01f3
            int r8 = r9.zzatm
            int r6 = r9.zzhc()
            r5 = r1
            r28 = r2
            r4 = r21
            goto L_0x01f5
        L_0x01eb:
            r23 = r0
            r21 = r1
            r22 = r4
            r15 = r34
        L_0x01f3:
            r4 = r22
        L_0x01f5:
            int r2 = r2 + 1
            r34 = r15
            r1 = r21
            r0 = r23
            goto L_0x0152
        L_0x01ff:
            r22 = r4
            r15 = r34
            int r3 = r3 + 1
            r21 = r5
            r23 = r8
            r7 = r25
            r2 = r26
            r0 = r28
            r9 = r30
            r5 = r31
            r8 = r32
            r10 = r33
            r22 = r6
            r6 = r27
            goto L_0x003f
        L_0x021d:
            r28 = r0
            r26 = r2
            r29 = r4
            r27 = r6
            if (r29 != 0) goto L_0x022a
            r16 = 0
            goto L_0x0235
        L_0x022a:
            com.google.android.gms.internal.ads.zzri r10 = new com.google.android.gms.internal.ads.zzri
            r1 = r28
            r0 = r29
            r10.<init>(r0, r1)
            r16 = r10
        L_0x0235:
            r26[r27] = r16
            r0 = r26[r27]
            if (r0 == 0) goto L_0x023d
            r7 = 1
            goto L_0x0247
        L_0x023d:
            r7 = 0
            goto L_0x0247
        L_0x023f:
            r24 = r1
            r26 = r2
            r20 = r4
            r27 = r6
        L_0x0247:
            r0 = r37[r27]
            int r0 = r0.length
            goto L_0x0254
        L_0x024c:
            r24 = r1
            r26 = r2
            r20 = r4
            r27 = r6
        L_0x0254:
            int r6 = r27 + 1
            r3 = r35
            r0 = r36
            r4 = r20
            r1 = r24
            r2 = r26
            goto L_0x0011
        L_0x0262:
            r26 = r2
            r20 = r4
            r0 = r1
            r1 = 0
            r2 = 0
            r3 = 0
        L_0x026a:
            if (r1 >= r0) goto L_0x045b
            r4 = r36[r1]
            int r4 = r4.getTrackType()
            r5 = 3
            r6 = 1
            if (r4 == r6) goto L_0x03b1
            r6 = 2
            if (r4 == r6) goto L_0x03a9
            if (r4 == r5) goto L_0x02fc
            r4 = r36[r1]
            r4.getTrackType()
            r4 = r37[r1]
            r5 = r38[r1]
            r6 = r20
            boolean r7 = r6.zzblm
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
        L_0x028c:
            int r12 = r4.length
            if (r8 >= r12) goto L_0x02e6
            com.google.android.gms.internal.ads.zzra r12 = r4.zzbg(r8)
            r13 = r5[r8]
            r14 = r11
            r11 = r10
            r10 = r9
            r9 = 0
        L_0x029a:
            int r15 = r12.length
            if (r9 >= r15) goto L_0x02dc
            r15 = r13[r9]
            boolean r15 = zzf(r15, r7)
            if (r15 == 0) goto L_0x02d1
            com.google.android.gms.internal.ads.zzlh r15 = r12.zzbf(r9)
            int r15 = r15.zzaub
            r19 = 1
            r15 = r15 & 1
            if (r15 == 0) goto L_0x02b4
            r15 = 1
            goto L_0x02b5
        L_0x02b4:
            r15 = 0
        L_0x02b5:
            if (r15 == 0) goto L_0x02bb
            r24 = r0
            r15 = 2
            goto L_0x02be
        L_0x02bb:
            r24 = r0
            r15 = 1
        L_0x02be:
            r0 = r13[r9]
            r20 = r4
            r4 = 0
            boolean r0 = zzf(r0, r4)
            if (r0 == 0) goto L_0x02cb
            int r15 = r15 + 1000
        L_0x02cb:
            if (r15 <= r14) goto L_0x02d5
            r11 = r9
            r10 = r12
            r14 = r15
            goto L_0x02d5
        L_0x02d1:
            r24 = r0
            r20 = r4
        L_0x02d5:
            int r9 = r9 + 1
            r4 = r20
            r0 = r24
            goto L_0x029a
        L_0x02dc:
            r24 = r0
            r20 = r4
            int r8 = r8 + 1
            r9 = r10
            r10 = r11
            r11 = r14
            goto L_0x028c
        L_0x02e6:
            r24 = r0
            if (r9 != 0) goto L_0x02ec
            r0 = 0
            goto L_0x02f1
        L_0x02ec:
            com.google.android.gms.internal.ads.zzri r0 = new com.google.android.gms.internal.ads.zzri
            r0.<init>(r9, r10)
        L_0x02f1:
            r26[r1] = r0
            r18 = r2
            r2 = 0
            r5 = -1
            r15 = 0
            r17 = 2
            goto L_0x044e
        L_0x02fc:
            r24 = r0
            r6 = r20
            if (r3 != 0) goto L_0x03ad
            r0 = r37[r1]
            r3 = r38[r1]
            boolean r4 = r6.zzblm
            r7 = 0
            r9 = 0
            r10 = 0
            r11 = 0
        L_0x030c:
            int r12 = r0.length
            if (r7 >= r12) goto L_0x038c
            com.google.android.gms.internal.ads.zzra r12 = r0.zzbg(r7)
            r13 = r3[r7]
            r14 = r11
            r11 = r10
            r10 = r9
            r9 = 0
        L_0x031a:
            int r15 = r12.length
            if (r9 >= r15) goto L_0x0381
            r15 = r13[r9]
            boolean r15 = zzf(r15, r4)
            if (r15 == 0) goto L_0x0377
            com.google.android.gms.internal.ads.zzlh r15 = r12.zzbf(r9)
            int r5 = r15.zzaub
            r19 = 1
            r5 = r5 & 1
            if (r5 == 0) goto L_0x0334
            r5 = 1
            goto L_0x0335
        L_0x0334:
            r5 = 0
        L_0x0335:
            int r8 = r15.zzaub
            r17 = 2
            r8 = r8 & 2
            if (r8 == 0) goto L_0x0342
            r22 = r0
            r0 = 0
            r8 = 1
            goto L_0x0346
        L_0x0342:
            r22 = r0
            r0 = 0
            r8 = 0
        L_0x0346:
            boolean r23 = zza(r15, r0)
            if (r23 == 0) goto L_0x0356
            if (r5 == 0) goto L_0x0350
            r8 = 6
            goto L_0x0366
        L_0x0350:
            if (r8 != 0) goto L_0x0354
            r8 = 5
            goto L_0x0366
        L_0x0354:
            r8 = 4
            goto L_0x0366
        L_0x0356:
            if (r5 == 0) goto L_0x035a
            r8 = 3
            goto L_0x0366
        L_0x035a:
            if (r8 == 0) goto L_0x037b
            r0 = 0
            boolean r5 = zza(r15, r0)
            if (r5 == 0) goto L_0x0365
            r8 = 2
            goto L_0x0366
        L_0x0365:
            r8 = 1
        L_0x0366:
            r0 = r13[r9]
            r5 = 0
            boolean r0 = zzf(r0, r5)
            if (r0 == 0) goto L_0x0371
            int r8 = r8 + 1000
        L_0x0371:
            if (r8 <= r14) goto L_0x037b
            r14 = r8
            r11 = r9
            r10 = r12
            goto L_0x037b
        L_0x0377:
            r22 = r0
            r17 = 2
        L_0x037b:
            int r9 = r9 + 1
            r0 = r22
            r5 = 3
            goto L_0x031a
        L_0x0381:
            r22 = r0
            r17 = 2
            int r7 = r7 + 1
            r9 = r10
            r10 = r11
            r11 = r14
            r5 = 3
            goto L_0x030c
        L_0x038c:
            r17 = 2
            if (r9 != 0) goto L_0x0392
            r0 = 0
            goto L_0x0397
        L_0x0392:
            com.google.android.gms.internal.ads.zzri r0 = new com.google.android.gms.internal.ads.zzri
            r0.<init>(r9, r10)
        L_0x0397:
            r26[r1] = r0
            r0 = r26[r1]
            if (r0 == 0) goto L_0x039f
            r0 = 1
            goto L_0x03a0
        L_0x039f:
            r0 = 0
        L_0x03a0:
            r3 = r0
            r0 = r2
            r2 = 0
            r5 = -1
            r15 = 0
            r19 = 1
            goto L_0x0452
        L_0x03a9:
            r24 = r0
            r6 = r20
        L_0x03ad:
            r17 = 2
            goto L_0x0449
        L_0x03b1:
            r24 = r0
            r6 = r20
            r17 = 2
            if (r2 != 0) goto L_0x0449
            r0 = r37[r1]
            r2 = r38[r1]
            boolean r4 = r6.zzblm
            r5 = 0
            r7 = -1
            r8 = -1
            r9 = 0
        L_0x03c3:
            int r10 = r0.length
            if (r5 >= r10) goto L_0x042d
            com.google.android.gms.internal.ads.zzra r10 = r0.zzbg(r5)
            r11 = r2[r5]
            r12 = r9
            r9 = r8
            r8 = r7
            r7 = 0
        L_0x03d1:
            int r13 = r10.length
            if (r7 >= r13) goto L_0x041f
            r13 = r11[r7]
            boolean r13 = zzf(r13, r4)
            if (r13 == 0) goto L_0x0414
            com.google.android.gms.internal.ads.zzlh r13 = r10.zzbf(r7)
            r14 = r11[r7]
            int r15 = r13.zzaub
            r19 = 1
            r15 = r15 & 1
            if (r15 == 0) goto L_0x03f0
            r16 = r2
            r2 = 0
            r15 = 1
            goto L_0x03f4
        L_0x03f0:
            r16 = r2
            r2 = 0
            r15 = 0
        L_0x03f4:
            boolean r13 = zza(r13, r2)
            if (r13 == 0) goto L_0x0400
            if (r15 == 0) goto L_0x03fe
            r13 = 4
            goto L_0x0405
        L_0x03fe:
            r13 = 3
            goto L_0x0405
        L_0x0400:
            if (r15 == 0) goto L_0x0404
            r13 = 2
            goto L_0x0405
        L_0x0404:
            r13 = 1
        L_0x0405:
            r15 = 0
            boolean r14 = zzf(r14, r15)
            if (r14 == 0) goto L_0x040e
            int r13 = r13 + 1000
        L_0x040e:
            if (r13 <= r12) goto L_0x041a
            r8 = r5
            r9 = r7
            r12 = r13
            goto L_0x041a
        L_0x0414:
            r16 = r2
            r2 = 0
            r15 = 0
            r19 = 1
        L_0x041a:
            int r7 = r7 + 1
            r2 = r16
            goto L_0x03d1
        L_0x041f:
            r16 = r2
            r2 = 0
            r15 = 0
            r19 = 1
            int r5 = r5 + 1
            r7 = r8
            r8 = r9
            r9 = r12
            r2 = r16
            goto L_0x03c3
        L_0x042d:
            r2 = 0
            r5 = -1
            r15 = 0
            r19 = 1
            if (r7 != r5) goto L_0x0436
            r10 = r2
            goto L_0x043f
        L_0x0436:
            com.google.android.gms.internal.ads.zzra r0 = r0.zzbg(r7)
            com.google.android.gms.internal.ads.zzri r10 = new com.google.android.gms.internal.ads.zzri
            r10.<init>(r0, r8)
        L_0x043f:
            r26[r1] = r10
            r0 = r26[r1]
            if (r0 == 0) goto L_0x0447
            r0 = 1
            goto L_0x0452
        L_0x0447:
            r0 = 0
            goto L_0x0452
        L_0x0449:
            r18 = r2
            r2 = 0
            r5 = -1
            r15 = 0
        L_0x044e:
            r19 = 1
            r0 = r18
        L_0x0452:
            int r1 = r1 + 1
            r2 = r0
            r20 = r6
            r0 = r24
            goto L_0x026a
        L_0x045b:
            return r26
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzrg.zza(com.google.android.gms.internal.ads.zzlp[], com.google.android.gms.internal.ads.zzrb[], int[][][]):com.google.android.gms.internal.ads.zzrm[]");
    }

    private static boolean zza(zzlh zzlh, String str) {
        return str != null && TextUtils.equals(str, zzsy.zzbg(zzlh.zzauc));
    }
}
