package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;

public final class zzdtd {
    public static final zzdtd zzhuc = new zzdtd(1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzdtd zzhud = new zzdtd(0.0d, 1.0d, -1.0d, 0.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzdtd zzhue = new zzdtd(-1.0d, 0.0d, 0.0d, -1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzdtd zzhuf = new zzdtd(0.0d, -1.0d, 1.0d, 0.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private final double a;
    private final double b;
    private final double c;
    private final double d;
    private final double w;
    private final double zzhty;
    private final double zzhtz;
    private final double zzhua;
    private final double zzhub;

    private zzdtd(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10) {
        this.zzhty = d6;
        this.zzhtz = d7;
        this.w = d8;
        this.a = d2;
        this.b = d3;
        this.c = d4;
        this.d = d5;
        this.zzhua = d9;
        this.zzhub = d10;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        zzdtd zzdtd = (zzdtd) obj;
        return Double.compare(zzdtd.a, this.a) == 0 && Double.compare(zzdtd.b, this.b) == 0 && Double.compare(zzdtd.c, this.c) == 0 && Double.compare(zzdtd.d, this.d) == 0 && Double.compare(zzdtd.zzhua, this.zzhua) == 0 && Double.compare(zzdtd.zzhub, this.zzhub) == 0 && Double.compare(zzdtd.zzhty, this.zzhty) == 0 && Double.compare(zzdtd.zzhtz, this.zzhtz) == 0 && Double.compare(zzdtd.w, this.w) == 0;
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.zzhty);
        long doubleToLongBits2 = Double.doubleToLongBits(this.zzhtz);
        long doubleToLongBits3 = Double.doubleToLongBits(this.w);
        long doubleToLongBits4 = Double.doubleToLongBits(this.a);
        long doubleToLongBits5 = Double.doubleToLongBits(this.b);
        long doubleToLongBits6 = Double.doubleToLongBits(this.c);
        long doubleToLongBits7 = Double.doubleToLongBits(this.d);
        long doubleToLongBits8 = Double.doubleToLongBits(this.zzhua);
        long doubleToLongBits9 = Double.doubleToLongBits(this.zzhub);
        return (((((((((((((((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)))) * 31) + ((int) (doubleToLongBits4 ^ (doubleToLongBits4 >>> 32)))) * 31) + ((int) (doubleToLongBits5 ^ (doubleToLongBits5 >>> 32)))) * 31) + ((int) (doubleToLongBits6 ^ (doubleToLongBits6 >>> 32)))) * 31) + ((int) (doubleToLongBits7 ^ (doubleToLongBits7 >>> 32)))) * 31) + ((int) (doubleToLongBits8 ^ (doubleToLongBits8 >>> 32)))) * 31) + ((int) (doubleToLongBits9 ^ (doubleToLongBits9 >>> 32)));
    }

    public final String toString() {
        if (equals(zzhuc)) {
            return "Rotate 0°";
        }
        if (equals(zzhud)) {
            return "Rotate 90°";
        }
        if (equals(zzhue)) {
            return "Rotate 180°";
        }
        if (equals(zzhuf)) {
            return "Rotate 270°";
        }
        double d2 = this.zzhty;
        double d3 = this.zzhtz;
        double d4 = this.w;
        double d5 = this.a;
        double d6 = this.b;
        double d7 = this.c;
        double d8 = this.d;
        double d9 = this.zzhua;
        double d10 = this.zzhub;
        double d11 = d8;
        StringBuilder sb = new StringBuilder(260);
        sb.append("Matrix{u=");
        sb.append(d2);
        sb.append(", v=");
        sb.append(d3);
        sb.append(", w=");
        sb.append(d4);
        sb.append(", a=");
        sb.append(d5);
        sb.append(", b=");
        sb.append(d6);
        sb.append(", c=");
        sb.append(d7);
        sb.append(", d=");
        sb.append(d11);
        sb.append(", tx=");
        sb.append(d9);
        sb.append(", ty=");
        sb.append(d10);
        sb.append("}");
        return sb.toString();
    }

    public static zzdtd zzp(ByteBuffer byteBuffer) {
        double zzd = zzbc.zzd(byteBuffer);
        double zzd2 = zzbc.zzd(byteBuffer);
        double zze = zzbc.zze(byteBuffer);
        return new zzdtd(zzd, zzd2, zzbc.zzd(byteBuffer), zzbc.zzd(byteBuffer), zze, zzbc.zze(byteBuffer), zzbc.zze(byteBuffer), zzbc.zzd(byteBuffer), zzbc.zzd(byteBuffer));
    }
}
