package com.google.android.gms.games.internal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ApiExceptionUtil;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.GamesClientStatusCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.TimeUnit;

public final class zzi {
    private static final zzr zzim = zzo.zziz;

    public static <R, PendingR extends Result> Task<R> toTask(@NonNull PendingResult<PendingR> pendingResult, @NonNull PendingResultUtil.ResultConverter<PendingR, R> resultConverter) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.addStatusListener(new zzk(pendingResult, taskCompletionSource, resultConverter));
        return taskCompletionSource.getTask();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.google.android.gms.common.api.PendingResult, com.google.android.gms.common.api.PendingResult<PendingR>] */
    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.android.gms.common.internal.PendingResultUtil$ResultConverter<PendingR, R>, com.google.android.gms.common.internal.PendingResultUtil$ResultConverter] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <R, PendingR extends com.google.android.gms.common.api.Result> com.google.android.gms.tasks.Task<com.google.android.gms.games.AnnotatedData<R>> zza(@androidx.annotation.NonNull com.google.android.gms.common.api.PendingResult<PendingR> r1, @androidx.annotation.NonNull com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<PendingR, R> r2) {
        /*
            r0 = 0
            com.google.android.gms.tasks.Task r1 = zza(r1, r2, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.internal.zzi.zza(com.google.android.gms.common.api.PendingResult, com.google.android.gms.common.internal.PendingResultUtil$ResultConverter):com.google.android.gms.tasks.Task");
    }

    public static <R, PendingR extends Result> Task<AnnotatedData<R>> zza(@NonNull PendingResult pendingResult, @NonNull PendingResultUtil.ResultConverter resultConverter, @Nullable zzq zzq) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.addStatusListener(new zzl(pendingResult, taskCompletionSource, resultConverter, zzq));
        return taskCompletionSource.getTask();
    }

    public static <R, PendingR extends Result> Task<R> zza(@NonNull PendingResult pendingResult, @NonNull zzr zzr, @NonNull PendingResultUtil.ResultConverter resultConverter) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.addStatusListener(new zzn(zzr, pendingResult, taskCompletionSource, resultConverter));
        return taskCompletionSource.getTask();
    }

    public static <R, PendingR extends Result, ExceptionData> Task<R> zza(@NonNull PendingResult pendingResult, @NonNull zzr zzr, @NonNull PendingResultUtil.ResultConverter resultConverter, @NonNull PendingResultUtil.ResultConverter resultConverter2, @NonNull zzp zzp) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.addStatusListener(new zzj(pendingResult, zzr, taskCompletionSource, resultConverter, resultConverter2, zzp));
        return taskCompletionSource.getTask();
    }

    static final /* synthetic */ void zza(PendingResult pendingResult, zzr zzr, TaskCompletionSource taskCompletionSource, PendingResultUtil.ResultConverter resultConverter, PendingResultUtil.ResultConverter resultConverter2, zzp zzp, Status status) {
        Result await = pendingResult.await(0, TimeUnit.MILLISECONDS);
        if (zzr.zza(status)) {
            taskCompletionSource.setResult(resultConverter.convert(await));
            return;
        }
        Object convert = resultConverter2.convert(await);
        if (convert != null) {
            taskCompletionSource.setException(zzp.zza(zzc(status), convert));
        } else {
            taskCompletionSource.setException(ApiExceptionUtil.fromStatus(zzc(status)));
        }
    }

    static final /* synthetic */ void zza(PendingResult pendingResult, TaskCompletionSource taskCompletionSource, PendingResultUtil.ResultConverter resultConverter, Status status) {
        Result await = pendingResult.await(0, TimeUnit.MILLISECONDS);
        if (status.isSuccess()) {
            taskCompletionSource.setResult(resultConverter.convert(await));
        } else {
            taskCompletionSource.setException(ApiExceptionUtil.fromStatus(zzc(status)));
        }
    }

    static final /* synthetic */ void zza(PendingResult pendingResult, TaskCompletionSource taskCompletionSource, PendingResultUtil.ResultConverter resultConverter, zzq zzq, Status status) {
        boolean z = status.getStatusCode() == 3;
        Result await = pendingResult.await(0, TimeUnit.MILLISECONDS);
        if (status.isSuccess() || z) {
            taskCompletionSource.setResult(new AnnotatedData(resultConverter.convert(await), z));
            return;
        }
        if (!(await == null || zzq == null)) {
            zzq.release(await);
        }
        taskCompletionSource.setException(ApiExceptionUtil.fromStatus(zzc(status)));
    }

    static final /* synthetic */ void zza(PendingResultUtil.ResultConverter resultConverter, PendingResult pendingResult, TaskCompletionSource taskCompletionSource, Status status) {
        boolean z = status.getStatusCode() == 3;
        Releasable releasable = (Releasable) resultConverter.convert(pendingResult.await(0, TimeUnit.MILLISECONDS));
        if (status.isSuccess() || z) {
            taskCompletionSource.setResult(new AnnotatedData(releasable, z));
            return;
        }
        if (releasable != null) {
            releasable.release();
        }
        taskCompletionSource.setException(ApiExceptionUtil.fromStatus(zzc(status)));
    }

    static final /* synthetic */ void zza(zzr zzr, PendingResult pendingResult, TaskCompletionSource taskCompletionSource, PendingResultUtil.ResultConverter resultConverter, Status status) {
        if (zzr.zza(status)) {
            taskCompletionSource.setResult(resultConverter.convert(pendingResult.await(0, TimeUnit.MILLISECONDS)));
        } else {
            taskCompletionSource.setException(ApiExceptionUtil.fromStatus(zzc(status)));
        }
    }

    public static <R extends Releasable, PendingR extends Result> Task<AnnotatedData<R>> zzb(@NonNull PendingResult<PendingR> pendingResult, @NonNull PendingResultUtil.ResultConverter<PendingR, R> resultConverter) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.addStatusListener(new zzm(resultConverter, pendingResult, taskCompletionSource));
        return taskCompletionSource.getTask();
    }

    private static Status zzc(@NonNull Status status) {
        int zzb = GamesClientStatusCodes.zzb(status.getStatusCode());
        return zzb != status.getStatusCode() ? GamesStatusCodes.getStatusString(status.getStatusCode()).equals(status.getStatusMessage()) ? GamesClientStatusCodes.zza(zzb) : new Status(zzb, status.getStatusMessage()) : status;
    }
}
