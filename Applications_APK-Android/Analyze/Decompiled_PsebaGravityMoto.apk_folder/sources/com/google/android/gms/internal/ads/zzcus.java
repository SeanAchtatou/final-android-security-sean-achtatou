package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcus implements zzcva<zzcur> {
    private final zzavg zzbsq;
    private final zzbbl zzfqw;
    private final Context zzlj;

    public zzcus(zzavg zzavg, zzbbl zzbbl, Context context) {
        this.zzbsq = zzavg;
        this.zzfqw = zzbbl;
        this.zzlj = context;
    }

    public final zzbbh<zzcur> zzalm() {
        return this.zzfqw.zza(new zzcut(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcur zzalx() throws Exception {
        String str;
        String str2;
        String str3;
        if (!this.zzbsq.zzx(this.zzlj)) {
            return new zzcur(null, null, null, null, null);
        }
        String zzaa = this.zzbsq.zzaa(this.zzlj);
        String str4 = zzaa == null ? "" : zzaa;
        String zzab = this.zzbsq.zzab(this.zzlj);
        if (zzab == null) {
            str = "";
        } else {
            str = zzab;
        }
        String zzac = this.zzbsq.zzac(this.zzlj);
        if (zzac == null) {
            str2 = "";
        } else {
            str2 = zzac;
        }
        String zzad = this.zzbsq.zzad(this.zzlj);
        if (zzad == null) {
            str3 = "";
        } else {
            str3 = zzad;
        }
        return new zzcur(str4, str, str2, str3, "TIME_OUT".equals(str) ? (Long) zzyt.zzpe().zzd(zzacu.zzcnx) : null);
    }
}
