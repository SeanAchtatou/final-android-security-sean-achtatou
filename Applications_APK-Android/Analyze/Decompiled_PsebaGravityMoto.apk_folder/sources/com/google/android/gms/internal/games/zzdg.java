package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.multiplayer.ParticipantResult;

final class zzdg extends zzdw {
    private final /* synthetic */ String zzew;
    private final /* synthetic */ byte[] zzkj;
    private final /* synthetic */ String zzkk;
    private final /* synthetic */ ParticipantResult[] zzkl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdg(zzdb zzdb, GoogleApiClient googleApiClient, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) {
        super(googleApiClient, null);
        this.zzew = str;
        this.zzkj = bArr;
        this.zzkk = str2;
        this.zzkl = participantResultArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzew, this.zzkj, this.zzkk, this.zzkl);
    }
}
