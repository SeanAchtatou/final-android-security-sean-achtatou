package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzcc extends zzcg {
    private final /* synthetic */ String[] zzjx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcc(zzca zzca, GoogleApiClient googleApiClient, String[] strArr) {
        super(googleApiClient, null);
        this.zzjx = strArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzjx);
    }
}
