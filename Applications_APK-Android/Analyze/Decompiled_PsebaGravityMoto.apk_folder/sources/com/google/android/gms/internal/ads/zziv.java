package com.google.android.gms.internal.ads;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public final class zziv implements zzid, zzio {
    private final zzkm zzamg = new zzkm(zzkj.zzaqt);
    private final zzkm zzamh = new zzkm(4);
    private final zzkm zzami = new zzkm(16);
    private final Stack<zzir> zzamj = new Stack<>();
    private int zzamk = 0;
    private long zzaml;
    private int zzamm;
    private long zzamn;
    private int zzamo;
    private zzkm zzamp;
    private int zzamq;
    private int zzamr;
    private int zzams;
    private zzif zzamt;
    private zziw[] zzamu;

    public final boolean zzfc() {
        return true;
    }

    public final void zza(zzif zzif) {
        this.zzamt = zzif;
    }

    public final void zzfh() {
        this.zzaml = 0;
        this.zzamr = 0;
        this.zzams = 0;
    }

    public final int zza(zzie zzie, zzij zzij) throws IOException, InterruptedException {
        zziw[] zziwArr;
        long j;
        int i;
        boolean z;
        zziv zziv;
        zzir zzir;
        boolean z2;
        int i2;
        zziv zziv2;
        ArrayList arrayList;
        zzix zza;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        long j2;
        int i10;
        int[] iArr;
        int i11;
        zzkm zzkm;
        zziv zziv3 = this;
        zzie zzie2 = zzie;
        zzij zzij2 = zzij;
        while (true) {
            int i12 = zziv3.zzamk;
            if (i12 == 0) {
                zziv zziv4 = zziv3;
                zzie zzie3 = zzie;
                boolean z3 = false;
                if (zzie3.zza(zziv4.zzami.data, 0, 8, true)) {
                    zziv4.zzami.setPosition(0);
                    zziv4.zzamn = zziv4.zzami.zzge();
                    zziv4.zzamm = zziv4.zzami.readInt();
                    if (zziv4.zzamn == 1) {
                        zzie3.readFully(zziv4.zzami.data, 8, 8);
                        zziv4.zzamn = zziv4.zzami.readLong();
                        zziv4.zzaml += 16;
                        zziv4.zzamo = 16;
                    } else {
                        zziv4.zzaml += 8;
                        zziv4.zzamo = 8;
                    }
                    int i13 = zziv4.zzamm;
                    if (i13 == zziq.zzako || i13 == zziq.zzakq || i13 == zziq.zzakr || i13 == zziq.zzaks || i13 == zziq.zzakt) {
                        long j3 = zziv4.zzamn;
                        if (j3 == 1) {
                            zziv4.zzamj.add(new zzir(zziv4.zzamm, (zziv4.zzaml + j3) - ((long) zziv4.zzamo)));
                        } else {
                            zziv4.zzamj.add(new zzir(zziv4.zzamm, (zziv4.zzaml + j3) - ((long) zziv4.zzamo)));
                        }
                        zziv4.zzamk = 0;
                        z3 = true;
                    } else {
                        int i14 = zziv4.zzamm;
                        if (i14 == zziq.zzakz || i14 == zziq.zzakp || i14 == zziq.zzala || i14 == zziq.zzalp || i14 == zziq.zzalq || i14 == zziq.zzalb || i14 == zziq.zzaka || i14 == zziq.zzaku || i14 == zziq.zzake || i14 == zziq.zzakc || i14 == zziq.zzals || i14 == zziq.zzalt || i14 == zziq.zzalu || i14 == zziq.zzalv || i14 == zziq.zzalw || i14 == zziq.zzalx || i14 == zziq.zzaly || i14 == zziq.zzaky) {
                            zzkh.checkState(zziv4.zzamn < 2147483647L);
                            zziv4.zzamp = new zzkm((int) zziv4.zzamn);
                            System.arraycopy(zziv4.zzami.data, 0, zziv4.zzamp.data, 0, 8);
                            z3 = true;
                            zziv4.zzamk = 1;
                        } else {
                            z3 = true;
                            zziv4.zzamp = null;
                            zziv4.zzamk = 1;
                        }
                    }
                }
                if (!z3) {
                    return -1;
                }
                zzij2 = zzij;
                zziv3 = zziv4;
                zzie2 = zzie3;
            } else if (i12 != 1) {
                long j4 = Long.MAX_VALUE;
                int i15 = 0;
                int i16 = -1;
                while (true) {
                    zziwArr = zziv3.zzamu;
                    if (i15 >= zziwArr.length) {
                        break;
                    }
                    zziw zziw = zziwArr[i15];
                    int i17 = zziw.zzamy;
                    if (i17 != zziw.zzamw.zzand) {
                        long j5 = zziw.zzamw.zzahq[i17];
                        if (j5 < j4) {
                            i16 = i15;
                            j4 = j5;
                        }
                    }
                    i15++;
                }
                if (i16 == -1) {
                    return -1;
                }
                zziw zziw2 = zziwArr[i16];
                int i18 = zziw2.zzamy;
                long j6 = zziw2.zzamw.zzahq[i18];
                long j7 = j6;
                long position = (j6 - zzie.getPosition()) + ((long) zziv3.zzamr);
                if (position < 0) {
                    i = 1;
                    j = j7;
                } else if (position >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
                    j = j7;
                    i = 1;
                } else {
                    zzie2.zzr((int) position);
                    zziv3.zzamq = zziw2.zzamw.zzahp[i18];
                    if (zziw2.zzamv.zzamf != -1) {
                        byte[] bArr = zziv3.zzamh.data;
                        bArr[0] = 0;
                        bArr[1] = 0;
                        bArr[2] = 0;
                        int i19 = zziw2.zzamv.zzamf;
                        int i20 = 4 - zziw2.zzamv.zzamf;
                        while (zziv3.zzamr < zziv3.zzamq) {
                            if (zziv3.zzams == 0) {
                                zzie2.readFully(zziv3.zzamh.data, i20, i19);
                                zziv3.zzamh.setPosition(0);
                                zziv3.zzams = zziv3.zzamh.zzgg();
                                zziv3.zzamg.setPosition(0);
                                zziw2.zzamx.zza(zziv3.zzamg, 4);
                                zziv3.zzamr += 4;
                                zziv3.zzamq += i20;
                            } else {
                                int zza2 = zziw2.zzamx.zza(zzie2, zziv3.zzams);
                                zziv3.zzamr += zza2;
                                zziv3.zzams -= zza2;
                            }
                        }
                    } else {
                        while (zziv3.zzamr < zziv3.zzamq) {
                            int zza3 = zziw2.zzamx.zza(zzie2, zziv3.zzamq - zziv3.zzamr);
                            zziv3.zzamr += zza3;
                            zziv3.zzams -= zza3;
                        }
                    }
                    zziw2.zzamx.zza(zziw2.zzamw.zzane[i18], zziw2.zzamw.zzajr[i18], zziv3.zzamq, 0, null);
                    zziw2.zzamy++;
                    zziv3.zzamr = 0;
                    zziv3.zzams = 0;
                    return 0;
                }
                zzij2.zzahv = j;
                return i;
            } else {
                zziv3.zzamk = 0;
                long j8 = zziv3.zzaml;
                long j9 = zziv3.zzamn;
                int i21 = zziv3.zzamo;
                zziv3.zzaml = j8 + (j9 - ((long) i21));
                long j10 = j9 - ((long) i21);
                boolean z4 = zziv3.zzamp == null && (j9 >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE || j9 > 2147483647L);
                if (z4) {
                    zzij2.zzahv = zziv3.zzaml;
                } else {
                    zzkm zzkm2 = zziv3.zzamp;
                    if (zzkm2 != null) {
                        zzie2.readFully(zzkm2.data, zziv3.zzamo, (int) j10);
                        if (!zziv3.zzamj.isEmpty()) {
                            zziv3.zzamj.peek().zzama.add(new zzis(zziv3.zzamm, zziv3.zzamp));
                        }
                    } else {
                        zzie2.zzr((int) j10);
                    }
                }
                while (!zziv3.zzamj.isEmpty() && zziv3.zzamj.peek().zzalz == zziv3.zzaml) {
                    zzir pop = zziv3.zzamj.pop();
                    if (pop.type == zziq.zzako) {
                        ArrayList arrayList2 = new ArrayList();
                        int i22 = 0;
                        while (i22 < pop.zzamb.size()) {
                            zzir zzir2 = pop.zzamb.get(i22);
                            if (zzir2.type == zziq.zzakq && (zza = zzit.zza(zzir2, pop.zzv(zziq.zzakp))) != null && (zza.type == 1936684398 || zza.type == 1986618469)) {
                                zzir zzw = zzir2.zzw(zziq.zzakr).zzw(zziq.zzaks).zzw(zziq.zzakt);
                                zzkm zzkm3 = zzw.zzv(zziq.zzalw).zzamc;
                                zzis zzv = zzw.zzv(zziq.zzalx);
                                if (zzv == null) {
                                    zzv = zzw.zzv(zziq.zzaly);
                                }
                                zzkm zzkm4 = zzv.zzamc;
                                zzkm zzkm5 = zzw.zzv(zziq.zzalv).zzamc;
                                zzkm zzkm6 = zzw.zzv(zziq.zzals).zzamc;
                                zzis zzv2 = zzw.zzv(zziq.zzalt);
                                zzkm zzkm7 = zzv2 != null ? zzv2.zzamc : null;
                                zzis zzv3 = zzw.zzv(zziq.zzalu);
                                zzkm zzkm8 = zzv3 != null ? zzv3.zzamc : null;
                                zzkm3.setPosition(12);
                                int zzgg = zzkm3.zzgg();
                                int zzgg2 = zzkm3.zzgg();
                                int[] iArr2 = new int[zzgg2];
                                long[] jArr = new long[zzgg2];
                                z2 = z4;
                                long[] jArr2 = new long[zzgg2];
                                zzir = pop;
                                int[] iArr3 = new int[zzgg2];
                                ArrayList arrayList3 = arrayList2;
                                zzkm4.setPosition(12);
                                int i23 = i22;
                                int zzgg3 = zzkm4.zzgg();
                                zzkm5.setPosition(12);
                                int zzgg4 = zzkm5.zzgg() - 1;
                                zzix zzix = zza;
                                if (zzkm5.readInt() == 1) {
                                    int zzgg5 = zzkm5.zzgg();
                                    zzkm5.zzac(4);
                                    if (zzgg4 > 0) {
                                        i3 = zzkm5.zzgg() - 1;
                                        i4 = 12;
                                    } else {
                                        i4 = 12;
                                        i3 = -1;
                                    }
                                    zzkm6.setPosition(i4);
                                    int zzgg6 = zzkm6.zzgg() - 1;
                                    int zzgg7 = zzkm6.zzgg();
                                    int zzgg8 = zzkm6.zzgg();
                                    if (zzkm8 != null) {
                                        zzkm8.setPosition(i4);
                                        i7 = zzkm8.zzgg() - 1;
                                        i6 = zzkm8.zzgg();
                                        i5 = zzkm8.readInt();
                                    } else {
                                        i7 = 0;
                                        i6 = 0;
                                        i5 = 0;
                                    }
                                    if (zzkm7 != null) {
                                        zzkm7.setPosition(i4);
                                        int zzgg9 = zzkm7.zzgg();
                                        i9 = zzkm7.zzgg() - 1;
                                        i8 = zzgg9;
                                    } else {
                                        i9 = -1;
                                        i8 = 0;
                                    }
                                    int i24 = zzgg4;
                                    if (zzv.type == zziq.zzalx) {
                                        j2 = zzkm4.zzge();
                                    } else {
                                        j2 = zzkm4.zzgh();
                                    }
                                    int i25 = zzgg5;
                                    int i26 = i3;
                                    int i27 = i5;
                                    int i28 = i24;
                                    int i29 = 0;
                                    int i30 = 0;
                                    zzkm zzkm9 = zzkm5;
                                    int i31 = i6;
                                    int i32 = i9;
                                    zzkm zzkm10 = zzkm4;
                                    int i33 = zzgg8;
                                    int i34 = i7;
                                    long j11 = 0;
                                    while (i29 < zzgg2) {
                                        jArr2[i29] = j2;
                                        iArr2[i29] = zzgg == 0 ? zzkm3.zzgg() : zzgg;
                                        int[] iArr4 = iArr2;
                                        long[] jArr3 = jArr2;
                                        jArr[i29] = j11 + ((long) i27);
                                        iArr3[i29] = zzkm7 == null ? 1 : 0;
                                        if (i29 == i32) {
                                            iArr3[i29] = 1;
                                            i8--;
                                            if (i8 > 0) {
                                                i32 = zzkm7.zzgg() - 1;
                                            }
                                        }
                                        j11 += (long) i33;
                                        zzgg7--;
                                        if (zzgg7 == 0 && zzgg6 > 0) {
                                            zzgg6--;
                                            zzgg7 = zzkm6.zzgg();
                                            i33 = zzkm6.zzgg();
                                        }
                                        if (zzkm8 != null && i31 - 1 == 0 && i34 > 0) {
                                            i34--;
                                            i31 = zzkm8.zzgg();
                                            i27 = zzkm8.readInt();
                                        }
                                        zzgg5--;
                                        if (zzgg5 == 0) {
                                            int i35 = i30 + 1;
                                            if (i35 < zzgg3) {
                                                i10 = i27;
                                                if (zzv.type == zziq.zzalx) {
                                                    j2 = zzkm10.zzge();
                                                } else {
                                                    j2 = zzkm10.zzgh();
                                                }
                                            } else {
                                                i10 = i27;
                                            }
                                            int i36 = i26;
                                            if (i35 == i36) {
                                                i25 = zzkm9.zzgg();
                                                i11 = i36;
                                                zzkm = zzkm9;
                                                zzkm.zzac(4);
                                                i28--;
                                                if (i28 > 0) {
                                                    i11 = zzkm.zzgg() - 1;
                                                }
                                            } else {
                                                i11 = i36;
                                                zzkm = zzkm9;
                                            }
                                            if (i35 < zzgg3) {
                                                i30 = i35;
                                                iArr = iArr3;
                                                zzgg5 = i25;
                                            } else {
                                                i30 = i35;
                                                iArr = iArr3;
                                            }
                                        } else {
                                            i10 = i27;
                                            zzkm = zzkm9;
                                            i11 = i26;
                                            iArr = iArr3;
                                            j2 += (long) iArr4[i29];
                                        }
                                        i29++;
                                        i26 = i11;
                                        iArr3 = iArr;
                                        iArr2 = iArr4;
                                        jArr2 = jArr3;
                                        zzkm9 = zzkm;
                                        i27 = i10;
                                    }
                                    int[] iArr5 = iArr2;
                                    long[] jArr4 = jArr2;
                                    int[] iArr6 = iArr3;
                                    zzix zzix2 = zzix;
                                    zzkq.zza(jArr, 1000000, zzix2.zzcs);
                                    zzkh.checkArgument(i8 == 0);
                                    zzkh.checkArgument(zzgg7 == 0);
                                    zzkh.checkArgument(zzgg5 == 0);
                                    zzkh.checkArgument(zzgg6 == 0);
                                    zzkh.checkArgument(i34 == 0);
                                    zziz zziz = new zziz(jArr4, iArr5, jArr, iArr6);
                                    if (zziz.zzand != 0) {
                                        zziv2 = this;
                                        i2 = i23;
                                        zziw zziw3 = new zziw(zzix2, zziz, zziv2.zzamt.zzs(i2));
                                        zziw3.zzamx.zza(zzix2.zzame);
                                        arrayList = arrayList3;
                                        arrayList.add(zziw3);
                                    } else {
                                        zziv2 = this;
                                        arrayList = arrayList3;
                                        i2 = i23;
                                    }
                                } else {
                                    throw new IllegalStateException("stsc first chunk must be 1");
                                }
                            } else {
                                z2 = z4;
                                zzir = pop;
                                i2 = i22;
                                zziv2 = zziv3;
                                arrayList = arrayList2;
                            }
                            i22 = i2 + 1;
                            arrayList2 = arrayList;
                            zziv3 = zziv2;
                            z4 = z2;
                            pop = zzir;
                        }
                        z = z4;
                        zziv = zziv3;
                        zziv.zzamu = (zziw[]) arrayList2.toArray(new zziw[0]);
                        zziv.zzamt.zzfi();
                        zziv.zzamt.zza(zziv);
                        zziv.zzamk = 2;
                    } else {
                        z = z4;
                        zzir zzir3 = pop;
                        zziv = zziv3;
                        if (!zziv.zzamj.isEmpty()) {
                            zziv.zzamj.peek().zzamb.add(zzir3);
                        }
                    }
                    zziv3 = zziv;
                    z4 = z;
                }
                boolean z5 = z4;
                zziv zziv5 = zziv3;
                if (z5) {
                    return 1;
                }
                zzie2 = zzie;
                zzij2 = zzij;
                zziv3 = zziv5;
            }
        }
    }

    public final long zzdq(long j) {
        int i;
        long j2 = Long.MAX_VALUE;
        int i2 = 0;
        while (true) {
            zziw[] zziwArr = this.zzamu;
            if (i2 >= zziwArr.length) {
                return j2;
            }
            zziz zziz = zziwArr[i2].zzamw;
            int zza = zzkq.zza(zziz.zzane, j, true, false);
            while (true) {
                if (zza >= 0) {
                    if (zziz.zzane[zza] <= j && (zziz.zzajr[zza] & 1) != 0) {
                        break;
                    }
                    zza--;
                } else {
                    zza = -1;
                    break;
                }
            }
            if (i == -1) {
                i = zzkq.zzb(zziz.zzane, j, true, false);
                while (true) {
                    if (i < zziz.zzane.length) {
                        if (zziz.zzane[i] >= j && (zziz.zzajr[i] & 1) != 0) {
                            break;
                        }
                        i++;
                    } else {
                        i = -1;
                        break;
                    }
                }
            }
            this.zzamu[i2].zzamy = i;
            long j3 = zziz.zzahq[this.zzamu[i2].zzamy];
            if (j3 < j2) {
                j2 = j3;
            }
            i2++;
        }
    }
}
