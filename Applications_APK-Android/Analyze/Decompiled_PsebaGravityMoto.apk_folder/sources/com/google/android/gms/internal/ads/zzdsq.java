package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzdsq extends zzdrr<zzdsq> {
    public String zzhte = null;
    public Long zzhtf = null;
    public Boolean zzhtg = null;

    public zzdsq() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        String str = this.zzhte;
        if (str != null) {
            zzdrp.zzf(1, str);
        }
        Long l = this.zzhtf;
        if (l != null) {
            long longValue = l.longValue();
            zzdrp.zzw(2, 0);
            zzdrp.zzfv(longValue);
        }
        Boolean bool = this.zzhtg;
        if (bool != null) {
            zzdrp.zzi(3, bool.booleanValue());
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        String str = this.zzhte;
        if (str != null) {
            zzor += zzdrp.zzg(1, str);
        }
        Long l = this.zzhtf;
        if (l != null) {
            zzor += zzdrp.zzgd(2) + zzdrp.zzfw(l.longValue());
        }
        Boolean bool = this.zzhtg;
        if (bool == null) {
            return zzor;
        }
        bool.booleanValue();
        return zzor + zzdrp.zzgd(3) + 1;
    }
}
