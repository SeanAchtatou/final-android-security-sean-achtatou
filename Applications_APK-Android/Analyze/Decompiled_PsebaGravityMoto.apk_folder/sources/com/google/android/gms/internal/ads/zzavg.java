package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.GuardedBy;

@ParametersAreNonnullByDefault
@zzard
public final class zzavg {
    private final AtomicReference<ThreadPoolExecutor> zzdsd = new AtomicReference<>(null);
    private final Object zzdse = new Object();
    @GuardedBy("gmpAppIdLock")
    @Nullable
    private String zzdsf = null;
    @GuardedBy("gmpAppIdLock")
    @Nullable
    private String zzdsg = null;
    @VisibleForTesting
    private final AtomicBoolean zzdsh = new AtomicBoolean(false);
    @VisibleForTesting
    private final AtomicInteger zzdsi = new AtomicInteger(-1);
    private final AtomicReference<Object> zzdsj = new AtomicReference<>(null);
    private final AtomicReference<Object> zzdsk = new AtomicReference<>(null);
    private final ConcurrentMap<String, Method> zzdsl = new ConcurrentHashMap(9);
    private final AtomicReference<zzbjf> zzdsm = new AtomicReference<>(null);
    @GuardedBy("proxyReference")
    private final List<FutureTask> zzdsn = new ArrayList();

    public final boolean zzx(Context context) {
        if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcns)).booleanValue() && !this.zzdsh.get()) {
            if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcoc)).booleanValue()) {
                return true;
            }
            if (this.zzdsi.get() == -1) {
                zzyt.zzpa();
                if (!zzazt.zzc(context, GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE)) {
                    zzyt.zzpa();
                    if (zzazt.zzbg(context)) {
                        zzawz.zzep("Google Play Service is out of date, the Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires updated Google Play Service.");
                        this.zzdsi.set(0);
                    }
                }
                this.zzdsi.set(1);
            }
            if (this.zzdsi.get() == 1) {
                return true;
            }
        }
        return false;
    }

    @VisibleForTesting
    private static boolean zzy(Context context) {
        if (!((Boolean) zzyt.zzpe().zzd(zzacu.zzcnz)).booleanValue()) {
            if (!((Boolean) zzyt.zzpe().zzd(zzacu.zzcoa)).booleanValue()) {
                return false;
            }
        }
        if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcob)).booleanValue()) {
            try {
                context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
                return false;
            } catch (ClassNotFoundException unused) {
            }
        }
        return true;
    }

    public final void zzd(Context context, String str) {
        if (zzx(context)) {
            if (zzy(context)) {
                zza("beginAdUnitExposure", new zzavh(str));
            } else {
                zza(context, str, "beginAdUnitExposure");
            }
        }
    }

    public final void zze(Context context, String str) {
        if (zzx(context)) {
            if (zzy(context)) {
                zza("endAdUnitExposure", new zzavi(str));
            } else {
                zza(context, str, "endAdUnitExposure");
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String zzz(android.content.Context r7) {
        /*
            r6 = this;
            java.lang.String r0 = "getCurrentScreenName"
            boolean r1 = r6.zzx(r7)
            java.lang.String r2 = ""
            if (r1 != 0) goto L_0x000b
            return r2
        L_0x000b:
            boolean r1 = zzy(r7)
            if (r1 == 0) goto L_0x001c
            com.google.android.gms.internal.ads.zzavv r7 = com.google.android.gms.internal.ads.zzavm.zzdst
            java.lang.String r0 = "getCurrentScreenNameOrScreenClass"
            java.lang.Object r7 = r6.zza(r0, r2, r7)
            java.lang.String r7 = (java.lang.String) r7
            return r7
        L_0x001c:
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r1 = r6.zzdsj
            r3 = 1
            java.lang.String r4 = "com.google.android.gms.measurement.AppMeasurement"
            boolean r1 = r6.zza(r7, r4, r1, r3)
            if (r1 != 0) goto L_0x0028
            return r2
        L_0x0028:
            r1 = 0
            java.lang.reflect.Method r3 = r6.zzl(r7, r0)     // Catch:{ Exception -> 0x0056 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r4 = r6.zzdsj     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r4 = r4.get()     // Catch:{ Exception -> 0x0056 }
            java.lang.Object[] r5 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r3 = r3.invoke(r4, r5)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0056 }
            if (r3 != 0) goto L_0x0052
            java.lang.String r3 = "getCurrentScreenClass"
            java.lang.reflect.Method r7 = r6.zzl(r7, r3)     // Catch:{ Exception -> 0x0056 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r3 = r6.zzdsj     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r3 = r3.get()     // Catch:{ Exception -> 0x0056 }
            java.lang.Object[] r4 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r7 = r7.invoke(r3, r4)     // Catch:{ Exception -> 0x0056 }
            r3 = r7
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0056 }
        L_0x0052:
            if (r3 == 0) goto L_0x0055
            return r3
        L_0x0055:
            return r2
        L_0x0056:
            r7 = move-exception
            r6.zza(r7, r0, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzavg.zzz(android.content.Context):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    public final void zzf(Context context, String str) {
        if (!zzx(context) || !(context instanceof Activity)) {
            return;
        }
        if (zzy(context)) {
            zza("setScreenName", new zzavn(context, str));
        } else if (zza(context, "com.google.firebase.analytics.FirebaseAnalytics", this.zzdsk, false)) {
            Method zzm = zzm(context, "setCurrentScreen");
            try {
                zzm.invoke(this.zzdsk.get(), (Activity) context, str, context.getPackageName());
            } catch (Exception e) {
                zza(e, "setCurrentScreen", false);
            }
        }
    }

    @Nullable
    public final String zzaa(Context context) {
        if (!zzx(context)) {
            return null;
        }
        synchronized (this.zzdse) {
            if (this.zzdsf != null) {
                String str = this.zzdsf;
                return str;
            }
            if (zzy(context)) {
                this.zzdsf = (String) zza("getGmpAppId", this.zzdsf, zzavo.zzdst);
            } else {
                this.zzdsf = (String) zza("getGmpAppId", context);
            }
            String str2 = this.zzdsf;
            return str2;
        }
    }

    @Nullable
    public final String zzab(Context context) {
        if (!zzx(context)) {
            return null;
        }
        long longValue = ((Long) zzyt.zzpe().zzd(zzacu.zzcnx)).longValue();
        if (zzy(context)) {
            if (longValue >= 0) {
                return (String) zza("getAppInstanceId", zzavq.zzdst).get(longValue, TimeUnit.MILLISECONDS);
            }
            try {
                return (String) zza("getAppInstanceId", zzavp.zzdst).get();
            } catch (Exception e) {
                if (e instanceof TimeoutException) {
                    return "TIME_OUT";
                }
                return null;
            }
        } else if (longValue < 0) {
            return (String) zza("getAppInstanceId", context);
        } else {
            Future submit = zzui().submit(new zzavr(this, context));
            try {
                return (String) submit.get(longValue, TimeUnit.MILLISECONDS);
            } catch (Exception e2) {
                submit.cancel(true);
                if (e2 instanceof TimeoutException) {
                    return "TIME_OUT";
                }
                return null;
            }
        }
    }

    @Nullable
    public final String zzac(Context context) {
        if (!zzx(context)) {
            return null;
        }
        if (zzy(context)) {
            Long l = (Long) zza("getAdEventId", (Object) null, zzavs.zzdst);
            if (l != null) {
                return Long.toString(l.longValue());
            }
            return null;
        }
        Object zza = zza("generateEventId", context);
        if (zza != null) {
            return zza.toString();
        }
        return null;
    }

    @Nullable
    public final String zzad(Context context) {
        if (!zzx(context)) {
            return null;
        }
        synchronized (this.zzdse) {
            if (this.zzdsg != null) {
                String str = this.zzdsg;
                return str;
            }
            if (zzy(context)) {
                this.zzdsg = (String) zza("getAppIdOrigin", this.zzdsg, zzavt.zzdst);
            } else {
                this.zzdsg = "fa";
            }
            String str2 = this.zzdsg;
            return str2;
        }
    }

    public final void zzg(Context context, String str) {
        zza(context, "_ac", str, (Bundle) null);
    }

    public final void zzh(Context context, String str) {
        zza(context, "_ai", str, (Bundle) null);
    }

    public final void zzi(Context context, String str) {
        zza(context, "_aq", str, (Bundle) null);
    }

    public final void zzj(Context context, String str) {
        zza(context, "_aa", str, (Bundle) null);
    }

    public final void zza(Context context, String str, String str2, String str3, int i) {
        if (zzx(context)) {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str2);
            bundle.putString("type", str3);
            bundle.putInt(AppMeasurementSdk.ConditionalUserProperty.VALUE, i);
            zza(context, "_ar", str, bundle);
            StringBuilder sb = new StringBuilder(String.valueOf(str3).length() + 75);
            sb.append("Log a Firebase reward video event, reward type: ");
            sb.append(str3);
            sb.append(", reward value: ");
            sb.append(i);
            zzawz.zzds(sb.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final void zza(Context context, String str, String str2, @Nullable Bundle bundle) {
        if (zzx(context)) {
            Bundle zzj = zzj(str2, str);
            if (bundle != null) {
                zzj.putAll(bundle);
            }
            if (zzy(context)) {
                zza("logEventInternal", new zzavj(str, zzj));
            } else if (zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdsj, true)) {
                Method zzae = zzae(context);
                try {
                    zzae.invoke(this.zzdsj.get(), "am", str, zzj);
                } catch (Exception e) {
                    zza(e, "logEventInternal", true);
                }
            }
        }
    }

    private static Bundle zzj(String str, String str2) {
        Bundle bundle = new Bundle();
        try {
            bundle.putLong("_aeid", Long.parseLong(str));
        } catch (NullPointerException | NumberFormatException e) {
            String valueOf = String.valueOf(str);
            zzawz.zzc(valueOf.length() != 0 ? "Invalid event ID: ".concat(valueOf) : new String("Invalid event ID: "), e);
        }
        if ("_ac".equals(str2)) {
            bundle.putInt("_r", 1);
        }
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    @Nullable
    private final Method zzae(Context context) {
        Method method = this.zzdsl.get("logEventInternal");
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod("logEventInternal", String.class, String.class, Bundle.class);
            this.zzdsl.put("logEventInternal", declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, "logEventInternal", true);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    @Nullable
    private final Method zzk(Context context, String str) {
        Method method = this.zzdsl.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, String.class);
            this.zzdsl.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    @Nullable
    private final Method zzl(Context context, String str) {
        Method method = this.zzdsl.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, new Class[0]);
            this.zzdsl.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzm(Context context, String str) {
        Method method = this.zzdsl.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics").getDeclaredMethod(str, Activity.class, String.class, String.class);
            this.zzdsl.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final void zza(Context context, String str, String str2) {
        if (zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdsj, true)) {
            Method zzk = zzk(context, str2);
            try {
                zzk.invoke(this.zzdsj.get(), str);
                StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 37 + String.valueOf(str).length());
                sb.append("Invoke Firebase method ");
                sb.append(str2);
                sb.append(", Ad Unit Id: ");
                sb.append(str);
                zzawz.zzds(sb.toString());
            } catch (Exception e) {
                zza(e, str2, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Object zza(String str, Context context) {
        if (!zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdsj, true)) {
            return null;
        }
        try {
            return zzl(context, str).invoke(this.zzdsj.get(), new Object[0]);
        } catch (Exception e) {
            zza(e, str, true);
            return null;
        }
    }

    private final void zza(Exception exc, String str, boolean z) {
        if (!this.zzdsh.get()) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 30);
            sb.append("Invoke Firebase method ");
            sb.append(str);
            sb.append(" error.");
            zzawz.zzep(sb.toString());
            if (z) {
                zzawz.zzep("The Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires the latest Firebase SDK jar, but Firebase SDK is either missing or out of date");
                this.zzdsh.set(true);
            }
        }
    }

    private final ThreadPoolExecutor zzui() {
        if (this.zzdsd.get() == null) {
            this.zzdsd.compareAndSet(null, new ThreadPoolExecutor(((Integer) zzyt.zzpe().zzd(zzacu.zzcny)).intValue(), ((Integer) zzyt.zzpe().zzd(zzacu.zzcny)).intValue(), 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), new zzavu(this)));
        }
        return this.zzdsd.get();
    }

    private final boolean zza(Context context, String str, AtomicReference<Object> atomicReference, boolean z) {
        if (atomicReference.get() == null) {
            try {
                atomicReference.compareAndSet(null, context.getClassLoader().loadClass(str).getDeclaredMethod("getInstance", Context.class).invoke(null, context));
            } catch (Exception e) {
                zza(e, "getInstance", z);
                return false;
            }
        }
        return true;
    }

    private final void zza(String str, zzavw zzavw) {
        synchronized (this.zzdsm) {
            FutureTask futureTask = new FutureTask(new zzavk(this, zzavw, str), null);
            if (this.zzdsm.get() != null) {
                futureTask.run();
            } else {
                this.zzdsn.add(futureTask);
            }
        }
    }

    private final <T> Future<T> zza(String str, zzavv<T> zzavv) {
        FutureTask futureTask;
        synchronized (this.zzdsm) {
            futureTask = new FutureTask(new zzavl(this, zzavv, str));
            if (this.zzdsm.get() != null) {
                zzui().submit(futureTask);
            } else {
                this.zzdsn.add(futureTask);
            }
        }
        return futureTask;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final <T> T zza(String str, @Nullable T t, zzavv<T> zzavv) {
        synchronized (this.zzdsm) {
            if (this.zzdsm.get() != null) {
                try {
                    T zzb = zzavv.zzb(this.zzdsm.get());
                    return zzb;
                } catch (Exception e) {
                    zza(e, str, false);
                    return t;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(zzavv zzavv, String str) throws Exception {
        try {
            return zzavv.zzb(this.zzdsm.get());
        } catch (Exception e) {
            zza(e, str, false);
            throw e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzavv):T
      com.google.android.gms.internal.ads.zzavg.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzavg.zza(java.lang.Exception, java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzavw zzavw, String str) {
        if (this.zzdsm.get() != null) {
            try {
                zzavw.zza(this.zzdsm.get());
            } catch (Exception e) {
                zza(e, str, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzaf(Context context) throws Exception {
        return (String) zza("getAppInstanceId", context);
    }
}
