package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdob;

final class zzdoa implements zzdpj {
    private static final zzdoa zzhhc = new zzdoa();

    private zzdoa() {
    }

    public static zzdoa zzaxq() {
        return zzhhc;
    }

    public final boolean zzc(Class<?> cls) {
        return zzdob.class.isAssignableFrom(cls);
    }

    public final zzdpi zzd(Class<?> cls) {
        if (!zzdob.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (zzdpi) zzdob.zze(cls.asSubclass(zzdob.class)).zza(zzdob.zze.zzhhm, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
