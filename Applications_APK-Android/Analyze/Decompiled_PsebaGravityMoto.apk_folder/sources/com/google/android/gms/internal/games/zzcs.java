package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.snapshot.Snapshots;

abstract class zzcs extends Games.zza<Snapshots.LoadSnapshotsResult> {
    private zzcs(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzcs(GoogleApiClient googleApiClient, zzcj zzcj) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzct(this, status);
    }
}
