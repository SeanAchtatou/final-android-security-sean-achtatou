package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrz;
import java.io.IOException;

public final class zzdsm extends zzdrr<zzdsm> {
    private zzdrz.zzb.zzd.C0010zzb zzhsp = null;
    public zzdsl[] zzhsq = zzdsl.zzbaz();
    private byte[] zzhsr = null;
    private byte[] zzhss = null;
    private Integer zzhst = null;

    public zzdsm() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        zzdsl[] zzdslArr = this.zzhsq;
        if (zzdslArr != null && zzdslArr.length > 0) {
            int i = 0;
            while (true) {
                zzdsl[] zzdslArr2 = this.zzhsq;
                if (i >= zzdslArr2.length) {
                    break;
                }
                zzdsl zzdsl = zzdslArr2[i];
                if (zzdsl != null) {
                    zzdrp.zza(2, zzdsl);
                }
                i++;
            }
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        zzdsl[] zzdslArr = this.zzhsq;
        if (zzdslArr != null && zzdslArr.length > 0) {
            int i = 0;
            while (true) {
                zzdsl[] zzdslArr2 = this.zzhsq;
                if (i >= zzdslArr2.length) {
                    break;
                }
                zzdsl zzdsl = zzdslArr2[i];
                if (zzdsl != null) {
                    zzor += zzdrp.zzb(2, zzdsl);
                }
                i++;
            }
        }
        return zzor;
    }
}
