package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzxo extends zzdrr<zzxo> {
    public Integer zzcfo = null;
    public Integer zzcfp = null;
    public Integer zzcfq = null;

    public zzxo() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        Integer num = this.zzcfo;
        if (num != null) {
            zzdrp.zzx(1, num.intValue());
        }
        Integer num2 = this.zzcfp;
        if (num2 != null) {
            zzdrp.zzx(2, num2.intValue());
        }
        Integer num3 = this.zzcfq;
        if (num3 != null) {
            zzdrp.zzx(3, num3.intValue());
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        Integer num = this.zzcfo;
        if (num != null) {
            zzor += zzdrp.zzab(1, num.intValue());
        }
        Integer num2 = this.zzcfp;
        if (num2 != null) {
            zzor += zzdrp.zzab(2, num2.intValue());
        }
        Integer num3 = this.zzcfq;
        return num3 != null ? zzor + zzdrp.zzab(3, num3.intValue()) : zzor;
    }
}
