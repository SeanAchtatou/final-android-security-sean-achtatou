package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;

final class zzdc extends zzdo {
    private final /* synthetic */ TurnBasedMatchConfig zzki;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdc(zzdb zzdb, GoogleApiClient googleApiClient, TurnBasedMatchConfig turnBasedMatchConfig) {
        super(googleApiClient, null);
        this.zzki = turnBasedMatchConfig;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzki);
    }
}
