package com.google.android.gms.games;

import androidx.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.GamesMetadata;

final class zzv implements PendingResultUtil.ResultConverter<GamesMetadata.LoadGamesResult, Game> {
    zzv() {
    }

    private static Game zza(@Nullable GamesMetadata.LoadGamesResult loadGamesResult) {
        GameBuffer games;
        if (loadGamesResult == null || (games = loadGamesResult.getGames()) == null) {
            return null;
        }
        try {
            if (games.getCount() > 0) {
                return (Game) ((Game) games.get(0)).freeze();
            }
            games.release();
            return null;
        } finally {
            games.release();
        }
    }

    public final /* synthetic */ Object convert(@Nullable Result result) {
        return zza((GamesMetadata.LoadGamesResult) result);
    }
}
