package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrz;
import java.io.IOException;

public final class zzdsj extends zzdrr<zzdsj> {
    public String url = null;
    public Integer zzhrv = null;
    private zzdrz.zza.zzc zzhrw = null;
    public String zzhrx = null;
    private String zzhry = null;
    public zzdsk zzhrz = null;
    public zzdsp[] zzhsa = zzdsp.zzbba();
    public String zzhsb = null;
    public zzdso zzhsc = null;
    private Boolean zzhsd = null;
    private String[] zzhse = zzdry.zzhog;
    private String zzhsf = null;
    private Boolean zzhsg = null;
    private Boolean zzhsh = null;
    private byte[] zzhsi = null;
    public zzdsq zzhsj = null;
    public String[] zzhsk = zzdry.zzhog;
    public String[] zzhsl = zzdry.zzhog;

    public zzdsj() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        String str = this.url;
        if (str != null) {
            zzdrp.zzf(1, str);
        }
        String str2 = this.zzhrx;
        if (str2 != null) {
            zzdrp.zzf(2, str2);
        }
        zzdsp[] zzdspArr = this.zzhsa;
        int i = 0;
        if (zzdspArr != null && zzdspArr.length > 0) {
            int i2 = 0;
            while (true) {
                zzdsp[] zzdspArr2 = this.zzhsa;
                if (i2 >= zzdspArr2.length) {
                    break;
                }
                zzdsp zzdsp = zzdspArr2[i2];
                if (zzdsp != null) {
                    zzdrp.zza(4, zzdsp);
                }
                i2++;
            }
        }
        String[] strArr = this.zzhse;
        if (strArr != null && strArr.length > 0) {
            int i3 = 0;
            while (true) {
                String[] strArr2 = this.zzhse;
                if (i3 >= strArr2.length) {
                    break;
                }
                String str3 = strArr2[i3];
                if (str3 != null) {
                    zzdrp.zzf(6, str3);
                }
                i3++;
            }
        }
        Integer num = this.zzhrv;
        if (num != null) {
            zzdrp.zzx(10, num.intValue());
        }
        zzdsk zzdsk = this.zzhrz;
        if (zzdsk != null) {
            zzdrp.zza(12, zzdsk);
        }
        String str4 = this.zzhsb;
        if (str4 != null) {
            zzdrp.zzf(13, str4);
        }
        zzdso zzdso = this.zzhsc;
        if (zzdso != null) {
            zzdrp.zza(14, zzdso);
        }
        zzdsq zzdsq = this.zzhsj;
        if (zzdsq != null) {
            zzdrp.zza(17, zzdsq);
        }
        String[] strArr3 = this.zzhsk;
        if (strArr3 != null && strArr3.length > 0) {
            int i4 = 0;
            while (true) {
                String[] strArr4 = this.zzhsk;
                if (i4 >= strArr4.length) {
                    break;
                }
                String str5 = strArr4[i4];
                if (str5 != null) {
                    zzdrp.zzf(20, str5);
                }
                i4++;
            }
        }
        String[] strArr5 = this.zzhsl;
        if (strArr5 != null && strArr5.length > 0) {
            while (true) {
                String[] strArr6 = this.zzhsl;
                if (i >= strArr6.length) {
                    break;
                }
                String str6 = strArr6[i];
                if (str6 != null) {
                    zzdrp.zzf(21, str6);
                }
                i++;
            }
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        String str = this.url;
        if (str != null) {
            zzor += zzdrp.zzg(1, str);
        }
        String str2 = this.zzhrx;
        if (str2 != null) {
            zzor += zzdrp.zzg(2, str2);
        }
        zzdsp[] zzdspArr = this.zzhsa;
        int i = 0;
        if (zzdspArr != null && zzdspArr.length > 0) {
            int i2 = zzor;
            int i3 = 0;
            while (true) {
                zzdsp[] zzdspArr2 = this.zzhsa;
                if (i3 >= zzdspArr2.length) {
                    break;
                }
                zzdsp zzdsp = zzdspArr2[i3];
                if (zzdsp != null) {
                    i2 += zzdrp.zzb(4, zzdsp);
                }
                i3++;
            }
            zzor = i2;
        }
        String[] strArr = this.zzhse;
        if (strArr != null && strArr.length > 0) {
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (true) {
                String[] strArr2 = this.zzhse;
                if (i4 >= strArr2.length) {
                    break;
                }
                String str3 = strArr2[i4];
                if (str3 != null) {
                    i6++;
                    i5 += zzdrp.zzgx(str3);
                }
                i4++;
            }
            zzor = zzor + i5 + (i6 * 1);
        }
        Integer num = this.zzhrv;
        if (num != null) {
            zzor += zzdrp.zzab(10, num.intValue());
        }
        zzdsk zzdsk = this.zzhrz;
        if (zzdsk != null) {
            zzor += zzdrp.zzb(12, zzdsk);
        }
        String str4 = this.zzhsb;
        if (str4 != null) {
            zzor += zzdrp.zzg(13, str4);
        }
        zzdso zzdso = this.zzhsc;
        if (zzdso != null) {
            zzor += zzdrp.zzb(14, zzdso);
        }
        zzdsq zzdsq = this.zzhsj;
        if (zzdsq != null) {
            zzor += zzdrp.zzb(17, zzdsq);
        }
        String[] strArr3 = this.zzhsk;
        if (strArr3 != null && strArr3.length > 0) {
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            while (true) {
                String[] strArr4 = this.zzhsk;
                if (i7 >= strArr4.length) {
                    break;
                }
                String str5 = strArr4[i7];
                if (str5 != null) {
                    i9++;
                    i8 += zzdrp.zzgx(str5);
                }
                i7++;
            }
            zzor = zzor + i8 + (i9 * 2);
        }
        String[] strArr5 = this.zzhsl;
        if (strArr5 == null || strArr5.length <= 0) {
            return zzor;
        }
        int i10 = 0;
        int i11 = 0;
        while (true) {
            String[] strArr6 = this.zzhsl;
            if (i >= strArr6.length) {
                return zzor + i10 + (i11 * 2);
            }
            String str6 = strArr6[i];
            if (str6 != null) {
                i11++;
                i10 += zzdrp.zzgx(str6);
            }
            i++;
        }
    }
}
