package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.video.Videos;

abstract class zzec extends Games.zza<Videos.CaptureAvailableResult> {
    private zzec(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzec(GoogleApiClient googleApiClient, zzdz zzdz) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzed(this, status);
    }
}
