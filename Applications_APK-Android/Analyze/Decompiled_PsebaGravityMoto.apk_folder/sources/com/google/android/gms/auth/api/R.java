package com.google.android.gms.auth.api;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int buttonSize = 2130837571;
        public static final int circleCrop = 2130837578;
        public static final int colorScheme = 2130837593;
        public static final int imageAspectRatio = 2130837646;
        public static final int imageAspectRatioAdjust = 2130837647;
        public static final int scopeUris = 2130837703;

        private attr() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968618;
        public static final int common_google_signin_btn_text_dark_default = 2130968619;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968620;
        public static final int common_google_signin_btn_text_dark_focused = 2130968621;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968622;
        public static final int common_google_signin_btn_text_light = 2130968623;
        public static final int common_google_signin_btn_text_light_default = 2130968624;
        public static final int common_google_signin_btn_text_light_disabled = 2130968625;
        public static final int common_google_signin_btn_text_light_focused = 2130968626;
        public static final int common_google_signin_btn_text_light_pressed = 2130968627;
        public static final int common_google_signin_btn_tint = 2130968628;

        private color() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099735;
        public static final int common_google_signin_btn_icon_dark = 2131099736;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099737;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099738;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131099739;
        public static final int common_google_signin_btn_icon_disabled = 2131099740;
        public static final int common_google_signin_btn_icon_light = 2131099741;
        public static final int common_google_signin_btn_icon_light_focused = 2131099742;
        public static final int common_google_signin_btn_icon_light_normal = 2131099743;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131099744;
        public static final int common_google_signin_btn_text_dark = 2131099745;
        public static final int common_google_signin_btn_text_dark_focused = 2131099746;
        public static final int common_google_signin_btn_text_dark_normal = 2131099747;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131099748;
        public static final int common_google_signin_btn_text_disabled = 2131099749;
        public static final int common_google_signin_btn_text_light = 2131099750;
        public static final int common_google_signin_btn_text_light_focused = 2131099751;
        public static final int common_google_signin_btn_text_light_normal = 2131099752;
        public static final int common_google_signin_btn_text_light_normal_background = 2131099753;
        public static final int googleg_disabled_color_18 = 2131099754;
        public static final int googleg_standard_color_18 = 2131099755;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131165212;
        public static final int adjust_width = 2131165213;
        public static final int auto = 2131165218;
        public static final int dark = 2131165241;
        public static final int icon_only = 2131165260;
        public static final int light = 2131165266;
        public static final int none = 2131165276;
        public static final int standard = 2131165314;
        public static final int wide = 2131165338;

        private id() {
        }
    }

    public static final class integer {
        public static final int google_play_services_version = 2131230724;

        private integer() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131361840;
        public static final int common_google_play_services_enable_text = 2131361841;
        public static final int common_google_play_services_enable_title = 2131361842;
        public static final int common_google_play_services_install_button = 2131361843;
        public static final int common_google_play_services_install_text = 2131361844;
        public static final int common_google_play_services_install_title = 2131361845;
        public static final int common_google_play_services_notification_channel_name = 2131361846;
        public static final int common_google_play_services_notification_ticker = 2131361847;
        public static final int common_google_play_services_unknown_issue = 2131361848;
        public static final int common_google_play_services_unsupported_text = 2131361849;
        public static final int common_google_play_services_update_button = 2131361850;
        public static final int common_google_play_services_update_text = 2131361851;
        public static final int common_google_play_services_update_title = 2131361852;
        public static final int common_google_play_services_updating_text = 2131361853;
        public static final int common_google_play_services_wear_update_text = 2131361854;
        public static final int common_open_on_phone = 2131361855;
        public static final int common_signin_button_text = 2131361856;
        public static final int common_signin_button_text_long = 2131361857;

        private string() {
        }
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.butakov.psebay.R.attr.circleCrop, com.butakov.psebay.R.attr.imageAspectRatio, com.butakov.psebay.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.butakov.psebay.R.attr.buttonSize, com.butakov.psebay.R.attr.colorScheme, com.butakov.psebay.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }
}
