package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzbq extends zzbv {
    private final /* synthetic */ String zzjt;
    private final /* synthetic */ String zzju;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbq(zzbo zzbo, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient, null);
        this.zzjt = str;
        this.zzju = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzjt, this.zzju);
    }
}
