package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdl extends zzdu {
    private final /* synthetic */ int zzkm;
    private final /* synthetic */ int[] zzkn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdl(zzdb zzdb, GoogleApiClient googleApiClient, int i, int[] iArr) {
        super(googleApiClient, null);
        this.zzkm = i;
        this.zzkn = iArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzkm, this.zzkn);
    }
}
