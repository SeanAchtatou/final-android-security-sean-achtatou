package com.google.android.gms.games.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.common.util.PlatformVersion;
import java.lang.ref.WeakReference;

@TargetApi(12)
final class zzaf extends zzac implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener {
    private boolean zzfu = false;
    private WeakReference<View> zzjf;

    protected zzaf(zze zze, int i) {
        super(zze, i);
    }

    @TargetApi(17)
    private final void zzc(View view) {
        Display display;
        int i = -1;
        if (PlatformVersion.isAtLeastJellyBeanMR1() && (display = view.getDisplay()) != null) {
            i = display.getDisplayId();
        }
        IBinder windowToken = view.getWindowToken();
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        int width = view.getWidth();
        int height = view.getHeight();
        this.zzjd.zzje = i;
        this.zzjd.zzjb = windowToken;
        this.zzjd.left = iArr[0];
        this.zzjd.top = iArr[1];
        this.zzjd.right = iArr[0] + width;
        this.zzjd.bottom = iArr[1] + height;
        if (this.zzfu) {
            zzbj();
        }
    }

    public final void onGlobalLayout() {
        View view;
        WeakReference<View> weakReference = this.zzjf;
        if (weakReference != null && (view = weakReference.get()) != null) {
            zzc(view);
        }
    }

    public final void onViewAttachedToWindow(View view) {
        zzc(view);
    }

    public final void onViewDetachedFromWindow(View view) {
        this.zzjc.zzbd();
        view.removeOnAttachStateChangeListener(this);
    }

    @TargetApi(16)
    public final void zzb(View view) {
        this.zzjc.zzbd();
        WeakReference<View> weakReference = this.zzjf;
        if (weakReference != null) {
            View view2 = weakReference.get();
            Context context = this.zzjc.getContext();
            if (view2 == null && (context instanceof Activity)) {
                view2 = ((Activity) context).getWindow().getDecorView();
            }
            if (view2 != null) {
                view2.removeOnAttachStateChangeListener(this);
                ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
                if (PlatformVersion.isAtLeastJellyBean()) {
                    viewTreeObserver.removeOnGlobalLayoutListener(this);
                } else {
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                }
            }
        }
        this.zzjf = null;
        Context context2 = this.zzjc.getContext();
        if (view == null && (context2 instanceof Activity)) {
            Activity activity = (Activity) context2;
            view = activity.findViewById(16908290);
            if (view == null) {
                view = activity.getWindow().getDecorView();
            }
            zzh.w("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view. Note that this may not work as expected in multi-screen environments");
        }
        if (view != null) {
            zzc(view);
            this.zzjf = new WeakReference<>(view);
            view.addOnAttachStateChangeListener(this);
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
            return;
        }
        zzh.e("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
    }

    public final void zzbj() {
        boolean z;
        if (this.zzjd.zzjb != null) {
            super.zzbj();
            z = false;
        } else {
            z = true;
        }
        this.zzfu = z;
    }

    /* access modifiers changed from: protected */
    public final void zzm(int i) {
        this.zzjd = new zzae(i, null);
    }
}
