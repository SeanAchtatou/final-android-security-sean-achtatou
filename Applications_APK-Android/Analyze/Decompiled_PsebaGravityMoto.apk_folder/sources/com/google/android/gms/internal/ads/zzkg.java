package com.google.android.gms.internal.ads;

import java.util.Collections;
import org.apache.http.HttpStatus;

public final class zzkg {
    private static final int[] zzaqp = {48000, 44100, 32000};
    private static final int[] zzaqq = {2, 1, 2, 3, 3, 4, 4, 5};
    private static final int[] zzaqr = {32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384, 448, 512, 576, 640};
    private static final int[] zzaqs = {69, 87, 104, 121, 139, 174, 208, 243, 278, 348, HttpStatus.SC_EXPECTATION_FAILED, 487, 557, 696, 835, 975, 1114, 1253, 1393};

    public static zzhj zza(zzkm zzkm) {
        int i = zzaqp[(zzkm.readUnsignedByte() & 192) >> 6];
        int readUnsignedByte = zzkm.readUnsignedByte();
        int i2 = zzaqq[(readUnsignedByte & 56) >> 3];
        if ((readUnsignedByte & 4) != 0) {
            i2++;
        }
        return zzhj.zzb("audio/ac3", -1, -1, i2, i, Collections.emptyList());
    }

    public static zzhj zzb(zzkm zzkm) {
        zzkm.zzac(2);
        int i = zzaqp[(zzkm.readUnsignedByte() & 192) >> 6];
        int readUnsignedByte = zzkm.readUnsignedByte();
        int i2 = zzaqq[(readUnsignedByte & 14) >> 1];
        if ((readUnsignedByte & 1) != 0) {
            i2++;
        }
        return zzhj.zza("audio/eac3", -1, i2, i, Collections.emptyList());
    }

    public static int zza(int i, int i2) {
        return (((i << 3) * i2) + 768000) / 1536000;
    }
}
