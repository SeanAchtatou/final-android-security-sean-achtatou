package com.google.android.gms.internal.ads;

final class zzbee implements zzgh {
    private final /* synthetic */ zzbdq zzegb;

    zzbee(zzbdq zzbdq) {
        this.zzegb = zzbdq;
    }

    public final void zzdo() {
    }

    public final void zza(boolean z, int i) {
        if (this.zzegb.zzeft != i) {
            int unused = this.zzegb.zzeft = i;
            if (i == 4) {
                this.zzegb.zzyy();
            } else if (i == 5) {
                this.zzegb.zzyk();
            }
        }
    }

    public final void zza(zzgd zzgd) {
        this.zzegb.zzn("PlayerError", zzgd.getMessage());
    }
}
