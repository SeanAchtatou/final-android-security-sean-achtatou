package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdk extends zzdm {
    private final /* synthetic */ String zzew;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdk(zzdb zzdb, String str, GoogleApiClient googleApiClient, String str2) {
        super(str, googleApiClient);
        this.zzew = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzf(this, this.zzew);
    }
}
