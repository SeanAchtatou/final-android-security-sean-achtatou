package com.google.android.gms.games.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.FirstPartyScopes;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.api.internal.DataHolderNotifier;
import com.google.android.gms.common.api.internal.DataHolderResult;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesClientStatusCodes;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardEntity;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScoreEntity;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchBuffer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.android.gms.games.quest.Milestone;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.QuestBuffer;
import com.google.android.gms.games.quest.QuestEntity;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.quest.Quests;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotEntity;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.SnapshotMetadataEntity;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.PlayerStatsBuffer;
import com.google.android.gms.games.stats.Stats;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.internal.games.zzej;
import com.google.android.gms.internal.games.zzel;
import com.google.android.gms.internal.games.zzem;
import com.google.android.gms.signin.internal.SignInClientImpl;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class zze extends GmsClient<zzy> {
    private zzel zzfp = new zzf(this);
    private final String zzfq;
    private PlayerEntity zzfr;
    private GameEntity zzfs;
    private final zzac zzft;
    private boolean zzfu = false;
    private final Binder zzfv;
    private final long zzfw;
    private final Games.GamesOptions zzfx;
    private boolean zzfy = false;
    private Bundle zzfz;

    private static abstract class zza extends zzc {
        private final ArrayList<String> zzgc = new ArrayList<>();

        zza(DataHolder dataHolder, String[] strArr) {
            super(dataHolder);
            for (String add : strArr) {
                this.zzgc.add(add);
            }
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            zza(roomStatusUpdateListener, room, this.zzgc);
        }

        /* access modifiers changed from: protected */
        public abstract void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList);
    }

    private static final class zzaa extends zzcr implements TurnBasedMultiplayer.InitiateMatchResult {
        zzaa(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    private static final class zzab extends zza {
        private final ListenerHolder<OnInvitationReceivedListener> zzgj;

        zzab(ListenerHolder<OnInvitationReceivedListener> listenerHolder) {
            this.zzgj = listenerHolder;
        }

        public final void onInvitationRemoved(String str) {
            this.zzgj.notifyListener(new zzad(str));
        }

        public final void zzl(DataHolder dataHolder) {
            InvitationBuffer invitationBuffer = new InvitationBuffer(dataHolder);
            try {
                Invitation invitation = invitationBuffer.getCount() > 0 ? (Invitation) ((Invitation) invitationBuffer.get(0)).freeze() : null;
                if (invitation != null) {
                    this.zzgj.notifyListener(new zzac(invitation));
                }
            } finally {
                invitationBuffer.release();
            }
        }
    }

    private static final class zzac implements ListenerHolder.Notifier<OnInvitationReceivedListener> {
        private final Invitation zzgq;

        zzac(Invitation invitation) {
            this.zzgq = invitation;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((OnInvitationReceivedListener) obj).onInvitationReceived(this.zzgq);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzad implements ListenerHolder.Notifier<OnInvitationReceivedListener> {
        private final String zzgr;

        zzad(String str) {
            this.zzgr = str;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((OnInvitationReceivedListener) obj).onInvitationRemoved(this.zzgr);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzae extends zza {
        private final BaseImplementation.ResultHolder<Invitations.LoadInvitationsResult> zzge;

        zzae(BaseImplementation.ResultHolder<Invitations.LoadInvitationsResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzk(DataHolder dataHolder) {
            this.zzge.setResult(new zzao(dataHolder));
        }
    }

    private static final class zzaf extends zzb {
        public zzaf(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onJoinedRoom(i, room);
        }
    }

    private static final class zzag extends zzw implements Leaderboards.LeaderboardMetadataResult {
        private final LeaderboardBuffer zzgs;

        zzag(DataHolder dataHolder) {
            super(dataHolder);
            this.zzgs = new LeaderboardBuffer(dataHolder);
        }

        public final LeaderboardBuffer getLeaderboards() {
            return this.zzgs;
        }
    }

    private static final class zzah extends zza {
        private final BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> zzge;

        zzah(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zza(DataHolder dataHolder, DataHolder dataHolder2) {
            this.zzge.setResult(new zzaw(dataHolder, dataHolder2));
        }
    }

    private static final class zzai extends zza {
        private final BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> zzge;

        zzai(BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzc(DataHolder dataHolder) {
            this.zzge.setResult(new zzag(dataHolder));
        }
    }

    private static final class zzaj extends zzcr implements TurnBasedMultiplayer.LeaveMatchResult {
        zzaj(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    private static final class zzak implements ListenerHolder.Notifier<RoomUpdateListener> {
        private final int statusCode;
        private final String zzgt;

        zzak(int i, String str) {
            this.statusCode = i;
            this.zzgt = str;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((RoomUpdateListener) obj).onLeftRoom(this.statusCode, this.zzgt);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzal extends zzw implements Achievements.LoadAchievementsResult {
        private final AchievementBuffer zzgu;

        zzal(DataHolder dataHolder) {
            super(dataHolder);
            this.zzgu = new AchievementBuffer(dataHolder);
        }

        public final AchievementBuffer getAchievements() {
            return this.zzgu;
        }
    }

    private static final class zzam extends zzw implements Events.LoadEventsResult {
        private final EventBuffer zzgv;

        zzam(DataHolder dataHolder) {
            super(dataHolder);
            this.zzgv = new EventBuffer(dataHolder);
        }

        public final EventBuffer getEvents() {
            return this.zzgv;
        }
    }

    private static final class zzan extends zzw implements GamesMetadata.LoadGamesResult {
        private final GameBuffer zzgw;

        zzan(DataHolder dataHolder) {
            super(dataHolder);
            this.zzgw = new GameBuffer(dataHolder);
        }

        public final GameBuffer getGames() {
            return this.zzgw;
        }
    }

    private static final class zzao extends zzw implements Invitations.LoadInvitationsResult {
        private final InvitationBuffer zzgx;

        zzao(DataHolder dataHolder) {
            super(dataHolder);
            this.zzgx = new InvitationBuffer(dataHolder);
        }

        public final InvitationBuffer getInvitations() {
            return this.zzgx;
        }
    }

    private static final class zzap extends zzcr implements TurnBasedMultiplayer.LoadMatchResult {
        zzap(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    private static final class zzaq implements TurnBasedMultiplayer.LoadMatchesResult {
        private final Status zzgf;
        private final LoadMatchesResponse zzgy;

        zzaq(Status status, Bundle bundle) {
            this.zzgf = status;
            this.zzgy = new LoadMatchesResponse(bundle);
        }

        public final LoadMatchesResponse getMatches() {
            return this.zzgy;
        }

        public final Status getStatus() {
            return this.zzgf;
        }

        public final void release() {
            this.zzgy.release();
        }
    }

    private static final class zzar extends zzw implements Leaderboards.LoadPlayerScoreResult {
        private final LeaderboardScoreEntity zzgz;

        zzar(DataHolder dataHolder) {
            super(dataHolder);
            LeaderboardScoreBuffer leaderboardScoreBuffer = new LeaderboardScoreBuffer(dataHolder);
            try {
                if (leaderboardScoreBuffer.getCount() > 0) {
                    this.zzgz = (LeaderboardScoreEntity) ((LeaderboardScore) leaderboardScoreBuffer.get(0)).freeze();
                } else {
                    this.zzgz = null;
                }
            } finally {
                leaderboardScoreBuffer.release();
            }
        }

        public final LeaderboardScore getScore() {
            return this.zzgz;
        }
    }

    private static final class zzas extends zzw implements Stats.LoadPlayerStatsResult {
        private final PlayerStats zzha;

        zzas(DataHolder dataHolder) {
            super(dataHolder);
            PlayerStatsBuffer playerStatsBuffer = new PlayerStatsBuffer(dataHolder);
            try {
                if (playerStatsBuffer.getCount() > 0) {
                    this.zzha = new com.google.android.gms.games.stats.zza((PlayerStats) playerStatsBuffer.get(0));
                } else {
                    this.zzha = null;
                }
            } finally {
                playerStatsBuffer.release();
            }
        }

        public final PlayerStats getPlayerStats() {
            return this.zzha;
        }
    }

    private static final class zzat extends zzw implements Players.LoadPlayersResult {
        private final PlayerBuffer zzhb;

        zzat(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhb = new PlayerBuffer(dataHolder);
        }

        public final PlayerBuffer getPlayers() {
            return this.zzhb;
        }
    }

    private static final class zzau extends zzw implements Quests.LoadQuestsResult {
        private final DataHolder zzhc;

        zzau(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhc = dataHolder;
        }

        public final QuestBuffer getQuests() {
            return new QuestBuffer(this.zzhc);
        }
    }

    private static final class zzav implements Requests.LoadRequestsResult {
        private final Status zzgf;
        private final Bundle zzhd;

        zzav(Status status, Bundle bundle) {
            this.zzgf = status;
            this.zzhd = bundle;
        }

        public final GameRequestBuffer getRequests(int i) {
            String str;
            if (i == 1) {
                str = "GIFT";
            } else if (i != 2) {
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unknown request type: ");
                sb.append(i);
                zzh.e("RequestType", sb.toString());
                str = "UNKNOWN_TYPE";
            } else {
                str = "WISH";
            }
            if (!this.zzhd.containsKey(str)) {
                return null;
            }
            return new GameRequestBuffer((DataHolder) this.zzhd.get(str));
        }

        public final Status getStatus() {
            return this.zzgf;
        }

        public final void release() {
            for (String parcelable : this.zzhd.keySet()) {
                DataHolder dataHolder = (DataHolder) this.zzhd.getParcelable(parcelable);
                if (dataHolder != null) {
                    dataHolder.close();
                }
            }
        }
    }

    private static final class zzaw extends zzw implements Leaderboards.LoadScoresResult {
        private final LeaderboardEntity zzhe;
        private final LeaderboardScoreBuffer zzhf;

        /* JADX INFO: finally extract failed */
        zzaw(DataHolder dataHolder, DataHolder dataHolder2) {
            super(dataHolder2);
            LeaderboardBuffer leaderboardBuffer = new LeaderboardBuffer(dataHolder);
            try {
                if (leaderboardBuffer.getCount() > 0) {
                    this.zzhe = (LeaderboardEntity) ((Leaderboard) leaderboardBuffer.get(0)).freeze();
                } else {
                    this.zzhe = null;
                }
                leaderboardBuffer.release();
                this.zzhf = new LeaderboardScoreBuffer(dataHolder2);
            } catch (Throwable th) {
                leaderboardBuffer.release();
                throw th;
            }
        }

        public final Leaderboard getLeaderboard() {
            return this.zzhe;
        }

        public final LeaderboardScoreBuffer getScores() {
            return this.zzhf;
        }
    }

    private static final class zzax extends zzw implements Snapshots.LoadSnapshotsResult {
        zzax(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final SnapshotMetadataBuffer getSnapshots() {
            return new SnapshotMetadataBuffer(this.mDataHolder);
        }
    }

    private static final class zzay implements ListenerHolder.Notifier<OnTurnBasedMatchUpdateReceivedListener> {
        private final String zzhg;

        zzay(String str) {
            this.zzhg = str;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((OnTurnBasedMatchUpdateReceivedListener) obj).onTurnBasedMatchRemoved(this.zzhg);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzaz extends zza {
        private final ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> zzgj;

        zzaz(ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> listenerHolder) {
            this.zzgj = listenerHolder;
        }

        public final void onTurnBasedMatchRemoved(String str) {
            this.zzgj.notifyListener(new zzay(str));
        }

        public final void zzr(DataHolder dataHolder) {
            TurnBasedMatchBuffer turnBasedMatchBuffer = new TurnBasedMatchBuffer(dataHolder);
            try {
                TurnBasedMatch turnBasedMatch = turnBasedMatchBuffer.getCount() > 0 ? (TurnBasedMatch) ((TurnBasedMatch) turnBasedMatchBuffer.get(0)).freeze() : null;
                if (turnBasedMatch != null) {
                    this.zzgj.notifyListener(new zzba(turnBasedMatch));
                }
            } finally {
                turnBasedMatchBuffer.release();
            }
        }
    }

    private static abstract class zzb extends DataHolderNotifier<RoomUpdateListener> {
        zzb(DataHolder dataHolder) {
            super(dataHolder);
        }

        /* access modifiers changed from: protected */
        public /* synthetic */ void notifyListener(Object obj, DataHolder dataHolder) {
            zza((RoomUpdateListener) obj, zze.zzba(dataHolder), dataHolder.getStatusCode());
        }

        /* access modifiers changed from: protected */
        public abstract void zza(RoomUpdateListener roomUpdateListener, Room room, int i);
    }

    private static final class zzba implements ListenerHolder.Notifier<OnTurnBasedMatchUpdateReceivedListener> {
        private final TurnBasedMatch match;

        zzba(TurnBasedMatch turnBasedMatch) {
            this.match = turnBasedMatch;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((OnTurnBasedMatchUpdateReceivedListener) obj).onTurnBasedMatchReceived(this.match);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzbb implements ListenerHolder.Notifier<RealTimeMessageReceivedListener> {
        private final RealTimeMessage zzhh;

        zzbb(RealTimeMessage realTimeMessage) {
            this.zzhh = realTimeMessage;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((RealTimeMessageReceivedListener) obj).onRealTimeMessageReceived(this.zzhh);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzbc extends zzw implements Snapshots.OpenSnapshotResult {
        private final String zzei;
        private final Snapshot zzhi;
        private final Snapshot zzhj;
        private final SnapshotContents zzhk;

        zzbc(DataHolder dataHolder, Contents contents) {
            this(dataHolder, null, contents, null, null);
        }

        /* JADX INFO: finally extract failed */
        zzbc(DataHolder dataHolder, String str, Contents contents, Contents contents2, Contents contents3) {
            super(dataHolder);
            SnapshotMetadataBuffer snapshotMetadataBuffer = new SnapshotMetadataBuffer(dataHolder);
            try {
                if (snapshotMetadataBuffer.getCount() == 0) {
                    this.zzhi = null;
                } else {
                    boolean z = true;
                    if (snapshotMetadataBuffer.getCount() == 1) {
                        if (dataHolder.getStatusCode() == 4004) {
                            z = false;
                        }
                        Asserts.checkState(z);
                        this.zzhi = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(0)), new com.google.android.gms.games.snapshot.zza(contents));
                    } else {
                        this.zzhi = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(0)), new com.google.android.gms.games.snapshot.zza(contents));
                        this.zzhj = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(1)), new com.google.android.gms.games.snapshot.zza(contents2));
                        snapshotMetadataBuffer.release();
                        this.zzei = str;
                        this.zzhk = new com.google.android.gms.games.snapshot.zza(contents3);
                    }
                }
                this.zzhj = null;
                snapshotMetadataBuffer.release();
                this.zzei = str;
                this.zzhk = new com.google.android.gms.games.snapshot.zza(contents3);
            } catch (Throwable th) {
                snapshotMetadataBuffer.release();
                throw th;
            }
        }

        public final String getConflictId() {
            return this.zzei;
        }

        public final Snapshot getConflictingSnapshot() {
            return this.zzhj;
        }

        public final SnapshotContents getResolutionSnapshotContents() {
            return this.zzhk;
        }

        public final Snapshot getSnapshot() {
            return this.zzhi;
        }
    }

    private static final class zzbd implements ListenerHolder.Notifier<RoomStatusUpdateListener> {
        private final String zzhl;

        zzbd(String str) {
            this.zzhl = str;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((RoomStatusUpdateListener) obj).onP2PConnected(this.zzhl);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzbe implements ListenerHolder.Notifier<RoomStatusUpdateListener> {
        private final String zzhl;

        zzbe(String str) {
            this.zzhl = str;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((RoomStatusUpdateListener) obj).onP2PDisconnected(this.zzhl);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzbf extends zza {
        zzbf(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeersConnected(room, arrayList);
        }
    }

    private static final class zzbg extends zza {
        zzbg(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerDeclined(room, arrayList);
        }
    }

    private static final class zzbh extends zza {
        zzbh(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeersDisconnected(room, arrayList);
        }
    }

    private static final class zzbi extends zza {
        zzbi(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerInvitedToRoom(room, arrayList);
        }
    }

    private static final class zzbj extends zza {
        zzbj(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerJoined(room, arrayList);
        }
    }

    private static final class zzbk extends zza {
        zzbk(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerLeft(room, arrayList);
        }
    }

    private static final class zzbl extends zza {
        private final BaseImplementation.ResultHolder<Leaderboards.LoadPlayerScoreResult> zzge;

        zzbl(BaseImplementation.ResultHolder<Leaderboards.LoadPlayerScoreResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzac(DataHolder dataHolder) {
            this.zzge.setResult(new zzar(dataHolder));
        }
    }

    private static final class zzbm extends zza {
        private final BaseImplementation.ResultHolder<Stats.LoadPlayerStatsResult> zzge;

        public zzbm(BaseImplementation.ResultHolder<Stats.LoadPlayerStatsResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzap(DataHolder dataHolder) {
            this.zzge.setResult(new zzas(dataHolder));
        }
    }

    private static final class zzbn extends zza {
        private final BaseImplementation.ResultHolder<Players.LoadPlayersResult> zzge;

        zzbn(BaseImplementation.ResultHolder<Players.LoadPlayersResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zze(DataHolder dataHolder) {
            this.zzge.setResult(new zzat(dataHolder));
        }

        public final void zzf(DataHolder dataHolder) {
            this.zzge.setResult(new zzat(dataHolder));
        }
    }

    private static final class zzbo extends zzb {
        private final zzac zzft;

        public zzbo(zzac zzac) {
            this.zzft = zzac;
        }

        public final zzaa zzn() {
            return new zzaa(this.zzft.zzjd);
        }
    }

    private static final class zzbp extends zza {
        private final BaseImplementation.ResultHolder<Quests.AcceptQuestResult> zzhm;

        public zzbp(BaseImplementation.ResultHolder<Quests.AcceptQuestResult> resultHolder) {
            this.zzhm = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzaj(DataHolder dataHolder) {
            this.zzhm.setResult(new zzd(dataHolder));
        }
    }

    private static final class zzbq implements ListenerHolder.Notifier<QuestUpdateListener> {
        private final Quest zzgd;

        zzbq(Quest quest) {
            this.zzgd = quest;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((QuestUpdateListener) obj).onQuestCompleted(this.zzgd);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzbr extends zza {
        private final BaseImplementation.ResultHolder<Quests.ClaimMilestoneResult> zzhn;
        private final String zzho;

        public zzbr(BaseImplementation.ResultHolder<Quests.ClaimMilestoneResult> resultHolder, String str) {
            this.zzhn = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
            this.zzho = (String) Preconditions.checkNotNull(str, "MilestoneId must not be null");
        }

        public final void zzai(DataHolder dataHolder) {
            this.zzhn.setResult(new zzp(dataHolder, this.zzho));
        }
    }

    private static final class zzbs extends zza {
        private final ListenerHolder<QuestUpdateListener> zzgj;

        zzbs(ListenerHolder<QuestUpdateListener> listenerHolder) {
            this.zzgj = listenerHolder;
        }

        private static Quest zzbc(DataHolder dataHolder) {
            QuestBuffer questBuffer = new QuestBuffer(dataHolder);
            try {
                return questBuffer.getCount() > 0 ? (Quest) ((Quest) questBuffer.get(0)).freeze() : null;
            } finally {
                questBuffer.release();
            }
        }

        public final void zzak(DataHolder dataHolder) {
            Quest zzbc = zzbc(dataHolder);
            if (zzbc != null) {
                this.zzgj.notifyListener(new zzbq(zzbc));
            }
        }
    }

    private static final class zzbt extends zza {
        private final BaseImplementation.ResultHolder<Quests.LoadQuestsResult> zzhp;

        public zzbt(BaseImplementation.ResultHolder<Quests.LoadQuestsResult> resultHolder) {
            this.zzhp = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzam(DataHolder dataHolder) {
            this.zzhp.setResult(new zzau(dataHolder));
        }
    }

    private static final class zzbu implements ListenerHolder.Notifier<RealTimeMultiplayer.ReliableMessageSentCallback> {
        private final int statusCode;
        private final int token;
        private final String zzhq;

        zzbu(int i, int i2, String str) {
            this.statusCode = i;
            this.token = i2;
            this.zzhq = str;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            RealTimeMultiplayer.ReliableMessageSentCallback reliableMessageSentCallback = (RealTimeMultiplayer.ReliableMessageSentCallback) obj;
            if (reliableMessageSentCallback != null) {
                reliableMessageSentCallback.onRealTimeMessageSent(this.statusCode, this.token, this.zzhq);
            }
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzbv extends zza {
        private final ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> zzhr;

        public zzbv(ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> listenerHolder) {
            this.zzhr = listenerHolder;
        }

        public final void zza(int i, int i2, String str) {
            ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> listenerHolder = this.zzhr;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbu(i, i2, str));
            }
        }
    }

    private static final class zzbw extends zza {
        private final ListenerHolder<OnRequestReceivedListener> zzgj;

        zzbw(ListenerHolder<OnRequestReceivedListener> listenerHolder) {
            this.zzgj = listenerHolder;
        }

        public final void onRequestRemoved(String str) {
            this.zzgj.notifyListener(new zzby(str));
        }

        public final void zzm(DataHolder dataHolder) {
            GameRequestBuffer gameRequestBuffer = new GameRequestBuffer(dataHolder);
            try {
                GameRequest gameRequest = gameRequestBuffer.getCount() > 0 ? (GameRequest) ((GameRequest) gameRequestBuffer.get(0)).freeze() : null;
                if (gameRequest != null) {
                    this.zzgj.notifyListener(new zzbx(gameRequest));
                }
            } finally {
                gameRequestBuffer.release();
            }
        }
    }

    private static final class zzbx implements ListenerHolder.Notifier<OnRequestReceivedListener> {
        private final GameRequest zzhs;

        zzbx(GameRequest gameRequest) {
            this.zzhs = gameRequest;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((OnRequestReceivedListener) obj).onRequestReceived(this.zzhs);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzby implements ListenerHolder.Notifier<OnRequestReceivedListener> {
        private final String zzht;

        zzby(String str) {
            this.zzht = str;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((OnRequestReceivedListener) obj).onRequestRemoved(this.zzht);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzbz extends zza {
        private final BaseImplementation.ResultHolder<Requests.LoadRequestsResult> zzhu;

        public zzbz(BaseImplementation.ResultHolder<Requests.LoadRequestsResult> resultHolder) {
            this.zzhu = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzb(int i, Bundle bundle) {
            bundle.setClassLoader(getClass().getClassLoader());
            this.zzhu.setResult(new zzav(GamesStatusCodes.zza(i), bundle));
        }
    }

    private static abstract class zzc extends DataHolderNotifier<RoomStatusUpdateListener> {
        zzc(DataHolder dataHolder) {
            super(dataHolder);
        }

        /* access modifiers changed from: protected */
        public /* synthetic */ void notifyListener(Object obj, DataHolder dataHolder) {
            zza((RoomStatusUpdateListener) obj, zze.zzba(dataHolder));
        }

        /* access modifiers changed from: protected */
        public abstract void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room);
    }

    private static final class zzca extends zza {
        private final BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> zzhv;

        public zzca(BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> resultHolder) {
            this.zzhv = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzad(DataHolder dataHolder) {
            this.zzhv.setResult(new zzcw(dataHolder));
        }
    }

    private static final class zzcb extends zzc {
        zzcb(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onRoomAutoMatching(room);
        }
    }

    private static final class zzcc extends zza {
        private final ListenerHolder<? extends RoomUpdateListener> zzhw;
        private final ListenerHolder<? extends RoomStatusUpdateListener> zzhx;
        private final ListenerHolder<? extends RealTimeMessageReceivedListener> zzhy;

        public zzcc(ListenerHolder<? extends RoomUpdateListener> listenerHolder) {
            this.zzhw = (ListenerHolder) Preconditions.checkNotNull(listenerHolder, "Callbacks must not be null");
            this.zzhx = null;
            this.zzhy = null;
        }

        public zzcc(ListenerHolder<? extends RoomUpdateListener> listenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder2, ListenerHolder<? extends RealTimeMessageReceivedListener> listenerHolder3) {
            this.zzhw = (ListenerHolder) Preconditions.checkNotNull(listenerHolder, "Callbacks must not be null");
            this.zzhx = listenerHolder2;
            this.zzhy = listenerHolder3;
        }

        public final void onLeftRoom(int i, String str) {
            this.zzhw.notifyListener(new zzak(i, str));
        }

        public final void onP2PConnected(String str) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbd(str));
            }
        }

        public final void onP2PDisconnected(String str) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbe(str));
            }
        }

        public final void onRealTimeMessageReceived(RealTimeMessage realTimeMessage) {
            ListenerHolder<? extends RealTimeMessageReceivedListener> listenerHolder = this.zzhy;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbb(realTimeMessage));
            }
        }

        public final void zza(DataHolder dataHolder, String[] strArr) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbi(dataHolder, strArr));
            }
        }

        public final void zzb(DataHolder dataHolder, String[] strArr) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbj(dataHolder, strArr));
            }
        }

        public final void zzc(DataHolder dataHolder, String[] strArr) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbk(dataHolder, strArr));
            }
        }

        public final void zzd(DataHolder dataHolder, String[] strArr) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbg(dataHolder, strArr));
            }
        }

        public final void zze(DataHolder dataHolder, String[] strArr) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbf(dataHolder, strArr));
            }
        }

        public final void zzf(DataHolder dataHolder, String[] strArr) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzbh(dataHolder, strArr));
            }
        }

        public final void zzs(DataHolder dataHolder) {
            this.zzhw.notifyListener(new zzcf(dataHolder));
        }

        public final void zzt(DataHolder dataHolder) {
            this.zzhw.notifyListener(new zzaf(dataHolder));
        }

        public final void zzu(DataHolder dataHolder) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzce(dataHolder));
            }
        }

        public final void zzv(DataHolder dataHolder) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzcb(dataHolder));
            }
        }

        public final void zzw(DataHolder dataHolder) {
            this.zzhw.notifyListener(new zzcd(dataHolder));
        }

        public final void zzx(DataHolder dataHolder) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzr(dataHolder));
            }
        }

        public final void zzy(DataHolder dataHolder) {
            ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder = this.zzhx;
            if (listenerHolder != null) {
                listenerHolder.notifyListener(new zzt(dataHolder));
            }
        }
    }

    private static final class zzcd extends zzb {
        zzcd(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onRoomConnected(i, room);
        }
    }

    private static final class zzce extends zzc {
        zzce(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onRoomConnecting(room);
        }
    }

    private static final class zzcf extends zzb {
        public zzcf(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onRoomCreated(i, room);
        }
    }

    private static final class zzcg extends zza {
        private final BaseImplementation.ResultHolder<Status> zzge;

        public zzcg(BaseImplementation.ResultHolder<Status> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void onSignOutComplete() {
            this.zzge.setResult(GamesStatusCodes.zza(0));
        }
    }

    private static final class zzch extends zza {
        private final BaseImplementation.ResultHolder<Snapshots.CommitSnapshotResult> zzhz;

        public zzch(BaseImplementation.ResultHolder<Snapshots.CommitSnapshotResult> resultHolder) {
            this.zzhz = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzah(DataHolder dataHolder) {
            this.zzhz.setResult(new zzq(dataHolder));
        }
    }

    static final class zzci extends zza {
        private final BaseImplementation.ResultHolder<Snapshots.DeleteSnapshotResult> zzge;

        public zzci(BaseImplementation.ResultHolder<Snapshots.DeleteSnapshotResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzd(int i, String str) {
            this.zzge.setResult(new zzs(i, str));
        }
    }

    private static final class zzcj extends zza {
        private final BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> zzia;

        public zzcj(BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> resultHolder) {
            this.zzia = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zza(DataHolder dataHolder, Contents contents) {
            this.zzia.setResult(new zzbc(dataHolder, contents));
        }

        public final void zza(DataHolder dataHolder, String str, Contents contents, Contents contents2, Contents contents3) {
            this.zzia.setResult(new zzbc(dataHolder, str, contents, contents2, contents3));
        }
    }

    private static final class zzck extends zza {
        private final BaseImplementation.ResultHolder<Snapshots.LoadSnapshotsResult> zzib;

        public zzck(BaseImplementation.ResultHolder<Snapshots.LoadSnapshotsResult> resultHolder) {
            this.zzib = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzag(DataHolder dataHolder) {
            this.zzib.setResult(new zzax(dataHolder));
        }
    }

    private static final class zzcl extends zza {
        private final BaseImplementation.ResultHolder<Leaderboards.SubmitScoreResult> zzge;

        public zzcl(BaseImplementation.ResultHolder<Leaderboards.SubmitScoreResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzd(DataHolder dataHolder) {
            this.zzge.setResult(new zzcm(dataHolder));
        }
    }

    private static final class zzcm extends zzw implements Leaderboards.SubmitScoreResult {
        private final ScoreSubmissionData zzic;

        public zzcm(DataHolder dataHolder) {
            super(dataHolder);
            try {
                this.zzic = new ScoreSubmissionData(dataHolder);
            } finally {
                dataHolder.close();
            }
        }

        public final ScoreSubmissionData getScoreData() {
            return this.zzic;
        }
    }

    private static final class zzcn extends zza {
        private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.CancelMatchResult> zzid;

        public zzcn(BaseImplementation.ResultHolder<TurnBasedMultiplayer.CancelMatchResult> resultHolder) {
            this.zzid = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzc(int i, String str) {
            this.zzid.setResult(new zzg(GamesStatusCodes.zza(i), str));
        }
    }

    private static final class zzco extends zza {
        private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> zzie;

        public zzco(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> resultHolder) {
            this.zzie = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzo(DataHolder dataHolder) {
            this.zzie.setResult(new zzaa(dataHolder));
        }
    }

    private static final class zzcp extends zza {
        private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> zzif;

        public zzcp(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> resultHolder) {
            this.zzif = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzq(DataHolder dataHolder) {
            this.zzif.setResult(new zzaj(dataHolder));
        }
    }

    private static final class zzcq extends zza {
        private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchResult> zzig;

        public zzcq(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchResult> resultHolder) {
            this.zzig = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzn(DataHolder dataHolder) {
            this.zzig.setResult(new zzap(dataHolder));
        }
    }

    private static abstract class zzcr extends zzw {
        private final TurnBasedMatch match;

        zzcr(DataHolder dataHolder) {
            super(dataHolder);
            TurnBasedMatchBuffer turnBasedMatchBuffer = new TurnBasedMatchBuffer(dataHolder);
            try {
                if (turnBasedMatchBuffer.getCount() > 0) {
                    this.match = (TurnBasedMatch) ((TurnBasedMatch) turnBasedMatchBuffer.get(0)).freeze();
                } else {
                    this.match = null;
                }
            } finally {
                turnBasedMatchBuffer.release();
            }
        }

        public TurnBasedMatch getMatch() {
            return this.match;
        }
    }

    private static final class zzcs extends zza {
        private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> zzih;

        public zzcs(BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> resultHolder) {
            this.zzih = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzp(DataHolder dataHolder) {
            this.zzih.setResult(new zzcv(dataHolder));
        }
    }

    private static final class zzct extends zza {
        private final BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchesResult> zzii;

        public zzct(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchesResult> resultHolder) {
            this.zzii = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zza(int i, Bundle bundle) {
            bundle.setClassLoader(getClass().getClassLoader());
            this.zzii.setResult(new zzaq(GamesStatusCodes.zza(i), bundle));
        }
    }

    private static final class zzcu implements Achievements.UpdateAchievementResult {
        private final String zzfa;
        private final Status zzgf;

        zzcu(int i, String str) {
            this.zzgf = GamesStatusCodes.zza(i);
            this.zzfa = str;
        }

        public final String getAchievementId() {
            return this.zzfa;
        }

        public final Status getStatus() {
            return this.zzgf;
        }
    }

    private static final class zzcv extends zzcr implements TurnBasedMultiplayer.UpdateMatchResult {
        zzcv(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    private static final class zzcw extends zzw implements Requests.UpdateRequestsResult {
        private final zzem zzij;

        zzcw(DataHolder dataHolder) {
            super(dataHolder);
            this.zzij = zzem.zzbd(dataHolder);
        }

        public final Set<String> getRequestIds() {
            return this.zzij.getRequestIds();
        }

        public final int getRequestOutcome(String str) {
            return this.zzij.getRequestOutcome(str);
        }
    }

    private static final class zzd extends zzw implements Quests.AcceptQuestResult {
        private final Quest zzgd;

        zzd(DataHolder dataHolder) {
            super(dataHolder);
            QuestBuffer questBuffer = new QuestBuffer(dataHolder);
            try {
                if (questBuffer.getCount() > 0) {
                    this.zzgd = new QuestEntity((Quest) questBuffer.get(0));
                } else {
                    this.zzgd = null;
                }
            } finally {
                questBuffer.release();
            }
        }

        public final Quest getQuest() {
            return this.zzgd;
        }
    }

    /* renamed from: com.google.android.gms.games.internal.zze$zze  reason: collision with other inner class name */
    private static final class C0001zze extends zza {
        private final BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> zzge;

        C0001zze(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzb(int i, String str) {
            this.zzge.setResult(new zzcu(i, str));
        }
    }

    private static final class zzf extends zza {
        private final BaseImplementation.ResultHolder<Achievements.LoadAchievementsResult> zzge;

        zzf(BaseImplementation.ResultHolder<Achievements.LoadAchievementsResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zza(DataHolder dataHolder) {
            this.zzge.setResult(new zzal(dataHolder));
        }
    }

    private static final class zzg implements TurnBasedMultiplayer.CancelMatchResult {
        private final Status zzgf;
        private final String zzgg;

        zzg(Status status, String str) {
            this.zzgf = status;
            this.zzgg = str;
        }

        public final String getMatchId() {
            return this.zzgg;
        }

        public final Status getStatus() {
            return this.zzgf;
        }
    }

    private static final class zzh extends zza {
        private final BaseImplementation.ResultHolder<Videos.CaptureAvailableResult> zzge;

        zzh(BaseImplementation.ResultHolder<Videos.CaptureAvailableResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zza(int i, boolean z) {
            this.zzge.setResult(new zzi(new Status(i), z));
        }
    }

    private static final class zzi implements Videos.CaptureAvailableResult {
        private final Status zzgf;
        private final boolean zzgh;

        zzi(Status status, boolean z) {
            this.zzgf = status;
            this.zzgh = z;
        }

        public final Status getStatus() {
            return this.zzgf;
        }

        public final boolean isAvailable() {
            return this.zzgh;
        }
    }

    private static final class zzj extends zza {
        private final BaseImplementation.ResultHolder<Videos.CaptureCapabilitiesResult> zzge;

        zzj(BaseImplementation.ResultHolder<Videos.CaptureCapabilitiesResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zza(int i, VideoCapabilities videoCapabilities) {
            this.zzge.setResult(new zzk(new Status(i), videoCapabilities));
        }
    }

    private static final class zzk implements Videos.CaptureCapabilitiesResult {
        private final Status zzgf;
        private final VideoCapabilities zzgi;

        zzk(Status status, VideoCapabilities videoCapabilities) {
            this.zzgf = status;
            this.zzgi = videoCapabilities;
        }

        public final VideoCapabilities getCapabilities() {
            return this.zzgi;
        }

        public final Status getStatus() {
            return this.zzgf;
        }
    }

    private static final class zzl extends zza {
        private final ListenerHolder<Videos.CaptureOverlayStateListener> zzgj;

        zzl(ListenerHolder<Videos.CaptureOverlayStateListener> listenerHolder) {
            this.zzgj = (ListenerHolder) Preconditions.checkNotNull(listenerHolder, "Callback must not be null");
        }

        public final void onCaptureOverlayStateChanged(int i) {
            this.zzgj.notifyListener(new zzm(i));
        }
    }

    private static final class zzm implements ListenerHolder.Notifier<Videos.CaptureOverlayStateListener> {
        private final int zzgk;

        zzm(int i) {
            this.zzgk = i;
        }

        public final /* synthetic */ void notifyListener(Object obj) {
            ((Videos.CaptureOverlayStateListener) obj).onCaptureOverlayStateChanged(this.zzgk);
        }

        public final void onNotifyListenerFailed() {
        }
    }

    private static final class zzn extends zza {
        private final BaseImplementation.ResultHolder<Videos.CaptureStateResult> zzge;

        public zzn(BaseImplementation.ResultHolder<Videos.CaptureStateResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzg(int i, Bundle bundle) {
            this.zzge.setResult(new zzo(new Status(i), CaptureState.zzb(bundle)));
        }
    }

    private static final class zzo implements Videos.CaptureStateResult {
        private final Status zzgf;
        private final CaptureState zzgl;

        zzo(Status status, CaptureState captureState) {
            this.zzgf = status;
            this.zzgl = captureState;
        }

        public final CaptureState getCaptureState() {
            return this.zzgl;
        }

        public final Status getStatus() {
            return this.zzgf;
        }
    }

    private static final class zzp extends zzw implements Quests.ClaimMilestoneResult {
        private final Quest zzgd;
        private final Milestone zzgm;

        zzp(DataHolder dataHolder, String str) {
            super(dataHolder);
            QuestBuffer questBuffer = new QuestBuffer(dataHolder);
            try {
                if (questBuffer.getCount() > 0) {
                    this.zzgd = new QuestEntity((Quest) questBuffer.get(0));
                    List<Milestone> zzcj = this.zzgd.zzcj();
                    int size = zzcj.size();
                    for (int i = 0; i < size; i++) {
                        if (zzcj.get(i).getMilestoneId().equals(str)) {
                            this.zzgm = zzcj.get(i);
                            return;
                        }
                    }
                    this.zzgm = null;
                } else {
                    this.zzgm = null;
                    this.zzgd = null;
                }
                questBuffer.release();
            } finally {
                questBuffer.release();
            }
        }

        public final Milestone getMilestone() {
            return this.zzgm;
        }

        public final Quest getQuest() {
            return this.zzgd;
        }
    }

    private static final class zzq extends zzw implements Snapshots.CommitSnapshotResult {
        private final SnapshotMetadata zzgn;

        zzq(DataHolder dataHolder) {
            super(dataHolder);
            SnapshotMetadataBuffer snapshotMetadataBuffer = new SnapshotMetadataBuffer(dataHolder);
            try {
                if (snapshotMetadataBuffer.getCount() > 0) {
                    this.zzgn = new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(0));
                } else {
                    this.zzgn = null;
                }
            } finally {
                snapshotMetadataBuffer.release();
            }
        }

        public final SnapshotMetadata getSnapshotMetadata() {
            return this.zzgn;
        }
    }

    private static final class zzr extends zzc {
        zzr(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onConnectedToRoom(room);
        }
    }

    private static final class zzs implements Snapshots.DeleteSnapshotResult {
        private final Status zzgf;
        private final String zzgo;

        zzs(int i, String str) {
            this.zzgf = GamesStatusCodes.zza(i);
            this.zzgo = str;
        }

        public final String getSnapshotId() {
            return this.zzgo;
        }

        public final Status getStatus() {
            return this.zzgf;
        }
    }

    private static final class zzt extends zzc {
        zzt(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onDisconnectedFromRoom(room);
        }
    }

    private static final class zzu extends zza {
        private final BaseImplementation.ResultHolder<Events.LoadEventsResult> zzge;

        zzu(BaseImplementation.ResultHolder<Events.LoadEventsResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzb(DataHolder dataHolder) {
            this.zzge.setResult(new zzam(dataHolder));
        }
    }

    private class zzv extends zzej {
        public zzv() {
            super(zze.this.getContext().getMainLooper(), 1000);
        }

        /* access modifiers changed from: protected */
        public final void zzf(String str, int i) {
            try {
                if (zze.this.isConnected()) {
                    ((zzy) zze.this.getService()).zza(str, i);
                    return;
                }
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 89);
                sb.append("Unable to increment event ");
                sb.append(str);
                sb.append(" by ");
                sb.append(i);
                sb.append(" because the games client is no longer connected");
                zzh.e("GamesClientImpl", sb.toString());
            } catch (RemoteException e) {
                zze.zza(e);
            } catch (SecurityException e2) {
                zze.zza(e2);
            }
        }
    }

    private static abstract class zzw extends DataHolderResult {
        protected zzw(DataHolder dataHolder) {
            super(dataHolder, GamesStatusCodes.zza(dataHolder.getStatusCode()));
        }
    }

    private static final class zzx extends zza {
        private final BaseImplementation.ResultHolder<GamesMetadata.LoadGamesResult> zzge;

        zzx(BaseImplementation.ResultHolder<GamesMetadata.LoadGamesResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zzg(DataHolder dataHolder) {
            this.zzge.setResult(new zzan(dataHolder));
        }
    }

    private static final class zzy extends zza {
        private final BaseImplementation.ResultHolder<Games.GetServerAuthCodeResult> zzge;

        public zzy(BaseImplementation.ResultHolder<Games.GetServerAuthCodeResult> resultHolder) {
            this.zzge = (BaseImplementation.ResultHolder) Preconditions.checkNotNull(resultHolder, "Holder must not be null");
        }

        public final void zza(int i, String str) {
            this.zzge.setResult(new zzz(GamesStatusCodes.zza(i), str));
        }
    }

    private static final class zzz implements Games.GetServerAuthCodeResult {
        private final Status zzgf;
        private final String zzgp;

        zzz(Status status, String str) {
            this.zzgf = status;
            this.zzgp = str;
        }

        public final String getCode() {
            return this.zzgp;
        }

        public final Status getStatus() {
            return this.zzgf;
        }
    }

    public zze(Context context, Looper looper, ClientSettings clientSettings, Games.GamesOptions gamesOptions, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 1, clientSettings, connectionCallbacks, onConnectionFailedListener);
        this.zzfq = clientSettings.getRealClientPackageName();
        this.zzfv = new Binder();
        this.zzft = zzac.zza(this, clientSettings.getGravityForPopups());
        this.zzfw = (long) hashCode();
        this.zzfx = gamesOptions;
        if (this.zzfx.zzaz) {
            return;
        }
        if (clientSettings.getViewForPopups() != null || (context instanceof Activity)) {
            zza(clientSettings.getViewForPopups());
        }
    }

    /* access modifiers changed from: private */
    public static void zza(RemoteException remoteException) {
        zzh.w("GamesClientImpl", "service died", remoteException);
    }

    private static <R> void zza(BaseImplementation.ResultHolder resultHolder, SecurityException securityException) {
        if (resultHolder != null) {
            resultHolder.setFailedResult(GamesClientStatusCodes.zza(4));
        }
    }

    /* access modifiers changed from: private */
    public static void zza(SecurityException securityException) {
        zzh.e("GamesClientImpl", "Is player signed out?", securityException);
    }

    /* access modifiers changed from: private */
    public static Room zzba(DataHolder dataHolder) {
        com.google.android.gms.games.multiplayer.realtime.zzb zzb2 = new com.google.android.gms.games.multiplayer.realtime.zzb(dataHolder);
        try {
            return zzb2.getCount() > 0 ? (Room) ((Room) zzb2.get(0)).freeze() : null;
        } finally {
            zzb2.release();
        }
    }

    public void connect(BaseGmsClient.ConnectionProgressReportCallbacks connectionProgressReportCallbacks) {
        this.zzfr = null;
        this.zzfs = null;
        super.connect(connectionProgressReportCallbacks);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ IInterface createServiceInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.games.internal.IGamesService");
        return queryLocalInterface instanceof zzy ? (zzy) queryLocalInterface : new zzz(iBinder);
    }

    public void disconnect() {
        this.zzfu = false;
        if (isConnected()) {
            try {
                zzy zzy2 = (zzy) getService();
                zzy2.zzbd();
                this.zzfp.flush();
                zzy2.zza(this.zzfw);
            } catch (RemoteException unused) {
                zzh.w("GamesClientImpl", "Failed to notify client disconnect.");
            }
        }
        super.disconnect();
    }

    public Bundle getConnectionHint() {
        try {
            Bundle connectionHint = ((zzy) getService()).getConnectionHint();
            if (connectionHint != null) {
                connectionHint.setClassLoader(zze.class.getClassLoader());
                this.zzfz = connectionHint;
            }
            return connectionHint;
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public Bundle getGetServiceRequestExtraArgs() {
        String locale = getContext().getResources().getConfiguration().locale.toString();
        Bundle zzf2 = this.zzfx.zzf();
        zzf2.putString(ServiceSpecificExtraArgs.GamesExtraArgs.GAME_PACKAGE_NAME, this.zzfq);
        zzf2.putString(ServiceSpecificExtraArgs.GamesExtraArgs.DESIRED_LOCALE, locale);
        zzf2.putParcelable(ServiceSpecificExtraArgs.GamesExtraArgs.WINDOW_TOKEN, new BinderWrapper(this.zzft.zzjd.zzjb));
        zzf2.putInt("com.google.android.gms.games.key.API_VERSION", 6);
        zzf2.putBundle(ServiceSpecificExtraArgs.GamesExtraArgs.SIGNIN_OPTIONS, SignInClientImpl.createBundleFromClientSettings(getClientSettings()));
        return zzf2;
    }

    public int getMinApkVersion() {
        return GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    /* access modifiers changed from: protected */
    public String getServiceDescriptor() {
        return "com.google.android.gms.games.internal.IGamesService";
    }

    /* access modifiers changed from: protected */
    public String getStartServiceAction() {
        return "com.google.android.gms.games.service.START";
    }

    public /* synthetic */ void onConnectedLocked(@NonNull IInterface iInterface) {
        zzy zzy2 = (zzy) iInterface;
        super.onConnectedLocked(zzy2);
        if (this.zzfu) {
            this.zzft.zzbj();
            this.zzfu = false;
        }
        if (!this.zzfx.zzar && !this.zzfx.zzaz) {
            try {
                zzy2.zza(new zzbo(this.zzft), this.zzfw);
            } catch (RemoteException e) {
                zza(e);
            }
        }
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        super.onConnectionFailed(connectionResult);
        this.zzfu = false;
    }

    /* access modifiers changed from: protected */
    public void onPostInitHandler(int i, IBinder iBinder, Bundle bundle, int i2) {
        if (i == 0 && bundle != null) {
            bundle.setClassLoader(zze.class.getClassLoader());
            this.zzfu = bundle.getBoolean("show_welcome_popup");
            this.zzfy = this.zzfu;
            this.zzfr = (PlayerEntity) bundle.getParcelable("com.google.android.gms.games.current_player");
            this.zzfs = (GameEntity) bundle.getParcelable("com.google.android.gms.games.current_game");
        }
        super.onPostInitHandler(i, iBinder, bundle, i2);
    }

    public void onUserSignOut(@NonNull BaseGmsClient.SignOutCallbacks signOutCallbacks) {
        try {
            zzb(new zzg(this, signOutCallbacks));
        } catch (RemoteException unused) {
            signOutCallbacks.onSignOutComplete();
        }
    }

    public boolean requiresSignIn() {
        return true;
    }

    /* access modifiers changed from: protected */
    public Set<Scope> validateScopes(Set<Scope> set) {
        HashSet hashSet = new HashSet(set);
        boolean contains = set.contains(Games.SCOPE_GAMES);
        boolean contains2 = set.contains(Games.SCOPE_GAMES_LITE);
        if (set.contains(Games.zzal)) {
            Preconditions.checkState(!contains, "Cannot have both %s and %s!", Scopes.GAMES, FirstPartyScopes.GAMES_1P);
        } else {
            Preconditions.checkState(contains || contains2, "Games APIs requires %s function.", Scopes.GAMES_LITE);
            if (contains2 && contains) {
                hashSet.remove(Games.SCOPE_GAMES_LITE);
            }
        }
        return hashSet;
    }

    public final int zza(ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> listenerHolder, byte[] bArr, String str, String str2) throws RemoteException {
        return ((zzy) getService()).zza(new zzbv(listenerHolder), bArr, str, str2);
    }

    public final int zza(byte[] bArr, String str) throws RemoteException {
        return ((zzy) getService()).zzb(bArr, str, (String[]) null);
    }

    public final int zza(byte[] bArr, String str, String[] strArr) {
        Preconditions.checkNotNull(strArr, "Participant IDs must not be null");
        try {
            Preconditions.checkNotNull(strArr, "Participant IDs must not be null");
            return ((zzy) getService()).zzb(bArr, str, strArr);
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final Intent zza(int i, int i2, boolean z) throws RemoteException {
        return ((zzy) getService()).zza(i, i2, z);
    }

    public final Intent zza(int i, byte[] bArr, int i2, Bitmap bitmap, String str) {
        try {
            Intent zza2 = ((zzy) getService()).zza(i, bArr, i2, str);
            Preconditions.checkNotNull(bitmap, "Must provide a non null icon");
            zza2.putExtra("com.google.android.gms.games.REQUEST_ITEM_ICON", bitmap);
            return zza2;
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zza(PlayerEntity playerEntity) throws RemoteException {
        return ((zzy) getService()).zza(playerEntity);
    }

    public final Intent zza(Room room, int i) throws RemoteException {
        return ((zzy) getService()).zza((RoomEntity) room.freeze(), i);
    }

    public final Intent zza(String str, int i, int i2) {
        try {
            return ((zzy) getService()).zzb(str, i, i2);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zza(String str, boolean z, boolean z2, int i) throws RemoteException {
        return ((zzy) getService()).zza(str, z, z2, i);
    }

    public final Intent zza(int[] iArr) {
        try {
            return ((zzy) getService()).zza(iArr);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final String zza(boolean z) throws RemoteException {
        PlayerEntity playerEntity = this.zzfr;
        return playerEntity != null ? playerEntity.getPlayerId() : ((zzy) getService()).zzbf();
    }

    public final void zza(IBinder iBinder, Bundle bundle) {
        if (isConnected()) {
            try {
                ((zzy) getService()).zza(iBinder, bundle);
            } catch (RemoteException e) {
                zza(e);
            }
        }
    }

    public final void zza(View view) {
        this.zzft.zzb(view);
    }

    public final void zza(BaseImplementation.ResultHolder<GamesMetadata.LoadGamesResult> resultHolder) throws RemoteException {
        try {
            ((zzy) getService()).zzb(new zzx(resultHolder));
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int):void
     arg types: [com.google.android.gms.games.internal.zze$zzae, int]
     candidates:
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.multiplayer.realtime.RoomEntity, int):android.content.Intent
      com.google.android.gms.games.internal.zzy.zza(android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, boolean):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String[]):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzw, long):void
      com.google.android.gms.games.internal.zzy.zza(java.lang.String, int):void
      com.google.android.gms.games.internal.zzy.zza(java.lang.String, com.google.android.gms.games.internal.zzu):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int):void */
    public final void zza(BaseImplementation.ResultHolder<Invitations.LoadInvitationsResult> resultHolder, int i) throws RemoteException {
        try {
            ((zzy) getService()).zza((zzu) new zzae(resultHolder), i);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Requests.LoadRequestsResult> resultHolder, int i, int i2, int i3) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzbz(resultHolder), i, i2, i3);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int, boolean, boolean):void
     arg types: [com.google.android.gms.games.internal.zze$zzbn, int, boolean, boolean]
     candidates:
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, byte[], java.lang.String, java.lang.String):int
      com.google.android.gms.games.internal.zzy.zza(int, byte[], int, java.lang.String):android.content.Intent
      com.google.android.gms.games.internal.zzy.zza(java.lang.String, boolean, boolean, int):android.content.Intent
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int, int, int):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, android.os.Bundle, int, int):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, long, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, com.google.android.gms.games.snapshot.zze, com.google.android.gms.drive.Contents):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, boolean, int):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, byte[], com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int[], int, boolean):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int, boolean, boolean):void */
    public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> resultHolder, int i, boolean z, boolean z2) throws RemoteException {
        try {
            ((zzy) getService()).zza((zzu) new zzbn(resultHolder), i, z, z2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchesResult> resultHolder, int i, int[] iArr) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzct(resultHolder), i, iArr);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> resultHolder, LeaderboardScoreBuffer leaderboardScoreBuffer, int i, int i2) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzah(resultHolder), leaderboardScoreBuffer.zzcb().zzcc(), i, i2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> resultHolder, TurnBasedMatchConfig turnBasedMatchConfig) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzco(resultHolder), turnBasedMatchConfig.getVariant(), turnBasedMatchConfig.zzci(), turnBasedMatchConfig.getInvitedPlayerIds(), turnBasedMatchConfig.getAutoMatchCriteria());
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Snapshots.CommitSnapshotResult> resultHolder, Snapshot snapshot, SnapshotMetadataChange snapshotMetadataChange) throws RemoteException {
        SnapshotContents snapshotContents = snapshot.getSnapshotContents();
        Preconditions.checkState(!snapshotContents.isClosed(), "Snapshot already closed");
        BitmapTeleporter zzcm2 = snapshotMetadataChange.zzcm();
        if (zzcm2 != null) {
            zzcm2.setTempDir(getContext().getCacheDir());
        }
        Contents zzcl2 = snapshotContents.zzcl();
        snapshotContents.close();
        try {
            ((zzy) getService()).zza(new zzch(resultHolder), snapshot.getMetadata().getSnapshotId(), (com.google.android.gms.games.snapshot.zze) snapshotMetadataChange, zzcl2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zza(resultHolder == null ? null : new C0001zze(resultHolder), str, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> resultHolder, String str, int i) throws RemoteException {
        try {
            ((zzy) getService()).zza(resultHolder == null ? null : new C0001zze(resultHolder), str, i, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> resultHolder, String str, int i, int i2, int i3, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzah(resultHolder), str, i, i2, i3, z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> resultHolder, String str, int i, boolean z, boolean z2) throws RemoteException {
        if (((str.hashCode() == 156408498 && str.equals("played_with")) ? (char) 0 : 65535) != 0) {
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Invalid player collection: ".concat(valueOf) : new String("Invalid player collection: "));
        }
        try {
            ((zzy) getService()).zza(new zzbn(resultHolder), str, i, z, z2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Leaderboards.SubmitScoreResult> resultHolder, String str, long j, String str2) throws RemoteException {
        try {
            ((zzy) getService()).zza(resultHolder == null ? null : new zzcl(resultHolder), str, j, str2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> resultHolder, String str, String str2) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzcp(resultHolder), str, str2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Leaderboards.LoadPlayerScoreResult> resultHolder, String str, String str2, int i, int i2) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzbl(resultHolder), (String) null, str2, i, i2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> resultHolder, String str, String str2, SnapshotMetadataChange snapshotMetadataChange, SnapshotContents snapshotContents) throws RemoteException {
        Preconditions.checkState(!snapshotContents.isClosed(), "SnapshotContents already closed");
        BitmapTeleporter zzcm2 = snapshotMetadataChange.zzcm();
        if (zzcm2 != null) {
            zzcm2.setTempDir(getContext().getCacheDir());
        }
        Contents zzcl2 = snapshotContents.zzcl();
        snapshotContents.close();
        try {
            ((zzy) getService()).zza(new zzcj(resultHolder), str, str2, (com.google.android.gms.games.snapshot.zze) snapshotMetadataChange, zzcl2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> resultHolder, String str, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zzb(new zzbn(resultHolder), str, z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Snapshots.OpenSnapshotResult> resultHolder, String str, boolean z, int i) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzcj(resultHolder), str, z, i);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> resultHolder, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzcs(resultHolder), str, bArr, str2, participantResultArr);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<TurnBasedMultiplayer.UpdateMatchResult> resultHolder, String str, byte[] bArr, ParticipantResult[] participantResultArr) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzcs(resultHolder), str, bArr, participantResultArr);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zzc(com.google.android.gms.games.internal.zzu, boolean):void
     arg types: [com.google.android.gms.games.internal.zze$zzbn, boolean]
     candidates:
      com.google.android.gms.games.internal.zzy.zzc(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzy.zzc(com.google.android.gms.games.internal.zzu, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zzc(com.google.android.gms.games.internal.zzu, boolean):void */
    public final void zza(BaseImplementation.ResultHolder<Players.LoadPlayersResult> resultHolder, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zzc((zzu) new zzbn(resultHolder), z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Events.LoadEventsResult> resultHolder, boolean z, String... strArr) throws RemoteException {
        this.zzfp.flush();
        try {
            ((zzy) getService()).zza(new zzu(resultHolder), z, strArr);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Quests.LoadQuestsResult> resultHolder, int[] iArr, int i, boolean z) throws RemoteException {
        this.zzfp.flush();
        try {
            ((zzy) getService()).zza(new zzbt(resultHolder), iArr, i, z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> resultHolder, String[] strArr) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzca(resultHolder), strArr);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zza(ListenerHolder<OnInvitationReceivedListener> listenerHolder) throws RemoteException {
        ((zzy) getService()).zza(new zzab(listenerHolder), this.zzfw);
    }

    public final void zza(ListenerHolder<? extends RoomUpdateListener> listenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder2, ListenerHolder<? extends RealTimeMessageReceivedListener> listenerHolder3, RoomConfig roomConfig) throws RemoteException {
        ((zzy) getService()).zza(new zzcc(listenerHolder, listenerHolder2, listenerHolder3), this.zzfv, roomConfig.getVariant(), roomConfig.getInvitedPlayerIds(), roomConfig.getAutoMatchCriteria(), false, this.zzfw);
    }

    public final void zza(ListenerHolder<? extends RoomUpdateListener> listenerHolder, String str) {
        try {
            ((zzy) getService()).zza(new zzcc(listenerHolder), str);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zza(Snapshot snapshot) throws RemoteException {
        SnapshotContents snapshotContents = snapshot.getSnapshotContents();
        Preconditions.checkState(!snapshotContents.isClosed(), "Snapshot already closed");
        Contents zzcl2 = snapshotContents.zzcl();
        snapshotContents.close();
        ((zzy) getService()).zza(zzcl2);
    }

    public final void zza(String str, int i) {
        this.zzfp.zza(str, i);
    }

    public final void zza(String str, BaseImplementation.ResultHolder<Games.GetServerAuthCodeResult> resultHolder) throws RemoteException {
        Preconditions.checkNotEmpty(str, "Please provide a valid serverClientId");
        try {
            ((zzy) getService()).zza(str, new zzy(resultHolder));
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzaa() throws RemoteException {
        ((zzy) getService()).zzb(this.zzfw);
    }

    public final void zzab() {
        try {
            zzaa();
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzac() throws RemoteException {
        ((zzy) getService()).zzc(this.zzfw);
    }

    public final void zzad() {
        try {
            zzac();
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzae() {
        try {
            ((zzy) getService()).zze(this.zzfw);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzaf() {
        try {
            ((zzy) getService()).zzd(this.zzfw);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final Intent zzag() throws RemoteException {
        return ((zzy) getService()).zzag();
    }

    public final Intent zzah() {
        try {
            return zzag();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzai() throws RemoteException {
        return ((zzy) getService()).zzai();
    }

    public final Intent zzaj() {
        try {
            return zzai();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final int zzak() throws RemoteException {
        return ((zzy) getService()).zzak();
    }

    public final int zzal() {
        try {
            return zzak();
        } catch (RemoteException e) {
            zza(e);
            return 4368;
        }
    }

    public final String zzam() throws RemoteException {
        return ((zzy) getService()).zzam();
    }

    public final String zzan() {
        try {
            return zzam();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final int zzao() throws RemoteException {
        return ((zzy) getService()).zzao();
    }

    public final int zzap() {
        try {
            return zzao();
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final Intent zzaq() {
        try {
            return ((zzy) getService()).zzaq();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final int zzar() {
        try {
            return ((zzy) getService()).zzar();
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final int zzas() {
        try {
            return ((zzy) getService()).zzas();
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final int zzat() throws RemoteException {
        return ((zzy) getService()).zzat();
    }

    public final int zzau() {
        try {
            return zzat();
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final int zzav() throws RemoteException {
        return ((zzy) getService()).zzav();
    }

    public final int zzaw() {
        try {
            return zzav();
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final Intent zzax() throws RemoteException {
        return ((zzy) getService()).zzbi();
    }

    public final Intent zzay() {
        try {
            return zzax();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final boolean zzaz() throws RemoteException {
        return ((zzy) getService()).zzaz();
    }

    public final int zzb(ListenerHolder<RealTimeMultiplayer.ReliableMessageSentCallback> listenerHolder, byte[] bArr, String str, String str2) {
        try {
            return zza(listenerHolder, bArr, str, str2);
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final int zzb(byte[] bArr, String str) {
        try {
            return zza(bArr, str);
        } catch (RemoteException e) {
            zza(e);
            return -1;
        }
    }

    public final Intent zzb(int i, int i2, boolean z) {
        try {
            return zza(i, i2, z);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzb(PlayerEntity playerEntity) {
        try {
            return zza(playerEntity);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzb(Room room, int i) {
        try {
            return zza(room, i);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzb(String str, boolean z, boolean z2, int i) {
        try {
            return zza(str, z, z2, i);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final String zzb(boolean z) {
        try {
            return zza(true);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Status> resultHolder) throws RemoteException {
        this.zzfp.flush();
        try {
            ((zzy) getService()).zza(new zzcg(resultHolder));
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, int):void
     arg types: [com.google.android.gms.games.internal.zze$zzh, int]
     candidates:
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, boolean):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, java.lang.String[]):void
      com.google.android.gms.games.internal.zzy.zzb(java.lang.String, int):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, int):void */
    public final void zzb(BaseImplementation.ResultHolder<Videos.CaptureAvailableResult> resultHolder, int i) throws RemoteException {
        try {
            ((zzy) getService()).zzb((zzu) new zzh(resultHolder), i);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zzb(resultHolder == null ? null : new C0001zze(resultHolder), str, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Achievements.UpdateAchievementResult> resultHolder, String str, int i) throws RemoteException {
        try {
            ((zzy) getService()).zzb(resultHolder == null ? null : new C0001zze(resultHolder), str, i, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Leaderboards.LoadScoresResult> resultHolder, String str, int i, int i2, int i3, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zzb(new zzah(resultHolder), str, i, i2, i3, z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Quests.ClaimMilestoneResult> resultHolder, String str, String str2) throws RemoteException {
        this.zzfp.flush();
        try {
            ((zzy) getService()).zzb(new zzbr(resultHolder, str2), str, str2);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> resultHolder, String str, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zza(new zzai(resultHolder), str, z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, boolean):void
     arg types: [com.google.android.gms.games.internal.zze$zzai, boolean]
     candidates:
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, int):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, java.lang.String[]):void
      com.google.android.gms.games.internal.zzy.zzb(java.lang.String, int):void
      com.google.android.gms.games.internal.zzy.zzb(com.google.android.gms.games.internal.zzu, boolean):void */
    public final void zzb(BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult> resultHolder, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zzb((zzu) new zzai(resultHolder), z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Quests.LoadQuestsResult> resultHolder, boolean z, String[] strArr) throws RemoteException {
        this.zzfp.flush();
        try {
            ((zzy) getService()).zza(new zzbt(resultHolder), strArr, z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(BaseImplementation.ResultHolder<Requests.UpdateRequestsResult> resultHolder, String[] strArr) throws RemoteException {
        try {
            ((zzy) getService()).zzb(new zzca(resultHolder), strArr);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzb(ListenerHolder<OnInvitationReceivedListener> listenerHolder) {
        try {
            zza(listenerHolder);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzb(ListenerHolder<? extends RoomUpdateListener> listenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder2, ListenerHolder<? extends RealTimeMessageReceivedListener> listenerHolder3, RoomConfig roomConfig) {
        try {
            zza(listenerHolder, listenerHolder2, listenerHolder3, roomConfig);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzb(Snapshot snapshot) {
        try {
            zza(snapshot);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzb(String str) throws RemoteException {
        ((zzy) getService()).zzf(str);
    }

    public final void zzb(String str, int i) throws RemoteException {
        ((zzy) getService()).zzb(str, i);
    }

    public final boolean zzba() {
        try {
            return zzaz();
        } catch (RemoteException e) {
            zza(e);
            return false;
        }
    }

    public final void zzbb() throws RemoteException {
        ((zzy) getService()).zzf(this.zzfw);
    }

    public final void zzbc() {
        try {
            zzbb();
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzbd() {
        if (isConnected()) {
            try {
                ((zzy) getService()).zzbd();
            } catch (RemoteException e) {
                zza(e);
            }
        }
    }

    public final Intent zzc(int i, int i2, boolean z) throws RemoteException {
        return ((zzy) getService()).zzc(i, i2, z);
    }

    public final void zzc(BaseImplementation.ResultHolder<Videos.CaptureCapabilitiesResult> resultHolder) throws RemoteException {
        try {
            ((zzy) getService()).zzc(new zzj(resultHolder));
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzc(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zzb(new zzco(resultHolder), str);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, boolean):void
     arg types: [com.google.android.gms.games.internal.zze$zzf, boolean]
     candidates:
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.multiplayer.realtime.RoomEntity, int):android.content.Intent
      com.google.android.gms.games.internal.zzy.zza(android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String[]):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzw, long):void
      com.google.android.gms.games.internal.zzy.zza(java.lang.String, int):void
      com.google.android.gms.games.internal.zzy.zza(java.lang.String, com.google.android.gms.games.internal.zzu):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, boolean):void */
    public final void zzc(BaseImplementation.ResultHolder<Achievements.LoadAchievementsResult> resultHolder, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zza((zzu) new zzf(resultHolder), z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzc(ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> listenerHolder) throws RemoteException {
        ((zzy) getService()).zzb(new zzaz(listenerHolder), this.zzfw);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, android.os.IBinder, java.lang.String, boolean, long):void
     arg types: [com.google.android.gms.games.internal.zze$zzcc, android.os.Binder, java.lang.String, int, long]
     candidates:
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, int, int, java.lang.String[], android.os.Bundle):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, int, android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, int, boolean, boolean):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, java.lang.String, int, int):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, java.lang.String, com.google.android.gms.games.snapshot.zze, com.google.android.gms.drive.Contents):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, java.lang.String, byte[], java.lang.String, com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.zzy.zza(com.google.android.gms.games.internal.zzu, android.os.IBinder, java.lang.String, boolean, long):void */
    public final void zzc(ListenerHolder<? extends RoomUpdateListener> listenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder2, ListenerHolder<? extends RealTimeMessageReceivedListener> listenerHolder3, RoomConfig roomConfig) throws RemoteException {
        ((zzy) getService()).zza((zzu) new zzcc(listenerHolder, listenerHolder2, listenerHolder3), (IBinder) this.zzfv, roomConfig.getInvitationId(), false, this.zzfw);
    }

    public final void zzc(String str) {
        try {
            zzb(str);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzc(String str, int i) {
        try {
            zzb(str, i);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final Intent zzd(int i, int i2, boolean z) {
        try {
            return zzc(i, i2, z);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzd(String str) {
        try {
            return ((zzy) getService()).zzd(str);
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final void zzd(BaseImplementation.ResultHolder<Videos.CaptureStateResult> resultHolder) throws RemoteException {
        try {
            ((zzy) getService()).zzd(new zzn(resultHolder));
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzd(BaseImplementation.ResultHolder<TurnBasedMultiplayer.InitiateMatchResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zzc(new zzco(resultHolder), str);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zze(com.google.android.gms.games.internal.zzu, boolean):void
     arg types: [com.google.android.gms.games.internal.zze$zzu, boolean]
     candidates:
      com.google.android.gms.games.internal.zzy.zze(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzy.zze(com.google.android.gms.games.internal.zzu, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zze(com.google.android.gms.games.internal.zzu, boolean):void */
    public final void zzd(BaseImplementation.ResultHolder<Events.LoadEventsResult> resultHolder, boolean z) throws RemoteException {
        this.zzfp.flush();
        try {
            ((zzy) getService()).zze((zzu) new zzu(resultHolder), z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzd(ListenerHolder<OnTurnBasedMatchUpdateReceivedListener> listenerHolder) {
        try {
            zzc(listenerHolder);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzd(ListenerHolder<? extends RoomUpdateListener> listenerHolder, ListenerHolder<? extends RoomStatusUpdateListener> listenerHolder2, ListenerHolder<? extends RealTimeMessageReceivedListener> listenerHolder3, RoomConfig roomConfig) {
        try {
            zzc(listenerHolder, listenerHolder2, listenerHolder3, roomConfig);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzd(String str, int i) throws RemoteException {
        ((zzy) getService()).zzd(str, i);
    }

    public final void zze(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LeaveMatchResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zze(new zzcp(resultHolder), str);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zze(BaseImplementation.ResultHolder<Stats.LoadPlayerStatsResult> resultHolder, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zzf(new zzbm(resultHolder), z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zze(ListenerHolder<QuestUpdateListener> listenerHolder) {
        try {
            ((zzy) getService()).zzd(new zzbs(listenerHolder), this.zzfw);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zze(String str) {
        try {
            ((zzy) getService()).zza(str, this.zzft.zzjd.zzjb, this.zzft.zzjd.zzbk());
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zze(String str, int i) {
        try {
            zzd(str, i);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzf(BaseImplementation.ResultHolder<TurnBasedMultiplayer.CancelMatchResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zzd(new zzcn(resultHolder), str);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzy.zzd(com.google.android.gms.games.internal.zzu, boolean):void
     arg types: [com.google.android.gms.games.internal.zze$zzck, boolean]
     candidates:
      com.google.android.gms.games.internal.zzy.zzd(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzy.zzd(com.google.android.gms.games.internal.zzu, java.lang.String):void
      com.google.android.gms.games.internal.zzy.zzd(java.lang.String, int):void
      com.google.android.gms.games.internal.zzy.zzd(com.google.android.gms.games.internal.zzu, boolean):void */
    public final void zzf(BaseImplementation.ResultHolder<Snapshots.LoadSnapshotsResult> resultHolder, boolean z) throws RemoteException {
        try {
            ((zzy) getService()).zzd((zzu) new zzck(resultHolder), z);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzf(ListenerHolder<OnRequestReceivedListener> listenerHolder) {
        try {
            ((zzy) getService()).zzc(new zzbw(listenerHolder), this.zzfw);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzg(BaseImplementation.ResultHolder<TurnBasedMultiplayer.LoadMatchResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zzf(new zzcq(resultHolder), str);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzg(ListenerHolder<Videos.CaptureOverlayStateListener> listenerHolder) throws RemoteException {
        ((zzy) getService()).zze(new zzl(listenerHolder), this.zzfw);
    }

    public final void zzh(BaseImplementation.ResultHolder<Quests.AcceptQuestResult> resultHolder, String str) throws RemoteException {
        this.zzfp.flush();
        try {
            ((zzy) getService()).zzh(new zzbp(resultHolder), str);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzh(ListenerHolder<Videos.CaptureOverlayStateListener> listenerHolder) {
        try {
            zzg(listenerHolder);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    public final void zzi(BaseImplementation.ResultHolder<Snapshots.DeleteSnapshotResult> resultHolder, String str) throws RemoteException {
        try {
            ((zzy) getService()).zzg(new zzci(resultHolder), str);
        } catch (SecurityException e) {
            zza(resultHolder, e);
        }
    }

    public final void zzj(int i) {
        this.zzft.zzjd.gravity = i;
    }

    public final void zzk(int i) throws RemoteException {
        ((zzy) getService()).zzk(i);
    }

    public final void zzl(int i) {
        try {
            zzk(i);
        } catch (RemoteException e) {
            zza(e);
        }
    }

    @Nullable
    public final Bundle zzo() {
        Bundle connectionHint = getConnectionHint();
        if (connectionHint == null) {
            connectionHint = this.zzfz;
        }
        this.zzfz = null;
        return connectionHint;
    }

    public final String zzp() throws RemoteException {
        return ((zzy) getService()).zzp();
    }

    public final String zzq() {
        try {
            return zzp();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public final Player zzr() throws RemoteException {
        checkConnected();
        synchronized (this) {
            if (this.zzfr == null) {
                PlayerBuffer playerBuffer = new PlayerBuffer(((zzy) getService()).zzbg());
                try {
                    if (playerBuffer.getCount() > 0) {
                        this.zzfr = (PlayerEntity) ((Player) playerBuffer.get(0)).freeze();
                    }
                    playerBuffer.release();
                } catch (Throwable th) {
                    playerBuffer.release();
                    throw th;
                }
            }
        }
        return this.zzfr;
    }

    public final Player zzs() {
        try {
            return zzr();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public final Game zzt() throws RemoteException {
        checkConnected();
        synchronized (this) {
            if (this.zzfs == null) {
                GameBuffer gameBuffer = new GameBuffer(((zzy) getService()).zzbh());
                try {
                    if (gameBuffer.getCount() > 0) {
                        this.zzfs = (GameEntity) ((Game) gameBuffer.get(0)).freeze();
                    }
                    gameBuffer.release();
                } catch (Throwable th) {
                    gameBuffer.release();
                    throw th;
                }
            }
        }
        return this.zzfs;
    }

    public final Game zzu() {
        try {
            return zzt();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzv() throws RemoteException {
        return ((zzy) getService()).zzv();
    }

    public final Intent zzw() {
        try {
            return zzv();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzx() {
        try {
            return ((zzy) getService()).zzx();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzy() {
        try {
            return ((zzy) getService()).zzy();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }

    public final Intent zzz() {
        try {
            return ((zzy) getService()).zzz();
        } catch (RemoteException e) {
            zza(e);
            return null;
        }
    }
}
