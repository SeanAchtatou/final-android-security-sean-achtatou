package com.google.android.gms.internal.ads;

import android.util.SparseArray;
import com.google.android.gms.internal.ads.zzne;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

public final class zzod implements zznn {
    private static final zznq zzazt = new zzoe();
    private static final byte[] zzazu = {49, 10, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 10};
    private static final byte[] zzazv = {32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32};
    /* access modifiers changed from: private */
    public static final UUID zzazw = new UUID(72057594037932032L, -9223371306706625679L);
    private long zzack;
    private int zzamr;
    private int zzams;
    private long zzans;
    private long zzant;
    private long zzanu;
    private int zzanz;
    private long zzaoa;
    private boolean zzaob;
    private long zzaoc;
    private long zzaod;
    private long zzaof;
    private boolean zzaoi;
    private boolean zzaoo;
    private boolean zzaop;
    private final zzoi zzazr;
    private final zzob zzazx;
    private final SparseArray<zzog> zzazy;
    private final boolean zzazz;
    private final zzst zzbaa;
    private final zzst zzbab;
    private final zzst zzbac;
    private final zzst zzbad;
    private final zzst zzbae;
    private final zzst zzbaf;
    private final zzst zzbag;
    private final zzst zzbah;
    private final zzst zzbai;
    private ByteBuffer zzbaj;
    private long zzbak;
    private zzog zzbal;
    private boolean zzbam;
    private zzsn zzban;
    private zzsn zzbao;
    private int zzbap;
    private long zzbaq;
    private long zzbar;
    private int zzbas;
    private int zzbat;
    private int[] zzbau;
    private int zzbav;
    private int zzbaw;
    private int zzbax;
    private int zzbay;
    private boolean zzbaz;
    private boolean zzbba;
    private boolean zzbbb;
    private boolean zzbbc;
    private byte zzbbd;
    private int zzbbe;
    private zznp zzbbf;

    public zzod() {
        this(0);
    }

    static boolean zzav(int i) {
        return i == 357149030 || i == 524531317 || i == 475249515 || i == 374648427;
    }

    static int zzx(int i) {
        switch (i) {
            case 131:
            case 136:
            case 155:
            case 159:
            case 176:
            case 179:
            case 186:
            case 215:
            case 231:
            case 241:
            case 251:
            case 16980:
            case 17029:
            case 17143:
            case 18401:
            case 18408:
            case 20529:
            case 20530:
            case 21420:
            case 21432:
            case 21680:
            case 21682:
            case 21690:
            case 21930:
            case 21945:
            case 21946:
            case 21947:
            case 21948:
            case 21949:
            case 22186:
            case 22203:
            case 25188:
            case 2352003:
            case 2807729:
                return 2;
            case 134:
            case 17026:
            case 2274716:
                return 3;
            case 160:
            case 174:
            case 183:
            case 187:
            case 224:
            case 225:
            case 18407:
            case 19899:
            case 20532:
            case 20533:
            case 21936:
            case 21968:
            case 25152:
            case 28032:
            case 30320:
            case 290298740:
            case 357149030:
            case 374648427:
            case 408125543:
            case 440786851:
            case 475249515:
            case 524531317:
                return 1;
            case 161:
            case 163:
            case 16981:
            case 18402:
            case 21419:
            case 25506:
            case 30322:
                return 4;
            case 181:
            case 17545:
            case 21969:
            case 21970:
            case 21971:
            case 21972:
            case 21973:
            case 21974:
            case 21975:
            case 21976:
            case 21977:
            case 21978:
                return 5;
            default:
                return 0;
        }
    }

    public final void release() {
    }

    private zzod(int i) {
        this(new zzny(), 0);
    }

    private zzod(zzob zzob, int i) {
        this.zzans = -1;
        this.zzanu = -9223372036854775807L;
        this.zzbak = -9223372036854775807L;
        this.zzack = -9223372036854775807L;
        this.zzaoc = -1;
        this.zzaod = -1;
        this.zzaof = -9223372036854775807L;
        this.zzazx = zzob;
        this.zzazx.zza(new zzof(this, null));
        this.zzazz = true;
        this.zzazr = new zzoi();
        this.zzazy = new SparseArray<>();
        this.zzbac = new zzst(4);
        this.zzbad = new zzst(ByteBuffer.allocate(4).putInt(-1).array());
        this.zzbae = new zzst(4);
        this.zzbaa = new zzst(zzsq.zzaqt);
        this.zzbab = new zzst(4);
        this.zzbaf = new zzst();
        this.zzbag = new zzst();
        this.zzbah = new zzst(8);
        this.zzbai = new zzst();
    }

    public final boolean zza(zzno zzno) throws IOException, InterruptedException {
        return new zzoh().zza(zzno);
    }

    public final void zza(zznp zznp) {
        this.zzbbf = zznp;
    }

    public final void zzd(long j, long j2) {
        this.zzaof = -9223372036854775807L;
        this.zzbap = 0;
        this.zzazx.reset();
        this.zzazr.reset();
        zzij();
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0039 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0005 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(com.google.android.gms.internal.ads.zzno r9, com.google.android.gms.internal.ads.zznt r10) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r8 = this;
            r0 = 0
            r8.zzaoo = r0
            r1 = 1
            r2 = 1
        L_0x0005:
            if (r2 == 0) goto L_0x003a
            boolean r3 = r8.zzaoo
            if (r3 != 0) goto L_0x003a
            com.google.android.gms.internal.ads.zzob r2 = r8.zzazx
            boolean r2 = r2.zzb(r9)
            if (r2 == 0) goto L_0x0005
            long r3 = r9.getPosition()
            boolean r5 = r8.zzaob
            if (r5 == 0) goto L_0x0025
            r8.zzaod = r3
            long r3 = r8.zzaoc
            r10.zzahv = r3
            r8.zzaob = r0
        L_0x0023:
            r3 = 1
            goto L_0x0037
        L_0x0025:
            boolean r3 = r8.zzbam
            if (r3 == 0) goto L_0x0036
            long r3 = r8.zzaod
            r5 = -1
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0036
            r10.zzahv = r3
            r8.zzaod = r5
            goto L_0x0023
        L_0x0036:
            r3 = 0
        L_0x0037:
            if (r3 == 0) goto L_0x0005
            return r1
        L_0x003a:
            if (r2 == 0) goto L_0x003d
            return r0
        L_0x003d:
            r9 = -1
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzod.zza(com.google.android.gms.internal.ads.zzno, com.google.android.gms.internal.ads.zznt):int");
    }

    /* access modifiers changed from: package-private */
    public final void zzb(int i, long j, long j2) throws zzlm {
        if (i == 160) {
            this.zzaop = false;
        } else if (i == 174) {
            this.zzbal = new zzog(null);
        } else if (i == 187) {
            this.zzaoi = false;
        } else if (i == 19899) {
            this.zzanz = -1;
            this.zzaoa = -1;
        } else if (i == 20533) {
            this.zzbal.zzaos = true;
        } else if (i == 21968) {
            this.zzbal.zzbbn = true;
        } else if (i == 25152) {
        } else {
            if (i == 408125543) {
                long j3 = this.zzans;
                if (j3 == -1 || j3 == j) {
                    this.zzans = j;
                    this.zzant = j2;
                    return;
                }
                throw new zzlm("Multiple Segment elements not supported");
            } else if (i == 475249515) {
                this.zzban = new zzsn();
                this.zzbao = new zzsn();
            } else if (i != 524531317 || this.zzbam) {
            } else {
                if (!this.zzazz || this.zzaoc == -1) {
                    this.zzbbf.zza(new zznv(this.zzack));
                    this.zzbam = true;
                    return;
                }
                this.zzaob = true;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzy(int i) throws zzlm {
        zznu zznu;
        zzsn zzsn;
        zzsn zzsn2;
        int i2;
        int i3 = 0;
        if (i != 160) {
            if (i == 174) {
                String str = this.zzbal.zzaor;
                if ("V_VP8".equals(str) || "V_VP9".equals(str) || "V_MPEG2".equals(str) || "V_MPEG4/ISO/SP".equals(str) || "V_MPEG4/ISO/ASP".equals(str) || "V_MPEG4/ISO/AP".equals(str) || "V_MPEG4/ISO/AVC".equals(str) || "V_MPEGH/ISO/HEVC".equals(str) || "V_MS/VFW/FOURCC".equals(str) || "V_THEORA".equals(str) || "A_OPUS".equals(str) || "A_VORBIS".equals(str) || "A_AAC".equals(str) || "A_MPEG/L2".equals(str) || "A_MPEG/L3".equals(str) || "A_AC3".equals(str) || "A_EAC3".equals(str) || "A_TRUEHD".equals(str) || "A_DTS".equals(str) || "A_DTS/EXPRESS".equals(str) || "A_DTS/LOSSLESS".equals(str) || "A_FLAC".equals(str) || "A_MS/ACM".equals(str) || "A_PCM/INT/LIT".equals(str) || "S_TEXT/UTF8".equals(str) || "S_VOBSUB".equals(str) || "S_HDMV/PGS".equals(str) || "S_DVBSUB".equals(str)) {
                    i3 = 1;
                }
                if (i3 != 0) {
                    zzog zzog = this.zzbal;
                    zzog.zza(this.zzbbf, zzog.number);
                    this.zzazy.put(this.zzbal.number, this.zzbal);
                }
                this.zzbal = null;
            } else if (i == 19899) {
                int i4 = this.zzanz;
                if (i4 != -1) {
                    long j = this.zzaoa;
                    if (j != -1) {
                        if (i4 == 475249515) {
                            this.zzaoc = j;
                            return;
                        }
                        return;
                    }
                }
                throw new zzlm("Mandatory element SeekID or SeekPosition not found");
            } else if (i != 25152) {
                if (i != 28032) {
                    if (i == 357149030) {
                        if (this.zzanu == -9223372036854775807L) {
                            this.zzanu = 1000000;
                        }
                        long j2 = this.zzbak;
                        if (j2 != -9223372036854775807L) {
                            this.zzack = zzdw(j2);
                        }
                    } else if (i != 374648427) {
                        if (i == 475249515 && !this.zzbam) {
                            zznp zznp = this.zzbbf;
                            if (this.zzans == -1 || this.zzack == -9223372036854775807L || (zzsn = this.zzban) == null || zzsn.size() == 0 || (zzsn2 = this.zzbao) == null || zzsn2.size() != this.zzban.size()) {
                                this.zzban = null;
                                this.zzbao = null;
                                zznu = new zznv(this.zzack);
                            } else {
                                int size = this.zzban.size();
                                int[] iArr = new int[size];
                                long[] jArr = new long[size];
                                long[] jArr2 = new long[size];
                                long[] jArr3 = new long[size];
                                for (int i5 = 0; i5 < size; i5++) {
                                    jArr3[i5] = this.zzban.get(i5);
                                    jArr[i5] = this.zzans + this.zzbao.get(i5);
                                }
                                while (true) {
                                    i2 = size - 1;
                                    if (i3 >= i2) {
                                        break;
                                    }
                                    int i6 = i3 + 1;
                                    iArr[i3] = (int) (jArr[i6] - jArr[i3]);
                                    jArr2[i3] = jArr3[i6] - jArr3[i3];
                                    i3 = i6;
                                }
                                iArr[i2] = (int) ((this.zzans + this.zzant) - jArr[i2]);
                                jArr2[i2] = this.zzack - jArr3[i2];
                                this.zzban = null;
                                this.zzbao = null;
                                zznu = new zznl(iArr, jArr, jArr2, jArr3);
                            }
                            zznp.zza(zznu);
                            this.zzbam = true;
                        }
                    } else if (this.zzazy.size() != 0) {
                        this.zzbbf.zzfi();
                    } else {
                        throw new zzlm("No valid tracks were found");
                    }
                } else if (this.zzbal.zzaos && this.zzbal.zzbbi != null) {
                    throw new zzlm("Combining encryption and compression is not supported");
                }
            } else if (!this.zzbal.zzaos) {
            } else {
                if (this.zzbal.zzbbj != null) {
                    this.zzbal.zzatr = new zzne(new zzne.zza(zzkt.zzarg, "video/webm", this.zzbal.zzbbj.zzazq));
                    return;
                }
                throw new zzlm("Encrypted Track found but ContentEncKeyID was not found");
            }
        } else if (this.zzbap == 2) {
            if (!this.zzaop) {
                this.zzbax |= 1;
            }
            zza(this.zzazy.get(this.zzbav), this.zzbaq);
            this.zzbap = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzc(int i, long j) throws zzlm {
        if (i != 20529) {
            if (i != 20530) {
                boolean z = false;
                switch (i) {
                    case 131:
                        this.zzbal.type = (int) j;
                        return;
                    case 136:
                        zzog zzog = this.zzbal;
                        if (j == 1) {
                            z = true;
                        }
                        zzog.zzbce = z;
                        return;
                    case 155:
                        this.zzbar = zzdw(j);
                        return;
                    case 159:
                        this.zzbal.zzafu = (int) j;
                        return;
                    case 176:
                        this.zzbal.width = (int) j;
                        return;
                    case 179:
                        this.zzban.add(zzdw(j));
                        return;
                    case 186:
                        this.zzbal.height = (int) j;
                        return;
                    case 215:
                        this.zzbal.number = (int) j;
                        return;
                    case 231:
                        this.zzaof = zzdw(j);
                        return;
                    case 241:
                        if (!this.zzaoi) {
                            this.zzbao.add(j);
                            this.zzaoi = true;
                            return;
                        }
                        return;
                    case 251:
                        this.zzaop = true;
                        return;
                    case 16980:
                        if (j != 3) {
                            StringBuilder sb = new StringBuilder(50);
                            sb.append("ContentCompAlgo ");
                            sb.append(j);
                            sb.append(" not supported");
                            throw new zzlm(sb.toString());
                        }
                        return;
                    case 17029:
                        if (j < 1 || j > 2) {
                            StringBuilder sb2 = new StringBuilder(53);
                            sb2.append("DocTypeReadVersion ");
                            sb2.append(j);
                            sb2.append(" not supported");
                            throw new zzlm(sb2.toString());
                        }
                        return;
                    case 17143:
                        if (j != 1) {
                            StringBuilder sb3 = new StringBuilder(50);
                            sb3.append("EBMLReadVersion ");
                            sb3.append(j);
                            sb3.append(" not supported");
                            throw new zzlm(sb3.toString());
                        }
                        return;
                    case 18401:
                        if (j != 5) {
                            StringBuilder sb4 = new StringBuilder(49);
                            sb4.append("ContentEncAlgo ");
                            sb4.append(j);
                            sb4.append(" not supported");
                            throw new zzlm(sb4.toString());
                        }
                        return;
                    case 18408:
                        if (j != 1) {
                            StringBuilder sb5 = new StringBuilder(56);
                            sb5.append("AESSettingsCipherMode ");
                            sb5.append(j);
                            sb5.append(" not supported");
                            throw new zzlm(sb5.toString());
                        }
                        return;
                    case 21420:
                        this.zzaoa = j + this.zzans;
                        return;
                    case 21432:
                        int i2 = (int) j;
                        if (i2 == 0) {
                            this.zzbal.zzatu = 0;
                            return;
                        } else if (i2 == 1) {
                            this.zzbal.zzatu = 2;
                            return;
                        } else if (i2 == 3) {
                            this.zzbal.zzatu = 1;
                            return;
                        } else if (i2 == 15) {
                            this.zzbal.zzatu = 3;
                            return;
                        } else {
                            return;
                        }
                    case 21680:
                        this.zzbal.zzbbk = (int) j;
                        return;
                    case 21682:
                        this.zzbal.zzbbm = (int) j;
                        return;
                    case 21690:
                        this.zzbal.zzbbl = (int) j;
                        return;
                    case 21930:
                        zzog zzog2 = this.zzbal;
                        if (j == 1) {
                            z = true;
                        }
                        zzog2.zzbcf = z;
                        return;
                    case 22186:
                        this.zzbal.zzaow = j;
                        return;
                    case 22203:
                        this.zzbal.zzaox = j;
                        return;
                    case 25188:
                        this.zzbal.zzbcd = (int) j;
                        return;
                    case 2352003:
                        this.zzbal.zzbbh = (int) j;
                        return;
                    case 2807729:
                        this.zzanu = j;
                        return;
                    default:
                        switch (i) {
                            case 21945:
                                int i3 = (int) j;
                                if (i3 == 1) {
                                    this.zzbal.zzbbq = 2;
                                    return;
                                } else if (i3 == 2) {
                                    this.zzbal.zzbbq = 1;
                                    return;
                                } else {
                                    return;
                                }
                            case 21946:
                                int i4 = (int) j;
                                if (i4 != 1) {
                                    if (i4 == 16) {
                                        this.zzbal.zzbbp = 6;
                                        return;
                                    } else if (i4 == 18) {
                                        this.zzbal.zzbbp = 7;
                                        return;
                                    } else if (!(i4 == 6 || i4 == 7)) {
                                        return;
                                    }
                                }
                                this.zzbal.zzbbp = 3;
                                return;
                            case 21947:
                                zzog zzog3 = this.zzbal;
                                zzog3.zzbbn = true;
                                int i5 = (int) j;
                                if (i5 == 1) {
                                    zzog3.zzbbo = 1;
                                    return;
                                } else if (i5 == 9) {
                                    zzog3.zzbbo = 6;
                                    return;
                                } else if (i5 == 4 || i5 == 5 || i5 == 6 || i5 == 7) {
                                    this.zzbal.zzbbo = 2;
                                    return;
                                } else {
                                    return;
                                }
                            case 21948:
                                this.zzbal.zzbbr = (int) j;
                                return;
                            case 21949:
                                this.zzbal.zzbbs = (int) j;
                                return;
                            default:
                                return;
                        }
                }
            } else if (j != 1) {
                StringBuilder sb6 = new StringBuilder(55);
                sb6.append("ContentEncodingScope ");
                sb6.append(j);
                sb6.append(" not supported");
                throw new zzlm(sb6.toString());
            }
        } else if (j != 0) {
            StringBuilder sb7 = new StringBuilder(55);
            sb7.append("ContentEncodingOrder ");
            sb7.append(j);
            sb7.append(" not supported");
            throw new zzlm(sb7.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(int i, double d) {
        if (i == 181) {
            this.zzbal.zzafv = (int) d;
        } else if (i != 17545) {
            switch (i) {
                case 21969:
                    this.zzbal.zzbbt = (float) d;
                    return;
                case 21970:
                    this.zzbal.zzbbu = (float) d;
                    return;
                case 21971:
                    this.zzbal.zzbbv = (float) d;
                    return;
                case 21972:
                    this.zzbal.zzbbw = (float) d;
                    return;
                case 21973:
                    this.zzbal.zzbbx = (float) d;
                    return;
                case 21974:
                    this.zzbal.zzbby = (float) d;
                    return;
                case 21975:
                    this.zzbal.zzbbz = (float) d;
                    return;
                case 21976:
                    this.zzbal.zzbca = (float) d;
                    return;
                case 21977:
                    this.zzbal.zzbcb = (float) d;
                    return;
                case 21978:
                    this.zzbal.zzbcc = (float) d;
                    return;
                default:
                    return;
            }
        } else {
            this.zzbak = (long) d;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(int i, String str) throws zzlm {
        if (i == 134) {
            this.zzbal.zzaor = str;
        } else if (i != 17026) {
            if (i == 2274716) {
                String unused = this.zzbal.zzauc = str;
            }
        } else if (!"webm".equals(str) && !"matroska".equals(str)) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 22);
            sb.append("DocType ");
            sb.append(str);
            sb.append(" not supported");
            throw new zzlm(sb.toString());
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01fe, code lost:
        throw new com.google.android.gms.internal.ads.zzlm("EBML lacing sample size out of range.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(int r20, int r21, com.google.android.gms.internal.ads.zzno r22) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r22
            r4 = 161(0xa1, float:2.26E-43)
            r5 = 163(0xa3, float:2.28E-43)
            r6 = 4
            r7 = 0
            r8 = 1
            if (r1 == r4) goto L_0x0091
            if (r1 == r5) goto L_0x0091
            r4 = 16981(0x4255, float:2.3795E-41)
            if (r1 == r4) goto L_0x0085
            r4 = 18402(0x47e2, float:2.5787E-41)
            if (r1 == r4) goto L_0x0076
            r4 = 21419(0x53ab, float:3.0014E-41)
            if (r1 == r4) goto L_0x0058
            r4 = 25506(0x63a2, float:3.5742E-41)
            if (r1 == r4) goto L_0x004c
            r4 = 30322(0x7672, float:4.249E-41)
            if (r1 != r4) goto L_0x0033
            com.google.android.gms.internal.ads.zzog r1 = r0.zzbal
            byte[] r4 = new byte[r2]
            r1.zzatv = r4
            byte[] r1 = r1.zzatv
            r3.readFully(r1, r7, r2)
            return
        L_0x0033:
            com.google.android.gms.internal.ads.zzlm r2 = new com.google.android.gms.internal.ads.zzlm
            r3 = 26
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r3)
            java.lang.String r3 = "Unexpected id: "
            r4.append(r3)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.<init>(r1)
            throw r2
        L_0x004c:
            com.google.android.gms.internal.ads.zzog r1 = r0.zzbal
            byte[] r4 = new byte[r2]
            r1.zzaov = r4
            byte[] r1 = r1.zzaov
            r3.readFully(r1, r7, r2)
            return
        L_0x0058:
            com.google.android.gms.internal.ads.zzst r1 = r0.zzbae
            byte[] r1 = r1.data
            java.util.Arrays.fill(r1, r7)
            com.google.android.gms.internal.ads.zzst r1 = r0.zzbae
            byte[] r1 = r1.data
            int r6 = r6 - r2
            r3.readFully(r1, r6, r2)
            com.google.android.gms.internal.ads.zzst r1 = r0.zzbae
            r1.setPosition(r7)
            com.google.android.gms.internal.ads.zzst r1 = r0.zzbae
            long r1 = r1.zzge()
            int r2 = (int) r1
            r0.zzanz = r2
            return
        L_0x0076:
            byte[] r1 = new byte[r2]
            r3.readFully(r1, r7, r2)
            com.google.android.gms.internal.ads.zzog r2 = r0.zzbal
            com.google.android.gms.internal.ads.zznx r3 = new com.google.android.gms.internal.ads.zznx
            r3.<init>(r8, r1)
            r2.zzbbj = r3
            return
        L_0x0085:
            com.google.android.gms.internal.ads.zzog r1 = r0.zzbal
            byte[] r4 = new byte[r2]
            r1.zzbbi = r4
            byte[] r1 = r1.zzbbi
            r3.readFully(r1, r7, r2)
            return
        L_0x0091:
            int r4 = r0.zzbap
            r9 = 8
            if (r4 != 0) goto L_0x00b6
            com.google.android.gms.internal.ads.zzoi r4 = r0.zzazr
            long r10 = r4.zza(r3, r7, r8, r9)
            int r4 = (int) r10
            r0.zzbav = r4
            com.google.android.gms.internal.ads.zzoi r4 = r0.zzazr
            int r4 = r4.zzfw()
            r0.zzbaw = r4
            r10 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r0.zzbar = r10
            r0.zzbap = r8
            com.google.android.gms.internal.ads.zzst r4 = r0.zzbac
            r4.reset()
        L_0x00b6:
            android.util.SparseArray<com.google.android.gms.internal.ads.zzog> r4 = r0.zzazy
            int r10 = r0.zzbav
            java.lang.Object r4 = r4.get(r10)
            com.google.android.gms.internal.ads.zzog r4 = (com.google.android.gms.internal.ads.zzog) r4
            if (r4 != 0) goto L_0x00cc
            int r1 = r0.zzbaw
            int r1 = r2 - r1
            r3.zzr(r1)
            r0.zzbap = r7
            return
        L_0x00cc:
            int r10 = r0.zzbap
            if (r10 != r8) goto L_0x0285
            r10 = 3
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzst r11 = r0.zzbac
            byte[] r11 = r11.data
            r12 = 2
            byte r11 = r11[r12]
            r11 = r11 & 6
            int r11 = r11 >> r8
            r13 = 255(0xff, float:3.57E-43)
            if (r11 != 0) goto L_0x00f6
            r0.zzbat = r8
            int[] r6 = r0.zzbau
            int[] r6 = zza(r6, r8)
            r0.zzbau = r6
            int[] r6 = r0.zzbau
            int r11 = r0.zzbaw
            int r2 = r2 - r11
            int r2 = r2 - r10
            r6[r7] = r2
            goto L_0x0212
        L_0x00f6:
            if (r1 != r5) goto L_0x027d
            r0.zzb(r3, r6)
            com.google.android.gms.internal.ads.zzst r14 = r0.zzbac
            byte[] r14 = r14.data
            byte r14 = r14[r10]
            r14 = r14 & r13
            int r14 = r14 + r8
            r0.zzbat = r14
            int[] r14 = r0.zzbau
            int r15 = r0.zzbat
            int[] r14 = zza(r14, r15)
            r0.zzbau = r14
            if (r11 != r12) goto L_0x011f
            int r10 = r0.zzbaw
            int r2 = r2 - r10
            int r2 = r2 - r6
            int r6 = r0.zzbat
            int r2 = r2 / r6
            int[] r10 = r0.zzbau
            java.util.Arrays.fill(r10, r7, r6, r2)
            goto L_0x0212
        L_0x011f:
            if (r11 != r8) goto L_0x0157
            r6 = 0
            r10 = 4
            r11 = 0
        L_0x0124:
            int r14 = r0.zzbat
            int r15 = r14 + -1
            if (r6 >= r15) goto L_0x014b
            int[] r14 = r0.zzbau
            r14[r6] = r7
        L_0x012e:
            int r10 = r10 + r8
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzst r14 = r0.zzbac
            byte[] r14 = r14.data
            int r15 = r10 + -1
            byte r14 = r14[r15]
            r14 = r14 & r13
            int[] r15 = r0.zzbau
            r16 = r15[r6]
            int r16 = r16 + r14
            r15[r6] = r16
            if (r14 == r13) goto L_0x012e
            r14 = r15[r6]
            int r11 = r11 + r14
            int r6 = r6 + 1
            goto L_0x0124
        L_0x014b:
            int[] r6 = r0.zzbau
            int r14 = r14 - r8
            int r15 = r0.zzbaw
            int r2 = r2 - r15
            int r2 = r2 - r10
            int r2 = r2 - r11
            r6[r14] = r2
            goto L_0x0212
        L_0x0157:
            if (r11 != r10) goto L_0x0264
            r6 = 0
            r10 = 4
            r11 = 0
        L_0x015c:
            int r14 = r0.zzbat
            int r15 = r14 + -1
            if (r6 >= r15) goto L_0x0207
            int[] r14 = r0.zzbau
            r14[r6] = r7
            int r10 = r10 + 1
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzst r14 = r0.zzbac
            byte[] r14 = r14.data
            int r15 = r10 + -1
            byte r14 = r14[r15]
            if (r14 == 0) goto L_0x01ff
            r16 = 0
            r14 = 0
        L_0x0178:
            if (r14 >= r9) goto L_0x01ca
            int r18 = 7 - r14
            int r18 = r8 << r18
            com.google.android.gms.internal.ads.zzst r5 = r0.zzbac
            byte[] r5 = r5.data
            byte r5 = r5[r15]
            r5 = r5 & r18
            if (r5 == 0) goto L_0x01c0
            int r10 = r10 + r14
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzst r5 = r0.zzbac
            byte[] r5 = r5.data
            int r16 = r15 + 1
            byte r5 = r5[r15]
            r5 = r5 & r13
            r15 = r18 ^ -1
            r5 = r5 & r15
            long r7 = (long) r5
            r5 = r16
        L_0x019b:
            r16 = r7
            if (r5 >= r10) goto L_0x01b2
            long r7 = r16 << r9
            com.google.android.gms.internal.ads.zzst r15 = r0.zzbac
            byte[] r15 = r15.data
            int r16 = r5 + 1
            byte r5 = r15[r5]
            r5 = r5 & r13
            long r12 = (long) r5
            long r7 = r7 | r12
            r5 = r16
            r12 = 2
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x019b
        L_0x01b2:
            if (r6 <= 0) goto L_0x01ca
            int r14 = r14 * 7
            int r14 = r14 + 6
            r7 = 1
            long r12 = r7 << r14
            long r12 = r12 - r7
            long r16 = r16 - r12
            goto L_0x01ca
        L_0x01c0:
            int r14 = r14 + 1
            r5 = 163(0xa3, float:2.28E-43)
            r7 = 0
            r8 = 1
            r12 = 2
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x0178
        L_0x01ca:
            r7 = r16
            r12 = -2147483648(0xffffffff80000000, double:NaN)
            int r5 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r5 < 0) goto L_0x01f7
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r5 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r5 > 0) goto L_0x01f7
            int r5 = (int) r7
            int[] r7 = r0.zzbau
            if (r6 != 0) goto L_0x01e0
            goto L_0x01e5
        L_0x01e0:
            int r8 = r6 + -1
            r8 = r7[r8]
            int r5 = r5 + r8
        L_0x01e5:
            r7[r6] = r5
            int[] r5 = r0.zzbau
            r5 = r5[r6]
            int r11 = r11 + r5
            int r6 = r6 + 1
            r5 = 163(0xa3, float:2.28E-43)
            r7 = 0
            r8 = 1
            r12 = 2
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x015c
        L_0x01f7:
            com.google.android.gms.internal.ads.zzlm r1 = new com.google.android.gms.internal.ads.zzlm
            java.lang.String r2 = "EBML lacing sample size out of range."
            r1.<init>(r2)
            throw r1
        L_0x01ff:
            com.google.android.gms.internal.ads.zzlm r1 = new com.google.android.gms.internal.ads.zzlm
            java.lang.String r2 = "No valid varint length mask found"
            r1.<init>(r2)
            throw r1
        L_0x0207:
            int[] r5 = r0.zzbau
            r6 = 1
            int r14 = r14 - r6
            int r6 = r0.zzbaw
            int r2 = r2 - r6
            int r2 = r2 - r10
            int r2 = r2 - r11
            r5[r14] = r2
        L_0x0212:
            com.google.android.gms.internal.ads.zzst r2 = r0.zzbac
            byte[] r2 = r2.data
            r5 = 0
            byte r2 = r2[r5]
            int r2 = r2 << r9
            com.google.android.gms.internal.ads.zzst r5 = r0.zzbac
            byte[] r5 = r5.data
            r6 = 1
            byte r5 = r5[r6]
            r6 = 255(0xff, float:3.57E-43)
            r5 = r5 & r6
            r2 = r2 | r5
            long r5 = r0.zzaof
            long r7 = (long) r2
            long r7 = r0.zzdw(r7)
            long r5 = r5 + r7
            r0.zzbaq = r5
            com.google.android.gms.internal.ads.zzst r2 = r0.zzbac
            byte[] r2 = r2.data
            r5 = 2
            byte r2 = r2[r5]
            r2 = r2 & r9
            if (r2 != r9) goto L_0x023b
            r2 = 1
            goto L_0x023c
        L_0x023b:
            r2 = 0
        L_0x023c:
            int r6 = r4.type
            if (r6 == r5) goto L_0x0252
            r6 = 163(0xa3, float:2.28E-43)
            if (r1 != r6) goto L_0x0250
            com.google.android.gms.internal.ads.zzst r6 = r0.zzbac
            byte[] r6 = r6.data
            byte r6 = r6[r5]
            r5 = 128(0x80, float:1.794E-43)
            r6 = r6 & r5
            if (r6 != r5) goto L_0x0250
            goto L_0x0252
        L_0x0250:
            r5 = 0
            goto L_0x0253
        L_0x0252:
            r5 = 1
        L_0x0253:
            if (r2 == 0) goto L_0x0258
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x0259
        L_0x0258:
            r7 = 0
        L_0x0259:
            r2 = r5 | r7
            r0.zzbax = r2
            r2 = 2
            r0.zzbap = r2
            r2 = 0
            r0.zzbas = r2
            goto L_0x0285
        L_0x0264:
            com.google.android.gms.internal.ads.zzlm r1 = new com.google.android.gms.internal.ads.zzlm
            r2 = 36
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Unexpected lacing value: "
            r3.append(r2)
            r3.append(r11)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            throw r1
        L_0x027d:
            com.google.android.gms.internal.ads.zzlm r1 = new com.google.android.gms.internal.ads.zzlm
            java.lang.String r2 = "Lacing only supported in SimpleBlocks."
            r1.<init>(r2)
            throw r1
        L_0x0285:
            r2 = 163(0xa3, float:2.28E-43)
            if (r1 != r2) goto L_0x02b0
        L_0x0289:
            int r1 = r0.zzbas
            int r2 = r0.zzbat
            if (r1 >= r2) goto L_0x02ac
            int[] r2 = r0.zzbau
            r1 = r2[r1]
            r0.zza(r3, r4, r1)
            long r1 = r0.zzbaq
            int r5 = r0.zzbas
            int r6 = r4.zzbbh
            int r5 = r5 * r6
            int r5 = r5 / 1000
            long r5 = (long) r5
            long r1 = r1 + r5
            r0.zza(r4, r1)
            int r1 = r0.zzbas
            r2 = 1
            int r1 = r1 + r2
            r0.zzbas = r1
            goto L_0x0289
        L_0x02ac:
            r1 = 0
            r0.zzbap = r1
            return
        L_0x02b0:
            r1 = 0
            int[] r2 = r0.zzbau
            r1 = r2[r1]
            r0.zza(r3, r4, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzod.zza(int, int, com.google.android.gms.internal.ads.zzno):void");
    }

    private final void zza(zzog zzog, long j) {
        byte[] bArr;
        if ("S_TEXT/UTF8".equals(zzog.zzaor)) {
            byte[] bArr2 = this.zzbag.data;
            long j2 = this.zzbar;
            if (j2 == -9223372036854775807L) {
                bArr = zzazv;
            } else {
                int i = (int) (j2 / 3600000000L);
                long j3 = j2 - (((long) i) * 3600000000L);
                int i2 = (int) (j3 / 60000000);
                long j4 = j3 - ((long) (60000000 * i2));
                int i3 = (int) (j4 / 1000000);
                bArr = zzsy.zzbh(String.format(Locale.US, "%02d:%02d:%02d,%03d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf((int) ((j4 - ((long) (1000000 * i3))) / 1000))));
            }
            System.arraycopy(bArr, 0, bArr2, 19, 12);
            zznw zznw = zzog.zzbcg;
            zzst zzst = this.zzbag;
            zznw.zza(zzst, zzst.limit());
            this.zzamr += this.zzbag.limit();
        }
        zzog.zzbcg.zza(j, this.zzbax, this.zzamr, 0, zzog.zzbbj);
        this.zzaoo = true;
        zzij();
    }

    private final void zzij() {
        this.zzbay = 0;
        this.zzamr = 0;
        this.zzams = 0;
        this.zzbaz = false;
        this.zzbba = false;
        this.zzbbc = false;
        this.zzbbe = 0;
        this.zzbbd = 0;
        this.zzbbb = false;
        this.zzbaf.reset();
    }

    private final void zzb(zzno zzno, int i) throws IOException, InterruptedException {
        if (this.zzbac.limit() < i) {
            if (this.zzbac.capacity() < i) {
                zzst zzst = this.zzbac;
                zzst.zzb(Arrays.copyOf(zzst.data, Math.max(this.zzbac.data.length << 1, i)), this.zzbac.limit());
            }
            zzno.readFully(this.zzbac.data, this.zzbac.limit(), i - this.zzbac.limit());
            this.zzbac.zzbo(i);
        }
    }

    private final void zza(zzno zzno, zzog zzog, int i) throws IOException, InterruptedException {
        int i2;
        if ("S_TEXT/UTF8".equals(zzog.zzaor)) {
            int length = zzazu.length + i;
            if (this.zzbag.capacity() < length) {
                this.zzbag.data = Arrays.copyOf(zzazu, length + i);
            }
            zzno.readFully(this.zzbag.data, zzazu.length, i);
            this.zzbag.setPosition(0);
            this.zzbag.zzbo(length);
            return;
        }
        zznw zznw = zzog.zzbcg;
        if (!this.zzbaz) {
            if (zzog.zzaos) {
                this.zzbax &= -1073741825;
                int i3 = 128;
                if (!this.zzbba) {
                    zzno.readFully(this.zzbac.data, 0, 1);
                    this.zzbay++;
                    if ((this.zzbac.data[0] & 128) != 128) {
                        this.zzbbd = this.zzbac.data[0];
                        this.zzbba = true;
                    } else {
                        throw new zzlm("Extension bit is set in signal byte");
                    }
                }
                byte b = this.zzbbd;
                if ((b & 1) == 1) {
                    boolean z = (b & 2) == 2;
                    this.zzbax |= 1073741824;
                    if (!this.zzbbb) {
                        zzno.readFully(this.zzbah.data, 0, 8);
                        this.zzbay += 8;
                        this.zzbbb = true;
                        byte[] bArr = this.zzbac.data;
                        if (!z) {
                            i3 = 0;
                        }
                        bArr[0] = (byte) (i3 | 8);
                        this.zzbac.setPosition(0);
                        zznw.zza(this.zzbac, 1);
                        this.zzamr++;
                        this.zzbah.setPosition(0);
                        zznw.zza(this.zzbah, 8);
                        this.zzamr += 8;
                    }
                    if (z) {
                        if (!this.zzbbc) {
                            zzno.readFully(this.zzbac.data, 0, 1);
                            this.zzbay++;
                            this.zzbac.setPosition(0);
                            this.zzbbe = this.zzbac.readUnsignedByte();
                            this.zzbbc = true;
                        }
                        int i4 = this.zzbbe << 2;
                        this.zzbac.reset(i4);
                        zzno.readFully(this.zzbac.data, 0, i4);
                        this.zzbay += i4;
                        short s = (short) ((this.zzbbe / 2) + 1);
                        int i5 = (s * 6) + 2;
                        ByteBuffer byteBuffer = this.zzbaj;
                        if (byteBuffer == null || byteBuffer.capacity() < i5) {
                            this.zzbaj = ByteBuffer.allocate(i5);
                        }
                        this.zzbaj.position(0);
                        this.zzbaj.putShort(s);
                        int i6 = 0;
                        int i7 = 0;
                        while (true) {
                            i2 = this.zzbbe;
                            if (i6 >= i2) {
                                break;
                            }
                            int zzgg = this.zzbac.zzgg();
                            if (i6 % 2 == 0) {
                                this.zzbaj.putShort((short) (zzgg - i7));
                            } else {
                                this.zzbaj.putInt(zzgg - i7);
                            }
                            i6++;
                            i7 = zzgg;
                        }
                        int i8 = (i - this.zzbay) - i7;
                        if (i2 % 2 == 1) {
                            this.zzbaj.putInt(i8);
                        } else {
                            this.zzbaj.putShort((short) i8);
                            this.zzbaj.putInt(0);
                        }
                        this.zzbai.zzb(this.zzbaj.array(), i5);
                        zznw.zza(this.zzbai, i5);
                        this.zzamr += i5;
                    }
                }
            } else if (zzog.zzbbi != null) {
                this.zzbaf.zzb(zzog.zzbbi, zzog.zzbbi.length);
            }
            this.zzbaz = true;
        }
        int limit = i + this.zzbaf.limit();
        if (!"V_MPEG4/ISO/AVC".equals(zzog.zzaor) && !"V_MPEGH/ISO/HEVC".equals(zzog.zzaor)) {
            while (true) {
                int i9 = this.zzbay;
                if (i9 >= limit) {
                    break;
                }
                zza(zzno, zznw, limit - i9);
            }
        } else {
            byte[] bArr2 = this.zzbab.data;
            bArr2[0] = 0;
            bArr2[1] = 0;
            bArr2[2] = 0;
            int i10 = zzog.zzamf;
            int i11 = 4 - zzog.zzamf;
            while (this.zzbay < limit) {
                int i12 = this.zzams;
                if (i12 == 0) {
                    int min = Math.min(i10, this.zzbaf.zzjz());
                    zzno.readFully(bArr2, i11 + min, i10 - min);
                    if (min > 0) {
                        this.zzbaf.zzb(bArr2, i11, min);
                    }
                    this.zzbay += i10;
                    this.zzbab.setPosition(0);
                    this.zzams = this.zzbab.zzgg();
                    this.zzbaa.setPosition(0);
                    zznw.zza(this.zzbaa, 4);
                    this.zzamr += 4;
                } else {
                    this.zzams = i12 - zza(zzno, zznw, i12);
                }
            }
        }
        if ("A_VORBIS".equals(zzog.zzaor)) {
            this.zzbad.setPosition(0);
            zznw.zza(this.zzbad, 4);
            this.zzamr += 4;
        }
    }

    private final int zza(zzno zzno, zznw zznw, int i) throws IOException, InterruptedException {
        int i2;
        int zzjz = this.zzbaf.zzjz();
        if (zzjz > 0) {
            i2 = Math.min(i, zzjz);
            zznw.zza(this.zzbaf, i2);
        } else {
            i2 = zznw.zza(zzno, i, false);
        }
        this.zzbay += i2;
        this.zzamr += i2;
        return i2;
    }

    private final long zzdw(long j) throws zzlm {
        long j2 = this.zzanu;
        if (j2 != -9223372036854775807L) {
            return zzsy.zza(j, j2, 1000);
        }
        throw new zzlm("Can't scale timecode prior to timecodeScale being set.");
    }

    private static int[] zza(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        if (iArr.length >= i) {
            return iArr;
        }
        return new int[Math.max(iArr.length << 1, i)];
    }
}
