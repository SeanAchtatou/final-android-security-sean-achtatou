package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzdso extends zzdrr<zzdso> {
    public String mimeType = null;
    public Integer zzhrv = null;
    public byte[] zzhsu = null;

    public zzdso() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        Integer num = this.zzhrv;
        if (num != null) {
            zzdrp.zzx(1, num.intValue());
        }
        String str = this.mimeType;
        if (str != null) {
            zzdrp.zzf(2, str);
        }
        byte[] bArr = this.zzhsu;
        if (bArr != null) {
            zzdrp.zza(3, bArr);
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        Integer num = this.zzhrv;
        if (num != null) {
            zzor += zzdrp.zzab(1, num.intValue());
        }
        String str = this.mimeType;
        if (str != null) {
            zzor += zzdrp.zzg(2, str);
        }
        byte[] bArr = this.zzhsu;
        return bArr != null ? zzor + zzdrp.zzb(3, bArr) : zzor;
    }
}
