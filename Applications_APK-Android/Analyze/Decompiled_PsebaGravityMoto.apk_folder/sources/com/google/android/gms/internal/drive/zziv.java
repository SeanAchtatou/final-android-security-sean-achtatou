package com.google.android.gms.internal.drive;

import java.nio.charset.Charset;
import org.apache.http.protocol.HTTP;

public final class zziv {
    private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    protected static final Charset UTF_8 = Charset.forName(HTTP.UTF_8);
    private static final Object zzne = new Object();

    public static void zza(zzir zzir, zzir zzir2) {
        if (zzir.zzmw != null) {
            zzir2.zzmw = (zzit) zzir.zzmw.clone();
        }
    }
}
