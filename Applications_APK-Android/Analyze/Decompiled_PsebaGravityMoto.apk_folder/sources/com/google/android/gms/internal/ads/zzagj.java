package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.MuteThisAdListener;
import com.google.android.gms.ads.MuteThisAdReason;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.List;

@zzard
public final class zzagj extends UnifiedNativeAd {
    private final VideoController zzcje = new VideoController();
    private final List<NativeAd.Image> zzcyt = new ArrayList();
    private final zzael zzcyu;
    private final NativeAd.AdChoicesInfo zzcyv;
    private final zzagg zzczb;
    private final List<MuteThisAdReason> zzczc = new ArrayList();
    private final UnifiedNativeAd.zza zzczd;

    /* JADX WARN: Type inference failed for: r3v5, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b2 A[Catch:{ RemoteException -> 0x00be }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00cd A[Catch:{ RemoteException -> 0x00da }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzagj(com.google.android.gms.internal.ads.zzagg r6) {
        /*
            r5 = this;
            java.lang.String r0 = ""
            r5.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5.zzcyt = r1
            com.google.android.gms.ads.VideoController r1 = new com.google.android.gms.ads.VideoController
            r1.<init>()
            r5.zzcje = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5.zzczc = r1
            r5.zzczb = r6
            r6 = 0
            com.google.android.gms.internal.ads.zzagg r1 = r5.zzczb     // Catch:{ RemoteException -> 0x005e }
            java.util.List r1 = r1.getImages()     // Catch:{ RemoteException -> 0x005e }
            if (r1 == 0) goto L_0x0062
            java.util.Iterator r1 = r1.iterator()     // Catch:{ RemoteException -> 0x005e }
        L_0x0029:
            boolean r2 = r1.hasNext()     // Catch:{ RemoteException -> 0x005e }
            if (r2 == 0) goto L_0x0062
            java.lang.Object r2 = r1.next()     // Catch:{ RemoteException -> 0x005e }
            boolean r3 = r2 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x005e }
            if (r3 == 0) goto L_0x0050
            android.os.IBinder r2 = (android.os.IBinder) r2     // Catch:{ RemoteException -> 0x005e }
            if (r2 == 0) goto L_0x0050
            java.lang.String r3 = "com.google.android.gms.ads.internal.formats.client.INativeAdImage"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)     // Catch:{ RemoteException -> 0x005e }
            boolean r4 = r3 instanceof com.google.android.gms.internal.ads.zzaei     // Catch:{ RemoteException -> 0x005e }
            if (r4 == 0) goto L_0x0049
            r2 = r3
            com.google.android.gms.internal.ads.zzaei r2 = (com.google.android.gms.internal.ads.zzaei) r2     // Catch:{ RemoteException -> 0x005e }
            goto L_0x0051
        L_0x0049:
            com.google.android.gms.internal.ads.zzaek r3 = new com.google.android.gms.internal.ads.zzaek     // Catch:{ RemoteException -> 0x005e }
            r3.<init>(r2)     // Catch:{ RemoteException -> 0x005e }
            r2 = r3
            goto L_0x0051
        L_0x0050:
            r2 = r6
        L_0x0051:
            if (r2 == 0) goto L_0x0029
            java.util.List<com.google.android.gms.ads.formats.NativeAd$Image> r3 = r5.zzcyt     // Catch:{ RemoteException -> 0x005e }
            com.google.android.gms.internal.ads.zzael r4 = new com.google.android.gms.internal.ads.zzael     // Catch:{ RemoteException -> 0x005e }
            r4.<init>(r2)     // Catch:{ RemoteException -> 0x005e }
            r3.add(r4)     // Catch:{ RemoteException -> 0x005e }
            goto L_0x0029
        L_0x005e:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzbad.zzc(r0, r1)
        L_0x0062:
            com.google.android.gms.internal.ads.zzagg r1 = r5.zzczb     // Catch:{ RemoteException -> 0x0091 }
            java.util.List r1 = r1.getMuteThisAdReasons()     // Catch:{ RemoteException -> 0x0091 }
            if (r1 == 0) goto L_0x0095
            java.util.Iterator r1 = r1.iterator()     // Catch:{ RemoteException -> 0x0091 }
        L_0x006e:
            boolean r2 = r1.hasNext()     // Catch:{ RemoteException -> 0x0091 }
            if (r2 == 0) goto L_0x0095
            java.lang.Object r2 = r1.next()     // Catch:{ RemoteException -> 0x0091 }
            boolean r3 = r2 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x0091 }
            if (r3 == 0) goto L_0x0083
            android.os.IBinder r2 = (android.os.IBinder) r2     // Catch:{ RemoteException -> 0x0091 }
            com.google.android.gms.internal.ads.zzaak r2 = com.google.android.gms.internal.ads.zzaal.zzf(r2)     // Catch:{ RemoteException -> 0x0091 }
            goto L_0x0084
        L_0x0083:
            r2 = r6
        L_0x0084:
            if (r2 == 0) goto L_0x006e
            java.util.List<com.google.android.gms.ads.MuteThisAdReason> r3 = r5.zzczc     // Catch:{ RemoteException -> 0x0091 }
            com.google.android.gms.internal.ads.zzaan r4 = new com.google.android.gms.internal.ads.zzaan     // Catch:{ RemoteException -> 0x0091 }
            r4.<init>(r2)     // Catch:{ RemoteException -> 0x0091 }
            r3.add(r4)     // Catch:{ RemoteException -> 0x0091 }
            goto L_0x006e
        L_0x0091:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzbad.zzc(r0, r1)
        L_0x0095:
            com.google.android.gms.internal.ads.zzagg r1 = r5.zzczb     // Catch:{ RemoteException -> 0x00a3 }
            com.google.android.gms.internal.ads.zzaei r1 = r1.zzri()     // Catch:{ RemoteException -> 0x00a3 }
            if (r1 == 0) goto L_0x00a7
            com.google.android.gms.internal.ads.zzael r2 = new com.google.android.gms.internal.ads.zzael     // Catch:{ RemoteException -> 0x00a3 }
            r2.<init>(r1)     // Catch:{ RemoteException -> 0x00a3 }
            goto L_0x00a8
        L_0x00a3:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzbad.zzc(r0, r1)
        L_0x00a7:
            r2 = r6
        L_0x00a8:
            r5.zzcyu = r2
            com.google.android.gms.internal.ads.zzagg r1 = r5.zzczb     // Catch:{ RemoteException -> 0x00be }
            com.google.android.gms.internal.ads.zzaea r1 = r1.zzrj()     // Catch:{ RemoteException -> 0x00be }
            if (r1 == 0) goto L_0x00c2
            com.google.android.gms.internal.ads.zzaed r1 = new com.google.android.gms.internal.ads.zzaed     // Catch:{ RemoteException -> 0x00be }
            com.google.android.gms.internal.ads.zzagg r2 = r5.zzczb     // Catch:{ RemoteException -> 0x00be }
            com.google.android.gms.internal.ads.zzaea r2 = r2.zzrj()     // Catch:{ RemoteException -> 0x00be }
            r1.<init>(r2)     // Catch:{ RemoteException -> 0x00be }
            goto L_0x00c3
        L_0x00be:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzbad.zzc(r0, r1)
        L_0x00c2:
            r1 = r6
        L_0x00c3:
            r5.zzcyv = r1
            com.google.android.gms.internal.ads.zzagg r1 = r5.zzczb     // Catch:{ RemoteException -> 0x00da }
            com.google.android.gms.internal.ads.zzaee r1 = r1.zzrp()     // Catch:{ RemoteException -> 0x00da }
            if (r1 == 0) goto L_0x00de
            com.google.android.gms.internal.ads.zzaeh r1 = new com.google.android.gms.internal.ads.zzaeh     // Catch:{ RemoteException -> 0x00da }
            com.google.android.gms.internal.ads.zzagg r2 = r5.zzczb     // Catch:{ RemoteException -> 0x00da }
            com.google.android.gms.internal.ads.zzaee r2 = r2.zzrp()     // Catch:{ RemoteException -> 0x00da }
            r1.<init>(r2)     // Catch:{ RemoteException -> 0x00da }
            r6 = r1
            goto L_0x00de
        L_0x00da:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzbad.zzc(r0, r1)
        L_0x00de:
            r5.zzczd = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzagj.<init>(com.google.android.gms.internal.ads.zzagg):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: zzrh */
    public final IObjectWrapper zzkq() {
        try {
            return this.zzczb.zzrh();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final Object zzkv() {
        try {
            IObjectWrapper zzrk = this.zzczb.zzrk();
            if (zzrk != null) {
                return ObjectWrapper.unwrap(zzrk);
            }
            return null;
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final void performClick(Bundle bundle) {
        try {
            this.zzczb.performClick(bundle);
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
    }

    public final boolean recordImpression(Bundle bundle) {
        try {
            return this.zzczb.recordImpression(bundle);
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return false;
        }
    }

    public final void reportTouchEvent(Bundle bundle) {
        try {
            this.zzczb.reportTouchEvent(bundle);
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
    }

    public final String getHeadline() {
        try {
            return this.zzczb.getHeadline();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final List<NativeAd.Image> getImages() {
        return this.zzcyt;
    }

    public final String getBody() {
        try {
            return this.zzczb.getBody();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final NativeAd.Image getIcon() {
        return this.zzcyu;
    }

    public final String getCallToAction() {
        try {
            return this.zzczb.getCallToAction();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final Double getStarRating() {
        try {
            double starRating = this.zzczb.getStarRating();
            if (starRating == -1.0d) {
                return null;
            }
            return Double.valueOf(starRating);
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final String getStore() {
        try {
            return this.zzczb.getStore();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final String getPrice() {
        try {
            return this.zzczb.getPrice();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            if (this.zzczb.getVideoController() != null) {
                this.zzcje.zza(this.zzczb.getVideoController());
            }
        } catch (RemoteException e) {
            zzbad.zzc("Exception occurred while getting video controller", e);
        }
        return this.zzcje;
    }

    public final NativeAd.AdChoicesInfo getAdChoicesInfo() {
        return this.zzcyv;
    }

    public final String getMediationAdapterClassName() {
        try {
            return this.zzczb.getMediationAdapterClassName();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }

    public final Bundle getExtras() {
        try {
            Bundle extras = this.zzczb.getExtras();
            if (extras != null) {
                return extras;
            }
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
        return new Bundle();
    }

    public final void enableCustomClickGesture() {
        try {
            this.zzczb.zzro();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
    }

    public final void recordCustomClickGesture() {
        try {
            this.zzczb.recordCustomClickGesture();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
    }

    public final List<MuteThisAdReason> getMuteThisAdReasons() {
        return this.zzczc;
    }

    public final boolean isCustomMuteThisAdEnabled() {
        try {
            return this.zzczb.isCustomMuteThisAdEnabled();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return false;
        }
    }

    public final void destroy() {
        try {
            this.zzczb.destroy();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
    }

    public final void setUnconfirmedClickListener(UnifiedNativeAd.UnconfirmedClickListener unconfirmedClickListener) {
        try {
            this.zzczb.zza(new zzagt(unconfirmedClickListener));
        } catch (RemoteException e) {
            zzbad.zzc("Failed to setUnconfirmedClickListener", e);
        }
    }

    public final void muteThisAd(MuteThisAdReason muteThisAdReason) {
        try {
            if (!isCustomMuteThisAdEnabled()) {
                zzbad.zzen("Ad is not custom mute enabled");
            } else if (muteThisAdReason == null) {
                this.zzczb.zza((zzaak) null);
            } else if (muteThisAdReason instanceof zzaan) {
                this.zzczb.zza(((zzaan) muteThisAdReason).zzpu());
            } else {
                zzbad.zzen("Use mute reason from UnifiedNativeAd.getMuteThisAdReasons() or null");
            }
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
    }

    public final void setMuteThisAdListener(MuteThisAdListener muteThisAdListener) {
        try {
            this.zzczb.zza(new zzaaj(muteThisAdListener));
        } catch (RemoteException e) {
            zzbad.zzc("", e);
        }
    }

    public final void cancelUnconfirmedClick() {
        try {
            this.zzczb.cancelUnconfirmedClick();
        } catch (RemoteException e) {
            zzbad.zzc("Failed to cancelUnconfirmedClick", e);
        }
    }

    public final String getAdvertiser() {
        try {
            return this.zzczb.getAdvertiser();
        } catch (RemoteException e) {
            zzbad.zzc("", e);
            return null;
        }
    }
}
