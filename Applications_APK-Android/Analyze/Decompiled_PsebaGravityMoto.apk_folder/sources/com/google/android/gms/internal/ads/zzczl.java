package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public final class zzczl<O> {
    private final E zzgme;
    @Nullable
    private final String zzgmf;
    private final List<zzbbh<?>> zzgmj;
    final /* synthetic */ zzczf zzgmk;
    private final zzbbh<?> zzgmm;
    private final zzbbh<O> zzgmn;

    private zzczl(zzczf zzczf, E e, String str, zzbbh<?> zzbbh, List<zzbbh<?>> list, zzbbh<O> zzbbh2) {
        this.zzgmk = zzczf;
        this.zzgme = e;
        this.zzgmf = str;
        this.zzgmm = zzbbh;
        this.zzgmj = list;
        this.zzgmn = zzbbh2;
    }

    public final zzczl<O> zzfy(String str) {
        return new zzczl(this.zzgmk, this.zzgme, str, this.zzgmm, this.zzgmj, this.zzgmn);
    }

    public final <O2> zzczl<O2> zzb(zzczc zzczc) {
        return zza(new zzczm(zzczc));
    }

    public final <O2> zzczl<O2> zza(zzbal<O, O2> zzbal) {
        return zza(zzbal, this.zzgmk.zzfqw);
    }

    private final <O2> zzczl<O2> zza(zzbal zzbal, Executor executor) {
        return new zzczl(this.zzgmk, this.zzgme, this.zzgmf, this.zzgmm, this.zzgmj, zzbar.zza(this.zzgmn, zzbal, executor));
    }

    public final <O2> zzczl<O2> zzb(zzbbh zzbbh) {
        return zza(new zzczn(zzbbh), zzbbm.zzeaf);
    }

    public final <T extends Throwable> zzczl<O> zza(Class cls, zzczc zzczc) {
        return zza(cls, new zzczo(zzczc));
    }

    public final <T extends Throwable> zzczl<O> zza(Class cls, zzbal zzbal) {
        zzczf zzczf = this.zzgmk;
        return new zzczl(zzczf, this.zzgme, this.zzgmf, this.zzgmm, this.zzgmj, zzbar.zza(this.zzgmn, cls, zzbal, zzczf.zzfqw));
    }

    public final zzczl<O> zza(long j, TimeUnit timeUnit) {
        zzczf zzczf = this.zzgmk;
        return new zzczl(zzczf, this.zzgme, this.zzgmf, this.zzgmm, this.zzgmj, zzbar.zza(this.zzgmn, j, timeUnit, zzczf.zzfkf));
    }

    public final zzcze<E, O> zzane() {
        E e = this.zzgme;
        String str = this.zzgmf;
        if (str == null) {
            str = this.zzgmk.zzw(e);
        }
        zzcze<E, O> zzcze = new zzcze<>(e, str, this.zzgmn);
        this.zzgmk.zzgmi.zza(zzcze);
        this.zzgmm.zza(new zzczp(this, zzcze), zzbbm.zzeaf);
        zzbar.zza(zzcze, new zzczq(this, zzcze), zzbbm.zzeaf);
        return zzcze;
    }

    public final zzczl<O> zzx(E e) {
        return this.zzgmk.zza(e, zzane());
    }
}
