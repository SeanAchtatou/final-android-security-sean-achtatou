package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.event.Events;

abstract class zzz extends Games.zza<Events.LoadEventsResult> {
    private zzz(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzz(GoogleApiClient googleApiClient, zzw zzw) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzaa(this, status);
    }
}
