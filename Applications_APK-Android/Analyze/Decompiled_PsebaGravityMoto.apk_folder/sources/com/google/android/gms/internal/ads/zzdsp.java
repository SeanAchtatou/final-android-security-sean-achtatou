package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzdsp extends zzdrr<zzdsp> {
    private static volatile zzdsp[] zzhsv;
    public String url = null;
    public Integer zzhsw = null;
    public zzdsm zzhsx = null;
    private zzdsn zzhsy = null;
    private Integer zzhsz = null;
    private int[] zzhta = zzdry.zzhjo;
    private String zzhtb = null;
    public Integer zzhtc = null;
    public String[] zzhtd = zzdry.zzhog;

    public static zzdsp[] zzbba() {
        if (zzhsv == null) {
            synchronized (zzdrv.zzhnw) {
                if (zzhsv == null) {
                    zzhsv = new zzdsp[0];
                }
            }
        }
        return zzhsv;
    }

    public zzdsp() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        zzdrp.zzx(1, this.zzhsw.intValue());
        String str = this.url;
        if (str != null) {
            zzdrp.zzf(2, str);
        }
        zzdsm zzdsm = this.zzhsx;
        if (zzdsm != null) {
            zzdrp.zza(3, zzdsm);
        }
        int[] iArr = this.zzhta;
        int i = 0;
        if (iArr != null && iArr.length > 0) {
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.zzhta;
                if (i2 >= iArr2.length) {
                    break;
                }
                zzdrp.zzx(6, iArr2[i2]);
                i2++;
            }
        }
        Integer num = this.zzhtc;
        if (num != null) {
            zzdrp.zzx(8, num.intValue());
        }
        String[] strArr = this.zzhtd;
        if (strArr != null && strArr.length > 0) {
            while (true) {
                String[] strArr2 = this.zzhtd;
                if (i >= strArr2.length) {
                    break;
                }
                String str2 = strArr2[i];
                if (str2 != null) {
                    zzdrp.zzf(9, str2);
                }
                i++;
            }
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int[] iArr;
        int zzor = super.zzor() + zzdrp.zzab(1, this.zzhsw.intValue());
        String str = this.url;
        if (str != null) {
            zzor += zzdrp.zzg(2, str);
        }
        zzdsm zzdsm = this.zzhsx;
        if (zzdsm != null) {
            zzor += zzdrp.zzb(3, zzdsm);
        }
        int[] iArr2 = this.zzhta;
        int i = 0;
        if (iArr2 != null && iArr2.length > 0) {
            int i2 = 0;
            int i3 = 0;
            while (true) {
                iArr = this.zzhta;
                if (i2 >= iArr.length) {
                    break;
                }
                i3 += zzdrp.zzge(iArr[i2]);
                i2++;
            }
            zzor = zzor + i3 + (iArr.length * 1);
        }
        Integer num = this.zzhtc;
        if (num != null) {
            zzor += zzdrp.zzab(8, num.intValue());
        }
        String[] strArr = this.zzhtd;
        if (strArr == null || strArr.length <= 0) {
            return zzor;
        }
        int i4 = 0;
        int i5 = 0;
        while (true) {
            String[] strArr2 = this.zzhtd;
            if (i >= strArr2.length) {
                return zzor + i4 + (i5 * 1);
            }
            String str2 = strArr2[i];
            if (str2 != null) {
                i5++;
                i4 += zzdrp.zzgx(str2);
            }
            i++;
        }
    }
}
