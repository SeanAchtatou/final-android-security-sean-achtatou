package com.google.android.gms.internal.ads;

import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.internal.ads.zzdob;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

final class zzdpo<T> implements zzdqb<T> {
    private static final int[] zzhjo = new int[0];
    private static final Unsafe zzhjp = zzdqz.zzbae();
    private final int[] zzhjq;
    private final Object[] zzhjr;
    private final int zzhjs;
    private final int zzhjt;
    private final zzdpk zzhju;
    private final boolean zzhjv;
    private final boolean zzhjw;
    private final boolean zzhjx;
    private final boolean zzhjy;
    private final int[] zzhjz;
    private final int zzhka;
    private final int zzhkb;
    private final zzdps zzhkc;
    private final zzdou zzhkd;
    private final zzdqt<?, ?> zzhke;
    private final zzdnp<?> zzhkf;
    private final zzdpf zzhkg;

    private zzdpo(int[] iArr, Object[] objArr, int i, int i2, zzdpk zzdpk, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzdps zzdps, zzdou zzdou, zzdqt<?, ?> zzdqt, zzdnp<?> zzdnp, zzdpf zzdpf) {
        this.zzhjq = iArr;
        this.zzhjr = objArr;
        this.zzhjs = i;
        this.zzhjt = i2;
        this.zzhjw = zzdpk instanceof zzdob;
        this.zzhjx = z;
        this.zzhjv = zzdnp != null && zzdnp.zzm(zzdpk);
        this.zzhjy = false;
        this.zzhjz = iArr2;
        this.zzhka = i3;
        this.zzhkb = i4;
        this.zzhkc = zzdps;
        this.zzhkd = zzdou;
        this.zzhke = zzdqt;
        this.zzhkf = zzdnp;
        this.zzhju = zzdpk;
        this.zzhkg = zzdpf;
    }

    private static boolean zzgw(int i) {
        return (i & DriveFile.MODE_WRITE_ONLY) != 0;
    }

    static <T> zzdpo<T> zza(Class<T> cls, zzdpi zzdpi, zzdps zzdps, zzdou zzdou, zzdqt<?, ?> zzdqt, zzdnp<?> zzdnp, zzdpf zzdpf) {
        int i;
        int i2;
        char c;
        int[] iArr;
        char c2;
        char c3;
        int i3;
        char c4;
        char c5;
        int i4;
        int i5;
        String str;
        char c6;
        int i6;
        char c7;
        int i7;
        int i8;
        int i9;
        int i10;
        Class<?> cls2;
        int i11;
        int i12;
        Field field;
        int i13;
        char charAt;
        int i14;
        char c8;
        Field field2;
        Field field3;
        int i15;
        char charAt2;
        int i16;
        char charAt3;
        int i17;
        char charAt4;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        char charAt5;
        int i24;
        char charAt6;
        int i25;
        char charAt7;
        int i26;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        zzdpi zzdpi2 = zzdpi;
        if (zzdpi2 instanceof zzdpz) {
            zzdpz zzdpz = (zzdpz) zzdpi2;
            char c9 = 0;
            boolean z = zzdpz.zzayz() == zzdob.zze.zzhht;
            String zzazi = zzdpz.zzazi();
            int length = zzazi.length();
            char charAt15 = zzazi.charAt(0);
            if (charAt15 >= 55296) {
                char c10 = charAt15 & 8191;
                int i27 = 1;
                int i28 = 13;
                while (true) {
                    i = i27 + 1;
                    charAt14 = zzazi.charAt(i27);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c10 |= (charAt14 & 8191) << i28;
                    i28 += 13;
                    i27 = i;
                }
                charAt15 = (charAt14 << i28) | c10;
            } else {
                i = 1;
            }
            int i29 = i + 1;
            char charAt16 = zzazi.charAt(i);
            if (charAt16 >= 55296) {
                char c11 = charAt16 & 8191;
                int i30 = 13;
                while (true) {
                    i2 = i29 + 1;
                    charAt13 = zzazi.charAt(i29);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c11 |= (charAt13 & 8191) << i30;
                    i30 += 13;
                    i29 = i2;
                }
                charAt16 = c11 | (charAt13 << i30);
            } else {
                i2 = i29;
            }
            if (charAt16 == 0) {
                iArr = zzhjo;
                c5 = 0;
                c4 = 0;
                i3 = 0;
                c3 = 0;
                c2 = 0;
                c = 0;
            } else {
                int i31 = i2 + 1;
                char charAt17 = zzazi.charAt(i2);
                if (charAt17 >= 55296) {
                    char c12 = charAt17 & 8191;
                    int i32 = 13;
                    while (true) {
                        i18 = i31 + 1;
                        charAt12 = zzazi.charAt(i31);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c12 |= (charAt12 & 8191) << i32;
                        i32 += 13;
                        i31 = i18;
                    }
                    charAt17 = (charAt12 << i32) | c12;
                } else {
                    i18 = i31;
                }
                int i33 = i18 + 1;
                char charAt18 = zzazi.charAt(i18);
                if (charAt18 >= 55296) {
                    char c13 = charAt18 & 8191;
                    int i34 = 13;
                    while (true) {
                        i19 = i33 + 1;
                        charAt11 = zzazi.charAt(i33);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c13 |= (charAt11 & 8191) << i34;
                        i34 += 13;
                        i33 = i19;
                    }
                    charAt18 = c13 | (charAt11 << i34);
                } else {
                    i19 = i33;
                }
                int i35 = i19 + 1;
                char charAt19 = zzazi.charAt(i19);
                if (charAt19 >= 55296) {
                    char c14 = charAt19 & 8191;
                    int i36 = 13;
                    while (true) {
                        i20 = i35 + 1;
                        charAt10 = zzazi.charAt(i35);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c14 |= (charAt10 & 8191) << i36;
                        i36 += 13;
                        i35 = i20;
                    }
                    charAt19 = (charAt10 << i36) | c14;
                } else {
                    i20 = i35;
                }
                int i37 = i20 + 1;
                c3 = zzazi.charAt(i20);
                if (c3 >= 55296) {
                    char c15 = c3 & 8191;
                    int i38 = 13;
                    while (true) {
                        i21 = i37 + 1;
                        charAt9 = zzazi.charAt(i37);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c15 |= (charAt9 & 8191) << i38;
                        i38 += 13;
                        i37 = i21;
                    }
                    c3 = (charAt9 << i38) | c15;
                } else {
                    i21 = i37;
                }
                int i39 = i21 + 1;
                c2 = zzazi.charAt(i21);
                if (c2 >= 55296) {
                    char c16 = c2 & 8191;
                    int i40 = 13;
                    while (true) {
                        i26 = i39 + 1;
                        charAt8 = zzazi.charAt(i39);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c16 |= (charAt8 & 8191) << i40;
                        i40 += 13;
                        i39 = i26;
                    }
                    c2 = (charAt8 << i40) | c16;
                    i39 = i26;
                }
                int i41 = i39 + 1;
                c5 = zzazi.charAt(i39);
                if (c5 >= 55296) {
                    char c17 = c5 & 8191;
                    int i42 = 13;
                    while (true) {
                        i25 = i41 + 1;
                        charAt7 = zzazi.charAt(i41);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c17 |= (charAt7 & 8191) << i42;
                        i42 += 13;
                        i41 = i25;
                    }
                    c5 = c17 | (charAt7 << i42);
                    i41 = i25;
                }
                int i43 = i41 + 1;
                char charAt20 = zzazi.charAt(i41);
                if (charAt20 >= 55296) {
                    int i44 = 13;
                    int i45 = i43;
                    char c18 = charAt20 & 8191;
                    int i46 = i45;
                    while (true) {
                        i24 = i46 + 1;
                        charAt6 = zzazi.charAt(i46);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c18 |= (charAt6 & 8191) << i44;
                        i44 += 13;
                        i46 = i24;
                    }
                    charAt20 = c18 | (charAt6 << i44);
                    i22 = i24;
                } else {
                    i22 = i43;
                }
                int i47 = i22 + 1;
                c9 = zzazi.charAt(i22);
                if (c9 >= 55296) {
                    int i48 = 13;
                    int i49 = i47;
                    char c19 = c9 & 8191;
                    int i50 = i49;
                    while (true) {
                        i23 = i50 + 1;
                        charAt5 = zzazi.charAt(i50);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c19 |= (charAt5 & 8191) << i48;
                        i48 += 13;
                        i50 = i23;
                    }
                    c9 = c19 | (charAt5 << i48);
                    i47 = i23;
                }
                iArr = new int[(c9 + c5 + charAt20)];
                i3 = (charAt17 << 1) + charAt18;
                int i51 = i47;
                c = charAt17;
                c4 = charAt19;
                i2 = i51;
            }
            Unsafe unsafe = zzhjp;
            Object[] zzazj = zzdpz.zzazj();
            Class<?> cls3 = zzdpz.zzazb().getClass();
            int i52 = i3;
            int[] iArr2 = new int[(c2 * 3)];
            Object[] objArr = new Object[(c2 << 1)];
            int i53 = c9 + c5;
            char c20 = c9;
            int i54 = i53;
            int i55 = 0;
            int i56 = 0;
            while (i2 < length) {
                int i57 = i2 + 1;
                char charAt21 = zzazi.charAt(i2);
                char c21 = 55296;
                if (charAt21 >= 55296) {
                    int i58 = 13;
                    int i59 = i57;
                    char c22 = charAt21 & 8191;
                    int i60 = i59;
                    while (true) {
                        i17 = i60 + 1;
                        charAt4 = zzazi.charAt(i60);
                        if (charAt4 < c21) {
                            break;
                        }
                        c22 |= (charAt4 & 8191) << i58;
                        i58 += 13;
                        i60 = i17;
                        c21 = 55296;
                    }
                    charAt21 = c22 | (charAt4 << i58);
                    i4 = i17;
                } else {
                    i4 = i57;
                }
                int i61 = i4 + 1;
                char charAt22 = zzazi.charAt(i4);
                int i62 = length;
                char c23 = 55296;
                if (charAt22 >= 55296) {
                    int i63 = 13;
                    int i64 = i61;
                    char c24 = charAt22 & 8191;
                    int i65 = i64;
                    while (true) {
                        i16 = i65 + 1;
                        charAt3 = zzazi.charAt(i65);
                        if (charAt3 < c23) {
                            break;
                        }
                        c24 |= (charAt3 & 8191) << i63;
                        i63 += 13;
                        i65 = i16;
                        c23 = 55296;
                    }
                    charAt22 = c24 | (charAt3 << i63);
                    i5 = i16;
                } else {
                    i5 = i61;
                }
                char c25 = c9;
                char c26 = charAt22 & 255;
                boolean z2 = z;
                if ((charAt22 & 1024) != 0) {
                    iArr[i55] = i56;
                    i55++;
                }
                int i66 = i55;
                if (c26 >= '3') {
                    int i67 = i5 + 1;
                    char charAt23 = zzazi.charAt(i5);
                    char c27 = 55296;
                    if (charAt23 >= 55296) {
                        char c28 = charAt23 & 8191;
                        int i68 = 13;
                        while (true) {
                            i15 = i67 + 1;
                            charAt2 = zzazi.charAt(i67);
                            if (charAt2 < c27) {
                                break;
                            }
                            c28 |= (charAt2 & 8191) << i68;
                            i68 += 13;
                            i67 = i15;
                            c27 = 55296;
                        }
                        charAt23 = c28 | (charAt2 << i68);
                        i67 = i15;
                    }
                    int i69 = c26 - '3';
                    int i70 = i67;
                    if (i69 == 9 || i69 == 17) {
                        c8 = 1;
                        objArr[((i56 / 3) << 1) + 1] = zzazj[i52];
                        i52++;
                    } else {
                        if (i69 == 12 && (charAt15 & 1) == 1) {
                            objArr[((i56 / 3) << 1) + 1] = zzazj[i52];
                            i52++;
                        }
                        c8 = 1;
                    }
                    int i71 = charAt23 << c8;
                    Object obj = zzazj[i71];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = zza(cls3, (String) obj);
                        zzazj[i71] = field2;
                    }
                    char c29 = c4;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i72 = i71 + 1;
                    Object obj2 = zzazj[i72];
                    int i73 = objectFieldOffset;
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = zza(cls3, (String) obj2);
                        zzazj[i72] = field3;
                    }
                    str = zzazi;
                    i9 = (int) unsafe.objectFieldOffset(field3);
                    cls2 = cls3;
                    i6 = i52;
                    i8 = i73;
                    i10 = 0;
                    c6 = c29;
                    c7 = c3;
                    i7 = charAt21;
                    i12 = i70;
                } else {
                    char c30 = c4;
                    int i74 = i52 + 1;
                    Field zza = zza(cls3, (String) zzazj[i52]);
                    c7 = c3;
                    if (c26 == 9 || c26 == 17) {
                        c6 = c30;
                        objArr[((i56 / 3) << 1) + 1] = zza.getType();
                    } else {
                        if (c26 == 27 || c26 == '1') {
                            c6 = c30;
                            i14 = i74 + 1;
                            objArr[((i56 / 3) << 1) + 1] = zzazj[i74];
                        } else if (c26 == 12 || c26 == 30 || c26 == ',') {
                            c6 = c30;
                            if ((charAt15 & 1) == 1) {
                                i14 = i74 + 1;
                                objArr[((i56 / 3) << 1) + 1] = zzazj[i74];
                            }
                        } else if (c26 == '2') {
                            int i75 = c20 + 1;
                            iArr[c20] = i56;
                            int i76 = (i56 / 3) << 1;
                            int i77 = i74 + 1;
                            objArr[i76] = zzazj[i74];
                            if ((charAt22 & 2048) != 0) {
                                i74 = i77 + 1;
                                objArr[i76 + 1] = zzazj[i77];
                                c6 = c30;
                                c20 = i75;
                            } else {
                                c20 = i75;
                                i74 = i77;
                                c6 = c30;
                            }
                        } else {
                            c6 = c30;
                        }
                        i7 = charAt21;
                        i74 = i14;
                        i8 = (int) unsafe.objectFieldOffset(zza);
                        if ((charAt15 & 1) == 1 || c26 > 17) {
                            str = zzazi;
                            cls2 = cls3;
                            i6 = i74;
                            i11 = i5;
                            i10 = 0;
                            i9 = 0;
                        } else {
                            i11 = i5 + 1;
                            char charAt24 = zzazi.charAt(i5);
                            if (charAt24 >= 55296) {
                                char c31 = charAt24 & 8191;
                                int i78 = 13;
                                while (true) {
                                    i13 = i11 + 1;
                                    charAt = zzazi.charAt(i11);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c31 |= (charAt & 8191) << i78;
                                    i78 += 13;
                                    i11 = i13;
                                }
                                charAt24 = c31 | (charAt << i78);
                                i11 = i13;
                            }
                            int i79 = (c << 1) + (charAt24 / ' ');
                            Object obj3 = zzazj[i79];
                            str = zzazi;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = zza(cls3, (String) obj3);
                                zzazj[i79] = field;
                            }
                            cls2 = cls3;
                            i6 = i74;
                            i9 = (int) unsafe.objectFieldOffset(field);
                            i10 = charAt24 % ' ';
                        }
                        if (c26 >= 18 && c26 <= '1') {
                            iArr[i54] = i8;
                            i54++;
                        }
                        i12 = i11;
                    }
                    i7 = charAt21;
                    i8 = (int) unsafe.objectFieldOffset(zza);
                    if ((charAt15 & 1) == 1) {
                    }
                    str = zzazi;
                    cls2 = cls3;
                    i6 = i74;
                    i11 = i5;
                    i10 = 0;
                    i9 = 0;
                    iArr[i54] = i8;
                    i54++;
                    i12 = i11;
                }
                int i80 = i56 + 1;
                iArr2[i56] = i7;
                int i81 = i80 + 1;
                iArr2[i80] = (c26 << 20) | ((charAt22 & 256) != 0 ? DriveFile.MODE_READ_ONLY : 0) | ((charAt22 & 512) != 0 ? DriveFile.MODE_WRITE_ONLY : 0) | i8;
                i56 = i81 + 1;
                iArr2[i81] = (i10 << 20) | i9;
                cls3 = cls2;
                c3 = c7;
                c9 = c25;
                i52 = i6;
                length = i62;
                z = z2;
                c4 = c6;
                i55 = i66;
                zzazi = str;
            }
            return new zzdpo(iArr2, objArr, c4, c3, zzdpz.zzazb(), z, false, iArr, c9, i53, zzdps, zzdou, zzdqt, zzdnp, zzdpf);
        }
        ((zzdqo) zzdpi2).zzayz();
        throw new NoSuchMethodError();
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public final T newInstance() {
        return this.zzhkc.newInstance(this.zzhju);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.ads.zzdqd.zze(com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6), com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.ads.zzdqd.zze(com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6), com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.ads.zzdqd.zze(com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6), com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.ads.zzdqd.zze(com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6), com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzm(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzm(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6) == com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.ads.zzdqz.zzn(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.ads.zzdqz.zzn(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.ads.zzdqz.zzo(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.ads.zzdqz.zzo(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.ads.zzdqd.zze(com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6), com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.zzhjq
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.zzgu(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.zzgv(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.google.android.gms.internal.ads.zzdqz.zzk(r10, r4)
            int r4 = com.google.android.gms.internal.ads.zzdqz.zzk(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.ads.zzdqd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)
            boolean r3 = com.google.android.gms.internal.ads.zzdqd.zze(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)
            boolean r3 = com.google.android.gms.internal.ads.zzdqd.zze(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.ads.zzdqd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.ads.zzdqd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.ads.zzdqd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdqz.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.ads.zzdqd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.google.android.gms.internal.ads.zzdqz.zzm(r10, r6)
            boolean r5 = com.google.android.gms.internal.ads.zzdqz.zzm(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.ads.zzdqz.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.ads.zzdqz.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.ads.zzdqz.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.ads.zzdqz.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.google.android.gms.internal.ads.zzdqz.zzn(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.google.android.gms.internal.ads.zzdqz.zzn(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.google.android.gms.internal.ads.zzdqz.zzo(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.google.android.gms.internal.ads.zzdqz.zzo(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.google.android.gms.internal.ads.zzdqt<?, ?> r0 = r9.zzhke
            java.lang.Object r0 = r0.zzao(r10)
            com.google.android.gms.internal.ads.zzdqt<?, ?> r2 = r9.zzhke
            java.lang.Object r2 = r2.zzao(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.zzhjv
            if (r0 == 0) goto L_0x01f1
            com.google.android.gms.internal.ads.zzdnp<?> r0 = r9.zzhkf
            com.google.android.gms.internal.ads.zzdns r10 = r0.zzy(r10)
            com.google.android.gms.internal.ads.zzdnp<?> r0 = r9.zzhkf
            com.google.android.gms.internal.ads.zzdns r11 = r0.zzy(r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.equals(java.lang.Object, java.lang.Object):boolean");
    }

    public final int hashCode(T t) {
        int i;
        int i2;
        int length = this.zzhjq.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int zzgu = zzgu(i4);
            int i5 = this.zzhjq[i4];
            long j = (long) (1048575 & zzgu);
            int i6 = 37;
            switch ((zzgu & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = zzdod.zzft(Double.doubleToLongBits(zzdqz.zzo(t, j)));
                    i3 = i2 + i;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(zzdqz.zzn(t, j));
                    i3 = i2 + i;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = zzdod.zzft(zzdqz.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = zzdod.zzft(zzdqz.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = zzdqz.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = zzdod.zzft(zzdqz.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = zzdqz.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = zzdod.zzbh(zzdqz.zzm(t, j));
                    i3 = i2 + i;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) zzdqz.zzp(t, j)).hashCode();
                    i3 = i2 + i;
                    break;
                case 9:
                    Object zzp = zzdqz.zzp(t, j);
                    if (zzp != null) {
                        i6 = zzp.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = zzdqz.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = zzdqz.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = zzdqz.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = zzdqz.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = zzdod.zzft(zzdqz.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = zzdqz.zzk(t, j);
                    i3 = i2 + i;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = zzdod.zzft(zzdqz.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 17:
                    Object zzp2 = zzdqz.zzp(t, j);
                    if (zzp2 != null) {
                        i6 = zzp2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = zzdqz.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = zzdqz.zzp(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 51:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdod.zzft(Double.doubleToLongBits(zzf(t, j)));
                        i3 = i2 + i;
                        break;
                    }
                case 52:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(zzg(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 53:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdod.zzft(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 54:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdod.zzft(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 55:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 56:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdod.zzft(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 57:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 58:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdod.zzbh(zzj(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 59:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = ((String) zzdqz.zzp(t, j)).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 60:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdqz.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 61:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdqz.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 62:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 63:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 64:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 65:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdod.zzft(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 66:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzh(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 67:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdod.zzft(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 68:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzdqz.zzp(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
            }
        }
        int hashCode = (i3 * 53) + this.zzhke.zzao(t).hashCode();
        return this.zzhjv ? (hashCode * 53) + this.zzhkf.zzy(t).hashCode() : hashCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void */
    public final void zzd(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.zzhjq.length; i += 3) {
                int zzgu = zzgu(i);
                long j = (long) (1048575 & zzgu);
                int i2 = this.zzhjq[i];
                switch ((zzgu & 267386880) >>> 20) {
                    case 0:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza(t, j, zzdqz.zzo(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 1:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza((Object) t, j, zzdqz.zzn(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 2:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza((Object) t, j, zzdqz.zzl(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 3:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza((Object) t, j, zzdqz.zzl(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 4:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zzb(t, j, zzdqz.zzk(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 5:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza((Object) t, j, zzdqz.zzl(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 6:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zzb(t, j, zzdqz.zzk(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 7:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza((Object) t, j, zzdqz.zzm(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 8:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza(t, j, zzdqz.zzp(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 9:
                        zza(t, t2, i);
                        break;
                    case 10:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza(t, j, zzdqz.zzp(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 11:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zzb(t, j, zzdqz.zzk(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 12:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zzb(t, j, zzdqz.zzk(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 13:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zzb(t, j, zzdqz.zzk(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 14:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza((Object) t, j, zzdqz.zzl(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 15:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zzb(t, j, zzdqz.zzk(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 16:
                        if (!zzd(t2, i)) {
                            break;
                        } else {
                            zzdqz.zza((Object) t, j, zzdqz.zzl(t2, j));
                            zze((Object) t, i);
                            break;
                        }
                    case 17:
                        zza(t, t2, i);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.zzhkd.zza(t, t2, j);
                        break;
                    case 50:
                        zzdqd.zza(this.zzhkg, t, t2, j);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzdqz.zza(t, j, zzdqz.zzp(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 60:
                        zzb(t, t2, i);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzdqz.zza(t, j, zzdqz.zzp(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 68:
                        zzb(t, t2, i);
                        break;
                }
            }
            if (!this.zzhjx) {
                zzdqd.zza(this.zzhke, t, t2);
                if (this.zzhjv) {
                    zzdqd.zza(this.zzhkf, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, int):void */
    private final void zza(T t, T t2, int i) {
        long zzgu = (long) (zzgu(i) & 1048575);
        if (zzd(t2, i)) {
            Object zzp = zzdqz.zzp(t, zzgu);
            Object zzp2 = zzdqz.zzp(t2, zzgu);
            if (zzp != null && zzp2 != null) {
                zzdqz.zza(t, zzgu, zzdod.zzb(zzp, zzp2));
                zze((Object) t, i);
            } else if (zzp2 != null) {
                zzdqz.zza(t, zzgu, zzp2);
                zze((Object) t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzgu = zzgu(i);
        int i2 = this.zzhjq[i];
        long j = (long) (zzgu & 1048575);
        if (zza(t2, i2, i)) {
            Object zzp = zzdqz.zzp(t, j);
            Object zzp2 = zzdqz.zzp(t2, j);
            if (zzp != null && zzp2 != null) {
                zzdqz.zza(t, j, zzdod.zzb(zzp, zzp2));
                zzb(t, i2, i);
            } else if (zzp2 != null) {
                zzdqz.zza(t, j, zzp2);
                zzb(t, i2, i);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdni.zzj(int, boolean):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.ads.zzdni.zzj(int, long):void
      com.google.android.gms.internal.ads.zzdni.zzj(int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdni.zzb(int, float):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.ads.zzdni.zzb(int, com.google.android.gms.internal.ads.zzdor):int
      com.google.android.gms.internal.ads.zzdni.zzb(com.google.android.gms.internal.ads.zzdpk, com.google.android.gms.internal.ads.zzdqb):int
      com.google.android.gms.internal.ads.zzdni.zzb(int, double):void
      com.google.android.gms.internal.ads.zzdni.zzb(int, com.google.android.gms.internal.ads.zzdmr):void
      com.google.android.gms.internal.ads.zzdni.zzb(int, com.google.android.gms.internal.ads.zzdpk):void
      com.google.android.gms.internal.ads.zzdni.zzb(int, float):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdpo.zza(com.google.android.gms.internal.ads.zzdqt, java.lang.Object):int
     arg types: [com.google.android.gms.internal.ads.zzdqt<?, ?>, T]
     candidates:
      com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Object, com.google.android.gms.internal.ads.zzdro):void
      com.google.android.gms.internal.ads.zzdqb.zza(java.lang.Object, com.google.android.gms.internal.ads.zzdro):void
      com.google.android.gms.internal.ads.zzdpo.zza(com.google.android.gms.internal.ads.zzdqt, java.lang.Object):int */
    public final int zzak(T t) {
        int i;
        int i2;
        long j;
        int i3;
        int zzj;
        int i4;
        int i5;
        int i6;
        int i7;
        int zzc;
        int i8;
        int i9;
        int i10;
        T t2 = t;
        int i11 = 267386880;
        if (this.zzhjx) {
            Unsafe unsafe = zzhjp;
            int i12 = 0;
            int i13 = 0;
            while (i12 < this.zzhjq.length) {
                int zzgu = zzgu(i12);
                int i14 = (zzgu & i11) >>> 20;
                int i15 = this.zzhjq[i12];
                long j2 = (long) (zzgu & 1048575);
                int i16 = (i14 < zzdnv.DOUBLE_LIST_PACKED.id() || i14 > zzdnv.SINT64_LIST_PACKED.id()) ? 0 : this.zzhjq[i12 + 2] & 1048575;
                switch (i14) {
                    case 0:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzc(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 1:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzb(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 2:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzk(i15, zzdqz.zzl(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 3:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzl(i15, zzdqz.zzl(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 4:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzab(i15, zzdqz.zzk(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 5:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzn(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 6:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzae(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 7:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzj(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 8:
                        if (zzd(t2, i12)) {
                            Object zzp = zzdqz.zzp(t2, j2);
                            if (!(zzp instanceof zzdmr)) {
                                zzc = zzdni.zzg(i15, (String) zzp);
                                break;
                            } else {
                                zzc = zzdni.zzc(i15, (zzdmr) zzp);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 9:
                        if (zzd(t2, i12)) {
                            zzc = zzdqd.zzc(i15, zzdqz.zzp(t2, j2), zzgr(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 10:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzc(i15, (zzdmr) zzdqz.zzp(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 11:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzac(i15, zzdqz.zzk(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 12:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzag(i15, zzdqz.zzk(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 13:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzaf(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 14:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzo(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 15:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzad(i15, zzdqz.zzk(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 16:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzm(i15, zzdqz.zzl(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 17:
                        if (zzd(t2, i12)) {
                            zzc = zzdni.zzc(i15, (zzdpk) zzdqz.zzp(t2, j2), zzgr(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 18:
                        zzc = zzdqd.zzw(i15, zze(t2, j2), false);
                        break;
                    case 19:
                        zzc = zzdqd.zzv(i15, zze(t2, j2), false);
                        break;
                    case 20:
                        zzc = zzdqd.zzo(i15, zze(t2, j2), false);
                        break;
                    case 21:
                        zzc = zzdqd.zzp(i15, zze(t2, j2), false);
                        break;
                    case 22:
                        zzc = zzdqd.zzs(i15, zze(t2, j2), false);
                        break;
                    case 23:
                        zzc = zzdqd.zzw(i15, zze(t2, j2), false);
                        break;
                    case 24:
                        zzc = zzdqd.zzv(i15, zze(t2, j2), false);
                        break;
                    case 25:
                        zzc = zzdqd.zzx(i15, zze(t2, j2), false);
                        break;
                    case 26:
                        zzc = zzdqd.zzc(i15, zze(t2, j2));
                        break;
                    case 27:
                        zzc = zzdqd.zzc(i15, (List<?>) zze(t2, j2), zzgr(i12));
                        break;
                    case 28:
                        zzc = zzdqd.zzd(i15, zze(t2, j2));
                        break;
                    case 29:
                        zzc = zzdqd.zzt(i15, zze(t2, j2), false);
                        break;
                    case 30:
                        zzc = zzdqd.zzr(i15, zze(t2, j2), false);
                        break;
                    case 31:
                        zzc = zzdqd.zzv(i15, zze(t2, j2), false);
                        break;
                    case 32:
                        zzc = zzdqd.zzw(i15, zze(t2, j2), false);
                        break;
                    case 33:
                        zzc = zzdqd.zzu(i15, zze(t2, j2), false);
                        break;
                    case 34:
                        zzc = zzdqd.zzq(i15, zze(t2, j2), false);
                        break;
                    case 35:
                        i9 = zzdqd.zzag((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 36:
                        i9 = zzdqd.zzaf((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 37:
                        i9 = zzdqd.zzy((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 38:
                        i9 = zzdqd.zzz((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 39:
                        i9 = zzdqd.zzac((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 40:
                        i9 = zzdqd.zzag((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 41:
                        i9 = zzdqd.zzaf((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 42:
                        i9 = zzdqd.zzah((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 43:
                        i9 = zzdqd.zzad((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 44:
                        i9 = zzdqd.zzab((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 45:
                        i9 = zzdqd.zzaf((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 46:
                        i9 = zzdqd.zzag((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 47:
                        i9 = zzdqd.zzae((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 48:
                        i9 = zzdqd.zzaa((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.zzhjy) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = zzdni.zzgd(i15);
                            i8 = zzdni.zzgf(i9);
                            zzc = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 49:
                        zzc = zzdqd.zzd(i15, zze(t2, j2), zzgr(i12));
                        break;
                    case 50:
                        zzc = this.zzhkg.zzb(i15, zzdqz.zzp(t2, j2), zzgs(i12));
                        break;
                    case 51:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzc(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 52:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzb(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 53:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzk(i15, zzi(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 54:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzl(i15, zzi(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 55:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzab(i15, zzh(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 56:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzn(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 57:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzae(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 58:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzj(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 59:
                        if (zza(t2, i15, i12)) {
                            Object zzp2 = zzdqz.zzp(t2, j2);
                            if (!(zzp2 instanceof zzdmr)) {
                                zzc = zzdni.zzg(i15, (String) zzp2);
                                break;
                            } else {
                                zzc = zzdni.zzc(i15, (zzdmr) zzp2);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 60:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdqd.zzc(i15, zzdqz.zzp(t2, j2), zzgr(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 61:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzc(i15, (zzdmr) zzdqz.zzp(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 62:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzac(i15, zzh(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 63:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzag(i15, zzh(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 64:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzaf(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 65:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzo(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 66:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzad(i15, zzh(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 67:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzm(i15, zzi(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 68:
                        if (zza(t2, i15, i12)) {
                            zzc = zzdni.zzc(i15, (zzdpk) zzdqz.zzp(t2, j2), zzgr(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    default:
                        i12 += 3;
                        i11 = 267386880;
                }
                i13 += zzc;
                i12 += 3;
                i11 = 267386880;
            }
            return i13 + zza((zzdqt) this.zzhke, (Object) t2);
        }
        Unsafe unsafe2 = zzhjp;
        int i17 = 0;
        int i18 = -1;
        int i19 = 0;
        for (int i20 = 0; i20 < this.zzhjq.length; i20 += 3) {
            int zzgu2 = zzgu(i20);
            int[] iArr = this.zzhjq;
            int i21 = iArr[i20];
            int i22 = (zzgu2 & 267386880) >>> 20;
            if (i22 <= 17) {
                i2 = iArr[i20 + 2];
                int i23 = i2 & 1048575;
                i = 1 << (i2 >>> 20);
                if (i23 != i18) {
                    i19 = unsafe2.getInt(t2, (long) i23);
                } else {
                    i23 = i18;
                }
                i18 = i23;
            } else {
                i2 = (!this.zzhjy || i22 < zzdnv.DOUBLE_LIST_PACKED.id() || i22 > zzdnv.SINT64_LIST_PACKED.id()) ? 0 : this.zzhjq[i20 + 2] & 1048575;
                i = 0;
            }
            long j3 = (long) (zzgu2 & 1048575);
            switch (i22) {
                case 0:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += zzdni.zzc(i21, 0.0d);
                        break;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += zzdni.zzb(i21, 0.0f);
                        break;
                    }
                case 2:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzdni.zzk(i21, unsafe2.getLong(t2, j3));
                        i17 += i3;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzdni.zzl(i21, unsafe2.getLong(t2, j3));
                        i17 += i3;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = zzdni.zzab(i21, unsafe2.getInt(t2, j3));
                        i17 += i3;
                    }
                    break;
                case 5:
                    if ((i19 & i) != 0) {
                        j = 0;
                        i3 = zzdni.zzn(i21, 0);
                        i17 += i3;
                        break;
                    } else {
                        j = 0;
                    }
                case 6:
                    if ((i19 & i) != 0) {
                        i17 += zzdni.zzae(i21, 0);
                        j = 0;
                        break;
                    }
                    j = 0;
                case 7:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzj(i21, true);
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 8:
                    if ((i19 & i) != 0) {
                        Object object = unsafe2.getObject(t2, j3);
                        zzj = object instanceof zzdmr ? zzdni.zzc(i21, (zzdmr) object) : zzdni.zzg(i21, (String) object);
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 9:
                    if ((i19 & i) != 0) {
                        zzj = zzdqd.zzc(i21, unsafe2.getObject(t2, j3), zzgr(i20));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 10:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzc(i21, (zzdmr) unsafe2.getObject(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 11:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzac(i21, unsafe2.getInt(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 12:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzag(i21, unsafe2.getInt(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 13:
                    if ((i19 & i) != 0) {
                        i4 = zzdni.zzaf(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 14:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzo(i21, 0);
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 15:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzad(i21, unsafe2.getInt(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 16:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzm(i21, unsafe2.getLong(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 17:
                    if ((i19 & i) != 0) {
                        zzj = zzdni.zzc(i21, (zzdpk) unsafe2.getObject(t2, j3), zzgr(i20));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 18:
                    zzj = zzdqd.zzw(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 19:
                    zzj = zzdqd.zzv(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 20:
                    zzj = zzdqd.zzo(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 21:
                    zzj = zzdqd.zzp(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 22:
                    zzj = zzdqd.zzs(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 23:
                    zzj = zzdqd.zzw(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 24:
                    zzj = zzdqd.zzv(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 25:
                    zzj = zzdqd.zzx(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 26:
                    zzj = zzdqd.zzc(i21, (List) unsafe2.getObject(t2, j3));
                    i17 += zzj;
                    j = 0;
                    break;
                case 27:
                    zzj = zzdqd.zzc(i21, (List<?>) ((List) unsafe2.getObject(t2, j3)), zzgr(i20));
                    i17 += zzj;
                    j = 0;
                    break;
                case 28:
                    zzj = zzdqd.zzd(i21, (List) unsafe2.getObject(t2, j3));
                    i17 += zzj;
                    j = 0;
                    break;
                case 29:
                    zzj = zzdqd.zzt(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 30:
                    zzj = zzdqd.zzr(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 31:
                    zzj = zzdqd.zzv(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 32:
                    zzj = zzdqd.zzw(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 33:
                    zzj = zzdqd.zzu(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 34:
                    zzj = zzdqd.zzq(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += zzj;
                    j = 0;
                    break;
                case 35:
                    i7 = zzdqd.zzag((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 36:
                    i7 = zzdqd.zzaf((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 37:
                    i7 = zzdqd.zzy((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 38:
                    i7 = zzdqd.zzz((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 39:
                    i7 = zzdqd.zzac((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 40:
                    i7 = zzdqd.zzag((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 41:
                    i7 = zzdqd.zzaf((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 42:
                    i7 = zzdqd.zzah((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 43:
                    i7 = zzdqd.zzad((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 44:
                    i7 = zzdqd.zzab((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 45:
                    i7 = zzdqd.zzaf((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 46:
                    i7 = zzdqd.zzag((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 47:
                    i7 = zzdqd.zzae((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 48:
                    i7 = zzdqd.zzaa((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.zzhjy) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = zzdni.zzgd(i21);
                        i5 = zzdni.zzgf(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 49:
                    zzj = zzdqd.zzd(i21, (List) unsafe2.getObject(t2, j3), zzgr(i20));
                    i17 += zzj;
                    j = 0;
                    break;
                case 50:
                    zzj = this.zzhkg.zzb(i21, unsafe2.getObject(t2, j3), zzgs(i20));
                    i17 += zzj;
                    j = 0;
                    break;
                case 51:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzc(i21, 0.0d);
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 52:
                    if (zza(t2, i21, i20)) {
                        i4 = zzdni.zzb(i21, 0.0f);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 53:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzk(i21, zzi(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 54:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzl(i21, zzi(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 55:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzab(i21, zzh(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 56:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzn(i21, 0);
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 57:
                    if (zza(t2, i21, i20)) {
                        i4 = zzdni.zzae(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 58:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzj(i21, true);
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 59:
                    if (zza(t2, i21, i20)) {
                        Object object2 = unsafe2.getObject(t2, j3);
                        if (object2 instanceof zzdmr) {
                            zzj = zzdni.zzc(i21, (zzdmr) object2);
                        } else {
                            zzj = zzdni.zzg(i21, (String) object2);
                        }
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 60:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdqd.zzc(i21, unsafe2.getObject(t2, j3), zzgr(i20));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 61:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzc(i21, (zzdmr) unsafe2.getObject(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 62:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzac(i21, zzh(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 63:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzag(i21, zzh(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 64:
                    if (zza(t2, i21, i20)) {
                        i4 = zzdni.zzaf(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 65:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzo(i21, 0);
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 66:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzad(i21, zzh(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 67:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzm(i21, zzi(t2, j3));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                case 68:
                    if (zza(t2, i21, i20)) {
                        zzj = zzdni.zzc(i21, (zzdpk) unsafe2.getObject(t2, j3), zzgr(i20));
                        i17 += zzj;
                    }
                    j = 0;
                    break;
                default:
                    j = 0;
                    break;
            }
        }
        int zza = i17 + zza((zzdqt) this.zzhke, (Object) t2);
        return this.zzhjv ? zza + this.zzhkf.zzy(t2).zzaxj() : zza;
    }

    private static <UT, UB> int zza(zzdqt<UT, UB> zzdqt, T t) {
        return zzdqt.zzak(zzdqt.zzao(t));
    }

    private static <E> List<E> zze(Object obj, long j) {
        return (List) zzdqz.zzp(obj, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.ads.zzdro, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.ads.zzdro, int]
     candidates:
      com.google.android.gms.internal.ads.zzdqd.zzb(int, java.util.List<?>, com.google.android.gms.internal.ads.zzdro, com.google.android.gms.internal.ads.zzdqb):void
      com.google.android.gms.internal.ads.zzdqd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.ads.zzdro, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.ads.zzdro, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.ads.zzdro, int]
     candidates:
      com.google.android.gms.internal.ads.zzdqd.zza(int, int, java.lang.Object, com.google.android.gms.internal.ads.zzdqt):UB
      com.google.android.gms.internal.ads.zzdqd.zza(int, java.util.List<?>, com.google.android.gms.internal.ads.zzdro, com.google.android.gms.internal.ads.zzdqb):void
      com.google.android.gms.internal.ads.zzdqd.zza(com.google.android.gms.internal.ads.zzdpf, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.ads.zzdqd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.ads.zzdro, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0511  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x054f  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a27  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r14, com.google.android.gms.internal.ads.zzdro r15) throws java.io.IOException {
        /*
            r13 = this;
            int r0 = r15.zzawy()
            int r1 = com.google.android.gms.internal.ads.zzdob.zze.zzhhw
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x0527
            com.google.android.gms.internal.ads.zzdqt<?, ?> r0 = r13.zzhke
            zza(r0, r14, r15)
            boolean r0 = r13.zzhjv
            if (r0 == 0) goto L_0x0030
            com.google.android.gms.internal.ads.zzdnp<?> r0 = r13.zzhkf
            com.google.android.gms.internal.ads.zzdns r0 = r0.zzy(r14)
            boolean r1 = r0.isEmpty()
            if (r1 != 0) goto L_0x0030
            java.util.Iterator r0 = r0.descendingIterator()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0032
        L_0x0030:
            r0 = r3
            r1 = r0
        L_0x0032:
            int[] r7 = r13.zzhjq
            int r7 = r7.length
            int r7 = r7 + -3
        L_0x0037:
            if (r7 < 0) goto L_0x050f
            int r8 = r13.zzgu(r7)
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
        L_0x0041:
            if (r1 == 0) goto L_0x005f
            com.google.android.gms.internal.ads.zzdnp<?> r10 = r13.zzhkf
            int r10 = r10.zza(r1)
            if (r10 <= r9) goto L_0x005f
            com.google.android.gms.internal.ads.zzdnp<?> r10 = r13.zzhkf
            r10.zza(r15, r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x005d
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0041
        L_0x005d:
            r1 = r3
            goto L_0x0041
        L_0x005f:
            r10 = r8 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x04fc;
                case 1: goto L_0x04ec;
                case 2: goto L_0x04dc;
                case 3: goto L_0x04cc;
                case 4: goto L_0x04bc;
                case 5: goto L_0x04ac;
                case 6: goto L_0x049c;
                case 7: goto L_0x048b;
                case 8: goto L_0x047a;
                case 9: goto L_0x0465;
                case 10: goto L_0x0452;
                case 11: goto L_0x0441;
                case 12: goto L_0x0430;
                case 13: goto L_0x041f;
                case 14: goto L_0x040e;
                case 15: goto L_0x03fd;
                case 16: goto L_0x03ec;
                case 17: goto L_0x03d7;
                case 18: goto L_0x03c6;
                case 19: goto L_0x03b5;
                case 20: goto L_0x03a4;
                case 21: goto L_0x0393;
                case 22: goto L_0x0382;
                case 23: goto L_0x0371;
                case 24: goto L_0x0360;
                case 25: goto L_0x034f;
                case 26: goto L_0x033e;
                case 27: goto L_0x0329;
                case 28: goto L_0x0318;
                case 29: goto L_0x0307;
                case 30: goto L_0x02f6;
                case 31: goto L_0x02e5;
                case 32: goto L_0x02d4;
                case 33: goto L_0x02c3;
                case 34: goto L_0x02b2;
                case 35: goto L_0x02a1;
                case 36: goto L_0x0290;
                case 37: goto L_0x027f;
                case 38: goto L_0x026e;
                case 39: goto L_0x025d;
                case 40: goto L_0x024c;
                case 41: goto L_0x023b;
                case 42: goto L_0x022a;
                case 43: goto L_0x0219;
                case 44: goto L_0x0208;
                case 45: goto L_0x01f7;
                case 46: goto L_0x01e6;
                case 47: goto L_0x01d5;
                case 48: goto L_0x01c4;
                case 49: goto L_0x01af;
                case 50: goto L_0x01a4;
                case 51: goto L_0x0193;
                case 52: goto L_0x0182;
                case 53: goto L_0x0171;
                case 54: goto L_0x0160;
                case 55: goto L_0x014f;
                case 56: goto L_0x013e;
                case 57: goto L_0x012d;
                case 58: goto L_0x011c;
                case 59: goto L_0x010b;
                case 60: goto L_0x00f6;
                case 61: goto L_0x00e3;
                case 62: goto L_0x00d2;
                case 63: goto L_0x00c1;
                case 64: goto L_0x00b0;
                case 65: goto L_0x009f;
                case 66: goto L_0x008e;
                case 67: goto L_0x007d;
                case 68: goto L_0x0068;
                default: goto L_0x0066;
            }
        L_0x0066:
            goto L_0x050b
        L_0x0068:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            com.google.android.gms.internal.ads.zzdqb r10 = r13.zzgr(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050b
        L_0x007d:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzi(r9, r10)
            goto L_0x050b
        L_0x008e:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzz(r9, r8)
            goto L_0x050b
        L_0x009f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzq(r9, r10)
            goto L_0x050b
        L_0x00b0:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzah(r9, r8)
            goto L_0x050b
        L_0x00c1:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzai(r9, r8)
            goto L_0x050b
        L_0x00d2:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzy(r9, r8)
            goto L_0x050b
        L_0x00e3:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            com.google.android.gms.internal.ads.zzdmr r8 = (com.google.android.gms.internal.ads.zzdmr) r8
            r15.zza(r9, r8)
            goto L_0x050b
        L_0x00f6:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            com.google.android.gms.internal.ads.zzdqb r10 = r13.zzgr(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050b
        L_0x010b:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050b
        L_0x011c:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = zzj(r14, r10)
            r15.zzi(r9, r8)
            goto L_0x050b
        L_0x012d:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzaa(r9, r8)
            goto L_0x050b
        L_0x013e:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzj(r9, r10)
            goto L_0x050b
        L_0x014f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzx(r9, r8)
            goto L_0x050b
        L_0x0160:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzh(r9, r10)
            goto L_0x050b
        L_0x0171:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzp(r9, r10)
            goto L_0x050b
        L_0x0182:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = zzg(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050b
        L_0x0193:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = zzf(r14, r10)
            r15.zzb(r9, r10)
            goto L_0x050b
        L_0x01a4:
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            r13.zza(r15, r9, r8, r7)
            goto L_0x050b
        L_0x01af:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqb r10 = r13.zzgr(r7)
            com.google.android.gms.internal.ads.zzdqd.zzb(r9, r8, r15, r10)
            goto L_0x050b
        L_0x01c4:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zze(r9, r8, r15, r4)
            goto L_0x050b
        L_0x01d5:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzj(r9, r8, r15, r4)
            goto L_0x050b
        L_0x01e6:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzg(r9, r8, r15, r4)
            goto L_0x050b
        L_0x01f7:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzl(r9, r8, r15, r4)
            goto L_0x050b
        L_0x0208:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzm(r9, r8, r15, r4)
            goto L_0x050b
        L_0x0219:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzi(r9, r8, r15, r4)
            goto L_0x050b
        L_0x022a:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzn(r9, r8, r15, r4)
            goto L_0x050b
        L_0x023b:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzk(r9, r8, r15, r4)
            goto L_0x050b
        L_0x024c:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzf(r9, r8, r15, r4)
            goto L_0x050b
        L_0x025d:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzh(r9, r8, r15, r4)
            goto L_0x050b
        L_0x026e:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzd(r9, r8, r15, r4)
            goto L_0x050b
        L_0x027f:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzc(r9, r8, r15, r4)
            goto L_0x050b
        L_0x0290:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzb(r9, r8, r15, r4)
            goto L_0x050b
        L_0x02a1:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zza(r9, r8, r15, r4)
            goto L_0x050b
        L_0x02b2:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zze(r9, r8, r15, r5)
            goto L_0x050b
        L_0x02c3:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzj(r9, r8, r15, r5)
            goto L_0x050b
        L_0x02d4:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzg(r9, r8, r15, r5)
            goto L_0x050b
        L_0x02e5:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzl(r9, r8, r15, r5)
            goto L_0x050b
        L_0x02f6:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzm(r9, r8, r15, r5)
            goto L_0x050b
        L_0x0307:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzi(r9, r8, r15, r5)
            goto L_0x050b
        L_0x0318:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzb(r9, r8, r15)
            goto L_0x050b
        L_0x0329:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqb r10 = r13.zzgr(r7)
            com.google.android.gms.internal.ads.zzdqd.zza(r9, r8, r15, r10)
            goto L_0x050b
        L_0x033e:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zza(r9, r8, r15)
            goto L_0x050b
        L_0x034f:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzn(r9, r8, r15, r5)
            goto L_0x050b
        L_0x0360:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzk(r9, r8, r15, r5)
            goto L_0x050b
        L_0x0371:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzf(r9, r8, r15, r5)
            goto L_0x050b
        L_0x0382:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzh(r9, r8, r15, r5)
            goto L_0x050b
        L_0x0393:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzd(r9, r8, r15, r5)
            goto L_0x050b
        L_0x03a4:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzc(r9, r8, r15, r5)
            goto L_0x050b
        L_0x03b5:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zzb(r9, r8, r15, r5)
            goto L_0x050b
        L_0x03c6:
            int[] r9 = r13.zzhjq
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.ads.zzdqd.zza(r9, r8, r15, r5)
            goto L_0x050b
        L_0x03d7:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            com.google.android.gms.internal.ads.zzdqb r10 = r13.zzgr(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050b
        L_0x03ec:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r10)
            r15.zzi(r9, r10)
            goto L_0x050b
        L_0x03fd:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r10)
            r15.zzz(r9, r8)
            goto L_0x050b
        L_0x040e:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r10)
            r15.zzq(r9, r10)
            goto L_0x050b
        L_0x041f:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r10)
            r15.zzah(r9, r8)
            goto L_0x050b
        L_0x0430:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r10)
            r15.zzai(r9, r8)
            goto L_0x050b
        L_0x0441:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r10)
            r15.zzy(r9, r8)
            goto L_0x050b
        L_0x0452:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            com.google.android.gms.internal.ads.zzdmr r8 = (com.google.android.gms.internal.ads.zzdmr) r8
            r15.zza(r9, r8)
            goto L_0x050b
        L_0x0465:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            com.google.android.gms.internal.ads.zzdqb r10 = r13.zzgr(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050b
        L_0x047a:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050b
        L_0x048b:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = com.google.android.gms.internal.ads.zzdqz.zzm(r14, r10)
            r15.zzi(r9, r8)
            goto L_0x050b
        L_0x049c:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r10)
            r15.zzaa(r9, r8)
            goto L_0x050b
        L_0x04ac:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r10)
            r15.zzj(r9, r10)
            goto L_0x050b
        L_0x04bc:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r10)
            r15.zzx(r9, r8)
            goto L_0x050b
        L_0x04cc:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r10)
            r15.zzh(r9, r10)
            goto L_0x050b
        L_0x04dc:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r10)
            r15.zzp(r9, r10)
            goto L_0x050b
        L_0x04ec:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = com.google.android.gms.internal.ads.zzdqz.zzn(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050b
        L_0x04fc:
            boolean r10 = r13.zzd(r14, r7)
            if (r10 == 0) goto L_0x050b
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = com.google.android.gms.internal.ads.zzdqz.zzo(r14, r10)
            r15.zzb(r9, r10)
        L_0x050b:
            int r7 = r7 + -3
            goto L_0x0037
        L_0x050f:
            if (r1 == 0) goto L_0x0526
            com.google.android.gms.internal.ads.zzdnp<?> r14 = r13.zzhkf
            r14.zza(r15, r1)
            boolean r14 = r0.hasNext()
            if (r14 == 0) goto L_0x0524
            java.lang.Object r14 = r0.next()
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14
            r1 = r14
            goto L_0x050f
        L_0x0524:
            r1 = r3
            goto L_0x050f
        L_0x0526:
            return
        L_0x0527:
            boolean r0 = r13.zzhjx
            if (r0 == 0) goto L_0x0a42
            boolean r0 = r13.zzhjv
            if (r0 == 0) goto L_0x0546
            com.google.android.gms.internal.ads.zzdnp<?> r0 = r13.zzhkf
            com.google.android.gms.internal.ads.zzdns r0 = r0.zzy(r14)
            boolean r1 = r0.isEmpty()
            if (r1 != 0) goto L_0x0546
            java.util.Iterator r0 = r0.iterator()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0548
        L_0x0546:
            r0 = r3
            r1 = r0
        L_0x0548:
            int[] r7 = r13.zzhjq
            int r7 = r7.length
            r8 = r1
            r1 = 0
        L_0x054d:
            if (r1 >= r7) goto L_0x0a25
            int r9 = r13.zzgu(r1)
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
        L_0x0557:
            if (r8 == 0) goto L_0x0575
            com.google.android.gms.internal.ads.zzdnp<?> r11 = r13.zzhkf
            int r11 = r11.zza(r8)
            if (r11 > r10) goto L_0x0575
            com.google.android.gms.internal.ads.zzdnp<?> r11 = r13.zzhkf
            r11.zza(r15, r8)
            boolean r8 = r0.hasNext()
            if (r8 == 0) goto L_0x0573
            java.lang.Object r8 = r0.next()
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8
            goto L_0x0557
        L_0x0573:
            r8 = r3
            goto L_0x0557
        L_0x0575:
            r11 = r9 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0a12;
                case 1: goto L_0x0a02;
                case 2: goto L_0x09f2;
                case 3: goto L_0x09e2;
                case 4: goto L_0x09d2;
                case 5: goto L_0x09c2;
                case 6: goto L_0x09b2;
                case 7: goto L_0x09a1;
                case 8: goto L_0x0990;
                case 9: goto L_0x097b;
                case 10: goto L_0x0968;
                case 11: goto L_0x0957;
                case 12: goto L_0x0946;
                case 13: goto L_0x0935;
                case 14: goto L_0x0924;
                case 15: goto L_0x0913;
                case 16: goto L_0x0902;
                case 17: goto L_0x08ed;
                case 18: goto L_0x08dc;
                case 19: goto L_0x08cb;
                case 20: goto L_0x08ba;
                case 21: goto L_0x08a9;
                case 22: goto L_0x0898;
                case 23: goto L_0x0887;
                case 24: goto L_0x0876;
                case 25: goto L_0x0865;
                case 26: goto L_0x0854;
                case 27: goto L_0x083f;
                case 28: goto L_0x082e;
                case 29: goto L_0x081d;
                case 30: goto L_0x080c;
                case 31: goto L_0x07fb;
                case 32: goto L_0x07ea;
                case 33: goto L_0x07d9;
                case 34: goto L_0x07c8;
                case 35: goto L_0x07b7;
                case 36: goto L_0x07a6;
                case 37: goto L_0x0795;
                case 38: goto L_0x0784;
                case 39: goto L_0x0773;
                case 40: goto L_0x0762;
                case 41: goto L_0x0751;
                case 42: goto L_0x0740;
                case 43: goto L_0x072f;
                case 44: goto L_0x071e;
                case 45: goto L_0x070d;
                case 46: goto L_0x06fc;
                case 47: goto L_0x06eb;
                case 48: goto L_0x06da;
                case 49: goto L_0x06c5;
                case 50: goto L_0x06ba;
                case 51: goto L_0x06a9;
                case 52: goto L_0x0698;
                case 53: goto L_0x0687;
                case 54: goto L_0x0676;
                case 55: goto L_0x0665;
                case 56: goto L_0x0654;
                case 57: goto L_0x0643;
                case 58: goto L_0x0632;
                case 59: goto L_0x0621;
                case 60: goto L_0x060c;
                case 61: goto L_0x05f9;
                case 62: goto L_0x05e8;
                case 63: goto L_0x05d7;
                case 64: goto L_0x05c6;
                case 65: goto L_0x05b5;
                case 66: goto L_0x05a4;
                case 67: goto L_0x0593;
                case 68: goto L_0x057e;
                default: goto L_0x057c;
            }
        L_0x057c:
            goto L_0x0a21
        L_0x057e:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            com.google.android.gms.internal.ads.zzdqb r11 = r13.zzgr(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a21
        L_0x0593:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzi(r10, r11)
            goto L_0x0a21
        L_0x05a4:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzz(r10, r9)
            goto L_0x0a21
        L_0x05b5:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzq(r10, r11)
            goto L_0x0a21
        L_0x05c6:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzah(r10, r9)
            goto L_0x0a21
        L_0x05d7:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzai(r10, r9)
            goto L_0x0a21
        L_0x05e8:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzy(r10, r9)
            goto L_0x0a21
        L_0x05f9:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            com.google.android.gms.internal.ads.zzdmr r9 = (com.google.android.gms.internal.ads.zzdmr) r9
            r15.zza(r10, r9)
            goto L_0x0a21
        L_0x060c:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            com.google.android.gms.internal.ads.zzdqb r11 = r13.zzgr(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a21
        L_0x0621:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a21
        L_0x0632:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = zzj(r14, r11)
            r15.zzi(r10, r9)
            goto L_0x0a21
        L_0x0643:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzaa(r10, r9)
            goto L_0x0a21
        L_0x0654:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzj(r10, r11)
            goto L_0x0a21
        L_0x0665:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzx(r10, r9)
            goto L_0x0a21
        L_0x0676:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzh(r10, r11)
            goto L_0x0a21
        L_0x0687:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzp(r10, r11)
            goto L_0x0a21
        L_0x0698:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = zzg(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a21
        L_0x06a9:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = zzf(r14, r11)
            r15.zzb(r10, r11)
            goto L_0x0a21
        L_0x06ba:
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            r13.zza(r15, r10, r9, r1)
            goto L_0x0a21
        L_0x06c5:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqb r11 = r13.zzgr(r1)
            com.google.android.gms.internal.ads.zzdqd.zzb(r10, r9, r15, r11)
            goto L_0x0a21
        L_0x06da:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zze(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x06eb:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzj(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x06fc:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzg(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x070d:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzl(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x071e:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzm(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x072f:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzi(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x0740:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzn(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x0751:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzk(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x0762:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzf(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x0773:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzh(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x0784:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzd(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x0795:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzc(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x07a6:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzb(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x07b7:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zza(r10, r9, r15, r4)
            goto L_0x0a21
        L_0x07c8:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zze(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x07d9:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzj(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x07ea:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzg(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x07fb:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzl(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x080c:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzm(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x081d:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzi(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x082e:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzb(r10, r9, r15)
            goto L_0x0a21
        L_0x083f:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqb r11 = r13.zzgr(r1)
            com.google.android.gms.internal.ads.zzdqd.zza(r10, r9, r15, r11)
            goto L_0x0a21
        L_0x0854:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zza(r10, r9, r15)
            goto L_0x0a21
        L_0x0865:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzn(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x0876:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzk(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x0887:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzf(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x0898:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzh(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x08a9:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzd(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x08ba:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzc(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x08cb:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzb(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x08dc:
            int[] r10 = r13.zzhjq
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zza(r10, r9, r15, r5)
            goto L_0x0a21
        L_0x08ed:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            com.google.android.gms.internal.ads.zzdqb r11 = r13.zzgr(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a21
        L_0x0902:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r11)
            r15.zzi(r10, r11)
            goto L_0x0a21
        L_0x0913:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r11)
            r15.zzz(r10, r9)
            goto L_0x0a21
        L_0x0924:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r11)
            r15.zzq(r10, r11)
            goto L_0x0a21
        L_0x0935:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r11)
            r15.zzah(r10, r9)
            goto L_0x0a21
        L_0x0946:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r11)
            r15.zzai(r10, r9)
            goto L_0x0a21
        L_0x0957:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r11)
            r15.zzy(r10, r9)
            goto L_0x0a21
        L_0x0968:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            com.google.android.gms.internal.ads.zzdmr r9 = (com.google.android.gms.internal.ads.zzdmr) r9
            r15.zza(r10, r9)
            goto L_0x0a21
        L_0x097b:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            com.google.android.gms.internal.ads.zzdqb r11 = r13.zzgr(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a21
        L_0x0990:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.ads.zzdqz.zzp(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a21
        L_0x09a1:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = com.google.android.gms.internal.ads.zzdqz.zzm(r14, r11)
            r15.zzi(r10, r9)
            goto L_0x0a21
        L_0x09b2:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r11)
            r15.zzaa(r10, r9)
            goto L_0x0a21
        L_0x09c2:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r11)
            r15.zzj(r10, r11)
            goto L_0x0a21
        L_0x09d2:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.ads.zzdqz.zzk(r14, r11)
            r15.zzx(r10, r9)
            goto L_0x0a21
        L_0x09e2:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r11)
            r15.zzh(r10, r11)
            goto L_0x0a21
        L_0x09f2:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.ads.zzdqz.zzl(r14, r11)
            r15.zzp(r10, r11)
            goto L_0x0a21
        L_0x0a02:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = com.google.android.gms.internal.ads.zzdqz.zzn(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a21
        L_0x0a12:
            boolean r11 = r13.zzd(r14, r1)
            if (r11 == 0) goto L_0x0a21
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = com.google.android.gms.internal.ads.zzdqz.zzo(r14, r11)
            r15.zzb(r10, r11)
        L_0x0a21:
            int r1 = r1 + 3
            goto L_0x054d
        L_0x0a25:
            if (r8 == 0) goto L_0x0a3c
            com.google.android.gms.internal.ads.zzdnp<?> r1 = r13.zzhkf
            r1.zza(r15, r8)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0a3a
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            r8 = r1
            goto L_0x0a25
        L_0x0a3a:
            r8 = r3
            goto L_0x0a25
        L_0x0a3c:
            com.google.android.gms.internal.ads.zzdqt<?, ?> r0 = r13.zzhke
            zza(r0, r14, r15)
            return
        L_0x0a42:
            r13.zzb(r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Object, com.google.android.gms.internal.ads.zzdro):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.ads.zzdro, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.ads.zzdro, int]
     candidates:
      com.google.android.gms.internal.ads.zzdqd.zzb(int, java.util.List<?>, com.google.android.gms.internal.ads.zzdro, com.google.android.gms.internal.ads.zzdqb):void
      com.google.android.gms.internal.ads.zzdqd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.ads.zzdro, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.ads.zzdro, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.ads.zzdro, int]
     candidates:
      com.google.android.gms.internal.ads.zzdqd.zza(int, int, java.lang.Object, com.google.android.gms.internal.ads.zzdqt):UB
      com.google.android.gms.internal.ads.zzdqd.zza(int, java.util.List<?>, com.google.android.gms.internal.ads.zzdro, com.google.android.gms.internal.ads.zzdqb):void
      com.google.android.gms.internal.ads.zzdqd.zza(com.google.android.gms.internal.ads.zzdpf, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.ads.zzdqd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.ads.zzdro, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04b3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzb(T r19, com.google.android.gms.internal.ads.zzdro r20) throws java.io.IOException {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            boolean r3 = r0.zzhjv
            if (r3 == 0) goto L_0x0021
            com.google.android.gms.internal.ads.zzdnp<?> r3 = r0.zzhkf
            com.google.android.gms.internal.ads.zzdns r3 = r3.zzy(r1)
            boolean r5 = r3.isEmpty()
            if (r5 != 0) goto L_0x0021
            java.util.Iterator r3 = r3.iterator()
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            goto L_0x0023
        L_0x0021:
            r3 = 0
            r5 = 0
        L_0x0023:
            r6 = -1
            int[] r7 = r0.zzhjq
            int r7 = r7.length
            sun.misc.Unsafe r8 = com.google.android.gms.internal.ads.zzdpo.zzhjp
            r10 = r5
            r5 = 0
            r11 = 0
        L_0x002c:
            if (r5 >= r7) goto L_0x04ad
            int r12 = r0.zzgu(r5)
            int[] r13 = r0.zzhjq
            r14 = r13[r5]
            r15 = 267386880(0xff00000, float:2.3665827E-29)
            r15 = r15 & r12
            int r15 = r15 >>> 20
            boolean r4 = r0.zzhjx
            r16 = 1048575(0xfffff, float:1.469367E-39)
            if (r4 != 0) goto L_0x0062
            r4 = 17
            if (r15 > r4) goto L_0x0062
            int r4 = r5 + 2
            r4 = r13[r4]
            r13 = r4 & r16
            if (r13 == r6) goto L_0x0056
            r17 = r10
            long r9 = (long) r13
            int r11 = r8.getInt(r1, r9)
            goto L_0x0059
        L_0x0056:
            r17 = r10
            r13 = r6
        L_0x0059:
            int r4 = r4 >>> 20
            r6 = 1
            int r9 = r6 << r4
            r6 = r13
            r10 = r17
            goto L_0x0067
        L_0x0062:
            r17 = r10
            r10 = r17
            r9 = 0
        L_0x0067:
            if (r10 == 0) goto L_0x0086
            com.google.android.gms.internal.ads.zzdnp<?> r4 = r0.zzhkf
            int r4 = r4.zza(r10)
            if (r4 > r14) goto L_0x0086
            com.google.android.gms.internal.ads.zzdnp<?> r4 = r0.zzhkf
            r4.zza(r2, r10)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0084
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            r10 = r4
            goto L_0x0067
        L_0x0084:
            r10 = 0
            goto L_0x0067
        L_0x0086:
            r4 = r12 & r16
            long r12 = (long) r4
            switch(r15) {
                case 0: goto L_0x049d;
                case 1: goto L_0x0490;
                case 2: goto L_0x0483;
                case 3: goto L_0x0476;
                case 4: goto L_0x0469;
                case 5: goto L_0x045c;
                case 6: goto L_0x044f;
                case 7: goto L_0x0442;
                case 8: goto L_0x0434;
                case 9: goto L_0x0422;
                case 10: goto L_0x0412;
                case 11: goto L_0x0404;
                case 12: goto L_0x03f6;
                case 13: goto L_0x03e8;
                case 14: goto L_0x03da;
                case 15: goto L_0x03cc;
                case 16: goto L_0x03be;
                case 17: goto L_0x03ac;
                case 18: goto L_0x039c;
                case 19: goto L_0x038c;
                case 20: goto L_0x037c;
                case 21: goto L_0x036c;
                case 22: goto L_0x035c;
                case 23: goto L_0x034c;
                case 24: goto L_0x033c;
                case 25: goto L_0x032c;
                case 26: goto L_0x031d;
                case 27: goto L_0x030a;
                case 28: goto L_0x02fb;
                case 29: goto L_0x02eb;
                case 30: goto L_0x02db;
                case 31: goto L_0x02cb;
                case 32: goto L_0x02bb;
                case 33: goto L_0x02ab;
                case 34: goto L_0x029b;
                case 35: goto L_0x028b;
                case 36: goto L_0x027b;
                case 37: goto L_0x026b;
                case 38: goto L_0x025b;
                case 39: goto L_0x024b;
                case 40: goto L_0x023b;
                case 41: goto L_0x022b;
                case 42: goto L_0x021b;
                case 43: goto L_0x020b;
                case 44: goto L_0x01fb;
                case 45: goto L_0x01eb;
                case 46: goto L_0x01db;
                case 47: goto L_0x01cb;
                case 48: goto L_0x01bb;
                case 49: goto L_0x01a8;
                case 50: goto L_0x019f;
                case 51: goto L_0x0190;
                case 52: goto L_0x0181;
                case 53: goto L_0x0172;
                case 54: goto L_0x0163;
                case 55: goto L_0x0154;
                case 56: goto L_0x0145;
                case 57: goto L_0x0136;
                case 58: goto L_0x0127;
                case 59: goto L_0x0118;
                case 60: goto L_0x0105;
                case 61: goto L_0x00f5;
                case 62: goto L_0x00e7;
                case 63: goto L_0x00d9;
                case 64: goto L_0x00cb;
                case 65: goto L_0x00bd;
                case 66: goto L_0x00af;
                case 67: goto L_0x00a1;
                case 68: goto L_0x008f;
                default: goto L_0x008c;
            }
        L_0x008c:
            r15 = 0
            goto L_0x04a9
        L_0x008f:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.ads.zzdqb r9 = r0.zzgr(r5)
            r2.zzb(r14, r4, r9)
            goto L_0x008c
        L_0x00a1:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzi(r1, r12)
            r2.zzi(r14, r12)
            goto L_0x008c
        L_0x00af:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzh(r1, r12)
            r2.zzz(r14, r4)
            goto L_0x008c
        L_0x00bd:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzi(r1, r12)
            r2.zzq(r14, r12)
            goto L_0x008c
        L_0x00cb:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzh(r1, r12)
            r2.zzah(r14, r4)
            goto L_0x008c
        L_0x00d9:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzh(r1, r12)
            r2.zzai(r14, r4)
            goto L_0x008c
        L_0x00e7:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzh(r1, r12)
            r2.zzy(r14, r4)
            goto L_0x008c
        L_0x00f5:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.ads.zzdmr r4 = (com.google.android.gms.internal.ads.zzdmr) r4
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0105:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.ads.zzdqb r9 = r0.zzgr(r5)
            r2.zza(r14, r4, r9)
            goto L_0x008c
        L_0x0118:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            zza(r14, r4, r2)
            goto L_0x008c
        L_0x0127:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            boolean r4 = zzj(r1, r12)
            r2.zzi(r14, r4)
            goto L_0x008c
        L_0x0136:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzh(r1, r12)
            r2.zzaa(r14, r4)
            goto L_0x008c
        L_0x0145:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzi(r1, r12)
            r2.zzj(r14, r12)
            goto L_0x008c
        L_0x0154:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzh(r1, r12)
            r2.zzx(r14, r4)
            goto L_0x008c
        L_0x0163:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzi(r1, r12)
            r2.zzh(r14, r12)
            goto L_0x008c
        L_0x0172:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzi(r1, r12)
            r2.zzp(r14, r12)
            goto L_0x008c
        L_0x0181:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            float r4 = zzg(r1, r12)
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0190:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            double r12 = zzf(r1, r12)
            r2.zzb(r14, r12)
            goto L_0x008c
        L_0x019f:
            java.lang.Object r4 = r8.getObject(r1, r12)
            r0.zza(r2, r14, r4, r5)
            goto L_0x008c
        L_0x01a8:
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqb r12 = r0.zzgr(r5)
            com.google.android.gms.internal.ads.zzdqd.zzb(r4, r9, r2, r12)
            goto L_0x008c
        L_0x01bb:
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 1
            com.google.android.gms.internal.ads.zzdqd.zze(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01cb:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzj(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01db:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzg(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01eb:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzl(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01fb:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzm(r4, r9, r2, r14)
            goto L_0x008c
        L_0x020b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzi(r4, r9, r2, r14)
            goto L_0x008c
        L_0x021b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzn(r4, r9, r2, r14)
            goto L_0x008c
        L_0x022b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzk(r4, r9, r2, r14)
            goto L_0x008c
        L_0x023b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzf(r4, r9, r2, r14)
            goto L_0x008c
        L_0x024b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzh(r4, r9, r2, r14)
            goto L_0x008c
        L_0x025b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzd(r4, r9, r2, r14)
            goto L_0x008c
        L_0x026b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzc(r4, r9, r2, r14)
            goto L_0x008c
        L_0x027b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzb(r4, r9, r2, r14)
            goto L_0x008c
        L_0x028b:
            r14 = 1
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zza(r4, r9, r2, r14)
            goto L_0x008c
        L_0x029b:
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 0
            com.google.android.gms.internal.ads.zzdqd.zze(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02ab:
            r14 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzj(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02bb:
            r14 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzg(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02cb:
            r14 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzl(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02db:
            r14 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzm(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02eb:
            r14 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzi(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02fb:
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzb(r4, r9, r2)
            goto L_0x008c
        L_0x030a:
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqb r12 = r0.zzgr(r5)
            com.google.android.gms.internal.ads.zzdqd.zza(r4, r9, r2, r12)
            goto L_0x008c
        L_0x031d:
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zza(r4, r9, r2)
            goto L_0x008c
        L_0x032c:
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r15 = 0
            com.google.android.gms.internal.ads.zzdqd.zzn(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x033c:
            r15 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzk(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x034c:
            r15 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzf(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x035c:
            r15 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzh(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x036c:
            r15 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzd(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x037c:
            r15 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzc(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x038c:
            r15 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zzb(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x039c:
            r15 = 0
            int[] r4 = r0.zzhjq
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.ads.zzdqd.zza(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x03ac:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.ads.zzdqb r9 = r0.zzgr(r5)
            r2.zzb(r14, r4, r9)
            goto L_0x04a9
        L_0x03be:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzi(r14, r12)
            goto L_0x04a9
        L_0x03cc:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzz(r14, r4)
            goto L_0x04a9
        L_0x03da:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzq(r14, r12)
            goto L_0x04a9
        L_0x03e8:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzah(r14, r4)
            goto L_0x04a9
        L_0x03f6:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzai(r14, r4)
            goto L_0x04a9
        L_0x0404:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzy(r14, r4)
            goto L_0x04a9
        L_0x0412:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.ads.zzdmr r4 = (com.google.android.gms.internal.ads.zzdmr) r4
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x0422:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.ads.zzdqb r9 = r0.zzgr(r5)
            r2.zza(r14, r4, r9)
            goto L_0x04a9
        L_0x0434:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            zza(r14, r4, r2)
            goto L_0x04a9
        L_0x0442:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            boolean r4 = com.google.android.gms.internal.ads.zzdqz.zzm(r1, r12)
            r2.zzi(r14, r4)
            goto L_0x04a9
        L_0x044f:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzaa(r14, r4)
            goto L_0x04a9
        L_0x045c:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzj(r14, r12)
            goto L_0x04a9
        L_0x0469:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzx(r14, r4)
            goto L_0x04a9
        L_0x0476:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzh(r14, r12)
            goto L_0x04a9
        L_0x0483:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzp(r14, r12)
            goto L_0x04a9
        L_0x0490:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            float r4 = com.google.android.gms.internal.ads.zzdqz.zzn(r1, r12)
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x049d:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            double r12 = com.google.android.gms.internal.ads.zzdqz.zzo(r1, r12)
            r2.zzb(r14, r12)
        L_0x04a9:
            int r5 = r5 + 3
            goto L_0x002c
        L_0x04ad:
            r17 = r10
            r4 = r17
        L_0x04b1:
            if (r4 == 0) goto L_0x04c7
            com.google.android.gms.internal.ads.zzdnp<?> r5 = r0.zzhkf
            r5.zza(r2, r4)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x04c5
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            goto L_0x04b1
        L_0x04c5:
            r4 = 0
            goto L_0x04b1
        L_0x04c7:
            com.google.android.gms.internal.ads.zzdqt<?, ?> r3 = r0.zzhke
            zza(r3, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.zzb(java.lang.Object, com.google.android.gms.internal.ads.zzdro):void");
    }

    private final <K, V> void zza(zzdro zzdro, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzdro.zza(i, this.zzhkg.zzaj(zzgs(i2)), this.zzhkg.zzaf(obj));
        }
    }

    private static <UT, UB> void zza(zzdqt<UT, UB> zzdqt, T t, zzdro zzdro) throws IOException {
        zzdqt.zza(zzdqt.zzao(t), zzdro);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.ads.zzdpo.zze(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:148|149|(1:151)|152|(5:174|154|(2:157|155)|256|(2:159|264)(1:265))(1:252)) */
    /* JADX WARNING: Code restructure failed: missing block: B:149:?, code lost:
        r7.zza(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x05a0, code lost:
        if (r10 == null) goto L_0x05a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x05a2, code lost:
        r10 = r7.zzap(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x05ab, code lost:
        if (r7.zza(r10, r14) == false) goto L_0x05ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x05ad, code lost:
        r14 = r12.zzhka;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x05b1, code lost:
        if (r14 < r12.zzhkb) goto L_0x05b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x05b3, code lost:
        r10 = zza(r13, r12.zzhjz[r14], r10, r7);
        r14 = r14 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x05be, code lost:
        if (r10 != null) goto L_0x05c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x05c0, code lost:
        r7.zzg(r13, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:?, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:148:0x059d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r13, com.google.android.gms.internal.ads.zzdqa r14, com.google.android.gms.internal.ads.zzdno r15) throws java.io.IOException {
        /*
            r12 = this;
            if (r15 == 0) goto L_0x05dc
            com.google.android.gms.internal.ads.zzdqt<?, ?> r7 = r12.zzhke
            com.google.android.gms.internal.ads.zzdnp<?> r8 = r12.zzhkf
            r9 = 0
            r0 = r9
            r10 = r0
        L_0x0009:
            int r1 = r14.zzaws()     // Catch:{ all -> 0x05c4 }
            int r2 = r12.zzgx(r1)     // Catch:{ all -> 0x05c4 }
            if (r2 >= 0) goto L_0x0077
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r1 != r2) goto L_0x002f
            int r14 = r12.zzhka
        L_0x001a:
            int r15 = r12.zzhkb
            if (r14 >= r15) goto L_0x0029
            int[] r15 = r12.zzhjz
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x001a
        L_0x0029:
            if (r10 == 0) goto L_0x002e
            r7.zzg(r13, r10)
        L_0x002e:
            return
        L_0x002f:
            boolean r2 = r12.zzhjv     // Catch:{ all -> 0x05c4 }
            if (r2 != 0) goto L_0x0035
            r2 = r9
            goto L_0x003c
        L_0x0035:
            com.google.android.gms.internal.ads.zzdpk r2 = r12.zzhju     // Catch:{ all -> 0x05c4 }
            java.lang.Object r1 = r8.zza(r15, r2, r1)     // Catch:{ all -> 0x05c4 }
            r2 = r1
        L_0x003c:
            if (r2 == 0) goto L_0x0051
            if (r0 != 0) goto L_0x0044
            com.google.android.gms.internal.ads.zzdns r0 = r8.zzz(r13)     // Catch:{ all -> 0x05c4 }
        L_0x0044:
            r11 = r0
            r0 = r8
            r1 = r14
            r3 = r15
            r4 = r11
            r5 = r10
            r6 = r7
            java.lang.Object r10 = r0.zza(r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x05c4 }
            r0 = r11
            goto L_0x0009
        L_0x0051:
            r7.zza(r14)     // Catch:{ all -> 0x05c4 }
            if (r10 != 0) goto L_0x005a
            java.lang.Object r10 = r7.zzap(r13)     // Catch:{ all -> 0x05c4 }
        L_0x005a:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ all -> 0x05c4 }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzhka
        L_0x0062:
            int r15 = r12.zzhkb
            if (r14 >= r15) goto L_0x0071
            int[] r15 = r12.zzhjz
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x0062
        L_0x0071:
            if (r10 == 0) goto L_0x0076
            r7.zzg(r13, r10)
        L_0x0076:
            return
        L_0x0077:
            int r3 = r12.zzgu(r2)     // Catch:{ all -> 0x05c4 }
            r4 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r3
            int r4 = r4 >>> 20
            r5 = 1048575(0xfffff, float:1.469367E-39)
            switch(r4) {
                case 0: goto L_0x0571;
                case 1: goto L_0x0562;
                case 2: goto L_0x0553;
                case 3: goto L_0x0544;
                case 4: goto L_0x0535;
                case 5: goto L_0x0526;
                case 6: goto L_0x0517;
                case 7: goto L_0x0508;
                case 8: goto L_0x0500;
                case 9: goto L_0x04cf;
                case 10: goto L_0x04c0;
                case 11: goto L_0x04b1;
                case 12: goto L_0x048f;
                case 13: goto L_0x0480;
                case 14: goto L_0x0471;
                case 15: goto L_0x0462;
                case 16: goto L_0x0453;
                case 17: goto L_0x0422;
                case 18: goto L_0x0414;
                case 19: goto L_0x0406;
                case 20: goto L_0x03f8;
                case 21: goto L_0x03ea;
                case 22: goto L_0x03dc;
                case 23: goto L_0x03ce;
                case 24: goto L_0x03c0;
                case 25: goto L_0x03b2;
                case 26: goto L_0x0390;
                case 27: goto L_0x037e;
                case 28: goto L_0x0370;
                case 29: goto L_0x0362;
                case 30: goto L_0x034d;
                case 31: goto L_0x033f;
                case 32: goto L_0x0331;
                case 33: goto L_0x0323;
                case 34: goto L_0x0315;
                case 35: goto L_0x0307;
                case 36: goto L_0x02f9;
                case 37: goto L_0x02eb;
                case 38: goto L_0x02dd;
                case 39: goto L_0x02cf;
                case 40: goto L_0x02c1;
                case 41: goto L_0x02b3;
                case 42: goto L_0x02a5;
                case 43: goto L_0x0297;
                case 44: goto L_0x0282;
                case 45: goto L_0x0274;
                case 46: goto L_0x0266;
                case 47: goto L_0x0258;
                case 48: goto L_0x024a;
                case 49: goto L_0x0238;
                case 50: goto L_0x01f6;
                case 51: goto L_0x01e4;
                case 52: goto L_0x01d2;
                case 53: goto L_0x01c0;
                case 54: goto L_0x01ae;
                case 55: goto L_0x019c;
                case 56: goto L_0x018a;
                case 57: goto L_0x0178;
                case 58: goto L_0x0166;
                case 59: goto L_0x015e;
                case 60: goto L_0x012d;
                case 61: goto L_0x011f;
                case 62: goto L_0x010d;
                case 63: goto L_0x00e8;
                case 64: goto L_0x00d6;
                case 65: goto L_0x00c4;
                case 66: goto L_0x00b2;
                case 67: goto L_0x00a0;
                case 68: goto L_0x008e;
                default: goto L_0x0086;
            }
        L_0x0086:
            if (r10 != 0) goto L_0x0580
            java.lang.Object r10 = r7.zzazy()     // Catch:{ zzdol -> 0x059d }
            goto L_0x0580
        L_0x008e:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r5 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r5 = r14.zzb(r5, r15)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x00a0:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzawi()     // Catch:{ zzdol -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x00b2:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            int r5 = r14.zzawh()     // Catch:{ zzdol -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x00c4:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzawg()     // Catch:{ zzdol -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x00d6:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            int r5 = r14.zzawf()     // Catch:{ zzdol -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x00e8:
            int r4 = r14.zzawe()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdog r6 = r12.zzgt(r2)     // Catch:{ zzdol -> 0x059d }
            if (r6 == 0) goto L_0x00ff
            boolean r6 = r6.zzf(r4)     // Catch:{ zzdol -> 0x059d }
            if (r6 == 0) goto L_0x00f9
            goto L_0x00ff
        L_0x00f9:
            java.lang.Object r10 = com.google.android.gms.internal.ads.zzdqd.zza(r1, r4, r10, r7)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x00ff:
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzdol -> 0x059d }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r5, r3)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x010d:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            int r5 = r14.zzawd()     // Catch:{ zzdol -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x011f:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdmr r5 = r14.zzawc()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x012d:
            boolean r4 = r12.zza(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            if (r4 == 0) goto L_0x0149
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdqz.zzp(r13, r3)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r6 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r6 = r14.zza(r6, r15)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdod.zzb(r5, r6)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0159
        L_0x0149:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r5 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r5 = r14.zza(r5, r15)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
        L_0x0159:
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x015e:
            r12.zza(r13, r3, r14)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0166:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            boolean r5 = r14.zzawa()     // Catch:{ zzdol -> 0x059d }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0178:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            int r5 = r14.zzavz()     // Catch:{ zzdol -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x018a:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzavy()     // Catch:{ zzdol -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x019c:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            int r5 = r14.zzavx()     // Catch:{ zzdol -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x01ae:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzavv()     // Catch:{ zzdol -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x01c0:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzavw()     // Catch:{ zzdol -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x01d2:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            float r5 = r14.readFloat()     // Catch:{ zzdol -> 0x059d }
            java.lang.Float r5 = java.lang.Float.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x01e4:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzdol -> 0x059d }
            double r5 = r14.readDouble()     // Catch:{ zzdol -> 0x059d }
            java.lang.Double r5 = java.lang.Double.valueOf(r5)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x01f6:
            java.lang.Object r1 = r12.zzgs(r2)     // Catch:{ zzdol -> 0x059d }
            int r2 = r12.zzgu(r2)     // Catch:{ zzdol -> 0x059d }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r4 = com.google.android.gms.internal.ads.zzdqz.zzp(r13, r2)     // Catch:{ zzdol -> 0x059d }
            if (r4 != 0) goto L_0x0210
            com.google.android.gms.internal.ads.zzdpf r4 = r12.zzhkg     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r4 = r4.zzai(r1)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r2, r4)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0227
        L_0x0210:
            com.google.android.gms.internal.ads.zzdpf r5 = r12.zzhkg     // Catch:{ zzdol -> 0x059d }
            boolean r5 = r5.zzag(r4)     // Catch:{ zzdol -> 0x059d }
            if (r5 == 0) goto L_0x0227
            com.google.android.gms.internal.ads.zzdpf r5 = r12.zzhkg     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r5 = r5.zzai(r1)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdpf r6 = r12.zzhkg     // Catch:{ zzdol -> 0x059d }
            r6.zzc(r5, r4)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r2, r5)     // Catch:{ zzdol -> 0x059d }
            r4 = r5
        L_0x0227:
            com.google.android.gms.internal.ads.zzdpf r2 = r12.zzhkg     // Catch:{ zzdol -> 0x059d }
            java.util.Map r2 = r2.zzae(r4)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdpf r3 = r12.zzhkg     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdpd r1 = r3.zzaj(r1)     // Catch:{ zzdol -> 0x059d }
            r14.zza(r2, r1, r15)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0238:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r1 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdou r2 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            java.util.List r2 = r2.zza(r13, r3)     // Catch:{ zzdol -> 0x059d }
            r14.zzb(r2, r1, r15)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x024a:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzx(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0258:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzw(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0266:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzv(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0274:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzu(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0282:
            com.google.android.gms.internal.ads.zzdou r4 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzdol -> 0x059d }
            java.util.List r3 = r4.zza(r13, r5)     // Catch:{ zzdol -> 0x059d }
            r14.zzt(r3)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdog r2 = r12.zzgt(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r10 = com.google.android.gms.internal.ads.zzdqd.zza(r1, r3, r2, r10, r7)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0297:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzs(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x02a5:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzp(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x02b3:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzo(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x02c1:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzn(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x02cf:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzm(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x02dd:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzk(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x02eb:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzl(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x02f9:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzj(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0307:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzi(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0315:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzx(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0323:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzw(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0331:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzv(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x033f:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzu(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x034d:
            com.google.android.gms.internal.ads.zzdou r4 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzdol -> 0x059d }
            java.util.List r3 = r4.zza(r13, r5)     // Catch:{ zzdol -> 0x059d }
            r14.zzt(r3)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdog r2 = r12.zzgt(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r10 = com.google.android.gms.internal.ads.zzdqd.zza(r1, r3, r2, r10, r7)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0362:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzs(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0370:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzr(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x037e:
            com.google.android.gms.internal.ads.zzdqb r1 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdou r4 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            java.util.List r2 = r4.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zza(r2, r1, r15)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0390:
            boolean r1 = zzgw(r3)     // Catch:{ zzdol -> 0x059d }
            if (r1 == 0) goto L_0x03a4
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzq(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x03a4:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.readStringList(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x03b2:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzp(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x03c0:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzo(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x03ce:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzn(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x03dc:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzm(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x03ea:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzk(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x03f8:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzl(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0406:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzj(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0414:
            com.google.android.gms.internal.ads.zzdou r1 = r12.zzhkd     // Catch:{ zzdol -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzdol -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzdol -> 0x059d }
            r14.zzi(r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0422:
            boolean r1 = r12.zzd(r13, r2)     // Catch:{ zzdol -> 0x059d }
            if (r1 == 0) goto L_0x0440
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.ads.zzdqz.zzp(r13, r3)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r2 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r2 = r14.zzb(r2, r15)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.ads.zzdod.zzb(r1, r2)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0440:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r1 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r1 = r14.zzb(r1, r15)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0453:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzawi()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0462:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            int r1 = r14.zzawh()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zzb(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0471:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzawg()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0480:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            int r1 = r14.zzawf()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zzb(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x048f:
            int r4 = r14.zzawe()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdog r6 = r12.zzgt(r2)     // Catch:{ zzdol -> 0x059d }
            if (r6 == 0) goto L_0x04a6
            boolean r6 = r6.zzf(r4)     // Catch:{ zzdol -> 0x059d }
            if (r6 == 0) goto L_0x04a0
            goto L_0x04a6
        L_0x04a0:
            java.lang.Object r10 = com.google.android.gms.internal.ads.zzdqd.zza(r1, r4, r10, r7)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x04a6:
            r1 = r3 & r5
            long r5 = (long) r1     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zzb(r13, r5, r4)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x04b1:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            int r1 = r14.zzawd()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zzb(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x04c0:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdmr r1 = r14.zzawc()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x04cf:
            boolean r1 = r12.zzd(r13, r2)     // Catch:{ zzdol -> 0x059d }
            if (r1 == 0) goto L_0x04ed
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.ads.zzdqz.zzp(r13, r3)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r2 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r2 = r14.zza(r2, r15)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.ads.zzdod.zzb(r1, r2)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x04ed:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqb r1 = r12.zzgr(r2)     // Catch:{ zzdol -> 0x059d }
            java.lang.Object r1 = r14.zza(r1, r15)     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0500:
            r12.zza(r13, r3, r14)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0508:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            boolean r1 = r14.zzawa()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0517:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            int r1 = r14.zzavz()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zzb(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0526:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzavy()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0535:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            int r1 = r14.zzavx()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zzb(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0544:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzavv()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0553:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            long r5 = r14.zzavw()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0562:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            float r1 = r14.readFloat()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r1)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0571:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzdol -> 0x059d }
            double r5 = r14.readDouble()     // Catch:{ zzdol -> 0x059d }
            com.google.android.gms.internal.ads.zzdqz.zza(r13, r3, r5)     // Catch:{ zzdol -> 0x059d }
            r12.zze(r13, r2)     // Catch:{ zzdol -> 0x059d }
            goto L_0x0009
        L_0x0580:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ zzdol -> 0x059d }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzhka
        L_0x0588:
            int r15 = r12.zzhkb
            if (r14 >= r15) goto L_0x0597
            int[] r15 = r12.zzhjz
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x0588
        L_0x0597:
            if (r10 == 0) goto L_0x059c
            r7.zzg(r13, r10)
        L_0x059c:
            return
        L_0x059d:
            r7.zza(r14)     // Catch:{ all -> 0x05c4 }
            if (r10 != 0) goto L_0x05a7
            java.lang.Object r1 = r7.zzap(r13)     // Catch:{ all -> 0x05c4 }
            r10 = r1
        L_0x05a7:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ all -> 0x05c4 }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzhka
        L_0x05af:
            int r15 = r12.zzhkb
            if (r14 >= r15) goto L_0x05be
            int[] r15 = r12.zzhjz
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x05af
        L_0x05be:
            if (r10 == 0) goto L_0x05c3
            r7.zzg(r13, r10)
        L_0x05c3:
            return
        L_0x05c4:
            r14 = move-exception
            int r15 = r12.zzhka
        L_0x05c7:
            int r0 = r12.zzhkb
            if (r15 >= r0) goto L_0x05d6
            int[] r0 = r12.zzhjz
            r0 = r0[r15]
            java.lang.Object r10 = r12.zza(r13, r0, r10, r7)
            int r15 = r15 + 1
            goto L_0x05c7
        L_0x05d6:
            if (r10 == 0) goto L_0x05db
            r7.zzg(r13, r10)
        L_0x05db:
            throw r14
        L_0x05dc:
            java.lang.NullPointerException r13 = new java.lang.NullPointerException
            r13.<init>()
            goto L_0x05e3
        L_0x05e2:
            throw r13
        L_0x05e3:
            goto L_0x05e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Object, com.google.android.gms.internal.ads.zzdqa, com.google.android.gms.internal.ads.zzdno):void");
    }

    private static zzdqu zzal(Object obj) {
        zzdob zzdob = (zzdob) obj;
        zzdqu zzdqu = zzdob.zzhhd;
        if (zzdqu != zzdqu.zzazz()) {
            return zzdqu;
        }
        zzdqu zzbaa = zzdqu.zzbaa();
        zzdob.zzhhd = zzbaa;
        return zzbaa;
    }

    private static int zza(byte[] bArr, int i, int i2, zzdri zzdri, Class<?> cls, zzdmo zzdmo) throws IOException {
        switch (zzdpp.zzhdm[zzdri.ordinal()]) {
            case 1:
                int zzb = zzdmn.zzb(bArr, i, zzdmo);
                zzdmo.zzhcn = Boolean.valueOf(zzdmo.zzhcm != 0);
                return zzb;
            case 2:
                return zzdmn.zze(bArr, i, zzdmo);
            case 3:
                zzdmo.zzhcn = Double.valueOf(zzdmn.zzh(bArr, i));
                return i + 8;
            case 4:
            case 5:
                zzdmo.zzhcn = Integer.valueOf(zzdmn.zzf(bArr, i));
                return i + 4;
            case 6:
            case 7:
                zzdmo.zzhcn = Long.valueOf(zzdmn.zzg(bArr, i));
                return i + 8;
            case 8:
                zzdmo.zzhcn = Float.valueOf(zzdmn.zzi(bArr, i));
                return i + 4;
            case 9:
            case 10:
            case 11:
                int zza = zzdmn.zza(bArr, i, zzdmo);
                zzdmo.zzhcn = Integer.valueOf(zzdmo.zzhcl);
                return zza;
            case 12:
            case 13:
                int zzb2 = zzdmn.zzb(bArr, i, zzdmo);
                zzdmo.zzhcn = Long.valueOf(zzdmo.zzhcm);
                return zzb2;
            case 14:
                return zzdmn.zza(zzdpx.zzazg().zzg(cls), bArr, i, i2, zzdmo);
            case 15:
                int zza2 = zzdmn.zza(bArr, i, zzdmo);
                zzdmo.zzhcn = Integer.valueOf(zzdnd.zzft(zzdmo.zzhcl));
                return zza2;
            case 16:
                int zzb3 = zzdmn.zzb(bArr, i, zzdmo);
                zzdmo.zzhcn = Long.valueOf(zzdnd.zzfi(zzdmo.zzhcm));
                return zzb3;
            case 17:
                return zzdmn.zzd(bArr, i, zzdmo);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0422 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01eb  */
    private final int zza(T r17, byte[] r18, int r19, int r20, int r21, int r22, int r23, int r24, long r25, int r27, long r28, com.google.android.gms.internal.ads.zzdmo r30) throws java.io.IOException {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r2 = r21
            r6 = r23
            r8 = r24
            r9 = r28
            r7 = r30
            sun.misc.Unsafe r11 = com.google.android.gms.internal.ads.zzdpo.zzhjp
            java.lang.Object r11 = r11.getObject(r1, r9)
            com.google.android.gms.internal.ads.zzdoj r11 = (com.google.android.gms.internal.ads.zzdoj) r11
            boolean r12 = r11.zzavi()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r11.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            com.google.android.gms.internal.ads.zzdoj r11 = r11.zzfl(r12)
            sun.misc.Unsafe r12 = com.google.android.gms.internal.ads.zzdpo.zzhjp
            r12.putObject(r1, r9, r11)
        L_0x0036:
            r9 = 5
            r14 = 0
            r10 = 2
            switch(r27) {
                case 18: goto L_0x03e4;
                case 19: goto L_0x03a6;
                case 20: goto L_0x0365;
                case 21: goto L_0x0365;
                case 22: goto L_0x034b;
                case 23: goto L_0x030c;
                case 24: goto L_0x02cd;
                case 25: goto L_0x0276;
                case 26: goto L_0x01c3;
                case 27: goto L_0x01a9;
                case 28: goto L_0x0151;
                case 29: goto L_0x034b;
                case 30: goto L_0x0119;
                case 31: goto L_0x02cd;
                case 32: goto L_0x030c;
                case 33: goto L_0x00cc;
                case 34: goto L_0x007f;
                case 35: goto L_0x03e4;
                case 36: goto L_0x03a6;
                case 37: goto L_0x0365;
                case 38: goto L_0x0365;
                case 39: goto L_0x034b;
                case 40: goto L_0x030c;
                case 41: goto L_0x02cd;
                case 42: goto L_0x0276;
                case 43: goto L_0x034b;
                case 44: goto L_0x0119;
                case 45: goto L_0x02cd;
                case 46: goto L_0x030c;
                case 47: goto L_0x00cc;
                case 48: goto L_0x007f;
                case 49: goto L_0x003f;
                default: goto L_0x003d;
            }
        L_0x003d:
            goto L_0x0422
        L_0x003f:
            r1 = 3
            if (r6 != r1) goto L_0x0422
            com.google.android.gms.internal.ads.zzdqb r1 = r0.zzgr(r8)
            r6 = r2 & -8
            r6 = r6 | 4
            r22 = r1
            r23 = r18
            r24 = r19
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.zzhcn
            r11.add(r8)
        L_0x005f:
            if (r4 >= r5) goto L_0x0422
            int r8 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r9 = r7.zzhcl
            if (r2 != r9) goto L_0x0422
            r22 = r1
            r23 = r18
            r24 = r8
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.zzhcn
            r11.add(r8)
            goto L_0x005f
        L_0x007f:
            if (r6 != r10) goto L_0x00a3
            com.google.android.gms.internal.ads.zzdoy r11 = (com.google.android.gms.internal.ads.zzdoy) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r2 = r7.zzhcl
            int r2 = r2 + r1
        L_0x008a:
            if (r1 >= r2) goto L_0x009a
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r1, r7)
            long r4 = r7.zzhcm
            long r4 = com.google.android.gms.internal.ads.zzdnd.zzfi(r4)
            r11.zzfu(r4)
            goto L_0x008a
        L_0x009a:
            if (r1 != r2) goto L_0x009e
            goto L_0x0423
        L_0x009e:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x00a3:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.ads.zzdoy r11 = (com.google.android.gms.internal.ads.zzdoy) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r4, r7)
            long r8 = r7.zzhcm
            long r8 = com.google.android.gms.internal.ads.zzdnd.zzfi(r8)
            r11.zzfu(r8)
        L_0x00b4:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r4, r7)
            long r8 = r7.zzhcm
            long r8 = com.google.android.gms.internal.ads.zzdnd.zzfi(r8)
            r11.zzfu(r8)
            goto L_0x00b4
        L_0x00cc:
            if (r6 != r10) goto L_0x00f0
            com.google.android.gms.internal.ads.zzdoc r11 = (com.google.android.gms.internal.ads.zzdoc) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r2 = r7.zzhcl
            int r2 = r2 + r1
        L_0x00d7:
            if (r1 >= r2) goto L_0x00e7
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r4 = r7.zzhcl
            int r4 = com.google.android.gms.internal.ads.zzdnd.zzft(r4)
            r11.zzgp(r4)
            goto L_0x00d7
        L_0x00e7:
            if (r1 != r2) goto L_0x00eb
            goto L_0x0423
        L_0x00eb:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x00f0:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.ads.zzdoc r11 = (com.google.android.gms.internal.ads.zzdoc) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r4 = r7.zzhcl
            int r4 = com.google.android.gms.internal.ads.zzdnd.zzft(r4)
            r11.zzgp(r4)
        L_0x0101:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r4 = r7.zzhcl
            int r4 = com.google.android.gms.internal.ads.zzdnd.zzft(r4)
            r11.zzgp(r4)
            goto L_0x0101
        L_0x0119:
            if (r6 != r10) goto L_0x0120
            int r2 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r11, r7)
            goto L_0x0131
        L_0x0120:
            if (r6 != 0) goto L_0x0422
            r2 = r21
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r11
            r7 = r30
            int r2 = com.google.android.gms.internal.ads.zzdmn.zza(r2, r3, r4, r5, r6, r7)
        L_0x0131:
            com.google.android.gms.internal.ads.zzdob r1 = (com.google.android.gms.internal.ads.zzdob) r1
            com.google.android.gms.internal.ads.zzdqu r3 = r1.zzhhd
            com.google.android.gms.internal.ads.zzdqu r4 = com.google.android.gms.internal.ads.zzdqu.zzazz()
            if (r3 != r4) goto L_0x013c
            r3 = 0
        L_0x013c:
            com.google.android.gms.internal.ads.zzdog r4 = r0.zzgt(r8)
            com.google.android.gms.internal.ads.zzdqt<?, ?> r5 = r0.zzhke
            r6 = r22
            java.lang.Object r3 = com.google.android.gms.internal.ads.zzdqd.zza(r6, r11, r4, r3, r5)
            com.google.android.gms.internal.ads.zzdqu r3 = (com.google.android.gms.internal.ads.zzdqu) r3
            if (r3 == 0) goto L_0x014e
            r1.zzhhd = r3
        L_0x014e:
            r1 = r2
            goto L_0x0423
        L_0x0151:
            if (r6 != r10) goto L_0x0422
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r4 = r7.zzhcl
            if (r4 < 0) goto L_0x01a4
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x019f
            if (r4 != 0) goto L_0x0167
            com.google.android.gms.internal.ads.zzdmr r4 = com.google.android.gms.internal.ads.zzdmr.zzhcr
            r11.add(r4)
            goto L_0x016f
        L_0x0167:
            com.google.android.gms.internal.ads.zzdmr r6 = com.google.android.gms.internal.ads.zzdmr.zzi(r3, r1, r4)
            r11.add(r6)
        L_0x016e:
            int r1 = r1 + r4
        L_0x016f:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r4 = r7.zzhcl
            if (r4 < 0) goto L_0x019a
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x0195
            if (r4 != 0) goto L_0x018d
            com.google.android.gms.internal.ads.zzdmr r4 = com.google.android.gms.internal.ads.zzdmr.zzhcr
            r11.add(r4)
            goto L_0x016f
        L_0x018d:
            com.google.android.gms.internal.ads.zzdmr r6 = com.google.android.gms.internal.ads.zzdmr.zzi(r3, r1, r4)
            r11.add(r6)
            goto L_0x016e
        L_0x0195:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x019a:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzaye()
            throw r1
        L_0x019f:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x01a4:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzaye()
            throw r1
        L_0x01a9:
            if (r6 != r10) goto L_0x0422
            com.google.android.gms.internal.ads.zzdqb r1 = r0.zzgr(r8)
            r22 = r1
            r23 = r21
            r24 = r18
            r25 = r19
            r26 = r20
            r27 = r11
            r28 = r30
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r22, r23, r24, r25, r26, r27, r28)
            goto L_0x0423
        L_0x01c3:
            if (r6 != r10) goto L_0x0422
            r8 = 536870912(0x20000000, double:2.652494739E-315)
            long r8 = r25 & r8
            java.lang.String r1 = ""
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 != 0) goto L_0x0216
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r6 = r7.zzhcl
            if (r6 < 0) goto L_0x0211
            if (r6 != 0) goto L_0x01de
            r11.add(r1)
            goto L_0x01e9
        L_0x01de:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.ads.zzdod.UTF_8
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
        L_0x01e8:
            int r4 = r4 + r6
        L_0x01e9:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r8 = r7.zzhcl
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r6, r7)
            int r6 = r7.zzhcl
            if (r6 < 0) goto L_0x020c
            if (r6 != 0) goto L_0x0201
            r11.add(r1)
            goto L_0x01e9
        L_0x0201:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.ads.zzdod.UTF_8
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
            goto L_0x01e8
        L_0x020c:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzaye()
            throw r1
        L_0x0211:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzaye()
            throw r1
        L_0x0216:
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r6 = r7.zzhcl
            if (r6 < 0) goto L_0x0271
            if (r6 != 0) goto L_0x0224
            r11.add(r1)
            goto L_0x0237
        L_0x0224:
            int r8 = r4 + r6
            boolean r9 = com.google.android.gms.internal.ads.zzdrb.zzl(r3, r4, r8)
            if (r9 == 0) goto L_0x026c
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.google.android.gms.internal.ads.zzdod.UTF_8
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
        L_0x0236:
            r4 = r8
        L_0x0237:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r8 = r7.zzhcl
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r6, r7)
            int r6 = r7.zzhcl
            if (r6 < 0) goto L_0x0267
            if (r6 != 0) goto L_0x024f
            r11.add(r1)
            goto L_0x0237
        L_0x024f:
            int r8 = r4 + r6
            boolean r9 = com.google.android.gms.internal.ads.zzdrb.zzl(r3, r4, r8)
            if (r9 == 0) goto L_0x0262
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.google.android.gms.internal.ads.zzdod.UTF_8
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
            goto L_0x0236
        L_0x0262:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayk()
            throw r1
        L_0x0267:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzaye()
            throw r1
        L_0x026c:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayk()
            throw r1
        L_0x0271:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzaye()
            throw r1
        L_0x0276:
            r1 = 0
            if (r6 != r10) goto L_0x029e
            com.google.android.gms.internal.ads.zzdmp r11 = (com.google.android.gms.internal.ads.zzdmp) r11
            int r2 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r4 = r7.zzhcl
            int r4 = r4 + r2
        L_0x0282:
            if (r2 >= r4) goto L_0x0295
            int r2 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r2, r7)
            long r5 = r7.zzhcm
            int r8 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0290
            r5 = 1
            goto L_0x0291
        L_0x0290:
            r5 = 0
        L_0x0291:
            r11.addBoolean(r5)
            goto L_0x0282
        L_0x0295:
            if (r2 != r4) goto L_0x0299
            goto L_0x014e
        L_0x0299:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x029e:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.ads.zzdmp r11 = (com.google.android.gms.internal.ads.zzdmp) r11
            int r4 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r4, r7)
            long r8 = r7.zzhcm
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02ae
            r6 = 1
            goto L_0x02af
        L_0x02ae:
            r6 = 0
        L_0x02af:
            r11.addBoolean(r6)
        L_0x02b2:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r8 = r7.zzhcl
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r6, r7)
            long r8 = r7.zzhcm
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02c8
            r6 = 1
            goto L_0x02c9
        L_0x02c8:
            r6 = 0
        L_0x02c9:
            r11.addBoolean(r6)
            goto L_0x02b2
        L_0x02cd:
            if (r6 != r10) goto L_0x02ed
            com.google.android.gms.internal.ads.zzdoc r11 = (com.google.android.gms.internal.ads.zzdoc) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r2 = r7.zzhcl
            int r2 = r2 + r1
        L_0x02d8:
            if (r1 >= r2) goto L_0x02e4
            int r4 = com.google.android.gms.internal.ads.zzdmn.zzf(r3, r1)
            r11.zzgp(r4)
            int r1 = r1 + 4
            goto L_0x02d8
        L_0x02e4:
            if (r1 != r2) goto L_0x02e8
            goto L_0x0423
        L_0x02e8:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x02ed:
            if (r6 != r9) goto L_0x0422
            com.google.android.gms.internal.ads.zzdoc r11 = (com.google.android.gms.internal.ads.zzdoc) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzf(r18, r19)
            r11.zzgp(r1)
        L_0x02f8:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzf(r3, r4)
            r11.zzgp(r1)
            goto L_0x02f8
        L_0x030c:
            if (r6 != r10) goto L_0x032c
            com.google.android.gms.internal.ads.zzdoy r11 = (com.google.android.gms.internal.ads.zzdoy) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r2 = r7.zzhcl
            int r2 = r2 + r1
        L_0x0317:
            if (r1 >= r2) goto L_0x0323
            long r4 = com.google.android.gms.internal.ads.zzdmn.zzg(r3, r1)
            r11.zzfu(r4)
            int r1 = r1 + 8
            goto L_0x0317
        L_0x0323:
            if (r1 != r2) goto L_0x0327
            goto L_0x0423
        L_0x0327:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x032c:
            if (r6 != r13) goto L_0x0422
            com.google.android.gms.internal.ads.zzdoy r11 = (com.google.android.gms.internal.ads.zzdoy) r11
            long r8 = com.google.android.gms.internal.ads.zzdmn.zzg(r18, r19)
            r11.zzfu(r8)
        L_0x0337:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            long r8 = com.google.android.gms.internal.ads.zzdmn.zzg(r3, r4)
            r11.zzfu(r8)
            goto L_0x0337
        L_0x034b:
            if (r6 != r10) goto L_0x0353
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r11, r7)
            goto L_0x0423
        L_0x0353:
            if (r6 != 0) goto L_0x0422
            r22 = r18
            r23 = r19
            r24 = r20
            r25 = r11
            r26 = r30
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r21, r22, r23, r24, r25, r26)
            goto L_0x0423
        L_0x0365:
            if (r6 != r10) goto L_0x0385
            com.google.android.gms.internal.ads.zzdoy r11 = (com.google.android.gms.internal.ads.zzdoy) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r2 = r7.zzhcl
            int r2 = r2 + r1
        L_0x0370:
            if (r1 >= r2) goto L_0x037c
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r1, r7)
            long r4 = r7.zzhcm
            r11.zzfu(r4)
            goto L_0x0370
        L_0x037c:
            if (r1 != r2) goto L_0x0380
            goto L_0x0423
        L_0x0380:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x0385:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.ads.zzdoy r11 = (com.google.android.gms.internal.ads.zzdoy) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r4, r7)
            long r8 = r7.zzhcm
            r11.zzfu(r8)
        L_0x0392:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzb(r3, r4, r7)
            long r8 = r7.zzhcm
            r11.zzfu(r8)
            goto L_0x0392
        L_0x03a6:
            if (r6 != r10) goto L_0x03c5
            com.google.android.gms.internal.ads.zzdny r11 = (com.google.android.gms.internal.ads.zzdny) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r2 = r7.zzhcl
            int r2 = r2 + r1
        L_0x03b1:
            if (r1 >= r2) goto L_0x03bd
            float r4 = com.google.android.gms.internal.ads.zzdmn.zzi(r3, r1)
            r11.zzi(r4)
            int r1 = r1 + 4
            goto L_0x03b1
        L_0x03bd:
            if (r1 != r2) goto L_0x03c0
            goto L_0x0423
        L_0x03c0:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x03c5:
            if (r6 != r9) goto L_0x0422
            com.google.android.gms.internal.ads.zzdny r11 = (com.google.android.gms.internal.ads.zzdny) r11
            float r1 = com.google.android.gms.internal.ads.zzdmn.zzi(r18, r19)
            r11.zzi(r1)
        L_0x03d0:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            float r1 = com.google.android.gms.internal.ads.zzdmn.zzi(r3, r4)
            r11.zzi(r1)
            goto L_0x03d0
        L_0x03e4:
            if (r6 != r10) goto L_0x0403
            com.google.android.gms.internal.ads.zzdnl r11 = (com.google.android.gms.internal.ads.zzdnl) r11
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r4, r7)
            int r2 = r7.zzhcl
            int r2 = r2 + r1
        L_0x03ef:
            if (r1 >= r2) goto L_0x03fb
            double r4 = com.google.android.gms.internal.ads.zzdmn.zzh(r3, r1)
            r11.zzd(r4)
            int r1 = r1 + 8
            goto L_0x03ef
        L_0x03fb:
            if (r1 != r2) goto L_0x03fe
            goto L_0x0423
        L_0x03fe:
            com.google.android.gms.internal.ads.zzdok r1 = com.google.android.gms.internal.ads.zzdok.zzayd()
            throw r1
        L_0x0403:
            if (r6 != r13) goto L_0x0422
            com.google.android.gms.internal.ads.zzdnl r11 = (com.google.android.gms.internal.ads.zzdnl) r11
            double r8 = com.google.android.gms.internal.ads.zzdmn.zzh(r18, r19)
            r11.zzd(r8)
        L_0x040e:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r3, r1, r7)
            int r6 = r7.zzhcl
            if (r2 != r6) goto L_0x0423
            double r8 = com.google.android.gms.internal.ads.zzdmn.zzh(r3, r4)
            r11.zzd(r8)
            goto L_0x040e
        L_0x0422:
            r1 = r4
        L_0x0423:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.ads.zzdmo):int");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final <K, V> int zza(T r8, byte[] r9, int r10, int r11, int r12, long r13, com.google.android.gms.internal.ads.zzdmo r15) throws java.io.IOException {
        /*
            r7 = this;
            sun.misc.Unsafe r0 = com.google.android.gms.internal.ads.zzdpo.zzhjp
            java.lang.Object r12 = r7.zzgs(r12)
            java.lang.Object r1 = r0.getObject(r8, r13)
            com.google.android.gms.internal.ads.zzdpf r2 = r7.zzhkg
            boolean r2 = r2.zzag(r1)
            if (r2 == 0) goto L_0x0021
            com.google.android.gms.internal.ads.zzdpf r2 = r7.zzhkg
            java.lang.Object r2 = r2.zzai(r12)
            com.google.android.gms.internal.ads.zzdpf r3 = r7.zzhkg
            r3.zzc(r2, r1)
            r0.putObject(r8, r13, r2)
            r1 = r2
        L_0x0021:
            com.google.android.gms.internal.ads.zzdpf r8 = r7.zzhkg
            com.google.android.gms.internal.ads.zzdpd r8 = r8.zzaj(r12)
            com.google.android.gms.internal.ads.zzdpf r12 = r7.zzhkg
            java.util.Map r12 = r12.zzae(r1)
            int r10 = com.google.android.gms.internal.ads.zzdmn.zza(r9, r10, r15)
            int r13 = r15.zzhcl
            if (r13 < 0) goto L_0x0097
            int r14 = r11 - r10
            if (r13 > r14) goto L_0x0097
            int r13 = r13 + r10
            K r14 = r8.zzhjj
            V r0 = r8.zzckh
        L_0x003e:
            if (r10 >= r13) goto L_0x008c
            int r1 = r10 + 1
            byte r10 = r9[r10]
            if (r10 >= 0) goto L_0x004c
            int r1 = com.google.android.gms.internal.ads.zzdmn.zza(r10, r9, r1, r15)
            int r10 = r15.zzhcl
        L_0x004c:
            r2 = r1
            int r1 = r10 >>> 3
            r3 = r10 & 7
            r4 = 1
            if (r1 == r4) goto L_0x0072
            r4 = 2
            if (r1 == r4) goto L_0x0058
            goto L_0x0087
        L_0x0058:
            com.google.android.gms.internal.ads.zzdri r1 = r8.zzhjk
            int r1 = r1.zzbak()
            if (r3 != r1) goto L_0x0087
            com.google.android.gms.internal.ads.zzdri r4 = r8.zzhjk
            V r10 = r8.zzckh
            java.lang.Class r5 = r10.getClass()
            r1 = r9
            r3 = r11
            r6 = r15
            int r10 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r0 = r15.zzhcn
            goto L_0x003e
        L_0x0072:
            com.google.android.gms.internal.ads.zzdri r1 = r8.zzhji
            int r1 = r1.zzbak()
            if (r3 != r1) goto L_0x0087
            com.google.android.gms.internal.ads.zzdri r4 = r8.zzhji
            r5 = 0
            r1 = r9
            r3 = r11
            r6 = r15
            int r10 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r14 = r15.zzhcn
            goto L_0x003e
        L_0x0087:
            int r10 = com.google.android.gms.internal.ads.zzdmn.zza(r10, r9, r2, r11, r15)
            goto L_0x003e
        L_0x008c:
            if (r10 != r13) goto L_0x0092
            r12.put(r14, r0)
            return r13
        L_0x0092:
            com.google.android.gms.internal.ads.zzdok r8 = com.google.android.gms.internal.ads.zzdok.zzayj()
            throw r8
        L_0x0097:
            com.google.android.gms.internal.ads.zzdok r8 = com.google.android.gms.internal.ads.zzdok.zzayd()
            goto L_0x009d
        L_0x009c:
            throw r8
        L_0x009d:
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Object, byte[], int, int, int, long, com.google.android.gms.internal.ads.zzdmo):int");
    }

    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzdmo zzdmo) throws IOException {
        int i9;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i10 = i;
        int i11 = i3;
        int i12 = i4;
        int i13 = i5;
        long j2 = j;
        int i14 = i8;
        zzdmo zzdmo2 = zzdmo;
        Unsafe unsafe = zzhjp;
        long j3 = (long) (this.zzhjq[i14 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Double.valueOf(zzdmn.zzh(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 52:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Float.valueOf(zzdmn.zzi(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 53:
            case 54:
                if (i13 == 0) {
                    i9 = zzdmn.zzb(bArr2, i10, zzdmo2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzdmo2.zzhcm));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 55:
            case 62:
                if (i13 == 0) {
                    i9 = zzdmn.zza(bArr2, i10, zzdmo2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzdmo2.zzhcl));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 56:
            case 65:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Long.valueOf(zzdmn.zzg(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 57:
            case 64:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Integer.valueOf(zzdmn.zzf(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 58:
                if (i13 == 0) {
                    i9 = zzdmn.zzb(bArr2, i10, zzdmo2);
                    unsafe.putObject(t2, j2, Boolean.valueOf(zzdmo2.zzhcm != 0));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 59:
                if (i13 == 2) {
                    int zza = zzdmn.zza(bArr2, i10, zzdmo2);
                    int i15 = zzdmo2.zzhcl;
                    if (i15 == 0) {
                        unsafe.putObject(t2, j2, "");
                    } else if ((i6 & DriveFile.MODE_WRITE_ONLY) == 0 || zzdrb.zzl(bArr2, zza, zza + i15)) {
                        unsafe.putObject(t2, j2, new String(bArr2, zza, i15, zzdod.UTF_8));
                        zza += i15;
                    } else {
                        throw zzdok.zzayk();
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza;
                }
                return i10;
            case 60:
                if (i13 == 2) {
                    int zza2 = zzdmn.zza(zzgr(i14), bArr2, i10, i2, zzdmo2);
                    Object object = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object == null) {
                        unsafe.putObject(t2, j2, zzdmo2.zzhcn);
                    } else {
                        unsafe.putObject(t2, j2, zzdod.zzb(object, zzdmo2.zzhcn));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza2;
                }
                return i10;
            case 61:
                if (i13 == 2) {
                    i9 = zzdmn.zze(bArr2, i10, zzdmo2);
                    unsafe.putObject(t2, j2, zzdmo2.zzhcn);
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 63:
                if (i13 == 0) {
                    int zza3 = zzdmn.zza(bArr2, i10, zzdmo2);
                    int i16 = zzdmo2.zzhcl;
                    zzdog zzgt = zzgt(i14);
                    if (zzgt == null || zzgt.zzf(i16)) {
                        unsafe.putObject(t2, j2, Integer.valueOf(i16));
                        i9 = zza3;
                        unsafe.putInt(t2, j3, i12);
                        return i9;
                    }
                    zzal(t).zzc(i11, Long.valueOf((long) i16));
                    return zza3;
                }
                return i10;
            case 66:
                if (i13 == 0) {
                    i9 = zzdmn.zza(bArr2, i10, zzdmo2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzdnd.zzft(zzdmo2.zzhcl)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 67:
                if (i13 == 0) {
                    i9 = zzdmn.zzb(bArr2, i10, zzdmo2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzdnd.zzfi(zzdmo2.zzhcm)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 68:
                if (i13 == 3) {
                    i9 = zzdmn.zza(zzgr(i14), bArr, i, i2, (i11 & -8) | 4, zzdmo);
                    Object object2 = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object2 == null) {
                        unsafe.putObject(t2, j2, zzdmo2.zzhcn);
                    } else {
                        unsafe.putObject(t2, j2, zzdod.zzb(object2, zzdmo2.zzhcn));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            default:
                return i10;
        }
    }

    private final zzdqb zzgr(int i) {
        int i2 = (i / 3) << 1;
        zzdqb zzdqb = (zzdqb) this.zzhjr[i2];
        if (zzdqb != null) {
            return zzdqb;
        }
        zzdqb zzg = zzdpx.zzazg().zzg((Class) this.zzhjr[i2 + 1]);
        this.zzhjr[i2] = zzg;
        return zzg;
    }

    private final Object zzgs(int i) {
        return this.zzhjr[(i / 3) << 1];
    }

    private final zzdog zzgt(int i) {
        return (zzdog) this.zzhjr[((i / 3) << 1) + 1];
    }

    /* JADX WARN: Type inference failed for: r34v0, types: [int] */
    /* JADX WARN: Type inference failed for: r3v27, types: [int] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0372, code lost:
        if (r0 == r15) goto L_0x03e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x03bb, code lost:
        if (r0 == r15) goto L_0x03e1;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x03e7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x03fc  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x0450  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(java.lang.Object r30, byte[] r31, int r32, int r33, int r34, com.google.android.gms.internal.ads.zzdmo r35) throws java.io.IOException {
        /*
            r29 = this;
            r15 = r29
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r34
            r9 = r35
            sun.misc.Unsafe r10 = com.google.android.gms.internal.ads.zzdpo.zzhjp
            r16 = 0
            r0 = r32
            r1 = -1
            r2 = 0
            r3 = 0
            r6 = 0
            r7 = -1
        L_0x0017:
            if (r0 >= r13) goto L_0x0492
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0028
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r12, r3, r9)
            int r3 = r9.zzhcl
            r4 = r0
            r5 = r3
            goto L_0x002a
        L_0x0028:
            r5 = r0
            r4 = r3
        L_0x002a:
            int r3 = r5 >>> 3
            r0 = r5 & 7
            r8 = 3
            if (r3 <= r1) goto L_0x0037
            int r2 = r2 / r8
            int r1 = r15.zzak(r3, r2)
            goto L_0x003b
        L_0x0037:
            int r1 = r15.zzgx(r3)
        L_0x003b:
            r2 = r1
            r1 = -1
            if (r2 != r1) goto L_0x004e
            r24 = r3
            r2 = r4
            r19 = r6
            r17 = r7
            r26 = r10
            r6 = r11
            r18 = 0
            r7 = r5
            goto L_0x03e5
        L_0x004e:
            int[] r1 = r15.zzhjq
            int r18 = r2 + 1
            r8 = r1[r18]
            r18 = 267386880(0xff00000, float:2.3665827E-29)
            r18 = r8 & r18
            int r11 = r18 >>> 20
            r18 = 1048575(0xfffff, float:1.469367E-39)
            r19 = r5
            r5 = r8 & r18
            long r12 = (long) r5
            r5 = 17
            r20 = r8
            if (r11 > r5) goto L_0x02dc
            int r5 = r2 + 2
            r1 = r1[r5]
            int r5 = r1 >>> 20
            r8 = 1
            int r22 = r8 << r5
            r1 = r1 & r18
            if (r1 == r7) goto L_0x0083
            r5 = -1
            if (r7 == r5) goto L_0x007c
            long r8 = (long) r7
            r10.putInt(r14, r8, r6)
        L_0x007c:
            long r6 = (long) r1
            int r6 = r10.getInt(r14, r6)
            r7 = r1
            goto L_0x0084
        L_0x0083:
            r5 = -1
        L_0x0084:
            r1 = 5
            switch(r11) {
                case 0: goto L_0x02a4;
                case 1: goto L_0x028a;
                case 2: goto L_0x0264;
                case 3: goto L_0x0264;
                case 4: goto L_0x0249;
                case 5: goto L_0x0224;
                case 6: goto L_0x0201;
                case 7: goto L_0x01d9;
                case 8: goto L_0x01b4;
                case 9: goto L_0x017e;
                case 10: goto L_0x0163;
                case 11: goto L_0x0249;
                case 12: goto L_0x0131;
                case 13: goto L_0x0201;
                case 14: goto L_0x0224;
                case 15: goto L_0x0116;
                case 16: goto L_0x00e9;
                case 17: goto L_0x0097;
                default: goto L_0x0088;
            }
        L_0x0088:
            r12 = r31
            r13 = r35
            r9 = r2
            r11 = r3
            r32 = r7
            r8 = r19
            r18 = -1
        L_0x0094:
            r7 = r4
            goto L_0x02cc
        L_0x0097:
            r8 = 3
            if (r0 != r8) goto L_0x00dd
            int r0 = r3 << 3
            r8 = r0 | 4
            com.google.android.gms.internal.ads.zzdqb r0 = r15.zzgr(r2)
            r1 = r31
            r9 = r2
            r2 = r4
            r11 = r3
            r3 = r33
            r4 = r8
            r8 = r19
            r18 = -1
            r5 = r35
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r1, r2, r3, r4, r5)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x00c0
            r5 = r35
            java.lang.Object r1 = r5.zzhcn
            r10.putObject(r14, r12, r1)
            goto L_0x00cf
        L_0x00c0:
            r5 = r35
            java.lang.Object r1 = r10.getObject(r14, r12)
            java.lang.Object r2 = r5.zzhcn
            java.lang.Object r1 = com.google.android.gms.internal.ads.zzdod.zzb(r1, r2)
            r10.putObject(r14, r12, r1)
        L_0x00cf:
            r6 = r6 | r22
            r12 = r31
            r13 = r33
            r3 = r8
            r2 = r9
            r1 = r11
            r11 = r34
            r9 = r5
            goto L_0x0017
        L_0x00dd:
            r9 = r2
            r11 = r3
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            goto L_0x0245
        L_0x00e9:
            r5 = r35
            r9 = r2
            r11 = r3
            r8 = r19
            r18 = -1
            if (r0 != 0) goto L_0x0111
            r2 = r12
            r12 = r31
            int r13 = com.google.android.gms.internal.ads.zzdmn.zzb(r12, r4, r5)
            long r0 = r5.zzhcm
            long r19 = com.google.android.gms.internal.ads.zzdnd.zzfi(r0)
            r0 = r10
            r1 = r30
            r32 = r13
            r13 = r5
            r4 = r19
            r0.putLong(r1, r2, r4)
            r6 = r6 | r22
            r0 = r32
            goto L_0x02c2
        L_0x0111:
            r12 = r31
            r13 = r5
            goto L_0x0245
        L_0x0116:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != 0) goto L_0x0245
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r12, r4, r13)
            int r1 = r13.zzhcl
            int r1 = com.google.android.gms.internal.ads.zzdnd.zzft(r1)
            r10.putInt(r14, r2, r1)
            goto L_0x017a
        L_0x0131:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != 0) goto L_0x0245
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r12, r4, r13)
            int r1 = r13.zzhcl
            com.google.android.gms.internal.ads.zzdog r4 = r15.zzgt(r9)
            if (r4 == 0) goto L_0x015f
            boolean r4 = r4.zzf(r1)
            if (r4 == 0) goto L_0x0151
            goto L_0x015f
        L_0x0151:
            com.google.android.gms.internal.ads.zzdqu r2 = zzal(r30)
            long r3 = (long) r1
            java.lang.Long r1 = java.lang.Long.valueOf(r3)
            r2.zzc(r8, r1)
            goto L_0x02c2
        L_0x015f:
            r10.putInt(r14, r2, r1)
            goto L_0x017a
        L_0x0163:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 2
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0245
            int r0 = com.google.android.gms.internal.ads.zzdmn.zze(r12, r4, r13)
            java.lang.Object r1 = r13.zzhcn
            r10.putObject(r14, r2, r1)
        L_0x017a:
            r6 = r6 | r22
            goto L_0x02c2
        L_0x017e:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 2
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x01b0
            com.google.android.gms.internal.ads.zzdqb r0 = r15.zzgr(r9)
            r5 = r33
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r12, r4, r5, r13)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x01a1
            java.lang.Object r1 = r13.zzhcn
            r10.putObject(r14, r2, r1)
            goto L_0x0219
        L_0x01a1:
            java.lang.Object r1 = r10.getObject(r14, r2)
            java.lang.Object r4 = r13.zzhcn
            java.lang.Object r1 = com.google.android.gms.internal.ads.zzdod.zzb(r1, r4)
            r10.putObject(r14, r2, r1)
            goto L_0x0219
        L_0x01b0:
            r5 = r33
            goto L_0x0245
        L_0x01b4:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 2
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0245
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r20 & r0
            if (r0 != 0) goto L_0x01cf
            int r0 = com.google.android.gms.internal.ads.zzdmn.zzc(r12, r4, r13)
            goto L_0x01d3
        L_0x01cf:
            int r0 = com.google.android.gms.internal.ads.zzdmn.zzd(r12, r4, r13)
        L_0x01d3:
            java.lang.Object r1 = r13.zzhcn
            r10.putObject(r14, r2, r1)
            goto L_0x0219
        L_0x01d9:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != 0) goto L_0x0245
            int r0 = com.google.android.gms.internal.ads.zzdmn.zzb(r12, r4, r13)
            r32 = r0
            long r0 = r13.zzhcm
            r19 = 0
            int r4 = (r0 > r19 ? 1 : (r0 == r19 ? 0 : -1))
            if (r4 == 0) goto L_0x01f8
            r0 = 1
            goto L_0x01f9
        L_0x01f8:
            r0 = 0
        L_0x01f9:
            com.google.android.gms.internal.ads.zzdqz.zza(r14, r2, r0)
            r6 = r6 | r22
            r0 = r32
            goto L_0x021b
        L_0x0201:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0245
            int r0 = com.google.android.gms.internal.ads.zzdmn.zzf(r12, r4)
            r10.putInt(r14, r2, r0)
            int r0 = r4 + 4
        L_0x0219:
            r6 = r6 | r22
        L_0x021b:
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
            r11 = r34
            r13 = r5
            goto L_0x0017
        L_0x0224:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 1
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0245
            long r19 = com.google.android.gms.internal.ads.zzdmn.zzg(r12, r4)
            r0 = r10
            r1 = r30
            r32 = r7
            r7 = r4
            r4 = r19
            r0.putLong(r1, r2, r4)
            goto L_0x02bc
        L_0x0245:
            r32 = r7
            goto L_0x0094
        L_0x0249:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != 0) goto L_0x02cc
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r12, r7, r13)
            int r1 = r13.zzhcl
            r10.putInt(r14, r2, r1)
            goto L_0x02be
        L_0x0264:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != 0) goto L_0x02cc
            int r7 = com.google.android.gms.internal.ads.zzdmn.zzb(r12, r7, r13)
            long r4 = r13.zzhcm
            r0 = r10
            r1 = r30
            r0.putLong(r1, r2, r4)
            r6 = r6 | r22
            r0 = r7
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
            r7 = r32
            goto L_0x02c6
        L_0x028a:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != r1) goto L_0x02cc
            float r0 = com.google.android.gms.internal.ads.zzdmn.zzi(r12, r7)
            com.google.android.gms.internal.ads.zzdqz.zza(r14, r2, r0)
            int r0 = r7 + 4
            goto L_0x02be
        L_0x02a4:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r1 = 1
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != r1) goto L_0x02cc
            double r0 = com.google.android.gms.internal.ads.zzdmn.zzh(r12, r7)
            com.google.android.gms.internal.ads.zzdqz.zza(r14, r2, r0)
        L_0x02bc:
            int r0 = r7 + 8
        L_0x02be:
            r6 = r6 | r22
            r7 = r32
        L_0x02c2:
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
        L_0x02c6:
            r13 = r33
            r11 = r34
            goto L_0x0017
        L_0x02cc:
            r17 = r32
            r19 = r6
            r2 = r7
            r7 = r8
            r18 = r9
            r26 = r10
            r24 = r11
            r6 = r34
            goto L_0x03e5
        L_0x02dc:
            r5 = r3
            r17 = r7
            r8 = r19
            r18 = -1
            r7 = r4
            r27 = r12
            r12 = r31
            r13 = r9
            r9 = r2
            r2 = r27
            r1 = 27
            if (r11 != r1) goto L_0x0341
            r1 = 2
            if (r0 != r1) goto L_0x0334
            java.lang.Object r0 = r10.getObject(r14, r2)
            com.google.android.gms.internal.ads.zzdoj r0 = (com.google.android.gms.internal.ads.zzdoj) r0
            boolean r1 = r0.zzavi()
            if (r1 != 0) goto L_0x0311
            int r1 = r0.size()
            if (r1 != 0) goto L_0x0308
            r1 = 10
            goto L_0x030a
        L_0x0308:
            int r1 = r1 << 1
        L_0x030a:
            com.google.android.gms.internal.ads.zzdoj r0 = r0.zzfl(r1)
            r10.putObject(r14, r2, r0)
        L_0x0311:
            r11 = r0
            com.google.android.gms.internal.ads.zzdqb r0 = r15.zzgr(r9)
            r1 = r8
            r2 = r31
            r3 = r7
            r4 = r33
            r7 = r5
            r5 = r11
            r19 = r6
            r6 = r35
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r1, r2, r3, r4, r5, r6)
            r11 = r34
            r1 = r7
            r3 = r8
            r2 = r9
            r9 = r13
            r7 = r17
            r6 = r19
            r13 = r33
            goto L_0x0017
        L_0x0334:
            r19 = r6
            r24 = r5
            r15 = r7
            r25 = r8
            r18 = r9
            r26 = r10
            goto L_0x03be
        L_0x0341:
            r19 = r6
            r6 = r5
            r1 = 49
            if (r11 > r1) goto L_0x0390
            r5 = r20
            long r4 = (long) r5
            r1 = r0
            r0 = r29
            r32 = r1
            r1 = r30
            r22 = r2
            r2 = r31
            r3 = r7
            r20 = r4
            r4 = r33
            r5 = r8
            r24 = r6
            r15 = r7
            r7 = r32
            r25 = r8
            r8 = r9
            r18 = r9
            r26 = r10
            r9 = r20
            r12 = r22
            r14 = r35
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0376
            goto L_0x03e1
        L_0x0376:
            r15 = r29
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r34
            r9 = r35
            r7 = r17
            r2 = r18
            r6 = r19
            r1 = r24
            r3 = r25
        L_0x038c:
            r10 = r26
            goto L_0x0017
        L_0x0390:
            r32 = r0
            r22 = r2
            r24 = r6
            r15 = r7
            r25 = r8
            r18 = r9
            r26 = r10
            r5 = r20
            r0 = 50
            if (r11 != r0) goto L_0x03c4
            r7 = r32
            r0 = 2
            if (r7 != r0) goto L_0x03be
            r0 = r29
            r1 = r30
            r2 = r31
            r3 = r15
            r4 = r33
            r5 = r18
            r6 = r22
            r8 = r35
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x0376
            goto L_0x03e1
        L_0x03be:
            r6 = r34
            r2 = r15
        L_0x03c1:
            r7 = r25
            goto L_0x03e5
        L_0x03c4:
            r7 = r32
            r0 = r29
            r1 = r30
            r2 = r31
            r3 = r15
            r4 = r33
            r8 = r5
            r5 = r25
            r6 = r24
            r9 = r11
            r10 = r22
            r12 = r18
            r13 = r35
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x0478
        L_0x03e1:
            r6 = r34
            r2 = r0
            goto L_0x03c1
        L_0x03e5:
            if (r7 != r6) goto L_0x03f6
            if (r6 != 0) goto L_0x03ea
            goto L_0x03f6
        L_0x03ea:
            r4 = -1
            r8 = r29
            r11 = r30
            r3 = r7
            r0 = r17
            r1 = r19
            goto L_0x04a1
        L_0x03f6:
            r8 = r29
            boolean r0 = r8.zzhjv
            if (r0 == 0) goto L_0x0450
            r9 = r35
            com.google.android.gms.internal.ads.zzdno r0 = r9.zzhco
            com.google.android.gms.internal.ads.zzdno r1 = com.google.android.gms.internal.ads.zzdno.zzaxd()
            if (r0 == r1) goto L_0x044d
            com.google.android.gms.internal.ads.zzdpk r0 = r8.zzhju
            com.google.android.gms.internal.ads.zzdno r1 = r9.zzhco
            r10 = r24
            com.google.android.gms.internal.ads.zzdob$zzd r0 = r1.zza(r0, r10)
            if (r0 != 0) goto L_0x042c
            com.google.android.gms.internal.ads.zzdqu r4 = zzal(r30)
            r0 = r7
            r1 = r31
            r3 = r33
            r5 = r35
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r1, r2, r3, r4, r5)
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r6
            r3 = r7
            r15 = r8
            goto L_0x0489
        L_0x042c:
            r11 = r30
            r0 = r11
            com.google.android.gms.internal.ads.zzdob$zzc r0 = (com.google.android.gms.internal.ads.zzdob.zzc) r0
            com.google.android.gms.internal.ads.zzdns<java.lang.Object> r1 = r0.zzhhj
            boolean r1 = r1.isImmutable()
            if (r1 == 0) goto L_0x0443
            com.google.android.gms.internal.ads.zzdns<java.lang.Object> r1 = r0.zzhhj
            java.lang.Object r1 = r1.clone()
            com.google.android.gms.internal.ads.zzdns r1 = (com.google.android.gms.internal.ads.zzdns) r1
            r0.zzhhj = r1
        L_0x0443:
            com.google.android.gms.internal.ads.zzdns<java.lang.Object> r1 = r0.zzhhj
            com.google.android.gms.internal.ads.zzdns<java.lang.Object> r0 = r0.zzhhj
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        L_0x044d:
            r11 = r30
            goto L_0x0454
        L_0x0450:
            r11 = r30
            r9 = r35
        L_0x0454:
            r10 = r24
            com.google.android.gms.internal.ads.zzdqu r4 = zzal(r30)
            r0 = r7
            r1 = r31
            r3 = r33
            r5 = r35
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r1, r2, r3, r4, r5)
            r12 = r31
            r13 = r33
            r3 = r7
            r15 = r8
            r1 = r10
            r14 = r11
            r7 = r17
            r2 = r18
            r10 = r26
            r11 = r6
            r6 = r19
            goto L_0x0017
        L_0x0478:
            r10 = r24
            r7 = r25
            r15 = r29
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r34
            r9 = r35
            r3 = r7
        L_0x0489:
            r1 = r10
            r7 = r17
            r2 = r18
            r6 = r19
            goto L_0x038c
        L_0x0492:
            r19 = r6
            r17 = r7
            r26 = r10
            r6 = r11
            r11 = r14
            r8 = r15
            r2 = r0
            r0 = r17
            r1 = r19
            r4 = -1
        L_0x04a1:
            if (r0 == r4) goto L_0x04a9
            long r4 = (long) r0
            r0 = r26
            r0.putInt(r11, r4, r1)
        L_0x04a9:
            r0 = 0
            int r1 = r8.zzhka
        L_0x04ac:
            int r4 = r8.zzhkb
            if (r1 >= r4) goto L_0x04bf
            int[] r4 = r8.zzhjz
            r4 = r4[r1]
            com.google.android.gms.internal.ads.zzdqt<?, ?> r5 = r8.zzhke
            java.lang.Object r0 = r8.zza(r11, r4, r0, r5)
            com.google.android.gms.internal.ads.zzdqu r0 = (com.google.android.gms.internal.ads.zzdqu) r0
            int r1 = r1 + 1
            goto L_0x04ac
        L_0x04bf:
            if (r0 == 0) goto L_0x04c6
            com.google.android.gms.internal.ads.zzdqt<?, ?> r1 = r8.zzhke
            r1.zzg(r11, r0)
        L_0x04c6:
            if (r6 != 0) goto L_0x04d2
            r0 = r33
            if (r2 != r0) goto L_0x04cd
            goto L_0x04d8
        L_0x04cd:
            com.google.android.gms.internal.ads.zzdok r0 = com.google.android.gms.internal.ads.zzdok.zzayj()
            throw r0
        L_0x04d2:
            r0 = r33
            if (r2 > r0) goto L_0x04d9
            if (r3 != r6) goto L_0x04d9
        L_0x04d8:
            return r2
        L_0x04d9:
            com.google.android.gms.internal.ads.zzdok r0 = com.google.android.gms.internal.ads.zzdok.zzayj()
            goto L_0x04df
        L_0x04de:
            throw r0
        L_0x04df:
            goto L_0x04de
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Object, byte[], int, int, int, com.google.android.gms.internal.ads.zzdmo):int");
    }

    /* JADX WARN: Type inference failed for: r3v13, types: [int] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.ads.zzdqz.zza(byte[], long, byte):void
      com.google.android.gms.internal.ads.zzdqz.zza(java.lang.Object, long, float):void */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01e2, code lost:
        if (r0 == r15) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x020f, code lost:
        if (r0 == r15) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x022e, code lost:
        if (r0 == r15) goto L_0x0230;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r28, byte[] r29, int r30, int r31, com.google.android.gms.internal.ads.zzdmo r32) throws java.io.IOException {
        /*
            r27 = this;
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r11 = r32
            boolean r0 = r15.zzhjx
            if (r0 == 0) goto L_0x025d
            sun.misc.Unsafe r9 = com.google.android.gms.internal.ads.zzdpo.zzhjp
            r10 = -1
            r16 = 0
            r0 = r30
            r1 = -1
            r2 = 0
        L_0x0017:
            if (r0 >= r13) goto L_0x0254
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0029
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r12, r3, r11)
            int r3 = r11.zzhcl
            r8 = r0
            r17 = r3
            goto L_0x002c
        L_0x0029:
            r17 = r0
            r8 = r3
        L_0x002c:
            int r7 = r17 >>> 3
            r6 = r17 & 7
            if (r7 <= r1) goto L_0x0039
            int r2 = r2 / 3
            int r0 = r15.zzak(r7, r2)
            goto L_0x003d
        L_0x0039:
            int r0 = r15.zzgx(r7)
        L_0x003d:
            r4 = r0
            if (r4 != r10) goto L_0x004b
            r24 = r7
            r2 = r8
            r18 = r9
            r19 = 0
            r26 = -1
            goto L_0x0231
        L_0x004b:
            int[] r0 = r15.zzhjq
            int r1 = r4 + 1
            r5 = r0[r1]
            r0 = 267386880(0xff00000, float:2.3665827E-29)
            r0 = r0 & r5
            int r3 = r0 >>> 20
            r0 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r0 & r5
            long r1 = (long) r0
            r0 = 17
            r10 = 2
            if (r3 > r0) goto L_0x0167
            r0 = 1
            switch(r3) {
                case 0: goto L_0x014e;
                case 1: goto L_0x013f;
                case 2: goto L_0x012d;
                case 3: goto L_0x012d;
                case 4: goto L_0x011f;
                case 5: goto L_0x010f;
                case 6: goto L_0x00fe;
                case 7: goto L_0x00e8;
                case 8: goto L_0x00d1;
                case 9: goto L_0x00b0;
                case 10: goto L_0x00a3;
                case 11: goto L_0x011f;
                case 12: goto L_0x0094;
                case 13: goto L_0x00fe;
                case 14: goto L_0x010f;
                case 15: goto L_0x0081;
                case 16: goto L_0x0066;
                default: goto L_0x0064;
            }
        L_0x0064:
            goto L_0x01a4
        L_0x0066:
            if (r6 != 0) goto L_0x01a4
            int r6 = com.google.android.gms.internal.ads.zzdmn.zzb(r12, r8, r11)
            r19 = r1
            long r0 = r11.zzhcm
            long r21 = com.google.android.gms.internal.ads.zzdnd.zzfi(r0)
            r0 = r9
            r2 = r19
            r1 = r28
            r10 = r4
            r4 = r21
            r0.putLong(r1, r2, r4)
            goto L_0x013d
        L_0x0081:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r12, r8, r11)
            int r1 = r11.zzhcl
            int r1 = com.google.android.gms.internal.ads.zzdnd.zzft(r1)
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x0094:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r12, r8, r11)
            int r1 = r11.zzhcl
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x00a3:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            int r0 = com.google.android.gms.internal.ads.zzdmn.zze(r12, r8, r11)
            java.lang.Object r1 = r11.zzhcn
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00b0:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            com.google.android.gms.internal.ads.zzdqb r0 = r15.zzgr(r4)
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r12, r8, r13, r11)
            java.lang.Object r1 = r9.getObject(r14, r2)
            if (r1 != 0) goto L_0x00c7
            java.lang.Object r1 = r11.zzhcn
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00c7:
            java.lang.Object r5 = r11.zzhcn
            java.lang.Object r1 = com.google.android.gms.internal.ads.zzdod.zzb(r1, r5)
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00d1:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r0 & r5
            if (r0 != 0) goto L_0x00de
            int r0 = com.google.android.gms.internal.ads.zzdmn.zzc(r12, r8, r11)
            goto L_0x00e2
        L_0x00de:
            int r0 = com.google.android.gms.internal.ads.zzdmn.zzd(r12, r8, r11)
        L_0x00e2:
            java.lang.Object r1 = r11.zzhcn
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00e8:
            r2 = r1
            if (r6 != 0) goto L_0x01a4
            int r1 = com.google.android.gms.internal.ads.zzdmn.zzb(r12, r8, r11)
            long r5 = r11.zzhcm
            r19 = 0
            int r8 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r8 == 0) goto L_0x00f8
            goto L_0x00f9
        L_0x00f8:
            r0 = 0
        L_0x00f9:
            com.google.android.gms.internal.ads.zzdqz.zza(r14, r2, r0)
            r0 = r1
            goto L_0x010b
        L_0x00fe:
            r2 = r1
            r0 = 5
            if (r6 != r0) goto L_0x01a4
            int r0 = com.google.android.gms.internal.ads.zzdmn.zzf(r12, r8)
            r9.putInt(r14, r2, r0)
            int r0 = r8 + 4
        L_0x010b:
            r2 = r4
            r1 = r7
            goto L_0x0251
        L_0x010f:
            r2 = r1
            if (r6 != r0) goto L_0x01a4
            long r5 = com.google.android.gms.internal.ads.zzdmn.zzg(r12, r8)
            r0 = r9
            r1 = r28
            r10 = r4
            r4 = r5
            r0.putLong(r1, r2, r4)
            goto L_0x0159
        L_0x011f:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r12, r8, r11)
            int r1 = r11.zzhcl
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x012d:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r6 = com.google.android.gms.internal.ads.zzdmn.zzb(r12, r8, r11)
            long r4 = r11.zzhcm
            r0 = r9
            r1 = r28
            r0.putLong(r1, r2, r4)
        L_0x013d:
            r0 = r6
            goto L_0x015b
        L_0x013f:
            r2 = r1
            r10 = r4
            r0 = 5
            if (r6 != r0) goto L_0x015f
            float r0 = com.google.android.gms.internal.ads.zzdmn.zzi(r12, r8)
            com.google.android.gms.internal.ads.zzdqz.zza(r14, r2, r0)
            int r0 = r8 + 4
            goto L_0x015b
        L_0x014e:
            r2 = r1
            r10 = r4
            if (r6 != r0) goto L_0x015f
            double r0 = com.google.android.gms.internal.ads.zzdmn.zzh(r12, r8)
            com.google.android.gms.internal.ads.zzdqz.zza(r14, r2, r0)
        L_0x0159:
            int r0 = r8 + 8
        L_0x015b:
            r1 = r7
            r2 = r10
            goto L_0x0251
        L_0x015f:
            r24 = r7
            r15 = r8
            r18 = r9
            r19 = r10
            goto L_0x01ab
        L_0x0167:
            r0 = 27
            if (r3 != r0) goto L_0x01af
            if (r6 != r10) goto L_0x01a4
            java.lang.Object r0 = r9.getObject(r14, r1)
            com.google.android.gms.internal.ads.zzdoj r0 = (com.google.android.gms.internal.ads.zzdoj) r0
            boolean r3 = r0.zzavi()
            if (r3 != 0) goto L_0x018b
            int r3 = r0.size()
            if (r3 != 0) goto L_0x0182
            r3 = 10
            goto L_0x0184
        L_0x0182:
            int r3 = r3 << 1
        L_0x0184:
            com.google.android.gms.internal.ads.zzdoj r0 = r0.zzfl(r3)
            r9.putObject(r14, r1, r0)
        L_0x018b:
            r5 = r0
            com.google.android.gms.internal.ads.zzdqb r0 = r15.zzgr(r4)
            r1 = r17
            r2 = r29
            r3 = r8
            r19 = r4
            r4 = r31
            r6 = r32
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r1, r2, r3, r4, r5, r6)
            r1 = r7
            r2 = r19
            goto L_0x0251
        L_0x01a4:
            r19 = r4
            r24 = r7
            r15 = r8
            r18 = r9
        L_0x01ab:
            r26 = -1
            goto L_0x0212
        L_0x01af:
            r19 = r4
            r0 = 49
            if (r3 > r0) goto L_0x01e5
            long r4 = (long) r5
            r0 = r27
            r20 = r1
            r1 = r28
            r2 = r29
            r10 = r3
            r3 = r8
            r22 = r4
            r4 = r31
            r5 = r17
            r30 = r6
            r6 = r7
            r24 = r7
            r7 = r30
            r15 = r8
            r8 = r19
            r18 = r9
            r25 = r10
            r26 = -1
            r9 = r22
            r11 = r25
            r12 = r20
            r14 = r32
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0241
            goto L_0x0230
        L_0x01e5:
            r20 = r1
            r25 = r3
            r30 = r6
            r24 = r7
            r15 = r8
            r18 = r9
            r26 = -1
            r0 = 50
            r9 = r25
            if (r9 != r0) goto L_0x0214
            r7 = r30
            if (r7 != r10) goto L_0x0212
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r5 = r19
            r6 = r20
            r8 = r32
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x0241
            goto L_0x0230
        L_0x0212:
            r2 = r15
            goto L_0x0231
        L_0x0214:
            r7 = r30
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r8 = r5
            r5 = r17
            r6 = r24
            r10 = r20
            r12 = r19
            r13 = r32
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x0241
        L_0x0230:
            r2 = r0
        L_0x0231:
            com.google.android.gms.internal.ads.zzdqu r4 = zzal(r28)
            r0 = r17
            r1 = r29
            r3 = r31
            r5 = r32
            int r0 = com.google.android.gms.internal.ads.zzdmn.zza(r0, r1, r2, r3, r4, r5)
        L_0x0241:
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r11 = r32
            r9 = r18
            r2 = r19
            r1 = r24
        L_0x0251:
            r10 = -1
            goto L_0x0017
        L_0x0254:
            r4 = r13
            if (r0 != r4) goto L_0x0258
            return
        L_0x0258:
            com.google.android.gms.internal.ads.zzdok r0 = com.google.android.gms.internal.ads.zzdok.zzayj()
            throw r0
        L_0x025d:
            r4 = r13
            r5 = 0
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r30
            r4 = r31
            r6 = r32
            r0.zza(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpo.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.ads.zzdmo):void");
    }

    public final void zzaa(T t) {
        int i;
        int i2 = this.zzhka;
        while (true) {
            i = this.zzhkb;
            if (i2 >= i) {
                break;
            }
            long zzgu = (long) (zzgu(this.zzhjz[i2]) & 1048575);
            Object zzp = zzdqz.zzp(t, zzgu);
            if (zzp != null) {
                zzdqz.zza(t, zzgu, this.zzhkg.zzah(zzp));
            }
            i2++;
        }
        int length = this.zzhjz.length;
        while (i < length) {
            this.zzhkd.zzb(t, (long) this.zzhjz[i]);
            i++;
        }
        this.zzhke.zzaa(t);
        if (this.zzhjv) {
            this.zzhkf.zzaa(t);
        }
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzdqt<UT, UB> zzdqt) {
        zzdog zzgt;
        int i2 = this.zzhjq[i];
        Object zzp = zzdqz.zzp(obj, (long) (zzgu(i) & 1048575));
        if (zzp == null || (zzgt = zzgt(i)) == null) {
            return ub;
        }
        return zza(i, i2, this.zzhkg.zzae(zzp), zzgt, ub, zzdqt);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map map, zzdog zzdog, Object obj, zzdqt zzdqt) {
        zzdpd<?, ?> zzaj = this.zzhkg.zzaj(zzgs(i));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (!zzdog.zzf(((Integer) entry.getValue()).intValue())) {
                if (obj == null) {
                    obj = zzdqt.zzazy();
                }
                zzdmz zzfo = zzdmr.zzfo(zzdpc.zza(zzaj, entry.getKey(), entry.getValue()));
                try {
                    zzdpc.zza(zzfo.zzavt(), zzaj, entry.getKey(), entry.getValue());
                    zzdqt.zza(obj, i2, zzfo.zzavs());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return obj;
    }

    public final boolean zzam(T t) {
        int i;
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        while (true) {
            boolean z = true;
            if (i2 >= this.zzhka) {
                return !this.zzhjv || this.zzhkf.zzy(t).isInitialized();
            }
            int i5 = this.zzhjz[i2];
            int i6 = this.zzhjq[i5];
            int zzgu = zzgu(i5);
            if (!this.zzhjx) {
                int i7 = this.zzhjq[i5 + 2];
                int i8 = i7 & 1048575;
                i = 1 << (i7 >>> 20);
                if (i8 != i3) {
                    i4 = zzhjp.getInt(t, (long) i8);
                    i3 = i8;
                }
            } else {
                i = 0;
            }
            if (((268435456 & zzgu) != 0) && !zza(t, i5, i4, i)) {
                return false;
            }
            int i9 = (267386880 & zzgu) >>> 20;
            if (i9 != 9 && i9 != 17) {
                if (i9 != 27) {
                    if (i9 == 60 || i9 == 68) {
                        if (zza(t, i6, i5) && !zza(t, zzgu, zzgr(i5))) {
                            return false;
                        }
                    } else if (i9 != 49) {
                        if (i9 != 50) {
                            continue;
                        } else {
                            Map<?, ?> zzaf = this.zzhkg.zzaf(zzdqz.zzp(t, (long) (zzgu & 1048575)));
                            if (!zzaf.isEmpty()) {
                                if (this.zzhkg.zzaj(zzgs(i5)).zzhjk.zzbaj() == zzdrn.MESSAGE) {
                                    zzdqb zzdqb = null;
                                    Iterator<?> it = zzaf.values().iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        Object next = it.next();
                                        if (zzdqb == null) {
                                            zzdqb = zzdpx.zzazg().zzg(next.getClass());
                                        }
                                        if (!zzdqb.zzam(next)) {
                                            z = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!z) {
                                return false;
                            }
                        }
                    }
                }
                List list = (List) zzdqz.zzp(t, (long) (zzgu & 1048575));
                if (!list.isEmpty()) {
                    zzdqb zzgr = zzgr(i5);
                    int i10 = 0;
                    while (true) {
                        if (i10 >= list.size()) {
                            break;
                        } else if (!zzgr.zzam(list.get(i10))) {
                            z = false;
                            break;
                        } else {
                            i10++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i5, i4, i) && !zza(t, zzgu, zzgr(i5))) {
                return false;
            }
            i2++;
        }
    }

    private static boolean zza(Object obj, int i, zzdqb zzdqb) {
        return zzdqb.zzam(zzdqz.zzp(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzdro zzdro) throws IOException {
        if (obj instanceof String) {
            zzdro.zzf(i, (String) obj);
        } else {
            zzdro.zza(i, (zzdmr) obj);
        }
    }

    private final void zza(Object obj, int i, zzdqa zzdqa) throws IOException {
        if (zzgw(i)) {
            zzdqz.zza(obj, (long) (i & 1048575), zzdqa.zzawb());
        } else if (this.zzhjw) {
            zzdqz.zza(obj, (long) (i & 1048575), zzdqa.readString());
        } else {
            zzdqz.zza(obj, (long) (i & 1048575), zzdqa.zzawc());
        }
    }

    private final int zzgu(int i) {
        return this.zzhjq[i + 1];
    }

    private final int zzgv(int i) {
        return this.zzhjq[i + 2];
    }

    private static <T> double zzf(T t, long j) {
        return ((Double) zzdqz.zzp(t, j)).doubleValue();
    }

    private static <T> float zzg(T t, long j) {
        return ((Float) zzdqz.zzp(t, j)).floatValue();
    }

    private static <T> int zzh(T t, long j) {
        return ((Integer) zzdqz.zzp(t, j)).intValue();
    }

    private static <T> long zzi(T t, long j) {
        return ((Long) zzdqz.zzp(t, j)).longValue();
    }

    private static <T> boolean zzj(T t, long j) {
        return ((Boolean) zzdqz.zzp(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zzd(t, i) == zzd(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3) {
        if (this.zzhjx) {
            return zzd(t, i);
        }
        return (i2 & i3) != 0;
    }

    private final boolean zzd(T t, int i) {
        if (this.zzhjx) {
            int zzgu = zzgu(i);
            long j = (long) (zzgu & 1048575);
            switch ((zzgu & 267386880) >>> 20) {
                case 0:
                    return zzdqz.zzo(t, j) != 0.0d;
                case 1:
                    return zzdqz.zzn(t, j) != 0.0f;
                case 2:
                    return zzdqz.zzl(t, j) != 0;
                case 3:
                    return zzdqz.zzl(t, j) != 0;
                case 4:
                    return zzdqz.zzk(t, j) != 0;
                case 5:
                    return zzdqz.zzl(t, j) != 0;
                case 6:
                    return zzdqz.zzk(t, j) != 0;
                case 7:
                    return zzdqz.zzm(t, j);
                case 8:
                    Object zzp = zzdqz.zzp(t, j);
                    if (zzp instanceof String) {
                        return !((String) zzp).isEmpty();
                    }
                    if (zzp instanceof zzdmr) {
                        return !zzdmr.zzhcr.equals(zzp);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzdqz.zzp(t, j) != null;
                case 10:
                    return !zzdmr.zzhcr.equals(zzdqz.zzp(t, j));
                case 11:
                    return zzdqz.zzk(t, j) != 0;
                case 12:
                    return zzdqz.zzk(t, j) != 0;
                case 13:
                    return zzdqz.zzk(t, j) != 0;
                case 14:
                    return zzdqz.zzl(t, j) != 0;
                case 15:
                    return zzdqz.zzk(t, j) != 0;
                case 16:
                    return zzdqz.zzl(t, j) != 0;
                case 17:
                    return zzdqz.zzp(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int zzgv = zzgv(i);
            return (zzdqz.zzk(t, (long) (zzgv & 1048575)) & (1 << (zzgv >>> 20))) != 0;
        }
    }

    private final void zze(T t, int i) {
        if (!this.zzhjx) {
            int zzgv = zzgv(i);
            long j = (long) (zzgv & 1048575);
            zzdqz.zzb(t, j, zzdqz.zzk(t, j) | (1 << (zzgv >>> 20)));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzdqz.zzk(t, (long) (zzgv(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzdqz.zzb(t, (long) (zzgv(i2) & 1048575), i);
    }

    private final int zzgx(int i) {
        if (i < this.zzhjs || i > this.zzhjt) {
            return -1;
        }
        return zzal(i, 0);
    }

    private final int zzak(int i, int i2) {
        if (i < this.zzhjs || i > this.zzhjt) {
            return -1;
        }
        return zzal(i, i2);
    }

    private final int zzal(int i, int i2) {
        int length = (this.zzhjq.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.zzhjq[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
