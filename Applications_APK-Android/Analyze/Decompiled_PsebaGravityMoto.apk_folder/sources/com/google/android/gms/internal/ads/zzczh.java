package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Callable;

public final class zzczh {
    private final E zzgme;
    private final List<zzbbh<?>> zzgmj;
    private final /* synthetic */ zzczf zzgmk;

    private zzczh(zzczf zzczf, E e, List<zzbbh<?>> list) {
        this.zzgmk = zzczf;
        this.zzgme = e;
        this.zzgmj = list;
    }

    public final <O> zzczl<O> zzc(Callable<O> callable) {
        zzbbc zzf = zzbar.zzf(this.zzgmj);
        zzbbh zza = zzf.zza(zzczi.zzghi, zzbbm.zzeaf);
        zzczf zzczf = this.zzgmk;
        return new zzczl(zzczf, this.zzgme, zza, this.zzgmj, zzf.zza(callable, zzczf.zzfqw));
    }
}
