package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zze;

final class zzbl extends zzbm {
    private final /* synthetic */ boolean zzjg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbl(zzbe zzbe, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient);
        this.zzjg = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza((BaseImplementation.ResultHolder<Players.LoadPlayersResult>) this, this.zzjg);
    }
}
