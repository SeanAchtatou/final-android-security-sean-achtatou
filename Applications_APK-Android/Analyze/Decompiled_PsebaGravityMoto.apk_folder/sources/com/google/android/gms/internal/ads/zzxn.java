package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzwt;
import java.io.IOException;

public final class zzxn extends zzdrr<zzxn> {
    private Integer zzcfc = null;
    public String zzcfd = null;
    private Integer zzcfe = null;
    private zzwx zzcff = null;
    private zzxo zzcfg = null;
    public long[] zzcfh = zzdry.zzhoc;
    public zzxl zzcfi = null;
    private zzxm zzcfj = null;
    private zzwt.zzg zzcfk = null;
    public zzxj zzcfl = null;
    public zzwt.zzi zzcfm = null;
    public zzwt.zzv zzcfn = null;

    public zzxn() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        String str = this.zzcfd;
        if (str != null) {
            zzdrp.zzf(10, str);
        }
        long[] jArr = this.zzcfh;
        if (jArr != null && jArr.length > 0) {
            int i = 0;
            while (true) {
                long[] jArr2 = this.zzcfh;
                if (i >= jArr2.length) {
                    break;
                }
                long j = jArr2[i];
                zzdrp.zzw(14, 0);
                zzdrp.zzfv(j);
                i++;
            }
        }
        zzxl zzxl = this.zzcfi;
        if (zzxl != null) {
            zzdrp.zza(15, zzxl);
        }
        zzxj zzxj = this.zzcfl;
        if (zzxj != null) {
            zzdrp.zza(18, zzxj);
        }
        zzwt.zzi zzi = this.zzcfm;
        if (zzi != null) {
            zzdrp.zze(19, zzi);
        }
        zzwt.zzv zzv = this.zzcfn;
        if (zzv != null) {
            zzdrp.zze(20, zzv);
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        long[] jArr;
        int zzor = super.zzor();
        String str = this.zzcfd;
        if (str != null) {
            zzor += zzdrp.zzg(10, str);
        }
        long[] jArr2 = this.zzcfh;
        if (jArr2 != null && jArr2.length > 0) {
            int i = 0;
            int i2 = 0;
            while (true) {
                jArr = this.zzcfh;
                if (i >= jArr.length) {
                    break;
                }
                i2 += zzdrp.zzfw(jArr[i]);
                i++;
            }
            zzor = zzor + i2 + (jArr.length * 1);
        }
        zzxl zzxl = this.zzcfi;
        if (zzxl != null) {
            zzor += zzdrp.zzb(15, zzxl);
        }
        zzxj zzxj = this.zzcfl;
        if (zzxj != null) {
            zzor += zzdrp.zzb(18, zzxj);
        }
        zzwt.zzi zzi = this.zzcfm;
        if (zzi != null) {
            zzor += zzdni.zzc(19, zzi);
        }
        zzwt.zzv zzv = this.zzcfn;
        return zzv != null ? zzor + zzdni.zzc(20, zzv) : zzor;
    }
}
