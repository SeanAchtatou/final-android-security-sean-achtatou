package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.quest.Quests;

abstract class zzbt extends Games.zza<Quests.AcceptQuestResult> {
    private zzbt(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzbt(GoogleApiClient googleApiClient, zzbp zzbp) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzbu(this, status);
    }
}
