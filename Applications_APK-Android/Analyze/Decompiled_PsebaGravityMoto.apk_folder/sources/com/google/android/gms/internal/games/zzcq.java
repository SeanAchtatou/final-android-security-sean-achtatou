package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.snapshot.Snapshots;

abstract class zzcq extends Games.zza<Snapshots.DeleteSnapshotResult> {
    private zzcq(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzcq(GoogleApiClient googleApiClient, zzcj zzcj) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzcr(this, status);
    }
}
