package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdha;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.http.protocol.HTTP;

public final class zzdca<P> {
    private static final Charset UTF_8 = Charset.forName(HTTP.UTF_8);
    private final Class<P> zzgpd;
    private ConcurrentMap<String, List<zzdcb<P>>> zzgpj = new ConcurrentHashMap();
    private zzdcb<P> zzgpk;

    public final zzdcb<P> zzanu() {
        return this.zzgpk;
    }

    private zzdca(Class<P> cls) {
        this.zzgpd = cls;
    }

    public static <P> zzdca<P> zza(Class cls) {
        return new zzdca<>(cls);
    }

    public final void zza(zzdcb zzdcb) {
        this.zzgpk = zzdcb;
    }

    public final zzdcb<P> zza(P p, zzdha.zzb zzb) throws GeneralSecurityException {
        byte[] bArr;
        int i = zzdbn.zzgpa[zzb.zzanw().ordinal()];
        if (i == 1 || i == 2) {
            bArr = ByteBuffer.allocate(5).put((byte) 0).putInt(zzb.zzasp()).array();
        } else if (i == 3) {
            bArr = ByteBuffer.allocate(5).put((byte) 1).putInt(zzb.zzasp()).array();
        } else if (i == 4) {
            bArr = zzdbm.zzgoz;
        } else {
            throw new GeneralSecurityException("unknown output prefix type");
        }
        zzdcb<P> zzdcb = new zzdcb<>(p, bArr, zzb.zzaso(), zzb.zzanw());
        ArrayList arrayList = new ArrayList();
        arrayList.add(zzdcb);
        String str = new String(zzdcb.zzanx(), UTF_8);
        List put = this.zzgpj.put(str, Collections.unmodifiableList(arrayList));
        if (put != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(put);
            arrayList2.add(zzdcb);
            this.zzgpj.put(str, Collections.unmodifiableList(arrayList2));
        }
        return zzdcb;
    }

    public final Class<P> zzanr() {
        return this.zzgpd;
    }
}
