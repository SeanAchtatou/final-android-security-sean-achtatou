package com.google.android.gms.games;

import androidx.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzbz implements PendingResultUtil.ResultConverter<Snapshots.DeleteSnapshotResult, String> {
    zzbz() {
    }

    public final /* synthetic */ Object convert(@Nullable Result result) {
        Snapshots.DeleteSnapshotResult deleteSnapshotResult = (Snapshots.DeleteSnapshotResult) result;
        if (deleteSnapshotResult == null) {
            return null;
        }
        return deleteSnapshotResult.getSnapshotId();
    }
}
