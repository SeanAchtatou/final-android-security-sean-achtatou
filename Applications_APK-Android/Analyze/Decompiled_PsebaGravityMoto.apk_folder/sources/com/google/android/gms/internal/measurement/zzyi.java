package com.google.android.gms.internal.measurement;

import java.io.IOException;

public abstract class zzyi {
    protected volatile int zzcff = -1;

    public abstract zzyi zza(zzxz zzxz) throws IOException;

    public void zza(zzya zzya) throws IOException {
    }

    /* access modifiers changed from: protected */
    public int zzf() {
        return 0;
    }

    public final int zzzh() {
        if (this.zzcff < 0) {
            zzvx();
        }
        return this.zzcff;
    }

    public final int zzvx() {
        int zzf = zzf();
        this.zzcff = zzf;
        return zzf;
    }

    public static final void zza(zzyi zzyi, byte[] bArr, int i, int i2) {
        try {
            zzya zzk = zzya.zzk(bArr, 0, i2);
            zzyi.zza(zzk);
            zzk.zzza();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final <T extends zzyi> T zza(T t, byte[] bArr) throws zzyh {
        return zzb(t, bArr, 0, bArr.length);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static final <T extends com.google.android.gms.internal.measurement.zzyi> T zzb(T r0, byte[] r1, int r2, int r3) throws com.google.android.gms.internal.measurement.zzyh {
        /*
            r2 = 0
            com.google.android.gms.internal.measurement.zzxz r1 = com.google.android.gms.internal.measurement.zzxz.zzj(r1, r2, r3)     // Catch:{ zzyh -> 0x0015, IOException -> 0x000c }
            r0.zza(r1)     // Catch:{ zzyh -> 0x0015, IOException -> 0x000c }
            r1.zzap(r2)     // Catch:{ zzyh -> 0x0015, IOException -> 0x000c }
            return r0
        L_0x000c:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "Reading from a byte array threw an IOException (should never happen)."
            r1.<init>(r2, r0)
            throw r1
        L_0x0015:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzyi.zzb(com.google.android.gms.internal.measurement.zzyi, byte[], int, int):com.google.android.gms.internal.measurement.zzyi");
    }

    public String toString() {
        return zzyj.zzc(this);
    }

    /* renamed from: zzzb */
    public zzyi clone() throws CloneNotSupportedException {
        return (zzyi) super.clone();
    }
}
