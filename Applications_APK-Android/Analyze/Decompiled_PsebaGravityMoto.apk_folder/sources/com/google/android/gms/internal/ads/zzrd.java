package com.google.android.gms.internal.ads;

import java.util.Arrays;

public class zzrd implements zzrm {
    private final int length;
    private int zzafx;
    private final zzlh[] zzbju;
    private final zzra zzbky;
    private final int[] zzbkz;
    private final long[] zzbla;

    public zzrd(zzra zzra, int... iArr) {
        int i = 0;
        zzsk.checkState(iArr.length > 0);
        this.zzbky = (zzra) zzsk.checkNotNull(zzra);
        this.length = iArr.length;
        this.zzbju = new zzlh[this.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            this.zzbju[i2] = zzra.zzbf(iArr[i2]);
        }
        Arrays.sort(this.zzbju, new zzrf());
        this.zzbkz = new int[this.length];
        while (true) {
            int i3 = this.length;
            if (i < i3) {
                this.zzbkz[i] = zzra.zzh(this.zzbju[i]);
                i++;
            } else {
                this.zzbla = new long[i3];
                return;
            }
        }
    }

    public final zzra zzjr() {
        return this.zzbky;
    }

    public final int length() {
        return this.zzbkz.length;
    }

    public final zzlh zzbf(int i) {
        return this.zzbju[i];
    }

    public final int zzbh(int i) {
        return this.zzbkz[0];
    }

    public int hashCode() {
        if (this.zzafx == 0) {
            this.zzafx = (System.identityHashCode(this.zzbky) * 31) + Arrays.hashCode(this.zzbkz);
        }
        return this.zzafx;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzrd zzrd = (zzrd) obj;
            return this.zzbky == zzrd.zzbky && Arrays.equals(this.zzbkz, zzrd.zzbkz);
        }
    }
}
