package com.google.android.gms.internal.ads;

import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.HashSet;

@zzard
public final class zzaog {
    public static int zza(AdRequest.ErrorCode errorCode) {
        int i = zzaoh.zzdha[errorCode.ordinal()];
        if (i == 2) {
            return 1;
        }
        if (i != 3) {
            return i != 4 ? 0 : 3;
        }
        return 2;
    }

    public static MediationAdRequest zza(zzxz zzxz, boolean z) {
        AdRequest.Gender gender;
        HashSet hashSet = zzxz.zzcgp != null ? new HashSet(zzxz.zzcgp) : null;
        Date date = new Date(zzxz.zzcgn);
        int i = zzxz.zzcgo;
        if (i == 1) {
            gender = AdRequest.Gender.MALE;
        } else if (i != 2) {
            gender = AdRequest.Gender.UNKNOWN;
        } else {
            gender = AdRequest.Gender.FEMALE;
        }
        return new MediationAdRequest(date, gender, hashSet, z, zzxz.zzmw);
    }
}
