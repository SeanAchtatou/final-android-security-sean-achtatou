package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

final class zzdpq<T> implements zzdqb<T> {
    private final zzdpk zzhju;
    private final boolean zzhjv;
    private final zzdqt<?, ?> zzhke;
    private final zzdnp<?> zzhkf;

    private zzdpq(zzdqt<?, ?> zzdqt, zzdnp<?> zzdnp, zzdpk zzdpk) {
        this.zzhke = zzdqt;
        this.zzhjv = zzdnp.zzm(zzdpk);
        this.zzhkf = zzdnp;
        this.zzhju = zzdpk;
    }

    static <T> zzdpq<T> zza(zzdqt<?, ?> zzdqt, zzdnp<?> zzdnp, zzdpk zzdpk) {
        return new zzdpq<>(zzdqt, zzdnp, zzdpk);
    }

    public final T newInstance() {
        return this.zzhju.zzaxu().zzaxz();
    }

    public final boolean equals(T t, T t2) {
        if (!this.zzhke.zzao(t).equals(this.zzhke.zzao(t2))) {
            return false;
        }
        if (this.zzhjv) {
            return this.zzhkf.zzy(t).equals(this.zzhkf.zzy(t2));
        }
        return true;
    }

    public final int hashCode(T t) {
        int hashCode = this.zzhke.zzao(t).hashCode();
        return this.zzhjv ? (hashCode * 53) + this.zzhkf.zzy(t).hashCode() : hashCode;
    }

    public final void zzd(T t, T t2) {
        zzdqd.zza(this.zzhke, t, t2);
        if (this.zzhjv) {
            zzdqd.zza(this.zzhkf, t, t2);
        }
    }

    public final void zza(T t, zzdro zzdro) throws IOException {
        Iterator<Map.Entry<?, Object>> it = this.zzhkf.zzy(t).iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            zzdnu zzdnu = (zzdnu) next.getKey();
            if (zzdnu.zzaxm() != zzdrn.MESSAGE || zzdnu.zzaxn() || zzdnu.zzaxo()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzdop) {
                zzdro.zzb(zzdnu.zzac(), ((zzdop) next).zzayn().zzavf());
            } else {
                zzdro.zzb(zzdnu.zzac(), next.getValue());
            }
        }
        zzdqt<?, ?> zzdqt = this.zzhke;
        zzdqt.zzc(zzdqt.zzao(t), zzdro);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: com.google.android.gms.internal.ads.zzdob$zzd} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r10, byte[] r11, int r12, int r13, com.google.android.gms.internal.ads.zzdmo r14) throws java.io.IOException {
        /*
            r9 = this;
            r0 = r10
            com.google.android.gms.internal.ads.zzdob r0 = (com.google.android.gms.internal.ads.zzdob) r0
            com.google.android.gms.internal.ads.zzdqu r1 = r0.zzhhd
            com.google.android.gms.internal.ads.zzdqu r2 = com.google.android.gms.internal.ads.zzdqu.zzazz()
            if (r1 != r2) goto L_0x0011
            com.google.android.gms.internal.ads.zzdqu r1 = com.google.android.gms.internal.ads.zzdqu.zzbaa()
            r0.zzhhd = r1
        L_0x0011:
            com.google.android.gms.internal.ads.zzdob$zzc r10 = (com.google.android.gms.internal.ads.zzdob.zzc) r10
            com.google.android.gms.internal.ads.zzdns<java.lang.Object> r0 = r10.zzhhj
            boolean r0 = r0.isImmutable()
            if (r0 == 0) goto L_0x0025
            com.google.android.gms.internal.ads.zzdns<java.lang.Object> r0 = r10.zzhhj
            java.lang.Object r0 = r0.clone()
            com.google.android.gms.internal.ads.zzdns r0 = (com.google.android.gms.internal.ads.zzdns) r0
            r10.zzhhj = r0
        L_0x0025:
            com.google.android.gms.internal.ads.zzdns<java.lang.Object> r10 = r10.zzhhj
            r10 = 0
            r0 = r10
        L_0x0029:
            if (r12 >= r13) goto L_0x00b5
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r11, r12, r14)
            int r2 = r14.zzhcl
            r12 = 11
            r3 = 2
            if (r2 == r12) goto L_0x0062
            r12 = r2 & 7
            if (r12 != r3) goto L_0x005d
            com.google.android.gms.internal.ads.zzdnp<?> r12 = r9.zzhkf
            com.google.android.gms.internal.ads.zzdno r0 = r14.zzhco
            com.google.android.gms.internal.ads.zzdpk r3 = r9.zzhju
            int r5 = r2 >>> 3
            java.lang.Object r12 = r12.zza(r0, r3, r5)
            r0 = r12
            com.google.android.gms.internal.ads.zzdob$zzd r0 = (com.google.android.gms.internal.ads.zzdob.zzd) r0
            if (r0 != 0) goto L_0x0054
            r3 = r11
            r5 = r13
            r6 = r1
            r7 = r14
            int r12 = com.google.android.gms.internal.ads.zzdmn.zza(r2, r3, r4, r5, r6, r7)
            goto L_0x0029
        L_0x0054:
            com.google.android.gms.internal.ads.zzdpx.zzazg()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x005d:
            int r12 = com.google.android.gms.internal.ads.zzdmn.zza(r2, r11, r4, r13, r14)
            goto L_0x0029
        L_0x0062:
            r12 = 0
            r2 = r10
        L_0x0064:
            if (r4 >= r13) goto L_0x00aa
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r11, r4, r14)
            int r5 = r14.zzhcl
            int r6 = r5 >>> 3
            r7 = r5 & 7
            if (r6 == r3) goto L_0x008c
            r8 = 3
            if (r6 == r8) goto L_0x0076
            goto L_0x00a1
        L_0x0076:
            if (r0 != 0) goto L_0x0083
            if (r7 != r3) goto L_0x00a1
            int r4 = com.google.android.gms.internal.ads.zzdmn.zze(r11, r4, r14)
            java.lang.Object r2 = r14.zzhcn
            com.google.android.gms.internal.ads.zzdmr r2 = (com.google.android.gms.internal.ads.zzdmr) r2
            goto L_0x0064
        L_0x0083:
            com.google.android.gms.internal.ads.zzdpx.zzazg()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x008c:
            if (r7 != 0) goto L_0x00a1
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r11, r4, r14)
            int r12 = r14.zzhcl
            com.google.android.gms.internal.ads.zzdnp<?> r0 = r9.zzhkf
            com.google.android.gms.internal.ads.zzdno r5 = r14.zzhco
            com.google.android.gms.internal.ads.zzdpk r6 = r9.zzhju
            java.lang.Object r0 = r0.zza(r5, r6, r12)
            com.google.android.gms.internal.ads.zzdob$zzd r0 = (com.google.android.gms.internal.ads.zzdob.zzd) r0
            goto L_0x0064
        L_0x00a1:
            r6 = 12
            if (r5 == r6) goto L_0x00aa
            int r4 = com.google.android.gms.internal.ads.zzdmn.zza(r5, r11, r4, r13, r14)
            goto L_0x0064
        L_0x00aa:
            if (r2 == 0) goto L_0x00b2
            int r12 = r12 << 3
            r12 = r12 | r3
            r1.zzc(r12, r2)
        L_0x00b2:
            r12 = r4
            goto L_0x0029
        L_0x00b5:
            if (r12 != r13) goto L_0x00b8
            return
        L_0x00b8:
            com.google.android.gms.internal.ads.zzdok r10 = com.google.android.gms.internal.ads.zzdok.zzayj()
            goto L_0x00be
        L_0x00bd:
            throw r10
        L_0x00be:
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdpq.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.ads.zzdmo):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, com.google.android.gms.internal.ads.zzdmr):void
     arg types: [?, int, com.google.android.gms.internal.ads.zzdmr]
     candidates:
      com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, long):void
      com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, com.google.android.gms.internal.ads.zzdmr):void */
    public final void zza(Object obj, zzdqa zzdqa, zzdno zzdno) throws IOException {
        boolean z;
        zzdqt<?, ?> zzdqt = this.zzhke;
        zzdnp<?> zzdnp = this.zzhkf;
        Object zzap = zzdqt.zzap(obj);
        zzdns<?> zzz = zzdnp.zzz(obj);
        do {
            try {
                if (zzdqa.zzaws() == Integer.MAX_VALUE) {
                    zzdqt.zzg(obj, zzap);
                    return;
                }
                int tag = zzdqa.getTag();
                if (tag == 11) {
                    int i = 0;
                    Object obj2 = null;
                    zzdmr zzdmr = null;
                    while (zzdqa.zzaws() != Integer.MAX_VALUE) {
                        int tag2 = zzdqa.getTag();
                        if (tag2 == 16) {
                            i = zzdqa.zzawd();
                            obj2 = zzdnp.zza(zzdno, this.zzhju, i);
                        } else if (tag2 == 26) {
                            if (obj2 != null) {
                                zzdnp.zza(zzdqa, obj2, zzdno, zzz);
                            } else {
                                zzdmr = zzdqa.zzawc();
                            }
                        } else if (!zzdqa.zzawt()) {
                            break;
                        }
                    }
                    if (zzdqa.getTag() != 12) {
                        throw zzdok.zzayh();
                    } else if (zzdmr != null) {
                        if (obj2 != null) {
                            zzdnp.zza(zzdmr, obj2, zzdno, zzz);
                        } else {
                            zzdqt.zza((Object) zzap, i, zzdmr);
                        }
                    }
                } else if ((tag & 7) == 2) {
                    Object zza = zzdnp.zza(zzdno, this.zzhju, tag >>> 3);
                    if (zza != null) {
                        zzdnp.zza(zzdqa, zza, zzdno, zzz);
                    } else {
                        z = zzdqt.zza(zzap, zzdqa);
                        continue;
                    }
                } else {
                    z = zzdqa.zzawt();
                    continue;
                }
                z = true;
                continue;
            } finally {
                zzdqt.zzg(obj, zzap);
            }
        } while (z);
    }

    public final void zzaa(T t) {
        this.zzhke.zzaa(t);
        this.zzhkf.zzaa(t);
    }

    public final boolean zzam(T t) {
        return this.zzhkf.zzy(t).isInitialized();
    }

    public final int zzak(T t) {
        zzdqt<?, ?> zzdqt = this.zzhke;
        int zzaq = zzdqt.zzaq(zzdqt.zzao(t)) + 0;
        return this.zzhjv ? zzaq + this.zzhkf.zzy(t).zzaxk() : zzaq;
    }
}
