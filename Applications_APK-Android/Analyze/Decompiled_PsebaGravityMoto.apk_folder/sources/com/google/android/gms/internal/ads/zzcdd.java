package com.google.android.gms.internal.ads;

import androidx.annotation.Nullable;

public final class zzcdd extends zzbvz {
    public zzcdd(zzbwz zzbwz) {
        super(zzbwz, null);
    }

    public zzcdd(zzbwz zzbwz, @Nullable zzbgz zzbgz) {
        super(zzbwz, zzbgz);
    }
}
