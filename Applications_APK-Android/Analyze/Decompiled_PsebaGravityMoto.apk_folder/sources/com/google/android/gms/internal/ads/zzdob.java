package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdob;
import com.google.android.gms.internal.ads.zzdob.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class zzdob<MessageType extends zzdob<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzdmi<MessageType, BuilderType> {
    private static Map<Object, zzdob<?, ?>> zzhhf = new ConcurrentHashMap();
    protected zzdqu zzhhd = zzdqu.zzazz();
    private int zzhhe = -1;

    public static class zzb<T extends zzdob<T, ?>> extends zzdmk<T> {
        private final T zzhhg;

        public zzb(T t) {
            this.zzhhg = t;
        }
    }

    public static abstract class zzc<MessageType extends zzc<MessageType, BuilderType>, BuilderType> extends zzdob<MessageType, BuilderType> implements zzdpm {
        protected zzdns<Object> zzhhj = zzdns.zzaxi();
    }

    public static class zzd<ContainingType extends zzdpk, Type> extends zzdnm<ContainingType, Type> {
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class zze {
        public static final int zzhhk = 1;
        public static final int zzhhl = 2;
        public static final int zzhhm = 3;
        public static final int zzhhn = 4;
        public static final int zzhho = 5;
        public static final int zzhhp = 6;
        public static final int zzhhq = 7;
        private static final /* synthetic */ int[] zzhhr = {zzhhk, zzhhl, zzhhm, zzhhn, zzhho, zzhhp, zzhhq};
        public static final int zzhhs = 1;
        public static final int zzhht = 2;
        private static final /* synthetic */ int[] zzhhu = {zzhhs, zzhht};
        public static final int zzhhv = 1;
        public static final int zzhhw = 2;
        private static final /* synthetic */ int[] zzhhx = {zzhhv, zzhhw};

        public static int[] zzayb() {
            return (int[]) zzhhr.clone();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    public String toString() {
        return zzdpn.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzhcf != 0) {
            return this.zzhcf;
        }
        this.zzhcf = zzdpx.zzazg().zzan(this).hashCode(this);
        return this.zzhcf;
    }

    public static abstract class zza<MessageType extends zzdob<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzdmj<MessageType, BuilderType> {
        private final MessageType zzhhg;
        protected MessageType zzhhh;
        private boolean zzhhi = false;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected zza(MessageType r3) {
            /*
                r2 = this;
                r2.<init>()
                r2.zzhhg = r3
                int r0 = com.google.android.gms.internal.ads.zzdob.zze.zzhhn
                r1 = 0
                java.lang.Object r3 = r3.zza(r0, r1, r1)
                com.google.android.gms.internal.ads.zzdob r3 = (com.google.android.gms.internal.ads.zzdob) r3
                r2.zzhhh = r3
                r3 = 0
                r2.zzhhi = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdob.zza.<init>(com.google.android.gms.internal.ads.zzdob):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected final void zzaxw() {
            /*
                r3 = this;
                boolean r0 = r3.zzhhi
                if (r0 == 0) goto L_0x0019
                MessageType r0 = r3.zzhhh
                int r1 = com.google.android.gms.internal.ads.zzdob.zze.zzhhn
                r2 = 0
                java.lang.Object r0 = r0.zza(r1, r2, r2)
                com.google.android.gms.internal.ads.zzdob r0 = (com.google.android.gms.internal.ads.zzdob) r0
                MessageType r1 = r3.zzhhh
                zza(r0, r1)
                r3.zzhhh = r0
                r0 = 0
                r3.zzhhi = r0
            L_0x0019:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdob.zza.zzaxw():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, com.google.android.gms.internal.ads.zzdmr):T
          com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, byte[]):T
          com.google.android.gms.internal.ads.zzdob.zza(java.lang.Class, com.google.android.gms.internal.ads.zzdob):void
          com.google.android.gms.internal.ads.zzdmi.zza(java.lang.Iterable, java.util.List):void
          com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, boolean):boolean */
        public final boolean isInitialized() {
            return zzdob.zza((zzdob) this.zzhhh, false);
        }

        /* renamed from: zzaxx */
        public MessageType zzaxz() {
            if (this.zzhhi) {
                return this.zzhhh;
            }
            MessageType messagetype = this.zzhhh;
            zzdpx.zzazg().zzan(messagetype).zzaa(messagetype);
            this.zzhhi = true;
            return this.zzhhh;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        /* renamed from: zzaxy */
        public final MessageType zzaya() {
            /*
                r5 = this;
                com.google.android.gms.internal.ads.zzdpk r0 = r5.zzaxz()
                com.google.android.gms.internal.ads.zzdob r0 = (com.google.android.gms.internal.ads.zzdob) r0
                java.lang.Boolean r1 = java.lang.Boolean.TRUE
                boolean r1 = r1.booleanValue()
                int r2 = com.google.android.gms.internal.ads.zzdob.zze.zzhhk
                r3 = 0
                java.lang.Object r2 = r0.zza(r2, r3, r3)
                java.lang.Byte r2 = (java.lang.Byte) r2
                byte r2 = r2.byteValue()
                r4 = 1
                if (r2 != r4) goto L_0x001d
                goto L_0x0039
            L_0x001d:
                if (r2 != 0) goto L_0x0021
                r4 = 0
                goto L_0x0039
            L_0x0021:
                com.google.android.gms.internal.ads.zzdpx r2 = com.google.android.gms.internal.ads.zzdpx.zzazg()
                com.google.android.gms.internal.ads.zzdqb r2 = r2.zzan(r0)
                boolean r4 = r2.zzam(r0)
                if (r1 == 0) goto L_0x0039
                int r1 = com.google.android.gms.internal.ads.zzdob.zze.zzhhl
                if (r4 == 0) goto L_0x0035
                r2 = r0
                goto L_0x0036
            L_0x0035:
                r2 = r3
            L_0x0036:
                r0.zza(r1, r2, r3)
            L_0x0039:
                if (r4 == 0) goto L_0x003c
                return r0
            L_0x003c:
                com.google.android.gms.internal.ads.zzdqs r1 = new com.google.android.gms.internal.ads.zzdqs
                r1.<init>(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdob.zza.zzaya():com.google.android.gms.internal.ads.zzdob");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.ads.zzdob.zza.zza(com.google.android.gms.internal.ads.zzdob, com.google.android.gms.internal.ads.zzdob):void
         arg types: [MessageType, MessageType]
         candidates:
          com.google.android.gms.internal.ads.zzdob.zza.zza(com.google.android.gms.internal.ads.zzdnd, com.google.android.gms.internal.ads.zzdno):com.google.android.gms.internal.ads.zzdmj
          com.google.android.gms.internal.ads.zzdmj.zza(com.google.android.gms.internal.ads.zzdnd, com.google.android.gms.internal.ads.zzdno):BuilderType
          com.google.android.gms.internal.ads.zzdob.zza.zza(com.google.android.gms.internal.ads.zzdob, com.google.android.gms.internal.ads.zzdob):void */
        public final BuilderType zza(MessageType messagetype) {
            zzaxw();
            zza((zzdob) this.zzhhh, (zzdob) messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzdpx.zzazg().zzan(messagetype).zzd(messagetype, messagetype2);
        }

        private final BuilderType zzb(byte[] bArr, int i, int i2, zzdno zzdno) throws zzdok {
            zzaxw();
            try {
                zzdpx.zzazg().zzan(this.zzhhh).zza(this.zzhhh, bArr, 0, i2 + 0, new zzdmo(zzdno));
                return this;
            } catch (zzdok e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw zzdok.zzayd();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: zzb */
        public final BuilderType zza(zzdnd zzdnd, zzdno zzdno) throws IOException {
            zzaxw();
            try {
                zzdpx.zzazg().zzan(this.zzhhh).zza(this.zzhhh, zzdng.zza(zzdnd), zzdno);
                return this;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw ((IOException) e.getCause());
                }
                throw e;
            }
        }

        public final /* synthetic */ zzdmj zza(byte[] bArr, int i, int i2, zzdno zzdno) throws zzdok {
            return zzb(bArr, 0, i2, zzdno);
        }

        public final /* synthetic */ zzdmj zzavh() {
            return (zza) clone();
        }

        public final /* synthetic */ zzdpk zzaxv() {
            return this.zzhhg;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zza zza = (zza) ((zzdob) this.zzhhg).zza(zze.zzhho, (Object) null, (Object) null);
            zza.zza((zzdob) zzaxz());
            return zza;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzdob) zza(zze.zzhhp, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return zzdpx.zzazg().zzan(this).equals(this, (zzdob) obj);
    }

    public final boolean isInitialized() {
        boolean booleanValue = Boolean.TRUE.booleanValue();
        byte byteValue = ((Byte) zza(zze.zzhhk, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzam = zzdpx.zzazg().zzan(this).zzam(this);
        if (booleanValue) {
            zza(zze.zzhhl, zzam ? this : null, (Object) null);
        }
        return zzam;
    }

    /* access modifiers changed from: package-private */
    public final int zzavg() {
        return this.zzhhe;
    }

    /* access modifiers changed from: package-private */
    public final void zzfi(int i) {
        this.zzhhe = i;
    }

    public final void zzb(zzdni zzdni) throws IOException {
        zzdpx.zzazg().zzg(getClass()).zza(this, zzdnk.zza(zzdni));
    }

    public final int zzaxj() {
        if (this.zzhhe == -1) {
            this.zzhhe = zzdpx.zzazg().zzan(this).zzak(this);
        }
        return this.zzhhe;
    }

    static <T extends zzdob<?, ?>> T zze(Class<T> cls) {
        T t = (zzdob) zzhhf.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzdob) zzhhf.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzdob) ((zzdob) zzdqz.zzi(cls)).zza(zze.zzhhp, (Object) null, (Object) null);
            if (t != null) {
                zzhhf.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends zzdob<?, ?>> void zza(Class cls, zzdob zzdob) {
        zzhhf.put(cls, zzdob);
    }

    protected static Object zza(zzdpk zzdpk, String str, Object[] objArr) {
        return new zzdpz(zzdpk, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static final <T extends zzdob<T, ?>> boolean zza(zzdob zzdob, boolean z) {
        byte byteValue = ((Byte) zzdob.zza(zze.zzhhk, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        return zzdpx.zzazg().zzan(zzdob).zzam(zzdob);
    }

    protected static zzdoh zzaxr() {
        return zzdoc.zzayc();
    }

    protected static <E> zzdoj<E> zzaxs() {
        return zzdpy.zzazh();
    }

    private static <T extends zzdob<T, ?>> T zza(zzdob zzdob, zzdnd zzdnd, zzdno zzdno) throws zzdok {
        T t = (zzdob) zzdob.zza(zze.zzhhn, (Object) null, (Object) null);
        try {
            zzdpx.zzazg().zzan(t).zza(t, zzdng.zza(zzdnd), zzdno);
            zzdpx.zzazg().zzan(t).zzaa(t);
            return t;
        } catch (IOException e) {
            if (e.getCause() instanceof zzdok) {
                throw ((zzdok) e.getCause());
            }
            throw new zzdok(e.getMessage()).zzo(t);
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof zzdok) {
                throw ((zzdok) e2.getCause());
            }
            throw e2;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static <T extends com.google.android.gms.internal.ads.zzdob<T, ?>> T zza(T r6, byte[] r7, int r8, int r9, com.google.android.gms.internal.ads.zzdno r10) throws com.google.android.gms.internal.ads.zzdok {
        /*
            int r8 = com.google.android.gms.internal.ads.zzdob.zze.zzhhn
            r0 = 0
            java.lang.Object r6 = r6.zza(r8, r0, r0)
            com.google.android.gms.internal.ads.zzdob r6 = (com.google.android.gms.internal.ads.zzdob) r6
            com.google.android.gms.internal.ads.zzdpx r8 = com.google.android.gms.internal.ads.zzdpx.zzazg()     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            com.google.android.gms.internal.ads.zzdqb r0 = r8.zzan(r6)     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            r3 = 0
            com.google.android.gms.internal.ads.zzdmo r5 = new com.google.android.gms.internal.ads.zzdmo     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            r5.<init>(r10)     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            r1 = r6
            r2 = r7
            r4 = r9
            r0.zza(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            com.google.android.gms.internal.ads.zzdpx r7 = com.google.android.gms.internal.ads.zzdpx.zzazg()     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            com.google.android.gms.internal.ads.zzdqb r7 = r7.zzan(r6)     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            r7.zzaa(r6)     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            int r7 = r6.zzhcf     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            if (r7 != 0) goto L_0x002d
            return r6
        L_0x002d:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            r7.<init>()     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
            throw r7     // Catch:{ IOException -> 0x003c, IndexOutOfBoundsException -> 0x0033 }
        L_0x0033:
            com.google.android.gms.internal.ads.zzdok r7 = com.google.android.gms.internal.ads.zzdok.zzayd()
            com.google.android.gms.internal.ads.zzdok r6 = r7.zzo(r6)
            throw r6
        L_0x003c:
            r7 = move-exception
            java.lang.Throwable r8 = r7.getCause()
            boolean r8 = r8 instanceof com.google.android.gms.internal.ads.zzdok
            if (r8 == 0) goto L_0x004c
            java.lang.Throwable r6 = r7.getCause()
            com.google.android.gms.internal.ads.zzdok r6 = (com.google.android.gms.internal.ads.zzdok) r6
            throw r6
        L_0x004c:
            com.google.android.gms.internal.ads.zzdok r8 = new com.google.android.gms.internal.ads.zzdok
            java.lang.String r7 = r7.getMessage()
            r8.<init>(r7)
            com.google.android.gms.internal.ads.zzdok r6 = r8.zzo(r6)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, byte[], int, int, com.google.android.gms.internal.ads.zzdno):com.google.android.gms.internal.ads.zzdob");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static <T extends com.google.android.gms.internal.ads.zzdob<T, ?>> T zza(com.google.android.gms.internal.ads.zzdob r5, com.google.android.gms.internal.ads.zzdmr r6) throws com.google.android.gms.internal.ads.zzdok {
        /*
            com.google.android.gms.internal.ads.zzdno r0 = com.google.android.gms.internal.ads.zzdno.zzaxd()
            com.google.android.gms.internal.ads.zzdob r5 = zza(r5, r6, r0)
            r6 = 0
            r0 = 1
            r1 = 0
            if (r5 == 0) goto L_0x0050
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            boolean r2 = r2.booleanValue()
            int r3 = com.google.android.gms.internal.ads.zzdob.zze.zzhhk
            java.lang.Object r3 = r5.zza(r3, r1, r1)
            java.lang.Byte r3 = (java.lang.Byte) r3
            byte r3 = r3.byteValue()
            if (r3 != r0) goto L_0x0023
            r3 = 1
            goto L_0x003f
        L_0x0023:
            if (r3 != 0) goto L_0x0027
            r3 = 0
            goto L_0x003f
        L_0x0027:
            com.google.android.gms.internal.ads.zzdpx r3 = com.google.android.gms.internal.ads.zzdpx.zzazg()
            com.google.android.gms.internal.ads.zzdqb r3 = r3.zzan(r5)
            boolean r3 = r3.zzam(r5)
            if (r2 == 0) goto L_0x003f
            int r2 = com.google.android.gms.internal.ads.zzdob.zze.zzhhl
            if (r3 == 0) goto L_0x003b
            r4 = r5
            goto L_0x003c
        L_0x003b:
            r4 = r1
        L_0x003c:
            r5.zza(r2, r4, r1)
        L_0x003f:
            if (r3 == 0) goto L_0x0042
            goto L_0x0050
        L_0x0042:
            com.google.android.gms.internal.ads.zzdqs r6 = new com.google.android.gms.internal.ads.zzdqs
            r6.<init>(r5)
            com.google.android.gms.internal.ads.zzdok r6 = r6.zzazx()
            com.google.android.gms.internal.ads.zzdok r5 = r6.zzo(r5)
            throw r5
        L_0x0050:
            if (r5 == 0) goto L_0x0094
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            boolean r2 = r2.booleanValue()
            int r3 = com.google.android.gms.internal.ads.zzdob.zze.zzhhk
            java.lang.Object r3 = r5.zza(r3, r1, r1)
            java.lang.Byte r3 = (java.lang.Byte) r3
            byte r3 = r3.byteValue()
            if (r3 != r0) goto L_0x0068
            r6 = 1
            goto L_0x0083
        L_0x0068:
            if (r3 != 0) goto L_0x006b
            goto L_0x0083
        L_0x006b:
            com.google.android.gms.internal.ads.zzdpx r6 = com.google.android.gms.internal.ads.zzdpx.zzazg()
            com.google.android.gms.internal.ads.zzdqb r6 = r6.zzan(r5)
            boolean r6 = r6.zzam(r5)
            if (r2 == 0) goto L_0x0083
            int r0 = com.google.android.gms.internal.ads.zzdob.zze.zzhhl
            if (r6 == 0) goto L_0x007f
            r2 = r5
            goto L_0x0080
        L_0x007f:
            r2 = r1
        L_0x0080:
            r5.zza(r0, r2, r1)
        L_0x0083:
            if (r6 == 0) goto L_0x0086
            goto L_0x0094
        L_0x0086:
            com.google.android.gms.internal.ads.zzdqs r6 = new com.google.android.gms.internal.ads.zzdqs
            r6.<init>(r5)
            com.google.android.gms.internal.ads.zzdok r6 = r6.zzazx()
            com.google.android.gms.internal.ads.zzdok r5 = r6.zzo(r5)
            throw r5
        L_0x0094:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, com.google.android.gms.internal.ads.zzdmr):com.google.android.gms.internal.ads.zzdob");
    }

    private static <T extends zzdob<T, ?>> T zza(zzdob zzdob, zzdmr zzdmr, zzdno zzdno) throws zzdok {
        T zza2;
        try {
            zzdnd zzavp = zzdmr.zzavp();
            zza2 = zza(zzdob, zzavp, zzdno);
            zzavp.zzfp(0);
            return zza2;
        } catch (zzdok e) {
            throw e.zzo(zza2);
        } catch (zzdok e2) {
            throw e2;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static <T extends com.google.android.gms.internal.ads.zzdob<T, ?>> T zza(com.google.android.gms.internal.ads.zzdob r4, byte[] r5) throws com.google.android.gms.internal.ads.zzdok {
        /*
            int r0 = r5.length
            com.google.android.gms.internal.ads.zzdno r1 = com.google.android.gms.internal.ads.zzdno.zzaxd()
            r2 = 0
            com.google.android.gms.internal.ads.zzdob r4 = zza(r4, r5, r2, r0, r1)
            if (r4 == 0) goto L_0x0050
            java.lang.Boolean r5 = java.lang.Boolean.TRUE
            boolean r5 = r5.booleanValue()
            int r0 = com.google.android.gms.internal.ads.zzdob.zze.zzhhk
            r1 = 0
            java.lang.Object r0 = r4.zza(r0, r1, r1)
            java.lang.Byte r0 = (java.lang.Byte) r0
            byte r0 = r0.byteValue()
            r3 = 1
            if (r0 != r3) goto L_0x0024
            r2 = 1
            goto L_0x003f
        L_0x0024:
            if (r0 != 0) goto L_0x0027
            goto L_0x003f
        L_0x0027:
            com.google.android.gms.internal.ads.zzdpx r0 = com.google.android.gms.internal.ads.zzdpx.zzazg()
            com.google.android.gms.internal.ads.zzdqb r0 = r0.zzan(r4)
            boolean r2 = r0.zzam(r4)
            if (r5 == 0) goto L_0x003f
            int r5 = com.google.android.gms.internal.ads.zzdob.zze.zzhhl
            if (r2 == 0) goto L_0x003b
            r0 = r4
            goto L_0x003c
        L_0x003b:
            r0 = r1
        L_0x003c:
            r4.zza(r5, r0, r1)
        L_0x003f:
            if (r2 == 0) goto L_0x0042
            goto L_0x0050
        L_0x0042:
            com.google.android.gms.internal.ads.zzdqs r5 = new com.google.android.gms.internal.ads.zzdqs
            r5.<init>(r4)
            com.google.android.gms.internal.ads.zzdok r5 = r5.zzazx()
            com.google.android.gms.internal.ads.zzdok r4 = r5.zzo(r4)
            throw r4
        L_0x0050:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, byte[]):com.google.android.gms.internal.ads.zzdob");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static <T extends com.google.android.gms.internal.ads.zzdob<T, ?>> T zza(com.google.android.gms.internal.ads.zzdob r3, byte[] r4, com.google.android.gms.internal.ads.zzdno r5) throws com.google.android.gms.internal.ads.zzdok {
        /*
            int r0 = r4.length
            r1 = 0
            com.google.android.gms.internal.ads.zzdob r3 = zza(r3, r4, r1, r0, r5)
            if (r3 == 0) goto L_0x004c
            java.lang.Boolean r4 = java.lang.Boolean.TRUE
            boolean r4 = r4.booleanValue()
            int r5 = com.google.android.gms.internal.ads.zzdob.zze.zzhhk
            r0 = 0
            java.lang.Object r5 = r3.zza(r5, r0, r0)
            java.lang.Byte r5 = (java.lang.Byte) r5
            byte r5 = r5.byteValue()
            r2 = 1
            if (r5 != r2) goto L_0x0020
            r1 = 1
            goto L_0x003b
        L_0x0020:
            if (r5 != 0) goto L_0x0023
            goto L_0x003b
        L_0x0023:
            com.google.android.gms.internal.ads.zzdpx r5 = com.google.android.gms.internal.ads.zzdpx.zzazg()
            com.google.android.gms.internal.ads.zzdqb r5 = r5.zzan(r3)
            boolean r1 = r5.zzam(r3)
            if (r4 == 0) goto L_0x003b
            int r4 = com.google.android.gms.internal.ads.zzdob.zze.zzhhl
            if (r1 == 0) goto L_0x0037
            r5 = r3
            goto L_0x0038
        L_0x0037:
            r5 = r0
        L_0x0038:
            r3.zza(r4, r5, r0)
        L_0x003b:
            if (r1 == 0) goto L_0x003e
            goto L_0x004c
        L_0x003e:
            com.google.android.gms.internal.ads.zzdqs r4 = new com.google.android.gms.internal.ads.zzdqs
            r4.<init>(r3)
            com.google.android.gms.internal.ads.zzdok r4 = r4.zzazx()
            com.google.android.gms.internal.ads.zzdok r3 = r4.zzo(r3)
            throw r3
        L_0x004c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdob.zza(com.google.android.gms.internal.ads.zzdob, byte[], com.google.android.gms.internal.ads.zzdno):com.google.android.gms.internal.ads.zzdob");
    }

    public final /* synthetic */ zzdpl zzaxt() {
        zza zza2 = (zza) zza(zze.zzhho, (Object) null, (Object) null);
        zza2.zza(this);
        return zza2;
    }

    public final /* synthetic */ zzdpl zzaxu() {
        return (zza) zza(zze.zzhho, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzdpk zzaxv() {
        return (zzdob) zza(zze.zzhhp, (Object) null, (Object) null);
    }
}
