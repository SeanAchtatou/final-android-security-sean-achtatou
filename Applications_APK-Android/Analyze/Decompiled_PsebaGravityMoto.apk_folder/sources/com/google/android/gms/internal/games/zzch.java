package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.request.Requests;
import java.util.Set;

final class zzch implements Requests.UpdateRequestsResult {
    private final /* synthetic */ Status zzbc;

    zzch(zzcg zzcg, Status status) {
        this.zzbc = status;
    }

    public final Set<String> getRequestIds() {
        return null;
    }

    public final int getRequestOutcome(String str) {
        String valueOf = String.valueOf(str);
        throw new IllegalArgumentException(valueOf.length() != 0 ? "Unknown request ID ".concat(valueOf) : new String("Unknown request ID "));
    }

    public final Status getStatus() {
        return this.zzbc;
    }

    public final void release() {
    }
}
