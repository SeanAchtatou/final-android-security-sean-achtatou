package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzdnl extends zzdml<Double> implements zzdoj<Double>, zzdpw, RandomAccess {
    private static final zzdnl zzhdw;
    private int size;
    private double[] zzhdx;

    zzdnl() {
        this(new double[10], 0);
    }

    private zzdnl(double[] dArr, int i) {
        this.zzhdx = dArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zzavk();
        if (i2 >= i) {
            double[] dArr = this.zzhdx;
            System.arraycopy(dArr, i2, dArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdnl)) {
            return super.equals(obj);
        }
        zzdnl zzdnl = (zzdnl) obj;
        if (this.size != zzdnl.size) {
            return false;
        }
        double[] dArr = zzdnl.zzhdx;
        for (int i = 0; i < this.size; i++) {
            if (Double.doubleToLongBits(this.zzhdx[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + zzdod.zzft(Double.doubleToLongBits(this.zzhdx[i2]));
        }
        return i;
    }

    public final int size() {
        return this.size;
    }

    public final void zzd(double d) {
        zzd(this.size, d);
    }

    private final void zzd(int i, double d) {
        int i2;
        zzavk();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzfk(i));
        }
        double[] dArr = this.zzhdx;
        if (i2 < dArr.length) {
            System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
        } else {
            double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.zzhdx, i, dArr2, i + 1, this.size - i);
            this.zzhdx = dArr2;
        }
        this.zzhdx[i] = d;
        this.size++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Double> collection) {
        zzavk();
        zzdod.checkNotNull(collection);
        if (!(collection instanceof zzdnl)) {
            return super.addAll(collection);
        }
        zzdnl zzdnl = (zzdnl) collection;
        int i = zzdnl.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.zzhdx;
            if (i3 > dArr.length) {
                this.zzhdx = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(zzdnl.zzhdx, 0, this.zzhdx, this.size, zzdnl.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean remove(Object obj) {
        zzavk();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Double.valueOf(this.zzhdx[i]))) {
                double[] dArr = this.zzhdx;
                System.arraycopy(dArr, i + 1, dArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzfj(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzfk(i));
        }
    }

    private final String zzfk(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        zzavk();
        zzfj(i);
        double[] dArr = this.zzhdx;
        double d = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d);
    }

    public final /* synthetic */ Object remove(int i) {
        zzavk();
        zzfj(i);
        double[] dArr = this.zzhdx;
        double d = dArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Double.valueOf(d);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        zzd(i, ((Double) obj).doubleValue());
    }

    public final /* synthetic */ zzdoj zzfl(int i) {
        if (i >= this.size) {
            return new zzdnl(Arrays.copyOf(this.zzhdx, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ Object get(int i) {
        zzfj(i);
        return Double.valueOf(this.zzhdx[i]);
    }

    static {
        zzdnl zzdnl = new zzdnl(new double[0], 0);
        zzhdw = zzdnl;
        zzdnl.zzavj();
    }
}
