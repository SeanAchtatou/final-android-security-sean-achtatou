package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzps extends zzpv {
    public static final Parcelable.Creator<zzps> CREATOR = new zzpt();
    public final String description;
    private final String zzauc;
    public final String zzbhy;

    public zzps(String str, String str2, String str3) {
        super("COMM");
        this.zzauc = str;
        this.description = str2;
        this.zzbhy = str3;
    }

    zzps(Parcel parcel) {
        super("COMM");
        this.zzauc = parcel.readString();
        this.description = parcel.readString();
        this.zzbhy = parcel.readString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzps zzps = (zzps) obj;
            return zzsy.zza(this.description, zzps.description) && zzsy.zza(this.zzauc, zzps.zzauc) && zzsy.zza(this.zzbhy, zzps.zzbhy);
        }
    }

    public final int hashCode() {
        String str = this.zzauc;
        int i = 0;
        int hashCode = ((str != null ? str.hashCode() : 0) + 527) * 31;
        String str2 = this.description;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.zzbhy;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.zzatl);
        parcel.writeString(this.zzauc);
        parcel.writeString(this.zzbhy);
    }
}
