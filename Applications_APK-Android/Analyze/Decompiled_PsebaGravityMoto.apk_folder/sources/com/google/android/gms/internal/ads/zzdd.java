package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.internal.ads.zzbk;
import com.google.android.gms.internal.ads.zzbp;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class zzdd implements zzdc {
    protected static volatile zzdy zzvd;
    protected MotionEvent zzvj;
    protected LinkedList<MotionEvent> zzvk = new LinkedList<>();
    protected long zzvl = 0;
    protected long zzvm = 0;
    protected long zzvn = 0;
    protected long zzvo = 0;
    protected long zzvp = 0;
    protected long zzvq = 0;
    protected long zzvr = 0;
    protected double zzvs;
    private double zzvt;
    private double zzvu;
    protected float zzvv;
    protected float zzvw;
    protected float zzvx;
    protected float zzvy;
    private boolean zzvz = false;
    protected boolean zzwa = false;
    protected DisplayMetrics zzwb;

    protected zzdd(Context context) {
        try {
            if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcrm)).booleanValue()) {
                zzci.zzcb();
            } else {
                zzed.zzb(zzvd);
            }
            this.zzwb = context.getResources().getDisplayMetrics();
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: protected */
    public abstract long zza(StackTraceElement[] stackTraceElementArr) throws zzdv;

    /* access modifiers changed from: protected */
    public abstract zzbp.zza.C0004zza zza(Context context, View view, Activity activity);

    /* access modifiers changed from: protected */
    public abstract zzbp.zza.C0004zza zza(Context context, zzbk.zza zza);

    /* access modifiers changed from: protected */
    public abstract zzee zzb(MotionEvent motionEvent) throws zzdv;

    public void zzb(View view) {
    }

    public final String zza(Context context) {
        if (zzef.isMainThread()) {
            if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcro)).booleanValue()) {
                throw new IllegalStateException("The caller must not be called from the UI thread.");
            }
        }
        return zza(context, null, false, null, null, null);
    }

    public final String zza(Context context, byte[] bArr) {
        if (zzef.isMainThread()) {
            if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcro)).booleanValue()) {
                throw new IllegalStateException("The caller must not be called from the UI thread.");
            }
        }
        return zza(context, null, false, null, null, bArr);
    }

    public final String zza(Context context, String str, View view) {
        return zza(context, str, view, null);
    }

    public final String zza(Context context, String str, View view, Activity activity) {
        return zza(context, str, true, view, activity, null);
    }

    public final void zza(MotionEvent motionEvent) {
        boolean z = false;
        if (this.zzvz) {
            this.zzvo = 0;
            this.zzvn = 0;
            this.zzvm = 0;
            this.zzvl = 0;
            this.zzvp = 0;
            this.zzvr = 0;
            this.zzvq = 0;
            Iterator<MotionEvent> it = this.zzvk.iterator();
            while (it.hasNext()) {
                it.next().recycle();
            }
            this.zzvk.clear();
            this.zzvj = null;
            this.zzvz = false;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            this.zzvs = 0.0d;
            this.zzvt = (double) motionEvent.getRawX();
            this.zzvu = (double) motionEvent.getRawY();
        } else if (action == 1 || action == 2) {
            double rawX = (double) motionEvent.getRawX();
            double rawY = (double) motionEvent.getRawY();
            double d = this.zzvt;
            Double.isNaN(rawX);
            double d2 = rawX - d;
            double d3 = this.zzvu;
            Double.isNaN(rawY);
            double d4 = rawY - d3;
            this.zzvs += Math.sqrt((d2 * d2) + (d4 * d4));
            this.zzvt = rawX;
            this.zzvu = rawY;
        }
        int action2 = motionEvent.getAction();
        if (action2 == 0) {
            this.zzvv = motionEvent.getX();
            this.zzvw = motionEvent.getY();
            this.zzvx = motionEvent.getRawX();
            this.zzvy = motionEvent.getRawY();
            this.zzvl++;
        } else if (action2 == 1) {
            this.zzvj = MotionEvent.obtain(motionEvent);
            this.zzvk.add(this.zzvj);
            if (this.zzvk.size() > 6) {
                this.zzvk.remove().recycle();
            }
            this.zzvn++;
            this.zzvp = zza(new Throwable().getStackTrace());
        } else if (action2 == 2) {
            this.zzvm += (long) (motionEvent.getHistorySize() + 1);
            try {
                zzee zzb = zzb(motionEvent);
                if ((zzb == null || zzb.zzye == null || zzb.zzyh == null) ? false : true) {
                    this.zzvq += zzb.zzye.longValue() + zzb.zzyh.longValue();
                }
                if (!(this.zzwb == null || zzb == null || zzb.zzyf == null || zzb.zzyi == null)) {
                    z = true;
                }
                if (z) {
                    this.zzvr += zzb.zzyf.longValue() + zzb.zzyi.longValue();
                }
            } catch (zzdv unused) {
            }
        } else if (action2 == 3) {
            this.zzvo++;
        }
        this.zzwa = true;
    }

    public final void zza(int i, int i2, int i3) {
        MotionEvent motionEvent = this.zzvj;
        if (motionEvent != null) {
            motionEvent.recycle();
        }
        DisplayMetrics displayMetrics = this.zzwb;
        if (displayMetrics != null) {
            this.zzvj = MotionEvent.obtain(0, (long) i3, 1, ((float) i) * displayMetrics.density, this.zzwb.density * ((float) i2), 0.0f, 0.0f, 0, 0.0f, 0.0f, 0, 0);
        } else {
            this.zzvj = null;
        }
        this.zzwa = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035 A[SYNTHETIC, Splitter:B:16:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003d A[Catch:{ Exception -> 0x0054 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0044 A[Catch:{ Exception -> 0x0054 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0071 A[SYNTHETIC, Splitter:B:33:0x0071] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.lang.String zza(android.content.Context r10, java.lang.String r11, boolean r12, android.view.View r13, android.app.Activity r14, byte[] r15) {
        /*
            r9 = this;
            r0 = 0
            if (r15 == 0) goto L_0x000f
            int r1 = r15.length
            if (r1 <= 0) goto L_0x000f
            com.google.android.gms.internal.ads.zzdno r1 = com.google.android.gms.internal.ads.zzdno.zzaxe()     // Catch:{ zzdok -> 0x000f }
            com.google.android.gms.internal.ads.zzbk$zza r15 = com.google.android.gms.internal.ads.zzbk.zza.zza(r15, r1)     // Catch:{ zzdok -> 0x000f }
            goto L_0x0010
        L_0x000f:
            r15 = r0
        L_0x0010:
            com.google.android.gms.internal.ads.zzdy r1 = com.google.android.gms.internal.ads.zzdd.zzvd
            if (r1 == 0) goto L_0x002d
            com.google.android.gms.internal.ads.zzacj<java.lang.Boolean> r1 = com.google.android.gms.internal.ads.zzacu.zzcrc
            com.google.android.gms.internal.ads.zzacr r2 = com.google.android.gms.internal.ads.zzyt.zzpe()
            java.lang.Object r1 = r2.zzd(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x002d
            com.google.android.gms.internal.ads.zzdy r1 = com.google.android.gms.internal.ads.zzdd.zzvd
            com.google.android.gms.internal.ads.zzda r1 = r1.zzcm()
            goto L_0x002e
        L_0x002d:
            r1 = r0
        L_0x002e:
            long r2 = java.lang.System.currentTimeMillis()
            r8 = -1
            if (r12 == 0) goto L_0x003d
            com.google.android.gms.internal.ads.zzbp$zza$zza r0 = r9.zza(r10, r13, r14)     // Catch:{ Exception -> 0x0054 }
            r10 = 1
            r9.zzvz = r10     // Catch:{ Exception -> 0x0054 }
            goto L_0x0042
        L_0x003d:
            com.google.android.gms.internal.ads.zzbp$zza$zza r10 = r9.zza(r10, r15)     // Catch:{ Exception -> 0x0054 }
            r0 = r10
        L_0x0042:
            if (r1 == 0) goto L_0x006b
            if (r12 == 0) goto L_0x0049
            r10 = 1002(0x3ea, float:1.404E-42)
            goto L_0x004b
        L_0x0049:
            r10 = 1000(0x3e8, float:1.401E-42)
        L_0x004b:
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0054 }
            long r13 = r13 - r2
            r1.zza(r10, r8, r13)     // Catch:{ Exception -> 0x0054 }
            goto L_0x006b
        L_0x0054:
            r10 = move-exception
            r7 = r10
            if (r1 == 0) goto L_0x006b
            if (r12 == 0) goto L_0x005d
            r10 = 1003(0x3eb, float:1.406E-42)
            goto L_0x005f
        L_0x005d:
            r10 = 1001(0x3e9, float:1.403E-42)
        L_0x005f:
            r4 = -1
            long r13 = java.lang.System.currentTimeMillis()
            long r5 = r13 - r2
            r2 = r1
            r3 = r10
            r2.zza(r3, r4, r5, r7)
        L_0x006b:
            long r13 = java.lang.System.currentTimeMillis()
            if (r0 == 0) goto L_0x009e
            com.google.android.gms.internal.ads.zzdpk r10 = r0.zzaya()     // Catch:{ Exception -> 0x00a4 }
            com.google.android.gms.internal.ads.zzdob r10 = (com.google.android.gms.internal.ads.zzdob) r10     // Catch:{ Exception -> 0x00a4 }
            com.google.android.gms.internal.ads.zzbp$zza r10 = (com.google.android.gms.internal.ads.zzbp.zza) r10     // Catch:{ Exception -> 0x00a4 }
            int r10 = r10.zzaxj()     // Catch:{ Exception -> 0x00a4 }
            if (r10 != 0) goto L_0x0080
            goto L_0x009e
        L_0x0080:
            com.google.android.gms.internal.ads.zzdpk r10 = r0.zzaya()     // Catch:{ Exception -> 0x00a4 }
            com.google.android.gms.internal.ads.zzdob r10 = (com.google.android.gms.internal.ads.zzdob) r10     // Catch:{ Exception -> 0x00a4 }
            com.google.android.gms.internal.ads.zzbp$zza r10 = (com.google.android.gms.internal.ads.zzbp.zza) r10     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r10 = com.google.android.gms.internal.ads.zzci.zzj(r10, r11)     // Catch:{ Exception -> 0x00a4 }
            if (r1 == 0) goto L_0x00c3
            if (r12 == 0) goto L_0x0093
            r11 = 1006(0x3ee, float:1.41E-42)
            goto L_0x0095
        L_0x0093:
            r11 = 1004(0x3ec, float:1.407E-42)
        L_0x0095:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00a4 }
            long r2 = r2 - r13
            r1.zza(r11, r8, r2)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00c3
        L_0x009e:
            r10 = 5
            java.lang.String r10 = java.lang.Integer.toString(r10)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00c3
        L_0x00a4:
            r10 = move-exception
            r7 = r10
            r10 = 7
            java.lang.String r10 = java.lang.Integer.toString(r10)
            if (r1 == 0) goto L_0x00c3
            if (r12 == 0) goto L_0x00b4
            r11 = 1007(0x3ef, float:1.411E-42)
            r3 = 1007(0x3ef, float:1.411E-42)
            goto L_0x00b8
        L_0x00b4:
            r11 = 1005(0x3ed, float:1.408E-42)
            r3 = 1005(0x3ed, float:1.408E-42)
        L_0x00b8:
            r4 = -1
            long r11 = java.lang.System.currentTimeMillis()
            long r5 = r11 - r13
            r2 = r1
            r2.zza(r3, r4, r5, r7)
        L_0x00c3:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdd.zza(android.content.Context, java.lang.String, boolean, android.view.View, android.app.Activity, byte[]):java.lang.String");
    }
}
