package com.google.android.gms.internal.ads;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Display;
import android.view.WindowManager;
import javax.annotation.concurrent.GuardedBy;

@zzard
final class zzbda implements SensorEventListener {
    private final SensorManager zzecv;
    private final Object zzecw = new Object();
    private final Display zzecx;
    private final float[] zzecy = new float[9];
    private final float[] zzecz = new float[9];
    @GuardedBy("sensorThreadLock")
    private float[] zzeda;
    private Handler zzedb;
    private zzbdc zzedc;

    zzbda(Context context) {
        this.zzecv = (SensorManager) context.getSystemService("sensor");
        this.zzecx = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    /* access modifiers changed from: package-private */
    public final void start() {
        if (this.zzedb == null) {
            Sensor defaultSensor = this.zzecv.getDefaultSensor(11);
            if (defaultSensor == null) {
                zzawz.zzen("No Sensor of TYPE_ROTATION_VECTOR");
                return;
            }
            HandlerThread handlerThread = new HandlerThread("OrientationMonitor");
            handlerThread.start();
            this.zzedb = new zzdbh(handlerThread.getLooper());
            if (!this.zzecv.registerListener(this, defaultSensor, 0, this.zzedb)) {
                zzawz.zzen("SensorManager.registerListener failed.");
                stop();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void stop() {
        if (this.zzedb != null) {
            this.zzecv.unregisterListener(this);
            this.zzedb.post(new zzbdb(this));
            this.zzedb = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzbdc zzbdc) {
        this.zzedc = zzbdc;
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        if (fArr[0] != 0.0f || fArr[1] != 0.0f || fArr[2] != 0.0f) {
            synchronized (this.zzecw) {
                if (this.zzeda == null) {
                    this.zzeda = new float[9];
                }
            }
            SensorManager.getRotationMatrixFromVector(this.zzecy, fArr);
            int rotation = this.zzecx.getRotation();
            if (rotation == 1) {
                SensorManager.remapCoordinateSystem(this.zzecy, 2, 129, this.zzecz);
            } else if (rotation == 2) {
                SensorManager.remapCoordinateSystem(this.zzecy, 129, 130, this.zzecz);
            } else if (rotation != 3) {
                System.arraycopy(this.zzecy, 0, this.zzecz, 0, 9);
            } else {
                SensorManager.remapCoordinateSystem(this.zzecy, 130, 1, this.zzecz);
            }
            zzl(1, 3);
            zzl(2, 6);
            zzl(5, 7);
            synchronized (this.zzecw) {
                System.arraycopy(this.zzecz, 0, this.zzeda, 0, 9);
            }
            zzbdc zzbdc = this.zzedc;
            if (zzbdc != null) {
                zzbdc.zztk();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(float[] fArr) {
        synchronized (this.zzecw) {
            if (this.zzeda == null) {
                return false;
            }
            System.arraycopy(this.zzeda, 0, fArr, 0, this.zzeda.length);
            return true;
        }
    }

    private final void zzl(int i, int i2) {
        float[] fArr = this.zzecz;
        float f = fArr[i];
        fArr[i] = fArr[i2];
        fArr[i2] = f;
    }
}
