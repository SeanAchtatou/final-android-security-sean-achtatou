package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import java.io.IOException;

final class zzlc implements Handler.Callback, zzqk, zzqm, zzrq {
    private final Handler handler;
    private int repeatMode = 0;
    private int state;
    private final Handler zzabq;
    private boolean zzabu;
    private final HandlerThread zzaby;
    private boolean zzacf;
    private boolean zzacg;
    private int zzach;
    private int zzaci;
    private long zzacj;
    private final zzlo[] zzaro;
    private final zzrp zzarp;
    private final zzlu zzars;
    private final zzlt zzart;
    private boolean zzarx;
    private zzlr zzary;
    private zzln zzasc;
    private zzle zzasd;
    private final zzlp[] zzasi;
    private final zzll zzasj;
    private final zzsw zzask;
    private final zzkv zzasl;
    private zzlo zzasm;
    private zzso zzasn;
    private zzql zzaso;
    private zzlo[] zzasp;
    private int zzasq;
    private zzlf zzasr;
    private long zzass;
    private zzld zzast;
    private zzld zzasu;
    private zzld zzasv;

    public zzlc(zzlo[] zzloArr, zzrp zzrp, zzll zzll, boolean z, int i, Handler handler2, zzle zzle, zzkv zzkv) {
        this.zzaro = zzloArr;
        this.zzarp = zzrp;
        this.zzasj = zzll;
        this.zzabu = z;
        this.zzabq = handler2;
        this.state = 1;
        this.zzasd = zzle;
        this.zzasl = zzkv;
        this.zzasi = new zzlp[zzloArr.length];
        for (int i2 = 0; i2 < zzloArr.length; i2++) {
            zzloArr[i2].setIndex(i2);
            this.zzasi[i2] = zzloArr[i2].zzgi();
        }
        this.zzask = new zzsw();
        this.zzasp = new zzlo[0];
        this.zzars = new zzlu();
        this.zzart = new zzlt();
        zzrp.zza(this);
        this.zzasc = zzln.zzaug;
        this.zzaby = new HandlerThread("ExoPlayerImplInternal:Handler", -16);
        this.zzaby.start();
        this.handler = new Handler(this.zzaby.getLooper(), this);
    }

    public final void zza(zzql zzql, boolean z) {
        this.handler.obtainMessage(0, 1, 0, zzql).sendToTarget();
    }

    public final void zzd(boolean z) {
        this.handler.obtainMessage(1, z ? 1 : 0, 0).sendToTarget();
    }

    public final void zza(zzlr zzlr, int i, long j) {
        this.handler.obtainMessage(3, new zzlf(zzlr, i, j)).sendToTarget();
    }

    public final void stop() {
        this.handler.sendEmptyMessage(5);
    }

    public final void zza(zzky... zzkyArr) {
        if (this.zzacf) {
            Log.w("ExoPlayerImplInternal", "Ignoring messages sent after release.");
            return;
        }
        this.zzach++;
        this.handler.obtainMessage(11, zzkyArr).sendToTarget();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:11|12|13|14|23|20|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x001f, code lost:
        continue;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0027 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zzb(com.google.android.gms.internal.ads.zzky... r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.zzacf     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x000e
            java.lang.String r4 = "ExoPlayerImplInternal"
            java.lang.String r0 = "Ignoring messages sent after release."
            android.util.Log.w(r4, r0)     // Catch:{ all -> 0x0031 }
            monitor-exit(r3)
            return
        L_0x000e:
            int r0 = r3.zzach     // Catch:{ all -> 0x0031 }
            int r1 = r0 + 1
            r3.zzach = r1     // Catch:{ all -> 0x0031 }
            android.os.Handler r1 = r3.handler     // Catch:{ all -> 0x0031 }
            r2 = 11
            android.os.Message r4 = r1.obtainMessage(r2, r4)     // Catch:{ all -> 0x0031 }
            r4.sendToTarget()     // Catch:{ all -> 0x0031 }
        L_0x001f:
            int r4 = r3.zzaci     // Catch:{ all -> 0x0031 }
            if (r4 > r0) goto L_0x002f
            r3.wait()     // Catch:{ InterruptedException -> 0x0027 }
            goto L_0x001f
        L_0x0027:
            java.lang.Thread r4 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0031 }
            r4.interrupt()     // Catch:{ all -> 0x0031 }
            goto L_0x001f
        L_0x002f:
            monitor-exit(r3)
            return
        L_0x0031:
            r4 = move-exception
            monitor-exit(r3)
            goto L_0x0035
        L_0x0034:
            throw r4
        L_0x0035:
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzlc.zzb(com.google.android.gms.internal.ads.zzky[]):void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:10|11|12|13|23|20|8) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x000d, code lost:
        continue;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0015 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void release() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.zzacf     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r2)
            return
        L_0x0007:
            android.os.Handler r0 = r2.handler     // Catch:{ all -> 0x0024 }
            r1 = 6
            r0.sendEmptyMessage(r1)     // Catch:{ all -> 0x0024 }
        L_0x000d:
            boolean r0 = r2.zzacf     // Catch:{ all -> 0x0024 }
            if (r0 != 0) goto L_0x001d
            r2.wait()     // Catch:{ InterruptedException -> 0x0015 }
            goto L_0x000d
        L_0x0015:
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0024 }
            r0.interrupt()     // Catch:{ all -> 0x0024 }
            goto L_0x000d
        L_0x001d:
            android.os.HandlerThread r0 = r2.zzaby     // Catch:{ all -> 0x0024 }
            r0.quit()     // Catch:{ all -> 0x0024 }
            monitor-exit(r2)
            return
        L_0x0024:
            r0 = move-exception
            monitor-exit(r2)
            goto L_0x0028
        L_0x0027:
            throw r0
        L_0x0028:
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzlc.release():void");
    }

    public final void zzb(zzlr zzlr, Object obj) {
        this.handler.obtainMessage(7, Pair.create(zzlr, obj)).sendToTarget();
    }

    public final void zza(zzqj zzqj) {
        this.handler.obtainMessage(8, zzqj).sendToTarget();
    }

    public final void zzgv() {
        this.handler.sendEmptyMessage(10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
     arg types: [int, com.google.android.gms.internal.ads.zzlt, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
     arg types: [int, com.google.android.gms.internal.ads.zzlu, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Code restructure failed: missing block: B:504:0x08b9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:505:0x08ba, code lost:
        r1 = r0;
        android.util.Log.e("ExoPlayerImplInternal", "Internal runtime error.", r1);
        r8.zzabq.obtainMessage(8, com.google.android.gms.internal.ads.zzku.zza(r1)).sendToTarget();
        zzds();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:506:0x08d4, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:507:0x08d5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:508:0x08d6, code lost:
        r3 = 8;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:511:0x08f1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:512:0x08f2, code lost:
        r3 = 8;
        r1 = r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x028f A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0292 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0296 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x0358 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x036c A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:307:0x0538 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x053f A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x0559 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:313:0x055c A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x0597 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x05ab A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x05c7 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }, LOOP:8: B:331:0x05c7->B:335:0x05d9, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:405:0x0729 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:444:0x07e2 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:504:0x08b9 A[ExcHandler: RuntimeException (r0v2 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean handleMessage(android.os.Message r35) {
        /*
            r34 = this;
            r8 = r34
            r1 = r35
            r10 = 1
            int r2 = r1.what     // Catch:{ zzku -> 0x08f1, IOException -> 0x08d5, RuntimeException -> 0x08b9 }
            r11 = 7
            r3 = 0
            r14 = 3
            r5 = -1
            r6 = 0
            r15 = 4
            r12 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r7 = 2
            r9 = 0
            switch(r2) {
                case 0: goto L_0x0878;
                case 1: goto L_0x084b;
                case 2: goto L_0x046e;
                case 3: goto L_0x03b5;
                case 4: goto L_0x0394;
                case 5: goto L_0x0390;
                case 6: goto L_0x0379;
                case 7: goto L_0x021b;
                case 8: goto L_0x01e5;
                case 9: goto L_0x01d2;
                case 10: goto L_0x00dd;
                case 11: goto L_0x009f;
                case 12: goto L_0x0019;
                default: goto L_0x0018;
            }
        L_0x0018:
            return r9
        L_0x0019:
            int r1 = r1.arg1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.repeatMode = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0024
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0026
        L_0x0024:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0026:
            if (r2 == 0) goto L_0x009e
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r3) goto L_0x002e
            r3 = 1
            goto L_0x002f
        L_0x002e:
            r3 = 0
        L_0x002f:
            com.google.android.gms.internal.ads.zzld r4 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r4) goto L_0x0037
            r4 = r3
            r3 = r2
            r2 = 1
            goto L_0x003a
        L_0x0037:
            r4 = r3
            r3 = r2
            r2 = 0
        L_0x003a:
            com.google.android.gms.internal.ads.zzlr r11 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r12 = r3.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r13 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlu r14 = r8.zzars     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.zza(r12, r13, r14, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r12 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x0065
            if (r11 == r5) goto L_0x0065
            com.google.android.gms.internal.ads.zzld r12 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r12 = r12.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 != r11) goto L_0x0065
            com.google.android.gms.internal.ads.zzld r3 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r11 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != r11) goto L_0x005a
            r11 = 1
            goto L_0x005b
        L_0x005a:
            r11 = 0
        L_0x005b:
            r4 = r4 | r11
            com.google.android.gms.internal.ads.zzld r11 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != r11) goto L_0x0062
            r11 = 1
            goto L_0x0063
        L_0x0062:
            r11 = 0
        L_0x0063:
            r2 = r2 | r11
            goto L_0x003a
        L_0x0065:
            com.google.android.gms.internal.ads.zzld r5 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r5 == 0) goto L_0x0070
            com.google.android.gms.internal.ads.zzld r5 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            zza(r5)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzatf = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0070:
            int r5 = r3.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r5 = r8.zzad(r5)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzatd = r5     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x007c
            r8.zzast = r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x007c:
            if (r4 != 0) goto L_0x0095
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0095
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r3 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r8.zzd(r2, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r5 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5.<init>(r2, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r5     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0095:
            int r2 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r15) goto L_0x009e
            if (r1 == 0) goto L_0x009e
            r8.setState(r7)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x009e:
            return r10
        L_0x009f:
            java.lang.Object r1 = r1.obj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzky[] r1 = (com.google.android.gms.internal.ads.zzky[]) r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r1.length     // Catch:{ all -> 0x00cc }
        L_0x00a4:
            if (r9 >= r2) goto L_0x00b4
            r3 = r1[r9]     // Catch:{ all -> 0x00cc }
            com.google.android.gms.internal.ads.zzkx r4 = r3.zzarl     // Catch:{ all -> 0x00cc }
            int r5 = r3.zzarm     // Catch:{ all -> 0x00cc }
            java.lang.Object r3 = r3.zzarn     // Catch:{ all -> 0x00cc }
            r4.zza(r5, r3)     // Catch:{ all -> 0x00cc }
            int r9 = r9 + 1
            goto L_0x00a4
        L_0x00b4:
            com.google.android.gms.internal.ads.zzql r1 = r8.zzaso     // Catch:{ all -> 0x00cc }
            if (r1 == 0) goto L_0x00bd
            android.os.Handler r1 = r8.handler     // Catch:{ all -> 0x00cc }
            r1.sendEmptyMessage(r7)     // Catch:{ all -> 0x00cc }
        L_0x00bd:
            monitor-enter(r34)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r8.zzaci     // Catch:{ all -> 0x00c8 }
            int r1 = r1 + r10
            r8.zzaci = r1     // Catch:{ all -> 0x00c8 }
            r34.notifyAll()     // Catch:{ all -> 0x00c8 }
            monitor-exit(r34)     // Catch:{ all -> 0x00c8 }
            return r10
        L_0x00c8:
            r0 = move-exception
            r1 = r0
            monitor-exit(r34)     // Catch:{ all -> 0x00c8 }
            throw r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x00cc:
            r0 = move-exception
            r1 = r0
            monitor-enter(r34)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r8.zzaci     // Catch:{ all -> 0x00d9 }
            int r2 = r2 + r10
            r8.zzaci = r2     // Catch:{ all -> 0x00d9 }
            r34.notifyAll()     // Catch:{ all -> 0x00d9 }
            monitor-exit(r34)     // Catch:{ all -> 0x00d9 }
            throw r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x00d9:
            r0 = move-exception
            r1 = r0
            monitor-exit(r34)     // Catch:{ all -> 0x00d9 }
            throw r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x00dd:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x01d1
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 1
        L_0x00e4:
            if (r1 == 0) goto L_0x01d1
            boolean r3 = r1.zzacs     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != 0) goto L_0x00ec
            goto L_0x01d1
        L_0x00ec:
            boolean r3 = r1.zzhb()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != 0) goto L_0x00fa
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r3) goto L_0x00f7
            r2 = 0
        L_0x00f7:
            com.google.android.gms.internal.ads.zzld r1 = r1.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x00e4
        L_0x00fa:
            if (r2 == 0) goto L_0x0198
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == r3) goto L_0x0104
            r2 = 1
            goto L_0x0105
        L_0x0104:
            r2 = 0
        L_0x0105:
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            zza(r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzatf = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzast = r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasu = r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlo[] r3 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean[] r3 = new boolean[r3]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r4 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r5 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r5.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r4.zza(r11, r2, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r2 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r2.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
            if (r2 == 0) goto L_0x0136
            com.google.android.gms.internal.ads.zzle r2 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zzacl = r4     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzeb(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0136:
            com.google.android.gms.internal.ads.zzlo[] r2 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean[] r2 = new boolean[r2]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4 = 0
            r5 = 0
        L_0x013d:
            com.google.android.gms.internal.ads.zzlo[] r11 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 >= r11) goto L_0x0189
            com.google.android.gms.internal.ads.zzlo[] r11 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11 = r11[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r12 = r11.getState()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x014e
            r12 = 1
            goto L_0x014f
        L_0x014e:
            r12 = 0
        L_0x014f:
            r2[r4] = r12     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r12 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqw[] r12 = r12.zzasy     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12 = r12[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x015b
            int r5 = r5 + 1
        L_0x015b:
            boolean r13 = r2[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r13 == 0) goto L_0x0186
            com.google.android.gms.internal.ads.zzqw r13 = r11.zzgk()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == r13) goto L_0x017d
            com.google.android.gms.internal.ads.zzlo r13 = r8.zzasm     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 != r13) goto L_0x0176
            if (r12 != 0) goto L_0x0172
            com.google.android.gms.internal.ads.zzsw r12 = r8.zzask     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzso r13 = r8.zzasn     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.zza(r13)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0172:
            r8.zzasn = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasm = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0176:
            zza(r11)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.disable()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0186
        L_0x017d:
            boolean r12 = r3[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x0186
            long r12 = r8.zzass     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.zzdx(r12)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0186:
            int r4 = r4 + 1
            goto L_0x013d
        L_0x0189:
            android.os.Handler r3 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzrr r1 = r1.zzatg     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r3.obtainMessage(r14, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zza(r2, r5)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x01c6
        L_0x0198:
            r8.zzast = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r1.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x019e:
            if (r1 == 0) goto L_0x01a6
            r1.release()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r1.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x019e
        L_0x01a6:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzatf = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzacs     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x01c6
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzatc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r8.zzass     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r3.zzgz()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r4 - r11
            long r1 = java.lang.Math.max(r1, r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zze(r1, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x01c6:
            r34.zzgy()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzgw()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r1 = r8.handler     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r7)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x01d1:
            return r10
        L_0x01d2:
            java.lang.Object r1 = r1.obj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r1 = (com.google.android.gms.internal.ads.zzqj) r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x01e4
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r2 = r2.zzasw     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == r1) goto L_0x01e1
            goto L_0x01e4
        L_0x01e1:
            r34.zzgy()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x01e4:
            return r10
        L_0x01e5:
            java.lang.Object r1 = r1.obj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r1 = (com.google.android.gms.internal.ads.zzqj) r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x021a
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r2 = r2.zzasw     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == r1) goto L_0x01f4
            goto L_0x021a
        L_0x01f4:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzacs = r10     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzhb()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r1.zzatc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r1.zze(r2, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzatc = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x0217
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasu = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzatc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzeb(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzb(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0217:
            r34.zzgy()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x021a:
            return r10
        L_0x021b:
            java.lang.Object r1 = r1.obj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.util.Pair r1 = (android.util.Pair) r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r3 = r1.first     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r3 = (com.google.android.gms.internal.ads.zzlr) r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzary = r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r1 = r1.second     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x028a
            int r3 = r8.zzasq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 <= 0) goto L_0x025a
            com.google.android.gms.internal.ads.zzlf r3 = r8.zzasr     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.util.Pair r3 = r8.zza(r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r4 = r8.zzasq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasq = r9     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasr = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != 0) goto L_0x0242
            r8.zza(r1, r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0378
        L_0x0242:
            com.google.android.gms.internal.ads.zzle r7 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r11 = r3.first     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r11 = (java.lang.Integer) r11     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.intValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r3 = r3.second     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r3.longValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r7.<init>(r11, r14)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r7     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x028b
        L_0x025a:
            com.google.android.gms.internal.ads.zzle r3 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzatc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r7 != 0) goto L_0x028a
            com.google.android.gms.internal.ads.zzlr r3 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r3 = r3.isEmpty()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x026f
            r8.zza(r1, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0378
        L_0x026f:
            android.util.Pair r3 = r8.zze(r9, r12)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r4 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r7 = r3.first     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r7 = (java.lang.Integer) r7     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.intValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r3 = r3.second     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r3.longValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4.<init>(r7, r14)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r4     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x028a:
            r4 = 0
        L_0x028b:
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x0292
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0294
        L_0x0292:
            com.google.android.gms.internal.ads.zzld r3 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0294:
            if (r3 == 0) goto L_0x0375
            com.google.android.gms.internal.ads.zzlr r7 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r11 = r3.zzasx     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.zzc(r11)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 != r5) goto L_0x02f8
            int r6 = r3.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r7 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r8.zza(r6, r2, r7)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r5) goto L_0x02af
            r8.zza(r1, r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0378
        L_0x02af:
            com.google.android.gms.internal.ads.zzlr r6 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r7 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r6.zza(r2, r7, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.util.Pair r2 = r8.zze(r9, r12)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r6 = r2.first     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r6 = r6.intValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r2 = r2.second     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r2.longValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r7 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r6, r7, r10)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r2 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r2 = r2.zzasx     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzatb = r5     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x02d7:
            com.google.android.gms.internal.ads.zzld r7 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x02eb
            com.google.android.gms.internal.ads.zzld r3 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r7 = r3.zzasx     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r7 = r7.equals(r2)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x02e7
            r7 = r6
            goto L_0x02e8
        L_0x02e7:
            r7 = -1
        L_0x02e8:
            r3.zzatb = r7     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x02d7
        L_0x02eb:
            long r2 = r8.zzd(r6, r11)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r5 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5.<init>(r6, r2)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r5     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0375
        L_0x02f8:
            boolean r2 = r8.zzad(r7)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzd(r7, r2)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != r2) goto L_0x0305
            r2 = 1
            goto L_0x0306
        L_0x0305:
            r2 = 0
        L_0x0306:
            com.google.android.gms.internal.ads.zzle r11 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == r11) goto L_0x031f
            com.google.android.gms.internal.ads.zzle r11 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r12 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r13 = r11.zzatc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.<init>(r7, r13)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r13 = r11.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.zzacl = r13     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r13 = r11.zzacm     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.zzacm = r13     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r12     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x031f:
            com.google.android.gms.internal.ads.zzld r11 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 == 0) goto L_0x0375
            com.google.android.gms.internal.ads.zzld r11 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r12 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r13 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlu r14 = r8.zzars     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r15 = r8.repeatMode     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r12.zza(r7, r13, r14, r15)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == r5) goto L_0x0356
            java.lang.Object r12 = r11.zzasx     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r13 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r14 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r13 = r13.zza(r7, r14, r10)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r13 = r13.zzasx     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r12 = r12.equals(r13)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x0356
            boolean r3 = r8.zzad(r7)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.zzd(r7, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 != r3) goto L_0x0352
            r3 = 1
            goto L_0x0353
        L_0x0352:
            r3 = 0
        L_0x0353:
            r2 = r2 | r3
            r3 = r11
            goto L_0x031f
        L_0x0356:
            if (r2 != 0) goto L_0x036c
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r3 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r3.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r8.zzd(r2, r5)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r3 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.<init>(r2, r5)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0375
        L_0x036c:
            r8.zzast = r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zzatf = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            zza(r11)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0375:
            r8.zzb(r1, r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0378:
            return r10
        L_0x0379:
            r8.zzj(r10)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzll r1 = r8.zzasj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzee()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r10)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            monitor-enter(r34)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzacf = r10     // Catch:{ all -> 0x038c }
            r34.notifyAll()     // Catch:{ all -> 0x038c }
            monitor-exit(r34)     // Catch:{ all -> 0x038c }
            return r10
        L_0x038c:
            r0 = move-exception
            r1 = r0
            monitor-exit(r34)     // Catch:{ all -> 0x038c }
            throw r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0390:
            r34.zzds()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            return r10
        L_0x0394:
            java.lang.Object r1 = r1.obj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzln r1 = (com.google.android.gms.internal.ads.zzln) r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzso r2 = r8.zzasn     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x03a3
            com.google.android.gms.internal.ads.zzso r2 = r8.zzasn     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzln r1 = r2.zzb(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x03a9
        L_0x03a3:
            com.google.android.gms.internal.ads.zzsw r2 = r8.zzask     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzln r1 = r2.zzb(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x03a9:
            r8.zzasc = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r2 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r2.obtainMessage(r11, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            return r10
        L_0x03b5:
            java.lang.Object r1 = r1.obj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlf r1 = (com.google.android.gms.internal.ads.zzlf) r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x03c6
            int r2 = r8.zzasq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2 + r10
            r8.zzasq = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasr = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0453
        L_0x03c6:
            android.util.Pair r2 = r8.zza(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x03ec
            com.google.android.gms.internal.ads.zzle r1 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.<init>(r9, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r1 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r2 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r1.obtainMessage(r15, r10, r9, r2)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r1 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.<init>(r9, r12)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r15)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzj(r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0453
        L_0x03ec:
            long r3 = r1.zzatj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r1 != 0) goto L_0x03f4
            r1 = 1
            goto L_0x03f5
        L_0x03f4:
            r1 = 0
        L_0x03f5:
            java.lang.Object r3 = r2.first     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.intValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r2 = r2.second     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r2.longValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r2 = r8.zzasd     // Catch:{ all -> 0x0454 }
            int r2 = r2.zzatb     // Catch:{ all -> 0x0454 }
            if (r3 != r2) goto L_0x0430
            r6 = 1000(0x3e8, double:4.94E-321)
            long r11 = r4 / r6
            com.google.android.gms.internal.ads.zzle r2 = r8.zzasd     // Catch:{ all -> 0x0454 }
            long r13 = r2.zzacl     // Catch:{ all -> 0x0454 }
            long r13 = r13 / r6
            int r2 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r2 != 0) goto L_0x0430
            com.google.android.gms.internal.ads.zzle r2 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.<init>(r3, r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r2 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0425
            r1 = 1
            goto L_0x0426
        L_0x0425:
            r1 = 0
        L_0x0426:
            com.google.android.gms.internal.ads.zzle r3 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r2.obtainMessage(r15, r1, r9, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0453
        L_0x0430:
            long r6 = r8.zzd(r3, r4)     // Catch:{ all -> 0x0454 }
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x043a
            r2 = 1
            goto L_0x043b
        L_0x043a:
            r2 = 0
        L_0x043b:
            r1 = r1 | r2
            com.google.android.gms.internal.ads.zzle r2 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.<init>(r3, r6)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r2 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0449
            r1 = 1
            goto L_0x044a
        L_0x0449:
            r1 = 0
        L_0x044a:
            com.google.android.gms.internal.ads.zzle r3 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r2.obtainMessage(r15, r1, r9, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0453:
            return r10
        L_0x0454:
            r0 = move-exception
            r2 = r0
            com.google.android.gms.internal.ads.zzle r6 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r6.<init>(r3, r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r6     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r3 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0463
            r1 = 1
            goto L_0x0464
        L_0x0463:
            r1 = 0
        L_0x0464:
            com.google.android.gms.internal.ads.zzle r4 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r3.obtainMessage(r15, r1, r9, r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            throw r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x046e:
            long r5 = android.os.SystemClock.elapsedRealtime()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r1 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x047e
            com.google.android.gms.internal.ads.zzql r1 = r8.zzaso     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzjf()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r14 = r5
            goto L_0x06da
        L_0x047e:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x0487
            com.google.android.gms.internal.ads.zzle r1 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x04c5
        L_0x0487:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzatd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x04d2
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzha()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x04d2
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r7 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r2 = r2.zza(r1, r7, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r2.zzack     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r2 != 0) goto L_0x04a8
            goto L_0x04d2
        L_0x04a8:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x04b9
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.index     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r7 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.index     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2 - r7
            r7 = 100
            if (r2 == r7) goto L_0x04d2
        L_0x04b9:
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r7 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlu r14 = r8.zzars     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r15 = r8.repeatMode     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r2.zza(r1, r7, r14, r15)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x04c5:
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzhg()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 < r2) goto L_0x04d5
            com.google.android.gms.internal.ads.zzql r1 = r8.zzaso     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzjf()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x04d2:
            r14 = r5
            goto L_0x05a7
        L_0x04d5:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x04df
            com.google.android.gms.internal.ads.zzle r2 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r2.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x04dd:
            r14 = r5
            goto L_0x0534
        L_0x04df:
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r7 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r1, r7, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlu r7 = r8.zzars     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r9, r7, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x04f0
            goto L_0x04dd
        L_0x04f0:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzgz()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r7 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r14 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r14 = r14.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r15 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r7 = r7.zza(r14, r15, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r7.zzack     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1 + r14
            long r14 = r8.zzass     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1 - r14
            com.google.android.gms.internal.ads.zzlr r7 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r14 = 0
            r16 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            long r18 = java.lang.Math.max(r3, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1 = r34
            r2 = r7
            r3 = r14
            r14 = r5
            r4 = r16
            r6 = r18
            android.util.Pair r1 = r1.zza(r2, r3, r4, r6)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05a7
            java.lang.Object r2 = r1.first     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.intValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r1 = r1.second     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r1.longValue()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1 = r2
        L_0x0534:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x053f
            r5 = 60000000(0x3938700, double:2.96439388E-316)
            long r5 = r5 + r3
        L_0x053c:
            r23 = r5
            goto L_0x0555
        L_0x053f:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r2.zzgz()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r7 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r11 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r2 = r2.zza(r7, r11, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r12 = r2.zzack     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r5 + r12
            goto L_0x053c
        L_0x0555:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x055c
            r29 = 0
            goto L_0x0563
        L_0x055c:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.index     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2 + r10
            r29 = r2
        L_0x0563:
            boolean r31 = r8.zzad(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r5 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r1, r5, r10)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = new com.google.android.gms.internal.ads.zzld     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlo[] r5 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlp[] r6 = r8.zzasi     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzrp r7 = r8.zzarp     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzll r11 = r8.zzasj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzql r12 = r8.zzaso     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r13 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r13 = r13.zzasx     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r20 = r2
            r21 = r5
            r22 = r6
            r25 = r7
            r26 = r11
            r27 = r12
            r28 = r13
            r30 = r1
            r32 = r3
            r20.<init>(r21, r22, r23, r25, r26, r27, r28, r29, r30, r31, r32)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x059b
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzatf = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x059b:
            r8.zzast = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r1 = r1.zzasw     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zza(r8, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzi(r10)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x05a7:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05c0
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzha()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05b4
            goto L_0x05c0
        L_0x05b4:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05c3
            boolean r1 = r8.zzarx     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x05c3
            r34.zzgy()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x05c3
        L_0x05c0:
            r8.zzi(r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x05c3:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x06da
        L_0x05c7:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == r2) goto L_0x0604
            long r1 = r8.zzass     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r3.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzata     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 < 0) goto L_0x0604
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.release()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r1.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzb(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r1 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzatc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.<init>(r2, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzgw()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r1 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 5
            com.google.android.gms.internal.ads.zzle r3 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r1.obtainMessage(r2, r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x05c7
        L_0x0604:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzatd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x062e
            r1 = 0
        L_0x060b:
            com.google.android.gms.internal.ads.zzlo[] r2 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 >= r2) goto L_0x06da
            com.google.android.gms.internal.ads.zzlo[] r2 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = r2[r1]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqw[] r3 = r3.zzasy     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3 = r3[r1]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x062b
            com.google.android.gms.internal.ads.zzqw r4 = r2.zzgk()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 != r3) goto L_0x062b
            boolean r3 = r2.zzgl()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x062b
            r2.zzgm()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x062b:
            int r1 = r1 + 1
            goto L_0x060b
        L_0x062e:
            r1 = 0
        L_0x062f:
            com.google.android.gms.internal.ads.zzlo[] r2 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 >= r2) goto L_0x0651
            com.google.android.gms.internal.ads.zzlo[] r2 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = r2[r1]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqw[] r3 = r3.zzasy     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3 = r3[r1]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqw r4 = r2.zzgk()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 != r3) goto L_0x06da
            if (r3 == 0) goto L_0x064e
            boolean r2 = r2.zzgl()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x064e
            goto L_0x06da
        L_0x064e:
            int r1 = r1 + 1
            goto L_0x062f
        L_0x0651:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r1.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x06da
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r1.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzacs     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x06da
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzrr r1 = r1.zzatg     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r2.zzatf     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasu = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzrr r2 = r2.zzatg     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r3 = r3.zzasw     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzja()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0680
            r3 = 1
            goto L_0x0681
        L_0x0680:
            r3 = 0
        L_0x0681:
            r4 = 0
        L_0x0682:
            com.google.android.gms.internal.ads.zzlo[] r5 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r5 = r5.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 >= r5) goto L_0x06da
            com.google.android.gms.internal.ads.zzlo[] r5 = r8.zzaro     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = r5[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzro r6 = r1.zzblz     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzrm r6 = r6.zzbi(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r6 == 0) goto L_0x06d7
            if (r3 != 0) goto L_0x06d4
            boolean r6 = r5.zzgn()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r6 != 0) goto L_0x06d7
            com.google.android.gms.internal.ads.zzro r6 = r2.zzblz     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzrm r6 = r6.zzbi(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlq[] r7 = r1.zzbmb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r7 = r7[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlq[] r11 = r2.zzbmb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11 = r11[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r6 == 0) goto L_0x06d4
            boolean r7 = r11.equals(r7)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x06d4
            int r7 = r6.length()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlh[] r7 = new com.google.android.gms.internal.ads.zzlh[r7]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11 = 0
        L_0x06b8:
            int r12 = r7.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 >= r12) goto L_0x06c4
            com.google.android.gms.internal.ads.zzlh r12 = r6.zzbf(r11)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r7[r11] = r12     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11 + 1
            goto L_0x06b8
        L_0x06c4:
            com.google.android.gms.internal.ads.zzld r6 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqw[] r6 = r6.zzasy     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r6 = r6[r4]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r11 = r8.zzasu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r11.zzgz()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5.zza(r7, r6, r11)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x06d7
        L_0x06d4:
            r5.zzgm()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x06d7:
            int r4 = r4 + 1
            goto L_0x0682
        L_0x06da:
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 10
            if (r1 != 0) goto L_0x06e8
            r34.zzgx()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzb(r14, r2)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x084a
        L_0x06e8:
            java.lang.String r1 = "doSomeWork"
            com.google.android.gms.internal.ads.zzsx.beginSection(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzgw()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r1 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r1 = r1.zzasw     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzle r4 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r4.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzem(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlo[] r1 = r8.zzasp     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r4 = r1.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = 0
            r6 = 1
            r7 = 1
        L_0x0701:
            if (r5 >= r4) goto L_0x0738
            r11 = r1[r5]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r12 = r8.zzass     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r8.zzacj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.zzc(r12, r2)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x0716
            boolean r2 = r11.zzdx()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0716
            r7 = 1
            goto L_0x0717
        L_0x0716:
            r7 = 0
        L_0x0717:
            boolean r2 = r11.isReady()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x0726
            boolean r2 = r11.zzdx()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0724
            goto L_0x0726
        L_0x0724:
            r2 = 0
            goto L_0x0727
        L_0x0726:
            r2 = 1
        L_0x0727:
            if (r2 != 0) goto L_0x072c
            r11.zzgo()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x072c:
            if (r6 == 0) goto L_0x0732
            if (r2 == 0) goto L_0x0732
            r6 = 1
            goto L_0x0733
        L_0x0732:
            r6 = 0
        L_0x0733:
            int r5 = r5 + 1
            r2 = 10
            goto L_0x0701
        L_0x0738:
            if (r6 != 0) goto L_0x073d
            r34.zzgx()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x073d:
            com.google.android.gms.internal.ads.zzso r1 = r8.zzasn     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0762
            com.google.android.gms.internal.ads.zzso r1 = r8.zzasn     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzln r1 = r1.zzhq()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzln r2 = r8.zzasc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r1.equals(r2)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x0762
            r8.zzasc = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzsw r2 = r8.zzask     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzso r3 = r8.zzasn     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r2 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3 = 7
            android.os.Message r1 = r2.obtainMessage(r3, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0762:
            com.google.android.gms.internal.ads.zzlr r1 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r3 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r1 = r1.zza(r2, r3, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzack     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x0793
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0783
            com.google.android.gms.internal.ads.zzle r3 = r8.zzasd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzacl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x0793
        L_0x0783:
            com.google.android.gms.internal.ads.zzld r3 = r8.zzasv     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r3 = r3.zzatd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x0793
            r3 = 4
            r8.setState(r3)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzdq()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4 = 2
            goto L_0x0813
        L_0x0793:
            int r3 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4 = 2
            if (r3 != r4) goto L_0x07f8
            com.google.android.gms.internal.ads.zzlo[] r3 = r8.zzasp     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 <= 0) goto L_0x07e6
            if (r6 == 0) goto L_0x07e4
            boolean r1 = r8.zzacg     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzacs     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x07ac
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r2.zzatc     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x07b4
        L_0x07ac:
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzqj r2 = r2.zzasw     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r2.zzdu()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07b4:
            r5 = -9223372036854775808
            int r7 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r7 != 0) goto L_0x07d0
            com.google.android.gms.internal.ads.zzld r2 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzatd     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x07c2
            r1 = 1
            goto L_0x07e0
        L_0x07c2:
            com.google.android.gms.internal.ads.zzlr r2 = r8.zzary     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r3 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.zzatb     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r5 = r8.zzart     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlt r2 = r2.zza(r3, r5, r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r2.zzack     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07d0:
            com.google.android.gms.internal.ads.zzll r5 = r8.zzasj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzld r6 = r8.zzast     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r8.zzass     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r6 = r6.zzgz()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r11 - r6
            long r2 = r2 - r11
            boolean r1 = r5.zzf(r2, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07e0:
            if (r1 == 0) goto L_0x07e4
            r1 = 1
            goto L_0x07ea
        L_0x07e4:
            r1 = 0
            goto L_0x07ea
        L_0x07e6:
            boolean r1 = r8.zzec(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07ea:
            if (r1 == 0) goto L_0x0813
            r1 = 3
            r8.setState(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r8.zzabu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0813
            r34.zzdp()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0813
        L_0x07f8:
            int r3 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = 3
            if (r3 != r5) goto L_0x0813
            com.google.android.gms.internal.ads.zzlo[] r3 = r8.zzasp     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 <= 0) goto L_0x0803
            goto L_0x0807
        L_0x0803:
            boolean r6 = r8.zzec(r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0807:
            if (r6 != 0) goto L_0x0813
            boolean r1 = r8.zzabu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzacg = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzdq()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0813:
            int r1 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r4) goto L_0x0824
            com.google.android.gms.internal.ads.zzlo[] r1 = r8.zzasp     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r1.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x081a:
            if (r9 >= r2) goto L_0x0824
            r3 = r1[r9]     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzgo()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r9 = r9 + 1
            goto L_0x081a
        L_0x0824:
            boolean r1 = r8.zzabu     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x082d
            int r1 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 3
            if (r1 == r2) goto L_0x0831
        L_0x082d:
            int r1 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r4) goto L_0x0837
        L_0x0831:
            r1 = 10
            r8.zzb(r14, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0847
        L_0x0837:
            com.google.android.gms.internal.ads.zzlo[] r1 = r8.zzasp     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.length     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0842
            r1 = 1000(0x3e8, double:4.94E-321)
            r8.zzb(r14, r1)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0847
        L_0x0842:
            android.os.Handler r1 = r8.handler     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.removeMessages(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0847:
            com.google.android.gms.internal.ads.zzsx.endSection()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x084a:
            return r10
        L_0x084b:
            r4 = 2
            int r1 = r1.arg1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0852
            r1 = 1
            goto L_0x0853
        L_0x0852:
            r1 = 0
        L_0x0853:
            r8.zzacg = r9     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzabu = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x0860
            r34.zzdq()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzgw()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0877
        L_0x0860:
            int r1 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 3
            if (r1 != r2) goto L_0x086e
            r34.zzdp()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r1 = r8.handler     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0877
        L_0x086e:
            int r1 = r8.state     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r4) goto L_0x0877
            android.os.Handler r1 = r8.handler     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0877:
            return r10
        L_0x0878:
            r4 = 2
            java.lang.Object r2 = r1.obj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzql r2 = (com.google.android.gms.internal.ads.zzql) r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.arg1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0883
            r1 = 1
            goto L_0x0884
        L_0x0883:
            r1 = 0
        L_0x0884:
            android.os.Handler r3 = r8.zzabq     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.sendEmptyMessage(r9)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzj(r10)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzll r3 = r8.zzasj     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzhd()     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x089f
            com.google.android.gms.internal.ads.zzle r1 = new com.google.android.gms.internal.ads.zzle     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r1.<init>(r9, r5)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzasd = r1     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x089f:
            r8.zzaso = r2     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzkv r1 = r8.zzasl     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r1, r10, r8)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Handler r1 = r8.handler     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r4)     // Catch:{ zzku -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            return r10
        L_0x08af:
            r0 = move-exception
            r1 = r0
            r3 = 8
            goto L_0x08d9
        L_0x08b4:
            r0 = move-exception
            r1 = r0
            r3 = 8
            goto L_0x08f5
        L_0x08b9:
            r0 = move-exception
            r1 = r0
            java.lang.String r2 = "ExoPlayerImplInternal"
            java.lang.String r3 = "Internal runtime error."
            android.util.Log.e(r2, r3, r1)
            android.os.Handler r2 = r8.zzabq
            com.google.android.gms.internal.ads.zzku r1 = com.google.android.gms.internal.ads.zzku.zza(r1)
            r3 = 8
            android.os.Message r1 = r2.obtainMessage(r3, r1)
            r1.sendToTarget()
            r34.zzds()
            return r10
        L_0x08d5:
            r0 = move-exception
            r3 = 8
            r1 = r0
        L_0x08d9:
            java.lang.String r2 = "ExoPlayerImplInternal"
            java.lang.String r4 = "Source error."
            android.util.Log.e(r2, r4, r1)
            android.os.Handler r2 = r8.zzabq
            com.google.android.gms.internal.ads.zzku r1 = com.google.android.gms.internal.ads.zzku.zza(r1)
            android.os.Message r1 = r2.obtainMessage(r3, r1)
            r1.sendToTarget()
            r34.zzds()
            return r10
        L_0x08f1:
            r0 = move-exception
            r3 = 8
            r1 = r0
        L_0x08f5:
            java.lang.String r2 = "ExoPlayerImplInternal"
            java.lang.String r4 = "Renderer error."
            android.util.Log.e(r2, r4, r1)
            android.os.Handler r2 = r8.zzabq
            android.os.Message r1 = r2.obtainMessage(r3, r1)
            r1.sendToTarget()
            r34.zzds()
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzlc.handleMessage(android.os.Message):boolean");
    }

    private final void setState(int i) {
        if (this.state != i) {
            this.state = i;
            this.zzabq.obtainMessage(1, i, 0).sendToTarget();
        }
    }

    private final void zzi(boolean z) {
        if (this.zzarx != z) {
            this.zzarx = z;
            this.zzabq.obtainMessage(2, z ? 1 : 0, 0).sendToTarget();
        }
    }

    private final void zzdp() throws zzku {
        this.zzacg = false;
        this.zzask.start();
        for (zzlo start : this.zzasp) {
            start.start();
        }
    }

    private final void zzdq() throws zzku {
        this.zzask.stop();
        for (zzlo zza : this.zzasp) {
            zza(zza);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
     arg types: [int, com.google.android.gms.internal.ads.zzlt, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt */
    private final void zzgw() throws zzku {
        long j;
        zzld zzld = this.zzasv;
        if (zzld != null) {
            long zzja = zzld.zzasw.zzja();
            if (zzja != -9223372036854775807L) {
                zzeb(zzja);
            } else {
                zzlo zzlo = this.zzasm;
                if (zzlo == null || zzlo.zzdx()) {
                    this.zzass = this.zzask.zzdv();
                } else {
                    this.zzass = this.zzasn.zzdv();
                    this.zzask.zzdj(this.zzass);
                }
                zzja = this.zzass - this.zzasv.zzgz();
            }
            this.zzasd.zzacl = zzja;
            this.zzacj = SystemClock.elapsedRealtime() * 1000;
            if (this.zzasp.length == 0) {
                j = Long.MIN_VALUE;
            } else {
                j = this.zzasv.zzasw.zzdu();
            }
            zzle zzle = this.zzasd;
            if (j == Long.MIN_VALUE) {
                j = this.zzary.zza(this.zzasv.zzatb, this.zzart, false).zzack;
            }
            zzle.zzacm = j;
        }
    }

    private final void zzb(long j, long j2) {
        this.handler.removeMessages(2);
        long elapsedRealtime = (j + j2) - SystemClock.elapsedRealtime();
        if (elapsedRealtime <= 0) {
            this.handler.sendEmptyMessage(2);
        } else {
            this.handler.sendEmptyMessageDelayed(2, elapsedRealtime);
        }
    }

    private final long zzd(int i, long j) throws zzku {
        zzld zzld;
        zzdq();
        this.zzacg = false;
        setState(2);
        zzld zzld2 = this.zzasv;
        if (zzld2 == null) {
            zzld zzld3 = this.zzast;
            if (zzld3 != null) {
                zzld3.release();
            }
            zzld = null;
        } else {
            zzld = null;
            while (zzld2 != null) {
                if (zzld2.zzatb != i || !zzld2.zzacs) {
                    zzld2.release();
                } else {
                    zzld = zzld2;
                }
                zzld2 = zzld2.zzatf;
            }
        }
        zzld zzld4 = this.zzasv;
        if (!(zzld4 == zzld && zzld4 == this.zzasu)) {
            for (zzlo disable : this.zzasp) {
                disable.disable();
            }
            this.zzasp = new zzlo[0];
            this.zzasn = null;
            this.zzasm = null;
            this.zzasv = null;
        }
        if (zzld != null) {
            zzld.zzatf = null;
            this.zzast = zzld;
            this.zzasu = zzld;
            zzb(zzld);
            if (this.zzasv.zzate) {
                j = this.zzasv.zzasw.zzen(j);
            }
            zzeb(j);
            zzgy();
        } else {
            this.zzast = null;
            this.zzasu = null;
            this.zzasv = null;
            zzeb(j);
        }
        this.handler.sendEmptyMessage(2);
        return j;
    }

    private final void zzeb(long j) throws zzku {
        long j2;
        zzld zzld = this.zzasv;
        if (zzld == null) {
            j2 = 60000000;
        } else {
            j2 = zzld.zzgz();
        }
        this.zzass = j + j2;
        this.zzask.zzdj(this.zzass);
        for (zzlo zzdx : this.zzasp) {
            zzdx.zzdx(this.zzass);
        }
    }

    private final void zzds() {
        zzj(true);
        this.zzasj.onStopped();
        setState(1);
    }

    private final void zzj(boolean z) {
        this.handler.removeMessages(2);
        this.zzacg = false;
        this.zzask.stop();
        this.zzasn = null;
        this.zzasm = null;
        this.zzass = 60000000;
        for (zzlo zzlo : this.zzasp) {
            try {
                zza(zzlo);
                zzlo.disable();
            } catch (zzku | RuntimeException e) {
                Log.e("ExoPlayerImplInternal", "Stop failed.", e);
            }
        }
        this.zzasp = new zzlo[0];
        zzld zzld = this.zzasv;
        if (zzld == null) {
            zzld = this.zzast;
        }
        zza(zzld);
        this.zzast = null;
        this.zzasu = null;
        this.zzasv = null;
        zzi(false);
        if (z) {
            zzql zzql = this.zzaso;
            if (zzql != null) {
                zzql.zzjg();
                this.zzaso = null;
            }
            this.zzary = null;
        }
    }

    private static void zza(zzlo zzlo) throws zzku {
        if (zzlo.getState() == 2) {
            zzlo.stop();
        }
    }

    private final boolean zzec(long j) {
        if (j == -9223372036854775807L || this.zzasd.zzacl < j) {
            return true;
        }
        return this.zzasv.zzatf != null && this.zzasv.zzatf.zzacs;
    }

    private final void zzgx() throws IOException {
        zzld zzld = this.zzast;
        if (zzld != null && !zzld.zzacs) {
            zzld zzld2 = this.zzasu;
            if (zzld2 == null || zzld2.zzatf == this.zzast) {
                zzlo[] zzloArr = this.zzasp;
                int length = zzloArr.length;
                int i = 0;
                while (i < length) {
                    if (zzloArr[i].zzgl()) {
                        i++;
                    } else {
                        return;
                    }
                }
                this.zzast.zzasw.zziy();
            }
        }
    }

    private final void zza(Object obj, int i) {
        this.zzasd = new zzle(0, 0);
        zzb(obj, i);
        this.zzasd = new zzle(0, -9223372036854775807L);
        setState(4);
        zzj(false);
    }

    private final void zzb(Object obj, int i) {
        this.zzabq.obtainMessage(6, new zzlg(this.zzary, obj, this.zzasd, i)).sendToTarget();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
     arg types: [int, com.google.android.gms.internal.ads.zzlt, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt */
    private final int zza(int i, zzlr zzlr, zzlr zzlr2) {
        int zzhg = zzlr.zzhg();
        int i2 = i;
        int i3 = -1;
        for (int i4 = 0; i4 < zzhg && i3 == -1; i4++) {
            i2 = zzlr.zza(i2, this.zzart, this.zzars, this.repeatMode);
            i3 = zzlr2.zzc(zzlr.zza(i2, this.zzart, true).zzasx);
        }
        return i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
     arg types: [int, com.google.android.gms.internal.ads.zzlt, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
     arg types: [int, com.google.android.gms.internal.ads.zzlu, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu */
    private final boolean zzad(int i) {
        this.zzary.zza(i, this.zzart, false);
        if (this.zzary.zza(0, this.zzars, false).zzaut || this.zzary.zza(i, this.zzart, this.zzars, this.repeatMode) != -1) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
     arg types: [int, com.google.android.gms.internal.ads.zzlt, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt */
    private final Pair<Integer, Long> zza(zzlf zzlf) {
        zzlr zzlr = zzlf.zzary;
        if (zzlr.isEmpty()) {
            zzlr = this.zzary;
        }
        try {
            Pair<Integer, Long> zzb = zzb(zzlr, zzlf.zzati, zzlf.zzatj);
            zzlr zzlr2 = this.zzary;
            if (zzlr2 == zzlr) {
                return zzb;
            }
            int zzc = zzlr2.zzc(zzlr.zza(((Integer) zzb.first).intValue(), this.zzart, true).zzasx);
            if (zzc != -1) {
                return Pair.create(Integer.valueOf(zzc), (Long) zzb.second);
            }
            int zza = zza(((Integer) zzb.first).intValue(), zzlr, this.zzary);
            if (zza == -1) {
                return null;
            }
            this.zzary.zza(zza, this.zzart, false);
            return zze(0, -9223372036854775807L);
        } catch (IndexOutOfBoundsException unused) {
            throw new zzlk(this.zzary, zzlf.zzati, zzlf.zzatj);
        }
    }

    private final Pair<Integer, Long> zze(int i, long j) {
        return zzb(this.zzary, i, -9223372036854775807L);
    }

    private final Pair<Integer, Long> zzb(zzlr zzlr, int i, long j) {
        return zza(zzlr, i, j, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean, long):com.google.android.gms.internal.ads.zzlu
     arg types: [int, com.google.android.gms.internal.ads.zzlu, int, long]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, com.google.android.gms.internal.ads.zzlu, int):int
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean, long):com.google.android.gms.internal.ads.zzlu */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt
     arg types: [int, com.google.android.gms.internal.ads.zzlt, int]
     candidates:
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlu, boolean):com.google.android.gms.internal.ads.zzlu
      com.google.android.gms.internal.ads.zzlr.zza(int, com.google.android.gms.internal.ads.zzlt, boolean):com.google.android.gms.internal.ads.zzlt */
    private final Pair<Integer, Long> zza(zzlr zzlr, int i, long j, long j2) {
        zzsk.zzc(i, 0, zzlr.zzhf());
        zzlr.zza(i, this.zzars, false, j2);
        if (j == -9223372036854775807L) {
            j = this.zzars.zzauw;
            if (j == -9223372036854775807L) {
                return null;
            }
        }
        zzlr.zza(0, this.zzart, false);
        return Pair.create(0, Long.valueOf(this.zzars.zzaux + j));
    }

    private final void zzgy() {
        long j;
        if (!this.zzast.zzacs) {
            j = 0;
        } else {
            j = this.zzast.zzasw.zzix();
        }
        if (j == Long.MIN_VALUE) {
            zzi(false);
            return;
        }
        long zzgz = this.zzass - this.zzast.zzgz();
        boolean zzee = this.zzasj.zzee(j - zzgz);
        zzi(zzee);
        if (zzee) {
            this.zzast.zzasw.zzel(zzgz);
        }
    }

    private static void zza(zzld zzld) {
        while (zzld != null) {
            zzld.release();
            zzld = zzld.zzatf;
        }
    }

    private final void zzb(zzld zzld) throws zzku {
        if (this.zzasv != zzld) {
            boolean[] zArr = new boolean[this.zzaro.length];
            int i = 0;
            int i2 = 0;
            while (true) {
                zzlo[] zzloArr = this.zzaro;
                if (i < zzloArr.length) {
                    zzlo zzlo = zzloArr[i];
                    zArr[i] = zzlo.getState() != 0;
                    zzrm zzbi = zzld.zzatg.zzblz.zzbi(i);
                    if (zzbi != null) {
                        i2++;
                    }
                    if (zArr[i] && (zzbi == null || (zzlo.zzgn() && zzlo.zzgk() == this.zzasv.zzasy[i]))) {
                        if (zzlo == this.zzasm) {
                            this.zzask.zza(this.zzasn);
                            this.zzasn = null;
                            this.zzasm = null;
                        }
                        zza(zzlo);
                        zzlo.disable();
                    }
                    i++;
                } else {
                    this.zzasv = zzld;
                    this.zzabq.obtainMessage(3, zzld.zzatg).sendToTarget();
                    zza(zArr, i2);
                    return;
                }
            }
        }
    }

    private final void zza(boolean[] zArr, int i) throws zzku {
        this.zzasp = new zzlo[i];
        int i2 = 0;
        int i3 = 0;
        while (true) {
            zzlo[] zzloArr = this.zzaro;
            if (i2 < zzloArr.length) {
                zzlo zzlo = zzloArr[i2];
                zzrm zzbi = this.zzasv.zzatg.zzblz.zzbi(i2);
                if (zzbi != null) {
                    int i4 = i3 + 1;
                    this.zzasp[i3] = zzlo;
                    if (zzlo.getState() == 0) {
                        zzlq zzlq = this.zzasv.zzatg.zzbmb[i2];
                        boolean z = this.zzabu && this.state == 3;
                        boolean z2 = !zArr[i2] && z;
                        zzlh[] zzlhArr = new zzlh[zzbi.length()];
                        for (int i5 = 0; i5 < zzlhArr.length; i5++) {
                            zzlhArr[i5] = zzbi.zzbf(i5);
                        }
                        zzlo.zza(zzlq, zzlhArr, this.zzasv.zzasy[i2], this.zzass, z2, this.zzasv.zzgz());
                        zzso zzgj = zzlo.zzgj();
                        if (zzgj != null) {
                            if (this.zzasn == null) {
                                this.zzasn = zzgj;
                                this.zzasm = zzlo;
                                this.zzasn.zzb(this.zzasc);
                            } else {
                                throw zzku.zza(new IllegalStateException("Multiple renderer media clocks enabled."));
                            }
                        }
                        if (z) {
                            zzlo.start();
                        }
                    }
                    i3 = i4;
                }
                i2++;
            } else {
                return;
            }
        }
    }

    public final /* synthetic */ void zza(zzqx zzqx) {
        this.handler.obtainMessage(9, (zzqj) zzqx).sendToTarget();
    }
}
