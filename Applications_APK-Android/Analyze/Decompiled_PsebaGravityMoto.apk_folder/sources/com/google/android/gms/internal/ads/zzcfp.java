package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;

public final class zzcfp {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzbrx>> zza(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<AppEventListener>> zzb(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzbsr>> zzc(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzbro>> zzd(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzbrl>> zze(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzbrw>> zzf(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzxr>> zzg(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzczz>> zzh(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>>
     arg types: [com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcfp.zzc(com.google.android.gms.internal.ads.zzcfz, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<com.google.android.gms.internal.ads.zzbsr>>
      com.google.android.gms.internal.ads.zzcfp.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbuz<T>> */
    public static Set<zzbuz<zzbtk>> zzi(zzcfz zzcfz, Executor executor) {
        return zzc((Object) zzcfz, executor);
    }

    private static <T> Set<zzbuz<T>> zzc(T t, Executor executor) {
        if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcqg)).booleanValue()) {
            return Collections.singleton(new zzbuz(t, executor));
        }
        return Collections.emptySet();
    }
}
