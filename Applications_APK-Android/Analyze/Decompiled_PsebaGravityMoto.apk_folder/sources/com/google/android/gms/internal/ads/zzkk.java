package com.google.android.gms.internal.ads;

import java.util.Arrays;

public final class zzkk {
    private int size;
    private long[] zzaqw;

    public zzkk() {
        this(32);
    }

    private zzkk(int i) {
        this.zzaqw = new long[32];
    }

    public final void add(long j) {
        int i = this.size;
        long[] jArr = this.zzaqw;
        if (i == jArr.length) {
            this.zzaqw = Arrays.copyOf(jArr, i << 1);
        }
        long[] jArr2 = this.zzaqw;
        int i2 = this.size;
        this.size = i2 + 1;
        jArr2[i2] = j;
    }

    public final long get(int i) {
        if (i >= 0 && i < this.size) {
            return this.zzaqw[i];
        }
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(45);
        sb.append("Invalid size ");
        sb.append(i);
        sb.append(", size is ");
        sb.append(i2);
        throw new IndexOutOfBoundsException(sb.toString());
    }

    public final int size() {
        return this.size;
    }
}
