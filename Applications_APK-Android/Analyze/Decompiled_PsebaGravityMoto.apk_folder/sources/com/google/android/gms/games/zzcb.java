package com.google.android.gms.games;

import androidx.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzcb implements PendingResultUtil.ResultConverter<Snapshots.OpenSnapshotResult, Snapshots.OpenSnapshotResult> {
    zzcb() {
    }

    public final /* synthetic */ Object convert(@Nullable Result result) {
        return (Snapshots.OpenSnapshotResult) result;
    }
}
