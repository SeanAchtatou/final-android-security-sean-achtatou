package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzay extends zzav {
    private final /* synthetic */ zzee zzej;
    private final /* synthetic */ zzgm zzek;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzay(zzaw zzaw, GoogleApiClient googleApiClient, zzgm zzgm, zzee zzee) {
        super(googleApiClient);
        this.zzek = zzgm;
        this.zzej = zzee;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzeo) ((zzaw) anyClient).getService()).zza(this.zzek, this.zzej, (String) null, new zzgs(this));
    }
}
