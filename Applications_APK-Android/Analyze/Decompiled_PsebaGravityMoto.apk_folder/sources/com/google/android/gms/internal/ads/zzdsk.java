package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzdsk extends zzdrr<zzdsk> {
    public String zzdrr = null;

    public zzdsk() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        String str = this.zzdrr;
        if (str != null) {
            zzdrp.zzf(1, str);
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        String str = this.zzdrr;
        return str != null ? zzor + zzdrp.zzg(1, str) : zzor;
    }
}
