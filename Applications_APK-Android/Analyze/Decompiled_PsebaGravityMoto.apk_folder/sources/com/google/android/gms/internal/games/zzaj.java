package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzaj extends zzak {
    private final /* synthetic */ int zzjl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaj(zzai zzai, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient, null);
        this.zzjl = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzjl);
    }
}
