package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.List;

@zzard
public final class zzant<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends zzamt {
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zzdgv;
    private final NETWORK_EXTRAS zzdgw;

    public zzant(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter, NETWORK_EXTRAS network_extras) {
        this.zzdgv = mediationAdapter;
        this.zzdgw = network_extras;
    }

    public final zzaar getVideoController() {
        return null;
    }

    public final boolean isInitialized() {
        return true;
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final void showVideo() {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzaiq zzaiq, List<zzaiw> list) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzatk zzatk, List<String> list) {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzxz zzxz, String str, zzatk zzatk, String str2) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzxz zzxz, String str, String str2, zzamv zzamv, zzady zzady, List<String> list) {
    }

    public final void zza(zzxz zzxz, String str) {
    }

    public final void zza(zzxz zzxz, String str, String str2) {
    }

    public final void zzb(IObjectWrapper iObjectWrapper, zzxz zzxz, String str, zzamv zzamv) throws RemoteException {
    }

    public final void zzr(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final void zzs(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final zzana zzsf() {
        return null;
    }

    public final zzand zzsg() {
        return null;
    }

    public final boolean zzsj() {
        return false;
    }

    public final zzafe zzsk() {
        return null;
    }

    public final zzang zzsl() {
        return null;
    }

    public final IObjectWrapper zzse() throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzdgv;
        if (!(mediationAdapter instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzbad.zzep(valueOf.length() != 0 ? "Not a MediationBannerAdapter: ".concat(valueOf) : new String("Not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        try {
            return ObjectWrapper.wrap(((MediationBannerAdapter) mediationAdapter).getBannerView());
        } catch (Throwable th) {
            zzbad.zzc("", th);
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzyd zzyd, zzxz zzxz, String str, zzamv zzamv) throws RemoteException {
        zza(iObjectWrapper, zzyd, zzxz, str, null, zzamv);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzyd zzyd, zzxz zzxz, String str, String str2, zzamv zzamv) throws RemoteException {
        AdSize adSize;
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzdgv;
        if (!(mediationAdapter instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzbad.zzep(valueOf.length() != 0 ? "Not a MediationBannerAdapter: ".concat(valueOf) : new String("Not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        zzbad.zzdp("Requesting banner ad from adapter.");
        try {
            MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter) this.zzdgv;
            zzanu zzanu = new zzanu(zzamv);
            Activity activity = (Activity) ObjectWrapper.unwrap(iObjectWrapper);
            MediationServerParameters zzda = zzda(str);
            int i = 0;
            AdSize[] adSizeArr = {AdSize.SMART_BANNER, AdSize.BANNER, AdSize.IAB_MRECT, AdSize.IAB_BANNER, AdSize.IAB_LEADERBOARD, AdSize.IAB_WIDE_SKYSCRAPER};
            while (true) {
                if (i < 6) {
                    if (adSizeArr[i].getWidth() == zzyd.width && adSizeArr[i].getHeight() == zzyd.height) {
                        adSize = adSizeArr[i];
                        break;
                    }
                    i++;
                } else {
                    adSize = new AdSize(zzb.zza(zzyd.width, zzyd.height, zzyd.zzaap));
                    break;
                }
            }
            mediationBannerAdapter.requestBannerAd(zzanu, activity, zzda, adSize, zzaog.zza(zzxz, zzc(zzxz)), this.zzdgw);
        } catch (Throwable th) {
            zzbad.zzc("", th);
            throw new RemoteException();
        }
    }

    public final Bundle zzsh() {
        return new Bundle();
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzxz zzxz, String str, zzamv zzamv) throws RemoteException {
        zza(iObjectWrapper, zzxz, str, (String) null, zzamv);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzxz zzxz, String str, String str2, zzamv zzamv) throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzdgv;
        if (!(mediationAdapter instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzbad.zzep(valueOf.length() != 0 ? "Not a MediationInterstitialAdapter: ".concat(valueOf) : new String("Not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzbad.zzdp("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzdgv).requestInterstitialAd(new zzanu(zzamv), (Activity) ObjectWrapper.unwrap(iObjectWrapper), zzda(str), zzaog.zza(zzxz, zzc(zzxz)), this.zzdgw);
        } catch (Throwable th) {
            zzbad.zzc("", th);
            throw new RemoteException();
        }
    }

    public final Bundle getInterstitialAdapterInfo() {
        return new Bundle();
    }

    public final void showInterstitial() throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzdgv;
        if (!(mediationAdapter instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzbad.zzep(valueOf.length() != 0 ? "Not a MediationInterstitialAdapter: ".concat(valueOf) : new String("Not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzbad.zzdp("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzdgv).showInterstitial();
        } catch (Throwable th) {
            zzbad.zzc("", th);
            throw new RemoteException();
        }
    }

    public final Bundle zzsi() {
        return new Bundle();
    }

    public final void destroy() throws RemoteException {
        try {
            this.zzdgv.destroy();
        } catch (Throwable th) {
            zzbad.zzc("", th);
            throw new RemoteException();
        }
    }

    public final void pause() throws RemoteException {
        throw new RemoteException();
    }

    public final void resume() throws RemoteException {
        throw new RemoteException();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: SERVER_PARAMETERS
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final SERVER_PARAMETERS zzda(java.lang.String r5) throws android.os.RemoteException {
        /*
            r4 = this;
            if (r5 == 0) goto L_0x0028
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0042 }
            r0.<init>(r5)     // Catch:{ Throwable -> 0x0042 }
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ Throwable -> 0x0042 }
            int r1 = r0.length()     // Catch:{ Throwable -> 0x0042 }
            r5.<init>(r1)     // Catch:{ Throwable -> 0x0042 }
            java.util.Iterator r1 = r0.keys()     // Catch:{ Throwable -> 0x0042 }
        L_0x0014:
            boolean r2 = r1.hasNext()     // Catch:{ Throwable -> 0x0042 }
            if (r2 == 0) goto L_0x002e
            java.lang.Object r2 = r1.next()     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Throwable -> 0x0042 }
            java.lang.String r3 = r0.getString(r2)     // Catch:{ Throwable -> 0x0042 }
            r5.put(r2, r3)     // Catch:{ Throwable -> 0x0042 }
            goto L_0x0014
        L_0x0028:
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ Throwable -> 0x0042 }
            r0 = 0
            r5.<init>(r0)     // Catch:{ Throwable -> 0x0042 }
        L_0x002e:
            com.google.ads.mediation.MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> r0 = r4.zzdgv     // Catch:{ Throwable -> 0x0042 }
            java.lang.Class r0 = r0.getServerParametersType()     // Catch:{ Throwable -> 0x0042 }
            r1 = 0
            if (r0 == 0) goto L_0x0041
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Throwable -> 0x0042 }
            r1 = r0
            com.google.ads.mediation.MediationServerParameters r1 = (com.google.ads.mediation.MediationServerParameters) r1     // Catch:{ Throwable -> 0x0042 }
            r1.load(r5)     // Catch:{ Throwable -> 0x0042 }
        L_0x0041:
            return r1
        L_0x0042:
            r5 = move-exception
            java.lang.String r0 = ""
            com.google.android.gms.internal.ads.zzbad.zzc(r0, r5)
            android.os.RemoteException r5 = new android.os.RemoteException
            r5.<init>()
            goto L_0x004f
        L_0x004e:
            throw r5
        L_0x004f:
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzant.zzda(java.lang.String):com.google.ads.mediation.MediationServerParameters");
    }

    private static boolean zzc(zzxz zzxz) {
        if (zzxz.zzcgq) {
            return true;
        }
        zzyt.zzpa();
        return zzazt.zzwx();
    }
}
