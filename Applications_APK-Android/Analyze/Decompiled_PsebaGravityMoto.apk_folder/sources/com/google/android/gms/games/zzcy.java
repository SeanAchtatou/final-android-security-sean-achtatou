package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzs;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcy extends zzs<Videos.CaptureOverlayStateListener> {
    private final /* synthetic */ ListenerHolder zzbi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcy(VideosClient videosClient, ListenerHolder listenerHolder, ListenerHolder listenerHolder2) {
        super(listenerHolder);
        this.zzbi = listenerHolder2;
    }

    /* access modifiers changed from: protected */
    public final void zzb(zze zze, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException, SecurityException {
        zze.zzg(this.zzbi);
        taskCompletionSource.setResult(null);
    }
}
