package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzju extends IOException {
    private final String zzaqd;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzju(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.String r0 = java.lang.String.valueOf(r4)
            int r1 = r0.length()
            java.lang.String r2 = "Unsupported URI scheme: "
            if (r1 == 0) goto L_0x0011
            java.lang.String r0 = r2.concat(r0)
            goto L_0x0016
        L_0x0011:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r2)
        L_0x0016:
            r3.<init>(r0)
            r3.zzaqd = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzju.<init>(java.lang.String):void");
    }
}
