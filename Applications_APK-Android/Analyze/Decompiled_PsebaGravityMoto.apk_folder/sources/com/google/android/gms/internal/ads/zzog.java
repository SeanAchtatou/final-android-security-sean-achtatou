package com.google.android.gms.internal.ads;

import android.util.Log;
import com.google.android.gms.games.GamesStatusCodes;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.http.HttpStatus;

final class zzog {
    public int height;
    public int number;
    public int type;
    public int width;
    public int zzafu;
    public int zzafv;
    public int zzamf;
    public String zzaor;
    public boolean zzaos;
    public byte[] zzaov;
    public long zzaow;
    public long zzaox;
    public zzne zzatr;
    public int zzatu;
    public byte[] zzatv;
    /* access modifiers changed from: private */
    public String zzauc;
    public int zzbbh;
    public byte[] zzbbi;
    public zznx zzbbj;
    public int zzbbk;
    public int zzbbl;
    public int zzbbm;
    public boolean zzbbn;
    public int zzbbo;
    public int zzbbp;
    public int zzbbq;
    public int zzbbr;
    public int zzbbs;
    public float zzbbt;
    public float zzbbu;
    public float zzbbv;
    public float zzbbw;
    public float zzbbx;
    public float zzbby;
    public float zzbbz;
    public float zzbca;
    public float zzbcb;
    public float zzbcc;
    public int zzbcd;
    public boolean zzbce;
    public boolean zzbcf;
    public zznw zzbcg;

    private zzog() {
        this.width = -1;
        this.height = -1;
        this.zzbbk = -1;
        this.zzbbl = -1;
        this.zzbbm = 0;
        this.zzatv = null;
        this.zzatu = -1;
        this.zzbbn = false;
        this.zzbbo = -1;
        this.zzbbp = -1;
        this.zzbbq = -1;
        this.zzbbr = 1000;
        this.zzbbs = HttpStatus.SC_OK;
        this.zzbbt = -1.0f;
        this.zzbbu = -1.0f;
        this.zzbbv = -1.0f;
        this.zzbbw = -1.0f;
        this.zzbbx = -1.0f;
        this.zzbby = -1.0f;
        this.zzbbz = -1.0f;
        this.zzbca = -1.0f;
        this.zzbcb = -1.0f;
        this.zzbcc = -1.0f;
        this.zzafu = 1;
        this.zzbcd = -1;
        this.zzafv = GamesStatusCodes.STATUS_MILESTONE_CLAIMED_PREVIOUSLY;
        this.zzaow = 0;
        this.zzaox = 0;
        this.zzbcf = true;
        this.zzauc = "eng";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void zza(zznp zznp, int i) throws zzlm {
        char c;
        int i2;
        int i3;
        String str;
        ArrayList arrayList;
        zzlh zzlh;
        zztb zztb;
        int i4;
        String str2;
        String str3;
        List<byte[]> list;
        String str4;
        String str5;
        int zzbs;
        String str6 = this.zzaor;
        int i5 = 3;
        switch (str6.hashCode()) {
            case -2095576542:
                if (str6.equals("V_MPEG4/ISO/AP")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case -2095575984:
                if (str6.equals("V_MPEG4/ISO/SP")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case -1985379776:
                if (str6.equals("A_MS/ACM")) {
                    c = 22;
                    break;
                }
                c = 65535;
                break;
            case -1784763192:
                if (str6.equals("A_TRUEHD")) {
                    c = 17;
                    break;
                }
                c = 65535;
                break;
            case -1730367663:
                if (str6.equals("A_VORBIS")) {
                    c = 10;
                    break;
                }
                c = 65535;
                break;
            case -1482641358:
                if (str6.equals("A_MPEG/L2")) {
                    c = 13;
                    break;
                }
                c = 65535;
                break;
            case -1482641357:
                if (str6.equals("A_MPEG/L3")) {
                    c = 14;
                    break;
                }
                c = 65535;
                break;
            case -1373388978:
                if (str6.equals("V_MS/VFW/FOURCC")) {
                    c = 8;
                    break;
                }
                c = 65535;
                break;
            case -933872740:
                if (str6.equals("S_DVBSUB")) {
                    c = 27;
                    break;
                }
                c = 65535;
                break;
            case -538363189:
                if (str6.equals("V_MPEG4/ISO/ASP")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case -538363109:
                if (str6.equals("V_MPEG4/ISO/AVC")) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            case -425012669:
                if (str6.equals("S_VOBSUB")) {
                    c = 25;
                    break;
                }
                c = 65535;
                break;
            case -356037306:
                if (str6.equals("A_DTS/LOSSLESS")) {
                    c = 20;
                    break;
                }
                c = 65535;
                break;
            case 62923557:
                if (str6.equals("A_AAC")) {
                    c = 12;
                    break;
                }
                c = 65535;
                break;
            case 62923603:
                if (str6.equals("A_AC3")) {
                    c = 15;
                    break;
                }
                c = 65535;
                break;
            case 62927045:
                if (str6.equals("A_DTS")) {
                    c = 18;
                    break;
                }
                c = 65535;
                break;
            case 82338133:
                if (str6.equals("V_VP8")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 82338134:
                if (str6.equals("V_VP9")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 99146302:
                if (str6.equals("S_HDMV/PGS")) {
                    c = 26;
                    break;
                }
                c = 65535;
                break;
            case 444813526:
                if (str6.equals("V_THEORA")) {
                    c = 9;
                    break;
                }
                c = 65535;
                break;
            case 542569478:
                if (str6.equals("A_DTS/EXPRESS")) {
                    c = 19;
                    break;
                }
                c = 65535;
                break;
            case 725957860:
                if (str6.equals("A_PCM/INT/LIT")) {
                    c = 23;
                    break;
                }
                c = 65535;
                break;
            case 855502857:
                if (str6.equals("V_MPEGH/ISO/HEVC")) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            case 1422270023:
                if (str6.equals("S_TEXT/UTF8")) {
                    c = 24;
                    break;
                }
                c = 65535;
                break;
            case 1809237540:
                if (str6.equals("V_MPEG2")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 1950749482:
                if (str6.equals("A_EAC3")) {
                    c = 16;
                    break;
                }
                c = 65535;
                break;
            case 1950789798:
                if (str6.equals("A_FLAC")) {
                    c = 21;
                    break;
                }
                c = 65535;
                break;
            case 1951062397:
                if (str6.equals("A_OPUS")) {
                    c = 11;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        byte[] bArr = null;
        switch (c) {
            case 0:
                str2 = "video/x-vnd.on2.vp8";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 1:
                str2 = "video/x-vnd.on2.vp9";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 2:
                str2 = "video/mpeg2";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 3:
            case 4:
            case 5:
                byte[] bArr2 = this.zzaov;
                arrayList = bArr2 == null ? null : Collections.singletonList(bArr2);
                str3 = "video/mp4v-es";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 6:
                zzta zzf = zzta.zzf(new zzst(this.zzaov));
                list = zzf.zzafw;
                this.zzamf = zzf.zzamf;
                str4 = "video/avc";
                str = str4;
                arrayList = list;
                i3 = -1;
                i2 = -1;
                break;
            case 7:
                zztg zzh = zztg.zzh(new zzst(this.zzaov));
                list = zzh.zzafw;
                this.zzamf = zzh.zzamf;
                str4 = "video/hevc";
                str = str4;
                arrayList = list;
                i3 = -1;
                i2 = -1;
                break;
            case 8:
                arrayList = zza(new zzst(this.zzaov));
                if (arrayList != null) {
                    str3 = "video/wvc1";
                    str = str3;
                    i3 = -1;
                    i2 = -1;
                    break;
                } else {
                    Log.w("MatroskaExtractor", "Unsupported FourCC. Setting mimeType to video/x-unknown");
                    str = "video/x-unknown";
                    i3 = -1;
                    i2 = -1;
                }
            case 9:
                str = "video/x-unknown";
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 10:
                arrayList = zze(this.zzaov);
                str = "audio/vorbis";
                i3 = 8192;
                i2 = -1;
                break;
            case 11:
                ArrayList arrayList2 = new ArrayList(3);
                arrayList2.add(this.zzaov);
                arrayList2.add(ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(this.zzaow).array());
                arrayList2.add(ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(this.zzaox).array());
                arrayList = arrayList2;
                str = "audio/opus";
                i3 = 5760;
                i2 = -1;
                break;
            case 12:
                arrayList = Collections.singletonList(this.zzaov);
                str3 = "audio/mp4a-latm";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 13:
                str5 = "audio/mpeg-L2";
                str = str5;
                arrayList = null;
                i3 = 4096;
                i2 = -1;
                break;
            case 14:
                str5 = "audio/mpeg";
                str = str5;
                arrayList = null;
                i3 = 4096;
                i2 = -1;
                break;
            case 15:
                str2 = "audio/ac3";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 16:
                str2 = "audio/eac3";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 17:
                str2 = "audio/true-hd";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 18:
            case 19:
                str2 = "audio/vnd.dts";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 20:
                str2 = "audio/vnd.dts.hd";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 21:
                arrayList = Collections.singletonList(this.zzaov);
                str3 = "audio/x-flac";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 22:
                if (zzb(new zzst(this.zzaov))) {
                    zzbs = zzsy.zzbs(this.zzbcd);
                    if (zzbs == 0) {
                        int i6 = this.zzbcd;
                        StringBuilder sb = new StringBuilder("audio/x-unknown".length() + 60);
                        sb.append("Unsupported PCM bit depth: ");
                        sb.append(i6);
                        sb.append(". Setting mimeType to ");
                        sb.append("audio/x-unknown");
                        Log.w("MatroskaExtractor", sb.toString());
                    }
                    i2 = zzbs;
                    str = "audio/raw";
                    arrayList = null;
                    i3 = -1;
                    break;
                } else {
                    Log.w("MatroskaExtractor", "audio/x-unknown".length() != 0 ? "Non-PCM MS/ACM is unsupported. Setting mimeType to ".concat("audio/x-unknown") : new String("Non-PCM MS/ACM is unsupported. Setting mimeType to "));
                }
                str = "audio/x-unknown";
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 23:
                zzbs = zzsy.zzbs(this.zzbcd);
                if (zzbs == 0) {
                    int i7 = this.zzbcd;
                    StringBuilder sb2 = new StringBuilder("audio/x-unknown".length() + 60);
                    sb2.append("Unsupported PCM bit depth: ");
                    sb2.append(i7);
                    sb2.append(". Setting mimeType to ");
                    sb2.append("audio/x-unknown");
                    Log.w("MatroskaExtractor", sb2.toString());
                    str = "audio/x-unknown";
                    arrayList = null;
                    i3 = -1;
                    i2 = -1;
                    break;
                }
                i2 = zzbs;
                str = "audio/raw";
                arrayList = null;
                i3 = -1;
                break;
            case 24:
                str2 = "application/x-subrip";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 25:
                arrayList = Collections.singletonList(this.zzaov);
                str3 = "application/vobsub";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 26:
                str2 = "application/pgs";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 27:
                byte[] bArr3 = this.zzaov;
                arrayList = Collections.singletonList(new byte[]{bArr3[0], bArr3[1], bArr3[2], bArr3[3]});
                str3 = "application/dvbsubs";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            default:
                throw new zzlm("Unrecognized codec identifier.");
        }
        boolean z = this.zzbcf | false | (this.zzbce ? (char) 2 : 0);
        if (zzsp.zzav(str)) {
            zzlh = zzlh.zza(Integer.toString(i), str, null, -1, i3, this.zzafu, this.zzafv, i2, arrayList, this.zzatr, z ? 1 : 0, this.zzauc);
            i5 = 1;
        } else if (zzsp.zzbf(str)) {
            if (this.zzbbm == 0) {
                int i8 = this.zzbbk;
                if (i8 == -1) {
                    i8 = this.width;
                }
                this.zzbbk = i8;
                int i9 = this.zzbbl;
                if (i9 == -1) {
                    i9 = this.height;
                }
                this.zzbbl = i9;
            }
            int i10 = this.zzbbk;
            float f = (i10 == -1 || (i4 = this.zzbbl) == -1) ? -1.0f : ((float) (this.height * i10)) / ((float) (this.width * i4));
            if (this.zzbbn) {
                if (!(this.zzbbt == -1.0f || this.zzbbu == -1.0f || this.zzbbv == -1.0f || this.zzbbw == -1.0f || this.zzbbx == -1.0f || this.zzbby == -1.0f || this.zzbbz == -1.0f || this.zzbca == -1.0f || this.zzbcb == -1.0f || this.zzbcc == -1.0f)) {
                    bArr = new byte[25];
                    ByteBuffer wrap = ByteBuffer.wrap(bArr);
                    wrap.put((byte) 0);
                    wrap.putShort((short) ((int) ((this.zzbbt * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzbbu * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzbbv * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzbbw * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzbbx * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzbby * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzbbz * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzbca * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) (this.zzbcb + 0.5f)));
                    wrap.putShort((short) ((int) (this.zzbcc + 0.5f)));
                    wrap.putShort((short) this.zzbbr);
                    wrap.putShort((short) this.zzbbs);
                }
                zztb = new zztb(this.zzbbo, this.zzbbq, this.zzbbp, bArr);
            } else {
                zztb = null;
            }
            zzlh = zzlh.zza(Integer.toString(i), str, null, -1, i3, this.width, this.height, -1.0f, arrayList, -1, f, this.zzatv, this.zzatu, zztb, this.zzatr);
            i5 = 2;
        } else if ("application/x-subrip".equals(str)) {
            zzlh = zzlh.zza(Integer.toString(i), str, (String) null, -1, z ? 1 : 0, this.zzauc, this.zzatr);
        } else if ("application/vobsub".equals(str) || "application/pgs".equals(str) || "application/dvbsubs".equals(str)) {
            zzlh = zzlh.zza(Integer.toString(i), str, (String) null, -1, arrayList, this.zzauc, this.zzatr);
        } else {
            throw new zzlm("Unexpected MIME type.");
        }
        this.zzbcg = zznp.zzd(this.number, i5);
        this.zzbcg.zze(zzlh);
    }

    private static List<byte[]> zza(zzst zzst) throws zzlm {
        try {
            zzst.zzac(16);
            if (zzst.zzkb() != 826496599) {
                return null;
            }
            byte[] bArr = zzst.data;
            for (int position = zzst.getPosition() + 20; position < bArr.length - 4; position++) {
                if (bArr[position] == 0 && bArr[position + 1] == 0 && bArr[position + 2] == 1 && bArr[position + 3] == 15) {
                    return Collections.singletonList(Arrays.copyOfRange(bArr, position, bArr.length));
                }
            }
            throw new zzlm("Failed to find FourCC VC1 initialization data");
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzlm("Error parsing FourCC VC1 codec private");
        }
    }

    private static List<byte[]> zze(byte[] bArr) throws zzlm {
        try {
            if (bArr[0] == 2) {
                int i = 1;
                int i2 = 0;
                while (bArr[i] == -1) {
                    i2 += 255;
                    i++;
                }
                int i3 = i + 1;
                int i4 = i2 + bArr[i];
                int i5 = 0;
                while (bArr[i3] == -1) {
                    i5 += 255;
                    i3++;
                }
                int i6 = i3 + 1;
                int i7 = i5 + bArr[i3];
                if (bArr[i6] == 1) {
                    byte[] bArr2 = new byte[i4];
                    System.arraycopy(bArr, i6, bArr2, 0, i4);
                    int i8 = i6 + i4;
                    if (bArr[i8] == 3) {
                        int i9 = i8 + i7;
                        if (bArr[i9] == 5) {
                            byte[] bArr3 = new byte[(bArr.length - i9)];
                            System.arraycopy(bArr, i9, bArr3, 0, bArr.length - i9);
                            ArrayList arrayList = new ArrayList(2);
                            arrayList.add(bArr2);
                            arrayList.add(bArr3);
                            return arrayList;
                        }
                        throw new zzlm("Error parsing vorbis codec private");
                    }
                    throw new zzlm("Error parsing vorbis codec private");
                }
                throw new zzlm("Error parsing vorbis codec private");
            }
            throw new zzlm("Error parsing vorbis codec private");
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzlm("Error parsing vorbis codec private");
        }
    }

    private static boolean zzb(zzst zzst) throws zzlm {
        try {
            int zzka = zzst.zzka();
            if (zzka == 1) {
                return true;
            }
            if (zzka == 65534) {
                zzst.setPosition(24);
                return zzst.readLong() == zzod.zzazw.getMostSignificantBits() && zzst.readLong() == zzod.zzazw.getLeastSignificantBits();
            }
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzlm("Error parsing MS/ACM codec private");
        }
    }

    /* synthetic */ zzog(zzoe zzoe) {
        this();
    }
}
