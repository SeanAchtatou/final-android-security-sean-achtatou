package com.google.android.gms.internal.ads;

final class zzoq implements zzoo {
    private final int zzand = this.zzbea.zzgg();
    private final zzst zzbea;
    private final int zzbeq = this.zzbea.zzgg();

    public zzoq(zzol zzol) {
        this.zzbea = zzol.zzbea;
        this.zzbea.setPosition(12);
    }

    public final int zzim() {
        return this.zzand;
    }

    public final int zzin() {
        int i = this.zzbeq;
        return i == 0 ? this.zzbea.zzgg() : i;
    }

    public final boolean zzio() {
        return this.zzbeq != 0;
    }
}
