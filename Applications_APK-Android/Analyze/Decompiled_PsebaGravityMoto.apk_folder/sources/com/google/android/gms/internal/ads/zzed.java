package com.google.android.gms.internal.ads;

final class zzed {
    static zzdbq zzya;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcg.zza(java.lang.String, boolean):byte[]
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzcg.zza(byte[], boolean):java.lang.String
      com.google.android.gms.internal.ads.zzcg.zza(java.lang.String, boolean):byte[] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean zzb(com.google.android.gms.internal.ads.zzdy r5) throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException {
        /*
            com.google.android.gms.internal.ads.zzdbq r0 = com.google.android.gms.internal.ads.zzed.zzya
            r1 = 1
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            com.google.android.gms.internal.ads.zzacj<java.lang.String> r0 = com.google.android.gms.internal.ads.zzacu.zzcrl
            com.google.android.gms.internal.ads.zzacr r2 = com.google.android.gms.internal.ads.zzyt.zzpe()
            java.lang.Object r0 = r2.zzd(r0)
            java.lang.String r0 = (java.lang.String) r0
            r2 = 0
            r3 = 0
            if (r0 == 0) goto L_0x001c
            int r4 = r0.length()
            if (r4 != 0) goto L_0x0037
        L_0x001c:
            if (r5 != 0) goto L_0x0020
        L_0x001e:
            r0 = r2
            goto L_0x0034
        L_0x0020:
            java.lang.String r0 = "zu6uZ8u7nNJHsIXbotuBCEBd9hieUh9UBKC94dMPsF422AtJb3FisPSqZI3W+06A"
            java.lang.String r4 = "tm6XtP5M5qvCs+TffoCZhF/AF3Fx7Ow8iqgApPbgXSw="
            java.lang.reflect.Method r5 = r5.zzc(r0, r4)
            if (r5 != 0) goto L_0x002b
            goto L_0x001e
        L_0x002b:
            java.lang.Object[] r0 = new java.lang.Object[r3]
            java.lang.Object r5 = r5.invoke(r2, r0)
            java.lang.String r5 = (java.lang.String) r5
            r0 = r5
        L_0x0034:
            if (r0 != 0) goto L_0x0037
            return r3
        L_0x0037:
            byte[] r5 = com.google.android.gms.internal.ads.zzcg.zza(r0, r1)     // Catch:{ IllegalArgumentException -> 0x005f }
            com.google.android.gms.internal.ads.zzdbu r5 = com.google.android.gms.internal.ads.zzdbz.zzl(r5)     // Catch:{  }
            com.google.android.gms.internal.ads.zzdho r0 = com.google.android.gms.internal.ads.zzddc.zzgpt     // Catch:{  }
            com.google.android.gms.internal.ads.zzdbl.zza(r0)     // Catch:{  }
            com.google.android.gms.internal.ads.zzddg r0 = new com.google.android.gms.internal.ads.zzddg     // Catch:{  }
            r0.<init>()     // Catch:{  }
            com.google.android.gms.internal.ads.zzdcf.zza(r0)     // Catch:{  }
            java.lang.Class<com.google.android.gms.internal.ads.zzdbq> r0 = com.google.android.gms.internal.ads.zzdbq.class
            com.google.android.gms.internal.ads.zzdca r5 = com.google.android.gms.internal.ads.zzdcf.zza(r5, r2, r0)     // Catch:{  }
            java.lang.Object r5 = com.google.android.gms.internal.ads.zzdcf.zza(r5)     // Catch:{  }
            com.google.android.gms.internal.ads.zzdbq r5 = (com.google.android.gms.internal.ads.zzdbq) r5     // Catch:{  }
            com.google.android.gms.internal.ads.zzed.zzya = r5     // Catch:{  }
            com.google.android.gms.internal.ads.zzdbq r5 = com.google.android.gms.internal.ads.zzed.zzya
            if (r5 == 0) goto L_0x005f
            return r1
        L_0x005f:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzed.zzb(com.google.android.gms.internal.ads.zzdy):boolean");
    }
}
