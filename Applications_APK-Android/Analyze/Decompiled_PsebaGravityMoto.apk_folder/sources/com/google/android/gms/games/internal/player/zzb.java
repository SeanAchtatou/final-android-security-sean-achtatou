package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.internal.zzd;

@SafeParcelable.Class(creator = "MostRecentGameInfoEntityCreator")
@SafeParcelable.Reserved({1000})
public final class zzb extends zzd implements zza {
    public static final Parcelable.Creator<zzb> CREATOR = new zzc();
    @SafeParcelable.Field(getter = "getGameId", id = 1)
    private final String zzlf;
    @SafeParcelable.Field(getter = "getGameName", id = 2)
    private final String zzlg;
    @SafeParcelable.Field(getter = "getActivityTimestampMillis", id = 3)
    private final long zzlh;
    @SafeParcelable.Field(getter = "getGameIconImageUri", id = 4)
    private final Uri zzli;
    @SafeParcelable.Field(getter = "getGameHiResImageUri", id = 5)
    private final Uri zzlj;
    @SafeParcelable.Field(getter = "getGameFeaturedImageUri", id = 6)
    private final Uri zzlk;

    public zzb(zza zza) {
        this.zzlf = zza.zzbt();
        this.zzlg = zza.zzbu();
        this.zzlh = zza.zzbv();
        this.zzli = zza.zzbw();
        this.zzlj = zza.zzbx();
        this.zzlk = zza.zzby();
    }

    @SafeParcelable.Constructor
    zzb(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) String str2, @SafeParcelable.Param(id = 3) long j, @SafeParcelable.Param(id = 4) Uri uri, @SafeParcelable.Param(id = 5) Uri uri2, @SafeParcelable.Param(id = 6) Uri uri3) {
        this.zzlf = str;
        this.zzlg = str2;
        this.zzlh = j;
        this.zzli = uri;
        this.zzlj = uri2;
        this.zzlk = uri3;
    }

    static int zza(zza zza) {
        return Objects.hashCode(zza.zzbt(), zza.zzbu(), Long.valueOf(zza.zzbv()), zza.zzbw(), zza.zzbx(), zza.zzby());
    }

    static boolean zza(zza zza, Object obj) {
        if (!(obj instanceof zza)) {
            return false;
        }
        if (zza == obj) {
            return true;
        }
        zza zza2 = (zza) obj;
        return Objects.equal(zza2.zzbt(), zza.zzbt()) && Objects.equal(zza2.zzbu(), zza.zzbu()) && Objects.equal(Long.valueOf(zza2.zzbv()), Long.valueOf(zza.zzbv())) && Objects.equal(zza2.zzbw(), zza.zzbw()) && Objects.equal(zza2.zzbx(), zza.zzbx()) && Objects.equal(zza2.zzby(), zza.zzby());
    }

    static String zzb(zza zza) {
        return Objects.toStringHelper(zza).add("GameId", zza.zzbt()).add("GameName", zza.zzbu()).add("ActivityTimestampMillis", Long.valueOf(zza.zzbv())).add("GameIconUri", zza.zzbw()).add("GameHiResUri", zza.zzbx()).add("GameFeaturedUri", zza.zzby()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zzlf, false);
        SafeParcelWriter.writeString(parcel, 2, this.zzlg, false);
        SafeParcelWriter.writeLong(parcel, 3, this.zzlh);
        SafeParcelWriter.writeParcelable(parcel, 4, this.zzli, i, false);
        SafeParcelWriter.writeParcelable(parcel, 5, this.zzlj, i, false);
        SafeParcelWriter.writeParcelable(parcel, 6, this.zzlk, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final String zzbt() {
        return this.zzlf;
    }

    public final String zzbu() {
        return this.zzlg;
    }

    public final long zzbv() {
        return this.zzlh;
    }

    public final Uri zzbw() {
        return this.zzli;
    }

    public final Uri zzbx() {
        return this.zzlj;
    }

    public final Uri zzby() {
        return this.zzlk;
    }
}
