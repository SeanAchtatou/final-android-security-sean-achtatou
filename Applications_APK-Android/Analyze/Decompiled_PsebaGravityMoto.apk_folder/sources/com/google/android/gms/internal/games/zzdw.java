package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

abstract class zzdw extends Games.zza<TurnBasedMultiplayer.UpdateMatchResult> {
    private zzdw(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzdw(GoogleApiClient googleApiClient, zzdc zzdc) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzdx(this, status);
    }
}
