package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.request.Requests;

abstract class zzce extends Games.zza<Requests.LoadRequestsResult> {
    private zzce(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzce(GoogleApiClient googleApiClient, zzcb zzcb) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzcf(this, status);
    }
}
