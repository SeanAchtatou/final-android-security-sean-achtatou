package com.google.android.gms.internal.ads;

import java.nio.charset.Charset;
import org.apache.http.protocol.HTTP;

public final class zzdrv {
    private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    private static final Charset UTF_8 = Charset.forName(HTTP.UTF_8);
    public static final Object zzhnw = new Object();

    public static void zza(zzdrr zzdrr, zzdrr zzdrr2) {
        if (zzdrr.zzhno != null) {
            zzdrr2.zzhno = (zzdrt) zzdrr.zzhno.clone();
        }
    }
}
