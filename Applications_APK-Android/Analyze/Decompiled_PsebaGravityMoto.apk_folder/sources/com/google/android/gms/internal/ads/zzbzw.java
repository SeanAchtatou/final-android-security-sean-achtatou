package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.Map;
import org.apache.http.protocol.HTTP;

final /* synthetic */ class zzbzw implements zzaho {
    private final zzbzt zzfqj;

    zzbzw(zzbzt zzbzt) {
        this.zzfqj = zzbzt;
    }

    public final void zza(Object obj, Map map) {
        zzbgz zzbgz = (zzbgz) obj;
        zzbgz.zzaai().zza(new zzbzz(this.zzfqj, map));
        String str = (String) map.get("overlayHtml");
        String str2 = (String) map.get("baseUrl");
        if (TextUtils.isEmpty(str2)) {
            zzbgz.loadData(str, "text/html", HTTP.UTF_8);
        } else {
            zzbgz.loadDataWithBaseURL(str2, str, "text/html", HTTP.UTF_8, null);
        }
    }
}
