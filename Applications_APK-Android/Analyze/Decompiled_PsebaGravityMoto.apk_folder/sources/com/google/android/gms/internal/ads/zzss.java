package com.google.android.gms.internal.ads;

public final class zzss {
    private byte[] data;
    private int zzbnl;
    private int zzbnm;
    private int zzbnn;

    public zzss() {
    }

    public zzss(byte[] bArr) {
        this(bArr, bArr.length);
    }

    private zzss(byte[] bArr, int i) {
        this.data = bArr;
        this.zzbnn = i;
    }

    public final int zzbn(int i) {
        int i2;
        int i3;
        byte b;
        byte b2;
        boolean z = false;
        if (i == 0) {
            return 0;
        }
        int i4 = i / 8;
        int i5 = i;
        byte b3 = 0;
        for (int i6 = 0; i6 < i4; i6++) {
            int i7 = this.zzbnm;
            if (i7 != 0) {
                byte[] bArr = this.data;
                int i8 = this.zzbnl;
                b2 = ((bArr[i8 + 1] & 255) >>> (8 - i7)) | ((bArr[i8] & 255) << i7);
            } else {
                b2 = this.data[this.zzbnl];
            }
            i5 -= 8;
            b3 |= (255 & b2) << i5;
            this.zzbnl++;
        }
        if (i5 > 0) {
            int i9 = this.zzbnm + i5;
            byte b4 = (byte) (255 >> (8 - i5));
            if (i9 > 8) {
                byte[] bArr2 = this.data;
                int i10 = this.zzbnl;
                b = (b4 & (((bArr2[i10 + 1] & 255) >> (16 - i9)) | ((bArr2[i10] & 255) << (i9 - 8)))) | b3;
                this.zzbnl = i10 + 1;
            } else {
                byte[] bArr3 = this.data;
                int i11 = this.zzbnl;
                b = (b4 & ((bArr3[i11] & 255) >> (8 - i9))) | b3;
                if (i9 == 8) {
                    this.zzbnl = i11 + 1;
                }
            }
            b3 = b;
            this.zzbnm = i9 % 8;
        }
        int i12 = this.zzbnl;
        if (i12 >= 0 && (i2 = this.zzbnm) >= 0 && i2 < 8 && (i12 < (i3 = this.zzbnn) || (i12 == i3 && i2 == 0))) {
            z = true;
        }
        zzsk.checkState(z);
        return b3;
    }
}
