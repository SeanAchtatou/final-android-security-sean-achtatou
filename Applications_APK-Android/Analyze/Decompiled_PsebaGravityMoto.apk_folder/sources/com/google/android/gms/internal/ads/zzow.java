package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public final class zzow implements zznn, zznu {
    private static final zznq zzazt = new zzox();
    private static final int zzbfz = zzsy.zzay("qt  ");
    private long zzack;
    private final Stack<zzok> zzamj = new Stack<>();
    private int zzamk;
    private int zzamm;
    private long zzamn;
    private int zzamr;
    private int zzams;
    private final zzst zzbaa = new zzst(zzsq.zzaqt);
    private final zzst zzbab = new zzst(4);
    private zznp zzbbf;
    private final zzst zzbga = new zzst(16);
    private int zzbgb;
    private zzst zzbgc;
    private zzoy[] zzbgd;
    private boolean zzbge;

    public final void release() {
    }

    public final boolean zzfc() {
        return true;
    }

    public final boolean zza(zzno zzno) throws IOException, InterruptedException {
        return zzoz.zzd(zzno);
    }

    public final void zza(zznp zznp) {
        this.zzbbf = zznp;
    }

    public final void zzd(long j, long j2) {
        this.zzamj.clear();
        this.zzbgb = 0;
        this.zzamr = 0;
        this.zzams = 0;
        if (j == 0) {
            zzip();
            return;
        }
        zzoy[] zzoyArr = this.zzbgd;
        if (zzoyArr != null) {
            for (zzoy zzoy : zzoyArr) {
                zzpc zzpc = zzoy.zzbgg;
                int zzej = zzpc.zzej(j2);
                if (zzej == -1) {
                    zzej = zzpc.zzek(j2);
                }
                zzoy.zzamy = zzej;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:150:0x019a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x02af A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0006 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(com.google.android.gms.internal.ads.zzno r25, com.google.android.gms.internal.ads.zznt r26) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r24 = this;
            r0 = r24
            r1 = r25
            r2 = r26
        L_0x0006:
            int r3 = r0.zzamk
            r4 = -1
            r5 = 8
            r6 = 1
            if (r3 == 0) goto L_0x019c
            r8 = 262144(0x40000, double:1.295163E-318)
            r10 = 2
            if (r3 == r6) goto L_0x0115
            if (r3 != r10) goto L_0x010f
            r12 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r3 = 0
            r5 = -1
        L_0x001d:
            com.google.android.gms.internal.ads.zzoy[] r14 = r0.zzbgd
            int r15 = r14.length
            if (r3 >= r15) goto L_0x003b
            r14 = r14[r3]
            int r15 = r14.zzamy
            com.google.android.gms.internal.ads.zzpc r11 = r14.zzbgg
            int r11 = r11.zzand
            if (r15 == r11) goto L_0x0038
            com.google.android.gms.internal.ads.zzpc r11 = r14.zzbgg
            long[] r11 = r11.zzahq
            r14 = r11[r15]
            int r11 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r11 >= 0) goto L_0x0038
            r5 = r3
            r12 = r14
        L_0x0038:
            int r3 = r3 + 1
            goto L_0x001d
        L_0x003b:
            if (r5 != r4) goto L_0x003e
            return r4
        L_0x003e:
            r3 = r14[r5]
            com.google.android.gms.internal.ads.zznw r4 = r3.zzbgh
            int r5 = r3.zzamy
            com.google.android.gms.internal.ads.zzpc r11 = r3.zzbgg
            long[] r11 = r11.zzahq
            r12 = r11[r5]
            com.google.android.gms.internal.ads.zzpc r11 = r3.zzbgg
            int[] r11 = r11.zzahp
            r11 = r11[r5]
            com.google.android.gms.internal.ads.zzpa r14 = r3.zzbgf
            int r14 = r14.zzbgk
            if (r14 != r6) goto L_0x005b
            r14 = 8
            long r12 = r12 + r14
            int r11 = r11 + -8
        L_0x005b:
            long r14 = r25.getPosition()
            long r14 = r12 - r14
            int r10 = r0.zzamr
            long r6 = (long) r10
            long r14 = r14 + r6
            r6 = 0
            int r10 = (r14 > r6 ? 1 : (r14 == r6 ? 0 : -1))
            if (r10 < 0) goto L_0x010b
            int r6 = (r14 > r8 ? 1 : (r14 == r8 ? 0 : -1))
            if (r6 < 0) goto L_0x0071
            goto L_0x010b
        L_0x0071:
            int r2 = (int) r14
            r1.zzr(r2)
            com.google.android.gms.internal.ads.zzpa r2 = r3.zzbgf
            int r2 = r2.zzamf
            if (r2 == 0) goto L_0x00d2
            com.google.android.gms.internal.ads.zzst r2 = r0.zzbab
            byte[] r2 = r2.data
            r6 = 0
            r2[r6] = r6
            r7 = 1
            r2[r7] = r6
            r7 = 2
            r2[r7] = r6
            com.google.android.gms.internal.ads.zzpa r2 = r3.zzbgf
            int r2 = r2.zzamf
            com.google.android.gms.internal.ads.zzpa r6 = r3.zzbgf
            int r6 = r6.zzamf
            r7 = 4
            int r6 = 4 - r6
        L_0x0093:
            int r7 = r0.zzamr
            if (r7 >= r11) goto L_0x00e8
            int r7 = r0.zzams
            if (r7 != 0) goto L_0x00c2
            com.google.android.gms.internal.ads.zzst r7 = r0.zzbab
            byte[] r7 = r7.data
            r1.readFully(r7, r6, r2)
            com.google.android.gms.internal.ads.zzst r7 = r0.zzbab
            r8 = 0
            r7.setPosition(r8)
            com.google.android.gms.internal.ads.zzst r7 = r0.zzbab
            int r7 = r7.zzgg()
            r0.zzams = r7
            com.google.android.gms.internal.ads.zzst r7 = r0.zzbaa
            r7.setPosition(r8)
            com.google.android.gms.internal.ads.zzst r7 = r0.zzbaa
            r9 = 4
            r4.zza(r7, r9)
            int r7 = r0.zzamr
            int r7 = r7 + r9
            r0.zzamr = r7
            int r11 = r11 + r6
            goto L_0x0093
        L_0x00c2:
            r8 = 0
            int r7 = r4.zza(r1, r7, r8)
            int r8 = r0.zzamr
            int r8 = r8 + r7
            r0.zzamr = r8
            int r8 = r0.zzams
            int r8 = r8 - r7
            r0.zzams = r8
            goto L_0x0093
        L_0x00d2:
            int r2 = r0.zzamr
            if (r2 >= r11) goto L_0x00e8
            int r2 = r11 - r2
            r6 = 0
            int r2 = r4.zza(r1, r2, r6)
            int r6 = r0.zzamr
            int r6 = r6 + r2
            r0.zzamr = r6
            int r6 = r0.zzams
            int r6 = r6 - r2
            r0.zzams = r6
            goto L_0x00d2
        L_0x00e8:
            r20 = r11
            com.google.android.gms.internal.ads.zzpc r1 = r3.zzbgg
            long[] r1 = r1.zzane
            r17 = r1[r5]
            com.google.android.gms.internal.ads.zzpc r1 = r3.zzbgg
            int[] r1 = r1.zzajr
            r19 = r1[r5]
            r21 = 0
            r22 = 0
            r16 = r4
            r16.zza(r17, r19, r20, r21, r22)
            int r1 = r3.zzamy
            r4 = 1
            int r1 = r1 + r4
            r3.zzamy = r1
            r1 = 0
            r0.zzamr = r1
            r0.zzams = r1
            return r1
        L_0x010b:
            r4 = 1
            r2.zzahv = r12
            return r4
        L_0x010f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            r1.<init>()
            throw r1
        L_0x0115:
            long r3 = r0.zzamn
            int r6 = r0.zzbgb
            long r6 = (long) r6
            long r3 = r3 - r6
            long r6 = r25.getPosition()
            long r6 = r6 + r3
            com.google.android.gms.internal.ads.zzst r10 = r0.zzbgc
            if (r10 == 0) goto L_0x0177
            byte[] r8 = r10.data
            int r9 = r0.zzbgb
            int r4 = (int) r3
            r1.readFully(r8, r9, r4)
            int r3 = r0.zzamm
            int r4 = com.google.android.gms.internal.ads.zzoj.zzajz
            if (r3 != r4) goto L_0x0158
            com.google.android.gms.internal.ads.zzst r3 = r0.zzbgc
            r3.setPosition(r5)
            int r4 = r3.readInt()
            int r5 = com.google.android.gms.internal.ads.zzow.zzbfz
            if (r4 != r5) goto L_0x0141
        L_0x013f:
            r3 = 1
            goto L_0x0155
        L_0x0141:
            r4 = 4
            r3.zzac(r4)
        L_0x0145:
            int r4 = r3.zzjz()
            if (r4 <= 0) goto L_0x0154
            int r4 = r3.readInt()
            int r5 = com.google.android.gms.internal.ads.zzow.zzbfz
            if (r4 != r5) goto L_0x0145
            goto L_0x013f
        L_0x0154:
            r3 = 0
        L_0x0155:
            r0.zzbge = r3
            goto L_0x017f
        L_0x0158:
            java.util.Stack<com.google.android.gms.internal.ads.zzok> r3 = r0.zzamj
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x017f
            java.util.Stack<com.google.android.gms.internal.ads.zzok> r3 = r0.zzamj
            java.lang.Object r3 = r3.peek()
            com.google.android.gms.internal.ads.zzok r3 = (com.google.android.gms.internal.ads.zzok) r3
            com.google.android.gms.internal.ads.zzol r4 = new com.google.android.gms.internal.ads.zzol
            int r5 = r0.zzamm
            com.google.android.gms.internal.ads.zzst r8 = r0.zzbgc
            r4.<init>(r5, r8)
            java.util.List<com.google.android.gms.internal.ads.zzol> r3 = r3.zzama
            r3.add(r4)
            goto L_0x017f
        L_0x0177:
            int r5 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r5 >= 0) goto L_0x0181
            int r4 = (int) r3
            r1.zzr(r4)
        L_0x017f:
            r3 = 0
            goto L_0x0189
        L_0x0181:
            long r8 = r25.getPosition()
            long r8 = r8 + r3
            r2.zzahv = r8
            r3 = 1
        L_0x0189:
            r0.zzei(r6)
            if (r3 == 0) goto L_0x0196
            int r3 = r0.zzamk
            r4 = 2
            if (r3 == r4) goto L_0x0196
            r23 = 1
            goto L_0x0198
        L_0x0196:
            r23 = 0
        L_0x0198:
            if (r23 == 0) goto L_0x0006
            r3 = 1
            return r3
        L_0x019c:
            r3 = 1
            int r6 = r0.zzbgb
            if (r6 != 0) goto L_0x01c6
            com.google.android.gms.internal.ads.zzst r6 = r0.zzbga
            byte[] r6 = r6.data
            r7 = 0
            boolean r6 = r1.zza(r6, r7, r5, r3)
            if (r6 != 0) goto L_0x01af
            r3 = 0
            goto L_0x02ad
        L_0x01af:
            r0.zzbgb = r5
            com.google.android.gms.internal.ads.zzst r3 = r0.zzbga
            r3.setPosition(r7)
            com.google.android.gms.internal.ads.zzst r3 = r0.zzbga
            long r6 = r3.zzge()
            r0.zzamn = r6
            com.google.android.gms.internal.ads.zzst r3 = r0.zzbga
            int r3 = r3.readInt()
            r0.zzamm = r3
        L_0x01c6:
            long r6 = r0.zzamn
            r8 = 1
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 != 0) goto L_0x01e2
            com.google.android.gms.internal.ads.zzst r3 = r0.zzbga
            byte[] r3 = r3.data
            r1.readFully(r3, r5, r5)
            int r3 = r0.zzbgb
            int r3 = r3 + r5
            r0.zzbgb = r3
            com.google.android.gms.internal.ads.zzst r3 = r0.zzbga
            long r6 = r3.zzgh()
            r0.zzamn = r6
        L_0x01e2:
            int r3 = r0.zzamm
            int r6 = com.google.android.gms.internal.ads.zzoj.zzako
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzoj.zzakq
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzoj.zzakr
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzoj.zzaks
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzoj.zzakt
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzoj.zzbcy
            if (r3 != r6) goto L_0x01fd
            goto L_0x01ff
        L_0x01fd:
            r3 = 0
            goto L_0x0200
        L_0x01ff:
            r3 = 1
        L_0x0200:
            if (r3 == 0) goto L_0x022c
            long r5 = r25.getPosition()
            long r7 = r0.zzamn
            long r5 = r5 + r7
            int r3 = r0.zzbgb
            long r7 = (long) r3
            long r5 = r5 - r7
            java.util.Stack<com.google.android.gms.internal.ads.zzok> r3 = r0.zzamj
            com.google.android.gms.internal.ads.zzok r7 = new com.google.android.gms.internal.ads.zzok
            int r8 = r0.zzamm
            r7.<init>(r8, r5)
            r3.add(r7)
            long r7 = r0.zzamn
            int r3 = r0.zzbgb
            long r9 = (long) r3
            int r3 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r3 != 0) goto L_0x0226
            r0.zzei(r5)
            goto L_0x0229
        L_0x0226:
            r24.zzip()
        L_0x0229:
            r3 = 1
            goto L_0x02ad
        L_0x022c:
            int r3 = r0.zzamm
            int r6 = com.google.android.gms.internal.ads.zzoj.zzakz
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzakp
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzala
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzalb
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzals
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzalt
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzalu
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzbcz
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzalv
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzalw
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzbdd
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzalx
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzaly
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzaky
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzajz
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzoj.zzbdk
            if (r3 != r6) goto L_0x026f
            goto L_0x0271
        L_0x026f:
            r3 = 0
            goto L_0x0272
        L_0x0271:
            r3 = 1
        L_0x0272:
            if (r3 == 0) goto L_0x02a7
            int r3 = r0.zzbgb
            if (r3 != r5) goto L_0x027a
            r3 = 1
            goto L_0x027b
        L_0x027a:
            r3 = 0
        L_0x027b:
            com.google.android.gms.internal.ads.zzsk.checkState(r3)
            long r6 = r0.zzamn
            r8 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 > 0) goto L_0x0289
            r3 = 1
            goto L_0x028a
        L_0x0289:
            r3 = 0
        L_0x028a:
            com.google.android.gms.internal.ads.zzsk.checkState(r3)
            com.google.android.gms.internal.ads.zzst r3 = new com.google.android.gms.internal.ads.zzst
            long r6 = r0.zzamn
            int r7 = (int) r6
            r3.<init>(r7)
            r0.zzbgc = r3
            com.google.android.gms.internal.ads.zzst r3 = r0.zzbga
            byte[] r3 = r3.data
            com.google.android.gms.internal.ads.zzst r6 = r0.zzbgc
            byte[] r6 = r6.data
            r7 = 0
            java.lang.System.arraycopy(r3, r7, r6, r7, r5)
            r3 = 1
            r0.zzamk = r3
            goto L_0x02ad
        L_0x02a7:
            r3 = 1
            r5 = 0
            r0.zzbgc = r5
            r0.zzamk = r3
        L_0x02ad:
            if (r3 != 0) goto L_0x0006
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzow.zza(com.google.android.gms.internal.ads.zzno, com.google.android.gms.internal.ads.zznt):int");
    }

    public final long getDurationUs() {
        return this.zzack;
    }

    public final long zzdq(long j) {
        long j2 = Long.MAX_VALUE;
        for (zzoy zzoy : this.zzbgd) {
            zzpc zzpc = zzoy.zzbgg;
            int zzej = zzpc.zzej(j);
            if (zzej == -1) {
                zzej = zzpc.zzek(j);
            }
            long j3 = zzpc.zzahq[zzej];
            if (j3 < j2) {
                j2 = j3;
            }
        }
        return j2;
    }

    private final void zzip() {
        this.zzamk = 0;
        this.zzbgb = 0;
    }

    private final void zzei(long j) throws zzlm {
        zzpa zza;
        while (!this.zzamj.isEmpty() && this.zzamj.peek().zzbdz == j) {
            zzok pop = this.zzamj.pop();
            if (pop.type == zzoj.zzako) {
                long j2 = -9223372036854775807L;
                ArrayList arrayList = new ArrayList();
                zzpo zzpo = null;
                zznr zznr = new zznr();
                zzol zzay = pop.zzay(zzoj.zzbdk);
                if (!(zzay == null || (zzpo = zzom.zza(zzay, this.zzbge)) == null)) {
                    zznr.zzb(zzpo);
                }
                for (int i = 0; i < pop.zzamb.size(); i++) {
                    zzok zzok = pop.zzamb.get(i);
                    if (zzok.type == zzoj.zzakq && (zza = zzom.zza(zzok, pop.zzay(zzoj.zzakp), -9223372036854775807L, (zzne) null, this.zzbge)) != null) {
                        zzpc zza2 = zzom.zza(zza, zzok.zzaz(zzoj.zzakr).zzaz(zzoj.zzaks).zzaz(zzoj.zzakt), zznr);
                        if (zza2.zzand != 0) {
                            zzoy zzoy = new zzoy(zza, zza2, this.zzbbf.zzd(i, zza.type));
                            zzlh zzae = zza.zzaue.zzae(zza2.zzbet + 30);
                            if (zza.type == 1) {
                                if (zznr.zzii()) {
                                    zzae = zzae.zzc(zznr.zzaty, zznr.zzatz);
                                }
                                if (zzpo != null) {
                                    zzae = zzae.zza(zzpo);
                                }
                            }
                            zzoy.zzbgh.zze(zzae);
                            j2 = Math.max(j2, zza.zzack);
                            arrayList.add(zzoy);
                        }
                    }
                }
                this.zzack = j2;
                this.zzbgd = (zzoy[]) arrayList.toArray(new zzoy[arrayList.size()]);
                this.zzbbf.zzfi();
                this.zzbbf.zza(this);
                this.zzamj.clear();
                this.zzamk = 2;
            } else if (!this.zzamj.isEmpty()) {
                this.zzamj.peek().zzamb.add(pop);
            }
        }
        if (this.zzamk != 2) {
            zzip();
        }
    }
}
