package com.google.android.gms.internal.ads;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class zzcqt {
    private String zzgft;

    public static class zza {
        /* access modifiers changed from: private */
        public String zzgft;

        public final zza zzfs(String str) {
            this.zzgft = str;
            return this;
        }
    }

    private zzcqt(zza zza2) {
        this.zzgft = zza2.zzgft;
    }

    public final Set<String> zzalj() {
        HashSet hashSet = new HashSet();
        hashSet.add(this.zzgft.toLowerCase(Locale.ROOT));
        return hashSet;
    }

    public final String zzalk() {
        return this.zzgft.toLowerCase(Locale.ROOT);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final int zzall() {
        char c;
        String str = this.zzgft;
        switch (str.hashCode()) {
            case -1999289321:
                if (str.equals("NATIVE")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case -1372958932:
                if (str.equals("INTERSTITIAL")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 543046670:
                if (str.equals("REWARDED")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1951953708:
                if (str.equals("BANNER")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            return 1;
        }
        if (c == 1) {
            return 3;
        }
        if (c != 2) {
            return c != 3 ? 0 : 7;
        }
        return 6;
    }
}
