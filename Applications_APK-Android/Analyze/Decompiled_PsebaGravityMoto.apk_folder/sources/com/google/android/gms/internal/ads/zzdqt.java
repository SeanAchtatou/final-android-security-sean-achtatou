package com.google.android.gms.internal.ads;

import java.io.IOException;

abstract class zzdqt<T, B> {
    zzdqt() {
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, long j);

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, zzdmr zzdmr);

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, T t);

    /* access modifiers changed from: package-private */
    public abstract void zza(T t, zzdro zzdro) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzdqa zzdqa);

    /* access modifiers changed from: package-private */
    public abstract void zzaa(Object obj);

    /* access modifiers changed from: package-private */
    public abstract T zzah(B b);

    /* access modifiers changed from: package-private */
    public abstract int zzak(T t);

    /* access modifiers changed from: package-private */
    public abstract T zzao(Object obj);

    /* access modifiers changed from: package-private */
    public abstract B zzap(Object obj);

    /* access modifiers changed from: package-private */
    public abstract int zzaq(T t);

    /* access modifiers changed from: package-private */
    public abstract B zzazy();

    /* access modifiers changed from: package-private */
    public abstract void zzb(B b, int i, long j);

    /* access modifiers changed from: package-private */
    public abstract void zzc(B b, int i, int i2);

    /* access modifiers changed from: package-private */
    public abstract void zzc(T t, zzdro zzdro) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zzf(Object obj, T t);

    /* access modifiers changed from: package-private */
    public abstract void zzg(Object obj, B b);

    /* access modifiers changed from: package-private */
    public abstract T zzh(T t, T t2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, com.google.android.gms.internal.ads.zzdmr):void
     arg types: [B, int, com.google.android.gms.internal.ads.zzdmr]
     candidates:
      com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, long):void
      com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, int, com.google.android.gms.internal.ads.zzdmr):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(B r7, com.google.android.gms.internal.ads.zzdqa r8) throws java.io.IOException {
        /*
            r6 = this;
            int r0 = r8.getTag()
            int r1 = r0 >>> 3
            r0 = r0 & 7
            r2 = 1
            if (r0 == 0) goto L_0x0061
            if (r0 == r2) goto L_0x0059
            r3 = 2
            if (r0 == r3) goto L_0x0051
            r3 = 4
            r4 = 3
            if (r0 == r4) goto L_0x0028
            if (r0 == r3) goto L_0x0026
            r3 = 5
            if (r0 != r3) goto L_0x0021
            int r8 = r8.zzavz()
            r6.zzc(r7, r1, r8)
            return r2
        L_0x0021:
            com.google.android.gms.internal.ads.zzdol r7 = com.google.android.gms.internal.ads.zzdok.zzayi()
            throw r7
        L_0x0026:
            r7 = 0
            return r7
        L_0x0028:
            java.lang.Object r0 = r6.zzazy()
            int r4 = r1 << 3
            r3 = r3 | r4
        L_0x002f:
            int r4 = r8.zzaws()
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r4 == r5) goto L_0x003e
            boolean r4 = r6.zza(r0, r8)
            if (r4 != 0) goto L_0x002f
        L_0x003e:
            int r8 = r8.getTag()
            if (r3 != r8) goto L_0x004c
            java.lang.Object r8 = r6.zzah(r0)
            r6.zza(r7, r1, r8)
            return r2
        L_0x004c:
            com.google.android.gms.internal.ads.zzdok r7 = com.google.android.gms.internal.ads.zzdok.zzayh()
            throw r7
        L_0x0051:
            com.google.android.gms.internal.ads.zzdmr r8 = r8.zzawc()
            r6.zza(r7, r1, r8)
            return r2
        L_0x0059:
            long r3 = r8.zzavy()
            r6.zzb(r7, r1, r3)
            return r2
        L_0x0061:
            long r3 = r8.zzavw()
            r6.zza(r7, r1, r3)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqt.zza(java.lang.Object, com.google.android.gms.internal.ads.zzdqa):boolean");
    }
}
