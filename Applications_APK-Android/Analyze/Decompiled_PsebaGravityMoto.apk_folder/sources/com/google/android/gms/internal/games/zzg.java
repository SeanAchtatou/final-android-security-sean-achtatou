package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzg extends zzp {
    private final /* synthetic */ boolean zzjg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzg(zzf zzf, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient, null);
        this.zzjg = z;
    }

    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzc(this, this.zzjg);
    }
}
