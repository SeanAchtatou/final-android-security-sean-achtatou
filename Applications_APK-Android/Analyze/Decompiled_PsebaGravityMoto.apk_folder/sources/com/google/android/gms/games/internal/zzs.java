package com.google.android.gms.games.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzs<L> extends RegisterListenerMethod<zze, L> {
    protected zzs(ListenerHolder<L> listenerHolder) {
        super(listenerHolder);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ void registerListener(Api.AnyClient anyClient, TaskCompletionSource taskCompletionSource) throws RemoteException {
        try {
            zzb((zze) anyClient, taskCompletionSource);
        } catch (SecurityException e) {
            taskCompletionSource.trySetException(e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzb(zze zze, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException;
}
