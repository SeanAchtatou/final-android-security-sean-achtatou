package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.stats.Stats;

abstract class zzcz extends Games.zza<Stats.LoadPlayerStatsResult> {
    private zzcz(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzcz(GoogleApiClient googleApiClient, zzcy zzcy) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzda(this, status);
    }
}
