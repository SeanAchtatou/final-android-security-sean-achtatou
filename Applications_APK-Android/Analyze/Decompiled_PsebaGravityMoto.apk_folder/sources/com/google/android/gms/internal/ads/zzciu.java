package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzciu implements zzczc<zzciv, zzciw> {
    private final String zzdmi;
    private final zzasm zzfxl;
    private final String zzfxz;
    private final Context zzlj;

    public zzciu(Context context, String str, zzasm zzasm, String str2) {
        this.zzlj = context;
        this.zzfxz = str;
        this.zzfxl = zzasm;
        this.zzdmi = str2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r2 = new java.io.InputStreamReader(r6.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        com.google.android.gms.ads.internal.zzk.zzlg();
        r0 = com.google.android.gms.internal.ads.zzaxi.zza(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        com.google.android.gms.common.util.IOUtils.closeQuietly(r2);
        r10.zzek(r0);
        r5.zzfya = r12;
        r5.zzdnh = r0;
        r5.zzab = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0182, code lost:
        if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x01a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0194, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.ads.zzyt.zzpe().zzd(com.google.android.gms.internal.ads.zzacu.zzcvk)).booleanValue() == false) goto L_0x0197;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x019f, code lost:
        throw new com.google.android.gms.internal.ads.zzcif("No Fill", 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01a0, code lost:
        r5.zzfyb = com.google.android.gms.ads.internal.zzk.zzln().elapsedRealtime() - r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r6.disconnect();
        r1.zzfxl.zzub();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01b3, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01b4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01b5, code lost:
        r16 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.ads.zzciw zza(java.lang.String r21, com.google.android.gms.internal.ads.zzasd r22, org.json.JSONObject r23, java.lang.String r24) throws com.google.android.gms.internal.ads.zzcif {
        /*
            r20 = this;
            r1 = r20
            java.lang.String r0 = "Received error HTTP response code: "
            java.lang.String r2 = "doritos_v2"
            java.lang.String r3 = "doritos"
            java.lang.String r4 = ""
            com.google.android.gms.internal.ads.zzciw r5 = new com.google.android.gms.internal.ads.zzciw     // Catch:{ IOException -> 0x0249 }
            r5.<init>()     // Catch:{ IOException -> 0x0249 }
            java.lang.String r6 = "SDK version: "
            java.lang.String r7 = r1.zzfxz     // Catch:{ IOException -> 0x0249 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ IOException -> 0x0249 }
            int r8 = r7.length()     // Catch:{ IOException -> 0x0249 }
            if (r8 == 0) goto L_0x0022
            java.lang.String r6 = r6.concat(r7)     // Catch:{ IOException -> 0x0249 }
            goto L_0x0028
        L_0x0022:
            java.lang.String r7 = new java.lang.String     // Catch:{ IOException -> 0x0249 }
            r7.<init>(r6)     // Catch:{ IOException -> 0x0249 }
            r6 = r7
        L_0x0028:
            com.google.android.gms.internal.ads.zzawz.zzeo(r6)     // Catch:{ IOException -> 0x0249 }
            java.lang.String r6 = "AdRequestServiceImpl: Sending request: "
            java.lang.String r7 = java.lang.String.valueOf(r21)     // Catch:{ IOException -> 0x0249 }
            int r8 = r7.length()     // Catch:{ IOException -> 0x0249 }
            if (r8 == 0) goto L_0x003c
            java.lang.String r6 = r6.concat(r7)     // Catch:{ IOException -> 0x0249 }
            goto L_0x0042
        L_0x003c:
            java.lang.String r7 = new java.lang.String     // Catch:{ IOException -> 0x0249 }
            r7.<init>(r6)     // Catch:{ IOException -> 0x0249 }
            r6 = r7
        L_0x0042:
            com.google.android.gms.internal.ads.zzawz.zzdp(r6)     // Catch:{ IOException -> 0x0249 }
            java.net.URL r6 = new java.net.URL     // Catch:{ IOException -> 0x0249 }
            r7 = r21
            r6.<init>(r7)     // Catch:{ IOException -> 0x0249 }
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ IOException -> 0x0249 }
            r7.<init>()     // Catch:{ IOException -> 0x0249 }
            com.google.android.gms.common.util.Clock r8 = com.google.android.gms.ads.internal.zzk.zzln()     // Catch:{ IOException -> 0x0249 }
            long r8 = r8.elapsedRealtime()     // Catch:{ IOException -> 0x0249 }
            r10 = 0
            r11 = 0
        L_0x005b:
            com.google.android.gms.internal.ads.zzasm r12 = r1.zzfxl     // Catch:{ IOException -> 0x0249 }
            r12.zzua()     // Catch:{ IOException -> 0x0249 }
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ IOException -> 0x0249 }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ IOException -> 0x0249 }
            com.google.android.gms.internal.ads.zzaxi r12 = com.google.android.gms.ads.internal.zzk.zzlg()     // Catch:{ all -> 0x023f }
            android.content.Context r13 = r1.zzlj     // Catch:{ all -> 0x023f }
            java.lang.String r14 = r1.zzfxz     // Catch:{ all -> 0x023f }
            r12.zza(r13, r14, r10, r6)     // Catch:{ all -> 0x023f }
            boolean r12 = android.text.TextUtils.isEmpty(r24)     // Catch:{ all -> 0x023f }
            if (r12 != 0) goto L_0x007f
            java.lang.String r12 = "Cookie"
            r13 = r24
            r6.addRequestProperty(r12, r13)     // Catch:{ all -> 0x023f }
            goto L_0x0081
        L_0x007f:
            r13 = r24
        L_0x0081:
            boolean r12 = r22.zztw()     // Catch:{ all -> 0x023f }
            if (r12 == 0) goto L_0x00be
            java.lang.String r12 = "pii"
            r14 = r23
            org.json.JSONObject r12 = r14.optJSONObject(r12)     // Catch:{ all -> 0x023f }
            if (r12 == 0) goto L_0x00b8
            java.lang.String r15 = r12.optString(r3, r4)     // Catch:{ all -> 0x023f }
            boolean r15 = android.text.TextUtils.isEmpty(r15)     // Catch:{ all -> 0x023f }
            if (r15 != 0) goto L_0x00a4
            java.lang.String r15 = "x-afma-drt-cookie"
            java.lang.String r10 = r12.optString(r3, r4)     // Catch:{ all -> 0x023f }
            r6.addRequestProperty(r15, r10)     // Catch:{ all -> 0x023f }
        L_0x00a4:
            java.lang.String r10 = r12.optString(r2, r4)     // Catch:{ all -> 0x023f }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ all -> 0x023f }
            if (r10 != 0) goto L_0x00c0
            java.lang.String r10 = "x-afma-drt-v2-cookie"
            java.lang.String r12 = r12.optString(r2, r4)     // Catch:{ all -> 0x023f }
            r6.addRequestProperty(r10, r12)     // Catch:{ all -> 0x023f }
            goto L_0x00c0
        L_0x00b8:
            java.lang.String r10 = "DSID signal does not exist."
            com.google.android.gms.internal.ads.zzawz.zzds(r10)     // Catch:{ all -> 0x023f }
            goto L_0x00c0
        L_0x00be:
            r14 = r23
        L_0x00c0:
            r10 = 1
            if (r22 == 0) goto L_0x00f7
            java.lang.String r15 = r22.zztv()     // Catch:{ all -> 0x023f }
            boolean r15 = android.text.TextUtils.isEmpty(r15)     // Catch:{ all -> 0x023f }
            if (r15 != 0) goto L_0x00f7
            r6.setDoOutput(r10)     // Catch:{ all -> 0x023f }
            java.lang.String r15 = r22.zztv()     // Catch:{ all -> 0x023f }
            byte[] r15 = r15.getBytes()     // Catch:{ all -> 0x023f }
            int r12 = r15.length     // Catch:{ all -> 0x023f }
            r6.setFixedLengthStreamingMode(r12)     // Catch:{ all -> 0x023f }
            java.io.BufferedOutputStream r12 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x00f0 }
            java.io.OutputStream r10 = r6.getOutputStream()     // Catch:{ all -> 0x00f0 }
            r12.<init>(r10)     // Catch:{ all -> 0x00f0 }
            r12.write(r15)     // Catch:{ all -> 0x00ec }
            com.google.android.gms.common.util.IOUtils.closeQuietly(r12)     // Catch:{ all -> 0x023f }
            goto L_0x00f8
        L_0x00ec:
            r0 = move-exception
            r16 = r12
            goto L_0x00f3
        L_0x00f0:
            r0 = move-exception
            r16 = 0
        L_0x00f3:
            com.google.android.gms.common.util.IOUtils.closeQuietly(r16)     // Catch:{ all -> 0x023f }
            throw r0     // Catch:{ all -> 0x023f }
        L_0x00f7:
            r15 = 0
        L_0x00f8:
            com.google.android.gms.internal.ads.zzazx r10 = new com.google.android.gms.internal.ads.zzazx     // Catch:{ all -> 0x023f }
            r10.<init>()     // Catch:{ all -> 0x023f }
            r10.zza(r6, r15)     // Catch:{ all -> 0x023f }
            int r12 = r6.getResponseCode()     // Catch:{ all -> 0x023f }
            java.util.Map r15 = r6.getHeaderFields()     // Catch:{ all -> 0x023f }
            java.util.Set r15 = r15.entrySet()     // Catch:{ all -> 0x023f }
            java.util.Iterator r15 = r15.iterator()     // Catch:{ all -> 0x023f }
        L_0x0110:
            boolean r17 = r15.hasNext()     // Catch:{ all -> 0x023f }
            if (r17 == 0) goto L_0x0151
            java.lang.Object r17 = r15.next()     // Catch:{ all -> 0x023f }
            java.util.Map$Entry r17 = (java.util.Map.Entry) r17     // Catch:{ all -> 0x023f }
            java.lang.Object r18 = r17.getKey()     // Catch:{ all -> 0x023f }
            r19 = r2
            r2 = r18
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x023f }
            java.lang.Object r17 = r17.getValue()     // Catch:{ all -> 0x023f }
            r18 = r3
            r3 = r17
            java.util.List r3 = (java.util.List) r3     // Catch:{ all -> 0x023f }
            boolean r17 = r7.containsKey(r2)     // Catch:{ all -> 0x023f }
            if (r17 == 0) goto L_0x0140
            java.lang.Object r2 = r7.get(r2)     // Catch:{ all -> 0x023f }
            java.util.List r2 = (java.util.List) r2     // Catch:{ all -> 0x023f }
            r2.addAll(r3)     // Catch:{ all -> 0x023f }
            goto L_0x014c
        L_0x0140:
            r17 = r4
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x023f }
            r4.<init>(r3)     // Catch:{ all -> 0x023f }
            r7.put(r2, r4)     // Catch:{ all -> 0x023f }
            r4 = r17
        L_0x014c:
            r3 = r18
            r2 = r19
            goto L_0x0110
        L_0x0151:
            r19 = r2
            r18 = r3
            r17 = r4
            r10.zza(r6, r12)     // Catch:{ all -> 0x023f }
            r2 = 200(0xc8, float:2.8E-43)
            r3 = 300(0x12c, float:4.2E-43)
            if (r12 < r2) goto L_0x01bf
            if (r12 >= r3) goto L_0x01bf
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x01b8 }
            java.io.InputStream r0 = r6.getInputStream()     // Catch:{ all -> 0x01b8 }
            r2.<init>(r0)     // Catch:{ all -> 0x01b8 }
            com.google.android.gms.ads.internal.zzk.zzlg()     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = com.google.android.gms.internal.ads.zzaxi.zza(r2)     // Catch:{ all -> 0x01b4 }
            com.google.android.gms.common.util.IOUtils.closeQuietly(r2)     // Catch:{ all -> 0x023f }
            r10.zzek(r0)     // Catch:{ all -> 0x023f }
            r5.zzfya = r12     // Catch:{ all -> 0x023f }
            r5.zzdnh = r0     // Catch:{ all -> 0x023f }
            r5.zzab = r7     // Catch:{ all -> 0x023f }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x023f }
            if (r0 == 0) goto L_0x01a0
            com.google.android.gms.internal.ads.zzacj<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzacu.zzcvk     // Catch:{ all -> 0x023f }
            com.google.android.gms.internal.ads.zzacr r2 = com.google.android.gms.internal.ads.zzyt.zzpe()     // Catch:{ all -> 0x023f }
            java.lang.Object r0 = r2.zzd(r0)     // Catch:{ all -> 0x023f }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x023f }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x023f }
            if (r0 == 0) goto L_0x0197
            goto L_0x01a0
        L_0x0197:
            com.google.android.gms.internal.ads.zzcif r0 = new com.google.android.gms.internal.ads.zzcif     // Catch:{ all -> 0x023f }
            java.lang.String r2 = "No Fill"
            r3 = 3
            r0.<init>(r2, r3)     // Catch:{ all -> 0x023f }
            throw r0     // Catch:{ all -> 0x023f }
        L_0x01a0:
            com.google.android.gms.common.util.Clock r0 = com.google.android.gms.ads.internal.zzk.zzln()     // Catch:{ all -> 0x023f }
            long r2 = r0.elapsedRealtime()     // Catch:{ all -> 0x023f }
            long r2 = r2 - r8
            r5.zzfyb = r2     // Catch:{ all -> 0x023f }
            r6.disconnect()     // Catch:{ IOException -> 0x0249 }
            com.google.android.gms.internal.ads.zzasm r0 = r1.zzfxl     // Catch:{ IOException -> 0x0249 }
            r0.zzub()     // Catch:{ IOException -> 0x0249 }
            return r5
        L_0x01b4:
            r0 = move-exception
            r16 = r2
            goto L_0x01bb
        L_0x01b8:
            r0 = move-exception
            r16 = 0
        L_0x01bb:
            com.google.android.gms.common.util.IOUtils.closeQuietly(r16)     // Catch:{ all -> 0x023f }
            throw r0     // Catch:{ all -> 0x023f }
        L_0x01bf:
            if (r12 < r3) goto L_0x0216
            r2 = 400(0x190, float:5.6E-43)
            if (r12 >= r2) goto L_0x0216
            java.lang.String r2 = "Location"
            java.lang.String r2 = r6.getHeaderField(r2)     // Catch:{ all -> 0x023f }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ all -> 0x023f }
            if (r3 != 0) goto L_0x0209
            java.net.URL r3 = new java.net.URL     // Catch:{ all -> 0x023f }
            r3.<init>(r2)     // Catch:{ all -> 0x023f }
            r2 = 1
            int r11 = r11 + r2
            com.google.android.gms.internal.ads.zzacj<java.lang.Integer> r2 = com.google.android.gms.internal.ads.zzacu.zzcut     // Catch:{ all -> 0x023f }
            com.google.android.gms.internal.ads.zzacr r4 = com.google.android.gms.internal.ads.zzyt.zzpe()     // Catch:{ all -> 0x023f }
            java.lang.Object r2 = r4.zzd(r2)     // Catch:{ all -> 0x023f }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ all -> 0x023f }
            int r2 = r2.intValue()     // Catch:{ all -> 0x023f }
            if (r11 > r2) goto L_0x01fc
            r6.disconnect()     // Catch:{ IOException -> 0x0249 }
            com.google.android.gms.internal.ads.zzasm r2 = r1.zzfxl     // Catch:{ IOException -> 0x0249 }
            r2.zzub()     // Catch:{ IOException -> 0x0249 }
            r6 = r3
            r4 = r17
            r3 = r18
            r2 = r19
            r10 = 0
            goto L_0x005b
        L_0x01fc:
            java.lang.String r0 = "Too many redirects."
            com.google.android.gms.internal.ads.zzawz.zzep(r0)     // Catch:{ all -> 0x023f }
            com.google.android.gms.internal.ads.zzcif r0 = new com.google.android.gms.internal.ads.zzcif     // Catch:{ all -> 0x023f }
            java.lang.String r2 = "Too many redirects"
            r0.<init>(r2)     // Catch:{ all -> 0x023f }
            throw r0     // Catch:{ all -> 0x023f }
        L_0x0209:
            java.lang.String r0 = "No location header to follow redirect."
            com.google.android.gms.internal.ads.zzawz.zzep(r0)     // Catch:{ all -> 0x023f }
            com.google.android.gms.internal.ads.zzcif r0 = new com.google.android.gms.internal.ads.zzcif     // Catch:{ all -> 0x023f }
            java.lang.String r2 = "No location header to follow redirect"
            r0.<init>(r2)     // Catch:{ all -> 0x023f }
            throw r0     // Catch:{ all -> 0x023f }
        L_0x0216:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x023f }
            r3 = 46
            r2.<init>(r3)     // Catch:{ all -> 0x023f }
            r2.append(r0)     // Catch:{ all -> 0x023f }
            r2.append(r12)     // Catch:{ all -> 0x023f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x023f }
            com.google.android.gms.internal.ads.zzawz.zzep(r2)     // Catch:{ all -> 0x023f }
            com.google.android.gms.internal.ads.zzcif r2 = new com.google.android.gms.internal.ads.zzcif     // Catch:{ all -> 0x023f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x023f }
            r4.<init>(r3)     // Catch:{ all -> 0x023f }
            r4.append(r0)     // Catch:{ all -> 0x023f }
            r4.append(r12)     // Catch:{ all -> 0x023f }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x023f }
            r2.<init>(r0)     // Catch:{ all -> 0x023f }
            throw r2     // Catch:{ all -> 0x023f }
        L_0x023f:
            r0 = move-exception
            r6.disconnect()     // Catch:{ IOException -> 0x0249 }
            com.google.android.gms.internal.ads.zzasm r2 = r1.zzfxl     // Catch:{ IOException -> 0x0249 }
            r2.zzub()     // Catch:{ IOException -> 0x0249 }
            throw r0     // Catch:{ IOException -> 0x0249 }
        L_0x0249:
            r0 = move-exception
            java.lang.String r2 = "Error while connecting to ad server: "
            java.lang.String r3 = r0.getMessage()
            java.lang.String r3 = java.lang.String.valueOf(r3)
            int r4 = r3.length()
            if (r4 == 0) goto L_0x025f
            java.lang.String r2 = r2.concat(r3)
            goto L_0x0265
        L_0x025f:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r2)
            r2 = r3
        L_0x0265:
            com.google.android.gms.internal.ads.zzawz.zzep(r2)
            com.google.android.gms.internal.ads.zzcif r2 = new com.google.android.gms.internal.ads.zzcif
            java.lang.String r3 = "Error connecting to ad server:"
            java.lang.String r0 = r0.getMessage()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r4 = r0.length()
            if (r4 == 0) goto L_0x027f
            java.lang.String r0 = r3.concat(r0)
            goto L_0x0284
        L_0x027f:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r3)
        L_0x0284:
            r3 = 2
            r2.<init>(r0, r3)
            goto L_0x028a
        L_0x0289:
            throw r2
        L_0x028a:
            goto L_0x0289
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzciu.zza(java.lang.String, com.google.android.gms.internal.ads.zzasd, org.json.JSONObject, java.lang.String):com.google.android.gms.internal.ads.zzciw");
    }

    public final /* synthetic */ Object apply(Object obj) throws Exception {
        zzciv zzciv = (zzciv) obj;
        return zza(zzciv.zzfxu.getUrl(), zzciv.zzfxu, zzciv.zzfxt, this.zzdmi);
    }
}
