package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.rewarded.RewardItem;

@zzard
public final class zzauh implements RewardItem {
    private final zzatq zzdqw;

    public zzauh(zzatq zzatq) {
        this.zzdqw = zzatq;
    }

    public final String getType() {
        zzatq zzatq = this.zzdqw;
        if (zzatq == null) {
            return null;
        }
        try {
            return zzatq.getType();
        } catch (RemoteException e) {
            zzbad.zzd("Could not forward getType to RewardItem", e);
            return null;
        }
    }

    public final int getAmount() {
        zzatq zzatq = this.zzdqw;
        if (zzatq == null) {
            return 0;
        }
        try {
            return zzatq.getAmount();
        } catch (RemoteException e) {
            zzbad.zzd("Could not forward getAmount to RewardItem", e);
            return 0;
        }
    }
}
