package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzeb extends zzec {
    private final /* synthetic */ int zzkp;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzeb(zzdy zzdy, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient, null);
        this.zzkp = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzkp);
    }
}
