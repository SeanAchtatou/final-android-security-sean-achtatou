package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.EOFException;
import java.io.IOException;

final class zzqf {
    private final zznp zzbbf;
    private final zznn[] zzbix;
    private zznn zzbiy;

    public zzqf(zznn[] zznnArr, zznp zznp) {
        this.zzbix = zznnArr;
        this.zzbbf = zznp;
    }

    public final zznn zza(zzno zzno, Uri uri) throws IOException, InterruptedException {
        zznn zznn = this.zzbiy;
        if (zznn != null) {
            return zznn;
        }
        zznn[] zznnArr = this.zzbix;
        int length = zznnArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            zznn zznn2 = zznnArr[i];
            try {
                if (zznn2.zza(zzno)) {
                    this.zzbiy = zznn2;
                    zzno.zzig();
                    break;
                }
                i++;
            } catch (EOFException unused) {
            } finally {
                zzno.zzig();
            }
        }
        zznn zznn3 = this.zzbiy;
        if (zznn3 != null) {
            zznn3.zza(this.zzbbf);
            return this.zzbiy;
        }
        String zza = zzsy.zza(this.zzbix);
        StringBuilder sb = new StringBuilder(String.valueOf(zza).length() + 58);
        sb.append("None of the available extractors (");
        sb.append(zza);
        sb.append(") could read the stream.");
        throw new zzrc(sb.toString(), uri);
    }

    public final void release() {
        zznn zznn = this.zzbiy;
        if (zznn != null) {
            zznn.release();
            this.zzbiy = null;
        }
    }
}
