package com.google.android.gms.internal.drive;

import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.drive.DriveFile;

final class zzbq implements ListenerHolder.Notifier<DriveFile.DownloadProgressListener> {
    private final /* synthetic */ long zzez;
    private final /* synthetic */ long zzfa;

    zzbq(zzbp zzbp, long j, long j2) {
        this.zzez = j;
        this.zzfa = j2;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((DriveFile.DownloadProgressListener) obj).onProgress(this.zzez, this.zzfa);
    }

    public final void onNotifyListenerFailed() {
    }
}
