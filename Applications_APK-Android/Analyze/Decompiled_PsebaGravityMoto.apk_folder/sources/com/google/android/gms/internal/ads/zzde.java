package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.internal.ads.zzbk;
import com.google.android.gms.internal.ads.zzbp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class zzde extends zzdf {
    private static final String TAG = "zzde";
    private AdvertisingIdClient.Info zzwc;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdf.zza(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.google.android.gms.internal.ads.zzde.zza(java.lang.String, java.lang.String):java.lang.String
      com.google.android.gms.internal.ads.zzdf.zza(android.content.Context, com.google.android.gms.internal.ads.zzbk$zza):com.google.android.gms.internal.ads.zzbp$zza$zza
      com.google.android.gms.internal.ads.zzdd.zza(android.content.Context, com.google.android.gms.internal.ads.zzbk$zza):com.google.android.gms.internal.ads.zzbp$zza$zza
      com.google.android.gms.internal.ads.zzdd.zza(android.content.Context, byte[]):java.lang.String
      com.google.android.gms.internal.ads.zzdf.zza(android.content.Context, boolean):void */
    public static zzde zzb(Context context) {
        zza(context, true);
        return new zzde(context);
    }

    /* access modifiers changed from: protected */
    public final zzbp.zza.C0004zza zza(Context context, View view, Activity activity) {
        return null;
    }

    private zzde(Context context) {
        super(context, "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzci.zza(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzci.zza(byte[], java.lang.String, boolean):byte[]
      com.google.android.gms.internal.ads.zzci.zza(java.lang.String, java.lang.String, boolean):java.lang.String */
    public static String zza(String str, String str2) {
        return zzci.zza(str, str2, true);
    }

    /* access modifiers changed from: protected */
    public final void zza(zzdy zzdy, Context context, zzbp.zza.C0004zza zza, zzbk.zza zza2) {
        if (zzdy.zzxp) {
            AdvertisingIdClient.Info info = this.zzwc;
            if (info != null) {
                String id = info.getId();
                if (!TextUtils.isEmpty(id)) {
                    zza.zzaf(zzef.zzap(id));
                    zza.zzb(zzbp.zza.zzc.DEVICE_IDENTIFIER_ANDROID_AD_ID);
                    zza.zzb(this.zzwc.isLimitAdTrackingEnabled());
                }
                this.zzwc = null;
                return;
            }
            return;
        }
        zza(zzb(zzdy, context, zza, zza2));
    }

    /* access modifiers changed from: protected */
    public final List<Callable<Void>> zzb(zzdy zzdy, Context context, zzbp.zza.C0004zza zza, zzbk.zza zza2) {
        ArrayList arrayList = new ArrayList();
        if (zzdy.zzch() == null) {
            return arrayList;
        }
        arrayList.add(new zzes(zzdy, "3pegtvj7nkb7e3rwh5b+3dnQATJj6aqtaosJ3DkOYPzNGN2w+CoarbJEsY1UQgeA", "/kRTFQbKQx44ublaFMNQ8yNL6QxOrgEofiWDpZSH6zA=", zza, zzdy.zzcd(), 24));
        return arrayList;
    }

    public final void zza(AdvertisingIdClient.Info info) {
        this.zzwc = info;
    }
}
