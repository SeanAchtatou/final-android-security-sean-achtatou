package com.google.android.gms.games.internal;

import androidx.annotation.NonNull;

public interface zzq<R> {
    void release(@NonNull R r);
}
