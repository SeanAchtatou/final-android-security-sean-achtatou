package com.google.android.gms.internal.ads;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.core.os.EnvironmentCompat;
import java.util.ArrayList;
import javax.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

public final class zzctp implements zzcva<zzcto> {
    private final PackageInfo zzdlm;
    private final zzaxb zzduk;
    private final zzcxv zzfjp;
    private final zzbbl zzfqw;

    public zzctp(zzbbl zzbbl, zzcxv zzcxv, @Nullable PackageInfo packageInfo, zzaxb zzaxb) {
        this.zzfqw = zzbbl;
        this.zzfjp = zzcxv;
        this.zzdlm = packageInfo;
        this.zzduk = zzaxb;
    }

    public final zzbbh<zzcto> zzalm() {
        return this.zzfqw.zza(new zzctq(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(ArrayList arrayList, Bundle bundle) {
        int i;
        JSONArray optJSONArray;
        String str;
        bundle.putInt("native_version", 3);
        bundle.putStringArrayList("native_templates", arrayList);
        bundle.putStringArrayList("native_custom_templates", this.zzfjp.zzgld);
        String str2 = "landscape";
        if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcsg)).booleanValue() && this.zzfjp.zzdgs.versionCode > 3) {
            bundle.putBoolean("enable_native_media_orientation", true);
            int i2 = this.zzfjp.zzdgs.zzbqd;
            if (i2 == 1) {
                str = "any";
            } else if (i2 == 2) {
                str = str2;
            } else if (i2 != 3) {
                str = i2 != 4 ? EnvironmentCompat.MEDIA_UNKNOWN : "square";
            } else {
                str = "portrait";
            }
            if (!EnvironmentCompat.MEDIA_UNKNOWN.equals(str)) {
                bundle.putString("native_media_orientation", str);
            }
        }
        int i3 = this.zzfjp.zzdgs.zzbqc;
        if (i3 == 0) {
            str2 = "any";
        } else if (i3 == 1) {
            str2 = "portrait";
        } else if (i3 != 2) {
            str2 = EnvironmentCompat.MEDIA_UNKNOWN;
        }
        if (!EnvironmentCompat.MEDIA_UNKNOWN.equals(str2)) {
            bundle.putString("native_image_orientation", str2);
        }
        bundle.putBoolean("native_multiple_images", this.zzfjp.zzdgs.zzbqe);
        bundle.putBoolean("use_custom_mute", this.zzfjp.zzdgs.zzbqh);
        PackageInfo packageInfo = this.zzdlm;
        if (packageInfo == null) {
            i = 0;
        } else {
            i = packageInfo.versionCode;
        }
        if (i > this.zzduk.zzvq()) {
            this.zzduk.zzvw();
            this.zzduk.zzct(i);
        }
        JSONObject zzvv = this.zzduk.zzvv();
        String jSONArray = (zzvv == null || (optJSONArray = zzvv.optJSONArray(this.zzfjp.zzglb)) == null) ? null : optJSONArray.toString();
        if (!TextUtils.isEmpty(jSONArray)) {
            bundle.putString("native_advanced_settings", jSONArray);
        }
        if (this.zzfjp.zzglg > 1) {
            bundle.putInt("max_num_ads", this.zzfjp.zzglg);
        }
        if (this.zzfjp.zzdne != null) {
            zzaiy zzaiy = this.zzfjp.zzdne;
            int i4 = zzaiy.zzdbe;
            String str3 = "l";
            if (i4 != 1) {
                if (i4 != 2) {
                    int i5 = zzaiy.zzdbe;
                    StringBuilder sb = new StringBuilder(52);
                    sb.append("Instream ad video aspect ratio ");
                    sb.append(i5);
                    sb.append(" is wrong.");
                    zzbad.zzen(sb.toString());
                } else {
                    str3 = "p";
                }
            }
            bundle.putString("ia_var", str3);
            bundle.putBoolean("instr", true);
        }
        if (this.zzfjp.zzamn() != null) {
            bundle.putBoolean("has_delayed_banner_listener", true);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcto zzalt() throws Exception {
        ArrayList<String> arrayList = this.zzfjp.zzglc;
        if (arrayList == null) {
            return zzctr.zzghl;
        }
        if (arrayList.isEmpty()) {
            return zzcts.zzghl;
        }
        return new zzctt(this, arrayList);
    }
}
