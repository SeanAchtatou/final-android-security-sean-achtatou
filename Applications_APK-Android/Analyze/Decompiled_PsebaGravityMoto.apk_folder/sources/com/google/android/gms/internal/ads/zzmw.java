package com.google.android.gms.internal.ads;

import com.google.android.gms.games.GamesStatusCodes;
import java.nio.ShortBuffer;
import java.util.Arrays;
import org.apache.http.HttpStatus;

final class zzmw {
    private final int zzafv;
    private float zzauh;
    private float zzaui;
    private final int zzaxx;
    private final int zzaxy;
    private final int zzaxz;
    private final int zzaya = (this.zzaxz * 2);
    private final short[] zzayb;
    private int zzayc;
    private short[] zzayd;
    private int zzaye;
    private short[] zzayf;
    private int zzayg;
    private short[] zzayh;
    private int zzayi;
    private int zzayj;
    private int zzayk;
    private int zzayl;
    private int zzaym;
    private int zzayn;
    private int zzayo;
    private int zzayp;
    private int zzayq;
    private int zzayr;

    public zzmw(int i, int i2) {
        this.zzafv = i;
        this.zzaxx = i2;
        this.zzaxy = i / HttpStatus.SC_BAD_REQUEST;
        this.zzaxz = i / 65;
        int i3 = this.zzaya;
        this.zzayb = new short[i3];
        this.zzayc = i3;
        this.zzayd = new short[(i3 * i2)];
        this.zzaye = i3;
        this.zzayf = new short[(i3 * i2)];
        this.zzayg = i3;
        this.zzayh = new short[(i3 * i2)];
        this.zzayi = 0;
        this.zzayj = 0;
        this.zzayo = 0;
        this.zzauh = 1.0f;
        this.zzaui = 1.0f;
    }

    public final void setSpeed(float f) {
        this.zzauh = f;
    }

    public final void zza(float f) {
        this.zzaui = f;
    }

    public final void zza(ShortBuffer shortBuffer) {
        int remaining = shortBuffer.remaining();
        int i = this.zzaxx;
        int i2 = remaining / i;
        zzak(i2);
        shortBuffer.get(this.zzayd, this.zzayk * this.zzaxx, ((i * i2) << 1) / 2);
        this.zzayk += i2;
        zzhy();
    }

    public final void zzb(ShortBuffer shortBuffer) {
        int min = Math.min(shortBuffer.remaining() / this.zzaxx, this.zzayl);
        shortBuffer.put(this.zzayf, 0, this.zzaxx * min);
        this.zzayl -= min;
        short[] sArr = this.zzayf;
        int i = this.zzaxx;
        System.arraycopy(sArr, min * i, sArr, 0, this.zzayl * i);
    }

    public final void zzhl() {
        int i;
        int i2 = this.zzayk;
        float f = this.zzauh;
        float f2 = this.zzaui;
        int i3 = this.zzayl + ((int) ((((((float) i2) / (f / f2)) + ((float) this.zzaym)) / f2) + 0.5f));
        zzak((this.zzaya * 2) + i2);
        int i4 = 0;
        while (true) {
            i = this.zzaya;
            int i5 = this.zzaxx;
            if (i4 >= i * 2 * i5) {
                break;
            }
            this.zzayd[(i5 * i2) + i4] = 0;
            i4++;
        }
        this.zzayk += i * 2;
        zzhy();
        if (this.zzayl > i3) {
            this.zzayl = i3;
        }
        this.zzayk = 0;
        this.zzayn = 0;
        this.zzaym = 0;
    }

    public final int zzhx() {
        return this.zzayl;
    }

    private final void zzaj(int i) {
        int i2 = this.zzayl + i;
        int i3 = this.zzaye;
        if (i2 > i3) {
            this.zzaye = i3 + (i3 / 2) + i;
            this.zzayf = Arrays.copyOf(this.zzayf, this.zzaye * this.zzaxx);
        }
    }

    private final void zzak(int i) {
        int i2 = this.zzayk + i;
        int i3 = this.zzayc;
        if (i2 > i3) {
            this.zzayc = i3 + (i3 / 2) + i;
            this.zzayd = Arrays.copyOf(this.zzayd, this.zzayc * this.zzaxx);
        }
    }

    private final void zza(short[] sArr, int i, int i2) {
        zzaj(i2);
        int i3 = this.zzaxx;
        System.arraycopy(sArr, i * i3, this.zzayf, this.zzayl * i3, i3 * i2);
        this.zzayl += i2;
    }

    private final void zzb(short[] sArr, int i, int i2) {
        int i3 = this.zzaya / i2;
        int i4 = this.zzaxx;
        int i5 = i2 * i4;
        int i6 = i * i4;
        for (int i7 = 0; i7 < i3; i7++) {
            int i8 = 0;
            for (int i9 = 0; i9 < i5; i9++) {
                i8 += sArr[(i7 * i5) + i6 + i9];
            }
            this.zzayb[i7] = (short) (i8 / i5);
        }
    }

    private final int zza(short[] sArr, int i, int i2, int i3) {
        int i4 = i * this.zzaxx;
        int i5 = 1;
        int i6 = 0;
        int i7 = 0;
        int i8 = 255;
        while (i2 <= i3) {
            int i9 = 0;
            for (int i10 = 0; i10 < i2; i10++) {
                short s = sArr[i4 + i10];
                short s2 = sArr[i4 + i2 + i10];
                i9 += s >= s2 ? s - s2 : s2 - s;
            }
            if (i9 * i6 < i5 * i2) {
                i6 = i2;
                i5 = i9;
            }
            if (i9 * i8 > i7 * i2) {
                i8 = i2;
                i7 = i9;
            }
            i2++;
        }
        this.zzayq = i5 / i6;
        this.zzayr = i7 / i8;
        return i6;
    }

    private final void zzhy() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = this.zzayl;
        float f = this.zzauh / this.zzaui;
        double d = (double) f;
        int i8 = 1;
        if (d > 1.00001d || d < 0.99999d) {
            int i9 = this.zzayk;
            if (i9 >= this.zzaya) {
                int i10 = 0;
                while (true) {
                    int i11 = this.zzayn;
                    if (i11 > 0) {
                        int min = Math.min(this.zzaya, i11);
                        zza(this.zzayd, i10, min);
                        this.zzayn -= min;
                        i10 += min;
                    } else {
                        short[] sArr = this.zzayd;
                        int i12 = this.zzafv;
                        int i13 = i12 > 4000 ? i12 / GamesStatusCodes.STATUS_SNAPSHOT_NOT_FOUND : 1;
                        if (this.zzaxx == i8 && i13 == i8) {
                            i4 = zza(sArr, i10, this.zzaxy, this.zzaxz);
                        } else {
                            zzb(sArr, i10, i13);
                            int zza = zza(this.zzayb, 0, this.zzaxy / i13, this.zzaxz / i13);
                            if (i13 != i8) {
                                int i14 = zza * i13;
                                int i15 = i13 << 2;
                                int i16 = i14 - i15;
                                int i17 = i14 + i15;
                                int i18 = this.zzaxy;
                                if (i16 >= i18) {
                                    i18 = i16;
                                }
                                int i19 = this.zzaxz;
                                if (i17 > i19) {
                                    i17 = i19;
                                }
                                if (this.zzaxx == i8) {
                                    i4 = zza(sArr, i10, i18, i17);
                                } else {
                                    zzb(sArr, i10, i8);
                                    i4 = zza(this.zzayb, 0, i18, i17);
                                }
                            } else {
                                i4 = zza;
                            }
                        }
                        int i20 = this.zzayq;
                        int i21 = i20 != 0 && this.zzayo != 0 && this.zzayr <= i20 * 3 && (i20 << 1) > this.zzayp * 3 ? this.zzayo : i4;
                        this.zzayp = this.zzayq;
                        this.zzayo = i4;
                        if (d > 1.0d) {
                            short[] sArr2 = this.zzayd;
                            if (f >= 2.0f) {
                                i6 = (int) (((float) i21) / (f - 1.0f));
                            } else {
                                this.zzayn = (int) ((((float) i21) * (2.0f - f)) / (f - 1.0f));
                                i6 = i21;
                            }
                            zzaj(i6);
                            int i22 = i6;
                            zza(i6, this.zzaxx, this.zzayf, this.zzayl, sArr2, i10, sArr2, i10 + i21);
                            this.zzayl += i22;
                            i10 += i21 + i22;
                        } else {
                            int i23 = i21;
                            short[] sArr3 = this.zzayd;
                            if (f < 0.5f) {
                                i5 = (int) ((((float) i23) * f) / (1.0f - f));
                            } else {
                                this.zzayn = (int) ((((float) i23) * ((2.0f * f) - 1.0f)) / (1.0f - f));
                                i5 = i23;
                            }
                            int i24 = i23 + i5;
                            zzaj(i24);
                            int i25 = this.zzaxx;
                            System.arraycopy(sArr3, i10 * i25, this.zzayf, this.zzayl * i25, i25 * i23);
                            zza(i5, this.zzaxx, this.zzayf, this.zzayl + i23, sArr3, i23 + i10, sArr3, i10);
                            this.zzayl += i24;
                            i10 += i5;
                        }
                    }
                    if (this.zzaya + i10 > i9) {
                        break;
                    }
                    i8 = 1;
                }
                int i26 = this.zzayk - i10;
                short[] sArr4 = this.zzayd;
                int i27 = this.zzaxx;
                System.arraycopy(sArr4, i10 * i27, sArr4, 0, i27 * i26);
                this.zzayk = i26;
            }
        } else {
            zza(this.zzayd, 0, this.zzayk);
            this.zzayk = 0;
        }
        float f2 = this.zzaui;
        if (f2 != 1.0f && this.zzayl != i7) {
            int i28 = this.zzafv;
            int i29 = (int) (((float) i28) / f2);
            while (true) {
                if (i29 <= 16384 && i28 <= 16384) {
                    break;
                }
                i29 /= 2;
                i28 /= 2;
            }
            int i30 = this.zzayl - i7;
            int i31 = this.zzaym + i30;
            int i32 = this.zzayg;
            if (i31 > i32) {
                this.zzayg = i32 + (i32 / 2) + i30;
                this.zzayh = Arrays.copyOf(this.zzayh, this.zzayg * this.zzaxx);
            }
            short[] sArr5 = this.zzayf;
            int i33 = this.zzaxx;
            System.arraycopy(sArr5, i7 * i33, this.zzayh, this.zzaym * i33, i33 * i30);
            this.zzayl = i7;
            this.zzaym += i30;
            int i34 = 0;
            while (true) {
                i = this.zzaym;
                if (i34 >= i - 1) {
                    break;
                }
                while (true) {
                    i2 = this.zzayi;
                    int i35 = (i2 + 1) * i29;
                    i3 = this.zzayj;
                    if (i35 <= i3 * i28) {
                        break;
                    }
                    zzaj(1);
                    int i36 = 0;
                    while (true) {
                        int i37 = this.zzaxx;
                        if (i36 >= i37) {
                            break;
                        }
                        short[] sArr6 = this.zzayh;
                        int i38 = (i34 * i37) + i36;
                        short s = sArr6[i38];
                        short s2 = sArr6[i38 + i37];
                        int i39 = this.zzayi;
                        int i40 = i39 * i29;
                        int i41 = (i39 + 1) * i29;
                        int i42 = i41 - (this.zzayj * i28);
                        int i43 = i41 - i40;
                        this.zzayf[(this.zzayl * i37) + i36] = (short) (((s * i42) + ((i43 - i42) * s2)) / i43);
                        i36++;
                    }
                    this.zzayj++;
                    this.zzayl++;
                }
                this.zzayi = i2 + 1;
                if (this.zzayi == i28) {
                    this.zzayi = 0;
                    zzsk.checkState(i3 == i29);
                    this.zzayj = 0;
                }
                i34++;
            }
            int i44 = i - 1;
            if (i44 != 0) {
                short[] sArr7 = this.zzayh;
                int i45 = this.zzaxx;
                System.arraycopy(sArr7, i44 * i45, sArr7, 0, (i - i44) * i45);
                this.zzaym -= i44;
            }
        }
    }

    private static void zza(int i, int i2, short[] sArr, int i3, short[] sArr2, int i4, short[] sArr3, int i5) {
        for (int i6 = 0; i6 < i2; i6++) {
            int i7 = (i4 * i2) + i6;
            int i8 = (i5 * i2) + i6;
            int i9 = (i3 * i2) + i6;
            for (int i10 = 0; i10 < i; i10++) {
                sArr[i9] = (short) (((sArr2[i7] * (i - i10)) + (sArr3[i8] * i10)) / i);
                i9 += i2;
                i7 += i2;
                i8 += i2;
            }
        }
    }
}
