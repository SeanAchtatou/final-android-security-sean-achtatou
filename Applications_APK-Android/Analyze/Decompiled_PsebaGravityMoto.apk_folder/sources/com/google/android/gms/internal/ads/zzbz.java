package com.google.android.gms.internal.ads;

public enum zzbz implements zzdoe {
    ENUM_FALSE(0),
    ENUM_TRUE(1),
    ENUM_FAILURE(2),
    ENUM_UNKNOWN(1000);
    
    private static final zzdof<zzbz> zzeg = new zzca();
    private final int value;

    public final int zzac() {
        return this.value;
    }

    public static zzbz zzj(int i) {
        if (i == 0) {
            return ENUM_FALSE;
        }
        if (i == 1) {
            return ENUM_TRUE;
        }
        if (i == 2) {
            return ENUM_FAILURE;
        }
        if (i != 1000) {
            return null;
        }
        return ENUM_UNKNOWN;
    }

    public static zzdog zzad() {
        return zzcb.zzei;
    }

    private zzbz(int i) {
        this.value = i;
    }
}
