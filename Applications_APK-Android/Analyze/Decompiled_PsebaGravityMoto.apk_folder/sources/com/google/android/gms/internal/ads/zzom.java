package com.google.android.gms.internal.ads;

import android.util.Log;
import android.util.Pair;
import com.google.android.gms.internal.ads.zzpo;
import java.util.ArrayList;

final class zzom {
    private static final int zzbdl = zzsy.zzay("meta");
    private static final int zzbeb = zzsy.zzay("vide");
    private static final int zzbec = zzsy.zzay("soun");
    private static final int zzbed = zzsy.zzay("text");
    private static final int zzbee = zzsy.zzay("sbtl");
    private static final int zzbef = zzsy.zzay("subt");
    private static final int zzbeg = zzsy.zzay("clcp");
    private static final int zzbeh = zzsy.zzay("cenc");

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v46, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r34v10, resolved type: byte[]} */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a3, code lost:
        if (r14 == 0) goto L_0x0093;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.ads.zzpa zza(com.google.android.gms.internal.ads.zzok r43, com.google.android.gms.internal.ads.zzol r44, long r45, com.google.android.gms.internal.ads.zzne r47, boolean r48) throws com.google.android.gms.internal.ads.zzlm {
        /*
            r0 = r43
            int r1 = com.google.android.gms.internal.ads.zzoj.zzakr
            com.google.android.gms.internal.ads.zzok r1 = r0.zzaz(r1)
            int r2 = com.google.android.gms.internal.ads.zzoj.zzala
            com.google.android.gms.internal.ads.zzol r2 = r1.zzay(r2)
            com.google.android.gms.internal.ads.zzst r2 = r2.zzbea
            r3 = 16
            r2.setPosition(r3)
            int r2 = r2.readInt()
            int r4 = com.google.android.gms.internal.ads.zzom.zzbec
            r5 = 4
            r6 = 3
            r8 = -1
            if (r2 != r4) goto L_0x0022
            r12 = 1
            goto L_0x0042
        L_0x0022:
            int r4 = com.google.android.gms.internal.ads.zzom.zzbeb
            if (r2 != r4) goto L_0x0028
            r12 = 2
            goto L_0x0042
        L_0x0028:
            int r4 = com.google.android.gms.internal.ads.zzom.zzbed
            if (r2 == r4) goto L_0x0041
            int r4 = com.google.android.gms.internal.ads.zzom.zzbee
            if (r2 == r4) goto L_0x0041
            int r4 = com.google.android.gms.internal.ads.zzom.zzbef
            if (r2 == r4) goto L_0x0041
            int r4 = com.google.android.gms.internal.ads.zzom.zzbeg
            if (r2 != r4) goto L_0x0039
            goto L_0x0041
        L_0x0039:
            int r4 = com.google.android.gms.internal.ads.zzom.zzbdl
            if (r2 != r4) goto L_0x003f
            r12 = 4
            goto L_0x0042
        L_0x003f:
            r12 = -1
            goto L_0x0042
        L_0x0041:
            r12 = 3
        L_0x0042:
            r2 = 0
            if (r12 != r8) goto L_0x0046
            return r2
        L_0x0046:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzaky
            com.google.android.gms.internal.ads.zzol r4 = r0.zzay(r4)
            com.google.android.gms.internal.ads.zzst r4 = r4.zzbea
            r10 = 8
            r4.setPosition(r10)
            int r11 = r4.readInt()
            int r11 = com.google.android.gms.internal.ads.zzoj.zzt(r11)
            if (r11 != 0) goto L_0x0060
            r13 = 8
            goto L_0x0062
        L_0x0060:
            r13 = 16
        L_0x0062:
            r4.zzac(r13)
            int r13 = r4.readInt()
            r4.zzac(r5)
            int r14 = r4.getPosition()
            if (r11 != 0) goto L_0x0074
            r15 = 4
            goto L_0x0076
        L_0x0074:
            r15 = 8
        L_0x0076:
            r9 = 0
        L_0x0077:
            if (r9 >= r15) goto L_0x0086
            byte[] r7 = r4.data
            int r16 = r14 + r9
            byte r7 = r7[r16]
            if (r7 == r8) goto L_0x0083
            r7 = 0
            goto L_0x0087
        L_0x0083:
            int r9 = r9 + 1
            goto L_0x0077
        L_0x0086:
            r7 = 1
        L_0x0087:
            r16 = 0
            r18 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r7 == 0) goto L_0x0096
            r4.zzac(r15)
        L_0x0093:
            r14 = r18
            goto L_0x00a6
        L_0x0096:
            if (r11 != 0) goto L_0x009d
            long r14 = r4.zzge()
            goto L_0x00a1
        L_0x009d:
            long r14 = r4.zzgh()
        L_0x00a1:
            int r7 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r7 != 0) goto L_0x00a6
            goto L_0x0093
        L_0x00a6:
            r4.zzac(r3)
            int r7 = r4.readInt()
            int r9 = r4.readInt()
            r4.zzac(r5)
            int r11 = r4.readInt()
            int r4 = r4.readInt()
            r5 = -65536(0xffffffffffff0000, float:NaN)
            if (r7 != 0) goto L_0x00cb
            r3 = 65536(0x10000, float:9.18355E-41)
            if (r9 != r3) goto L_0x00cb
            if (r11 != r5) goto L_0x00cb
            if (r4 != 0) goto L_0x00cb
            r7 = 90
            goto L_0x00e4
        L_0x00cb:
            if (r7 != 0) goto L_0x00d8
            if (r9 != r5) goto L_0x00d8
            r3 = 65536(0x10000, float:9.18355E-41)
            if (r11 != r3) goto L_0x00d8
            if (r4 != 0) goto L_0x00d8
            r7 = 270(0x10e, float:3.78E-43)
            goto L_0x00e4
        L_0x00d8:
            if (r7 != r5) goto L_0x00e3
            if (r9 != 0) goto L_0x00e3
            if (r11 != 0) goto L_0x00e3
            if (r4 != r5) goto L_0x00e3
            r7 = 180(0xb4, float:2.52E-43)
            goto L_0x00e4
        L_0x00e3:
            r7 = 0
        L_0x00e4:
            com.google.android.gms.internal.ads.zzos r3 = new com.google.android.gms.internal.ads.zzos
            r3.<init>(r13, r14, r7)
            long r22 = r3.zzct
            r4 = r44
            com.google.android.gms.internal.ads.zzst r4 = r4.zzbea
            r4.setPosition(r10)
            int r5 = r4.readInt()
            int r5 = com.google.android.gms.internal.ads.zzoj.zzt(r5)
            if (r5 != 0) goto L_0x0101
            r5 = 8
            goto L_0x0103
        L_0x0101:
            r5 = 16
        L_0x0103:
            r4.zzac(r5)
            long r4 = r4.zzge()
            int r7 = (r22 > r18 ? 1 : (r22 == r18 ? 0 : -1))
            if (r7 != 0) goto L_0x010f
            goto L_0x011a
        L_0x010f:
            r24 = 1000000(0xf4240, double:4.940656E-318)
            r26 = r4
            long r13 = com.google.android.gms.internal.ads.zzsy.zza(r22, r24, r26)
            r18 = r13
        L_0x011a:
            int r7 = com.google.android.gms.internal.ads.zzoj.zzaks
            com.google.android.gms.internal.ads.zzok r7 = r1.zzaz(r7)
            int r9 = com.google.android.gms.internal.ads.zzoj.zzakt
            com.google.android.gms.internal.ads.zzok r7 = r7.zzaz(r9)
            int r9 = com.google.android.gms.internal.ads.zzoj.zzakz
            com.google.android.gms.internal.ads.zzol r1 = r1.zzay(r9)
            com.google.android.gms.internal.ads.zzst r1 = r1.zzbea
            r1.setPosition(r10)
            int r9 = r1.readInt()
            int r9 = com.google.android.gms.internal.ads.zzoj.zzt(r9)
            if (r9 != 0) goto L_0x013e
            r11 = 8
            goto L_0x0140
        L_0x013e:
            r11 = 16
        L_0x0140:
            r1.zzac(r11)
            long r13 = r1.zzge()
            if (r9 != 0) goto L_0x014b
            r9 = 4
            goto L_0x014d
        L_0x014b:
            r9 = 8
        L_0x014d:
            r1.zzac(r9)
            int r1 = r1.readUnsignedShort()
            int r9 = r1 >> 10
            r9 = r9 & 31
            int r9 = r9 + 96
            char r9 = (char) r9
            int r11 = r1 >> 5
            r11 = r11 & 31
            int r11 = r11 + 96
            char r11 = (char) r11
            r1 = r1 & 31
            int r1 = r1 + 96
            char r1 = (char) r1
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>(r6)
            r15.append(r9)
            r15.append(r11)
            r15.append(r1)
            java.lang.String r1 = r15.toString()
            java.lang.Long r9 = java.lang.Long.valueOf(r13)
            android.util.Pair r1 = android.util.Pair.create(r9, r1)
            int r9 = com.google.android.gms.internal.ads.zzoj.zzalb
            com.google.android.gms.internal.ads.zzol r7 = r7.zzay(r9)
            com.google.android.gms.internal.ads.zzst r7 = r7.zzbea
            int r9 = r3.id
            int r11 = r3.zzatt
            java.lang.Object r13 = r1.second
            java.lang.String r13 = (java.lang.String) r13
            r14 = 12
            r7.setPosition(r14)
            int r14 = r7.readInt()
            com.google.android.gms.internal.ads.zzop r15 = new com.google.android.gms.internal.ads.zzop
            r15.<init>(r14)
            r6 = 0
        L_0x01a4:
            if (r6 >= r14) goto L_0x0678
            int r10 = r7.getPosition()
            int r2 = r7.readInt()
            if (r2 <= 0) goto L_0x01b4
            r44 = r14
            r8 = 1
            goto L_0x01b7
        L_0x01b4:
            r44 = r14
            r8 = 0
        L_0x01b7:
            java.lang.String r14 = "childAtomSize should be positive"
            com.google.android.gms.internal.ads.zzsk.checkArgument(r8, r14)
            int r8 = r7.readInt()
            r37 = r4
            int r4 = com.google.android.gms.internal.ads.zzoj.zzaka
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzakb
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzalh
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzalr
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcj
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbck
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcl
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdu
            if (r8 == r4) goto L_0x04c9
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdv
            if (r8 != r4) goto L_0x01e8
            goto L_0x04c9
        L_0x01e8:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzake
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzali
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzakf
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzakh
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcr
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcu
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcs
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbct
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdi
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdj
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcp
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcq
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbcn
            if (r8 == r4) goto L_0x02ba
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdy
            if (r8 != r4) goto L_0x0222
            goto L_0x02ba
        L_0x0222:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzalo
            if (r8 == r4) goto L_0x024b
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbde
            if (r8 == r4) goto L_0x024b
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdf
            if (r8 == r4) goto L_0x024b
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdg
            if (r8 == r4) goto L_0x024b
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdh
            if (r8 != r4) goto L_0x0237
            goto L_0x024b
        L_0x0237:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdx
            if (r8 != r4) goto L_0x02fa
            java.lang.String r4 = java.lang.Integer.toString(r9)
            java.lang.String r5 = "application/x-camera-motion"
            r8 = -1
            r14 = 0
            com.google.android.gms.internal.ads.zzlh r4 = com.google.android.gms.internal.ads.zzlh.zza(r4, r5, r14, r8, r14)
            r15.zzaue = r4
            goto L_0x02fa
        L_0x024b:
            int r4 = r10 + 8
            r5 = 8
            int r4 = r4 + r5
            r7.setPosition(r4)
            r22 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            int r4 = com.google.android.gms.internal.ads.zzoj.zzalo
            if (r8 != r4) goto L_0x0265
            java.lang.String r4 = "application/ttml+xml"
        L_0x025e:
            r30 = r22
            r32 = 0
            r23 = r4
            goto L_0x029d
        L_0x0265:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbde
            if (r8 != r4) goto L_0x027f
            int r4 = r2 + -8
            int r4 = r4 - r5
            byte[] r5 = new byte[r4]
            r8 = 0
            r7.zzb(r5, r8, r4)
            java.util.List r4 = java.util.Collections.singletonList(r5)
            java.lang.String r5 = "application/x-quicktime-tx3g"
            r32 = r4
            r30 = r22
            r23 = r5
            goto L_0x029d
        L_0x027f:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdf
            if (r8 != r4) goto L_0x0286
            java.lang.String r4 = "application/x-mp4-vtt"
            goto L_0x025e
        L_0x0286:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdg
            if (r8 != r4) goto L_0x0293
            java.lang.String r4 = "application/ttml+xml"
            r23 = r4
            r30 = r16
            r32 = 0
            goto L_0x029d
        L_0x0293:
            int r4 = com.google.android.gms.internal.ads.zzoj.zzbdh
            if (r8 != r4) goto L_0x02b4
            r4 = 1
            r15.zzbep = r4
            java.lang.String r4 = "application/x-mp4-cea-608"
            goto L_0x025e
        L_0x029d:
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = 0
            r28 = -1
            r29 = 0
            r27 = r13
            com.google.android.gms.internal.ads.zzlh r4 = com.google.android.gms.internal.ads.zzlh.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r32)
            r15.zzaue = r4
            goto L_0x02fa
        L_0x02b4:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x02ba:
            int r4 = r10 + 8
            r5 = 8
            int r4 = r4 + r5
            r7.setPosition(r4)
            if (r48 == 0) goto L_0x02cd
            int r4 = r7.readUnsignedShort()
            r5 = 6
            r7.zzac(r5)
            goto L_0x02d3
        L_0x02cd:
            r4 = 8
            r7.zzac(r4)
            r4 = 0
        L_0x02d3:
            if (r4 == 0) goto L_0x0305
            r5 = 1
            if (r4 != r5) goto L_0x02d9
            goto L_0x0305
        L_0x02d9:
            r5 = 2
            if (r4 != r5) goto L_0x02fa
            r4 = 16
            r7.zzac(r4)
            long r4 = r7.readLong()
            double r4 = java.lang.Double.longBitsToDouble(r4)
            long r4 = java.lang.Math.round(r4)
            int r5 = (int) r4
            int r4 = r7.zzgg()
            r22 = r4
            r4 = 20
            r7.zzac(r4)
            goto L_0x031f
        L_0x02fa:
            r40 = r1
            r41 = r3
            r42 = r11
            r39 = r12
        L_0x0302:
            r1 = 3
            goto L_0x065e
        L_0x0305:
            int r5 = r7.readUnsignedShort()
            r22 = r5
            r5 = 6
            r7.zzac(r5)
            int r5 = r7.zzgf()
            r23 = r5
            r5 = 1
            if (r4 != r5) goto L_0x031d
            r4 = 16
            r7.zzac(r4)
        L_0x031d:
            r5 = r23
        L_0x031f:
            int r4 = r7.getPosition()
            r23 = r5
            int r5 = com.google.android.gms.internal.ads.zzoj.zzali
            if (r8 != r5) goto L_0x0330
            int r8 = zza(r7, r10, r2, r15, r6)
            r7.setPosition(r4)
        L_0x0330:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzakf
            if (r8 != r5) goto L_0x0337
            java.lang.String r5 = "audio/ac3"
            goto L_0x0381
        L_0x0337:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzakh
            if (r8 != r5) goto L_0x033e
            java.lang.String r5 = "audio/eac3"
            goto L_0x0381
        L_0x033e:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbcr
            if (r8 != r5) goto L_0x0345
            java.lang.String r5 = "audio/vnd.dts"
            goto L_0x0381
        L_0x0345:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbcs
            if (r8 == r5) goto L_0x037f
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbct
            if (r8 != r5) goto L_0x034e
            goto L_0x037f
        L_0x034e:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbcu
            if (r8 != r5) goto L_0x0355
            java.lang.String r5 = "audio/vnd.dts.hd;profile=lbr"
            goto L_0x0381
        L_0x0355:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbdi
            if (r8 != r5) goto L_0x035c
            java.lang.String r5 = "audio/3gpp"
            goto L_0x0381
        L_0x035c:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbdj
            if (r8 != r5) goto L_0x0363
            java.lang.String r5 = "audio/amr-wb"
            goto L_0x0381
        L_0x0363:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbcp
            if (r8 == r5) goto L_0x037c
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbcq
            if (r8 != r5) goto L_0x036c
            goto L_0x037c
        L_0x036c:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbcn
            if (r8 != r5) goto L_0x0373
            java.lang.String r5 = "audio/mpeg"
            goto L_0x0381
        L_0x0373:
            int r5 = com.google.android.gms.internal.ads.zzoj.zzbdy
            if (r8 != r5) goto L_0x037a
            java.lang.String r5 = "audio/alac"
            goto L_0x0381
        L_0x037a:
            r5 = 0
            goto L_0x0381
        L_0x037c:
            java.lang.String r5 = "audio/raw"
            goto L_0x0381
        L_0x037f:
            java.lang.String r5 = "audio/vnd.dts.hd"
        L_0x0381:
            r8 = r4
            r39 = r12
            r4 = r22
            r33 = r23
            r34 = 0
        L_0x038a:
            int r12 = r8 - r10
            if (r12 >= r2) goto L_0x0486
            r7.setPosition(r8)
            int r12 = r7.readInt()
            if (r12 <= 0) goto L_0x039b
            r40 = r1
            r1 = 1
            goto L_0x039e
        L_0x039b:
            r40 = r1
            r1 = 0
        L_0x039e:
            com.google.android.gms.internal.ads.zzsk.checkArgument(r1, r14)
            int r1 = r7.readInt()
            r41 = r3
            int r3 = com.google.android.gms.internal.ads.zzoj.zzakc
            if (r1 == r3) goto L_0x0414
            if (r48 == 0) goto L_0x03b2
            int r3 = com.google.android.gms.internal.ads.zzoj.zzbco
            if (r1 != r3) goto L_0x03b2
            goto L_0x0414
        L_0x03b2:
            int r3 = com.google.android.gms.internal.ads.zzoj.zzakg
            if (r1 != r3) goto L_0x03c8
            int r1 = r8 + 8
            r7.setPosition(r1)
            java.lang.String r1 = java.lang.Integer.toString(r9)
            r3 = 0
            com.google.android.gms.internal.ads.zzlh r1 = com.google.android.gms.internal.ads.zzlv.zza(r7, r1, r13, r3)
            r15.zzaue = r1
        L_0x03c6:
            r3 = 0
            goto L_0x040f
        L_0x03c8:
            int r3 = com.google.android.gms.internal.ads.zzoj.zzaki
            if (r1 != r3) goto L_0x03dd
            int r1 = r8 + 8
            r7.setPosition(r1)
            java.lang.String r1 = java.lang.Integer.toString(r9)
            r3 = 0
            com.google.android.gms.internal.ads.zzlh r1 = com.google.android.gms.internal.ads.zzlv.zzb(r7, r1, r13, r3)
            r15.zzaue = r1
            goto L_0x03c6
        L_0x03dd:
            int r3 = com.google.android.gms.internal.ads.zzoj.zzbcv
            if (r1 != r3) goto L_0x0400
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = -1
            r29 = 0
            r30 = 0
            r31 = 0
            r23 = r5
            r27 = r4
            r28 = r33
            r32 = r13
            com.google.android.gms.internal.ads.zzlh r1 = com.google.android.gms.internal.ads.zzlh.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)
            r15.zzaue = r1
            goto L_0x03c6
        L_0x0400:
            int r3 = com.google.android.gms.internal.ads.zzoj.zzbdy
            if (r1 != r3) goto L_0x03c6
            byte[] r1 = new byte[r12]
            r7.setPosition(r8)
            r3 = 0
            r7.zzb(r1, r3, r12)
            r34 = r1
        L_0x040f:
            r42 = r11
            r0 = -1
            goto L_0x047b
        L_0x0414:
            int r3 = com.google.android.gms.internal.ads.zzoj.zzakc
            if (r1 != r3) goto L_0x041d
            r1 = r8
            r42 = r11
        L_0x041b:
            r0 = -1
            goto L_0x0449
        L_0x041d:
            int r1 = r7.getPosition()
        L_0x0421:
            int r3 = r1 - r8
            if (r3 >= r12) goto L_0x0445
            r7.setPosition(r1)
            int r3 = r7.readInt()
            if (r3 <= 0) goto L_0x0430
            r0 = 1
            goto L_0x0431
        L_0x0430:
            r0 = 0
        L_0x0431:
            com.google.android.gms.internal.ads.zzsk.checkArgument(r0, r14)
            int r0 = r7.readInt()
            r42 = r11
            int r11 = com.google.android.gms.internal.ads.zzoj.zzakc
            if (r0 != r11) goto L_0x043f
            goto L_0x041b
        L_0x043f:
            int r1 = r1 + r3
            r0 = r43
            r11 = r42
            goto L_0x0421
        L_0x0445:
            r42 = r11
            r0 = -1
            r1 = -1
        L_0x0449:
            if (r1 == r0) goto L_0x047b
            android.util.Pair r1 = zzb(r7, r1)
            java.lang.Object r3 = r1.first
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r1 = r1.second
            r34 = r1
            byte[] r34 = (byte[]) r34
            java.lang.String r1 = "audio/mp4a-latm"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x047a
            android.util.Pair r1 = com.google.android.gms.internal.ads.zzsl.zzf(r34)
            java.lang.Object r4 = r1.first
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            java.lang.Object r1 = r1.second
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r1 = r1.intValue()
            r5 = r3
            r33 = r4
            r4 = r1
            goto L_0x047b
        L_0x047a:
            r5 = r3
        L_0x047b:
            int r8 = r8 + r12
            r0 = r43
            r1 = r40
            r3 = r41
            r11 = r42
            goto L_0x038a
        L_0x0486:
            r40 = r1
            r41 = r3
            r42 = r11
            r0 = -1
            com.google.android.gms.internal.ads.zzlh r1 = r15.zzaue
            if (r1 != 0) goto L_0x0302
            if (r5 == 0) goto L_0x0302
            java.lang.String r1 = "audio/raw"
            boolean r1 = r1.equals(r5)
            if (r1 == 0) goto L_0x049e
            r29 = 2
            goto L_0x04a0
        L_0x049e:
            r29 = -1
        L_0x04a0:
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = -1
            if (r34 != 0) goto L_0x04af
            r30 = 0
            goto L_0x04b5
        L_0x04af:
            java.util.List r1 = java.util.Collections.singletonList(r34)
            r30 = r1
        L_0x04b5:
            r31 = 0
            r32 = 0
            r23 = r5
            r27 = r4
            r28 = r33
            r33 = r13
            com.google.android.gms.internal.ads.zzlh r1 = com.google.android.gms.internal.ads.zzlh.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33)
            r15.zzaue = r1
            goto L_0x0302
        L_0x04c9:
            r40 = r1
            r41 = r3
            r42 = r11
            r39 = r12
            r0 = -1
            int r1 = r10 + 8
            r3 = 8
            int r1 = r1 + r3
            r7.setPosition(r1)
            r1 = 16
            r7.zzac(r1)
            int r27 = r7.readUnsignedShort()
            int r28 = r7.readUnsignedShort()
            r3 = 1065353216(0x3f800000, float:1.0)
            r4 = 50
            r7.zzac(r4)
            int r4 = r7.getPosition()
            int r5 = com.google.android.gms.internal.ads.zzoj.zzalh
            if (r8 != r5) goto L_0x04fd
            int r8 = zza(r7, r10, r2, r15, r6)
            r7.setPosition(r4)
        L_0x04fd:
            r3 = 0
            r23 = 0
            r30 = 0
            r32 = 1065353216(0x3f800000, float:1.0)
            r33 = 0
            r34 = -1
        L_0x0508:
            int r5 = r4 - r10
            if (r5 >= r2) goto L_0x0643
            r7.setPosition(r4)
            int r5 = r7.getPosition()
            int r11 = r7.readInt()
            if (r11 != 0) goto L_0x0520
            int r12 = r7.getPosition()
            int r12 = r12 - r10
            if (r12 == r2) goto L_0x0643
        L_0x0520:
            if (r11 <= 0) goto L_0x0524
            r12 = 1
            goto L_0x0525
        L_0x0524:
            r12 = 0
        L_0x0525:
            com.google.android.gms.internal.ads.zzsk.checkArgument(r12, r14)
            int r12 = r7.readInt()
            int r0 = com.google.android.gms.internal.ads.zzoj.zzaku
            if (r12 != r0) goto L_0x0550
            if (r23 != 0) goto L_0x0534
            r0 = 1
            goto L_0x0535
        L_0x0534:
            r0 = 0
        L_0x0535:
            com.google.android.gms.internal.ads.zzsk.checkState(r0)
            int r5 = r5 + 8
            r7.setPosition(r5)
            com.google.android.gms.internal.ads.zzta r0 = com.google.android.gms.internal.ads.zzta.zzf(r7)
            java.util.List<byte[]> r5 = r0.zzafw
            int r12 = r0.zzamf
            r15.zzamf = r12
            if (r3 != 0) goto L_0x054d
            float r0 = r0.zzbne
            r32 = r0
        L_0x054d:
            java.lang.String r0 = "video/avc"
            goto L_0x056d
        L_0x0550:
            int r0 = com.google.android.gms.internal.ads.zzoj.zzbcw
            if (r12 != r0) goto L_0x0574
            if (r23 != 0) goto L_0x0558
            r0 = 1
            goto L_0x0559
        L_0x0558:
            r0 = 0
        L_0x0559:
            com.google.android.gms.internal.ads.zzsk.checkState(r0)
            int r5 = r5 + 8
            r7.setPosition(r5)
            com.google.android.gms.internal.ads.zztg r0 = com.google.android.gms.internal.ads.zztg.zzh(r7)
            java.util.List<byte[]> r5 = r0.zzafw
            int r0 = r0.zzamf
            r15.zzamf = r0
            java.lang.String r0 = "video/hevc"
        L_0x056d:
            r23 = r0
            r30 = r5
        L_0x0571:
            r1 = 3
            goto L_0x063d
        L_0x0574:
            int r0 = com.google.android.gms.internal.ads.zzoj.zzbdw
            if (r12 != r0) goto L_0x058a
            if (r23 != 0) goto L_0x057c
            r0 = 1
            goto L_0x057d
        L_0x057c:
            r0 = 0
        L_0x057d:
            com.google.android.gms.internal.ads.zzsk.checkState(r0)
            int r0 = com.google.android.gms.internal.ads.zzoj.zzbdu
            if (r8 != r0) goto L_0x0587
            java.lang.String r0 = "video/x-vnd.on2.vp8"
            goto L_0x0598
        L_0x0587:
            java.lang.String r0 = "video/x-vnd.on2.vp9"
            goto L_0x0598
        L_0x058a:
            int r0 = com.google.android.gms.internal.ads.zzoj.zzbcm
            if (r12 != r0) goto L_0x059b
            if (r23 != 0) goto L_0x0592
            r0 = 1
            goto L_0x0593
        L_0x0592:
            r0 = 0
        L_0x0593:
            com.google.android.gms.internal.ads.zzsk.checkState(r0)
            java.lang.String r0 = "video/3gpp"
        L_0x0598:
            r23 = r0
            goto L_0x0571
        L_0x059b:
            int r0 = com.google.android.gms.internal.ads.zzoj.zzakc
            if (r12 != r0) goto L_0x05bc
            if (r23 != 0) goto L_0x05a3
            r0 = 1
            goto L_0x05a4
        L_0x05a3:
            r0 = 0
        L_0x05a4:
            com.google.android.gms.internal.ads.zzsk.checkState(r0)
            android.util.Pair r0 = zzb(r7, r5)
            java.lang.Object r5 = r0.first
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r0 = r0.second
            byte[] r0 = (byte[]) r0
            java.util.List r0 = java.util.Collections.singletonList(r0)
            r30 = r0
            r23 = r5
            goto L_0x0571
        L_0x05bc:
            int r0 = com.google.android.gms.internal.ads.zzoj.zzaln
            if (r12 != r0) goto L_0x05d5
            int r5 = r5 + 8
            r7.setPosition(r5)
            int r0 = r7.zzgg()
            int r3 = r7.zzgg()
            float r0 = (float) r0
            float r3 = (float) r3
            float r32 = r0 / r3
            r1 = 3
            r3 = 1
            goto L_0x063d
        L_0x05d5:
            int r0 = com.google.android.gms.internal.ads.zzoj.zzbds
            if (r12 != r0) goto L_0x0607
            int r0 = r5 + 8
        L_0x05db:
            int r12 = r0 - r5
            if (r12 >= r11) goto L_0x05fe
            r7.setPosition(r0)
            int r12 = r7.readInt()
            int r1 = r7.readInt()
            r22 = r3
            int r3 = com.google.android.gms.internal.ads.zzoj.zzbdt
            if (r1 != r3) goto L_0x05f8
            byte[] r1 = r7.data
            int r12 = r12 + r0
            byte[] r0 = java.util.Arrays.copyOfRange(r1, r0, r12)
            goto L_0x0601
        L_0x05f8:
            int r0 = r0 + r12
            r3 = r22
            r1 = 16
            goto L_0x05db
        L_0x05fe:
            r22 = r3
            r0 = 0
        L_0x0601:
            r33 = r0
            r3 = r22
            goto L_0x0571
        L_0x0607:
            r22 = r3
            int r0 = com.google.android.gms.internal.ads.zzoj.zzbdr
            if (r12 != r0) goto L_0x063a
            int r0 = r7.readUnsignedByte()
            r1 = 3
            r7.zzac(r1)
            if (r0 != 0) goto L_0x063b
            int r0 = r7.readUnsignedByte()
            if (r0 == 0) goto L_0x0635
            r3 = 1
            if (r0 == r3) goto L_0x0630
            r3 = 2
            if (r0 == r3) goto L_0x062b
            if (r0 == r1) goto L_0x0626
            goto L_0x063b
        L_0x0626:
            r3 = r22
            r34 = 3
            goto L_0x063d
        L_0x062b:
            r3 = r22
            r34 = 2
            goto L_0x063d
        L_0x0630:
            r3 = r22
            r34 = 1
            goto L_0x063d
        L_0x0635:
            r3 = r22
            r34 = 0
            goto L_0x063d
        L_0x063a:
            r1 = 3
        L_0x063b:
            r3 = r22
        L_0x063d:
            int r4 = r4 + r11
            r0 = -1
            r1 = 16
            goto L_0x0508
        L_0x0643:
            r1 = 3
            if (r23 == 0) goto L_0x065e
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = -1
            r29 = -1082130432(0xffffffffbf800000, float:-1.0)
            r35 = 0
            r36 = 0
            r31 = r42
            com.google.android.gms.internal.ads.zzlh r0 = com.google.android.gms.internal.ads.zzlh.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36)
            r15.zzaue = r0
        L_0x065e:
            int r10 = r10 + r2
            r7.setPosition(r10)
            int r6 = r6 + 1
            r0 = r43
            r14 = r44
            r4 = r37
            r12 = r39
            r1 = r40
            r3 = r41
            r11 = r42
            r2 = 0
            r8 = -1
            r10 = 8
            goto L_0x01a4
        L_0x0678:
            r40 = r1
            r41 = r3
            r37 = r4
            r39 = r12
            int r0 = com.google.android.gms.internal.ads.zzoj.zzbcy
            r1 = r43
            com.google.android.gms.internal.ads.zzok r0 = r1.zzaz(r0)
            if (r0 == 0) goto L_0x06e5
            int r1 = com.google.android.gms.internal.ads.zzoj.zzbcz
            com.google.android.gms.internal.ads.zzol r0 = r0.zzay(r1)
            if (r0 != 0) goto L_0x0693
            goto L_0x06e5
        L_0x0693:
            com.google.android.gms.internal.ads.zzst r0 = r0.zzbea
            r1 = 8
            r0.setPosition(r1)
            int r1 = r0.readInt()
            int r1 = com.google.android.gms.internal.ads.zzoj.zzt(r1)
            int r2 = r0.zzgg()
            long[] r3 = new long[r2]
            long[] r4 = new long[r2]
            r5 = 0
        L_0x06ab:
            if (r5 >= r2) goto L_0x06de
            r6 = 1
            if (r1 != r6) goto L_0x06b5
            long r7 = r0.zzgh()
            goto L_0x06b9
        L_0x06b5:
            long r7 = r0.zzge()
        L_0x06b9:
            r3[r5] = r7
            if (r1 != r6) goto L_0x06c2
            long r7 = r0.readLong()
            goto L_0x06c7
        L_0x06c2:
            int r7 = r0.readInt()
            long r7 = (long) r7
        L_0x06c7:
            r4[r5] = r7
            short r7 = r0.readShort()
            if (r7 != r6) goto L_0x06d6
            r7 = 2
            r0.zzac(r7)
            int r5 = r5 + 1
            goto L_0x06ab
        L_0x06d6:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Unsupported media rate."
            r0.<init>(r1)
            throw r0
        L_0x06de:
            android.util.Pair r0 = android.util.Pair.create(r3, r4)
            r1 = r0
            r0 = 0
            goto L_0x06ea
        L_0x06e5:
            r0 = 0
            android.util.Pair r1 = android.util.Pair.create(r0, r0)
        L_0x06ea:
            com.google.android.gms.internal.ads.zzlh r2 = r15.zzaue
            if (r2 != 0) goto L_0x06ef
            return r0
        L_0x06ef:
            com.google.android.gms.internal.ads.zzpa r0 = new com.google.android.gms.internal.ads.zzpa
            int r11 = r41.id
            r2 = r40
            java.lang.Object r2 = r2.first
            java.lang.Long r2 = (java.lang.Long) r2
            long r13 = r2.longValue()
            com.google.android.gms.internal.ads.zzlh r2 = r15.zzaue
            int r3 = r15.zzbep
            com.google.android.gms.internal.ads.zzpb[] r4 = r15.zzbeo
            int r5 = r15.zzamf
            java.lang.Object r6 = r1.first
            r23 = r6
            long[] r23 = (long[]) r23
            java.lang.Object r1 = r1.second
            r24 = r1
            long[] r24 = (long[]) r24
            r10 = r0
            r12 = r39
            r15 = r37
            r17 = r18
            r19 = r2
            r20 = r3
            r21 = r4
            r22 = r5
            r10.<init>(r11, r12, r13, r15, r17, r19, r20, r21, r22, r23, r24)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzom.zza(com.google.android.gms.internal.ads.zzok, com.google.android.gms.internal.ads.zzol, long, com.google.android.gms.internal.ads.zzne, boolean):com.google.android.gms.internal.ads.zzpa");
    }

    public static zzpc zza(zzpa zzpa, zzok zzok, zznr zznr) throws zzlm {
        zzoo zzoo;
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        int[] iArr;
        long[] jArr;
        int[] iArr2;
        long[] jArr2;
        long j;
        boolean z2;
        int[] iArr3;
        int i5;
        long[] jArr3;
        int[] iArr4;
        int[] iArr5;
        int[] iArr6;
        int i6;
        int i7;
        zzpa zzpa2 = zzpa;
        zzok zzok2 = zzok;
        zzol zzay = zzok2.zzay(zzoj.zzalw);
        if (zzay != null) {
            zzoo = new zzoq(zzay);
        } else {
            zzol zzay2 = zzok2.zzay(zzoj.zzbdd);
            if (zzay2 != null) {
                zzoo = new zzor(zzay2);
            } else {
                throw new zzlm("Track has no sample table size information");
            }
        }
        int zzim = zzoo.zzim();
        if (zzim == 0) {
            return new zzpc(new long[0], new int[0], 0, new long[0], new int[0]);
        }
        zzol zzay3 = zzok2.zzay(zzoj.zzalx);
        if (zzay3 == null) {
            zzay3 = zzok2.zzay(zzoj.zzaly);
            z = true;
        } else {
            z = false;
        }
        zzst zzst = zzay3.zzbea;
        zzst zzst2 = zzok2.zzay(zzoj.zzalv).zzbea;
        zzst zzst3 = zzok2.zzay(zzoj.zzals).zzbea;
        zzol zzay4 = zzok2.zzay(zzoj.zzalt);
        zzst zzst4 = zzay4 != null ? zzay4.zzbea : null;
        zzol zzay5 = zzok2.zzay(zzoj.zzalu);
        zzst zzst5 = zzay5 != null ? zzay5.zzbea : null;
        zzon zzon = new zzon(zzst2, zzst, z);
        zzst3.setPosition(12);
        int zzgg = zzst3.zzgg() - 1;
        int zzgg2 = zzst3.zzgg();
        int zzgg3 = zzst3.zzgg();
        if (zzst5 != null) {
            zzst5.setPosition(12);
            i = zzst5.zzgg();
        } else {
            i = 0;
        }
        int i8 = -1;
        if (zzst4 != null) {
            zzst4.setPosition(12);
            i2 = zzst4.zzgg();
            if (i2 > 0) {
                i8 = zzst4.zzgg() - 1;
            } else {
                zzst4 = null;
            }
        } else {
            i2 = 0;
        }
        long j2 = 0;
        if (!(zzoo.zzio() && "audio/raw".equals(zzpa2.zzaue.zzatq) && zzgg == 0 && i == 0 && i2 == 0)) {
            jArr2 = new long[zzim];
            iArr = new int[zzim];
            jArr = new long[zzim];
            int i9 = i2;
            iArr2 = new int[zzim];
            int i10 = i9;
            zzst zzst6 = zzst3;
            int i11 = zzgg3;
            int i12 = i;
            int i13 = i8;
            long j3 = 0;
            long j4 = 0;
            int i14 = 0;
            int i15 = 0;
            int i16 = 0;
            int i17 = 0;
            int i18 = zzgg2;
            int i19 = zzgg;
            int i20 = 0;
            while (i20 < zzim) {
                long j5 = j3;
                int i21 = i14;
                while (i21 == 0) {
                    zzsk.checkState(zzon.zzil());
                    j5 = zzon.zzajx;
                    i21 = zzon.zzbei;
                    i19 = i19;
                    i11 = i11;
                }
                int i22 = i19;
                int i23 = i11;
                if (zzst5 != null) {
                    while (i17 == 0 && i12 > 0) {
                        i17 = zzst5.zzgg();
                        i16 = zzst5.readInt();
                        i12--;
                    }
                    i17--;
                }
                int i24 = i16;
                jArr2[i20] = j5;
                iArr[i20] = zzoo.zzin();
                if (iArr[i20] > i15) {
                    i6 = zzim;
                    i15 = iArr[i20];
                } else {
                    i6 = zzim;
                }
                zzoo zzoo2 = zzoo;
                jArr[i20] = j4 + ((long) i24);
                iArr2[i20] = zzst4 == null ? 1 : 0;
                if (i20 == i13) {
                    iArr2[i20] = 1;
                    i10--;
                    if (i10 > 0) {
                        i13 = zzst4.zzgg() - 1;
                    }
                }
                int i25 = i10;
                int i26 = i13;
                int i27 = i23;
                j4 += (long) i27;
                i18--;
                if (i18 != 0 || i22 <= 0) {
                    i7 = i22;
                } else {
                    i7 = i22 - 1;
                    i18 = zzst6.zzgg();
                    i27 = zzst6.zzgg();
                }
                int i28 = i7;
                i20++;
                i13 = i26;
                zzim = i6;
                i14 = i21 - 1;
                i16 = i24;
                i19 = i28;
                j3 = j5 + ((long) iArr[i20]);
                zzoo zzoo3 = zzoo2;
                i11 = i27;
                i10 = i25;
                zzoo = zzoo3;
            }
            i4 = zzim;
            int i29 = i19;
            zzsk.checkArgument(i17 == 0);
            while (i12 > 0) {
                zzsk.checkArgument(zzst5.zzgg() == 0);
                zzst5.readInt();
                i12--;
            }
            if (i10 == 0 && i18 == 0 && i14 == 0 && i29 == 0) {
                zzpa2 = zzpa;
            } else {
                int i30 = i10;
                zzpa2 = zzpa;
                int i31 = zzpa2.id;
                StringBuilder sb = new StringBuilder(215);
                sb.append("Inconsistent stbl box for track ");
                sb.append(i31);
                sb.append(": remainingSynchronizationSamples ");
                sb.append(i30);
                sb.append(", remainingSamplesAtTimestampDelta ");
                sb.append(i18);
                sb.append(", remainingSamplesInChunk ");
                sb.append(i14);
                sb.append(", remainingTimestampDeltaChanges ");
                sb.append(i29);
                Log.w("AtomParsers", sb.toString());
            }
            j = j4;
            i3 = i15;
        } else {
            i4 = zzim;
            zzoo zzoo4 = zzoo;
            long[] jArr4 = new long[zzon.length];
            int[] iArr7 = new int[zzon.length];
            while (zzon.zzil()) {
                jArr4[zzon.index] = zzon.zzajx;
                iArr7[zzon.index] = zzon.zzbei;
            }
            int zzin = zzoo4.zzin();
            long j6 = (long) zzgg3;
            int i32 = 8192 / zzin;
            int i33 = 0;
            for (int zzb : iArr7) {
                i33 += zzsy.zzb(zzb, i32);
            }
            long[] jArr5 = new long[i33];
            int[] iArr8 = new int[i33];
            long[] jArr6 = new long[i33];
            int[] iArr9 = new int[i33];
            int i34 = 0;
            int i35 = 0;
            int i36 = 0;
            int i37 = 0;
            while (i34 < iArr7.length) {
                int i38 = iArr7[i34];
                long j7 = jArr4[i34];
                int i39 = i35;
                int i40 = i37;
                while (i38 > 0) {
                    int min = Math.min(i32, i38);
                    jArr5[i36] = j7;
                    iArr8[i36] = zzin * min;
                    i40 = Math.max(i40, iArr8[i36]);
                    jArr6[i36] = ((long) i39) * j6;
                    iArr9[i36] = 1;
                    j7 += (long) iArr8[i36];
                    i39 += min;
                    i38 -= min;
                    i36++;
                    jArr4 = jArr4;
                    iArr7 = iArr7;
                }
                i34++;
                i37 = i40;
                i35 = i39;
            }
            zzou zzou = new zzou(jArr5, iArr8, i37, jArr6, iArr9);
            jArr2 = zzou.zzahq;
            iArr = zzou.zzahp;
            int i41 = zzou.zzbet;
            jArr = zzou.zzbeu;
            iArr2 = zzou.zzajr;
            i3 = i41;
            j = 0;
        }
        if (zzpa2.zzbgm == null || zznr.zzii()) {
            zzsy.zza(jArr, 1000000, zzpa2.zzcs);
            return new zzpc(jArr2, iArr, i3, jArr, iArr2);
        }
        if (zzpa2.zzbgm.length == 1 && zzpa2.type == 1 && jArr.length >= 2) {
            long j8 = zzpa2.zzbgn[0];
            long zza = zzsy.zza(zzpa2.zzbgm[0], zzpa2.zzcs, zzpa2.zzbgj) + j8;
            if (jArr[0] <= j8 && j8 < jArr[1] && jArr[jArr.length - 1] < zza && zza <= j) {
                long j9 = j - zza;
                long zza2 = zzsy.zza(j8 - jArr[0], (long) zzpa2.zzaue.zzafv, zzpa2.zzcs);
                long zza3 = zzsy.zza(j9, (long) zzpa2.zzaue.zzafv, zzpa2.zzcs);
                if (!(zza2 == 0 && zza3 == 0) && zza2 <= 2147483647L && zza3 <= 2147483647L) {
                    int i42 = (int) zza2;
                    zznr zznr2 = zznr;
                    zznr2.zzaty = i42;
                    zznr2.zzatz = (int) zza3;
                    zzsy.zza(jArr, 1000000, zzpa2.zzcs);
                    return new zzpc(jArr2, iArr, i3, jArr, iArr2);
                }
            }
        }
        if (zzpa2.zzbgm.length == 1) {
            char c = 0;
            if (zzpa2.zzbgm[0] == 0) {
                int i43 = 0;
                while (i43 < jArr.length) {
                    jArr[i43] = zzsy.zza(jArr[i43] - zzpa2.zzbgn[c], 1000000, zzpa2.zzcs);
                    i43++;
                    c = 0;
                }
                return new zzpc(jArr2, iArr, i3, jArr, iArr2);
            }
        }
        boolean z3 = zzpa2.type == 1;
        int i44 = 0;
        boolean z4 = false;
        int i45 = 0;
        int i46 = 0;
        while (i44 < zzpa2.zzbgm.length) {
            long j10 = zzpa2.zzbgn[i44];
            if (j10 != -1) {
                iArr6 = iArr;
                long zza4 = zzsy.zza(zzpa2.zzbgm[i44], zzpa2.zzcs, zzpa2.zzbgj);
                int zzb2 = zzsy.zzb(jArr, j10, true, true);
                int zzb3 = zzsy.zzb(jArr, j10 + zza4, z3, false);
                i45 += zzb3 - zzb2;
                z4 |= i46 != zzb2;
                i46 = zzb3;
            } else {
                iArr6 = iArr;
            }
            i44++;
            iArr = iArr6;
        }
        int[] iArr10 = iArr;
        boolean z5 = (i45 != i4) | z4;
        long[] jArr7 = z5 ? new long[i45] : jArr2;
        int[] iArr11 = z5 ? new int[i45] : iArr10;
        if (z5) {
            i3 = 0;
        }
        int[] iArr12 = z5 ? new int[i45] : iArr2;
        long[] jArr8 = new long[i45];
        int i47 = i3;
        int i48 = 0;
        int i49 = 0;
        while (i48 < zzpa2.zzbgm.length) {
            long j11 = zzpa2.zzbgn[i48];
            long j12 = zzpa2.zzbgm[i48];
            if (j11 != -1) {
                long j13 = zzpa2.zzcs;
                int[] iArr13 = iArr12;
                i5 = i48;
                int zzb4 = zzsy.zzb(jArr, j11, true, true);
                int zzb5 = zzsy.zzb(jArr, zzsy.zza(j12, j13, zzpa2.zzbgj) + j11, z3, false);
                if (z5) {
                    int i50 = zzb5 - zzb4;
                    System.arraycopy(jArr2, zzb4, jArr7, i49, i50);
                    iArr4 = iArr10;
                    System.arraycopy(iArr4, zzb4, iArr11, i49, i50);
                    z2 = z3;
                    iArr5 = iArr13;
                    System.arraycopy(iArr2, zzb4, iArr5, i49, i50);
                } else {
                    iArr4 = iArr10;
                    z2 = z3;
                    iArr5 = iArr13;
                }
                int i51 = i47;
                while (zzb4 < zzb5) {
                    long[] jArr9 = jArr2;
                    int[] iArr14 = iArr2;
                    long j14 = j11;
                    jArr8[i49] = zzsy.zza(j2, 1000000, zzpa2.zzbgj) + zzsy.zza(jArr[zzb4] - j11, 1000000, zzpa2.zzcs);
                    if (z5 && iArr11[i49] > i51) {
                        i51 = iArr4[zzb4];
                    }
                    i49++;
                    zzb4++;
                    jArr2 = jArr9;
                    j11 = j14;
                    iArr2 = iArr14;
                }
                jArr3 = jArr2;
                iArr3 = iArr2;
                i47 = i51;
            } else {
                iArr4 = iArr10;
                z2 = z3;
                jArr3 = jArr2;
                iArr3 = iArr2;
                iArr5 = iArr12;
                i5 = i48;
            }
            j2 += j12;
            i48 = i5 + 1;
            iArr12 = iArr5;
            jArr2 = jArr3;
            iArr2 = iArr3;
            z3 = z2;
            iArr10 = iArr4;
        }
        int[] iArr15 = iArr12;
        boolean z6 = false;
        for (int i52 = 0; i52 < iArr15.length && !z6; i52++) {
            z6 |= (iArr15[i52] & 1) != 0;
        }
        if (z6) {
            return new zzpc(jArr7, iArr11, i47, jArr8, iArr15);
        }
        throw new zzlm("The edited sample sequence does not contain a sync sample.");
    }

    public static zzpo zza(zzol zzol, boolean z) {
        if (z) {
            return null;
        }
        zzst zzst = zzol.zzbea;
        zzst.setPosition(8);
        while (zzst.zzjz() >= 8) {
            int position = zzst.getPosition();
            int readInt = zzst.readInt();
            if (zzst.readInt() == zzoj.zzbdl) {
                zzst.setPosition(position);
                int i = position + readInt;
                zzst.zzac(12);
                while (true) {
                    if (zzst.getPosition() >= i) {
                        break;
                    }
                    int position2 = zzst.getPosition();
                    int readInt2 = zzst.readInt();
                    if (zzst.readInt() == zzoj.zzbdm) {
                        zzst.setPosition(position2);
                        int i2 = position2 + readInt2;
                        zzst.zzac(8);
                        ArrayList arrayList = new ArrayList();
                        while (zzst.getPosition() < i2) {
                            zzpo.zza zzd = zzov.zzd(zzst);
                            if (zzd != null) {
                                arrayList.add(zzd);
                            }
                        }
                        if (!arrayList.isEmpty()) {
                            return new zzpo(arrayList);
                        }
                    } else {
                        zzst.zzac(readInt2 - 8);
                    }
                }
                return null;
            }
            zzst.zzac(readInt - 8);
        }
        return null;
    }

    private static Pair<String, byte[]> zzb(zzst zzst, int i) {
        zzst.setPosition(i + 8 + 4);
        zzst.zzac(1);
        zzc(zzst);
        zzst.zzac(2);
        int readUnsignedByte = zzst.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            zzst.zzac(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            zzst.zzac(zzst.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            zzst.zzac(2);
        }
        zzst.zzac(1);
        zzc(zzst);
        int readUnsignedByte2 = zzst.readUnsignedByte();
        String str = null;
        if (readUnsignedByte2 == 32) {
            str = "video/mp4v-es";
        } else if (readUnsignedByte2 == 33) {
            str = "video/avc";
        } else if (readUnsignedByte2 != 35) {
            if (readUnsignedByte2 != 64) {
                if (readUnsignedByte2 == 107) {
                    return Pair.create("audio/mpeg", null);
                }
                if (readUnsignedByte2 == 165) {
                    str = "audio/ac3";
                } else if (readUnsignedByte2 != 166) {
                    switch (readUnsignedByte2) {
                        case 102:
                        case 103:
                        case 104:
                            break;
                        default:
                            switch (readUnsignedByte2) {
                                case 169:
                                case 172:
                                    return Pair.create("audio/vnd.dts", null);
                                case 170:
                                case 171:
                                    return Pair.create("audio/vnd.dts.hd", null);
                            }
                    }
                } else {
                    str = "audio/eac3";
                }
            }
            str = "audio/mp4a-latm";
        } else {
            str = "video/hevc";
        }
        zzst.zzac(12);
        zzst.zzac(1);
        int zzc = zzc(zzst);
        byte[] bArr = new byte[zzc];
        zzst.zzb(bArr, 0, zzc);
        return Pair.create(str, bArr);
    }

    private static int zza(zzst zzst, int i, int i2, zzop zzop, int i3) {
        zzpb zzpb;
        int position = zzst.getPosition();
        while (true) {
            boolean z = false;
            if (position - i >= i2) {
                return 0;
            }
            zzst.setPosition(position);
            int readInt = zzst.readInt();
            zzsk.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (zzst.readInt() == zzoj.zzald) {
                int i4 = position + 8;
                Pair pair = null;
                Integer num = null;
                zzpb zzpb2 = null;
                boolean z2 = false;
                while (i4 - position < readInt) {
                    zzst.setPosition(i4);
                    int readInt2 = zzst.readInt();
                    int readInt3 = zzst.readInt();
                    if (readInt3 == zzoj.zzalj) {
                        num = Integer.valueOf(zzst.readInt());
                    } else if (readInt3 == zzoj.zzale) {
                        zzst.zzac(4);
                        z2 = zzst.readInt() == zzbeh;
                    } else if (readInt3 == zzoj.zzalf) {
                        int i5 = i4 + 8;
                        while (true) {
                            if (i5 - i4 >= readInt2) {
                                zzpb = null;
                                break;
                            }
                            zzst.setPosition(i5);
                            int readInt4 = zzst.readInt();
                            if (zzst.readInt() == zzoj.zzalg) {
                                zzst.zzac(6);
                                boolean z3 = zzst.readUnsignedByte() == 1;
                                int readUnsignedByte = zzst.readUnsignedByte();
                                byte[] bArr = new byte[16];
                                zzst.zzb(bArr, 0, 16);
                                zzpb = new zzpb(z3, readUnsignedByte, bArr);
                            } else {
                                i5 += readInt4;
                            }
                        }
                        zzpb2 = zzpb;
                    }
                    i4 += readInt2;
                }
                if (z2) {
                    zzsk.checkArgument(num != null, "frma atom is mandatory");
                    if (zzpb2 != null) {
                        z = true;
                    }
                    zzsk.checkArgument(z, "schi->tenc atom is mandatory");
                    pair = Pair.create(num, zzpb2);
                }
                if (pair != null) {
                    zzop.zzbeo[i3] = (zzpb) pair.second;
                    return ((Integer) pair.first).intValue();
                }
            }
            position += readInt;
        }
    }

    private static int zzc(zzst zzst) {
        int readUnsignedByte = zzst.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = zzst.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }
}
