package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.Leaderboards;

abstract class zzau extends Games.zza<Leaderboards.LeaderboardMetadataResult> {
    private zzau(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzau(GoogleApiClient googleApiClient, zzan zzan) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzav(this, status);
    }
}
