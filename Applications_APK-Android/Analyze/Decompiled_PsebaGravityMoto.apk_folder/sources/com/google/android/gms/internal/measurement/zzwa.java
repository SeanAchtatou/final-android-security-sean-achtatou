package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

final class zzwa<T> implements zzwl<T> {
    private final zzvv zzcaw;
    private final boolean zzcax;
    private final zzxd<?, ?> zzcbg;
    private final zzuc<?> zzcbh;

    private zzwa(zzxd<?, ?> zzxd, zzuc<?> zzuc, zzvv zzvv) {
        this.zzcbg = zzxd;
        this.zzcax = zzuc.zze(zzvv);
        this.zzcbh = zzuc;
        this.zzcaw = zzvv;
    }

    static <T> zzwa<T> zza(zzxd<?, ?> zzxd, zzuc<?> zzuc, zzvv zzvv) {
        return new zzwa<>(zzxd, zzuc, zzvv);
    }

    public final T newInstance() {
        return this.zzcaw.zzwi().zzwn();
    }

    public final boolean equals(T t, T t2) {
        if (!this.zzcbg.zzal(t).equals(this.zzcbg.zzal(t2))) {
            return false;
        }
        if (this.zzcax) {
            return this.zzcbh.zzw(t).equals(this.zzcbh.zzw(t2));
        }
        return true;
    }

    public final int hashCode(T t) {
        int hashCode = this.zzcbg.zzal(t).hashCode();
        return this.zzcax ? (hashCode * 53) + this.zzcbh.zzw(t).hashCode() : hashCode;
    }

    public final void zzd(T t, T t2) {
        zzwn.zza(this.zzcbg, t, t2);
        if (this.zzcax) {
            zzwn.zza(this.zzcbh, t, t2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzxy.zza(int, java.lang.Object):void
     arg types: [int, com.google.android.gms.internal.measurement.zzte]
     candidates:
      com.google.android.gms.internal.measurement.zzxy.zza(int, double):void
      com.google.android.gms.internal.measurement.zzxy.zza(int, float):void
      com.google.android.gms.internal.measurement.zzxy.zza(int, long):void
      com.google.android.gms.internal.measurement.zzxy.zza(int, com.google.android.gms.internal.measurement.zzte):void
      com.google.android.gms.internal.measurement.zzxy.zza(int, java.util.List<java.lang.String>):void
      com.google.android.gms.internal.measurement.zzxy.zza(int, java.lang.Object):void */
    public final void zza(T t, zzxy zzxy) throws IOException {
        Iterator<Map.Entry<?, Object>> it = this.zzcbh.zzw(t).iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            zzuh zzuh = (zzuh) next.getKey();
            if (zzuh.zzwa() != zzxx.MESSAGE || zzuh.zzwb() || zzuh.zzwc()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzva) {
                zzxy.zza(zzuh.zzc(), (Object) ((zzva) next).zzxa().zztw());
            } else {
                zzxy.zza(zzuh.zzc(), next.getValue());
            }
        }
        zzxd<?, ?> zzxd = this.zzcbg;
        zzxd.zzc(zzxd.zzal(t), zzxy);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzxd.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzte):void
     arg types: [?, int, com.google.android.gms.internal.measurement.zzte]
     candidates:
      com.google.android.gms.internal.measurement.zzxd.zza(java.lang.Object, int, long):void
      com.google.android.gms.internal.measurement.zzxd.zza(java.lang.Object, int, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzxd.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzte):void */
    public final void zza(Object obj, zzwk zzwk, zzub zzub) throws IOException {
        boolean z;
        zzxd<?, ?> zzxd = this.zzcbg;
        zzuc<?> zzuc = this.zzcbh;
        Object zzam = zzxd.zzam(obj);
        zzuf<?> zzx = zzuc.zzx(obj);
        do {
            try {
                if (zzwk.zzvh() == Integer.MAX_VALUE) {
                    zzxd.zzg(obj, zzam);
                    return;
                }
                int tag = zzwk.getTag();
                if (tag == 11) {
                    int i = 0;
                    Object obj2 = null;
                    zzte zzte = null;
                    while (zzwk.zzvh() != Integer.MAX_VALUE) {
                        int tag2 = zzwk.getTag();
                        if (tag2 == 16) {
                            i = zzwk.zzus();
                            obj2 = zzuc.zza(zzub, this.zzcaw, i);
                        } else if (tag2 == 26) {
                            if (obj2 != null) {
                                zzuc.zza(zzwk, obj2, zzub, zzx);
                            } else {
                                zzte = zzwk.zzur();
                            }
                        } else if (!zzwk.zzvi()) {
                            break;
                        }
                    }
                    if (zzwk.getTag() != 12) {
                        throw zzuv.zzwt();
                    } else if (zzte != null) {
                        if (obj2 != null) {
                            zzuc.zza(zzte, obj2, zzub, zzx);
                        } else {
                            zzxd.zza((Object) zzam, i, zzte);
                        }
                    }
                } else if ((tag & 7) == 2) {
                    Object zza = zzuc.zza(zzub, this.zzcaw, tag >>> 3);
                    if (zza != null) {
                        zzuc.zza(zzwk, zza, zzub, zzx);
                    } else {
                        z = zzxd.zza(zzam, zzwk);
                        continue;
                    }
                } else {
                    z = zzwk.zzvi();
                    continue;
                }
                z = true;
                continue;
            } finally {
                zzxd.zzg(obj, zzam);
            }
        } while (z);
    }

    public final void zzy(T t) {
        this.zzcbg.zzy(t);
        this.zzcbh.zzy(t);
    }

    public final boolean zzaj(T t) {
        return this.zzcbh.zzw(t).isInitialized();
    }

    public final int zzai(T t) {
        zzxd<?, ?> zzxd = this.zzcbg;
        int zzan = zzxd.zzan(zzxd.zzal(t)) + 0;
        return this.zzcax ? zzan + this.zzcbh.zzw(t).zzvy() : zzan;
    }
}
