package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

@RetainForClient
@SafeParcelable.Class(creator = "ParticipantEntityCreator")
@SafeParcelable.Reserved({1000})
public final class ParticipantEntity extends GamesDowngradeableSafeParcel implements Participant {
    public static final Parcelable.Creator<ParticipantEntity> CREATOR = new zza();
    @SafeParcelable.Field(getter = "getStatus", id = 5)
    private final int status;
    @SafeParcelable.Field(getter = "getIconImageUrl", id = 11)
    private final String zzac;
    @SafeParcelable.Field(getter = "getHiResImageUrl", id = 12)
    private final String zzad;
    @SafeParcelable.Field(getter = "getPlayer", id = 8)
    private final PlayerEntity zzfh;
    @SafeParcelable.Field(getter = "getParticipantId", id = 1)
    private final String zzhl;
    @SafeParcelable.Field(getter = "getDisplayName", id = 2)
    private final String zzn;
    @SafeParcelable.Field(getter = "getClientAddress", id = 6)
    private final String zzoh;
    @SafeParcelable.Field(getter = "isConnectedToRoom", id = 7)
    private final boolean zzoi;
    @SafeParcelable.Field(getter = "getCapabilities", id = 9)
    private final int zzoj;
    @SafeParcelable.Field(getter = "getResult", id = 10)
    private final ParticipantResult zzok;
    @SafeParcelable.Field(getter = "getIconImageUri", id = 3)
    private final Uri zzr;
    @SafeParcelable.Field(getter = "getHiResImageUri", id = 4)
    private final Uri zzs;

    static final class zza extends zzc {
        zza() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }

        public final ParticipantEntity zze(Parcel parcel) {
            if (ParticipantEntity.zzb(ParticipantEntity.getUnparcelClientVersion()) || ParticipantEntity.canUnparcelSafely(ParticipantEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            String readString4 = parcel.readString();
            Uri parse2 = readString4 == null ? null : Uri.parse(readString4);
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z = true;
            boolean z2 = parcel.readInt() > 0;
            if (parcel.readInt() <= 0) {
                z = false;
            }
            return new ParticipantEntity(readString, readString2, parse, parse2, readInt, readString5, z2, z ? PlayerEntity.CREATOR.createFromParcel(parcel) : null, 7, null, null, null);
        }
    }

    public ParticipantEntity(Participant participant) {
        this.zzhl = participant.getParticipantId();
        this.zzn = participant.getDisplayName();
        this.zzr = participant.getIconImageUri();
        this.zzs = participant.getHiResImageUri();
        this.status = participant.getStatus();
        this.zzoh = participant.zzcg();
        this.zzoi = participant.isConnectedToRoom();
        Player player = participant.getPlayer();
        this.zzfh = player == null ? null : new PlayerEntity(player);
        this.zzoj = participant.getCapabilities();
        this.zzok = participant.getResult();
        this.zzac = participant.getIconImageUrl();
        this.zzad = participant.getHiResImageUrl();
    }

    @SafeParcelable.Constructor
    ParticipantEntity(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) String str2, @SafeParcelable.Param(id = 3) Uri uri, @SafeParcelable.Param(id = 4) Uri uri2, @SafeParcelable.Param(id = 5) int i, @SafeParcelable.Param(id = 6) String str3, @SafeParcelable.Param(id = 7) boolean z, @SafeParcelable.Param(id = 8) PlayerEntity playerEntity, @SafeParcelable.Param(id = 9) int i2, @SafeParcelable.Param(id = 10) ParticipantResult participantResult, @SafeParcelable.Param(id = 11) String str4, @SafeParcelable.Param(id = 12) String str5) {
        this.zzhl = str;
        this.zzn = str2;
        this.zzr = uri;
        this.zzs = uri2;
        this.status = i;
        this.zzoh = str3;
        this.zzoi = z;
        this.zzfh = playerEntity;
        this.zzoj = i2;
        this.zzok = participantResult;
        this.zzac = str4;
        this.zzad = str5;
    }

    static int zza(Participant participant) {
        return Objects.hashCode(participant.getPlayer(), Integer.valueOf(participant.getStatus()), participant.zzcg(), Boolean.valueOf(participant.isConnectedToRoom()), participant.getDisplayName(), participant.getIconImageUri(), participant.getHiResImageUri(), Integer.valueOf(participant.getCapabilities()), participant.getResult(), participant.getParticipantId());
    }

    static boolean zza(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return Objects.equal(participant2.getPlayer(), participant.getPlayer()) && Objects.equal(Integer.valueOf(participant2.getStatus()), Integer.valueOf(participant.getStatus())) && Objects.equal(participant2.zzcg(), participant.zzcg()) && Objects.equal(Boolean.valueOf(participant2.isConnectedToRoom()), Boolean.valueOf(participant.isConnectedToRoom())) && Objects.equal(participant2.getDisplayName(), participant.getDisplayName()) && Objects.equal(participant2.getIconImageUri(), participant.getIconImageUri()) && Objects.equal(participant2.getHiResImageUri(), participant.getHiResImageUri()) && Objects.equal(Integer.valueOf(participant2.getCapabilities()), Integer.valueOf(participant.getCapabilities())) && Objects.equal(participant2.getResult(), participant.getResult()) && Objects.equal(participant2.getParticipantId(), participant.getParticipantId());
    }

    static String zzb(Participant participant) {
        return Objects.toStringHelper(participant).add("ParticipantId", participant.getParticipantId()).add("Player", participant.getPlayer()).add("Status", Integer.valueOf(participant.getStatus())).add("ClientAddress", participant.zzcg()).add("ConnectedToRoom", Boolean.valueOf(participant.isConnectedToRoom())).add("DisplayName", participant.getDisplayName()).add("IconImage", participant.getIconImageUri()).add("IconImageUrl", participant.getIconImageUrl()).add("HiResImage", participant.getHiResImageUri()).add("HiResImageUrl", participant.getHiResImageUrl()).add("Capabilities", Integer.valueOf(participant.getCapabilities())).add("Result", participant.getResult()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final Participant freeze() {
        return this;
    }

    public final int getCapabilities() {
        return this.zzoj;
    }

    public final String getDisplayName() {
        PlayerEntity playerEntity = this.zzfh;
        return playerEntity == null ? this.zzn : playerEntity.getDisplayName();
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        PlayerEntity playerEntity = this.zzfh;
        if (playerEntity == null) {
            DataUtils.copyStringToBuffer(this.zzn, charArrayBuffer);
        } else {
            playerEntity.getDisplayName(charArrayBuffer);
        }
    }

    public final Uri getHiResImageUri() {
        PlayerEntity playerEntity = this.zzfh;
        return playerEntity == null ? this.zzs : playerEntity.getHiResImageUri();
    }

    public final String getHiResImageUrl() {
        PlayerEntity playerEntity = this.zzfh;
        return playerEntity == null ? this.zzad : playerEntity.getHiResImageUrl();
    }

    public final Uri getIconImageUri() {
        PlayerEntity playerEntity = this.zzfh;
        return playerEntity == null ? this.zzr : playerEntity.getIconImageUri();
    }

    public final String getIconImageUrl() {
        PlayerEntity playerEntity = this.zzfh;
        return playerEntity == null ? this.zzac : playerEntity.getIconImageUrl();
    }

    public final String getParticipantId() {
        return this.zzhl;
    }

    public final Player getPlayer() {
        return this.zzfh;
    }

    public final ParticipantResult getResult() {
        return this.zzok;
    }

    public final int getStatus() {
        return this.status;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isConnectedToRoom() {
        return this.zzoi;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final void setShouldDowngrade(boolean z) {
        super.setShouldDowngrade(z);
        PlayerEntity playerEntity = this.zzfh;
        if (playerEntity != null) {
            playerEntity.setShouldDowngrade(z);
        }
    }

    public final String toString() {
        return zzb(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        if (!shouldDowngrade()) {
            int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeString(parcel, 1, getParticipantId(), false);
            SafeParcelWriter.writeString(parcel, 2, getDisplayName(), false);
            SafeParcelWriter.writeParcelable(parcel, 3, getIconImageUri(), i, false);
            SafeParcelWriter.writeParcelable(parcel, 4, getHiResImageUri(), i, false);
            SafeParcelWriter.writeInt(parcel, 5, getStatus());
            SafeParcelWriter.writeString(parcel, 6, this.zzoh, false);
            SafeParcelWriter.writeBoolean(parcel, 7, isConnectedToRoom());
            SafeParcelWriter.writeParcelable(parcel, 8, getPlayer(), i, false);
            SafeParcelWriter.writeInt(parcel, 9, this.zzoj);
            SafeParcelWriter.writeParcelable(parcel, 10, getResult(), i, false);
            SafeParcelWriter.writeString(parcel, 11, getIconImageUrl(), false);
            SafeParcelWriter.writeString(parcel, 12, getHiResImageUrl(), false);
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
            return;
        }
        parcel.writeString(this.zzhl);
        parcel.writeString(this.zzn);
        Uri uri = this.zzr;
        String str = null;
        parcel.writeString(uri == null ? null : uri.toString());
        Uri uri2 = this.zzs;
        if (uri2 != null) {
            str = uri2.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.status);
        parcel.writeString(this.zzoh);
        parcel.writeInt(this.zzoi ? 1 : 0);
        if (this.zzfh == null) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        PlayerEntity playerEntity = this.zzfh;
        if (playerEntity != null) {
            playerEntity.writeToParcel(parcel, i);
        }
    }

    public final String zzcg() {
        return this.zzoh;
    }
}
