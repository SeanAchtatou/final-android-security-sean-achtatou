package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public final class zzcsh implements zzcva<zzcsg> {
    private final zzbbl zzfqw;
    private final Context zzlj;

    public zzcsh(zzbbl zzbbl, Context context) {
        this.zzfqw = zzbbl;
        this.zzlj = context;
    }

    public final zzbbh<zzcsg> zzalm() {
        return this.zzfqw.zza(new zzcsi(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcsg zzalp() throws Exception {
        double d;
        Intent registerReceiver = this.zzlj.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean z = false;
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra("status", -1);
            double intExtra2 = (double) registerReceiver.getIntExtra("level", -1);
            double intExtra3 = (double) registerReceiver.getIntExtra("scale", -1);
            Double.isNaN(intExtra2);
            Double.isNaN(intExtra3);
            d = intExtra2 / intExtra3;
            if (intExtra == 2 || intExtra == 5) {
                z = true;
            }
        } else {
            d = -1.0d;
        }
        return new zzcsg(d, z);
    }
}
