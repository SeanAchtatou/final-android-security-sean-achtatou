package com.google.android.gms.internal.ads;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

public final class zzccz implements zzahy {
    private final zzbse zzfjg;
    @Nullable
    private final zzato zzfsz;
    private final String zzfta;
    private final String zzftb;

    public zzccz(zzbse zzbse, zzcxm zzcxm) {
        this.zzfjg = zzbse;
        this.zzfsz = zzcxm.zzdnx;
        this.zzfta = zzcxm.zzdeu;
        this.zzftb = zzcxm.zzdev;
    }

    public final void zzrq() {
        this.zzfjg.onRewardedVideoStarted();
    }

    @ParametersAreNonnullByDefault
    public final void zza(zzato zzato) {
        int i;
        String str;
        zzato zzato2 = this.zzfsz;
        if (zzato2 != null) {
            zzato = zzato2;
        }
        if (zzato != null) {
            str = zzato.type;
            i = zzato.zzdqm;
        } else {
            str = "";
            i = 1;
        }
        this.zzfjg.zzb(new zzasp(str, i), this.zzfta, this.zzftb);
    }

    public final void zzrr() {
        this.zzfjg.onRewardedVideoCompleted();
    }
}
