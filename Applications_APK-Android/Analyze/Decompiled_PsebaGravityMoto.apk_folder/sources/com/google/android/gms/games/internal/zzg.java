package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzg implements BaseImplementation.ResultHolder<Status> {
    private final /* synthetic */ BaseGmsClient.SignOutCallbacks zzgb;

    zzg(zze zze, BaseGmsClient.SignOutCallbacks signOutCallbacks) {
        this.zzgb = signOutCallbacks;
    }

    public final void setFailedResult(Status status) {
        this.zzgb.onSignOutComplete();
    }

    public final /* synthetic */ void setResult(Object obj) {
        this.zzgb.onSignOutComplete();
    }
}
