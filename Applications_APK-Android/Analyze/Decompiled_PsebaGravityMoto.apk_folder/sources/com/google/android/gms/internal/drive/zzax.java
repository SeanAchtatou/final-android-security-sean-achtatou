package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzax extends zzav {
    private final /* synthetic */ zzj zzei;
    private final /* synthetic */ zzee zzej;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzax(zzaw zzaw, GoogleApiClient googleApiClient, zzj zzj, zzee zzee) {
        super(googleApiClient);
        this.zzei = zzj;
        this.zzej = zzee;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzeo) ((zzaw) anyClient).getService()).zza(this.zzei, this.zzej, (String) null, new zzgs(this));
    }
}
