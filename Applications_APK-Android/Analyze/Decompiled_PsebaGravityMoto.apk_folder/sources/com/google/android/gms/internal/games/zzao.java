package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzao extends zzau {
    private final /* synthetic */ String zzbq;
    private final /* synthetic */ boolean zzjg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzao(zzam zzam, GoogleApiClient googleApiClient, String str, boolean z) {
        super(googleApiClient, null);
        this.zzbq = str;
        this.zzjg = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb((BaseImplementation.ResultHolder<Leaderboards.LeaderboardMetadataResult>) this, this.zzbq, this.zzjg);
    }
}
