package com.google.android.gms.internal.ads;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;

final class zzoh {
    private final zzst zzbac = new zzst(8);
    private int zzbch;

    public final boolean zza(zzno zzno) throws IOException, InterruptedException {
        zzno zzno2 = zzno;
        long length = zzno.getLength();
        long j = PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
        if (length != -1 && length <= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            j = length;
        }
        int i = (int) j;
        zzno2.zzc(this.zzbac.data, 0, 4);
        this.zzbch = 4;
        for (long zzge = this.zzbac.zzge(); zzge != 440786851; zzge = ((zzge << 8) & -256) | ((long) (this.zzbac.data[0] & 255))) {
            int i2 = this.zzbch + 1;
            this.zzbch = i2;
            if (i2 == i) {
                return false;
            }
            zzno2.zzc(this.zzbac.data, 0, 1);
        }
        long zzc = zzc(zzno);
        long j2 = (long) this.zzbch;
        if (zzc != Long.MIN_VALUE && (length == -1 || j2 + zzc < length)) {
            while (true) {
                int i3 = this.zzbch;
                long j3 = j2 + zzc;
                if (((long) i3) < j3) {
                    if (zzc(zzno) == Long.MIN_VALUE) {
                        return false;
                    }
                    long zzc2 = zzc(zzno);
                    if (zzc2 < 0 || zzc2 > 2147483647L) {
                        return false;
                    }
                    if (zzc2 != 0) {
                        zzno2.zzar((int) zzc2);
                        this.zzbch = (int) (((long) this.zzbch) + zzc2);
                    }
                } else if (((long) i3) == j3) {
                    return true;
                }
            }
        }
        return false;
    }

    private final long zzc(zzno zzno) throws IOException, InterruptedException {
        int i = 0;
        zzno.zzc(this.zzbac.data, 0, 1);
        byte b = this.zzbac.data[0] & 255;
        if (b == 0) {
            return Long.MIN_VALUE;
        }
        int i2 = 128;
        int i3 = 0;
        while ((b & i2) == 0) {
            i2 >>= 1;
            i3++;
        }
        int i4 = b & (i2 ^ -1);
        zzno.zzc(this.zzbac.data, 1, i3);
        while (i < i3) {
            i++;
            i4 = (this.zzbac.data[i] & 255) + (i4 << 8);
        }
        this.zzbch += i3 + 1;
        return (long) i4;
    }
}
