package com.google.android.gms.internal.ads;

final class zzor implements zzoo {
    private int zzamy;
    private final int zzand = this.zzbea.zzgg();
    private final zzst zzbea;
    private final int zzber = (this.zzbea.zzgg() & 255);
    private int zzbes;

    public zzor(zzol zzol) {
        this.zzbea = zzol.zzbea;
        this.zzbea.setPosition(12);
    }

    public final boolean zzio() {
        return false;
    }

    public final int zzim() {
        return this.zzand;
    }

    public final int zzin() {
        int i = this.zzber;
        if (i == 8) {
            return this.zzbea.readUnsignedByte();
        }
        if (i == 16) {
            return this.zzbea.readUnsignedShort();
        }
        int i2 = this.zzamy;
        this.zzamy = i2 + 1;
        if (i2 % 2 != 0) {
            return this.zzbes & 15;
        }
        this.zzbes = this.zzbea.readUnsignedByte();
        return (this.zzbes & 240) >> 4;
    }
}
