package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzb;
import com.google.android.gms.common.util.CollectionUtils;
import java.util.Map;

@zzard
public final class zzahr implements zzaho<zzbgz> {
    private static final Map<String, Integer> zzdam = CollectionUtils.mapOfKeyValueArrays(new String[]{"resize", "playVideo", "storePicture", "createCalendarEvent", "setOrientationProperties", "closeResizedAd", "unload"}, new Integer[]{1, 2, 3, 4, 5, 6, 7});
    private final zzb zzdaj;
    private final zzapr zzdak;
    private final zzaqc zzdal;

    public zzahr(zzb zzb, zzapr zzapr, zzaqc zzaqc) {
        this.zzdaj = zzb;
        this.zzdak = zzapr;
        this.zzdal = zzaqc;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzb zzb;
        zzbgz zzbgz = (zzbgz) obj;
        int intValue = zzdam.get((String) map.get("a")).intValue();
        if (intValue != 5 && intValue != 7 && (zzb = this.zzdaj) != null && !zzb.zzkx()) {
            this.zzdaj.zzbk(null);
        } else if (intValue == 1) {
            this.zzdak.zzg(map);
        } else if (intValue == 3) {
            new zzapu(zzbgz, map).execute();
        } else if (intValue == 4) {
            new zzapo(zzbgz, map).execute();
        } else if (intValue == 5) {
            new zzapt(zzbgz, map).execute();
        } else if (intValue == 6) {
            this.zzdak.zzw(true);
        } else if (intValue != 7) {
            zzawz.zzeo("Unknown MRAID command called.");
        } else if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcmw)).booleanValue()) {
            this.zzdal.zztd();
        }
    }
}
