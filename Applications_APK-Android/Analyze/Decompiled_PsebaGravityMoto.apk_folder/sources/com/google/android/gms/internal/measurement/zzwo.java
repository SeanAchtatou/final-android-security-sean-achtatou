package com.google.android.gms.internal.measurement;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class zzwo<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zzbqa;
    private final int zzcbt;
    /* access modifiers changed from: private */
    public List<zzwv> zzcbu;
    /* access modifiers changed from: private */
    public Map<K, V> zzcbv;
    private volatile zzwx zzcbw;
    /* access modifiers changed from: private */
    public Map<K, V> zzcbx;
    private volatile zzwr zzcby;

    static <FieldDescriptorType extends zzuh<FieldDescriptorType>> zzwo<FieldDescriptorType, Object> zzbw(int i) {
        return new zzwp(i);
    }

    private zzwo(int i) {
        this.zzcbt = i;
        this.zzcbu = Collections.emptyList();
        this.zzcbv = Collections.emptyMap();
        this.zzcbx = Collections.emptyMap();
    }

    public void zzsw() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.zzbqa) {
            if (this.zzcbv.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.zzcbv);
            }
            this.zzcbv = map;
            if (this.zzcbx.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.zzcbx);
            }
            this.zzcbx = map2;
            this.zzbqa = true;
        }
    }

    public final boolean isImmutable() {
        return this.zzbqa;
    }

    public final int zzyc() {
        return this.zzcbu.size();
    }

    public final Map.Entry<K, V> zzbx(int i) {
        return this.zzcbu.get(i);
    }

    public final Iterable<Map.Entry<K, V>> zzyd() {
        if (this.zzcbv.isEmpty()) {
            return zzws.zzyi();
        }
        return this.zzcbv.entrySet();
    }

    public int size() {
        return this.zzcbu.size() + this.zzcbv.size();
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzcbv.containsKey(comparable);
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzcbu.get(zza).getValue();
        }
        return this.zzcbv.get(comparable);
    }

    /* renamed from: zza */
    public final V put(Comparable comparable, Object obj) {
        zzyf();
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzcbu.get(zza).setValue(obj);
        }
        zzyf();
        if (this.zzcbu.isEmpty() && !(this.zzcbu instanceof ArrayList)) {
            this.zzcbu = new ArrayList(this.zzcbt);
        }
        int i = -(zza + 1);
        if (i >= this.zzcbt) {
            return zzyg().put(comparable, obj);
        }
        int size = this.zzcbu.size();
        int i2 = this.zzcbt;
        if (size == i2) {
            zzwv remove = this.zzcbu.remove(i2 - 1);
            zzyg().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.zzcbu.add(i, new zzwv(this, comparable, obj));
        return null;
    }

    public void clear() {
        zzyf();
        if (!this.zzcbu.isEmpty()) {
            this.zzcbu.clear();
        }
        if (!this.zzcbv.isEmpty()) {
            this.zzcbv.clear();
        }
    }

    public V remove(Object obj) {
        zzyf();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return zzby(zza);
        }
        if (this.zzcbv.isEmpty()) {
            return null;
        }
        return this.zzcbv.remove(comparable);
    }

    /* access modifiers changed from: private */
    public final V zzby(int i) {
        zzyf();
        V value = this.zzcbu.remove(i).getValue();
        if (!this.zzcbv.isEmpty()) {
            Iterator it = zzyg().entrySet().iterator();
            this.zzcbu.add(new zzwv(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final int zza(K r5) {
        /*
            r4 = this;
            java.util.List<com.google.android.gms.internal.measurement.zzwv> r0 = r4.zzcbu
            int r0 = r0.size()
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0025
            java.util.List<com.google.android.gms.internal.measurement.zzwv> r1 = r4.zzcbu
            java.lang.Object r1 = r1.get(r0)
            com.google.android.gms.internal.measurement.zzwv r1 = (com.google.android.gms.internal.measurement.zzwv) r1
            java.lang.Object r1 = r1.getKey()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r1 = r5.compareTo(r1)
            if (r1 <= 0) goto L_0x0022
            int r0 = r0 + 2
            int r5 = -r0
            return r5
        L_0x0022:
            if (r1 != 0) goto L_0x0025
            return r0
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 > r0) goto L_0x0049
            int r2 = r1 + r0
            int r2 = r2 / 2
            java.util.List<com.google.android.gms.internal.measurement.zzwv> r3 = r4.zzcbu
            java.lang.Object r3 = r3.get(r2)
            com.google.android.gms.internal.measurement.zzwv r3 = (com.google.android.gms.internal.measurement.zzwv) r3
            java.lang.Object r3 = r3.getKey()
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r3 = r5.compareTo(r3)
            if (r3 >= 0) goto L_0x0043
            int r0 = r2 + -1
            goto L_0x0026
        L_0x0043:
            if (r3 <= 0) goto L_0x0048
            int r1 = r2 + 1
            goto L_0x0026
        L_0x0048:
            return r2
        L_0x0049:
            int r1 = r1 + 1
            int r5 = -r1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzwo.zza(java.lang.Comparable):int");
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzcbw == null) {
            this.zzcbw = new zzwx(this, null);
        }
        return this.zzcbw;
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> zzye() {
        if (this.zzcby == null) {
            this.zzcby = new zzwr(this, null);
        }
        return this.zzcby;
    }

    /* access modifiers changed from: private */
    public final void zzyf() {
        if (this.zzbqa) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzyg() {
        zzyf();
        if (this.zzcbv.isEmpty() && !(this.zzcbv instanceof TreeMap)) {
            this.zzcbv = new TreeMap();
            this.zzcbx = ((TreeMap) this.zzcbv).descendingMap();
        }
        return (SortedMap) this.zzcbv;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzwo)) {
            return super.equals(obj);
        }
        zzwo zzwo = (zzwo) obj;
        int size = size();
        if (size != zzwo.size()) {
            return false;
        }
        int zzyc = zzyc();
        if (zzyc != zzwo.zzyc()) {
            return entrySet().equals(zzwo.entrySet());
        }
        for (int i = 0; i < zzyc; i++) {
            if (!zzbx(i).equals(zzwo.zzbx(i))) {
                return false;
            }
        }
        if (zzyc != size) {
            return this.zzcbv.equals(zzwo.zzcbv);
        }
        return true;
    }

    public int hashCode() {
        int zzyc = zzyc();
        int i = 0;
        for (int i2 = 0; i2 < zzyc; i2++) {
            i += this.zzcbu.get(i2).hashCode();
        }
        return this.zzcbv.size() > 0 ? i + this.zzcbv.hashCode() : i;
    }

    /* synthetic */ zzwo(int i, zzwp zzwp) {
        this(i);
    }
}
