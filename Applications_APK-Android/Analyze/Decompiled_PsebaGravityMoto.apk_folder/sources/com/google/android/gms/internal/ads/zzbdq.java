package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.TextureView;
import androidx.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import org.checkerframework.checker.nullness.qual.EnsuresNonNullIf;
import org.checkerframework.dataflow.qual.SideEffectFree;

@TargetApi(16)
@ParametersAreNonnullByDefault
@zzard
public final class zzbdq extends zzbco implements TextureView.SurfaceTextureListener {
    @Nullable
    private Surface zzaez;
    /* access modifiers changed from: private */
    public float zzaft;
    private final zzbdg zzeaw;
    private final boolean zzeax;
    /* access modifiers changed from: private */
    public int zzebc;
    /* access modifiers changed from: private */
    public int zzebd;
    private int zzebf;
    private int zzebg;
    @Nullable
    private zzbdd zzebh;
    private boolean zzebi;
    private zzbcn zzebk;
    private final zzbdf zzebt;
    @Nullable
    private zzge zzefe;
    @Nullable
    private zzhd zzeff;
    @Nullable
    private zzgn zzefg;
    private float zzefn;
    private final int zzefo;
    private final zzbde zzefp;
    @Nullable
    private zzbdk zzefq;
    private String zzefr;
    private boolean zzefs;
    /* access modifiers changed from: private */
    public int zzeft = 1;
    private boolean zzefu;
    private boolean zzefv;
    private final zzgh zzefw = new zzbee(this);
    private final zzhh zzefx = new zzbef(this);
    private final zzgq zzefy = new zzbeg(this);
    private final Context zzlj;

    public zzbdq(Context context, zzbdg zzbdg, zzbdf zzbdf, int i, boolean z, boolean z2, zzbde zzbde) {
        super(context);
        this.zzlj = context;
        this.zzeax = z2;
        this.zzebt = zzbdf;
        this.zzefo = i;
        this.zzeaw = zzbdg;
        this.zzebi = z;
        this.zzefp = zzbde;
        setSurfaceTextureListener(this);
        this.zzeaw.zzb(this);
    }

    @EnsuresNonNullIf(expression = {"mPlayer"}, result = true)
    private final boolean zzyv() {
        return this.zzefe != null && !this.zzefs;
    }

    @EnsuresNonNullIf(expression = {"mPlayer"}, result = true)
    private final boolean zzyw() {
        return zzyv() && this.zzeft != 1;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v34, types: [com.google.android.gms.internal.ads.zzgl] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void
     arg types: [android.view.Surface, int]
     candidates:
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, float):float
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, int):int
      com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, float):void
      com.google.android.gms.internal.ads.zzbco.zza(float, float):void
      com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzyx() {
        /*
            r14 = this;
            com.google.android.gms.internal.ads.zzge r0 = r14.zzefe
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            java.lang.String r0 = r14.zzefr
            if (r0 == 0) goto L_0x021e
            android.view.Surface r1 = r14.zzaez
            if (r1 == 0) goto L_0x021e
            java.lang.String r1 = "cache:"
            boolean r0 = r0.startsWith(r1)
            java.lang.String r1 = "AdExoPlayerHelper Error"
            java.lang.String r2 = "video/webm"
            r3 = 0
            r4 = 0
            if (r0 == 0) goto L_0x012e
            com.google.android.gms.internal.ads.zzbdf r0 = r14.zzebt
            java.lang.String r5 = r14.zzefr
            com.google.android.gms.internal.ads.zzbft r0 = r0.zzet(r5)
            if (r0 == 0) goto L_0x003d
            boolean r5 = r0 instanceof com.google.android.gms.internal.ads.zzbgl
            if (r5 == 0) goto L_0x003d
            com.google.android.gms.internal.ads.zzbgl r0 = (com.google.android.gms.internal.ads.zzbgl) r0
            r0.zzzx()
            com.google.android.gms.internal.ads.zzbdk r4 = r0.zzzy()
            com.google.android.gms.internal.ads.zzgh r0 = r14.zzefw
            com.google.android.gms.internal.ads.zzhh r1 = r14.zzefx
            com.google.android.gms.internal.ads.zzgq r2 = r14.zzefy
            r4.zza(r0, r1, r2)
            goto L_0x01cc
        L_0x003d:
            boolean r5 = r0 instanceof com.google.android.gms.internal.ads.zzbgg
            if (r5 == 0) goto L_0x0110
            com.google.android.gms.internal.ads.zzbgg r0 = (com.google.android.gms.internal.ads.zzbgg) r0
            java.nio.ByteBuffer r5 = r0.getByteBuffer()
            java.lang.String r6 = r0.getUrl()
            boolean r0 = r0.zzzv()
            com.google.android.gms.internal.ads.zzbdk r7 = new com.google.android.gms.internal.ads.zzbdk
            r7.<init>()
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0060
            com.google.android.gms.internal.ads.zzjg r2 = new com.google.android.gms.internal.ads.zzjg
            r2.<init>()
            goto L_0x0065
        L_0x0060:
            com.google.android.gms.internal.ads.zziv r2 = new com.google.android.gms.internal.ads.zziv
            r2.<init>()
        L_0x0065:
            r11 = r2
            if (r0 == 0) goto L_0x008c
            int r0 = r5.limit()
            if (r0 <= 0) goto L_0x008c
            int r0 = r5.limit()
            byte[] r0 = new byte[r0]
            r5.get(r0)
            com.google.android.gms.internal.ads.zzjo r10 = new com.google.android.gms.internal.ads.zzjo
            r10.<init>(r0)
            com.google.android.gms.internal.ads.zzig r0 = new com.google.android.gms.internal.ads.zzig
            android.net.Uri r9 = android.net.Uri.parse(r6)
            r12 = 2
            com.google.android.gms.internal.ads.zzbde r2 = r14.zzefp
            int r13 = r2.zzeee
            r8 = r0
            r8.<init>(r9, r10, r11, r12, r13)
            goto L_0x00f9
        L_0x008c:
            com.google.android.gms.internal.ads.zzaxi r0 = com.google.android.gms.ads.internal.zzk.zzlg()
            com.google.android.gms.internal.ads.zzbdf r2 = r14.zzebt
            android.content.Context r2 = r2.getContext()
            com.google.android.gms.internal.ads.zzbdf r4 = r14.zzebt
            com.google.android.gms.internal.ads.zzbai r4 = r4.zzyh()
            java.lang.String r4 = r4.zzbsx
            java.lang.String r0 = r0.zzq(r2, r4)
            com.google.android.gms.internal.ads.zzjt r2 = new com.google.android.gms.internal.ads.zzjt
            com.google.android.gms.internal.ads.zzbdf r4 = r14.zzebt
            android.content.Context r4 = r4.getContext()
            r2.<init>(r4, r0)
            com.google.android.gms.internal.ads.zzacj<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzacu.zzctr
            com.google.android.gms.internal.ads.zzacr r4 = com.google.android.gms.internal.ads.zzyt.zzpe()
            java.lang.Object r0 = r4.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00cc
            com.google.android.gms.internal.ads.zzbeh r0 = new com.google.android.gms.internal.ads.zzbeh
            android.content.Context r4 = r14.zzlj
            com.google.android.gms.internal.ads.zzbds r8 = new com.google.android.gms.internal.ads.zzbds
            r8.<init>(r14)
            r0.<init>(r4, r2, r8)
            goto L_0x00cd
        L_0x00cc:
            r0 = r2
        L_0x00cd:
            int r2 = r5.limit()
            if (r2 <= 0) goto L_0x00e9
            int r2 = r5.limit()
            byte[] r2 = new byte[r2]
            r5.get(r2)
            com.google.android.gms.internal.ads.zzjo r4 = new com.google.android.gms.internal.ads.zzjo
            r4.<init>(r2)
            com.google.android.gms.internal.ads.zzbej r5 = new com.google.android.gms.internal.ads.zzbej
            int r2 = r2.length
            r5.<init>(r4, r2, r0)
            r10 = r5
            goto L_0x00ea
        L_0x00e9:
            r10 = r0
        L_0x00ea:
            com.google.android.gms.internal.ads.zzig r0 = new com.google.android.gms.internal.ads.zzig
            android.net.Uri r9 = android.net.Uri.parse(r6)
            r12 = 2
            com.google.android.gms.internal.ads.zzbde r2 = r14.zzefp
            int r13 = r2.zzeee
            r8 = r0
            r8.<init>(r9, r10, r11, r12, r13)
        L_0x00f9:
            com.google.android.gms.internal.ads.zzgh r2 = r14.zzefw
            com.google.android.gms.internal.ads.zzhh r4 = r14.zzefx
            com.google.android.gms.internal.ads.zzgq r5 = r14.zzefy
            r7.zza(r2, r4, r5)
            boolean r0 = r7.zza(r0)
            if (r0 != 0) goto L_0x010d
            java.lang.String r0 = "Prepare from ByteBuffer failed."
            r14.zzn(r1, r0)
        L_0x010d:
            r4 = r7
            goto L_0x01cc
        L_0x0110:
            java.lang.String r0 = "Source is MD5 but not found in cache. Source: "
            java.lang.String r1 = r14.zzefr
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r2 = r1.length()
            if (r2 == 0) goto L_0x0123
            java.lang.String r0 = r0.concat(r1)
            goto L_0x0129
        L_0x0123:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r0)
            r0 = r1
        L_0x0129:
            com.google.android.gms.internal.ads.zzawz.zzep(r0)
            goto L_0x01cc
        L_0x012e:
            int r0 = r14.zzefo
            r5 = 2
            r6 = 1
            if (r0 != r6) goto L_0x0147
            com.google.android.gms.internal.ads.zzgl r0 = new com.google.android.gms.internal.ads.zzgl
            com.google.android.gms.internal.ads.zzbdf r2 = r14.zzebt
            android.content.Context r2 = r2.getContext()
            java.lang.String r6 = r14.zzefr
            android.net.Uri r6 = android.net.Uri.parse(r6)
            r0.<init>(r2, r6, r4, r5)
            goto L_0x01b3
        L_0x0147:
            if (r0 != r5) goto L_0x014a
            goto L_0x014b
        L_0x014a:
            r6 = 0
        L_0x014b:
            com.google.android.gms.common.internal.Preconditions.checkArgument(r6)
            com.google.android.gms.internal.ads.zzaxi r0 = com.google.android.gms.ads.internal.zzk.zzlg()
            com.google.android.gms.internal.ads.zzbdf r5 = r14.zzebt
            android.content.Context r5 = r5.getContext()
            com.google.android.gms.internal.ads.zzbdf r6 = r14.zzebt
            com.google.android.gms.internal.ads.zzbai r6 = r6.zzyh()
            java.lang.String r6 = r6.zzbsx
            java.lang.String r0 = r0.zzq(r5, r6)
            com.google.android.gms.internal.ads.zzjt r5 = new com.google.android.gms.internal.ads.zzjt
            com.google.android.gms.internal.ads.zzbdf r6 = r14.zzebt
            android.content.Context r6 = r6.getContext()
            r5.<init>(r6, r0)
            com.google.android.gms.internal.ads.zzacj<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzacu.zzctr
            com.google.android.gms.internal.ads.zzacr r6 = com.google.android.gms.internal.ads.zzyt.zzpe()
            java.lang.Object r0 = r6.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x018f
            com.google.android.gms.internal.ads.zzbeh r0 = new com.google.android.gms.internal.ads.zzbeh
            android.content.Context r6 = r14.zzlj
            com.google.android.gms.internal.ads.zzbdr r7 = new com.google.android.gms.internal.ads.zzbdr
            r7.<init>(r14)
            r0.<init>(r6, r5, r7)
            r10 = r0
            goto L_0x0190
        L_0x018f:
            r10 = r5
        L_0x0190:
            boolean r0 = r2.equals(r4)
            if (r0 == 0) goto L_0x019c
            com.google.android.gms.internal.ads.zzjg r0 = new com.google.android.gms.internal.ads.zzjg
            r0.<init>()
            goto L_0x01a1
        L_0x019c:
            com.google.android.gms.internal.ads.zziv r0 = new com.google.android.gms.internal.ads.zziv
            r0.<init>()
        L_0x01a1:
            r11 = r0
            com.google.android.gms.internal.ads.zzig r0 = new com.google.android.gms.internal.ads.zzig
            java.lang.String r2 = r14.zzefr
            android.net.Uri r9 = android.net.Uri.parse(r2)
            r12 = 2
            com.google.android.gms.internal.ads.zzbde r2 = r14.zzefp
            int r13 = r2.zzeee
            r8 = r0
            r8.<init>(r9, r10, r11, r12, r13)
        L_0x01b3:
            com.google.android.gms.internal.ads.zzbdk r4 = new com.google.android.gms.internal.ads.zzbdk
            r4.<init>()
            com.google.android.gms.internal.ads.zzgh r2 = r14.zzefw
            com.google.android.gms.internal.ads.zzhh r5 = r14.zzefx
            com.google.android.gms.internal.ads.zzgq r6 = r14.zzefy
            r4.zza(r2, r5, r6)
            boolean r0 = r4.zza(r0)
            if (r0 != 0) goto L_0x01cc
            java.lang.String r0 = "Prepare failed."
            r14.zzn(r1, r0)
        L_0x01cc:
            r14.zzefq = r4
            com.google.android.gms.internal.ads.zzbdk r0 = r14.zzefq
            if (r0 != 0) goto L_0x01ef
            java.lang.String r0 = "AdExoPlayerHelper is null. Source: "
            java.lang.String r1 = r14.zzefr
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r2 = r1.length()
            if (r2 == 0) goto L_0x01e5
            java.lang.String r0 = r0.concat(r1)
            goto L_0x01eb
        L_0x01e5:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r0)
            r0 = r1
        L_0x01eb:
            com.google.android.gms.internal.ads.zzawz.zzep(r0)
            return
        L_0x01ef:
            com.google.android.gms.internal.ads.zzge r0 = r0.zzys()
            r14.zzefe = r0
            com.google.android.gms.internal.ads.zzbdk r0 = r14.zzefq
            com.google.android.gms.internal.ads.zzhd r0 = r0.zzyt()
            r14.zzeff = r0
            com.google.android.gms.internal.ads.zzbdk r0 = r14.zzefq
            com.google.android.gms.internal.ads.zzgn r0 = r0.zzyu()
            r14.zzefg = r0
            com.google.android.gms.internal.ads.zzge r0 = r14.zzefe
            if (r0 == 0) goto L_0x021e
            android.view.Surface r0 = r14.zzaez
            r14.zza(r0, r3)
            com.google.android.gms.internal.ads.zzge r0 = r14.zzefe
            int r0 = r0.getPlaybackState()
            r14.zzeft = r0
            int r0 = r14.zzeft
            r1 = 4
            if (r0 != r1) goto L_0x021e
            r14.zzyy()
        L_0x021e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbdq.zzyx():void");
    }

    @SideEffectFree
    private final void zza(@Nullable Surface surface, boolean z) {
        zzhd zzhd;
        zzge zzge = this.zzefe;
        if (zzge == null || (zzhd = this.zzeff) == null) {
            zzawz.zzep("Trying to set surface before player and renderers are initalized.");
        } else if (z) {
            zzge.zzb(zzhd, 1, surface);
        } else {
            zzge.zza(zzhd, 1, surface);
        }
    }

    @SideEffectFree
    private final void zza(float f, boolean z) {
        zzgn zzgn;
        zzge zzge = this.zzefe;
        if (zzge == null || (zzgn = this.zzefg) == null) {
            zzawz.zzep("Trying to set volume before player and renderers are initalized.");
        } else if (z) {
            zzge.zzb(zzgn, 1, Float.valueOf(f));
        } else {
            zzge.zza(zzgn, 1, Float.valueOf(f));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void
     arg types: [float, int]
     candidates:
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, float):float
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, int):int
      com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, float):void
      com.google.android.gms.internal.ads.zzbco.zza(float, float):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void */
    public final void zzxk() {
        zza(this.zzebs.getVolume(), false);
    }

    /* access modifiers changed from: private */
    public final void zzb(int i, int i2, float f) {
        float f2 = i2 == 0 ? 1.0f : (((float) i) * f) / ((float) i2);
        if (this.zzefn != f2) {
            this.zzefn = f2;
            requestLayout();
        }
    }

    /* access modifiers changed from: private */
    public final void zzyy() {
        if (!this.zzefu) {
            this.zzefu = true;
            zzawz.zzds("Video is ready.");
            zzaxi.zzdvv.post(new zzbdw(this));
            zzxk();
            this.zzeaw.zzhd();
            if (this.zzefv) {
                play();
            }
        }
    }

    /* access modifiers changed from: private */
    public final void zzyk() {
        zzawz.zzds("Video ended.");
        if (this.zzefp.zzeec) {
            zzza();
        }
        this.zzeaw.zzym();
        this.zzebs.zzym();
        zzaxi.zzdvv.post(new zzbdx(this));
    }

    /* access modifiers changed from: private */
    public final void zzn(String str, @Nullable String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 19 + String.valueOf(str2).length());
        sb.append("Error received: ");
        sb.append(str);
        sb.append(" : ");
        sb.append(str2);
        zzawz.zzep(sb.toString());
        this.zzefs = true;
        if (this.zzefp.zzeec) {
            zzza();
        }
        zzaxi.zzdvv.post(new zzbdy(this, str, str2));
    }

    public final String zzxg() {
        String str;
        int i = this.zzefo;
        if (i == 1) {
            str = "/Framework";
        } else if (i == 2) {
            StringBuilder sb = new StringBuilder("null".length() + 12);
            sb.append("/Extractor(");
            sb.append((String) null);
            sb.append(")");
            str = sb.toString();
        } else {
            str = "/Unknown";
        }
        String str2 = this.zzebi ? " spherical" : "";
        StringBuilder sb2 = new StringBuilder(String.valueOf(str).length() + 11 + str2.length());
        sb2.append("ExoPlayer/1");
        sb2.append(str);
        sb2.append(str2);
        return sb2.toString();
    }

    public final void zza(zzbcn zzbcn) {
        this.zzebk = zzbcn;
    }

    public final void setVideoPath(String str) {
        if (str != null) {
            this.zzefr = str;
            zzyx();
            return;
        }
        zzawz.zzep("Path is null.");
    }

    public final void play() {
        if (zzyw()) {
            if (this.zzefp.zzeec) {
                zzyz();
            }
            this.zzefe.zzd(true);
            this.zzeaw.zzyl();
            this.zzebs.zzyl();
            this.zzebr.zzxm();
            zzaxi.zzdvv.post(new zzbdz(this));
            return;
        }
        this.zzefv = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, float):float
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, int):int
      com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, float):void
      com.google.android.gms.internal.ads.zzbco.zza(float, float):void
      com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void */
    public final void stop() {
        if (zzyv()) {
            this.zzefe.stop();
            if (this.zzefe != null) {
                zza((Surface) null, true);
                zzbdk zzbdk = this.zzefq;
                if (zzbdk != null) {
                    zzbdk.zzyr();
                    this.zzefq = null;
                }
                this.zzefe = null;
                this.zzeff = null;
                this.zzefg = null;
                this.zzeft = 1;
                this.zzefs = false;
                this.zzefu = false;
                this.zzefv = false;
            }
        }
        this.zzeaw.zzym();
        this.zzebs.zzym();
        this.zzeaw.onStop();
    }

    public final void pause() {
        if (zzyw()) {
            if (this.zzefp.zzeec) {
                zzza();
            }
            this.zzefe.zzd(false);
            this.zzeaw.zzym();
            this.zzebs.zzym();
            zzaxi.zzdvv.post(new zzbea(this));
        }
    }

    public final void seekTo(int i) {
        if (zzyw()) {
            this.zzefe.seekTo((long) i);
        }
    }

    public final void zza(float f, float f2) {
        zzbdd zzbdd = this.zzebh;
        if (zzbdd != null) {
            zzbdd.zzb(f, f2);
        }
    }

    public final int getCurrentPosition() {
        if (zzyw()) {
            return (int) this.zzefe.zzdn();
        }
        return 0;
    }

    public final int getDuration() {
        if (zzyw()) {
            return (int) this.zzefe.getDuration();
        }
        return 0;
    }

    public final int getVideoWidth() {
        return this.zzebc;
    }

    public final int getVideoHeight() {
        return this.zzebd;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, float):float
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, int):int
      com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, float):void
      com.google.android.gms.internal.ads.zzbco.zza(float, float):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r10, int r11) {
        /*
            r9 = this;
            super.onMeasure(r10, r11)
            int r10 = r9.getMeasuredWidth()
            int r11 = r9.getMeasuredHeight()
            float r0 = r9.zzefn
            r1 = 0
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 == 0) goto L_0x0033
            com.google.android.gms.internal.ads.zzbdd r2 = r9.zzebh
            if (r2 != 0) goto L_0x0033
            float r2 = (float) r10
            float r3 = (float) r11
            float r4 = r2 / r3
            float r4 = r0 / r4
            r5 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r5
            r5 = 1008981770(0x3c23d70a, float:0.01)
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 <= 0) goto L_0x0029
            float r2 = r2 / r0
            int r11 = (int) r2
            goto L_0x0033
        L_0x0029:
            r2 = -1138501878(0xffffffffbc23d70a, float:-0.01)
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0033
            float r3 = r3 * r0
            int r10 = (int) r3
        L_0x0033:
            r9.setMeasuredDimension(r10, r11)
            com.google.android.gms.internal.ads.zzbdd r0 = r9.zzebh
            if (r0 == 0) goto L_0x003d
            r0.zzm(r10, r11)
        L_0x003d:
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 16
            if (r0 != r2) goto L_0x00b6
            int r0 = r9.zzebf
            if (r0 <= 0) goto L_0x0049
            if (r0 != r10) goto L_0x004f
        L_0x0049:
            int r0 = r9.zzebg
            if (r0 <= 0) goto L_0x00b2
            if (r0 == r11) goto L_0x00b2
        L_0x004f:
            boolean r0 = r9.zzeax
            if (r0 == 0) goto L_0x00b2
            boolean r0 = r9.zzyv()
            if (r0 == 0) goto L_0x00b2
            com.google.android.gms.internal.ads.zzge r0 = r9.zzefe
            long r2 = r0.zzdn()
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00b2
            com.google.android.gms.internal.ads.zzge r0 = r9.zzefe
            boolean r0 = r0.zzdm()
            if (r0 != 0) goto L_0x00b2
            r0 = 1
            r9.zza(r1, r0)
            com.google.android.gms.internal.ads.zzge r1 = r9.zzefe
            r1.zzd(r0)
            com.google.android.gms.internal.ads.zzge r0 = r9.zzefe
            long r0 = r0.zzdn()
            com.google.android.gms.common.util.Clock r2 = com.google.android.gms.ads.internal.zzk.zzln()
            long r2 = r2.currentTimeMillis()
        L_0x0084:
            boolean r4 = r9.zzyv()
            if (r4 == 0) goto L_0x00a3
            com.google.android.gms.internal.ads.zzge r4 = r9.zzefe
            long r4 = r4.zzdn()
            int r6 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r6 != 0) goto L_0x00a3
            com.google.android.gms.common.util.Clock r4 = com.google.android.gms.ads.internal.zzk.zzln()
            long r4 = r4.currentTimeMillis()
            long r4 = r4 - r2
            r6 = 250(0xfa, double:1.235E-321)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 <= 0) goto L_0x0084
        L_0x00a3:
            boolean r0 = r9.zzyv()
            if (r0 == 0) goto L_0x00af
            com.google.android.gms.internal.ads.zzge r0 = r9.zzefe
            r1 = 0
            r0.zzd(r1)
        L_0x00af:
            r9.zzxk()
        L_0x00b2:
            r9.zzebf = r10
            r9.zzebg = r11
        L_0x00b6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbdq.onMeasure(int, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void
     arg types: [android.view.Surface, int]
     candidates:
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, float):float
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, int):int
      com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, float):void
      com.google.android.gms.internal.ads.zzbco.zza(float, float):void
      com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void */
    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        int i3;
        if (this.zzebi) {
            this.zzebh = new zzbdd(getContext());
            this.zzebh.zza(surfaceTexture, i, i2);
            this.zzebh.start();
            SurfaceTexture zzxy = this.zzebh.zzxy();
            if (zzxy != null) {
                surfaceTexture = zzxy;
            } else {
                this.zzebh.zzxx();
                this.zzebh = null;
            }
        }
        this.zzaez = new Surface(surfaceTexture);
        if (this.zzefe == null) {
            zzyx();
        } else {
            zza(this.zzaez, true);
            if (!this.zzefp.zzeec) {
                zzyz();
            }
        }
        float f = 1.0f;
        int i4 = this.zzebc;
        if (!(i4 == 0 || (i3 = this.zzebd) == 0)) {
            f = this.zzaft;
            i = i4;
            i2 = i3;
        }
        zzb(i, i2, f);
        zzaxi.zzdvv.post(new zzbeb(this));
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        zzbdd zzbdd = this.zzebh;
        if (zzbdd != null) {
            zzbdd.zzm(i, i2);
        }
        zzaxi.zzdvv.post(new zzbec(this, i, i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, float):float
      com.google.android.gms.internal.ads.zzbdq.zza(com.google.android.gms.internal.ads.zzbdq, int):int
      com.google.android.gms.internal.ads.zzbdq.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbdq.zza(float, float):void
      com.google.android.gms.internal.ads.zzbco.zza(float, float):void
      com.google.android.gms.internal.ads.zzbdq.zza(android.view.Surface, boolean):void */
    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        zzawz.zzds("Surface destroyed");
        pause();
        zzbdd zzbdd = this.zzebh;
        if (zzbdd != null) {
            zzbdd.zzxx();
            this.zzebh = null;
        }
        if (this.zzefe != null) {
            zzza();
            Surface surface = this.zzaez;
            if (surface != null) {
                surface.release();
            }
            this.zzaez = null;
            zza((Surface) null, true);
        }
        zzaxi.zzdvv.post(new zzbed(this));
        return true;
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.zzeaw.zzc(this);
        this.zzebr.zza(surfaceTexture, this.zzebk);
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i) {
        StringBuilder sb = new StringBuilder(57);
        sb.append("AdExoPlayerView1 window visibility changed to ");
        sb.append(i);
        zzawz.zzds(sb.toString());
        zzaxi.zzdvv.post(new zzbdt(this, i));
        super.onWindowVisibilityChanged(i);
    }

    private final void zzyz() {
        zzge zzge = this.zzefe;
        if (zzge != null) {
            zzge.zzc(0, true);
        }
    }

    private final void zzza() {
        zzge zzge = this.zzefe;
        if (zzge != null) {
            zzge.zzc(0, false);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzdd(int i) {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.onWindowVisibilityChanged(i);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzb() {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.zzxo();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzn(int i, int i2) {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.zzk(i, i2);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzc() {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.zzxl();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzd() {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.onPaused();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzze() {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.zzxm();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzo(String str, String str2) {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.zzl(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzf() {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.zzxn();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzg() {
        zzbcn zzbcn = this.zzebk;
        if (zzbcn != null) {
            zzbcn.zzhd();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(boolean z, long j) {
        this.zzebt.zza(z, j);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(boolean z, long j) {
        this.zzebt.zza(z, j);
    }
}
