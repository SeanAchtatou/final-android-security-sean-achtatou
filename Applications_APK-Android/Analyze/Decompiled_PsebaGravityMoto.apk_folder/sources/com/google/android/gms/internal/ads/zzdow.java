package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class zzdow extends zzdou {
    private static final Class<?> zzhjc = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private zzdow() {
        super();
    }

    /* access modifiers changed from: package-private */
    public final <L> List<L> zza(Object obj, long j) {
        return zza(obj, j, 10);
    }

    /* access modifiers changed from: package-private */
    public final void zzb(Object obj, long j) {
        Object obj2;
        List list = (List) zzdqz.zzp(obj, j);
        if (list instanceof zzdot) {
            obj2 = ((zzdot) list).zzayp();
        } else if (!zzhjc.isAssignableFrom(list.getClass())) {
            if (!(list instanceof zzdpw) || !(list instanceof zzdoj)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                zzdoj zzdoj = (zzdoj) list;
                if (zzdoj.zzavi()) {
                    zzdoj.zzavj();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        zzdqz.zza(obj, j, obj2);
    }

    private static <L> List<L> zza(Object obj, long j, int i) {
        List<L> zzdos;
        List<L> list;
        List<L> zzc = zzc(obj, j);
        if (zzc.isEmpty()) {
            if (zzc instanceof zzdot) {
                list = new zzdos(i);
            } else if (!(zzc instanceof zzdpw) || !(zzc instanceof zzdoj)) {
                list = new ArrayList<>(i);
            } else {
                list = ((zzdoj) zzc).zzfl(i);
            }
            zzdqz.zza(obj, j, list);
            return list;
        }
        if (zzhjc.isAssignableFrom(zzc.getClass())) {
            zzdos = new ArrayList<>(zzc.size() + i);
            zzdos.addAll(zzc);
            zzdqz.zza(obj, j, zzdos);
        } else if (zzc instanceof zzdqw) {
            zzdos = new zzdos(zzc.size() + i);
            zzdos.addAll((zzdqw) zzc);
            zzdqz.zza(obj, j, zzdos);
        } else if (!(zzc instanceof zzdpw) || !(zzc instanceof zzdoj)) {
            return zzc;
        } else {
            zzdoj zzdoj = (zzdoj) zzc;
            if (zzdoj.zzavi()) {
                return zzc;
            }
            zzdoj zzfl = zzdoj.zzfl(zzc.size() + i);
            zzdqz.zza(obj, j, zzfl);
            return zzfl;
        }
        return zzdos;
    }

    /* access modifiers changed from: package-private */
    public final <E> void zza(Object obj, Object obj2, long j) {
        List zzc = zzc(obj2, j);
        List zza = zza(obj, j, zzc.size());
        int size = zza.size();
        int size2 = zzc.size();
        if (size > 0 && size2 > 0) {
            zza.addAll(zzc);
        }
        if (size > 0) {
            zzc = zza;
        }
        zzdqz.zza(obj, j, zzc);
    }

    private static <E> List<E> zzc(Object obj, long j) {
        return (List) zzdqz.zzp(obj, j);
    }
}
