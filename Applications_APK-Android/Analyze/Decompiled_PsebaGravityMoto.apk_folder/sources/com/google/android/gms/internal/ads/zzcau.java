package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.common.internal.ImagesContract;
import com.google.android.gms.games.GamesStatusCodes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzcau {
    private final zzbai zzbtc;
    private final zzady zzdgs;
    private final zzwj zzejd;
    private final zzdh zzeko;
    private final Executor zzffi;
    private final ScheduledExecutorService zzfiw;
    private final zzcan zzfrl;
    private final zza zzfrm;
    private final zzcbi zzfrn;
    private final Context zzlj;

    public zzcau(Context context, zzcan zzcan, zzdh zzdh, zzbai zzbai, zza zza, zzwj zzwj, Executor executor, zzcxv zzcxv, zzcbi zzcbi, ScheduledExecutorService scheduledExecutorService) {
        this.zzlj = context;
        this.zzfrl = zzcan;
        this.zzeko = zzdh;
        this.zzbtc = zzbai;
        this.zzfrm = zza;
        this.zzejd = zzwj;
        this.zzffi = executor;
        this.zzdgs = zzcxv.zzdgs;
        this.zzfrn = zzcbi;
        this.zzfiw = scheduledExecutorService;
    }

    public static List<zzabj> zzi(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("mute");
        if (optJSONObject == null) {
            return Collections.emptyList();
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("reasons");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < optJSONArray.length(); i++) {
            zzabj zzk = zzk(optJSONArray.optJSONObject(i));
            if (zzk != null) {
                arrayList.add(zzk);
            }
        }
        return arrayList;
    }

    @Nullable
    public static zzabj zzj(JSONObject jSONObject) {
        JSONObject optJSONObject;
        JSONObject optJSONObject2 = jSONObject.optJSONObject("mute");
        if (optJSONObject2 == null || (optJSONObject = optJSONObject2.optJSONObject("default_reason")) == null) {
            return null;
        }
        return zzk(optJSONObject);
    }

    @Nullable
    private static zzabj zzk(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("reason");
        String optString2 = jSONObject.optString("ping_url");
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2)) {
            return null;
        }
        return new zzabj(optString, optString2);
    }

    public final zzbbh<zzadw> zzc(JSONObject jSONObject, String str) {
        return zza(jSONObject.optJSONObject(str), this.zzdgs.zzcym);
    }

    public final zzbbh<List<zzadw>> zzd(JSONObject jSONObject, String str) {
        return zza(jSONObject.optJSONArray(str), this.zzdgs.zzcym, this.zzdgs.zzbqe);
    }

    private final zzbbh<List<zzadw>> zza(@Nullable JSONArray jSONArray, boolean z, boolean z2) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return zzbar.zzm(Collections.emptyList());
        }
        ArrayList arrayList = new ArrayList();
        int length = z2 ? jSONArray.length() : 1;
        for (int i = 0; i < length; i++) {
            arrayList.add(zza(jSONArray.optJSONObject(i), z));
        }
        return zzbar.zza(zzbar.zze(arrayList), zzcav.zzdrn, this.zzffi);
    }

    private final zzbbh<zzadw> zza(@Nullable JSONObject jSONObject, boolean z) {
        if (jSONObject == null) {
            return zzbar.zzm(null);
        }
        String optString = jSONObject.optString(ImagesContract.URL);
        if (TextUtils.isEmpty(optString)) {
            return zzbar.zzm(null);
        }
        double optDouble = jSONObject.optDouble("scale", 1.0d);
        boolean optBoolean = jSONObject.optBoolean("is_transparent", true);
        int optInt = jSONObject.optInt("width", -1);
        int optInt2 = jSONObject.optInt("height", -1);
        if (z) {
            return zzbar.zzm(new zzadw(null, Uri.parse(optString), optDouble, optInt, optInt2));
        }
        return zza(jSONObject.optBoolean("require"), zzbar.zza(this.zzfrl.zza(optString, optDouble, optBoolean), new zzcaw(optString, optDouble, optInt, optInt2), this.zzffi), (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcau.zza(org.json.JSONArray, boolean, boolean):com.google.android.gms.internal.ads.zzbbh<java.util.List<com.google.android.gms.internal.ads.zzadw>>
     arg types: [org.json.JSONArray, int, int]
     candidates:
      com.google.android.gms.internal.ads.zzcau.zza(boolean, com.google.android.gms.internal.ads.zzbbh, java.lang.Object):com.google.android.gms.internal.ads.zzbbh<T>
      com.google.android.gms.internal.ads.zzcau.zza(org.json.JSONArray, boolean, boolean):com.google.android.gms.internal.ads.zzbbh<java.util.List<com.google.android.gms.internal.ads.zzadw>> */
    public final zzbbh<zzadt> zze(JSONObject jSONObject, String str) {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return zzbar.zzm(null);
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("images");
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("image");
        if (optJSONArray == null && optJSONObject2 != null) {
            optJSONArray = new JSONArray();
            optJSONArray.put(optJSONObject2);
        }
        return zza(optJSONObject.optBoolean("require"), zzbar.zza(zza(optJSONArray, false, true), new zzcax(this, optJSONObject), this.zzffi), (Object) null);
    }

    private static Integer zzf(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt("r"), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException unused) {
            return null;
        }
    }

    public final zzbbh<zzbgz> zzl(JSONObject jSONObject) {
        JSONObject zza = zzazc.zza(jSONObject, "html_containers", "instream");
        if (zza == null) {
            JSONObject optJSONObject = jSONObject.optJSONObject("video");
            if (optJSONObject == null) {
                return zzbar.zzm(null);
            }
            if (TextUtils.isEmpty(optJSONObject.optString("vast_xml"))) {
                zzawz.zzep("Required field 'vast_xml' is missing");
                return zzbar.zzm(null);
            }
            return zza(zzbar.zza(this.zzfrn.zzm(optJSONObject), (long) ((Integer) zzyt.zzpe().zzd(zzacu.zzcse)).intValue(), TimeUnit.SECONDS, this.zzfiw), (Object) null);
        }
        return zza(zza.optBoolean("require"), this.zzfrn.zzq(zza.optString("base_url"), zza.optString("html")), (Object) null);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.android.gms.internal.ads.zzbbh<T>, com.google.android.gms.internal.ads.zzbbh] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> com.google.android.gms.internal.ads.zzbbh<T> zza(com.google.android.gms.internal.ads.zzbbh<T> r2, T r3) {
        /*
            java.lang.Class<java.lang.Exception> r3 = java.lang.Exception.class
            com.google.android.gms.internal.ads.zzcaz r0 = new com.google.android.gms.internal.ads.zzcaz
            r1 = 0
            r0.<init>(r1)
            java.util.concurrent.Executor r1 = com.google.android.gms.internal.ads.zzbbm.zzeaf
            com.google.android.gms.internal.ads.zzbbh r2 = com.google.android.gms.internal.ads.zzbar.zza(r2, r3, r0, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcau.zza(com.google.android.gms.internal.ads.zzbbh, java.lang.Object):com.google.android.gms.internal.ads.zzbbh");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.google.android.gms.internal.ads.zzbbh<T>, com.google.android.gms.internal.ads.zzbbh] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> com.google.android.gms.internal.ads.zzbbh<T> zza(boolean r0, com.google.android.gms.internal.ads.zzbbh<T> r1, T r2) {
        /*
            if (r0 == 0) goto L_0x000e
            com.google.android.gms.internal.ads.zzcba r0 = new com.google.android.gms.internal.ads.zzcba
            r0.<init>(r1)
            java.util.concurrent.Executor r2 = com.google.android.gms.internal.ads.zzbbm.zzeaf
            com.google.android.gms.internal.ads.zzbbh r0 = com.google.android.gms.internal.ads.zzbar.zza(r1, r0, r2)
            return r0
        L_0x000e:
            r0 = 0
            com.google.android.gms.internal.ads.zzbbh r0 = zza(r1, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcau.zza(boolean, com.google.android.gms.internal.ads.zzbbh, java.lang.Object):com.google.android.gms.internal.ads.zzbbh");
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbbh zzb(String str, Object obj) throws Exception {
        zzk.zzlh();
        zzbgz zza = zzbhf.zza(this.zzlj, zzbin.zzabu(), "native-omid", false, false, this.zzeko, this.zzbtc, null, null, this.zzfrm, this.zzejd);
        zzbbq zzn = zzbbq.zzn(zza);
        zza.zzaai().zza(new zzcbb(zzn));
        zza.loadData(str, "text/html", HTTP.UTF_8);
        return zzn;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzadt zza(JSONObject jSONObject, List list) {
        Integer num = null;
        if (list == null || list.isEmpty()) {
            return null;
        }
        String optString = jSONObject.optString("text");
        Integer zzf = zzf(jSONObject, "bg_color");
        Integer zzf2 = zzf(jSONObject, "text_color");
        int optInt = jSONObject.optInt("text_size", -1);
        boolean optBoolean = jSONObject.optBoolean("allow_pub_rendering");
        int optInt2 = jSONObject.optInt("animation_ms", 1000);
        int optInt3 = jSONObject.optInt("presentation_ms", GamesStatusCodes.STATUS_SNAPSHOT_NOT_FOUND);
        if (optInt > 0) {
            num = Integer.valueOf(optInt);
        }
        return new zzadt(optString, list, zzf, zzf2, num, optInt3 + optInt2, this.zzdgs.zzbqf, optBoolean);
    }
}
