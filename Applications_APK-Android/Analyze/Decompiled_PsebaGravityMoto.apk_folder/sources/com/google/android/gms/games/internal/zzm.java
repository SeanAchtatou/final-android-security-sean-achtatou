package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.tasks.TaskCompletionSource;

final /* synthetic */ class zzm implements PendingResult.StatusListener {
    private final TaskCompletionSource zzip;
    private final PendingResultUtil.ResultConverter zziw;
    private final PendingResult zzix;

    zzm(PendingResultUtil.ResultConverter resultConverter, PendingResult pendingResult, TaskCompletionSource taskCompletionSource) {
        this.zziw = resultConverter;
        this.zzix = pendingResult;
        this.zzip = taskCompletionSource;
    }

    public final void onComplete(Status status) {
        zzi.zza(this.zziw, this.zzix, this.zzip, status);
    }
}
