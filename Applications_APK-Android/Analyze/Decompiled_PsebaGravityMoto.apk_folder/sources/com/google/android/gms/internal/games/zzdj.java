package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdj extends zzdq {
    private final /* synthetic */ String zzew;
    private final /* synthetic */ String zzkk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdj(zzdb zzdb, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient, null);
        this.zzew = str;
        this.zzkk = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzew, this.zzkk);
    }
}
