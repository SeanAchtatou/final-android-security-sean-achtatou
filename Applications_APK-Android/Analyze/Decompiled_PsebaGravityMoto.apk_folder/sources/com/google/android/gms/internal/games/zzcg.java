package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.request.Requests;

abstract class zzcg extends Games.zza<Requests.UpdateRequestsResult> {
    private zzcg(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzcg(GoogleApiClient googleApiClient, zzcb zzcb) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzch(this, status);
    }
}
