package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdd extends zzds {
    private final /* synthetic */ String zzew;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdd(zzdb zzdb, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzew = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzg(this, this.zzew);
    }
}
