package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievements;

abstract class zzp extends Games.zza<Achievements.LoadAchievementsResult> {
    private zzp(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzp(GoogleApiClient googleApiClient, zzg zzg) {
        this(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzq(this, status);
    }
}
