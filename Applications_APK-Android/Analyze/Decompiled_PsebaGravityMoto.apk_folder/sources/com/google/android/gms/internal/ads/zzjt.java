package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.games.GamesStatusCodes;
import java.io.IOException;
import org.apache.http.HttpHost;

public final class zzjt implements zzkf {
    private final zzkf zzapz;
    private final zzkf zzaqa;
    private final zzkf zzaqb;
    private zzkf zzaqc;

    public zzjt(Context context, String str) {
        this(context, null, str, false);
    }

    private zzjt(Context context, zzke zzke, String str, boolean z) {
        this(context, null, new zzjs(str, null, null, GamesStatusCodes.STATUS_MILESTONE_CLAIMED_PREVIOUSLY, GamesStatusCodes.STATUS_MILESTONE_CLAIMED_PREVIOUSLY, false));
    }

    private zzjt(Context context, zzke zzke, zzkf zzkf) {
        this.zzapz = (zzkf) zzkh.checkNotNull(zzkf);
        this.zzaqa = new zzjv(null);
        this.zzaqb = new zzjm(context, null);
    }

    public final long zza(zzjq zzjq) throws IOException {
        zzkh.checkState(this.zzaqc == null);
        String scheme = zzjq.uri.getScheme();
        if (HttpHost.DEFAULT_SCHEME_NAME.equals(scheme) || "https".equals(scheme)) {
            this.zzaqc = this.zzapz;
        } else if ("file".equals(scheme)) {
            if (zzjq.uri.getPath().startsWith("/android_asset/")) {
                this.zzaqc = this.zzaqb;
            } else {
                this.zzaqc = this.zzaqa;
            }
        } else if ("asset".equals(scheme)) {
            this.zzaqc = this.zzaqb;
        } else {
            throw new zzju(scheme);
        }
        return this.zzaqc.zza(zzjq);
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        return this.zzaqc.read(bArr, i, i2);
    }

    public final void close() throws IOException {
        zzkf zzkf = this.zzaqc;
        if (zzkf != null) {
            try {
                zzkf.close();
            } finally {
                this.zzaqc = null;
            }
        }
    }
}
