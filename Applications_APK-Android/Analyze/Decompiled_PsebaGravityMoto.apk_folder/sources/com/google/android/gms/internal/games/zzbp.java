package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzbp extends zzbt {
    private final /* synthetic */ String zzjt;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbp(zzbo zzbo, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzjt = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzh(this, this.zzjt);
    }
}
