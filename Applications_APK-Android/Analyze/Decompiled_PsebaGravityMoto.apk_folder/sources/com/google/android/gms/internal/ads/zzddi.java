package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

final class zzddi {
    public static void zza(zzdft zzdft) throws GeneralSecurityException {
        zzdkr.zza(zza(zzdft.zzaqp().zzara()));
        zza(zzdft.zzaqp().zzaoo());
        if (zzdft.zzaqr() != zzdfd.UNKNOWN_FORMAT) {
            zzdcf.zza(zzdft.zzaqq().zzaqk());
            return;
        }
        throw new GeneralSecurityException("unknown EC point format");
    }

    public static String zza(zzdgj zzdgj) throws NoSuchAlgorithmException {
        int i = zzddj.zzgqb[zzdgj.ordinal()];
        if (i == 1) {
            return "HmacSha1";
        }
        if (i == 2) {
            return "HmacSha256";
        }
        if (i == 3) {
            return "HmacSha512";
        }
        String valueOf = String.valueOf(zzdgj);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
        sb.append("hash unsupported for HMAC: ");
        sb.append(valueOf);
        throw new NoSuchAlgorithmException(sb.toString());
    }

    public static zzdkt zza(zzdgf zzdgf) throws GeneralSecurityException {
        int i = zzddj.zzgqc[zzdgf.ordinal()];
        if (i == 1) {
            return zzdkt.NIST_P256;
        }
        if (i == 2) {
            return zzdkt.NIST_P384;
        }
        if (i == 3) {
            return zzdkt.NIST_P521;
        }
        String valueOf = String.valueOf(zzdgf);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20);
        sb.append("unknown curve type: ");
        sb.append(valueOf);
        throw new GeneralSecurityException(sb.toString());
    }

    public static zzdkv zza(zzdfd zzdfd) throws GeneralSecurityException {
        int i = zzddj.zzgqd[zzdfd.ordinal()];
        if (i == 1) {
            return zzdkv.UNCOMPRESSED;
        }
        if (i == 2) {
            return zzdkv.DO_NOT_USE_CRUNCHY_UNCOMPRESSED;
        }
        if (i == 3) {
            return zzdkv.COMPRESSED;
        }
        String valueOf = String.valueOf(zzdfd);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("unknown point format: ");
        sb.append(valueOf);
        throw new GeneralSecurityException(sb.toString());
    }
}
