package com.google.android.gms.ads.impl;

public final class R {
    private R() {
    }

    public static final class string {
        public static final int s1 = 2131361869;
        public static final int s2 = 2131361870;
        public static final int s3 = 2131361871;
        public static final int s4 = 2131361872;
        public static final int s5 = 2131361873;
        public static final int s6 = 2131361874;
        public static final int s7 = 2131361875;

        private string() {
        }
    }
}
