package com.google.android.gms.internal.ads;

public enum zzdrn {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(zzdmr.zzhcr),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzhiq;

    private zzdrn(Object obj) {
        this.zzhiq = obj;
    }
}
