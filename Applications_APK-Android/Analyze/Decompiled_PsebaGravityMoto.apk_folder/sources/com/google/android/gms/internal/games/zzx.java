package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzx extends zzz {
    private final /* synthetic */ boolean zzjg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzx(zzv zzv, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient, null);
        this.zzjg = z;
    }

    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzd(this, this.zzjg);
    }
}
