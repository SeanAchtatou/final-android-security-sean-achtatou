package com.google.android.gms.internal.ads;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class zzdqe<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zzheh;
    private final int zzhkr;
    /* access modifiers changed from: private */
    public List<zzdql> zzhks;
    /* access modifiers changed from: private */
    public Map<K, V> zzhkt;
    private volatile zzdqn zzhku;
    /* access modifiers changed from: private */
    public Map<K, V> zzhkv;
    private volatile zzdqh zzhkw;

    static <FieldDescriptorType extends zzdnu<FieldDescriptorType>> zzdqe<FieldDescriptorType, Object> zzgy(int i) {
        return new zzdqf(i);
    }

    private zzdqe(int i) {
        this.zzhkr = i;
        this.zzhks = Collections.emptyList();
        this.zzhkt = Collections.emptyMap();
        this.zzhkv = Collections.emptyMap();
    }

    public void zzavj() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.zzheh) {
            if (this.zzhkt.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.zzhkt);
            }
            this.zzhkt = map;
            if (this.zzhkv.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.zzhkv);
            }
            this.zzhkv = map2;
            this.zzheh = true;
        }
    }

    public final boolean isImmutable() {
        return this.zzheh;
    }

    public final int zzazp() {
        return this.zzhks.size();
    }

    public final Map.Entry<K, V> zzgz(int i) {
        return this.zzhks.get(i);
    }

    public final Iterable<Map.Entry<K, V>> zzazq() {
        if (this.zzhkt.isEmpty()) {
            return zzdqi.zzazv();
        }
        return this.zzhkt.entrySet();
    }

    public int size() {
        return this.zzhks.size() + this.zzhkt.size();
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzhkt.containsKey(comparable);
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzhks.get(zza).getValue();
        }
        return this.zzhkt.get(comparable);
    }

    /* renamed from: zza */
    public final V put(Comparable comparable, Object obj) {
        zzazs();
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzhks.get(zza).setValue(obj);
        }
        zzazs();
        if (this.zzhks.isEmpty() && !(this.zzhks instanceof ArrayList)) {
            this.zzhks = new ArrayList(this.zzhkr);
        }
        int i = -(zza + 1);
        if (i >= this.zzhkr) {
            return zzazt().put(comparable, obj);
        }
        int size = this.zzhks.size();
        int i2 = this.zzhkr;
        if (size == i2) {
            zzdql remove = this.zzhks.remove(i2 - 1);
            zzazt().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.zzhks.add(i, new zzdql(this, comparable, obj));
        return null;
    }

    public void clear() {
        zzazs();
        if (!this.zzhks.isEmpty()) {
            this.zzhks.clear();
        }
        if (!this.zzhkt.isEmpty()) {
            this.zzhkt.clear();
        }
    }

    public V remove(Object obj) {
        zzazs();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return zzha(zza);
        }
        if (this.zzhkt.isEmpty()) {
            return null;
        }
        return this.zzhkt.remove(comparable);
    }

    /* access modifiers changed from: private */
    public final V zzha(int i) {
        zzazs();
        V value = this.zzhks.remove(i).getValue();
        if (!this.zzhkt.isEmpty()) {
            Iterator it = zzazt().entrySet().iterator();
            this.zzhks.add(new zzdql(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final int zza(K r5) {
        /*
            r4 = this;
            java.util.List<com.google.android.gms.internal.ads.zzdql> r0 = r4.zzhks
            int r0 = r0.size()
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0025
            java.util.List<com.google.android.gms.internal.ads.zzdql> r1 = r4.zzhks
            java.lang.Object r1 = r1.get(r0)
            com.google.android.gms.internal.ads.zzdql r1 = (com.google.android.gms.internal.ads.zzdql) r1
            java.lang.Object r1 = r1.getKey()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r1 = r5.compareTo(r1)
            if (r1 <= 0) goto L_0x0022
            int r0 = r0 + 2
            int r5 = -r0
            return r5
        L_0x0022:
            if (r1 != 0) goto L_0x0025
            return r0
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 > r0) goto L_0x0049
            int r2 = r1 + r0
            int r2 = r2 / 2
            java.util.List<com.google.android.gms.internal.ads.zzdql> r3 = r4.zzhks
            java.lang.Object r3 = r3.get(r2)
            com.google.android.gms.internal.ads.zzdql r3 = (com.google.android.gms.internal.ads.zzdql) r3
            java.lang.Object r3 = r3.getKey()
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r3 = r5.compareTo(r3)
            if (r3 >= 0) goto L_0x0043
            int r0 = r2 + -1
            goto L_0x0026
        L_0x0043:
            if (r3 <= 0) goto L_0x0048
            int r1 = r2 + 1
            goto L_0x0026
        L_0x0048:
            return r2
        L_0x0049:
            int r1 = r1 + 1
            int r5 = -r1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqe.zza(java.lang.Comparable):int");
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzhku == null) {
            this.zzhku = new zzdqn(this, null);
        }
        return this.zzhku;
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> zzazr() {
        if (this.zzhkw == null) {
            this.zzhkw = new zzdqh(this, null);
        }
        return this.zzhkw;
    }

    /* access modifiers changed from: private */
    public final void zzazs() {
        if (this.zzheh) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzazt() {
        zzazs();
        if (this.zzhkt.isEmpty() && !(this.zzhkt instanceof TreeMap)) {
            this.zzhkt = new TreeMap();
            this.zzhkv = ((TreeMap) this.zzhkt).descendingMap();
        }
        return (SortedMap) this.zzhkt;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdqe)) {
            return super.equals(obj);
        }
        zzdqe zzdqe = (zzdqe) obj;
        int size = size();
        if (size != zzdqe.size()) {
            return false;
        }
        int zzazp = zzazp();
        if (zzazp != zzdqe.zzazp()) {
            return entrySet().equals(zzdqe.entrySet());
        }
        for (int i = 0; i < zzazp; i++) {
            if (!zzgz(i).equals(zzdqe.zzgz(i))) {
                return false;
            }
        }
        if (zzazp != size) {
            return this.zzhkt.equals(zzdqe.zzhkt);
        }
        return true;
    }

    public int hashCode() {
        int zzazp = zzazp();
        int i = 0;
        for (int i2 = 0; i2 < zzazp; i2++) {
            i += this.zzhks.get(i2).hashCode();
        }
        return this.zzhkt.size() > 0 ? i + this.zzhkt.hashCode() : i;
    }

    /* synthetic */ zzdqe(int i, zzdqf zzdqf) {
        this(i);
    }
}
