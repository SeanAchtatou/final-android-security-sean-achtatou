package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzwt;
import java.io.IOException;

public final class zzxj extends zzdrr<zzxj> {
    public Integer zzcee = null;
    private zzwx zzcef = null;
    private zzwt.zzb zzceg = null;
    public zzxk zzceh = null;
    private zzwt.zza[] zzcei = new zzwt.zza[0];
    private zzwt.zzc zzcej = null;
    private zzwt.zzj zzcek = null;
    private zzwt.zzh zzcel = null;
    private zzwt.zze zzcem = null;
    private zzwt.zzf zzcen = null;
    private zzxp[] zzceo = zzxp.zzos();

    public zzxj() {
        this.zzhno = null;
        this.zzhnx = -1;
    }

    public final void zza(zzdrp zzdrp) throws IOException {
        Integer num = this.zzcee;
        if (num != null) {
            zzdrp.zzx(7, num.intValue());
        }
        zzxk zzxk = this.zzceh;
        if (zzxk != null) {
            zzdrp.zza(10, zzxk);
        }
        zzwt.zza[] zzaArr = this.zzcei;
        int i = 0;
        if (zzaArr != null && zzaArr.length > 0) {
            int i2 = 0;
            while (true) {
                zzwt.zza[] zzaArr2 = this.zzcei;
                if (i2 >= zzaArr2.length) {
                    break;
                }
                zzwt.zza zza = zzaArr2[i2];
                if (zza != null) {
                    zzdrp.zze(11, zza);
                }
                i2++;
            }
        }
        zzxp[] zzxpArr = this.zzceo;
        if (zzxpArr != null && zzxpArr.length > 0) {
            while (true) {
                zzxp[] zzxpArr2 = this.zzceo;
                if (i >= zzxpArr2.length) {
                    break;
                }
                zzxp zzxp = zzxpArr2[i];
                if (zzxp != null) {
                    zzdrp.zza(17, zzxp);
                }
                i++;
            }
        }
        super.zza(zzdrp);
    }

    /* access modifiers changed from: protected */
    public final int zzor() {
        int zzor = super.zzor();
        Integer num = this.zzcee;
        if (num != null) {
            zzor += zzdrp.zzab(7, num.intValue());
        }
        zzxk zzxk = this.zzceh;
        if (zzxk != null) {
            zzor += zzdrp.zzb(10, zzxk);
        }
        zzwt.zza[] zzaArr = this.zzcei;
        int i = 0;
        if (zzaArr != null && zzaArr.length > 0) {
            int i2 = zzor;
            int i3 = 0;
            while (true) {
                zzwt.zza[] zzaArr2 = this.zzcei;
                if (i3 >= zzaArr2.length) {
                    break;
                }
                zzwt.zza zza = zzaArr2[i3];
                if (zza != null) {
                    i2 += zzdni.zzc(11, zza);
                }
                i3++;
            }
            zzor = i2;
        }
        zzxp[] zzxpArr = this.zzceo;
        if (zzxpArr != null && zzxpArr.length > 0) {
            while (true) {
                zzxp[] zzxpArr2 = this.zzceo;
                if (i >= zzxpArr2.length) {
                    break;
                }
                zzxp zzxp = zzxpArr2[i];
                if (zzxp != null) {
                    zzor += zzdrp.zzb(17, zzxp);
                }
                i++;
            }
        }
        return zzor;
    }
}
