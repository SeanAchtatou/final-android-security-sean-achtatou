package com.google.android.gms.internal.drive;

import java.io.IOException;

public abstract class zzix {
    protected volatile int zznf = -1;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends com.google.android.gms.internal.drive.zzix> T zza(T r0, byte[] r1, int r2, int r3) throws com.google.android.gms.internal.drive.zziw {
        /*
            r2 = 0
            com.google.android.gms.internal.drive.zzio r1 = com.google.android.gms.internal.drive.zzio.zza(r1, r2, r3)     // Catch:{ zziw -> 0x0015, IOException -> 0x000c }
            r0.zza(r1)     // Catch:{ zziw -> 0x0015, IOException -> 0x000c }
            r1.zzj(r2)     // Catch:{ zziw -> 0x0015, IOException -> 0x000c }
            return r0
        L_0x000c:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "Reading from a byte array threw an IOException (should never happen)."
            r1.<init>(r2, r0)
            throw r1
        L_0x0015:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.drive.zzix.zza(com.google.android.gms.internal.drive.zzix, byte[], int, int):com.google.android.gms.internal.drive.zzix");
    }

    public static final byte[] zza(zzix zzix) {
        int zzaq = zzix.zzaq();
        zzix.zznf = zzaq;
        byte[] bArr = new byte[zzaq];
        try {
            zzip zzb = zzip.zzb(bArr, 0, bArr.length);
            zzix.zza(zzb);
            zzb.zzbh();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public String toString() {
        return zziy.zzb(this);
    }

    public abstract zzix zza(zzio zzio) throws IOException;

    public void zza(zzip zzip) throws IOException {
    }

    /* access modifiers changed from: protected */
    public int zzaq() {
        return 0;
    }

    /* renamed from: zzbi */
    public zzix clone() throws CloneNotSupportedException {
        return (zzix) super.clone();
    }
}
