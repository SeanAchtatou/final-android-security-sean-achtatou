package com.google.android.gms.games.leaderboard;

import android.os.Bundle;

public final class zza {
    private final Bundle zznb;

    public zza(Bundle bundle) {
        this.zznb = bundle == null ? new Bundle() : bundle;
    }

    public final Bundle zzcc() {
        return this.zznb;
    }
}
