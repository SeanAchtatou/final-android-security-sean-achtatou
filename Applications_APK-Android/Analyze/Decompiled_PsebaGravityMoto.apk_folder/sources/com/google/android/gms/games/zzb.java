package com.google.android.gms.games;

import androidx.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;

final class zzb implements PendingResultUtil.ResultConverter<Achievements.LoadAchievementsResult, AchievementBuffer> {
    zzb() {
    }

    public final /* synthetic */ Object convert(@Nullable Result result) {
        Achievements.LoadAchievementsResult loadAchievementsResult = (Achievements.LoadAchievementsResult) result;
        if (loadAchievementsResult == null) {
            return null;
        }
        return loadAchievementsResult.getAchievements();
    }
}
