package com.google.ads.consent;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.common.internal.ImagesContract;
import com.google.gson.Gson;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.HashMap;

public class ConsentForm {
    private final boolean adFreeOption;
    private final URL appPrivacyPolicyURL;
    private final Context context;
    private final Dialog dialog;
    /* access modifiers changed from: private */
    public final ConsentFormListener listener;
    /* access modifiers changed from: private */
    public LoadState loadState;
    private final boolean nonPersonalizedAdsOption;
    private final boolean personalizedAdsOption;
    private final WebView webView;

    private enum LoadState {
        NOT_READY,
        LOADING,
        LOADED
    }

    private ConsentForm(Builder builder) {
        this.context = builder.context;
        if (builder.listener == null) {
            this.listener = new ConsentFormListener(this) {
            };
        } else {
            this.listener = builder.listener;
        }
        this.personalizedAdsOption = builder.personalizedAdsOption;
        this.nonPersonalizedAdsOption = builder.nonPersonalizedAdsOption;
        this.adFreeOption = builder.adFreeOption;
        this.appPrivacyPolicyURL = builder.appPrivacyPolicyURL;
        this.dialog = new Dialog(this.context);
        this.loadState = LoadState.NOT_READY;
        this.webView = new WebView(this.context);
        this.dialog.setContentView(this.webView);
        this.dialog.setCancelable(false);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new WebViewClient() {
            boolean isInternalRedirect;

            private boolean isConsentFormUrl(String str) {
                return !TextUtils.isEmpty(str) && str.startsWith("consent://");
            }

            private void handleUrl(String str) {
                if (isConsentFormUrl(str)) {
                    this.isInternalRedirect = true;
                    Uri parse = Uri.parse(str);
                    String queryParameter = parse.getQueryParameter("action");
                    String queryParameter2 = parse.getQueryParameter("status");
                    String queryParameter3 = parse.getQueryParameter(ImagesContract.URL);
                    char c = 65535;
                    int hashCode = queryParameter.hashCode();
                    if (hashCode != -1370505102) {
                        if (hashCode != 150940456) {
                            if (hashCode == 1671672458 && queryParameter.equals("dismiss")) {
                                c = 1;
                            }
                        } else if (queryParameter.equals("browser")) {
                            c = 2;
                        }
                    } else if (queryParameter.equals("load_complete")) {
                        c = 0;
                    }
                    if (c == 0) {
                        ConsentForm.this.handleLoadComplete(queryParameter2);
                    } else if (c == 1) {
                        this.isInternalRedirect = false;
                        ConsentForm.this.handleDismiss(queryParameter2);
                    } else if (c == 2) {
                        ConsentForm.this.handleOpenBrowser(queryParameter3);
                    }
                }
            }

            public void onLoadResource(WebView webView, String str) {
                handleUrl(str);
            }

            @TargetApi(24)
            public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
                String uri = webResourceRequest.getUrl().toString();
                if (!isConsentFormUrl(uri)) {
                    return false;
                }
                handleUrl(uri);
                return true;
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (!isConsentFormUrl(str)) {
                    return false;
                }
                handleUrl(str);
                return true;
            }

            public void onPageFinished(WebView webView, String str) {
                if (!this.isInternalRedirect) {
                    ConsentForm.this.updateDialogContent(webView);
                }
                super.onPageFinished(webView, str);
            }

            public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                super.onReceivedError(webView, webResourceRequest, webResourceError);
                LoadState unused = ConsentForm.this.loadState = LoadState.NOT_READY;
                ConsentForm.this.listener.onConsentFormError(webResourceError.toString());
            }
        });
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean adFreeOption = false;
        /* access modifiers changed from: private */
        public final URL appPrivacyPolicyURL;
        /* access modifiers changed from: private */
        public final Context context;
        /* access modifiers changed from: private */
        public ConsentFormListener listener;
        /* access modifiers changed from: private */
        public boolean nonPersonalizedAdsOption = false;
        /* access modifiers changed from: private */
        public boolean personalizedAdsOption = false;

        public Builder(Context context2, URL url) {
            this.context = context2;
            this.appPrivacyPolicyURL = url;
            if (this.appPrivacyPolicyURL == null) {
                throw new IllegalArgumentException("Must provide valid app privacy policy url to create a ConsentForm");
            }
        }

        public Builder withListener(ConsentFormListener consentFormListener) {
            this.listener = consentFormListener;
            return this;
        }

        public Builder withPersonalizedAdsOption() {
            this.personalizedAdsOption = true;
            return this;
        }

        public Builder withNonPersonalizedAdsOption() {
            this.nonPersonalizedAdsOption = true;
            return this;
        }

        public Builder withAdFreeOption() {
            this.adFreeOption = true;
            return this;
        }

        public ConsentForm build() {
            return new ConsentForm(this);
        }
    }

    private static String getApplicationName(Context context2) {
        return context2.getApplicationInfo().loadLabel(context2.getPackageManager()).toString();
    }

    private static String getAppIconURIString(Context context2) {
        Drawable applicationIcon = context2.getPackageManager().getApplicationIcon(context2.getApplicationInfo());
        Bitmap createBitmap = Bitmap.createBitmap(applicationIcon.getIntrinsicWidth(), applicationIcon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        applicationIcon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        applicationIcon.draw(canvas);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        createBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        String valueOf = String.valueOf(Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0));
        return valueOf.length() != 0 ? "data:image/png;base64,".concat(valueOf) : new String("data:image/png;base64,");
    }

    private static String createJavascriptCommand(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("info", str2);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("args", hashMap);
        return String.format("javascript:%s(%s)", str, new Gson().toJson(hashMap2));
    }

    /* access modifiers changed from: private */
    public void updateDialogContent(WebView webView2) {
        HashMap hashMap = new HashMap();
        hashMap.put("app_name", getApplicationName(this.context));
        hashMap.put("app_icon", getAppIconURIString(this.context));
        hashMap.put("offer_personalized", Boolean.valueOf(this.personalizedAdsOption));
        hashMap.put("offer_non_personalized", Boolean.valueOf(this.nonPersonalizedAdsOption));
        hashMap.put("offer_ad_free", Boolean.valueOf(this.adFreeOption));
        hashMap.put("is_request_in_eea_or_unknown", Boolean.valueOf(ConsentInformation.getInstance(this.context).isRequestLocationInEeaOrUnknown()));
        hashMap.put("app_privacy_url", this.appPrivacyPolicyURL);
        ConsentData loadConsentData = ConsentInformation.getInstance(this.context).loadConsentData();
        hashMap.put("plat", loadConsentData.getSDKPlatformString());
        hashMap.put("consent_info", loadConsentData);
        webView2.loadUrl(createJavascriptCommand("setUpConsentDialog", new Gson().toJson(hashMap)));
    }

    public void load() {
        if (this.loadState == LoadState.LOADING) {
            this.listener.onConsentFormError("Cannot simultaneously load multiple consent forms.");
        } else if (this.loadState == LoadState.LOADED) {
            this.listener.onConsentFormLoaded();
        } else {
            this.loadState = LoadState.LOADING;
            this.webView.loadUrl("file:///android_asset/consentform.html");
        }
    }

    /* access modifiers changed from: private */
    public void handleLoadComplete(String str) {
        if (TextUtils.isEmpty(str)) {
            this.loadState = LoadState.NOT_READY;
            this.listener.onConsentFormError("No information");
        } else if (str.contains("Error")) {
            this.loadState = LoadState.NOT_READY;
            this.listener.onConsentFormError(str);
        } else {
            this.loadState = LoadState.LOADED;
            this.listener.onConsentFormLoaded();
        }
    }

    /* access modifiers changed from: private */
    public void handleOpenBrowser(String str) {
        if (TextUtils.isEmpty(str)) {
            this.listener.onConsentFormError("No valid URL for browser navigation.");
            return;
        }
        this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    /* access modifiers changed from: private */
    public void handleDismiss(String str) {
        ConsentStatus consentStatus;
        this.loadState = LoadState.NOT_READY;
        this.dialog.dismiss();
        if (TextUtils.isEmpty(str)) {
            this.listener.onConsentFormError("No information provided.");
        } else if (str.contains("Error")) {
            this.listener.onConsentFormError(str);
        } else {
            char c = 65535;
            int hashCode = str.hashCode();
            boolean z = false;
            if (hashCode != -1152655096) {
                if (hashCode != -258041904) {
                    if (hashCode == 1666911234 && str.equals("non_personalized")) {
                        c = 1;
                    }
                } else if (str.equals("personalized")) {
                    c = 0;
                }
            } else if (str.equals("ad_free")) {
                c = 2;
            }
            if (c == 0) {
                consentStatus = ConsentStatus.PERSONALIZED;
            } else if (c == 1) {
                consentStatus = ConsentStatus.NON_PERSONALIZED;
            } else if (c != 2) {
                consentStatus = ConsentStatus.UNKNOWN;
            } else {
                consentStatus = ConsentStatus.UNKNOWN;
                z = true;
            }
            ConsentInformation.getInstance(this.context).setConsentStatus(consentStatus, "form");
            this.listener.onConsentFormClosed(consentStatus, Boolean.valueOf(z));
        }
    }

    public void show() {
        if (this.loadState != LoadState.LOADED) {
            this.listener.onConsentFormError("Consent form is not ready to be displayed.");
        } else if (ConsentInformation.getInstance(this.context).isTaggedForUnderAgeOfConsent()) {
            this.listener.onConsentFormError("Error: tagged for under age of consent");
        } else {
            this.dialog.getWindow().setLayout(-1, -1);
            this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            this.dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                public void onShow(DialogInterface dialogInterface) {
                    ConsentForm.this.listener.onConsentFormOpened();
                }
            });
            this.dialog.show();
            if (!this.dialog.isShowing()) {
                this.listener.onConsentFormError("Consent form could not be displayed.");
            }
        }
    }

    public boolean isShowing() {
        return this.dialog.isShowing();
    }
}
