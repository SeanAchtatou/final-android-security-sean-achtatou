package com.yoyogames.runner;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Debug;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.butakov.psebay.DemoGLSurfaceView;
import com.butakov.psebay.DemoRenderer;
import com.butakov.psebay.Gamepad;
import com.butakov.psebay.IAdExt;
import com.butakov.psebay.ISocial;
import com.butakov.psebay.R;
import com.butakov.psebay.RunnerActivity;
import com.butakov.psebay.RunnerKeyboardController;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import org.ini4j.Registry;

public class RunnerJNILib {
    public static final int eOF_AchievementSendFail = 3;
    public static final int eOF_AchievementSendOK = 2;
    public static final int eOF_HighScoreSendFail = 5;
    public static final int eOF_HighScoreSendOK = 4;
    public static final int eOF_UserLoggedIn = 0;
    public static final int eOF_UserLoggedOut = 1;
    private static int mAdNum = 0;
    private static int mAdX = 0;
    private static int mAdY = 0;
    private static boolean mPlaybackStateStored = false;
    private static boolean mStoredPlaybackLoop = false;
    private static long mStoredPlaybackOffset = 0;
    private static int mStoredPlaybackPosition = 0;
    private static int mStoredPlaybackSessionId = 0;
    private static long mStoredPlaybackSize = 0;
    private static float mStoredVolume = 1.0f;
    public static Context ms_context = null;
    public static boolean ms_exitcalled = false;
    public static boolean ms_loadLibraryFailed = false;
    public static MediaPlayer ms_mp;
    public static String ms_versionName;

    public static void AdsEventPreload(String str) {
    }

    public static void AdsSetup(String str) {
    }

    public static native void BackKeyLongPressEvent();

    public static void ClearGamepads() {
    }

    public static native void CloudResultData(byte[] bArr, byte[] bArr2, int i, int i2);

    public static native void CloudResultString(String str, int i, int i2);

    public static native void CreateAsynEventWithDSMap(int i, int i2);

    public static native int CreateVersionDSMap(int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, boolean z);

    public static native void DsMapAddDouble(int i, String str, double d);

    public static native void DsMapAddString(int i, String str, String str2);

    public static native String[] ExpandCompressedFile(String str, String str2);

    public static native void GCMPushResult(String str, int i, boolean z);

    public static float[] GamepadAxesValues(int i) {
        return null;
    }

    public static float[] GamepadButtonValues(int i) {
        return null;
    }

    public static boolean GamepadConnected(int i) {
        return false;
    }

    public static String GamepadDescription(int i) {
        return "";
    }

    public static int GamepadGMLMapping(int i, int i2) {
        return -1;
    }

    public static int GamepadsCount() {
        return 0;
    }

    public static native String GetAppID(int i);

    public static native String GetSaveFileName(String str);

    public static native void HttpProgress(byte[] bArr, int i, int i2, String str, String str2, int i3);

    public static native void HttpResult(byte[] bArr, int i, int i2, String str, String str2);

    public static native void HttpResultString(String str, int i, int i2);

    public static native void InputResult(String str, int i, int i2);

    public static native void KeyEvent(int i, int i2, int i3, int i4);

    public static native void LoadPicForUserWithUrl(String str, String str2);

    public static native void LoginResult(String str, String str2, int i);

    public static native void OFNotify(int i, String str, String str2, String str3, String str4);

    public static native void OnLoginSuccess(String str, String str2, String str3, String str4, String str5, String str6, String str7);

    public static native void OnVirtualKeyboardStatus(String str, int i);

    public static native void OnVirtualKeyboardTextInserted(int[] iArr, int i);

    public static native void Pause(int i);

    public static native int Process(int i, int i2, float f, float f2, float f3, int i3, int i4, float f4);

    public static native void RenderSplash(String str, String str2, int i, int i2, int i3, int i4, int i5, int i6);

    public static native void Resume(int i);

    public static native void SetAchievementsAvailable(boolean z);

    public static native void SetKeyValue(int i, int i2, String str);

    public static native void Startup(String str, String str2, String str3, int i);

    public static native void TouchEvent(int i, int i2, float f, float f2);

    public static void analyticsEvent(String str) {
    }

    public static void analyticsEventExt(String str, String[] strArr) {
    }

    public static native void callreward(int i, int i2, String str);

    public static native boolean canFlip();

    public static native void dsListAddInt(int i, int i2);

    public static native void dsListAddString(int i, String str);

    public static native int dsListCreate();

    public static native int dsListGetSize(int i);

    public static native double dsListGetValueDouble(int i, int i2);

    public static native int dsListGetValueInt(int i, int i2);

    public static native String dsListGetValueString(int i, int i2);

    public static native void dsMapAddInt(int i, String str, int i2);

    public static native void dsMapAddString(int i, String str, String str2);

    public static native int dsMapCreate();

    public static native int getGuiHeight();

    public static native int getGuiWidth();

    public static native void iCadeEventDispatch(int i, boolean z);

    public static native int initGLFuncs(int i);

    public static native int jCreateDsMap(String[] strArr, String[] strArr2, double[] dArr);

    public static native void onGPDeviceAdded(int i, String str, String str2, int i2, int i3, int i4, int i5, int i6);

    public static native void onGPDeviceRemoved(int i);

    public static native void onGPKeyDown(int i, int i2);

    public static native void onGPKeyUp(int i, int i2);

    public static native void onGPNativeAxis(int i, int i2, float f);

    public static native void onGPNativeHat(int i, int i2, float f, float f2);

    public static native void onGamepadChange();

    public static native void onItemPurchase(String str, int i);

    public static native void registerGamepadConnected(int i, int i2, int i3);

    public static void Init() {
        try {
            System.loadLibrary("c++_shared");
            System.loadLibrary("yoyo");
        } catch (UnsatisfiedLinkError e) {
            Log.i("yoyo", "Unsatisfied link error - " + e);
            ms_loadLibraryFailed = true;
            ms_exitcalled = true;
        }
        ms_mp = null;
    }

    public static void Init(Context context) {
        ms_context = context;
    }

    public static void ExitApplication() {
        if (!ms_exitcalled) {
            Log.i("yoyo", "First exit application called");
            ms_exitcalled = true;
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    RunnerActivity.CurrentActivity.finish();
                }
            });
        }
    }

    public static int GetDefaultFrameBuffer() {
        return DemoRenderer.m_defaultFrameBuffer;
    }

    public static int UsingGL2() {
        return DemoGLSurfaceView.m_usingGL2;
    }

    public static void WaitForVsync() {
        DemoRenderer.WaitForVsync();
    }

    public static int HasVsyncHandler() {
        return RunnerActivity.CurrentActivity.vsyncHandler != null ? 1 : 0;
    }

    public static void OpenURL(String str) {
        try {
            ms_context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Exception e) {
            Log.i("yoyo", "OpenURL failed: " + e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x008a A[SYNTHETIC, Splitter:B:16:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void PlayMP3(java.lang.String r17, int r18) {
        /*
            r0 = 32
            r1 = 95
            r2 = r17
            java.lang.String r0 = r2.replace(r0, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Request to play mp3 - \""
            r1.append(r2)
            r1.append(r0)
            java.lang.String r2 = "\""
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r3 = "yoyo"
            android.util.Log.i(r3, r1)
            android.media.MediaPlayer r1 = com.yoyogames.runner.RunnerJNILib.ms_mp
            if (r1 == 0) goto L_0x002c
            StopMP3()
        L_0x002c:
            r1 = 1
            r4 = 0
            java.lang.String r5 = "com.butakov.psebay.R$raw"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0087 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0087 }
            r6.<init>()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r7 = "mp3_"
            r6.append(r7)     // Catch:{ Exception -> 0x0087 }
            r6.append(r0)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0087 }
            java.lang.reflect.Field r5 = r5.getField(r6)     // Catch:{ Exception -> 0x0087 }
            r6 = 0
            int r5 = r5.getInt(r6)     // Catch:{ Exception -> 0x0087 }
            if (r5 == 0) goto L_0x0087
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0087 }
            r6.<init>()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r7 = "Playing mp3 - \""
            r6.append(r7)     // Catch:{ Exception -> 0x0087 }
            r6.append(r0)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r7 = "\" id="
            r6.append(r7)     // Catch:{ Exception -> 0x0087 }
            r6.append(r5)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0087 }
            android.util.Log.i(r3, r6)     // Catch:{ Exception -> 0x0087 }
            com.yoyogames.runner.RunnerJNILib.mStoredPlaybackSessionId = r5     // Catch:{ Exception -> 0x0087 }
            android.content.Context r6 = com.yoyogames.runner.RunnerJNILib.ms_context     // Catch:{ Exception -> 0x0087 }
            android.media.MediaPlayer r5 = android.media.MediaPlayer.create(r6, r5)     // Catch:{ Exception -> 0x0087 }
            com.yoyogames.runner.RunnerJNILib.ms_mp = r5     // Catch:{ Exception -> 0x0087 }
            android.media.MediaPlayer r5 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x0087 }
            if (r18 == 0) goto L_0x007c
            r6 = 1
            goto L_0x007d
        L_0x007c:
            r6 = 0
        L_0x007d:
            r5.setLooping(r6)     // Catch:{ Exception -> 0x0087 }
            android.media.MediaPlayer r5 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x0087 }
            r5.start()     // Catch:{ Exception -> 0x0087 }
            r5 = 1
            goto L_0x0088
        L_0x0087:
            r5 = 0
        L_0x0088:
            if (r5 != 0) goto L_0x0173
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x015e }
            r5.<init>()     // Catch:{ Exception -> 0x015e }
            java.lang.String r6 = "Request to play zip - \""
            r5.append(r6)     // Catch:{ Exception -> 0x015e }
            java.lang.String r6 = com.butakov.psebay.DemoRenderer.m_apkFilePath     // Catch:{ Exception -> 0x015e }
            r5.append(r6)     // Catch:{ Exception -> 0x015e }
            r5.append(r2)     // Catch:{ Exception -> 0x015e }
            java.lang.String r2 = r5.toString()     // Catch:{ Exception -> 0x015e }
            android.util.Log.i(r3, r2)     // Catch:{ Exception -> 0x015e }
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ Exception -> 0x015e }
            java.lang.String r5 = com.butakov.psebay.DemoRenderer.m_apkFilePath     // Catch:{ Exception -> 0x015e }
            r2.<init>(r5)     // Catch:{ Exception -> 0x015e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x015e }
            r5.<init>()     // Catch:{ Exception -> 0x015e }
            java.lang.String r6 = "assets/"
            r5.append(r6)     // Catch:{ Exception -> 0x015e }
            java.util.Locale r6 = java.util.Locale.US     // Catch:{ Exception -> 0x015e }
            java.lang.String r0 = r0.toLowerCase(r6)     // Catch:{ Exception -> 0x015e }
            r5.append(r0)     // Catch:{ Exception -> 0x015e }
            java.lang.String r0 = ".mp3"
            r5.append(r0)     // Catch:{ Exception -> 0x015e }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x015e }
            java.util.zip.ZipEntry r0 = r2.getEntry(r0)     // Catch:{ Exception -> 0x015e }
            if (r0 == 0) goto L_0x015a
            java.util.Enumeration r5 = r2.entries()     // Catch:{ Exception -> 0x015e }
            r6 = 0
            r8 = r6
        L_0x00d3:
            boolean r10 = r5.hasMoreElements()     // Catch:{ Exception -> 0x015e }
            if (r10 == 0) goto L_0x0116
            java.lang.Object r10 = r5.nextElement()     // Catch:{ Exception -> 0x015e }
            java.util.zip.ZipEntry r10 = (java.util.zip.ZipEntry) r10     // Catch:{ Exception -> 0x015e }
            byte[] r11 = r10.getExtra()     // Catch:{ Exception -> 0x015e }
            if (r11 != 0) goto L_0x00e7
            r11 = r6
            goto L_0x00ed
        L_0x00e7:
            byte[] r11 = r10.getExtra()     // Catch:{ Exception -> 0x015e }
            int r11 = r11.length     // Catch:{ Exception -> 0x015e }
            long r11 = (long) r11     // Catch:{ Exception -> 0x015e }
        L_0x00ed:
            java.lang.String r13 = r10.getName()     // Catch:{ Exception -> 0x015e }
            int r13 = r13.length()     // Catch:{ Exception -> 0x015e }
            int r13 = r13 + 30
            long r13 = (long) r13     // Catch:{ Exception -> 0x015e }
            long r13 = r13 + r11
            long r8 = r8 + r13
            boolean r11 = r10.isDirectory()     // Catch:{ Exception -> 0x015e }
            if (r11 != 0) goto L_0x0105
            long r11 = r10.getCompressedSize()     // Catch:{ Exception -> 0x015e }
            goto L_0x0106
        L_0x0105:
            r11 = r6
        L_0x0106:
            long r13 = r10.getCrc()     // Catch:{ Exception -> 0x015e }
            long r15 = r0.getCrc()     // Catch:{ Exception -> 0x015e }
            int r10 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r10 != 0) goto L_0x0114
            r5 = 1
            goto L_0x0117
        L_0x0114:
            long r8 = r8 + r11
            goto L_0x00d3
        L_0x0116:
            r5 = 0
        L_0x0117:
            if (r5 == 0) goto L_0x015a
            r5 = -1
            com.yoyogames.runner.RunnerJNILib.mStoredPlaybackSessionId = r5     // Catch:{ Exception -> 0x015e }
            com.yoyogames.runner.RunnerJNILib.mStoredPlaybackOffset = r8     // Catch:{ Exception -> 0x015e }
            long r5 = r0.getSize()     // Catch:{ Exception -> 0x015e }
            com.yoyogames.runner.RunnerJNILib.mStoredPlaybackSize = r5     // Catch:{ Exception -> 0x015e }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x015e }
            java.lang.String r5 = com.butakov.psebay.DemoRenderer.m_apkFilePath     // Catch:{ Exception -> 0x015e }
            r0.<init>(r5)     // Catch:{ Exception -> 0x015e }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x015e }
            r5.<init>(r0)     // Catch:{ Exception -> 0x015e }
            android.media.MediaPlayer r0 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x015e }
            r0.<init>()     // Catch:{ Exception -> 0x015e }
            com.yoyogames.runner.RunnerJNILib.ms_mp = r0     // Catch:{ Exception -> 0x015e }
            android.media.MediaPlayer r6 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x015e }
            java.io.FileDescriptor r7 = r5.getFD()     // Catch:{ Exception -> 0x015e }
            long r8 = com.yoyogames.runner.RunnerJNILib.mStoredPlaybackOffset     // Catch:{ Exception -> 0x015e }
            long r10 = com.yoyogames.runner.RunnerJNILib.mStoredPlaybackSize     // Catch:{ Exception -> 0x015e }
            r6.setDataSource(r7, r8, r10)     // Catch:{ Exception -> 0x015e }
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x015e }
            if (r18 == 0) goto L_0x0149
            goto L_0x014a
        L_0x0149:
            r1 = 0
        L_0x014a:
            r0.setLooping(r1)     // Catch:{ Exception -> 0x015e }
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x015e }
            r0.prepare()     // Catch:{ Exception -> 0x015e }
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x015e }
            r0.start()     // Catch:{ Exception -> 0x015e }
            r5.close()     // Catch:{ Exception -> 0x015e }
        L_0x015a:
            r2.close()     // Catch:{ Exception -> 0x015e }
            goto L_0x0173
        L_0x015e:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Exception while opening mp3 - "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            android.util.Log.i(r3, r0)
        L_0x0173:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yoyogames.runner.RunnerJNILib.PlayMP3(java.lang.String, int):void");
    }

    public static void PauseMP3() {
        if (ms_mp != null) {
            Log.i("yoyo", "pause mp3");
            try {
                ms_mp.pause();
            } catch (Exception e) {
                Log.i("yoyo", "Exception while pausing mp3 - " + e);
            }
        }
    }

    public static void ResumeMP3() {
        if (ms_mp != null) {
            Log.i("yoyo", "resume mp3");
            try {
                ms_mp.start();
            } catch (Exception e) {
                Log.i("yoyo", "Exception while resuming mp3 - " + e);
            }
        }
    }

    public static void StopMP3() {
        if (ms_mp != null) {
            Log.i("yoyo", "stop mp3");
            try {
                ms_mp.stop();
                ms_mp.release();
            } catch (Exception e) {
                Log.i("yoyo", "Exception while stopping mp3 - " + e);
            }
            ms_mp = null;
        }
    }

    public static boolean PlayingMP3() {
        MediaPlayer mediaPlayer = ms_mp;
        if (mediaPlayer != null) {
            return mediaPlayer.isPlaying();
        }
        Log.i("yoyo", "PlayingMP3(): ms_mp is NULL");
        return false;
    }

    public static void SetMP3Volume(float f) {
        MediaPlayer mediaPlayer = ms_mp;
        if (mediaPlayer != null) {
            mediaPlayer.setVolume(f, f);
            mStoredVolume = f;
        }
    }

    public static void StoreMP3State() {
        MediaPlayer mediaPlayer = ms_mp;
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mStoredPlaybackPosition = ms_mp.getCurrentPosition();
            mStoredPlaybackLoop = ms_mp.isLooping();
            mPlaybackStateStored = true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0066 A[Catch:{ Exception -> 0x007d }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0079 A[Catch:{ Exception -> 0x007d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void RestoreMP3State() {
        /*
            boolean r0 = com.yoyogames.runner.RunnerJNILib.mPlaybackStateStored
            if (r0 == 0) goto L_0x0093
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp
            if (r0 != 0) goto L_0x0093
            r0 = 0
            int r1 = com.yoyogames.runner.RunnerJNILib.mStoredPlaybackSessionId
            r2 = -1
            java.lang.String r3 = "Exception while opening mp3 - "
            java.lang.String r4 = "yoyo"
            if (r1 == r2) goto L_0x001c
            android.content.Context r2 = com.yoyogames.runner.RunnerJNILib.ms_context
            android.media.MediaPlayer r1 = android.media.MediaPlayer.create(r2, r1)
            com.yoyogames.runner.RunnerJNILib.ms_mp = r1
            r2 = r0
            goto L_0x0054
        L_0x001c:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x003f }
            java.lang.String r2 = com.butakov.psebay.DemoRenderer.m_apkFilePath     // Catch:{ Exception -> 0x003f }
            r1.<init>(r2)     // Catch:{ Exception -> 0x003f }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x003f }
            r2.<init>(r1)     // Catch:{ Exception -> 0x003f }
            android.media.MediaPlayer r0 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x003d }
            r0.<init>()     // Catch:{ Exception -> 0x003d }
            com.yoyogames.runner.RunnerJNILib.ms_mp = r0     // Catch:{ Exception -> 0x003d }
            android.media.MediaPlayer r5 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x003d }
            java.io.FileDescriptor r6 = r2.getFD()     // Catch:{ Exception -> 0x003d }
            long r7 = com.yoyogames.runner.RunnerJNILib.mStoredPlaybackOffset     // Catch:{ Exception -> 0x003d }
            long r9 = com.yoyogames.runner.RunnerJNILib.mStoredPlaybackSize     // Catch:{ Exception -> 0x003d }
            r5.setDataSource(r6, r7, r9)     // Catch:{ Exception -> 0x003d }
            goto L_0x0054
        L_0x003d:
            r0 = move-exception
            goto L_0x0042
        L_0x003f:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0042:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            android.util.Log.i(r4, r0)
        L_0x0054:
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x007d }
            boolean r1 = com.yoyogames.runner.RunnerJNILib.mStoredPlaybackLoop     // Catch:{ Exception -> 0x007d }
            r0.setLooping(r1)     // Catch:{ Exception -> 0x007d }
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x007d }
            float r1 = com.yoyogames.runner.RunnerJNILib.mStoredVolume     // Catch:{ Exception -> 0x007d }
            float r5 = com.yoyogames.runner.RunnerJNILib.mStoredVolume     // Catch:{ Exception -> 0x007d }
            r0.setVolume(r1, r5)     // Catch:{ Exception -> 0x007d }
            if (r2 == 0) goto L_0x006b
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x007d }
            r0.prepare()     // Catch:{ Exception -> 0x007d }
        L_0x006b:
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x007d }
            int r1 = com.yoyogames.runner.RunnerJNILib.mStoredPlaybackPosition     // Catch:{ Exception -> 0x007d }
            r0.seekTo(r1)     // Catch:{ Exception -> 0x007d }
            android.media.MediaPlayer r0 = com.yoyogames.runner.RunnerJNILib.ms_mp     // Catch:{ Exception -> 0x007d }
            r0.start()     // Catch:{ Exception -> 0x007d }
            if (r2 == 0) goto L_0x0090
            r2.close()     // Catch:{ Exception -> 0x007d }
            goto L_0x0090
        L_0x007d:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            android.util.Log.i(r4, r0)
        L_0x0090:
            r0 = 0
            com.yoyogames.runner.RunnerJNILib.mPlaybackStateStored = r0
        L_0x0093:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yoyogames.runner.RunnerJNILib.RestoreMP3State():void");
    }

    public static void MoveAds(int i, int i2, int i3) {
        if (RunnerActivity.mExtension != null) {
            for (int i4 = 0; i4 < RunnerActivity.mExtension.length; i4++) {
                if (RunnerActivity.mExtension[i4] instanceof IAdExt) {
                    ((IAdExt) RunnerActivity.mExtension[i4]).move(i, i2, i3);
                }
            }
        }
    }

    public static void EnableAds(int i, int i2, int i3) {
        if (RunnerActivity.mExtension != null) {
            for (int i4 = 0; i4 < RunnerActivity.mExtension.length; i4++) {
                if (RunnerActivity.mExtension[i4] instanceof IAdExt) {
                    ((IAdExt) RunnerActivity.mExtension[i4]).enable(i, i2, i3);
                }
            }
        }
    }

    public static int AdsDisplayWidth(int i) {
        if (RunnerActivity.mExtension != null) {
            for (int i2 = 0; i2 < RunnerActivity.mExtension.length; i2++) {
                if (RunnerActivity.mExtension[i2] instanceof IAdExt) {
                    return ((IAdExt) RunnerActivity.mExtension[i2]).getAdDisplayWidth(i);
                }
            }
        }
        return 0;
    }

    public static int AdsDisplayHeight(int i) {
        if (RunnerActivity.mExtension != null) {
            for (int i2 = 0; i2 < RunnerActivity.mExtension.length; i2++) {
                if (RunnerActivity.mExtension[i2] instanceof IAdExt) {
                    return ((IAdExt) RunnerActivity.mExtension[i2]).getAdDisplayHeight(i);
                }
            }
        }
        return 0;
    }

    public static void DisableAds(int i) {
        Log.i("yoyo", "DisableAds");
        if (RunnerActivity.mExtension != null) {
            for (int i2 = 0; i2 < RunnerActivity.mExtension.length; i2++) {
                if (RunnerActivity.mExtension[i2] instanceof IAdExt) {
                    ((IAdExt) RunnerActivity.mExtension[i2]).disable(i);
                }
            }
        }
    }

    public static void AdsEvent(String str) {
        if (RunnerActivity.mExtension != null) {
            for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                if (RunnerActivity.mExtension[i] instanceof IAdExt) {
                    ((IAdExt) RunnerActivity.mExtension[i]).event(str);
                }
            }
        }
    }

    public static void SetThreadPriority(int i) {
        Log.i("yoyo", "SetThreadPriority(" + i);
        Thread.currentThread().setPriority(i);
    }

    public static void LeaveRating(final String str, final String str2, final String str3, final String str4) {
        Log.i("yoyo", "LeaveRating(" + str + ", " + str2 + ", " + str3 + ", " + str4 + ")");
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                builder.setMessage(str).setCancelable(false).setPositiveButton(str2, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerJNILib.OpenURL(str4);
                    }
                }).setNegativeButton(str3, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                builder.create().show();
            }
        });
    }

    public static void OpenAchievements() {
        if (RunnerActivity.isLoggedInGooglePlay()) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    RunnerJNILib.CallExtensionFunction("GooglePlayServicesExtension", "onShowGSAchievements", 0, null);
                }
            });
        }
    }

    public static void AchievementLoadFriends() {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).LoadFriends();
                        }
                    }
                }
            }
        });
    }

    public static void AchievementGetInfo(final String str) {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                Log.i("yoyo", "Calling to social interface to get info for id " + str);
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).GetInfo(str);
                        }
                    }
                }
            }
        });
    }

    public static void AchievementShow(final int i, final String str, final int i2) {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).Show(i, str, i2);
                        }
                    }
                }
            }
        });
    }

    public static void AchievementLoadPic(final String str) {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                Log.i("yoyo", "Calling to social interface to load pic for id " + str);
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).LoadPic(str);
                        }
                    }
                }
            }
        });
    }

    public static void AchievementEvent(final String str) {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                Log.i("yoyo", "Calling to social interface to register event " + str);
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).Event(str);
                        }
                    }
                }
            }
        });
    }

    public static void AchievementLoadLeaderboard(final String str, final int i, final int i2, final int i3) {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).LoadLeaderboard(str, i, i2, i3);
                        }
                    }
                }
            }
        });
    }

    public static int OsGetInfo() {
        boolean z;
        String property = System.getProperty("os.version", "");
        String property2 = System.getProperty("user.region", "");
        RunnerKeyboardController GetKeyboardController = RunnerActivity.CurrentActivity.GetKeyboardController();
        if (GetKeyboardController != null) {
            z = GetKeyboardController.GetPhysicalKeyboardConnected();
        } else {
            z = false;
        }
        return CreateVersionDSMap(Build.VERSION.SDK_INT, Build.VERSION.RELEASE, Build.MODEL, Build.DEVICE, Build.MANUFACTURER, Build.CPU_ABI, Build.CPU_ABI2, Build.BOOTLOADER, Build.BOARD, property, property2, ms_versionName, z);
    }

    public static void OpenLeaderboards() {
        if (RunnerActivity.isLoggedInGooglePlay()) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    RunnerJNILib.CallExtensionFunction("GooglePlayServicesExtension", "onShowGSLeaderboards", 0, null);
                }
            });
        }
    }

    public static void AchievementLogout() {
        Log.i("yoyo", "AchievementLogout()");
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).Logout();
                        }
                    }
                }
            }
        });
    }

    public static void AchievementLogin() {
        Log.i("yoyo", "AchievementLogin()");
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                RunnerActivity.ach_login();
            }
        });
    }

    public static int AchievementLoginStatus() {
        return RunnerActivity.isLoggedInGooglePlay() ? 1 : 0;
    }

    public static void SendAchievement(final String str, final float f) {
        if (RunnerActivity.isLoggedInGooglePlay()) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    RunnerJNILib.CallExtensionFunction("GooglePlayServicesExtension", "onPostAchievement", 2, new Object[]{str, Float.valueOf(f)});
                }
            });
        }
    }

    public static void IncrementAchievement(final String str, final float f) {
        if (RunnerActivity.isLoggedInGooglePlay()) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    RunnerJNILib.CallExtensionFunction("GooglePlayServicesExtension", "onIncrementAchievement", 2, new Object[]{str, Float.valueOf(f)});
                }
            });
        } else {
            Log.i("yoyo", "Current achievements system does not support incrementing achievements");
        }
    }

    public static void SendHighScore(final String str, final int i) {
        Log.i("yoyo", "SendHighScore(" + str + "," + i + ")");
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (RunnerActivity.mExtension != null) {
                    for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                        if (RunnerActivity.mExtension[i] instanceof ISocial) {
                            ((ISocial) RunnerActivity.mExtension[i]).PostScore(str, i);
                        }
                    }
                }
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        android.util.Log.i("yoyo", "Extension Class not found: com.butakov.psebay." + r12 + " attempting to call " + r13);
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0189, code lost:
        android.util.Log.i("yoyo", "Exception thrown trying to call method " + r13 + " on " + r12 + " " + r14.getMessage());
        r14.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0044, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0047 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object CallExtensionFunction(java.lang.String r12, java.lang.String r13, int r14, java.lang.Object[] r15) {
        /*
            java.lang.String r0 = " "
            java.lang.String r1 = " on "
            java.lang.Object[] r2 = com.butakov.psebay.RunnerActivity.mExtension
            r3 = 0
            java.lang.String r4 = "yoyo"
            if (r2 != 0) goto L_0x0028
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Attempting to call extension function with no extensions loaded "
            r14.append(r15)
            r14.append(r13)
            java.lang.String r13 = " on class "
            r14.append(r13)
            r14.append(r12)
            java.lang.String r12 = r14.toString()
            android.util.Log.i(r4, r12)
            return r3
        L_0x0028:
            if (r12 == 0) goto L_0x01b1
            if (r13 != 0) goto L_0x002e
            goto L_0x01b1
        L_0x002e:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0047 }
            r2.<init>()     // Catch:{ ClassNotFoundException -> 0x0047 }
            java.lang.String r5 = "com.butakov.psebay."
            r2.append(r5)     // Catch:{ ClassNotFoundException -> 0x0047 }
            r2.append(r12)     // Catch:{ ClassNotFoundException -> 0x0047 }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x0047 }
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x0047 }
            goto L_0x0064
        L_0x0044:
            r14 = move-exception
            goto L_0x0189
        L_0x0047:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r2.<init>()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r5 = "Extension Class not found: com.butakov.psebay."
            r2.append(r5)     // Catch:{ Exception -> 0x0044 }
            r2.append(r12)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r5 = " attempting to call "
            r2.append(r5)     // Catch:{ Exception -> 0x0044 }
            r2.append(r13)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0044 }
            android.util.Log.i(r4, r2)     // Catch:{ Exception -> 0x0044 }
            r2 = r3
        L_0x0064:
            if (r2 == 0) goto L_0x01b0
            r5 = 0
            r6 = 0
        L_0x0068:
            java.lang.Object[] r7 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0044 }
            int r7 = r7.length     // Catch:{ Exception -> 0x0044 }
            if (r6 >= r7) goto L_0x01b0
            java.lang.Object[] r7 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0044 }
            r7 = r7[r6]     // Catch:{ Exception -> 0x0044 }
            if (r7 == 0) goto L_0x0185
            java.lang.Object[] r7 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0044 }
            r7 = r7[r6]     // Catch:{ Exception -> 0x0044 }
            boolean r7 = r2.isInstance(r7)     // Catch:{ Exception -> 0x0044 }
            if (r7 == 0) goto L_0x0185
            java.lang.String r7 = " looking for "
            if (r14 <= 0) goto L_0x013e
            java.lang.Class[] r8 = new java.lang.Class[r14]     // Catch:{ Exception -> 0x0044 }
            r9 = 0
        L_0x0084:
            if (r9 >= r14) goto L_0x0091
            r10 = r15[r9]     // Catch:{ Exception -> 0x0044 }
            java.lang.Class r10 = r10.getClass()     // Catch:{ Exception -> 0x0044 }
            r8[r9] = r10     // Catch:{ Exception -> 0x0044 }
            int r9 = r9 + 1
            goto L_0x0084
        L_0x0091:
            java.lang.Object[] r9 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x00bb }
            r9 = r9[r6]     // Catch:{ Exception -> 0x00bb }
            java.lang.Class r9 = r9.getClass()     // Catch:{ Exception -> 0x00bb }
            java.lang.reflect.Method r9 = r9.getMethod(r13, r8)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            r10.<init>()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r11 = "Method found, attempting to invoke "
            r10.append(r11)     // Catch:{ Exception -> 0x00bb }
            r10.append(r13)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00bb }
            android.util.Log.i(r4, r10)     // Catch:{ Exception -> 0x00bb }
            java.lang.Object[] r10 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x00bb }
            r10 = r10[r6]     // Catch:{ Exception -> 0x00bb }
            java.lang.Object r3 = r9.invoke(r10, r15)     // Catch:{ Exception -> 0x00bb }
            goto L_0x01b0
        L_0x00bb:
            r9 = move-exception
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r10.<init>()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r11 = "Exception thrown calling method on extension class:"
            r10.append(r11)     // Catch:{ Exception -> 0x0044 }
            r10.append(r9)     // Catch:{ Exception -> 0x0044 }
            r10.append(r7)     // Catch:{ Exception -> 0x0044 }
            r10.append(r13)     // Catch:{ Exception -> 0x0044 }
            r10.append(r1)     // Catch:{ Exception -> 0x0044 }
            r10.append(r12)     // Catch:{ Exception -> 0x0044 }
            r10.append(r0)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r7 = r9.getMessage()     // Catch:{ Exception -> 0x0044 }
            r10.append(r7)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r7 = r10.toString()     // Catch:{ Exception -> 0x0044 }
            android.util.Log.i(r4, r7)     // Catch:{ Exception -> 0x0044 }
            r7 = 0
        L_0x00e7:
            if (r7 >= r14) goto L_0x010e
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r10.<init>()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r11 = "Argument "
            r10.append(r11)     // Catch:{ Exception -> 0x0044 }
            r10.append(r7)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r11 = " of type "
            r10.append(r11)     // Catch:{ Exception -> 0x0044 }
            r11 = r8[r7]     // Catch:{ Exception -> 0x0044 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0044 }
            r10.append(r11)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0044 }
            android.util.Log.i(r4, r10)     // Catch:{ Exception -> 0x0044 }
            int r7 = r7 + 1
            goto L_0x00e7
        L_0x010e:
            r9.printStackTrace()     // Catch:{ Exception -> 0x0044 }
            java.lang.Object[] r7 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0044 }
            r7 = r7[r6]     // Catch:{ Exception -> 0x0044 }
            java.lang.Class r7 = r7.getClass()     // Catch:{ Exception -> 0x0044 }
            java.lang.reflect.Method[] r7 = r7.getMethods()     // Catch:{ Exception -> 0x0044 }
            r8 = 0
        L_0x011e:
            int r9 = r7.length     // Catch:{ Exception -> 0x0044 }
            if (r8 >= r9) goto L_0x0185
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r9.<init>()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r10 = "Found method "
            r9.append(r10)     // Catch:{ Exception -> 0x0044 }
            r10 = r7[r8]     // Catch:{ Exception -> 0x0044 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0044 }
            r9.append(r10)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0044 }
            android.util.Log.i(r4, r9)     // Catch:{ Exception -> 0x0044 }
            int r8 = r8 + 1
            goto L_0x011e
        L_0x013e:
            java.lang.Object[] r8 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0157 }
            r8 = r8[r6]     // Catch:{ Exception -> 0x0157 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x0157 }
            java.lang.Class[] r9 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x0157 }
            java.lang.reflect.Method r8 = r8.getMethod(r13, r9)     // Catch:{ Exception -> 0x0157 }
            java.lang.Object[] r9 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0157 }
            r9 = r9[r6]     // Catch:{ Exception -> 0x0157 }
            java.lang.Object[] r10 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0157 }
            java.lang.Object r3 = r8.invoke(r9, r10)     // Catch:{ Exception -> 0x0157 }
            goto L_0x01b0
        L_0x0157:
            r8 = move-exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r9.<init>()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r10 = "Exception thrown calling argfree method on extension class:"
            r9.append(r10)     // Catch:{ Exception -> 0x0044 }
            r9.append(r8)     // Catch:{ Exception -> 0x0044 }
            r9.append(r7)     // Catch:{ Exception -> 0x0044 }
            r9.append(r13)     // Catch:{ Exception -> 0x0044 }
            r9.append(r1)     // Catch:{ Exception -> 0x0044 }
            r9.append(r12)     // Catch:{ Exception -> 0x0044 }
            r9.append(r0)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r7 = r8.getMessage()     // Catch:{ Exception -> 0x0044 }
            r9.append(r7)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r7 = r9.toString()     // Catch:{ Exception -> 0x0044 }
            android.util.Log.i(r4, r7)     // Catch:{ Exception -> 0x0044 }
            r8.printStackTrace()     // Catch:{ Exception -> 0x0044 }
        L_0x0185:
            int r6 = r6 + 1
            goto L_0x0068
        L_0x0189:
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r2 = "Exception thrown trying to call method "
            r15.append(r2)
            r15.append(r13)
            r15.append(r1)
            r15.append(r12)
            r15.append(r0)
            java.lang.String r12 = r14.getMessage()
            r15.append(r12)
            java.lang.String r12 = r15.toString()
            android.util.Log.i(r4, r12)
            r14.printStackTrace()
        L_0x01b0:
            return r3
        L_0x01b1:
            if (r12 != 0) goto L_0x01c8
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r14 = "Attempting to call extension function with null classname method:"
            r12.append(r14)
            r12.append(r13)
            java.lang.String r12 = r12.toString()
            android.util.Log.i(r4, r12)
            goto L_0x01de
        L_0x01c8:
            if (r13 != 0) goto L_0x01de
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "Attempting to call extension function with null methodname on class:"
            r13.append(r14)
            r13.append(r12)
            java.lang.String r12 = r13.toString()
            android.util.Log.i(r4, r12)
        L_0x01de:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yoyogames.runner.RunnerJNILib.CallExtensionFunction(java.lang.String, java.lang.String, int, java.lang.Object[]):java.lang.Object");
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x004f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object CallExtensionFunction(java.lang.String r20, java.lang.String r21, int r22, double[] r23, java.lang.Object[] r24) {
        /*
            r1 = r20
            r2 = r21
            r3 = r22
            java.lang.String r4 = "Found method "
            java.lang.String r5 = " on "
            java.lang.Object[] r0 = com.butakov.psebay.RunnerActivity.mExtension
            java.lang.String r6 = " on class "
            r7 = 0
            java.lang.String r8 = "yoyo"
            if (r0 != 0) goto L_0x002e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "Attempting to call extension function with no extensions loaded "
            r0.append(r3)
            r0.append(r2)
            r0.append(r6)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r8, r0)
            return r7
        L_0x002e:
            if (r1 == 0) goto L_0x02c1
            if (r2 != 0) goto L_0x0034
            goto L_0x02c1
        L_0x0034:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x004f, Exception -> 0x004b }
            r0.<init>()     // Catch:{ ClassNotFoundException -> 0x004f, Exception -> 0x004b }
            java.lang.String r9 = "com.butakov.psebay."
            r0.append(r9)     // Catch:{ ClassNotFoundException -> 0x004f, Exception -> 0x004b }
            r0.append(r1)     // Catch:{ ClassNotFoundException -> 0x004f, Exception -> 0x004b }
            java.lang.String r0 = r0.toString()     // Catch:{ ClassNotFoundException -> 0x004f, Exception -> 0x004b }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x004f, Exception -> 0x004b }
            r9 = r0
            goto L_0x006c
        L_0x004b:
            r0 = move-exception
            r12 = r7
            goto L_0x029c
        L_0x004f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x029a }
            r0.<init>()     // Catch:{ Exception -> 0x029a }
            java.lang.String r9 = "Extension Class not found: com.butakov.psebay."
            r0.append(r9)     // Catch:{ Exception -> 0x029a }
            r0.append(r1)     // Catch:{ Exception -> 0x029a }
            java.lang.String r9 = " attempting to call "
            r0.append(r9)     // Catch:{ Exception -> 0x029a }
            r0.append(r2)     // Catch:{ Exception -> 0x029a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x029a }
            android.util.Log.i(r8, r0)     // Catch:{ Exception -> 0x029a }
            r9 = r7
        L_0x006c:
            if (r9 == 0) goto L_0x0298
            r12 = r7
            r11 = 0
        L_0x0070:
            java.lang.Object[] r0 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0296 }
            int r0 = r0.length     // Catch:{ Exception -> 0x0296 }
            if (r11 >= r0) goto L_0x02c0
            java.lang.Object[] r0 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0296 }
            r0 = r0[r11]     // Catch:{ Exception -> 0x0296 }
            boolean r0 = r9.isInstance(r0)     // Catch:{ Exception -> 0x0296 }
            if (r0 == 0) goto L_0x028a
            if (r3 <= 0) goto L_0x0237
            java.lang.Class[] r0 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x0296 }
            java.lang.Object[] r13 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0296 }
            r14 = 0
        L_0x0086:
            if (r14 >= r3) goto L_0x00a8
            r15 = r24[r14]     // Catch:{ Exception -> 0x0296 }
            if (r15 == 0) goto L_0x0099
            r15 = r24[r14]     // Catch:{ Exception -> 0x0296 }
            java.lang.Class r15 = r15.getClass()     // Catch:{ Exception -> 0x0296 }
            r0[r14] = r15     // Catch:{ Exception -> 0x0296 }
            r15 = r24[r14]     // Catch:{ Exception -> 0x0296 }
            r13[r14] = r15     // Catch:{ Exception -> 0x0296 }
            goto L_0x00a5
        L_0x0099:
            java.lang.Class r15 = java.lang.Double.TYPE     // Catch:{ Exception -> 0x0296 }
            r0[r14] = r15     // Catch:{ Exception -> 0x0296 }
            r15 = r23[r14]     // Catch:{ Exception -> 0x0296 }
            java.lang.Double r15 = java.lang.Double.valueOf(r15)     // Catch:{ Exception -> 0x0296 }
            r13[r14] = r15     // Catch:{ Exception -> 0x0296 }
        L_0x00a5:
            int r14 = r14 + 1
            goto L_0x0086
        L_0x00a8:
            java.lang.reflect.Method[] r14 = r9.getMethods()     // Catch:{ Exception -> 0x0296 }
            int r15 = r14.length     // Catch:{ Exception -> 0x0296 }
            r7 = 0
        L_0x00ae:
            if (r7 >= r15) goto L_0x00ef
            r17 = r14[r7]     // Catch:{ Exception -> 0x0296 }
            java.lang.String r10 = r17.getName()     // Catch:{ Exception -> 0x0296 }
            boolean r10 = r2.equals(r10)     // Catch:{ Exception -> 0x0296 }
            if (r10 != 0) goto L_0x00bf
        L_0x00bc:
            r18 = r9
            goto L_0x00e8
        L_0x00bf:
            java.lang.Class[] r10 = r17.getParameterTypes()     // Catch:{ Exception -> 0x0296 }
            if (r10 == 0) goto L_0x00bc
            r18 = r9
            int r9 = r10.length     // Catch:{ Exception -> 0x0296 }
            if (r9 == r3) goto L_0x00cb
            goto L_0x00e8
        L_0x00cb:
            r9 = 0
        L_0x00cc:
            int r3 = r0.length     // Catch:{ Exception -> 0x0296 }
            if (r9 >= r3) goto L_0x00e1
            r3 = r10[r9]     // Catch:{ Exception -> 0x0296 }
            r19 = r10
            r10 = r0[r9]     // Catch:{ Exception -> 0x0296 }
            boolean r3 = r3.isAssignableFrom(r10)     // Catch:{ Exception -> 0x0296 }
            if (r3 != 0) goto L_0x00dc
            goto L_0x00e1
        L_0x00dc:
            int r9 = r9 + 1
            r10 = r19
            goto L_0x00cc
        L_0x00e1:
            int r3 = r0.length     // Catch:{ Exception -> 0x0296 }
            if (r9 == r3) goto L_0x00e5
            goto L_0x00e8
        L_0x00e5:
            r3 = r17
            goto L_0x00f2
        L_0x00e8:
            int r7 = r7 + 1
            r3 = r22
            r9 = r18
            goto L_0x00ae
        L_0x00ef:
            r18 = r9
            r3 = 0
        L_0x00f2:
            if (r3 == 0) goto L_0x01ef
            java.lang.Object[] r0 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ InvocationTargetException -> 0x0175, Exception -> 0x00ff }
            r0 = r0[r11]     // Catch:{ InvocationTargetException -> 0x0175, Exception -> 0x00ff }
            java.lang.Object r0 = r3.invoke(r0, r13)     // Catch:{ InvocationTargetException -> 0x0175, Exception -> 0x00ff }
            r12 = r0
            goto L_0x0235
        L_0x00ff:
            r0 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r3.<init>()     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = "Exception3 thrown trying to call method "
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            r3.append(r2)     // Catch:{ Exception -> 0x0296 }
            r3.append(r5)     // Catch:{ Exception -> 0x0296 }
            r3.append(r1)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r3)     // Catch:{ Exception -> 0x0296 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r3.<init>()     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = "Exception:"
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = r0.getMessage()     // Catch:{ Exception -> 0x0296 }
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = ". Details: "
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = r0.toString()     // Catch:{ Exception -> 0x0296 }
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = ". Stack trace:\n"
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r0 = android.util.Log.getStackTraceString(r0)     // Catch:{ Exception -> 0x0296 }
            r3.append(r0)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r0)     // Catch:{ Exception -> 0x0296 }
            java.lang.Object[] r0 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0296 }
            r0 = r0[r11]     // Catch:{ Exception -> 0x0296 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0296 }
            java.lang.reflect.Method[] r0 = r0.getMethods()     // Catch:{ Exception -> 0x0296 }
            r3 = 0
        L_0x0157:
            int r7 = r0.length     // Catch:{ Exception -> 0x0296 }
            if (r3 >= r7) goto L_0x0235
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r7.<init>()     // Catch:{ Exception -> 0x0296 }
            r7.append(r4)     // Catch:{ Exception -> 0x0296 }
            r9 = r0[r3]     // Catch:{ Exception -> 0x0296 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0296 }
            r7.append(r9)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r7)     // Catch:{ Exception -> 0x0296 }
            int r3 = r3 + 1
            goto L_0x0157
        L_0x0175:
            r0 = move-exception
            java.lang.Throwable r0 = r0.getTargetException()     // Catch:{ Exception -> 0x0296 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r3.<init>()     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = "InvocationTargetException thrown trying to call method "
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            r3.append(r2)     // Catch:{ Exception -> 0x0296 }
            r3.append(r5)     // Catch:{ Exception -> 0x0296 }
            r3.append(r1)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r3)     // Catch:{ Exception -> 0x0296 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r3.<init>()     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = "Target exception: "
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = r0.getMessage()     // Catch:{ Exception -> 0x0296 }
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = ". Cause: "
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.Throwable r7 = r0.getCause()     // Catch:{ Exception -> 0x0296 }
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = ". Stack trace: \n"
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r0 = android.util.Log.getStackTraceString(r0)     // Catch:{ Exception -> 0x0296 }
            r3.append(r0)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r0)     // Catch:{ Exception -> 0x0296 }
            java.lang.Object[] r0 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x0296 }
            r0 = r0[r11]     // Catch:{ Exception -> 0x0296 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0296 }
            java.lang.reflect.Method[] r0 = r0.getMethods()     // Catch:{ Exception -> 0x0296 }
            r3 = 0
        L_0x01d1:
            int r7 = r0.length     // Catch:{ Exception -> 0x0296 }
            if (r3 >= r7) goto L_0x0235
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r7.<init>()     // Catch:{ Exception -> 0x0296 }
            r7.append(r4)     // Catch:{ Exception -> 0x0296 }
            r9 = r0[r3]     // Catch:{ Exception -> 0x0296 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0296 }
            r7.append(r9)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r7)     // Catch:{ Exception -> 0x0296 }
            int r3 = r3 + 1
            goto L_0x01d1
        L_0x01ef:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r3.<init>()     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = "Unable to find method to invoke matching methodname:"
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            r3.append(r2)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = " on class:"
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            r3.append(r1)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = " with params:"
            r3.append(r7)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r3)     // Catch:{ Exception -> 0x0296 }
            r3 = 0
        L_0x0211:
            int r7 = r0.length     // Catch:{ Exception -> 0x0296 }
            if (r3 >= r7) goto L_0x0235
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r7.<init>()     // Catch:{ Exception -> 0x0296 }
            java.lang.String r9 = "param:"
            r7.append(r9)     // Catch:{ Exception -> 0x0296 }
            r7.append(r3)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r9 = ":"
            r7.append(r9)     // Catch:{ Exception -> 0x0296 }
            r9 = r0[r3]     // Catch:{ Exception -> 0x0296 }
            r7.append(r9)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r7)     // Catch:{ Exception -> 0x0296 }
            int r3 = r3 + 1
            goto L_0x0211
        L_0x0235:
            r7 = 0
            goto L_0x028d
        L_0x0237:
            r18 = r9
            java.lang.Object[] r0 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x025e }
            r0 = r0[r11]     // Catch:{ Exception -> 0x025e }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x025e }
            r3 = 0
            java.lang.Class[] r7 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x025e }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r7)     // Catch:{ Exception -> 0x025e }
            if (r0 != 0) goto L_0x0250
            java.lang.String r0 = "Can't find argfree method on extension class:"
            android.util.Log.i(r8, r0)     // Catch:{ Exception -> 0x025e }
            goto L_0x0235
        L_0x0250:
            java.lang.Object[] r3 = com.butakov.psebay.RunnerActivity.mExtension     // Catch:{ Exception -> 0x025e }
            r3 = r3[r11]     // Catch:{ Exception -> 0x025e }
            r7 = 0
            java.lang.Object[] r9 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x025c }
            java.lang.Object r12 = r0.invoke(r3, r9)     // Catch:{ Exception -> 0x025c }
            goto L_0x028d
        L_0x025c:
            r0 = move-exception
            goto L_0x0260
        L_0x025e:
            r0 = move-exception
            r7 = 0
        L_0x0260:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0296 }
            r3.<init>()     // Catch:{ Exception -> 0x0296 }
            java.lang.String r9 = "Exception thrown trying to call "
            r3.append(r9)     // Catch:{ Exception -> 0x0296 }
            r3.append(r2)     // Catch:{ Exception -> 0x0296 }
            r3.append(r6)     // Catch:{ Exception -> 0x0296 }
            r3.append(r1)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r9 = " with no arguments:"
            r3.append(r9)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r9 = r0.getMessage()     // Catch:{ Exception -> 0x0296 }
            r3.append(r9)     // Catch:{ Exception -> 0x0296 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0296 }
            android.util.Log.i(r8, r3)     // Catch:{ Exception -> 0x0296 }
            r0.printStackTrace()     // Catch:{ Exception -> 0x0296 }
            goto L_0x028d
        L_0x028a:
            r18 = r9
            goto L_0x0235
        L_0x028d:
            int r11 = r11 + 1
            r3 = r22
            r9 = r18
            r7 = 0
            goto L_0x0070
        L_0x0296:
            r0 = move-exception
            goto L_0x029c
        L_0x0298:
            r12 = 0
            goto L_0x02c0
        L_0x029a:
            r0 = move-exception
            r12 = 0
        L_0x029c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Exception thrown trying to call method "
            r3.append(r4)
            r3.append(r2)
            r3.append(r5)
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            android.util.Log.i(r8, r1)
            java.lang.String r1 = r0.getMessage()
            android.util.Log.i(r8, r1)
            r0.printStackTrace()
        L_0x02c0:
            return r12
        L_0x02c1:
            if (r1 != 0) goto L_0x02d8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Attempting to call extension function with null classname method:"
            r0.append(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r8, r0)
            goto L_0x02ee
        L_0x02d8:
            if (r2 != 0) goto L_0x02ee
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Attempting to call extension function with null methodname on class:"
            r0.append(r2)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r8, r0)
        L_0x02ee:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yoyogames.runner.RunnerJNILib.CallExtensionFunction(java.lang.String, java.lang.String, int, double[], java.lang.Object[]):java.lang.Object");
    }

    public static boolean DownloadFileTo(String str, String str2) {
        try {
            Log.i("yoyo", "DownloadFileTo( " + str + " , " + str2 + " )");
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.setUseCaches(false);
            openConnection.connect();
            FileOutputStream fileOutputStream = new FileOutputStream(new File(str2));
            InputStream inputStream = openConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read > 0) {
                    Log.i("yoyo", "downloaded " + read + " bytes");
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.close();
                    return true;
                }
            }
        } catch (MalformedURLException e) {
            Log.i("yoyo", "MalformedURLException on DownloadFileTo" + e);
            return false;
        } catch (ProtocolException e2) {
            Log.i("yoyo", "ProtocolException on DownloadFileTo" + e2);
            return false;
        } catch (FileNotFoundException e3) {
            Log.i("yoyo", "FileNotFoundException on DownloadFileTo" + e3);
            return false;
        } catch (IOException e4) {
            Log.i("yoyo", "IOException on DownloadFileTo" + e4);
            return false;
        }
    }

    public static void ShowMessage(final String str) {
        Log.i("yoyo", "ShowMessage(\"" + str + "\")");
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                builder.setMessage(str).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        countDownLatch.countDown();
                    }
                });
                builder.create().show();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
    }

    public static void ShowMessageAsync(final String str, final int i) {
        Log.i("yoyo", "ShowMessageAsync(\"" + str + "\"," + i + ")");
        RunnerActivity.FocusOverride = true;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                builder.setMessage(str).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerJNILib.InputResult("OK", 1, i);
                    }
                });
                builder.create().show();
            }
        });
    }

    public static String InputString(final String str, final String str2) {
        Log.i("yoyo", "InputString(\"" + str + "\", \"" + str2 + "\")");
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                final EditText editText = new EditText(RunnerJNILib.ms_context);
                editText.setText(str2);
                builder.setView(editText);
                builder.setMessage(str).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerActivity.InputStringResult = editText.getText().toString();
                        countDownLatch.countDown();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerActivity.InputStringResult = str2;
                        countDownLatch.countDown();
                    }
                });
                builder.create().show();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
        return RunnerActivity.InputStringResult;
    }

    public static void InputStringAsync(final String str, final String str2, final int i) {
        Log.i("yoyo", "InputStringAsync(\"" + str + "\", \"" + str2 + "\"," + i + ")");
        RunnerActivity.FocusOverride = true;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                final EditText editText = new EditText(RunnerJNILib.ms_context);
                editText.setText(str2);
                builder.setView(editText);
                builder.setMessage(str).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerActivity.InputStringResult = editText.getText().toString();
                        RunnerJNILib.InputResult(RunnerActivity.InputStringResult, 1, i);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerActivity.InputStringResult = str2;
                        RunnerJNILib.InputResult(RunnerActivity.InputStringResult, 0, i);
                    }
                });
                builder.create().show();
            }
        });
    }

    public static int ShowQuestion(final String str) {
        Log.i("yoyo", "ShowQuestion(\"" + str + "\")");
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        RunnerActivity.ShowQuestionYesNo = 0;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                builder.setMessage(str).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerActivity.ShowQuestionYesNo = 1;
                        countDownLatch.countDown();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerActivity.ShowQuestionYesNo = 0;
                        countDownLatch.countDown();
                    }
                });
                builder.create().show();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
        return RunnerActivity.ShowQuestionYesNo;
    }

    public static void ShowQuestionAsync(final String str, final int i) {
        Log.i("yoyo", "ShowQuestionAsync(\"" + str + "\"," + i + ")");
        RunnerActivity.FocusOverride = true;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                builder.setMessage(str).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerJNILib.InputResult("1", 1, i);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerJNILib.InputResult("0", 0, i);
                    }
                });
                builder.create().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public static String HttpGetHeaders(HttpURLConnection httpURLConnection) {
        String str = "";
        for (Map.Entry next : httpURLConnection.getHeaderFields().entrySet()) {
            for (String str2 : (List) next.getValue()) {
                if (next.getKey() != null) {
                    str = str + ((String) next.getKey()) + ": " + str2 + Registry.LINE_SEPARATOR;
                } else {
                    str = str + "nokey: " + str2 + Registry.LINE_SEPARATOR;
                }
            }
        }
        return str;
    }

    public static void HttpGet(final String str, final int i) {
        Log.i("yoyo", "HttpGet(\"" + str + "\", " + i + ")");
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r15 = this;
                    java.lang.String r0 = "IOException"
                    java.lang.String r1 = "yoyo"
                    r2 = 404(0x194, float:5.66E-43)
                    r3 = 0
                    java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    java.lang.String r5 = r2     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    r4.<init>(r5)     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    java.net.URLConnection r4 = r4.openConnection()     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    goto L_0x0023
                L_0x0015:
                    int r4 = r3
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r0, r2, r4)
                    goto L_0x0022
                L_0x001b:
                    int r4 = r3
                    java.lang.String r5 = "MalformedURLException"
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r5, r2, r4)
                L_0x0022:
                    r4 = r3
                L_0x0023:
                    if (r4 == 0) goto L_0x00d9
                    java.lang.String r5 = "Accept-Encoding"
                    java.lang.String r6 = "identity"
                    r4.setRequestProperty(r5, r6)     // Catch:{ Exception -> 0x00bb }
                    r5 = 1
                    r4.setDoInput(r5)     // Catch:{ Exception -> 0x00bb }
                    r5 = 0
                    r4.setDoOutput(r5)     // Catch:{ Exception -> 0x00bb }
                    r4.setUseCaches(r5)     // Catch:{ Exception -> 0x00bb }
                    r4.connect()     // Catch:{ Exception -> 0x00bb }
                    int r5 = r4.getResponseCode()     // Catch:{ Exception -> 0x00bb }
                    r6 = 200(0xc8, float:2.8E-43)
                    if (r5 != r6) goto L_0x0067
                    java.io.InputStream r6 = r4.getInputStream()     // Catch:{ Exception -> 0x00bb }
                    r7 = 4096(0x1000, float:5.74E-42)
                    byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x00bb }
                    java.net.URL r8 = r4.getURL()     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r14 = r8.toString()     // Catch:{ Exception -> 0x00bb }
                L_0x0052:
                    int r9 = r6.read(r7)     // Catch:{ Exception -> 0x00bb }
                    r8 = -1
                    if (r9 == r8) goto L_0x007c
                    int r10 = r3     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r12 = ""
                    int r13 = r4.getContentLength()     // Catch:{ Exception -> 0x00bb }
                    r8 = r7
                    r11 = r14
                    com.yoyogames.runner.RunnerJNILib.HttpProgress(r8, r9, r10, r11, r12, r13)     // Catch:{ Exception -> 0x00bb }
                    goto L_0x0052
                L_0x0067:
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
                    r6.<init>()     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r7 = "Received responseCode "
                    r6.append(r7)     // Catch:{ Exception -> 0x00bb }
                    r6.append(r5)     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00bb }
                    android.util.Log.i(r1, r6)     // Catch:{ Exception -> 0x00bb }
                    r14 = r3
                L_0x007c:
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
                    r6.<init>()     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r7 = "http_get responseCode="
                    r6.append(r7)     // Catch:{ Exception -> 0x00bb }
                    r6.append(r5)     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r7 = ", id="
                    r6.append(r7)     // Catch:{ Exception -> 0x00bb }
                    int r7 = r3     // Catch:{ Exception -> 0x00bb }
                    r6.append(r7)     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r7 = ", finalurl="
                    r6.append(r7)     // Catch:{ Exception -> 0x00bb }
                    r6.append(r14)     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r7 = " headers="
                    r6.append(r7)     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r7 = com.yoyogames.runner.RunnerJNILib.HttpGetHeaders(r4)     // Catch:{ Exception -> 0x00bb }
                    r6.append(r7)     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00bb }
                    android.util.Log.i(r1, r6)     // Catch:{ Exception -> 0x00bb }
                    int r6 = r3     // Catch:{ Exception -> 0x00bb }
                    java.lang.String r7 = com.yoyogames.runner.RunnerJNILib.HttpGetHeaders(r4)     // Catch:{ Exception -> 0x00bb }
                    com.yoyogames.runner.RunnerJNILib.HttpResult(r3, r5, r6, r14, r7)     // Catch:{ Exception -> 0x00bb }
                    r4.disconnect()     // Catch:{ Exception -> 0x00bb }
                    goto L_0x00d9
                L_0x00bb:
                    r3 = move-exception
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "Exception = "
                    r4.append(r5)
                    java.lang.String r3 = r3.toString()
                    r4.append(r3)
                    java.lang.String r3 = r4.toString()
                    android.util.Log.i(r1, r3)
                    int r1 = r3
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r0, r2, r1)
                L_0x00d9:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.yoyogames.runner.RunnerJNILib.AnonymousClass22.run():void");
            }
        }).start();
    }

    public static void HttpPost(final String str, final String str2, final int i) {
        Log.i("yoyo", "HttpPost(\"" + str + "\", \"" + str2 + "\", " + i + ")");
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r13 = this;
                    java.lang.String r0 = "yoyo"
                    java.lang.String r1 = "IOException"
                    r2 = 404(0x194, float:5.66E-43)
                    r3 = 0
                    java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    java.lang.String r5 = r2     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    r4.<init>(r5)     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    java.net.URLConnection r4 = r4.openConnection()     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ MalformedURLException -> 0x001b, IOException -> 0x0015 }
                    goto L_0x0023
                L_0x0015:
                    int r4 = r4
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r1, r2, r4)
                    goto L_0x0022
                L_0x001b:
                    int r4 = r4
                    java.lang.String r5 = "MalformedURLException"
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r5, r2, r4)
                L_0x0022:
                    r4 = r3
                L_0x0023:
                    if (r4 == 0) goto L_0x00bb
                    r5 = 1
                    r4.setDoInput(r5)     // Catch:{ Exception -> 0x00b6 }
                    r4.setDoOutput(r5)     // Catch:{ Exception -> 0x00b6 }
                    r6 = 0
                    r4.setUseCaches(r6)     // Catch:{ Exception -> 0x00b6 }
                    java.lang.String r7 = "POST"
                    r4.setRequestMethod(r7)     // Catch:{ Exception -> 0x00b6 }
                    r4.connect()     // Catch:{ Exception -> 0x00b6 }
                    java.lang.String r7 = r3     // Catch:{ Exception -> 0x00b6 }
                    java.lang.String r8 = "UTF-8"
                    byte[] r7 = r7.getBytes(r8)     // Catch:{ Exception -> 0x00b6 }
                    java.io.OutputStream r8 = r4.getOutputStream()     // Catch:{ Exception -> 0x00b6 }
                    r8.write(r7)     // Catch:{ Exception -> 0x00b6 }
                    java.io.OutputStream r7 = r4.getOutputStream()     // Catch:{ Exception -> 0x00b6 }
                    r7.flush()     // Catch:{ Exception -> 0x00b6 }
                    java.io.OutputStream r7 = r4.getOutputStream()     // Catch:{ Exception -> 0x00b6 }
                    r7.close()     // Catch:{ Exception -> 0x00b6 }
                    int r7 = r4.getResponseCode()     // Catch:{ Exception -> 0x00b6 }
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b6 }
                    r8.<init>()     // Catch:{ Exception -> 0x00b6 }
                    java.lang.String r9 = "HttpPost: Got response code '"
                    r8.append(r9)     // Catch:{ Exception -> 0x00b6 }
                    r8.append(r7)     // Catch:{ Exception -> 0x00b6 }
                    java.lang.String r9 = "'"
                    r8.append(r9)     // Catch:{ Exception -> 0x00b6 }
                    java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00b6 }
                    android.util.Log.i(r0, r8)     // Catch:{ Exception -> 0x00b6 }
                    java.io.InputStream r8 = r4.getInputStream()     // Catch:{ IOException -> 0x009a }
                    java.io.ByteArrayOutputStream r9 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x009a }
                    r9.<init>()     // Catch:{ IOException -> 0x009a }
                    r10 = 4096(0x1000, float:5.74E-42)
                    byte[] r10 = new byte[r10]     // Catch:{ IOException -> 0x009a }
                L_0x007f:
                    int r11 = r8.read(r10)     // Catch:{ IOException -> 0x009a }
                    r12 = -1
                    if (r11 == r12) goto L_0x008a
                    r9.write(r10, r6, r11)     // Catch:{ IOException -> 0x009a }
                    goto L_0x007f
                L_0x008a:
                    byte[] r8 = r9.toByteArray()     // Catch:{ IOException -> 0x009a }
                    r9.close()     // Catch:{ IOException -> 0x009b }
                    java.net.URL r9 = r4.getURL()     // Catch:{ IOException -> 0x009b }
                    java.lang.String r3 = r9.toString()     // Catch:{ IOException -> 0x009b }
                    goto L_0x00a0
                L_0x009a:
                    r8 = r3
                L_0x009b:
                    java.lang.String r9 = "HttpPost: IO exception"
                    android.util.Log.i(r0, r9)     // Catch:{ Exception -> 0x00b6 }
                L_0x00a0:
                    if (r8 == 0) goto L_0x00a5
                    int r0 = r8.length     // Catch:{ Exception -> 0x00b6 }
                    if (r0 != 0) goto L_0x00a9
                L_0x00a5:
                    byte[] r8 = new byte[r5]     // Catch:{ Exception -> 0x00b6 }
                    r8[r6] = r6     // Catch:{ Exception -> 0x00b6 }
                L_0x00a9:
                    int r0 = r4     // Catch:{ Exception -> 0x00b6 }
                    java.lang.String r5 = com.yoyogames.runner.RunnerJNILib.HttpGetHeaders(r4)     // Catch:{ Exception -> 0x00b6 }
                    com.yoyogames.runner.RunnerJNILib.HttpResult(r8, r7, r0, r3, r5)     // Catch:{ Exception -> 0x00b6 }
                    r4.disconnect()     // Catch:{ Exception -> 0x00b6 }
                    goto L_0x00bb
                L_0x00b6:
                    int r0 = r4
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r1, r2, r0)
                L_0x00bb:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.yoyogames.runner.RunnerJNILib.AnonymousClass23.run():void");
            }
        }).start();
    }

    public static void HttpRequest(String str, String str2, String str3, byte[] bArr, int i) {
        Log.i("yoyo", "HttpRequest(\"" + str + "\", \"" + str2 + "\", \"" + bArr + "\", " + i + ")");
        final String str4 = str;
        final int i2 = i;
        final String str5 = str2;
        final String str6 = str3;
        final byte[] bArr2 = bArr;
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:20:0x0066 A[Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }] */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x00f8 A[SYNTHETIC, Splitter:B:36:0x00f8] */
            /* JADX WARNING: Removed duplicated region for block: B:41:0x0101 A[Catch:{ IOException -> 0x00fd }] */
            /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x0029  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r15 = this;
                    java.lang.String r0 = ": "
                    java.lang.String r1 = "GET"
                    java.lang.String r2 = "yoyo"
                    r3 = 0
                    r4 = 404(0x194, float:5.66E-43)
                    java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x001f, IOException -> 0x0017 }
                    java.lang.String r6 = r2     // Catch:{ MalformedURLException -> 0x001f, IOException -> 0x0017 }
                    r5.<init>(r6)     // Catch:{ MalformedURLException -> 0x001f, IOException -> 0x0017 }
                    java.net.URLConnection r5 = r5.openConnection()     // Catch:{ MalformedURLException -> 0x001f, IOException -> 0x0017 }
                    java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ MalformedURLException -> 0x001f, IOException -> 0x0017 }
                    goto L_0x0027
                L_0x0017:
                    int r5 = r3
                    java.lang.String r6 = "IOException"
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r6, r4, r5)
                    goto L_0x0026
                L_0x001f:
                    int r5 = r3
                    java.lang.String r6 = "MalformedURLException"
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r6, r4, r5)
                L_0x0026:
                    r5 = r3
                L_0x0027:
                    if (r5 == 0) goto L_0x0178
                    r6 = 1
                    r5.setDoInput(r6)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r7 = r4     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    boolean r7 = r7.equals(r1)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r8 = 0
                    if (r7 != 0) goto L_0x004a
                    java.lang.String r7 = r4     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r9 = "HEAD"
                    boolean r7 = r7.equals(r9)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    if (r7 == 0) goto L_0x0041
                    goto L_0x004a
                L_0x0041:
                    java.lang.String r7 = "Setting do output to true"
                    android.util.Log.i(r2, r7)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r5.setDoOutput(r6)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    goto L_0x0052
                L_0x004a:
                    r5.setDoOutput(r8)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r7 = "Setting do output to false"
                    android.util.Log.i(r2, r7)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                L_0x0052:
                    r5.setUseCaches(r8)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r7 = r4     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r5.setRequestMethod(r7)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r7 = r5     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r9 = "\r\n"
                    java.lang.String[] r7 = r7.split(r9)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    int r9 = r7.length     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r10 = 0
                L_0x0064:
                    if (r10 >= r9) goto L_0x00ad
                    r11 = r7[r10]     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String[] r12 = r11.split(r0)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    int r13 = r12.length     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r14 = 2
                    if (r13 != r14) goto L_0x0096
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r11.<init>()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r13 = "HttpRequest: Found header "
                    r11.append(r13)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r13 = r12[r8]     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r11.append(r13)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r11.append(r0)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r13 = r12[r6]     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r11.append(r13)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r11 = r11.toString()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    android.util.Log.i(r2, r11)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r11 = r12[r8]     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r12 = r12[r6]     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r5.setRequestProperty(r11, r12)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    goto L_0x00aa
                L_0x0096:
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r12.<init>()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r13 = "HttpRequest: Malformed header "
                    r12.append(r13)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r12.append(r11)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r11 = r12.toString()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    android.util.Log.i(r2, r11)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                L_0x00aa:
                    int r10 = r10 + 1
                    goto L_0x0064
                L_0x00ad:
                    r5.connect()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    byte[] r0 = r6     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    if (r0 == 0) goto L_0x00d3
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    boolean r0 = r0.equals(r1)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    if (r0 != 0) goto L_0x00d3
                    java.io.OutputStream r0 = r5.getOutputStream()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    byte[] r1 = r6     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.io.OutputStream r0 = r5.getOutputStream()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r0.flush()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.io.OutputStream r0 = r5.getOutputStream()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r0.close()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                L_0x00d3:
                    int r0 = r5.getResponseCode()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r1.<init>()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r7 = "HttpRequest: Got response code '"
                    r1.append(r7)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r1.append(r0)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r7 = "'"
                    r1.append(r7)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r1 = r1.toString()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    android.util.Log.i(r2, r1)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.io.InputStream r1 = r5.getInputStream()     // Catch:{ IOException -> 0x00f5 }
                    goto L_0x00f6
                L_0x00f5:
                    r1 = r3
                L_0x00f6:
                    if (r1 != 0) goto L_0x00ff
                    java.io.InputStream r1 = r5.getErrorStream()     // Catch:{ IOException -> 0x00fd }
                    goto L_0x00ff
                L_0x00fd:
                    r1 = move-exception
                    goto L_0x011d
                L_0x00ff:
                    if (r1 == 0) goto L_0x0131
                    java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x00fd }
                    r7.<init>()     // Catch:{ IOException -> 0x00fd }
                    r9 = 4096(0x1000, float:5.74E-42)
                    byte[] r9 = new byte[r9]     // Catch:{ IOException -> 0x00fd }
                L_0x010a:
                    int r10 = r1.read(r9)     // Catch:{ IOException -> 0x00fd }
                    r11 = -1
                    if (r10 == r11) goto L_0x0115
                    r7.write(r9, r8, r10)     // Catch:{ IOException -> 0x00fd }
                    goto L_0x010a
                L_0x0115:
                    byte[] r3 = r7.toByteArray()     // Catch:{ IOException -> 0x00fd }
                    r7.close()     // Catch:{ IOException -> 0x00fd }
                    goto L_0x0131
                L_0x011d:
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r7.<init>()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r9 = "HttpRequest: IO exception:"
                    r7.append(r9)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r7.append(r1)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r1 = r7.toString()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    android.util.Log.i(r2, r1)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                L_0x0131:
                    if (r3 == 0) goto L_0x0136
                    int r1 = r3.length     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    if (r1 != 0) goto L_0x013a
                L_0x0136:
                    byte[] r3 = new byte[r6]     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r3[r8] = r8     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                L_0x013a:
                    java.net.URL r1 = r5.getURL()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r1 = r1.toString()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    int r6 = r3     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    java.lang.String r7 = com.yoyogames.runner.RunnerJNILib.HttpGetHeaders(r5)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    com.yoyogames.runner.RunnerJNILib.HttpResult(r3, r0, r6, r1, r7)     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    r5.disconnect()     // Catch:{ SocketTimeoutException -> 0x016c, Exception -> 0x014f }
                    goto L_0x0178
                L_0x014f:
                    r0 = move-exception
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    r1.<init>()
                    java.lang.String r3 = "HttpRequest: exception:"
                    r1.append(r3)
                    r1.append(r0)
                    java.lang.String r0 = r1.toString()
                    android.util.Log.i(r2, r0)
                    int r0 = r3
                    java.lang.String r1 = "HTTP request exception"
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r1, r4, r0)
                    goto L_0x0178
                L_0x016c:
                    java.lang.String r0 = "HttpRequest: request timed out"
                    android.util.Log.i(r2, r0)
                    int r0 = r3
                    java.lang.String r1 = "HTTP request timed out"
                    com.yoyogames.runner.RunnerJNILib.HttpResultString(r1, r4, r0)
                L_0x0178:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.yoyogames.runner.RunnerJNILib.AnonymousClass24.run():void");
            }
        }).start();
    }

    @SuppressLint({"InflateParams"})
    public static void ShowLogin(final String str, final String str2, final int i) {
        Log.i("yoyo", "LoginDialog(\"" + str + "\", \"" + str2 + "\"," + i + ")");
        RunnerActivity.FocusOverride = true;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerActivity.CurrentActivity);
                View inflate = LayoutInflater.from(RunnerActivity.CurrentActivity).inflate((int) R.layout.userpasslayout, (ViewGroup) null);
                builder.setView(inflate);
                final EditText editText = (EditText) inflate.findViewById(R.id.username);
                final EditText editText2 = (EditText) inflate.findViewById(R.id.password);
                editText.setText(str);
                editText2.setText(str2);
                builder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerJNILib.LoginResult(editText.getText().toString() + '#' + editText2.getText().toString(), editText.getText().toString(), i);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunnerJNILib.LoginResult("", "", i);
                    }
                });
                builder.create().show();
            }
        });
    }

    public static int CheckPermission(String str) {
        Log.i("yoyo", "Checking permission: " + str);
        ms_context.getPackageManager();
        if (ContextCompat.checkSelfPermission(RunnerActivity.CurrentActivity, str) == 0) {
            Log.i("yoyo", "permission granted: " + str);
            return 1;
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(RunnerActivity.CurrentActivity, str)) {
            Log.i("yoyo", "permission denied but not prevented from asking: " + str);
            return 0;
        } else {
            Log.i("yoyo", "permission denied and shouldn't be requested: " + str);
            return -1;
        }
    }

    public static void RequestPermission(String str) {
        ActivityCompat.requestPermissions(RunnerActivity.CurrentActivity, new String[]{str}, 97);
    }

    @SuppressLint({"MissingPermission"})
    public static boolean isNetworkConnected() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) ms_context.getSystemService("connectivity");
        if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static void setSystemUIVisibilityFlags(final int i) {
        Log.i("yoyo", "Calling setSystemUIVisibilityFlags");
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                RunnerActivity.UIVisibilityFlags = i;
                RunnerActivity.CurrentActivity.setupUiVisibility();
                RunnerActivity.CurrentActivity.setupUiVisibilityDelayed();
            }
        });
    }

    public static void powersaveEnable(final boolean z) {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (z) {
                    RunnerActivity.CurrentActivity.getWindow().clearFlags(128);
                } else {
                    RunnerActivity.CurrentActivity.getWindow().addFlags(128);
                }
            }
        });
    }

    public static void MoveTaskToBack() {
        RunnerActivity.CurrentActivity.runOnUiThread(new Runnable() {
            public void run() {
                RunnerActivity.CurrentActivity.moveTaskToBack(true);
            }
        });
    }

    public static void cloudStringSave(String str, String str2, int i) {
        if (RunnerActivity.isLoggedInGooglePlay()) {
            CallExtensionFunction("GooglePlayServicesExtension", "onGSStringSave", 3, new Object[]{str, str2, Integer.valueOf(i)});
            return;
        }
        Log.i("yoyo", "cloud_string_save() called when not logged in to appropriate service");
    }

    public static void cloudSynchronise(int i) {
        if (RunnerActivity.isLoggedInGooglePlay()) {
            CallExtensionFunction("GooglePlayServicesExtension", "onGSCloudSync", 1, new Object[]{Integer.valueOf(i)});
            return;
        }
        Log.i("yoyo", "cloudSynchronise called when not logged in to appropriate service");
    }

    public static void DumpUsedMemory() {
        Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
        Debug.getMemoryInfo(memoryInfo);
        Locale locale = Locale.US;
        double totalPss = (double) memoryInfo.getTotalPss();
        Double.isNaN(totalPss);
        double totalPrivateDirty = (double) memoryInfo.getTotalPrivateDirty();
        Double.isNaN(totalPrivateDirty);
        double totalSharedDirty = (double) memoryInfo.getTotalSharedDirty();
        Double.isNaN(totalSharedDirty);
        Log.i("yoyo", String.format(locale, "App Memory: Pss=%.2f MB\nPrivate=%.2f MB\nShared=%.2f MB", Double.valueOf(totalPss / 1024.0d), Double.valueOf(totalPrivateDirty / 1024.0d), Double.valueOf(totalSharedDirty / 1024.0d)));
    }

    public static Context GetApplicationContext() {
        return ms_context;
    }

    public static void PushLocalNotification(float f, String str, String str2, String str3) {
        CallExtensionFunction("GooglePushNotificationsExtension", "pushLocalNotification", 4, new Object[]{Float.valueOf(f), str, str2, str3});
    }

    public static int PushGetLocalNotification(int i, int i2) {
        Object CallExtensionFunction = CallExtensionFunction("GooglePushNotificationsExtension", "pushGetLocalNotification", 2, new Object[]{Integer.valueOf(i), Integer.valueOf(i2)});
        if (CallExtensionFunction != null) {
            return ((Integer) CallExtensionFunction).intValue();
        }
        return -1;
    }

    public static int PushCancelLocalNotification(int i) {
        Object CallExtensionFunction = CallExtensionFunction("GooglePushNotificationsExtension", "pushCancelLocalNotification", 1, new Object[]{Integer.valueOf(i)});
        if (CallExtensionFunction != null) {
            return ((Integer) CallExtensionFunction).intValue();
        }
        return -1;
    }

    public static void RestrictOrientation(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        RunnerActivity.CurrentActivity.RestrictOrientation(z, z2, z3, z4, z5);
    }

    public static String GetUDID() {
        String string = Settings.Secure.getString(ms_context.getContentResolver(), "android_id");
        return string == null ? "UDID NOT AVAILABLE" : string;
    }

    public static void SetLaunchedFromPlayer(String str, boolean z) {
        RunnerActivity.CurrentActivity.SetLaunchedFromPlayer(str, z);
    }

    public static void VirtualKeyboardToggle(boolean z, int i, int i2, int i3, boolean z2, int[] iArr) {
        RunnerKeyboardController GetKeyboardController = RunnerActivity.CurrentActivity.GetKeyboardController();
        if (GetKeyboardController != null) {
            GetKeyboardController.VirtualKeyboardToggle(z, i, i2, i3, z2, iArr);
        }
    }

    public static boolean VirtualKeyboardGetStatus() {
        RunnerKeyboardController GetKeyboardController = RunnerActivity.CurrentActivity.GetKeyboardController();
        if (GetKeyboardController != null) {
            return GetKeyboardController.VirtualKeyboardGetStatus();
        }
        return false;
    }

    public static int VirtualKeyboardGetHeight() {
        RunnerKeyboardController GetKeyboardController = RunnerActivity.CurrentActivity.GetKeyboardController();
        if (GetKeyboardController != null) {
            return GetKeyboardController.GetVirtualKeyboardHeightCached();
        }
        return 0;
    }

    public static void OnKeyboardStringSet(final int[] iArr) {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                RunnerKeyboardController GetKeyboardController = RunnerActivity.CurrentActivity.GetKeyboardController();
                if (GetKeyboardController != null) {
                    GetKeyboardController.SetInputString(iArr, true);
                }
            }
        });
    }

    public static void EnumerateGamepadDevices() {
        Gamepad.EnumerateDevices();
    }
}
