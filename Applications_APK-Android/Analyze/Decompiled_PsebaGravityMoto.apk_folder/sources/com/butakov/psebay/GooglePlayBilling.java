package com.butakov.psebay;

import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GooglePlayBilling {
    private final int EXT_ERROR_NOT_INITIALISED = 1;
    private final int EXT_ERROR_NO_SKUS = 2;
    private final int EXT_ERROR_SELECTED_SKU_LIST_EMPTY = 3;
    private final int EXT_ERROR_UNKNOWN = -1;
    private final int EXT_NO_ERROR = 0;
    private List<String> m_iapList = null;
    private GooglePlayBillingService m_runnerBilling;
    private List<String> m_subList = null;

    private String _statusToString(int i) {
        return i != 1 ? i != 2 ? i != 3 ? "Error Unknown." : "Selected SKU List was empty" : "IAP List is empty." : "Google Play Billing has not been initialised.";
    }

    public double GPBilling_Init() {
        this.m_runnerBilling = new GooglePlayBillingService();
        return 0.0d;
    }

    public double GPBilling_ConnectToStore() {
        GooglePlayBillingService googlePlayBillingService = this.m_runnerBilling;
        if (googlePlayBillingService == null) {
            return 1.0d;
        }
        googlePlayBillingService.loadStore();
        return 0.0d;
    }

    public double GPBilling_IsStoreConnected() {
        GooglePlayBillingService googlePlayBillingService = this.m_runnerBilling;
        if (googlePlayBillingService != null) {
            return googlePlayBillingService.isStoreConnected() ? 1.0d : 0.0d;
        }
        return 0.0d;
    }

    public double GPBilling_GetStatus() {
        return (double) getStatus();
    }

    public double GPBilling_AddProduct(String str) {
        if (this.m_iapList == null) {
            this.m_iapList = new ArrayList();
        }
        if (this.m_iapList.contains(str)) {
            return -1.0d;
        }
        this.m_iapList.add(str);
        return 0.0d;
    }

    public double GPBilling_AddSubscription(String str) {
        if (this.m_subList == null) {
            this.m_subList = new ArrayList();
        }
        if (this.m_subList.contains(str)) {
            return -1.0d;
        }
        this.m_subList.add(str);
        return 0.0d;
    }

    public double GPBilling_QueryProducts() {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        List<String> list = this.m_iapList;
        if (list == null) {
            return 3.0d;
        }
        return (double) this.m_runnerBilling.retrieveManagedProducts(list);
    }

    public double GPBilling_QuerySubscriptions() {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        List<String> list = this.m_subList;
        if (list == null) {
            return 3.0d;
        }
        return (double) this.m_runnerBilling.retrieveSubscriptions(list);
    }

    public String GPBilling_QueryPurchases(String str) {
        if (getStatus() > 0) {
            return "";
        }
        return this.m_runnerBilling.queryPurchases(str);
    }

    public double GPBilling_PurchaseProduct(String str) {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        this.m_runnerBilling.purchaseCatalogItem(str);
        return 0.0d;
    }

    public double GPBilling_PurchaseSubscription(String str) {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        this.m_runnerBilling.purchaseSubscription(str);
        return 0.0d;
    }

    public double GPBilling_AcknowledgePurchase(String str) {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        this.m_runnerBilling.acknowledgePurchase(str);
        return 0.0d;
    }

    public double GPBilling_ConsumeProduct(String str) {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        this.m_runnerBilling.consumeProduct(str);
        return 0.0d;
    }

    public String GPBilling_Sku_GetDescription(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getDescription();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getDescription();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetFreeTrialPeriod(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getFreeTrialPeriod();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getFreeTrialPeriod();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetIconUrl(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getIconUrl();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getIconUrl();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetIntroductoryPrice(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getIntroductoryPrice();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getIntroductoryPrice();
            }
        }
        return "";
    }

    public double GPBilling_Sku_GetIntroductoryPriceAmountMicros(String str) {
        long introductoryPriceAmountMicros;
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        Iterator<SkuDetails> it = this.m_runnerBilling.GetSkuDetails().iterator();
        while (true) {
            if (it.hasNext()) {
                SkuDetails next = it.next();
                if (next.getSku().equals(str)) {
                    introductoryPriceAmountMicros = next.getIntroductoryPriceAmountMicros();
                    break;
                }
            } else {
                for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
                    if (next2.getSku().equals(str)) {
                        introductoryPriceAmountMicros = next2.getIntroductoryPriceAmountMicros();
                    }
                }
                return -1.0d;
            }
        }
        return (double) introductoryPriceAmountMicros;
    }

    public String GPBilling_Sku_GetIntroductoryPriceCycles(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getIntroductoryPrice();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getIntroductoryPrice();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetIntroductoryPricePeriod(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getIntroductoryPricePeriod();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getIntroductoryPricePeriod();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetOriginalJson(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getOriginalJson();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getOriginalJson();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetOriginalPrice(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getOriginalPrice();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getOriginalPrice();
            }
        }
        return "";
    }

    public double GPBilling_Sku_GetOriginalPriceAmountMicros(String str) {
        long originalPriceAmountMicros;
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        Iterator<SkuDetails> it = this.m_runnerBilling.GetSkuDetails().iterator();
        while (true) {
            if (it.hasNext()) {
                SkuDetails next = it.next();
                if (next.getSku().equals(str)) {
                    originalPriceAmountMicros = next.getOriginalPriceAmountMicros();
                    break;
                }
            } else {
                for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
                    if (next2.getSku().equals(str)) {
                        originalPriceAmountMicros = next2.getOriginalPriceAmountMicros();
                    }
                }
                return -1.0d;
            }
        }
        return (double) originalPriceAmountMicros;
    }

    public String GPBilling_Sku_GetPrice(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getPrice();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getPrice();
            }
        }
        return "";
    }

    public double GPBilling_Sku_GetPriceAmountMicros(String str) {
        long priceAmountMicros;
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        Iterator<SkuDetails> it = this.m_runnerBilling.GetSkuDetails().iterator();
        while (true) {
            if (it.hasNext()) {
                SkuDetails next = it.next();
                if (next.getSku().equals(str)) {
                    priceAmountMicros = next.getPriceAmountMicros();
                    break;
                }
            } else {
                for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
                    if (next2.getSku().equals(str)) {
                        priceAmountMicros = next2.getPriceAmountMicros();
                    }
                }
                return -1.0d;
            }
        }
        return (double) priceAmountMicros;
    }

    public String GPBilling_Sku_GetPriceCurrencyCode(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getPriceCurrencyCode();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getPriceCurrencyCode();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetSku(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getSku();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getSku();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetSubscriptionPeriod(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getSubscriptionPeriod();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getSubscriptionPeriod();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetTitle(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getTitle();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getTitle();
            }
        }
        return "";
    }

    public String GPBilling_Sku_GetType(String str) {
        if (getStatus() > 0) {
            return "";
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.getType();
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.getType();
            }
        }
        return "";
    }

    public double GPBilling_Sku_IsRewarded(String str) {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        for (SkuDetails next : this.m_runnerBilling.GetSkuDetails()) {
            if (next.getSku().equals(str)) {
                return next.isRewarded() ? 1.0d : 0.0d;
            }
        }
        for (SkuDetails next2 : this.m_runnerBilling.GetSubSkuDetails()) {
            if (next2.getSku().equals(str)) {
                return next2.isRewarded() ? 1.0d : 0.0d;
            }
        }
        return -1.0d;
    }

    public double GPBilling_Purchase_GetState(String str) {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        Purchase purchase = this.m_runnerBilling.GetPurchases().get(str);
        if (purchase == null) {
            return -1.0d;
        }
        int purchaseState = purchase.getPurchaseState();
        if (purchaseState == 1) {
            return 13001.0d;
        }
        return purchaseState == 2 ? 13002.0d : 13000.0d;
    }

    public String GPBilling_Purchase_GetSignature(String str) {
        Purchase purchase;
        if (getStatus() <= 0 && (purchase = this.m_runnerBilling.GetPurchases().get(str)) != null) {
            return purchase.getSignature();
        }
        return "";
    }

    public String GPBilling_Purchase_GetOriginalJson(String str) {
        Purchase purchase;
        if (getStatus() <= 0 && (purchase = this.m_runnerBilling.GetPurchases().get(str)) != null) {
            return purchase.getOriginalJson();
        }
        return "";
    }

    public double GPBilling_Purchase_VerifySignature(String str, String str2) {
        int status = getStatus();
        if (status > 0) {
            return (double) status;
        }
        return RunnerBillingSecurity.verifyPurchase(str, str2) ? 1.0d : 0.0d;
    }

    private int getStatus() {
        if (this.m_runnerBilling == null) {
            return 1;
        }
        return (this.m_iapList == null && this.m_subList == null) ? 2 : 0;
    }
}
