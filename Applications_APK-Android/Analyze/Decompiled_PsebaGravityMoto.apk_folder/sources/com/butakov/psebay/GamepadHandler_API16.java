package com.butakov.psebay;

import android.annotation.TargetApi;
import android.view.InputDevice;

@TargetApi(16)
/* compiled from: Gamepad */
class GamepadHandler_API16 extends GamepadHandler_API12 {
    GamepadHandler_API16() {
    }

    public String GetJoystickDescriptor(InputDevice inputDevice) {
        String descriptor = inputDevice.getDescriptor();
        if (descriptor == null || descriptor.isEmpty()) {
            return super.GetJoystickDescriptor(inputDevice);
        }
        return descriptor;
    }
}
