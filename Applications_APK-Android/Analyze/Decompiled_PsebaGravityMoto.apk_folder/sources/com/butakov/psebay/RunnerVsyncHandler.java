package com.butakov.psebay;

import android.annotation.TargetApi;
import android.view.Choreographer;

@TargetApi(16)
public class RunnerVsyncHandler implements Choreographer.FrameCallback {
    private static final Accessor accessor = new Accessor();

    public static final class Accessor {
        private Accessor() {
        }
    }

    public void doFrame(long j) {
        DemoGLSurfaceView GetGLView = RunnerActivity.CurrentActivity.GetGLView(accessor);
        if (!(GetGLView == null || GetGLView.mRenderer == null)) {
            DemoRenderer.elapsedVsyncs++;
        }
        Choreographer.getInstance().postFrameCallback(this);
    }

    public void PostFrameCallback() {
        Choreographer.getInstance().postFrameCallback(this);
    }

    public void RemoveFrameCallback() {
        Choreographer.getInstance().removeFrameCallback(this);
    }
}
