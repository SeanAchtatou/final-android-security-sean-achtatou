package com.butakov.psebay;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import androidx.core.view.InputDeviceCompat;
import com.yoyogames.runner.RunnerJNILib;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* compiled from: Gamepad */
class GamepadHandler_API12 extends GamepadHandler {
    private ArrayList<GamepadInstance> m_Gamepads = new ArrayList<>();

    public int GetButtonMask(InputDevice inputDevice) {
        return 0;
    }

    public int GetProductId(InputDevice inputDevice) {
        return 0;
    }

    public int GetVendorId(InputDevice inputDevice) {
        return 0;
    }

    /* compiled from: Gamepad */
    static class GamepadInstance {
        public ArrayList<InputDevice.MotionRange> axes;
        public int buttonMask;
        public String desc;
        public ArrayList<InputDevice.MotionRange> hats;
        public int idDevice;
        public String name;
        public int productId;
        public int vendorId;

        GamepadInstance() {
        }
    }

    /* compiled from: Gamepad */
    static class RangeComparator implements Comparator<InputDevice.MotionRange> {
        RangeComparator() {
        }

        public int compare(InputDevice.MotionRange motionRange, InputDevice.MotionRange motionRange2) {
            int axis = motionRange.getAxis();
            int axis2 = motionRange2.getAxis();
            if (axis == 22) {
                axis = 23;
            } else if (axis == 23) {
                axis = 22;
            }
            if (axis2 == 22) {
                axis2 = 23;
            } else if (axis2 == 23) {
                axis2 = 22;
            }
            return axis - axis2;
        }
    }

    public String GetJoystickDescriptor(InputDevice inputDevice) {
        return inputDevice.getName();
    }

    public GamepadInstance GetGamepad(int i) {
        for (int i2 = 0; i2 < this.m_Gamepads.size(); i2++) {
            GamepadInstance gamepadInstance = this.m_Gamepads.get(i2);
            if (gamepadInstance.idDevice == i) {
                return gamepadInstance;
            }
        }
        return null;
    }

    @SuppressLint({"MissingPermission"})
    public void PollInputDevices() {
        GamepadHandler_API12 gamepadHandler_API12 = this;
        int[] deviceIds = InputDevice.getDeviceIds();
        int i = 0;
        while (i < deviceIds.length) {
            int i2 = deviceIds[i];
            if (i2 >= 0 && gamepadHandler_API12.GetGamepad(i2) == null) {
                InputDevice device = InputDevice.getDevice(i2);
                int sources = device.getSources();
                if ((sources & 16) == 16 || (sources & InputDeviceCompat.SOURCE_GAMEPAD) == 1025 || (sources & InputDeviceCompat.SOURCE_DPAD) == 513) {
                    GamepadInstance gamepadInstance = new GamepadInstance();
                    List<InputDevice.MotionRange> motionRanges = device.getMotionRanges();
                    Collections.sort(motionRanges, new RangeComparator());
                    gamepadInstance.idDevice = i2;
                    gamepadInstance.name = device.getName();
                    gamepadInstance.desc = gamepadHandler_API12.GetJoystickDescriptor(device);
                    gamepadInstance.axes = new ArrayList<>();
                    gamepadInstance.hats = new ArrayList<>();
                    gamepadInstance.vendorId = gamepadHandler_API12.GetVendorId(device);
                    gamepadInstance.productId = gamepadHandler_API12.GetProductId(device);
                    gamepadInstance.buttonMask = gamepadHandler_API12.GetButtonMask(device);
                    for (InputDevice.MotionRange next : motionRanges) {
                        if ((next.getSource() & 16) != 0) {
                            if (next.getAxis() != 15) {
                                if (next.getAxis() != 16) {
                                    gamepadInstance.axes.add(next);
                                }
                            }
                            gamepadInstance.hats.add(next);
                        }
                    }
                    gamepadHandler_API12.m_Gamepads.add(gamepadInstance);
                    int i3 = gamepadInstance.idDevice;
                    String str = gamepadInstance.name;
                    String str2 = gamepadInstance.desc;
                    int i4 = gamepadInstance.productId;
                    RunnerJNILib.onGPDeviceAdded(i3, str, str2, i4, gamepadInstance.vendorId, gamepadInstance.axes.size(), gamepadInstance.hats.size() / 2, gamepadInstance.buttonMask);
                    Log.i("yoyo", "GAMEPAD :: found device id:" + deviceIds[i] + " name:" + gamepadInstance.name + " desc:" + gamepadInstance.desc + " productId:" + gamepadInstance.productId + " vendorId:" + gamepadInstance.vendorId + " maskButtons:" + Integer.toHexString(gamepadInstance.buttonMask) + " numHats:" + (gamepadInstance.hats.size() / 2) + " numAxes:" + gamepadInstance.axes.size());
                }
            }
            i++;
            gamepadHandler_API12 = this;
        }
        String str3 = "yoyo";
        ArrayList arrayList = new ArrayList();
        for (int i5 = 0; i5 < this.m_Gamepads.size(); i5++) {
            GamepadInstance gamepadInstance2 = this.m_Gamepads.get(i5);
            int i6 = 0;
            while (i6 < deviceIds.length && gamepadInstance2.idDevice != deviceIds[i6]) {
                i6++;
            }
            if (i6 == deviceIds.length) {
                arrayList.add(Integer.valueOf(gamepadInstance2.idDevice));
                Log.i(str3, "GAMEPAD :: removed device id:" + gamepadInstance2.idDevice + " name:" + gamepadInstance2.name + " desc:" + gamepadInstance2.desc + " productId:" + gamepadInstance2.productId + " vendorId:" + gamepadInstance2.vendorId + " maskButtons:" + Integer.toHexString(gamepadInstance2.buttonMask) + " numHats:" + (gamepadInstance2.hats.size() / 2) + " numAxes:" + gamepadInstance2.axes.size());
            }
        }
        for (int i7 = 0; i7 < arrayList.size(); i7++) {
            int intValue = ((Integer) arrayList.get(i7)).intValue();
            RunnerJNILib.onGPDeviceRemoved(intValue);
            int i8 = 0;
            while (true) {
                if (i8 >= this.m_Gamepads.size()) {
                    break;
                } else if (this.m_Gamepads.get(i8).idDevice == intValue) {
                    this.m_Gamepads.remove(i8);
                    break;
                } else {
                    i8++;
                }
            }
        }
        Log.i(str3, "GAMEPAD: Enumeration complete");
    }

    @SuppressLint({"MissingPermission"})
    public boolean HandleMotionEvent(MotionEvent motionEvent) {
        GamepadInstance GetGamepad;
        if ((motionEvent.getSource() & InputDeviceCompat.SOURCE_JOYSTICK) == 0) {
            return true;
        }
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getActionMasked() != 2 || (GetGamepad = GetGamepad(motionEvent.getDeviceId())) == null) {
            return true;
        }
        for (int i = 0; i < GetGamepad.axes.size(); i++) {
            InputDevice.MotionRange motionRange = GetGamepad.axes.get(i);
            RunnerJNILib.onGPNativeAxis(GetGamepad.idDevice, i, (((motionEvent.getAxisValue(motionRange.getAxis(), actionIndex) - motionRange.getMin()) / motionRange.getRange()) * 2.0f) - 1.0f);
        }
        for (int i2 = 0; i2 < GetGamepad.hats.size(); i2 += 2) {
            RunnerJNILib.onGPNativeHat(GetGamepad.idDevice, i2 / 2, (float) Math.round(motionEvent.getAxisValue(GetGamepad.hats.get(i2).getAxis(), actionIndex)), (float) Math.round(motionEvent.getAxisValue(GetGamepad.hats.get(i2 + 1).getAxis(), actionIndex)));
        }
        return true;
    }

    @SuppressLint({"MissingPermission"})
    public boolean HandleKeyEvent(int i, KeyEvent keyEvent) {
        GamepadInstance GetGamepad = GetGamepad(i);
        if (GetGamepad != null) {
            int action = keyEvent.getAction();
            if (action == 0) {
                RunnerJNILib.onGPKeyDown(GetGamepad.idDevice, keyEvent.getKeyCode());
                return true;
            } else if (action == 1) {
                RunnerJNILib.onGPKeyUp(GetGamepad.idDevice, keyEvent.getKeyCode());
                return true;
            }
        }
        return false;
    }
}
