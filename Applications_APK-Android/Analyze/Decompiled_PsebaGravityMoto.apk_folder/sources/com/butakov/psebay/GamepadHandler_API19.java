package com.butakov.psebay;

import android.annotation.TargetApi;
import android.view.InputDevice;
import com.google.android.gms.drive.DriveFile;
import org.apache.http.HttpStatus;

@TargetApi(19)
/* compiled from: Gamepad */
class GamepadHandler_API19 extends GamepadHandler_API16 {
    GamepadHandler_API19() {
    }

    public int GetProductId(InputDevice inputDevice) {
        return inputDevice.getProductId();
    }

    public int GetVendorId(InputDevice inputDevice) {
        return inputDevice.getVendorId();
    }

    public int GetButtonMask(InputDevice inputDevice) {
        int[] iArr = {96, 97, 99, 100, 4, 110, 108, 106, 107, 102, 103, 19, 20, 21, 22, 109, 23, 104, 105, 98, 101, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, HttpStatus.SC_OK, HttpStatus.SC_CREATED, HttpStatus.SC_ACCEPTED, HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION};
        int[] iArr2 = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 16, 1, 32768, 65536, 131072, 262144, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, DriveFile.MODE_READ_ONLY, DriveFile.MODE_WRITE_ONLY, 1073741824, Integer.MIN_VALUE, -1, -1, -1, -1};
        boolean[] hasKeys = inputDevice.hasKeys(iArr);
        int i = 0;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            if (hasKeys[i2]) {
                i |= iArr2[i2];
            }
        }
        return i;
    }
}
