package com.butakov.psebay;

import android.os.Bundle;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import org.ini4j.Ini;
import org.ini4j.Profile;

class IniBundle {
    Profile.Section m_android = null;
    Bundle m_bundle;

    public IniBundle(Bundle bundle, InputStream inputStream) {
        this.m_bundle = bundle;
        if (inputStream != null) {
            try {
                this.m_android = (Profile.Section) new Ini(inputStream).get("Android");
            } catch (IOException e) {
                Log.d("yoyo", "INI exception " + e.toString());
            }
        }
    }

    public String getString(String str) {
        int i;
        Profile.Section section = this.m_android;
        if (section == null || !section.containsKey(str)) {
            String string = this.m_bundle.getString(str);
            return (string != null || (i = this.m_bundle.getInt(str, -9876543)) == -9876543) ? string : Integer.toString(i);
        }
        String str2 = (String) this.m_android.get(str);
        if (!str2.startsWith("\"") || !str2.endsWith("\"")) {
            return str2;
        }
        return str2.substring(1, str2.length() - 1);
    }

    public boolean getBoolean(String str) {
        Profile.Section section = this.m_android;
        if (section == null || !section.containsKey(str)) {
            return this.m_bundle.getBoolean(str);
        }
        return Boolean.parseBoolean((String) this.m_android.get(str));
    }

    public int getInt(String str) {
        Profile.Section section = this.m_android;
        if (section == null || !section.containsKey(str)) {
            return this.m_bundle.getInt(str);
        }
        return Integer.parseInt((String) this.m_android.get(str));
    }

    public boolean keyExists(String str) {
        Profile.Section section = this.m_android;
        if (section == null || !section.containsKey(str)) {
            return this.m_bundle.containsKey(str);
        }
        return true;
    }

    public boolean hasAndroidIni() {
        return this.m_android != null;
    }

    public void setAndroidIni(Profile.Section section) {
        this.m_android = section;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object}
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
      SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object} */
    public void setAndroidString(String str, String str2) {
        Profile.Section section = this.m_android;
        if (section == null) {
            Log.d("yoyo", "Could not setIniString - no INI file in current bundle.");
        } else {
            section.put((Object) str, (Object) str2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String */
    public void setAndroidInt(String str, int i) {
        Profile.Section section = this.m_android;
        if (section == null) {
            Log.d("yoyo", "Could not setIniString - no INI file in current bundle.");
        } else {
            section.put(str, (Object) Integer.valueOf(i));
        }
    }
}
