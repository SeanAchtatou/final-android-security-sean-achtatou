package com.butakov.psebay;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;

public class RunnerBillingSecurity {
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final SecureRandom RANDOM = new SecureRandom();
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static HashSet<Long> sKnownNonces = new HashSet<>();

    public static String getBase64EncodedPublicKey() {
        return RunnerActivity.BASE64_PUBLIC_KEY;
    }

    public static long generateNonce() {
        long nextLong = RANDOM.nextLong();
        sKnownNonces.add(Long.valueOf(nextLong));
        return nextLong;
    }

    public static void removeNonce(long j) {
        sKnownNonces.remove(Long.valueOf(j));
    }

    public static boolean isNonceKnown(long j) {
        return sKnownNonces.contains(Long.valueOf(j));
    }

    public static boolean verifyPurchase(String str, String str2) {
        if (str == null) {
            Log.e("yoyo", "BILLING: signedData is null");
            return false;
        } else if (TextUtils.isEmpty(str2) || verify(generatePublicKey(getBase64EncodedPublicKey()), str, str2)) {
            return true;
        } else {
            Log.w("yoyo", "BILLING: signature does not match data.");
            return false;
        }
    }

    public static PublicKey generatePublicKey(String str) {
        try {
            return KeyFactory.getInstance(KEY_FACTORY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64.decode(str, 0)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e2) {
            Log.e("yoyo", "BILLING: Invalid key specification.");
            throw new IllegalArgumentException(e2);
        } catch (IllegalArgumentException e3) {
            Log.e("yoyo", "BILLING: Base64 decoding failed.");
            throw new IllegalArgumentException(e3);
        }
    }

    public static boolean verify(PublicKey publicKey, String str, String str2) {
        try {
            Signature instance = Signature.getInstance(SIGNATURE_ALGORITHM);
            instance.initVerify(publicKey);
            instance.update(str.getBytes());
            if (instance.verify(Base64.decode(str2, 0))) {
                return true;
            }
            Log.e("yoyo", "BILLING: Signature verification failed.");
            return false;
        } catch (NoSuchAlgorithmException unused) {
            Log.e("yoyo", "BILLING: NoSuchAlgorithmException.");
            return false;
        } catch (InvalidKeyException unused2) {
            Log.e("yoyo", "BILLING: Invalid key specification.");
            return false;
        } catch (SignatureException unused3) {
            Log.e("yoyo", "BILLING: Signature exception.");
            return false;
        } catch (IllegalArgumentException unused4) {
            Log.e("yoyo", "BILLING: Base64 decoding failed.");
            return false;
        }
    }
}
