package com.butakov.psebay;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

public class RunnerAdExt implements IAdExt {
    public void disable(int i) {
    }

    public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
        return false;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return false;
    }

    public void enable(int i, int i2, int i3) {
    }

    public void event(String str) {
    }

    public int getAdDisplayHeight(int i) {
        return 0;
    }

    public int getAdDisplayWidth(int i) {
        return 0;
    }

    public void move(int i, int i2, int i3) {
    }

    public void onActivityResult(int i, int i2, Intent intent) {
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public Dialog onCreateDialog(int i) {
        return null;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    public void onDestroy() {
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        return false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onKeyLongPress(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return false;
    }

    public void onPause() {
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
    }

    public void onRestart() {
    }

    public void onResume() {
    }

    public void onStart() {
    }

    public void onStop() {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public void onWindowFocusChanged(boolean z) {
    }

    public void setup() {
    }
}
