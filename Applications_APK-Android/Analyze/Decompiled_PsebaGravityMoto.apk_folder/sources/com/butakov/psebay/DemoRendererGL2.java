package com.butakov.psebay;

import android.content.Context;
import android.util.Log;
import com.butakov.psebay.DemoRenderer;
import java.nio.IntBuffer;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class DemoRendererGL2 extends DemoRenderer {
    public DemoRendererGL2(Context context) {
        super(context);
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        if (m_state != DemoRenderer.eState.Startup) {
            Log.i("yoyo", "onSurfaceCreated() aborted on re-create 1, state is currently " + m_state);
            return;
        }
        IntBuffer allocate = IntBuffer.allocate(1);
        gl10.glGetIntegerv(36006, allocate);
        m_defaultFrameBuffer = allocate.get(0);
        Log.i("yoyo", "Renderer instance is gl2.0, framebuffer object is: " + m_defaultFrameBuffer);
        super.onSurfaceCreated(gl10, eGLConfig);
    }
}
