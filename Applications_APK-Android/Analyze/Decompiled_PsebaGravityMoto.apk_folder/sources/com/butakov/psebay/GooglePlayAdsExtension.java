package com.butakov.psebay;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.AbsoluteLayout;
import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.DebugGeography;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.yoyogames.runner.RunnerJNILib;
import java.net.MalformedURLException;
import java.net.URL;

public class GooglePlayAdsExtension extends RunnerSocial implements RewardedVideoAdListener {
    public static GooglePlayAdsExtension CurrentGoogleExtension = null;
    private static final int EVENT_OTHER_SOCIAL = 70;
    private static final int GoogleMobileAds_ASyncEvent = 9817;
    private String BannerId = "";
    /* access modifiers changed from: private */
    public AdSize BannerSize;
    private int BannerXPos;
    private int BannerYPos;
    private String InterstitialId;
    /* access modifiers changed from: private */
    public String InterstitialStatus = "Not Ready";
    /* access modifiers changed from: private */
    public String RewardedVideoStatus = "Not Ready";
    /* access modifiers changed from: private */
    public String TestDeviceId;
    /* access modifiers changed from: private */
    public AdView adView = null;
    private AdListener adlistener = new AdListener() {
        public void onAdLoaded() {
            Log.i("yoyo", "onAdLoaded called");
            GooglePlayAdsExtension.this.sendInterstitialLoadedEvent(true);
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    if (GooglePlayAdsExtension.this.interstitialAd.isLoaded()) {
                        String unused = GooglePlayAdsExtension.this.InterstitialStatus = "Ready";
                    } else {
                        String unused2 = GooglePlayAdsExtension.this.InterstitialStatus = "Not Ready";
                    }
                }
            });
        }

        public void onAdFailedToLoad(int i) {
            Log.i("yoyo", "onAdFailedToLoad called");
            GooglePlayAdsExtension.this.sendInterstitialLoadedEvent(false);
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    if (GooglePlayAdsExtension.this.interstitialAd.isLoaded()) {
                        String unused = GooglePlayAdsExtension.this.InterstitialStatus = "Ready";
                    } else {
                        String unused2 = GooglePlayAdsExtension.this.InterstitialStatus = "Not Ready";
                    }
                }
            });
        }

        public void onAdClosed() {
            GooglePlayAdsExtension.this.sendInterstitialClosedEvent();
        }
    };
    /* access modifiers changed from: private */
    public boolean bUseTestAds = false;
    /* access modifiers changed from: private */
    public InterstitialAd interstitialAd = null;
    private boolean mConsentPreferAdFree = false;
    /* access modifiers changed from: private */
    public RewardedVideoAd mRewardedVideoAd;

    public void onRewardedVideoCompleted() {
    }

    public void Init() {
        CurrentGoogleExtension = this;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (this.BannerSize == AdSize.SMART_BANNER) {
            GoogleMobileAds_RemoveBanner();
            int i = 0;
            if (this.BannerSize == AdSize.BANNER) {
                i = 1;
            } else if (this.BannerSize == AdSize.MEDIUM_RECTANGLE) {
                i = 2;
            } else if (this.BannerSize == AdSize.FULL_BANNER) {
                i = 3;
            } else if (this.BannerSize == AdSize.LEADERBOARD) {
                i = 4;
            } else if (this.BannerSize == AdSize.WIDE_SKYSCRAPER) {
                i = 5;
            } else if (this.BannerSize == AdSize.SMART_BANNER) {
                i = 6;
            }
            GoogleMobileAds_AddBannerAt(this.BannerId, (double) i, (double) this.BannerXPos, (double) this.BannerYPos);
        }
    }

    public void GoogleMobileAds_LoadRewardedVideo(final String str) {
        RewardedVideoAd rewardedVideoAd = this.mRewardedVideoAd;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (GooglePlayAdsExtension.this.mRewardedVideoAd == null) {
                    RewardedVideoAd unused = GooglePlayAdsExtension.this.mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(RunnerActivity.CurrentActivity);
                    GooglePlayAdsExtension.this.mRewardedVideoAd.setRewardedVideoAdListener(GooglePlayAdsExtension.CurrentGoogleExtension);
                }
                AdRequest.Builder builder = new AdRequest.Builder();
                builder.addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB");
                if (GooglePlayAdsExtension.this.bUseTestAds) {
                    builder.addTestDevice(GooglePlayAdsExtension.this.TestDeviceId);
                }
                GooglePlayAdsExtension.this.mRewardedVideoAd.loadAd(str, GooglePlayAdsExtension.this.GoogleMobileAds_BuildAdRequestWithConsent(builder));
            }
        });
    }

    public void GoogleMobileAds_ShowRewardedVideo() {
        if (this.mRewardedVideoAd != null) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    if (GooglePlayAdsExtension.this.mRewardedVideoAd.isLoaded()) {
                        GooglePlayAdsExtension.this.mRewardedVideoAd.show();
                    }
                }
            });
        }
    }

    public String GoogleMobileAds_RewardedVideoStatus() {
        if (this.mRewardedVideoAd != null) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    if (GooglePlayAdsExtension.this.mRewardedVideoAd == null) {
                        return;
                    }
                    if (GooglePlayAdsExtension.this.mRewardedVideoAd.isLoaded()) {
                        String unused = GooglePlayAdsExtension.this.RewardedVideoStatus = "Ready";
                    } else {
                        String unused2 = GooglePlayAdsExtension.this.RewardedVideoStatus = "Not Ready";
                    }
                }
            });
        }
        return this.RewardedVideoStatus;
    }

    public void onRewarded(RewardItem rewardItem) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "rewardedvideo_watched");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "amount", (double) rewardItem.getAmount());
        RunnerJNILib.DsMapAddString(jCreateDsMap, "currency", rewardItem.getType());
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onRewardedVideoAdLeftApplication() {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "rewardedvideo_leftapplication");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onRewardedVideoAdClosed() {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "rewardedvideo_adclosed");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onRewardedVideoAdFailedToLoad(int i) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "rewardedvideo_loadfailed");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "errorcode", (double) i);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (GooglePlayAdsExtension.this.mRewardedVideoAd == null) {
                    return;
                }
                if (GooglePlayAdsExtension.this.mRewardedVideoAd.isLoaded()) {
                    String unused = GooglePlayAdsExtension.this.RewardedVideoStatus = "Ready";
                } else {
                    String unused2 = GooglePlayAdsExtension.this.RewardedVideoStatus = "Not Ready";
                }
            }
        });
    }

    public void onRewardedVideoAdLoaded() {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "rewardedvideo_adloaded");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (GooglePlayAdsExtension.this.mRewardedVideoAd == null) {
                    return;
                }
                if (GooglePlayAdsExtension.this.mRewardedVideoAd.isLoaded()) {
                    String unused = GooglePlayAdsExtension.this.RewardedVideoStatus = "Ready";
                } else {
                    String unused2 = GooglePlayAdsExtension.this.RewardedVideoStatus = "Not Ready";
                }
            }
        });
    }

    public void onRewardedVideoAdOpened() {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "rewardedvideo_adopened");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onRewardedVideoStarted() {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "rewardedvideo_videostarted");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void GoogleMobileAds_Init(String str, String str2) {
        this.InterstitialId = str;
        MobileAds.initialize(RunnerActivity.CurrentActivity, str2);
    }

    public void GoogleMobileAds_ShowInterstitial() {
        if (this.interstitialAd != null) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    Log.i("yoyo", "showinterstitial called");
                    if (GooglePlayAdsExtension.this.interstitialAd.isLoaded()) {
                        RunnerActivity.CurrentActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                GooglePlayAdsExtension.this.interstitialAd.show();
                            }
                        });
                    } else {
                        Log.i("yoyo", "Interstitial ad was not ready to be shown.");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void initInterstitial() {
        this.interstitialAd = new InterstitialAd(RunnerActivity.CurrentActivity);
        this.interstitialAd.setAdUnitId(this.InterstitialId);
        this.interstitialAd.setAdListener(this.adlistener);
    }

    public void GoogleMobileAds_LoadInterstitial() {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (GooglePlayAdsExtension.this.interstitialAd == null) {
                    GooglePlayAdsExtension.this.initInterstitial();
                }
                AdRequest.Builder builder = new AdRequest.Builder();
                builder.addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB");
                if (GooglePlayAdsExtension.this.bUseTestAds) {
                    builder.addTestDevice(GooglePlayAdsExtension.this.TestDeviceId);
                }
                GooglePlayAdsExtension.this.interstitialAd.loadAd(GooglePlayAdsExtension.this.GoogleMobileAds_BuildAdRequestWithConsent(builder));
            }
        });
    }

    /* access modifiers changed from: private */
    public void sendBannerLoadedEvent(boolean z) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "banner_load");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "loaded", z ? 1.0d : 0.0d);
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        if (z) {
            GoogleMobileAds_MoveBanner((double) this.BannerXPos, (double) this.BannerYPos);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "width", GoogleMobileAds_BannerGetWidth());
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "height", GoogleMobileAds_BannerGetHeight());
        }
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    /* access modifiers changed from: private */
    public void sendInterstitialLoadedEvent(boolean z) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "interstitial_load");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "loaded", z ? 1.0d : 0.0d);
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    /* access modifiers changed from: private */
    public void sendInterstitialClosedEvent() {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "interstitial_closed");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void GoogleMobileAds_AddBanner(String str, double d) {
        GoogleMobileAds_AddBannerAt(str, d, 0.0d, 0.0d);
    }

    /* access modifiers changed from: package-private */
    @TargetApi(11)
    public void SetLayerType() {
        if (Build.VERSION.SDK_INT > 10) {
            this.adView.setLayerType(1, null);
        }
    }

    public void GoogleMobileAds_ShowBanner() {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (GooglePlayAdsExtension.this.adView != null) {
                    GooglePlayAdsExtension.this.adView.setVisibility(0);
                }
            }
        });
    }

    public void GoogleMobileAds_HideBanner() {
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                if (GooglePlayAdsExtension.this.adView != null) {
                    GooglePlayAdsExtension.this.adView.setVisibility(8);
                }
            }
        });
    }

    public void GoogleMobileAds_AddBannerAt(final String str, double d, double d2, double d3) {
        this.BannerId = str;
        this.BannerXPos = (int) d2;
        this.BannerYPos = (int) d3;
        switch ((int) (0.5d + d)) {
            case 1:
                this.BannerSize = AdSize.BANNER;
                break;
            case 2:
                this.BannerSize = AdSize.MEDIUM_RECTANGLE;
                break;
            case 3:
                this.BannerSize = AdSize.FULL_BANNER;
                break;
            case 4:
                this.BannerSize = AdSize.LEADERBOARD;
                break;
            case 5:
                this.BannerSize = AdSize.WIDE_SKYSCRAPER;
                break;
            case 6:
                this.BannerSize = AdSize.SMART_BANNER;
                break;
            default:
                Log.i("yoyo", "AddBanner illegal banner size type:" + d);
                return;
        }
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                AbsoluteLayout absoluteLayout = (AbsoluteLayout) RunnerActivity.CurrentActivity.findViewById(R.id.ad);
                if (GooglePlayAdsExtension.this.adView != null) {
                    if (absoluteLayout != null) {
                        absoluteLayout.removeView(GooglePlayAdsExtension.this.adView);
                    }
                    GooglePlayAdsExtension.this.adView.destroy();
                    AdView unused = GooglePlayAdsExtension.this.adView = null;
                }
                AdView unused2 = GooglePlayAdsExtension.this.adView = new AdView(RunnerActivity.CurrentActivity);
                GooglePlayAdsExtension.this.adView.setAdListener(new AdListener() {
                    public void onAdLoaded() {
                        Log.i("yoyo", "Banner Ad onAdLoaded");
                        GooglePlayAdsExtension.this.sendBannerLoadedEvent(true);
                    }

                    public void onAdFailedToLoad(int i) {
                        Log.i("yoyo", "Banner Ad onAdFailedToLoad");
                        GooglePlayAdsExtension.this.sendBannerLoadedEvent(false);
                    }
                });
                GooglePlayAdsExtension.this.SetLayerType();
                GooglePlayAdsExtension.this.adView.setAdSize(GooglePlayAdsExtension.this.BannerSize);
                GooglePlayAdsExtension.this.adView.setAdUnitId(str);
                if (absoluteLayout != null) {
                    absoluteLayout.addView(GooglePlayAdsExtension.this.adView);
                    AdRequest.Builder builder = new AdRequest.Builder();
                    builder.addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB");
                    if (GooglePlayAdsExtension.this.bUseTestAds) {
                        builder.addTestDevice(GooglePlayAdsExtension.this.TestDeviceId);
                    }
                    GooglePlayAdsExtension.this.adView.loadAd(GooglePlayAdsExtension.this.GoogleMobileAds_BuildAdRequestWithConsent(builder));
                }
            }
        });
    }

    public void GoogleMobileAds_RemoveBanner() {
        if (this.adView != null) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    AbsoluteLayout absoluteLayout = (AbsoluteLayout) RunnerActivity.CurrentActivity.findViewById(R.id.ad);
                    if (absoluteLayout != null) {
                        absoluteLayout.removeView(GooglePlayAdsExtension.this.adView);
                    }
                    GooglePlayAdsExtension.this.adView.destroy();
                    AdView unused = GooglePlayAdsExtension.this.adView = null;
                }
            });
        }
    }

    public double GoogleMobileAds_BannerGetWidth() {
        AdSize adSize = this.BannerSize;
        if (adSize != null) {
            return (double) adSize.getWidthInPixels(RunnerJNILib.ms_context);
        }
        return 0.0d;
    }

    public double GoogleMobileAds_BannerGetHeight() {
        AdSize adSize = this.BannerSize;
        if (adSize == null) {
            return 0.0d;
        }
        int heightInPixels = adSize.getHeightInPixels(RunnerJNILib.ms_context);
        if (this.BannerSize == AdSize.SMART_BANNER) {
            DisplayMetrics displayMetrics = RunnerJNILib.ms_context.getResources().getDisplayMetrics();
            int round = Math.round(((float) displayMetrics.heightPixels) / displayMetrics.density);
            int round2 = Math.round(displayMetrics.density);
            heightInPixels = round < 400 ? round2 * 32 : round <= 720 ? round2 * 50 : round2 * 90;
        }
        return (double) heightInPixels;
    }

    public void GoogleMobileAds_MoveBanner(double d, double d2) {
        final int i = (int) d;
        final int i2 = (int) d2;
        this.BannerXPos = i;
        this.BannerYPos = i2;
        if (this.adView != null) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    GooglePlayAdsExtension.this.adView.setLayoutParams(new AbsoluteLayout.LayoutParams(-2, -2, i, i2));
                    GooglePlayAdsExtension.this.adView.requestLayout();
                }
            });
        }
    }

    public void GoogleMobileAds_UseTestAds(double d, String str) {
        this.bUseTestAds = d >= 0.5d;
        this.TestDeviceId = str;
    }

    public String GoogleMobileAds_InterstitialStatus() {
        if (this.interstitialAd != null) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    if (GooglePlayAdsExtension.this.interstitialAd.isLoaded()) {
                        String unused = GooglePlayAdsExtension.this.InterstitialStatus = "Ready";
                    } else {
                        String unused2 = GooglePlayAdsExtension.this.InterstitialStatus = "Not Ready";
                    }
                }
            });
        }
        return this.InterstitialStatus;
    }

    private class AdsConsentFormListener extends ConsentFormListener {
        private ConsentForm m_consentForm = null;

        public AdsConsentFormListener(ConsentForm consentForm) {
            this.m_consentForm = consentForm;
        }

        public void setForm(ConsentForm consentForm) {
            this.m_consentForm = consentForm;
        }

        public void onConsentFormLoaded() {
            StringBuilder sb = new StringBuilder();
            sb.append("Consent form loaded. Consent form object: ");
            ConsentForm consentForm = this.m_consentForm;
            sb.append(consentForm != null ? consentForm.toString() : "NULL");
            Log.i("yoyo", sb.toString());
            ConsentForm consentForm2 = this.m_consentForm;
            if (consentForm2 != null) {
                consentForm2.show();
            } else {
                GooglePlayAdsExtension.this.GoogleMobileAds_ConsentReportStatus(ConsentStatus.UNKNOWN, -1, "Error: Consent form is NULL in load finished callback.");
            }
        }

        public void onConsentFormOpened() {
            Log.i("yoyo", "Consent form opened successfully!");
        }

        public void onConsentFormClosed(ConsentStatus consentStatus, Boolean bool) {
            Log.i("yoyo", "Consent form closed. Status: " + consentStatus.toString() + ". Adfree: " + bool.toString());
            GooglePlayAdsExtension.this.GoogleMobileAds_ConsentReportStatus(consentStatus, bool.booleanValue() ? 1 : 0, null);
        }

        public void onConsentFormError(String str) {
            Log.i("yoyo", "Consent form error: " + str);
            GooglePlayAdsExtension.this.GoogleMobileAds_ConsentReportStatus(ConsentStatus.UNKNOWN, -1, str);
        }
    }

    /* access modifiers changed from: private */
    public AdRequest GoogleMobileAds_BuildAdRequestWithConsent(AdRequest.Builder builder) {
        Bundle bundle = new Bundle();
        if (GoogleMobileAds_ConsentGetAllowPersonalizedAds() != 0.0d) {
            bundle.putString("npa", "1");
        }
        builder.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
        return builder.build();
    }

    public void GoogleMobileAds_ConsentUpdate(String str, String str2, double d, double d2, double d3) {
        ConsentInformation.getInstance(RunnerJNILib.ms_context).setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA);
        ConsentInformation instance = ConsentInformation.getInstance(RunnerJNILib.ms_context);
        String[] split = str.split(",");
        Log.i("yoyo", "Attempting to update consent with publisher IDs: " + str);
        final String str3 = str2;
        final double d4 = d;
        final double d5 = d2;
        final double d6 = d3;
        instance.requestConsentInfoUpdate(split, new ConsentInfoUpdateListener() {
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                if (consentStatus == ConsentStatus.UNKNOWN) {
                    GooglePlayAdsExtension.this.GoogleMobileAds_ConsentFormShow(str3, d4, d5, d6);
                } else {
                    GooglePlayAdsExtension.this.GoogleMobileAds_ConsentReportStatus(consentStatus, -1, null);
                }
            }

            public void onFailedToUpdateConsentInfo(String str) {
                Log.i("yoyo", "Error updating consent: " + str);
                GooglePlayAdsExtension.this.GoogleMobileAds_ConsentReportStatus(ConsentStatus.UNKNOWN, -1, str);
            }
        });
    }

    public void GoogleMobileAds_ConsentFormShow(String str, double d, double d2, double d3) {
        URL url;
        Log.i("yoyo", "Attempting to show consent form.");
        try {
            url = new URL(str);
        } catch (MalformedURLException e) {
            Log.i("yoyo", "Invalid privacy policy URL passed to consent form!");
            e.printStackTrace();
            url = null;
        }
        AdsConsentFormListener adsConsentFormListener = new AdsConsentFormListener(null);
        ConsentForm.Builder withListener = new ConsentForm.Builder(RunnerJNILib.ms_context, url).withListener(adsConsentFormListener);
        if (d != 0.0d) {
            withListener = withListener.withPersonalizedAdsOption();
        }
        if (d2 != 0.0d) {
            withListener = withListener.withNonPersonalizedAdsOption();
        }
        if (d3 != 0.0d) {
            withListener = withListener.withAdFreeOption();
        }
        ConsentForm build = withListener.build();
        adsConsentFormListener.setForm(build);
        build.load();
        Log.i("yoyo", "Consent form load triggered. Form object: " + build.toString());
    }

    /* access modifiers changed from: private */
    public void GoogleMobileAds_ConsentReportStatus(ConsentStatus consentStatus, int i, String str) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        if (str == null) {
            this.mConsentPreferAdFree = i != 0;
            RunnerJNILib.DsMapAddString(jCreateDsMap, "status", consentStatus.toString());
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "ad_free", this.mConsentPreferAdFree ? 1.0d : 0.0d);
        } else {
            RunnerJNILib.DsMapAddString(jCreateDsMap, "error", str);
        }
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "consent_status");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9817.0d);
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void GoogleMobileAds_ConsentSetUserUnderAge(double d) {
        ConsentInformation.getInstance(RunnerJNILib.ms_context).setTagForUnderAgeOfConsent(d != 0.0d);
    }

    public boolean GoogleMobileAds_ConsentIsUserUnderAge() {
        return ConsentInformation.getInstance(RunnerJNILib.ms_context).isTaggedForUnderAgeOfConsent();
    }

    public boolean GoogleMobileAds_ConsentIsUserInEEA() {
        return ConsentInformation.getInstance(RunnerJNILib.ms_context).isRequestLocationInEeaOrUnknown();
    }

    public void GoogleMobileAds_ConsentDebugAddDevice(String str) {
        ConsentInformation.getInstance(RunnerJNILib.ms_context).addTestDevice(str);
    }

    public void GoogleMobileAds_ConsentDebugSetDeviceInEEA(double d) {
        ConsentInformation.getInstance(RunnerJNILib.ms_context).setDebugGeography(d != 0.0d ? DebugGeography.DEBUG_GEOGRAPHY_EEA : DebugGeography.DEBUG_GEOGRAPHY_NOT_EEA);
    }

    public double GoogleMobileAds_ConsentGetAllowPersonalizedAds() {
        return (double) (ConsentInformation.getInstance(RunnerJNILib.ms_context).getConsentStatus() == ConsentStatus.PERSONALIZED ? 1 : 0);
    }

    public void GoogleMobileAds_ConsentSetAllowPersonalizedAds(double d) {
        ConsentInformation.getInstance(RunnerJNILib.ms_context).setConsentStatus(d != 0.0d ? ConsentStatus.PERSONALIZED : ConsentStatus.NON_PERSONALIZED);
    }
}
