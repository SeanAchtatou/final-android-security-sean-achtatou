package com.butakov.psebay;

public interface IAdvertising {

    public enum AdTypes {
        BANNER,
        MRECT,
        FULL_BANNER,
        LEADERBOARD,
        SKYSCRAPER,
        INTERSTITIAL
    }

    void define(int i, String str, AdTypes adTypes);

    void disable(int i);

    void enable(int i, int i2, int i3);

    boolean engagement_active();

    boolean engagement_available();

    void engagement_launch();

    void event(String str);

    void event_preload(String str);

    int getAdDisplayHeight(int i);

    int getAdDisplayWidth(int i);

    boolean interstitial_available();

    boolean interstitial_display();

    void move(int i, int i2, int i3);

    void onPause();

    void onResume();

    void on_iap_cancelled(String str);

    void on_iap_failed(String str);

    void on_iap_success(String str);

    void pause();

    void pc_badge_add(int i, int i2, int i3, int i4, String str);

    void pc_badge_hide();

    void pc_badge_move(int i, int i2, int i3, int i4);

    void pc_badge_update();

    void refresh(int i);

    void resume();

    void reward_callback(int i);

    void setView(int i);

    void setup(String str);
}
