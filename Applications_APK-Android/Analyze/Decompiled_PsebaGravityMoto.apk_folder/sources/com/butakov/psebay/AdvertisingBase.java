package com.butakov.psebay;

import android.app.Activity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import com.butakov.psebay.IAdvertising;
import com.yoyogames.runner.RunnerJNILib;

class AdvertisingBase implements IAdvertising {
    public Activity m_activity;
    public String[] m_adDefinitions = new String[10];
    public IAdvertising.AdTypes[] m_adTypes = new IAdvertising.AdTypes[10];
    public boolean m_usetestads;
    public View m_view = null;
    public ViewGroup m_viewGroup;

    public boolean engagement_active() {
        return false;
    }

    public boolean engagement_available() {
        return false;
    }

    public void engagement_launch() {
    }

    public void event(String str) {
    }

    public void event_preload(String str) {
    }

    public boolean interstitial_available() {
        return false;
    }

    public boolean interstitial_display() {
        return false;
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void on_iap_cancelled(String str) {
    }

    public void on_iap_failed(String str) {
    }

    public void on_iap_success(String str) {
    }

    public void pause() {
    }

    public void pc_badge_add(int i, int i2, int i3, int i4, String str) {
    }

    public void pc_badge_hide() {
    }

    public void pc_badge_move(int i, int i2, int i3, int i4) {
    }

    public void pc_badge_update() {
    }

    public void refresh(int i) {
    }

    public void resume() {
    }

    public void reward_callback(int i) {
    }

    public void setView(int i) {
    }

    public void setup(String str) {
    }

    public AdvertisingBase(Activity activity, ViewGroup viewGroup, boolean z) {
        this.m_activity = activity;
        this.m_viewGroup = viewGroup;
        this.m_usetestads = z;
    }

    public static IAdvertising.AdTypes ConvertToAdType(int i) {
        IAdvertising.AdTypes adTypes = IAdvertising.AdTypes.BANNER;
        if (i == 0) {
            return IAdvertising.AdTypes.BANNER;
        }
        if (i == 1) {
            return IAdvertising.AdTypes.MRECT;
        }
        if (i == 2) {
            return IAdvertising.AdTypes.FULL_BANNER;
        }
        if (i == 3) {
            return IAdvertising.AdTypes.LEADERBOARD;
        }
        if (i == 4) {
            return IAdvertising.AdTypes.SKYSCRAPER;
        }
        if (i != 5) {
            return adTypes;
        }
        return IAdvertising.AdTypes.INTERSTITIAL;
    }

    public int getAdWidth(IAdvertising.AdTypes adTypes) {
        float f;
        float f2 = this.m_activity.getResources().getDisplayMetrics().density;
        if (adTypes != null) {
            int i = AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes[adTypes.ordinal()];
            if (i == 1) {
                f = 320.0f;
            } else if (i == 2) {
                f = 300.0f;
            } else if (i == 3) {
                f = 468.0f;
            } else if (i == 4) {
                f = 728.0f;
            } else if (i == 5) {
                f = 120.0f;
            }
            return (int) (f * f2);
        }
        f = 0.0f;
        return (int) (f * f2);
    }

    /* renamed from: com.butakov.psebay.AdvertisingBase$4  reason: invalid class name */
    static /* synthetic */ class AnonymousClass4 {
        static final /* synthetic */ int[] $SwitchMap$com$butakov$psebay$IAdvertising$AdTypes = new int[IAdvertising.AdTypes.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.butakov.psebay.IAdvertising$AdTypes[] r0 = com.butakov.psebay.IAdvertising.AdTypes.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.butakov.psebay.AdvertisingBase.AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes = r0
                int[] r0 = com.butakov.psebay.AdvertisingBase.AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.butakov.psebay.IAdvertising$AdTypes r1 = com.butakov.psebay.IAdvertising.AdTypes.BANNER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.butakov.psebay.AdvertisingBase.AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes     // Catch:{ NoSuchFieldError -> 0x001f }
                com.butakov.psebay.IAdvertising$AdTypes r1 = com.butakov.psebay.IAdvertising.AdTypes.MRECT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.butakov.psebay.AdvertisingBase.AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes     // Catch:{ NoSuchFieldError -> 0x002a }
                com.butakov.psebay.IAdvertising$AdTypes r1 = com.butakov.psebay.IAdvertising.AdTypes.FULL_BANNER     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.butakov.psebay.AdvertisingBase.AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.butakov.psebay.IAdvertising$AdTypes r1 = com.butakov.psebay.IAdvertising.AdTypes.LEADERBOARD     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.butakov.psebay.AdvertisingBase.AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.butakov.psebay.IAdvertising$AdTypes r1 = com.butakov.psebay.IAdvertising.AdTypes.SKYSCRAPER     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.butakov.psebay.AdvertisingBase.AnonymousClass4.<clinit>():void");
        }
    }

    public int getAdHeight(IAdvertising.AdTypes adTypes) {
        float f;
        float f2 = this.m_activity.getResources().getDisplayMetrics().density;
        if (adTypes != null) {
            int i = AnonymousClass4.$SwitchMap$com$butakov$psebay$IAdvertising$AdTypes[adTypes.ordinal()];
            if (i == 1) {
                f = 53.0f;
            } else if (i == 2) {
                f = 250.0f;
            } else if (i == 3) {
                f = 60.0f;
            } else if (i == 4) {
                f = 90.0f;
            } else if (i == 5) {
                f = 600.0f;
            }
            return (int) (f * f2);
        }
        f = 0.0f;
        return (int) (f * f2);
    }

    public int getAdDisplayWidth(int i) {
        if (i < 0) {
            return 0;
        }
        IAdvertising.AdTypes[] adTypesArr = this.m_adTypes;
        if (i >= adTypesArr.length) {
            return 0;
        }
        int adWidth = getAdWidth(adTypesArr[i]);
        Display defaultDisplay = this.m_activity.getWindowManager().getDefaultDisplay();
        return (adWidth * defaultDisplay.getWidth()) / RunnerJNILib.getGuiWidth();
    }

    public int getAdDisplayHeight(int i) {
        if (i < 0) {
            return 0;
        }
        IAdvertising.AdTypes[] adTypesArr = this.m_adTypes;
        if (i >= adTypesArr.length) {
            return 0;
        }
        int adHeight = getAdHeight(adTypesArr[i]);
        Display defaultDisplay = this.m_activity.getWindowManager().getDefaultDisplay();
        return (adHeight * defaultDisplay.getHeight()) / RunnerJNILib.getGuiHeight();
    }

    public void enable_interstitial(int i) {
        Log.i("yoyo", "Interstitials not supported for this provider");
    }

    public void enable(int i, int i2, int i3) {
        if (this.m_adTypes[i3] == IAdvertising.AdTypes.INTERSTITIAL) {
            enable_interstitial(i3);
            return;
        }
        setView(i3);
        final View view = this.m_view;
        final int adWidth = getAdWidth(this.m_adTypes[i3]);
        final int adHeight = getAdHeight(this.m_adTypes[i3]);
        if (adWidth == 0 || adHeight == 0) {
            Log.i("yoyo", "error enabling ad with index " + i3);
            return;
        }
        final int i4 = i3;
        final int i5 = i;
        final int i6 = i2;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                Log.i("yoyo", "adding view for index " + i4);
                if (view.getParent() == null) {
                    AdvertisingBase.this.m_viewGroup.addView(view);
                }
                view.setLayoutParams(new AbsoluteLayout.LayoutParams(adWidth, adHeight, i5, i6));
                if (!AdvertisingBase.this.m_usetestads) {
                    view.setBackgroundColor(0);
                } else {
                    view.setBackgroundColor(-16776961);
                }
                view.requestLayout();
                this.refresh(i4);
                view.setVisibility(0);
            }
        });
    }

    public void move(int i, int i2, int i3) {
        setView(i3);
        final View view = this.m_view;
        final int adWidth = getAdWidth(this.m_adTypes[i3]);
        final int adHeight = getAdHeight(this.m_adTypes[i3]);
        final int i4 = i;
        final int i5 = i2;
        RunnerActivity.ViewHandler.post(new Runnable() {
            public void run() {
                view.setLayoutParams(new AbsoluteLayout.LayoutParams(adWidth, adHeight, i4, i5));
                if (!AdvertisingBase.this.m_usetestads) {
                    view.setBackgroundColor(0);
                } else {
                    view.setBackgroundColor(-16776961);
                }
                view.requestLayout();
                view.setVisibility(0);
            }
        });
    }

    public void disable(int i) {
        setView(i);
        final View view = this.m_view;
        if (view != null) {
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    view.setVisibility(8);
                    if (view.getParent() != null) {
                        AdvertisingBase.this.m_viewGroup.removeView(view);
                    }
                }
            });
        }
    }

    public void define(int i, String str, IAdvertising.AdTypes adTypes) {
        if (i >= 0) {
            String[] strArr = this.m_adDefinitions;
            if (i < strArr.length) {
                strArr[i] = str;
                this.m_adTypes[i] = adTypes;
            }
        }
    }
}
