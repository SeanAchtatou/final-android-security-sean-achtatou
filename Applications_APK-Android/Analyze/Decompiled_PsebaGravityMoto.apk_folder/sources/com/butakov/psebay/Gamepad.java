package com.butakov.psebay;

import android.os.Build;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import androidx.core.view.InputDeviceCompat;

public class Gamepad {
    protected static GamepadHandler ms_GamepadHandler;

    public static void Initialise() {
        if (Build.VERSION.SDK_INT >= 19) {
            ms_GamepadHandler = new GamepadHandler_API19();
        } else if (Build.VERSION.SDK_INT >= 16) {
            ms_GamepadHandler = new GamepadHandler_API16();
        } else if (Build.VERSION.SDK_INT >= 12) {
            ms_GamepadHandler = new GamepadHandler_API12();
        } else {
            ms_GamepadHandler = new GamepadHandler();
        }
    }

    public static void EnumerateDevices() {
        if (ms_GamepadHandler == null) {
            Initialise();
        }
        ms_GamepadHandler.PollInputDevices();
    }

    public static void handleKeyEvent(KeyEvent keyEvent) {
        GamepadHandler gamepadHandler;
        int deviceId = keyEvent.getDeviceId();
        InputDevice device = InputDevice.getDevice(deviceId);
        if (device != null && deviceId >= 0) {
            int sources = device.getSources();
            if (((sources & 16) == 16 || (sources & InputDeviceCompat.SOURCE_GAMEPAD) == 1025 || (sources & InputDeviceCompat.SOURCE_DPAD) == 513) && (gamepadHandler = ms_GamepadHandler) != null) {
                gamepadHandler.HandleKeyEvent(deviceId, keyEvent);
            }
        }
    }

    public static void handleMotionEvent(MotionEvent motionEvent) {
        GamepadHandler gamepadHandler;
        int source = motionEvent.getSource();
        if ((source == 513 || source == 1025 || source == 16777232) && (gamepadHandler = ms_GamepadHandler) != null) {
            gamepadHandler.HandleMotionEvent(motionEvent);
        }
    }
}
