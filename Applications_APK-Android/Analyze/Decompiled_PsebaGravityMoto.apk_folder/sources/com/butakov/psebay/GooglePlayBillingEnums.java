package com.butakov.psebay;

public class GooglePlayBillingEnums {
    static final int EVENT_OTHER_WEB_IAP = 66;
    static final double YY_ACKNOWLEDGE_PURCHASE_RESPONSE = 12008.0d;
    static final double YY_IAP_RECEIPT = 12001.0d;
    static final double YY_PRODUCT_CONSUME_RESPONSE = 12007.0d;
    static final double YY_PRODUCT_DATA_RESPONSE = 12003.0d;
    static final double YY_PURCHASE_STATUS = 12002.0d;
    static final double YY_STORE_CONNECT = 12005.0d;
    static final double YY_STORE_CONNECT_FAILED = 12006.0d;
    static final double YY_SUBSCRIPTION_DATA_RESPONSE = 12009.0d;
    static final double YY_USER_DATA_RESPONSE = 12004.0d;
    static final int yy_purchase_state_pending = 13002;
    static final int yy_purchase_state_purchased = 13001;
    static final int yy_purchase_state_unspecified = 13000;
}
