package com.butakov.psebay;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.view.InputDeviceCompat;
import com.yoyogames.runner.RunnerJNILib;
import java.util.regex.Pattern;

public class RunnerKeyboardController {
    public static final boolean DEBUG_SHOW_HIDDEN_TEXT_FIELD = false;
    public static final int KEYBOARD_EVENT_SOURCE_DEFAULT = 4355;
    public static final String KEYBOARD_STATUS_HIDDEN = "hidden";
    public static final String KEYBOARD_STATUS_HIDING = "hiding";
    public static final String KEYBOARD_STATUS_SHOWING = "showing";
    public static final String KEYBOARD_STATUS_VISIBLE = "visible";
    private static int ms_estimatedKeyboardHeight = ((Build.VERSION.SDK_INT >= 21 ? 48 : 0) + 100);
    /* access modifiers changed from: private */
    public View m_activityView = null;
    /* access modifiers changed from: private */
    public Context m_context = null;
    /* access modifiers changed from: private */
    public int m_currentAutocapitalizationType = 0;
    private int m_currentKeyboardHeight = 0;
    /* access modifiers changed from: private */
    public int m_currentKeyboardType = 0;
    /* access modifiers changed from: private */
    public boolean m_currentPredictiveTextEnabled = false;
    /* access modifiers changed from: private */
    public int m_currentReturnKeyType = 0;
    /* access modifiers changed from: private */
    public EditText m_editText = null;
    /* access modifiers changed from: private */
    public InputMethodManager m_inputMethodManager = null;
    private String m_keyboardStatus = KEYBOARD_STATUS_HIDDEN;
    private boolean m_physicalKeyboardConnected = false;
    /* access modifiers changed from: private */
    public boolean m_setTextHandlerEnabled = false;
    private Rect m_viewActiveRect = new Rect();
    private Handler m_viewHandler = null;
    private boolean m_virtualKeyboardActive = false;
    private boolean m_virtualKeyboardStatusRequested = false;
    /* access modifiers changed from: private */
    public KeyboardResultReceiver m_virtualKeyboardToggleResultReceiver = null;
    /* access modifiers changed from: private */
    public KeyboardResultReceiver m_virtualKeyboardVisibilityCheckAdjustReceiver = null;
    private KeyboardResultReceiver m_virtualKeyboardVisibilityCheckResultReceiver = null;
    private boolean m_virtualKeyboardVisible = false;

    public void OnPhysicalKeyboardKeyEvent(int i, KeyEvent keyEvent) {
    }

    private class KeyboardResultReceiver extends ResultReceiver {
        protected RunnerKeyboardController m_keyboardController = null;

        /* access modifiers changed from: protected */
        public void onReceiveResult(int i, Bundle bundle) {
        }

        KeyboardResultReceiver(RunnerKeyboardController runnerKeyboardController) {
            super(null);
            this.m_keyboardController = runnerKeyboardController;
        }
    }

    private class KeyboardLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        protected RunnerKeyboardController m_keyboardController = null;

        KeyboardLayoutListener(RunnerKeyboardController runnerKeyboardController) {
            this.m_keyboardController = runnerKeyboardController;
        }

        public void onGlobalLayout() {
            this.m_keyboardController.OnActivityLayoutChanged();
        }
    }

    private class KeyboardInputConnectionWrapper extends InputConnectionWrapper {
        protected InputConnection m_inputConnection = null;
        protected RunnerKeyboardController m_keyboardController = null;

        public KeyboardInputConnectionWrapper(InputConnection inputConnection, boolean z, RunnerKeyboardController runnerKeyboardController) {
            super(inputConnection, z);
            this.m_keyboardController = runnerKeyboardController;
            this.m_inputConnection = inputConnection;
        }

        public void setTarget(InputConnection inputConnection) {
            super.setTarget(inputConnection);
            this.m_inputConnection = inputConnection;
        }

        public boolean setComposingRegion(int i, int i2) {
            if (i > 0) {
                ExtractedText extractedText = getExtractedText(new ExtractedTextRequest(), 0);
                String charSequence = (extractedText == null || extractedText.text == null) ? "" : extractedText.text.toString();
                int length = charSequence.length();
                if (length > 0) {
                    i = Math.min(i, length - 1);
                    while (i > 0) {
                        char charAt = charSequence.charAt(i - 1);
                        String ch = Character.toString(charAt);
                        if (Character.isWhitespace(charAt) || Pattern.matches("\\p{Punct}", ch)) {
                            break;
                        }
                        i--;
                    }
                }
            }
            return super.setComposingRegion(i, i2);
        }
    }

    private class KeyboardInputEditText extends AppCompatEditText {
        protected RunnerKeyboardController m_keyboardController = null;

        public boolean onCheckIsTextEditor() {
            return true;
        }

        public KeyboardInputEditText(Context context, RunnerKeyboardController runnerKeyboardController) {
            super(context);
            this.m_keyboardController = runnerKeyboardController;
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() != 1) {
                return false;
            }
            if (i != 4 && (i != 97 || (keyEvent.getSource() & InputDeviceCompat.SOURCE_GAMEPAD) != 1025)) {
                return false;
            }
            RunnerKeyboardController.this.VirtualKeyboardHide();
            return false;
        }

        public boolean isSuggestionsEnabled() {
            RunnerKeyboardController runnerKeyboardController = this.m_keyboardController;
            if (runnerKeyboardController != null) {
                return runnerKeyboardController.GetPredictiveTextEnabled();
            }
            return super.isSuggestionsEnabled();
        }

        public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
            return new KeyboardInputConnectionWrapper(super.onCreateInputConnection(editorInfo), true, this.m_keyboardController);
        }
    }

    RunnerKeyboardController(Context context, InputMethodManager inputMethodManager, View view, Handler handler) {
        this.m_context = context;
        this.m_inputMethodManager = inputMethodManager;
        this.m_activityView = view;
        this.m_viewHandler = handler;
    }

    public static RunnerKeyboardController Create(Context context, View view, Handler handler) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager == null) {
            return null;
        }
        RunnerKeyboardController runnerKeyboardController = new RunnerKeyboardController(context, inputMethodManager, view, handler);
        runnerKeyboardController.Init();
        return runnerKeyboardController;
    }

    private void Init() {
        InitTextField();
        InitReceivers();
    }

    private void InitTextField() {
        this.m_viewHandler.post(new Runnable() {
            public void run() {
                RunnerKeyboardController runnerKeyboardController = RunnerKeyboardController.this;
                EditText unused = runnerKeyboardController.m_editText = new KeyboardInputEditText(runnerKeyboardController.m_context, this);
                RunnerKeyboardController.this.m_editText.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable editable) {
                    }

                    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    }

                    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                        if (RunnerKeyboardController.this.m_setTextHandlerEnabled) {
                            String charSequence2 = charSequence.toString();
                            if (charSequence2.length() != 0) {
                                int[] access$400 = RunnerKeyboardController.GetStringCodepoints(charSequence2);
                                RunnerJNILib.OnVirtualKeyboardTextInserted(access$400, access$400.length);
                            } else if (RunnerKeyboardController.this.m_currentPredictiveTextEnabled) {
                                RunnerJNILib.OnVirtualKeyboardTextInserted(new int[]{0}, 0);
                            }
                        }
                    }
                });
                RunnerKeyboardController.this.m_editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        RunnerJNILib.KeyEvent(0, 13, 13, RunnerKeyboardController.KEYBOARD_EVENT_SOURCE_DEFAULT);
                        RunnerJNILib.KeyEvent(1, 13, 13, RunnerKeyboardController.KEYBOARD_EVENT_SOURCE_DEFAULT);
                        return true;
                    }
                });
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1, 1);
                layoutParams.leftMargin = -1;
                layoutParams.topMargin = -1;
                RunnerKeyboardController.this.m_editText.setLayoutParams(layoutParams);
                RunnerKeyboardController.this.m_editText.setFocusable(true);
                RunnerKeyboardController.this.m_editText.setFocusableInTouchMode(true);
                RunnerKeyboardController.this.m_editText.setSingleLine(true);
                RunnerKeyboardController.this.m_editText.setBackgroundColor(0);
                RunnerKeyboardController.this.m_editText.setTextColor(0);
                RunnerKeyboardController.this.m_editText.setCursorVisible(false);
                ViewGroup viewGroup = (ViewGroup) RunnerKeyboardController.this.m_activityView;
                viewGroup.setDescendantFocusability(131072);
                viewGroup.setFocusableInTouchMode(true);
                viewGroup.addView(RunnerKeyboardController.this.m_editText);
                RunnerKeyboardController.this.SetInputString(new int[]{0}, true);
            }
        });
    }

    private void InitReceivers() {
        this.m_virtualKeyboardToggleResultReceiver = new KeyboardResultReceiver(this) {
            /* access modifiers changed from: protected */
            /* JADX WARNING: Code restructure failed: missing block: B:6:0x000c, code lost:
                if (r2 != 3) goto L_0x001b;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onReceiveResult(int r2, android.os.Bundle r3) {
                /*
                    r1 = this;
                    super.onReceiveResult(r2, r3)
                    r3 = 1
                    if (r2 == 0) goto L_0x0016
                    if (r2 == r3) goto L_0x000f
                    r0 = 2
                    if (r2 == r0) goto L_0x0016
                    r3 = 3
                    if (r2 == r3) goto L_0x000f
                    goto L_0x001b
                L_0x000f:
                    com.butakov.psebay.RunnerKeyboardController r3 = r1.m_keyboardController
                    r0 = 0
                    r3.SetVirtualKeyboardActive(r0)
                    goto L_0x001b
                L_0x0016:
                    com.butakov.psebay.RunnerKeyboardController r0 = r1.m_keyboardController
                    r0.SetVirtualKeyboardActive(r3)
                L_0x001b:
                    com.butakov.psebay.RunnerKeyboardController r3 = r1.m_keyboardController
                    r3.UpdateKeyboardStatusFromIMMResult(r2)
                    com.butakov.psebay.RunnerKeyboardController r2 = r1.m_keyboardController
                    r2.VirtualKeyboardReportStatus()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.butakov.psebay.RunnerKeyboardController.AnonymousClass2.onReceiveResult(int, android.os.Bundle):void");
            }
        };
        this.m_virtualKeyboardVisibilityCheckAdjustReceiver = new KeyboardResultReceiver(this) {
            /* access modifiers changed from: protected */
            public void onReceiveResult(int i, Bundle bundle) {
                super.onReceiveResult(i, bundle);
                this.m_keyboardController.VirtualKeyboardReportStatus();
            }
        };
        this.m_virtualKeyboardVisibilityCheckResultReceiver = new KeyboardResultReceiver(this) {
            /* access modifiers changed from: protected */
            public void onReceiveResult(int i, Bundle bundle) {
                super.onReceiveResult(i, bundle);
                this.m_keyboardController.OnVirtualKeyboardVisibilityCheckResult(i);
            }
        };
        this.m_activityView.getViewTreeObserver().addOnGlobalLayoutListener(new KeyboardLayoutListener(this));
    }

    public void VirtualKeyboardToggle(boolean z, int i, int i2, int i3, boolean z2, int[] iArr) {
        final boolean z3 = z;
        final int i4 = i;
        final int i5 = i3;
        final boolean z4 = z2;
        final int i6 = i2;
        final int[] iArr2 = iArr;
        this.m_viewHandler.post(new Runnable() {
            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            public void run() {
                int i;
                if (z3) {
                    String str = null;
                    switch (i4) {
                        case 0:
                            i = 1;
                            break;
                        case 1:
                        default:
                            i = 0;
                            break;
                        case 2:
                            i = 16;
                            break;
                        case 3:
                            i = 32;
                            break;
                        case 4:
                            i = 2;
                            break;
                        case 5:
                            i = 3;
                            break;
                        case 6:
                            i = 96;
                            break;
                    }
                    int i2 = i5;
                    if (i2 == 1) {
                        i |= 8192;
                    } else if (i2 == 2) {
                        i |= 16384;
                    } else if (i2 == 3) {
                        i |= 4096;
                    }
                    if (!z4) {
                        i |= Build.MANUFACTURER.equalsIgnoreCase("HTC") ? 524288 : 524432;
                    }
                    int i3 = 838860805;
                    switch (i6) {
                        case 1:
                            str = "Go";
                            i3 = 838860802;
                            break;
                        case 2:
                            str = "Google";
                            i3 = 838860803;
                            break;
                        case 3:
                            str = "Join";
                            i3 = 838860802;
                            break;
                        case 4:
                            str = "Next";
                            break;
                        case 5:
                            str = "Route";
                            i3 = 838860802;
                            break;
                        case 6:
                            i3 = 838860803;
                            break;
                        case 7:
                            i3 = 838860804;
                            break;
                        case 8:
                            str = "Yahoo";
                            i3 = 838860803;
                            break;
                        case 9:
                            i3 = 838860806;
                            break;
                        case 10:
                            str = "Continue";
                            break;
                        case 11:
                            str = "Emergency Call";
                            i3 = 838860802;
                            break;
                        default:
                            i3 = 838860800;
                            break;
                    }
                    int i4 = -65537 & i3 & -32769;
                    int unused = RunnerKeyboardController.this.m_currentKeyboardType = i4;
                    int unused2 = RunnerKeyboardController.this.m_currentReturnKeyType = i6;
                    int unused3 = RunnerKeyboardController.this.m_currentAutocapitalizationType = i5;
                    boolean unused4 = RunnerKeyboardController.this.m_currentPredictiveTextEnabled = z4;
                    RunnerKeyboardController.this.m_editText.setImeOptions(i4);
                    RunnerKeyboardController.this.m_editText.setImeActionLabel(str, i4);
                    RunnerKeyboardController.this.m_editText.setInputType(i);
                    RunnerKeyboardController.this.m_editText.requestFocus();
                    RunnerKeyboardController.this.SetInputString(iArr2, true);
                    RunnerKeyboardController.this.m_inputMethodManager.showSoftInput(RunnerKeyboardController.this.m_editText, 0, RunnerKeyboardController.this.m_virtualKeyboardToggleResultReceiver);
                    return;
                }
                RunnerKeyboardController.this.m_editText.clearFocus();
                RunnerKeyboardController.this.m_inputMethodManager.hideSoftInputFromWindow(RunnerKeyboardController.this.m_editText.getWindowToken(), 0, RunnerKeyboardController.this.m_virtualKeyboardToggleResultReceiver);
            }
        });
    }

    public void VirtualKeyboardHide() {
        VirtualKeyboardToggle(false, 0, 0, 0, true, GetStringCodepoints(GetInputString()));
    }

    public boolean VirtualKeyboardGetStatus() {
        return this.m_virtualKeyboardActive;
    }

    public void VirtualKeyboardReportStatus() {
        this.m_virtualKeyboardStatusRequested = false;
        this.m_currentKeyboardHeight = VirtualKeyboardGetHeight();
        RunnerJNILib.OnVirtualKeyboardStatus(this.m_keyboardStatus, this.m_currentKeyboardHeight);
    }

    public void OnVirtualKeyboardVisibilityCheckResult(final int i) {
        if (i == 0) {
            SetVirtualKeyboardActive(true);
            this.m_keyboardStatus = KEYBOARD_STATUS_VISIBLE;
        } else if (i == 1 || i == 2 || i == 3) {
            SetVirtualKeyboardActive(false);
            this.m_keyboardStatus = KEYBOARD_STATUS_HIDDEN;
        }
        this.m_viewHandler.post(new Runnable() {
            public void run() {
                int i = i;
                if (i == 3) {
                    RunnerKeyboardController.this.m_inputMethodManager.showSoftInput(RunnerKeyboardController.this.m_editText, 0, RunnerKeyboardController.this.m_virtualKeyboardVisibilityCheckAdjustReceiver);
                } else if (i == 2) {
                    RunnerKeyboardController.this.m_inputMethodManager.hideSoftInputFromWindow(RunnerKeyboardController.this.m_editText.getWindowToken(), 0, RunnerKeyboardController.this.m_virtualKeyboardVisibilityCheckAdjustReceiver);
                } else {
                    RunnerKeyboardController.this.VirtualKeyboardReportStatus();
                }
            }
        });
    }

    public int VirtualKeyboardGetHeight() {
        this.m_activityView.getWindowVisibleDisplayFrame(this.m_viewActiveRect);
        return this.m_activityView.getHeight() - (this.m_viewActiveRect.bottom - this.m_viewActiveRect.top);
    }

    public void OnActivityLayoutChanged() {
        boolean z = true;
        if (VirtualKeyboardGetHeight() < ((int) TypedValue.applyDimension(1, (float) ms_estimatedKeyboardHeight, this.m_activityView.getResources().getDisplayMetrics()))) {
            z = false;
        }
        if (z != this.m_virtualKeyboardVisible) {
            SetVirtualKeyboardVisible(z);
            if (z) {
                this.m_keyboardStatus = KEYBOARD_STATUS_VISIBLE;
            } else {
                this.m_keyboardStatus = KEYBOARD_STATUS_HIDDEN;
            }
            VirtualKeyboardReportStatus();
        }
    }

    public void UpdateKeyboardStatusFromIMMResult(int i) {
        if (i == 0) {
            this.m_keyboardStatus = KEYBOARD_STATUS_VISIBLE;
        } else if (i == 1) {
            this.m_keyboardStatus = KEYBOARD_STATUS_HIDDEN;
        } else if (i == 2) {
            this.m_keyboardStatus = KEYBOARD_STATUS_SHOWING;
        } else if (i == 3) {
            this.m_keyboardStatus = KEYBOARD_STATUS_HIDING;
        }
    }

    public void SetVirtualKeyboardActive(boolean z) {
        this.m_virtualKeyboardActive = z;
    }

    public boolean GetVirtualKeyboardActive() {
        return this.m_virtualKeyboardActive;
    }

    public void SetVirtualKeyboardVisible(boolean z) {
        this.m_virtualKeyboardVisible = z;
    }

    public boolean GetVirtualKeyboardVisible() {
        return this.m_virtualKeyboardVisible;
    }

    public void SetPhysicalKeyboardConnected(boolean z) {
        this.m_physicalKeyboardConnected = z;
    }

    public boolean GetPhysicalKeyboardConnected() {
        return this.m_physicalKeyboardConnected;
    }

    public int GetVirtualKeyboardHeightCached() {
        return this.m_currentKeyboardHeight;
    }

    public boolean GetPredictiveTextEnabled() {
        return this.m_currentPredictiveTextEnabled;
    }

    public void SetInputString(int[] iArr, boolean z) {
        if (this.m_editText != null) {
            if (z) {
                this.m_setTextHandlerEnabled = false;
            }
            StringBuilder sb = new StringBuilder();
            for (int appendCodePoint : iArr) {
                sb.appendCodePoint(appendCodePoint);
            }
            String sb2 = sb.toString();
            Log.i("yoyo", "[VK] SetInputString. Length: " + iArr.length + ". New string: " + sb2);
            this.m_editText.setText(sb2);
            this.m_editText.setSelection(sb2.length());
            if (z) {
                this.m_setTextHandlerEnabled = true;
            }
        }
    }

    public String GetInputString() {
        EditText editText = this.m_editText;
        return editText != null ? editText.getText().toString() : "";
    }

    /* access modifiers changed from: private */
    public static int[] GetStringCodepoints(String str) {
        int length = str.length();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            i3++;
            i2 += Character.charCount(str.codePointAt(i2));
        }
        int[] iArr = new int[i3];
        int i4 = 0;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            iArr[i4] = codePointAt;
            i += Character.charCount(codePointAt);
            i4++;
        }
        return iArr;
    }
}
