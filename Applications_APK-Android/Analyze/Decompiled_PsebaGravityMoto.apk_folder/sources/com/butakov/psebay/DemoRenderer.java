package com.butakov.psebay;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ConfigurationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.os.Build;
import android.util.Log;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.yoyogames.runner.RunnerJNILib;
import java.io.IOException;
import java.io.InputStream;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public class DemoRenderer implements GLSurfaceView.Renderer {
    public static final int RUNNER_STORAGE_PERMISSION_REQUEST = 85;
    public static volatile int elapsedVsyncs = -1;
    public static final String kGameAssetsDROID = "GameAssetsDROID.zip";
    public static String m_apkFilePath = null;
    public static int m_defaultFrameBuffer = -1;
    public static String m_saveFilesDir = null;
    public static eState m_state = null;
    public static boolean ms_displayedLoadLibraryFailed = false;
    public static volatile Object waiterObject;
    private Map<String, Locale> localeMap;
    public boolean m_RequestedPermissions = false;
    private Context m_context;
    private int m_height;
    private String m_packageName;
    public boolean m_pauseRunner = false;
    public int m_pausecountdown = -1;
    public float m_refreshRate = 60.0f;
    public int m_renderCount;
    private String m_splashFilePath = "assets/splash.png";
    private int m_texHeight;
    private int m_texRawHeight;
    private int m_texRawWidth;
    private int m_texWidth;
    private int m_width;
    private long splashEndTime = 0;

    public enum eState {
        Startup,
        Splash,
        Splash2,
        APKExpansionDownload,
        InitRunner,
        WaitForDoStartup,
        WaitOnTimer,
        DoStartup,
        Process
    }

    private int getNextPow2(int i) {
        int i2 = i - 1;
        int i3 = i2 | (i2 >> 1);
        int i4 = i3 | (i3 >> 2);
        int i5 = i4 | (i4 >> 4);
        int i6 = i5 | (i5 >> 8);
        int i7 = (i6 | (i6 >> 16)) + 1;
        return i7 == 0 ? i7 + 1 : i7;
    }

    private void initCountryCodeMapping() {
        String[] iSOCountries = Locale.getISOCountries();
        this.localeMap = new HashMap(iSOCountries.length);
        for (String locale : iSOCountries) {
            Locale locale2 = new Locale("", locale);
            this.localeMap.put(locale2.getISO3Country().toUpperCase(Locale.US), locale2);
        }
    }

    private String iso3CountryCodeToIso2CountryCode(String str) {
        Locale locale;
        Map<String, Locale> map = this.localeMap;
        return (map == null || (locale = map.get(str)) == null) ? str : locale.getCountry();
    }

    public DemoRenderer(Context context) {
        waiterObject = new Object();
        this.m_context = context;
        m_state = eState.Startup;
        this.m_renderCount = 0;
        this.m_packageName = this.m_context.getPackageName();
    }

    public InputStream getResourceAsReader(String str) {
        System.out.println(str);
        try {
            return this.m_context.getResources().getAssets().open(str);
        } catch (Exception unused) {
            System.out.println("Exception while getting Resource");
            return null;
        }
    }

    public int getScreenOrientation() {
        return this.m_context.getResources().getConfiguration().orientation;
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        InputStream inputStream;
        if (m_state != eState.Startup) {
            Log.i("yoyo", "onSurfaceCreated() aborted on re-create, state is currently " + m_state);
            return;
        }
        if (m_defaultFrameBuffer == -1 && (gl10 instanceof GL11)) {
            IntBuffer allocate = IntBuffer.allocate(1);
            gl10.glGetIntegerv(36006, allocate);
            m_defaultFrameBuffer = allocate.get(0);
            Log.i("yoyo", "Renderer instance is gl1.1, framebuffer object is: " + m_defaultFrameBuffer);
        }
        if (RunnerActivity.YoYoRunner) {
            ArrayList arrayList = new ArrayList();
            if (Build.VERSION.SDK_INT > 16) {
                if (ContextCompat.checkSelfPermission(RunnerActivity.CurrentActivity, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                    arrayList.add("android.permission.WRITE_EXTERNAL_STORAGE");
                }
                if (ContextCompat.checkSelfPermission(RunnerActivity.CurrentActivity, "android.permission.READ_EXTERNAL_STORAGE") != 0) {
                    arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
                }
            }
            if (arrayList.size() > 0) {
                String[] strArr = new String[arrayList.size()];
                arrayList.toArray(strArr);
                ActivityCompat.requestPermissions(RunnerActivity.CurrentActivity, strArr, 85);
                m_saveFilesDir = "";
                this.m_RequestedPermissions = true;
            } else {
                m_saveFilesDir = "";
                this.m_RequestedPermissions = true;
            }
        } else {
            m_saveFilesDir = this.m_context.getFilesDir().getAbsolutePath() + "/";
        }
        m_apkFilePath = null;
        try {
            m_apkFilePath = this.m_context.getPackageManager().getApplicationInfo(BuildConfig.APPLICATION_ID, 0).sourceDir;
            Log.i("yoyo", "APK File Path :: " + m_apkFilePath);
            int[] iArr = new int[1];
            gl10.glGenTextures(1, iArr, 0);
            gl10.glBindTexture(3553, iArr[0]);
            gl10.glTexParameterf(3553, 10241, 9728.0f);
            gl10.glTexParameterf(3553, 10240, 9729.0f);
            ConfigurationInfo deviceConfigurationInfo = ((ActivityManager) this.m_context.getSystemService("activity")).getDeviceConfigurationInfo();
            if (deviceConfigurationInfo.reqGlEsVersion >= 131072) {
                Log.i("yoyo", "OpenGL ES-2.0 is supported: " + deviceConfigurationInfo.reqGlEsVersion);
            } else {
                Log.i("yoyo", "OpenGL ES-CM 1.1 is supported: " + deviceConfigurationInfo.reqGlEsVersion);
            }
            if (getScreenOrientation() == 2) {
                inputStream = getResourceAsReader("splash.png");
            } else {
                this.m_splashFilePath = "assets/portrait_splash.png";
                inputStream = getResourceAsReader("portrait_splash.png");
            }
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inDither = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options);
                this.m_texWidth = decodeStream.getWidth();
                this.m_texHeight = decodeStream.getHeight();
                this.m_texRawWidth = getNextPow2(this.m_texWidth);
                this.m_texRawHeight = getNextPow2(this.m_texHeight);
                GLUtils.texImage2D(3553, 0, Bitmap.createBitmap(this.m_texRawWidth, this.m_texRawHeight, Bitmap.Config.ARGB_8888), 0);
                GLUtils.texSubImage2D(3553, 0, 0, 0, decodeStream);
                decodeStream.recycle();
                initCountryCodeMapping();
                if (!RunnerJNILib.ms_loadLibraryFailed) {
                    RunnerJNILib.SetKeyValue(0, RunnerActivity.CurrentActivity.isTablet() ? 1 : 0, "");
                    RunnerJNILib.SetKeyValue(1, 0, this.m_context.getCacheDir().getAbsolutePath());
                    RunnerJNILib.SetKeyValue(2, 0, Locale.getDefault().getLanguage());
                    RunnerJNILib.SetKeyValue(3, this.m_context.getResources().getDisplayMetrics().densityDpi, "");
                    RunnerJNILib.SetKeyValue(4, this.m_context.getResources().getDisplayMetrics().densityDpi, "");
                    RunnerJNILib.SetKeyValue(5, Build.VERSION.SDK_INT, Build.VERSION.RELEASE);
                    try {
                        RunnerJNILib.SetKeyValue(8, 0, iso3CountryCodeToIso2CountryCode(Locale.getDefault().getISO3Country()));
                    } catch (MissingResourceException unused) {
                        RunnerJNILib.SetKeyValue(8, 0, "zz");
                    }
                }
            } finally {
                try {
                    inputStream.close();
                } catch (IOException unused2) {
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to locate assets, aborting...");
        }
    }

    public void onSurfaceChanged(GL10 gl10, int i, int i2) {
        this.m_width = i;
        this.m_height = i2;
        gl10.glViewport(0, 0, i, i2);
        Log.i("yoyo", "onSurfaceChanged :: width=" + this.m_width + " height=" + this.m_height);
    }

    public static void WaitForVsync() {
        long nanoTime = System.nanoTime();
        int i = elapsedVsyncs;
        while (elapsedVsyncs != -1 && i == elapsedVsyncs) {
            if (System.nanoTime() - nanoTime > 100000000) {
                Log.i("yoyo", "vsync timeout...");
                return;
            }
        }
    }

    public void onDrawFrame(GL10 gl10) {
        GL10 gl102 = gl10;
        if (!RunnerJNILib.ms_loadLibraryFailed) {
            int i = this.m_pausecountdown;
            if (i > 0) {
                this.m_pausecountdown = i - 1;
                if (this.m_pausecountdown <= 0) {
                    this.m_pauseRunner = true;
                }
            }
            if (this.m_pauseRunner) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Log.i("yoyo", "Paused runner has thrown an exception!");
                    e.printStackTrace();
                }
            } else {
                int i2 = 0;
                switch (m_state) {
                    case Startup:
                        m_state = eState.Splash;
                        Log.i("yoyo", "State->Splash");
                        gl102.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                        gl102.glClear(16384);
                        break;
                    case Splash:
                        if (RunnerActivity.mYYPrefs == null) {
                            this.splashEndTime = System.currentTimeMillis() + 1000;
                        } else {
                            this.splashEndTime = System.currentTimeMillis() + ((long) (RunnerActivity.mYYPrefs.getInt("SplashscreenTime") * 1000));
                        }
                        Log.i("yoyo", "State->Splash    time: " + System.currentTimeMillis());
                        Log.i("yoyo", "State->Splash endTime: " + this.splashEndTime);
                        if (RunnerActivity.YoYoRunner) {
                            m_state = eState.Splash2;
                            Log.i("yoyo", "State->Splash2");
                        } else if (RunnerActivity.UseAPKExpansionFile) {
                            m_state = eState.APKExpansionDownload;
                            Log.i("yoyo", "State->APKExpansionDownload");
                        } else {
                            Log.i("yoyo", "State->InitRunner");
                            m_state = eState.InitRunner;
                        }
                        RunnerJNILib.RenderSplash(m_apkFilePath, this.m_splashFilePath, this.m_width, this.m_height, this.m_texRawWidth, this.m_texRawHeight, this.m_texWidth, this.m_texHeight);
                        break;
                    case Splash2:
                        RunnerJNILib.RenderSplash(m_apkFilePath, this.m_splashFilePath, this.m_width, this.m_height, this.m_texRawWidth, this.m_texRawHeight, this.m_texWidth, this.m_texHeight);
                        break;
                    case APKExpansionDownload:
                        RunnerJNILib.RenderSplash(m_apkFilePath, this.m_splashFilePath, this.m_width, this.m_height, this.m_texRawWidth, this.m_texRawHeight, this.m_texWidth, this.m_texHeight);
                        if (RunnerActivity.APKExpansionFileReady) {
                            m_apkFilePath = (String) RunnerJNILib.CallExtensionFunction("PlayAPKExpansionExtension", "GetExpansionAPKFilename", 0, null);
                            Log.i("yoyo", "Download complete- path is:" + m_apkFilePath);
                            m_state = eState.InitRunner;
                            break;
                        }
                        break;
                    case InitRunner:
                        RunnerJNILib.RenderSplash(m_apkFilePath, this.m_splashFilePath, this.m_width, this.m_height, this.m_texRawWidth, this.m_texRawHeight, this.m_texWidth, this.m_texHeight);
                        m_state = eState.WaitForDoStartup;
                        RunnerActivity.ViewHandler.post(new Runnable() {
                            public void run() {
                                RunnerActivity.CurrentActivity.doSetup(DemoRenderer.m_apkFilePath);
                            }
                        });
                        break;
                    case WaitForDoStartup:
                        RunnerJNILib.RenderSplash(m_apkFilePath, this.m_splashFilePath, this.m_width, this.m_height, this.m_texRawWidth, this.m_texRawHeight, this.m_texWidth, this.m_texHeight);
                        break;
                    case WaitOnTimer:
                        RunnerJNILib.RenderSplash(m_apkFilePath, this.m_splashFilePath, this.m_width, this.m_height, this.m_texRawWidth, this.m_texRawHeight, this.m_texWidth, this.m_texHeight);
                        if (System.currentTimeMillis() >= this.splashEndTime) {
                            m_state = eState.DoStartup;
                            break;
                        }
                        break;
                    case DoStartup:
                        gl102.glDeleteTextures(1, new int[1], 0);
                        if (RunnerActivity.mYYPrefs == null) {
                            RunnerJNILib.Startup(m_apkFilePath, m_saveFilesDir, this.m_packageName, 0);
                        } else {
                            Log.i("yoyo", "Sleepmargin: " + RunnerActivity.mYYPrefs.getInt("SleepMargin"));
                            RunnerJNILib.Startup(m_apkFilePath, m_saveFilesDir, this.m_packageName, RunnerActivity.mYYPrefs.getInt("SleepMargin"));
                        }
                        m_state = eState.Process;
                        break;
                    case Process:
                        if (!RunnerJNILib.ms_exitcalled) {
                            if (RunnerActivity.XPeriaPlay && this.m_context.getResources().getConfiguration().navigation == 2 && this.m_context.getResources().getConfiguration().navigationHidden == 1) {
                                i2 = 1;
                            }
                            do {
                                this.m_refreshRate = RunnerActivity.CurrentActivity.getRefreshRate();
                                int Process = RunnerJNILib.Process(this.m_width, this.m_height, RunnerActivity.AccelX, RunnerActivity.AccelY, RunnerActivity.AccelZ, i2, RunnerActivity.Orientation, this.m_refreshRate);
                                if (Process == 0) {
                                    Log.i("yoyo", "RunnerJNILib.Process returned 0");
                                    RunnerJNILib.ExitApplication();
                                } else if (Process == 2) {
                                    Log.i("yoyo", "RunnerJNILib.Process has returned that it is due to restart");
                                    m_state = eState.Startup;
                                    RunnerActivity runnerActivity = RunnerActivity.CurrentActivity;
                                    RunnerActivity.HasRestarted = true;
                                }
                                if (RunnerJNILib.canFlip()) {
                                    break;
                                }
                            } while (m_state == eState.Process);
                            break;
                        }
                        break;
                }
                this.m_renderCount--;
            }
        } else if (!ms_displayedLoadLibraryFailed) {
            ms_displayedLoadLibraryFailed = true;
            RunnerActivity.ViewHandler.post(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RunnerJNILib.ms_context);
                    builder.setMessage("Unable to find library for this devices architecture, which is " + System.getProperty("os.arch") + ", ensure you have included the correct architecture in your APK").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            RunnerActivity.CurrentActivity.finish();
                        }
                    });
                    builder.create().show();
                }
            });
        }
    }
}
