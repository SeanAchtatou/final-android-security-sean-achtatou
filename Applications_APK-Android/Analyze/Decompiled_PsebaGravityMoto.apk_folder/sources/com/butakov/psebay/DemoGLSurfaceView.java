package com.butakov.psebay;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import com.yoyogames.runner.RunnerJNILib;
import java.util.Locale;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

public class DemoGLSurfaceView extends GLSurfaceView implements GLSurfaceView.EGLConfigChooser {
    public static int m_usingGL2;
    public DemoRenderer mRenderer;
    private Context m_context;
    /* access modifiers changed from: private */
    public int m_fpsTime;
    int m_prev;
    /* access modifiers changed from: private */
    public Handler m_refreshHandler = new Handler();
    private Runnable m_refreshTick = new Runnable() {
        public void run() {
            DemoGLSurfaceView.this.m_refreshHandler.postDelayed(this, (long) DemoGLSurfaceView.this.m_fpsTime);
            if (DemoGLSurfaceView.this.mRenderer.m_renderCount <= 0) {
                DemoGLSurfaceView.this.mRenderer.m_renderCount++;
                DemoGLSurfaceView.this.requestRender();
            }
        }
    };

    /* JADX WARNING: Removed duplicated region for block: B:107:0x02c7  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x02ca  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0207 A[LOOP:0: B:71:0x0205->B:72:0x0207, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x023d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public javax.microedition.khronos.egl.EGLConfig chooseConfig(javax.microedition.khronos.egl.EGL10 r19, javax.microedition.khronos.egl.EGLDisplay r20) {
        /*
            r18 = this;
            r0 = r18
            r7 = r19
            r8 = r20
            java.lang.String r9 = "yoyo"
            java.lang.String r1 = "chooseConfig"
            android.util.Log.i(r9, r1)
            r10 = 1
            int[] r11 = new int[r10]
            r12 = 0
            r11[r12] = r12
            r1 = 11
            int[] r13 = new int[r1]
            r1 = 12324(0x3024, float:1.727E-41)
            r13[r12] = r1
            r1 = 5
            r13[r10] = r1
            r14 = 2
            r2 = 12323(0x3023, float:1.7268E-41)
            r13[r14] = r2
            r2 = 6
            r15 = 3
            r13[r15] = r2
            r3 = 4
            r4 = 12322(0x3022, float:1.7267E-41)
            r13[r3] = r4
            r13[r1] = r1
            r1 = 12325(0x3025, float:1.7271E-41)
            r13[r2] = r1
            r1 = 7
            r2 = 16
            r13[r1] = r2
            r1 = 8
            r2 = 12352(0x3040, float:1.7309E-41)
            r13[r1] = r2
            int r1 = com.butakov.psebay.DemoGLSurfaceView.m_usingGL2
            if (r1 == 0) goto L_0x0042
            goto L_0x0043
        L_0x0042:
            r3 = 1
        L_0x0043:
            r6 = 9
            r13[r6] = r3
            r1 = 10
            r16 = 12344(0x3038, float:1.7298E-41)
            r13[r1] = r16
            r4 = 0
            r5 = 0
            r1 = r19
            r2 = r20
            r3 = r13
            r14 = 9
            r6 = r11
            r1.eglChooseConfig(r2, r3, r4, r5, r6)
            int r1 = r19.eglGetError()
            java.lang.String r6 = "No EGL configs match our minimum required spec"
            r5 = 12288(0x3000, float:1.7219E-41)
            if (r5 == r1) goto L_0x00ad
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error choosing original minspec EGL config : "
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            android.util.Log.i(r9, r1)
            int[] r13 = new int[r14]
            r13 = {12324, 5, 12323, 6, 12322, 5, 12325, 16, 12344} // fill-array
            r4 = 0
            r14 = 0
            r1 = r19
            r2 = r20
            r3 = r13
            r10 = 12288(0x3000, float:1.7219E-41)
            r5 = r14
            r14 = r6
            r6 = r11
            r1.eglChooseConfig(r2, r3, r4, r5, r6)
            int r1 = r19.eglGetError()
            if (r10 != r1) goto L_0x0093
            goto L_0x00b0
        L_0x0093:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Still an error choosing cutdown minspec EGL config : "
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            android.util.Log.i(r9, r1)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            r1.<init>(r14)
            throw r1
        L_0x00ad:
            r14 = r6
            r10 = 12288(0x3000, float:1.7219E-41)
        L_0x00b0:
            r3 = r13
            r1 = r11[r12]
            if (r1 <= 0) goto L_0x02d2
            r13 = r11[r12]
            javax.microedition.khronos.egl.EGLConfig[] r14 = new javax.microedition.khronos.egl.EGLConfig[r13]
            r1 = r19
            r2 = r20
            r4 = r14
            r5 = r13
            r6 = r11
            r1.eglChooseConfig(r2, r3, r4, r5, r6)
            int r1 = r19.eglGetError()
            if (r10 == r1) goto L_0x00dd
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error fetching EGL configs : "
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            android.util.Log.i(r9, r1)
        L_0x00dd:
            r1 = 12440(0x3098, float:1.7432E-41)
            int[] r2 = new int[r15]
            r2[r12] = r1
            int r1 = com.butakov.psebay.DemoGLSurfaceView.m_usingGL2
            if (r1 == 0) goto L_0x00eb
            r1 = 1
            r17 = 2
            goto L_0x00ee
        L_0x00eb:
            r1 = 1
            r17 = 1
        L_0x00ee:
            r2[r1] = r17
            r1 = 2
            r2[r1] = r16
            r1 = r14[r12]
            javax.microedition.khronos.egl.EGLContext r3 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            javax.microedition.khronos.egl.EGLContext r1 = r7.eglCreateContext(r8, r1, r3, r2)
            int r3 = r19.eglGetError()
            r4 = 12292(0x3004, float:1.7225E-41)
            r5 = 0
            if (r4 != r3) goto L_0x0116
            java.lang.String r1 = "Bad Attrib on eglCreateContext... using empty attrib_list"
            android.util.Log.i(r9, r1)
            r1 = r14[r12]
            javax.microedition.khronos.egl.EGLContext r2 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            javax.microedition.khronos.egl.EGLContext r1 = r7.eglCreateContext(r8, r1, r2, r5)
            int r3 = r19.eglGetError()
            r2 = r5
        L_0x0116:
            if (r3 != r10) goto L_0x0190
            if (r1 == 0) goto L_0x0190
            javax.microedition.khronos.egl.EGLContext r4 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            if (r1 == r4) goto L_0x0190
            r3 = r14[r12]
            android.view.SurfaceHolder r4 = r18.getHolder()
            javax.microedition.khronos.egl.EGLSurface r3 = r7.eglCreateWindowSurface(r8, r3, r4, r5)
            if (r3 == 0) goto L_0x0180
            javax.microedition.khronos.egl.EGLSurface r4 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE
            if (r3 != r4) goto L_0x012f
            goto L_0x0180
        L_0x012f:
            boolean r4 = r7.eglMakeCurrent(r8, r3, r3, r1)
            if (r4 != 0) goto L_0x013e
            java.lang.String r4 = "EGLHelper"
            java.lang.String r6 = "eglMakeCurrent broke"
            android.util.Log.i(r4, r6)
            r4 = 1
            goto L_0x017c
        L_0x013e:
            javax.microedition.khronos.opengles.GL r4 = r1.getGL()
            boolean r6 = r4 instanceof javax.microedition.khronos.opengles.GL10
            if (r6 == 0) goto L_0x0172
            javax.microedition.khronos.opengles.GL10 r4 = (javax.microedition.khronos.opengles.GL10) r4
            r6 = 7939(0x1f03, float:1.1125E-41)
            java.lang.String r4 = r4.glGetString(r6)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r11 = "OpenGL ES Extensions : "
            r6.append(r11)
            r6.append(r4)
            java.lang.String r6 = r6.toString()
            android.util.Log.i(r9, r6)
            java.lang.String r6 = "GL_OES_rgb8_rgba8"
            boolean r4 = r4.contains(r6)
            r6 = 1
            if (r4 != r6) goto L_0x0172
            java.lang.String r4 = "Device supports 32bit display formats"
            android.util.Log.i(r9, r4)
            r4 = 0
            goto L_0x0173
        L_0x0172:
            r4 = 1
        L_0x0173:
            javax.microedition.khronos.egl.EGLSurface r6 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE
            javax.microedition.khronos.egl.EGLSurface r11 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE
            javax.microedition.khronos.egl.EGLContext r15 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            r7.eglMakeCurrent(r8, r6, r11, r15)
        L_0x017c:
            r7.eglDestroySurface(r8, r3)
            goto L_0x018c
        L_0x0180:
            int r3 = r19.eglGetError()
            if (r3 == r10) goto L_0x018b
            java.lang.String r3 = "window surface can't be created"
            android.util.Log.i(r9, r3)
        L_0x018b:
            r4 = 1
        L_0x018c:
            r7.eglDestroyContext(r8, r1)
            goto L_0x01b6
        L_0x0190:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "Could not create test "
            r1.append(r4)
            int r4 = com.butakov.psebay.DemoGLSurfaceView.m_usingGL2
            if (r4 == 0) goto L_0x01a1
            java.lang.String r4 = "GL2"
            goto L_0x01a3
        L_0x01a1:
            java.lang.String r4 = "GL1"
        L_0x01a3:
            r1.append(r4)
            java.lang.String r4 = "context. EGLError: "
            r1.append(r4)
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.i(r9, r1)
            r4 = 1
        L_0x01b6:
            android.content.Context r1 = r0.m_context
            if (r1 == 0) goto L_0x01e0
            boolean r3 = r1 instanceof com.butakov.psebay.RunnerActivity
            if (r3 == 0) goto L_0x01e0
            com.butakov.psebay.RunnerActivity r1 = (com.butakov.psebay.RunnerActivity) r1
            com.butakov.psebay.IniBundle r1 = com.butakov.psebay.RunnerActivity.mYYPrefs
            if (r1 == 0) goto L_0x01e0
            android.content.Context r1 = r0.m_context
            com.butakov.psebay.RunnerActivity r1 = (com.butakov.psebay.RunnerActivity) r1
            com.butakov.psebay.IniBundle r1 = com.butakov.psebay.RunnerActivity.mYYPrefs
            java.lang.String r3 = "YYUse24Bit"
            int r1 = r1.getInt(r3)
            r3 = 1
            if (r1 != r3) goto L_0x01d9
            java.lang.String r1 = "24 bit colour depth allowed"
            android.util.Log.i(r9, r1)
            goto L_0x0202
        L_0x01d9:
            java.lang.String r1 = "16 bit colour depth forced"
            android.util.Log.i(r9, r1)
            r4 = 1
            goto L_0x0202
        L_0x01e0:
            r3 = 1
            android.content.Context r1 = r0.m_context
            if (r1 != 0) goto L_0x01ea
            java.lang.String r1 = "Context NULL"
            android.util.Log.i(r9, r1)
        L_0x01ea:
            android.content.Context r1 = r0.m_context
            boolean r1 = r1 instanceof com.butakov.psebay.RunnerActivity
            if (r1 != 0) goto L_0x01f5
            java.lang.String r1 = "Context not RunnerActivity"
            android.util.Log.i(r9, r1)
        L_0x01f5:
            android.content.Context r1 = r0.m_context
            com.butakov.psebay.RunnerActivity r1 = (com.butakov.psebay.RunnerActivity) r1
            com.butakov.psebay.IniBundle r1 = com.butakov.psebay.RunnerActivity.mYYPrefs
            if (r1 != 0) goto L_0x0202
            java.lang.String r1 = "mYYPrefs null"
            android.util.Log.i(r9, r1)
        L_0x0202:
            int[] r1 = new int[r13]
            r6 = 0
        L_0x0205:
            if (r6 >= r13) goto L_0x0212
            r11 = r14[r6]
            int r11 = r0.generateConfigSortKey(r7, r8, r11, r4)
            r1[r6] = r11
            int r6 = r6 + 1
            goto L_0x0205
        L_0x0212:
            r4 = 0
        L_0x0213:
            if (r4 != 0) goto L_0x0239
            r4 = 0
            r6 = 1
        L_0x0217:
            int r11 = r13 + -1
            if (r4 >= r11) goto L_0x0237
            r11 = r1[r4]
            int r15 = r4 + 1
            r3 = r1[r15]
            if (r11 >= r3) goto L_0x0234
            r3 = r14[r4]
            r6 = r14[r15]
            r14[r4] = r6
            r14[r15] = r3
            r3 = r1[r4]
            r6 = r1[r15]
            r1[r4] = r6
            r1[r15] = r3
            r6 = 0
        L_0x0234:
            r4 = r15
            r3 = 1
            goto L_0x0217
        L_0x0237:
            r4 = r6
            goto L_0x0213
        L_0x0239:
            r1 = -1
            r3 = -1
        L_0x023b:
            if (r12 >= r13) goto L_0x02c5
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "Trying EGL config : "
            r4.append(r6)
            r6 = r14[r12]
            java.lang.String r6 = r0.printConfig(r7, r8, r6)
            r4.append(r6)
            java.lang.String r4 = r4.toString()
            android.util.Log.i(r9, r4)
            r4 = r14[r12]
            javax.microedition.khronos.egl.EGLContext r6 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            javax.microedition.khronos.egl.EGLContext r4 = r7.eglCreateContext(r8, r4, r6, r2)
            int r6 = r19.eglGetError()
            if (r6 != r10) goto L_0x02aa
            if (r4 == 0) goto L_0x02aa
            javax.microedition.khronos.egl.EGLContext r11 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            if (r4 == r11) goto L_0x02aa
            r6 = r14[r12]
            android.view.SurfaceHolder r11 = r18.getHolder()
            javax.microedition.khronos.egl.EGLSurface r6 = r7.eglCreateWindowSurface(r8, r6, r11, r5)
            if (r6 == 0) goto L_0x029b
            javax.microedition.khronos.egl.EGLSurface r11 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE
            if (r6 != r11) goto L_0x027c
            goto L_0x029b
        L_0x027c:
            boolean r11 = r7.eglMakeCurrent(r8, r6, r6, r4)
            if (r11 != 0) goto L_0x0288
            java.lang.String r11 = "eglMakeCurrent failed - can't use this mode"
            android.util.Log.i(r9, r11)
            goto L_0x0297
        L_0x0288:
            java.lang.String r3 = "Selected EGL config working"
            android.util.Log.i(r9, r3)
            javax.microedition.khronos.egl.EGLSurface r3 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE
            javax.microedition.khronos.egl.EGLSurface r11 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE
            javax.microedition.khronos.egl.EGLContext r15 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            r7.eglMakeCurrent(r8, r3, r11, r15)
            r3 = r12
        L_0x0297:
            r7.eglDestroySurface(r8, r6)
            goto L_0x02a6
        L_0x029b:
            int r6 = r19.eglGetError()
            if (r6 == r10) goto L_0x02a6
            java.lang.String r6 = "Surface can't be created - can't use this mode"
            android.util.Log.i(r9, r6)
        L_0x02a6:
            r7.eglDestroyContext(r8, r4)
            goto L_0x02be
        L_0x02aa:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r11 = "Selected EGL config failed: "
            r4.append(r11)
            r4.append(r6)
            java.lang.String r4 = r4.toString()
            android.util.Log.i(r9, r4)
        L_0x02be:
            if (r3 == r1) goto L_0x02c1
            goto L_0x02c5
        L_0x02c1:
            int r12 = r12 + 1
            goto L_0x023b
        L_0x02c5:
            if (r3 == r1) goto L_0x02ca
            r1 = r14[r3]
            return r1
        L_0x02ca:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "No valid EGL configs match our minimum required spec"
            r1.<init>(r2)
            throw r1
        L_0x02d2:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            r1.<init>(r14)
            goto L_0x02d9
        L_0x02d8:
            throw r1
        L_0x02d9:
            goto L_0x02d8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.butakov.psebay.DemoGLSurfaceView.chooseConfig(javax.microedition.khronos.egl.EGL10, javax.microedition.khronos.egl.EGLDisplay):javax.microedition.khronos.egl.EGLConfig");
    }

    private String printConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
        int findConfigAttrib = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12328, 0);
        int findConfigAttrib2 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12324, 0);
        int findConfigAttrib3 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12323, 0);
        int findConfigAttrib4 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12322, 0);
        int findConfigAttrib5 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12321, 0);
        int findConfigAttrib6 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12325, 0);
        int findConfigAttrib7 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12326, 0);
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(Locale.US, "EGLConfig %d: rgba=%d%d%d%d depth=%d stencil=%d", Integer.valueOf(findConfigAttrib), Integer.valueOf(findConfigAttrib2), Integer.valueOf(findConfigAttrib3), Integer.valueOf(findConfigAttrib4), Integer.valueOf(findConfigAttrib5), Integer.valueOf(findConfigAttrib6), Integer.valueOf(findConfigAttrib7)));
        sb.append(" EGL_ALPHA_MASK_SIZE=");
        EGL10 egl102 = egl10;
        EGLDisplay eGLDisplay2 = eGLDisplay;
        EGLConfig eGLConfig2 = eGLConfig;
        sb.append(findConfigAttrib(egl102, eGLDisplay2, eGLConfig2, 12350, 0));
        sb.append(" EGL_BUFFER_SIZE=");
        sb.append(findConfigAttrib(egl102, eGLDisplay2, eGLConfig2, 12320, 0));
        sb.append(" EGL_COLOR_BUFFER_TYPE=");
        sb.append(findConfigAttrib(egl102, eGLDisplay2, eGLConfig2, 12351, 0));
        sb.append(String.format(Locale.US, " EGL_CONFIG_CAVEAT=0x%04x", Integer.valueOf(findConfigAttrib(egl102, eGLDisplay2, eGLConfig2, 12327, 0))));
        sb.append(" EGL_LEVEL=");
        EGL10 egl103 = egl10;
        EGLDisplay eGLDisplay3 = eGLDisplay;
        EGLConfig eGLConfig3 = eGLConfig;
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12329, 0));
        sb.append(" EGL_LUMINANCE_SIZE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12349, 0));
        sb.append(" EGL_MAX_PBUFFER_WIDTH=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12332, 0));
        sb.append(" EGL_MAX_PBUFFER_HEIGHT=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12330, 0));
        sb.append(" EGL_MAX_PBUFFER_PIXELS=");
        EGL10 egl104 = egl10;
        EGLDisplay eGLDisplay4 = eGLDisplay;
        EGLConfig eGLConfig4 = eGLConfig;
        sb.append(findConfigAttrib(egl104, eGLDisplay4, eGLConfig4, 12331, 0));
        sb.append(" EGL_MAX_PBUFFER_HEIGHT=");
        sb.append(findConfigAttrib(egl104, eGLDisplay4, eGLConfig4, 12330, 0));
        sb.append(" EGL_MAX_PBUFFER_HEIGHT=");
        sb.append(findConfigAttrib(egl104, eGLDisplay4, eGLConfig4, 12330, 0));
        sb.append(" EGL_NATIVE_RENDERABLE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12333, 0));
        sb.append(" EGL_NATIVE_VISUAL_TYPE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12335, 0));
        sb.append(" EGL_RENDERABLE_TYPE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12352, 0));
        sb.append(" EGL_SAMPLE_BUFFERS=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12338, 0));
        sb.append(" EGL_SAMPLES=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12337, 0));
        sb.append(" EGL_SURFACE_TYPE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12339, 0));
        sb.append(" EGL_TRANSPARENT_TYPE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12340, 0));
        sb.append(" EGL_TRANSPARENT_RED_VALUE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12343, 0));
        sb.append(" EGL_TRANSPARENT_GREEN_VALUE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12342, 0));
        sb.append(" EGL_TRANSPARENT_BLUE_VALUE=");
        sb.append(findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12341, 0));
        return sb.toString();
    }

    private int findConfigAttrib(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i, int i2) {
        int[] iArr = new int[1];
        return egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, iArr) ? iArr[0] : i2;
    }

    private int generateConfigSortKey(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, boolean z) {
        int findConfigAttrib = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12320, 0);
        int findConfigAttrib2 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12325, 0);
        int findConfigAttrib3 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12326, 0);
        int findConfigAttrib4 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12327, 0);
        int findConfigAttrib5 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12338, 0);
        int i = 0;
        if (findConfigAttrib4 == 12344) {
            i = 2;
        } else if (findConfigAttrib4 == 12368) {
            i = 1;
        }
        int i2 = ((32 - findConfigAttrib5) << 18) | (i << 24) | (findConfigAttrib << 12) | (findConfigAttrib2 << 6) | findConfigAttrib3;
        if (!z || findConfigAttrib <= 16) {
            return i2;
        }
        return -1;
    }

    private boolean checkGL20Support(Context context) {
        if (Build.VERSION.SDK_INT < 8) {
            Log.i("yoyo", "Android OS version below minimum required for GL2...");
            return false;
        }
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl10.eglInitialize(eglGetDisplay, new int[2]);
        int[] iArr = new int[1];
        egl10.eglChooseConfig(eglGetDisplay, new int[]{12324, 4, 12323, 4, 12322, 4, 12352, 4, 12344}, new EGLConfig[10], 10, iArr);
        egl10.eglTerminate(eglGetDisplay);
        if (iArr[0] > 0) {
            return true;
        }
        return false;
    }

    public DemoGLSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        RunnerActivity.CurrentActivity.setupIniFile();
        RunnerActivity.CurrentActivity.RestrictOrientation(false, false, false, false, true);
        RunnerActivity runnerActivity = (RunnerActivity) this.m_context;
        IniBundle iniBundle = RunnerActivity.mYYPrefs;
        Log.i("yoyo", "Trying GL2 config...");
        int i = !checkGL20Support(context) ? 0 : 1;
        if (!RunnerJNILib.ms_loadLibraryFailed) {
            if (i > 0) {
                i = RunnerJNILib.initGLFuncs(1);
            } else {
                i = RunnerJNILib.initGLFuncs(0);
            }
        }
        if (i == 0) {
            m_usingGL2 = 0;
            Log.i("yoyo", "Using OpenGL ES 1 renderer");
            Log.i("yoyo", "DemoGLSurfaceView: CREATED");
            this.m_context = context;
            this.m_prev = 0;
            this.m_fpsTime = 16;
            this.mRenderer = new DemoRenderer(context);
            setEGLConfigChooser(this);
            setRenderer(this.mRenderer);
            return;
        }
        m_usingGL2 = 1;
        setEGLContextClientVersion(2);
        Log.i("yoyo", "Using OpenGL ES 2 renderer");
        Log.i("yoyo", "DemoGLSurfaceView: CREATED");
        this.m_context = context;
        this.m_prev = 0;
        this.m_fpsTime = 16;
        this.mRenderer = new DemoRendererGL2(context);
        setEGLConfigChooser(this);
        setRenderer(this.mRenderer);
    }

    private void dumpEvent(MotionEvent motionEvent) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        int action = motionEvent.getAction();
        int i2 = action & 255;
        sb.append("event ACTION_");
        sb.append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[i2]);
        if (i2 == 5 || i2 == 6) {
            sb.append("(pid ");
            sb.append(action >> 8);
            sb.append(")");
        }
        sb.append("[");
        while (i < motionEvent.getPointerCount()) {
            sb.append("#");
            sb.append(i);
            sb.append("(pid ");
            sb.append(motionEvent.getPointerId(i));
            sb.append(")=");
            sb.append((int) motionEvent.getX(i));
            sb.append(",");
            sb.append((int) motionEvent.getY(i));
            i++;
            if (i < motionEvent.getPointerCount()) {
                sb.append(";");
            }
        }
        sb.append("]");
        Log.i("yoyo", sb.toString());
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        boolean onGenericMotionEvent;
        if (RunnerActivity.mExtension != null) {
            for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                if ((RunnerActivity.mExtension[i] instanceof IExtensionBase) && (onGenericMotionEvent = ((IExtensionBase) RunnerActivity.mExtension[i]).onGenericMotionEvent(motionEvent))) {
                    return onGenericMotionEvent;
                }
            }
        }
        return super.onGenericMotionEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent;
        if (RunnerActivity.mExtension != null) {
            for (int i = 0; i < RunnerActivity.mExtension.length; i++) {
                if ((RunnerActivity.mExtension[i] instanceof IExtensionBase) && (onTouchEvent = ((IExtensionBase) RunnerActivity.mExtension[i]).onTouchEvent(motionEvent))) {
                    return onTouchEvent;
                }
            }
        }
        if (RunnerJNILib.ms_loadLibraryFailed) {
            return true;
        }
        int action = motionEvent.getAction();
        int i2 = action & 255;
        for (int i3 = 0; i3 < motionEvent.getPointerCount(); i3++) {
            int pointerId = motionEvent.getPointerId(i3);
            if (i2 != 5 && i2 != 6) {
                RunnerJNILib.TouchEvent(i2, pointerId, motionEvent.getX(i3), motionEvent.getY(i3));
            } else if (((65280 & action) >> 8) == i3) {
                RunnerJNILib.TouchEvent(i2, pointerId, motionEvent.getX(i3), motionEvent.getY(i3));
            } else {
                RunnerJNILib.TouchEvent(2, pointerId, motionEvent.getX(i3), motionEvent.getY(i3));
            }
        }
        if (RunnerActivity.CurrentActivity.vsyncHandler == null) {
            try {
                Thread.sleep(16);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }
}
