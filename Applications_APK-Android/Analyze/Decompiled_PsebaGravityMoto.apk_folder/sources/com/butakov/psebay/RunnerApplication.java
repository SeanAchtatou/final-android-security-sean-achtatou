package com.butakov.psebay;

import androidx.multidex.MultiDexApplication;
import com.yoyogames.runner.RunnerJNILib;

public class RunnerApplication extends MultiDexApplication {
    public void onCreate() {
        super.onCreate();
        if (!RunnerJNILib.ms_loadLibraryFailed) {
            RunnerJNILib.Init();
        }
    }
}
