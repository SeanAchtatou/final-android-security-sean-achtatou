package com.butakov.psebay;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.SnapshotsClient;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.yoyogames.runner.RunnerJNILib;

public class GooglePlayServicesExtension extends RunnerSocial implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String DIALOG_ERROR = "dialog_error";
    private static final int EVENT_OTHER_SOCIAL = 70;
    private static final int GooglePlayServices_IncrementAchievementResultEvent = 9820;
    private static final int GooglePlayServices_PostAchievementResultEvent = 9819;
    private static final int GooglePlayServices_PostScoreResultEvent = 9818;
    private static final int GooglePlayServices_RevealAchievementResultEvent = 9821;
    private static final int MAX_SNAPSHOT_RESOLVE_RETRIES = 5;
    private static final int RC_SIGN_IN = 9001;
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private int LastGSId = -1;
    final int RC_GPS_ACTIVITY = 5011;
    final int RC_RESOLVE = 5000;
    final int RC_UNUSED = 5001;
    private RunnerBillingInterface iap_controller = null;
    private boolean mAutoStartSignInFlow = true;
    private boolean mCloudServicesEnabled = false;
    private int mCloudSyncConflictRetries = 0;
    private boolean mCloudSyncInProgress = false;
    private String mCurrentSaveName = "DefaultSave";
    private GoogleSignInAccount mGoogleSignInAccount = null;
    private GoogleSignInClient mGoogleSigninClient = null;
    /* access modifiers changed from: private */
    public final OnSnapshotResolvedListener mOnCloudSyncListener = new OnSnapshotResolvedListener() {
        public void onSuccess(Snapshot snapshot, int i) {
            try {
                byte[] unused = GooglePlayServicesExtension.this.mSaveGameData = snapshot.getSnapshotContents().readFully();
                String description = snapshot.getMetadata().getDescription();
                Log.i("yoyo", "Successfully loaded cloud save data. Description: " + description);
                RunnerJNILib.CloudResultData(GooglePlayServicesExtension.this.mSaveGameData, description.getBytes(), 0, i);
            } catch (Exception e) {
                onFailure("Error while reading cloud save snapshot. Sending Fail event" + e, i);
                RunnerJNILib.CloudResultData(null, null, 0, i);
            }
        }

        public void onFailure(String str, int i) {
            Log.i("yoyo", "Cloud sync failed: " + str);
            RunnerJNILib.CloudResultData(null, null, 0, i);
        }
    };
    private boolean mResolvingConnectionFailure = false;
    Object mRunnerBilling = null;
    /* access modifiers changed from: private */
    public byte[] mSaveGameData = null;
    private boolean mSignInClicked = false;
    /* access modifiers changed from: private */
    public boolean mSignOutClicked = false;
    private boolean mSignedInOnPause = false;

    public interface OnSnapshotResolvedListener {
        void onFailure(String str, int i);

        void onSuccess(Snapshot snapshot, int i);
    }

    public void onConnected(Bundle bundle) {
    }

    public void GooglePlayServices_Init() {
        if (this.mGoogleSigninClient != null) {
            Log.i("yoyo", "Attempting to initialise Google Play Signin Client when it has already been initialised.");
            return;
        }
        RunnerActivity runnerActivity = RunnerActivity.CurrentActivity;
        String string = RunnerActivity.mYYPrefs.getString("com.google.android.gms.games.APP_ID");
        if (string == null || string.isEmpty()) {
            Log.i("yoyo", "Failed to find appid, Google Play Services will not be initialised.");
            return;
        }
        Log.i("yoyo", "Initialising Google Play Services. App id: " + string);
        GoogleSignInOptions.Builder builder = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN);
        builder.requestScopes(Games.SCOPE_GAMES, new Scope[0]);
        RunnerActivity runnerActivity2 = RunnerActivity.CurrentActivity;
        this.mCloudServicesEnabled = RunnerActivity.mYYPrefs.getBoolean("YYGoogleCloudSavingEnabled");
        if (this.mCloudServicesEnabled) {
            builder.requestScopes(Drive.SCOPE_APPFOLDER, new Scope[0]);
        }
        Log.i("yoyo", "Signing into google game services. Cloud enabled: " + this.mCloudServicesEnabled + ". Options builder: " + builder);
        this.mGoogleSigninClient = GoogleSignIn.getClient(RunnerJNILib.ms_context, builder.build());
    }

    public void Init() {
        if (GooglePlayServices_Status() == 0.0d) {
            Log.i("yoyo", "Google Play Services extension initialising");
            GooglePlayServices_Init();
            return;
        }
        Log.i("yoyo", "Google Play Services extension not initialising as AreGooglePlayServicesAvailable returns false");
    }

    public void onStart() {
        Log.i("yoyo", "googleplayservices extension onStart called");
    }

    public void onStop() {
        Log.i("yoyo", "googleplayservices extension onStop called");
    }

    public void onPause() {
        this.mSignedInOnPause = isSignedIn();
    }

    public void onResume() {
        Log.i("yoyo", "googleplayservices extension onResume called. Signed in on pause: " + this.mSignedInOnPause + ". Signed in now: " + isSignedIn());
        if (this.mSignedInOnPause && !isSignedIn()) {
            Log.i("yoyo", "Re-logging in..");
            Login();
        }
    }

    public double GooglePlayServices_Status() {
        return (double) GooglePlayServicesUtil.isGooglePlayServicesAvailable(RunnerJNILib.ms_context);
    }

    private void showErrorDialog(int i, int i2) {
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(i, RunnerActivity.CurrentActivity, i2);
        if (errorDialog != null) {
            errorDialog.show();
        } else {
            Log.i("yoyo", "Google Play Services Error and unable to initialise GooglePlayServicesUtil error dialog");
        }
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("yoyo", "onConnectionFailed called with result:" + connectionResult);
    }

    public void onConnectionSuspended(int i) {
        Log.i("yoyo", "onConnectionSuspended call with " + i);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        Log.i("yoyo", "gps onActivityResult called. RequestCode: " + i + ". ResponseCode: " + i2);
        if (i != 5011) {
            if (i != 9001) {
                Log.i("yoyo", "onActivityResult called with " + i);
                return;
            }
            this.mSignInClicked = false;
            try {
                Task<GoogleSignInAccount> signedInAccountFromIntent = GoogleSignIn.getSignedInAccountFromIntent(intent);
                if (signedInAccountFromIntent.isSuccessful()) {
                    this.mGoogleSignInAccount = signedInAccountFromIntent.getResult();
                    onLoginSuccess(this.mGoogleSignInAccount);
                    return;
                }
                Log.i("yoyo", "Login failed! Exception: " + signedInAccountFromIntent.getException() + ". Message: " + signedInAccountFromIntent.getException().getMessage() + ". Stack: " + signedInAccountFromIntent.getException().getStackTrace());
                onLoginFailed();
            } catch (Exception e) {
                Log.i("yoyo", "Exception when handling GP services login: " + e);
                onLoginFailed();
            }
        } else if (i2 == 10001) {
            Log.i("yoyo", "Activity result resulted in reconnect required");
            Logout();
        }
    }

    public Object InitRunnerBilling() {
        if (this.mRunnerBilling == null) {
            try {
                this.iap_controller = (RunnerBillingInterface) Class.forName("com.butakov.psebay.RunnerBilling").getConstructor(new Class[0]).newInstance(new Object[0]);
                Log.i("yoyo", "Created iap_controller, about to call InitRunnerBilling");
                this.mRunnerBilling = this.iap_controller.InitRunnerBilling();
                Log.i("yoyo", "iap_controller successfully created");
            } catch (Exception e) {
                Log.i("yoyo", "Failed to initialize Google Play Services IAP functionality - could not initialise RunnerBilling:" + e);
                Log.i("yoyo", "If you are intending to use Google Play Services IAP's please ensure you have the GooglePlayServicesIAPExtension added to your project");
                Log.i("yoyo", e.toString());
                Log.i("yoyo", e.getMessage());
                e.printStackTrace();
            }
        }
        return this.mRunnerBilling;
    }

    private GoogleSignInAccount getCurrentAccountSignedIn() {
        GoogleSignInAccount googleSignInAccount = this.mGoogleSignInAccount;
        if (googleSignInAccount != null) {
            return googleSignInAccount;
        }
        return GoogleSignIn.getLastSignedInAccount(RunnerJNILib.ms_context);
    }

    public boolean isSignedIn() {
        return getCurrentAccountSignedIn() != null;
    }

    public void Login() {
        if (isSignedIn()) {
            Log.i("yoyo", "Called achievement_login when already logged in");
        } else if (this.mGoogleSigninClient == null) {
            Log.i("yoyo", "Called achievement_login with a NULL GoogleSigninClient");
        } else {
            Log.i("yoyo", "Signing in..");
            Task<GoogleSignInAccount> silentSignIn = this.mGoogleSigninClient.silentSignIn();
            this.mSignInClicked = true;
            if (silentSignIn.isComplete()) {
                onLoginTaskComplete(silentSignIn);
            } else {
                silentSignIn.addOnCompleteListener(new OnCompleteListener<GoogleSignInAccount>() {
                    public void onComplete(Task<GoogleSignInAccount> task) {
                        GooglePlayServicesExtension.this.onLoginTaskComplete(task);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public void onLoginTaskComplete(Task<GoogleSignInAccount> task) {
        if (task.isSuccessful()) {
            Log.i("yoyo", "Silent sign in successful");
            this.mSignInClicked = false;
            this.mGoogleSignInAccount = task.getResult();
            onLoginSuccess(this.mGoogleSignInAccount);
            return;
        }
        Log.i("yoyo", "Silent sign in failed, attempting normal sign in..");
        RunnerActivity.CurrentActivity.startActivityForResult(this.mGoogleSigninClient.getSignInIntent(), 9001);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.Games.getGamesClient(android.app.Activity, com.google.android.gms.auth.api.signin.GoogleSignInAccount):com.google.android.gms.games.GamesClient
     arg types: [com.butakov.psebay.RunnerActivity, com.google.android.gms.auth.api.signin.GoogleSignInAccount]
     candidates:
      com.google.android.gms.games.Games.getGamesClient(android.content.Context, com.google.android.gms.auth.api.signin.GoogleSignInAccount):com.google.android.gms.games.GamesClient
      com.google.android.gms.games.Games.getGamesClient(android.app.Activity, com.google.android.gms.auth.api.signin.GoogleSignInAccount):com.google.android.gms.games.GamesClient */
    private void onLoginSuccess(GoogleSignInAccount googleSignInAccount) {
        Games.getGamesClient((Activity) RunnerActivity.CurrentActivity, googleSignInAccount).setViewForPopups(RunnerActivity.CurrentActivity.getWindow().getDecorView());
        String displayName = googleSignInAccount.getDisplayName();
        String id = googleSignInAccount.getId();
        Log.i("yoyo", "User " + displayName + " (ID: " + id + ") logged in successfully.");
        RunnerJNILib.OnLoginSuccess(displayName, id, "", "", "", "", "");
    }

    private void onLoginFailed() {
        RunnerJNILib.OnLoginSuccess("Not logged in", "-1", "", "", "", "", "");
    }

    public void Logout() {
        GoogleSignInClient googleSignInClient;
        if (!this.mSignOutClicked && (googleSignInClient = this.mGoogleSigninClient) != null) {
            this.mSignOutClicked = true;
            Task<Void> signOut = googleSignInClient.signOut();
            if (signOut.isComplete()) {
                onLogoutTaskComplete(signOut);
            } else {
                signOut.addOnCompleteListener(new OnCompleteListener() {
                    public void onComplete(Task task) {
                        boolean unused = GooglePlayServicesExtension.this.mSignOutClicked = false;
                        GooglePlayServicesExtension.this.onLogoutTaskComplete(task);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public void onLogoutTaskComplete(Task task) {
        this.mSignOutClicked = false;
        if (task.isSuccessful()) {
            Log.i("yoyo", "Signed out successfully.");
            RunnerJNILib.OnLoginSuccess("Not logged in", "-1", "", "", "", "", "");
            this.mGoogleSignInAccount = null;
            return;
        }
        Log.i("yoyo", "Signing out failed.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.SnapshotsClient.open(java.lang.String, boolean):com.google.android.gms.tasks.Task<com.google.android.gms.games.SnapshotsClient$DataOrConflict<com.google.android.gms.games.snapshot.Snapshot>>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.games.SnapshotsClient.open(com.google.android.gms.games.snapshot.SnapshotMetadata, int):com.google.android.gms.tasks.Task<com.google.android.gms.games.SnapshotsClient$DataOrConflict<com.google.android.gms.games.snapshot.Snapshot>>
      com.google.android.gms.games.SnapshotsClient.open(java.lang.String, boolean):com.google.android.gms.tasks.Task<com.google.android.gms.games.SnapshotsClient$DataOrConflict<com.google.android.gms.games.snapshot.Snapshot>> */
    public void onGSCloudSync(final Integer num) {
        if (!this.mCloudServicesEnabled) {
            Log.i("yoyo", "Could not perform cloud sync - please tick the 'Enable Google Cloud Saving' option in the Android options of your project.");
        } else if (this.mCloudSyncInProgress) {
            Log.i("yoyo", "Could not perform cloud sync - another sync is already in progress.");
        } else {
            GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
            if (currentAccountSignedIn == null) {
                Log.i("yoyo", "Could not perform cloud sync - not signed in.");
                return;
            }
            this.mCloudSyncInProgress = true;
            this.mCloudSyncConflictRetries = 0;
            Log.i("yoyo", "Google Play Cloud Sync called with ID: " + num);
            try {
                final SnapshotsClient snapshotsClient = Games.getSnapshotsClient(RunnerJNILib.ms_context, currentAccountSignedIn);
                snapshotsClient.open(this.mCurrentSaveName, true).addOnCompleteListener(new OnCompleteListener<SnapshotsClient.DataOrConflict<Snapshot>>() {
                    public void onComplete(Task<SnapshotsClient.DataOrConflict<Snapshot>> task) {
                        GooglePlayServicesExtension.this.onCloudSyncTaskComplete(task, num.intValue(), snapshotsClient, GooglePlayServicesExtension.this.mOnCloudSyncListener);
                    }
                });
            } catch (Exception e) {
                Log.i("yoyo", "Error starting Google Play sync: " + e);
                this.mCloudSyncInProgress = false;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.SnapshotsClient.open(java.lang.String, boolean):com.google.android.gms.tasks.Task<com.google.android.gms.games.SnapshotsClient$DataOrConflict<com.google.android.gms.games.snapshot.Snapshot>>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.games.SnapshotsClient.open(com.google.android.gms.games.snapshot.SnapshotMetadata, int):com.google.android.gms.tasks.Task<com.google.android.gms.games.SnapshotsClient$DataOrConflict<com.google.android.gms.games.snapshot.Snapshot>>
      com.google.android.gms.games.SnapshotsClient.open(java.lang.String, boolean):com.google.android.gms.tasks.Task<com.google.android.gms.games.SnapshotsClient$DataOrConflict<com.google.android.gms.games.snapshot.Snapshot>> */
    public void onGSStringSave(final String str, final String str2, final Integer num) {
        if (!this.mCloudServicesEnabled) {
            Log.i("yoyo", "Could not perform cloud save - please tick the 'Enable Google Cloud Saving' option in the Android options of your project.");
        } else if (this.mCloudSyncInProgress) {
            Log.i("yoyo", "Could not perform cloud save - another sync is already in progress.");
        } else {
            GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
            if (currentAccountSignedIn == null) {
                Log.i("yoyo", "Could not perform cloud save - not signed in.");
                return;
            }
            this.mCloudSyncInProgress = true;
            this.mCloudSyncConflictRetries = 0;
            Log.i("yoyo", "Performing cloud save. ID: " + num + " .Data: " + str);
            try {
                final SnapshotsClient snapshotsClient = Games.getSnapshotsClient(RunnerJNILib.ms_context, currentAccountSignedIn);
                Task<SnapshotsClient.DataOrConflict<Snapshot>> open = snapshotsClient.open(this.mCurrentSaveName, true);
                final AnonymousClass5 r4 = new OnSnapshotResolvedListener() {
                    public void onSuccess(Snapshot snapshot, final int i) {
                        try {
                            snapshot.getSnapshotContents().writeBytes(str.getBytes());
                            final String str = str2;
                            final String str2 = new String(snapshot.getSnapshotContents().readFully());
                            Log.i("yoyo", "Saving snapshot:" + str2);
                            snapshotsClient.commitAndClose(snapshot, new SnapshotMetadataChange.Builder().fromMetadata(snapshot.getMetadata()).setDescription(str2).build()).addOnCompleteListener(new OnCompleteListener<SnapshotMetadata>() {
                                public void onComplete(Task<SnapshotMetadata> task) {
                                    if (task.isSuccessful()) {
                                        Log.i("yoyo", "Cloud save successful!");
                                        RunnerJNILib.CloudResultData(str2.getBytes(), str.getBytes(), 0, i);
                                        return;
                                    }
                                    AnonymousClass5 r0 = AnonymousClass5.this;
                                    r0.onFailure("Error while saving snapshot." + task.getException(), i);
                                }
                            });
                        } catch (Exception e) {
                            onFailure("Error while saving snapshot." + e, i);
                        }
                    }

                    public void onFailure(String str, int i) {
                        Log.i("yoyo", "Cloud save failed: " + str);
                        RunnerJNILib.CloudResultData(null, null, 0, i);
                    }
                };
                open.addOnCompleteListener(new OnCompleteListener<SnapshotsClient.DataOrConflict<Snapshot>>() {
                    public void onComplete(Task<SnapshotsClient.DataOrConflict<Snapshot>> task) {
                        GooglePlayServicesExtension.this.onCloudSyncTaskComplete(task, num.intValue(), snapshotsClient, r4);
                    }
                });
            } catch (Exception e) {
                Log.i("yoyo", "Error starting Google Play sync: " + e);
                this.mCloudSyncInProgress = false;
            }
        }
    }

    public void onCloudSyncTaskComplete(Task<SnapshotsClient.DataOrConflict<Snapshot>> task, final int i, final SnapshotsClient snapshotsClient, final OnSnapshotResolvedListener onSnapshotResolvedListener) {
        if (!task.isSuccessful()) {
            Log.i("yoyo", "Cloud sync failed. Error: " + task.getException());
            this.mCloudSyncInProgress = false;
            return;
        }
        Log.i("yoyo", "Cloud sync successful! Retrieving data..");
        SnapshotsClient.DataOrConflict result = task.getResult();
        if (result.isConflict()) {
            int i2 = this.mCloudSyncConflictRetries;
            if (i2 < 5) {
                this.mCloudSyncConflictRetries = i2 + 1;
                Log.i("yoyo", "Detected conflict in cloud save. Attempting to resolve: " + this.mCloudSyncConflictRetries);
                try {
                    SnapshotsClient.SnapshotConflict conflict = result.getConflict();
                    snapshotsClient.resolveConflict(conflict.getConflictId(), conflict.getConflictingSnapshot()).addOnCompleteListener(new OnCompleteListener<SnapshotsClient.DataOrConflict<Snapshot>>() {
                        public void onComplete(Task<SnapshotsClient.DataOrConflict<Snapshot>> task) {
                            GooglePlayServicesExtension.this.onCloudSyncTaskComplete(task, i, snapshotsClient, onSnapshotResolvedListener);
                        }
                    });
                } catch (Exception e) {
                    this.mCloudSyncInProgress = false;
                    onSnapshotResolvedListener.onFailure("Error while resolving cloud save conflict. " + e, i);
                }
            } else {
                this.mCloudSyncInProgress = false;
                onSnapshotResolvedListener.onFailure("Failed to resolve cloud save conflict.", i);
            }
        } else {
            this.mCloudSyncInProgress = false;
            onSnapshotResolvedListener.onSuccess((Snapshot) result.getData(), i);
        }
    }

    public void PostScore(final String str, int i) {
        GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
        if (currentAccountSignedIn == null) {
            Log.i("yoyo", "Could not post score - not signed in.");
            return;
        }
        Log.i("yoyo", "Posting score to leaderboard " + str + ": " + i);
        Games.getLeaderboardsClient(RunnerJNILib.ms_context, currentAccountSignedIn).submitScoreImmediate(str, (long) i).addOnCompleteListener(new OnCompleteListener<ScoreSubmissionData>() {
            public void onComplete(Task<ScoreSubmissionData> task) {
                GooglePlayServicesExtension.this.onLeaderboardPostTaskComplete(task, str);
            }
        });
    }

    public void onLeaderboardPostTaskComplete(Task<ScoreSubmissionData> task, String str) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "achievement_post_score_result");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9818.0d);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "leaderboard_id", str);
        double d = 0.0d;
        if (task.isSuccessful()) {
            ScoreSubmissionData result = task.getResult();
            Log.i("yoyo", "Score submit request processed. Result: " + result);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 1.0d);
            ScoreSubmissionData.Result scoreResult = result.getScoreResult(0);
            if (scoreResult != null) {
                RunnerJNILib.DsMapAddDouble(jCreateDsMap, "daily_new_best", scoreResult.newBest ? 1.0d : 0.0d);
                RunnerJNILib.DsMapAddDouble(jCreateDsMap, "daily_best", (double) scoreResult.rawScore);
            }
            ScoreSubmissionData.Result scoreResult2 = result.getScoreResult(1);
            if (scoreResult2 != null) {
                RunnerJNILib.DsMapAddDouble(jCreateDsMap, "weekly_new_best", scoreResult2.newBest ? 1.0d : 0.0d);
                RunnerJNILib.DsMapAddDouble(jCreateDsMap, "weekly_best", (double) scoreResult2.rawScore);
            }
            ScoreSubmissionData.Result scoreResult3 = result.getScoreResult(2);
            if (scoreResult3 != null) {
                if (scoreResult3.newBest) {
                    d = 1.0d;
                }
                RunnerJNILib.DsMapAddDouble(jCreateDsMap, "all_time_new_best", d);
                RunnerJNILib.DsMapAddDouble(jCreateDsMap, "all_time_best", (double) scoreResult3.rawScore);
            }
        } else {
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 0.0d);
        }
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onShowGSLeaderboards() {
        GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
        if (currentAccountSignedIn == null) {
            Log.i("yoyo", "Could not show leaderboards - user not signed in.");
            return;
        }
        Log.i("yoyo", "Attempting to show leaderboards.");
        Task<Intent> allLeaderboardsIntent = Games.getLeaderboardsClient(RunnerJNILib.ms_context, currentAccountSignedIn).getAllLeaderboardsIntent();
        allLeaderboardsIntent.addOnSuccessListener(new OnSuccessListener<Intent>() {
            public void onSuccess(Intent intent) {
                Log.i("yoyo", "Showing leaderboards.");
                RunnerActivity.CurrentActivity.startActivityForResult(intent, 5011);
            }
        });
        allLeaderboardsIntent.addOnFailureListener(new OnFailureListener() {
            public void onFailure(Exception exc) {
                Log.i("yoyo", "Failed to show leaderboards: " + exc);
            }
        });
    }

    public void onIncrementAchievement(final String str, Float f) {
        float floatValue = f.floatValue();
        if (floatValue < 1.0f) {
            Log.i("yoyo", "achievement_increment must be called with a positive value above 1");
            return;
        }
        GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
        if (currentAccountSignedIn == null) {
            Log.i("yoyo", "Could not increment achievement - not signed in.");
            return;
        }
        Log.i("yoyo", "Incrementing achievement " + str + " by " + floatValue);
        Games.getAchievementsClient(RunnerJNILib.ms_context, currentAccountSignedIn).incrementImmediate(str, (int) floatValue).addOnCompleteListener(new OnCompleteListener<Boolean>() {
            public void onComplete(Task<Boolean> task) {
                GooglePlayServicesExtension.this.onAchievementIncrementTaskComplete(task, str);
            }
        });
    }

    /* access modifiers changed from: private */
    public void onAchievementIncrementTaskComplete(Task<Boolean> task, String str) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "achievement_increment_result");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9820.0d);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "achievement_id", str);
        double d = 0.0d;
        if (task.isSuccessful()) {
            Log.i("yoyo", "Successfully incremented achievement: " + str);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 1.0d);
            if (task.getResult().booleanValue()) {
                d = 1.0d;
            }
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "complete", d);
        } else {
            Log.i("yoyo", "Failed to increment achievement: " + str);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 0.0d);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "complete", 0.0d);
        }
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onPostAchievement(final String str, Float f) {
        if (f.floatValue() <= 0.0f) {
            Log.i("yoyo", "achievement_post must be called with a positive value");
        } else if (((double) f.floatValue()) >= 100.0d) {
            GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
            if (currentAccountSignedIn == null) {
                Log.i("yoyo", "Could not post achievement - not signed in.");
                return;
            }
            Log.i("yoyo", "Posting achievement: " + str);
            Games.getAchievementsClient(RunnerJNILib.ms_context, currentAccountSignedIn).unlockImmediate(str).addOnCompleteListener(new OnCompleteListener() {
                public void onComplete(Task task) {
                    GooglePlayServicesExtension.this.onAchievementPostTaskComplete(task, str);
                }
            });
        } else {
            Log.i("yoyo", "Google Play Services does not currently support posting partially complete achievements. Either call achievement_post() when the achievement is complete or use achievement_increment()");
        }
    }

    public void onAchievementPostTaskComplete(Task task, String str) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "achievement_post_result");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9819.0d);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "achievement_id", str);
        if (task.isSuccessful()) {
            Log.i("yoyo", "Successfully posted achievement progress for: " + str);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 1.0d);
        } else {
            Log.i("yoyo", "Failed to post achievement progress for: " + str);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 0.0d);
        }
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onRevealAchievement(final String str) {
        GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
        if (currentAccountSignedIn == null) {
            Log.i("yoyo", "Could not reveal achievement - not signed in.");
            return;
        }
        Log.i("yoyo", "Revealing achievement: " + str);
        Games.getAchievementsClient(RunnerJNILib.ms_context, currentAccountSignedIn).revealImmediate(str).addOnCompleteListener(new OnCompleteListener() {
            public void onComplete(Task task) {
                GooglePlayServicesExtension.this.onAchievementRevealTaskComplete(task, str);
            }
        });
    }

    public void onAchievementRevealTaskComplete(Task task, String str) {
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "achievement_reveal_result");
        RunnerJNILib.DsMapAddDouble(jCreateDsMap, "id", 9821.0d);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "achievement_id", str);
        if (task.isSuccessful()) {
            Log.i("yoyo", "Successfully revealed achievement: " + str);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 1.0d);
        } else {
            Log.i("yoyo", "Failed to reveal achievement: " + str);
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, "status", 0.0d);
        }
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 70);
    }

    public void onShowGSAchievements() {
        GoogleSignInAccount currentAccountSignedIn = getCurrentAccountSignedIn();
        if (currentAccountSignedIn == null) {
            Log.i("yoyo", "Could not show achievements - user not signed in.");
            return;
        }
        Log.i("yoyo", "Attempting to show achievements.");
        Task<Intent> achievementsIntent = Games.getAchievementsClient(RunnerJNILib.ms_context, currentAccountSignedIn).getAchievementsIntent();
        achievementsIntent.addOnSuccessListener(new OnSuccessListener<Intent>() {
            public void onSuccess(Intent intent) {
                Log.i("yoyo", "Showing achievements!");
                RunnerActivity.CurrentActivity.startActivityForResult(intent, 5011);
            }
        });
        achievementsIntent.addOnFailureListener(new OnFailureListener() {
            public void onFailure(Exception exc) {
                Log.i("yoyo", "Failed to show achievements: " + exc);
            }
        });
    }
}
