package com.butakov.psebay;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.yoyogames.runner.RunnerJNILib;

public class GooglePlayLicensingAsExt extends RunnerSocial {
    /* access modifiers changed from: private */
    public LicenseChecker mChecker;
    /* access modifiers changed from: private */
    public LicenseCheckerCallback mLicenseCheckerCallback;
    private Handler mLicenseHandler = new Handler();

    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {
        private MyLicenseCheckerCallback() {
        }

        public void allow(int i) {
            if (!RunnerActivity.CurrentActivity.isFinishing()) {
                Log.i("yoyo", "!!!!##### Successful license check #####!!!!!! ");
            }
        }

        public void dontAllow(int i) {
            if (!RunnerActivity.CurrentActivity.isFinishing()) {
                Log.i("yoyo", "!!!!##### failed license check reason=" + i + " #####!!!!!! ");
                if (i == 291) {
                    GooglePlayLicensingAsExt.this.displayResult(RunnerActivity.CurrentActivity.getString(R.string.yyg_gpl_ext_retry_needed));
                } else {
                    GooglePlayLicensingAsExt.this.displayResult(RunnerActivity.CurrentActivity.getString(R.string.yyg_gpl_ext_license_fail));
                }
            }
        }

        public void applicationError(int i) {
            Log.i("yoyo", "License Error - " + i);
            dontAllow(0);
        }
    }

    public void displayResult(final String str) {
        Log.i("yoyo", "Attempting to display result:" + str);
        this.mLicenseHandler.post(new Runnable() {
            public void run() {
                if (str == null) {
                    GooglePlayLicensingAsExt.this.mChecker.checkAccess(GooglePlayLicensingAsExt.this.mLicenseCheckerCallback);
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(RunnerActivity.CurrentActivity);
                if (!RunnerJNILib.isNetworkConnected()) {
                    builder.setMessage(RunnerActivity.CurrentActivity.getString(R.string.yyg_gpl_ext_check_network)).setCancelable(false).setNegativeButton(RunnerActivity.CurrentActivity.getString(R.string.yyg_gpl_ext_retry), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            GooglePlayLicensingAsExt.this.mChecker.checkAccess(GooglePlayLicensingAsExt.this.mLicenseCheckerCallback);
                            dialogInterface.dismiss();
                        }
                    });
                } else {
                    builder.setMessage(str).setCancelable(false).setNegativeButton(RunnerActivity.CurrentActivity.getString(R.string.yyg_gpl_ext_retry), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            GooglePlayLicensingAsExt.this.mChecker.checkAccess(GooglePlayLicensingAsExt.this.mLicenseCheckerCallback);
                            dialogInterface.dismiss();
                        }
                    }).setPositiveButton(RunnerActivity.CurrentActivity.getString(R.string.yyg_gpl_ext_buy), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            RunnerActivity.CurrentActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + RunnerActivity.CurrentActivity.getPackageName())));
                            RunnerJNILib.ExitApplication();
                        }
                    });
                }
                AlertDialog create = builder.create();
                Log.i("yoyo", "License display - " + str);
                create.show();
            }
        });
    }

    public void Init() {
        Log.i("yoyo", "Google Play Licensing extension initialising");
    }

    public void checkLicensing() {
        if (RunnerActivity.CurrentActivity.checkCallingOrSelfPermission("com.android.vending.CHECK_LICENSE") == 0) {
            this.mLicenseCheckerCallback = new MyLicenseCheckerCallback();
            String string = Settings.Secure.getString(RunnerActivity.CurrentActivity.getContentResolver(), "android_id");
            Log.i("yoyo", "deviceId=" + string);
            RunnerActivity runnerActivity = RunnerActivity.CurrentActivity;
            RunnerActivity runnerActivity2 = RunnerActivity.CurrentActivity;
            try {
                this.mChecker = new LicenseChecker(RunnerActivity.CurrentActivity, new ServerManagedPolicy(runnerActivity, new AESObfuscator(RunnerActivity.SALT, RunnerActivity.CurrentActivity.getPackageName(), string)), RunnerActivity.BASE64_PUBLIC_KEY);
                this.mChecker.checkAccess(this.mLicenseCheckerCallback);
            } catch (IllegalArgumentException e) {
                Log.i("yoyo", "exception while doing license check! invalid license key????" + e);
            }
        } else {
            Log.i("yoyo", "@@@@@@ Google Licensing permission not set");
        }
    }
}
