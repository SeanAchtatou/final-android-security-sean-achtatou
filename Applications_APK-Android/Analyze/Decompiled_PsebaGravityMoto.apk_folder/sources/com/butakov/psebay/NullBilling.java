package com.butakov.psebay;

import android.content.Context;
import android.util.Log;

public class NullBilling extends IRunnerBilling {
    public static final String EmptyString = "";

    public void Destroy() {
    }

    /* access modifiers changed from: protected */
    public String getContentPurchasedKey(String str) {
        return "";
    }

    public NullBilling(Context context) {
    }

    public void loadStore(String[] strArr) {
        Log.i("yoyo", "NULL-BILLING: Store is not available");
    }

    public void restorePurchasedItems() {
        Log.i("yoyo", "NULL-BILLING: Store is not available");
    }

    public void purchaseCatalogItem(String str, String str2, int i) {
        Log.i("yoyo", "NULL-BILLING: Store is not available");
    }

    public void consumeCatalogItem(String str, String str2) {
        Log.i("yoyo", "NULL-BILLING: Store is not available");
    }

    public void getCatalogItemDetails(String str) {
        Log.i("yoyo", "NULL-BILLING: Store is not available");
    }

    public void revokeCatalogItem(String str) {
        Log.i("yoyo", "NULL-BILLING: Store is not available");
    }
}
