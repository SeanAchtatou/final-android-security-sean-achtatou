package com.butakov.psebay;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.core.view.InputDeviceCompat;
import androidx.fragment.app.FragmentActivity;
import com.butakov.psebay.DemoRenderer;
import com.butakov.psebay.RunnerVsyncHandler;
import com.yoyogames.runner.RunnerJNILib;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.ini4j.Ini;
import org.ini4j.Profile;

public class RunnerActivity extends FragmentActivity implements SensorEventListener, SurfaceHolder.Callback {
    public static boolean APKExpansionFileReady = false;
    public static float AccelX = 0.0f;
    public static float AccelY = 0.0f;
    public static float AccelZ = 0.0f;
    public static int AllowedOrientationMask = 15;
    public static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr8bXLMbyycJqBJSPT9AqTUaozD46kHFXDOiQO9yRIUSXaHzpGWAvOFl8xZeGjjcXt8DJLBUme/uq6uo2gmm91HX2kAti0Y4znfLVPSPNnSrIr8fZ3mornQbxM+q0lL2p9CpmQ/y2yNWDovu/YNTq5aFvG1gmtsz7LSRD9XpOfGhH+e5cbdxXzLjmu7ppji0YGYmxTiz0bLCVNJKJUMQieD8PdtVNWFu91bmdQr3btBAJUdE9SYnpAzCkSpIwsiEIukM3sGibzQwV0HThaG0AQfJEeRKV0XvU7qTufS/q5ZGVn1N3DnYQR46Ih0IQ+y6iLZlFjm0VlrQakVcwQRypNQIDAQAB";
    public static int ConfigOrientation;
    public static RunnerActivity CurrentActivity;
    public static int DefaultOrientation;
    public static int DisplayHeight;
    public static int DisplayWidth;
    public static boolean FocusOverride = false;
    public static boolean HasFocus = false;
    public static boolean HasRestarted = false;
    public static String InputStringResult;
    public static boolean LaunchedFromPlayer = false;
    public static String LaunchedFromPlayerGameFile = "";
    public static int Orientation;
    public static final byte[] SALT = {-5, 12, -68, 7, -12, 67, 3, 4, 4, 19, 6, 7, 16, 11, 9, 51, 71, 34, 19, 16};
    public static String SaveFilesDir = null;
    public static int ShowQuestionYesNo;
    public static int UIVisibilityFlags = 5894;
    public static boolean UseAPKExpansionFile = false;
    public static Handler ViewHandler;
    public static boolean XPeriaPlay = false;
    public static boolean YoYoRunner = false;
    public static Object[] mExtension;
    private static Method mSetSystemUiVisibility = null;
    public static IniBundle mYYPrefs;
    public static String m_versionName;
    int EVENT_OTHER_SYSTEM_EVENT = 75;
    private Sensor mAccelerometer;
    private DemoGLSurfaceView mGLView;
    private Handler mHandler = new Handler();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if ("android.bluetooth.device.action.ACL_CONNECTED".equals(action)) {
                RunnerJNILib.onGamepadChange();
            } else if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {
                RunnerJNILib.onGamepadChange();
            }
        }
    };
    private Handler mRestoreImmersiveModeHandler = new Handler();
    private SensorManager mSensorManager;
    private Runnable mUpdateTimerTask = new Runnable() {
        public void run() {
        }
    };
    private RunnerKeyboardController m_keyboardController = null;
    public boolean mbAppSuspended = false;
    private Runnable restoreImmersiveModeRunnable = new Runnable() {
        public void run() {
            RunnerActivity.this.setupUiVisibility();
        }
    };
    public Object vsyncHandler = null;

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public DemoGLSurfaceView GetGLView(RunnerVsyncHandler.Accessor accessor) {
        accessor.hashCode();
        return this.mGLView;
    }

    public boolean onKeyLongPress(int i, KeyEvent keyEvent) {
        boolean onKeyLongPress;
        if (mExtension != null) {
            int i2 = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i2 >= objArr.length) {
                    break;
                } else if ((objArr[i2] instanceof IExtensionBase) && (onKeyLongPress = ((IExtensionBase) objArr[i2]).onKeyLongPress(i, keyEvent))) {
                    return onKeyLongPress;
                } else {
                    i2++;
                }
            }
        }
        if (i != 4) {
            return super.onKeyLongPress(i, keyEvent);
        }
        RunnerJNILib.BackKeyLongPressEvent();
        return true;
    }

    public static boolean isLoggedInGooglePlay() {
        Object CallExtensionFunction = RunnerJNILib.CallExtensionFunction("GooglePlayServicesExtension", "isSignedIn", 0, null);
        if (CallExtensionFunction != null) {
            return ((Boolean) CallExtensionFunction).booleanValue();
        }
        return false;
    }

    public static void ach_login() {
        if (mYYPrefs == null) {
            Log.i("yoyo", "login called when prefs null");
        } else if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i < objArr.length) {
                    if (objArr[i] instanceof ISocial) {
                        ((ISocial) objArr[i]).Login();
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean isTablet() {
        try {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            float f = ((float) displayMetrics.widthPixels) / displayMetrics.xdpi;
            float f2 = ((float) displayMetrics.heightPixels) / displayMetrics.ydpi;
            if (Math.sqrt((double) ((f * f) + (f2 * f2))) >= 6.0d) {
                return true;
            }
            return false;
        } catch (Throwable unused) {
            Log.i("yoyo", "Failed to compute screen size");
            return false;
        }
    }

    @TargetApi(11)
    public void onCreate(Bundle bundle) {
        Object CallExtensionFunction;
        requestWindowFeature(1);
        ViewHandler = new Handler();
        Log.i("yoyo", "onCreate");
        super.onCreate(bundle);
        RunnerJNILib.Init(this);
        YoYoRunner = checkIsYoYoRunner();
        boolean z = false;
        try {
            m_versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            m_versionName = "1.0.xxx";
            Log.v("yoyo", e.getMessage());
        }
        RunnerJNILib.ms_versionName = m_versionName;
        Log.i("yoyo", "###@@@@!!!~~~~###### versionName - " + m_versionName);
        if (getResources().getConfiguration().orientation != 1) {
            Orientation = 0;
        } else {
            Orientation = 1;
        }
        CurrentActivity = this;
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        DisplayWidth = defaultDisplay.getWidth();
        DisplayHeight = defaultDisplay.getHeight();
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mAccelerometer = this.mSensorManager.getDefaultSensor(1);
        DefaultOrientation = getDeviceDefaultOrientation();
        Log.i("yoyo", "###@@@@!!!~~~~###### default orientation - " + DefaultOrientation);
        setupIniFile();
        setupExtensions();
        checkXPeriaPlay();
        RunnerJNILib.CallExtensionFunction("GooglePlayLicensingAsExt", "checkLicensing", 0, null);
        Log.i("yoyo", "!!!!!!! Checking if APK Expansion file required...");
        if (!((!UseAPKExpansionFile || (CallExtensionFunction = RunnerJNILib.CallExtensionFunction("PlayAPKExpansionExtension", "StartAPKExpansionDownload", 0, null)) == null) ? false : ((Boolean) CallExtensionFunction).booleanValue()) || HasRestarted) {
            setupView();
        } else {
            RunnerJNILib.CallExtensionFunction("PlayAPKExpansionExtension", "setupDownloadView", 0, null);
        }
        IntentFilter intentFilter = new IntentFilter("android.bluetooth.device.action.ACL_CONNECTED");
        IntentFilter intentFilter2 = new IntentFilter("android.bluetooth.device.action.ACL_DISCONNECTED");
        registerReceiver(this.mReceiver, intentFilter);
        registerReceiver(this.mReceiver, intentFilter2);
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                public void onSystemUiVisibilityChange(int i) {
                    RunnerActivity.this.setupUiVisibility();
                    RunnerActivity.this.setupUiVisibilityDelayed();
                }
            });
        }
        if (Build.VERSION.SDK_INT >= 16) {
            Log.i("yoyo", "!!!!!!! Using frame count timing if possible...");
            this.vsyncHandler = new RunnerVsyncHandler();
        }
        RunnerKeyboardController GetKeyboardController = GetKeyboardController();
        if (GetKeyboardController != null) {
            if (getResources().getConfiguration().hardKeyboardHidden == 1) {
                z = true;
            }
            GetKeyboardController.SetPhysicalKeyboardConnected(z);
        }
    }

    public IniBundle DoSetupIniFile(String str) {
        Bundle bundle;
        Log.i("yoyo", "File Path for INI:: " + str);
        InputStream inputStream = null;
        try {
            bundle = getPackageManager().getApplicationInfo(getComponentName().getPackageName(), 128).metaData;
            try {
                ZipFile zipFile = new ZipFile(str);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (true) {
                    if (!entries.hasMoreElements()) {
                        break;
                    }
                    ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                    if (zipEntry.getName().indexOf(".ini") > 0) {
                        Log.d("yoyo", "found INI file - " + zipEntry.getName());
                        inputStream = zipFile.getInputStream(zipEntry);
                        break;
                    }
                }
            } catch (Exception e) {
                e = e;
                Log.d("yoyo", "Exception while setting up Ini" + e.toString());
                return new IniBundle(bundle, inputStream);
            }
        } catch (Exception e2) {
            e = e2;
            bundle = null;
            Log.d("yoyo", "Exception while setting up Ini" + e.toString());
            return new IniBundle(bundle, inputStream);
        }
        return new IniBundle(bundle, inputStream);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicProfile.put(java.lang.String, java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object, int):V
      org.ini4j.MultiMap.put(java.lang.Object, java.lang.Object, int):V
      org.ini4j.MultiMap.put(java.lang.Object, java.lang.Object, int):V
      org.ini4j.BasicProfile.put(java.lang.String, java.lang.String, java.lang.Object):java.lang.String */
    public void setupIniFile() {
        Bundle bundle;
        if (LaunchedFromPlayer && mYYPrefs != null) {
            String str = LaunchedFromPlayerGameFile;
            String str2 = str.substring(0, Math.max(str.lastIndexOf("/"), LaunchedFromPlayerGameFile.lastIndexOf("\\"))) + "/options.ini";
            File file = new File(str2);
            if (file.exists()) {
                Log.i("yoyo", "Loading game INI file from yyg_player_run directory: " + str2);
                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    Profile.Section section = (Profile.Section) new Ini(fileInputStream).get("Android");
                    if (section != null) {
                        if (!mYYPrefs.hasAndroidIni()) {
                            Log.i("yoyo", "Setting up new android INI section from game INI.");
                            Ini ini = new Ini();
                            ini.put("Android", "OrientLandscape", (Object) -1);
                            ini.put("Android", "OrientPortrait", (Object) -1);
                            ini.put("Android", "OrientLandscapeFlipped", (Object) -1);
                            ini.put("Android", "OrientPortraitFlipped", (Object) -1);
                            mYYPrefs.setAndroidIni((Profile.Section) ini.get("Android"));
                        }
                        mYYPrefs.setAndroidString("OrientLandscape", (String) section.get("OrientLandscape"));
                        mYYPrefs.setAndroidString("OrientPortrait", (String) section.get("OrientPortrait"));
                        mYYPrefs.setAndroidString("OrientLandscapeFlipped", (String) section.get("OrientLandscapeFlipped"));
                        mYYPrefs.setAndroidString("OrientPortraitFlipped", (String) section.get("OrientPortraitFlipped"));
                    }
                    fileInputStream.close();
                } catch (Exception e) {
                    Log.i("yoyo", "Exception while trying to load game INI file in yyg_player_run: " + e.toString());
                }
            } else {
                Log.i("yoyo", "Could not locate game INI file in yyg_player_run directory: " + str2);
            }
        } else if (YoYoRunner) {
            String str3 = (Environment.getExternalStorageDirectory() + "/GMstudio") + '/';
            String str4 = str3 + DemoRenderer.kGameAssetsDROID;
            File file2 = new File(str4);
            File file3 = new File(str3 + "GameDownload.lock");
            if (!file3.exists() || (file3.exists() && file3.lastModified() < file2.lastModified())) {
                Log.i("yoyo", "Don't have up-to-date INI file at this point");
                return;
            }
            mYYPrefs = DoSetupIniFile(str4);
            Log.i("yoyo", "INI loaded");
        } else {
            String packageName = getComponentName().getPackageName();
            Log.i("yoyo", "#####!!!! package name is " + packageName);
            if (packageName.equals("com.yoyogames.runner")) {
                try {
                    mYYPrefs = DoSetupIniFile(getPackageManager().getApplicationInfo(getComponentName().getPackageName(), 128).sourceDir);
                    Log.i("yoyo", "INI loaded from assets/Options.INI");
                } catch (Exception e2) {
                    Log.d("yoyo", "Exception while setting up Ini" + e2.toString());
                }
            } else {
                Log.i("yoyo", "Loading INI from manifest file");
                try {
                    bundle = getPackageManager().getApplicationInfo(getComponentName().getPackageName(), 128).metaData;
                } catch (Exception e3) {
                    Log.d("yoyo", "Exception while setting up Ini" + e3.toString());
                    bundle = null;
                }
                mYYPrefs = new IniBundle(bundle, null);
                Log.i("yoyo", "INI loaded from AndroidManifest.xml");
            }
        }
    }

    public void doSetup(String str) {
        Bundle bundle;
        Log.d("yoyo", "doSetup called - " + str);
        if (mYYPrefs == null) {
            InputStream inputStream = null;
            try {
                bundle = getPackageManager().getApplicationInfo(getComponentName().getPackageName(), 128).metaData;
                try {
                    ZipFile zipFile = new ZipFile(str);
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (true) {
                        if (!entries.hasMoreElements()) {
                            break;
                        }
                        ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                        if (zipEntry.getName().indexOf(".ini") > 0) {
                            Log.d("yoyo", "found INI file - " + zipEntry.getName());
                            inputStream = zipFile.getInputStream(zipEntry);
                            break;
                        }
                    }
                } catch (Exception e) {
                    e = e;
                    Log.d("yoyo", "Exception while setting up Ini" + e.toString());
                    mYYPrefs = new IniBundle(bundle, inputStream);
                    setupExtensions();
                    setupAdvertising();
                    Gamepad.Initialise();
                    RestrictOrientation(false, false, false, false, true);
                    DemoRenderer.m_state = DemoRenderer.eState.WaitOnTimer;
                }
            } catch (Exception e2) {
                e = e2;
                bundle = null;
                Log.d("yoyo", "Exception while setting up Ini" + e.toString());
                mYYPrefs = new IniBundle(bundle, inputStream);
                setupExtensions();
                setupAdvertising();
                Gamepad.Initialise();
                RestrictOrientation(false, false, false, false, true);
                DemoRenderer.m_state = DemoRenderer.eState.WaitOnTimer;
            }
            mYYPrefs = new IniBundle(bundle, inputStream);
        }
        setupExtensions();
        setupAdvertising();
        Gamepad.Initialise();
        RestrictOrientation(false, false, false, false, true);
        DemoRenderer.m_state = DemoRenderer.eState.WaitOnTimer;
    }

    public void RestrictOrientation(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        if (z5) {
            if (mYYPrefs != null) {
                Log.i("yoyo", "RestrictOrientation setting from YYPrefs");
                z = mYYPrefs.getInt("OrientLandscape") != 0;
                z2 = mYYPrefs.getInt("OrientPortrait") != 0;
                z3 = mYYPrefs.getInt("OrientLandscapeFlipped") != 0;
                if (mYYPrefs.getInt("OrientPortraitFlipped") != 0) {
                    z4 = true;
                }
            } else {
                z = false;
                z2 = false;
                z3 = false;
            }
            z4 = false;
        }
        Log.i("yoyo", "RestrictOrientation(\"" + z + "\", \"" + z2 + "\"" + z3 + "\"" + z4 + "\")");
        AllowedOrientationMask = (z ? 1 : 0) | (z2 ? 2 : 0) | (z3 ? 4 : 0) | (z4 ? 8 : 0);
        if (z && !z2 && !z3 && !z4) {
            setRequestedOrientation(0);
        } else if (!z && z2 && !z3 && !z4) {
            setRequestedOrientation(1);
        } else if (!z && !z2 && z3 && !z4) {
            setRequestedOrientation(8);
        } else if (!z && !z2 && !z3 && z4) {
            setRequestedOrientation(9);
        } else if ((z && !z2 && z3 && !z4) || ((z && z2 && z3 && !z4) || (z && !z2 && z3 && z4))) {
            setRequestedOrientation(6);
        } else if ((z || !z2 || z3 || !z4) && ((!z || !z2 || z3 || !z4) && (z || !z2 || !z3 || !z4))) {
            setRequestedOrientation(-1);
        } else {
            setRequestedOrientation(7);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Log.i("yoyo", "onStart");
        super.onStart();
        if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i < objArr.length) {
                    if (objArr[i] instanceof IExtensionBase) {
                        ((IExtensionBase) objArr[i]).onStart();
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        Log.i("yoyo", "onRestart");
        super.onRestart();
        if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i < objArr.length) {
                    if (objArr[i] instanceof IExtensionBase) {
                        ((IExtensionBase) objArr[i]).onRestart();
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.i("yoyo", "onStop");
        DemoGLSurfaceView demoGLSurfaceView = this.mGLView;
        int i = 0;
        if (!(demoGLSurfaceView == null || demoGLSurfaceView.mRenderer == null || this.mGLView.mRenderer.m_pausecountdown <= 0)) {
            this.mGLView.mRenderer.m_pausecountdown = 0;
            this.mGLView.mRenderer.m_pauseRunner = true;
        }
        super.onStop();
        if (mExtension != null) {
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                }
                if (objArr[i] instanceof IExtensionBase) {
                    ((IExtensionBase) objArr[i]).onStop();
                }
                i++;
            }
        }
        RunnerKeyboardController runnerKeyboardController = this.m_keyboardController;
        if (runnerKeyboardController != null && runnerKeyboardController.GetVirtualKeyboardVisible()) {
            this.m_keyboardController.VirtualKeyboardHide();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i("yoyo", "onDestroy");
        super.onDestroy();
        if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                }
                if (objArr[i] instanceof IExtensionBase) {
                    ((IExtensionBase) objArr[i]).onDestroy();
                }
                i++;
            }
        }
        RunnerKeyboardController runnerKeyboardController = this.m_keyboardController;
        if (runnerKeyboardController != null && runnerKeyboardController.GetVirtualKeyboardVisible()) {
            this.m_keyboardController.VirtualKeyboardHide();
        }
        System.exit(0);
    }

    /* access modifiers changed from: protected */
    public void resumeApp() {
        if (this.mbAppSuspended) {
            this.mbAppSuspended = false;
            Log.i("yoyo", "resumeApp");
            DemoGLSurfaceView demoGLSurfaceView = this.mGLView;
            if (demoGLSurfaceView != null) {
                demoGLSurfaceView.onResume();
            }
            if (RunnerJNILib.ms_context != null && !RunnerJNILib.ms_loadLibraryFailed) {
                Log.i("yoyo", "Resuming the C++ Runner/resetting GL state");
                RunnerJNILib.Resume(0);
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        Log.i("yoyo", "onWindowFocusChanged(" + z + "|" + FocusOverride + ")");
        super.onWindowFocusChanged(z);
        if (RunnerJNILib.ms_exitcalled) {
            Log.i("yoyo", "Ignoring focus change as we are exiting");
            return;
        }
        HasFocus = FocusOverride | z;
        int i = 0;
        FocusOverride = false;
        setupUiVisibility();
        setupUiVisibilityDelayed();
        if (mExtension != null) {
            while (true) {
                Object[] objArr = mExtension;
                if (i < objArr.length) {
                    if (objArr[i] instanceof IExtensionBase) {
                        ((IExtensionBase) objArr[i]).onWindowFocusChanged(z);
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.i("yoyo", "onPause");
        super.onPause();
        this.mSensorManager.unregisterListener(this);
        Log.i("yoyo", "Pausing the Runner");
        int i = 0;
        if (!RunnerJNILib.ms_loadLibraryFailed) {
            RunnerJNILib.Pause(0);
        }
        this.mbAppSuspended = true;
        DemoGLSurfaceView demoGLSurfaceView = this.mGLView;
        if (demoGLSurfaceView != null) {
            demoGLSurfaceView.onPause();
        }
        DemoGLSurfaceView demoGLSurfaceView2 = this.mGLView;
        if (demoGLSurfaceView2 != null) {
            demoGLSurfaceView2.mRenderer.m_pauseRunner = true;
        }
        RunnerJNILib.Pause(0);
        RunnerJNILib.StoreMP3State();
        RunnerJNILib.StopMP3();
        if (mExtension != null) {
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                }
                if (objArr[i] instanceof IExtensionBase) {
                    ((IExtensionBase) objArr[i]).onPause();
                }
                i++;
            }
        }
        DemoRenderer.elapsedVsyncs = -1;
        Object obj = this.vsyncHandler;
        if (obj != null) {
            ((RunnerVsyncHandler) obj).RemoveFrameCallback();
        }
        RunnerKeyboardController runnerKeyboardController = this.m_keyboardController;
        if (runnerKeyboardController != null && runnerKeyboardController.GetVirtualKeyboardVisible()) {
            this.m_keyboardController.VirtualKeyboardHide();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.i("yoyo", "onResume");
        super.onResume();
        setupUiVisibility();
        setupUiVisibilityDelayed();
        DemoGLSurfaceView demoGLSurfaceView = this.mGLView;
        int i = 0;
        if (!(demoGLSurfaceView == null || demoGLSurfaceView.mRenderer == null || (!this.mGLView.mRenderer.m_pauseRunner && this.mGLView.mRenderer.m_pausecountdown <= 0))) {
            this.mGLView.mRenderer.m_pausecountdown = 0;
            resumeApp();
            if (RunnerJNILib.ms_context != null) {
                RunnerJNILib.RestoreMP3State();
            }
            this.mGLView.mRenderer.m_pauseRunner = false;
        }
        this.mSensorManager.registerListener(this, this.mAccelerometer, 1);
        if (mExtension != null) {
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                }
                if (objArr[i] instanceof IExtensionBase) {
                    ((IExtensionBase) objArr[i]).onResume();
                }
                i++;
            }
        }
        Object obj = this.vsyncHandler;
        if (obj != null) {
            ((RunnerVsyncHandler) obj).PostFrameCallback();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean onCreateOptionsMenu;
        super.onCreateOptionsMenu(menu);
        if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                } else if ((objArr[i] instanceof IExtensionBase) && (onCreateOptionsMenu = ((IExtensionBase) objArr[i]).onCreateOptionsMenu(menu))) {
                    return onCreateOptionsMenu;
                } else {
                    i++;
                }
            }
        }
        return YoYoRunner;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean onOptionsItemSelected;
        if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                } else if ((objArr[i] instanceof IExtensionBase) && (onOptionsItemSelected = ((IExtensionBase) objArr[i]).onOptionsItemSelected(menuItem))) {
                    return onOptionsItemSelected;
                } else {
                    i++;
                }
            }
        }
        menuItem.getItemId();
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        RunnerKeyboardController GetKeyboardController;
        boolean onKeyDown;
        int source = keyEvent.getSource();
        boolean z = keyEvent.getDeviceId() > 0;
        boolean z2 = (source & 257) == 257;
        boolean z3 = (source & InputDeviceCompat.SOURCE_GAMEPAD) == 1025;
        boolean z4 = (source & InputDeviceCompat.SOURCE_JOYSTICK) == 16777232;
        if (mExtension != null) {
            int i2 = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i2 >= objArr.length) {
                    break;
                } else if ((objArr[i2] instanceof IExtensionBase) && (onKeyDown = ((IExtensionBase) objArr[i2]).onKeyDown(i, keyEvent))) {
                    return onKeyDown;
                } else {
                    i2++;
                }
            }
        }
        if (z && z2 && !z3 && !z4 && (GetKeyboardController = GetKeyboardController()) != null) {
            GetKeyboardController.OnPhysicalKeyboardKeyEvent(i, keyEvent);
        }
        if (i != 0 && !z3 && !z4) {
            RunnerJNILib.KeyEvent(0, i, keyEvent.getUnicodeChar(), source);
        }
        if (i == 4) {
            keyEvent.startTracking();
            return true;
        } else if (i != 24 && i != 25 && i != 3 && i != 82 && i < 79) {
            return true;
        } else {
            setupUiVisibility();
            setupUiVisibilityDelayed();
            return super.onKeyDown(i, keyEvent);
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        boolean onKeyUp;
        int source = keyEvent.getSource();
        int i2 = 0;
        boolean z = (source & InputDeviceCompat.SOURCE_GAMEPAD) == 1025;
        boolean z2 = (source & InputDeviceCompat.SOURCE_JOYSTICK) == 16777232;
        if (mExtension != null) {
            while (true) {
                Object[] objArr = mExtension;
                if (i2 >= objArr.length) {
                    break;
                } else if ((objArr[i2] instanceof IExtensionBase) && (onKeyUp = ((IExtensionBase) objArr[i2]).onKeyUp(i, keyEvent))) {
                    return onKeyUp;
                } else {
                    i2++;
                }
            }
        }
        if (!z && !z2) {
            RunnerJNILib.KeyEvent(1, i, keyEvent.getUnicodeChar(), keyEvent.getSource());
        }
        if (i == 24 || i == 25 || i == 3 || i == 82 || i >= 79) {
            return super.onKeyUp(i, keyEvent);
        }
        return true;
    }

    public int getDeviceDefaultOrientation() {
        try {
            if (Class.forName("android.view.Display").getDeclaredMethod("getRotation", new Class[0]) != null) {
                Configuration configuration = getResources().getConfiguration();
                int rotation = ((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation();
                if (((rotation == 0 || rotation == 2) && configuration.orientation == 2) || ((rotation == 1 || rotation == 3) && configuration.orientation == 1)) {
                    return 2;
                }
            }
        } catch (Exception e) {
            Log.i("yoyo", "ERROR: Enumerating API level " + e.getMessage());
        }
        return 1;
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (DefaultOrientation != 2) {
            AccelX = sensorEvent.values[0] / 9.80665f;
            AccelY = sensorEvent.values[1] / 9.80665f;
            AccelZ = sensorEvent.values[2] / 9.80665f;
            return;
        }
        AccelX = sensorEvent.values[1] / 9.80665f;
        AccelY = (-sensorEvent.values[0]) / 9.80665f;
        AccelZ = sensorEvent.values[2] / 9.80665f;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        Log.i("yoyo", "Got activity result: " + i2);
        super.onActivityResult(i, i2, intent);
        if (mExtension != null) {
            int i3 = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i3 >= objArr.length) {
                    break;
                }
                if (objArr[i3] instanceof IExtensionBase) {
                    ((IExtensionBase) objArr[i3]).onActivityResult(i, i2, intent);
                }
                i3++;
            }
        }
        setupUiVisibility();
        setupUiVisibilityDelayed();
        Log.i("yoyo", "End Got activity result");
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Log.i("yoyo", "onRequestPermissionsResult " + i + " returned with: " + strArr.toString() + " results:" + iArr.toString());
        int i2 = 0;
        if (i == 85) {
            for (int i3 : iArr) {
                if (i3 == -1) {
                    Toast.makeText(CurrentActivity, "Cannot function as a runner without these permissions, use Create Executable for Target", 1).show();
                    return;
                }
            }
            this.mGLView.mRenderer.m_RequestedPermissions = false;
            DemoRenderer demoRenderer = this.mGLView.mRenderer;
            DemoRenderer.m_saveFilesDir = Environment.getExternalStorageDirectory() + "/GMstudio";
            DemoRenderer demoRenderer2 = this.mGLView.mRenderer;
            new File(DemoRenderer.m_saveFilesDir).mkdir();
            DemoRenderer demoRenderer3 = this.mGLView.mRenderer;
            StringBuilder sb = new StringBuilder();
            DemoRenderer demoRenderer4 = this.mGLView.mRenderer;
            sb.append(DemoRenderer.m_saveFilesDir);
            sb.append('/');
            DemoRenderer.m_saveFilesDir = sb.toString();
        }
        int jCreateDsMap = RunnerJNILib.jCreateDsMap(null, null, null);
        RunnerJNILib.DsMapAddString(jCreateDsMap, "type", "permission_request_result");
        for (int i4 = 0; i4 < iArr.length; i4++) {
            RunnerJNILib.DsMapAddDouble(jCreateDsMap, strArr[i4], (double) (iArr[i4] == 0 ? 1 : 0));
        }
        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, this.EVENT_OTHER_SYSTEM_EVENT);
        if (mExtension != null) {
            while (true) {
                Object[] objArr = mExtension;
                if (i2 < objArr.length) {
                    if (objArr[i2] instanceof IExtensionBase) {
                        ((IExtensionBase) objArr[i2]).onRequestPermissionsResult(i, strArr, iArr);
                    }
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        Dialog onCreateDialog;
        if (mExtension == null) {
            return null;
        }
        int i2 = 0;
        while (true) {
            Object[] objArr = mExtension;
            if (i2 >= objArr.length) {
                return null;
            }
            if ((objArr[i2] instanceof IExtensionBase) && (onCreateDialog = ((IExtensionBase) objArr[i2]).onCreateDialog(i)) != null) {
                return onCreateDialog;
            }
            i2++;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        boolean dispatchKeyEvent;
        if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                } else if ((objArr[i] instanceof IExtensionBase) && (dispatchKeyEvent = ((IExtensionBase) objArr[i]).dispatchKeyEvent(keyEvent))) {
                    return dispatchKeyEvent;
                } else {
                    i++;
                }
            }
        }
        Gamepad.handleKeyEvent(keyEvent);
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
        boolean dispatchGenericMotionEvent;
        if (mExtension != null) {
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i >= objArr.length) {
                    break;
                } else if ((objArr[i] instanceof IExtensionBase) && (dispatchGenericMotionEvent = ((IExtensionBase) objArr[i]).dispatchGenericMotionEvent(motionEvent))) {
                    return dispatchGenericMotionEvent;
                } else {
                    i++;
                }
            }
        }
        if (motionEvent.getSource() == 16777232 || motionEvent.getSource() == 1025) {
            Gamepad.handleMotionEvent(motionEvent);
            RunnerKeyboardController runnerKeyboardController = this.m_keyboardController;
            if (runnerKeyboardController == null || !runnerKeyboardController.VirtualKeyboardGetStatus()) {
                return true;
            }
        }
        return super.dispatchGenericMotionEvent(motionEvent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c A[SYNTHETIC, Splitter:B:10:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean checkIsYoYoRunner() {
        /*
            r7 = this;
            java.lang.String r0 = "yoyo"
            r1 = 0
            r2 = 1
            android.content.res.Resources r3 = r7.getResources()     // Catch:{ Exception -> 0x0024 }
            android.content.res.AssetManager r3 = r3.getAssets()     // Catch:{ Exception -> 0x0024 }
            java.lang.String r4 = "game.droid"
            java.io.InputStream r3 = r3.open(r4)     // Catch:{ Exception -> 0x0024 }
            if (r3 == 0) goto L_0x001e
            java.lang.String r4 = "#######!!!!!!! Checking for runner - found assets "
            android.util.Log.i(r0, r4)     // Catch:{ Exception -> 0x0024 }
            r3.close()     // Catch:{ Exception -> 0x0024 }
            r3 = 0
            goto L_0x002a
        L_0x001e:
            java.lang.String r3 = "#######!!!!!!! Checking for runner - not found assets"
            android.util.Log.i(r0, r3)     // Catch:{ Exception -> 0x0024 }
            goto L_0x0029
        L_0x0024:
            java.lang.String r3 = "#######!!!!!!! Checking for runner! failed"
            android.util.Log.i(r0, r3)
        L_0x0029:
            r3 = 1
        L_0x002a:
            if (r3 == 0) goto L_0x007c
            android.content.pm.PackageManager r4 = r7.getPackageManager()     // Catch:{ Exception -> 0x0063 }
            android.content.ComponentName r5 = r7.getComponentName()     // Catch:{ Exception -> 0x0063 }
            java.lang.String r5 = r5.getPackageName()     // Catch:{ Exception -> 0x0063 }
            r6 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r4 = r4.getApplicationInfo(r5, r6)     // Catch:{ Exception -> 0x0063 }
            android.os.Bundle r4 = r4.metaData     // Catch:{ Exception -> 0x0063 }
            java.lang.String r5 = "IsBuiltAsYoYoRunner"
            java.lang.String r4 = r4.getString(r5)     // Catch:{ Exception -> 0x0063 }
            if (r4 == 0) goto L_0x0056
            java.lang.String r5 = "Yes"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x0063 }
            if (r4 == 0) goto L_0x0056
            java.lang.String r1 = "Found Runner flag in manifest, not using APK expansion"
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x0063 }
            goto L_0x007c
        L_0x0056:
            java.lang.String r4 = "#######!!!!!!! using APK Expansion file"
            android.util.Log.i(r0, r4)     // Catch:{ Exception -> 0x0063 }
            com.butakov.psebay.RunnerActivity.UseAPKExpansionFile = r2     // Catch:{ Exception -> 0x005f }
            r3 = 0
            goto L_0x007c
        L_0x005f:
            r2 = move-exception
            r1 = r2
            r3 = 0
            goto L_0x0064
        L_0x0063:
            r1 = move-exception
        L_0x0064:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Exception while reading package meta data"
            r2.append(r4)
            java.lang.String r1 = r1.toString()
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            android.util.Log.d(r0, r1)
        L_0x007c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.butakov.psebay.RunnerActivity.checkIsYoYoRunner():boolean");
    }

    private void setupExtensions() {
        if (mExtension == null) {
            IniBundle iniBundle = mYYPrefs;
            if (iniBundle == null) {
                Log.i("yoyo", "Unable to initialise extensions as preferences have not been loaded");
                return;
            }
            int i = iniBundle.getInt("YYNumExtensionClasses");
            if (i > 0) {
                mExtension = new Object[i];
            }
            for (int i2 = 0; i2 < i; i2++) {
                String string = mYYPrefs.getString("YYExtensionClass" + i2);
                if (string != null && !string.isEmpty()) {
                    try {
                        String str = "com.butakov.psebay." + string;
                        Log.i("yoyo", "Attempting to initialise extension class " + str);
                        mExtension[i2] = Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
                        try {
                            Method method = mExtension[i2].getClass().getMethod("Init", new Class[0]);
                            if (method != null) {
                                Log.i("yoyo", "Method found, attempting to invoke Init");
                                method.invoke(mExtension[i2], new Object[0]);
                            }
                        } catch (Exception e) {
                            Log.i("yoyo", "No Init method found on extension class:" + string + " returned " + e.getMessage());
                            e.printStackTrace();
                        }
                    } catch (Exception e2) {
                        Log.i("yoyo", "Exception thrown attempting to create extension class " + e2.getMessage());
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    private void setupAdvertising() {
        if (mExtension != null) {
            Log.i("yoyo", "checking " + mExtension.length + " extensions for ad interface");
            int i = 0;
            while (true) {
                Object[] objArr = mExtension;
                if (i < objArr.length) {
                    if (objArr[i] instanceof IAdExt) {
                        Log.i("yoyo", "Found advertising extension interface, calling setup");
                        ((IAdExt) mExtension[i]).setup();
                    }
                    i++;
                } else {
                    return;
                }
            }
        } else {
            Log.i("yoyo", "No extensions defined prior to advertising check");
        }
    }

    private void checkXPeriaPlay() {
        Log.i("yoyo", "@@@@@@@ Build.Display = " + Build.DISPLAY + " BRAND=" + Build.BRAND + " DEVICE=" + Build.DEVICE + " MANUFACTURER=" + Build.MANUFACTURER + " MODEL=" + Build.MODEL + " PRODUCT=" + Build.PRODUCT);
        XPeriaPlay = Build.MANUFACTURER.equals("Sony Ericsson") && Build.MODEL.startsWith("R800");
        Log.i("yoyo", "@@@@@@@ XPeriaPlay=" + XPeriaPlay + " manufacturer=" + Build.MANUFACTURER.equals("Sony Ericsson") + " model=" + Build.MODEL.startsWith("R800"));
    }

    public void setupUiVisibilityDelayed() {
        this.mRestoreImmersiveModeHandler.postDelayed(this.restoreImmersiveModeRunnable, 500);
    }

    public void setupUiVisibility() {
        if (this.mGLView != null) {
            int i = 1;
            try {
                mSetSystemUiVisibility = View.class.getMethod("setSystemUiVisibility", Integer.TYPE);
                if (Build.VERSION.SDK_INT >= 19) {
                    if ((UIVisibilityFlags & 256) != 0) {
                        ourSetSystemUiVisibility(UIVisibilityFlags & -257);
                    }
                    Log.i("yoyo", "Setting vis flags to " + UIVisibilityFlags);
                    i = UIVisibilityFlags;
                }
                ourSetSystemUiVisibility(i);
            } catch (Exception e) {
                Log.i("yoyo", "Exception while getting setSystemUiVisibility :: " + e.toString());
            }
        }
    }

    public void setupView() {
        Log.i("yoyo", " + + + + setupView + + + +");
        setContentView((int) R.layout.main);
        this.mGLView = (DemoGLSurfaceView) findViewById(R.id.demogl);
        setupUiVisibility();
        setupUiVisibilityDelayed();
        Object obj = this.vsyncHandler;
        if (obj != null) {
            ((RunnerVsyncHandler) obj).PostFrameCallback();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        DemoGLSurfaceView demoGLSurfaceView = this.mGLView;
        if (demoGLSurfaceView != null) {
            demoGLSurfaceView.surfaceCreated(surfaceHolder);
        }
        Object obj = this.vsyncHandler;
        if (obj != null) {
            ((RunnerVsyncHandler) obj).PostFrameCallback();
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        DemoGLSurfaceView demoGLSurfaceView = this.mGLView;
        if (demoGLSurfaceView != null) {
            demoGLSurfaceView.surfaceChanged(surfaceHolder, i, i2, i3);
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        DemoGLSurfaceView demoGLSurfaceView = this.mGLView;
        if (demoGLSurfaceView != null) {
            demoGLSurfaceView.surfaceDestroyed(surfaceHolder);
        }
    }

    private void ourSetSystemUiVisibility(int i) {
        Method method = mSetSystemUiVisibility;
        if (method != null) {
            try {
                method.invoke(this.mGLView, Integer.valueOf(i));
            } catch (Exception e) {
                Log.i("yoyo", "Exception while calling setSystemUiVisibility " + e.toString());
            }
        } else {
            Log.i("yoyo", "!!!!Unable to do mSetSystemUiVisibility(" + i + ")");
        }
    }

    private String replaceLanguageAndRegion(String str) {
        if (!str.contains("%lang%") && !str.contains("%region%")) {
            return str;
        }
        Locale locale = Locale.getDefault();
        return str.replace("%lang%", locale.getLanguage().toLowerCase(Locale.US)).replace("%region%", locale.getCountry().toLowerCase(Locale.US));
    }

    public int GetOrientation() {
        int i;
        if (Build.VERSION.SDK_INT > 7) {
            i = getWindowManager().getDefaultDisplay().getRotation();
        } else {
            i = getWindowManager().getDefaultDisplay().getOrientation();
        }
        Configuration configuration = getResources().getConfiguration();
        if (!((configuration.orientation == 2 && (i == 0 || i == 2)) || (configuration.orientation == 1 && (i == 1 || i == 3)))) {
            i = (i + 1) & 3;
        }
        Log.i("yoyo", "calculated orientation - " + Orientation);
        return i;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Orientation = GetOrientation();
        RunnerKeyboardController GetKeyboardController = GetKeyboardController();
        int i = 0;
        if (GetKeyboardController != null) {
            boolean z = true;
            if (getResources().getConfiguration().hardKeyboardHidden != 1) {
                z = false;
            }
            GetKeyboardController.SetPhysicalKeyboardConnected(z);
        }
        if (mExtension != null) {
            while (true) {
                Object[] objArr = mExtension;
                if (i < objArr.length) {
                    if (objArr[i] instanceof IExtensionBase) {
                        ((IExtensionBase) objArr[i]).onConfigurationChanged(configuration);
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public void SetLaunchedFromPlayer(String str, boolean z) {
        LaunchedFromPlayer = z;
        LaunchedFromPlayerGameFile = str;
        Log.i("yoyo", "JNI SetLaunchedFromPlayer. Game path: " + str);
        setupIniFile();
    }

    public float getRefreshRate() {
        float refreshRate = getWindowManager().getDefaultDisplay().getRefreshRate();
        if (refreshRate < 10.0f) {
            return 60.0f;
        }
        return refreshRate;
    }

    public RunnerKeyboardController GetKeyboardController() {
        if (this.m_keyboardController == null) {
            this.m_keyboardController = RunnerKeyboardController.Create(this, getWindow().getDecorView(), ViewHandler);
        }
        return this.m_keyboardController;
    }
}
