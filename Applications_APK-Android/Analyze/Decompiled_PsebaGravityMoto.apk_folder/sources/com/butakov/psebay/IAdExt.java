package com.butakov.psebay;

public interface IAdExt extends IExtensionBase {
    void disable(int i);

    void enable(int i, int i2, int i3);

    void event(String str);

    int getAdDisplayHeight(int i);

    int getAdDisplayWidth(int i);

    void move(int i, int i2, int i3);

    void setup();
}
