package com.butakov.psebay;

import android.util.Log;
import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.yoyogames.runner.RunnerJNILib;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GooglePlayBillingService {
    private static final String TAG = "yoyo";
    /* access modifiers changed from: private */
    public static boolean m_isStoreConnected = false;
    /* access modifiers changed from: private */
    public BillingClient m_billingClient;
    /* access modifiers changed from: private */
    public HashMap<String, Purchase> m_purchaseRequests = new HashMap<>();
    private YYPurchasesUpdatedListener m_purchaseUpdatedListener;
    /* access modifiers changed from: private */
    public List<SkuDetails> m_skuDetails;
    /* access modifiers changed from: private */
    public List<SkuDetails> m_subSkuDetails;

    public void Destroy() {
    }

    public Object InitRunnerBilling() {
        return this;
    }

    public void restorePurchasedItems() {
    }

    private class YYPurchasesUpdatedListener implements PurchasesUpdatedListener {
        private YYPurchasesUpdatedListener() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
        public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> list) {
            String[] strArr = {"id"};
            double[] dArr = {12001.0d};
            if (billingResult.getResponseCode() == 0) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    JSONArray jSONArray = new JSONArray();
                    jSONObject.put("success", true);
                    for (Purchase next : list) {
                        if (!GooglePlayBillingService.this.m_purchaseRequests.containsKey(next.getPurchaseToken())) {
                            GooglePlayBillingService.this.m_purchaseRequests.put(next.getPurchaseToken(), next);
                        }
                        jSONArray.put(new JSONObject(next.getOriginalJson()));
                    }
                    jSONObject.put("purchases", jSONArray);
                    String jSONObject2 = jSONObject.toString();
                    int jCreateDsMap = RunnerJNILib.jCreateDsMap(strArr, null, dArr);
                    RunnerJNILib.DsMapAddString(jCreateDsMap, "response_json", jSONObject2);
                    RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 66);
                } catch (JSONException unused) {
                    Log.e(GooglePlayBillingService.TAG, "Malformed JSON data from queryPurchases.");
                }
            } else if (billingResult.getResponseCode() == 1) {
                Log.w(GooglePlayBillingService.TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping");
                int jCreateDsMap2 = RunnerJNILib.jCreateDsMap(strArr, null, dArr);
                RunnerJNILib.DsMapAddString(jCreateDsMap2, "response_json", "{ \"success\":false, \"failure\":\"user_cancelled\" }");
                RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap2, 66);
            } else {
                Log.w(GooglePlayBillingService.TAG, "onPurchasesUpdated() got unknown resultCode: " + billingResult.getResponseCode());
                int jCreateDsMap3 = RunnerJNILib.jCreateDsMap(strArr, null, dArr);
                RunnerJNILib.DsMapAddString(jCreateDsMap3, "response_json", "{ \"success\":false, \"failure\":\"unknown error\", \"responseCode\":" + Integer.toString(billingResult.getResponseCode()) + " }");
                RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap3, 66);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public String queryPurchases(String str) {
        Purchase.PurchasesResult queryPurchases = this.m_billingClient.queryPurchases(str);
        if (queryPurchases != null && queryPurchases.getBillingResult().getResponseCode() == 0) {
            try {
                JSONObject jSONObject = new JSONObject();
                JSONArray jSONArray = new JSONArray();
                for (Purchase originalJson : queryPurchases.getPurchasesList()) {
                    jSONArray.put(new JSONObject(originalJson.getOriginalJson()));
                }
                jSONObject.put("purchases", jSONArray);
                jSONObject.put("success", true);
                return jSONObject.toString();
            } catch (JSONException unused) {
                Log.e(TAG, "Malformed JSON data from queryPurchases.");
            }
        }
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("success", false);
            if (queryPurchases != null) {
                jSONObject2.put("responseCode", queryPurchases.getBillingResult().getResponseCode());
            }
            return jSONObject2.toString();
        } catch (JSONException unused2) {
            Log.e(TAG, "Malformed JSON data from queryPurchases after failure.");
            return "";
        }
    }

    public void loadStore() {
        m_isStoreConnected = false;
        this.m_purchaseUpdatedListener = new YYPurchasesUpdatedListener();
        this.m_billingClient = BillingClient.newBuilder(RunnerJNILib.ms_context).setListener(this.m_purchaseUpdatedListener).enablePendingPurchases().build();
        this.m_billingClient.startConnection(new BillingClientStateListener() {
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == 0) {
                    boolean unused = GooglePlayBillingService.m_isStoreConnected = true;
                    RunnerJNILib.CreateAsynEventWithDSMap(RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12005.0d}), 66);
                    return;
                }
                boolean unused2 = GooglePlayBillingService.m_isStoreConnected = false;
                RunnerJNILib.CreateAsynEventWithDSMap(RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12006.0d}), 66);
            }

            public void onBillingServiceDisconnected() {
                boolean unused = GooglePlayBillingService.m_isStoreConnected = false;
                RunnerJNILib.CreateAsynEventWithDSMap(RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12006.0d}), 66);
            }
        });
    }

    public boolean isStoreConnected() {
        return m_isStoreConnected;
    }

    public int retrieveManagedProducts(List<String> list) {
        SkuDetailsParams.Builder newBuilder = SkuDetailsParams.newBuilder();
        newBuilder.setSkusList(list).setType(BillingClient.SkuType.INAPP);
        this.m_billingClient.querySkuDetailsAsync(newBuilder.build(), new SkuDetailsResponseListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                if (billingResult.getResponseCode() == 0) {
                    try {
                        List unused = GooglePlayBillingService.this.m_skuDetails = list;
                        JSONObject jSONObject = new JSONObject();
                        JSONArray jSONArray = new JSONArray();
                        for (SkuDetails originalJson : list) {
                            jSONArray.put(new JSONObject(originalJson.getOriginalJson()));
                        }
                        jSONObject.put("skuDetails", jSONArray);
                        jSONObject.put("success", true);
                        String jSONObject2 = jSONObject.toString();
                        int jCreateDsMap = RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12003.0d});
                        RunnerJNILib.DsMapAddString(jCreateDsMap, "response_json", jSONObject2);
                        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 66);
                    } catch (JSONException unused2) {
                        Log.e(GooglePlayBillingService.TAG, "Malformed JSON data from queryPurchases.");
                    }
                } else {
                    try {
                        Log.w(GooglePlayBillingService.TAG, "onSkuDetailsResponse response was unsuccessful! Error Code: " + Integer.toString(billingResult.getResponseCode()));
                        JSONObject jSONObject3 = new JSONObject();
                        jSONObject3.put("success", false);
                        jSONObject3.put("responseCode", billingResult.getResponseCode());
                        String jSONObject4 = jSONObject3.toString();
                        int jCreateDsMap2 = RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12003.0d});
                        RunnerJNILib.DsMapAddString(jCreateDsMap2, "response_json", jSONObject4);
                        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap2, 66);
                    } catch (JSONException unused3) {
                        Log.e(GooglePlayBillingService.TAG, "Malformed JSON data from queryPurchases.");
                    }
                }
            }
        });
        return 0;
    }

    public int retrieveSubscriptions(List<String> list) {
        SkuDetailsParams.Builder newBuilder = SkuDetailsParams.newBuilder();
        newBuilder.setSkusList(list).setType(BillingClient.SkuType.SUBS);
        this.m_billingClient.querySkuDetailsAsync(newBuilder.build(), new SkuDetailsResponseListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                if (billingResult.getResponseCode() == 0) {
                    try {
                        List unused = GooglePlayBillingService.this.m_subSkuDetails = list;
                        JSONObject jSONObject = new JSONObject();
                        JSONArray jSONArray = new JSONArray();
                        for (SkuDetails originalJson : list) {
                            jSONArray.put(new JSONObject(originalJson.getOriginalJson()));
                        }
                        jSONObject.put("skuDetails", jSONArray);
                        jSONObject.put("success", true);
                        String jSONObject2 = jSONObject.toString();
                        int jCreateDsMap = RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12009.0d});
                        RunnerJNILib.DsMapAddString(jCreateDsMap, "response_json", jSONObject2);
                        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 66);
                    } catch (JSONException unused2) {
                        Log.e(GooglePlayBillingService.TAG, "Malformed JSON data from queryPurchases.");
                    }
                } else {
                    try {
                        Log.w(GooglePlayBillingService.TAG, "onSkuDetailsResponse response was unsuccessful! Error Code: " + Integer.toString(billingResult.getResponseCode()));
                        JSONObject jSONObject3 = new JSONObject();
                        jSONObject3.put("success", false);
                        jSONObject3.put("responseCode", billingResult.getResponseCode());
                        String jSONObject4 = jSONObject3.toString();
                        int jCreateDsMap2 = RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12009.0d});
                        RunnerJNILib.DsMapAddString(jCreateDsMap2, "response_json", jSONObject4);
                        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap2, 66);
                    } catch (JSONException unused3) {
                        Log.e(GooglePlayBillingService.TAG, "Malformed JSON data from queryPurchases.");
                    }
                }
            }
        });
        return 0;
    }

    public List<SkuDetails> GetSkuDetails() {
        return this.m_skuDetails;
    }

    public List<SkuDetails> GetSubSkuDetails() {
        return this.m_subSkuDetails;
    }

    public Map<String, Purchase> GetPurchases() {
        return this.m_purchaseRequests;
    }

    public boolean isFeatureSupported(String str) {
        BillingResult isFeatureSupported = this.m_billingClient.isFeatureSupported(str);
        if (isFeatureSupported.getResponseCode() != 0) {
            Log.w(TAG, "feature: " + str + "() got an error response: " + isFeatureSupported);
        }
        return isFeatureSupported.getResponseCode() == 0;
    }

    private int purchaseSku(String str, List<SkuDetails> list) {
        final SkuDetails skuDetails;
        Iterator<SkuDetails> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                skuDetails = null;
                break;
            }
            skuDetails = it.next();
            if (skuDetails.getSku().equals(str)) {
                break;
            }
        }
        if (skuDetails == null) {
            Log.e(TAG, "Sku: " + str + ", not found.");
            return 1;
        }
        RunnerActivity.CurrentActivity.runOnUiThread(new Runnable() {
            public void run() {
                GooglePlayBillingService.this.m_billingClient.launchBillingFlow(RunnerActivity.CurrentActivity, BillingFlowParams.newBuilder().setSkuDetails(skuDetails).build());
            }
        });
        return 0;
    }

    public int purchaseCatalogItem(String str) {
        return purchaseSku(str, this.m_skuDetails);
    }

    public int purchaseSubscription(String str) {
        return purchaseSku(str, this.m_subSkuDetails);
    }

    public void acknowledgePurchase(final String str) {
        RunnerActivity.CurrentActivity.runOnUiThread(new Runnable() {
            public void run() {
                GooglePlayBillingService.this.m_billingClient.acknowledgePurchase(AcknowledgePurchaseParams.newBuilder().setPurchaseToken(str).build(), new AcknowledgePurchaseResponseListener() {
                    public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
                        int jCreateDsMap = RunnerJNILib.jCreateDsMap(new String[]{"id"}, null, new double[]{12008.0d});
                        RunnerJNILib.DsMapAddString(jCreateDsMap, "response_json", "{ \"responseCode\" : " + Integer.toString(billingResult.getResponseCode()) + " }");
                        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 66);
                    }
                });
            }
        });
    }

    public void consumeProduct(final String str) {
        RunnerActivity.CurrentActivity.runOnUiThread(new Runnable() {
            public void run() {
                GooglePlayBillingService.this.m_billingClient.consumeAsync(ConsumeParams.newBuilder().setPurchaseToken(str).build(), new ConsumeResponseListener() {
                    public void onConsumeResponse(BillingResult billingResult, String str) {
                        String str2;
                        if (GooglePlayBillingService.this.m_purchaseRequests.containsKey(str)) {
                            GooglePlayBillingService.this.m_purchaseRequests.remove(str);
                        }
                        String[] strArr = {"id"};
                        double[] dArr = {12007.0d};
                        if (billingResult.getResponseCode() == 0) {
                            str2 = "{ \"success\":true, \"purchaseToken\" : \"" + str + "\" }";
                        } else {
                            str2 = "{ \"success\":false, \"responseCode\" : " + Integer.toString(billingResult.getResponseCode()) + " }";
                        }
                        int jCreateDsMap = RunnerJNILib.jCreateDsMap(strArr, null, dArr);
                        RunnerJNILib.DsMapAddString(jCreateDsMap, "response_json", str2);
                        RunnerJNILib.CreateAsynEventWithDSMap(jCreateDsMap, 66);
                    }
                });
            }
        });
    }
}
