package com.butakov.psebay;

public interface ISocial extends IExtensionBase {
    void Event(String str);

    void GetInfo(String str);

    void Init();

    void LoadFriends();

    void LoadLeaderboard(String str, int i, int i2, int i3);

    void LoadPic(String str);

    void Login();

    void Logout();

    void PostScore(String str, int i);

    void Show(int i, String str, int i2);
}
