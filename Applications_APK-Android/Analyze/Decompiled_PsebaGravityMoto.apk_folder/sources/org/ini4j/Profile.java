package org.ini4j;

public interface Profile extends MultiMap<String, Section>, CommentedMap<String, Section> {
    public static final char PATH_SEPARATOR = '/';

    public interface Section extends OptionMap {
        Section addChild(String str);

        String[] childrenNames();

        Section getChild(String str);

        String getName();

        Section getParent();

        String getSimpleName();

        Section lookup(String... strArr);

        void removeChild(String str);
    }

    Section add(String str);

    void add(String str, String str2, Object obj);

    <T> T as(Class<T> cls);

    <T> T as(Class<T> cls, String str);

    <T> T fetch(Object obj, Object obj2, Class<T> cls);

    String fetch(Object obj, Object obj2);

    <T> T get(Object obj, Object obj2, Class<T> cls);

    String get(Object obj, Object obj2);

    String getComment();

    String put(String str, String str2, Object obj);

    String remove(Object obj, Object obj2);

    Section remove(Section section);

    void setComment(String str);
}
