package org.ini4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import org.ini4j.Profile;

public class IniPreferences extends AbstractPreferences {
    /* access modifiers changed from: private */
    public static final String[] EMPTY = new String[0];
    /* access modifiers changed from: private */
    public final Ini _ini;

    /* access modifiers changed from: protected */
    public void flushSpi() throws BackingStoreException {
    }

    /* access modifiers changed from: protected */
    public void syncSpi() throws BackingStoreException {
    }

    public IniPreferences(Ini ini) {
        super(null, "");
        this._ini = ini;
    }

    public IniPreferences(Reader reader) throws IOException, InvalidFileFormatException {
        super(null, "");
        this._ini = new Ini(reader);
    }

    public IniPreferences(InputStream inputStream) throws IOException, InvalidFileFormatException {
        super(null, "");
        this._ini = new Ini(inputStream);
    }

    public IniPreferences(URL url) throws IOException, InvalidFileFormatException {
        super(null, "");
        this._ini = new Ini(url);
    }

    /* access modifiers changed from: protected */
    public Ini getIni() {
        return this._ini;
    }

    /* access modifiers changed from: protected */
    public String getSpi(String str) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: protected */
    public String[] childrenNamesSpi() throws BackingStoreException {
        ArrayList arrayList = new ArrayList();
        for (String str : this._ini.keySet()) {
            if (str.indexOf(this._ini.getPathSeparator()) < 0) {
                arrayList.add(str);
            }
        }
        return (String[]) arrayList.toArray(EMPTY);
    }

    /* access modifiers changed from: protected */
    public SectionPreferences childSpi(String str) {
        Profile.Section section = (Profile.Section) this._ini.get(str);
        boolean z = section == null;
        if (z) {
            section = this._ini.add(str);
        }
        return new SectionPreferences(this, section, z);
    }

    /* access modifiers changed from: protected */
    public String[] keysSpi() throws BackingStoreException {
        return EMPTY;
    }

    /* access modifiers changed from: protected */
    public void putSpi(String str, String str2) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: protected */
    public void removeNodeSpi() throws BackingStoreException, UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: protected */
    public void removeSpi(String str) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    protected class SectionPreferences extends AbstractPreferences {
        private final Profile.Section _section;

        /* access modifiers changed from: protected */
        public void flushSpi() throws BackingStoreException {
        }

        /* access modifiers changed from: protected */
        public void syncSpi() throws BackingStoreException {
        }

        SectionPreferences(AbstractPreferences abstractPreferences, Profile.Section section, boolean z) {
            super(abstractPreferences, section.getSimpleName());
            this._section = section;
            this.newNode = z;
        }

        public void flush() throws BackingStoreException {
            parent().flush();
        }

        public void sync() throws BackingStoreException {
            parent().sync();
        }

        /* access modifiers changed from: protected */
        public String getSpi(String str) {
            return this._section.fetch(str);
        }

        /* access modifiers changed from: protected */
        public String[] childrenNamesSpi() throws BackingStoreException {
            return this._section.childrenNames();
        }

        /* access modifiers changed from: protected */
        public SectionPreferences childSpi(String str) throws UnsupportedOperationException {
            Profile.Section child = this._section.getChild(str);
            boolean z = child == null;
            if (z) {
                child = this._section.addChild(str);
            }
            return new SectionPreferences(this, child, z);
        }

        /* access modifiers changed from: protected */
        public String[] keysSpi() throws BackingStoreException {
            return (String[]) this._section.keySet().toArray(IniPreferences.EMPTY);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object}
         arg types: [java.lang.String, java.lang.String]
         candidates:
          org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
          SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object} */
        /* access modifiers changed from: protected */
        public void putSpi(String str, String str2) {
            this._section.put((Object) str, (Object) str2);
        }

        /* access modifiers changed from: protected */
        public void removeNodeSpi() throws BackingStoreException {
            IniPreferences.this._ini.remove(this._section);
        }

        /* access modifiers changed from: protected */
        public void removeSpi(String str) {
            this._section.remove(str);
        }
    }
}
