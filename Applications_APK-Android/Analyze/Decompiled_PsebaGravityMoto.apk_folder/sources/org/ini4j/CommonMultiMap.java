package org.ini4j;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class CommonMultiMap<K, V> extends BasicMultiMap<K, V> implements CommentedMap<K, V> {
    private static final String FIRST_CATEGORY = "";
    private static final String LAST_CATEGORY = "zzzzzzzzzzzzzzzzzzzzzz";
    private static final String META_COMMENT = "comment";
    private static final String SEPARATOR = ";#;";
    private static final long serialVersionUID = 3012579878005541746L;
    private SortedMap<String, Object> _meta;

    public String getComment(Object obj) {
        return (String) getMeta("comment", obj);
    }

    public void clear() {
        super.clear();
        SortedMap<String, Object> sortedMap = this._meta;
        if (sortedMap != null) {
            sortedMap.clear();
        }
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        SortedMap<String, Object> sortedMap;
        super.putAll(map);
        if ((map instanceof CommonMultiMap) && (sortedMap = ((CommonMultiMap) map)._meta) != null) {
            meta().putAll(sortedMap);
        }
    }

    public String putComment(K k, String str) {
        return (String) putMeta("comment", k, str);
    }

    public V remove(Object obj) {
        V remove = super.remove(obj);
        removeMeta(obj);
        return remove;
    }

    public V remove(Object obj, int i) {
        V remove = super.remove(obj, i);
        if (length(obj) == 0) {
            removeMeta(obj);
        }
        return remove;
    }

    public String removeComment(Object obj) {
        return (String) removeMeta("comment", obj);
    }

    /* access modifiers changed from: package-private */
    public Object getMeta(String str, Object obj) {
        SortedMap<String, Object> sortedMap = this._meta;
        if (sortedMap == null) {
            return null;
        }
        return sortedMap.get(makeKey(str, obj));
    }

    /* access modifiers changed from: package-private */
    public Object putMeta(String str, K k, Object obj) {
        return meta().put(makeKey(str, k), obj);
    }

    /* access modifiers changed from: package-private */
    public void removeMeta(Object obj) {
        SortedMap<String, Object> sortedMap = this._meta;
        if (sortedMap != null) {
            sortedMap.subMap(makeKey("", obj), makeKey(LAST_CATEGORY, obj)).clear();
        }
    }

    /* access modifiers changed from: package-private */
    public Object removeMeta(String str, Object obj) {
        SortedMap<String, Object> sortedMap = this._meta;
        if (sortedMap == null) {
            return null;
        }
        return sortedMap.remove(makeKey(str, obj));
    }

    private String makeKey(String str, Object obj) {
        return String.valueOf(obj) + SEPARATOR + str;
    }

    private Map<String, Object> meta() {
        if (this._meta == null) {
            this._meta = new TreeMap();
        }
        return this._meta;
    }
}
