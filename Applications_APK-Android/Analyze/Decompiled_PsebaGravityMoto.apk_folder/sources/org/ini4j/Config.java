package org.ini4j;

import java.io.Serializable;
import java.nio.charset.Charset;
import org.apache.http.protocol.HTTP;

public class Config implements Cloneable, Serializable {
    public static final boolean DEFAULT_COMMENT = true;
    public static final boolean DEFAULT_EMPTY_OPTION = false;
    public static final boolean DEFAULT_EMPTY_SECTION = false;
    public static final boolean DEFAULT_ESCAPE = true;
    public static final boolean DEFAULT_ESCAPE_NEWLINE = true;
    public static final Charset DEFAULT_FILE_ENCODING = Charset.forName(HTTP.UTF_8);
    public static final boolean DEFAULT_GLOBAL_SECTION = false;
    public static final String DEFAULT_GLOBAL_SECTION_NAME = "?";
    public static final boolean DEFAULT_HEADER_COMMENT = true;
    public static final boolean DEFAULT_INCLUDE = false;
    public static final String DEFAULT_LINE_SEPARATOR = getSystemProperty("line.separator", "\n");
    public static final boolean DEFAULT_LOWER_CASE_OPTION = false;
    public static final boolean DEFAULT_LOWER_CASE_SECTION = false;
    public static final boolean DEFAULT_MULTI_OPTION = true;
    public static final boolean DEFAULT_MULTI_SECTION = false;
    public static final char DEFAULT_PATH_SEPARATOR = '/';
    public static final boolean DEFAULT_PROPERTY_FIRST_UPPER = false;
    public static final boolean DEFAULT_STRICT_OPERATOR = false;
    public static final boolean DEFAULT_TREE = true;
    public static final boolean DEFAULT_UNNAMED_SECTION = false;
    private static final Config GLOBAL = new Config();
    public static final String KEY_PREFIX = "org.ini4j.config.";
    public static final String PROP_COMMENT = "comment";
    public static final String PROP_EMPTY_OPTION = "emptyOption";
    public static final String PROP_EMPTY_SECTION = "emptySection";
    public static final String PROP_ESCAPE = "escape";
    public static final String PROP_ESCAPE_NEWLINE = "escapeNewline";
    public static final String PROP_FILE_ENCODING = "fileEncoding";
    public static final String PROP_GLOBAL_SECTION = "globalSection";
    public static final String PROP_GLOBAL_SECTION_NAME = "globalSectionName";
    public static final String PROP_HEADER_COMMENT = "headerComment";
    public static final String PROP_INCLUDE = "include";
    public static final String PROP_LINE_SEPARATOR = "lineSeparator";
    public static final String PROP_LOWER_CASE_OPTION = "lowerCaseOption";
    public static final String PROP_LOWER_CASE_SECTION = "lowerCaseSection";
    public static final String PROP_MULTI_OPTION = "multiOption";
    public static final String PROP_MULTI_SECTION = "multiSection";
    public static final String PROP_PATH_SEPARATOR = "pathSeparator";
    public static final String PROP_PROPERTY_FIRST_UPPER = "propertyFirstUpper";
    public static final String PROP_STRICT_OPERATOR = "strictOperator";
    public static final String PROP_TREE = "tree";
    public static final String PROP_UNNAMED_SECTION = "unnamedSection";
    private static final long serialVersionUID = 2865793267410367814L;
    private boolean _comment;
    private boolean _emptyOption;
    private boolean _emptySection;
    private boolean _escape;
    private boolean _escapeNewline;
    private Charset _fileEncoding;
    private boolean _globalSection;
    private String _globalSectionName;
    private boolean _headerComment;
    private boolean _include;
    private String _lineSeparator;
    private boolean _lowerCaseOption;
    private boolean _lowerCaseSection;
    private boolean _multiOption;
    private boolean _multiSection;
    private char _pathSeparator;
    private boolean _propertyFirstUpper;
    private boolean _strictOperator;
    private boolean _tree;
    private boolean _unnamedSection;

    public Config() {
        reset();
    }

    public static String getEnvironment(String str) {
        return getEnvironment(str, null);
    }

    public static String getEnvironment(String str, String str2) {
        String str3;
        try {
            str3 = System.getenv(str);
        } catch (SecurityException unused) {
            str3 = null;
        }
        return str3 == null ? str2 : str3;
    }

    public static Config getGlobal() {
        return GLOBAL;
    }

    public static String getSystemProperty(String str) {
        return getSystemProperty(str, null);
    }

    public static String getSystemProperty(String str, String str2) {
        String str3;
        try {
            str3 = System.getProperty(str);
        } catch (SecurityException unused) {
            str3 = null;
        }
        return str3 == null ? str2 : str3;
    }

    public void setComment(boolean z) {
        this._comment = z;
    }

    public boolean isEscape() {
        return this._escape;
    }

    public boolean isEscapeNewline() {
        return this._escapeNewline;
    }

    public boolean isInclude() {
        return this._include;
    }

    public boolean isTree() {
        return this._tree;
    }

    public void setEmptyOption(boolean z) {
        this._emptyOption = z;
    }

    public void setEmptySection(boolean z) {
        this._emptySection = z;
    }

    public void setEscape(boolean z) {
        this._escape = z;
    }

    public void setEscapeNewline(boolean z) {
        this._escapeNewline = z;
    }

    public Charset getFileEncoding() {
        return this._fileEncoding;
    }

    public void setFileEncoding(Charset charset) {
        this._fileEncoding = charset;
    }

    public void setGlobalSection(boolean z) {
        this._globalSection = z;
    }

    public String getGlobalSectionName() {
        return this._globalSectionName;
    }

    public void setGlobalSectionName(String str) {
        this._globalSectionName = str;
    }

    public void setHeaderComment(boolean z) {
        this._headerComment = z;
    }

    public void setInclude(boolean z) {
        this._include = z;
    }

    public String getLineSeparator() {
        return this._lineSeparator;
    }

    public void setLineSeparator(String str) {
        this._lineSeparator = str;
    }

    public void setLowerCaseOption(boolean z) {
        this._lowerCaseOption = z;
    }

    public void setLowerCaseSection(boolean z) {
        this._lowerCaseSection = z;
    }

    public void setMultiOption(boolean z) {
        this._multiOption = z;
    }

    public void setMultiSection(boolean z) {
        this._multiSection = z;
    }

    public boolean isEmptyOption() {
        return this._emptyOption;
    }

    public boolean isEmptySection() {
        return this._emptySection;
    }

    public boolean isGlobalSection() {
        return this._globalSection;
    }

    public boolean isLowerCaseOption() {
        return this._lowerCaseOption;
    }

    public boolean isLowerCaseSection() {
        return this._lowerCaseSection;
    }

    public boolean isMultiOption() {
        return this._multiOption;
    }

    public boolean isMultiSection() {
        return this._multiSection;
    }

    public boolean isUnnamedSection() {
        return this._unnamedSection;
    }

    public char getPathSeparator() {
        return this._pathSeparator;
    }

    public void setPathSeparator(char c) {
        this._pathSeparator = c;
    }

    public void setPropertyFirstUpper(boolean z) {
        this._propertyFirstUpper = z;
    }

    public boolean isPropertyFirstUpper() {
        return this._propertyFirstUpper;
    }

    public boolean isStrictOperator() {
        return this._strictOperator;
    }

    public void setStrictOperator(boolean z) {
        this._strictOperator = z;
    }

    public boolean isComment() {
        return this._comment;
    }

    public boolean isHeaderComment() {
        return this._headerComment;
    }

    public void setTree(boolean z) {
        this._tree = z;
    }

    public void setUnnamedSection(boolean z) {
        this._unnamedSection = z;
    }

    public Config clone() {
        try {
            return (Config) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final void reset() {
        this._emptyOption = getBoolean(PROP_EMPTY_OPTION, false);
        this._emptySection = getBoolean(PROP_EMPTY_SECTION, false);
        this._globalSection = getBoolean(PROP_GLOBAL_SECTION, false);
        this._globalSectionName = getString(PROP_GLOBAL_SECTION_NAME, DEFAULT_GLOBAL_SECTION_NAME);
        this._include = getBoolean(PROP_INCLUDE, false);
        this._lowerCaseOption = getBoolean(PROP_LOWER_CASE_OPTION, false);
        this._lowerCaseSection = getBoolean(PROP_LOWER_CASE_SECTION, false);
        this._multiOption = getBoolean(PROP_MULTI_OPTION, true);
        this._multiSection = getBoolean(PROP_MULTI_SECTION, false);
        this._strictOperator = getBoolean(PROP_STRICT_OPERATOR, false);
        this._unnamedSection = getBoolean(PROP_UNNAMED_SECTION, false);
        this._escape = getBoolean(PROP_ESCAPE, true);
        this._escapeNewline = getBoolean(PROP_ESCAPE_NEWLINE, true);
        this._pathSeparator = getChar(PROP_PATH_SEPARATOR, '/');
        this._tree = getBoolean(PROP_TREE, true);
        this._propertyFirstUpper = getBoolean(PROP_PROPERTY_FIRST_UPPER, false);
        this._lineSeparator = getString(PROP_LINE_SEPARATOR, DEFAULT_LINE_SEPARATOR);
        this._fileEncoding = getCharset(PROP_FILE_ENCODING, DEFAULT_FILE_ENCODING);
        this._comment = getBoolean("comment", true);
        this._headerComment = getBoolean(PROP_HEADER_COMMENT, true);
    }

    private boolean getBoolean(String str, boolean z) {
        String systemProperty = getSystemProperty(KEY_PREFIX + str);
        return systemProperty == null ? z : Boolean.parseBoolean(systemProperty);
    }

    private char getChar(String str, char c) {
        String systemProperty = getSystemProperty(KEY_PREFIX + str);
        return systemProperty == null ? c : systemProperty.charAt(0);
    }

    private Charset getCharset(String str, Charset charset) {
        String systemProperty = getSystemProperty(KEY_PREFIX + str);
        return systemProperty == null ? charset : Charset.forName(systemProperty);
    }

    private String getString(String str, String str2) {
        return getSystemProperty(KEY_PREFIX + str, str2);
    }
}
