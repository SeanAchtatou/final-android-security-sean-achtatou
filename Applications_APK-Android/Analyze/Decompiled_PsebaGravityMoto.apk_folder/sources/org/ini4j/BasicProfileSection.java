package org.ini4j;

import java.util.ArrayList;
import java.util.regex.Pattern;
import org.ini4j.Profile;

class BasicProfileSection extends BasicOptionMap implements Profile.Section {
    private static final String[] EMPTY_STRING_ARRAY = new String[0];
    private static final char REGEXP_ESCAPE_CHAR = '\\';
    private static final long serialVersionUID = 985800697957194374L;
    private final Pattern _childPattern;
    private final String _name;
    private final BasicProfile _profile;

    protected BasicProfileSection(BasicProfile basicProfile, String str) {
        this._profile = basicProfile;
        this._name = str;
        this._childPattern = newChildPattern(str);
    }

    public Profile.Section getChild(String str) {
        return (Profile.Section) this._profile.get(childName(str));
    }

    public String getName() {
        return this._name;
    }

    public Profile.Section getParent() {
        int lastIndexOf = this._name.lastIndexOf(this._profile.getPathSeparator());
        if (lastIndexOf < 0) {
            return null;
        }
        return (Profile.Section) this._profile.get(this._name.substring(0, lastIndexOf));
    }

    public String getSimpleName() {
        int lastIndexOf = this._name.lastIndexOf(this._profile.getPathSeparator());
        return lastIndexOf < 0 ? this._name : this._name.substring(lastIndexOf + 1);
    }

    public Profile.Section addChild(String str) {
        return this._profile.add(childName(str));
    }

    public String[] childrenNames() {
        ArrayList arrayList = new ArrayList();
        for (String str : this._profile.keySet()) {
            if (this._childPattern.matcher(str).matches()) {
                arrayList.add(str.substring(this._name.length() + 1));
            }
        }
        return (String[]) arrayList.toArray(EMPTY_STRING_ARRAY);
    }

    public Profile.Section lookup(String... strArr) {
        StringBuilder sb = new StringBuilder();
        for (String str : strArr) {
            if (sb.length() != 0) {
                sb.append(this._profile.getPathSeparator());
            }
            sb.append(str);
        }
        return (Profile.Section) this._profile.get(childName(sb.toString()));
    }

    public void removeChild(String str) {
        this._profile.remove(childName(str));
    }

    /* access modifiers changed from: package-private */
    public boolean isPropertyFirstUpper() {
        return this._profile.isPropertyFirstUpper();
    }

    /* access modifiers changed from: package-private */
    public void resolve(StringBuilder sb) {
        this._profile.resolve(sb, this);
    }

    private String childName(String str) {
        return this._name + this._profile.getPathSeparator() + str;
    }

    private Pattern newChildPattern(String str) {
        return Pattern.compile('^' + Pattern.quote(str) + '\\' + this._profile.getPathSeparator() + "[^" + '\\' + this._profile.getPathSeparator() + "]+$");
    }
}
