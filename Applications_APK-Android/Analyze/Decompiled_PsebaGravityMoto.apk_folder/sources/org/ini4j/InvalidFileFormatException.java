package org.ini4j;

import java.io.IOException;

public class InvalidFileFormatException extends IOException {
    private static final long serialVersionUID = -4354616830804732309L;

    public InvalidFileFormatException(String str) {
        super(str);
    }
}
