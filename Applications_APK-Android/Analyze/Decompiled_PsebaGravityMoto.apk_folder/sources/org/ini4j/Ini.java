package org.ini4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import org.ini4j.Profile;
import org.ini4j.spi.IniBuilder;
import org.ini4j.spi.IniFormatter;
import org.ini4j.spi.IniHandler;
import org.ini4j.spi.IniParser;

public class Ini extends BasicProfile implements Persistable, Configurable {
    private static final long serialVersionUID = -6029486578113700585L;
    private Config _config;
    private File _file;

    public Ini() {
        this._config = Config.getGlobal();
    }

    public Ini(Reader reader) throws IOException, InvalidFileFormatException {
        this();
        load(reader);
    }

    public Ini(InputStream inputStream) throws IOException, InvalidFileFormatException {
        this();
        load(inputStream);
    }

    public Ini(URL url) throws IOException, InvalidFileFormatException {
        this();
        load(url);
    }

    public Ini(File file) throws IOException, InvalidFileFormatException {
        this();
        this._file = file;
        load();
    }

    public Config getConfig() {
        return this._config;
    }

    public void setConfig(Config config) {
        this._config = config;
    }

    public File getFile() {
        return this._file;
    }

    public void setFile(File file) {
        this._file = file;
    }

    public void load() throws IOException, InvalidFileFormatException {
        File file = this._file;
        if (file != null) {
            load(file);
            return;
        }
        throw new FileNotFoundException();
    }

    public void load(InputStream inputStream) throws IOException, InvalidFileFormatException {
        load(new InputStreamReader(inputStream, getConfig().getFileEncoding()));
    }

    public void load(Reader reader) throws IOException, InvalidFileFormatException {
        IniParser.newInstance(getConfig()).parse(reader, newBuilder());
    }

    public void load(File file) throws IOException, InvalidFileFormatException {
        load(file.toURI().toURL());
    }

    public void load(URL url) throws IOException, InvalidFileFormatException {
        IniParser.newInstance(getConfig()).parse(url, newBuilder());
    }

    public void store() throws IOException {
        File file = this._file;
        if (file != null) {
            store(file);
            return;
        }
        throw new FileNotFoundException();
    }

    public void store(OutputStream outputStream) throws IOException {
        store(new OutputStreamWriter(outputStream, getConfig().getFileEncoding()));
    }

    public void store(Writer writer) throws IOException {
        store(IniFormatter.newInstance(writer, getConfig()));
    }

    public void store(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        store(fileOutputStream);
        fileOutputStream.close();
    }

    /* access modifiers changed from: protected */
    public IniHandler newBuilder() {
        return IniBuilder.newInstance(this);
    }

    /* access modifiers changed from: protected */
    public void store(IniHandler iniHandler, Profile.Section section) {
        if (getConfig().isEmptySection() || section.size() != 0) {
            super.store(iniHandler, section);
        }
    }

    /* access modifiers changed from: protected */
    public void store(IniHandler iniHandler, Profile.Section section, String str, int i) {
        if (getConfig().isMultiOption() || i == section.length(str) - 1) {
            super.store(iniHandler, section, str, i);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isTreeMode() {
        return getConfig().isTree();
    }

    /* access modifiers changed from: package-private */
    public char getPathSeparator() {
        return getConfig().getPathSeparator();
    }

    /* access modifiers changed from: package-private */
    public boolean isPropertyFirstUpper() {
        return getConfig().isPropertyFirstUpper();
    }
}
