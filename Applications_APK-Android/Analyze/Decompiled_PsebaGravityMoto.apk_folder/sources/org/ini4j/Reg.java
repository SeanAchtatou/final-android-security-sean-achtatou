package org.ini4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import org.ini4j.spi.IniFormatter;
import org.ini4j.spi.IniHandler;
import org.ini4j.spi.IniParser;
import org.ini4j.spi.RegBuilder;

public class Reg extends BasicRegistry implements Registry, Persistable, Configurable {
    private static final char CR = '\r';
    protected static final String DEFAULT_SUFFIX = ".reg";
    private static final char LF = '\n';
    private static final String PROP_OS_NAME = "os.name";
    private static final int STDERR_BUFF_SIZE = 8192;
    protected static final String TMP_PREFIX = "reg-";
    private static final boolean WINDOWS = Config.getSystemProperty(PROP_OS_NAME, "Unknown").startsWith("Windows");
    private static final long serialVersionUID = -1485602876922985912L;
    private Config _config;
    private File _file;

    public Reg() {
        Config clone = Config.getGlobal().clone();
        clone.setEscape(false);
        clone.setGlobalSection(false);
        clone.setEmptyOption(true);
        clone.setMultiOption(true);
        clone.setStrictOperator(true);
        clone.setEmptySection(true);
        clone.setPathSeparator('\\');
        clone.setFileEncoding(FILE_ENCODING);
        clone.setLineSeparator(Registry.LINE_SEPARATOR);
        this._config = clone;
    }

    public Reg(String str) throws IOException {
        this();
        read(str);
    }

    public Reg(File file) throws IOException, InvalidFileFormatException {
        this();
        this._file = file;
        load();
    }

    public Reg(URL url) throws IOException, InvalidFileFormatException {
        this();
        load(url);
    }

    public Reg(InputStream inputStream) throws IOException, InvalidFileFormatException {
        this();
        load(inputStream);
    }

    public Reg(Reader reader) throws IOException, InvalidFileFormatException {
        this();
        load(reader);
    }

    public static boolean isWindows() {
        return WINDOWS;
    }

    public Config getConfig() {
        return this._config;
    }

    public void setConfig(Config config) {
        this._config = config;
    }

    public File getFile() {
        return this._file;
    }

    public void setFile(File file) {
        this._file = file;
    }

    public void load() throws IOException, InvalidFileFormatException {
        File file = this._file;
        if (file != null) {
            load(file);
            return;
        }
        throw new FileNotFoundException();
    }

    public void load(InputStream inputStream) throws IOException, InvalidFileFormatException {
        load(new InputStreamReader(inputStream, getConfig().getFileEncoding()));
    }

    public void load(URL url) throws IOException, InvalidFileFormatException {
        load(new InputStreamReader(url.openStream(), getConfig().getFileEncoding()));
    }

    public void load(Reader reader) throws IOException, InvalidFileFormatException {
        StringBuilder sb = new StringBuilder();
        int read = reader.read();
        int i = 2;
        while (read != -1) {
            if (read == 10) {
                i--;
                if (i == 0) {
                    break;
                }
            } else if (!(read == 13 || i == 1)) {
                sb.append((char) read);
            }
            read = reader.read();
        }
        if (sb.length() == 0) {
            throw new InvalidFileFormatException("Missing version header");
        } else if (sb.toString().equals(getVersion())) {
            IniParser.newInstance(getConfig()).parse(reader, newBuilder());
        } else {
            throw new InvalidFileFormatException("Unsupported version: " + sb.toString());
        }
    }

    public void load(File file) throws IOException, InvalidFileFormatException {
        load(file.toURI().toURL());
    }

    public void read(String str) throws IOException {
        File createTempFile = createTempFile();
        try {
            regExport(str, createTempFile);
            load(createTempFile);
        } finally {
            createTempFile.delete();
        }
    }

    public void store() throws IOException {
        File file = this._file;
        if (file != null) {
            store(file);
            return;
        }
        throw new FileNotFoundException();
    }

    public void store(OutputStream outputStream) throws IOException {
        store(new OutputStreamWriter(outputStream, getConfig().getFileEncoding()));
    }

    public void store(Writer writer) throws IOException {
        writer.write(getVersion());
        writer.write(getConfig().getLineSeparator());
        writer.write(getConfig().getLineSeparator());
        store(IniFormatter.newInstance(writer, getConfig()));
    }

    public void store(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        store(fileOutputStream);
        fileOutputStream.close();
    }

    public void write() throws IOException {
        File createTempFile = createTempFile();
        try {
            store(createTempFile);
            regImport(createTempFile);
        } finally {
            createTempFile.delete();
        }
    }

    /* access modifiers changed from: protected */
    public IniHandler newBuilder() {
        return RegBuilder.newInstance(this);
    }

    /* access modifiers changed from: package-private */
    public boolean isTreeMode() {
        return getConfig().isTree();
    }

    /* access modifiers changed from: package-private */
    public char getPathSeparator() {
        return getConfig().getPathSeparator();
    }

    /* access modifiers changed from: package-private */
    public boolean isPropertyFirstUpper() {
        return getConfig().isPropertyFirstUpper();
    }

    /* access modifiers changed from: package-private */
    public void exec(String[] strArr) throws IOException {
        Process exec = Runtime.getRuntime().exec(strArr);
        try {
            if (exec.waitFor() != 0) {
                InputStreamReader inputStreamReader = new InputStreamReader(exec.getErrorStream());
                char[] cArr = new char[8192];
                int read = inputStreamReader.read(cArr);
                inputStreamReader.close();
                throw new IOException(new String(cArr, 0, read).trim());
            }
        } catch (InterruptedException e) {
            throw ((IOException) new InterruptedIOException().initCause(e));
        }
    }

    private File createTempFile() throws IOException {
        File createTempFile = File.createTempFile(TMP_PREFIX, DEFAULT_SUFFIX);
        createTempFile.deleteOnExit();
        return createTempFile;
    }

    private void regExport(String str, File file) throws IOException {
        requireWindows();
        exec(new String[]{"cmd", "/c", "reg", "export", str, file.getAbsolutePath()});
    }

    private void regImport(File file) throws IOException {
        requireWindows();
        exec(new String[]{"cmd", "/c", "reg", "import", file.getAbsolutePath()});
    }

    private void requireWindows() {
        if (!WINDOWS) {
            throw new UnsupportedOperationException("Unsupported operating system or runtime environment");
        }
    }
}
