package org.ini4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BasicMultiMap<K, V> implements MultiMap<K, V>, Serializable {
    private static final long serialVersionUID = 4716749660560043989L;
    private final Map<K, List<V>> _impl;

    public BasicMultiMap() {
        this(new LinkedHashMap());
    }

    public BasicMultiMap(Map<K, List<V>> map) {
        this._impl = map;
    }

    public List<V> getAll(Object obj) {
        return this._impl.get(obj);
    }

    public boolean isEmpty() {
        return this._impl.isEmpty();
    }

    public void add(K k, V v) {
        getList(k, true).add(v);
    }

    public void add(K k, V v, int i) {
        getList(k, true).add(i, v);
    }

    public void clear() {
        this._impl.clear();
    }

    public boolean containsKey(Object obj) {
        return this._impl.containsKey(obj);
    }

    public boolean containsValue(Object obj) {
        for (List<V> contains : this._impl.values()) {
            if (contains.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        HashSet hashSet = new HashSet();
        for (Object shadowEntry : keySet()) {
            hashSet.add(new ShadowEntry(shadowEntry));
        }
        return hashSet;
    }

    public V get(Object obj) {
        List list = getList(obj, false);
        if (list == null) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    public V get(Object obj, int i) {
        List list = getList(obj, false);
        if (list == null) {
            return null;
        }
        return list.get(i);
    }

    public Set<K> keySet() {
        return this._impl.keySet();
    }

    public int length(Object obj) {
        List list = getList(obj, false);
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public V put(K k, V v) {
        List list = getList(k, true);
        if (!list.isEmpty()) {
            return list.set(list.size() - 1, v);
        }
        list.add(v);
        return null;
    }

    public V put(Object obj, Object obj2, int i) {
        return getList(obj, false).set(i, obj2);
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        if (map instanceof MultiMap) {
            MultiMap multiMap = (MultiMap) map;
            for (Object next : multiMap.keySet()) {
                putAll(next, multiMap.getAll(next));
            }
            return;
        }
        for (Object next2 : map.keySet()) {
            put(next2, map.get(next2));
        }
    }

    public List<V> putAll(K k, List<V> list) {
        List<V> list2 = this._impl.get(k);
        this._impl.put(k, new ArrayList(list));
        return list2;
    }

    public V remove(Object obj) {
        List remove = this._impl.remove(obj);
        if (remove == null) {
            return null;
        }
        return remove.get(0);
    }

    public V remove(Object obj, int i) {
        List list = getList(obj, false);
        if (list == null) {
            return null;
        }
        V remove = list.remove(i);
        if (!list.isEmpty()) {
            return remove;
        }
        this._impl.remove(obj);
        return remove;
    }

    public int size() {
        return this._impl.size();
    }

    public String toString() {
        return this._impl.toString();
    }

    public Collection<V> values() {
        ArrayList arrayList = new ArrayList(this._impl.size());
        for (List<V> addAll : this._impl.values()) {
            arrayList.addAll(addAll);
        }
        return arrayList;
    }

    private List<V> getList(Object obj, boolean z) {
        List<V> list = this._impl.get(obj);
        if (list != null || !z) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        this._impl.put(obj, arrayList);
        return arrayList;
    }

    class ShadowEntry implements Map.Entry<K, V> {
        private final K _key;

        ShadowEntry(K k) {
            this._key = k;
        }

        public K getKey() {
            return this._key;
        }

        public V getValue() {
            return BasicMultiMap.this.get(this._key);
        }

        public V setValue(V v) {
            return BasicMultiMap.this.put(this._key, v);
        }
    }
}
