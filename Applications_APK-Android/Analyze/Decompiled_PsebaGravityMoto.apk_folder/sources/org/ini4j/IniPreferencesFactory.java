package org.ini4j;

import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

public class IniPreferencesFactory implements PreferencesFactory {
    public static final String KEY_SYSTEM = "org.ini4j.prefs.system";
    public static final String KEY_USER = "org.ini4j.prefs.user";
    public static final String PROPERTIES = "ini4j.properties";
    private Preferences _system;
    private Preferences _user;

    public synchronized Preferences systemRoot() {
        if (this._system == null) {
            this._system = newIniPreferences(KEY_SYSTEM);
        }
        return this._system;
    }

    public synchronized Preferences userRoot() {
        if (this._user == null) {
            this._user = newIniPreferences(KEY_USER);
        }
        return this._user;
    }

    /* access modifiers changed from: package-private */
    public String getIniLocation(String str) {
        String systemProperty = Config.getSystemProperty(str);
        if (systemProperty != null) {
            return systemProperty;
        }
        try {
            Properties properties = new Properties();
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(PROPERTIES));
            return properties.getProperty(str);
        } catch (Exception unused) {
            return systemProperty;
        }
    }

    /* access modifiers changed from: package-private */
    public URL getResource(String str) throws IllegalArgumentException {
        try {
            URI uri = new URI(str);
            if (uri.getScheme() == null) {
                return Thread.currentThread().getContextClassLoader().getResource(str);
            }
            return uri.toURL();
        } catch (Exception e) {
            throw ((IllegalArgumentException) new IllegalArgumentException().initCause(e));
        }
    }

    /* access modifiers changed from: package-private */
    public InputStream getResourceAsStream(String str) throws IllegalArgumentException {
        try {
            return getResource(str).openStream();
        } catch (Exception e) {
            throw ((IllegalArgumentException) new IllegalArgumentException().initCause(e));
        }
    }

    /* access modifiers changed from: package-private */
    public Preferences newIniPreferences(String str) {
        Ini ini = new Ini();
        String iniLocation = getIniLocation(str);
        if (iniLocation != null) {
            try {
                ini.load(getResourceAsStream(iniLocation));
            } catch (Exception e) {
                throw ((IllegalArgumentException) new IllegalArgumentException().initCause(e));
            }
        }
        return new IniPreferences(ini);
    }
}
