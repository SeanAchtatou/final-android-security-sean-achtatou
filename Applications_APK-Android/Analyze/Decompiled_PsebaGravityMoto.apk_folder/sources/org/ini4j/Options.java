package org.ini4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import org.ini4j.spi.OptionsBuilder;
import org.ini4j.spi.OptionsFormatter;
import org.ini4j.spi.OptionsHandler;
import org.ini4j.spi.OptionsParser;

public class Options extends BasicOptionMap implements Persistable, Configurable {
    private static final long serialVersionUID = -1119753444859181822L;
    private String _comment;
    private Config _config;
    private File _file;

    public Options() {
        this._config = Config.getGlobal().clone();
        this._config.setEmptyOption(true);
    }

    public Options(Reader reader) throws IOException, InvalidFileFormatException {
        this();
        load(reader);
    }

    public Options(InputStream inputStream) throws IOException, InvalidFileFormatException {
        this();
        load(inputStream);
    }

    public Options(URL url) throws IOException, InvalidFileFormatException {
        this();
        load(url);
    }

    public Options(File file) throws IOException, InvalidFileFormatException {
        this();
        this._file = file;
        load();
    }

    public String getComment() {
        return this._comment;
    }

    public void setComment(String str) {
        this._comment = str;
    }

    public Config getConfig() {
        return this._config;
    }

    public void setConfig(Config config) {
        this._config = config;
    }

    public File getFile() {
        return this._file;
    }

    public void setFile(File file) {
        this._file = file;
    }

    public void load() throws IOException, InvalidFileFormatException {
        File file = this._file;
        if (file != null) {
            load(file);
            return;
        }
        throw new FileNotFoundException();
    }

    public void load(InputStream inputStream) throws IOException, InvalidFileFormatException {
        load(new InputStreamReader(inputStream, getConfig().getFileEncoding()));
    }

    public void load(Reader reader) throws IOException, InvalidFileFormatException {
        OptionsParser.newInstance(getConfig()).parse(reader, newBuilder());
    }

    public void load(URL url) throws IOException, InvalidFileFormatException {
        OptionsParser.newInstance(getConfig()).parse(url, newBuilder());
    }

    public void load(File file) throws IOException, InvalidFileFormatException {
        load(file.toURI().toURL());
    }

    public void store() throws IOException {
        File file = this._file;
        if (file != null) {
            store(file);
            return;
        }
        throw new FileNotFoundException();
    }

    public void store(OutputStream outputStream) throws IOException {
        store(new OutputStreamWriter(outputStream, getConfig().getFileEncoding()));
    }

    public void store(Writer writer) throws IOException {
        store(OptionsFormatter.newInstance(writer, getConfig()));
    }

    public void store(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        store(fileOutputStream);
        fileOutputStream.close();
    }

    /* access modifiers changed from: protected */
    public OptionsHandler newBuilder() {
        return OptionsBuilder.newInstance(this);
    }

    /* access modifiers changed from: protected */
    public void store(OptionsHandler optionsHandler) throws IOException {
        optionsHandler.startOptions();
        storeComment(optionsHandler, this._comment);
        for (String str : keySet()) {
            storeComment(optionsHandler, getComment(str));
            int length = getConfig().isMultiOption() ? length(str) : 1;
            for (int i = 0; i < length; i++) {
                optionsHandler.handleOption(str, (String) get(str, i));
            }
        }
        optionsHandler.endOptions();
    }

    /* access modifiers changed from: package-private */
    public boolean isPropertyFirstUpper() {
        return getConfig().isPropertyFirstUpper();
    }

    private void storeComment(OptionsHandler optionsHandler, String str) {
        optionsHandler.handleComment(str);
    }
}
