package org.ini4j;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ini4j.Profile;
import org.ini4j.spi.IniHandler;

public class ConfigParser implements Serializable {
    private static final long serialVersionUID = 9118857036229164353L;
    private PyIni _ini;

    public ConfigParser() {
        this(Collections.EMPTY_MAP);
    }

    public ConfigParser(Map<String, String> map) {
        this._ini = new PyIni(map);
    }

    public boolean getBoolean(String str, String str2) throws NoSectionException, NoOptionException, InterpolationException {
        String str3 = get(str, str2);
        if ("1".equalsIgnoreCase(str3) || "yes".equalsIgnoreCase(str3) || "true".equalsIgnoreCase(str3) || "on".equalsIgnoreCase(str3)) {
            return true;
        }
        if ("0".equalsIgnoreCase(str3) || "no".equalsIgnoreCase(str3) || "false".equalsIgnoreCase(str3) || "off".equalsIgnoreCase(str3)) {
            return false;
        }
        throw new IllegalArgumentException(str3);
    }

    public double getDouble(String str, String str2) throws NoSectionException, NoOptionException, InterpolationException {
        return Double.parseDouble(get(str, str2));
    }

    public float getFloat(String str, String str2) throws NoSectionException, NoOptionException, InterpolationException {
        return Float.parseFloat(get(str, str2));
    }

    public int getInt(String str, String str2) throws NoSectionException, NoOptionException, InterpolationException {
        return Integer.parseInt(get(str, str2));
    }

    public long getLong(String str, String str2) throws NoSectionException, NoOptionException, InterpolationException {
        return Long.parseLong(get(str, str2));
    }

    public void addSection(String str) throws DuplicateSectionException {
        if (this._ini.containsKey(str)) {
            throw new DuplicateSectionException(str);
        } else if (!"DEFAULT".equalsIgnoreCase(str)) {
            this._ini.add(str);
        } else {
            throw new IllegalArgumentException(str);
        }
    }

    public Map<String, String> defaults() {
        return this._ini.getDefaults();
    }

    public String get(String str, String str2) throws NoSectionException, NoOptionException, InterpolationException {
        return get(str, str2, false, Collections.EMPTY_MAP);
    }

    public String get(String str, String str2, boolean z) throws NoSectionException, NoOptionException, InterpolationException {
        return get(str, str2, z, Collections.EMPTY_MAP);
    }

    public String get(String str, String str2, boolean z, Map<String, String> map) throws NoSectionException, NoOptionException, InterpolationException {
        String requireOption = requireOption(str, str2);
        return (z || requireOption == null || requireOption.indexOf(37) < 0) ? requireOption : this._ini.fetch(str, str2, map);
    }

    public boolean hasOption(String str, String str2) {
        Profile.Section section = (Profile.Section) this._ini.get(str);
        return section != null && section.containsKey(str2);
    }

    public boolean hasSection(String str) {
        return this._ini.containsKey(str);
    }

    public List<Map.Entry<String, String>> items(String str) throws NoSectionException, InterpolationMissingOptionException {
        return items(str, false, Collections.EMPTY_MAP);
    }

    public List<Map.Entry<String, String>> items(String str, boolean z) throws NoSectionException, InterpolationMissingOptionException {
        return items(str, z, Collections.EMPTY_MAP);
    }

    public List<Map.Entry<String, String>> items(String str, boolean z, Map<String, String> map) throws NoSectionException, InterpolationMissingOptionException {
        HashMap hashMap;
        Profile.Section requireSection = requireSection(str);
        if (z) {
            hashMap = new HashMap(requireSection);
        } else {
            hashMap = new HashMap();
            for (String str2 : requireSection.keySet()) {
                hashMap.put(str2, this._ini.fetch(requireSection, str2, map));
            }
        }
        return new ArrayList(hashMap.entrySet());
    }

    public List<String> options(String str) throws NoSectionException {
        requireSection(str);
        return new ArrayList(((Profile.Section) this._ini.get(str)).keySet());
    }

    public void read(String... strArr) throws IOException, ParsingException {
        for (String file : strArr) {
            read(new File(file));
        }
    }

    public void read(Reader reader) throws IOException, ParsingException {
        try {
            this._ini.load(reader);
        } catch (InvalidFileFormatException e) {
            throw new ParsingException(e);
        }
    }

    public void read(URL url) throws IOException, ParsingException {
        try {
            this._ini.load(url);
        } catch (InvalidFileFormatException e) {
            throw new ParsingException(e);
        }
    }

    public void read(File file) throws IOException, ParsingException {
        try {
            this._ini.load(new FileReader(file));
        } catch (InvalidFileFormatException e) {
            throw new ParsingException(e);
        }
    }

    public void read(InputStream inputStream) throws IOException, ParsingException {
        try {
            this._ini.load(inputStream);
        } catch (InvalidFileFormatException e) {
            throw new ParsingException(e);
        }
    }

    public boolean removeOption(String str, String str2) throws NoSectionException {
        Profile.Section requireSection = requireSection(str);
        boolean containsKey = requireSection.containsKey(str2);
        requireSection.remove(str2);
        return containsKey;
    }

    public boolean removeSection(String str) {
        boolean containsKey = this._ini.containsKey(str);
        this._ini.remove(str);
        return containsKey;
    }

    public List<String> sections() {
        return new ArrayList(this._ini.keySet());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object}
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
      SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object} */
    public void set(String str, String str2, Object obj) throws NoSectionException {
        Profile.Section requireSection = requireSection(str);
        if (obj == null) {
            requireSection.remove(str2);
        } else {
            requireSection.put((Object) str2, (Object) obj.toString());
        }
    }

    public void write(Writer writer) throws IOException {
        this._ini.store(writer);
    }

    public void write(OutputStream outputStream) throws IOException {
        this._ini.store(outputStream);
    }

    public void write(File file) throws IOException {
        this._ini.store(new FileWriter(file));
    }

    /* access modifiers changed from: protected */
    public Ini getIni() {
        return this._ini;
    }

    private String requireOption(String str, String str2) throws NoSectionException, NoOptionException {
        String str3 = (String) requireSection(str).get(str2);
        if (str3 != null) {
            return str3;
        }
        throw new NoOptionException(str2);
    }

    private Profile.Section requireSection(String str) throws NoSectionException {
        Profile.Section section = (Profile.Section) this._ini.get(str);
        if (section != null) {
            return section;
        }
        throw new NoSectionException(str);
    }

    public static class ConfigParserException extends Exception {
        private static final long serialVersionUID = -6845546313519392093L;

        public ConfigParserException(String str) {
            super(str);
        }
    }

    public static final class DuplicateSectionException extends ConfigParserException {
        private static final long serialVersionUID = -5244008445735700699L;

        private DuplicateSectionException(String str) {
            super(str);
        }
    }

    public static class InterpolationException extends ConfigParserException {
        private static final long serialVersionUID = 8924443303158546939L;

        protected InterpolationException(String str) {
            super(str);
        }
    }

    public static final class InterpolationMissingOptionException extends InterpolationException {
        private static final long serialVersionUID = 2903136975820447879L;

        private InterpolationMissingOptionException(String str) {
            super(str);
        }
    }

    public static final class NoOptionException extends ConfigParserException {
        private static final long serialVersionUID = 8460082078809425858L;

        private NoOptionException(String str) {
            super(str);
        }
    }

    public static final class NoSectionException extends ConfigParserException {
        private static final long serialVersionUID = 8553627727493146118L;

        private NoSectionException(String str) {
            super(str);
        }
    }

    public static final class ParsingException extends IOException {
        private static final long serialVersionUID = -5395990242007205038L;

        private ParsingException(Throwable th) {
            super(th.getMessage());
            initCause(th);
        }
    }

    static class PyIni extends Ini {
        protected static final String DEFAULT_SECTION_NAME = "DEFAULT";
        private static final Pattern EXPRESSION = Pattern.compile("(?<!\\\\)\\%\\(([^\\)]+)\\)");
        private static final int G_OPTION = 1;
        private static final char SUBST_CHAR = '%';
        private static final long serialVersionUID = -7152857626328996122L;
        private Profile.Section _defaultSection;
        private final Map<String, String> _defaults;

        public void setConfig(Config config) {
        }

        public PyIni(Map<String, String> map) {
            this._defaults = map;
            Config clone = getConfig().clone();
            clone.setEscape(false);
            clone.setMultiOption(false);
            clone.setMultiSection(false);
            clone.setLowerCaseOption(true);
            clone.setLowerCaseSection(true);
            super.setConfig(clone);
        }

        public Map<String, String> getDefaults() {
            return this._defaults;
        }

        public Profile.Section add(String str) {
            if (!DEFAULT_SECTION_NAME.equalsIgnoreCase(str)) {
                return super.add(str);
            }
            if (this._defaultSection == null) {
                this._defaultSection = newSection(str);
            }
            return this._defaultSection;
        }

        public String fetch(String str, String str2, Map<String, String> map) throws InterpolationMissingOptionException {
            return fetch((Profile.Section) get(str), str2, map);
        }

        /* access modifiers changed from: protected */
        public Profile.Section getDefaultSection() {
            return this._defaultSection;
        }

        /* access modifiers changed from: protected */
        public String fetch(Profile.Section section, String str, Map<String, String> map) throws InterpolationMissingOptionException {
            String str2 = (String) section.get(str);
            if (str2 == null || str2.indexOf(37) < 0) {
                return str2;
            }
            StringBuilder sb = new StringBuilder(str2);
            resolve(sb, section, map);
            return sb.toString();
        }

        /* access modifiers changed from: protected */
        public void resolve(StringBuilder sb, Profile.Section section, Map<String, String> map) throws InterpolationMissingOptionException {
            Profile.Section section2;
            Matcher matcher = EXPRESSION.matcher(sb);
            while (matcher.find()) {
                String group = matcher.group(1);
                String str = (String) section.get(group);
                if (str == null) {
                    str = map.get(group);
                }
                if (str == null) {
                    str = this._defaults.get(group);
                }
                if (str == null && (section2 = this._defaultSection) != null) {
                    str = (String) section2.get(group);
                }
                if (str != null) {
                    sb.replace(matcher.start(), matcher.end(), str);
                    matcher.reset(sb);
                } else {
                    throw new InterpolationMissingOptionException(group);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void store(IniHandler iniHandler) {
            iniHandler.startIni();
            Profile.Section section = this._defaultSection;
            if (section != null) {
                store(iniHandler, section);
            }
            for (Profile.Section store : values()) {
                store(iniHandler, store);
            }
            iniHandler.endIni();
        }

        /* access modifiers changed from: protected */
        public void store(IniHandler iniHandler, Profile.Section section) {
            iniHandler.startSection(section.getName());
            for (String str : section.keySet()) {
                iniHandler.handleOption(str, (String) section.get(str));
            }
            iniHandler.endSection();
        }
    }
}
