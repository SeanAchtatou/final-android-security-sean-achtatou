package org.ini4j;

import java.util.Map;

public interface CommentedMap<K, V> extends Map<K, V> {
    String getComment(Object obj);

    String putComment(K k, String str);

    String removeComment(Object obj);
}
