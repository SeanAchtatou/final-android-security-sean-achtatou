package org.ini4j.spi;

public interface IniHandler extends HandlerBase {
    void endIni();

    void endSection();

    void handleComment(String str);

    void handleOption(String str, String str2);

    void startIni();

    void startSection(String str);
}
