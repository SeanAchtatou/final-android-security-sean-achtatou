package org.ini4j.spi;

import java.beans.Introspector;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public abstract class AbstractBeanInvocationHandler implements InvocationHandler {
    private static final String ADD_PREFIX = "add";
    private static final String HAS_PREFIX = "has";
    private static final String PROPERTY_CHANGE_LISTENER = "PropertyChangeListener";
    private static final String READ_BOOLEAN_PREFIX = "is";
    private static final String READ_PREFIX = "get";
    private static final String REMOVE_PREFIX = "remove";
    private static final String VETOABLE_CHANGE_LISTENER = "VetoableChangeListener";
    private static final String WRITE_PREFIX = "set";
    private PropertyChangeSupport _pcSupport;
    private Object _proxy;
    private VetoableChangeSupport _vcSupport;

    /* access modifiers changed from: protected */
    public abstract Object getPropertySpi(String str, Class<?> cls);

    /* access modifiers changed from: protected */
    public abstract boolean hasPropertySpi(String str);

    /* access modifiers changed from: protected */
    public abstract void setPropertySpi(String str, Object obj, Class<?> cls);

    private enum Prefix {
        READ(AbstractBeanInvocationHandler.READ_PREFIX),
        READ_BOOLEAN(AbstractBeanInvocationHandler.READ_BOOLEAN_PREFIX),
        WRITE(AbstractBeanInvocationHandler.WRITE_PREFIX),
        ADD_CHANGE("addPropertyChangeListener"),
        ADD_VETO("addVetoableChangeListener"),
        REMOVE_CHANGE("removePropertyChangeListener"),
        REMOVE_VETO("removeVetoableChangeListener"),
        HAS(AbstractBeanInvocationHandler.HAS_PREFIX);
        
        private int _len;
        private String _value;

        private Prefix(String str) {
            this._value = str;
            this._len = str.length();
        }

        public static Prefix parse(String str) {
            for (Prefix prefix : values()) {
                if (str.startsWith(prefix.getValue())) {
                    return prefix;
                }
            }
            return null;
        }

        public String getTail(String str) {
            return Introspector.decapitalize(str.substring(this._len));
        }

        public String getValue() {
            return this._value;
        }
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws PropertyVetoException {
        Prefix parse = Prefix.parse(method.getName());
        if (parse != null) {
            String tail = parse.getTail(method.getName());
            updateProxy(obj);
            switch (parse) {
                case READ:
                    return getProperty(parse.getTail(method.getName()), method.getReturnType());
                case READ_BOOLEAN:
                    return getProperty(parse.getTail(method.getName()), method.getReturnType());
                case WRITE:
                    setProperty(tail, objArr[0], method.getParameterTypes()[0]);
                    break;
                case HAS:
                    return Boolean.valueOf(hasProperty(parse.getTail(method.getName())));
                case ADD_CHANGE:
                    addPropertyChangeListener((String) objArr[0], (PropertyChangeListener) objArr[1]);
                    break;
                case ADD_VETO:
                    addVetoableChangeListener((String) objArr[0], (VetoableChangeListener) objArr[1]);
                    break;
                case REMOVE_CHANGE:
                    removePropertyChangeListener((String) objArr[0], (PropertyChangeListener) objArr[1]);
                    break;
                case REMOVE_VETO:
                    removeVetoableChangeListener((String) objArr[0], (VetoableChangeListener) objArr[1]);
                    break;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r5 = zero(r6);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0056 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Object getProperty(java.lang.String r5, java.lang.Class<?> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.lang.Object r5 = r4.getPropertySpi(r5, r6)     // Catch:{ Exception -> 0x0056 }
            if (r5 != 0) goto L_0x000c
            java.lang.Object r5 = r4.zero(r6)     // Catch:{ Exception -> 0x0056 }
            goto L_0x005a
        L_0x000c:
            boolean r0 = r6.isArray()     // Catch:{ Exception -> 0x0056 }
            if (r0 == 0) goto L_0x0041
            boolean r0 = r5 instanceof java.lang.String[]     // Catch:{ Exception -> 0x0056 }
            if (r0 == 0) goto L_0x0041
            java.lang.Class<java.lang.String[]> r0 = java.lang.String[].class
            boolean r0 = r6.equals(r0)     // Catch:{ Exception -> 0x0056 }
            if (r0 != 0) goto L_0x0041
            java.lang.String[] r5 = (java.lang.String[]) r5     // Catch:{ Exception -> 0x0056 }
            java.lang.String[] r5 = (java.lang.String[]) r5     // Catch:{ Exception -> 0x0056 }
            java.lang.Class r0 = r6.getComponentType()     // Catch:{ Exception -> 0x0056 }
            int r1 = r5.length     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r0, r1)     // Catch:{ Exception -> 0x0056 }
            r1 = 0
        L_0x002c:
            int r2 = r5.length     // Catch:{ Exception -> 0x0056 }
            if (r1 >= r2) goto L_0x003f
            r2 = r5[r1]     // Catch:{ Exception -> 0x0056 }
            java.lang.Class r3 = r6.getComponentType()     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r2 = r4.parse(r2, r3)     // Catch:{ Exception -> 0x0056 }
            java.lang.reflect.Array.set(r0, r1, r2)     // Catch:{ Exception -> 0x0056 }
            int r1 = r1 + 1
            goto L_0x002c
        L_0x003f:
            r5 = r0
            goto L_0x005a
        L_0x0041:
            boolean r0 = r5 instanceof java.lang.String     // Catch:{ Exception -> 0x0056 }
            if (r0 == 0) goto L_0x005a
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            boolean r0 = r6.equals(r0)     // Catch:{ Exception -> 0x0056 }
            if (r0 != 0) goto L_0x005a
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r5 = r4.parse(r5, r6)     // Catch:{ Exception -> 0x0056 }
            goto L_0x005a
        L_0x0054:
            r5 = move-exception
            goto L_0x005c
        L_0x0056:
            java.lang.Object r5 = r4.zero(r6)     // Catch:{ all -> 0x0054 }
        L_0x005a:
            monitor-exit(r4)
            return r5
        L_0x005c:
            monitor-exit(r4)
            goto L_0x005f
        L_0x005e:
            throw r5
        L_0x005f:
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ini4j.spi.AbstractBeanInvocationHandler.getProperty(java.lang.String, java.lang.Class):java.lang.Object");
    }

    /* access modifiers changed from: protected */
    public synchronized void setProperty(String str, Object obj, Class<?> cls) throws PropertyVetoException {
        boolean z = true;
        boolean z2 = this._pcSupport != null && this._pcSupport.hasListeners(str);
        if (this._vcSupport == null || !this._vcSupport.hasListeners(str)) {
            z = false;
        }
        Object obj2 = null;
        Object obj3 = (obj == null || !cls.equals(String.class) || (obj instanceof String)) ? obj : obj.toString();
        if (z2 || z) {
            obj2 = getProperty(str, cls);
        }
        if (z) {
            fireVetoableChange(str, obj2, obj);
        }
        setPropertySpi(str, obj3, cls);
        if (z2) {
            firePropertyChange(str, obj2, obj);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized Object getProxy() {
        return this._proxy;
    }

    /* access modifiers changed from: protected */
    public synchronized void addPropertyChangeListener(String str, PropertyChangeListener propertyChangeListener) {
        if (this._pcSupport == null) {
            this._pcSupport = new PropertyChangeSupport(this._proxy);
        }
        this._pcSupport.addPropertyChangeListener(str, propertyChangeListener);
    }

    /* access modifiers changed from: protected */
    public synchronized void addVetoableChangeListener(String str, VetoableChangeListener vetoableChangeListener) {
        if (this._vcSupport == null) {
            this._vcSupport = new VetoableChangeSupport(this._proxy);
        }
        this._vcSupport.addVetoableChangeListener(str, vetoableChangeListener);
    }

    /* access modifiers changed from: protected */
    public synchronized void firePropertyChange(String str, Object obj, Object obj2) {
        if (this._pcSupport != null) {
            this._pcSupport.firePropertyChange(str, obj, obj2);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void fireVetoableChange(String str, Object obj, Object obj2) throws PropertyVetoException {
        if (this._vcSupport != null) {
            this._vcSupport.fireVetoableChange(str, obj, obj2);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0009, code lost:
        r1 = false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean hasProperty(java.lang.String r1) {
        /*
            r0 = this;
            monitor-enter(r0)
            boolean r1 = r0.hasPropertySpi(r1)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            goto L_0x000a
        L_0x0006:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0009:
            r1 = 0
        L_0x000a:
            monitor-exit(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ini4j.spi.AbstractBeanInvocationHandler.hasProperty(java.lang.String):boolean");
    }

    /* access modifiers changed from: protected */
    public Object parse(String str, Class<?> cls) throws IllegalArgumentException {
        return BeanTool.getInstance().parse(str, cls);
    }

    /* access modifiers changed from: protected */
    public synchronized void removePropertyChangeListener(String str, PropertyChangeListener propertyChangeListener) {
        if (this._pcSupport != null) {
            this._pcSupport.removePropertyChangeListener(str, propertyChangeListener);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void removeVetoableChangeListener(String str, VetoableChangeListener vetoableChangeListener) {
        if (this._vcSupport != null) {
            this._vcSupport.removeVetoableChangeListener(str, vetoableChangeListener);
        }
    }

    /* access modifiers changed from: protected */
    public Object zero(Class<?> cls) {
        return BeanTool.getInstance().zero(cls);
    }

    private synchronized void updateProxy(Object obj) {
        if (this._proxy == null) {
            this._proxy = obj;
        }
    }
}
