package org.ini4j.spi;

public interface OptionsHandler extends HandlerBase {
    void endOptions();

    void handleComment(String str);

    void handleOption(String str, String str2);

    void startOptions();
}
