package org.ini4j.spi;

import java.io.PrintWriter;
import java.io.Writer;
import org.ini4j.Config;

public class IniFormatter extends AbstractFormatter implements IniHandler {
    public void startIni() {
    }

    public /* bridge */ /* synthetic */ void handleComment(String str) {
        super.handleComment(str);
    }

    public /* bridge */ /* synthetic */ void handleOption(String str, String str2) {
        super.handleOption(str, str2);
    }

    public static IniFormatter newInstance(Writer writer, Config config) {
        IniFormatter newInstance = newInstance();
        newInstance.setOutput(writer instanceof PrintWriter ? (PrintWriter) writer : new PrintWriter(writer));
        newInstance.setConfig(config);
        return newInstance;
    }

    public void endIni() {
        getOutput().flush();
    }

    public void endSection() {
        getOutput().print(getConfig().getLineSeparator());
    }

    public void startSection(String str) {
        setHeader(false);
        if (!getConfig().isGlobalSection() || !str.equals(getConfig().getGlobalSectionName())) {
            getOutput().print('[');
            getOutput().print(escapeFilter(str));
            getOutput().print(']');
            getOutput().print(getConfig().getLineSeparator());
        }
    }

    private static IniFormatter newInstance() {
        return (IniFormatter) ServiceFinder.findService(IniFormatter.class);
    }
}
