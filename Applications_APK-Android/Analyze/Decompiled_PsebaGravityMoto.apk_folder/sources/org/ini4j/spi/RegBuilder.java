package org.ini4j.spi;

import org.ini4j.Config;
import org.ini4j.Profile;
import org.ini4j.Reg;
import org.ini4j.Registry;

public class RegBuilder extends AbstractProfileBuilder {
    private Reg _reg;

    public /* bridge */ /* synthetic */ void endIni() {
        super.endIni();
    }

    public /* bridge */ /* synthetic */ void endSection() {
        super.endSection();
    }

    public /* bridge */ /* synthetic */ void handleComment(String str) {
        super.handleComment(str);
    }

    public /* bridge */ /* synthetic */ void startIni() {
        super.startIni();
    }

    public /* bridge */ /* synthetic */ void startSection(String str) {
        super.startSection(str);
    }

    public static RegBuilder newInstance(Reg reg) {
        RegBuilder newInstance = newInstance();
        newInstance.setReg(reg);
        return newInstance;
    }

    public void setReg(Reg reg) {
        this._reg = reg;
    }

    public void handleOption(String str, String str2) {
        if (str.charAt(0) == '\"') {
            str = RegEscapeTool.getInstance().unquote(str);
        }
        TypeValuesPair decode = RegEscapeTool.getInstance().decode(str2);
        if (decode.getType() != Registry.Type.REG_SZ) {
            ((Registry.Key) getCurrentSection()).putType(str, decode.getType());
        }
        for (String handleOption : decode.getValues()) {
            super.handleOption(str, handleOption);
        }
    }

    /* access modifiers changed from: package-private */
    public Config getConfig() {
        return this._reg.getConfig();
    }

    /* access modifiers changed from: package-private */
    public Profile getProfile() {
        return this._reg;
    }

    private static RegBuilder newInstance() {
        return (RegBuilder) ServiceFinder.findService(RegBuilder.class);
    }
}
