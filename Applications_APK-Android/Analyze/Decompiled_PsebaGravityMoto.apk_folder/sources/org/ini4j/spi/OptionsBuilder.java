package org.ini4j.spi;

import org.ini4j.Config;
import org.ini4j.Options;

public class OptionsBuilder implements OptionsHandler {
    private boolean _header;
    private String _lastComment;
    private Options _options;

    public static OptionsBuilder newInstance(Options options) {
        OptionsBuilder newInstance = newInstance();
        newInstance.setOptions(options);
        return newInstance;
    }

    public void setOptions(Options options) {
        this._options = options;
    }

    public void endOptions() {
        if (this._lastComment != null && this._header) {
            setHeaderComment();
        }
    }

    public void handleComment(String str) {
        if (this._lastComment != null && this._header) {
            setHeaderComment();
            this._header = false;
        }
        this._lastComment = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.BasicOptionMap.add(java.lang.String, java.lang.Object):void
      org.ini4j.OptionMap.add(java.lang.String, java.lang.Object):void
      org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.BasicOptionMap.put(java.lang.String, java.lang.Object):java.lang.String
      org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
      org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V */
    public void handleOption(String str, String str2) {
        if (getConfig().isMultiOption()) {
            this._options.add((Object) str, (Object) str2);
        } else {
            this._options.put((Object) str, (Object) str2);
        }
        if (this._lastComment != null) {
            if (this._header) {
                setHeaderComment();
            } else {
                putComment(str);
            }
            this._lastComment = null;
        }
        this._header = false;
    }

    public void startOptions() {
        if (getConfig().isHeaderComment()) {
            this._header = true;
        }
    }

    protected static OptionsBuilder newInstance() {
        return (OptionsBuilder) ServiceFinder.findService(OptionsBuilder.class);
    }

    private Config getConfig() {
        return this._options.getConfig();
    }

    private void setHeaderComment() {
        if (getConfig().isComment()) {
            this._options.setComment(this._lastComment);
        }
    }

    private void putComment(String str) {
        if (getConfig().isComment()) {
            this._options.putComment(str, this._lastComment);
        }
    }
}
