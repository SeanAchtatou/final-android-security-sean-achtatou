package org.ini4j.spi;

import org.ini4j.CommentedMap;
import org.ini4j.Config;
import org.ini4j.Profile;

abstract class AbstractProfileBuilder implements IniHandler {
    private Profile.Section _currentSection;
    private boolean _header;
    private String _lastComment;

    /* access modifiers changed from: package-private */
    public abstract Config getConfig();

    /* access modifiers changed from: package-private */
    public abstract Profile getProfile();

    AbstractProfileBuilder() {
    }

    public void endIni() {
        if (this._lastComment != null && this._header) {
            setHeaderComment();
        }
    }

    public void endSection() {
        this._currentSection = null;
    }

    public void handleComment(String str) {
        if (this._lastComment != null && this._header) {
            this._header = false;
            setHeaderComment();
        }
        this._lastComment = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.MultiMap.add(java.lang.Object, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.OptionMap.add(java.lang.String, java.lang.Object):void
      org.ini4j.MultiMap.add(java.lang.Object, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object}
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
      SimpleMethodDetails{org.ini4j.Profile.Section.put(java.lang.Object, java.lang.Object):java.lang.Object} */
    public void handleOption(String str, String str2) {
        this._header = false;
        if (getConfig().isMultiOption()) {
            this._currentSection.add((Object) str, (Object) str2);
        } else {
            this._currentSection.put((Object) str, (Object) str2);
        }
        if (this._lastComment != null) {
            putComment(this._currentSection, str);
            this._lastComment = null;
        }
    }

    public void startIni() {
        if (getConfig().isHeaderComment()) {
            this._header = true;
        }
    }

    public void startSection(String str) {
        if (getConfig().isMultiSection()) {
            this._currentSection = getProfile().add(str);
        } else {
            Profile.Section section = (Profile.Section) getProfile().get(str);
            if (section == null) {
                section = getProfile().add(str);
            }
            this._currentSection = section;
        }
        if (this._lastComment != null) {
            if (this._header) {
                setHeaderComment();
            } else {
                putComment(getProfile(), str);
            }
            this._lastComment = null;
        }
        this._header = false;
    }

    /* access modifiers changed from: package-private */
    public Profile.Section getCurrentSection() {
        return this._currentSection;
    }

    private void setHeaderComment() {
        if (getConfig().isComment()) {
            getProfile().setComment(this._lastComment);
        }
    }

    private void putComment(CommentedMap<String, ?> commentedMap, String str) {
        if (getConfig().isComment()) {
            commentedMap.putComment(str, this._lastComment);
        }
    }
}
