package org.ini4j.spi;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.protocol.HTTP;

final class ServiceFinder {
    private static final String SERVICES_PATH = "META-INF/services/";

    private ServiceFinder() {
    }

    static <T> T findService(Class<T> cls) {
        try {
            return cls.cast(findServiceClass(cls).newInstance());
        } catch (Exception e) {
            throw ((IllegalArgumentException) new IllegalArgumentException("Provider " + cls.getName() + " could not be instantiated: " + e).initCause(e));
        }
    }

    static <T> Class<? extends T> findServiceClass(Class<T> cls) throws IllegalArgumentException {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        String findServiceClassName = findServiceClassName(cls.getName());
        if (findServiceClassName == null) {
            return cls;
        }
        if (contextClassLoader != null) {
            return contextClassLoader.loadClass(findServiceClassName);
        }
        try {
            return Class.forName(findServiceClassName);
        } catch (ClassNotFoundException e) {
            throw ((IllegalArgumentException) new IllegalArgumentException("Provider " + findServiceClassName + " not found").initCause(e));
        }
    }

    static String findServiceClassName(String str) throws IllegalArgumentException {
        String str2 = null;
        try {
            String property = System.getProperty(str);
            if (property != null) {
                str2 = property;
            }
        } catch (SecurityException unused) {
        }
        if (str2 != null) {
            return str2;
        }
        return loadLine(SERVICES_PATH + str);
    }

    private static String loadLine(String str) {
        InputStream inputStream;
        try {
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            if (contextClassLoader == null) {
                inputStream = ClassLoader.getSystemResourceAsStream(str);
            } else {
                inputStream = contextClassLoader.getResourceAsStream(str);
            }
            if (inputStream == null) {
                return null;
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, HTTP.UTF_8));
            String readLine = bufferedReader.readLine();
            bufferedReader.close();
            if (readLine == null) {
                return null;
            }
            String trim = readLine.trim();
            if (trim.length() != 0) {
                return trim.split("\\s|#")[0];
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }
}
