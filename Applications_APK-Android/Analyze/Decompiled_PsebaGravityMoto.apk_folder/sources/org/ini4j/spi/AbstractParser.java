package org.ini4j.spi;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.Locale;
import org.ini4j.Config;
import org.ini4j.InvalidFileFormatException;

abstract class AbstractParser {
    private final String _comments;
    private Config _config = Config.getGlobal();
    private final String _operators;

    protected AbstractParser(String str, String str2) {
        this._operators = str;
        this._comments = str2;
    }

    /* access modifiers changed from: protected */
    public Config getConfig() {
        return this._config;
    }

    /* access modifiers changed from: protected */
    public void setConfig(Config config) {
        this._config = config;
    }

    /* access modifiers changed from: protected */
    public void parseError(String str, int i) throws InvalidFileFormatException {
        throw new InvalidFileFormatException("parse error (at line: " + i + "): " + str);
    }

    /* access modifiers changed from: package-private */
    public IniSource newIniSource(InputStream inputStream, HandlerBase handlerBase) {
        return new IniSource(inputStream, handlerBase, this._comments, getConfig());
    }

    /* access modifiers changed from: package-private */
    public IniSource newIniSource(Reader reader, HandlerBase handlerBase) {
        return new IniSource(reader, handlerBase, this._comments, getConfig());
    }

    /* access modifiers changed from: package-private */
    public IniSource newIniSource(URL url, HandlerBase handlerBase) throws IOException {
        return new IniSource(url, handlerBase, this._comments, getConfig());
    }

    /* access modifiers changed from: package-private */
    public void parseOptionLine(String str, HandlerBase handlerBase, int i) throws InvalidFileFormatException {
        String str2;
        int indexOfOperator = indexOfOperator(str);
        String str3 = null;
        if (indexOfOperator >= 0) {
            str3 = unescapeFilter(str.substring(0, indexOfOperator)).trim();
            str2 = unescapeFilter(str.substring(indexOfOperator + 1)).trim();
        } else if (getConfig().isEmptyOption()) {
            str2 = null;
            str3 = str;
        } else {
            parseError(str, i);
            str2 = null;
        }
        if (str3.length() == 0) {
            parseError(str, i);
        }
        if (getConfig().isLowerCaseOption()) {
            str3 = str3.toLowerCase(Locale.getDefault());
        }
        handlerBase.handleOption(str3, str2);
    }

    /* access modifiers changed from: package-private */
    public String unescapeFilter(String str) {
        return getConfig().isEscape() ? EscapeTool.getInstance().unescape(str) : str;
    }

    private int indexOfOperator(String str) {
        int i = -1;
        for (char indexOf : this._operators.toCharArray()) {
            int indexOf2 = str.indexOf(indexOf);
            if (indexOf2 >= 0 && (i == -1 || indexOf2 < i)) {
                i = indexOf2;
            }
        }
        return i;
    }
}
