package org.ini4j.spi;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import org.apache.http.protocol.HTTP;

class UnicodeInputStreamReader extends Reader {
    private static final int BOM_SIZE = 4;
    private final Charset _defaultEncoding;
    private InputStreamReader _reader;
    private final PushbackInputStream _stream;

    private enum Bom {
        UTF32BE("UTF-32BE", new byte[]{0, 0, -2, -1}),
        UTF32LE("UTF-32LE", new byte[]{-1, -2, 0, 0}),
        UTF16BE("UTF-16BE", new byte[]{-2, -1}),
        UTF16LE("UTF-16LE", new byte[]{-1, -2}),
        UTF8(HTTP.UTF_8, new byte[]{-17, -69, -65});
        
        private final byte[] _bytes;
        private Charset _charset;

        private Bom(String str, byte[] bArr) {
            try {
                this._charset = Charset.forName(str);
            } catch (Exception unused) {
                this._charset = null;
            }
            this._bytes = bArr;
        }

        private static Bom find(byte[] bArr) {
            for (Bom bom : values()) {
                if (bom.supported() && bom.match(bArr)) {
                    return bom;
                }
            }
            return null;
        }

        private boolean match(byte[] bArr) {
            int i = 0;
            while (true) {
                byte[] bArr2 = this._bytes;
                if (i >= bArr2.length) {
                    return true;
                }
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
                i++;
            }
        }

        private boolean supported() {
            return this._charset != null;
        }
    }

    UnicodeInputStreamReader(InputStream inputStream, Charset charset) {
        this._stream = new PushbackInputStream(inputStream, 4);
        this._defaultEncoding = charset;
    }

    public void close() throws IOException {
        init();
        this._reader.close();
    }

    public int read(char[] cArr, int i, int i2) throws IOException {
        init();
        return this._reader.read(cArr, i, i2);
    }

    /* access modifiers changed from: protected */
    public void init() throws IOException {
        Charset charset;
        int i;
        if (this._reader == null) {
            byte[] bArr = new byte[4];
            int read = this._stream.read(bArr, 0, bArr.length);
            Bom access$000 = Bom.access$000(bArr);
            if (access$000 == null) {
                charset = this._defaultEncoding;
                i = read;
            } else {
                charset = Bom.access$100(access$000);
                i = bArr.length - Bom.access$200(access$000).length;
            }
            if (i > 0) {
                this._stream.unread(bArr, read - i, i);
            }
            this._reader = new InputStreamReader(this._stream, charset);
        }
    }
}
