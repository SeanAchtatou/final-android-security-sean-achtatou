package org.ini4j.spi;

import java.nio.charset.Charset;
import java.util.Arrays;
import org.ini4j.Registry;

public class RegEscapeTool extends EscapeTool {
    private static final int DIGIT_SIZE = 4;
    private static final Charset HEX_CHARSET = Charset.forName("UTF-16LE");
    private static final RegEscapeTool INSTANCE = ((RegEscapeTool) ServiceFinder.findService(RegEscapeTool.class));
    private static final int LOWER_DIGIT = 15;
    private static final int UPPER_DIGIT = 240;

    public static final RegEscapeTool getInstance() {
        return INSTANCE;
    }

    public TypeValuesPair decode(String str) {
        Registry.Type type = type(str);
        String unquote = type == Registry.Type.REG_SZ ? unquote(str) : str.substring(type.toString().length() + 1);
        int i = AnonymousClass1.$SwitchMap$org$ini4j$Registry$Type[type.ordinal()];
        if (i == 1 || i == 2) {
            unquote = bytes2string(binary(unquote));
        } else if (i == 3) {
            unquote = String.valueOf(Long.parseLong(unquote, 16));
        }
        return new TypeValuesPair(type, type == Registry.Type.REG_MULTI_SZ ? splitMulti(unquote) : new String[]{unquote});
    }

    /* renamed from: org.ini4j.spi.RegEscapeTool$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$ini4j$Registry$Type = new int[Registry.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                org.ini4j.Registry$Type[] r0 = org.ini4j.Registry.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.ini4j.spi.RegEscapeTool.AnonymousClass1.$SwitchMap$org$ini4j$Registry$Type = r0
                int[] r0 = org.ini4j.spi.RegEscapeTool.AnonymousClass1.$SwitchMap$org$ini4j$Registry$Type     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.ini4j.Registry$Type r1 = org.ini4j.Registry.Type.REG_EXPAND_SZ     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.ini4j.spi.RegEscapeTool.AnonymousClass1.$SwitchMap$org$ini4j$Registry$Type     // Catch:{ NoSuchFieldError -> 0x001f }
                org.ini4j.Registry$Type r1 = org.ini4j.Registry.Type.REG_MULTI_SZ     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.ini4j.spi.RegEscapeTool.AnonymousClass1.$SwitchMap$org$ini4j$Registry$Type     // Catch:{ NoSuchFieldError -> 0x002a }
                org.ini4j.Registry$Type r1 = org.ini4j.Registry.Type.REG_DWORD     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = org.ini4j.spi.RegEscapeTool.AnonymousClass1.$SwitchMap$org$ini4j$Registry$Type     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.ini4j.Registry$Type r1 = org.ini4j.Registry.Type.REG_SZ     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.ini4j.spi.RegEscapeTool.AnonymousClass1.<clinit>():void");
        }
    }

    public String encode(TypeValuesPair typeValuesPair) {
        if (typeValuesPair.getType() == Registry.Type.REG_SZ) {
            return quote(typeValuesPair.getValues()[0]);
        }
        if (typeValuesPair.getValues()[0] != null) {
            return encode(typeValuesPair.getType(), typeValuesPair.getValues());
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public byte[] binary(String str) {
        byte[] bArr = new byte[str.length()];
        int i = 0;
        int i2 = 4;
        for (int i3 = 0; i3 < str.length(); i3++) {
            char charAt = str.charAt(i3);
            if (!Character.isWhitespace(charAt)) {
                if (charAt == ',') {
                    i++;
                    i2 = 4;
                } else {
                    int digit = Character.digit(charAt, 16);
                    if (digit >= 0) {
                        bArr[i] = (byte) ((digit << i2) | bArr[i]);
                        i2 = 0;
                    }
                }
            }
        }
        return Arrays.copyOfRange(bArr, 0, i + 1);
    }

    /* access modifiers changed from: package-private */
    public String encode(Registry.Type type, String[] strArr) {
        StringBuilder sb = new StringBuilder();
        sb.append(type.toString());
        sb.append(':');
        int i = AnonymousClass1.$SwitchMap$org$ini4j$Registry$Type[type.ordinal()];
        if (i == 1) {
            sb.append(hexadecimal(strArr[0]));
        } else if (i == 2) {
            for (String hexadecimal : strArr) {
                sb.append(hexadecimal(hexadecimal));
                sb.append(',');
            }
            sb.append("00,00");
        } else if (i != 3) {
            sb.append(strArr[0]);
        } else {
            sb.append(String.format("%08x", Long.valueOf(Long.parseLong(strArr[0]))));
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public String hexadecimal(String str) {
        StringBuilder sb = new StringBuilder();
        if (!(str == null || str.length() == 0)) {
            byte[] string2bytes = string2bytes(str);
            for (int i = 0; i < string2bytes.length; i++) {
                sb.append(Character.forDigit((string2bytes[i] & 240) >> 4, 16));
                sb.append(Character.forDigit(string2bytes[i] & 15, 16));
                sb.append(',');
            }
            sb.append("00,00");
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public Registry.Type type(String str) {
        if (str.charAt(0) == '\"') {
            return Registry.Type.REG_SZ;
        }
        int indexOf = str.indexOf(58);
        return indexOf < 0 ? Registry.Type.REG_SZ : Registry.Type.fromString(str.substring(0, indexOf));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|8) */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        throw new java.lang.IllegalStateException(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        return new java.lang.String(r5, 0, r5.length - 2, org.ini4j.spi.RegEscapeTool.HEX_CHARSET);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String bytes2string(byte[] r5) {
        /*
            r4 = this;
            r0 = 0
            java.lang.String r1 = new java.lang.String     // Catch:{ NoSuchMethodError -> 0x000c }
            int r2 = r5.length     // Catch:{ NoSuchMethodError -> 0x000c }
            int r2 = r2 + -2
            java.nio.charset.Charset r3 = org.ini4j.spi.RegEscapeTool.HEX_CHARSET     // Catch:{ NoSuchMethodError -> 0x000c }
            r1.<init>(r5, r0, r2, r3)     // Catch:{ NoSuchMethodError -> 0x000c }
            goto L_0x0018
        L_0x000c:
            java.lang.String r1 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x0019 }
            int r2 = r5.length     // Catch:{ UnsupportedEncodingException -> 0x0019 }
            java.nio.charset.Charset r3 = org.ini4j.spi.RegEscapeTool.HEX_CHARSET     // Catch:{ UnsupportedEncodingException -> 0x0019 }
            java.lang.String r3 = r3.name()     // Catch:{ UnsupportedEncodingException -> 0x0019 }
            r1.<init>(r5, r0, r2, r3)     // Catch:{ UnsupportedEncodingException -> 0x0019 }
        L_0x0018:
            return r1
        L_0x0019:
            r5 = move-exception
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ini4j.spi.RegEscapeTool.bytes2string(byte[]):java.lang.String");
    }

    private String[] splitMulti(String str) {
        int length = str.length();
        int indexOf = str.indexOf(0, 0);
        int i = 0;
        while (indexOf >= 0) {
            i++;
            int i2 = indexOf + 1;
            if (i2 >= length) {
                break;
            }
            indexOf = str.indexOf(0, i2);
        }
        String[] strArr = new String[i];
        int i3 = 0;
        for (int i4 = 0; i4 < i; i4++) {
            int indexOf2 = str.indexOf(0, i3);
            strArr[i4] = str.substring(i3, indexOf2);
            i3 = indexOf2 + 1;
        }
        return strArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0012, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        throw new java.lang.IllegalStateException(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        return r2.getBytes(org.ini4j.spi.RegEscapeTool.HEX_CHARSET.name());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:2:0x0007 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] string2bytes(java.lang.String r2) {
        /*
            r1 = this;
            java.nio.charset.Charset r0 = org.ini4j.spi.RegEscapeTool.HEX_CHARSET     // Catch:{ NoSuchMethodError -> 0x0007 }
            byte[] r2 = r2.getBytes(r0)     // Catch:{ NoSuchMethodError -> 0x0007 }
            goto L_0x0011
        L_0x0007:
            java.nio.charset.Charset r0 = org.ini4j.spi.RegEscapeTool.HEX_CHARSET     // Catch:{ UnsupportedEncodingException -> 0x0012 }
            java.lang.String r0 = r0.name()     // Catch:{ UnsupportedEncodingException -> 0x0012 }
            byte[] r2 = r2.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x0012 }
        L_0x0011:
            return r2
        L_0x0012:
            r2 = move-exception
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ini4j.spi.RegEscapeTool.string2bytes(java.lang.String):byte[]");
    }
}
