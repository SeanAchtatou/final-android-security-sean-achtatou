package org.ini4j.spi;

interface HandlerBase {
    void handleComment(String str);

    void handleOption(String str, String str2);
}
