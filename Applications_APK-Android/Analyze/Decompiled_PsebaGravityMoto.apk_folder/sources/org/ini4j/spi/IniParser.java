package org.ini4j.spi;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.Locale;
import org.ini4j.Config;
import org.ini4j.InvalidFileFormatException;

public class IniParser extends AbstractParser {
    private static final String COMMENTS = ";#";
    private static final String OPERATORS = ":=";
    static final char SECTION_BEGIN = '[';
    static final char SECTION_END = ']';

    public IniParser() {
        super(OPERATORS, COMMENTS);
    }

    public static IniParser newInstance() {
        return (IniParser) ServiceFinder.findService(IniParser.class);
    }

    public static IniParser newInstance(Config config) {
        IniParser newInstance = newInstance();
        newInstance.setConfig(config);
        return newInstance;
    }

    public void parse(InputStream inputStream, IniHandler iniHandler) throws IOException, InvalidFileFormatException {
        parse(newIniSource(inputStream, iniHandler), iniHandler);
    }

    public void parse(Reader reader, IniHandler iniHandler) throws IOException, InvalidFileFormatException {
        parse(newIniSource(reader, iniHandler), iniHandler);
    }

    public void parse(URL url, IniHandler iniHandler) throws IOException, InvalidFileFormatException {
        parse(newIniSource(url, iniHandler), iniHandler);
    }

    private void parse(IniSource iniSource, IniHandler iniHandler) throws IOException, InvalidFileFormatException {
        iniHandler.startIni();
        String readLine = iniSource.readLine();
        String str = null;
        while (readLine != null) {
            if (readLine.charAt(0) == '[') {
                if (str != null) {
                    iniHandler.endSection();
                }
                str = parseSectionLine(readLine, iniSource, iniHandler);
            } else {
                if (str == null) {
                    if (getConfig().isGlobalSection()) {
                        str = getConfig().getGlobalSectionName();
                        iniHandler.startSection(str);
                    } else {
                        parseError(readLine, iniSource.getLineNumber());
                    }
                }
                parseOptionLine(readLine, iniHandler, iniSource.getLineNumber());
            }
            readLine = iniSource.readLine();
        }
        if (str != null) {
            iniHandler.endSection();
        }
        iniHandler.endIni();
    }

    private String parseSectionLine(String str, IniSource iniSource, IniHandler iniHandler) throws InvalidFileFormatException {
        if (str.charAt(str.length() - 1) != ']') {
            parseError(str, iniSource.getLineNumber());
        }
        String unescapeFilter = unescapeFilter(str.substring(1, str.length() - 1).trim());
        if (unescapeFilter.length() == 0 && !getConfig().isUnnamedSection()) {
            parseError(str, iniSource.getLineNumber());
        }
        if (getConfig().isLowerCaseSection()) {
            unescapeFilter = unescapeFilter.toLowerCase(Locale.getDefault());
        }
        iniHandler.startSection(unescapeFilter);
        return unescapeFilter;
    }
}
