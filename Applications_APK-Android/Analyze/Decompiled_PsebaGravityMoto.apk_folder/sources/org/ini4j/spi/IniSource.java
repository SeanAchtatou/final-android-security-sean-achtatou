package org.ini4j.spi;

import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.Reader;
import java.net.URL;
import org.ini4j.Config;

class IniSource {
    private static final char ESCAPE_CHAR = '\\';
    public static final char INCLUDE_BEGIN = '<';
    public static final char INCLUDE_END = '>';
    public static final char INCLUDE_OPTIONAL = '?';
    private URL _base;
    private IniSource _chain;
    private final String _commentChars;
    private final Config _config;
    private final HandlerBase _handler;
    private final LineNumberReader _reader;

    IniSource(InputStream inputStream, HandlerBase handlerBase, String str, Config config) {
        this(new UnicodeInputStreamReader(inputStream, config.getFileEncoding()), handlerBase, str, config);
    }

    IniSource(Reader reader, HandlerBase handlerBase, String str, Config config) {
        this._reader = new LineNumberReader(reader);
        this._handler = handlerBase;
        this._commentChars = str;
        this._config = config;
    }

    IniSource(URL url, HandlerBase handlerBase, String str, Config config) throws IOException {
        this(new UnicodeInputStreamReader(url.openStream(), config.getFileEncoding()), handlerBase, str, config);
        this._base = url;
    }

    /* access modifiers changed from: package-private */
    public int getLineNumber() {
        IniSource iniSource = this._chain;
        if (iniSource == null) {
            return this._reader.getLineNumber();
        }
        return iniSource.getLineNumber();
    }

    /* access modifiers changed from: package-private */
    public String readLine() throws IOException {
        IniSource iniSource = this._chain;
        if (iniSource == null) {
            return readLineLocal();
        }
        String readLine = iniSource.readLine();
        if (readLine != null) {
            return readLine;
        }
        this._chain = null;
        return readLine();
    }

    private void close() throws IOException {
        this._reader.close();
    }

    private int countEndingEscapes(String str) {
        int length = str.length() - 1;
        int i = 0;
        while (length >= 0 && str.charAt(length) == '\\') {
            i++;
            length--;
        }
        return i;
    }

    private void handleComment(StringBuilder sb) {
        if (sb.length() != 0) {
            sb.deleteCharAt(sb.length() - 1);
            this._handler.handleComment(sb.toString());
            sb.delete(0, sb.length());
        }
    }

    private String handleInclude(String str) throws IOException {
        if (!this._config.isInclude() || str.length() <= 2) {
            return str;
        }
        boolean z = false;
        if (str.charAt(0) != '<' || str.charAt(str.length() - 1) != '>') {
            return str;
        }
        String trim = str.substring(1, str.length() - 1).trim();
        if (trim.charAt(0) == '?') {
            z = true;
        }
        if (z) {
            trim = trim.substring(1).trim();
        }
        URL url = this._base;
        URL url2 = url == null ? new URL(trim) : new URL(url, trim);
        if (z) {
            try {
                this._chain = new IniSource(url2, this._handler, this._commentChars, this._config);
            } catch (IOException unused) {
            } catch (Throwable th) {
                readLine();
                throw th;
            }
            return readLine();
        }
        this._chain = new IniSource(url2, this._handler, this._commentChars, this._config);
        return readLine();
    }

    private String readLineLocal() throws IOException {
        String readLineSkipComments = readLineSkipComments();
        if (readLineSkipComments != null) {
            return handleInclude(readLineSkipComments);
        }
        close();
        return readLineSkipComments;
    }

    private String readLineSkipComments() throws IOException {
        String readLine;
        String trim;
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        while (true) {
            readLine = this._reader.readLine();
            if (readLine == null) {
                break;
            }
            trim = readLine.trim();
            if (trim.length() == 0) {
                handleComment(sb);
            } else if (this._commentChars.indexOf(trim.charAt(0)) < 0 || sb2.length() != 0) {
                handleComment(sb);
                if (!this._config.isEscapeNewline() || (countEndingEscapes(trim) & 1) == 0) {
                    sb2.append(trim);
                    readLine = sb2.toString();
                } else {
                    sb2.append(trim.subSequence(0, trim.length() - 1));
                }
            } else {
                sb.append(trim.substring(1));
                sb.append(this._config.getLineSeparator());
            }
        }
        sb2.append(trim);
        readLine = sb2.toString();
        if (readLine == null && sb.length() != 0) {
            handleComment(sb);
        }
        return readLine;
    }
}
