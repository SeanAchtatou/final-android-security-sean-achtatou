package org.ini4j.spi;

import java.io.PrintWriter;
import org.ini4j.Config;

abstract class AbstractFormatter implements HandlerBase {
    private static final char COMMENT = '#';
    private static final char OPERATOR = '=';
    private static final char SPACE = ' ';
    private Config _config = Config.getGlobal();
    private boolean _header = true;
    private PrintWriter _output;

    AbstractFormatter() {
    }

    public void handleComment(String str) {
        if (getConfig().isComment() && !((this._header && !getConfig().isHeaderComment()) || str == null || str.length() == 0)) {
            for (String print : str.split(getConfig().getLineSeparator())) {
                getOutput().print((char) COMMENT);
                getOutput().print(print);
                getOutput().print(getConfig().getLineSeparator());
            }
            if (this._header) {
                getOutput().print(getConfig().getLineSeparator());
            }
        }
        this._header = false;
    }

    public void handleOption(String str, String str2) {
        if (getConfig().isStrictOperator()) {
            if (getConfig().isEmptyOption() || str2 != null) {
                getOutput().print(escapeFilter(str));
                getOutput().print((char) OPERATOR);
            }
            if (str2 != null) {
                getOutput().print(escapeFilter(str2));
            }
            if (getConfig().isEmptyOption() || str2 != null) {
                getOutput().print(getConfig().getLineSeparator());
            }
        } else {
            if (str2 == null && getConfig().isEmptyOption()) {
                str2 = "";
            }
            if (str2 != null) {
                getOutput().print(escapeFilter(str));
                getOutput().print((char) SPACE);
                getOutput().print((char) OPERATOR);
                getOutput().print((char) SPACE);
                getOutput().print(escapeFilter(str2));
                getOutput().print(getConfig().getLineSeparator());
            }
        }
        setHeader(false);
    }

    /* access modifiers changed from: protected */
    public Config getConfig() {
        return this._config;
    }

    /* access modifiers changed from: protected */
    public void setConfig(Config config) {
        this._config = config;
    }

    /* access modifiers changed from: protected */
    public PrintWriter getOutput() {
        return this._output;
    }

    /* access modifiers changed from: protected */
    public void setOutput(PrintWriter printWriter) {
        this._output = printWriter;
    }

    /* access modifiers changed from: package-private */
    public void setHeader(boolean z) {
        this._header = z;
    }

    /* access modifiers changed from: package-private */
    public String escapeFilter(String str) {
        return getConfig().isEscape() ? EscapeTool.getInstance().escape(str) : str;
    }
}
