package org.ini4j.spi;

import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.Profile;

public class IniBuilder extends AbstractProfileBuilder implements IniHandler {
    private Ini _ini;

    public /* bridge */ /* synthetic */ void endIni() {
        super.endIni();
    }

    public /* bridge */ /* synthetic */ void endSection() {
        super.endSection();
    }

    public /* bridge */ /* synthetic */ void handleComment(String str) {
        super.handleComment(str);
    }

    public /* bridge */ /* synthetic */ void handleOption(String str, String str2) {
        super.handleOption(str, str2);
    }

    public /* bridge */ /* synthetic */ void startIni() {
        super.startIni();
    }

    public /* bridge */ /* synthetic */ void startSection(String str) {
        super.startSection(str);
    }

    public static IniBuilder newInstance(Ini ini) {
        IniBuilder newInstance = newInstance();
        newInstance.setIni(ini);
        return newInstance;
    }

    public void setIni(Ini ini) {
        this._ini = ini;
    }

    /* access modifiers changed from: package-private */
    public Config getConfig() {
        return this._ini.getConfig();
    }

    /* access modifiers changed from: package-private */
    public Profile getProfile() {
        return this._ini;
    }

    private static IniBuilder newInstance() {
        return (IniBuilder) ServiceFinder.findService(IniBuilder.class);
    }
}
