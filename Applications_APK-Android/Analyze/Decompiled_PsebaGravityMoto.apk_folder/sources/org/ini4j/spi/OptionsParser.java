package org.ini4j.spi;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import org.ini4j.Config;
import org.ini4j.InvalidFileFormatException;

public class OptionsParser extends AbstractParser {
    private static final String COMMENTS = "!#";
    private static final String OPERATORS = ":=";

    public OptionsParser() {
        super(OPERATORS, COMMENTS);
    }

    public static OptionsParser newInstance() {
        return (OptionsParser) ServiceFinder.findService(OptionsParser.class);
    }

    public static OptionsParser newInstance(Config config) {
        OptionsParser newInstance = newInstance();
        newInstance.setConfig(config);
        return newInstance;
    }

    public void parse(InputStream inputStream, OptionsHandler optionsHandler) throws IOException, InvalidFileFormatException {
        parse(newIniSource(inputStream, optionsHandler), optionsHandler);
    }

    public void parse(Reader reader, OptionsHandler optionsHandler) throws IOException, InvalidFileFormatException {
        parse(newIniSource(reader, optionsHandler), optionsHandler);
    }

    public void parse(URL url, OptionsHandler optionsHandler) throws IOException, InvalidFileFormatException {
        parse(newIniSource(url, optionsHandler), optionsHandler);
    }

    private void parse(IniSource iniSource, OptionsHandler optionsHandler) throws IOException, InvalidFileFormatException {
        optionsHandler.startOptions();
        while (true) {
            String readLine = iniSource.readLine();
            if (readLine != null) {
                parseOptionLine(readLine, optionsHandler, iniSource.getLineNumber());
            } else {
                optionsHandler.endOptions();
                return;
            }
        }
    }
}
