package org.ini4j.spi;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URI;
import java.net.URL;
import java.util.TimeZone;

public class BeanTool {
    private static final BeanTool INSTANCE = ((BeanTool) ServiceFinder.findService(BeanTool.class));
    private static final String PARSE_METHOD = "valueOf";

    public static final BeanTool getInstance() {
        return INSTANCE;
    }

    public void inject(Object obj, BeanAccess beanAccess) {
        Object obj2;
        PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(obj.getClass());
        int length = propertyDescriptors.length;
        int i = 0;
        while (i < length) {
            PropertyDescriptor propertyDescriptor = propertyDescriptors[i];
            try {
                Method writeMethod = propertyDescriptor.getWriteMethod();
                String name = propertyDescriptor.getName();
                if (!(writeMethod == null || beanAccess.propLength(name) == 0)) {
                    if (propertyDescriptor.getPropertyType().isArray()) {
                        obj2 = Array.newInstance(propertyDescriptor.getPropertyType().getComponentType(), beanAccess.propLength(name));
                        for (int i2 = 0; i2 < beanAccess.propLength(name); i2++) {
                            Array.set(obj2, i2, parse(beanAccess.propGet(name, i2), propertyDescriptor.getPropertyType().getComponentType()));
                        }
                    } else {
                        obj2 = parse(beanAccess.propGet(name), propertyDescriptor.getPropertyType());
                    }
                    writeMethod.invoke(obj, obj2);
                }
                i++;
            } catch (Exception e) {
                throw ((IllegalArgumentException) new IllegalArgumentException("Failed to set property: " + propertyDescriptor.getDisplayName()).initCause(e));
            }
        }
    }

    public void inject(BeanAccess beanAccess, Object obj) {
        Object invoke;
        PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(obj.getClass());
        int length = propertyDescriptors.length;
        int i = 0;
        while (i < length) {
            PropertyDescriptor propertyDescriptor = propertyDescriptors[i];
            try {
                Method readMethod = propertyDescriptor.getReadMethod();
                if (!(readMethod == null || "class".equals(propertyDescriptor.getName()) || (invoke = readMethod.invoke(obj, null)) == null)) {
                    if (propertyDescriptor.getPropertyType().isArray()) {
                        for (int i2 = 0; i2 < Array.getLength(invoke); i2++) {
                            Object obj2 = Array.get(invoke, i2);
                            if (obj2 != null && !obj2.getClass().equals(String.class)) {
                                obj2 = obj2.toString();
                            }
                            beanAccess.propAdd(propertyDescriptor.getName(), (String) obj2);
                        }
                    } else {
                        beanAccess.propSet(propertyDescriptor.getName(), invoke.toString());
                    }
                }
                i++;
            } catch (Exception e) {
                throw new IllegalArgumentException("Failed to set property: " + propertyDescriptor.getDisplayName(), e);
            }
        }
    }

    public <T> T parse(String str, Class<T> cls) throws IllegalArgumentException {
        if (cls == null) {
            throw new IllegalArgumentException("null argument");
        } else if (str == null) {
            return zero(cls);
        } else {
            if (cls.isPrimitive()) {
                return parsePrimitiveValue(str, cls);
            }
            if (cls == String.class) {
                return str;
            }
            if (cls == Character.class) {
                return new Character(str.charAt(0));
            }
            return parseSpecialValue(str, cls);
        }
    }

    public <T> T proxy(Class<T> cls, BeanAccess beanAccess) {
        return cls.cast(Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{cls}, new BeanInvocationHandler(beanAccess)));
    }

    public <T> T zero(Class<T> cls) {
        if (cls.isPrimitive()) {
            if (cls == Boolean.TYPE) {
                return Boolean.FALSE;
            }
            if (cls == Byte.TYPE) {
                return (byte) 0;
            }
            if (cls == Character.TYPE) {
                return new Character(0);
            }
            if (cls == Double.TYPE) {
                return new Double(0.0d);
            }
            if (cls == Float.TYPE) {
                return new Float(0.0f);
            }
            if (cls == Integer.TYPE) {
                return 0;
            }
            if (cls == Long.TYPE) {
                return 0L;
            }
            if (cls == Short.TYPE) {
                return (short) 0;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Object parseSpecialValue(String str, Class cls) throws IllegalArgumentException {
        if (cls == File.class) {
            try {
                return new File(str);
            } catch (Exception e) {
                throw ((IllegalArgumentException) new IllegalArgumentException().initCause(e));
            }
        } else if (cls == URL.class) {
            return new URL(str);
        } else {
            if (cls == URI.class) {
                return new URI(str);
            }
            if (cls == Class.class) {
                return Class.forName(str);
            }
            if (cls == TimeZone.class) {
                return TimeZone.getTimeZone(str);
            }
            return cls.getMethod(PARSE_METHOD, String.class).invoke(null, str);
        }
    }

    private PropertyDescriptor[] getPropertyDescriptors(Class cls) {
        try {
            return Introspector.getBeanInfo(cls).getPropertyDescriptors();
        } catch (IntrospectionException e) {
            throw new IllegalArgumentException((Throwable) e);
        }
    }

    private Object parsePrimitiveValue(String str, Class cls) throws IllegalArgumentException {
        try {
            if (cls == Boolean.TYPE) {
                return Boolean.valueOf(str);
            }
            if (cls == Byte.TYPE) {
                return Byte.valueOf(str);
            }
            if (cls == Character.TYPE) {
                return new Character(str.charAt(0));
            }
            if (cls == Double.TYPE) {
                return Double.valueOf(str);
            }
            if (cls == Float.TYPE) {
                return Float.valueOf(str);
            }
            if (cls == Integer.TYPE) {
                return Integer.valueOf(str);
            }
            if (cls == Long.TYPE) {
                return Long.valueOf(str);
            }
            if (cls == Short.TYPE) {
                return Short.valueOf(str);
            }
            return null;
        } catch (Exception e) {
            throw ((IllegalArgumentException) new IllegalArgumentException().initCause(e));
        }
    }

    static class BeanInvocationHandler extends AbstractBeanInvocationHandler {
        private final BeanAccess _backend;

        BeanInvocationHandler(BeanAccess beanAccess) {
            this._backend = beanAccess;
        }

        /* access modifiers changed from: protected */
        public Object getPropertySpi(String str, Class<?> cls) {
            if (!cls.isArray()) {
                return this._backend.propGet(str);
            }
            int propLength = this._backend.propLength(str);
            if (propLength == 0) {
                return null;
            }
            String[] strArr = new String[propLength];
            for (int i = 0; i < strArr.length; i++) {
                strArr[i] = this._backend.propGet(str, i);
            }
            return strArr;
        }

        /* access modifiers changed from: protected */
        public void setPropertySpi(String str, Object obj, Class<?> cls) {
            if (cls.isArray()) {
                this._backend.propDel(str);
                for (int i = 0; i < Array.getLength(obj); i++) {
                    this._backend.propAdd(str, Array.get(obj, i).toString());
                }
                return;
            }
            this._backend.propSet(str, obj.toString());
        }

        /* access modifiers changed from: protected */
        public boolean hasPropertySpi(String str) {
            return this._backend.propLength(str) != 0;
        }
    }
}
