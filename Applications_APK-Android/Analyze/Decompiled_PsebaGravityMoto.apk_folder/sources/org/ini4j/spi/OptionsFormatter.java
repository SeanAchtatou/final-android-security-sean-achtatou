package org.ini4j.spi;

import java.io.PrintWriter;
import java.io.Writer;
import org.ini4j.Config;

public class OptionsFormatter extends AbstractFormatter implements OptionsHandler {
    public void startOptions() {
    }

    public /* bridge */ /* synthetic */ void handleComment(String str) {
        super.handleComment(str);
    }

    public /* bridge */ /* synthetic */ void handleOption(String str, String str2) {
        super.handleOption(str, str2);
    }

    public static OptionsFormatter newInstance(Writer writer, Config config) {
        OptionsFormatter newInstance = newInstance();
        newInstance.setOutput(writer instanceof PrintWriter ? (PrintWriter) writer : new PrintWriter(writer));
        newInstance.setConfig(config);
        return newInstance;
    }

    public void endOptions() {
        getOutput().flush();
    }

    private static OptionsFormatter newInstance() {
        return (OptionsFormatter) ServiceFinder.findService(OptionsFormatter.class);
    }
}
