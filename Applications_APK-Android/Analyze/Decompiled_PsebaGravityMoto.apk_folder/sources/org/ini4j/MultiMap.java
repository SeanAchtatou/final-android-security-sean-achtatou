package org.ini4j;

import java.util.List;
import java.util.Map;

public interface MultiMap<K, V> extends Map<K, V> {
    void add(K k, V v);

    void add(K k, V v, int i);

    V get(Object obj, int i);

    List<V> getAll(Object obj);

    int length(Object obj);

    V put(Object obj, Object obj2, int i);

    List<V> putAll(K k, List<V> list);

    V remove(Object obj, int i);
}
