package org.ini4j;

import org.ini4j.Profile;
import org.ini4j.Registry;
import org.ini4j.spi.IniHandler;
import org.ini4j.spi.RegEscapeTool;
import org.ini4j.spi.TypeValuesPair;

public class BasicRegistry extends BasicProfile implements Registry {
    private static final long serialVersionUID = -6432826330714504802L;
    private String _version = Registry.VERSION;

    public String getVersion() {
        return this._version;
    }

    public void setVersion(String str) {
        this._version = str;
    }

    public Registry.Key add(String str) {
        return (Registry.Key) super.add(str);
    }

    public Registry.Key get(Object obj) {
        return (Registry.Key) super.get(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.get(java.lang.Object, int):V
     arg types: [java.lang.Object, int]
     candidates:
      org.ini4j.BasicRegistry.get(java.lang.Object, int):org.ini4j.Registry$Key
      org.ini4j.BasicProfile.get(java.lang.Object, java.lang.Object):java.lang.String
      org.ini4j.Profile.get(java.lang.Object, java.lang.Object):java.lang.String
      org.ini4j.Registry.get(java.lang.Object, int):org.ini4j.Registry$Key
      org.ini4j.Profile.get(java.lang.Object, java.lang.Object):java.lang.String
      org.ini4j.BasicMultiMap.get(java.lang.Object, int):V */
    public Registry.Key get(Object obj, int i) {
        return (Registry.Key) super.get(obj, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V
     arg types: [java.lang.String, org.ini4j.Profile$Section]
     candidates:
      org.ini4j.BasicRegistry.put(java.lang.String, org.ini4j.Profile$Section):org.ini4j.Registry$Key
      org.ini4j.Registry.put(java.lang.String, org.ini4j.Profile$Section):org.ini4j.Registry$Key
      org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V */
    public Registry.Key put(String str, Profile.Section section) {
        return (Registry.Key) super.put((Object) str, (Object) section);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object, int):V
     arg types: [java.lang.String, org.ini4j.Profile$Section, int]
     candidates:
      org.ini4j.BasicRegistry.put(java.lang.String, org.ini4j.Profile$Section, int):org.ini4j.Registry$Key
      org.ini4j.BasicProfile.put(java.lang.String, java.lang.String, java.lang.Object):java.lang.String
      org.ini4j.Profile.put(java.lang.String, java.lang.String, java.lang.Object):java.lang.String
      org.ini4j.Registry.put(java.lang.String, org.ini4j.Profile$Section, int):org.ini4j.Registry$Key
      org.ini4j.Profile.put(java.lang.String, java.lang.String, java.lang.Object):java.lang.String
      org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object, int):V */
    public Registry.Key put(String str, Profile.Section section, int i) {
        return (Registry.Key) super.put((Object) str, (Object) section, i);
    }

    public Registry.Key remove(Profile.Section section) {
        return (Registry.Key) super.remove(section);
    }

    public Registry.Key remove(Object obj) {
        return (Registry.Key) super.remove(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.CommonMultiMap.remove(java.lang.Object, int):V
     arg types: [java.lang.Object, int]
     candidates:
      org.ini4j.BasicRegistry.remove(java.lang.Object, int):org.ini4j.Registry$Key
      org.ini4j.BasicProfile.remove(java.lang.Object, java.lang.Object):java.lang.String
      org.ini4j.Profile.remove(java.lang.Object, java.lang.Object):java.lang.String
      org.ini4j.Registry.remove(java.lang.Object, int):org.ini4j.Registry$Key
      org.ini4j.Profile.remove(java.lang.Object, java.lang.Object):java.lang.String
      org.ini4j.CommonMultiMap.remove(java.lang.Object, int):V */
    public Registry.Key remove(Object obj, int i) {
        return (Registry.Key) super.remove(obj, i);
    }

    /* access modifiers changed from: package-private */
    public Registry.Key newSection(String str) {
        return new BasicRegistryKey(this, str);
    }

    /* access modifiers changed from: package-private */
    public void store(IniHandler iniHandler, Profile.Section section, String str) {
        store(iniHandler, section.getComment(str));
        Registry.Type type = ((Registry.Key) section).getType(str, Registry.Type.REG_SZ);
        String quote = str.equals(Registry.Key.DEFAULT_NAME) ? str : RegEscapeTool.getInstance().quote(str);
        String[] strArr = new String[section.length(str)];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = (String) section.get(str, i);
        }
        iniHandler.handleOption(quote, RegEscapeTool.getInstance().encode(new TypeValuesPair(type, strArr)));
    }
}
