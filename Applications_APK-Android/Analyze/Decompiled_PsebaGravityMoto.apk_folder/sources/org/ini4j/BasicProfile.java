package org.ini4j;

import java.lang.reflect.Array;
import java.lang.reflect.Proxy;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ini4j.Profile;
import org.ini4j.spi.AbstractBeanInvocationHandler;
import org.ini4j.spi.BeanTool;
import org.ini4j.spi.IniHandler;

public class BasicProfile extends CommonMultiMap<String, Profile.Section> implements Profile {
    private static final Pattern EXPRESSION = Pattern.compile("(?<!\\\\)\\$\\{(([^\\[\\}]+)(\\[([0-9]+)\\])?/)?([^\\[^/\\}]+)(\\[(([0-9]+))\\])?\\}");
    private static final int G_OPTION = 5;
    private static final int G_OPTION_IDX = 7;
    private static final int G_SECTION = 2;
    private static final int G_SECTION_IDX = 4;
    private static final String SECTION_ENVIRONMENT = "@env";
    private static final String SECTION_SYSTEM_PROPERTIES = "@prop";
    private static final long serialVersionUID = -1817521505004015256L;
    private String _comment;
    private final boolean _propertyFirstUpper;
    private final boolean _treeMode;

    /* access modifiers changed from: package-private */
    public char getPathSeparator() {
        return '/';
    }

    public BasicProfile() {
        this(false, false);
    }

    public BasicProfile(boolean z, boolean z2) {
        this._treeMode = z;
        this._propertyFirstUpper = z2;
    }

    public String getComment() {
        return this._comment;
    }

    public void setComment(String str) {
        this._comment = str;
    }

    public Profile.Section add(String str) {
        int lastIndexOf;
        if (isTreeMode() && (lastIndexOf = str.lastIndexOf(getPathSeparator())) > 0) {
            String substring = str.substring(0, lastIndexOf);
            if (!containsKey(substring)) {
                add(substring);
            }
        }
        Profile.Section newSection = newSection(str);
        add(str, newSection);
        return newSection;
    }

    public void add(String str, String str2, Object obj) {
        getOrAdd(str).add(str2, obj);
    }

    public <T> T as(Class<T> cls) {
        return as(cls, null);
    }

    public <T> T as(Class<T> cls, String str) {
        return cls.cast(Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{cls}, new BeanInvocationHandler(str)));
    }

    public String fetch(Object obj, Object obj2) {
        Profile.Section section = (Profile.Section) get(obj);
        if (section == null) {
            return null;
        }
        return section.fetch(obj2);
    }

    public <T> T fetch(Object obj, Object obj2, Class<T> cls) {
        Profile.Section section = (Profile.Section) get(obj);
        return section == null ? BeanTool.getInstance().zero(cls) : section.fetch(obj2, cls);
    }

    public String get(Object obj, Object obj2) {
        Profile.Section section = (Profile.Section) get(obj);
        if (section == null) {
            return null;
        }
        return (String) section.get(obj2);
    }

    public <T> T get(Object obj, Object obj2, Class<T> cls) {
        Profile.Section section = (Profile.Section) get(obj);
        return section == null ? BeanTool.getInstance().zero(cls) : section.get(obj2, cls);
    }

    public String put(String str, String str2, Object obj) {
        return getOrAdd(str).put(str2, obj);
    }

    public Profile.Section remove(Profile.Section section) {
        return (Profile.Section) remove(section.getName());
    }

    public String remove(Object obj, Object obj2) {
        Profile.Section section = (Profile.Section) get(obj);
        if (section == null) {
            return null;
        }
        return (String) section.remove(obj2);
    }

    /* access modifiers changed from: package-private */
    public boolean isTreeMode() {
        return this._treeMode;
    }

    /* access modifiers changed from: package-private */
    public boolean isPropertyFirstUpper() {
        return this._propertyFirstUpper;
    }

    /* access modifiers changed from: package-private */
    public Profile.Section newSection(String str) {
        return new BasicProfileSection(this, str);
    }

    /* access modifiers changed from: package-private */
    public void resolve(StringBuilder sb, Profile.Section section) {
        Matcher matcher = EXPRESSION.matcher(sb);
        while (matcher.find()) {
            String group = matcher.group(2);
            String group2 = matcher.group(5);
            int parseOptionIndex = parseOptionIndex(matcher);
            Profile.Section parseSection = parseSection(matcher, section);
            String str = null;
            if (SECTION_ENVIRONMENT.equals(group)) {
                str = Config.getEnvironment(group2);
            } else if (SECTION_SYSTEM_PROPERTIES.equals(group)) {
                str = Config.getSystemProperty(group2);
            } else if (parseSection != null) {
                str = parseOptionIndex == -1 ? parseSection.fetch(group2) : parseSection.fetch(group2, parseOptionIndex);
            }
            if (str != null) {
                sb.replace(matcher.start(), matcher.end(), str);
                matcher.reset(sb);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void store(IniHandler iniHandler) {
        iniHandler.startIni();
        store(iniHandler, getComment());
        for (Profile.Section store : values()) {
            store(iniHandler, store);
        }
        iniHandler.endIni();
    }

    /* access modifiers changed from: package-private */
    public void store(IniHandler iniHandler, Profile.Section section) {
        store(iniHandler, getComment(section.getName()));
        iniHandler.startSection(section.getName());
        for (String store : section.keySet()) {
            store(iniHandler, section, store);
        }
        iniHandler.endSection();
    }

    /* access modifiers changed from: package-private */
    public void store(IniHandler iniHandler, String str) {
        iniHandler.handleComment(str);
    }

    /* access modifiers changed from: package-private */
    public void store(IniHandler iniHandler, Profile.Section section, String str) {
        store(iniHandler, section.getComment(str));
        int length = section.length(str);
        for (int i = 0; i < length; i++) {
            store(iniHandler, section, str, i);
        }
    }

    /* access modifiers changed from: package-private */
    public void store(IniHandler iniHandler, Profile.Section section, String str, int i) {
        iniHandler.handleOption(str, (String) section.get(str, i));
    }

    private Profile.Section getOrAdd(String str) {
        Profile.Section section = (Profile.Section) get(str);
        return section == null ? add(str) : section;
    }

    private int parseOptionIndex(Matcher matcher) {
        if (matcher.group(7) == null) {
            return -1;
        }
        return Integer.parseInt(matcher.group(7));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private Profile.Section parseSection(Matcher matcher, Profile.Section section) {
        String group = matcher.group(2);
        int parseSectionIndex = parseSectionIndex(matcher);
        if (group == null) {
            return section;
        }
        return (Profile.Section) (parseSectionIndex == -1 ? get(group) : get(group, parseSectionIndex));
    }

    private int parseSectionIndex(Matcher matcher) {
        if (matcher.group(4) == null) {
            return -1;
        }
        return Integer.parseInt(matcher.group(4));
    }

    private final class BeanInvocationHandler extends AbstractBeanInvocationHandler {
        private final String _prefix;

        private BeanInvocationHandler(String str) {
            this._prefix = str;
        }

        /* access modifiers changed from: protected */
        public Object getPropertySpi(String str, Class<?> cls) {
            String transform = transform(str);
            if (!BasicProfile.this.containsKey(transform)) {
                return null;
            }
            if (!cls.isArray()) {
                return ((Profile.Section) BasicProfile.this.get(transform)).as(cls);
            }
            Object newInstance = Array.newInstance(cls.getComponentType(), BasicProfile.this.length(transform));
            for (int i = 0; i < BasicProfile.this.length(transform); i++) {
                Array.set(newInstance, i, ((Profile.Section) BasicProfile.this.get(transform, i)).as(cls.getComponentType()));
            }
            return newInstance;
        }

        /* access modifiers changed from: protected */
        public void setPropertySpi(String str, Object obj, Class<?> cls) {
            String transform = transform(str);
            BasicProfile.this.remove(transform);
            if (obj == null) {
                return;
            }
            if (cls.isArray()) {
                for (int i = 0; i < Array.getLength(obj); i++) {
                    BasicProfile.this.add(transform).from(Array.get(obj, i));
                }
                return;
            }
            BasicProfile.this.add(transform).from(obj);
        }

        /* access modifiers changed from: protected */
        public boolean hasPropertySpi(String str) {
            return BasicProfile.this.containsKey(transform(str));
        }

        /* access modifiers changed from: package-private */
        public String transform(String str) {
            String str2;
            if (this._prefix == null) {
                str2 = str;
            } else {
                str2 = this._prefix + str;
            }
            if (!BasicProfile.this.isPropertyFirstUpper()) {
                return str2;
            }
            return Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
    }
}
