package org.ini4j;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import org.ini4j.spi.WinEscapeTool;

public class Wini extends Ini {
    public static final char PATH_SEPARATOR = '\\';
    private static final long serialVersionUID = -2781377824232440728L;

    public Wini() {
        Config clone = Config.getGlobal().clone();
        clone.setEscape(false);
        clone.setEscapeNewline(false);
        clone.setGlobalSection(true);
        clone.setEmptyOption(true);
        clone.setMultiOption(false);
        clone.setPathSeparator('\\');
        setConfig(clone);
    }

    public Wini(File file) throws IOException, InvalidFileFormatException {
        this();
        setFile(file);
        load();
    }

    public Wini(URL url) throws IOException, InvalidFileFormatException {
        this();
        load(url);
    }

    public Wini(InputStream inputStream) throws IOException, InvalidFileFormatException {
        this();
        load(inputStream);
    }

    public Wini(Reader reader) throws IOException, InvalidFileFormatException {
        this();
        load(reader);
    }

    public String escape(String str) {
        return WinEscapeTool.getInstance().escape(str);
    }

    public String unescape(String str) {
        return WinEscapeTool.getInstance().unescape(str);
    }
}
