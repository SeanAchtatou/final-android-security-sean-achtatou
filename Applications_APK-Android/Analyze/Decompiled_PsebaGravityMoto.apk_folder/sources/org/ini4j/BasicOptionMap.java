package org.ini4j;

import java.lang.reflect.Array;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ini4j.spi.BeanAccess;
import org.ini4j.spi.BeanTool;

public class BasicOptionMap extends CommonMultiMap<String, String> implements OptionMap {
    private static final String ENVIRONMENT_PREFIX = "@env/";
    private static final int ENVIRONMENT_PREFIX_LEN = 5;
    private static final Pattern EXPRESSION = Pattern.compile("(?<!\\\\)\\$\\{(([^\\[\\}]+)(\\[([0-9]+)\\])?)\\}");
    private static final int G_INDEX = 4;
    private static final int G_OPTION = 2;
    private static final char SUBST_CHAR = '$';
    private static final String SYSTEM_PROPERTY_PREFIX = "@prop/";
    private static final int SYSTEM_PROPERTY_PREFIX_LEN = 6;
    private static final long serialVersionUID = 325469712293707584L;
    private BeanAccess _defaultBeanAccess;
    private final boolean _propertyFirstUpper;

    public BasicOptionMap() {
        this(false);
    }

    public BasicOptionMap(boolean z) {
        this._propertyFirstUpper = z;
    }

    public <T> T getAll(Object obj, Class<T> cls) {
        requireArray(cls);
        T newInstance = Array.newInstance(cls.getComponentType(), length(obj));
        for (int i = 0; i < length(obj); i++) {
            Array.set(newInstance, i, BeanTool.getInstance().parse((String) get(obj, i), cls.getComponentType()));
        }
        return newInstance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.BasicOptionMap.add(java.lang.String, java.lang.Object):void
      org.ini4j.OptionMap.add(java.lang.String, java.lang.Object):void
      org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object):void */
    public void add(String str, Object obj) {
        super.add((Object) str, (Object) ((obj == null || (obj instanceof String)) ? (String) obj : String.valueOf(obj)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object, int):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      org.ini4j.BasicOptionMap.add(java.lang.String, java.lang.Object, int):void
      org.ini4j.OptionMap.add(java.lang.String, java.lang.Object, int):void
      org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object, int):void */
    public void add(String str, Object obj, int i) {
        super.add((Object) str, (Object) ((obj == null || (obj instanceof String)) ? (String) obj : String.valueOf(obj)), i);
    }

    public <T> T as(Class<T> cls) {
        return BeanTool.getInstance().proxy(cls, getDefaultBeanAccess());
    }

    public <T> T as(Class<T> cls, String str) {
        return BeanTool.getInstance().proxy(cls, newBeanAccess(str));
    }

    public String fetch(Object obj) {
        int length = length(obj);
        if (length == 0) {
            return null;
        }
        return fetch(obj, length - 1);
    }

    public String fetch(Object obj, String str) {
        String str2 = (String) get(obj);
        return str2 == null ? str : str2;
    }

    public String fetch(Object obj, int i) {
        String str = (String) get(obj, i);
        if (str == null || str.indexOf(36) < 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        resolve(sb);
        return sb.toString();
    }

    public <T> T fetch(Object obj, Class<T> cls) {
        return BeanTool.getInstance().parse(fetch(obj), cls);
    }

    public <T> T fetch(Object obj, Class<T> cls, T t) {
        String fetch = fetch(obj);
        return fetch == null ? t : BeanTool.getInstance().parse(fetch, cls);
    }

    public <T> T fetch(Object obj, int i, Class<T> cls) {
        return BeanTool.getInstance().parse(fetch(obj, i), cls);
    }

    public <T> T fetchAll(Object obj, Class<T> cls) {
        requireArray(cls);
        T newInstance = Array.newInstance(cls.getComponentType(), length(obj));
        for (int i = 0; i < length(obj); i++) {
            Array.set(newInstance, i, BeanTool.getInstance().parse(fetch(obj, i), cls.getComponentType()));
        }
        return newInstance;
    }

    public void from(Object obj) {
        BeanTool.getInstance().inject(getDefaultBeanAccess(), obj);
    }

    public void from(Object obj, String str) {
        BeanTool.getInstance().inject(newBeanAccess(str), obj);
    }

    public <T> T get(Object obj, Class<T> cls) {
        return BeanTool.getInstance().parse((String) get(obj), cls);
    }

    public String get(Object obj, String str) {
        String str2 = (String) get(obj);
        return str2 == null ? str : str2;
    }

    public <T> T get(Object obj, Class<T> cls, T t) {
        String str = (String) get(obj);
        return str == null ? t : BeanTool.getInstance().parse(str, cls);
    }

    public <T> T get(Object obj, int i, Class<T> cls) {
        return BeanTool.getInstance().parse((String) get(obj, i), cls);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.ini4j.BasicOptionMap.put(java.lang.String, java.lang.Object):java.lang.String
      org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
      org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V */
    public String put(String str, Object obj) {
        return (String) super.put((Object) str, (Object) ((obj == null || (obj instanceof String)) ? (String) obj : String.valueOf(obj)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object, int):V
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      org.ini4j.BasicOptionMap.put(java.lang.String, java.lang.Object, int):java.lang.String
      org.ini4j.OptionMap.put(java.lang.String, java.lang.Object, int):java.lang.String
      org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object, int):V */
    public String put(String str, Object obj, int i) {
        return (String) super.put((Object) str, (Object) ((obj == null || (obj instanceof String)) ? (String) obj : String.valueOf(obj)), i);
    }

    public void putAll(String str, Object obj) {
        if (obj != null) {
            requireArray(obj.getClass());
        }
        remove(str);
        if (obj != null) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                add(str, Array.get(obj, i));
            }
        }
    }

    public void to(Object obj) {
        BeanTool.getInstance().inject(obj, getDefaultBeanAccess());
    }

    public void to(Object obj, String str) {
        BeanTool.getInstance().inject(obj, newBeanAccess(str));
    }

    /* access modifiers changed from: package-private */
    public synchronized BeanAccess getDefaultBeanAccess() {
        if (this._defaultBeanAccess == null) {
            this._defaultBeanAccess = newBeanAccess();
        }
        return this._defaultBeanAccess;
    }

    /* access modifiers changed from: package-private */
    public boolean isPropertyFirstUpper() {
        return this._propertyFirstUpper;
    }

    /* access modifiers changed from: package-private */
    public BeanAccess newBeanAccess() {
        return new Access(this);
    }

    /* access modifiers changed from: package-private */
    public BeanAccess newBeanAccess(String str) {
        return new Access(str);
    }

    /* access modifiers changed from: package-private */
    public void resolve(StringBuilder sb) {
        String str;
        Matcher matcher = EXPRESSION.matcher(sb);
        while (matcher.find()) {
            String group = matcher.group(2);
            int parseInt = matcher.group(4) == null ? -1 : Integer.parseInt(matcher.group(4));
            if (group.startsWith(ENVIRONMENT_PREFIX)) {
                str = Config.getEnvironment(group.substring(ENVIRONMENT_PREFIX_LEN));
            } else if (group.startsWith(SYSTEM_PROPERTY_PREFIX)) {
                str = Config.getSystemProperty(group.substring(SYSTEM_PROPERTY_PREFIX_LEN));
            } else {
                str = parseInt == -1 ? fetch(group) : fetch(group, parseInt);
            }
            if (str != null) {
                sb.replace(matcher.start(), matcher.end(), str);
                matcher.reset(sb);
            }
        }
    }

    private void requireArray(Class cls) {
        if (!cls.isArray()) {
            throw new IllegalArgumentException("Array required");
        }
    }

    class Access implements BeanAccess {
        private final String _prefix;

        Access(BasicOptionMap basicOptionMap) {
            this(null);
        }

        Access(String str) {
            this._prefix = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          org.ini4j.BasicOptionMap.add(java.lang.String, java.lang.Object):void
          org.ini4j.OptionMap.add(java.lang.String, java.lang.Object):void
          org.ini4j.BasicMultiMap.add(java.lang.Object, java.lang.Object):void */
        public void propAdd(String str, String str2) {
            BasicOptionMap.this.add((Object) transform(str), (Object) str2);
        }

        public String propDel(String str) {
            return (String) BasicOptionMap.this.remove(transform(str));
        }

        public String propGet(String str) {
            return BasicOptionMap.this.fetch(transform(str));
        }

        public String propGet(String str, int i) {
            return BasicOptionMap.this.fetch(transform(str), i);
        }

        public int propLength(String str) {
            return BasicOptionMap.this.length(transform(str));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V
         arg types: [java.lang.String, java.lang.String]
         candidates:
          org.ini4j.BasicOptionMap.put(java.lang.String, java.lang.Object):java.lang.String
          org.ini4j.OptionMap.put(java.lang.String, java.lang.Object):java.lang.String
          org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object):V */
        public String propSet(String str, String str2) {
            return (String) BasicOptionMap.this.put((Object) transform(str), (Object) str2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object, int):V
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          org.ini4j.BasicOptionMap.put(java.lang.String, java.lang.Object, int):java.lang.String
          org.ini4j.OptionMap.put(java.lang.String, java.lang.Object, int):java.lang.String
          org.ini4j.BasicMultiMap.put(java.lang.Object, java.lang.Object, int):V */
        public String propSet(String str, String str2, int i) {
            return (String) BasicOptionMap.this.put((Object) transform(str), (Object) str2, i);
        }

        private String transform(String str) {
            if ((this._prefix == null && !BasicOptionMap.this.isPropertyFirstUpper()) || str == null) {
                return str;
            }
            StringBuilder sb = new StringBuilder();
            String str2 = this._prefix;
            if (str2 != null) {
                sb.append(str2);
            }
            if (BasicOptionMap.this.isPropertyFirstUpper()) {
                sb.append(Character.toUpperCase(str.charAt(0)));
                sb.append(str.substring(1));
            } else {
                sb.append(str);
            }
            return sb.toString();
        }
    }
}
