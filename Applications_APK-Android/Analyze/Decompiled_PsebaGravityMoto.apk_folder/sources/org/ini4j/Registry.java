package org.ini4j;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import org.ini4j.Profile;

public interface Registry extends Profile {
    public static final char ESCAPE_CHAR = '\\';
    public static final Charset FILE_ENCODING = Charset.forName("UnicodeLittle");
    public static final char KEY_SEPARATOR = '\\';
    public static final String LINE_SEPARATOR = "\r\n";
    public static final char TYPE_SEPARATOR = ':';
    public static final String VERSION = "Windows Registry Editor Version 5.00";

    public enum Hive {
        HKEY_CLASSES_ROOT,
        HKEY_CURRENT_CONFIG,
        HKEY_CURRENT_USER,
        HKEY_LOCAL_MACHINE,
        HKEY_USERS
    }

    public interface Key extends Profile.Section {
        public static final String DEFAULT_NAME = "@";

        Key addChild(String str);

        Key getChild(String str);

        Key getParent();

        Type getType(Object obj);

        Type getType(Object obj, Type type);

        Key lookup(String... strArr);

        Type putType(String str, Type type);

        Type removeType(Object obj);
    }

    Key get(Object obj);

    Key get(Object obj, int i);

    String getVersion();

    Key put(String str, Profile.Section section);

    Key put(String str, Profile.Section section, int i);

    Key remove(Object obj);

    Key remove(Object obj, int i);

    void setVersion(String str);

    public enum Type {
        REG_NONE("hex(0)"),
        REG_SZ(""),
        REG_EXPAND_SZ("hex(2)"),
        REG_BINARY("hex"),
        REG_DWORD("dword"),
        REG_DWORD_BIG_ENDIAN("hex(5)"),
        REG_LINK("hex(6)"),
        REG_MULTI_SZ("hex(7)"),
        REG_RESOURCE_LIST("hex(8)"),
        REG_FULL_RESOURCE_DESCRIPTOR("hex(9)"),
        REG_RESOURCE_REQUIREMENTS_LIST("hex(a)"),
        REG_QWORD("hex(b)");
        
        private static final Map<String, Type> MAPPING = new HashMap();
        public static final String REMOVE = String.valueOf((char) REMOVE_CHAR);
        public static final char REMOVE_CHAR = '-';
        public static final String SEPARATOR = String.valueOf(':');
        public static final char SEPARATOR_CHAR = ':';
        private final String _prefix;

        static {
            for (Type type : values()) {
                MAPPING.put(type.toString(), type);
            }
        }

        private Type(String str) {
            this._prefix = str;
        }

        public static Type fromString(String str) {
            return MAPPING.get(str);
        }

        public String toString() {
            return this._prefix;
        }
    }
}
