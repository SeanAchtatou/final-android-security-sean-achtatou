package org.ini4j;

import org.ini4j.Registry;

class BasicRegistryKey extends BasicProfileSection implements Registry.Key {
    private static final String META_TYPE = "type";
    private static final long serialVersionUID = -1390060044244350928L;

    public BasicRegistryKey(BasicRegistry basicRegistry, String str) {
        super(basicRegistry, str);
    }

    public Registry.Key getChild(String str) {
        return (Registry.Key) super.getChild(str);
    }

    public Registry.Key getParent() {
        return (Registry.Key) super.getParent();
    }

    public Registry.Type getType(Object obj) {
        return (Registry.Type) getMeta(META_TYPE, obj);
    }

    public Registry.Type getType(Object obj, Registry.Type type) {
        Registry.Type type2 = getType(obj);
        return type2 == null ? type : type2;
    }

    public Registry.Key addChild(String str) {
        return (Registry.Key) super.addChild(str);
    }

    public Registry.Key lookup(String... strArr) {
        return (Registry.Key) super.lookup(strArr);
    }

    public Registry.Type putType(String str, Registry.Type type) {
        return (Registry.Type) putMeta(META_TYPE, str, type);
    }

    public Registry.Type removeType(Object obj) {
        return (Registry.Type) removeMeta(META_TYPE, obj);
    }
}
