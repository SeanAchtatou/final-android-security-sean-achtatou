package org.apache.http.impl.auth;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.util.CharArrayBuffer;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.Oid;

public class NegotiateScheme extends AuthSchemeBase {
    private static final String KERBEROS_OID = "1.2.840.113554.1.2.2";
    private static final String SPNEGO_OID = "1.3.6.1.5.5.2";
    private GSSContext gssContext;
    private final Log log;
    private Oid negotiationOid;
    private final SpnegoTokenGenerator spengoGenerator;
    private State state;
    private final boolean stripPort;
    private byte[] token;

    enum State {
        UNINITIATED,
        CHALLENGE_RECEIVED,
        TOKEN_GENERATED,
        FAILED
    }

    public String getRealm() {
        return null;
    }

    public String getSchemeName() {
        return "Negotiate";
    }

    public boolean isConnectionBased() {
        return true;
    }

    public NegotiateScheme(SpnegoTokenGenerator spnegoTokenGenerator, boolean z) {
        this.log = LogFactory.getLog(getClass());
        this.gssContext = null;
        this.negotiationOid = null;
        this.state = State.UNINITIATED;
        this.spengoGenerator = spnegoTokenGenerator;
        this.stripPort = z;
    }

    public NegotiateScheme(SpnegoTokenGenerator spnegoTokenGenerator) {
        this(spnegoTokenGenerator, false);
    }

    public NegotiateScheme() {
        this(null, false);
    }

    public boolean isComplete() {
        return this.state == State.TOKEN_GENERATED || this.state == State.FAILED;
    }

    @Deprecated
    public Header authenticate(Credentials credentials, HttpRequest httpRequest) throws AuthenticationException {
        return authenticate(credentials, httpRequest, null);
    }

    /* access modifiers changed from: protected */
    public GSSManager getManager() {
        return GSSManager.getInstance();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0173, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0174, code lost:
        r5.state = org.apache.http.impl.auth.NegotiateScheme.State.FAILED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0181, code lost:
        throw new org.apache.http.auth.AuthenticationException(r6.getMessage());
     */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0173 A[ExcHandler: IOException (r6v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:4:0x000a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.http.Header authenticate(org.apache.http.auth.Credentials r6, org.apache.http.HttpRequest r7, org.apache.http.protocol.HttpContext r8) throws org.apache.http.auth.AuthenticationException {
        /*
            r5 = this;
            java.lang.String r6 = "HTTP@"
            if (r7 == 0) goto L_0x01e8
            org.apache.http.impl.auth.NegotiateScheme$State r7 = r5.state
            org.apache.http.impl.auth.NegotiateScheme$State r0 = org.apache.http.impl.auth.NegotiateScheme.State.CHALLENGE_RECEIVED
            if (r7 != r0) goto L_0x01e0
            boolean r7 = r5.isProxy()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r7 == 0) goto L_0x0013
            java.lang.String r7 = "http.proxy_host"
            goto L_0x0015
        L_0x0013:
            java.lang.String r7 = "http.target_host"
        L_0x0015:
            java.lang.Object r7 = r8.getAttribute(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.apache.http.HttpHost r7 = (org.apache.http.HttpHost) r7     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r7 == 0) goto L_0x016b
            boolean r8 = r5.stripPort     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r8 != 0) goto L_0x002c
            int r8 = r7.getPort()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r8 <= 0) goto L_0x002c
            java.lang.String r7 = r7.toHostString()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            goto L_0x0030
        L_0x002c:
            java.lang.String r7 = r7.getHostName()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x0030:
            org.apache.commons.logging.Log r8 = r5.log     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            boolean r8 = r8.isDebugEnabled()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r8 == 0) goto L_0x004e
            org.apache.commons.logging.Log r8 = r5.log     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r0.<init>()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r1 = "init "
            r0.append(r1)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r0.append(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r0 = r0.toString()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r8.debug(r0)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x004e:
            org.ietf.jgss.Oid r8 = new org.ietf.jgss.Oid     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r0 = "1.3.6.1.5.5.2"
            r8.<init>(r0)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.negotiationOid = r8     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r8 = 0
            r0 = 1
            r1 = 0
            org.ietf.jgss.GSSManager r2 = r5.getManager()     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            r3.<init>()     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            r3.append(r6)     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            r3.append(r7)     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            java.lang.String r3 = r3.toString()     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.Oid r4 = org.ietf.jgss.GSSName.NT_HOSTBASED_SERVICE     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.GSSName r3 = r2.createName(r3, r4)     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.Oid r4 = r5.negotiationOid     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.GSSName r3 = r3.canonicalize(r4)     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.Oid r4 = r5.negotiationOid     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.GSSContext r2 = r2.createContext(r3, r4, r8, r1)     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            r5.gssContext = r2     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.GSSContext r2 = r5.gssContext     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            r2.requestMutualAuth(r0)     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            org.ietf.jgss.GSSContext r2 = r5.gssContext     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            r2.requestCredDeleg(r0)     // Catch:{ GSSException -> 0x008d, IOException -> 0x0173 }
            r2 = 0
            goto L_0x009d
        L_0x008d:
            r2 = move-exception
            int r3 = r2.getMajor()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r4 = 2
            if (r3 != r4) goto L_0x016a
            org.apache.commons.logging.Log r2 = r5.log     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r3 = "GSSException BAD_MECH, retry with Kerberos MECH"
            r2.debug(r3)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r2 = 1
        L_0x009d:
            java.lang.String r3 = "1.2.840.113554.1.2.2"
            if (r2 == 0) goto L_0x00e0
            org.apache.commons.logging.Log r2 = r5.log     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r4 = "Using Kerberos MECH 1.2.840.113554.1.2.2"
            r2.debug(r4)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.Oid r2 = new org.ietf.jgss.Oid     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r2.<init>(r3)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.negotiationOid = r2     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.GSSManager r2 = r5.getManager()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r4.<init>()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r4.append(r6)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r4.append(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r6 = r4.toString()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.Oid r7 = org.ietf.jgss.GSSName.NT_HOSTBASED_SERVICE     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.GSSName r6 = r2.createName(r6, r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.Oid r7 = r5.negotiationOid     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.GSSName r6 = r6.canonicalize(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.Oid r7 = r5.negotiationOid     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.GSSContext r6 = r2.createContext(r6, r7, r8, r1)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.gssContext = r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.GSSContext r6 = r5.gssContext     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r6.requestMutualAuth(r0)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.ietf.jgss.GSSContext r6 = r5.gssContext     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r6.requestCredDeleg(r0)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x00e0:
            byte[] r6 = r5.token     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r6 != 0) goto L_0x00e8
            byte[] r6 = new byte[r1]     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.token = r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x00e8:
            org.ietf.jgss.GSSContext r6 = r5.gssContext     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r7 = r5.token     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r8 = r5.token     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            int r8 = r8.length     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r6 = r6.initSecContext(r7, r1, r8)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.token = r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r6 = r5.token     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r6 == 0) goto L_0x015e
            org.apache.http.impl.auth.SpnegoTokenGenerator r6 = r5.spengoGenerator     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r6 == 0) goto L_0x0113
            org.ietf.jgss.Oid r6 = r5.negotiationOid     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r6 = r6.toString()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            boolean r6 = r6.equals(r3)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r6 == 0) goto L_0x0113
            org.apache.http.impl.auth.SpnegoTokenGenerator r6 = r5.spengoGenerator     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r7 = r5.token     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r6 = r6.generateSpnegoDERObject(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.token = r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x0113:
            org.apache.http.impl.auth.NegotiateScheme$State r6 = org.apache.http.impl.auth.NegotiateScheme.State.TOKEN_GENERATED     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.state = r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r6 = new java.lang.String     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r7 = r5.token     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            byte[] r7 = org.apache.commons.codec.binary.Base64.encodeBase64(r7, r1)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r6.<init>(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.apache.commons.logging.Log r7 = r5.log     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            boolean r7 = r7.isDebugEnabled()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            if (r7 == 0) goto L_0x0145
            org.apache.commons.logging.Log r7 = r5.log     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r8.<init>()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r0 = "Sending response '"
            r8.append(r0)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r8.append(r6)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r0 = "' back to the auth server"
            r8.append(r0)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r8 = r8.toString()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r7.debug(r8)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x0145:
            org.apache.http.message.BasicHeader r7 = new org.apache.http.message.BasicHeader     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r8 = "Authorization"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r0.<init>()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r1 = "Negotiate "
            r0.append(r1)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r0.append(r6)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r6 = r0.toString()     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r7.<init>(r8, r6)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            return r7
        L_0x015e:
            org.apache.http.impl.auth.NegotiateScheme$State r6 = org.apache.http.impl.auth.NegotiateScheme.State.FAILED     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            r5.state = r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            org.apache.http.auth.AuthenticationException r6 = new org.apache.http.auth.AuthenticationException     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r7 = "GSS security context initialization failed"
            r6.<init>(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            throw r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x016a:
            throw r2     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x016b:
            org.apache.http.auth.AuthenticationException r6 = new org.apache.http.auth.AuthenticationException     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            java.lang.String r7 = "Authentication host is not set in the execution context"
            r6.<init>(r7)     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
            throw r6     // Catch:{ GSSException -> 0x0182, IOException -> 0x0173 }
        L_0x0173:
            r6 = move-exception
            org.apache.http.impl.auth.NegotiateScheme$State r7 = org.apache.http.impl.auth.NegotiateScheme.State.FAILED
            r5.state = r7
            org.apache.http.auth.AuthenticationException r7 = new org.apache.http.auth.AuthenticationException
            java.lang.String r6 = r6.getMessage()
            r7.<init>(r6)
            throw r7
        L_0x0182:
            r6 = move-exception
            org.apache.http.impl.auth.NegotiateScheme$State r7 = org.apache.http.impl.auth.NegotiateScheme.State.FAILED
            r5.state = r7
            int r7 = r6.getMajor()
            r8 = 9
            if (r7 == r8) goto L_0x01d6
            int r7 = r6.getMajor()
            r8 = 8
            if (r7 == r8) goto L_0x01d6
            int r7 = r6.getMajor()
            r8 = 13
            if (r7 == r8) goto L_0x01cc
            int r7 = r6.getMajor()
            r8 = 10
            if (r7 == r8) goto L_0x01c2
            int r7 = r6.getMajor()
            r8 = 19
            if (r7 == r8) goto L_0x01c2
            int r7 = r6.getMajor()
            r8 = 20
            if (r7 != r8) goto L_0x01b8
            goto L_0x01c2
        L_0x01b8:
            org.apache.http.auth.AuthenticationException r7 = new org.apache.http.auth.AuthenticationException
            java.lang.String r6 = r6.getMessage()
            r7.<init>(r6)
            throw r7
        L_0x01c2:
            org.apache.http.auth.AuthenticationException r7 = new org.apache.http.auth.AuthenticationException
            java.lang.String r8 = r6.getMessage()
            r7.<init>(r8, r6)
            throw r7
        L_0x01cc:
            org.apache.http.auth.InvalidCredentialsException r7 = new org.apache.http.auth.InvalidCredentialsException
            java.lang.String r8 = r6.getMessage()
            r7.<init>(r8, r6)
            throw r7
        L_0x01d6:
            org.apache.http.auth.InvalidCredentialsException r7 = new org.apache.http.auth.InvalidCredentialsException
            java.lang.String r8 = r6.getMessage()
            r7.<init>(r8, r6)
            throw r7
        L_0x01e0:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "Negotiation authentication process has not been initiated"
            r6.<init>(r7)
            throw r6
        L_0x01e8:
            java.lang.IllegalArgumentException r6 = new java.lang.IllegalArgumentException
            java.lang.String r7 = "HTTP request may not be null"
            r6.<init>(r7)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.auth.NegotiateScheme.authenticate(org.apache.http.auth.Credentials, org.apache.http.HttpRequest, org.apache.http.protocol.HttpContext):org.apache.http.Header");
    }

    public String getParameter(String str) {
        if (str != null) {
            return null;
        }
        throw new IllegalArgumentException("Parameter name may not be null");
    }

    /* access modifiers changed from: protected */
    public void parseChallenge(CharArrayBuffer charArrayBuffer, int i, int i2) throws MalformedChallengeException {
        String substringTrimmed = charArrayBuffer.substringTrimmed(i, i2);
        if (this.log.isDebugEnabled()) {
            Log log2 = this.log;
            log2.debug("Received challenge '" + substringTrimmed + "' from the auth server");
        }
        if (this.state == State.UNINITIATED) {
            this.token = new Base64().decode(substringTrimmed.getBytes());
            this.state = State.CHALLENGE_RECEIVED;
            return;
        }
        this.log.debug("Authentication already attempted");
        this.state = State.FAILED;
    }
}
