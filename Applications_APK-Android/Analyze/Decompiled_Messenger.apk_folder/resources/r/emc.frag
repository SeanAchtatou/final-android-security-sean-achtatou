// The iOS version of this shader is the source of truth.

precision mediump float;

uniform sampler2D u_Texture;
varying vec2 v_TexCoordinate;

uniform float brightness;
uniform float contrast;
uniform float saturation;
uniform float temperature;
uniform float TOOL_ON_EPSILON;

vec3 rgb_to_hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
    vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv_to_rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// magnitude can be around the range ~ -1.0 -> 1.0
vec3 bowRgbChannels(vec3 inVal, float mag) {

    vec3 outVal;
    float power = 1.0 + abs(mag);

    if (mag < 0.0) {
        power = 1.0 / power;
    }

    // a bow function that uses a "power curve" to bow the value
    // we flip it so it does more on the high end.
    outVal.r = 1.0 - pow((1.0 - inVal.r), power);
    outVal.g = 1.0 - pow((1.0 - inVal.g), power);
    outVal.b = 1.0 - pow((1.0 - inVal.b), power);

    return outVal;

}

float getLuma(vec3 rgbP) {
    return  (0.299 * rgbP.r) +
    (0.587 * rgbP.g) +
    (0.114 * rgbP.b);
}

vec3 rgbToYuv(vec3 inP) {
    vec3 outP;
    outP.r = getLuma(inP);
    outP.g = (1.0/1.772)*(inP.b - outP.r);
    outP.b = (1.0/1.402)*(inP.r - outP.r);
    return outP;
}

vec3 yuvToRgb(vec3 inP) {
    float y = inP.r;
    float u = inP.g;
    float v = inP.b;
    vec3 outP;
    outP.r = 1.402 * v + y;
    outP.g = (y - (0.299 * 1.402 / 0.587) * v -
             (0.114 * 1.772 / 0.587) * u);
    outP.b = 1.772 * u + y;
    return outP;
}

vec3 adjustTemperature(float tempDelta, vec3 inRgb) {
    // we're adjusting the temperature by shifting the chroma channels in yuv space.
    vec3 yuvVec;
    // XXX TODO: optimization, move yuvVec to rgbSpace if we use the same curveScale per channel in yuv space.

    if (tempDelta > 0.0 ) {
        // "warm" midtone change
        yuvVec =  vec3(0.1765, -0.1255, 0.0902);
    } else {
        // "cool" midtone change
        yuvVec = -vec3(0.0588,  0.1569, -0.1255);
    }
    vec3 yuvColor = rgbToYuv(inRgb);

    float luma = yuvColor.r;

    float curveScale = sin(luma * 3.14159); // a hump

    yuvColor += 0.375 * tempDelta * curveScale * yuvVec;
    inRgb = yuvToRgb(yuvColor);
    return inRgb;
}

// Acceptable ranges for strength are (-1, 1)
// Sample output for strength = 0.4: https://www.latest.facebook.com/pxlcld/l743
// When approaching the 1/-1 value the curve approaches a step function
// A negative strength produces an easeOutIn curve.
float easeInOutSigmoid(float value, float strength) {
    float t = 1.0 / (1.0 - strength);
    if (value > 0.5) {
        return 1.0 - pow(2.0 - 2.0 * value, t) * 0.5;
    } else {
        return pow(2.0 * value, t) * 0.5;
    }
}

vec3 basicAdjust(vec3 texel)
{
    // we're in HSV space for the next bunch of operations
    vec3 hsv = rgb_to_hsv(texel.rgb);

    // saturation, scale from -1->1 to 50% max adjustment
    if (abs(saturation) > TOOL_ON_EPSILON) {
       float saturationFactor = 1.0 + saturation;
       hsv.y = hsv.y * saturationFactor;
       hsv.y = clamp(hsv.y, 0.0, 1.0);
    }

    texel.rgb = hsv_to_rgb(hsv);

    // contrast
    if (abs(contrast) > TOOL_ON_EPSILON) {
       float strength = contrast * 0.5; // adjust range to useful values

       vec3 yuv = rgbToYuv(texel.rgb);
       yuv.x = easeInOutSigmoid(yuv.x, strength);
       yuv.y = easeInOutSigmoid(yuv.y + 0.5, strength * 0.65) - 0.5;
       yuv.z = easeInOutSigmoid(yuv.z + 0.5, strength * 0.65) - 0.5;
       texel.rgb = yuvToRgb(yuv);
    }

    // brightness, scale exponent from
    if (abs(brightness) > TOOL_ON_EPSILON) {
       texel.rgb = clamp(texel.rgb, 0.0, 1.0);
       texel.rgb = bowRgbChannels(texel.rgb, brightness * 1.1);
    }

    // temperature
    if (abs(temperature) > TOOL_ON_EPSILON) {
       texel.rgb = adjustTemperature(temperature, texel.rgb);
    }

    return texel;
}

void main(void)
{
    vec4 texel = texture2D(u_Texture, v_TexCoordinate);
    texel.rgb = basicAdjust(texel.rgb);
    gl_FragColor = texel;
}
