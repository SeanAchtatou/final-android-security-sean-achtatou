package com.facebook.debug.activitytracer;

import X.AnonymousClass08S;
import X.AnonymousClass0UX;
import X.AnonymousClass0WD;
import X.AnonymousClass0WP;
import X.AnonymousClass0X5;
import X.AnonymousClass0X6;
import X.AnonymousClass0j4;
import X.AnonymousClass0u3;
import X.AnonymousClass1BT;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y6;
import X.AnonymousClass24B;
import X.AnonymousClass2Re;
import X.C010708t;
import X.C02190Di;
import X.C02240Ds;
import X.C25091Yh;
import X.C45702Nd;
import X.C98644nb;
import android.os.Handler;
import android.os.Looper;
import com.facebook.common.util.TriState;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
public class ActivityTracer {
    private static volatile ActivityTracer A06;
    public C45702Nd A00;
    public final Handler A01 = new Handler(Looper.getMainLooper());
    public final AnonymousClass1Y6 A02;
    public final Set A03;
    private final AnonymousClass0WP A04;
    private final AnonymousClass1BT A05;

    public static final ActivityTracer A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (ActivityTracer.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new ActivityTracer(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static boolean A01(ActivityTracer activityTracer, String str) {
        boolean z = false;
        if (activityTracer.A00 != null) {
            z = true;
        }
        Preconditions.checkState(z);
        if (activityTracer.A04.BDj()) {
            if (activityTracer.A04.BHL()) {
                if (str == "draw") {
                    C45702Nd r4 = activityTracer.A00;
                    if (!r4.A07) {
                        long j = r4.A00;
                        r4.A07 = true;
                        r4.A01(AnonymousClass08S.A0J("UILoadWait:", Long.toString(j)));
                    }
                }
            } else if (activityTracer.A04.BFP()) {
                C45702Nd r42 = activityTracer.A00;
                r42.A00 = C02240Ds.A00(r42.A09, 0, false) / 1000000;
                C02240Ds r43 = r42.A09;
                r43.A03.A02(r43.A01, 3, C02190Di.A00(ActivityTracer.class));
                Iterator it = activityTracer.A03.iterator();
                while (it.hasNext()) {
                    it.next();
                    C45702Nd r2 = activityTracer.A00;
                    r2.A08.AOz();
                    DataFetchDisposition dataFetchDisposition = (DataFetchDisposition) r2.A0A.get("data_fetch_disposition");
                    if (dataFetchDisposition != null) {
                        r2.A08.AOz();
                        r2.A0A.remove("data_fetch_disposition");
                        r2.A04("data_fetch_disposition_succeeded", true);
                        r2.A04("data_fetch_disposition_has_data", Boolean.valueOf(dataFetchDisposition.A08));
                        TriState triState = dataFetchDisposition.A04;
                        if (triState.isSet()) {
                            r2.A04("data_fetch_disposition_stale_data", Boolean.valueOf(triState.asBoolean()));
                        }
                        AnonymousClass0u3 r0 = dataFetchDisposition.A07;
                        if (r0 != null) {
                            r2.A04("data_fetch_disposition_data_source", r0.toString());
                        }
                        TriState triState2 = dataFetchDisposition.A06;
                        if (triState2.isSet()) {
                            r2.A04("data_fetch_disposition_synchronous_fetch", Boolean.valueOf(triState2.asBoolean()));
                        }
                        TriState triState3 = dataFetchDisposition.A03;
                        if (triState3.isSet()) {
                            r2.A04("data_fetch_disposition_incomplete_data", Boolean.valueOf(triState3.asBoolean()));
                        }
                        TriState triState4 = dataFetchDisposition.A01;
                        if (triState4.isSet()) {
                            r2.A04("data_fetch_disposition_server_error_fallback", Boolean.valueOf(triState4.asBoolean()));
                        }
                    }
                    String $const$string = AnonymousClass24B.$const$string(AnonymousClass1Y3.A7n);
                    r2.A08.AOz();
                    Boolean bool = (Boolean) r2.A0A.get($const$string);
                    if (bool != null) {
                        r2.A04($const$string, bool);
                    }
                    if (C010708t.A0X(3)) {
                        r2.A08.AOz();
                        Map map = r2.A0A;
                        if (!map.isEmpty()) {
                            Joiner.on(", ").join(AnonymousClass0j4.A00(map.entrySet(), new C98644nb()));
                        }
                    }
                }
                activityTracer.A00 = null;
                return true;
            }
        }
        return false;
    }

    public C45702Nd A02(String str, String str2) {
        this.A02.AOz();
        if (this.A00 != null) {
            return null;
        }
        this.A00 = new C45702Nd(this.A05, C02240Ds.A01(str2, null), str);
        this.A02.C4C(new AnonymousClass2Re(this));
        return this.A00;
    }

    private ActivityTracer(AnonymousClass1XY r3) {
        this.A02 = AnonymousClass0UX.A07(r3);
        this.A04 = C25091Yh.A00(r3);
        this.A05 = new AnonymousClass1BT(r3);
        this.A03 = new AnonymousClass0X5(r3, AnonymousClass0X6.A0o);
    }
}
