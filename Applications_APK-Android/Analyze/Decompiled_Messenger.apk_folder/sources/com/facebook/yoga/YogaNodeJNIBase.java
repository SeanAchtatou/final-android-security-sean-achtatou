package com.facebook.yoga;

import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass10G;
import X.AnonymousClass10W;
import X.AnonymousClass10X;
import X.AnonymousClass119;
import X.AnonymousClass1K1;
import X.AnonymousClass1LC;
import X.AnonymousClass22K;
import X.AnonymousClass2N4;
import X.AnonymousClass6I0;
import X.C14940uO;
import X.C14950uP;
import X.C17660zG;
import java.util.ArrayList;
import java.util.List;

public abstract class YogaNodeJNIBase extends AnonymousClass10W implements Cloneable {
    private float[] arr;
    private AnonymousClass22K mBaselineFunction;
    public List mChildren;
    private Object mData;
    private boolean mHasNewLayout;
    private int mLayoutDirection;
    public AnonymousClass119 mMeasureFunction;
    public long mNativePointer;
    public YogaNodeJNIBase mOwner;

    public void reset() {
        this.mMeasureFunction = null;
        this.mBaselineFunction = null;
        this.mData = null;
        this.arr = null;
        this.mHasNewLayout = true;
        this.mLayoutDirection = 0;
        YogaNative.jni_YGNodeResetJNI(this.mNativePointer);
    }

    public static Integer A00(int i) {
        if (i == 0) {
            return AnonymousClass07B.A00;
        }
        if (i == 1) {
            return AnonymousClass07B.A01;
        }
        if (i == 2) {
            return AnonymousClass07B.A0C;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", i));
    }

    private final long replaceChild(YogaNodeJNIBase yogaNodeJNIBase, int i) {
        List list = this.mChildren;
        if (list != null) {
            list.remove(i);
            this.mChildren.add(i, yogaNodeJNIBase);
            yogaNodeJNIBase.mOwner = this;
            return yogaNodeJNIBase.mNativePointer;
        }
        throw new IllegalStateException("Cannot replace child. YogaNode does not have children");
    }

    private static AnonymousClass1K1 valueFromLong(long j) {
        Integer num;
        float intBitsToFloat = Float.intBitsToFloat((int) j);
        int i = (int) (j >> 32);
        if (i == 0) {
            num = AnonymousClass07B.A00;
        } else if (i == 1) {
            num = AnonymousClass07B.A01;
        } else if (i == 2) {
            num = AnonymousClass07B.A0C;
        } else if (i == 3) {
            num = AnonymousClass07B.A0N;
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", i));
        }
        return new AnonymousClass1K1(intBitsToFloat, num);
    }

    public void addChildAt(AnonymousClass10W r5, int i) {
        YogaNodeJNIBase yogaNodeJNIBase = (YogaNodeJNIBase) r5;
        if (yogaNodeJNIBase.mOwner == null) {
            if (this.mChildren == null) {
                this.mChildren = new ArrayList(4);
            }
            this.mChildren.add(i, yogaNodeJNIBase);
            yogaNodeJNIBase.mOwner = this;
            YogaNative.jni_YGNodeInsertChildJNI(this.mNativePointer, yogaNodeJNIBase.mNativePointer, i);
            return;
        }
        throw new IllegalStateException("Child already has a parent, it must be removed first.");
    }

    public final float baseline(float f, float f2) {
        return this.mBaselineFunction.APg(this, f, f2);
    }

    public void calculateLayout(float f, float f2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(this);
        for (int i = 0; i < arrayList.size(); i++) {
            List list = ((YogaNodeJNIBase) arrayList.get(i)).mChildren;
            if (list != null) {
                arrayList.addAll(list);
            }
        }
        YogaNodeJNIBase[] yogaNodeJNIBaseArr = (YogaNodeJNIBase[]) arrayList.toArray(new YogaNodeJNIBase[arrayList.size()]);
        int length = yogaNodeJNIBaseArr.length;
        long[] jArr = new long[length];
        for (int i2 = 0; i2 < length; i2++) {
            jArr[i2] = yogaNodeJNIBaseArr[i2].mNativePointer;
        }
        YogaNative.jni_YGNodeCalculateLayoutJNI(this.mNativePointer, f, f2, jArr, yogaNodeJNIBaseArr);
    }

    public void copyStyle(AnonymousClass10W r5) {
        YogaNative.jni_YGNodeCopyStyleJNI(this.mNativePointer, ((YogaNodeJNIBase) r5).mNativePointer);
    }

    public void dirty() {
        YogaNative.jni_YGNodeMarkDirtyJNI(this.mNativePointer);
    }

    public /* bridge */ /* synthetic */ AnonymousClass10W getChildAt(int i) {
        List list = this.mChildren;
        if (list != null) {
            return (YogaNodeJNIBase) list.get(i);
        }
        throw new IllegalStateException("YogaNode does not have children");
    }

    public int getChildCount() {
        List list = this.mChildren;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public Object getData() {
        return this.mData;
    }

    public AnonymousClass2N4 getDisplay() {
        int jni_YGNodeStyleGetDisplayJNI = YogaNative.jni_YGNodeStyleGetDisplayJNI(this.mNativePointer);
        if (jni_YGNodeStyleGetDisplayJNI == 0) {
            return AnonymousClass2N4.A00;
        }
        if (jni_YGNodeStyleGetDisplayJNI == 1) {
            return AnonymousClass2N4.A01;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", jni_YGNodeStyleGetDisplayJNI));
    }

    public AnonymousClass10X getFlexDirection() {
        return AnonymousClass10X.A00(YogaNative.jni_YGNodeStyleGetFlexDirectionJNI(this.mNativePointer));
    }

    public AnonymousClass1K1 getHeight() {
        return valueFromLong(YogaNative.jni_YGNodeStyleGetHeightJNI(this.mNativePointer));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
        if (getLayoutDirection() == X.C17660zG.RTL) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        return r6[r3 + 2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003d, code lost:
        if (getLayoutDirection() == X.C17660zG.RTL) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        return r6[r3];
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float getLayoutBorder(X.AnonymousClass10G r9) {
        /*
            r8 = this;
            float[] r6 = r8.arr
            if (r6 == 0) goto L_0x004b
            r7 = 0
            r0 = r6[r7]
            int r5 = (int) r0
            r1 = 4
            r0 = r5 & r1
            if (r0 != r1) goto L_0x004b
            r4 = 1
            r1 = r5 & r4
            r0 = 4
            if (r1 != r4) goto L_0x0014
            r0 = 0
        L_0x0014:
            int r3 = 14 - r0
            r2 = 2
            r5 = r5 & r2
            if (r5 == r2) goto L_0x001b
            r7 = 4
        L_0x001b:
            int r3 = r3 - r7
            int r0 = r9.ordinal()
            switch(r0) {
                case 0: goto L_0x0048;
                case 1: goto L_0x0047;
                case 2: goto L_0x0045;
                case 3: goto L_0x0042;
                case 4: goto L_0x002b;
                case 5: goto L_0x0037;
                default: goto L_0x0023;
            }
        L_0x0023:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Cannot get layout border of multi-edge shorthands"
            r1.<init>(r0)
            throw r1
        L_0x002b:
            X.0zG r1 = r8.getLayoutDirection()
            X.0zG r0 = X.C17660zG.RTL
            if (r1 != r0) goto L_0x003f
        L_0x0033:
            int r3 = r3 + r2
            r0 = r6[r3]
            return r0
        L_0x0037:
            X.0zG r1 = r8.getLayoutDirection()
            X.0zG r0 = X.C17660zG.RTL
            if (r1 != r0) goto L_0x0033
        L_0x003f:
            r0 = r6[r3]
            return r0
        L_0x0042:
            int r3 = r3 + 3
            goto L_0x0048
        L_0x0045:
            int r3 = r3 + r2
            goto L_0x0048
        L_0x0047:
            int r3 = r3 + r4
        L_0x0048:
            r0 = r6[r3]
            return r0
        L_0x004b:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.yoga.YogaNodeJNIBase.getLayoutBorder(X.10G):float");
    }

    public C17660zG getLayoutDirection() {
        int i;
        float[] fArr = this.arr;
        if (fArr != null) {
            i = (int) fArr[5];
        } else {
            i = this.mLayoutDirection;
        }
        return C17660zG.A00(i);
    }

    public float getLayoutHeight() {
        float[] fArr = this.arr;
        if (fArr != null) {
            return fArr[2];
        }
        return 0.0f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        if (getLayoutDirection() == X.C17660zG.RTL) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return r5[r2 + 2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (getLayoutDirection() == X.C17660zG.RTL) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
        return r5[r2];
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float getLayoutPadding(X.AnonymousClass10G r7) {
        /*
            r6 = this;
            float[] r5 = r6.arr
            if (r5 == 0) goto L_0x0043
            r4 = 0
            r0 = r5[r4]
            int r2 = (int) r0
            r3 = 2
            r0 = r2 & r3
            if (r0 != r3) goto L_0x0043
            r1 = 1
            r2 = r2 & r1
            if (r2 == r1) goto L_0x0012
            r4 = 4
        L_0x0012:
            int r2 = 10 - r4
            int r0 = r7.ordinal()
            switch(r0) {
                case 0: goto L_0x0040;
                case 1: goto L_0x003f;
                case 2: goto L_0x003d;
                case 3: goto L_0x003a;
                case 4: goto L_0x0023;
                case 5: goto L_0x002f;
                default: goto L_0x001b;
            }
        L_0x001b:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Cannot get layout paddings of multi-edge shorthands"
            r1.<init>(r0)
            throw r1
        L_0x0023:
            X.0zG r1 = r6.getLayoutDirection()
            X.0zG r0 = X.C17660zG.RTL
            if (r1 != r0) goto L_0x0037
        L_0x002b:
            int r2 = r2 + r3
            r0 = r5[r2]
            return r0
        L_0x002f:
            X.0zG r1 = r6.getLayoutDirection()
            X.0zG r0 = X.C17660zG.RTL
            if (r1 != r0) goto L_0x002b
        L_0x0037:
            r0 = r5[r2]
            return r0
        L_0x003a:
            int r2 = r2 + 3
            goto L_0x0040
        L_0x003d:
            int r2 = r2 + r3
            goto L_0x0040
        L_0x003f:
            int r2 = r2 + r1
        L_0x0040:
            r0 = r5[r2]
            return r0
        L_0x0043:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.yoga.YogaNodeJNIBase.getLayoutPadding(X.10G):float");
    }

    public float getLayoutWidth() {
        float[] fArr = this.arr;
        if (fArr != null) {
            return fArr[1];
        }
        return 0.0f;
    }

    public float getLayoutX() {
        float[] fArr = this.arr;
        if (fArr != null) {
            return fArr[3];
        }
        return 0.0f;
    }

    public float getLayoutY() {
        float[] fArr = this.arr;
        if (fArr != null) {
            return fArr[4];
        }
        return 0.0f;
    }

    public /* bridge */ /* synthetic */ AnonymousClass10W getOwner() {
        return this.mOwner;
    }

    public C17660zG getStyleDirection() {
        return C17660zG.A00(YogaNative.jni_YGNodeStyleGetDirectionJNI(this.mNativePointer));
    }

    public AnonymousClass1K1 getWidth() {
        return valueFromLong(YogaNative.jni_YGNodeStyleGetWidthJNI(this.mNativePointer));
    }

    public boolean hasNewLayout() {
        float[] fArr = this.arr;
        if (fArr == null) {
            return this.mHasNewLayout;
        }
        if ((((int) fArr[0]) & 16) == 16) {
            return true;
        }
        return false;
    }

    public void markLayoutSeen() {
        float[] fArr = this.arr;
        if (fArr != null) {
            fArr[0] = (float) (((int) fArr[0]) & -17);
        }
        this.mHasNewLayout = false;
    }

    public final long measure(float f, int i, float f2, int i2) {
        AnonymousClass119 r1 = this.mMeasureFunction;
        boolean z = false;
        if (r1 != null) {
            z = true;
        }
        if (z) {
            return r1.BKx(this, f, A00(i), f2, A00(i2));
        }
        throw new RuntimeException("Measure function isn't defined!");
    }

    public /* bridge */ /* synthetic */ AnonymousClass10W removeChildAt(int i) {
        List list = this.mChildren;
        if (list != null) {
            YogaNodeJNIBase yogaNodeJNIBase = (YogaNodeJNIBase) list.remove(i);
            yogaNodeJNIBase.mOwner = null;
            YogaNative.jni_YGNodeRemoveChildJNI(this.mNativePointer, yogaNodeJNIBase.mNativePointer);
            return yogaNodeJNIBase;
        }
        throw new IllegalStateException("Trying to remove a child of a YogaNode that does not have children");
    }

    public void setAlignContent(C14940uO r4) {
        YogaNative.jni_YGNodeStyleSetAlignContentJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setAlignItems(C14940uO r4) {
        YogaNative.jni_YGNodeStyleSetAlignItemsJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setAlignSelf(C14940uO r4) {
        YogaNative.jni_YGNodeStyleSetAlignSelfJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setAspectRatio(float f) {
        YogaNative.jni_YGNodeStyleSetAspectRatioJNI(this.mNativePointer, f);
    }

    public void setBaselineFunction(AnonymousClass22K r4) {
        this.mBaselineFunction = r4;
        long j = this.mNativePointer;
        boolean z = false;
        if (r4 != null) {
            z = true;
        }
        YogaNative.jni_YGNodeSetHasBaselineFuncJNI(j, z);
    }

    public void setBorder(AnonymousClass10G r4, float f) {
        YogaNative.jni_YGNodeStyleSetBorderJNI(this.mNativePointer, r4.mIntValue, f);
    }

    public void setDirection(C17660zG r4) {
        YogaNative.jni_YGNodeStyleSetDirectionJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setDisplay(AnonymousClass2N4 r4) {
        YogaNative.jni_YGNodeStyleSetDisplayJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setFlex(float f) {
        YogaNative.jni_YGNodeStyleSetFlexJNI(this.mNativePointer, f);
    }

    public void setFlexBasis(float f) {
        YogaNative.jni_YGNodeStyleSetFlexBasisJNI(this.mNativePointer, f);
    }

    public void setFlexBasisPercent(float f) {
        YogaNative.jni_YGNodeStyleSetFlexBasisPercentJNI(this.mNativePointer, f);
    }

    public void setFlexDirection(AnonymousClass10X r4) {
        YogaNative.jni_YGNodeStyleSetFlexDirectionJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setFlexGrow(float f) {
        YogaNative.jni_YGNodeStyleSetFlexGrowJNI(this.mNativePointer, f);
    }

    public void setFlexShrink(float f) {
        YogaNative.jni_YGNodeStyleSetFlexShrinkJNI(this.mNativePointer, f);
    }

    public void setHeight(float f) {
        YogaNative.jni_YGNodeStyleSetHeightJNI(this.mNativePointer, f);
    }

    public void setHeightAuto() {
        YogaNative.jni_YGNodeStyleSetHeightAutoJNI(this.mNativePointer);
    }

    public void setHeightPercent(float f) {
        YogaNative.jni_YGNodeStyleSetHeightPercentJNI(this.mNativePointer, f);
    }

    public void setIsReferenceBaseline(boolean z) {
        YogaNative.jni_YGNodeSetIsReferenceBaselineJNI(this.mNativePointer, z);
    }

    public void setJustifyContent(C14950uP r4) {
        YogaNative.jni_YGNodeStyleSetJustifyContentJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setMargin(AnonymousClass10G r4, float f) {
        YogaNative.jni_YGNodeStyleSetMarginJNI(this.mNativePointer, r4.mIntValue, f);
    }

    public void setMarginAuto(AnonymousClass10G r4) {
        YogaNative.jni_YGNodeStyleSetMarginAutoJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setMarginPercent(AnonymousClass10G r4, float f) {
        YogaNative.jni_YGNodeStyleSetMarginPercentJNI(this.mNativePointer, r4.mIntValue, f);
    }

    public void setMaxHeight(float f) {
        YogaNative.jni_YGNodeStyleSetMaxHeightJNI(this.mNativePointer, f);
    }

    public void setMaxHeightPercent(float f) {
        YogaNative.jni_YGNodeStyleSetMaxHeightPercentJNI(this.mNativePointer, f);
    }

    public void setMaxWidth(float f) {
        YogaNative.jni_YGNodeStyleSetMaxWidthJNI(this.mNativePointer, f);
    }

    public void setMaxWidthPercent(float f) {
        YogaNative.jni_YGNodeStyleSetMaxWidthPercentJNI(this.mNativePointer, f);
    }

    public void setMeasureFunction(AnonymousClass119 r4) {
        this.mMeasureFunction = r4;
        long j = this.mNativePointer;
        boolean z = false;
        if (r4 != null) {
            z = true;
        }
        YogaNative.jni_YGNodeSetHasMeasureFuncJNI(j, z);
    }

    public void setMinHeight(float f) {
        YogaNative.jni_YGNodeStyleSetMinHeightJNI(this.mNativePointer, f);
    }

    public void setMinHeightPercent(float f) {
        YogaNative.jni_YGNodeStyleSetMinHeightPercentJNI(this.mNativePointer, f);
    }

    public void setMinWidth(float f) {
        YogaNative.jni_YGNodeStyleSetMinWidthJNI(this.mNativePointer, f);
    }

    public void setMinWidthPercent(float f) {
        YogaNative.jni_YGNodeStyleSetMinWidthPercentJNI(this.mNativePointer, f);
    }

    public void setPadding(AnonymousClass10G r4, float f) {
        YogaNative.jni_YGNodeStyleSetPaddingJNI(this.mNativePointer, r4.mIntValue, f);
    }

    public void setPaddingPercent(AnonymousClass10G r4, float f) {
        YogaNative.jni_YGNodeStyleSetPaddingPercentJNI(this.mNativePointer, r4.mIntValue, f);
    }

    public void setPosition(AnonymousClass10G r4, float f) {
        YogaNative.jni_YGNodeStyleSetPositionJNI(this.mNativePointer, r4.mIntValue, f);
    }

    public void setPositionPercent(AnonymousClass10G r4, float f) {
        YogaNative.jni_YGNodeStyleSetPositionPercentJNI(this.mNativePointer, r4.mIntValue, f);
    }

    public void setPositionType(AnonymousClass1LC r4) {
        YogaNative.jni_YGNodeStyleSetPositionTypeJNI(this.mNativePointer, r4.mIntValue);
    }

    public void setWidth(float f) {
        YogaNative.jni_YGNodeStyleSetWidthJNI(this.mNativePointer, f);
    }

    public void setWidthAuto() {
        YogaNative.jni_YGNodeStyleSetWidthAutoJNI(this.mNativePointer);
    }

    public void setWidthPercent(float f) {
        YogaNative.jni_YGNodeStyleSetWidthPercentJNI(this.mNativePointer, f);
    }

    public void setWrap(AnonymousClass6I0 r4) {
        YogaNative.jni_YGNodeStyleSetFlexWrapJNI(this.mNativePointer, r4.mIntValue);
    }

    public /* bridge */ /* synthetic */ AnonymousClass10W cloneWithoutChildren() {
        try {
            YogaNodeJNIBase yogaNodeJNIBase = (YogaNodeJNIBase) super.clone();
            long jni_YGNodeCloneJNI = YogaNative.jni_YGNodeCloneJNI(this.mNativePointer);
            yogaNodeJNIBase.mOwner = null;
            yogaNodeJNIBase.mNativePointer = jni_YGNodeCloneJNI;
            yogaNodeJNIBase.mChildren = null;
            YogaNative.jni_YGNodeClearChildrenJNI(jni_YGNodeCloneJNI);
            return yogaNodeJNIBase;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public void setData(Object obj) {
        this.mData = obj;
    }

    public YogaNodeJNIBase() {
        this(YogaNative.jni_YGNodeNewJNI());
    }

    public YogaNodeJNIBase(long j) {
        this.arr = null;
        this.mLayoutDirection = 0;
        this.mHasNewLayout = true;
        if (j != 0) {
            this.mNativePointer = j;
            return;
        }
        throw new IllegalStateException("Failed to allocate native memory");
    }
}
