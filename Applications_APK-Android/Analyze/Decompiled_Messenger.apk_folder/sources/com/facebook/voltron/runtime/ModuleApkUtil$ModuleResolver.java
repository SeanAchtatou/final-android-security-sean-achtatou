package com.facebook.voltron.runtime;

import X.AnonymousClass08S;
import android.content.Context;

public class ModuleApkUtil$ModuleResolver {
    private ModuleApkUtil$ModuleResolver() {
    }

    public static String A00(String str, Context context) {
        String[] strArr = context.getApplicationInfo().splitSourceDirs;
        if (strArr == null) {
            return null;
        }
        String A0P = AnonymousClass08S.A0P("split_", str, ".apk");
        for (String str2 : strArr) {
            if (str2.endsWith(A0P)) {
                return str2;
            }
        }
        return null;
    }
}
