package com.facebook.mig.scheme.schemes;

import X.C29481gU;
import X.C29511gX;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

public abstract class BaseMigColorScheme implements MigColorScheme {
    public int B7f() {
        return -1064923495;
    }

    public int AbV() {
        return C3l(C29481gU.ACCENT, C29511gX.A00);
    }

    public int AkX() {
        return C3l(C29481gU.DISABLED_GLYPH, C29511gX.A00);
    }

    public int Aqi() {
        return C3l(C29481gU.INVERSE_PRIMARY_GLYPH, C29511gX.A00);
    }

    public int Asg() {
        if (!(this instanceof LightColorScheme)) {
            return AzL().AhV();
        }
        return ((LightColorScheme) this).Aea();
    }

    public int AzK() {
        return C3l(C29481gU.PRIMARY_GLYPH, C29511gX.A00);
    }

    public int B2B() {
        return C3l(C29481gU.SECONDARY_GLYPH, C29511gX.A00);
    }

    public int B2D() {
        return C3l(C29481gU.SECONDARY_WASH, C29511gX.A00);
    }

    public int B5T() {
        return C3l(C29481gU.TERTIARY_GLYPH, C29511gX.A00);
    }

    public int B9m() {
        return C3l(C29481gU.WASH, C29511gX.A00);
    }
}
