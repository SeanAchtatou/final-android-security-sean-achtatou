package com.facebook.mig.scheme.schemes;

import X.AnonymousClass1JZ;
import X.C012509m;
import X.C13940sK;
import X.C15320v6;
import X.C16930y3;
import X.C29021fk;
import X.C29521gY;
import android.content.res.ColorStateList;
import android.os.Parcel;
import android.os.Parcelable;

public final class LightColorScheme extends BaseMigColorScheme {
    private static LightColorScheme A01;
    private static final int A02 = C012509m.A02(167772160, -1);
    private static final ColorStateList A03;
    private static final ColorStateList A04;
    private static final ColorStateList A05;
    private static final ColorStateList A06;
    private static final ColorStateList A07;
    private static final ColorStateList A08;
    public static final Parcelable.Creator CREATOR = new C29021fk();
    private final int A00 = C012509m.A00(-10824391, 0.15f);

    public int Aea() {
        return -16737793;
    }

    public int Agf() {
        return 167772160;
    }

    public int Akh() {
        return 520093696;
    }

    public int AmM() {
        return C15320v6.MEASURED_STATE_MASK;
    }

    public int AmN() {
        return 872415231;
    }

    public int AmP() {
        return 855638016;
    }

    public int AoD() {
        return -10824391;
    }

    public int AyH() {
        return 520093696;
    }

    public int Azn() {
        return -8963329;
    }

    public int B0I() {
        return -54999;
    }

    public int B2A() {
        return 520093696;
    }

    public int B2X() {
        return -2138665319;
    }

    public int B9u() {
        return -1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
    }

    static {
        C13940sK r1 = new C13940sK();
        r1.A02(251658240);
        r1.A03(520093696);
        A03 = r1.A01();
        C13940sK r12 = new C13940sK();
        r12.A02(-1191182337);
        r12.A03(520093696);
        A08 = r12.A01();
        C13940sK r13 = new C13940sK();
        r13.A02(-16737793);
        r13.A03(872415231);
        A04 = r13.A01();
        C13940sK r14 = new C13940sK();
        r14.A02(-54999);
        r14.A03(872415231);
        A06 = r14.A01();
        C13940sK r15 = new C13940sK();
        r15.A02(-8963329);
        r15.A03(872415231);
        A05 = r15.A01();
        C13940sK r0 = new C13940sK();
        r0.A02(-1);
        r0.A03(520093696);
        A07 = r0.A01();
    }

    public static LightColorScheme A00() {
        if (A01 == null) {
            A01 = new LightColorScheme();
        }
        return A01;
    }

    public int Abs() {
        return this.A00;
    }

    public C16930y3 Aeb() {
        return AnonymousClass1JZ.A02;
    }

    public ColorStateList AfS() {
        return A03;
    }

    public ColorStateList AfT() {
        return A04;
    }

    public ColorStateList AfU() {
        return A05;
    }

    public ColorStateList AfV() {
        return A06;
    }

    public C16930y3 AkY() {
        return AnonymousClass1JZ.A04;
    }

    public C16930y3 AoE() {
        return AnonymousClass1JZ.A06;
    }

    public C16930y3 Aou() {
        return AnonymousClass1JZ.A08;
    }

    public C16930y3 Apm() {
        return AnonymousClass1JZ.A01;
    }

    public C16930y3 Aqj() {
        return AnonymousClass1JZ.A0A;
    }

    public int Aro() {
        return A02;
    }

    public C16930y3 AzL() {
        return AnonymousClass1JZ.A0C;
    }

    public C16930y3 B0J() {
        return AnonymousClass1JZ.A0E;
    }

    public C16930y3 B2C() {
        return AnonymousClass1JZ.A0G;
    }

    public C16930y3 B5U() {
        return AnonymousClass1JZ.A0I;
    }

    public ColorStateList B9n() {
        return A07;
    }

    public ColorStateList BAA() {
        return A08;
    }

    private LightColorScheme() {
    }

    public int AvQ() {
        return B9m();
    }

    public int B3r() {
        return B9m();
    }

    public int C3l(Object obj, C29521gY r3) {
        return r3.AsM(this, obj);
    }

    public int Aez() {
        return 2132476323;
    }

    public int AkT() {
        return 2132476321;
    }

    public int Akv() {
        return 2132214329;
    }
}
