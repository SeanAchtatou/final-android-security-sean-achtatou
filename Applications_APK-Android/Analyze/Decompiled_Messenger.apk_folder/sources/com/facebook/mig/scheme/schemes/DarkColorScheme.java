package com.facebook.mig.scheme.schemes;

import X.AnonymousClass1JZ;
import X.C012509m;
import X.C13940sK;
import X.C15320v6;
import X.C16930y3;
import X.C29521gY;
import X.C36931uL;
import android.content.res.ColorStateList;
import android.os.Parcel;
import android.os.Parcelable;

public final class DarkColorScheme extends BaseMigColorScheme {
    private static DarkColorScheme A00;
    private static final int A01 = C012509m.A00(-9644465, 0.34f);
    private static final int A02 = C012509m.A02(872415231, C15320v6.MEASURED_STATE_MASK);
    private static final int A03 = C012509m.A02(536870911, C15320v6.MEASURED_STATE_MASK);
    private static final int A04 = C012509m.A02(872415231, C15320v6.MEASURED_STATE_MASK);
    private static final ColorStateList A05;
    private static final ColorStateList A06;
    private static final ColorStateList A07;
    private static final ColorStateList A08;
    private static final ColorStateList A09;
    private static final ColorStateList A0A;
    public static final Parcelable.Creator CREATOR = new C36931uL();

    public int Aea() {
        return -15096833;
    }

    public int Akh() {
        return 872415231;
    }

    public int AmM() {
        return 872415231;
    }

    public int AmN() {
        return 352321535;
    }

    public int AmP() {
        return 1476395007;
    }

    public int AoD() {
        return -9644465;
    }

    public int AyH() {
        return 872415231;
    }

    public int Azn() {
        return -7711489;
    }

    public int B0I() {
        return -48574;
    }

    public int B2A() {
        return 536870911;
    }

    public int B2X() {
        return -1064923495;
    }

    public int B9u() {
        return -1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
    }

    static {
        C13940sK r0 = new C13940sK();
        r0.A02(872415231);
        r0.A03(872415231);
        A05 = r0.A01();
        C13940sK r1 = new C13940sK();
        r1.A02(872415231);
        r1.A03(1476395007);
        A0A = r1.A01();
        C13940sK r12 = new C13940sK();
        r12.A02(-15096833);
        r12.A03(872415231);
        A06 = r12.A01();
        C13940sK r13 = new C13940sK();
        r13.A02(-48574);
        r13.A03(872415231);
        A08 = r13.A01();
        C13940sK r14 = new C13940sK();
        r14.A02(-7711489);
        r14.A03(872415231);
        A07 = r14.A01();
        C13940sK r02 = new C13940sK();
        r02.A02(C15320v6.MEASURED_STATE_MASK);
        r02.A03(872415231);
        A09 = r02.A01();
        C13940sK r15 = new C13940sK();
        r15.A02(A04);
        r15.A03(872415231);
        r15.A01();
    }

    public static DarkColorScheme A00() {
        if (A00 == null) {
            A00 = new DarkColorScheme();
        }
        return A00;
    }

    public int Abs() {
        return A01;
    }

    public C16930y3 Aeb() {
        return AnonymousClass1JZ.A03;
    }

    public ColorStateList AfS() {
        return A05;
    }

    public ColorStateList AfT() {
        return A06;
    }

    public ColorStateList AfU() {
        return A07;
    }

    public ColorStateList AfV() {
        return A08;
    }

    public int Agf() {
        return A02;
    }

    public C16930y3 AkY() {
        return AnonymousClass1JZ.A05;
    }

    public C16930y3 AoE() {
        return AnonymousClass1JZ.A07;
    }

    public C16930y3 Aou() {
        return AnonymousClass1JZ.A09;
    }

    public C16930y3 Apm() {
        return AnonymousClass1JZ.A0M;
    }

    public C16930y3 Aqj() {
        return AnonymousClass1JZ.A0B;
    }

    public int Aro() {
        return A03;
    }

    public C16930y3 AzL() {
        return AnonymousClass1JZ.A0D;
    }

    public C16930y3 B0J() {
        return AnonymousClass1JZ.A0F;
    }

    public C16930y3 B2C() {
        return AnonymousClass1JZ.A0H;
    }

    public C16930y3 B5U() {
        return AnonymousClass1JZ.A0J;
    }

    public ColorStateList B9n() {
        return A09;
    }

    public ColorStateList BAA() {
        return A0A;
    }

    public int AvQ() {
        return B9m();
    }

    public int B3r() {
        return B9m();
    }

    public int C3l(Object obj, C29521gY r3) {
        return r3.AjS(this, obj);
    }

    private DarkColorScheme() {
    }

    public int Aez() {
        return 2132476322;
    }

    public int AkT() {
        return 2132476320;
    }

    public int Akv() {
        return 2132214330;
    }
}
