package com.facebook.mig.scheme.interfaces;

import X.C16930y3;
import X.C29521gY;
import android.content.res.ColorStateList;
import android.os.Parcelable;

public interface MigColorScheme extends Parcelable {
    int AbV();

    int Abs();

    int Aea();

    C16930y3 Aeb();

    int Aez();

    ColorStateList AfS();

    ColorStateList AfT();

    ColorStateList AfU();

    ColorStateList AfV();

    int Agf();

    int AkT();

    int AkX();

    C16930y3 AkY();

    int Akh();

    int Akv();

    int AmM();

    int AmN();

    int AmP();

    int AoD();

    C16930y3 AoE();

    C16930y3 Aou();

    C16930y3 Apm();

    int Aqi();

    C16930y3 Aqj();

    int Aro();

    int Asg();

    int AvQ();

    int AyH();

    int AzK();

    C16930y3 AzL();

    int Azn();

    int B0I();

    C16930y3 B0J();

    int B2A();

    int B2B();

    C16930y3 B2C();

    int B2D();

    int B2X();

    int B3r();

    int B5T();

    C16930y3 B5U();

    int B7f();

    int B9m();

    ColorStateList B9n();

    int B9u();

    ColorStateList BAA();

    int C3l(Object obj, C29521gY r2);
}
