package com.facebook.flexiblesampling;

import X.AnonymousClass064;
import X.AnonymousClass08S;
import X.AnonymousClass0YK;
import java.util.Random;

public final class SamplingResult {
    private static SamplingResult A04;
    private static final Random A05 = new Random();
    public int A00;
    public boolean A01;
    public boolean A02 = false;
    public boolean A03;

    public static SamplingResult A00() {
        if (A04 == null) {
            AnonymousClass0YK r1 = new AnonymousClass0YK();
            r1.A01 = true;
            r1.A00 = 1;
            A04 = new SamplingResult(r1);
        }
        return A04;
    }

    public boolean A01() {
        int i = this.A00;
        boolean z = false;
        if (i >= 0) {
            z = true;
        }
        AnonymousClass064.A05(z, AnonymousClass08S.A09("Not sure how to proceed with negative sampling rate ", i));
        if (i != 0 && A05.nextInt(i) == 0) {
            return true;
        }
        return false;
    }

    public String toString() {
        return AnonymousClass08S.A0T("com.facebook.flexiblesampling.SamplingResult", AnonymousClass08S.A09("\nSamplingRate: ", this.A00), AnonymousClass08S.A0X("\nHasUserConfig: ", this.A01), AnonymousClass08S.A0X("\nInUserConfig: ", this.A03), AnonymousClass08S.A0X("\nInSessionlessConfig: ", this.A02));
    }

    public SamplingResult(AnonymousClass0YK r2) {
        this.A00 = r2.A00;
        this.A01 = r2.A01;
        this.A03 = r2.A02;
    }
}
