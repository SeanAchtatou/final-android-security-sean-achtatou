package com.facebook.orca.threadview;

import X.AnonymousClass00S;
import X.AnonymousClass06A;
import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass09P;
import X.AnonymousClass0D4;
import X.AnonymousClass0Tc;
import X.AnonymousClass0UN;
import X.AnonymousClass0VB;
import X.AnonymousClass0VG;
import X.AnonymousClass0VK;
import X.AnonymousClass0W9;
import X.AnonymousClass0XJ;
import X.AnonymousClass0XV;
import X.AnonymousClass0ZM;
import X.AnonymousClass0g4;
import X.AnonymousClass0r6;
import X.AnonymousClass108;
import X.AnonymousClass146;
import X.AnonymousClass1F1;
import X.AnonymousClass1U0;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y6;
import X.AnonymousClass1Y7;
import X.AnonymousClass2KP;
import X.AnonymousClass2x4;
import X.AnonymousClass339;
import X.AnonymousClass33A;
import X.AnonymousClass33D;
import X.AnonymousClass33E;
import X.AnonymousClass33F;
import X.AnonymousClass33G;
import X.AnonymousClass33H;
import X.AnonymousClass33J;
import X.AnonymousClass3B5;
import X.AnonymousClass3IL;
import X.AnonymousClass3J9;
import X.AnonymousClass3LU;
import X.AnonymousClass3M2;
import X.AnonymousClass3R9;
import X.AnonymousClass3RA;
import X.AnonymousClass3RB;
import X.AnonymousClass3RC;
import X.AnonymousClass3RD;
import X.AnonymousClass3RF;
import X.AnonymousClass3RG;
import X.AnonymousClass3RH;
import X.AnonymousClass3RI;
import X.AnonymousClass3RT;
import X.AnonymousClass3RU;
import X.AnonymousClass3RW;
import X.AnonymousClass3RZ;
import X.AnonymousClass3SG;
import X.AnonymousClass3SP;
import X.AnonymousClass3SQ;
import X.AnonymousClass3SR;
import X.AnonymousClass3SU;
import X.AnonymousClass3T1;
import X.AnonymousClass3T3;
import X.AnonymousClass3VF;
import X.AnonymousClass3YL;
import X.AnonymousClass6Rm;
import X.AnonymousClass70M;
import X.AnonymousClass92O;
import X.AnonymousClass92U;
import X.AnonymousClass92W;
import X.C000700l;
import X.C001300x;
import X.C001500z;
import X.C005505z;
import X.C010708t;
import X.C04310Tq;
import X.C04430Uq;
import X.C04460Ut;
import X.C05690aA;
import X.C06600bl;
import X.C06680bu;
import X.C06790c5;
import X.C08770fv;
import X.C08860g6;
import X.C10960l9;
import X.C10990lD;
import X.C111935Up;
import X.C12390pG;
import X.C126065wF;
import X.C13060qW;
import X.C13220qv;
import X.C13290rA;
import X.C13460rT;
import X.C134626Rk;
import X.C13500rX;
import X.C135796Wl;
import X.C136696a5;
import X.C136956aY;
import X.C137076ak;
import X.C140416go;
import X.C14220so;
import X.C145246pO;
import X.C14780u1;
import X.C14810uA;
import X.C148156uH;
import X.C14840uE;
import X.C150826yl;
import X.C150906yt;
import X.C150946yx;
import X.C150986z1;
import X.C15760vw;
import X.C15770vx;
import X.C15780vy;
import X.C15870w7;
import X.C16000wK;
import X.C161757eG;
import X.C16290wo;
import X.C163217gf;
import X.C1754487k;
import X.C185414b;
import X.C190616q;
import X.C191016u;
import X.C195218m;
import X.C20021Ap;
import X.C21501Hg;
import X.C22298Ase;
import X.C22431Lh;
import X.C22541Lt;
import X.C24361Ti;
import X.C25051Yd;
import X.C28711fF;
import X.C28891fX;
import X.C30281hn;
import X.C33701o0;
import X.C34731q5;
import X.C34761q8;
import X.C37531vp;
import X.C401020h;
import X.C417626w;
import X.C42782Bv;
import X.C44342Hv;
import X.C45702Nd;
import X.C52432j5;
import X.C52752jd;
import X.C53482kr;
import X.C53492ks;
import X.C54602mg;
import X.C55462o7;
import X.C55482o9;
import X.C55922oy;
import X.C55932oz;
import X.C55972p3;
import X.C56742ql;
import X.C56882r2;
import X.C57752sZ;
import X.C57762sa;
import X.C57822sg;
import X.C57952su;
import X.C60252wq;
import X.C60402x8;
import X.C60412x9;
import X.C60422xA;
import X.C60432xB;
import X.C61152yQ;
import X.C64773Dj;
import X.C64833Du;
import X.C66003Ik;
import X.C66123Iw;
import X.C66233Jj;
import X.C66763Ln;
import X.C66773Lo;
import X.C67023Mq;
import X.C68333Sk;
import X.C68413Ss;
import X.C68423St;
import X.C68443Sv;
import X.C73263fn;
import X.C73443g5;
import X.C78763pU;
import X.C79913rT;
import X.C80043rl;
import X.C80093rq;
import X.C81953vA;
import X.C88844Mn;
import X.C92994bu;
import X.C93014bw;
import X.C99084oO;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.facebook.appirater.api.FetchISRConfigResult;
import com.facebook.common.ui.keyboard.CustomKeyboardLayout;
import com.facebook.content.SecureContextHelper;
import com.facebook.fbservice.service.ServiceException;
import com.facebook.graphql.enums.GraphQLMessengerThreadViewMode;
import com.facebook.litho.LithoView;
import com.facebook.messaging.blocking.view.PlatformMessagesToggleButton;
import com.facebook.messaging.business.common.calltoaction.CallToActionContextParams;
import com.facebook.messaging.business.mdotme.model.PlatformRefParams;
import com.facebook.messaging.composer.ComposeFragment;
import com.facebook.messaging.messengerprefs.tincan.TincanNuxFragment;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.GroupThreadAssociatedFbGroup;
import com.facebook.messaging.model.threads.MarketplaceThreadData;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams;
import com.facebook.messaging.profile.ContextualProfileLoggingData;
import com.facebook.messaging.profile.ProfilePopoverFragment;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.facebook.messaging.sms.matching.IdentityMatchingInterstitialActivity;
import com.facebook.messaging.threadview.params.MessageDeepLinkInfo;
import com.facebook.messaging.threadview.params.ThreadViewMessagesInitParams;
import com.facebook.messaging.threadview.params.ThreadViewParams;
import com.facebook.messaging.threadview.scheme.interfaces.ThreadViewColorScheme;
import com.facebook.payments.currency.CurrencyAmount;
import com.facebook.payments.p2p.awareness.PaymentAwarenessActivity;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.rtc.interfaces.RtcCallStartParams;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.facebook.widget.RoundedCornersFrameLayout;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ThreadViewFragment extends C13460rT implements C15760vw, C12390pG, C15770vx, C15780vy {
    public int A00;
    public int A01;
    public long A02;
    public Context A03;
    public Rect A04;
    public View A05;
    public ViewGroup A06;
    public LinearLayout A07;
    public C13060qW A08;
    public C06790c5 A09;
    public C06790c5 A0A;
    public C04460Ut A0B;
    public C20021Ap A0C;
    public AnonymousClass0UN A0D;
    public C24361Ti A0E;
    public C08770fv A0F;
    public C15870w7 A0G;
    public ThreadKey A0H;
    public GroupThreadAssociatedFbGroup A0I;
    public C191016u A0J;
    public C14780u1 A0K;
    public NavigationTrigger A0L;
    public NavigationTrigger A0M;
    public C53482kr A0N;
    public AnonymousClass3M2 A0O;
    public C55922oy A0P;
    public C60402x8 A0Q = C60402x8.A02;
    public MessageDeepLinkInfo A0R;
    public ThreadViewMessagesInitParams A0S;
    public ThreadViewColorScheme A0T = C60252wq.A01();
    public C13220qv A0U = C13220qv.A0I;
    public AnonymousClass2x4 A0V = AnonymousClass2x4.A00;
    public C60432xB A0W;
    public C67023Mq A0X;
    public C57952su A0Y;
    public AnonymousClass3SQ A0Z;
    public AnonymousClass3SR A0a;
    public C14840uE A0b;
    public ThreadViewMessagesFragment A0c;
    public AnonymousClass33E A0d;
    public C53492ks A0e;
    public C57822sg A0f;
    public FbSharedPreferences A0g;
    public C195218m A0h = C195218m.A0A;
    public C56882r2 A0i;
    public C55932oz A0j;
    public User A0k;
    public RoundedCornersFrameLayout A0l;
    public Integer A0m;
    public String A0n;
    public Random A0o;
    public ScheduledFuture A0p;
    public C04310Tq A0q;
    public C04310Tq A0r;
    public C04310Tq A0s;
    public C04310Tq A0t;
    public C04310Tq A0u;
    public boolean A0v;
    public boolean A0w = true;
    public boolean A0x = false;
    public boolean A0y;
    public boolean A0z = false;
    public boolean A10;
    public boolean A11;
    public boolean A12;
    public boolean A13;
    public boolean A14;
    public boolean A15;
    public boolean A16;
    public boolean A17;
    public boolean A18;
    private int A19 = -1;
    private LayoutInflater A1A = null;
    private C06790c5 A1B;
    private C06790c5 A1C;
    private C06790c5 A1D;
    private C06790c5 A1E;
    private C06790c5 A1F;
    private AnonymousClass70M A1G;
    private C66773Lo A1H;
    private AnonymousClass0ZM A1I;
    private String A1J;
    private boolean A1K;
    public final C34731q5 A1L;
    public final AnonymousClass33A A1M;
    private final C60422xA A1N;
    private final AnonymousClass0ZM A1O;
    private final AnonymousClass0ZM A1P;

    private void A0C() {
        A0b(this, false);
        A06();
        AnonymousClass3SG.A02((AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, this.A0D), AnonymousClass07B.A0n);
        A07();
        A0e("Click on back_to_home button");
        ((C111935Up) AnonymousClass1XX.A02(32, AnonymousClass1Y3.BTo, this.A0D)).A01(this.A0H);
        this.A0b.BC4(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b9, code lost:
        if (r9.A0H.A0K() == false) goto L_0x00bb;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0U(com.facebook.orca.threadview.ThreadViewFragment r9, int r10) {
        /*
            r5 = 0
            com.facebook.user.model.User r4 = r9.A0k
            com.facebook.messaging.model.threadkey.ThreadKey r8 = r9.A0H
            java.lang.String r6 = "ThreadViewFragment_ThreadSettings_NoUser"
            r2 = 14
            if (r8 != 0) goto L_0x0046
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r9.A0D
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r1 = (X.AnonymousClass09P) r1
            java.lang.String r0 = "Trying to open thread settings with no thread key"
        L_0x0017:
            r1.CGS(r6, r0)
            r1 = 0
        L_0x001b:
            if (r1 == 0) goto L_0x0045
            X.2x8 r0 = r9.A0Q
            X.2yQ r0 = r0.A01
            com.facebook.messaging.model.threads.ThreadSummary r6 = r0.A02
            X.3M2 r0 = r9.A0O
            if (r0 != 0) goto L_0x0039
            X.2kr r3 = r9.A0N
            android.content.Context r2 = r9.A1j()
            X.6dJ r1 = new X.6dJ
            r1.<init>(r9)
            X.6WV r0 = new X.6WV
            r0.<init>(r3, r2, r1)
            r9.A0O = r0
        L_0x0039:
            X.3M2 r0 = r9.A0O
            r0.CG9(r6, r4, r10, r5)
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r9.A0c
            com.facebook.messaging.composer.ComposeFragment r0 = r0.A0P
            r0.A2W()
        L_0x0045:
            return
        L_0x0046:
            X.2x8 r0 = r9.A0Q
            X.2yQ r0 = r0.A01
            com.facebook.messaging.model.threads.ThreadSummary r3 = r0.A02
            if (r3 == 0) goto L_0x005a
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0S
            X.1fF r1 = r0.A05
            X.1fF r0 = X.C28711fF.GROUP
            if (r1 == r0) goto L_0x00bb
            X.1fF r0 = X.C28711fF.SMS
            if (r1 == r0) goto L_0x00bb
        L_0x005a:
            if (r4 != 0) goto L_0x0069
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r9.A0D
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r1 = (X.AnonymousClass09P) r1
            java.lang.String r0 = "Trying to open thread settings with a null stored user"
            goto L_0x0017
        L_0x0069:
            com.facebook.user.model.UserKey r7 = r4.A0Q
            long r0 = r8.A01
            java.lang.String r0 = java.lang.String.valueOf(r0)
            com.facebook.user.model.UserKey r8 = com.facebook.user.model.UserKey.A01(r0)
            boolean r0 = r7.equals(r8)
            if (r0 != 0) goto L_0x00b0
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r9.A0D
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r3 = (X.AnonymousClass09P) r3
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r9.A0H
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Stored user does not match other user in thread."
            r1.<init>(r0)
            java.lang.String r0 = " Thread key is "
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = ". Stored user is "
            r1.append(r0)
            r1.append(r7)
            java.lang.String r0 = ". Other user is "
            r1.append(r0)
            r1.append(r8)
            java.lang.String r0 = r1.toString()
            r3.CGS(r6, r0)
            r1 = 0
            goto L_0x001b
        L_0x00b0:
            if (r3 != 0) goto L_0x00bb
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0H
            boolean r0 = r0.A0K()
            r1 = 0
            if (r0 != 0) goto L_0x001b
        L_0x00bb:
            r1 = 1
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A0U(com.facebook.orca.threadview.ThreadViewFragment, int):void");
    }

    public void A2Y(boolean z) {
        this.A16 = true;
        if (z) {
            A0L(this);
        } else if (this.A0Q.A01.A02 == null) {
            this.A14 = true;
        }
    }

    public boolean A2a(String str) {
        boolean z;
        C137076ak r0;
        Activity activity;
        this.A0z = false;
        C13060qW r02 = this.A08;
        if (r02 != null && C16000wK.A01(r02)) {
            A0b(this, false);
            A06();
            AnonymousClass3SG.A02((AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, this.A0D), AnonymousClass07B.A0n);
            A07();
            int i = AnonymousClass1Y3.B3a;
            AnonymousClass0UN r2 = this.A0D;
            if (((C001300x) AnonymousClass1XX.A02(4, i, r2)).A02 == C001500z.A07) {
                AnonymousClass92W r8 = (AnonymousClass92W) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ADi, r2);
                FragmentActivity A152 = A15();
                if (r8.A07() && !r8.A06 && !((AnonymousClass92U) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AmN, r8.A03)).A00.Aep(AnonymousClass92O.A05, false)) {
                    C13060qW A002 = AnonymousClass92W.A00(r8);
                    boolean z2 = false;
                    if (!(A002 == null || A002.A0Q("appirater_isr_dialog_fragment") == null)) {
                        z2 = true;
                    }
                    if (!z2) {
                        r8.A00 = 0;
                        r8.A04 = new WeakReference(A152);
                        int i2 = AnonymousClass1Y3.AmN;
                        if (((AnonymousClass92U) AnonymousClass1XX.A02(0, i2, r8.A03)).A03() != null) {
                            AnonymousClass92W.A03(r8);
                        } else {
                            WeakReference weakReference = r8.A04;
                            if (weakReference == null) {
                                activity = null;
                            } else {
                                activity = (Activity) weakReference.get();
                            }
                            if (activity != null) {
                                if (!((AnonymousClass92U) AnonymousClass1XX.A02(0, i2, r8.A03)).A00.Aep(AnonymousClass92O.A03, false) || AnonymousClass92W.A02(r8) == null) {
                                    FetchISRConfigResult A052 = r8.A05();
                                    if (A052 != null && A052.A00()) {
                                        boolean z3 = false;
                                        if (AnonymousClass06A.A00.now() > A052.resultRecievedAtMillis + A052.delayTillNextPingMillis) {
                                            z3 = true;
                                        }
                                        if (!z3 && A052.shouldAskUser) {
                                            r8.A06 = true;
                                            AnonymousClass00S.A05((Handler) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amu, r8.A03), r8.A08, A052.delayAskingMillis, 1796197477);
                                        }
                                    }
                                } else {
                                    AnonymousClass92W.A04(r8);
                                }
                            }
                        }
                    }
                }
            }
            ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
            if (threadViewMessagesFragment == null || !threadViewMessagesFragment.A2g()) {
                C66773Lo r22 = this.A1H;
                if (r22 != null) {
                    boolean z4 = false;
                    if (r22.A07.A00 != null) {
                        z4 = true;
                    }
                    if (!z4 || (r0 = r22.A0C) == null) {
                        z = false;
                    } else {
                        r0.A00();
                        AnonymousClass3J9 r03 = r22.A08;
                        if (r03 != null) {
                            r03.A02();
                        }
                        z = true;
                    }
                    if (z) {
                        return true;
                    }
                }
                C13060qW r1 = this.A08;
                if (!C16000wK.A01(r1)) {
                    return r1.A0L() > 0;
                }
                if (!r1.A14()) {
                    return false;
                }
                if (str != null) {
                    ((AnonymousClass146) AnonymousClass1XX.A02(7, AnonymousClass1Y3.A5w, this.A0D)).A0D(str);
                }
                C24361Ti r23 = this.A0E;
                Activity A2J = A2J();
                String str2 = (String) r23.A05.get();
                C24361Ti.A00(r23, "thread", "thread", str2);
                r23.A01.A0C(A2J, "thread", "thread", str2, null);
                A0Z(this, C99084oO.$const$string(11));
                return true;
            }
        }
        return true;
    }

    public String Act() {
        return "thread";
    }

    private int A00() {
        Random random;
        if (this.A19 == -1 && (random = this.A0o) != null) {
            this.A19 = random.nextInt(Integer.MAX_VALUE);
        }
        return this.A19;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0033, code lost:
        if (r3 >= 10000000000000L) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long A04() {
        /*
            r9 = this;
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r0 = r9.A0R
            r7 = -1
            if (r0 == 0) goto L_0x0051
            r2 = 23
            int r1 = X.AnonymousClass1Y3.Aoo
            X.0UN r0 = r9.A0D
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1BX r0 = (X.AnonymousClass1BX) r0
            boolean r0 = r0.A01()
            if (r0 == 0) goto L_0x0051
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r0 = r9.A0R
            long r3 = r0.A00
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0035
            r1 = 10000000000(0x2540be400, double:4.9406564584E-314)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0035
            r5 = 10000000000000(0x9184e72a000, double:4.9406564584125E-311)
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            r0 = 1
            if (r1 < 0) goto L_0x0036
        L_0x0035:
            r0 = 0
        L_0x0036:
            if (r0 != 0) goto L_0x0050
            r2 = 14
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r9.A0D
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r0 = "ScrollToUnread: timestampOfMessageToShow is invalid: "
            java.lang.String r1 = X.AnonymousClass08S.A0G(r0, r3)
            java.lang.String r0 = "ThreadViewFragment"
            r2.CGS(r0, r1)
            return r7
        L_0x0050:
            return r3
        L_0x0051:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A04():long");
    }

    public static ThreadViewFragment A05(ThreadViewParams threadViewParams) {
        C005505z.A03("ThreadViewFragment.newInstance", -1780685512);
        try {
            ThreadViewFragment threadViewFragment = new ThreadViewFragment();
            if (threadViewParams != null) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("args_thread_params", threadViewParams);
                threadViewFragment.A1P(bundle);
            }
            return threadViewFragment;
        } finally {
            C005505z.A00(-89967482);
        }
    }

    private void A06() {
        ScheduledFuture scheduledFuture = this.A0p;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            this.A0p = null;
        }
        AnonymousClass3SG.A02((AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, this.A0D), AnonymousClass07B.A0i);
    }

    private void A07() {
        AnonymousClass3SG.A02((AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, this.A0D), AnonymousClass07B.A0p);
        this.A0c.A1d = false;
    }

    private void A08() {
        C55462o7 r0;
        ThreadKey threadKey;
        C55922oy r02 = this.A0P;
        if (r02 == null) {
            r0 = null;
        } else {
            r0 = r02.A00;
        }
        if (r0 != null) {
            AnonymousClass3SU r1 = r0.A05;
            if (r1 == null) {
                threadKey = null;
            } else {
                threadKey = r1.A03;
            }
            if (!Objects.equal(threadKey, this.A0H)) {
                r0.ARp();
                r0.C6Z(null);
            }
        }
    }

    private void A0A() {
        ThreadSummary threadSummary;
        C61152yQ r0 = this.A0Q.A01;
        if (!(r0 == null || (threadSummary = r0.A02) == null)) {
            this.A0g.CJr(C05690aA.A08(threadSummary.A0S), this.A1I);
        }
        this.A0g.CJs(ImmutableSet.A05(C21501Hg.A02, C21501Hg.A01), this.A1O);
    }

    private void A0B() {
        if (this.A0i && A1a()) {
            C45702Nd r0 = this.A0E.A02.A00;
            if (r0 != null) {
                r0.A02("thread");
            }
            C61152yQ r02 = this.A0Q.A01;
            if (r02 != null) {
                this.A0E.A08("data_fetch_disposition", r02.A00);
            }
        }
    }

    private void A0D() {
        if (!ThreadKey.A0F(this.A0H)) {
            A0E();
            return;
        }
        C04460Ut r1 = this.A0B;
        if (r1 != null) {
            if (this.A1F == null) {
                C06600bl BMm = r1.BMm();
                BMm.A02(C06680bu.A14, new C1754487k(this));
                this.A1F = BMm.A00();
            }
            if (!this.A1F.A02()) {
                this.A1F.A00();
            }
        }
    }

    private void A0E() {
        C06790c5 r0 = this.A1F;
        if (r0 != null && r0.A02()) {
            this.A1F.A01();
        }
        C06790c5 r02 = this.A0A;
        if (r02 != null && r02.A02()) {
            this.A0A.A01();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002f, code lost:
        if (r2 != 0) goto L_0x0031;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0F(com.facebook.messaging.model.threads.ThreadSummary r8) {
        /*
            r7 = this;
            com.facebook.orca.threadview.ThreadViewMessagesFragment r6 = r7.A0c
            if (r6 == 0) goto L_0x003c
            r2 = 12
            int r1 = X.AnonymousClass1Y3.AJC
            X.0UN r0 = r7.A0D
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2sa r5 = (X.C57762sa) r5
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r7.A0H
            com.facebook.messaging.threadview.scheme.interfaces.ThreadViewColorScheme r3 = r7.A0T
            if (r4 == 0) goto L_0x0059
            r2 = 0
            if (r8 == 0) goto L_0x0029
            int r1 = X.AnonymousClass1Y3.B44
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2jd r1 = (X.C52752jd) r1
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r0 = r8.A0M
            int r2 = r1.A04(r0)
        L_0x0029:
            if (r2 != 0) goto L_0x002f
            int r2 = X.C57762sa.A00(r5, r8, r4, r3)
        L_0x002f:
            if (r2 == 0) goto L_0x0059
        L_0x0031:
            com.facebook.messaging.composer.ComposeFragment r4 = r6.A0P
            android.view.View r0 = r4.A0I
            if (r0 == 0) goto L_0x003c
            if (r2 == 0) goto L_0x003d
            com.facebook.messaging.composer.ComposeFragment.A0M(r4, r2)
        L_0x003c:
            return
        L_0x003d:
            com.facebook.messaging.model.threads.ThreadSummary r3 = com.facebook.messaging.composer.ComposeFragment.A04(r4)
            if (r3 == 0) goto L_0x003c
            r2 = 69
            int r1 = X.AnonymousClass1Y3.B44
            X.0UN r0 = r4.A09
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2jd r1 = (X.C52752jd) r1
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r0 = r3.A0M
            int r0 = r1.A04(r0)
            com.facebook.messaging.composer.ComposeFragment.A0M(r4, r0)
            return
        L_0x0059:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r3.A0F
            int r2 = r0.AbV()
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A0F(com.facebook.messaging.model.threads.ThreadSummary):void");
    }

    private void A0G(ThreadViewMessagesInitParams threadViewMessagesInitParams) {
        Integer num;
        ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
        if (threadViewMessagesInitParams != null) {
            threadViewMessagesFragment.A0q = threadViewMessagesInitParams;
            threadViewMessagesFragment.A1s.BaS(threadViewMessagesInitParams);
            if (threadViewMessagesFragment.A0A != null) {
                threadViewMessagesFragment.A2Z();
                threadViewMessagesFragment.A0P.A2f(threadViewMessagesInitParams.A05);
            } else {
                threadViewMessagesFragment.A0S = threadViewMessagesInitParams.A05;
            }
            AnonymousClass3SP r1 = (AnonymousClass3SP) AnonymousClass1XX.A02(AnonymousClass1Y3.A0i, AnonymousClass1Y3.Agy, threadViewMessagesFragment.A0M);
            Context A1j = threadViewMessagesFragment.A1j();
            if (threadViewMessagesInitParams.A09 != null) {
                C92994bu r3 = (C92994bu) r1.A00.get();
                if (threadViewMessagesInitParams.A09 == C64773Dj.MEDIA_UPLOAD_FILE_NOT_FOUND_LOW_DISK_SPACE) {
                    C13500rX r2 = new C13500rX(A1j);
                    r2.A09(2131832775);
                    r2.A08(2131832776);
                    r2.A02(2131833426, new C93014bw(r3, A1j));
                    r2.A00(2131822403, new AnonymousClass2KP());
                    r2.A06().show();
                }
            }
            Intent intent = threadViewMessagesInitParams.A00;
            if (intent != null) {
                C417626w.A06(intent, threadViewMessagesFragment.A1j());
            }
            PlatformRefParams platformRefParams = threadViewMessagesInitParams.A04;
            if (platformRefParams != null && platformRefParams.A00()) {
                AnonymousClass3VF r12 = (AnonymousClass3VF) AnonymousClass1XX.A02(45, AnonymousClass1Y3.BFO, threadViewMessagesFragment.A0M);
                PlatformRefParams platformRefParams2 = r12.A00;
                if (platformRefParams2 == null || !platformRefParams2.equals(platformRefParams)) {
                    r12.A01 = false;
                    r12.A00 = platformRefParams;
                }
                threadViewMessagesFragment.A15.A01 = threadViewMessagesInitParams.A04;
            }
            if (threadViewMessagesInitParams.A03 != null) {
                C54602mg r13 = new C54602mg(threadViewMessagesInitParams.A02);
                r13.A01 = threadViewMessagesFragment.B4m();
                r13.A06 = threadViewMessagesFragment.A0Y;
                ((AnonymousClass3LU) AnonymousClass1XX.A02(47, AnonymousClass1Y3.AHO, threadViewMessagesFragment.A0M)).A02(threadViewMessagesInitParams.A03, new CallToActionContextParams(r13));
            }
            if (!Platform.stringIsNullOrEmpty(threadViewMessagesInitParams.A0C)) {
                threadViewMessagesFragment.A0e.BKi(threadViewMessagesInitParams);
            }
        }
        GroupThreadAssociatedFbGroup groupThreadAssociatedFbGroup = null;
        if (threadViewMessagesInitParams != null) {
            num = threadViewMessagesInitParams.A0A;
        } else {
            num = null;
        }
        this.A0m = num;
        if (threadViewMessagesInitParams != null) {
            groupThreadAssociatedFbGroup = threadViewMessagesInitParams.A08;
        }
        this.A0I = groupThreadAssociatedFbGroup;
    }

    public static void A0H(ThreadViewFragment threadViewFragment) {
        C55462o7 r1 = threadViewFragment.A0P.A00;
        if (r1 != null) {
            r1.ARp();
            r1.C6Z(null);
        }
    }

    public static void A0I(ThreadViewFragment threadViewFragment) {
        ThreadViewMessagesFragment threadViewMessagesFragment;
        ThreadSummary threadSummary = threadViewFragment.A0Q.A01.A02;
        if (threadSummary != null && threadSummary.A0y && (threadViewMessagesFragment = threadViewFragment.A0c) != null) {
            threadViewMessagesFragment.A2Z();
            threadViewFragment.A0c.A0P.A2Y();
        }
    }

    public static void A0J(ThreadViewFragment threadViewFragment) {
        ScheduledFuture scheduledFuture = threadViewFragment.A0p;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            threadViewFragment.A0p = null;
        }
        AnonymousClass3SG.A02((AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, threadViewFragment.A0D), AnonymousClass07B.A01);
    }

    public static void A0L(ThreadViewFragment threadViewFragment) {
        Window window;
        if (threadViewFragment.A18 && threadViewFragment.A16) {
            threadViewFragment.A16 = false;
            Object A032 = threadViewFragment.A0N.A03();
            if (A032 instanceof Activity) {
                window = ((Activity) A032).getWindow();
            } else {
                window = null;
            }
            if (window != null) {
                window.setSoftInputMode(4);
            }
            A0I(threadViewFragment);
        }
    }

    public static void A0M(ThreadViewFragment threadViewFragment) {
        if (threadViewFragment.A18 && threadViewFragment.A1K && threadViewFragment.A0Q.A01 != C61152yQ.A07) {
            threadViewFragment.A0c.A2U();
        }
    }

    public static void A0N(ThreadViewFragment threadViewFragment) {
        User A012 = ((AnonymousClass3RZ) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AyS, threadViewFragment.A0D)).A01(null, threadViewFragment.A0H);
        if (A012 != null && A012.A0F()) {
            Optional A2L = threadViewFragment.A2L(2131299650);
            if (A2L.isPresent()) {
                PlatformMessagesToggleButton platformMessagesToggleButton = (PlatformMessagesToggleButton) A2L.get();
                boolean z = false;
                if (A012.A06() == AnonymousClass07B.A00) {
                    z = true;
                }
                if (z) {
                    platformMessagesToggleButton.A01.setVisibility(0);
                    platformMessagesToggleButton.A00.setVisibility(4);
                    return;
                }
                platformMessagesToggleButton.A01.setVisibility(4);
                platformMessagesToggleButton.A00.setVisibility(0);
            }
        }
    }

    public static void A0O(ThreadViewFragment threadViewFragment) {
        threadViewFragment.A0F.A05.markerStart(5505186);
        threadViewFragment.A0c.A0P.A2X();
        if (!((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, threadViewFragment.A0D)).Aem(2306128143502546308L) || !A0k(threadViewFragment)) {
            threadViewFragment.A0C();
        }
    }

    public static void A0P(ThreadViewFragment threadViewFragment) {
        MarketplaceThreadData marketplaceThreadData;
        String str;
        ThreadSummary threadSummary = threadViewFragment.A0Q.A01.A02;
        if (threadSummary == null || (marketplaceThreadData = threadSummary.A0W) == null || marketplaceThreadData.A01 == null || (str = ((UserKey) threadViewFragment.A0t.get()).id) == null || !str.equals(threadSummary.A0W.A01.A08)) {
            A0K(threadViewFragment);
        }
    }

    public static void A0Q(ThreadViewFragment threadViewFragment) {
        if (threadViewFragment.A0H != null) {
            ((AnonymousClass1U0) AnonymousClass1XX.A03(AnonymousClass1Y3.BUA, threadViewFragment.A0D)).A0A();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        if (r1 == false) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0R(com.facebook.orca.threadview.ThreadViewFragment r8) {
        /*
            X.2x4 r3 = r8.A0V
            boolean r0 = r3.BFQ()
            if (r0 == 0) goto L_0x005a
            boolean r0 = r8.A1X()
            if (r0 == 0) goto L_0x005a
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r8.A0H
            X.2x8 r0 = r8.A0Q
            X.2yQ r1 = r0.A01
            boolean r0 = r3.BKZ(r2, r1)
            if (r0 != 0) goto L_0x005a
            if (r1 == 0) goto L_0x0027
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A02
            if (r0 == 0) goto L_0x0027
            boolean r1 = r0.A0A()
            r0 = 1
            if (r1 != 0) goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            if (r0 != 0) goto L_0x0032
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0H
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0D(r0)
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            r2 = 42
            int r1 = X.AnonymousClass1Y3.B9O
            X.0UN r0 = r8.A0D
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0u0 r0 = (X.AnonymousClass0u0) r0
            X.1Yd r2 = r0.A00
            r0 = 284833641272342(0x1030e00021416, double:1.407265169325336E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 != 0) goto L_0x005b
            X.2x4 r0 = r8.A0V
            r0.ASd()
        L_0x0050:
            X.2x4 r1 = r8.A0V
            boolean r0 = r8.A0w
            r1.C8D(r0)
            A0N(r8)
        L_0x005a:
            return
        L_0x005b:
            r2 = 16
            int r1 = X.AnonymousClass1Y3.AyS
            X.0UN r0 = r8.A0D
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3RZ r2 = (X.AnonymousClass3RZ) r2
            X.2x8 r0 = r8.A0Q
            X.2yQ r1 = r0.A01
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0H
            com.facebook.user.model.User r0 = r2.A01(r1, r0)
            r8.A0k = r0
            X.2x4 r1 = r8.A0V
            android.content.Context r2 = r8.A1j()
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r8.A0H
            X.2x8 r0 = r8.A0Q
            X.2yQ r4 = r0.A01
            com.facebook.user.model.User r5 = r8.A0k
            r6 = 0
            r7 = 0
            r1.CKJ(r2, r3, r4, r5, r6, r7)
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A0R(com.facebook.orca.threadview.ThreadViewFragment):void");
    }

    public static void A0S(ThreadViewFragment threadViewFragment) {
        ThreadViewColorScheme A012;
        GraphQLMessengerThreadViewMode B61;
        ThreadSummary threadSummary = threadViewFragment.A0Q.A01.A02;
        if (threadViewFragment.A0x) {
            AnonymousClass33D r4 = (AnonymousClass33D) AnonymousClass1XX.A03(AnonymousClass1Y3.BH0, threadViewFragment.A0D);
            if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C28891fX) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Auq, r4.A00)).A00)).Aem(282716222523222L)) {
                A012 = null;
                if (threadSummary != null && ((B61 = threadSummary.A0M.B61()) == GraphQLMessengerThreadViewMode.DARK_MODE || B61 == GraphQLMessengerThreadViewMode.LIGHT_MODE)) {
                    switch (B61.ordinal()) {
                        case 2:
                            A012 = C60252wq.A00();
                            break;
                        case 3:
                            A012 = C60252wq.A01();
                            break;
                    }
                }
            } else {
                A012 = null;
            }
            if (A012 == null) {
                A012 = (ThreadViewColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.B2u, r4.A00);
            }
        } else {
            A012 = C60252wq.A01();
        }
        C14840uE r1 = threadViewFragment.A0b;
        if (r1 != null) {
            r1.Bpy(A012.A0F.B9m());
        }
        if (!Objects.equal(threadViewFragment.A0T, A012)) {
            threadViewFragment.A0T = A012;
            ThreadViewMessagesFragment threadViewMessagesFragment = threadViewFragment.A0c;
            if (threadViewMessagesFragment != null) {
                threadViewMessagesFragment.A2d(A012);
            }
            AnonymousClass2x4 r12 = threadViewFragment.A0V;
            if (r12 != null) {
                r12.C6y(threadViewFragment.A0T);
            }
            threadViewFragment.A0F(threadSummary);
        }
    }

    public static void A0T(ThreadViewFragment threadViewFragment, int i) {
        C66123Iw r5;
        ThreadSummary threadSummary;
        C140416go A012;
        String str;
        String str2;
        UserKey userKey;
        ThreadSummary threadSummary2;
        String str3;
        C88844Mn r1;
        long j;
        boolean z;
        boolean z2;
        String str4;
        String str5;
        int i2 = AnonymousClass1Y3.A5w;
        AnonymousClass0UN r2 = threadViewFragment.A0D;
        ((AnonymousClass146) AnonymousClass1XX.A02(7, i2, r2)).A0D("tap_top_right_nav");
        C150986z1 r4 = (C150986z1) AnonymousClass1XX.A02(37, AnonymousClass1Y3.AYu, r2);
        if (i == 5) {
            r5 = C66123Iw.A0Q;
        } else if (i == 16) {
            r5 = C66123Iw.A0U;
        } else if (i == 25) {
            r5 = C66123Iw.A0V;
        } else if (i == 7) {
            r5 = C66123Iw.A0S;
        } else if (i == 6) {
            r5 = C66123Iw.A0W;
        } else if (i == 10) {
            r5 = C66123Iw.A0Y;
        } else if (i == 19) {
            r5 = C66123Iw.A0R;
        } else if (i == 20) {
            r5 = C66123Iw.A0T;
        } else if (i == 30) {
            r5 = C66123Iw.A0X;
        } else {
            C010708t.A0O("SearchEndActionFromButtonIdConverter", "Couldn't convert button %s to SearchEndAction", Integer.valueOf(i));
            r5 = C66123Iw.A0Z;
        }
        C148156uH r42 = r4.A00;
        if (r42 != null && r42.A01()) {
            if (r5 == C66123Iw.A0W) {
                C150906yt r22 = new C150906yt();
                r22.A01(AnonymousClass07B.A0B, r5);
                C150826yl r12 = r42.A00.A03;
                if (!C150826yl.A01(r12)) {
                    C150826yl.A00(r12, C145246pO.A08, r22);
                }
            } else if (r42 != null) {
                C150906yt r23 = new C150906yt();
                r23.A01(AnonymousClass07B.A0A, C150946yx.A01);
                r23.A01(AnonymousClass07B.A0B, r5);
                r42.A00(r23);
            }
        }
        boolean A002 = C22541Lt.A00(threadViewFragment.A1j());
        C61152yQ r13 = threadViewFragment.A0Q.A01;
        if (i == 5) {
            ThreadKey threadKey = threadViewFragment.A0H;
            if (ThreadKey.A0B(threadKey) || (ThreadKey.A0F(threadKey) && ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, threadViewFragment.A0D)).Aem(283051229710500L))) {
                Preconditions.checkNotNull(threadViewFragment.A0H);
                threadViewFragment.A0e("Click on Voip call button");
                AnonymousClass33E r3 = threadViewFragment.A0d;
                ThreadKey threadKey2 = threadViewFragment.A0H;
                if (C22541Lt.A00(threadViewFragment.A1j())) {
                    str4 = "chat_head_button";
                } else {
                    str4 = "thread_view_button";
                }
                NavigationTrigger navigationTrigger = threadViewFragment.A0M;
                if (navigationTrigger != null) {
                    str5 = navigationTrigger.toString();
                } else {
                    str5 = null;
                }
                r3.A04(threadKey2, str4, str5);
                A0J(threadViewFragment);
                A0Q(threadViewFragment);
                r1 = (C88844Mn) AnonymousClass1XX.A02(36, AnonymousClass1Y3.AH5, threadViewFragment.A0D);
                j = threadViewFragment.A0H.A01;
                z = false;
                z2 = false;
            } else {
                return;
            }
        } else if (i == 16) {
            threadViewFragment.A0h(false, A002);
            return;
        } else if (i == 25) {
            threadViewFragment.A0h(true, A002);
            return;
        } else if (i == 7) {
            threadViewFragment.A0e("Click on invite button");
            threadViewFragment.A0c.A12.A0I("invite_button");
            return;
        } else if (i == 6) {
            threadViewFragment.A0e("Click on thread setting button");
            A0U(threadViewFragment, 0);
            return;
        } else if (i == 10) {
            if (threadViewFragment.A0H != null) {
                String str6 = null;
                if (r13 != null) {
                    threadSummary2 = r13.A02;
                } else {
                    threadSummary2 = null;
                }
                threadViewFragment.A0e("Click on video call button");
                AnonymousClass33E r14 = threadViewFragment.A0d;
                ThreadKey threadKey3 = threadViewFragment.A0H;
                User user = threadViewFragment.A0k;
                if (ThreadKey.A0E(threadKey3)) {
                    if (A002) {
                        str3 = "sms_chat_head_video_button";
                    } else {
                        str3 = "sms_thread_view_video_button";
                    }
                } else if (A002) {
                    str3 = "chat_video_button";
                } else {
                    str3 = "thread_view_button_video";
                }
                NavigationTrigger navigationTrigger2 = threadViewFragment.A0M;
                if (navigationTrigger2 != null) {
                    str6 = navigationTrigger2.toString();
                }
                r14.A02(threadKey3, threadSummary2, user, str3, str6, A002);
                A0Q(threadViewFragment);
                r1 = (C88844Mn) AnonymousClass1XX.A02(36, AnonymousClass1Y3.AH5, threadViewFragment.A0D);
                j = threadViewFragment.A0H.A01;
                z = false;
                z2 = true;
            } else {
                return;
            }
        } else if (i == 19) {
            A0J(threadViewFragment);
            AnonymousClass33E r32 = threadViewFragment.A0d;
            Context A1j = threadViewFragment.A1j();
            ThreadKey threadKey4 = threadViewFragment.A0H;
            User user2 = threadViewFragment.A0k;
            if (ThreadKey.A0B(threadKey4)) {
                userKey = ThreadKey.A07(threadKey4);
            } else {
                userKey = null;
            }
            if (userKey == null) {
                return;
            }
            if (!A002 || !((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, r32.A00)).Aem(282316790564139L)) {
                C163217gf A003 = RtcCallStartParams.A00();
                A003.A01 = Long.parseLong(userKey.id);
                A003.A02("thread_view_button_video");
                A003.A0L = true;
                ((C56742ql) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BKu, r32.A00)).A09(A1j, A003.A00());
                return;
            }
            AnonymousClass33E.A01(r32, "instant_video_chat_head", user2, new C161757eG(r32, user2, A1j, userKey, "thread_view_button_video"));
            return;
        } else if (i == 20) {
            ThreadSummary threadSummary3 = r13.A02;
            boolean z3 = false;
            if (threadSummary3 != null) {
                z3 = true;
            }
            if (z3) {
                if (A002) {
                    str = "sms_chat_head_phone_picker_audio";
                } else {
                    str = "sms_thread_view_phone_picker_audio";
                }
                if (A002) {
                    str2 = "sms_chat_head_phone_picker_video";
                } else {
                    str2 = "sms_thread_view_phone_picker_video";
                }
                threadViewFragment.A0d.A05(threadSummary3, str, str2);
                return;
            }
            return;
        } else if (i == 30) {
            threadViewFragment.A0c.A0e.Bho();
            return;
        } else if (i == 31 && r13 != null && (A012 = ((C135796Wl) AnonymousClass1XX.A03(AnonymousClass1Y3.ArX, threadViewFragment.A0D)).A01((threadSummary = threadViewFragment.A0Q.A01.A02))) != null) {
            ((C66233Jj) AnonymousClass1XX.A02(44, AnonymousClass1Y3.AWF, threadViewFragment.A0D)).A03(threadViewFragment.A17(), threadSummary, A012, AnonymousClass07B.A05);
            return;
        } else {
            return;
        }
        r1.A01(j, z, z2, threadViewFragment.A0M);
    }

    public static void A0V(ThreadViewFragment threadViewFragment, Intent intent) {
        C13290rA r1 = new C13290rA();
        r1.A01((ThreadKey) intent.getParcelableExtra(C99084oO.$const$string(85)));
        r1.A02(C13220qv.A0I);
        threadViewFragment.A2V(r1.A00());
    }

    public static void A0W(ThreadViewFragment threadViewFragment, ServiceException serviceException, boolean z) {
        View A2K;
        if (!((Boolean) threadViewFragment.A0s.get()).booleanValue() || ((C33701o0) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AxV, threadViewFragment.A0D)).isConnected()) {
            if (threadViewFragment.A0X == null) {
                threadViewFragment.A0X = (C67023Mq) threadViewFragment.A0r.get();
            }
            C67023Mq r2 = threadViewFragment.A0X;
            r2.A03 = LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT;
            r2.A08 = true;
            if (z) {
                A2K = threadViewFragment.A0c.A0P.A0I;
                r2.A00 = 48;
            } else {
                A2K = threadViewFragment.A2K(2131301065);
                threadViewFragment.A0X.A00 = 80;
            }
            threadViewFragment.A0X.A02(A2K, ((C55972p3) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ANl, threadViewFragment.A0D)).A01(serviceException, false, true));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:127:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01f3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0X(com.facebook.orca.threadview.ThreadViewFragment r7, X.C61152yQ r8) {
        /*
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0H
            if (r0 == 0) goto L_0x02e1
            if (r0 == 0) goto L_0x007e
            X.0Tq r0 = r7.A0t
            java.lang.Object r0 = r0.get()
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            java.lang.String r4 = r0.id
            com.facebook.messaging.model.threads.ThreadSummary r1 = r8.A02     // Catch:{ NumberFormatException -> 0x0047 }
            r0 = 0
            if (r1 == 0) goto L_0x0016
            r0 = 1
        L_0x0016:
            if (r0 == 0) goto L_0x001b
            com.facebook.messaging.model.threadkey.ThreadKey r6 = r1.A0S     // Catch:{ NumberFormatException -> 0x0047 }
            goto L_0x005b
        L_0x001b:
            com.facebook.user.model.User r2 = r8.A04     // Catch:{ NumberFormatException -> 0x0047 }
            r0 = 0
            if (r2 == 0) goto L_0x0021
            r0 = 1
        L_0x0021:
            if (r0 == 0) goto L_0x005a
            X.2if r1 = r8.A03     // Catch:{ NumberFormatException -> 0x0047 }
            X.2if r0 = X.C52192if.TINCAN     // Catch:{ NumberFormatException -> 0x0047 }
            if (r1 != r0) goto L_0x0038
            java.lang.String r0 = r2.A0j     // Catch:{ NumberFormatException -> 0x0047 }
            long r2 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x0047 }
            long r0 = java.lang.Long.parseLong(r4)     // Catch:{ NumberFormatException -> 0x0047 }
            com.facebook.messaging.model.threadkey.ThreadKey r6 = com.facebook.messaging.model.threadkey.ThreadKey.A05(r2, r0)     // Catch:{ NumberFormatException -> 0x0047 }
            goto L_0x005b
        L_0x0038:
            java.lang.String r0 = r2.A0j     // Catch:{ NumberFormatException -> 0x0047 }
            long r2 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x0047 }
            long r0 = java.lang.Long.parseLong(r4)     // Catch:{ NumberFormatException -> 0x0047 }
            com.facebook.messaging.model.threadkey.ThreadKey r6 = com.facebook.messaging.model.threadkey.ThreadKey.A04(r2, r0)     // Catch:{ NumberFormatException -> 0x0047 }
            goto L_0x005b
        L_0x0047:
            java.lang.String r3 = "User ID of viewer: "
            java.lang.String r2 = " or other person: "
            com.facebook.user.model.User r0 = r8.A04
            java.lang.String r1 = r0.A0j
            java.lang.String r0 = " is in incorrect format"
            java.lang.String r1 = X.AnonymousClass08S.A0T(r3, r4, r2, r1, r0)
            java.lang.String r0 = "thread_view_loader_incorrect_threadkey"
            X.C010708t.A0K(r0, r1)
        L_0x005a:
            r6 = 0
        L_0x005b:
            if (r6 == 0) goto L_0x007e
            com.facebook.messaging.model.threadkey.ThreadKey r5 = r7.A0H
            boolean r0 = r6.equals(r5)
            if (r0 != 0) goto L_0x00a1
            boolean r0 = r5.A0N()
            if (r0 == 0) goto L_0x009b
            boolean r0 = r6.A0N()
            if (r0 == 0) goto L_0x009b
            long r3 = r5.A01
            long r1 = r6.A01
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0095
            java.lang.String r0 = "thread_view_fragment_threadkey_mismatch_other_user_id"
            r7.A0g(r0, r5, r6)
        L_0x007e:
            r0 = 0
        L_0x007f:
            if (r0 == 0) goto L_0x02e1
            boolean r0 = r8.A06
            if (r0 == 0) goto L_0x00a3
            com.facebook.messaging.model.messages.MessagesCollection r0 = r8.A01
            if (r0 != 0) goto L_0x00a3
            X.2x4 r3 = r7.A0V
            com.facebook.messaging.model.threads.ThreadSummary r2 = r8.A02
            com.facebook.user.model.User r1 = r8.A04
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0H
            r3.CCO(r2, r1, r0)
            return
        L_0x0095:
            java.lang.String r0 = "thread_view_fragment_threadkey_mismatch_viewer_user_id"
            r7.A0g(r0, r5, r6)
            goto L_0x007e
        L_0x009b:
            java.lang.String r0 = "thread_view_fragment_threadkey_mismatch"
            r7.A0g(r0, r5, r6)
            goto L_0x007e
        L_0x00a1:
            r0 = 1
            goto L_0x007f
        L_0x00a3:
            X.2x8 r1 = new X.2x8
            r0 = 0
            r1.<init>(r8, r0)
            r7.A0Q = r1
            r7.A0B()
            com.facebook.messaging.model.threads.ThreadSummary r2 = r8.A02
            r0 = 0
            if (r2 == 0) goto L_0x00b4
            r0 = 1
        L_0x00b4:
            if (r0 == 0) goto L_0x02a8
            X.2oz r1 = r7.A0j
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A0S
            r1.A04(r0)
            com.facebook.messaging.model.threads.ThreadSummary r0 = r8.A02
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r0.A0S
            X.0u1 r0 = r7.A0K
            com.facebook.messaging.model.threads.NotificationSetting r0 = r0.A04(r1)
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x00e1
            X.1Y7 r3 = X.C05690aA.A08(r1)
            com.facebook.prefs.shared.FbSharedPreferences r0 = r7.A0g
            X.1hn r2 = r0.edit()
            com.facebook.messaging.model.threads.NotificationSetting r0 = com.facebook.messaging.model.threads.NotificationSetting.A06
            long r0 = r0.A01()
            r2.BzA(r3, r0)
            r2.commit()
        L_0x00e1:
            com.facebook.prefs.shared.FbSharedPreferences r2 = r7.A0g
            com.facebook.messaging.model.threads.ThreadSummary r0 = r8.A02
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            X.1Y7 r1 = X.C05690aA.A08(r0)
            X.0ZM r0 = r7.A1I
            r2.C0f(r1, r0)
            X.2su r1 = r7.A0Y
            if (r1 == 0) goto L_0x00fb
            com.facebook.messaging.model.threads.ThreadSummary r0 = r8.A02
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            r1.A02(r0)
        L_0x00fb:
            java.lang.Integer r0 = r7.A0m
            if (r0 == 0) goto L_0x0109
            int r0 = r0.intValue()
            A0U(r7, r0)
            r0 = 0
            r7.A0m = r0
        L_0x0109:
            com.facebook.messaging.model.threads.ThreadSummary r5 = r8.A02
            if (r5 == 0) goto L_0x02a5
            boolean r0 = r5.A13
            if (r0 == 0) goto L_0x02a5
            r2 = 16
            int r1 = X.AnonymousClass1Y3.AyS
            X.0UN r0 = r7.A0D
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3RZ r2 = (X.AnonymousClass3RZ) r2
            r1 = 0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0S
            com.facebook.user.model.User r0 = r2.A01(r1, r0)
            if (r0 == 0) goto L_0x02a5
            boolean r0 = r0.A0F()
            if (r0 == 0) goto L_0x02a5
            r2 = 21
            int r1 = X.AnonymousClass1Y3.AWS
            X.0UN r0 = r7.A0D
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3EI r0 = (X.AnonymousClass3EI) r0
            boolean r0 = X.AnonymousClass3EI.A01(r0)
            r2 = 22
            r4 = 1
            if (r0 != 0) goto L_0x0267
            r0 = 2131299650(0x7f090d42, float:1.8217307E38)
            com.google.common.base.Optional r4 = r7.A2L(r0)
            if (r4 == 0) goto L_0x02a5
            boolean r0 = r4.isPresent()
            if (r0 == 0) goto L_0x02a5
            int r1 = X.AnonymousClass1Y3.AYe
            X.0UN r0 = r7.A0D
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Bz r3 = (X.C20331Bz) r3
            android.content.Context r2 = r7.A1j()
            com.facebook.interstitial.triggers.InterstitialTrigger r1 = new com.facebook.interstitial.triggers.InterstitialTrigger
            com.facebook.interstitial.triggers.InterstitialTrigger$Action r0 = com.facebook.interstitial.triggers.InterstitialTrigger.Action.A3j
            r1.<init>(r0)
            java.lang.Class<X.1mO> r0 = X.C32821mO.class
            boolean r0 = r3.A03(r2, r1, r0, r7)
            if (r0 == 0) goto L_0x02a5
            r2 = 13
            int r1 = X.AnonymousClass1Y3.AaR
            X.0UN r0 = r7.A0D
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3SG r1 = (X.AnonymousClass3SG) r1
            java.lang.Object r0 = r4.get()
            android.view.View r0 = (android.view.View) r0
            r1.A04(r5, r0)
            r0 = 1
        L_0x0183:
            if (r0 != 0) goto L_0x01ef
            com.facebook.messaging.model.threads.ThreadSummary r4 = r8.A02
            boolean r0 = r7.A0z
            if (r0 != 0) goto L_0x01ef
            if (r4 == 0) goto L_0x01ef
            long r5 = r4.A03
            r1 = 3
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x01ef
            r2 = 16
            int r1 = X.AnonymousClass1Y3.AyS
            X.0UN r0 = r7.A0D
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3RZ r2 = (X.AnonymousClass3RZ) r2
            r1 = 0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r4.A0S
            com.facebook.user.model.User r0 = r2.A01(r1, r0)
            if (r0 == 0) goto L_0x01ef
            boolean r0 = r0.A0F()
            if (r0 == 0) goto L_0x01ef
            com.facebook.prefs.shared.FbSharedPreferences r2 = r7.A0g
            X.1Y7 r1 = X.C05690aA.A1G
            r0 = 0
            int r5 = r2.AqN(r1, r0)
            r0 = 10
            if (r5 > r0) goto L_0x01ef
            r0 = 2131299650(0x7f090d42, float:1.8217307E38)
            com.google.common.base.Optional r3 = r7.A2L(r0)
            boolean r0 = r3.isPresent()
            if (r0 == 0) goto L_0x01ef
            com.facebook.prefs.shared.FbSharedPreferences r0 = r7.A0g
            X.1hn r2 = r0.edit()
            r0 = 1
            int r5 = r5 + r0
            r2.Bz6(r1, r5)
            r2.commit()
            r7.A0z = r0
            r2 = 13
            int r1 = X.AnonymousClass1Y3.AaR
            X.0UN r0 = r7.A0D
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3SG r1 = (X.AnonymousClass3SG) r1
            java.lang.Object r0 = r3.get()
            android.view.View r0 = (android.view.View) r0
            r1.A04(r4, r0)
        L_0x01ef:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r7.A0c
            if (r0 == 0) goto L_0x01f9
            r0 = 2131299650(0x7f090d42, float:1.8217307E38)
            r7.A2L(r0)
        L_0x01f9:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0H
            com.facebook.user.model.UserKey r3 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r0)
            if (r3 == 0) goto L_0x0252
            r2 = 8
            int r1 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r7.A0D
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0r6 r1 = (X.AnonymousClass0r6) r1
            X.1gw r0 = r1.A0G
            java.lang.Object r0 = r0.A05(r3, r3)
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            if (r0 != 0) goto L_0x0222
            X.1gw r0 = r1.A0F
            java.lang.Object r0 = r0.A03(r3)
            if (r0 != 0) goto L_0x0222
            r1.A0R()
        L_0x0222:
            X.0Tq r0 = r7.A0u
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0252
            com.facebook.messaging.model.threads.ThreadSummary r4 = r8.A02
            if (r4 == 0) goto L_0x0252
            X.2xB r3 = r7.A0W
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r3.A00
            if (r0 != 0) goto L_0x0252
            r2 = 6
            int r1 = X.AnonymousClass1Y3.AnB
            X.0UN r0 = r7.A0D
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Ke r0 = (X.C22171Ke) r0
            com.facebook.messaging.ui.name.MessengerThreadNameViewData r0 = r0.A03(r4)
            r3.A07(r0)
            X.2xB r1 = r7.A0W
            r0 = 1
            r1.A08(r0)
        L_0x0252:
            com.facebook.prefs.shared.FbSharedPreferences r2 = r7.A0g
            X.1Y8 r1 = X.C21501Hg.A02
            X.1Y8 r0 = X.C21501Hg.A01
            com.google.common.collect.ImmutableSet r1 = com.google.common.collect.ImmutableSet.A05(r1, r0)
            X.0ZM r0 = r7.A1O
            r2.C0h(r1, r0)
            java.lang.String r0 = "thread_load_new_result"
            A0Z(r7, r0)
            return
        L_0x0267:
            int r1 = X.AnonymousClass1Y3.AYe
            X.0UN r0 = r7.A0D
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Bz r3 = (X.C20331Bz) r3
            android.content.Context r2 = r7.A1j()
            com.facebook.interstitial.triggers.InterstitialTrigger r1 = new com.facebook.interstitial.triggers.InterstitialTrigger
            com.facebook.interstitial.triggers.InterstitialTrigger$Action r0 = com.facebook.interstitial.triggers.InterstitialTrigger.Action.A3j
            r1.<init>(r0)
            java.lang.Class<X.1mO> r0 = X.C32821mO.class
            boolean r0 = r3.A03(r2, r1, r0, r7)
            if (r0 == 0) goto L_0x02a5
            A0a(r7, r4)
            r2 = 34
            int r1 = X.AnonymousClass1Y3.B4H
            X.0UN r0 = r7.A0D
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.5VP r4 = (X.AnonymousClass5VP) r4
            if (r5 == 0) goto L_0x02a3
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r5.A0S
        L_0x0297:
            java.lang.Integer r2 = X.AnonymousClass07B.A0N
            java.lang.String r1 = "messenger_business_integrity_page_subscription_nux"
            java.lang.String r0 = "page_subscription_nux_opened"
            X.AnonymousClass5VP.A01(r4, r1, r0, r3, r2)
            r0 = 1
            goto L_0x0183
        L_0x02a3:
            r3 = 0
            goto L_0x0297
        L_0x02a5:
            r0 = 0
            goto L_0x0183
        L_0x02a8:
            com.facebook.user.model.User r1 = r8.A04
            r0 = 0
            if (r1 == 0) goto L_0x02ae
            r0 = 1
        L_0x02ae:
            if (r0 == 0) goto L_0x01f9
            X.2oz r1 = r7.A0j
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0H
            r1.A04(r0)
            com.facebook.orca.threadview.ThreadViewMessagesFragment r3 = r7.A0c
            if (r3 == 0) goto L_0x01f9
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r7.A0H
            com.facebook.user.model.User r0 = r8.A04
            com.facebook.user.model.Name r1 = r0.A0L
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0Y
            boolean r0 = com.google.common.base.Objects.equal(r2, r0)
            if (r0 == 0) goto L_0x02da
            com.facebook.user.model.Name r0 = r3.A1A
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 != 0) goto L_0x02da
            r3.A1A = r1
            X.3JO r0 = r3.A12
            r0.A0A = r1
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0W(r3)
        L_0x02da:
            X.3JO r0 = r3.A12
            r0.A0D()
            goto L_0x01f9
        L_0x02e1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A0X(com.facebook.orca.threadview.ThreadViewFragment, X.2yQ):void");
    }

    public static void A0Y(ThreadViewFragment threadViewFragment, Runnable runnable) {
        threadViewFragment.A0p = ((AnonymousClass0VK) AnonymousClass1XX.A02(18, AnonymousClass1Y3.B6E, threadViewFragment.A0D)).C4Y(runnable, 1000, TimeUnit.MILLISECONDS);
    }

    public static void A0Z(ThreadViewFragment threadViewFragment, String str) {
        boolean A0W2;
        boolean z;
        C80043rl r3;
        MessagesCollection messagesCollection;
        C61152yQ r5 = threadViewFragment.A0Q.A01;
        ThreadSummary threadSummary = r5.A02;
        if (!r5.A06 || (messagesCollection = r5.A01) == null || messagesCollection.A04() <= 1) {
            threadViewFragment.A0V.CCO(threadSummary, r5.A04, threadViewFragment.A0H);
            threadViewFragment.A0F(threadSummary);
            threadViewFragment.A0c.A0P.A0O.C9S(C52752jd.A00(threadViewFragment.A03, ((C57762sa) AnonymousClass1XX.A02(12, AnonymousClass1Y3.AJC, threadViewFragment.A0D)).A02(threadSummary, threadViewFragment.A0H)));
            A0R(threadViewFragment);
            C68413Ss r32 = (C68413Ss) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Aek, ((AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, threadViewFragment.A0D)).A01);
            synchronized (r32) {
                C68423St r2 = r32.A02;
                r2.A00 = threadSummary;
                A0W2 = r32.A00.A0W(r2, C68423St.A04);
                r32.A02.A00 = null;
            }
            if (A0W2 && threadViewFragment.A0p == null) {
                A0Y(threadViewFragment, new C136956aY(threadViewFragment, threadSummary));
            }
            if (!(threadSummary == null || threadViewFragment.A0I == null)) {
                Optional A2L = threadViewFragment.A2L(2131301032);
                if (A2L.isPresent()) {
                    AnonymousClass3SG r6 = (AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, threadViewFragment.A0D);
                    View view = (View) A2L.get();
                    if (r6.A02 == AnonymousClass07B.A00) {
                        int i = AnonymousClass1Y3.BPL;
                        AnonymousClass0UN r33 = r6.A01;
                        if (((C401020h) AnonymousClass1XX.A02(8, i, r33)).A01(threadSummary)) {
                            C81953vA r7 = (C81953vA) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A0n, r33);
                            if (!((AnonymousClass1F1) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BE8, r7.A00)).A0W(r7, C81953vA.A01)) {
                                r3 = null;
                            } else {
                                r3 = new C80043rl(view.getContext(), 2);
                                r3.A0M(2131825524);
                                r3.A0F(view);
                                ((AnonymousClass1F1) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BE8, r7.A00)).A0R().A02("5933");
                            }
                            if (r3 != null) {
                                r6.A00 = r3;
                                r6.A02 = AnonymousClass07B.A0n;
                            }
                        }
                    }
                }
            }
            if (!(threadSummary == null || threadViewFragment.A0I == null || !threadViewFragment.A0c.A1d)) {
                Optional A2L2 = threadViewFragment.A2L(2131301032);
                if (A2L2.isPresent()) {
                    AnonymousClass3SG r34 = (AnonymousClass3SG) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AaR, threadViewFragment.A0D);
                    View view2 = (View) A2L2.get();
                    if (r34.A02 == AnonymousClass07B.A00) {
                        C80043rl A002 = AnonymousClass3SG.A00(r34, r34.A03.getString(2131827377), view2, threadSummary.A0S);
                        A002.A0F(view2);
                        r34.A00 = A002;
                        r34.A02 = AnonymousClass07B.A0p;
                    }
                }
            }
            ThreadViewMessagesFragment threadViewMessagesFragment = threadViewFragment.A0c;
            if (threadViewMessagesFragment != null) {
                threadViewMessagesFragment.A2c(r5, threadViewFragment.A0U, str, C68443Sv.A00(threadViewFragment.A1j(), threadSummary));
            }
            A0S(threadViewFragment);
            C57952su r1 = threadViewFragment.A0Y;
            if (r1.A01 == null) {
                if (threadSummary != null) {
                    r1.A02(threadSummary.A0S);
                } else {
                    r1.A02(threadViewFragment.A0H);
                }
            }
            C57952su r52 = threadViewFragment.A0Y;
            ThreadKey threadKey = r52.A01;
            if (threadKey != null && r52.A00 == null) {
                UserKey A072 = ThreadKey.A07(threadKey);
                if (A072 == null) {
                    z = false;
                } else {
                    User A032 = r52.A07.A03(A072);
                    if (A032 != null) {
                        z = !A032.A0F();
                    } else {
                        z = true;
                    }
                }
                if (z) {
                    r52.A04 = r52.A05.CIG("ThreadViewOptionsHandler.setupContactLoader", new AnonymousClass3T1(r52, r52.A01), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, AnonymousClass07B.A00);
                }
            }
        }
    }

    public static void A0a(ThreadViewFragment threadViewFragment, boolean z) {
        AnonymousClass2x4 r1 = threadViewFragment.A0V;
        if (r1 != null) {
            r1.CKJ(threadViewFragment.A1j(), threadViewFragment.A0H, threadViewFragment.A0Q.A01, threadViewFragment.A0k, z, false);
        }
    }

    public static void A0b(ThreadViewFragment threadViewFragment, boolean z) {
        ThreadKey threadKey;
        C56882r2 r2;
        if (!z) {
            A0J(threadViewFragment);
            threadViewFragment.A02 = 0;
        }
        if (!A0j(threadViewFragment) && (threadKey = threadViewFragment.A0H) != null && (r2 = threadViewFragment.A0i) != null) {
            if ((!z || threadViewFragment.A1K) && !ThreadKey.A0F(threadKey)) {
                r2.A07(String.valueOf(threadKey.A01), z ? 1 : 0);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: X.3SU} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: java.lang.String} */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARN: Type inference failed for: r3v13 */
    /* JADX WARN: Type inference failed for: r3v14 */
    /* JADX WARN: Type inference failed for: r3v15 */
    /* JADX WARN: Type inference failed for: r3v16 */
    /* JADX WARN: Type inference failed for: r3v17 */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009c, code lost:
        if (r1 >= 20) goto L_0x009f;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0c(com.facebook.orca.threadview.ThreadViewFragment r8, boolean r9, boolean r10, java.lang.String r11) {
        /*
            java.lang.Class<com.facebook.orca.threadview.ThreadViewFragment> r4 = com.facebook.orca.threadview.ThreadViewFragment.class
            java.lang.String r1 = "ThreadViewFragment.refreshDataInternal"
            r0 = -744094878(0xffffffffd3a60362, float:-1.42604265E12)
            X.C005505z.A03(r1, r0)
            X.0Tq r0 = r8.A0u     // Catch:{ all -> 0x01de }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x01de }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x01de }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x01de }
            java.lang.String r5 = "ThreadViewFragment"
            if (r0 == 0) goto L_0x001f
            if (r9 == 0) goto L_0x001f
            A0Q(r8)     // Catch:{ all -> 0x01de }
        L_0x001f:
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r8.A0H     // Catch:{ all -> 0x01de }
            if (r2 != 0) goto L_0x002a
            r0 = -199382075(0xfffffffff41dabc5, float:-4.9967926E31)
            X.C005505z.A00(r0)
            return
        L_0x002a:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r6 = r8.A0c     // Catch:{ all -> 0x01de }
            if (r6 == 0) goto L_0x0035
            if (r10 != 0) goto L_0x0032
            if (r9 == 0) goto L_0x0035
        L_0x0032:
            r0 = 1
            r6.A1O = r0     // Catch:{ all -> 0x01de }
        L_0x0035:
            r3 = 0
            if (r9 == 0) goto L_0x0068
            X.3SS r1 = new X.3SS     // Catch:{ all -> 0x01de }
            r1.<init>()     // Catch:{ all -> 0x01de }
            r1.A00(r2)     // Catch:{ all -> 0x01de }
            X.3ST r0 = X.AnonymousClass3ST.THREAD_VIEW     // Catch:{ all -> 0x01de }
            r1.A04 = r0     // Catch:{ all -> 0x01de }
            r0 = 1
            r1.A0B = r0     // Catch:{ all -> 0x01de }
            r1.A0A = r0     // Catch:{ all -> 0x01de }
            r0 = 20
            r1.A00 = r0     // Catch:{ all -> 0x01de }
            r1.A08 = r10     // Catch:{ all -> 0x01de }
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A07(r4, r11)     // Catch:{ all -> 0x01de }
            r1.A02 = r0     // Catch:{ all -> 0x01de }
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r0 = r8.A0R     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x005b
            java.lang.String r3 = r0.A01     // Catch:{ all -> 0x01de }
        L_0x005b:
            r1.A06 = r3     // Catch:{ all -> 0x01de }
            java.lang.String r0 = r8.A1J     // Catch:{ all -> 0x01de }
            r1.A07 = r0     // Catch:{ all -> 0x01de }
            X.3SU r3 = new X.3SU     // Catch:{ all -> 0x01de }
            r3.<init>(r1)     // Catch:{ all -> 0x01de }
            goto L_0x017c
        L_0x0068:
            r2 = -1
            if (r6 == 0) goto L_0x009e
            X.3EO r0 = r6.A17     // Catch:{ all -> 0x01de }
            com.facebook.threadview.ThreadViewMessagesRecyclerView r7 = r0.A0I     // Catch:{ all -> 0x01de }
            if (r7 == 0) goto L_0x009e
            r1 = 0
            X.2yQ r0 = r6.A0i     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x0089
            com.facebook.messaging.model.messages.MessagesCollection r0 = r0.A01     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x007e
            int r1 = r0.A04()     // Catch:{ all -> 0x01de }
        L_0x007e:
            X.2yQ r0 = r6.A0i     // Catch:{ all -> 0x01de }
            com.google.common.collect.ImmutableList r0 = r0.A05     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x0089
            int r0 = r0.size()     // Catch:{ all -> 0x01de }
            int r1 = r1 + r0
        L_0x0089:
            if (r1 == 0) goto L_0x009e
            int r0 = r7.getChildCount()     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x009e
            X.3EO r0 = r6.A17     // Catch:{ all -> 0x01de }
            int r0 = r0.A04()     // Catch:{ all -> 0x01de }
            int r1 = r1 - r0
            int r1 = r1 + 10
            r0 = 20
            if (r1 >= r0) goto L_0x009f
        L_0x009e:
            r1 = -1
        L_0x009f:
            if (r1 != r2) goto L_0x014b
            com.facebook.messaging.model.threadkey.ThreadKey r6 = r8.A0H     // Catch:{ all -> 0x01de }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0D(r6)     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x011b
            int r1 = X.AnonymousClass1Y3.Ags     // Catch:{ 3Dq -> 0x00f7 }
            X.0UN r0 = r8.A0D     // Catch:{ 3Dq -> 0x00f7 }
            r2 = 17
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ 3Dq -> 0x00f7 }
            X.B4H r1 = (X.B4H) r1     // Catch:{ 3Dq -> 0x00f7 }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0D(r6)     // Catch:{ 3Dq -> 0x00f7 }
            com.google.common.base.Preconditions.checkArgument(r0)     // Catch:{ 3Dq -> 0x00f7 }
            java.util.Map r0 = r1.A03     // Catch:{ 3Dq -> 0x00f7 }
            boolean r0 = r0.containsKey(r6)     // Catch:{ 3Dq -> 0x00f7 }
            if (r0 == 0) goto L_0x017c
            int r1 = X.AnonymousClass1Y3.Ags     // Catch:{ 3Dq -> 0x00f7 }
            X.0UN r0 = r8.A0D     // Catch:{ 3Dq -> 0x00f7 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ 3Dq -> 0x00f7 }
            X.B4H r1 = (X.B4H) r1     // Catch:{ 3Dq -> 0x00f7 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0H     // Catch:{ 3Dq -> 0x00f7 }
            X.B4I r0 = X.B4H.A01(r1, r0)     // Catch:{ 3Dq -> 0x00f7 }
            com.google.common.collect.ImmutableList r2 = r0.A03     // Catch:{ 3Dq -> 0x00f7 }
            X.3SS r1 = new X.3SS     // Catch:{ 3Dq -> 0x00f7 }
            r1.<init>()     // Catch:{ 3Dq -> 0x00f7 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0H     // Catch:{ 3Dq -> 0x00f7 }
            r1.A00(r0)     // Catch:{ 3Dq -> 0x00f7 }
            X.3ST r0 = X.AnonymousClass3ST.THREAD_VIEW     // Catch:{ 3Dq -> 0x00f7 }
            r1.A04 = r0     // Catch:{ 3Dq -> 0x00f7 }
            r1.A05 = r2     // Catch:{ 3Dq -> 0x00f7 }
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A07(r4, r11)     // Catch:{ 3Dq -> 0x00f7 }
            r1.A02 = r0     // Catch:{ 3Dq -> 0x00f7 }
            java.lang.String r0 = r8.A1J     // Catch:{ 3Dq -> 0x00f7 }
            r1.A07 = r0     // Catch:{ 3Dq -> 0x00f7 }
            X.3SU r0 = new X.3SU     // Catch:{ 3Dq -> 0x00f7 }
            r0.<init>(r1)     // Catch:{ 3Dq -> 0x00f7 }
            goto L_0x017b
        L_0x00f7:
            r4 = move-exception
            r2 = 14
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x01de }
            X.0UN r0 = r8.A0D     // Catch:{ all -> 0x01de }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01de }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x01de }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01de }
            r1.<init>()     // Catch:{ all -> 0x01de }
            java.lang.String r0 = "PendingThreadsManager doesn't have pending thread key: "
            r1.append(r0)     // Catch:{ all -> 0x01de }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0H     // Catch:{ all -> 0x01de }
            r1.append(r0)     // Catch:{ all -> 0x01de }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x01de }
            r2.softReport(r5, r0, r4)     // Catch:{ all -> 0x01de }
            goto L_0x017c
        L_0x011b:
            X.3SS r2 = new X.3SS     // Catch:{ all -> 0x01de }
            r2.<init>()     // Catch:{ all -> 0x01de }
            r2.A00(r6)     // Catch:{ all -> 0x01de }
            X.3ST r0 = X.AnonymousClass3ST.THREAD_VIEW     // Catch:{ all -> 0x01de }
            r2.A04 = r0     // Catch:{ all -> 0x01de }
            r0 = 20
            r2.A00 = r0     // Catch:{ all -> 0x01de }
            r2.A08 = r10     // Catch:{ all -> 0x01de }
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A07(r4, r11)     // Catch:{ all -> 0x01de }
            r2.A02 = r0     // Catch:{ all -> 0x01de }
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r0 = r8.A0R     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x0139
            java.lang.String r3 = r0.A01     // Catch:{ all -> 0x01de }
        L_0x0139:
            r2.A06 = r3     // Catch:{ all -> 0x01de }
            long r0 = r8.A04()     // Catch:{ all -> 0x01de }
            r2.A01 = r0     // Catch:{ all -> 0x01de }
            java.lang.String r0 = r8.A1J     // Catch:{ all -> 0x01de }
            r2.A07 = r0     // Catch:{ all -> 0x01de }
            X.3SU r3 = new X.3SU     // Catch:{ all -> 0x01de }
            r3.<init>(r2)     // Catch:{ all -> 0x01de }
            goto L_0x017c
        L_0x014b:
            X.3SS r2 = new X.3SS     // Catch:{ all -> 0x01de }
            r2.<init>()     // Catch:{ all -> 0x01de }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0H     // Catch:{ all -> 0x01de }
            r2.A00(r0)     // Catch:{ all -> 0x01de }
            X.3ST r0 = X.AnonymousClass3ST.THREAD_VIEW     // Catch:{ all -> 0x01de }
            r2.A04 = r0     // Catch:{ all -> 0x01de }
            r2.A00 = r1     // Catch:{ all -> 0x01de }
            r2.A08 = r10     // Catch:{ all -> 0x01de }
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A07(r4, r11)     // Catch:{ all -> 0x01de }
            r2.A02 = r0     // Catch:{ all -> 0x01de }
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r0 = r8.A0R     // Catch:{ all -> 0x01de }
            if (r0 == 0) goto L_0x0169
            java.lang.String r3 = r0.A01     // Catch:{ all -> 0x01de }
        L_0x0169:
            r2.A06 = r3     // Catch:{ all -> 0x01de }
            long r0 = r8.A04()     // Catch:{ all -> 0x01de }
            r2.A01 = r0     // Catch:{ all -> 0x01de }
            java.lang.String r0 = r8.A1J     // Catch:{ all -> 0x01de }
            r2.A07 = r0     // Catch:{ all -> 0x01de }
            X.3SU r3 = new X.3SU     // Catch:{ all -> 0x01de }
            r3.<init>(r2)     // Catch:{ all -> 0x01de }
            goto L_0x017c
        L_0x017b:
            r3 = r0
        L_0x017c:
            if (r3 == 0) goto L_0x01d7
            r8.A08()     // Catch:{ all -> 0x01de }
            r1 = 4
            int r0 = X.AnonymousClass1Y3.B3a     // Catch:{ all -> 0x01de }
            X.0UN r4 = r8.A0D     // Catch:{ all -> 0x01de }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r4)     // Catch:{ all -> 0x01de }
            X.00x r0 = (X.C001300x) r0     // Catch:{ all -> 0x01de }
            X.00z r1 = r0.A02     // Catch:{ all -> 0x01de }
            X.00z r0 = X.C001500z.A08     // Catch:{ all -> 0x01de }
            if (r1 != r0) goto L_0x01c9
            int r0 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x01de }
            r2 = 43
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r0, r4)     // Catch:{ all -> 0x01de }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x01de }
            r4 = 36241412(0x2290004, float:1.2416164E-37)
            boolean r0 = r0.isMarkerOn(r4)     // Catch:{ all -> 0x01de }
            if (r0 != 0) goto L_0x01c9
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x01de }
            X.0UN r0 = r8.A0D     // Catch:{ all -> 0x01de }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01de }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x01de }
            r0.markerStart(r4)     // Catch:{ all -> 0x01de }
            X.0UN r0 = r8.A0D     // Catch:{ all -> 0x01de }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01de }
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2     // Catch:{ all -> 0x01de }
            java.lang.String r1 = "page_id"
            X.0Tq r0 = r8.A0t     // Catch:{ all -> 0x01de }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x01de }
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0     // Catch:{ all -> 0x01de }
            java.lang.String r0 = r0.id     // Catch:{ all -> 0x01de }
            r2.markerAnnotate(r4, r1, r0)     // Catch:{ all -> 0x01de }
        L_0x01c9:
            X.2oy r0 = r8.A0P     // Catch:{ all -> 0x01de }
            X.2o7 r1 = r0.A01()     // Catch:{ all -> 0x01de }
            X.1Ap r0 = r8.A0C     // Catch:{ all -> 0x01de }
            r1.C6Z(r0)     // Catch:{ all -> 0x01de }
            r1.CHB(r3)     // Catch:{ all -> 0x01de }
        L_0x01d7:
            r0 = -361095391(0xffffffffea7a1f21, float:-7.5594614E25)
            X.C005505z.A00(r0)
            return
        L_0x01de:
            r1 = move-exception
            r0 = -1868357374(0xffffffff90a32102, float:-6.434302E-29)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A0c(com.facebook.orca.threadview.ThreadViewFragment, boolean, boolean, java.lang.String):void");
    }

    private void A0d(String str) {
        if (this.A0J != null && this.A0H != null) {
            AnonymousClass108 r1 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, this.A0D);
            r1.A02(new AnonymousClass3T3(this, str));
            r1.A02 = "ClearNotification";
            r1.A03("ForUiThread");
            r1.A04 = this.A0H.toString();
            ((AnonymousClass0g4) AnonymousClass1XX.A02(29, AnonymousClass1Y3.ASI, this.A0D)).A04(r1.A01(), "KeepExisting");
        }
    }

    private void A0e(String str) {
        ((C14220so) AnonymousClass1XX.A02(15, AnonymousClass1Y3.A6G, this.A0D)).A01(str, AnonymousClass07B.A0o);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(27:0|1|2|(1:4)|5|(2:7|(3:9|(1:11)(1:37)|12))|13|(1:15)(1:36)|16|(1:35)|20|(3:28|38|39)|40|41|(1:43)(1:44)|45|(3:(4:48|(1:56)|58|(2:64|(1:68)))|70|(1:72))(28:73|74|75|(1:79)|81|83|84|85|(2:87|89)(1:88)|90|(1:92)|93|(1:95)|96|(1:98)|99|(1:101)|102|(1:104)|105|(1:107)|108|(1:110)|111|(1:113)|114|115|116)|117|(1:119)|120|(1:122)|123|(1:125)|126|(3:128|(1:130)|131)|132|133) */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0165, code lost:
        if (r1 == false) goto L_0x0167;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01a9, code lost:
        if (((X.C420527z) X.AnonymousClass1XX.A02(23, X.AnonymousClass1Y3.AEE, r8.A0M)).A0N() != false) goto L_0x01ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01e6, code lost:
        if (r6.equals(X.B4H.A01((X.B4H) X.AnonymousClass1XX.A02(19, X.AnonymousClass1Y3.Ags, r8.A0M), r7).A00) == false) goto L_0x01e8;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x0134 */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x02e7 A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x02f8 A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0309 A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0318 A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0139 A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0140 A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x014c A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01b6 A[Catch:{ all -> 0x0346, all -> 0x034e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0f(java.lang.String r17) {
        /*
            r16 = this;
            java.lang.String r1 = "setupThreadViewFragment"
            r0 = -1769507164(0xffffffff968776a4, float:-2.18853E-25)
            X.C005505z.A03(r1, r0)
            X.18m r0 = X.C195218m.A0A     // Catch:{ all -> 0x034e }
            r3 = r16
            r3.A0h = r0     // Catch:{ all -> 0x034e }
            X.2xB r0 = r3.A0W     // Catch:{ all -> 0x034e }
            r2 = 0
            if (r0 == 0) goto L_0x0016
            r0.A07(r2)     // Catch:{ all -> 0x034e }
        L_0x0016:
            X.2x8 r0 = X.C60402x8.A02     // Catch:{ all -> 0x034e }
            r3.A0Q = r0     // Catch:{ all -> 0x034e }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0H     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0040
            r3.A08()     // Catch:{ all -> 0x034e }
            X.2x4 r0 = r3.A0V     // Catch:{ all -> 0x034e }
            boolean r0 = r0.BFQ()     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0040
            A0Q(r3)     // Catch:{ all -> 0x034e }
            X.2x8 r0 = r3.A0Q     // Catch:{ all -> 0x034e }
            X.2yQ r0 = r0.A01     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x00e9
            X.2x4 r5 = r3.A0V     // Catch:{ all -> 0x034e }
            com.facebook.messaging.model.threads.ThreadSummary r4 = r0.A02     // Catch:{ all -> 0x034e }
            com.facebook.user.model.User r1 = r0.A04     // Catch:{ all -> 0x034e }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0H     // Catch:{ all -> 0x034e }
            r5.CCO(r4, r1, r0)     // Catch:{ all -> 0x034e }
        L_0x003d:
            A0R(r3)     // Catch:{ all -> 0x034e }
        L_0x0040:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r8 = r3.A0c     // Catch:{ all -> 0x034e }
            com.facebook.messaging.model.threadkey.ThreadKey r6 = r3.A0H     // Catch:{ all -> 0x034e }
            X.0qv r10 = r3.A0U     // Catch:{ all -> 0x034e }
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r9 = r3.A0R     // Catch:{ all -> 0x034e }
            r8.A0u = r10     // Catch:{ all -> 0x034e }
            int r4 = X.AnonymousClass1Y3.BHX     // Catch:{ all -> 0x034e }
            X.0UN r1 = r8.A0M     // Catch:{ all -> 0x034e }
            r0 = 92
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r4, r1)     // Catch:{ all -> 0x034e }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x034e }
            long r0 = r0.now()     // Catch:{ all -> 0x034e }
            r8.A05 = r0     // Catch:{ all -> 0x034e }
            int r1 = r10.ordinal()     // Catch:{ all -> 0x034e }
            r0 = 30
            if (r1 == r0) goto L_0x00e5
            X.3Rp r5 = X.C68133Rn.A01     // Catch:{ all -> 0x034e }
        L_0x0066:
            r8.A0o = r5     // Catch:{ all -> 0x034e }
            int r4 = X.AnonymousClass1Y3.BLC     // Catch:{ all -> 0x034e }
            X.0UN r1 = r8.A0M     // Catch:{ all -> 0x034e }
            r0 = 112(0x70, float:1.57E-43)
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r4, r1)     // Catch:{ all -> 0x034e }
            X.3Lj r0 = (X.C66723Lj) r0     // Catch:{ all -> 0x034e }
            r0.A02 = r5     // Catch:{ all -> 0x034e }
            r4 = r17
            java.lang.String r0 = "initial_setup"
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x00c9
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0Y     // Catch:{ all -> 0x034e }
            boolean r0 = com.google.common.base.Objects.equal(r6, r0)     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x00c9
        L_0x0088:
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0R(r8)     // Catch:{ all -> 0x034e }
            int r5 = X.AnonymousClass1Y3.AQk     // Catch:{ all -> 0x034e }
            X.0UN r1 = r8.A0M     // Catch:{ all -> 0x034e }
            r0 = 17
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r5, r1)     // Catch:{ all -> 0x034e }
            X.3Rr r7 = (X.C68163Rr) r7     // Catch:{ all -> 0x034e }
            if (r6 == 0) goto L_0x0134
            boolean r0 = r6.A0N()     // Catch:{ all -> 0x034e }
            if (r0 != 0) goto L_0x00a5
            boolean r0 = r6.A0L()     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0134
        L_0x00a5:
            int r5 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x034e }
            X.0UN r1 = r7.A00     // Catch:{ all -> 0x034e }
            r0 = 1
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r5, r1)     // Catch:{ all -> 0x034e }
            X.1Yd r5 = (X.C25051Yd) r5     // Catch:{ all -> 0x034e }
            r0 = 284936725402906(0x10326004d151a, double:1.407774472600767E-309)
            boolean r0 = r5.Aem(r0)     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0134
            X.3Rs r13 = new X.3Rs     // Catch:{ all -> 0x034e }
            long r0 = r6.A0G()     // Catch:{ all -> 0x034e }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x034e }
            r13.<init>(r0)     // Catch:{ all -> 0x034e }
            goto L_0x00f7
        L_0x00c9:
            android.content.Context r0 = r8.A1j()     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0088
            X.0qW r1 = r8.A17()     // Catch:{ all -> 0x034e }
            java.lang.String r0 = "add_to_montage_dialog"
            androidx.fragment.app.Fragment r1 = r1.A0Q(r0)     // Catch:{ all -> 0x034e }
            com.facebook.messaging.montage.upsell.AddToMontageDialogFragment r1 = (com.facebook.messaging.montage.upsell.AddToMontageDialogFragment) r1     // Catch:{ all -> 0x034e }
            if (r1 == 0) goto L_0x0088
            boolean r0 = r1.A0i     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0088
            r1.A22()     // Catch:{ all -> 0x034e }
            goto L_0x0088
        L_0x00e5:
            X.3Rp r5 = X.C68133Rn.A00     // Catch:{ all -> 0x034e }
            goto L_0x0066
        L_0x00e9:
            X.2x4 r1 = r3.A0V     // Catch:{ all -> 0x034e }
            r0 = 2131833831(0x7f1133e7, float:1.9300755E38)
            java.lang.String r0 = r3.A1A(r0)     // Catch:{ all -> 0x034e }
            r1.CCY(r0)     // Catch:{ all -> 0x034e }
            goto L_0x003d
        L_0x00f7:
            X.1so r12 = new X.1so     // Catch:{ 3bP -> 0x0134 }
            X.1sp r0 = new X.1sp     // Catch:{ 3bP -> 0x0134 }
            r0.<init>()     // Catch:{ 3bP -> 0x0134 }
            r12.<init>(r0)     // Catch:{ 3bP -> 0x0134 }
            X.1uS r1 = new X.1uS     // Catch:{ 3bP -> 0x0134 }
            java.lang.String r0 = ""
            r1.<init>(r0)     // Catch:{ 3bP -> 0x0134 }
            byte[] r11 = r12.A00(r1)     // Catch:{ 3bP -> 0x0134 }
            X.3Rt r5 = new X.3Rt     // Catch:{ 3bP -> 0x0134 }
            r0 = 0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)     // Catch:{ 3bP -> 0x0134 }
            byte[] r0 = r12.A00(r13)     // Catch:{ 3bP -> 0x0134 }
            r5.<init>(r1, r0)     // Catch:{ 3bP -> 0x0134 }
            byte[] r0 = r12.A00(r5)     // Catch:{ 3bP -> 0x0134 }
            byte[][] r0 = new byte[][]{r11, r0}     // Catch:{ 3bP -> 0x0134 }
            byte[] r0 = X.C68193Ru.A00(r0)     // Catch:{ 3bP -> 0x0134 }
            java.util.concurrent.ExecutorService r5 = r7.A01     // Catch:{ 3bP -> 0x0134 }
            X.3Rv r1 = new X.3Rv     // Catch:{ 3bP -> 0x0134 }
            r1.<init>(r7, r0)     // Catch:{ 3bP -> 0x0134 }
            r0 = -1929072544(0xffffffff8d04b060, float:-4.0887944E-31)
            X.AnonymousClass07A.A04(r5, r1, r0)     // Catch:{ 3bP -> 0x0134 }
        L_0x0134:
            r8.A0p = r9     // Catch:{ all -> 0x034e }
            r0 = -1
            if (r9 == 0) goto L_0x0140
            X.2JL r0 = com.facebook.orca.threadview.ThreadViewMessagesFragment.A0A(r8, r9, r0)     // Catch:{ all -> 0x034e }
            r8.A0y = r0     // Catch:{ all -> 0x034e }
            goto L_0x0143
        L_0x0140:
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0S(r8)     // Catch:{ all -> 0x034e }
        L_0x0143:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0Y     // Catch:{ all -> 0x034e }
            boolean r0 = com.google.common.base.Objects.equal(r6, r0)     // Catch:{ all -> 0x034e }
            r12 = 0
            if (r0 == 0) goto L_0x01b6
            if (r6 == 0) goto L_0x01ab
            X.3U3 r0 = r8.A0l     // Catch:{ all -> 0x034e }
            X.3U4 r1 = r0.A00     // Catch:{ all -> 0x034e }
            if (r1 == 0) goto L_0x0167
            X.3U4 r0 = X.AnonymousClass3U3.A04     // Catch:{ all -> 0x034e }
            if (r1 == r0) goto L_0x0167
            android.view.View r0 = r1.A01     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0167
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A02     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0167
            boolean r1 = r0.equals(r6)     // Catch:{ all -> 0x034e }
            r0 = 1
            if (r1 != 0) goto L_0x0168
        L_0x0167:
            r0 = 0
        L_0x0168:
            if (r0 == 0) goto L_0x01ab
            java.util.List r0 = r8.A1K     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0174
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x01ab
        L_0x0174:
            r1 = 49
            int r0 = X.AnonymousClass1Y3.BK3     // Catch:{ all -> 0x034e }
            X.0UN r5 = r8.A0M     // Catch:{ all -> 0x034e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r5)     // Catch:{ all -> 0x034e }
            X.3Vn r0 = (X.C69073Vn) r0     // Catch:{ all -> 0x034e }
            boolean r0 = r0.A02     // Catch:{ all -> 0x034e }
            if (r0 != 0) goto L_0x01ab
            r1 = 32
            int r0 = X.AnonymousClass1Y3.AIv     // Catch:{ all -> 0x034e }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r0, r5)     // Catch:{ all -> 0x034e }
            X.383 r5 = (X.AnonymousClass383) r5     // Catch:{ all -> 0x034e }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r8.A0Y     // Catch:{ all -> 0x034e }
            java.util.List r0 = r8.A1M     // Catch:{ all -> 0x034e }
            boolean r0 = r5.A07(r1, r0)     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x01ab
            r5 = 23
            int r1 = X.AnonymousClass1Y3.AEE     // Catch:{ all -> 0x034e }
            X.0UN r0 = r8.A0M     // Catch:{ all -> 0x034e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x034e }
            X.27z r0 = (X.C420527z) r0     // Catch:{ all -> 0x034e }
            boolean r1 = r0.A0N()     // Catch:{ all -> 0x034e }
            r0 = 1
            if (r1 == 0) goto L_0x01ac
        L_0x01ab:
            r0 = 0
        L_0x01ac:
            if (r0 == 0) goto L_0x02d5
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0o(r8, r12)     // Catch:{ all -> 0x034e }
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0Q(r8)     // Catch:{ all -> 0x034e }
            goto L_0x02d5
        L_0x01b6:
            com.google.common.base.Preconditions.checkNotNull(r6)     // Catch:{ all -> 0x034e }
            r8.A2V()     // Catch:{ all -> 0x034e }
            r1 = 252786688(0xf113800, float:7.159837E-30)
            java.lang.String r0 = "ThreadViewMessagesFragment.setThread"
            X.C005505z.A03(r0, r1)     // Catch:{ all -> 0x034e }
            r11 = 1
            com.facebook.messaging.model.threadkey.ThreadKey r7 = r8.A0Y     // Catch:{ 3Dq -> 0x01e8 }
            if (r7 == 0) goto L_0x01e8
            boolean r0 = r7.A0P()     // Catch:{ 3Dq -> 0x01e8 }
            if (r0 == 0) goto L_0x01e8
            r5 = 19
            int r1 = X.AnonymousClass1Y3.Ags     // Catch:{ 3Dq -> 0x01e8 }
            X.0UN r0 = r8.A0M     // Catch:{ 3Dq -> 0x01e8 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ 3Dq -> 0x01e8 }
            X.B4H r0 = (X.B4H) r0     // Catch:{ 3Dq -> 0x01e8 }
            X.B4I r0 = X.B4H.A01(r0, r7)     // Catch:{ 3Dq -> 0x01e8 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A00     // Catch:{ 3Dq -> 0x01e8 }
            boolean r0 = r6.equals(r0)     // Catch:{ 3Dq -> 0x01e8 }
            r15 = 1
            if (r0 != 0) goto L_0x01e9
        L_0x01e8:
            r15 = 0
        L_0x01e9:
            r5 = 25
            int r1 = X.AnonymousClass1Y3.AOt     // Catch:{ all -> 0x0346 }
            X.0UN r0 = r8.A0M     // Catch:{ all -> 0x0346 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0346 }
            X.3Rw r5 = (X.C68213Rw) r5     // Catch:{ all -> 0x0346 }
            com.facebook.messaging.model.threadkey.ThreadKey r14 = r8.A0Y     // Catch:{ all -> 0x0346 }
            int r13 = r8.hashCode()     // Catch:{ all -> 0x0346 }
            com.google.gson.JsonObject r7 = new com.google.gson.JsonObject     // Catch:{ all -> 0x0346 }
            r7.<init>()     // Catch:{ all -> 0x0346 }
            java.lang.String r1 = "event"
            java.lang.String r0 = "threadViewMessagesFragment.setThread"
            r7.addProperty(r1, r0)     // Catch:{ all -> 0x0346 }
            java.lang.String r0 = "reason"
            r7.addProperty(r0, r4)     // Catch:{ all -> 0x0346 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x0346 }
            java.lang.String r0 = "id"
            r7.addProperty(r0, r1)     // Catch:{ all -> 0x0346 }
            r13 = 0
            if (r14 == 0) goto L_0x0219
            goto L_0x021b
        L_0x0219:
            r1 = r2
            goto L_0x021f
        L_0x021b:
            java.lang.String r1 = r14.toString()     // Catch:{ all -> 0x0346 }
        L_0x021f:
            java.lang.String r0 = "old_thread_key"
            r7.addProperty(r0, r1)     // Catch:{ all -> 0x0346 }
            if (r6 == 0) goto L_0x022a
            java.lang.String r13 = r6.toString()     // Catch:{ all -> 0x0346 }
        L_0x022a:
            java.lang.String r0 = "thread_key"
            r7.addProperty(r0, r13)     // Catch:{ all -> 0x0346 }
            X.06B r0 = r5.A02     // Catch:{ all -> 0x0346 }
            long r0 = r0.now()     // Catch:{ all -> 0x0346 }
            java.lang.Long r1 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0346 }
            java.lang.String r0 = "timestampMS"
            r7.addProperty(r0, r1)     // Catch:{ all -> 0x0346 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x0346 }
            X.C68213Rw.A02(r5, r0)     // Catch:{ all -> 0x0346 }
            boolean r7 = r8.A1X     // Catch:{ all -> 0x0346 }
            if (r7 == 0) goto L_0x024c
            r8.A2X()     // Catch:{ all -> 0x0346 }
        L_0x024c:
            r5 = 0
            if (r15 != 0) goto L_0x0250
            r5 = 1
        L_0x0250:
            X.0qv r1 = X.C13220qv.A0H     // Catch:{ all -> 0x0346 }
            r0 = 0
            if (r10 == r1) goto L_0x0256
            r0 = 1
        L_0x0256:
            r8.A2f(r5, r0, r11)     // Catch:{ all -> 0x0346 }
            r8.A0Y = r6     // Catch:{ all -> 0x0346 }
            if (r7 == 0) goto L_0x0260
            r8.A2Y()     // Catch:{ all -> 0x0346 }
        L_0x0260:
            X.2xe r0 = r8.A1s     // Catch:{ all -> 0x0346 }
            r0.Brl(r6)     // Catch:{ all -> 0x0346 }
            r8.A1O = r11     // Catch:{ all -> 0x0346 }
            r8.A1P = r11     // Catch:{ all -> 0x0346 }
            if (r9 == 0) goto L_0x026e
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0f(r8, r9, r12)     // Catch:{ all -> 0x0346 }
        L_0x026e:
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0k(r8, r4)     // Catch:{ all -> 0x0346 }
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0Y(r8)     // Catch:{ all -> 0x0346 }
            X.3Rp r0 = r8.A0o     // Catch:{ all -> 0x0346 }
            boolean r5 = r0.A03     // Catch:{ all -> 0x0346 }
            X.3JO r1 = r8.A12     // Catch:{ all -> 0x0346 }
            X.3L0 r0 = X.AnonymousClass3L0.A04     // Catch:{ all -> 0x0346 }
            r1.A0H(r0, r5)     // Catch:{ all -> 0x0346 }
            if (r5 == 0) goto L_0x0290
            X.3JO r1 = r8.A12     // Catch:{ all -> 0x0346 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0Y     // Catch:{ all -> 0x0346 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x0346 }
            X.AnonymousClass3JO.A09(r1, r0)     // Catch:{ all -> 0x0346 }
            X.3JO r0 = r8.A12     // Catch:{ all -> 0x0346 }
            r0.A0D()     // Catch:{ all -> 0x0346 }
        L_0x0290:
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0W(r8)     // Catch:{ all -> 0x0346 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ all -> 0x0346 }
            java.lang.String r0 = X.C06680bu.A0x     // Catch:{ all -> 0x0346 }
            r1.<init>(r0)     // Catch:{ all -> 0x0346 }
            java.lang.String r0 = "thread_key"
            r1.putExtra(r0, r6)     // Catch:{ all -> 0x0346 }
            X.0Ut r0 = r8.A0E     // Catch:{ all -> 0x0346 }
            r0.C4x(r1)     // Catch:{ all -> 0x0346 }
            X.3MF r7 = r8.A15     // Catch:{ all -> 0x0346 }
            com.facebook.messaging.model.threadkey.ThreadKey r6 = r8.A0Y     // Catch:{ all -> 0x0346 }
            r5 = 109(0x6d, float:1.53E-43)
            int r1 = X.AnonymousClass1Y3.AxE     // Catch:{ all -> 0x0346 }
            X.0UN r0 = r8.A0M     // Catch:{ all -> 0x0346 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0346 }
            X.0t0 r1 = (X.C14300t0) r1     // Catch:{ all -> 0x0346 }
            com.facebook.user.model.UserKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r6)     // Catch:{ all -> 0x0346 }
            com.facebook.user.model.User r1 = r1.A03(r0)     // Catch:{ all -> 0x0346 }
            com.facebook.messaging.send.trigger.NavigationTrigger r0 = r8.A0c     // Catch:{ all -> 0x0346 }
            r7.A03 = r6     // Catch:{ all -> 0x0346 }
            r7.A06 = r1     // Catch:{ all -> 0x0346 }
            r7.A04 = r0     // Catch:{ all -> 0x0346 }
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0N(r8)     // Catch:{ all -> 0x0346 }
            X.3SM r1 = r8.A0v     // Catch:{ all -> 0x0346 }
            if (r1 == 0) goto L_0x02cf
            com.facebook.messaging.model.threads.ThreadSummary r0 = r8.A0Z     // Catch:{ all -> 0x0346 }
            r1.A02 = r0     // Catch:{ all -> 0x0346 }
        L_0x02cf:
            r0 = 1827014343(0x6ce606c7, float:2.2246795E27)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x034e }
        L_0x02d5:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r1 = r3.A0c     // Catch:{ all -> 0x034e }
            com.facebook.messaging.send.trigger.NavigationTrigger r0 = r3.A0L     // Catch:{ all -> 0x034e }
            r1.A0c = r0     // Catch:{ all -> 0x034e }
            r1.A2V()     // Catch:{ all -> 0x034e }
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r3.A0c     // Catch:{ all -> 0x034e }
            r0.A2W()     // Catch:{ all -> 0x034e }
            com.facebook.messaging.threadview.params.ThreadViewMessagesInitParams r0 = r3.A0S     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x02ec
            r3.A0G(r0)     // Catch:{ all -> 0x034e }
            r3.A0S = r2     // Catch:{ all -> 0x034e }
        L_0x02ec:
            X.2oz r0 = r3.A0j     // Catch:{ all -> 0x034e }
            r0.A02()     // Catch:{ all -> 0x034e }
            A0L(r3)     // Catch:{ all -> 0x034e }
            X.3SQ r0 = r3.A0Z     // Catch:{ all -> 0x034e }
            if (r0 != 0) goto L_0x02ff
            X.3SQ r0 = new X.3SQ     // Catch:{ all -> 0x034e }
            r0.<init>(r3)     // Catch:{ all -> 0x034e }
            r3.A0Z = r0     // Catch:{ all -> 0x034e }
        L_0x02ff:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r2 = r3.A0c     // Catch:{ all -> 0x034e }
            X.3SQ r0 = r3.A0Z     // Catch:{ all -> 0x034e }
            r2.A0z = r0     // Catch:{ all -> 0x034e }
            X.3SR r0 = r3.A0a     // Catch:{ all -> 0x034e }
            if (r0 != 0) goto L_0x0310
            X.3SR r0 = new X.3SR     // Catch:{ all -> 0x034e }
            r0.<init>(r3)     // Catch:{ all -> 0x034e }
            r3.A0a = r0     // Catch:{ all -> 0x034e }
        L_0x0310:
            X.3SR r0 = r3.A0a     // Catch:{ all -> 0x034e }
            r2.A10 = r0     // Catch:{ all -> 0x034e }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0H     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x033f
            A0c(r3, r12, r12, r4)     // Catch:{ all -> 0x034e }
            java.lang.String r0 = "setupThreadViewFragment_"
            java.lang.String r2 = X.AnonymousClass08S.A0J(r0, r4)     // Catch:{ all -> 0x034e }
            boolean r0 = r3.A0i     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0328
            r3.A0d(r2)     // Catch:{ all -> 0x034e }
        L_0x0328:
            A0M(r3)     // Catch:{ all -> 0x034e }
            r2 = 30
            int r1 = X.AnonymousClass1Y3.BTA     // Catch:{ all -> 0x034e }
            X.0UN r0 = r3.A0D     // Catch:{ all -> 0x034e }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x034e }
            X.3T5 r1 = (X.AnonymousClass3T5) r1     // Catch:{ all -> 0x034e }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0H     // Catch:{ all -> 0x034e }
            r1.A06(r0)     // Catch:{ all -> 0x034e }
            r3.A21()     // Catch:{ all -> 0x034e }
        L_0x033f:
            r0 = -1529362133(0xffffffffa4d7c92b, float:-9.358218E-17)
            X.C005505z.A00(r0)
            return
        L_0x0346:
            r1 = move-exception
            r0 = 1883324808(0x70414188, float:2.3923932E29)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x034e }
            throw r1     // Catch:{ all -> 0x034e }
        L_0x034e:
            r1 = move-exception
            r0 = 2122654496(0x7e852320, float:8.848485E37)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A0f(java.lang.String):void");
    }

    private void A0g(String str, ThreadKey threadKey, ThreadKey threadKey2) {
        ((AnonymousClass09P) AnonymousClass1XX.A02(14, AnonymousClass1Y3.Amr, this.A0D)).CGS(str, AnonymousClass08S.A0S("Fragment key: ", threadKey.toString(), " Result key: ", threadKey2.toString()));
    }

    private void A0h(boolean z, boolean z2) {
        ThreadSummary threadSummary;
        String str;
        ThreadKey threadKey = this.A0H;
        if (threadKey != null && threadKey.A05 == C28711fF.GROUP) {
            C61152yQ r0 = this.A0Q.A01;
            if (r0 == null || (threadSummary = r0.A02) == null || threadSummary.A0m == null) {
                C010708t.A0I("ThreadViewFragment", "Incomplete thread summary information, unable to start or join multiway call");
                return;
            }
            boolean z3 = z;
            String A002 = C126065wF.A00(C42782Bv.A03(threadSummary), z, z2);
            AnonymousClass33E r1 = this.A0d;
            NavigationTrigger navigationTrigger = this.A0M;
            if (navigationTrigger != null) {
                str = navigationTrigger.toString();
            } else {
                str = null;
            }
            r1.A03(threadKey, threadSummary, z3, A002, str);
            ((C88844Mn) AnonymousClass1XX.A02(36, AnonymousClass1Y3.AH5, this.A0D)).A01(this.A0H.A01, true, z, this.A0M);
        }
    }

    public static boolean A0j(ThreadViewFragment threadViewFragment) {
        ThreadKey threadKey = threadViewFragment.A0H;
        if (threadKey == null || threadKey.A05 != C28711fF.GROUP) {
            return false;
        }
        return true;
    }

    public static boolean A0k(ThreadViewFragment threadViewFragment) {
        if (threadViewFragment.A15) {
            Context A1j = threadViewFragment.A1j();
            if (A1j instanceof Activity) {
                ((Activity) A1j).finish();
                return true;
            } else if (C22541Lt.A00(A1j)) {
                threadViewFragment.A0b.BC4(false);
                return true;
            }
        }
        return false;
    }

    public Animation A1i(int i, boolean z, int i2) {
        if (i2 != 0) {
            return AnimationUtils.loadAnimation(A1j(), i2);
        }
        return super.A1i(i, z, i2);
    }

    public void A1w(Fragment fragment) {
        if (fragment instanceof ThreadViewMessagesFragment) {
            ThreadViewMessagesFragment threadViewMessagesFragment = (ThreadViewMessagesFragment) fragment;
            this.A0c = threadViewMessagesFragment;
            threadViewMessagesFragment.A2d(this.A0T);
            ThreadViewMessagesFragment threadViewMessagesFragment2 = this.A0c;
            threadViewMessagesFragment2.A0c = this.A0L;
            threadViewMessagesFragment2.A11 = new C66763Ln(this);
        }
        if (fragment instanceof C66773Lo) {
            C66773Lo r3 = (C66773Lo) fragment;
            this.A1H = r3;
            r3.A0C = new C137076ak(this);
            C14840uE r1 = this.A0b;
            if (r1 != null) {
                r1.BnQ(true);
            }
        }
    }

    public void A2F(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (C64833Du.A00 == null) {
            synchronized (C64833Du.class) {
                if (C64833Du.A00 == null) {
                    C64833Du.A00 = new C64833Du();
                }
            }
        }
        Context CNV = C64833Du.A00.CNV(A1j());
        this.A03 = CNV;
        LayoutInflater cloneInContext = layoutInflater.cloneInContext(CNV);
        this.A1A = cloneInContext;
        super.A2F(cloneInContext, viewGroup, bundle);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0097 A[Catch:{ all -> 0x0125 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2M(android.os.Bundle r7) {
        /*
            r6 = this;
            java.lang.String r5 = "args_thread_params"
            java.lang.String r2 = "saved_thread_params"
            super.A2M(r7)
            if (r7 == 0) goto L_0x0012
            r1 = 0
            java.lang.String r0 = "is_opened"
            boolean r0 = r7.getBoolean(r0, r1)
            r6.A0y = r0
        L_0x0012:
            r1 = -280930805(0xffffffffef41560b, float:-5.9834626E28)
            java.lang.String r0 = "ThreadViewFragment.onFragmentCreate"
            X.C005505z.A03(r0, r1)
            r4 = 44
            r3 = 5505047(0x540017, float:7.714214E-39)
            android.os.Bundle r1 = r6.A0G     // Catch:{ all -> 0x0125 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r6.A0H     // Catch:{ all -> 0x0125 }
            if (r0 != 0) goto L_0x0055
            if (r7 == 0) goto L_0x0040
            boolean r0 = r7.containsKey(r2)     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x0055
            android.os.Parcelable r1 = r7.getParcelable(r2)     // Catch:{ all -> 0x0125 }
            com.facebook.messaging.threadview.params.ThreadViewParams r1 = (com.facebook.messaging.threadview.params.ThreadViewParams) r1     // Catch:{ all -> 0x0125 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A02     // Catch:{ all -> 0x0125 }
            r6.A0H = r0     // Catch:{ all -> 0x0125 }
            X.0qv r0 = r1.A06     // Catch:{ all -> 0x0125 }
            r6.A0U = r0     // Catch:{ all -> 0x0125 }
            com.facebook.messaging.threadview.params.ThreadViewMessagesInitParams r0 = r1.A05     // Catch:{ all -> 0x0125 }
            r6.A0S = r0     // Catch:{ all -> 0x0125 }
            goto L_0x0055
        L_0x0040:
            if (r1 == 0) goto L_0x0055
            boolean r0 = r1.containsKey(r5)     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x0055
            android.os.Parcelable r1 = r1.getParcelable(r5)     // Catch:{ all -> 0x0125 }
            com.facebook.messaging.threadview.params.ThreadViewParams r1 = (com.facebook.messaging.threadview.params.ThreadViewParams) r1     // Catch:{ all -> 0x0125 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A02     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x0055
            r6.A2V(r1)     // Catch:{ all -> 0x0125 }
        L_0x0055:
            X.0Ut r0 = r6.A0B     // Catch:{ all -> 0x0125 }
            X.0bl r2 = r0.BMm()     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = X.C06680bu.A0k     // Catch:{ all -> 0x0125 }
            X.2xJ r0 = new X.2xJ     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r2.A02(r1, r0)     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = X.C06680bu.A09     // Catch:{ all -> 0x0125 }
            X.32u r0 = new X.32u     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r2.A02(r1, r0)     // Catch:{ all -> 0x0125 }
            r5 = 31
            int r1 = X.AnonymousClass1Y3.BAo     // Catch:{ all -> 0x0125 }
            X.0UN r0 = r6.A0D     // Catch:{ all -> 0x0125 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0125 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x0125 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x00ab
            boolean r0 = r6.A10     // Catch:{ all -> 0x0125 }
            r1 = 1
            if (r0 == 0) goto L_0x008b
            com.facebook.messaging.model.threads.GroupThreadAssociatedFbGroup r0 = r6.A0I     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x0094
            goto L_0x0095
        L_0x008b:
            com.facebook.messaging.threadview.params.ThreadViewMessagesInitParams r0 = r6.A0S     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x0094
            com.facebook.messaging.model.threads.GroupThreadAssociatedFbGroup r0 = r0.A08     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x0094
            goto L_0x0095
        L_0x0094:
            r1 = 0
        L_0x0095:
            if (r1 == 0) goto L_0x00ab
            java.lang.String r1 = X.C06680bu.A0p     // Catch:{ all -> 0x0125 }
            X.6y7 r0 = new X.6y7     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r2.A02(r1, r0)     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = X.C06680bu.A0q     // Catch:{ all -> 0x0125 }
            X.6xt r0 = new X.6xt     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r2.A02(r1, r0)     // Catch:{ all -> 0x0125 }
        L_0x00ab:
            X.0c5 r0 = r2.A00()     // Catch:{ all -> 0x0125 }
            r6.A1E = r0     // Catch:{ all -> 0x0125 }
            r0.A00()     // Catch:{ all -> 0x0125 }
            X.0Ut r0 = r6.A0B     // Catch:{ all -> 0x0125 }
            X.0bl r2 = r0.BMm()     // Catch:{ all -> 0x0125 }
            r0 = 433(0x1b1, float:6.07E-43)
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ all -> 0x0125 }
            X.33P r0 = new X.33P     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r2.A02(r1, r0)     // Catch:{ all -> 0x0125 }
            X.0c5 r0 = r2.A00()     // Catch:{ all -> 0x0125 }
            r6.A1C = r0     // Catch:{ all -> 0x0125 }
            r0.A00()     // Catch:{ all -> 0x0125 }
            X.0Ut r0 = r6.A0B     // Catch:{ all -> 0x0125 }
            X.0bl r2 = r0.BMm()     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = X.C06680bu.A0a     // Catch:{ all -> 0x0125 }
            X.33Q r0 = new X.33Q     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r2.A02(r1, r0)     // Catch:{ all -> 0x0125 }
            X.0c5 r0 = r2.A00()     // Catch:{ all -> 0x0125 }
            r6.A1B = r0     // Catch:{ all -> 0x0125 }
            r0.A00()     // Catch:{ all -> 0x0125 }
            X.0Ut r0 = r6.A0B     // Catch:{ all -> 0x0125 }
            X.0bl r2 = r0.BMm()     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = "open_page_frx_fragment"
            X.2EE r0 = new X.2EE     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r2.A02(r1, r0)     // Catch:{ all -> 0x0125 }
            X.0c5 r0 = r2.A00()     // Catch:{ all -> 0x0125 }
            r6.A1D = r0     // Catch:{ all -> 0x0125 }
            r0.A00()     // Catch:{ all -> 0x0125 }
            X.0UN r1 = r6.A0D     // Catch:{ all -> 0x0125 }
            int r0 = X.AnonymousClass1Y3.AqE     // Catch:{ all -> 0x0125 }
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r0, r1)     // Catch:{ all -> 0x0125 }
            X.0yH r1 = (X.C17070yH) r1     // Catch:{ all -> 0x0125 }
            X.33R r0 = new X.33R     // Catch:{ all -> 0x0125 }
            r0.<init>(r6)     // Catch:{ all -> 0x0125 }
            r1.A01(r6, r0)     // Catch:{ all -> 0x0125 }
            X.0fv r1 = r6.A0F
            int r0 = r6.A00()
            r1.A0D(r3, r0, r4)
            r0 = 1490918028(0x58dd9a8c, float:1.94924675E15)
            X.C005505z.A00(r0)
            return
        L_0x0125:
            r2 = move-exception
            X.0fv r1 = r6.A0F
            int r0 = r6.A00()
            r1.A0D(r3, r0, r4)
            r0 = -425346403(0xffffffffe6a5ba9d, float:-3.9131644E23)
            X.C005505z.A00(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A2M(android.os.Bundle):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void A2S() {
        if (this.A0y) {
            this.A0y = false;
            C60432xB r0 = this.A0W;
            if (r0 != null) {
                r0.A08(false);
            }
            A0b(this, false);
            this.A0V.Brh();
            ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
            if (threadViewMessagesFragment != null) {
                threadViewMessagesFragment.A2X();
            }
            C55932oz r02 = this.A0j;
            if (r02 != null) {
                r02.A03();
            }
            A0H(this);
            A0A();
            ThreadKey threadKey = this.A0H;
            if (threadKey != null) {
                this.A0F.A0I(threadKey.A0J());
                this.A0f.A02.markerEnd(9961513, this.A0H.A0J().hashCode(), (short) 4);
            }
            A09();
            this.A0H = null;
            this.A1J = null;
            this.A0k = null;
            this.A0Q = C60402x8.A02;
            ((C78763pU) AnonymousClass1XX.A03(AnonymousClass1Y3.B5r, this.A0D)).A00.AYS(C78763pU.A01);
            C68333Sk r1 = (C68333Sk) AnonymousClass1XX.A02(39, AnonymousClass1Y3.Ag9, this.A0D);
            r1.A01 = null;
            r1.A02 = null;
        }
    }

    public void A2T() {
        ThreadViewMessagesFragment threadViewMessagesFragment;
        if (!this.A0y) {
            this.A0y = true;
            AnonymousClass2x4 r1 = this.A0V;
            if (r1.BFQ()) {
                r1.Bro();
            }
            A0b(this, true);
            if (!(!this.A10 || (threadViewMessagesFragment = this.A0c) == null || this.A0H == null)) {
                threadViewMessagesFragment.A2Y();
            }
            A0d("onThreadOpened");
        }
    }

    public void A2U(AnonymousClass70M r3) {
        this.A1G = r3;
        ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
        if (threadViewMessagesFragment != null) {
            ComposeFragment composeFragment = threadViewMessagesFragment.A0P;
            if (composeFragment.A0J == null) {
                composeFragment.A0J = new AnonymousClass3R9();
            }
            composeFragment.A0J.A00 = r3;
        }
    }

    public void A2V(ThreadViewParams threadViewParams) {
        FbSharedPreferences fbSharedPreferences;
        C56882r2 r5;
        ThreadKey threadKey;
        C56882r2 r1;
        Preconditions.checkNotNull(threadViewParams.A02);
        this.A0V.ALG();
        try {
            ThreadKey threadKey2 = this.A0H;
            if (!(threadKey2 == null || (r1 = this.A0i) == null)) {
                r1.A06(ThreadKey.A07(threadKey2));
                if (this.A0H.A0G() != threadViewParams.A02.A0G()) {
                    A0b(this, false);
                }
            }
            C13220qv r12 = threadViewParams.A06;
            if (this.A0U != r12) {
                this.A0U = r12;
                C14840uE r0 = this.A0b;
                if (r0 != null) {
                    r0.Bp5(r12);
                }
            }
            this.A0H = threadViewParams.A02;
            this.A1J = threadViewParams.A06.name();
            this.A0k = null;
            this.A0R = threadViewParams.A04;
            A0S(this);
            ProfilePopoverFragment profilePopoverFragment = (ProfilePopoverFragment) B4m().A0Q("com.facebook.messaging.profile.ProfileFragmentLauncher");
            if (profilePopoverFragment != null) {
                ThreadKey threadKey3 = threadViewParams.A02;
                ContextualProfileLoggingData contextualProfileLoggingData = profilePopoverFragment.A01;
                if (contextualProfileLoggingData != null) {
                    threadKey = contextualProfileLoggingData.A00;
                } else {
                    threadKey = null;
                }
                if (!threadKey3.equals(threadKey)) {
                    C16290wo A0T2 = B4m().A0T();
                    A0T2.A0I(profilePopoverFragment);
                    A0T2.A02();
                }
            }
            if (C22541Lt.A00(A1j()) && this.A1H != null) {
                ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
                if (threadViewMessagesFragment != null) {
                    threadViewMessagesFragment.A2T();
                }
                C14840uE r02 = this.A0b;
                if (r02 != null) {
                    r02.BnQ(false);
                }
                this.A1H = null;
            }
            ThreadViewMessagesInitParams threadViewMessagesInitParams = threadViewParams.A05;
            if (threadViewMessagesInitParams != null) {
                this.A15 = threadViewMessagesInitParams.A0H;
                int i = AnonymousClass1Y3.BGf;
                AnonymousClass3B5 r2 = (AnonymousClass3B5) AnonymousClass1XX.A02(35, i, this.A0D);
                if (r2 != null) {
                    r2.A03(threadViewMessagesInitParams.A0G, hashCode());
                    ((AnonymousClass3B5) AnonymousClass1XX.A02(35, i, this.A0D)).A02(this.A0H);
                }
            } else {
                this.A15 = false;
            }
            NavigationTrigger navigationTrigger = threadViewParams.A03;
            this.A0L = navigationTrigger;
            this.A0M = navigationTrigger;
            ThreadViewMessagesFragment threadViewMessagesFragment2 = this.A0c;
            if (threadViewMessagesFragment2 != null) {
                threadViewMessagesFragment2.A0c = navigationTrigger;
            }
            A0D();
            AnonymousClass2x4 r13 = this.A0V;
            if (r13.BFQ()) {
                r13.CCO(null, null, null);
                this.A0V.C9a(null);
            }
            C57952su r14 = this.A0Y;
            if (r14 != null) {
                r14.A02(this.A0H);
            }
            A08();
            if (this.A10) {
                A0f(this.A0U.toString());
            }
            ThreadViewMessagesInitParams threadViewMessagesInitParams2 = threadViewParams.A05;
            if (this.A10) {
                A0G(threadViewMessagesInitParams2);
            } else {
                this.A0S = threadViewMessagesInitParams2;
            }
            if (!A0j(this)) {
                ThreadKey threadKey4 = this.A0H;
                if (!ThreadKey.A0E(threadKey4) && !ThreadKey.A0D(threadKey4)) {
                    ThreadKey threadKey5 = threadKey4;
                    if (!(threadKey4 == null || ((AnonymousClass0r6) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Auh, this.A0D)) == null || (r5 = this.A0i) == null)) {
                        UserKey A072 = ThreadKey.A07(threadKey5);
                        C60422xA r3 = this.A1N;
                        if (C56882r2.A04(r5)) {
                            ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, r5.A01)).AOz();
                            if (A072 != null) {
                                r5.A08.C1N(A072);
                                r5.A08.Byx(A072, r3);
                            }
                        }
                    }
                }
            }
            if (ThreadKey.A0F(this.A0H) && (fbSharedPreferences = this.A0g) != null) {
                AnonymousClass1Y7 r22 = C14810uA.A02;
                if (!fbSharedPreferences.Aep(r22, false)) {
                    C30281hn edit = this.A0g.edit();
                    edit.putBoolean(r22, true);
                    edit.commit();
                    TincanNuxFragment tincanNuxFragment = new TincanNuxFragment();
                    tincanNuxFragment.A1P(new Bundle());
                    C16290wo A0T3 = B4m().A0T();
                    A0T3.A0C(tincanNuxFragment, "TincanNuxFragment");
                    A0T3.A03();
                }
            }
            if (ThreadKey.A0E(this.A0H) && A1j() != null && !((C52432j5) AnonymousClass1XX.A02(24, AnonymousClass1Y3.AsF, this.A0D)).A01.Aep(C10990lD.A0L, false)) {
                int i2 = AnonymousClass1Y3.AsF;
                if (((C52432j5) AnonymousClass1XX.A02(24, i2, this.A0D)).A02() && !((C52432j5) AnonymousClass1XX.A02(24, i2, this.A0D)).A03() && !C22541Lt.A00(A1j())) {
                    Intent intent = new Intent(A1j(), IdentityMatchingInterstitialActivity.class);
                    intent.putExtra("source", "ThreadViewFragment");
                    ((SecureContextHelper) AnonymousClass1XX.A02(9, AnonymousClass1Y3.A7S, this.A0D)).startFacebookActivity(intent, A1j());
                }
            }
            if (ThreadKey.A09(this.A0H)) {
                int i3 = AnonymousClass1Y3.BJs;
                AnonymousClass3IL r15 = (AnonymousClass3IL) AnonymousClass1XX.A02(25, i3, this.A0D);
                if (!(r15 == null || !r15.A03(C80093rq.ORION_GROUP_REQUEST) || this.A0Q.A01.A02 == null)) {
                    C80093rq r8 = C80093rq.ORION_GROUP_REQUEST;
                    Context A1j = A1j();
                    ThreadSummary threadSummary = this.A0Q.A01.A02;
                    Intent intent2 = new Intent(A1j, PaymentAwarenessActivity.class);
                    intent2.putExtra(C22298Ase.$const$string(302), r8);
                    intent2.putExtra("thread_summary", threadSummary);
                    intent2.putExtra(C22298Ase.$const$string(AnonymousClass1Y3.A2O), (Parcelable) null);
                    ((SecureContextHelper) AnonymousClass1XX.A02(9, AnonymousClass1Y3.A7S, this.A0D)).CGt(intent2, AnonymousClass1Y3.A0j, this);
                    ((AnonymousClass3IL) AnonymousClass1XX.A02(25, i3, this.A0D)).A02(r8);
                }
            }
            int i4 = threadViewParams.A00;
            if (i4 != 0) {
                A0U(this, i4);
            }
            if (!(!threadViewParams.A08 || this.A0H == null || A1j() == null)) {
                ((C66003Ik) AnonymousClass1XX.A02(45, AnonymousClass1Y3.Alp, this.A0D)).A03(this.A0H, A1j());
            }
            A0b(this, true);
        } finally {
            this.A0V.ALH();
        }
    }

    public void A2W(String str) {
        ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
        CurrencyAmount currencyAmount = new CurrencyAmount(str, 0);
        C79913rT r2 = C79913rT.A0O;
        ComposeFragment composeFragment = threadViewMessagesFragment.A0P;
        if (composeFragment != null) {
            composeFragment.A2l(currencyAmount, r2, AnonymousClass07B.A00);
        }
    }

    public void A2X(List list) {
        C134626Rk r1 = new C134626Rk();
        r1.A00(A1j());
        Preconditions.checkNotNull(list);
        r1.A0B = list;
        r1.A01(C44342Hv.A09);
        ((AnonymousClass0Tc) this.A0q.get()).A08(new AnonymousClass6Rm(r1));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        if (r1 == false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A2Z() {
        /*
            r3 = this;
            com.facebook.user.model.User r2 = r3.A0k
            X.2x8 r0 = r3.A0Q
            X.2yQ r1 = r0.A01
            if (r1 == 0) goto L_0x002d
            boolean r0 = A0j(r3)
            if (r0 == 0) goto L_0x0012
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A02
            if (r0 != 0) goto L_0x0014
        L_0x0012:
            if (r2 == 0) goto L_0x002d
        L_0x0014:
            r0 = 1
        L_0x0015:
            if (r0 == 0) goto L_0x002f
            if (r1 == 0) goto L_0x0024
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A02
            if (r0 == 0) goto L_0x0024
            boolean r1 = r0.A0A()
            r0 = 1
            if (r1 != 0) goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            if (r0 != 0) goto L_0x002f
            r0 = 0
            A0U(r3, r0)
            r0 = 1
            return r0
        L_0x002d:
            r0 = 0
            goto L_0x0015
        L_0x002f:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A2Z():boolean");
    }

    public boolean ARa(MotionEvent motionEvent) {
        ThreadViewMessagesFragment threadViewMessagesFragment;
        ThreadViewMessagesFragment threadViewMessagesFragment2 = this.A0c;
        if (threadViewMessagesFragment2 == null || !threadViewMessagesFragment2.A1a()) {
            threadViewMessagesFragment = null;
        } else {
            threadViewMessagesFragment = this.A0c;
        }
        if (!(threadViewMessagesFragment instanceof C15760vw) || !threadViewMessagesFragment.ARa(motionEvent)) {
            return false;
        }
        return true;
    }

    public Map Ajk() {
        C136696a5 r6 = (C136696a5) AnonymousClass1XX.A02(11, AnonymousClass1Y3.Ahh, this.A0D);
        ThreadKey threadKey = this.A0H;
        C60402x8 r2 = this.A0Q;
        ImmutableMap.Builder builder = new ImmutableMap.Builder();
        C61152yQ r5 = r2.A01;
        if (threadKey != null) {
            builder.put("thread.threadKey", threadKey.toString());
        }
        if (r5 != null) {
            boolean z = false;
            if (r5.A04 != null) {
                z = true;
            }
            builder.put("thread.isForUser", Boolean.toString(z));
            ThreadSummary threadSummary = r5.A02;
            boolean z2 = false;
            if (threadSummary != null) {
                z2 = true;
            }
            if (z2) {
                builder.put("thread.canReplyTo", Boolean.toString(threadSummary.A0y));
                builder.put("thread.cannotReplyReason", r5.A02.A0H.toString());
                builder.put("thread.isSubscribed", Boolean.toString(r5.A02.A16));
                builder.put("thread.hasFailedMessageSend", Boolean.toString(r5.A02.A10));
                Integer A022 = r6.A00.A04(r5.A02.A0S).A02();
                boolean z3 = true;
                boolean z4 = false;
                if (A022 == AnonymousClass07B.A0C) {
                    z4 = true;
                }
                builder.put("thread.notif_muted", Boolean.toString(z4));
                if (A022 != AnonymousClass07B.A01) {
                    z3 = false;
                }
                builder.put("thread.notif_disabled", Boolean.toString(z3));
                ImmutableList immutableList = r5.A02.A0m;
                if (immutableList != null) {
                    builder.put("thread.numParticipants", Long.toString((long) immutableList.size()));
                }
                ImmutableList immutableList2 = r5.A02.A0k;
                if (immutableList2 != null) {
                    builder.put("thread.numBotParticipants", Long.toString((long) immutableList2.size()));
                }
            }
            MessagesCollection messagesCollection = r5.A01;
            if (messagesCollection != null && !messagesCollection.A08()) {
                builder.put("thread.latest_message_id", r5.A01.A07(0).A0q);
            }
        }
        C55482o9 r0 = r2.A00;
        if (r0 != null) {
            ServiceException serviceException = r0.A00;
            builder.put("threadViewLoader.serviceException.errorCode", serviceException.errorCode.name());
            builder.put("threadViewLoader.serviceException.throwableMessage", serviceException.getMessage());
            builder.put("threadViewLoader.serviceException.stackTrace", AnonymousClass0D4.A01(serviceException));
        }
        return builder.build();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        if (r6 != 102) goto L_0x0008;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BNH(int r6, int r7, android.content.Intent r8) {
        /*
            r5 = this;
            r0 = 101(0x65, float:1.42E-43)
            if (r6 == r0) goto L_0x000c
            r0 = 102(0x66, float:1.43E-43)
            if (r6 == r0) goto L_0x001e
        L_0x0008:
            super.BNH(r6, r7, r8)
            return
        L_0x000c:
            r0 = 109(0x6d, float:1.53E-43)
            if (r7 == r0) goto L_0x00a9
            r0 = 111(0x6f, float:1.56E-43)
            if (r7 == r0) goto L_0x009f
            switch(r7) {
                case 101: goto L_0x0044;
                case 102: goto L_0x004e;
                case 103: goto L_0x0058;
                case 104: goto L_0x0091;
                case 105: goto L_0x0095;
                case 106: goto L_0x00a9;
                default: goto L_0x0017;
            }
        L_0x0017:
            java.lang.String r2 = "thread_settings_finished"
            r1 = 1
            r0 = 0
            A0c(r5, r0, r1, r2)
        L_0x001e:
            r0 = -1
            if (r7 != r0) goto L_0x0008
            java.lang.String r0 = "nux_action"
            java.io.Serializable r1 = r8.getSerializableExtra(r0)
            X.6p5 r1 = (X.C145086p5) r1
            X.6p5 r0 = X.C145086p5.MAIN
            if (r1 != r0) goto L_0x0008
            com.facebook.orca.threadview.ThreadViewMessagesFragment r4 = r5.A0c
            com.facebook.payments.currency.CurrencyAmount r3 = new com.facebook.payments.currency.CurrencyAmount
            r0 = 0
            java.lang.String r2 = "USD"
            r3.<init>(r2, r0)
            X.3rT r2 = X.C79913rT.A0L
            com.facebook.messaging.composer.ComposeFragment r1 = r4.A0P
            if (r1 == 0) goto L_0x0008
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.A2l(r3, r2, r0)
            goto L_0x0008
        L_0x0044:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r5.A0c
            java.lang.String r1 = "thread_settings"
            X.3Ez r0 = r0.A0e
            r0.Bw8(r1)
            goto L_0x001e
        L_0x004e:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r5.A0c
            java.lang.String r1 = "thread_settings"
            X.3Ez r0 = r0.A0e
            r0.Bvk(r1)
            goto L_0x001e
        L_0x0058:
            java.lang.String r0 = "bug_screenshot_extra"
            java.lang.String r2 = r8.getStringExtra(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 != 0) goto L_0x0082
            X.0Tq r0 = r5.A0q
            r0.get()
            android.content.Context r0 = r5.A1j()
            r1 = 0
            java.io.FileInputStream r0 = r0.openFileInput(r2)     // Catch:{ Exception -> 0x0083 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0083 }
            r0.close()     // Catch:{ Exception -> 0x0083 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0083 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0083 }
            r0.delete()     // Catch:{ Exception -> 0x0083 }
            goto L_0x0083
        L_0x0082:
            r1 = 0
        L_0x0083:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            if (r1 == 0) goto L_0x008d
            r0.add(r1)
        L_0x008d:
            r5.A2X(r0)
            goto L_0x001e
        L_0x0091:
            A0P(r5)
            goto L_0x001e
        L_0x0095:
            java.lang.String r0 = "extra_data_payment_currency"
            java.lang.String r0 = r8.getStringExtra(r0)
            r5.A2W(r0)
            goto L_0x001e
        L_0x009f:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r5.A0c
            r0.A2V()
            r0.A2W()
            goto L_0x001e
        L_0x00a9:
            A0K(r5)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.BNH(int, int, android.content.Intent):void");
    }

    public ThreadViewFragment() {
        if (C34731q5.A0F == null) {
            C34731q5.A0F = new C34731q5();
        }
        this.A1L = C34731q5.A0F;
        this.A12 = false;
        this.A1P = new AnonymousClass339(this);
        this.A1O = new C60412x9(this);
        this.A1M = new AnonymousClass33A(this);
        this.A1N = new C60422xA(this);
    }

    private void A09() {
        ThreadKey threadKey;
        C56882r2 r1;
        if (!A0j(this) && (threadKey = this.A0H) != null && ((AnonymousClass0r6) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Auh, this.A0D)) != null && (r1 = this.A0i) != null) {
            r1.A06(ThreadKey.A07(threadKey));
        }
    }

    public static void A0K(ThreadViewFragment threadViewFragment) {
        Context A1j = threadViewFragment.A1j();
        if (A1j instanceof Activity) {
            if (threadViewFragment.A0b == null) {
                ((Activity) A1j).finish();
            } else if (C16000wK.A01(threadViewFragment.A08)) {
                while (threadViewFragment.A08.A0L() > 0) {
                    threadViewFragment.A08.A14();
                }
                threadViewFragment.A21();
                C14840uE r1 = threadViewFragment.A0b;
                if (r1 != null) {
                    r1.BC4(true);
                }
            }
        }
        C68333Sk r12 = (C68333Sk) AnonymousClass1XX.A02(39, AnonymousClass1Y3.Ag9, threadViewFragment.A0D);
        r12.A01 = null;
        r12.A02 = null;
    }

    private boolean A0i() {
        if (C22541Lt.A00(A1j())) {
            return this.A0v;
        }
        Activity A2J = A2J();
        if (Build.VERSION.SDK_INT < 11 || A2J == null || !A2J.isChangingConfigurations() || (A2J.getChangingConfigurations() & 128) == 0) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int A022 = C000700l.A02(1669101759);
        if (this.A1A == null) {
            this.A1A = layoutInflater;
        }
        this.A0F.A0C(5505047, A00(), "onCreateView");
        this.A0F.A0D(5505047, A00(), 45);
        View inflate = this.A1A.inflate(2132412209, viewGroup, false);
        this.A05 = inflate;
        C000700l.A08(-1766537504, A022);
        return inflate;
    }

    public void A1l() {
        ThreadViewMessagesFragment threadViewMessagesFragment;
        int A022 = C000700l.A02(-295468241);
        super.A1l();
        this.A0j.A03();
        this.A1E.A01();
        this.A1C.A01();
        C06790c5 r0 = this.A1B;
        if (r0 != null) {
            r0.A01();
        }
        this.A1D.A01();
        C67023Mq r02 = this.A0X;
        if (r02 != null) {
            r02.A01();
        }
        A0A();
        A09();
        if (!A0i() && (threadViewMessagesFragment = this.A0c) != null) {
            threadViewMessagesFragment.A2a();
        }
        C000700l.A08(1419357035, A022);
    }

    public void A1m() {
        int A022 = C000700l.A02(244928036);
        A0H(this);
        super.A1m();
        AnonymousClass3B5 r1 = (AnonymousClass3B5) AnonymousClass1XX.A02(35, AnonymousClass1Y3.BGf, this.A0D);
        if (r1 != null) {
            r1.A01(hashCode());
        }
        C000700l.A08(-1348216509, A022);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void A1o() {
        int A022 = C000700l.A02(119577108);
        if (this.A12) {
            this.A1L.A01();
        }
        super.A1o();
        this.A1K = false;
        C67023Mq r0 = this.A0X;
        if (r0 != null) {
            r0.A01();
        }
        A0J(this);
        synchronized (this) {
            try {
                this.A0V.onPause();
            } catch (Throwable th) {
                while (true) {
                    C000700l.A08(19876821, A022);
                    throw th;
                }
            }
        }
        AnonymousClass92W r4 = (AnonymousClass92W) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ADi, this.A0D);
        if (r4.A07()) {
            r4.A06 = false;
            AnonymousClass00S.A02((Handler) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amu, r4.A03), r4.A08);
            WeakReference weakReference = r4.A04;
            if (weakReference != null) {
                weakReference.clear();
                r4.A04 = null;
            }
        }
        this.A0g.CJr(C05690aA.A1c, this.A1P);
        ThreadKey threadKey = this.A0H;
        if (threadKey != null) {
            this.A0F.A0I(threadKey.A0J());
            this.A0f.A02.markerEnd(9961513, this.A0H.A0J().hashCode(), (short) 4);
        }
        C06790c5 r02 = this.A09;
        if (r02 != null) {
            r02.A01();
        }
        A0E();
        ((C22431Lh) AnonymousClass1XX.A03(AnonymousClass1Y3.B2N, this.A0D)).A01();
        C000700l.A08(-295448321, A022);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void A1p() {
        ThreadSummary threadSummary;
        int A022 = C000700l.A02(1366326257);
        boolean Aem = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).Aem(286839390870786L);
        if (Aem) {
            int AqL = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).AqL(568314368035082L, -4);
            int AqL2 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).AqL(568314367969545L, Integer.MAX_VALUE);
            C34731q5 r0 = this.A1L;
            r0.A0D = Aem;
            r0.A03 = AqL;
            r0.A04 = AqL2;
        }
        boolean Aem2 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).Aem(286839391001860L);
        if (Aem2) {
            int AqL3 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).AqL(568314368166156L, -4);
            int AqL4 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).AqL(568314368100619L, Integer.MAX_VALUE);
            C34731q5 r02 = this.A1L;
            r02.A0C = Aem2;
            r02.A01 = AqL3;
            r02.A02 = AqL4;
        }
        boolean Aem3 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).Aem(286839391132934L);
        if (Aem3) {
            int AqL5 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).AqL(568314368297230L, 0);
            int AqL6 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).AqL(568314368362767L, 0);
            int AqL7 = ((C25051Yd) AnonymousClass1XX.A02(46, AnonymousClass1Y3.AOJ, this.A0D)).AqL(568314368231693L, Integer.MAX_VALUE);
            C34731q5 r3 = this.A1L;
            C73263fn A002 = C73263fn.A00(AqL5);
            if (A002 != null) {
                r3.A0B = Aem3;
                r3.A05 = new C34761q8(A002, AqL6);
                r3.A00 = AqL7;
            }
        }
        if (Aem || Aem2 || Aem3) {
            this.A12 = true;
            this.A1L.A00();
        }
        super.A1p();
        this.A1K = true;
        this.A0v = false;
        this.A0V.onResume();
        this.A0V.C5h(AnonymousClass3RG.A01(A1j()));
        A0B();
        if (this.A09 == null) {
            C06600bl BMm = this.A0B.BMm();
            BMm.A02(C99084oO.$const$string(1), new AnonymousClass3YL(this));
            this.A09 = BMm.A00();
        }
        this.A09.A00();
        ThreadKey threadKey = this.A0H;
        if (threadKey == null) {
            this.A0F.A05.markerEnd(5505047, A00(), (short) 42);
            C000700l.A08(-1543096058, A022);
        } else if (!ThreadKey.A0E(threadKey) || ((C10960l9) AnonymousClass1XX.A02(20, AnonymousClass1Y3.BOk, this.A0D)).A0B()) {
            this.A0G.A07();
            C08860g6 r32 = (C08860g6) AnonymousClass1XX.A02(33, AnonymousClass1Y3.Avu, this.A0D);
            if (r32.A01 == AnonymousClass07B.A00) {
                ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r32.A00)).AOI(r32.A05, "threadview_resume");
            }
            if (this.A11) {
                this.A11 = false;
                A0c(this, true, false, this.A0n);
                this.A0n = null;
            } else {
                A0c(this, false, false, "on_resume");
            }
            C61152yQ r03 = this.A0Q.A01;
            if (!(r03 == null || (threadSummary = r03.A02) == null)) {
                this.A0J.ASx(threadSummary.A0S, "threadViewFragment_onResume");
            }
            UserKey A072 = ThreadKey.A07(this.A0H);
            if (A072 != null) {
                this.A0J.ASn(A072.id);
            }
            A0M(this);
            A0b(this, true);
            this.A0g.C0f(C05690aA.A1c, this.A1P);
            this.A0F.A05.markerEnd(5505047, A00(), (short) 42);
            ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
            if (threadViewMessagesFragment != null) {
                threadViewMessagesFragment.A0z = this.A0Z;
                threadViewMessagesFragment.A10 = this.A0a;
            }
            A0D();
            C000700l.A08(-97063415, A022);
        } else {
            this.A0b.BC4(true);
            C000700l.A08(-1511139387, A022);
        }
    }

    public void A1q() {
        int A022 = C000700l.A02(-88119393);
        this.A0F.A0C(5505047, A00(), "onStart");
        super.A1q();
        this.A0j.A05 = true;
        if (((Boolean) this.A0u.get()).booleanValue()) {
            this.A0W.A08(true);
        }
        A0Q(this);
        this.A0F.A0D(5505047, A00(), 47);
        C000700l.A08(-1559637058, A022);
    }

    public void A1r() {
        int A022 = C000700l.A02(-427853077);
        super.A1r();
        this.A0j.A05 = false;
        this.A0W.A08(false);
        A0b(this, A0i());
        ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
        if (threadViewMessagesFragment != null) {
            threadViewMessagesFragment.A0z = null;
        }
        C000700l.A08(-2062292637, A022);
    }

    /* JADX INFO: finally extract failed */
    public void A1s(Context context) {
        super.A1s(context);
        C005505z.A03("ThreadViewFragment.onAttach", 1801250326);
        try {
            if (!this.A0x) {
                synchronized (this) {
                    C005505z.A03("ThreadViewFragment.injectMe", -2024978086);
                    try {
                        AnonymousClass1XX r5 = AnonymousClass1XX.get(context);
                        this.A0D = new AnonymousClass0UN(47, r5);
                        this.A0r = AnonymousClass0VG.A00(AnonymousClass1Y3.AlH, r5);
                        this.A0W = new C60432xB(r5);
                        this.A0G = C15870w7.A00(r5);
                        this.A0g = FbSharedPreferencesModule.A00(r5);
                        this.A0s = AnonymousClass0VG.A00(AnonymousClass1Y3.AXO, r5);
                        this.A0u = AnonymousClass0VG.A00(AnonymousClass1Y3.B0T, r5);
                        this.A0J = C190616q.A00(r5);
                        this.A0E = C24361Ti.A01(r5);
                        this.A0K = C14780u1.A00(r5);
                        this.A0i = C56882r2.A00(r5);
                        new C57752sZ(r5);
                        this.A0j = C55932oz.A00(r5);
                        this.A0P = C55922oy.A00(r5);
                        this.A0Y = C57952su.A00(r5);
                        this.A0q = AnonymousClass0VB.A00(AnonymousClass1Y3.A3D, r5);
                        this.A0F = C08770fv.A00(r5);
                        this.A0o = AnonymousClass0W9.A01();
                        this.A0B = C04430Uq.A02(r5);
                        this.A0f = C57822sg.A00(r5);
                        this.A0t = AnonymousClass0XJ.A0J(r5);
                        this.A0N = new C53482kr(r5);
                        this.A0e = new C53492ks(r5);
                        C005505z.A00(1584306767);
                        this.A0x = true;
                    } catch (Throwable th) {
                        C005505z.A00(-96835028);
                        throw th;
                    }
                }
            }
            this.A0F.A0C(5505047, A00(), "onAttach");
            A0S(this);
            this.A08 = A17();
            this.A0d = new AnonymousClass33E(this.A0e, context);
            this.A1I = new AnonymousClass33F(this);
            C57952su r2 = this.A0Y;
            r2.A02 = new AnonymousClass33G(this);
            r2.A06.A00 = new AnonymousClass33H(r2);
            r2.A03 = new AnonymousClass33J(r2);
            C08770fv r1 = this.A0F;
            if (r1 != null) {
                r1.A0D(5505047, A00(), 43);
            }
            C005505z.A00(-1561116973);
        } catch (Throwable th2) {
            C08770fv r12 = this.A0F;
            if (r12 != null) {
                r12.A0D(5505047, A00(), 43);
            }
            C005505z.A00(-1521980067);
            throw th2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void A1t(Bundle bundle) {
        int A022 = C000700l.A02(1070328428);
        super.A1t(bundle);
        if (this.A0H == null) {
            C010708t.A0I("ThreadViewFragment", "thread wasn't set before fragment was attached to activity!");
        }
        if (this.A17) {
            this.A17 = false;
            ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
            if (threadViewMessagesFragment != null) {
                threadViewMessagesFragment.A0e.Bvq(NavigationTrigger.A00("messenger_montage_quick_cam"), C73443g5.A0S, new MontageComposerFragmentParams.Builder(), this.A0H);
            }
        }
        LinearLayout linearLayout = (LinearLayout) ((ViewStubCompat) A2K(2131301078)).A00();
        this.A07 = linearLayout;
        LithoView lithoView = (LithoView) LayoutInflater.from(this.A03).inflate(2132411026, (ViewGroup) linearLayout, false);
        linearLayout.addView(lithoView);
        AnonymousClass3RA r5 = (AnonymousClass3RA) AnonymousClass1XX.A03(AnonymousClass1Y3.Aww, this.A0D);
        AnonymousClass3RH r4 = new AnonymousClass3RH(r5, lithoView, new AnonymousClass3RB(this), new AnonymousClass3RC(this), new AnonymousClass3RD(this), this.A1M, this.A0T, new AnonymousClass3RF(this), AnonymousClass3RG.A01(A1j()), new AnonymousClass3RI(r5));
        this.A0V = r4;
        if (r4 != null) {
            r4.CBq(true);
        }
        this.A0C = new AnonymousClass3RT(this);
        C55932oz r1 = this.A0j;
        r1.A03 = new AnonymousClass3RU(this);
        r1.A02();
        if (((Boolean) this.A0u.get()).booleanValue()) {
            this.A0W.A00 = new AnonymousClass3RW(this);
        }
        this.A10 = true;
        A0f("initial_setup");
        this.A0F.A0D(5505047, A00(), 46);
        C000700l.A08(-207470730, A022);
    }

    public void A1u(Bundle bundle) {
        super.A1u(bundle);
        if (this.A0H != null) {
            bundle.putBoolean("is_opened", this.A0y);
            C13290rA r1 = new C13290rA();
            r1.A01(this.A0H);
            r1.A02(this.A0U);
            r1.A05 = this.A0S;
            bundle.putParcelable("saved_thread_params", r1.A00());
        }
    }

    public void A1v(View view, Bundle bundle) {
        super.A1v(view, bundle);
        ViewGroup viewGroup = (ViewGroup) A2K(2131301070);
        this.A06 = viewGroup;
        Rect rect = this.A04;
        if (rect != null) {
            if (viewGroup == null) {
                this.A04 = rect;
            } else {
                C37531vp.A01(viewGroup, rect.top);
                this.A04 = null;
            }
        }
        ComposeFragment composeFragment = this.A0c.A0P;
        AnonymousClass70M r1 = this.A1G;
        if (composeFragment.A0J == null) {
            composeFragment.A0J = new AnonymousClass3R9();
        }
        composeFragment.A0J.A00 = r1;
        composeFragment.A0K = null;
        this.A0l = (RoundedCornersFrameLayout) A2K(2131301068);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b1, code lost:
        if (r10.A01 != false) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00cd, code lost:
        if (r10.A01 != false) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d0, code lost:
        r6.add(r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1x(boolean r12) {
        /*
            r11 = this;
            super.A1x(r12)
            com.facebook.orca.threadview.ThreadViewMessagesFragment r1 = r11.A0c
            if (r1 == 0) goto L_0x014a
            r0 = r12 ^ 1
            r1.A1W(r0)
            com.facebook.orca.threadview.ThreadViewMessagesFragment r4 = r11.A0c
            boolean r0 = r4.A1Z()
            if (r0 == 0) goto L_0x014a
            if (r12 == 0) goto L_0x00d4
            X.3EZ r1 = r4.A0f
            java.lang.String r0 = r1.A01
            if (r0 == 0) goto L_0x001f
            X.AnonymousClass3EZ.A03(r1, r0)
        L_0x001f:
            com.facebook.messaging.composer.ComposeFragment r0 = r4.A0P
            r0.A2a()
            X.3MF r0 = r4.A15
            r3 = 0
            r0.A01 = r3
            r2 = 45
            int r1 = X.AnonymousClass1Y3.BFO
            X.0UN r0 = r4.A0M
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3VF r1 = (X.AnonymousClass3VF) r1
            r0 = 0
            r1.A01 = r0
            r1.A00 = r3
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A0Y
            com.facebook.messaging.model.messages.MessagesCollection r0 = r4.A0X
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0e(r4, r1, r0)
            r2 = 26
            int r1 = X.AnonymousClass1Y3.AaR
            X.0UN r0 = r4.A0M
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3SG r0 = (X.AnonymousClass3SG) r0
            r0.A03()
            r2 = 61
            int r1 = X.AnonymousClass1Y3.AX1
            X.0UN r0 = r4.A0M
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6aG r1 = (X.C136786aG) r1
            java.util.Set r0 = r1.A01
            r0.clear()
            r1.A00 = r3
            r4.A1I = r3
            X.3Dy r0 = r4.A0d
            r0.A02()
            r2 = 74
            int r1 = X.AnonymousClass1Y3.AHs
            X.0UN r0 = r4.A0M
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.5hg r1 = (X.C117605hg) r1
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r4.A0Y
            X.3EO r0 = r4.A17
            com.google.common.collect.ImmutableList r9 = r0.A0J
            if (r2 == 0) goto L_0x010c
            com.facebook.analytics.DeprecatedAnalyticsLogger r1 = r1.A00
            r8 = 0
            java.lang.String r0 = "message_collapsing_groups_impression"
            X.1La r5 = r1.A04(r0, r8)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            int r3 = r9.size()
        L_0x0095:
            if (r8 >= r3) goto L_0x00da
            java.lang.Object r10 = r9.get(r8)
            X.3Wm r10 = (X.C69293Wm) r10
            boolean r0 = r10 instanceof X.AnonymousClass3XN
            if (r0 == 0) goto L_0x00b9
            X.3XN r10 = (X.AnonymousClass3XN) r10
            X.20N r0 = r10.A00
            java.lang.String r1 = X.C117605hg.A02(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x00b6
            boolean r0 = r10.A01
            if (r0 == 0) goto L_0x00d0
        L_0x00b3:
            r7.add(r1)
        L_0x00b6:
            int r8 = r8 + 1
            goto L_0x0095
        L_0x00b9:
            boolean r0 = r10 instanceof X.C117545hZ
            if (r0 == 0) goto L_0x00b6
            X.5hZ r10 = (X.C117545hZ) r10
            X.5hb r0 = r10.A00
            java.lang.String r1 = X.C117605hg.A03(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x00b6
            boolean r0 = r10.A01
            if (r0 == 0) goto L_0x00d0
            goto L_0x00b3
        L_0x00d0:
            r6.add(r1)
            goto L_0x00b6
        L_0x00d4:
            com.facebook.messaging.composer.ComposeFragment r0 = r4.A0P
            r0.A2b()
            goto L_0x010c
        L_0x00da:
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r1 = r7.toString()
            java.lang.String r0 = "expanded_admin_message_groups"
            r3.put(r0, r1)
            java.lang.String r1 = r6.toString()
            java.lang.String r0 = "collapsed_admin_message_groups"
            r3.put(r0, r1)
            boolean r0 = r5.A0B()
            if (r0 == 0) goto L_0x010c
            long r1 = r2.A0G()
            java.lang.String r0 = "tfbid"
            r5.A03(r0, r1)
            java.lang.String r1 = r3.toString()
            java.lang.String r0 = "payload"
            r5.A06(r0, r1)
            r5.A0A()
        L_0x010c:
            r2 = 102(0x66, float:1.43E-43)
            int r1 = X.AnonymousClass1Y3.AS8
            X.0UN r0 = r4.A0M
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3YZ r0 = (X.AnonymousClass3YZ) r0
            java.util.WeakHashMap r0 = r0.A00
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0122:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0134
            java.lang.Object r0 = r1.next()
            X.Ewx r0 = (X.C30465Ewx) r0
            if (r0 == 0) goto L_0x0122
            r0.onHostHiddenChanged(r12)
            goto L_0x0122
        L_0x0134:
            if (r12 == 0) goto L_0x014a
            android.content.Context r0 = r11.A1j()
            boolean r0 = X.C22541Lt.A00(r0)
            if (r0 == 0) goto L_0x0145
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r11.A0c
            r0.A2S()
        L_0x0145:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r11.A0c
            r0.A2a()
        L_0x014a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadview.ThreadViewFragment.A1x(boolean):void");
    }

    public void A2A() {
        super.A2A();
        if (this.A0y && this.A0H != null) {
            ThreadViewMessagesFragment threadViewMessagesFragment = this.A0c;
            if (!threadViewMessagesFragment.A1X) {
                threadViewMessagesFragment.A2Y();
            }
        }
    }

    public void A2G(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, View view) {
        super.A2G(layoutInflater, viewGroup, bundle, view);
        this.A1A = null;
    }

    public void A2H(boolean z, boolean z2) {
        super.A2H(z, z2);
        A0B();
        if (z) {
            if (this.A0i) {
                A0d("setUserVisibleHint");
            }
            A0M(this);
        }
    }

    public CustomKeyboardLayout AjP() {
        return (CustomKeyboardLayout) A2K(2131297523);
    }
}
