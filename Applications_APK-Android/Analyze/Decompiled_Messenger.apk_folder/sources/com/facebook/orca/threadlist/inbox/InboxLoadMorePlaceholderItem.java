package com.facebook.orca.threadlist.inbox;

import X.AnonymousClass1GG;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class InboxLoadMorePlaceholderItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1GG();

    public int hashCode() {
        return 0;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public InboxLoadMorePlaceholderItem(X.C27161ck r3) {
        /*
            r2 = this;
            X.1GH r1 = X.AnonymousClass1GH.A01
            r0 = 0
            r2.<init>(r3, r0, r1)
            com.google.common.base.Preconditions.checkNotNull(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem.<init>(X.1ck):void");
    }

    public InboxLoadMorePlaceholderItem(Parcel parcel) {
        super(parcel);
    }
}
