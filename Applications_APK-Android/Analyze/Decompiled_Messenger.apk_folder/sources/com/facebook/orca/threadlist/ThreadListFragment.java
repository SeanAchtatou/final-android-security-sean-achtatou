package com.facebook.orca.threadlist;

import X.AnonymousClass07A;
import X.AnonymousClass07B;
import X.AnonymousClass07c;
import X.AnonymousClass08D;
import X.AnonymousClass0UN;
import X.AnonymousClass0UU;
import X.AnonymousClass0UX;
import X.AnonymousClass0VG;
import X.AnonymousClass0VL;
import X.AnonymousClass0W9;
import X.AnonymousClass0WP;
import X.AnonymousClass0WT;
import X.AnonymousClass0WY;
import X.AnonymousClass0X5;
import X.AnonymousClass0X6;
import X.AnonymousClass0XV;
import X.AnonymousClass0p4;
import X.AnonymousClass0qy;
import X.AnonymousClass0tR;
import X.AnonymousClass106;
import X.AnonymousClass10D;
import X.AnonymousClass10M;
import X.AnonymousClass10Q;
import X.AnonymousClass10R;
import X.AnonymousClass10S;
import X.AnonymousClass10U;
import X.AnonymousClass144;
import X.AnonymousClass16N;
import X.AnonymousClass16R;
import X.AnonymousClass17O;
import X.AnonymousClass17P;
import X.AnonymousClass17Q;
import X.AnonymousClass17S;
import X.AnonymousClass17W;
import X.AnonymousClass17Y;
import X.AnonymousClass18d;
import X.AnonymousClass19B;
import X.AnonymousClass19G;
import X.AnonymousClass19M;
import X.AnonymousClass19S;
import X.AnonymousClass1AB;
import X.AnonymousClass1AC;
import X.AnonymousClass1AD;
import X.AnonymousClass1AP;
import X.AnonymousClass1AQ;
import X.AnonymousClass1AR;
import X.AnonymousClass1AV;
import X.AnonymousClass1AY;
import X.AnonymousClass1B0;
import X.AnonymousClass1B1;
import X.AnonymousClass1B2;
import X.AnonymousClass1B7;
import X.AnonymousClass1B9;
import X.AnonymousClass1BC;
import X.AnonymousClass1BX;
import X.AnonymousClass1BY;
import X.AnonymousClass1CV;
import X.AnonymousClass1DW;
import X.AnonymousClass1F9;
import X.AnonymousClass1FC;
import X.AnonymousClass1FD;
import X.AnonymousClass1G0;
import X.AnonymousClass1G2;
import X.AnonymousClass1G3;
import X.AnonymousClass1GA;
import X.AnonymousClass1K9;
import X.AnonymousClass1MT;
import X.AnonymousClass1Ri;
import X.AnonymousClass1TC;
import X.AnonymousClass1TF;
import X.AnonymousClass1TR;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y6;
import X.AnonymousClass1YI;
import X.AnonymousClass1ZD;
import X.AnonymousClass1ZE;
import X.AnonymousClass23D;
import X.AnonymousClass23O;
import X.AnonymousClass24E;
import X.AnonymousClass2QE;
import X.AnonymousClass2W7;
import X.AnonymousClass43X;
import X.AnonymousClass7KN;
import X.C000700l;
import X.C001500z;
import X.C005505z;
import X.C013509w;
import X.C013709y;
import X.C04310Tq;
import X.C04480Uv;
import X.C04490Ux;
import X.C05690aA;
import X.C05720aD;
import X.C05730aE;
import X.C06460bX;
import X.C06480bZ;
import X.C06680bu;
import X.C06920cI;
import X.C08770fv;
import X.C10700ki;
import X.C10920l5;
import X.C10960l9;
import X.C10990lD;
import X.C11670nb;
import X.C11680nc;
import X.C13030qT;
import X.C13060qW;
import X.C13240qx;
import X.C13250qz;
import X.C13460rT;
import X.C13570rf;
import X.C13820s8;
import X.C13860sC;
import X.C13880sE;
import X.C13890sF;
import X.C13910sH;
import X.C13920sI;
import X.C13930sJ;
import X.C13980sP;
import X.C13990sQ;
import X.C14080sa;
import X.C14160si;
import X.C14220so;
import X.C14420tH;
import X.C144516o1;
import X.C14990uX;
import X.C15320v6;
import X.C15330v7;
import X.C15340v8;
import X.C15410vF;
import X.C15420vG;
import X.C15430vH;
import X.C15440vI;
import X.C15450vJ;
import X.C15460vK;
import X.C15490vN;
import X.C15500vO;
import X.C15540vS;
import X.C15560vU;
import X.C15750vt;
import X.C15780vy;
import X.C15870w7;
import X.C16430x3;
import X.C16440x4;
import X.C16450x5;
import X.C16470x7;
import X.C16480x8;
import X.C16700xd;
import X.C186414n;
import X.C187715a;
import X.C187815b;
import X.C188115e;
import X.C190318tG;
import X.C191016u;
import X.C191917d;
import X.C192017f;
import X.C192117g;
import X.C192217h;
import X.C193017p;
import X.C193217r;
import X.C194318c;
import X.C194518f;
import X.C194618g;
import X.C195518s;
import X.C195618t;
import X.C195918w;
import X.C196118y;
import X.C197919r;
import X.C19911Ae;
import X.C19941Ah;
import X.C20031Aq;
import X.C20051As;
import X.C20061At;
import X.C20071Au;
import X.C20081Av;
import X.C20101Ax;
import X.C201109dk;
import X.C20121Az;
import X.C20181Bf;
import X.C20191Bg;
import X.C20871Ed;
import X.C20961Em;
import X.C20971En;
import X.C21441Gz;
import X.C21451Ha;
import X.C22431Lh;
import X.C24361Ti;
import X.C25011Xz;
import X.C25051Yd;
import X.C25091Yh;
import X.C27717Dhl;
import X.C27811DjO;
import X.C27814DjR;
import X.C28461eq;
import X.C29631gj;
import X.C30111hV;
import X.C30281hn;
import X.C31371ja;
import X.C32491lq;
import X.C32541lv;
import X.C32871mT;
import X.C32901mW;
import X.C33701o0;
import X.C33741o4;
import X.C33891oJ;
import X.C34861qI;
import X.C34881qK;
import X.C35201qq;
import X.C35211qr;
import X.C35221qs;
import X.C35231qt;
import X.C35241qu;
import X.C35321r2;
import X.C410023t;
import X.C417626w;
import X.C45702Nd;
import X.C46062Op;
import X.C46902Si;
import X.C50462e4;
import X.C52652jT;
import X.C55452o6;
import X.C60982y9;
import X.C637038i;
import X.C67023Mq;
import X.C69053Vl;
import X.C71193by;
import X.C72243dw;
import X.C74163hN;
import X.C79923rU;
import X.C88684Lp;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.common.util.TriState;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.litho.LithoView;
import com.facebook.messaging.business.inboxads.postclick.fragment.InboxAdsPostClickFragment;
import com.facebook.messaging.inbox2.analytics.InboxSourceLoggingData;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.neue.nullstate.RecentsTabEmptyView;
import com.facebook.messaging.threadlist.ui.extendedfab.ExtendedFloatingActionButton;
import com.facebook.messaging.threadlist.ui.extendedfab.agent.ThreadListExtendedFloatingActionButtonAgent$AutoSizeAdjustOnScrollBehavior;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.resources.ui.FbFrameLayout;
import com.facebook.widget.listview.EmptyListViewItem;
import com.facebook.widget.recyclerview.BetterRecyclerView;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableSet;
import io.card.payment.BuildConfig;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

public final class ThreadListFragment extends C13460rT implements C13240qx, AnonymousClass0qy, C13250qz, C13570rf, C15780vy, CallerContextable {
    public static final CallerContext A1m = CallerContext.A06(ThreadListFragment.class, "messages");
    public int A00;
    public int A01;
    public int A02 = 0;
    public Context A03;
    public AudioManager A04;
    public View A05;
    public View A06;
    public ViewGroup A07;
    public ViewGroup A08;
    public C20871Ed A09;
    public DeprecatedAnalyticsLogger A0A;
    public AnonymousClass144 A0B;
    public AnonymousClass1ZE A0C;
    public C14220so A0D;
    public C04480Uv A0E;
    public AnonymousClass0WP A0F;
    public C14080sa A0G;
    public AnonymousClass1Y6 A0H;
    public C06480bZ A0I;
    public C001500z A0J;
    public C15440vI A0K;
    public C15450vJ A0L;
    public C192117g A0M;
    public BlueServiceOperationFactory A0N;
    public AnonymousClass0UN A0O;
    public C15420vG A0P;
    public AnonymousClass1GA A0Q;
    public AnonymousClass1AC A0R;
    public AnonymousClass1AB A0S;
    public AnonymousClass1Ri A0T;
    public C24361Ti A0U;
    public C08770fv A0V;
    public C15500vO A0W;
    public C14420tH A0X;
    public C15870w7 A0Y;
    public C33701o0 A0Z;
    public AnonymousClass10R A0a;
    public C15340v8 A0b;
    public C15330v7 A0c;
    public C15430vH A0d;
    public C32871mT A0e;
    public C20181Bf A0f;
    public C10700ki A0g = C10700ki.ALL;
    public RecentsTabEmptyView A0h;
    public AnonymousClass10S A0i;
    public C22431Lh A0j;
    public C14160si A0k;
    public AnonymousClass1BC A0l;
    public AnonymousClass10Q A0m;
    public C55452o6 A0n;
    public C15460vK A0o;
    public C27814DjR A0p;
    public AnonymousClass1BY A0q;
    public C67023Mq A0r;
    public MigColorScheme A0s;
    public C25051Yd A0t;
    public C19941Ah A0u;
    public AnonymousClass106 A0v;
    public C196118y A0w;
    public AnonymousClass10D A0x;
    public C15490vN A0y;
    public C33741o4 A0z;
    public C13880sE A10 = C13880sE.A07;
    public AnonymousClass16N A11;
    public AnonymousClass1B2 A12;
    public AnonymousClass19G A13;
    public AnonymousClass16R A14;
    public AnonymousClass1K9 A15;
    public C15410vF A16;
    public C15560vU A17;
    public C15540vS A18;
    public C187715a A19;
    public C35201qq A1A;
    public C186414n A1B;
    public FbSharedPreferences A1C;
    public C15750vt A1D;
    public EmptyListViewItem A1E;
    public BetterRecyclerView A1F;
    public C79923rU A1G;
    public AnonymousClass10U A1H;
    public Boolean A1I;
    public Integer A1J = AnonymousClass07B.A01;
    public String A1K;
    public Random A1L;
    public Set A1M;
    public Executor A1N;
    public ScheduledExecutorService A1O;
    public ScheduledFuture A1P;
    public C04310Tq A1Q;
    public C04310Tq A1R;
    public C04310Tq A1S;
    public C04310Tq A1T;
    public boolean A1U;
    public boolean A1V;
    public boolean A1W;
    public boolean A1X;
    public boolean A1Y;
    public boolean A1Z;
    public boolean A1a;
    public boolean A1b;
    public boolean A1c;
    private int A1d = -1;
    private AnonymousClass1AY A1e;
    private C20061At A1f;
    private C191917d A1g;
    private C16450x5 A1h;
    private boolean A1i = true;
    private final C13990sQ A1j = new C13990sQ(this, new C13920sI(this));
    private final C13030qT A1k = new C13910sH(this);
    private final Object A1l = new Object();
    public MediaPlayer mMediaPlayer = null;

    public void Bkm(String str) {
    }

    private int A00() {
        if (this.A1d == -1) {
            this.A1d = this.A1L.nextInt(Integer.MAX_VALUE);
        }
        return this.A1d;
    }

    public static int A04(ImmutableList immutableList, long j, int i, int i2) {
        if (i >= i2) {
            return i2;
        }
        int i3 = (i2 + i) >> 1;
        if (((ThreadSummary) immutableList.get(i3)).A0B == j) {
            return i3;
        }
        if (((ThreadSummary) immutableList.get(i3)).A0B < j) {
            return A04(immutableList, j, i, i3 - 1);
        }
        return A04(immutableList, j, i3 + 1, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0067, code lost:
        if (r2 > 0) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0073, code lost:
        if (r2 > 0) goto L_0x0075;
     */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0311  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C16070wR A07(com.facebook.orca.threadlist.ThreadListFragment r35, X.C13880sE r36, java.lang.Integer r37) {
        /*
            r6 = r36
            X.101 r2 = r6.A03
            r1 = r35
            if (r2 == 0) goto L_0x03cc
            X.19G r0 = r1.A13
            boolean r0 = r0.A0A(r2)
            if (r0 == 0) goto L_0x03cc
            X.101 r0 = r6.A03
            X.12V r3 = X.AnonymousClass19G.A03(r0)
            if (r3 == 0) goto L_0x03cc
            int r2 = X.AnonymousClass1Y3.AkU
            X.0UN r0 = r1.A0O
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r0)
            X.0tM r2 = (X.C14450tM) r2
            X.19G r10 = r1.A13
            X.0sF r0 = r6.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r0 = r0.A00
            com.google.common.collect.ImmutableList r7 = r2.A02(r0)
            X.1ck r0 = r3.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = r0.A0R()
            boolean r9 = X.AnonymousClass19G.A07(r5)
            if (r9 == 0) goto L_0x03c8
            com.facebook.graphql.enums.GraphQLMessengerInbox2RecentUnitConfigType r2 = com.facebook.graphql.enums.GraphQLMessengerInbox2RecentUnitConfigType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            r0 = 17530391(0x10b7e17, float:2.5620733E-38)
            java.lang.Enum r4 = r5.A0O(r0, r2)
            com.facebook.graphql.enums.GraphQLMessengerInbox2RecentUnitConfigType r4 = (com.facebook.graphql.enums.GraphQLMessengerInbox2RecentUnitConfigType) r4
        L_0x0045:
            if (r9 == 0) goto L_0x03c4
            r0 = -1060097582(0xffffffffc0d031d2, float:-6.5060816)
            int r11 = r5.getIntValue(r0)
        L_0x004e:
            boolean r0 = X.AnonymousClass19G.A07(r5)
            if (r0 == 0) goto L_0x03c1
            r0 = -656472187(0xffffffffd8df0785, float:-1.96178711E15)
            int r0 = r5.getIntValue(r0)
            if (r0 <= 0) goto L_0x03c1
        L_0x005d:
            if (r9 == 0) goto L_0x03bd
            r2 = 581618071(0x22aac997, float:4.6292034E-18)
            int r2 = r5.getIntValue(r2)
            r8 = r2
            if (r2 <= 0) goto L_0x03bd
        L_0x0069:
            if (r9 == 0) goto L_0x03ba
            r2 = -929496119(0xffffffffc89903c9, float:-313374.28)
            int r2 = r5.getIntValue(r2)
            r3 = r2
            if (r2 <= 0) goto L_0x03ba
        L_0x0075:
            if (r9 == 0) goto L_0x03b6
            r2 = 863271010(0x33747862, float:5.6920165E-8)
            int r9 = r5.getIntValue(r2)
        L_0x007e:
            int r2 = r4.ordinal()
            r12 = 3600000(0x36ee80, double:1.7786363E-317)
            switch(r2) {
                case 2: goto L_0x0360;
                case 3: goto L_0x039a;
                case 4: goto L_0x036f;
                default: goto L_0x0088;
            }
        L_0x0088:
            long r2 = A05(r7, r3)
        L_0x008c:
            X.0ki r5 = r1.A0g
            X.0ki r4 = X.C10700ki.ALL
            r0 = 0
            if (r5 != r4) goto L_0x0094
            r0 = 1
        L_0x0094:
            if (r0 == 0) goto L_0x04c0
            int r4 = X.AnonymousClass1Y3.AkU
            X.0UN r0 = r1.A0O
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r4, r0)
            X.0tM r5 = (X.C14450tM) r5
            X.0sF r0 = r6.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r4 = r0.A00
            X.0sF r0 = r6.A05
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r0 = r0.A00
            com.google.common.collect.ImmutableList r0 = X.AnonymousClass170.A01(r4, r0)
            com.google.common.collect.ImmutableList r24 = r5.A02(r0)
            X.19G r4 = r1.A13
            X.101 r0 = r6.A03
            boolean r0 = r4.A0A(r0)
            if (r0 == 0) goto L_0x046c
            X.1I1 r5 = new X.1I1
            r5.<init>(r2)
            int r2 = X.AnonymousClass1Y3.BJA
            X.0UN r0 = r1.A0O
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r0)
            X.1BA r3 = (X.AnonymousClass1BA) r3
            X.19G r4 = r1.A13
            X.101 r0 = r6.A03
            com.google.common.collect.ImmutableList r0 = r0.A03
            r36 = r0
            X.0sF r0 = r6.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            boolean r0 = r0.A01
            r23 = r0
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r0 = r6.A04
            r35 = r0
            X.1qC r0 = r6.A00
            r34 = r0
            X.0w2 r0 = r6.A01
            r33 = r0
            X.101 r0 = r6.A02
            r31 = r0
            X.1Hz r22 = new X.1Hz
            r22.<init>()
            X.1Bu r2 = new X.1Bu
            X.1nx r0 = r3.A00
            r2.<init>(r0)
            boolean r0 = r2.A01()
            if (r0 == 0) goto L_0x0106
            r0 = r22
            java.util.List r0 = r0.A01
            r0.add(r2)
        L_0x0106:
            r3 = r36
            X.1lp r21 = new X.1lp
            r21.<init>()
            java.util.ArrayList r20 = new java.util.ArrayList
            r20.<init>()
            int r8 = r36.size()
            r7 = 0
        L_0x0117:
            r19 = 0
            if (r7 >= r8) goto L_0x035c
            java.lang.Object r6 = r3.get(r7)
            X.12V r6 = (X.AnonymousClass12V) r6
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r2 = r6.A00
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A0I
            if (r2 != r0) goto L_0x0358
            X.1ck r0 = r6.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = r0.A0R()
            boolean r0 = X.AnonymousClass19G.A07(r2)
            if (r0 == 0) goto L_0x0355
            r0 = -656472187(0xffffffffd8df0785, float:-1.96178711E15)
            int r0 = r2.getIntValue(r0)
            if (r0 <= 0) goto L_0x0355
        L_0x013c:
            java.lang.Integer r18 = java.lang.Integer.valueOf(r0)
        L_0x0140:
            int r17 = r36.size()
            r16 = r19
            r7 = 0
            r6 = 0
        L_0x0148:
            r0 = r17
            if (r7 >= r0) goto L_0x03d0
            java.lang.Object r8 = r3.get(r7)
            X.12V r8 = (X.AnonymousClass12V) r8
            r0 = r21
            r10 = r18
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r2 = r8.A00
            int r2 = r2.ordinal()
            switch(r2) {
                case 3: goto L_0x0224;
                case 25: goto L_0x02a0;
                case 29: goto L_0x01a4;
                default: goto L_0x015f;
            }
        L_0x015f:
            r32 = 0
            r25 = r4
            r26 = r0
            r27 = r8
            r28 = r35
            r29 = r34
            r30 = r33
            X.AnonymousClass19G.A05(r25, r26, r27, r28, r29, r30, r31, r32)
        L_0x0170:
            com.google.common.collect.ImmutableList$Builder r0 = r0.A03
            com.google.common.collect.ImmutableList r0 = r0.build()
            r2 = r22
            com.google.common.collect.ImmutableList r3 = r2.A00(r0)
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r2 = r8.A00
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A02
            if (r2 != r0) goto L_0x0186
            java.lang.Integer r16 = java.lang.Integer.valueOf(r7)
        L_0x0186:
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A0I
            if (r2 != r0) goto L_0x0192
            r0 = r22
            int r6 = r0.A00
            java.lang.Integer r19 = java.lang.Integer.valueOf(r7)
        L_0x0192:
            r0 = r20
            r0.add(r3)
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()
            r0 = r21
            r0.A03 = r2
            int r7 = r7 + 1
            r3 = r36
            goto L_0x0148
        L_0x01a4:
            X.0tc r2 = r4.A04
            r2.A07(r0, r8)
            int r9 = X.AnonymousClass1Y3.BAo
            X.0UN r3 = r4.A00
            r2 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r9, r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 != 0) goto L_0x021a
            com.google.common.base.Predicates$ObjectPredicate r11 = com.google.common.base.Predicates.ObjectPredicate.ALWAYS_FALSE
        L_0x01bc:
            X.0tV r3 = r4.A05
            X.0tc r2 = r4.A04
            r28 = r24
            r25 = r3
            r26 = r0
            r27 = r8
            r29 = r2
            r30 = r11
            r25.A01(r26, r27, r28, r29, r30)
            int r9 = X.AnonymousClass1Y3.BO4
            X.0UN r3 = r4.A00
            r2 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r9, r3)
            X.10R r2 = (X.AnonymousClass10R) r2
            X.1Yd r9 = r2.A00
            r2 = 282325382792517(0x100c600270545, double:1.39487272586762E-309)
            boolean r2 = r9.Aem(r2)
            if (r2 == 0) goto L_0x021d
            X.0tc r13 = r4.A04
            r12 = 3
            if (r18 == 0) goto L_0x01f0
            int r12 = r10.intValue()
        L_0x01f0:
            r2 = r24
            int r10 = r2.size()
            r9 = 0
            r3 = 0
        L_0x01f8:
            if (r9 >= r10) goto L_0x0170
            r14 = r24
            java.lang.Object r14 = r14.get(r9)
            com.facebook.messaging.model.threads.ThreadSummary r14 = (com.facebook.messaging.model.threads.ThreadSummary) r14
            boolean r2 = r11.apply(r14)
            if (r2 != 0) goto L_0x0215
            X.1ck r2 = r8.A01
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = r13.A05(r2, r14)
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r2
            r0.A01(r2)
            int r3 = r3 + 1
        L_0x0215:
            if (r3 == r12) goto L_0x0170
            int r9 = r9 + 1
            goto L_0x01f8
        L_0x021a:
            com.google.common.base.Predicate r11 = X.AnonymousClass19G.A0B
            goto L_0x01bc
        L_0x021d:
            r12 = r24
            X.AnonymousClass19G.A06(r4, r0, r8, r12, r5)
            goto L_0x0170
        L_0x0224:
            int r9 = X.AnonymousClass1Y3.BO4
            X.0UN r3 = r4.A00
            r2 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r9, r3)
            X.10R r2 = (X.AnonymousClass10R) r2
            X.1Yd r9 = r2.A00
            r2 = 282325382792517(0x100c600270545, double:1.39487272586762E-309)
            boolean r2 = r9.Aem(r2)
            if (r2 == 0) goto L_0x0288
            r9 = 0
            int r3 = X.AnonymousClass1Y3.BAo
            X.0UN r2 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r9, r3, r2)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 != 0) goto L_0x0285
            com.google.common.base.Predicates$ObjectPredicate r14 = com.google.common.base.Predicates.ObjectPredicate.ALWAYS_FALSE
        L_0x024f:
            X.0tc r13 = r4.A04
            r12 = 3
            if (r18 == 0) goto L_0x0258
            int r12 = r10.intValue()
        L_0x0258:
            r2 = r24
            int r11 = r2.size()
            r10 = 0
        L_0x025f:
            if (r10 >= r11) goto L_0x0292
            r2 = r24
            java.lang.Object r3 = r2.get(r10)
            com.facebook.messaging.model.threads.ThreadSummary r3 = (com.facebook.messaging.model.threads.ThreadSummary) r3
            boolean r2 = r14.apply(r3)
            if (r2 != 0) goto L_0x0282
            if (r9 < r12) goto L_0x0280
            X.1ck r2 = r8.A01
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = r13.A05(r2, r3)
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r2
            r25 = r0
            r26 = r2
            r25.A01(r26)
        L_0x0280:
            int r9 = r9 + 1
        L_0x0282:
            int r10 = r10 + 1
            goto L_0x025f
        L_0x0285:
            com.google.common.base.Predicate r14 = X.AnonymousClass19G.A0B
            goto L_0x024f
        L_0x0288:
            com.google.common.base.Predicates$NotPredicate r2 = new com.google.common.base.Predicates$NotPredicate
            r2.<init>(r5)
            r12 = r24
            X.AnonymousClass19G.A06(r4, r0, r8, r12, r2)
        L_0x0292:
            if (r23 != 0) goto L_0x0170
            com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem r3 = new com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem
            X.1ck r2 = r8.A01
            r3.<init>(r2)
            r0.A01(r3)
            goto L_0x0170
        L_0x02a0:
            if (r18 == 0) goto L_0x0305
            int r10 = r10.intValue()
        L_0x02a6:
            java.lang.Object r2 = r8.A02
            com.google.common.base.Preconditions.checkNotNull(r2)
            X.0tc r2 = r4.A04
            r2.A07(r0, r8)
            int r9 = X.AnonymousClass1Y3.BO4
            X.0UN r3 = r4.A00
            r2 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r9, r3)
            X.10R r2 = (X.AnonymousClass10R) r2
            X.1Yd r9 = r2.A00
            r2 = 282325382792517(0x100c600270545, double:1.39487272586762E-309)
            boolean r2 = r9.Aem(r2)
            r11 = 0
            if (r2 == 0) goto L_0x032b
            int r3 = X.AnonymousClass1Y3.BAo
            X.0UN r2 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r11, r3, r2)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 != 0) goto L_0x0302
            com.google.common.base.Predicates$ObjectPredicate r14 = com.google.common.base.Predicates.ObjectPredicate.ALWAYS_FALSE
        L_0x02db:
            java.util.HashSet r12 = new java.util.HashSet
            r12.<init>()
            r2 = r24
            int r13 = r2.size()
            r9 = 0
            r3 = 0
        L_0x02e8:
            if (r9 >= r13) goto L_0x0307
            r25 = r9
            java.lang.Object r2 = r24.get(r25)
            com.facebook.messaging.model.threads.ThreadSummary r2 = (com.facebook.messaging.model.threads.ThreadSummary) r2
            boolean r15 = r14.apply(r2)
            if (r15 != 0) goto L_0x02fd
            r12.add(r2)
            int r3 = r3 + 1
        L_0x02fd:
            if (r3 == r10) goto L_0x0307
            int r9 = r9 + 1
            goto L_0x02e8
        L_0x0302:
            com.google.common.base.Predicate r14 = X.AnonymousClass19G.A0B
            goto L_0x02db
        L_0x0305:
            r10 = 3
            goto L_0x02a6
        L_0x0307:
            java.lang.Object r10 = r8.A02
            com.google.common.collect.ImmutableList r10 = (com.google.common.collect.ImmutableList) r10
            int r9 = r10.size()
        L_0x030f:
            if (r11 >= r9) goto L_0x0170
            java.lang.Object r13 = r10.get(r11)
            com.facebook.messaging.model.threads.ThreadSummary r13 = (com.facebook.messaging.model.threads.ThreadSummary) r13
            boolean r2 = r12.contains(r13)
            if (r2 != 0) goto L_0x0328
            X.0tc r3 = r4.A04
            X.1ck r2 = r8.A01
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = r3.A05(r2, r13)
            r0.A01(r2)
        L_0x0328:
            int r11 = r11 + 1
            goto L_0x030f
        L_0x032b:
            java.lang.Object r10 = r8.A02
            com.google.common.collect.ImmutableList r10 = (com.google.common.collect.ImmutableList) r10
            int r9 = r10.size()
        L_0x0333:
            if (r11 >= r9) goto L_0x0170
            java.lang.Object r12 = r10.get(r11)
            com.facebook.messaging.model.threads.ThreadSummary r12 = (com.facebook.messaging.model.threads.ThreadSummary) r12
            long r2 = r12.A0B
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            boolean r2 = r5.apply(r2)
            if (r2 != 0) goto L_0x0352
            X.0tc r3 = r4.A04
            X.1ck r2 = r8.A01
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = r3.A05(r2, r12)
            r0.A01(r2)
        L_0x0352:
            int r11 = r11 + 1
            goto L_0x0333
        L_0x0355:
            r0 = 3
            goto L_0x013c
        L_0x0358:
            int r7 = r7 + 1
            goto L_0x0117
        L_0x035c:
            r18 = r19
            goto L_0x0140
        L_0x0360:
            X.06B r2 = r10.A01
            long r4 = r2.now()
            long r2 = (long) r11
            long r2 = r2 * r12
            long r4 = r4 - r2
            long r2 = A06(r7, r0, r8, r4)
            goto L_0x008c
        L_0x036f:
            X.06B r2 = r10.A01
            long r2 = r2.now()
            long r4 = (long) r11
            long r4 = r4 * r12
            long r2 = r2 - r4
            long r2 = A06(r7, r0, r8, r2)
            int r4 = X.AnonymousClass19G.A01(r10, r7, r9)
            if (r4 >= r0) goto L_0x038c
            long r4 = A05(r7, r0)
        L_0x0386:
            long r2 = java.lang.Math.min(r2, r4)
            goto L_0x008c
        L_0x038c:
            if (r4 < r8) goto L_0x0393
            long r4 = A05(r7, r8)
            goto L_0x0386
        L_0x0393:
            int r0 = r4 + 1
            long r4 = A05(r7, r0)
            goto L_0x0386
        L_0x039a:
            int r2 = X.AnonymousClass19G.A01(r10, r7, r11)
            if (r2 >= r0) goto L_0x03a6
            long r2 = A05(r7, r0)
            goto L_0x008c
        L_0x03a6:
            if (r2 < r8) goto L_0x03ae
            long r2 = A05(r7, r8)
            goto L_0x008c
        L_0x03ae:
            int r0 = r2 + 1
            long r2 = A05(r7, r0)
            goto L_0x008c
        L_0x03b6:
            r9 = 24
            goto L_0x007e
        L_0x03ba:
            r3 = 3
            goto L_0x0075
        L_0x03bd:
            r8 = 20
            goto L_0x0069
        L_0x03c1:
            r0 = 3
            goto L_0x005d
        L_0x03c4:
            r11 = 24
            goto L_0x004e
        L_0x03c8:
            com.facebook.graphql.enums.GraphQLMessengerInbox2RecentUnitConfigType r4 = com.facebook.graphql.enums.GraphQLMessengerInbox2RecentUnitConfigType.UNREAD_BASED
            goto L_0x0045
        L_0x03cc:
            r2 = 0
            goto L_0x008c
        L_0x03d0:
            r5 = r20
            r4 = r19
            r2 = r16
            r0 = r22
            java.util.List r0 = r0.A01
            boolean r0 = r0.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0453
            if (r19 == 0) goto L_0x0453
            if (r16 == 0) goto L_0x0453
            if (r6 <= 0) goto L_0x0453
            int r7 = r2.intValue()
            java.lang.Object r9 = r5.get(r7)
            com.google.common.collect.ImmutableList r9 = (com.google.common.collect.ImmutableList) r9
            int r8 = r4.intValue()
            java.lang.Object r2 = r5.get(r8)
            com.google.common.collect.ImmutableList r2 = (com.google.common.collect.ImmutableList) r2
            int r0 = r9.size()
            int r6 = java.lang.Math.min(r0, r6)
            int r0 = r9.size()
            if (r6 < r0) goto L_0x0420
            boolean r0 = r9.isEmpty()
            if (r0 != 0) goto L_0x0420
            int r0 = r9.size()
            int r0 = r0 + -1
            java.lang.Object r0 = r9.get(r0)
            boolean r0 = r0 instanceof com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem
            if (r0 == 0) goto L_0x0420
            int r6 = r6 + -1
        L_0x0420:
            if (r6 == 0) goto L_0x0453
            com.google.common.collect.ImmutableList$Builder r4 = com.google.common.collect.ImmutableList.builder()
            com.google.common.collect.ImmutableList$Builder r3 = com.google.common.collect.ImmutableList.builder()
            r4.addAll(r2)
            int r0 = r9.size()
            com.google.common.collect.ImmutableList r0 = r9.subList(r6, r0)
            r3.addAll(r0)
            r2 = 0
        L_0x0439:
            if (r2 >= r6) goto L_0x0445
            java.lang.Object r0 = r9.get(r2)
            r4.add(r0)
            int r2 = r2 + 1
            goto L_0x0439
        L_0x0445:
            com.google.common.collect.ImmutableList r0 = r4.build()
            r5.set(r8, r0)
            com.google.common.collect.ImmutableList r0 = r3.build()
            r5.set(r7, r0)
        L_0x0453:
            com.google.common.collect.ImmutableList$Builder r3 = new com.google.common.collect.ImmutableList$Builder
            r3.<init>()
            java.util.Iterator r2 = r20.iterator()
        L_0x045c:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x04cc
            java.lang.Object r0 = r2.next()
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            r3.addAll(r0)
            goto L_0x045c
        L_0x046c:
            X.19G r5 = r1.A13
            X.101 r2 = r6.A03
            X.0sF r0 = r6.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            boolean r4 = r0.A01
            if (r2 == 0) goto L_0x04be
            X.12V r8 = X.AnonymousClass19G.A03(r2)
        L_0x047c:
            if (r8 != 0) goto L_0x0482
            X.12V r8 = X.AnonymousClass19G.A04(r5)
        L_0x0482:
            X.1lp r7 = new X.1lp
            r7.<init>()
            X.0tV r6 = r5.A05
            X.0tc r10 = r5.A04
            int r3 = X.AnonymousClass1Y3.BAo
            X.0UN r2 = r5.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r2)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x04bb
            com.google.common.base.Predicates$ObjectPredicate r11 = com.google.common.base.Predicates.ObjectPredicate.ALWAYS_FALSE
        L_0x049e:
            r9 = r24
            r6.A01(r7, r8, r9, r10, r11)
            com.google.common.base.Predicates$ObjectPredicate r2 = com.google.common.base.Predicates.ObjectPredicate.ALWAYS_TRUE
            X.AnonymousClass19G.A06(r5, r7, r8, r9, r2)
            if (r4 != 0) goto L_0x04b4
            com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem r2 = new com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem
            X.1ck r0 = r8.A01
            r2.<init>(r0)
            r7.A01(r2)
        L_0x04b4:
            com.google.common.collect.ImmutableList$Builder r0 = r7.A03
            com.google.common.collect.ImmutableList r5 = r0.build()
            goto L_0x04d0
        L_0x04bb:
            com.google.common.base.Predicate r11 = X.AnonymousClass19G.A0B
            goto L_0x049e
        L_0x04be:
            r8 = 0
            goto L_0x047c
        L_0x04c0:
            X.19G r4 = r1.A13
            X.0sF r3 = r6.A06
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r2 = r6.A04
            r0 = 1
            com.google.common.collect.ImmutableList r5 = r4.A08(r3, r2, r0)
            goto L_0x04d0
        L_0x04cc:
            com.google.common.collect.ImmutableList r5 = r3.build()
        L_0x04d0:
            X.0v8 r0 = r1.A0b
            r0.A04(r5)
            X.1GA r6 = r1.A0Q
            r2 = 8
            r0 = 146(0x92, float:2.05E-43)
            java.lang.String r7 = X.C99084oO.$const$string(r0)
            java.lang.String r8 = "filterState"
            java.lang.String r9 = "impressionTracker"
            java.lang.String r10 = "inboxUnitItems"
            java.lang.String r11 = "lithoViewCompatCreators"
            r0 = 550(0x226, float:7.71E-43)
            java.lang.String r12 = X.C99084oO.$const$string(r0)
            r0 = 552(0x228, float:7.74E-43)
            java.lang.String r13 = X.C99084oO.$const$string(r0)
            java.lang.String r14 = "threadListPrefetchExperiment"
            java.lang.String[] r4 = new java.lang.String[]{r7, r8, r9, r10, r11, r12, r13, r14}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.1qz r2 = new X.1qz
            android.content.Context r0 = r6.A09
            r2.<init>(r0)
            r3.clear()
            r2.A08 = r5
            r0 = 3
            r3.set(r0)
            r0 = r37
            r2.A09 = r0
            r0 = 5
            r3.set(r0)
            X.17d r0 = r1.A1g
            r2.A05 = r0
            r0 = 6
            r3.set(r0)
            X.0x5 r0 = r1.A1h
            r2.A06 = r0
            r0 = 4
            r3.set(r0)
            X.0v8 r0 = r1.A0b
            r2.A01 = r0
            r0 = 2
            r3.set(r0)
            X.0vK r0 = r1.A0o
            r2.A03 = r0
            r0 = 7
            r3.set(r0)
            X.0vU r0 = r1.A17
            r2.A07 = r0
            r0 = 0
            r3.set(r0)
            X.0ki r0 = r1.A0g
            r2.A02 = r0
            r0 = 1
            r3.set(r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r1.A0s
            r2.A04 = r0
            r0 = 8
            X.AnonymousClass1CY.A00(r0, r3, r4)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadlist.ThreadListFragment.A07(com.facebook.orca.threadlist.ThreadListFragment, X.0sE, java.lang.Integer):X.0wR");
    }

    private void A08() {
        if (this.A08 != null) {
            A09();
            View view = this.A05;
            if (view != null) {
                this.A08.removeView(view);
                this.A05 = null;
                this.A0h = null;
            }
            if (this.A09.getVisibility() != 0) {
                this.A09.setVisibility(0);
            }
        }
    }

    private void A09() {
        ViewGroup viewGroup;
        View view = this.A06;
        if (view != null && (viewGroup = this.A08) != null) {
            viewGroup.removeView(view);
            this.A06 = null;
        }
    }

    private void A0A() {
        if (!((C05720aD) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Az1, this.A0O)).A08("pbd_auto_mode")) {
            C15750vt r0 = this.A1D;
            if (r0 != null) {
                ((FbFrameLayout) r0.A01()).setVisibility(8);
                return;
            }
            return;
        }
        if (this.A1D == null) {
            this.A1D = C15750vt.A00((ViewStubCompat) A2K(2131297270));
        }
        if (this.A1G == null) {
            this.A1G = new C79923rU(this.A1H, A1j(), (FbFrameLayout) this.A1D.A01());
        }
        if (this.A1C.Aep(C05730aE.A00("auto_free_messenger_snack_bar"), false)) {
            FbSharedPreferences fbSharedPreferences = this.A1C;
            boolean z = false;
            if (fbSharedPreferences.Aep(C05730aE.A0E, false) && fbSharedPreferences.Aep(C05730aE.A0D, false)) {
                z = true;
            }
            if (!z) {
                if (this.A1G != null) {
                    C30281hn edit = this.A1C.edit();
                    edit.putBoolean(C05730aE.A0E, true);
                    edit.commit();
                    this.A1G.A00(true);
                    return;
                }
                return;
            }
        }
        C15750vt r02 = this.A1D;
        if (r02 != null) {
            ((FbFrameLayout) r02.A01()).setVisibility(8);
        }
        AnonymousClass43X.A00(this.A1C);
        C30281hn edit2 = this.A1C.edit();
        edit2.putBoolean(C05730aE.A00("auto_free_messenger_snack_bar"), false);
        edit2.commit();
    }

    private void A0B(ViewGroup viewGroup) {
        if (viewGroup != null && this.A17 != null && this.A06 == null) {
            AnonymousClass0p4 r3 = new AnonymousClass0p4(A1j());
            this.A06 = LithoView.A01(r3, C15560vU.A00(this.A17, r3, this.A0g, this.A0s).A36(), false);
            viewGroup.addView(this.A06, new ViewGroup.LayoutParams(-1, -2));
        }
    }

    public static void A0C(ThreadListFragment threadListFragment) {
        if (threadListFragment.A1i) {
            threadListFragment.A1i = false;
            ((C28461eq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aq4, threadListFragment.A0O)).A06(threadListFragment.A1l);
        }
    }

    public static void A0E(ThreadListFragment threadListFragment) {
        AnonymousClass19S r0 = (AnonymousClass19S) threadListFragment.A1F.A0L;
        threadListFragment.A0R.A0M(r0.A1u(), r0.AZs(), r0.A1t(), r0.A1v(), 0);
    }

    public static void A0F(ThreadListFragment threadListFragment) {
        int i = AnonymousClass1Y3.BBd;
        QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(4, i, threadListFragment.A0O);
        if (quickPerformanceLogger != null && quickPerformanceLogger.isMarkerOn(5505122)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, i, threadListFragment.A0O)).markerEnd(5505122, 2);
        }
    }

    public static void A0G(ThreadListFragment threadListFragment) {
        ImmutableList immutableList = threadListFragment.A14.A09().A02.A00;
        int size = immutableList.size();
        for (int i = 0; i < size; i++) {
            ThreadSummary threadSummary = (ThreadSummary) immutableList.get(i);
            if (ThreadKey.A0E(threadSummary.A0S)) {
                ((C50462e4) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AKa, threadListFragment.A0O)).A0G(threadSummary);
            }
        }
    }

    public static void A0H(ThreadListFragment threadListFragment, C10700ki r8, boolean z) {
        boolean z2;
        if (threadListFragment.A14 != null && threadListFragment.A0g != r8) {
            C10960l9 r3 = (C10960l9) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BOk, threadListFragment.A0O);
            boolean z3 = false;
            if (r3.A05() == AnonymousClass07B.A00 && r3.A07.Aep(C10990lD.A08, false)) {
                z3 = true;
            }
            if (z3) {
                C417626w.A06(new Intent(C60982y9.A03).setData(Uri.parse(StringFormatUtil.formatStrLocaleSafe(C52652jT.A0X, C201109dk.A01.name()))), threadListFragment.A1j());
                return;
            }
            C31371ja r1 = threadListFragment.A14.A0F;
            if (r1.A05 != r8) {
                r1.A05 = r8;
                C31371ja.A0G(r1, false);
                z2 = true;
            } else {
                z2 = false;
            }
            if (z2) {
                if (z) {
                    C21451Ha r6 = (C21451Ha) AnonymousClass1XX.A03(AnonymousClass1Y3.AFw, threadListFragment.A0O);
                    String name = threadListFragment.A0g.name();
                    String name2 = r8.name();
                    C46902Si r32 = C46902Si.A01;
                    C11670nb A012 = C21451Ha.A01("sms_takeover_inbox_filter_action");
                    A012.A0D("old_tab", name);
                    A012.A0D("new_tab", name2);
                    A012.A0D("state_now", C190318tG.A00(r6.A0B()));
                    A012.A0C("position", r32);
                    C21451Ha.A05(r6, A012);
                }
                threadListFragment.A0g = r8;
                A0K(threadListFragment, threadListFragment.A10, threadListFragment.A1J, false);
                threadListFragment.A14.A0C(AnonymousClass1FD.INBOX_FILTER_CHANGE, "inbox_filter_change", "ThreadListFragment");
            }
            AnonymousClass1CV r12 = (AnonymousClass1CV) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AdF, threadListFragment.A0O);
            if (r12.A09 != r8) {
                r12.A09 = r8;
                if (AnonymousClass1CV.A0A(r12)) {
                    r8.toString();
                    r12.A09 = r8;
                    if (r8 == C10700ki.SMS) {
                        AnonymousClass1CV.A08(r12, TriState.NO);
                    } else if (r8 == C10700ki.ALL) {
                        AnonymousClass1CV.A08(r12, TriState.UNSET);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.facebook.resources.ui.FbLinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008c, code lost:
        if (r5 == X.AnonymousClass1GF.A02) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0014, code lost:
        if (r6.A0B != null) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x016a, code lost:
        if (r5 == X.AnonymousClass1GF.A02) goto L_0x016c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01fa  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x02a2  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x02bb  */
    /* JADX WARNING: Removed duplicated region for block: B:168:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0182  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0I(com.facebook.orca.threadlist.ThreadListFragment r9, X.C13880sE r10) {
        /*
            X.16R r6 = r9.A14
            X.0sF r5 = r6.A09()
            boolean r1 = X.AnonymousClass16R.A06(r6)
            boolean r0 = X.AnonymousClass16R.A07(r6, r5)
            if (r1 == 0) goto L_0x009c
            if (r0 == 0) goto L_0x00a2
            X.101 r0 = r6.A0B
            if (r0 == 0) goto L_0x00a2
        L_0x0016:
            X.1GF r5 = X.AnonymousClass1GF.A02
        L_0x0018:
            X.0ki r2 = r9.A0g
            X.0ki r1 = X.C10700ki.ALL
            r0 = 0
            if (r2 != r1) goto L_0x0020
            r0 = 1
        L_0x0020:
            r4 = 0
            if (r0 == 0) goto L_0x0085
            X.1GF r0 = X.AnonymousClass1GF.A05
            if (r5 != r0) goto L_0x0085
            java.util.concurrent.ScheduledFuture r0 = r9.A1P
            if (r0 != 0) goto L_0x0085
            X.16R r6 = r9.A14
            X.17O r0 = r6.A0C
            long r0 = r0.A00
            r2 = 8000(0x1f40, double:3.9525E-320)
            long r0 = r0 + r2
            X.06B r2 = r6.A06
            long r2 = r2.now()
            long r0 = r0 - r2
            java.util.concurrent.ScheduledExecutorService r6 = r9.A1O
            X.69Y r3 = new X.69Y
            r3.<init>(r9, r10)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS
            java.util.concurrent.ScheduledFuture r0 = r6.schedule(r3, r0, r2)
            r9.A1P = r0
        L_0x004a:
            boolean r0 = r9.A1Y
            if (r0 == 0) goto L_0x0163
            java.util.Set r0 = r9.A1M
            if (r0 == 0) goto L_0x0111
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0111
            X.1Ah r0 = r9.A0u
            android.view.View r0 = r0.A00
            if (r0 == 0) goto L_0x0111
            java.util.ArrayList r8 = X.C04300To.A00()
            X.0sF r0 = r10.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r7 = r0.A00
            int r6 = r7.size()
            r3 = 0
        L_0x006d:
            if (r3 >= r6) goto L_0x00dd
            java.lang.Object r2 = r7.get(r3)
            com.facebook.messaging.model.threads.ThreadSummary r2 = (com.facebook.messaging.model.threads.ThreadSummary) r2
            java.util.Set r1 = r9.A1M
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A0S
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0082
            r8.add(r2)
        L_0x0082:
            int r3 = r3 + 1
            goto L_0x006d
        L_0x0085:
            X.1GF r0 = X.AnonymousClass1GF.A01
            if (r5 == r0) goto L_0x008e
            X.1GF r1 = X.AnonymousClass1GF.A02
            r0 = 0
            if (r5 != r1) goto L_0x008f
        L_0x008e:
            r0 = 1
        L_0x008f:
            if (r0 == 0) goto L_0x004a
            java.util.concurrent.ScheduledFuture r0 = r9.A1P
            if (r0 == 0) goto L_0x004a
            r0.cancel(r4)
            r0 = 0
            r9.A1P = r0
            goto L_0x004a
        L_0x009c:
            if (r0 == 0) goto L_0x00a2
            X.1GF r5 = X.AnonymousClass1GF.A01
            goto L_0x0018
        L_0x00a2:
            r8 = 0
            X.101 r0 = r6.A0B
            r7 = 1
            if (r0 != 0) goto L_0x00c4
            X.17O r0 = r6.A0C
            X.1G0 r1 = r0.A00
            r0 = 0
            if (r1 == 0) goto L_0x00b0
            r0 = 1
        L_0x00b0:
            if (r0 == 0) goto L_0x00c4
            X.06B r0 = r6.A06
            long r3 = r0.now()
            X.17O r0 = r6.A0C
            long r0 = r0.A00
            long r3 = r3 - r0
            r1 = 8000(0x1f40, double:3.9525E-320)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c4
            r8 = 1
        L_0x00c4:
            boolean r0 = X.AnonymousClass16R.A07(r6, r5)
            r0 = r0 ^ r7
            if (r8 == 0) goto L_0x00d1
            if (r0 == 0) goto L_0x00d1
            X.1GF r5 = X.AnonymousClass1GF.A03
            goto L_0x0018
        L_0x00d1:
            if (r0 == 0) goto L_0x00d7
            X.1GF r5 = X.AnonymousClass1GF.A04
            goto L_0x0018
        L_0x00d7:
            if (r8 == 0) goto L_0x0016
            X.1GF r5 = X.AnonymousClass1GF.A05
            goto L_0x0018
        L_0x00dd:
            boolean r0 = r8.isEmpty()
            if (r0 != 0) goto L_0x010c
            boolean r0 = A0M(r9)
            if (r0 == 0) goto L_0x010c
            X.1Ah r6 = r9.A0u
            java.util.Iterator r3 = r8.iterator()
            r2 = 0
        L_0x00f0:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0107
            java.lang.Object r1 = r3.next()
            boolean r0 = r6.A0O(r1)
            if (r0 == 0) goto L_0x00f0
            r2 = 1
            X.1Aj r0 = r6.A01
            r0.add(r1)
            goto L_0x00f0
        L_0x0107:
            if (r2 == 0) goto L_0x010c
            X.C19951Ai.A00(r6)
        L_0x010c:
            java.util.Set r0 = r9.A1M
            r0.clear()
        L_0x0111:
            X.0sE r0 = r9.A10
            r2 = 1
            if (r0 == 0) goto L_0x0122
            boolean r0 = r9.A0N(r0)
            if (r0 == 0) goto L_0x01c8
            boolean r0 = r9.A0N(r10)
            if (r0 != 0) goto L_0x01c8
        L_0x0122:
            r1 = 1
        L_0x0123:
            java.lang.Integer r0 = r9.A1J
            r1 = r1 ^ r2
            A0K(r9, r10, r0, r1)
            X.10R r0 = r9.A0a
            X.1Yd r2 = r0.A00
            r0 = 282325382333758(0x100c60020053e, double:1.39487272360105E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0151
            int r0 = r9.A02
            if (r0 > 0) goto L_0x0151
            com.facebook.widget.recyclerview.BetterRecyclerView r0 = r9.A1F
            X.19T r0 = r0.A0L
            X.19S r0 = (X.AnonymousClass19S) r0
            if (r0 == 0) goto L_0x0151
            int r1 = r0.A1v()
            int r0 = r0.A1t()
            int r1 = r1 - r0
            int r0 = r1 + -2
            r9.A02 = r0
        L_0x0151:
            r2 = 12
            int r1 = X.AnonymousClass1Y3.Anp
            X.0UN r0 = r9.A0O
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1GS r1 = (X.AnonymousClass1GS) r1
            X.0sF r0 = r10.A06
            r1.A00 = r0
            r9.A1Y = r4
        L_0x0163:
            X.1GF r0 = X.AnonymousClass1GF.A01
            if (r5 == r0) goto L_0x016c
            X.1GF r1 = X.AnonymousClass1GF.A02
            r0 = 0
            if (r5 != r1) goto L_0x016d
        L_0x016c:
            r0 = 1
        L_0x016d:
            if (r0 != 0) goto L_0x01a9
            java.lang.Integer r0 = X.AnonymousClass07B.A01
        L_0x0171:
            X.0fv r8 = r9.A0V
            int r7 = r0.intValue()
            switch(r7) {
                case 1: goto L_0x01a6;
                case 2: goto L_0x01a3;
                case 3: goto L_0x01a0;
                default: goto L_0x017a;
            }
        L_0x017a:
            java.lang.String r6 = "EMPTY"
        L_0x017c:
            int[] r5 = X.C08770fv.A0T
            int r4 = r5.length
            r3 = 0
        L_0x0180:
            if (r3 >= r4) goto L_0x01cb
            r2 = r5[r3]
            boolean r0 = X.C004004z.A02(r2)
            if (r0 == 0) goto L_0x019d
            com.facebook.quicklog.QuickPerformanceLogger r1 = r8.A05
            java.util.concurrent.atomic.AtomicInteger r0 = r8.A0P
            int r0 = r0.incrementAndGet()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r6, r0)
            r1.markerPoint(r2, r0)
        L_0x019d:
            int r3 = r3 + 1
            goto L_0x0180
        L_0x01a0:
            java.lang.String r6 = "ERROR"
            goto L_0x017c
        L_0x01a3:
            java.lang.String r6 = "LOADED"
            goto L_0x017c
        L_0x01a6:
            java.lang.String r6 = "LOADING"
            goto L_0x017c
        L_0x01a9:
            X.0sF r0 = r10.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r0 = r0.A00
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x01c5
            X.2o6 r0 = r9.A0n
            if (r0 == 0) goto L_0x01bc
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            goto L_0x0171
        L_0x01bc:
            boolean r0 = r9.A0N(r10)
            if (r0 == 0) goto L_0x01c5
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            goto L_0x0171
        L_0x01c5:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            goto L_0x0171
        L_0x01c8:
            r1 = 0
            goto L_0x0123
        L_0x01cb:
            r4 = 1
            r2 = 0
            r3 = 8
            switch(r7) {
                case 0: goto L_0x02bb;
                case 1: goto L_0x0243;
                case 2: goto L_0x02a2;
                case 3: goto L_0x0263;
                default: goto L_0x01d2;
            }
        L_0x01d2:
            A0J(r9, r10)
            boolean r0 = r9.A0N(r10)
            if (r0 != 0) goto L_0x01e9
            X.0sF r0 = r10.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            r0.A02()
            X.0sF r0 = r10.A05
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            r0.A02()
        L_0x01e9:
            r9.A10 = r10
            X.10R r0 = r9.A0a
            X.1Yd r2 = r0.A00
            r0 = 282325382333758(0x100c60020053e, double:1.39487272360105E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0355
            int r0 = r9.A02
            if (r0 <= 0) goto L_0x0355
            X.0sE r0 = r9.A10
            X.0sF r0 = r0.A06
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            com.google.common.collect.ImmutableList r5 = r0.A00
            X.10Q r4 = r9.A0m
            int r0 = r5.size()
            int r1 = r9.A02
            if (r0 < r1) goto L_0x0215
            r0 = 0
            com.google.common.collect.ImmutableList r5 = r5.subList(r0, r1)
        L_0x0215:
            java.util.Map r0 = r4.A00
            r0.clear()
            r3 = 0
        L_0x021b:
            int r0 = r5.size()
            if (r3 >= r0) goto L_0x0355
            java.lang.Object r0 = r5.get(r3)
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r0.A0S
            if (r1 == 0) goto L_0x0240
            boolean r0 = r1.A0N()
            if (r0 == 0) goto L_0x0240
            java.util.Map r2 = r4.A00
            long r0 = r1.A01
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            r2.put(r1, r0)
        L_0x0240:
            int r3 = r3 + 1
            goto L_0x021b
        L_0x0243:
            android.view.ViewGroup r0 = r9.A07
            r0.setVisibility(r3)
            com.facebook.widget.listview.EmptyListViewItem r0 = r9.A1E
            r0.setVisibility(r2)
            com.facebook.widget.listview.EmptyListViewItem r0 = r9.A1E
            r0.A0I(r4)
            r9.A08()
            android.view.ViewGroup r0 = r9.A08
            r9.A0B(r0)
            X.0vO r1 = r9.A0W
            java.lang.String r0 = "android_messenger_thread_list_state_loading"
            r1.A02(r0)
            goto L_0x01d2
        L_0x0263:
            android.view.ViewGroup r0 = r9.A07
            r0.setVisibility(r3)
            com.facebook.widget.listview.EmptyListViewItem r0 = r9.A1E
            r0.setVisibility(r2)
            int r1 = X.AnonymousClass1Y3.ANl
            X.0UN r0 = r9.A0O
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.2p3 r1 = (X.C55972p3) r1
            X.2o6 r0 = r9.A0n
            com.facebook.fbservice.service.ServiceException r0 = r0.A00
            java.lang.String r1 = r1.A01(r0, r2, r4)
            boolean r0 = X.C06850cB.A0B(r1)
            if (r0 == 0) goto L_0x0287
            java.lang.String r1 = ""
        L_0x0287:
            com.facebook.widget.listview.EmptyListViewItem r0 = r9.A1E
            r0.A0H(r1)
            com.facebook.widget.listview.EmptyListViewItem r0 = r9.A1E
            r0.A0I(r2)
            r9.A08()
            android.view.ViewGroup r0 = r9.A08
            r9.A0B(r0)
            X.0vO r1 = r9.A0W
            java.lang.String r0 = "android_messenger_thread_list_state_error"
            r1.A02(r0)
            goto L_0x01d2
        L_0x02a2:
            com.facebook.widget.listview.EmptyListViewItem r0 = r9.A1E
            r0.setVisibility(r3)
            r9.A09()
            r9.A08()
            android.view.ViewGroup r0 = r9.A07
            r0.setVisibility(r2)
            X.0vO r1 = r9.A0W
            java.lang.String r0 = "android_messenger_thread_list_state_loaded"
            r1.A02(r0)
            goto L_0x01d2
        L_0x02bb:
            com.facebook.widget.listview.EmptyListViewItem r0 = r9.A1E
            r0.setVisibility(r3)
            r9.A09()
            X.1Ti r1 = r9.A0U
            java.lang.String r0 = "thread_list"
            r1.A06(r0)
            android.view.View r0 = r9.A05
            if (r0 != 0) goto L_0x0347
            android.view.ViewGroup r0 = r9.A08
            if (r0 == 0) goto L_0x0347
            com.facebook.resources.ui.FbLinearLayout r4 = new com.facebook.resources.ui.FbLinearLayout
            android.content.Context r0 = r9.A03
            r4.<init>(r0)
            r0 = 1
            r4.setOrientation(r0)
            android.content.Context r0 = r9.A03
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132411637(0x7f1a04f5, float:2.0472685E38)
            android.view.View r0 = r1.inflate(r0, r4, r2)
            com.facebook.messaging.neue.nullstate.RecentsTabEmptyView r0 = (com.facebook.messaging.neue.nullstate.RecentsTabEmptyView) r0
            r9.A0h = r0
            r9.A0B(r4)
            com.facebook.messaging.neue.nullstate.RecentsTabEmptyView r0 = r9.A0h
            r4.addView(r0)
            android.view.ViewGroup$LayoutParams r1 = new android.view.ViewGroup$LayoutParams
            r0 = -1
            r1.<init>(r0, r0)
            android.view.ViewGroup r0 = r9.A08
            r0.addView(r4, r2, r1)
            com.facebook.messaging.neue.nullstate.RecentsTabEmptyView r2 = r9.A0h
            X.15a r1 = r9.A19
            r2.A03 = r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r9.A0s
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r2.A02
            boolean r0 = com.google.common.base.Objects.equal(r0, r1)
            if (r0 != 0) goto L_0x0316
            r2.A02 = r1
            com.facebook.messaging.neue.nullstate.RecentsTabEmptyView.A02(r2)
        L_0x0316:
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Azw
            X.0UN r0 = r9.A0O
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.4WX r0 = (X.AnonymousClass4WX) r0
            X.1ZE r2 = r0.A00
            X.0Xc r1 = X.C04970Xc.A02
            java.lang.String r0 = "recents_tab_null_state_shown"
            X.0bW r2 = r2.A02(r0, r1)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r1 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 516(0x204, float:7.23E-43)
            r1.<init>(r2, r0)
            boolean r0 = r1.A0G()
            if (r0 == 0) goto L_0x0340
            java.lang.String r0 = "thread_list"
            r1.A0O(r0)
            r1.A06()
        L_0x0340:
            r9.A05 = r4
            X.1Ed r1 = r9.A09
            r1.setVisibility(r3)
        L_0x0347:
            android.view.ViewGroup r0 = r9.A07
            r0.setVisibility(r3)
            X.0vO r1 = r9.A0W
            java.lang.String r0 = "android_messenger_thread_list_state_empty"
            r1.A02(r0)
            goto L_0x01d2
        L_0x0355:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadlist.ThreadListFragment.A0I(com.facebook.orca.threadlist.ThreadListFragment, X.0sE):void");
    }

    public static void A0J(ThreadListFragment threadListFragment, C13880sE r3) {
        Integer num;
        if (threadListFragment.A1U) {
            num = AnonymousClass07B.A00;
        } else {
            boolean z = false;
            if (threadListFragment.A14.A0F.A01 != null) {
                z = true;
            }
            if (z) {
                num = AnonymousClass07B.A0C;
            } else {
                num = AnonymousClass07B.A01;
            }
        }
        threadListFragment.A1J = num;
        A0K(threadListFragment, r3, num, true);
    }

    public static void A0K(ThreadListFragment threadListFragment, C13880sE r4, Integer num, boolean z) {
        if (threadListFragment.A0Q == null) {
            return;
        }
        if (z) {
            AnonymousClass07A.A04((ScheduledExecutorService) AnonymousClass1XX.A02(30, AnonymousClass1Y3.A1P, threadListFragment.A0O), new C20071Au(threadListFragment, r4, num), -229942484);
        } else {
            threadListFragment.A0R.A0N(A07(threadListFragment, r4, num));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0107, code lost:
        if (r5.A02.A09() != false) goto L_0x0109;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x010a, code lost:
        if (r0 != false) goto L_0x010c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0L(java.lang.String r14) {
        /*
            r13 = this;
            X.10S r0 = r13.A0i
            com.facebook.prefs.shared.FbSharedPreferences r1 = r0.A00
            X.1Y8 r0 = X.C05690aA.A1k
            r6 = 0
            boolean r0 = r1.Aep(r0, r6)
            if (r0 == 0) goto L_0x002a
            X.10S r4 = r13.A0i
            android.content.Context r3 = r13.A1j()
            com.facebook.prefs.shared.FbSharedPreferences r0 = r4.A00
            X.1hn r2 = r0.edit()
            X.1Y8 r1 = X.C05690aA.A1k
            r2.putBoolean(r1, r6)
            r2.commit()
            if (r3 == 0) goto L_0x002a
            X.10T r1 = r4.A01
            java.lang.String r0 = "831403647209504"
            r1.A02(r0, r3)
        L_0x002a:
            X.10D r8 = r13.A0x
            if (r8 == 0) goto L_0x011e
            android.content.Context r1 = r8.A00
            java.lang.String r0 = "Init must be called first"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            X.00z r1 = r8.A04
            X.00z r0 = X.C001500z.A07
            if (r1 != r0) goto L_0x011e
            X.0Tq r0 = r8.A06
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x011e
            boolean r0 = X.C006006f.A01()
            r7 = 1
            if (r0 != 0) goto L_0x01b0
            android.content.Context r1 = r8.A00
            java.lang.Class<android.app.Service> r0 = android.app.Service.class
            java.lang.Object r1 = X.AnonymousClass065.A00(r1, r0)
            r0 = 0
            if (r1 == 0) goto L_0x005c
            r0 = 1
        L_0x005c:
            if (r0 != 0) goto L_0x01b0
            int r1 = X.AnonymousClass1Y3.A6c
            X.0UN r0 = r8.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.1kO r0 = (X.C31791kO) r0
            boolean r0 = r0.A02()
            if (r0 != 0) goto L_0x01b0
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AVJ
            X.0UN r0 = r8.A01
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Ek r5 = (X.C20941Ek) r5
            X.1El r0 = r5.A03
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x01ac
            java.lang.Integer r0 = X.AnonymousClass07B.A01
        L_0x0083:
            boolean r0 = X.C20941Ek.A01(r5, r0)
            if (r0 != 0) goto L_0x0109
            com.facebook.prefs.shared.FbSharedPreferences r1 = r5.A04
            X.1Y7 r0 = X.C10990lD.A0F
            boolean r0 = r1.Aep(r0, r6)
            r12 = 1
            if (r0 != 0) goto L_0x00fe
            com.facebook.prefs.shared.FbSharedPreferences r1 = r5.A04
            X.1Y7 r0 = X.C10990lD.A0H
            boolean r0 = r1.Aep(r0, r6)
            if (r0 != 0) goto L_0x01a9
            X.0lA r0 = r5.A01
            X.1YI r3 = r0.A00
            r1 = 264(0x108, float:3.7E-43)
            boolean r0 = r3.AbO(r1, r6)
            if (r0 != 0) goto L_0x01a9
            X.1El r0 = r5.A03
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x01a9
            X.0l9 r0 = r5.A02
            boolean r0 = r0.A0A()
            if (r0 == 0) goto L_0x01a9
            X.0l9 r0 = r5.A02
            boolean r0 = r0.A08()
            if (r0 != 0) goto L_0x01a9
            com.facebook.prefs.shared.FbSharedPreferences r1 = r5.A04
            X.1Y7 r0 = X.C10990lD.A0V
            boolean r0 = r1.Aep(r0, r6)
            if (r0 != 0) goto L_0x01a9
            com.facebook.prefs.shared.FbSharedPreferences r1 = r5.A04
            X.1Y7 r0 = X.C10990lD.A00
            boolean r0 = r1.Aep(r0, r6)
            if (r0 != 0) goto L_0x00fe
            X.06B r0 = r5.A00
            long r10 = r0.now()
            com.facebook.prefs.shared.FbSharedPreferences r3 = r5.A04
            X.1Y7 r2 = X.C10990lD.A0E
            r0 = 0
            long r2 = r3.At2(r2, r0)
            com.facebook.prefs.shared.FbSharedPreferences r9 = r5.A04
            X.1Y7 r4 = X.C10990lD.A0R
            long r0 = r9.At2(r4, r0)
            long r0 = java.lang.Math.max(r2, r0)
            long r10 = r10 - r0
            long r3 = java.lang.Math.abs(r10)
            r1 = 259200000(0xf731400, double:1.280618154E-315)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x01a9
        L_0x00fe:
            if (r12 != 0) goto L_0x0109
            X.0l9 r0 = r5.A02
            boolean r1 = r0.A09()
            r0 = 0
            if (r1 == 0) goto L_0x010a
        L_0x0109:
            r0 = 1
        L_0x010a:
            if (r0 == 0) goto L_0x01b0
        L_0x010c:
            if (r7 == 0) goto L_0x011e
            X.0WP r4 = r8.A03
            X.69p r3 = new X.69p
            r3.<init>(r8)
            X.0XV r2 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            java.lang.String r0 = "showSmsInterstitial"
            r4.CIG(r0, r3, r2, r1)
        L_0x011e:
            boolean r0 = r13.A1X
            if (r0 == 0) goto L_0x0127
            A0G(r13)
            r13.A1X = r6
        L_0x0127:
            int r2 = X.AnonymousClass1Y3.B4T
            X.0UN r1 = r13.A0O
            r0 = 10
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1En r5 = (X.C20971En) r5
            if (r5 == 0) goto L_0x0156
            X.1mY r4 = new X.1mY
            r4.<init>(r5, r14)
            java.util.concurrent.ScheduledFuture r1 = r5.A01
            if (r1 == 0) goto L_0x0141
            r1.cancel(r6)
        L_0x0141:
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BKX
            X.0UN r0 = r5.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.util.concurrent.ScheduledExecutorService r3 = (java.util.concurrent.ScheduledExecutorService) r3
            r1 = 3
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            java.util.concurrent.ScheduledFuture r0 = r3.schedule(r4, r1, r0)
            r5.A01 = r0
        L_0x0156:
            int r2 = X.AnonymousClass1Y3.A88
            X.0UN r1 = r13.A0O
            r0 = 11
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1mZ r5 = (X.C32931mZ) r5
            if (r5 == 0) goto L_0x01a5
            X.16R r0 = r13.A14
            X.0l8 r3 = r0.A0E
            r4 = r3
            boolean r0 = r5.A01
            if (r0 != 0) goto L_0x01a5
            if (r3 == 0) goto L_0x01a5
            int r1 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.1bq r0 = (X.C26681bq) r0
            int r2 = X.AnonymousClass1Y3.ATy
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            com.facebook.messaging.model.folders.FolderCounts r0 = r0.A0N(r3)
            if (r0 == 0) goto L_0x01a5
            int r0 = r0.A01
            if (r0 == 0) goto L_0x01a5
            r2 = 1
            r5.A01 = r2
            int r1 = X.AnonymousClass1Y3.APr
            X.0UN r0 = r5.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Y6 r3 = (X.AnonymousClass1Y6) r3
            X.5kD r2 = new X.5kD
            r2.<init>(r5, r4)
            r0 = 2000(0x7d0, double:9.88E-321)
            r3.BxS(r2, r0)
        L_0x01a5:
            r13.A0A()
            return
        L_0x01a9:
            r12 = 0
            goto L_0x00fe
        L_0x01ac:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            goto L_0x0083
        L_0x01b0:
            r7 = 0
            goto L_0x010c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadlist.ThreadListFragment.A0L(java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
        if (r0.A0b == false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0M(com.facebook.orca.threadlist.ThreadListFragment r1) {
        /*
            boolean r0 = r1.A0i
            if (r0 == 0) goto L_0x0017
            boolean r0 = r1.A1a()
            if (r0 == 0) goto L_0x0017
            androidx.fragment.app.Fragment r0 = r1.A0L
            if (r0 == 0) goto L_0x0013
            boolean r0 = r0.A0b
            r1 = 1
            if (r0 != 0) goto L_0x0014
        L_0x0013:
            r1 = 0
        L_0x0014:
            r0 = 1
            if (r1 == 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadlist.ThreadListFragment.A0M(com.facebook.orca.threadlist.ThreadListFragment):boolean");
    }

    private boolean A0N(C13880sE r4) {
        if (this.A1I.booleanValue()) {
            return false;
        }
        if (this.A0g == C10700ki.SMS) {
            return r4.A06.A02.A00.isEmpty();
        }
        if (!r4.A06.A02.A00.isEmpty() || !C013509w.A02(r4.A00.A00) || r4.A04 != null) {
            return false;
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.coordinatorlayout.widget.CoordinatorLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void A1v(View view, Bundle bundle) {
        Drawable drawable;
        Bundle bundle2 = bundle;
        super.A1v(view, bundle2);
        C005505z.A03("ThreadListFragment.onViewCreated", 1767597255);
        try {
            C005505z.A03("ThreadListFragment.onViewCreatedFindViews", 2049081259);
            try {
                this.A08 = (ViewGroup) A2K(2131301075);
                this.A07 = (ViewGroup) A2K(2131298406);
                C15750vt A002 = C15750vt.A00((ViewStubCompat) A2K(2131300042));
                C15750vt A003 = C15750vt.A00((ViewStubCompat) A2K(2131298670));
                AnonymousClass10D r8 = this.A0x;
                C32901mW r7 = new C32901mW(this);
                MigColorScheme migColorScheme = this.A0s;
                Preconditions.checkNotNull(A003);
                Preconditions.checkNotNull(r7);
                C16430x3 r0 = (C16430x3) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AZ7, r8.A01);
                r0.A07 = A002;
                r0.A04 = migColorScheme;
                C32541lv r1 = r8.A05;
                r1.A01 = A003;
                r1.A00 = new C16440x4(r8);
                r8.A02 = r7;
                r8.A00 = A002.A02().getContext();
                A0A();
                this.A1F = (BetterRecyclerView) A2K(2131301008);
                this.A09 = (C20871Ed) A2K(2131301074);
                C27814DjR djR = this.A0p;
                if (djR != null) {
                    Context A1j2 = A1j();
                    MigColorScheme migColorScheme2 = this.A0s;
                    ExtendedFloatingActionButton extendedFloatingActionButton = (ExtendedFloatingActionButton) LayoutInflater.from(A1j2).inflate(2132410791, (ViewGroup) djR.A01, true).findViewById(2131297889);
                    djR.A03 = extendedFloatingActionButton;
                    extendedFloatingActionButton.setTypeface(AnonymousClass10M.ROBOTO_MEDIUM.A00(A1j2));
                    ExtendedFloatingActionButton extendedFloatingActionButton2 = djR.A03;
                    int A032 = ((AnonymousClass1MT) AnonymousClass1XX.A03(AnonymousClass1Y3.AJa, djR.A02)).A03(C29631gj.A1g, AnonymousClass07B.A0N);
                    if (A032 != 0) {
                        drawable = C27717Dhl.A01(extendedFloatingActionButton2.getContext(), A032);
                    } else {
                        drawable = null;
                    }
                    if (extendedFloatingActionButton2.A02 != drawable) {
                        extendedFloatingActionButton2.A02 = drawable;
                        C27811DjO.A03(extendedFloatingActionButton2, true);
                    }
                    djR.A03.setOnClickListener(djR.A00);
                    C46062Op r12 = (C46062Op) djR.A03.getLayoutParams();
                    r12.A01(new ThreadListExtendedFloatingActionButtonAgent$AutoSizeAdjustOnScrollBehavior());
                    djR.A03.setLayoutParams(r12);
                    djR.A01(migColorScheme2);
                }
                C005505z.A00(-1419448038);
                this.A1h = new C16450x5();
                if (new C16470x7(this.A0P, 5000).A00()) {
                    this.A0Q = new AnonymousClass1GA(this.A03, "messenger_thread_list", (C637038i) AnonymousClass1XX.A03(AnonymousClass1Y3.Aua, this.A0O), null);
                } else {
                    this.A0Q = new AnonymousClass1GA(this.A03);
                }
                AnonymousClass1F9 r13 = new AnonymousClass1F9();
                r13.A0K = false;
                r13.A07 = new AnonymousClass19M(1, false);
                r13.A06 = new C35241qu(this);
                r13.A0A = new C193217r(this);
                AnonymousClass1Ri A004 = r13.A00(this.A0Q);
                this.A0T = A004;
                AnonymousClass1AB r72 = new AnonymousClass1AB(A004, false);
                this.A0S = r72;
                AnonymousClass1AD r14 = new AnonymousClass1AD(this.A0Q, r72);
                String name = getClass().getName();
                if (name == null) {
                    name = BuildConfig.FLAVOR;
                }
                r14.A01 = name;
                this.A0R = new AnonymousClass1AC(r14);
                this.A0w = new C196118y(this.A09);
                if (this.A0a.A00.Aem(282325382792517L)) {
                    AnonymousClass1TR r02 = (AnonymousClass1TR) AnonymousClass1XX.A02(26, AnonymousClass1Y3.AgT, this.A0O);
                    AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ASK, r02.A00), new C144516o1(r02), 206428684);
                }
                C196118y r15 = this.A0w;
                if (r15 != null) {
                    r15.A00(this.A0s);
                }
                this.A0w.A00 = new C194318c(this);
                C005505z.A03("ThreadListFragment.onViewCreatedRecyclerView", 1113548898);
                BetterRecyclerView betterRecyclerView = this.A1F;
                betterRecyclerView.A06 = new C194618g(this, (C197919r) AnonymousClass1XX.A03(AnonymousClass1Y3.BBD, this.A0O));
                betterRecyclerView.A0V = true;
                betterRecyclerView.A0k(0);
                if (!(!((AnonymousClass1YI) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AcD, this.A0O)).AbO(AnonymousClass1Y3.A27, false))) {
                    this.A1F.A0x(null);
                } else {
                    C32491lq r16 = new C32491lq();
                    r16.A00 = false;
                    this.A1F.A0x(r16);
                }
                C005505z.A00(523076272);
                this.A1F.A12(new C195918w(this));
                this.A1F.A12(new C16480x8(this));
                C20181Bf r73 = this.A0f;
                C20191Bg r17 = new C20191Bg();
                r17.A02 = "MESSENGER_INBOX2";
                r17.A00 = AnonymousClass07B.A00;
                C32871mT A005 = r73.A00(new InboxSourceLoggingData(r17));
                this.A0e = A005;
                this.A1e = new AnonymousClass1AY(new AnonymousClass0X5(this.A0d, AnonymousClass0X6.A1O), A005);
                C005505z.A03("ThreadListFragment.onViewCreatedInbox2Views", 450011201);
                try {
                    this.A0b = this.A0c.A00(this.A1e, this.A0e);
                    C005505z.A00(1500680655);
                    this.A1F.A1J(true);
                    this.A1F.setOverScrollMode(2);
                    AnonymousClass106 r03 = this.A0v;
                    C19941Ah r74 = new C19941Ah(this.A1F, AnonymousClass0UX.A0j(r03), AnonymousClass0WY.A01(r03));
                    this.A0u = r74;
                    C15320v6.setAccessibilityDelegate(this.A1F, r74);
                    this.A0u.A00 = this.A1F;
                    C005505z.A03("ThreadListFragment.onViewCreatedListenerSetup", -51561097);
                } catch (Throwable th) {
                    th = th;
                    C005505z.A00(1647595970);
                    throw th;
                }
                try {
                    C195618t r04 = this.A1F.A0K;
                    synchronized (r04.A00) {
                        r04.A00.clear();
                    }
                    this.A1F.A1G(new AnonymousClass1AV(this));
                    this.A1F.A12(new AnonymousClass1AP(this));
                    this.A1E = (EmptyListViewItem) A2K(2131301007);
                    this.A0G.A02(this.A0I, "messenger_thread_list", this);
                    C005505z.A00(1403088620);
                    C13880sE r18 = this.A14.A0H;
                    if (r18 == null) {
                        r18 = C13880sE.A07;
                    }
                    this.A1J = AnonymousClass07B.A01;
                    this.A10 = r18;
                    this.A1f = new C20061At(new AnonymousClass1AQ(this));
                    if (this.A17 == null) {
                        AnonymousClass1AR r2 = new AnonymousClass1AR(this);
                        CallerContext callerContext = A1m;
                        C20961Em r75 = new C20961Em((AnonymousClass1B1) AnonymousClass1XX.A03(AnonymousClass1Y3.AfG, this.A0O), callerContext, A1j(), this.A0P);
                        AnonymousClass1B2 r11 = new AnonymousClass1B2((C20101Ax) AnonymousClass1XX.A03(AnonymousClass1Y3.AgA, this.A0O), callerContext, A2J(), A1j(), this.A0P, (C20121Az) AnonymousClass1XX.A03(AnonymousClass1Y3.A9G, this.A0O));
                        this.A12 = r11;
                        r11.A07 = new C20081Av(this);
                        Context context = this.A03;
                        C13060qW A172 = A17();
                        AnonymousClass16R r05 = this.A14;
                        AnonymousClass17O r122 = r05.A0C;
                        C32871mT r10 = this.A0e;
                        AnonymousClass1B2 r19 = this.A12;
                        AnonymousClass1B7 r142 = new AnonymousClass1B7((AnonymousClass1B0) AnonymousClass1XX.A03(AnonymousClass1Y3.BK0, this.A0O), context, A172, r122, (AnonymousClass17S) AnonymousClass1XX.A02(21, AnonymousClass1Y3.BSW, r05.A08), r10, r19, r75, new AnonymousClass1B9(this, (AnonymousClass1BX) AnonymousClass1XX.A03(AnonymousClass1Y3.Aoo, this.A0O)));
                        this.A0l = r142;
                        this.A17 = this.A18.A00(this.A03, r142, this.A0P, new C20031Aq(this), r2, this.A0q, this.A1f);
                        if (!(bundle == null || bundle2.getSerializable("m4ThreadTypeFilter") == null)) {
                            A0H(this, (C10700ki) bundle2.getSerializable("m4ThreadTypeFilter"), false);
                        }
                    }
                    A0K(this, this.A10, this.A1J, false);
                    AnonymousClass1AB r06 = this.A0S;
                    r06.A00.BLE(this.A1F);
                    AnonymousClass1CV r5 = (AnonymousClass1CV) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AdF, this.A0O);
                    C15750vt A006 = C15750vt.A00((ViewStubCompat) A2K(2131301241));
                    BetterRecyclerView betterRecyclerView2 = this.A1F;
                    AnonymousClass1Ri r82 = this.A0T;
                    MigColorScheme migColorScheme3 = this.A0s;
                    C20061At r4 = this.A1f;
                    C20051As r3 = new C20051As(this);
                    if (((C35321r2) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Avj, r5.A07)).A00.Aem(282282430694678L)) {
                        r5.A0D = betterRecyclerView2;
                        r5.A08 = r82;
                        r5.A0A = migColorScheme3;
                        r5.A0C = r4;
                        r5.A0B = r3;
                        r5.A05 = A006.A01();
                        ((FbSharedPreferences) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B6q, r5.A07)).C0f(C05690aA.A18, r5.A0I);
                        if (AnonymousClass1CV.A0A(r5)) {
                            r5.A0E = ((AnonymousClass0WP) AnonymousClass1XX.A02(7, AnonymousClass1Y3.A8a, r5.A07)).CIG("Load Unreal pill feature", new C71193by(r5), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY, AnonymousClass07B.A00);
                        }
                    }
                    this.A1F.A12(this.A0y);
                    C005505z.A00(935227795);
                } catch (Throwable th2) {
                    th = th2;
                    C005505z.A00(-141303815);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                C005505z.A00(517495004);
                throw th;
            }
        } catch (Throwable th4) {
            C005505z.A00(-775484039);
            throw th4;
        }
    }

    public void Aay(AnonymousClass18d r2) {
        r2.A00(24);
    }

    public String Act() {
        return A1m.A0F();
    }

    public boolean BPB() {
        if (this.A0g != C10700ki.SMS) {
            return false;
        }
        A0H(this, C10700ki.ALL, false);
        return true;
    }

    public void CAE(boolean z) {
        C13990sQ r0 = this.A1j;
        r0.A02 = z;
        C13990sQ.A00(r0);
    }

    public static long A05(ImmutableList immutableList, int i) {
        if (immutableList.size() > i) {
            return ((ThreadSummary) immutableList.get(i - 1)).A0B;
        }
        if (immutableList.isEmpty()) {
            return 0;
        }
        return ((ThreadSummary) immutableList.get(immutableList.size() - 1)).A0B;
    }

    public static long A06(ImmutableList immutableList, int i, int i2, long j) {
        if (!immutableList.isEmpty()) {
            Preconditions.checkArgument(!immutableList.isEmpty());
            int A042 = A04(immutableList, j, 0, immutableList.size() - 1);
            if (A042 < i) {
                return A05(immutableList, i);
            }
            if (A042 >= i2) {
                return A05(immutableList, i2);
            }
            if (A042 == immutableList.size() - 1) {
                if (immutableList.isEmpty()) {
                    return 0;
                }
                return ((ThreadSummary) immutableList.get(immutableList.size() - 1)).A0B;
            }
        }
        return j;
    }

    public static void A0D(ThreadListFragment threadListFragment) {
        if (A0M(threadListFragment)) {
            C45702Nd r0 = threadListFragment.A0U.A02.A00;
            if (r0 != null) {
                r0.A02("thread_list");
            }
            C13890sF A092 = threadListFragment.A14.A09();
            if (A092 != null) {
                threadListFragment.A0U.A08("data_fetch_disposition", A092.A01);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view;
        ViewGroup.LayoutParams layoutParams;
        int A022 = C000700l.A02(-1286745713);
        this.A0s = (MigColorScheme) AnonymousClass1XX.A02(19, AnonymousClass1Y3.Anh, this.A0O);
        this.A0V.A0C(5505045, A00(), "onCreateView");
        C10920l5 r7 = (C10920l5) AnonymousClass1XX.A03(AnonymousClass1Y3.B4R, this.A0O);
        LayoutInflater cloneInContext = layoutInflater.cloneInContext(this.A03);
        C13860sC r9 = (C13860sC) this.A0j.A00.get(r7);
        if (r9 != null) {
            if (r9.A05 <= r9.A06) {
                if (r9.A05 == r9.A01.length) {
                    r9.A00.A03("android.messenger.preinflater.preinflates_exhausted");
                } else {
                    r9.A00.A03("android.messenger.preinflater.view_not_ready");
                }
            }
            int i = 0;
            while (true) {
                View[] viewArr = r9.A01;
                if (i >= viewArr.length) {
                    view = null;
                    break;
                }
                view = viewArr[i];
                if (view != null) {
                    viewArr[i] = null;
                    r9.A06++;
                    r9.A00.A03("android.messenger.preinflater.preinflated_view_used");
                    break;
                }
                i++;
            }
            if (!(view == null || viewGroup == null || (layoutParams = r7.A02) == null)) {
                view.setLayoutParams(layoutParams);
            }
        } else {
            view = null;
        }
        if (view == null) {
            C005505z.A05("normal_inflation for %s", r7.A04, -1984640447);
            try {
                view = cloneInContext.inflate(r7.A00, viewGroup, false);
            } finally {
                C005505z.A00(-1933104228);
            }
        }
        this.A0V.A0D(5505045, A00(), 45);
        C27814DjR djR = this.A0p;
        if (djR != null) {
            CoordinatorLayout coordinatorLayout = new CoordinatorLayout(view.getContext());
            djR.A01 = coordinatorLayout;
            coordinatorLayout.addView(view);
            CoordinatorLayout coordinatorLayout2 = djR.A01;
            C000700l.A08(-504020050, A022);
            return coordinatorLayout2;
        }
        C000700l.A08(-894475634, A022);
        return view;
    }

    public void A1l() {
        int A022 = C000700l.A02(653514323);
        super.A1l();
        C67023Mq r0 = this.A0r;
        if (r0 != null) {
            r0.A01();
        }
        this.A0z.A03();
        C14420tH r1 = this.A0X;
        r1.A01.AOz();
        r1.A04 = null;
        this.A1b = false;
        AnonymousClass16R r7 = this.A14;
        if (r7.A0F != null) {
            AnonymousClass16R.A01(r7);
            r7.A0F.ARp();
        }
        C31371ja r02 = r7.A0U;
        if (r02 != null) {
            r02.ARp();
        }
        AnonymousClass1TC r03 = (AnonymousClass1TC) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AEP, r7.A08);
        if (r03 != null) {
            r03.ARp();
        }
        AnonymousClass1TF r04 = (AnonymousClass1TF) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Atk, r7.A08);
        if (r04 != null) {
            r04.ARp();
        }
        if (r7.A0C != null) {
            AnonymousClass16R.A00(r7);
            r7.A0C.ARp();
        }
        AnonymousClass17S r05 = (AnonymousClass17S) AnonymousClass1XX.A02(21, AnonymousClass1Y3.BSW, r7.A08);
        if (r05 != null) {
            r05.ARp();
        }
        AnonymousClass0tR r06 = (AnonymousClass0tR) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ASr, r7.A08);
        r06.A00 = null;
        r06.A02.A01();
        r7.A0X.clear();
        AnonymousClass1G0 r07 = r7.A04;
        if (r07 != null) {
            r07.A01(true);
            r7.A04 = null;
        }
        AnonymousClass7KN r5 = r7.A0J;
        if (r5 != null) {
            ((C72243dw) AnonymousClass1XX.A02(27, AnonymousClass1Y3.AL1, r7.A08)).unsubscribe(r5);
            r7.A0J = null;
        }
        ((C28461eq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aq4, this.A0O)).A06(this.A1l);
        ((C13980sP) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AXL, this.A0O)).A02(this.A1k);
        if (((C195518s) AnonymousClass1XX.A02(25, AnonymousClass1Y3.AQC, this.A0O)).A01()) {
            ((C194518f) AnonymousClass1XX.A02(24, AnonymousClass1Y3.Ayd, this.A0O)).A04(this);
        }
        this.A0V.A04 = null;
        AnonymousClass2QE r3 = (AnonymousClass2QE) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AYi, ((C33891oJ) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AoV, this.A0O)).A00);
        synchronized (r3.A03) {
            try {
                r3.A03.clear();
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (r3.A02) {
            C69053Vl r08 = (C69053Vl) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A5K, r3.A00);
            synchronized (r08.A01) {
                try {
                    r08.A01.clear();
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        C000700l.A08(384075358, A022);
    }

    public void A1m() {
        int A022 = C000700l.A02(1283462764);
        this.A0w = null;
        AnonymousClass1CV r4 = (AnonymousClass1CV) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AdF, this.A0O);
        Future future = r4.A0E;
        if (future != null) {
            future.cancel(true);
            r4.A0E = null;
        }
        AnonymousClass1CV.A04(r4);
        ((FbSharedPreferences) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B6q, r4.A07)).CJr(C05690aA.A18, r4.A0I);
        r4.A0B = null;
        C27814DjR djR = this.A0p;
        if (djR != null) {
            djR.A03 = null;
            djR.A01 = null;
        }
        super.A1m();
        C000700l.A08(18086095, A022);
    }

    public void A1o() {
        int A022 = C000700l.A02(-228304656);
        this.A1V = false;
        this.A16.A00 = false;
        this.A0M.disable();
        this.A0j.A01();
        super.A1o();
        C67023Mq r0 = this.A0r;
        if (r0 != null) {
            r0.A01();
        }
        C08770fv.A02(this.A0V);
        C19941Ah r1 = this.A0u;
        ScheduledFuture scheduledFuture = r1.A02;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
        r1.A01.clear();
        r1.A00 = null;
        C15340v8 r02 = this.A0b;
        if (r02 != null) {
            r02.A05(false);
            this.A0b.A06(false);
        }
        AnonymousClass16R r3 = this.A14;
        r3.A0A.A06(false);
        ((AnonymousClass1G2) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Aj7, r3.A08)).A05(false);
        if (r3.A0R.A00.Aem(282325383054663L)) {
            C88684Lp r4 = (C88684Lp) AnonymousClass1XX.A02(33, AnonymousClass1Y3.A6A, r3.A08);
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ap7, r4.A05)).A01("messenger_active_now_and_stories_inventory"), AnonymousClass1Y3.A2s);
            if (uSLEBaseShape0S0000000.A0G()) {
                synchronized (r4.A06) {
                    try {
                        uSLEBaseShape0S0000000.A0C("number_of_inbox_active_now_item", Long.valueOf((long) r4.A00));
                        uSLEBaseShape0S0000000.A0C("number_of_inbox_story_item", Long.valueOf((long) r4.A02));
                        uSLEBaseShape0S0000000.A0C("number_of_inbox_story_and_active_now_item", Long.valueOf((long) r4.A01));
                        uSLEBaseShape0S0000000.A0C("number_of_people_tab_active_now_item", Long.valueOf((long) r4.A03));
                        uSLEBaseShape0S0000000.A0C("number_of_people_tab_story_item", Long.valueOf((long) r4.A04));
                        uSLEBaseShape0S0000000.A06();
                        r4.A04 = 0;
                        r4.A03 = 0;
                        r4.A00 = 0;
                        r4.A01 = 0;
                        r4.A02 = 0;
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
            }
        }
        int i = AnonymousClass1Y3.AOb;
        AnonymousClass0UN r2 = this.A0O;
        C19911Ae r12 = (C19911Ae) AnonymousClass1XX.A02(20, i, r2);
        r12.A01.A09();
        r12.A00.A09();
        C33891oJ r32 = (C33891oJ) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AoV, r2);
        if (r32.A02 != null) {
            r32.A05 = RegularImmutableSet.A05;
            ((C04480Uv) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BPt, r32.A00)).A01(r32.A02);
        }
        synchronized (r32.A03) {
            try {
                r32.A03.clear();
            } catch (Throwable th2) {
                while (true) {
                    th = th2;
                }
            }
        }
        C20971En r33 = (C20971En) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B4T, this.A0O);
        ScheduledFuture scheduledFuture2 = r33.A01;
        if (scheduledFuture2 != null) {
            scheduledFuture2.cancel(false);
            ((C191016u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AXk, r33.A00)).ASY("ThreadListFragment.onPause");
            r33.A01 = null;
        }
        AnonymousClass1CV r13 = (AnonymousClass1CV) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AdF, this.A0O);
        r13.A03 = -1;
        r13.A02 = -1;
        this.A0l.BC7();
        this.A0E.A04(new Intent(C06680bu.A0u));
        A0F(this);
        if (AnonymousClass10R.A01(this.A0a, true)) {
            ((C74163hN) AnonymousClass1XX.A02(27, AnonymousClass1Y3.AoY, this.A0O)).A05();
        }
        C000700l.A08(845428362, A022);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x012c, code lost:
        if (r1 == false) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0130, code lost:
        if (r0 != false) goto L_0x0132;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0101  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1p() {
        /*
            r12 = this;
            r0 = -1083801977(0xffffffffbf667e87, float:-0.90036815)
            int r3 = X.C000700l.A02(r0)
            super.A1p()
            X.10R r1 = r12.A0a
            r0 = 1
            boolean r0 = X.AnonymousClass10R.A01(r1, r0)
            if (r0 == 0) goto L_0x0039
            X.10R r0 = r12.A0a
            X.1Yd r2 = r0.A00
            r0 = 282325384627539(0x100c600430553, double:1.394872734933834E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 != 0) goto L_0x0039
            r2 = 29
            int r1 = X.AnonymousClass1Y3.ASK
            X.0UN r0 = r12.A0O
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0VL r2 = (X.AnonymousClass0VL) r2
            X.21S r1 = new X.21S
            r1.<init>(r12)
            r0 = 1859833030(0x6edaccc6, float:3.3857644E28)
            X.AnonymousClass07A.A04(r2, r1, r0)
        L_0x0039:
            java.lang.String r4 = "ThreadListFragment.onResume"
            java.lang.String r7 = "ThreadListFragment"
            r2 = 1
            r12.A1V = r2
            X.0vF r0 = r12.A16
            r0.A00 = r2
            X.16N r1 = r12.A11
            int r0 = r1.A00
            int r0 = r0 + 1
            r1.A00 = r0
            boolean r0 = A0M(r12)
            if (r0 == 0) goto L_0x0055
            r12.A0L(r4)
        L_0x0055:
            X.0ki r1 = r12.A0g
            X.0ki r0 = X.C10700ki.SMS
            r4 = 0
            if (r1 != r0) goto L_0x0075
            r5 = 8
            int r1 = X.AnonymousClass1Y3.BOk
            X.0UN r0 = r12.A0O
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0l9 r0 = (X.C10960l9) r0
            java.lang.Integer r1 = r0.A05()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x0075
            X.0ki r0 = X.C10700ki.ALL
            A0H(r12, r0, r4)
        L_0x0075:
            A0D(r12)
            X.0w7 r0 = r12.A0Y
            r0.A07()
            com.facebook.widget.recyclerview.BetterRecyclerView r1 = r12.A1F
            X.1Ah r0 = r12.A0u
            X.C15320v6.setAccessibilityDelegate(r1, r0)
            X.1Ah r1 = r12.A0u
            com.facebook.widget.recyclerview.BetterRecyclerView r0 = r12.A1F
            r1.A00 = r0
            X.10D r10 = r12.A0x
            boolean r9 = A0M(r12)
            android.content.Context r1 = r10.A00
            java.lang.String r0 = "Init must be called first"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            X.00z r1 = r10.A04
            X.00z r0 = X.C001500z.A07
            if (r1 != r0) goto L_0x0111
            X.0Tq r0 = r10.A06
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0111
            com.facebook.interstitial.triggers.InterstitialTrigger r1 = new com.facebook.interstitial.triggers.InterstitialTrigger
            com.facebook.interstitial.triggers.InterstitialTrigger$Action r0 = com.facebook.interstitial.triggers.InterstitialTrigger.Action.A3v
            r1.<init>(r0)
            com.facebook.interstitial.triggers.InterstitialTrigger r6 = X.AnonymousClass10D.A00(r10, r1)
            int r5 = X.AnonymousClass1Y3.BE8
            X.0UN r1 = r10.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r5, r1)
            X.1F1 r0 = (X.AnonymousClass1F1) r0
            X.1mO r11 = r0.A0M(r6)
            if (r11 == 0) goto L_0x02b0
            boolean r0 = r11 instanceof X.C22281Ks
            if (r0 == 0) goto L_0x02b0
            X.1Ks r11 = (X.C22281Ks) r11
            java.lang.String r1 = r11.Aqc()
            java.lang.String r0 = "4563"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x02b0
            r5 = 5
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r10.A01
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.1Yd r5 = (X.C25051Yd) r5
            r0 = 282548718536352(0x100fa000006a0, double:1.395976151052793E-309)
            boolean r0 = r5.Aem(r0)
            if (r0 == 0) goto L_0x02b0
            X.0WP r8 = r10.A03
            X.69n r6 = new X.69n
            r6.<init>(r10, r11)
            X.0XV r5 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            java.lang.String r0 = "launchBlockingInterstitial"
            r8.CIG(r0, r6, r5, r1)
            r0 = 1
        L_0x00ff:
            if (r0 != 0) goto L_0x0111
            X.0WP r8 = r10.A03
            X.1Er r6 = new X.1Er
            r6.<init>(r10, r9)
            X.0XV r5 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            java.lang.String r0 = "maybeShowPromotionHeader"
            r8.CIG(r0, r6, r5, r1)
        L_0x0111:
            X.0fv r0 = r12.A0V
            int r6 = r12.A00()
            r5 = 5505045(0x540015, float:7.714211E-39)
            com.facebook.quicklog.QuickPerformanceLogger r1 = r0.A05
            r0 = 42
            r1.markerEnd(r5, r6, r0)
            boolean r0 = r12.A0i
            if (r0 == 0) goto L_0x0132
            androidx.fragment.app.Fragment r0 = r12.A0L
            if (r0 == 0) goto L_0x012e
            boolean r1 = r0.A0b
            r0 = 1
            if (r1 != 0) goto L_0x012f
        L_0x012e:
            r0 = 0
        L_0x012f:
            r6 = 1
            if (r0 == 0) goto L_0x0133
        L_0x0132:
            r6 = 0
        L_0x0133:
            X.0v8 r0 = r12.A0b
            if (r0 == 0) goto L_0x013f
            r0.A05(r2)
            X.0v8 r0 = r12.A0b
            r0.A06(r6)
        L_0x013f:
            r5 = 23
            int r1 = X.AnonymousClass1Y3.BMw
            X.0UN r0 = r12.A0O
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.1Ei r0 = (X.C20921Ei) r0
            r0.A07(r2, r2)
            X.0fv r0 = r12.A0V
            boolean r0 = r0.A0M()
            if (r0 != 0) goto L_0x015e
            X.0fv r0 = r12.A0V
            boolean r0 = r0.A0N()
            if (r0 == 0) goto L_0x017b
        L_0x015e:
            int r1 = X.AnonymousClass1Y3.Aq4
            X.0UN r0 = r12.A0O
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1eq r1 = (X.C28461eq) r1
            java.lang.Object r0 = r12.A1l
            r1.A07(r0)
            r12.A1i = r2
            X.1Y6 r4 = r12.A0H
            X.1r4 r2 = new X.1r4
            r2.<init>(r12)
            r0 = 2000(0x7d0, double:9.88E-321)
            r4.BxS(r2, r0)
        L_0x017b:
            X.16R r5 = r12.A14
            android.view.ViewGroup r8 = r12.A08
            X.0vF r0 = r12.A16
            boolean r2 = r0.A01
            int r4 = X.AnonymousClass1Y3.AEk
            X.0UN r1 = r5.A08
            r0 = 32
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r4, r1)
            X.0uR r0 = (X.C14970uR) r0
            X.1Yd r4 = r0.A00
            r0 = 282325384103247(0x100c6003b054f, double:1.394872732343487E-309)
            boolean r0 = r4.Aem(r0)
            if (r0 == 0) goto L_0x01b3
            r4 = 16
            int r1 = X.AnonymousClass1Y3.ASK
            X.0UN r0 = r5.A08
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0VL r4 = (X.AnonymousClass0VL) r4
            X.5qY r1 = new X.5qY
            r1.<init>(r5)
            r0 = -1411300572(0xffffffffabe14324, float:-1.6005847E-12)
            X.AnonymousClass07A.A04(r4, r1, r0)
        L_0x01b3:
            X.16S r0 = r5.A0A
            r4 = 1
            r0.A06(r4)
            X.16S r1 = r5.A0A
            boolean r0 = r1.A09
            if (r6 != r0) goto L_0x02a6
            if (r6 != 0) goto L_0x02a6
        L_0x01c1:
            X.0sF r1 = r5.A09()
            X.0sF r0 = X.C13890sF.A03
            java.lang.String r9 = "onResume"
            r6 = 0
            if (r1 == r0) goto L_0x024c
            if (r2 != 0) goto L_0x024c
            X.1qC r0 = r5.A09
            if (r0 == 0) goto L_0x01e1
            r2 = 17
            int r1 = X.AnonymousClass1Y3.BMw
            X.0UN r0 = r5.A08
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Ei r0 = (X.C20921Ei) r0
            r0.A04()
        L_0x01e1:
            X.1Y6 r2 = r5.A02
            X.1rF r1 = new X.1rF
            r1.<init>(r5)
            java.lang.Void[] r0 = new java.lang.Void[r6]
            r2.AZ9(r1, r0)
            r5.A0D(r6)
            r2 = 7
            int r1 = X.AnonymousClass1Y3.Aj7
            X.0UN r0 = r5.A08
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1G2 r0 = (X.AnonymousClass1G2) r0
            r0.A05(r4)
            X.0vF r1 = r12.A16
            r1.A01 = r6
            r2 = 14
            int r1 = X.AnonymousClass1Y3.AoV
            X.0UN r0 = r12.A0O
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1oJ r0 = (X.C33891oJ) r0
            android.content.BroadcastReceiver r4 = r0.A02
            if (r4 == 0) goto L_0x0227
            r2 = 5
            int r1 = X.AnonymousClass1Y3.BPt
            X.0UN r0 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0Uv r2 = (X.C04480Uv) r2
            android.content.IntentFilter r1 = new android.content.IntentFilter
            java.lang.String r0 = X.C06680bu.A0u
            r1.<init>(r0)
            r2.A02(r4, r1)
        L_0x0227:
            X.16N r2 = r12.A11
            int r0 = r2.A00
            int r0 = r0 + -1
            r2.A00 = r0
            if (r0 >= 0) goto L_0x0238
            java.lang.Class r1 = X.AnonymousClass16N.A09
            java.lang.String r0 = "[TL] Suspend count < 0"
            X.C010708t.A06(r1, r0)
        L_0x0238:
            X.AnonymousClass16N.A01(r2)
            X.16N r1 = r12.A11
            r1.A04 = r6
            X.AnonymousClass16N.A01(r1)
            r12.A0A()
            r0 = -846462239(0xffffffffcd8c02e1, float:-2.93624864E8)
            X.C000700l.A08(r0, r3)
            return
        L_0x024c:
            r2 = 18
            int r1 = X.AnonymousClass1Y3.B9x
            X.0UN r0 = r5.A08
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0fv r1 = (X.C08770fv) r1
            java.lang.String r0 = "inbox_data_loading"
            r1.A0F(r0)
            r5.A0b = r6
            int r1 = X.AnonymousClass1Y3.Aq4
            X.0UN r0 = r5.A08
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.1eq r1 = (X.C28461eq) r1
            java.lang.Object r0 = r5.A0W
            r1.A07(r0)
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r5.A08
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 163(0xa3, float:2.28E-43)
            boolean r0 = r1.AbO(r0, r4)
            if (r0 == 0) goto L_0x0292
            r5.A0a = r6
            int r1 = X.AnonymousClass1Y3.Aq4
            X.0UN r0 = r5.A08
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.1eq r1 = (X.C28461eq) r1
            java.lang.Object r0 = r5.A0V
            r1.A07(r0)
        L_0x0292:
            X.1r7 r2 = new X.1r7
            r2.<init>(r5)
            r0 = 2000(0x7d0, double:9.88E-321)
            r8.postDelayed(r2, r0)
            X.1FD r0 = X.AnonymousClass1FD.REFRESH_AFTER_RESUME
            r5.A0C(r0, r9, r7)
            r5.A0E(r6)
            goto L_0x01e1
        L_0x02a6:
            r1.A09 = r6
            X.AnonymousClass16S.A02(r1)
            X.AnonymousClass16S.A01(r1)
            goto L_0x01c1
        L_0x02b0:
            r0 = 0
            goto L_0x00ff
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadlist.ThreadListFragment.A1p():void");
    }

    public void A1q() {
        int A022 = C000700l.A02(688483605);
        this.A0V.A0C(5505045, A00(), "onStart");
        super.A1q();
        AnonymousClass1DW r1 = (AnonymousClass1DW) BzI(AnonymousClass1DW.class);
        if (r1 != null) {
            r1.CCZ(2131833830);
            r1.CBV();
        }
        this.A0V.A0D(5505045, A00(), 47);
        C000700l.A08(-1946752967, A022);
    }

    public void A1s(Context context) {
        super.A1s(context);
        if (this.A03 == null) {
            A2S(A1j());
        }
        C34861qI r3 = (C34861qI) AnonymousClass1XX.A03(AnonymousClass1Y3.AzU, this.A0O);
        if (((C34881qK) AnonymousClass1XX.A02(22, AnonymousClass1Y3.BUW, this.A0O)).A01(28) && r3.A01() && this.A0p == null) {
            this.A0p = (C27814DjR) AnonymousClass1XX.A03(AnonymousClass1Y3.AEq, this.A0O);
        }
    }

    public void A1t(Bundle bundle) {
        int A022 = C000700l.A02(-939586297);
        super.A1t(bundle);
        A1T(true);
        this.A0V.A0D(5505045, A00(), 46);
        C000700l.A08(-1129703321, A022);
    }

    public void A1u(Bundle bundle) {
        super.A1u(bundle);
        bundle.putSerializable("m4ThreadTypeFilter", this.A0g);
    }

    public void A1w(Fragment fragment) {
        super.A1w(fragment);
        if (fragment instanceof InboxAdsPostClickFragment) {
            ((InboxAdsPostClickFragment) fragment).A03 = new AnonymousClass23O(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
        if (r1 == false) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        if (r0 != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2H(boolean r6, boolean r7) {
        /*
            r5 = this;
            super.A2H(r6, r7)
            boolean r0 = A0M(r5)
            if (r6 == 0) goto L_0x0012
            if (r7 != 0) goto L_0x0012
            if (r0 == 0) goto L_0x0012
            java.lang.String r0 = "ThreadListFragment.setUserVisibleHint"
            r5.A0L(r0)
        L_0x0012:
            A0D(r5)
            if (r6 == 0) goto L_0x0024
            androidx.fragment.app.Fragment r0 = r5.A0L
            if (r0 == 0) goto L_0x0020
            boolean r1 = r0.A0b
            r0 = 1
            if (r1 != 0) goto L_0x0021
        L_0x0020:
            r0 = 0
        L_0x0021:
            r2 = 1
            if (r0 == 0) goto L_0x0025
        L_0x0024:
            r2 = 0
        L_0x0025:
            X.0v8 r0 = r5.A0b
            if (r0 == 0) goto L_0x002c
            r0.A06(r2)
        L_0x002c:
            X.16R r3 = r5.A14
            if (r3 == 0) goto L_0x004b
            X.16S r1 = r3.A0A
            boolean r0 = r1.A09
            if (r2 != r0) goto L_0x00cd
            if (r2 != 0) goto L_0x00cd
        L_0x0038:
            X.1qC r0 = r3.A09
            if (r0 == 0) goto L_0x004b
            r2 = 17
            int r1 = X.AnonymousClass1Y3.BMw
            X.0UN r0 = r3.A08
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Ei r0 = (X.C20921Ei) r0
            r0.A04()
        L_0x004b:
            if (r6 != 0) goto L_0x0076
            A0F(r5)
            X.0vU r1 = r5.A17
            if (r1 == 0) goto L_0x005e
            X.1BY r0 = r1.A06
            com.google.common.base.Preconditions.checkNotNull(r0)
            X.1BY r0 = r1.A06
            r0.A01()
        L_0x005e:
            int r2 = X.AnonymousClass1Y3.AOb
            X.0UN r1 = r5.A0O
            r0 = 20
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Ae r1 = (X.C19911Ae) r1
            if (r1 == 0) goto L_0x0076
            X.0zB r0 = r1.A01
            r0.A09()
            X.0zB r0 = r1.A00
            r0.A09()
        L_0x0076:
            if (r6 == 0) goto L_0x0088
            X.0WP r4 = r5.A0F
            X.1r3 r3 = new X.1r3
            r3.<init>(r5)
            X.0XV r2 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            java.lang.String r0 = "maybeShowUnreadThreadsPill"
            r4.CIG(r0, r3, r2, r1)
        L_0x0088:
            if (r6 != 0) goto L_0x00cc
            if (r7 == 0) goto L_0x00cc
            X.0qW r2 = r5.A0P
            if (r2 == 0) goto L_0x00cc
            java.lang.String r0 = "ignore_messages_dialog_fragment"
            androidx.fragment.app.Fragment r1 = r2.A0Q(r0)
            boolean r0 = r1 instanceof X.C12590pe
            if (r0 == 0) goto L_0x009f
            X.0pe r1 = (X.C12590pe) r1
            r1.A21()
        L_0x009f:
            java.lang.String r0 = "deleteThreadDialog"
            androidx.fragment.app.Fragment r1 = r2.A0Q(r0)
            boolean r0 = r1 instanceof X.C12590pe
            if (r0 == 0) goto L_0x00ae
            X.0pe r1 = (X.C12590pe) r1
            r1.A21()
        L_0x00ae:
            java.lang.String r0 = "notificationSettingsDialog"
            androidx.fragment.app.Fragment r1 = r2.A0Q(r0)
            boolean r0 = r1 instanceof X.C12590pe
            if (r0 == 0) goto L_0x00bd
            X.0pe r1 = (X.C12590pe) r1
            r1.A21()
        L_0x00bd:
            java.lang.String r0 = "InboxAdsPostClickFragment"
            androidx.fragment.app.Fragment r1 = r2.A0Q(r0)
            boolean r0 = r1 instanceof X.C12590pe
            if (r0 == 0) goto L_0x00cc
            X.0pe r1 = (X.C12590pe) r1
            r1.A21()
        L_0x00cc:
            return
        L_0x00cd:
            r1.A09 = r2
            X.AnonymousClass16S.A02(r1)
            X.AnonymousClass16S.A01(r1)
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.orca.threadlist.ThreadListFragment.A2H(boolean, boolean):void");
    }

    public void A2M(Bundle bundle) {
        C192117g r7;
        super.A2M(bundle);
        AnonymousClass07c.disableComponentHostPool = this.A0t.Aem(2306129062625548358L);
        AnonymousClass07c.inheritPriorityFromUiThread = this.A0t.Aem(285035504670053L);
        AnonymousClass16N r1 = this.A11;
        r1.A04 = true;
        r1.A01 = new C13930sJ(this);
        super.A1W(false);
        AnonymousClass16R r6 = new AnonymousClass16R(this.A15, new C187815b(this));
        this.A14 = r6;
        AnonymousClass07A.A02((ExecutorService) AnonymousClass1XX.A02(28, AnonymousClass1Y3.BKH, r6.A08), new AnonymousClass17P(r6), 873076763);
        r6.A0F.C6Z(new AnonymousClass17Q(r6));
        r6.A0C.C6Z(new AnonymousClass17W(r6));
        int i = AnonymousClass1Y3.BSW;
        AnonymousClass0UN r2 = r6.A08;
        ((AnonymousClass17S) AnonymousClass1XX.A02(21, i, r2)).C6Z(new AnonymousClass1FC(r6));
        r6.A0A.A04 = new AnonymousClass1G3(r6);
        AnonymousClass0tR r12 = (AnonymousClass0tR) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ASr, r2);
        r12.A00 = new C35211qr(r6);
        r12.A02.A00();
        int i2 = AnonymousClass1Y3.BAo;
        AnonymousClass0UN r22 = r6.A08;
        if (((Boolean) AnonymousClass1XX.A02(25, i2, r22)).booleanValue()) {
            r6.A0U.C6Z(new AnonymousClass24E(r6));
            if (((C21441Gz) AnonymousClass1XX.A02(26, AnonymousClass1Y3.BDi, r22)).A01.booleanValue()) {
                AnonymousClass1XX.A03(AnonymousClass1Y3.BPX, r22);
                C410023t r3 = new C410023t();
                r6.A0J = r3;
                ((C72243dw) AnonymousClass1XX.A02(27, AnonymousClass1Y3.AL1, r6.A08)).subscribe(r3);
            }
        }
        boolean z = false;
        if (((int) ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C35221qs) AnonymousClass1XX.A02(34, AnonymousClass1Y3.AP0, r6.A08)).A00)).At0(564058055180978L)) > 0) {
            z = true;
        }
        r6.A0O = z;
        this.A1g = new C188115e(this);
        this.A1M = C25011Xz.A03();
        this.A0V.A04 = new AnonymousClass17Y(this);
        if (Build.VERSION.SDK_INT >= 16) {
            r7 = new C192017f(new C192217h((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, this.A0O), 5505221, "android_messenger_thread_list_scroll_perf"), this.A03, false);
        } else {
            r7 = new AnonymousClass23D();
        }
        this.A0M = r7;
        this.A0K = this.A0L.A00(5505081);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, this.A0O)).markerEnd(5505072, 2);
        this.A0V.A0C(5505045, A00(), "onFragmentCreate");
        this.A0V.A0D(5505045, A00(), 44);
        C14420tH r23 = this.A0X;
        C35231qt r13 = new C35231qt(this);
        r23.A01.AOz();
        r23.A04 = r13;
        C33741o4 r14 = this.A0z;
        r14.A03 = new C193017p(this);
        r14.A02();
        if (Build.VERSION.SDK_INT <= 19) {
            C013709y[] r15 = AnonymousClass08D.A02;
            if (r15[1] == null) {
                r15[1] = new C013709y();
            }
        }
        ((C13980sP) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AXL, this.A0O)).A01(this.A1k);
        if (((C195518s) AnonymousClass1XX.A02(25, AnonymousClass1Y3.AQC, this.A0O)).A01()) {
            ((C194518f) AnonymousClass1XX.A02(24, AnonymousClass1Y3.Ayd, this.A0O)).A03(this);
        }
        this.A1a = this.A0t.Aem(284975375193413L);
        this.A1Z = this.A0t.Aem(284983964996938L);
        this.A1W = this.A0t.Aem(284983965062475L);
    }

    public void A2S(Context context) {
        this.A03 = C13820s8.A00().CNV(context);
        C005505z.A03("ThreadListFragment.injectMe", 1191319236);
        try {
            AnonymousClass1XX r1 = AnonymousClass1XX.get(this.A03);
            this.A0O = new AnonymousClass0UN(31, r1);
            this.A0C = AnonymousClass1ZD.A00(r1);
            this.A0F = C25091Yh.A00(r1);
            this.A0H = AnonymousClass0UX.A07(r1);
            this.A0A = C06920cI.A00(r1);
            this.A0G = C14080sa.A01(r1);
            this.A04 = C04490Ux.A0Q(r1);
            this.A0N = C30111hV.A00(r1);
            this.A0Y = C15870w7.A00(r1);
            this.A0Z = C16700xd.A00(r1);
            this.A0z = new C33741o4(r1);
            this.A0B = AnonymousClass144.A00(r1);
            C11680nc.A00(r1);
            this.A0E = C04490Ux.A0n(r1);
            this.A0U = C24361Ti.A01(r1);
            this.A0k = C14160si.A00(r1);
            this.A0J = AnonymousClass0UU.A05(r1);
            this.A1Q = AnonymousClass0VG.A00(AnonymousClass1Y3.AlH, r1);
            this.A1R = AnonymousClass0VG.A00(AnonymousClass1Y3.AXO, r1);
            this.A1S = AnonymousClass0VG.A00(AnonymousClass1Y3.BKW, r1);
            this.A1N = AnonymousClass0UX.A0U(r1);
            this.A1O = AnonymousClass0UX.A0e(r1);
            this.A0V = C08770fv.A00(r1);
            this.A1L = AnonymousClass0W9.A01();
            this.A0D = C14220so.A00(r1);
            this.A0X = C14420tH.A00(r1);
            this.A13 = AnonymousClass19B.A00(r1);
            this.A0f = new C20181Bf(r1);
            this.A0v = new AnonymousClass106(r1);
            this.A11 = new AnonymousClass16N(C06460bX.A00(r1));
            this.A0j = C22431Lh.A00(r1);
            this.A0x = new AnonymousClass10D(r1);
            this.A0m = AnonymousClass10Q.A00(r1);
            this.A0a = AnonymousClass10R.A00(r1);
            this.A0i = new AnonymousClass10S(r1);
            this.A1H = new AnonymousClass10U(r1);
            this.A1C = FbSharedPreferencesModule.A00(r1);
            this.A0c = new C15330v7(r1);
            this.A15 = new AnonymousClass1K9(r1);
            this.A16 = new C15410vF();
            this.A0I = C06460bX.A00(r1);
            this.A1I = AnonymousClass0UU.A08(r1);
            this.A1T = AnonymousClass0VG.A00(AnonymousClass1Y3.ALj, r1);
            this.A0P = new C15420vG(r1);
            this.A0d = new C15430vH(r1);
            this.A0L = new C15450vJ(r1);
            this.A18 = new C15540vS(r1);
            this.A0t = AnonymousClass0WT.A00(r1);
            this.A0o = new C15460vK(r1);
            this.A0q = new AnonymousClass1BY();
            this.A0y = new C15490vN(r1);
            this.A0W = new C15500vO(r1);
            C14990uX.A00(r1);
        } finally {
            C005505z.A00(409030893);
        }
    }

    public void Aaz(AnonymousClass2W7 r4) {
        if (r4.Aax() == 24) {
            C005505z.A03("ThreadListFragment.onRefreshMontageBusEvent", -1478627908);
            try {
                C15410vF r2 = this.A16;
                boolean z = true;
                if (!r2.A00) {
                    r2.A01 = true;
                    z = false;
                }
                if (z) {
                    this.A14.A0B(AnonymousClass1FD.AUTOMATIC_REFRESH, "MontageOmnistoreInitializedBusEvent");
                }
            } finally {
                C005505z.A00(-13053434);
            }
        }
    }
}
