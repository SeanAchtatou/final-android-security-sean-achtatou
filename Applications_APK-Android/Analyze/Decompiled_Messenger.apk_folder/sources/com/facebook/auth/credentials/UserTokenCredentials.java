package com.facebook.auth.credentials;

import X.AnonymousClass08S;
import X.C32951mb;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import java.util.Arrays;

public final class UserTokenCredentials implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C32951mb();
    public final String A00;
    public final String A01;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof UserTokenCredentials) {
                UserTokenCredentials userTokenCredentials = (UserTokenCredentials) obj;
                if (!Objects.equal(this.A00, userTokenCredentials.A00) || !Objects.equal(this.A01, userTokenCredentials.A01)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01});
    }

    public String toString() {
        return AnonymousClass08S.A0P("UserTokenCredentials{userId='", this.A00, "'}");
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }

    public UserTokenCredentials(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }
}
