package com.facebook.auth.viewercontext;

import X.C05580Zo;
import X.C25181Yq;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Preconditions;

public final class ViewerContext implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C25181Yq();
    public final String A00;
    public final String mAuthToken;
    public final boolean mIsDittoContext;
    public final boolean mIsFoxContext;
    public final boolean mIsPageContext;
    public final boolean mIsTimelineViewAsContext;
    public final String mSessionCookiesString;
    public final String mSessionKey;
    public final String mSessionSecret;
    public final String mUserId;
    public final String mUsername;

    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0034, code lost:
        if (r1.equals(r5.mUserId) == false) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x00a4
            r2 = 0
            if (r5 == 0) goto L_0x0036
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x0036
            com.facebook.auth.viewercontext.ViewerContext r5 = (com.facebook.auth.viewercontext.ViewerContext) r5
            boolean r1 = r4.mIsPageContext
            boolean r0 = r5.mIsPageContext
            if (r1 != r0) goto L_0x0036
            boolean r1 = r4.mIsFoxContext
            boolean r0 = r5.mIsFoxContext
            if (r1 != r0) goto L_0x0036
            boolean r1 = r4.mIsDittoContext
            boolean r0 = r5.mIsDittoContext
            if (r1 != r0) goto L_0x0036
            boolean r1 = r4.mIsTimelineViewAsContext
            boolean r0 = r5.mIsTimelineViewAsContext
            if (r1 != r0) goto L_0x0036
            java.lang.String r1 = r4.mUserId
            if (r1 == 0) goto L_0x0037
            java.lang.String r0 = r5.mUserId
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x003c
        L_0x0036:
            return r2
        L_0x0037:
            java.lang.String r0 = r5.mUserId
            if (r0 == 0) goto L_0x003c
            return r2
        L_0x003c:
            java.lang.String r1 = r4.mAuthToken
            if (r1 == 0) goto L_0x0049
            java.lang.String r0 = r5.mAuthToken
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x004e
            return r2
        L_0x0049:
            java.lang.String r0 = r5.mAuthToken
            if (r0 == 0) goto L_0x004e
            return r2
        L_0x004e:
            java.lang.String r1 = r4.mSessionCookiesString
            if (r1 == 0) goto L_0x005b
            java.lang.String r0 = r5.mSessionCookiesString
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0060
            return r2
        L_0x005b:
            java.lang.String r0 = r5.mSessionCookiesString
            if (r0 == 0) goto L_0x0060
            return r2
        L_0x0060:
            java.lang.String r1 = r4.mSessionSecret
            if (r1 == 0) goto L_0x006d
            java.lang.String r0 = r5.mSessionSecret
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0072
            return r2
        L_0x006d:
            java.lang.String r0 = r5.mSessionSecret
            if (r0 == 0) goto L_0x0072
            return r2
        L_0x0072:
            java.lang.String r1 = r4.mSessionKey
            if (r1 == 0) goto L_0x007f
            java.lang.String r0 = r5.mSessionKey
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0084
            return r2
        L_0x007f:
            java.lang.String r0 = r5.mSessionKey
            if (r0 == 0) goto L_0x0084
            return r2
        L_0x0084:
            java.lang.String r1 = r4.A00
            if (r1 == 0) goto L_0x0091
            java.lang.String r0 = r5.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0096
            return r2
        L_0x0091:
            java.lang.String r0 = r5.A00
            if (r0 == 0) goto L_0x0096
            return r2
        L_0x0096:
            java.lang.String r1 = r4.mUsername
            java.lang.String r0 = r5.mUsername
            if (r1 == 0) goto L_0x00a1
            boolean r3 = r1.equals(r0)
            return r3
        L_0x00a1:
            if (r0 == 0) goto L_0x00a4
            r3 = 0
        L_0x00a4:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.auth.viewercontext.ViewerContext.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        String str = this.mUserId;
        int i7 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i8 = i * 31;
        String str2 = this.mAuthToken;
        if (str2 != null) {
            i2 = str2.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 31;
        String str3 = this.mSessionCookiesString;
        if (str3 != null) {
            i3 = str3.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (((((((((i9 + i3) * 31) + (this.mIsPageContext ? 1 : 0)) * 31) + (this.mIsFoxContext ? 1 : 0)) * 31) + (this.mIsDittoContext ? 1 : 0)) * 31) + (this.mIsTimelineViewAsContext ? 1 : 0)) * 31;
        String str4 = this.mSessionSecret;
        if (str4 != null) {
            i4 = str4.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 31;
        String str5 = this.mSessionKey;
        if (str5 != null) {
            i5 = str5.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 31;
        String str6 = this.mUsername;
        if (str6 != null) {
            i6 = str6.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 31;
        String str7 = this.A00;
        if (str7 != null) {
            i7 = str7.hashCode();
        }
        return i13 + i7;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mUserId);
        parcel.writeString(this.mAuthToken);
        parcel.writeString(this.mSessionCookiesString);
        parcel.writeInt(this.mIsPageContext ? 1 : 0);
        parcel.writeInt(this.mIsFoxContext ? 1 : 0);
        parcel.writeInt(this.mIsDittoContext ? 1 : 0);
        parcel.writeInt(this.mIsTimelineViewAsContext ? 1 : 0);
        parcel.writeString(this.mSessionSecret);
        parcel.writeString(this.mSessionKey);
        parcel.writeString(this.mUsername);
        parcel.writeString(this.A00);
    }

    private ViewerContext() {
        this.mUserId = null;
        this.mAuthToken = null;
        this.mSessionCookiesString = null;
        this.mIsPageContext = false;
        this.mIsFoxContext = false;
        this.mIsDittoContext = false;
        this.mIsTimelineViewAsContext = false;
        this.mSessionSecret = null;
        this.mSessionKey = null;
        this.mUsername = null;
        this.A00 = null;
    }

    public ViewerContext(C05580Zo r2) {
        String str = r2.A05;
        Preconditions.checkNotNull(str);
        this.mUserId = str;
        String str2 = r2.A01;
        Preconditions.checkNotNull(str2);
        this.mAuthToken = str2;
        this.mSessionCookiesString = r2.A02;
        this.mIsPageContext = r2.A07;
        this.mIsFoxContext = false;
        this.mIsDittoContext = false;
        this.mIsTimelineViewAsContext = false;
        this.mSessionSecret = r2.A04;
        this.mSessionKey = r2.A03;
        this.mUsername = r2.A06;
        this.A00 = r2.A00;
    }

    public ViewerContext(Parcel parcel) {
        this.mUserId = parcel.readString();
        this.mAuthToken = parcel.readString();
        this.mSessionCookiesString = parcel.readString();
        boolean z = false;
        this.mIsPageContext = parcel.readInt() == 1;
        this.mIsFoxContext = parcel.readInt() == 1;
        this.mIsDittoContext = parcel.readInt() == 1;
        this.mIsTimelineViewAsContext = parcel.readInt() == 1 ? true : z;
        this.mSessionSecret = parcel.readString();
        this.mSessionKey = parcel.readString();
        this.mUsername = parcel.readString();
        this.A00 = parcel.readString();
    }
}
