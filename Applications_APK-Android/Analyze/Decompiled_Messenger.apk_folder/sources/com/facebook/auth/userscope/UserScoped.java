package com.facebook.auth.userscope;

import javax.inject.Scope;

@Scope
public @interface UserScoped {
}
