package com.facebook.forker;

import X.AnonymousClass01q;
import X.AnonymousClass08S;
import android.os.ParcelFileDescriptor;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class Process extends Process implements Closeable {
    private static final int FD_STREAM_INPUT = 0;
    private static final int FD_STREAM_OUTPUT = 1;
    private static final int IGNORE_FD = -1;
    public static final int SD_BLACK_HOLE = -3;
    public static final int SD_INHERIT = -2;
    public static final int SD_PIPE = -4;
    public static final int SD_STDOUT = -5;
    public static final int SIGCONT = 18;
    public static final int SIGKILL = 9;
    public static final int SIGSTOP = 19;
    public static final int SIGTERM = 15;
    public static final int SIGTSTP = 20;
    public static final int STATUS_EXITED = 4;
    public static final int STATUS_RUNNING = 1;
    public static final int STATUS_STOPPED = 2;
    public static final int STDERR = 2;
    public static final int STDIN = 0;
    public static final int STDOUT = 1;
    private static final String TAG = "fb-Process";
    public static final int WAIT_RESULT_RUNNING = -2147483646;
    public static final int WAIT_RESULT_STOPPED = -2147483647;
    public static final int WAIT_RESULT_TIMEOUT = Integer.MIN_VALUE;
    private InputStream mChildStderr;
    private OutputStream mChildStdin;
    private InputStream mChildStdout;
    private final int mExitStatus;
    public int mPid = -1;
    private final int mProcessStatus;
    private WaiterThread mWaiterThread = new WaiterThread();

    public final class WaiterThread extends Thread {
        public WaiterThread() {
            super("PidWaiter:Ready");
        }

        public void run() {
            Process.this.nativeWait();
        }
    }

    private native void nativeKill(int i);

    private native int nativeLaunch(String str, String[] strArr, byte[] bArr, int[] iArr, int[] iArr2);

    private static native void nativeUnixClose(int i);

    private static native int nativeUnixCreateTmpFile(String str);

    private static native int nativeUnixOpen(String str);

    private static native int[] nativeUnixPipe(int[] iArr);

    /* access modifiers changed from: private */
    public native void nativeWait();

    public synchronized int exitValueEx() {
        if (this.mProcessStatus == 4) {
        } else {
            throw new IllegalThreadStateException(AnonymousClass08S.A09("Process has not yet terminated: ", this.mPid));
        }
        return this.mExitStatus;
    }

    static {
        AnonymousClass01q.A08("forker");
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x0111  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Process(java.lang.String r17, java.lang.String[] r18, byte[] r19, int[] r20, int[] r21, java.io.File r22) {
        /*
            r16 = this;
            r3 = r16
            r3.<init>()
            r1 = -1
            r3.mPid = r1
            com.facebook.forker.Process$WaiterThread r0 = new com.facebook.forker.Process$WaiterThread
            r0.<init>()
            r3.mWaiterThread = r0
            r10 = 6
            int[] r5 = new int[r10]     // Catch:{ all -> 0x00f1 }
            r2 = 0
            r0 = 0
        L_0x0014:
            if (r0 >= r10) goto L_0x001b
            r5[r0] = r1     // Catch:{ all -> 0x00f4 }
            int r0 = r0 + 1
            goto L_0x0014
        L_0x001b:
            r7 = 0
        L_0x001c:
            r8 = 2
            r9 = 1
            if (r7 >= r10) goto L_0x003e
            int[] r0 = new int[r8]     // Catch:{ all -> 0x00f4 }
            int[] r6 = nativeUnixPipe(r0)     // Catch:{ all -> 0x00f4 }
            if (r7 != 0) goto L_0x0031
            r0 = r6[r9]     // Catch:{ all -> 0x00f4 }
            r5[r2] = r0     // Catch:{ all -> 0x00f4 }
            r0 = r6[r2]     // Catch:{ all -> 0x00f4 }
            r5[r9] = r0     // Catch:{ all -> 0x00f4 }
            goto L_0x003b
        L_0x0031:
            r0 = r6[r2]     // Catch:{ all -> 0x00f4 }
            r5[r7] = r0     // Catch:{ all -> 0x00f4 }
            int r4 = r7 + 1
            r0 = r6[r9]     // Catch:{ all -> 0x00f4 }
            r5[r4] = r0     // Catch:{ all -> 0x00f4 }
        L_0x003b:
            int r7 = r7 + 2
            goto L_0x001c
        L_0x003e:
            int[] r14 = new int[r10]     // Catch:{ all -> 0x00f4 }
            r0 = 0
            r4 = -1
        L_0x0042:
            r11 = -5
            r10 = 3
            if (r0 >= r10) goto L_0x009b
            r10 = r21[r0]     // Catch:{ all -> 0x00f7 }
            if (r10 == r11) goto L_0x0071
            r6 = -4
            if (r10 == r6) goto L_0x0073
            r6 = -3
            if (r10 == r6) goto L_0x0056
            r6 = -2
            if (r10 == r6) goto L_0x0071
            if (r10 >= 0) goto L_0x006f
            goto L_0x0083
        L_0x0056:
            if (r4 != r1) goto L_0x005e
            java.lang.String r6 = "/dev/null"
            int r4 = nativeUnixOpen(r6)     // Catch:{ all -> 0x00f7 }
        L_0x005e:
            if (r4 != r1) goto L_0x006b
            if (r22 == 0) goto L_0x006b
            java.lang.String r6 = r22.getCanonicalPath()     // Catch:{ all -> 0x00f7 }
            int r10 = nativeUnixCreateTmpFile(r6)     // Catch:{ all -> 0x00f7 }
            goto L_0x006c
        L_0x006b:
            r10 = r4
        L_0x006c:
            r7 = -1
            if (r10 == r1) goto L_0x0078
        L_0x006f:
            r7 = r10
            goto L_0x0078
        L_0x0071:
            r7 = r0
            goto L_0x0078
        L_0x0073:
            int r6 = r0 << 1
            int r6 = r6 + r9
            r7 = r5[r6]     // Catch:{ all -> 0x00f7 }
        L_0x0078:
            int r6 = r0 << 1
            r14[r6] = r7     // Catch:{ all -> 0x00f7 }
            int r6 = r6 + 1
            r14[r6] = r0     // Catch:{ all -> 0x00f7 }
            int r0 = r0 + 1
            goto L_0x0042
        L_0x0083:
            java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00f7 }
            java.lang.String r6 = "illegal stream disposition %s for fd %s"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x00f7 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00f7 }
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r0}     // Catch:{ all -> 0x00f7 }
            java.lang.String r0 = java.lang.String.format(r6, r0)     // Catch:{ all -> 0x00f7 }
            r7.<init>(r0)     // Catch:{ all -> 0x00f7 }
            throw r7     // Catch:{ all -> 0x00f7 }
        L_0x009b:
            r7 = 0
        L_0x009c:
            if (r7 >= r10) goto L_0x00ac
            r0 = r21[r7]     // Catch:{ all -> 0x00f7 }
            if (r0 != r11) goto L_0x00a9
            int r6 = r7 << 1
            int r6 = r6 + r2
            r0 = r14[r8]     // Catch:{ all -> 0x00f7 }
            r14[r6] = r0     // Catch:{ all -> 0x00f7 }
        L_0x00a9:
            int r7 = r7 + 1
            goto L_0x009c
        L_0x00ac:
            r10 = r3
            r11 = r17
            r12 = r18
            r15 = r20
            r13 = r19
            int r7 = r10.nativeLaunch(r11, r12, r13, r14, r15)     // Catch:{ all -> 0x00f7 }
            r3.mPid = r7     // Catch:{ all -> 0x00f7 }
            com.facebook.forker.Process$WaiterThread r6 = r3.mWaiterThread     // Catch:{ all -> 0x00f7 }
            java.lang.String r0 = "PidWaiter:"
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r7)     // Catch:{ all -> 0x00f7 }
            r6.setName(r0)     // Catch:{ all -> 0x00f7 }
            r0 = r5[r2]     // Catch:{ all -> 0x00f7 }
            java.lang.Object r0 = openFdStream(r0, r9)     // Catch:{ all -> 0x00f7 }
            java.io.OutputStream r0 = (java.io.OutputStream) r0     // Catch:{ all -> 0x00f7 }
            r3.mChildStdin = r0     // Catch:{ all -> 0x00f7 }
            r0 = r5[r8]     // Catch:{ all -> 0x00f7 }
            java.lang.Object r0 = openFdStream(r0, r2)     // Catch:{ all -> 0x00f7 }
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ all -> 0x00f7 }
            r3.mChildStdout = r0     // Catch:{ all -> 0x00f7 }
            r0 = 4
            r0 = r5[r0]     // Catch:{ all -> 0x00f7 }
            java.lang.Object r0 = openFdStream(r0, r2)     // Catch:{ all -> 0x00f7 }
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ all -> 0x00f7 }
            r3.mChildStderr = r0     // Catch:{ all -> 0x00f7 }
            com.facebook.forker.Process$WaiterThread r0 = r3.mWaiterThread     // Catch:{ all -> 0x00f7 }
            r0.start()     // Catch:{ all -> 0x00f7 }
            unixClose(r4)
            unixClose(r5)
            return
        L_0x00f1:
            r2 = move-exception
            r5 = 0
            goto L_0x00f5
        L_0x00f4:
            r2 = move-exception
        L_0x00f5:
            r4 = -1
            goto L_0x00f8
        L_0x00f7:
            r2 = move-exception
        L_0x00f8:
            unixClose(r4)
            unixClose(r5)
            java.io.OutputStream r0 = r3.mChildStdin
            safeClose(r0)
            java.io.InputStream r0 = r3.mChildStdout
            safeClose(r0)
            java.io.InputStream r0 = r3.mChildStderr
            safeClose(r0)
            int r0 = r3.mPid
            if (r0 == r1) goto L_0x0119
            r1 = 9
            r3.nativeKill(r1)
            r3.nativeWait()
        L_0x0119:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.forker.Process.<init>(java.lang.String, java.lang.String[], byte[], int[], int[], java.io.File):void");
    }

    public static String describeStatus(int i) {
        if (i < 0) {
            return AnonymousClass08S.A09("killed by signal ", -i);
        }
        if (i > 0) {
            return AnonymousClass08S.A09("exited with status ", i);
        }
        return "exited successfully";
    }

    private static String fdMagicName(int i) {
        return AnonymousClass08S.A0D("/proc/", android.os.Process.myPid(), "/task/", android.os.Process.myTid(), "/fd/", i);
    }

    private static void safeClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    public void destroy() {
        nativeKill(9);
        boolean z = false;
        while (true) {
            try {
                this.mWaiterThread.join();
                break;
            } catch (InterruptedException unused) {
                z = true;
                Thread.interrupted();
            }
        }
        synchronized (this) {
            safeClose(this.mChildStdin);
            safeClose(this.mChildStdout);
            safeClose(this.mChildStderr);
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
    }

    private static Object openFdStream(int i, int i2) {
        String fdMagicName = fdMagicName(i);
        if (i2 != 0) {
            return new FileOutputStream(fdMagicName);
        }
        try {
            return new FileInputStream(fdMagicName);
        } catch (FileNotFoundException unused) {
            ParcelFileDescriptor fromFd = ParcelFileDescriptor.fromFd(i);
            if (i2 == 0) {
                return new ParcelFileDescriptor.AutoCloseInputStream(fromFd);
            }
            return new ParcelFileDescriptor.AutoCloseOutputStream(fromFd);
        }
    }

    public void close() {
        destroy();
    }

    public int exitValue() {
        int exitValueEx = exitValueEx();
        if (exitValueEx < 0) {
            return (-exitValueEx) + 128;
        }
        return exitValueEx;
    }

    public InputStream getErrorStream() {
        return this.mChildStderr;
    }

    public InputStream getInputStream() {
        return this.mChildStdout;
    }

    public OutputStream getOutputStream() {
        return this.mChildStdin;
    }

    public int getPid() {
        return this.mPid;
    }

    public void kill(int i) {
        nativeKill(i);
    }

    private static void unixClose(int i) {
        if (i != -1) {
            nativeUnixClose(i);
        }
    }

    private static void unixClose(int[] iArr) {
        if (iArr != null) {
            for (int unixClose : iArr) {
                unixClose(unixClose);
            }
        }
    }

    public synchronized int waitFor() {
        while (this.mProcessStatus != 4) {
            wait();
        }
        return exitValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0051  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int waitFor(int r10, int r11) {
        /*
            r9 = this;
            monitor-enter(r9)
            r1 = 0
            r7 = 0
        L_0x0005:
            int r4 = r9.mProcessStatus     // Catch:{ all -> 0x0063 }
            r0 = r4 & r11
            r3 = 4
            if (r0 != 0) goto L_0x0033
            if (r4 == r3) goto L_0x0033
            if (r10 == 0) goto L_0x0033
            if (r10 <= 0) goto L_0x001b
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0063 }
            long r3 = (long) r10     // Catch:{ all -> 0x0063 }
            r9.wait(r3)     // Catch:{ all -> 0x0063 }
            goto L_0x001e
        L_0x001b:
            r9.wait()     // Catch:{ all -> 0x0063 }
        L_0x001e:
            if (r10 <= 0) goto L_0x0005
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0063 }
            long r3 = r3 - r7
            long r3 = java.lang.Math.max(r1, r3)     // Catch:{ all -> 0x0063 }
            long r5 = (long) r10     // Catch:{ all -> 0x0063 }
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x0030
            r10 = 0
            goto L_0x0005
        L_0x0030:
            int r0 = (int) r3     // Catch:{ all -> 0x0063 }
            int r10 = r10 - r0
            goto L_0x0005
        L_0x0033:
            if (r0 == 0) goto L_0x0051
            r0 = 1
            if (r4 == r0) goto L_0x004c
            r0 = 2
            if (r4 == r0) goto L_0x0047
            if (r4 != r3) goto L_0x0041
            int r0 = r9.mExitStatus     // Catch:{ all -> 0x0063 }
            monitor-exit(r9)
            return r0
        L_0x0041:
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0063 }
            r0.<init>()     // Catch:{ all -> 0x0063 }
            throw r0     // Catch:{ all -> 0x0063 }
        L_0x0047:
            r0 = -2147483647(0xffffffff80000001, float:-1.4E-45)
            monitor-exit(r9)
            return r0
        L_0x004c:
            r0 = -2147483646(0xffffffff80000002, float:-2.8E-45)
            monitor-exit(r9)
            return r0
        L_0x0051:
            if (r10 != 0) goto L_0x0057
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            monitor-exit(r9)
            return r0
        L_0x0057:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = "process entered unexpected state "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r4)     // Catch:{ all -> 0x0063 }
            r1.<init>(r0)     // Catch:{ all -> 0x0063 }
            throw r1     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.forker.Process.waitFor(int, int):int");
    }

    public int waitForUninterruptibly() {
        int waitFor;
        boolean z = false;
        while (true) {
            try {
                waitFor = waitFor();
                break;
            } catch (InterruptedException unused) {
                z = true;
                Thread.interrupted();
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return waitFor;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public int waitForUninterruptibly(int i, int i2) {
        long j = 0;
        boolean z = false;
        int i3 = Integer.MIN_VALUE;
        while (true) {
            if (i > 0) {
                j = System.currentTimeMillis();
            }
            try {
                i3 = waitFor(i, i2);
                if (i3 != Integer.MIN_VALUE) {
                    break;
                }
            } catch (InterruptedException unused) {
                z = true;
                Thread.interrupted();
            }
            if (i > 0) {
                long max = Math.max(0L, System.currentTimeMillis() - j);
                if (((long) i) < max) {
                    i = 0;
                    continue;
                } else {
                    i -= (int) max;
                    continue;
                }
            }
            if (i == 0) {
                break;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return i3;
    }
}
