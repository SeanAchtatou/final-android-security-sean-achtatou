package com.facebook.analytics2.uploader.fbhttp;

import X.AnonymousClass062;
import X.AnonymousClass07B;
import X.AnonymousClass0WA;
import X.AnonymousClass0WT;
import X.AnonymousClass14J;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YI;
import X.AnonymousClass8E9;
import X.C08270ey;
import X.C10740km;
import X.C183988gJ;
import X.C25051Yd;
import X.C30261hl;
import X.C47692Xl;
import X.C51532hC;
import X.C632535h;
import X.C633035m;
import X.C633135n;
import X.C633235o;
import X.C633835v;
import android.content.Context;
import android.os.SystemClock;
import com.facebook.analytics2.logger.Uploader;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.http.interfaces.RequestPriority;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CancellationException;
import org.apache.http.message.BasicHeader;

public class FbHttpUploader implements Uploader, CallerContextable, AnonymousClass062 {
    public static Set A0B;
    public static final ArrayList A0C = new ArrayList(2);
    public AnonymousClass14J A00;
    public AnonymousClass8E9 A01;
    public AnonymousClass8E9 A02;
    public AnonymousClass1YI A03;
    public C10740km A04;
    public C25051Yd A05;
    public C08270ey A06;
    public C51532hC A07;
    public Boolean A08;
    public boolean A09;
    public boolean A0A;

    public void CL9(C633035m r7, C633135n r8) {
        RequestPriority requestPriority;
        A00(r7, 0);
        C47692Xl r3 = new C47692Xl();
        if (r7.A02 == AnonymousClass07B.A01) {
            r3.A01(AnonymousClass07B.A0C);
        }
        if (r7.A01.intValue() != 1) {
            requestPriority = RequestPriority.A03;
        } else if (this.A03.AbO(AnonymousClass1Y3.A3L, false)) {
            requestPriority = RequestPriority.A04;
        } else {
            requestPriority = RequestPriority.A05;
        }
        r3.A01 = requestPriority;
        Boolean bool = this.A08;
        if (bool == null) {
            bool = Boolean.valueOf(this.A03.AbO(540, false));
            this.A08 = bool;
        }
        r3.A08 = bool.booleanValue();
        if (this.A03.AbO(500, false)) {
            ImmutableList.Builder builder = new ImmutableList.Builder();
            ImmutableList immutableList = r3.A04;
            if (immutableList != null) {
                builder.addAll((Iterable) immutableList);
            }
            builder.add((Object) new BasicHeader("X-fb-a2-upload-elapsed-realtime-ms", Long.toString(SystemClock.elapsedRealtime())));
            r3.A00(builder.build());
        }
        try {
            A01(r7, new C633235o(r8, this.A03), r3);
        } catch (IOException e) {
            if (this.A03.AbO(321, false)) {
                try {
                    A01(r7, new C183988gJ(r8, this.A03), r3);
                } catch (IOException e2) {
                    r8.A00(e2);
                }
            } else {
                r8.A00(e);
            }
        }
    }

    static {
        HashSet hashSet = new HashSet(1);
        hashSet.add("native_newsfeed");
        A0B = hashSet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0088, code lost:
        if (r12.A05.Aem(286014757935145L) == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0021, code lost:
        if (r12.A05.Aem(286014757541923L) == false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A00(X.C633035m r13, int r14) {
        /*
            r12 = this;
            boolean r0 = r12.A0A
            if (r0 != 0) goto L_0x00ee
            X.1Yd r2 = r12.A05
            r0 = 286014757148701(0x104210000181d, double:1.413100657108E-309)
            boolean r0 = r2.Aem(r0)
            r12.A09 = r0
            r3 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0023
            X.1Yd r4 = r12.A05
            r0 = 286014757541923(0x1042100061823, double:1.413100659050777E-309)
            boolean r0 = r4.Aem(r0)
            r8 = 1
            if (r0 != 0) goto L_0x0024
        L_0x0023:
            r8 = 0
        L_0x0024:
            if (r8 == 0) goto L_0x0078
            X.1Yd r4 = r12.A05
            r0 = 286014757214238(0x104210001181e, double:1.4131006574318E-309)
            boolean r9 = r4.Aem(r0)
            X.8E9 r6 = new X.8E9
            X.0ey r7 = r12.A06
            r10 = 0
            r6.<init>(r7, r8, r9, r10)
            boolean[] r5 = r6.A04
            X.1Yd r4 = r12.A05
            r0 = 286014757279775(0x104210002181f, double:1.413100657755594E-309)
            boolean r0 = r4.Aem(r0)
            r5[r2] = r0
            boolean[] r5 = r6.A04
            X.1Yd r4 = r12.A05
            r0 = 286014757345312(0x1042100031820, double:1.41310065807939E-309)
            boolean r0 = r4.Aem(r0)
            r5[r3] = r0
            boolean[] r5 = r6.A05
            X.1Yd r4 = r12.A05
            r0 = 286014757410849(0x1042100041821, double:1.413100658403186E-309)
            boolean r0 = r4.Aem(r0)
            r5[r2] = r0
            boolean[] r5 = r6.A05
            X.1Yd r4 = r12.A05
            r0 = 286014757476386(0x1042100051822, double:1.41310065872698E-309)
            boolean r0 = r4.Aem(r0)
            r5[r3] = r0
            r12.A01 = r6
        L_0x0078:
            boolean r0 = r12.A09
            if (r0 == 0) goto L_0x008a
            X.1Yd r4 = r12.A05
            r0 = 286014757935145(0x10421000c1829, double:1.41310066099355E-309)
            boolean r0 = r4.Aem(r0)
            r8 = 1
            if (r0 != 0) goto L_0x008b
        L_0x008a:
            r8 = 0
        L_0x008b:
            if (r8 == 0) goto L_0x00ec
            X.1Yd r4 = r12.A05
            r0 = 286014757607460(0x1042100071824, double:1.413100659374573E-309)
            boolean r9 = r4.Aem(r0)
            X.1Yd r4 = r12.A05
            r0 = 567489734707108(0x20421000d07a4, double:2.80377182286345E-309)
            int r0 = r4.AqL(r0, r2)
            long r0 = (long) r0
            r10 = 1000(0x3e8, double:4.94E-321)
            long r10 = r10 * r0
            X.8E9 r6 = new X.8E9
            X.2hC r7 = r12.A07
            r6.<init>(r7, r8, r9, r10)
            boolean[] r5 = r6.A04
            X.1Yd r4 = r12.A05
            r0 = 286014757672997(0x1042100081825, double:1.41310065969837E-309)
            boolean r0 = r4.Aem(r0)
            r5[r2] = r0
            boolean[] r5 = r6.A04
            X.1Yd r4 = r12.A05
            r0 = 286014757738534(0x1042100091826, double:1.413100660022165E-309)
            boolean r0 = r4.Aem(r0)
            r5[r3] = r0
            boolean[] r5 = r6.A05
            X.1Yd r4 = r12.A05
            r0 = 286014757804071(0x10421000a1827, double:1.41310066034596E-309)
            boolean r0 = r4.Aem(r0)
            r5[r2] = r0
            boolean[] r4 = r6.A05
            X.1Yd r2 = r12.A05
            r0 = 286014757869608(0x10421000b1828, double:1.413100660669756E-309)
            boolean r0 = r2.Aem(r0)
            r4[r3] = r0
            r12.A02 = r6
        L_0x00ec:
            r12.A0A = r3
        L_0x00ee:
            boolean r0 = r12.A09
            if (r0 == 0) goto L_0x0106
            r3 = 0
        L_0x00f3:
            r0 = 2
            if (r3 >= r0) goto L_0x0106
            X.8E9 r0 = r12.A01
            boolean r2 = r12.A04(r0, r13, r14)
            X.8E9 r0 = r12.A02
            boolean r1 = r12.A04(r0, r13, r14)
            if (r2 != 0) goto L_0x0107
            if (r1 != 0) goto L_0x0107
        L_0x0106:
            return
        L_0x0107:
            if (r2 == 0) goto L_0x0112
            X.8E9 r0 = r12.A01
            boolean r0 = A03(r0)
            if (r0 != 0) goto L_0x0112
            return
        L_0x0112:
            if (r1 == 0) goto L_0x011d
            X.8E9 r0 = r12.A02
            boolean r0 = A03(r0)
            if (r0 != 0) goto L_0x011d
            return
        L_0x011d:
            int r3 = r3 + 1
            goto L_0x00f3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.analytics2.uploader.fbhttp.FbHttpUploader.A00(X.35m, int):void");
    }

    /* JADX INFO: finally extract failed */
    private void A01(C633035m r7, C633235o r8, C47692Xl r9) {
        StringWriter stringWriter;
        C632535h r5 = r7.A00;
        C10740km r4 = this.A04;
        int Alm = r5.Alm();
        ArrayList arrayList = A0C;
        synchronized (arrayList) {
            if (arrayList.isEmpty()) {
                stringWriter = new StringWriter(Math.max(Alm, 100000));
            } else {
                stringWriter = (StringWriter) arrayList.remove(arrayList.size() - 1);
            }
        }
        try {
            r5.CNf(stringWriter);
            C633835v r2 = new C633835v(stringWriter.toString(), r5.BFt());
            stringWriter.close();
            A02(stringWriter);
            try {
                r4.A07(r8, r2, r9, CallerContext.A07(FbHttpUploader.class, "MAGIC_LOGOUT_TAG"));
                A00(r7, 1);
            } catch (IOException e) {
                throw e;
            } catch (CancellationException e2) {
                IOException iOException = new IOException();
                iOException.initCause(e2);
                throw iOException;
            } catch (Exception e3) {
                if (e3 != null) {
                    Throwables.throwIfUnchecked(e3);
                }
                IOException iOException2 = new IOException();
                iOException2.initCause(e3);
                throw iOException2;
            }
        } catch (Throwable th) {
            stringWriter.close();
            A02(stringWriter);
            throw th;
        }
    }

    private void A02(StringWriter stringWriter) {
        ArrayList arrayList = A0C;
        synchronized (arrayList) {
            if (arrayList.size() < 2 && stringWriter.getBuffer().capacity() <= 100000) {
                stringWriter.getBuffer().setLength(0);
                arrayList.add(stringWriter);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:17|18|19|20|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0032, code lost:
        return true;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0027 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean A03(X.AnonymousClass8E9 r3) {
        /*
            if (r3 == 0) goto L_0x0037
            X.0eU r2 = r3.A01
            if (r2 == 0) goto L_0x0037
            boolean r0 = r3.A03
            if (r0 == 0) goto L_0x001a
            long r0 = r3.A00
            boolean r0 = r2.A03(r0)     // Catch:{ InterruptedException -> 0x0011 }
            return r0
        L_0x0011:
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            r0 = 0
            return r0
        L_0x001a:
            java.lang.Object r1 = r2.A01
            monitor-enter(r1)
            boolean r0 = r2.A00     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x0031
            java.lang.Object r0 = r2.A01     // Catch:{ InterruptedException -> 0x0027 }
            r0.wait()     // Catch:{ InterruptedException -> 0x0027 }
            goto L_0x0031
        L_0x0027:
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0034 }
            r0.interrupt()     // Catch:{ all -> 0x0034 }
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            r0 = 0
            return r0
        L_0x0031:
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            r0 = 1
            return r0
        L_0x0034:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            throw r0
        L_0x0037:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.analytics2.uploader.fbhttp.FbHttpUploader.A03(X.8E9):boolean");
    }

    private boolean A04(AnonymousClass8E9 r3, C633035m r4, int i) {
        boolean z;
        boolean[] zArr;
        if (r3 == null || !r3.A02) {
            z = false;
        } else {
            if (r4.A01.intValue() != 1) {
                zArr = r3.A05;
            } else {
                zArr = r3.A04;
            }
            z = zArr[i];
        }
        if (z && r3 != null && r3.A02 && r3.A01.A00 && A0B.contains(this.A00.A02())) {
            return true;
        }
        return false;
    }

    public FbHttpUploader(Context context) {
        AnonymousClass1XX r1 = AnonymousClass1XX.get(context);
        this.A04 = C30261hl.A02(r1);
        this.A03 = AnonymousClass0WA.A00(r1);
        this.A05 = AnonymousClass0WT.A00(r1);
        this.A06 = C08270ey.A00(r1);
        this.A07 = C51532hC.A00(r1);
        this.A00 = AnonymousClass14J.A00(r1);
    }
}
