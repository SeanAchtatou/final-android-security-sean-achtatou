package com.facebook.analytics2.loggermodule;

import X.AnonymousClass062;
import X.AnonymousClass15D;
import X.AnonymousClass15G;
import X.AnonymousClass1XX;
import android.content.Context;
import android.os.HandlerThread;
import com.facebook.analytics2.logger.HandlerThreadFactory;

public class Analytics2HandlerThreadFactory implements HandlerThreadFactory, AnonymousClass062 {
    public AnonymousClass15D A00;
    public AnonymousClass15G A01;

    public HandlerThread AUr(String str, int i) {
        HandlerThread A012;
        AnonymousClass15D r3 = this.A00;
        synchronized (r3) {
            A012 = r3.A01.A01(str, i, r3.A00);
        }
        AnonymousClass15G r1 = this.A01;
        if (10 <= i) {
            r1.A03 = A012.getThreadId();
            if (r1.A0A) {
                AnonymousClass15G.A02(r1);
                return A012;
            }
        } else {
            r1.A02 = A012.getThreadId();
            if (r1.A0A) {
                AnonymousClass15G.A01(r1);
            }
        }
        return A012;
    }

    public Analytics2HandlerThreadFactory(Context context) {
        AnonymousClass1XX r1 = AnonymousClass1XX.get(context);
        this.A00 = AnonymousClass15D.A00(r1);
        this.A01 = AnonymousClass15G.A00(r1);
    }
}
