package com.facebook.fbservice.ops;

import X.AnonymousClass0lL;
import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;

public interface BlueServiceOperationFactory {
    public static final String BLUESERVICE_NO_AUTH = "BLUESERVICE_NO_AUTH";

    AnonymousClass0lL newInstance(String str, Bundle bundle);

    AnonymousClass0lL newInstance(String str, Bundle bundle, int i);

    AnonymousClass0lL newInstance(String str, Bundle bundle, int i, CallerContext callerContext);

    AnonymousClass0lL newInstance(String str, Bundle bundle, CallerContext callerContext);
}
