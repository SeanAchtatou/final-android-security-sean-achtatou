package com.facebook.fbservice.ops;

import X.AnonymousClass08S;
import X.C000700l;
import X.C1940697y;
import X.C27171cl;
import com.facebook.fbservice.service.ICompletionHandler;
import com.facebook.fbservice.service.OperationResult;

public final class DefaultBlueServiceOperation$3 extends ICompletionHandler.Stub {
    public final /* synthetic */ C27171cl A00;

    public DefaultBlueServiceOperation$3(C27171cl r3) {
        this.A00 = r3;
        C000700l.A09(608762445, C000700l.A03(-1826168735));
    }

    public void BhE(OperationResult operationResult) {
        int A03 = C000700l.A03(-2015683598);
        this.A00.A05(operationResult);
        C000700l.A09(-1744303350, A03);
    }

    public void BhG(OperationResult operationResult) {
        int A03 = C000700l.A03(-626413271);
        C27171cl r2 = this.A00;
        if (!r2.BEO() && !r2.A0C) {
            C27171cl.A03(r2, AnonymousClass08S.A0J("ReportProgress-", r2.A0O), new C1940697y(r2, operationResult));
        }
        C000700l.A09(800762246, A03);
    }
}
