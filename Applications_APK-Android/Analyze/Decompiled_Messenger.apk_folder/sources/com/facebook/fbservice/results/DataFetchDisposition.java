package com.facebook.fbservice.results;

import X.AnonymousClass08B;
import X.AnonymousClass0T1;
import X.AnonymousClass0u3;
import X.AnonymousClass162;
import X.AnonymousClass1CN;
import X.AnonymousClass1CO;
import X.AnonymousClass1CP;
import X.AnonymousClass1CQ;
import X.C004205c;
import X.C04300To;
import X.C10170jf;
import X.C10500kH;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.util.TriState;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.Iterator;
import java.util.List;

public final class DataFetchDisposition implements Parcelable {
    public static final DataFetchDisposition A09;
    public static final DataFetchDisposition A0A;
    public static final DataFetchDisposition A0B;
    public static final DataFetchDisposition A0C;
    public static final DataFetchDisposition A0D;
    public static final DataFetchDisposition A0E;
    public static final DataFetchDisposition A0F;
    public static final DataFetchDisposition A0G;
    public static final DataFetchDisposition A0H;
    public static final DataFetchDisposition A0I = new DataFetchDisposition();
    public static final Parcelable.Creator CREATOR = new AnonymousClass0T1();
    public final int A00;
    public final TriState A01;
    public final TriState A02;
    public final TriState A03;
    public final TriState A04;
    public final TriState A05;
    public final TriState A06;
    public final AnonymousClass0u3 A07;
    public final boolean A08;

    public int describeContents() {
        return 0;
    }

    static {
        AnonymousClass162 r1 = new AnonymousClass162();
        r1.A07 = AnonymousClass0u3.SERVER;
        TriState triState = TriState.YES;
        r1.A02 = triState;
        TriState triState2 = TriState.NO;
        r1.A04 = triState2;
        r1.A06 = triState2;
        r1.A00 = 2;
        A0G = new DataFetchDisposition(r1);
        AnonymousClass162 r12 = new AnonymousClass162();
        AnonymousClass0u3 r4 = AnonymousClass0u3.IN_MEMORY_CACHE;
        r12.A07 = r4;
        r12.A02 = triState2;
        r12.A04 = triState2;
        r12.A06 = triState2;
        r12.A00 = 0;
        A0D = new DataFetchDisposition(r12);
        AnonymousClass162 r13 = new AnonymousClass162();
        r13.A07 = r4;
        r13.A02 = triState2;
        r13.A04 = triState;
        r13.A06 = triState2;
        r13.A00 = 0;
        A0C = new DataFetchDisposition(r13);
        AnonymousClass162 r14 = new AnonymousClass162();
        AnonymousClass0u3 r42 = AnonymousClass0u3.LOCAL_DISK_CACHE;
        r14.A07 = r42;
        r14.A02 = triState2;
        r14.A04 = triState2;
        r14.A06 = triState2;
        r14.A00 = 1;
        A0F = new DataFetchDisposition(r14);
        AnonymousClass162 r15 = new AnonymousClass162();
        r15.A07 = r42;
        r15.A02 = triState2;
        r15.A04 = triState;
        r15.A06 = triState2;
        r15.A00 = 1;
        A0E = new DataFetchDisposition(r15);
        AnonymousClass162 r16 = new AnonymousClass162();
        AnonymousClass0u3 r43 = AnonymousClass0u3.LOCAL_UNSPECIFIED_CACHE;
        r16.A07 = r43;
        r16.A02 = triState2;
        r16.A04 = triState2;
        r16.A06 = triState2;
        r16.A00 = 1;
        A0B = new DataFetchDisposition(r16);
        AnonymousClass162 r17 = new AnonymousClass162();
        r17.A07 = r43;
        r17.A02 = triState2;
        r17.A04 = triState;
        r17.A06 = triState2;
        r17.A00 = 1;
        new DataFetchDisposition(r17);
        AnonymousClass162 r18 = new AnonymousClass162();
        r18.A07 = r43;
        r18.A02 = triState2;
        r18.A04 = triState;
        r18.A01 = triState;
        r18.A06 = triState2;
        r18.A00 = 1;
        A09 = new DataFetchDisposition(r18);
        AnonymousClass162 r19 = new AnonymousClass162();
        r19.A07 = r43;
        r19.A02 = triState2;
        r19.A03 = triState;
        r19.A06 = triState2;
        r19.A00 = 1;
        A0A = new DataFetchDisposition(r19);
        AnonymousClass162 r110 = new AnonymousClass162();
        r110.A07 = AnonymousClass0u3.SMS;
        r110.A02 = triState;
        r110.A04 = triState2;
        r110.A06 = triState2;
        r110.A00 = 1;
        A0H = new DataFetchDisposition(r110);
    }

    public void writeToParcel(Parcel parcel, int i) {
        C417826y.A0V(parcel, this.A08);
        parcel.writeSerializable(this.A07);
        parcel.writeSerializable(this.A02);
        parcel.writeSerializable(this.A04);
        parcel.writeSerializable(this.A03);
        parcel.writeSerializable(this.A01);
        parcel.writeSerializable(this.A05);
        parcel.writeSerializable(this.A06);
        parcel.writeInt(this.A00);
    }

    public static DataFetchDisposition A00(List list) {
        boolean z;
        if (!list.isEmpty()) {
            if (list.size() == 1) {
                return (DataFetchDisposition) list.get(0);
            }
            Iterator it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((DataFetchDisposition) it.next()) != A0I) {
                        z = false;
                        break;
                    }
                } else {
                    z = true;
                    break;
                }
            }
            if (!z) {
                AnonymousClass162 r2 = new AnonymousClass162();
                r2.A07 = AnonymousClass0u3.COMPOSED;
                r2.A02 = AnonymousClass08B.A00(C04300To.A07(list, new AnonymousClass1CN()), AnonymousClass08B.A00, TriState.NO);
                List A072 = C04300To.A07(list, new AnonymousClass1CO());
                C004205c r0 = AnonymousClass08B.A01;
                TriState triState = TriState.YES;
                r2.A04 = AnonymousClass08B.A00(A072, r0, triState);
                r2.A03 = AnonymousClass08B.A00(C04300To.A07(list, new AnonymousClass1CP()), AnonymousClass08B.A01, triState);
                r2.A01 = AnonymousClass08B.A00(C04300To.A07(list, new AnonymousClass1CQ()), AnonymousClass08B.A01, triState);
                r2.A05 = AnonymousClass08B.A00(C04300To.A07(list, new C10170jf()), AnonymousClass08B.A01, triState);
                r2.A06 = AnonymousClass08B.A00(C04300To.A07(list, new C10500kH()), AnonymousClass08B.A01, triState);
                int i = ((DataFetchDisposition) list.get(0)).A00;
                for (int i2 = 1; i2 < list.size(); i2++) {
                    int i3 = ((DataFetchDisposition) list.get(i2)).A00;
                    if (i < i3) {
                        i = i3;
                    }
                }
                r2.A00 = i;
                return new DataFetchDisposition(r2);
            }
        }
        return A0I;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("dataSource", this.A07);
        stringHelper.add("fromAuthoritativeData", this.A02);
        stringHelper.add("isStaleData", this.A04);
        stringHelper.add("isIncompleteData", this.A03);
        stringHelper.add("fellbackToCachedDataAfterFailedToHitServer", this.A01);
        stringHelper.add("needsInitialFetch", this.A05);
        stringHelper.add("wasFetchSynchronous", this.A06);
        stringHelper.add("performanceCategory", this.A00);
        return stringHelper.toString();
    }

    private DataFetchDisposition() {
        this.A08 = false;
        this.A07 = null;
        TriState triState = TriState.UNSET;
        this.A02 = triState;
        this.A04 = triState;
        this.A03 = triState;
        this.A01 = triState;
        this.A05 = triState;
        this.A06 = triState;
        this.A00 = -1;
    }

    public DataFetchDisposition(AnonymousClass162 r2) {
        this.A08 = true;
        AnonymousClass0u3 r0 = r2.A07;
        Preconditions.checkNotNull(r0);
        this.A07 = r0;
        TriState triState = r2.A02;
        Preconditions.checkNotNull(triState);
        this.A02 = triState;
        TriState triState2 = r2.A04;
        Preconditions.checkNotNull(triState2);
        this.A04 = triState2;
        TriState triState3 = r2.A03;
        Preconditions.checkNotNull(triState3);
        this.A03 = triState3;
        TriState triState4 = r2.A01;
        Preconditions.checkNotNull(triState4);
        this.A01 = triState4;
        TriState triState5 = r2.A05;
        Preconditions.checkNotNull(triState5);
        this.A05 = triState5;
        TriState triState6 = r2.A06;
        Preconditions.checkNotNull(triState6);
        this.A06 = triState6;
        this.A00 = r2.A00;
    }

    public DataFetchDisposition(Parcel parcel) {
        this.A08 = C417826y.A0W(parcel);
        this.A07 = (AnonymousClass0u3) parcel.readSerializable();
        this.A02 = (TriState) parcel.readSerializable();
        this.A04 = (TriState) parcel.readSerializable();
        this.A03 = (TriState) parcel.readSerializable();
        this.A01 = (TriState) parcel.readSerializable();
        this.A05 = (TriState) parcel.readSerializable();
        this.A06 = (TriState) parcel.readSerializable();
        this.A00 = parcel.readInt();
    }
}
