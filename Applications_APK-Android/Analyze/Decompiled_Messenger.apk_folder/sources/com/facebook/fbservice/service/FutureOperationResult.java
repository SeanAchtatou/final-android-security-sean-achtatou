package com.facebook.fbservice.service;

import com.google.common.util.concurrent.ListenableFuture;

public final class FutureOperationResult extends OperationResult {
    public final ListenableFuture future;
}
