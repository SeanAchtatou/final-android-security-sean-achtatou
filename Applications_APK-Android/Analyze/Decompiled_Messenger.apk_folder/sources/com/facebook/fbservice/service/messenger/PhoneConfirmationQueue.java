package com.facebook.fbservice.service.messenger;

import com.google.inject.BindingAnnotation;

@BindingAnnotation
public @interface PhoneConfirmationQueue {
}
