package com.facebook.fbservice.service;

import com.google.inject.BindingAnnotation;

@BindingAnnotation
public @interface AuthQueue {
}
