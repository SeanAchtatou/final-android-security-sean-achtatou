package com.facebook.fbservice.service;

import X.AnonymousClass80H;
import X.C000700l;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.http.interfaces.RequestPriority;

public interface IBlueService extends IInterface {

    public abstract class Stub extends Binder implements IBlueService {

        public final class Proxy implements IBlueService {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(-1738721481);
                this.A00 = iBinder;
                C000700l.A09(1833494170, A03);
            }

            public boolean ARd(String str) {
                int A03 = C000700l.A03(89602169);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.fbservice.service.IBlueService");
                    obtain.writeString(str);
                    boolean z = false;
                    this.A00.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1608345885, A03);
                }
            }

            public boolean AS8(String str, RequestPriority requestPriority) {
                int A03 = C000700l.A03(-2026143436);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.fbservice.service.IBlueService");
                    obtain.writeString(str);
                    boolean z = true;
                    if (requestPriority != null) {
                        obtain.writeInt(1);
                        requestPriority.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1457264975, A03);
                }
            }

            public boolean C0R(String str, ICompletionHandler iCompletionHandler) {
                IBinder iBinder;
                int A03 = C000700l.A03(-1616644568);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.fbservice.service.IBlueService");
                    obtain.writeString(str);
                    if (iCompletionHandler != null) {
                        iBinder = iCompletionHandler.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    boolean z = false;
                    this.A00.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1225561177, A03);
                }
            }

            public String CHM(String str, Bundle bundle, boolean z, CallerContext callerContext) {
                int A03 = C000700l.A03(-350739940);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.fbservice.service.IBlueService");
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    int i = 0;
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    if (callerContext != null) {
                        obtain.writeInt(1);
                        callerContext.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(2130871210, A03);
                }
            }

            public String CHN(String str, Bundle bundle, boolean z, ICompletionHandler iCompletionHandler, CallerContext callerContext) {
                IBinder iBinder;
                int A03 = C000700l.A03(8452278);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.fbservice.service.IBlueService");
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    int i = 0;
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    if (iCompletionHandler != null) {
                        iBinder = iCompletionHandler.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    if (callerContext != null) {
                        obtain.writeInt(1);
                        callerContext.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-119010638, A03);
                }
            }

            public String CHO(String str, Bundle bundle, boolean z, boolean z2, ICompletionHandler iCompletionHandler, CallerContext callerContext) {
                IBinder iBinder;
                int A03 = C000700l.A03(-294906940);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.fbservice.service.IBlueService");
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    int i = 0;
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    int i2 = 0;
                    if (z2) {
                        i2 = 1;
                    }
                    obtain.writeInt(i2);
                    if (iCompletionHandler != null) {
                        iBinder = iCompletionHandler.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    if (callerContext != null) {
                        obtain.writeInt(1);
                        callerContext.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1916498138, A03);
                }
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(1307174292);
                IBinder iBinder = this.A00;
                C000700l.A09(-1307656054, A03);
                return iBinder;
            }
        }

        public static IBlueService A02(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(AnonymousClass80H.$const$string(54));
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IBlueService)) {
                return new Proxy(iBinder);
            }
            return (IBlueService) queryLocalInterface;
        }

        public Stub() {
            int A03 = C000700l.A03(-2051041382);
            attachInterface(this, AnonymousClass80H.$const$string(54));
            C000700l.A09(471917658, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(1612977669, C000700l.A03(347609561));
            return this;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r14v0 */
        /* JADX WARN: Type inference failed for: r14v1, types: [com.facebook.common.callercontext.CallerContext] */
        /* JADX WARN: Type inference failed for: r14v4, types: [com.facebook.common.callercontext.CallerContext] */
        /* JADX WARN: Type inference failed for: r14v7, types: [com.facebook.common.callercontext.CallerContext] */
        /* JADX WARN: Type inference failed for: r14v10, types: [com.facebook.http.interfaces.RequestPriority] */
        /* JADX WARN: Type inference failed for: r14v13 */
        /* JADX WARN: Type inference failed for: r14v14 */
        /* JADX WARN: Type inference failed for: r14v15 */
        /* JADX WARN: Type inference failed for: r14v16 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTransact(int r16, android.os.Parcel r17, android.os.Parcel r18, int r19) {
            /*
                r15 = this;
                r0 = 1004720701(0x3be2d23d, float:0.006922035)
                int r1 = X.C000700l.A03(r0)
                r3 = 1598968902(0x5f4e5446, float:1.4867585E19)
                r7 = 1
                r0 = 54
                java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
                r2 = r18
                r5 = r16
                if (r5 == r3) goto L_0x015c
                r6 = 0
                r14 = 0
                r4 = r17
                switch(r16) {
                    case 1: goto L_0x011f;
                    case 2: goto L_0x00d8;
                    case 3: goto L_0x0089;
                    case 4: goto L_0x0071;
                    case 5: goto L_0x004b;
                    case 6: goto L_0x002b;
                    default: goto L_0x001e;
                }
            L_0x001e:
                r0 = r19
                boolean r2 = super.onTransact(r5, r4, r2, r0)
                r0 = 1380546366(0x5249773e, float:2.16322245E11)
                X.C000700l.A09(r0, r1)
                return r2
            L_0x002b:
                r4.enforceInterface(r0)
                java.lang.String r3 = r4.readString()
                android.os.IBinder r0 = r4.readStrongBinder()
                com.facebook.fbservice.service.ICompletionHandler r0 = com.facebook.fbservice.service.ICompletionHandler.Stub.A00(r0)
                boolean r0 = r15.C0R(r3, r0)
                r2.writeNoException()
                r2.writeInt(r0)
                r0 = -1676543128(0xffffffff9c11fb68, float:-4.830141E-22)
                X.C000700l.A09(r0, r1)
                return r7
            L_0x004b:
                r4.enforceInterface(r0)
                java.lang.String r3 = r4.readString()
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x0060
                android.os.Parcelable$Creator r0 = com.facebook.http.interfaces.RequestPriority.CREATOR
                java.lang.Object r14 = r0.createFromParcel(r4)
                com.facebook.http.interfaces.RequestPriority r14 = (com.facebook.http.interfaces.RequestPriority) r14
            L_0x0060:
                boolean r0 = r15.AS8(r3, r14)
                r2.writeNoException()
                r2.writeInt(r0)
                r0 = -1610194430(0xffffffffa0066202, float:-1.138267E-19)
                X.C000700l.A09(r0, r1)
                return r7
            L_0x0071:
                r4.enforceInterface(r0)
                java.lang.String r0 = r4.readString()
                boolean r0 = r15.ARd(r0)
                r2.writeNoException()
                r2.writeInt(r0)
                r0 = -1894138814(0xffffffff8f19bc42, float:-7.5797395E-30)
                X.C000700l.A09(r0, r1)
                return r7
            L_0x0089:
                r4.enforceInterface(r0)
                java.lang.String r9 = r4.readString()
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x00d6
                android.os.Parcelable$Creator r0 = android.os.Bundle.CREATOR
                java.lang.Object r10 = r0.createFromParcel(r4)
                android.os.Bundle r10 = (android.os.Bundle) r10
            L_0x009e:
                int r0 = r4.readInt()
                r11 = 0
                if (r0 == 0) goto L_0x00a6
                r11 = 1
            L_0x00a6:
                int r0 = r4.readInt()
                r12 = 0
                if (r0 == 0) goto L_0x00ae
                r12 = 1
            L_0x00ae:
                android.os.IBinder r0 = r4.readStrongBinder()
                com.facebook.fbservice.service.ICompletionHandler r13 = com.facebook.fbservice.service.ICompletionHandler.Stub.A00(r0)
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x00c4
                android.os.Parcelable$Creator r0 = com.facebook.common.callercontext.CallerContext.CREATOR
                java.lang.Object r14 = r0.createFromParcel(r4)
                com.facebook.common.callercontext.CallerContext r14 = (com.facebook.common.callercontext.CallerContext) r14
            L_0x00c4:
                r8 = r15
                java.lang.String r0 = r8.CHO(r9, r10, r11, r12, r13, r14)
                r2.writeNoException()
                r2.writeString(r0)
                r0 = 1479050157(0x582883ad, float:7.4113399E14)
                X.C000700l.A09(r0, r1)
                return r7
            L_0x00d6:
                r10 = r14
                goto L_0x009e
            L_0x00d8:
                r4.enforceInterface(r0)
                java.lang.String r10 = r4.readString()
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x011d
                android.os.Parcelable$Creator r0 = android.os.Bundle.CREATOR
                java.lang.Object r11 = r0.createFromParcel(r4)
                android.os.Bundle r11 = (android.os.Bundle) r11
            L_0x00ed:
                int r0 = r4.readInt()
                r12 = 0
                if (r0 == 0) goto L_0x00f5
                r12 = 1
            L_0x00f5:
                android.os.IBinder r0 = r4.readStrongBinder()
                com.facebook.fbservice.service.ICompletionHandler r13 = com.facebook.fbservice.service.ICompletionHandler.Stub.A00(r0)
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x010b
                android.os.Parcelable$Creator r0 = com.facebook.common.callercontext.CallerContext.CREATOR
                java.lang.Object r14 = r0.createFromParcel(r4)
                com.facebook.common.callercontext.CallerContext r14 = (com.facebook.common.callercontext.CallerContext) r14
            L_0x010b:
                r9 = r15
                java.lang.String r0 = r9.CHN(r10, r11, r12, r13, r14)
                r2.writeNoException()
                r2.writeString(r0)
                r0 = 1839622595(0x6da669c3, float:6.437796E27)
                X.C000700l.A09(r0, r1)
                return r7
            L_0x011d:
                r11 = r14
                goto L_0x00ed
            L_0x011f:
                r4.enforceInterface(r0)
                java.lang.String r5 = r4.readString()
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x015a
                android.os.Parcelable$Creator r0 = android.os.Bundle.CREATOR
                java.lang.Object r3 = r0.createFromParcel(r4)
                android.os.Bundle r3 = (android.os.Bundle) r3
            L_0x0134:
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x013b
                r6 = 1
            L_0x013b:
                int r0 = r4.readInt()
                if (r0 == 0) goto L_0x0149
                android.os.Parcelable$Creator r0 = com.facebook.common.callercontext.CallerContext.CREATOR
                java.lang.Object r14 = r0.createFromParcel(r4)
                com.facebook.common.callercontext.CallerContext r14 = (com.facebook.common.callercontext.CallerContext) r14
            L_0x0149:
                java.lang.String r0 = r15.CHM(r5, r3, r6, r14)
                r2.writeNoException()
                r2.writeString(r0)
                r0 = 1275036587(0x4bff83ab, float:3.3490774E7)
                X.C000700l.A09(r0, r1)
                return r7
            L_0x015a:
                r3 = r14
                goto L_0x0134
            L_0x015c:
                r2.writeString(r0)
                r0 = -1240768423(0xffffffffb60b6059, float:-2.0768696E-6)
                X.C000700l.A09(r0, r1)
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.fbservice.service.IBlueService.Stub.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }
    }

    boolean ARd(String str);

    boolean AS8(String str, RequestPriority requestPriority);

    boolean C0R(String str, ICompletionHandler iCompletionHandler);

    String CHM(String str, Bundle bundle, boolean z, CallerContext callerContext);

    String CHN(String str, Bundle bundle, boolean z, ICompletionHandler iCompletionHandler, CallerContext callerContext);

    String CHO(String str, Bundle bundle, boolean z, boolean z2, ICompletionHandler iCompletionHandler, CallerContext callerContext);
}
