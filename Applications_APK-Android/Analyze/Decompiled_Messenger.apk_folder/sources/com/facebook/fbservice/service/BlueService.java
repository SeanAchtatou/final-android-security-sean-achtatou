package com.facebook.fbservice.service;

import X.AnonymousClass0UN;
import X.AnonymousClass0lO;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.C000700l;
import X.C005505z;
import X.C25051Yd;
import X.C34711q3;
import android.content.Intent;
import android.os.IBinder;

public class BlueService extends AnonymousClass0lO {
    public AnonymousClass0UN A00;
    private C34711q3 A01;

    public IBinder onBind(Intent intent) {
        BlueServiceLogic blueServiceLogic = (BlueServiceLogic) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AVT, this.A00);
        blueServiceLogic.asBinder();
        return blueServiceLogic;
    }

    public void A0k() {
        int A04 = C000700l.A04(609419904);
        C005505z.A03("BlueService.onCreate", -518753481);
        try {
            super.A0k();
            AnonymousClass0UN r2 = new AnonymousClass0UN(2, AnonymousClass1XX.get(this));
            this.A00 = r2;
            if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r2)).Aem(282909497034759L)) {
                C34711q3 r0 = (C34711q3) AnonymousClass1XX.A03(AnonymousClass1Y3.ABI, this.A00);
                this.A01 = r0;
                r0.A02();
            }
        } finally {
            C005505z.A00(-341424697);
            C000700l.A0A(51356322, A04);
        }
    }

    public void A0l() {
        int A04 = C000700l.A04(1931519140);
        super.A0l();
        C34711q3 r1 = this.A01;
        if (r1 != null) {
            this.A01 = null;
            r1.A01();
        } else {
            ((BlueServiceLogic) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AVT, this.A00)).A03();
        }
        C000700l.A0A(-5095172, A04);
    }

    public void onRebind(Intent intent) {
        int A04 = C000700l.A04(-1128904757);
        super.onRebind(intent);
        C000700l.A0A(-2064956719, A04);
    }
}
