package com.facebook.fbservice.service;

import X.C000700l;
import X.C05360Yq;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public interface ICompletionHandler extends IInterface {

    public abstract class Stub extends Binder implements ICompletionHandler {

        public final class Proxy implements ICompletionHandler {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(1942643731);
                this.A00 = iBinder;
                C000700l.A09(422116867, A03);
            }

            public void BhE(OperationResult operationResult) {
                int A03 = C000700l.A03(-1150804591);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(C05360Yq.$const$string(413));
                    if (operationResult != null) {
                        obtain.writeInt(1);
                        operationResult.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1369832899, A03);
                }
            }

            public void BhG(OperationResult operationResult) {
                int A03 = C000700l.A03(1969291029);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(C05360Yq.$const$string(413));
                    if (operationResult != null) {
                        obtain.writeInt(1);
                        operationResult.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-197486710, A03);
                }
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(942509027);
                IBinder iBinder = this.A00;
                C000700l.A09(1525615751, A03);
                return iBinder;
            }
        }

        public static ICompletionHandler A00(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.facebook.fbservice.service.ICompletionHandler");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof ICompletionHandler)) {
                return new Proxy(iBinder);
            }
            return (ICompletionHandler) queryLocalInterface;
        }

        public Stub() {
            int A03 = C000700l.A03(1275859781);
            attachInterface(this, "com.facebook.fbservice.service.ICompletionHandler");
            C000700l.A09(83655131, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(-1077249399, C000700l.A03(-101075406));
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            int A03 = C000700l.A03(-1076387320);
            OperationResult operationResult = null;
            if (i == 1) {
                parcel.enforceInterface("com.facebook.fbservice.service.ICompletionHandler");
                if (parcel.readInt() != 0) {
                    operationResult = (OperationResult) OperationResult.CREATOR.createFromParcel(parcel);
                }
                BhG(operationResult);
                parcel2.writeNoException();
                C000700l.A09(325341519, A03);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("com.facebook.fbservice.service.ICompletionHandler");
                if (parcel.readInt() != 0) {
                    operationResult = (OperationResult) OperationResult.CREATOR.createFromParcel(parcel);
                }
                BhE(operationResult);
                parcel2.writeNoException();
                C000700l.A09(-245909772, A03);
                return true;
            } else if (i != 1598968902) {
                boolean onTransact = super.onTransact(i, parcel, parcel2, i2);
                C000700l.A09(-606832073, A03);
                return onTransact;
            } else {
                parcel2.writeString("com.facebook.fbservice.service.ICompletionHandler");
                C000700l.A09(489942589, A03);
                return true;
            }
        }
    }

    void BhE(OperationResult operationResult);

    void BhG(OperationResult operationResult);
}
