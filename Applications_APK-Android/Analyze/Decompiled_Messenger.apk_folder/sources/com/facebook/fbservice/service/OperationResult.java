package com.facebook.fbservice.service;

import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass0D4;
import X.AnonymousClass324;
import X.C103004vS;
import X.C12200oo;
import X.C14880uI;
import X.C30161hb;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.stringformat.StringFormatUtil;
import io.card.payment.BuildConfig;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class OperationResult implements Parcelable, Serializable {
    public static final OperationResult A00 = new OperationResult();
    public static final Parcelable.Creator CREATOR = new C30161hb();
    private static final long serialVersionUID = 1;
    public C14880uI errorCode;
    public String errorDescription;
    public Throwable errorThrowable;
    public Bundle resultDataBundle;
    public String resultDataString;
    public boolean success;

    public int describeContents() {
        return 0;
    }

    public static OperationResult A00(C14880uI r4) {
        Bundle bundle = new Bundle();
        bundle.putInt("resultType", AnonymousClass07B.A01.intValue());
        return new OperationResult(r4, r4.toString(), bundle, null);
    }

    public static OperationResult A01(C14880uI r2, Bundle bundle, Throwable th) {
        bundle.putInt("resultType", AnonymousClass07B.A01.intValue());
        return new OperationResult(r2, r2.toString(), bundle, th);
    }

    public static OperationResult A02(C14880uI r3, String str) {
        Bundle bundle = new Bundle();
        bundle.putInt("resultType", AnonymousClass07B.A01.intValue());
        return new OperationResult(r3, str, bundle, null);
    }

    public static OperationResult A03(C14880uI r3, Throwable th) {
        Bundle bundle = new Bundle();
        bundle.putInt("resultType", AnonymousClass07B.A01.intValue());
        return new OperationResult(r3, r3.toString(), bundle, th);
    }

    public static OperationResult A04(Object obj) {
        Integer num;
        if (obj instanceof String) {
            return new OperationResult((String) obj, null);
        }
        Bundle bundle = new Bundle();
        if (obj == null) {
            num = AnonymousClass07B.A00;
        } else if (obj instanceof Parcelable) {
            num = AnonymousClass07B.A01;
        } else {
            num = AnonymousClass07B.A0C;
        }
        bundle.putInt("resultType", num.intValue());
        if (obj instanceof Parcelable) {
            bundle.putParcelable("result", (Parcelable) obj);
        } else if (obj instanceof C12200oo) {
            AnonymousClass324.A09(bundle, "result", (C12200oo) obj);
        } else if (obj != null) {
            throw new UnsupportedOperationException("Can not create result for object " + obj);
        }
        return new OperationResult(null, bundle);
    }

    public static OperationResult A05(ArrayList arrayList) {
        Integer num;
        Bundle bundle = new Bundle();
        int i = 0;
        boolean z = false;
        if (arrayList == null) {
            z = true;
        }
        bundle.putSerializable("resultNull", Boolean.valueOf(z));
        if (arrayList != null) {
            bundle.putInt("resultSize", arrayList.size());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                Object next = it.next();
                String A09 = AnonymousClass08S.A09("resultType", i);
                if (next == null) {
                    num = AnonymousClass07B.A00;
                } else if (next instanceof Parcelable) {
                    num = AnonymousClass07B.A01;
                } else {
                    num = AnonymousClass07B.A0C;
                }
                bundle.putInt(A09, num.intValue());
                if (next instanceof Parcelable) {
                    bundle.putParcelable(AnonymousClass08S.A09("result", i), (Parcelable) next);
                } else if (next instanceof C12200oo) {
                    AnonymousClass324.A09(bundle, AnonymousClass08S.A09("result", i), (C12200oo) next);
                } else {
                    throw new UnsupportedOperationException("Can not create result for object " + next);
                }
                i++;
            }
        }
        return new OperationResult(null, bundle);
    }

    public static OperationResult A06(HashMap hashMap) {
        Bundle bundle = new Bundle();
        bundle.putInt("resultType", AnonymousClass07B.A01.intValue());
        bundle.putSerializable("result", new HashMap(hashMap));
        return new OperationResult(null, bundle);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        HashMap hashMap;
        objectOutputStream.writeBoolean(this.success);
        objectOutputStream.writeObject(this.resultDataString);
        Bundle bundle = this.resultDataBundle;
        if (bundle != null) {
            hashMap = (HashMap) bundle.getSerializable("result");
        } else {
            hashMap = null;
        }
        objectOutputStream.writeObject(hashMap);
        objectOutputStream.writeObject(this.errorCode);
        objectOutputStream.writeObject(this.errorDescription);
        objectOutputStream.writeObject(this.errorThrowable);
    }

    public Object A08() {
        if (this.resultDataBundle != null) {
            Integer num = AnonymousClass07B.A00(3)[this.resultDataBundle.getInt("resultType")];
            if (!AnonymousClass07B.A00.equals(num)) {
                if (AnonymousClass07B.A0C.equals(num)) {
                    return AnonymousClass324.A02(this.resultDataBundle, "result");
                }
                this.resultDataBundle.setClassLoader(getClass().getClassLoader());
                return this.resultDataBundle.get("result");
            }
        }
        return null;
    }

    public Object A09(Class cls) {
        HashMap hashMap;
        if (Parcelable.class.isAssignableFrom(cls)) {
            return A07();
        }
        if (Bundle.class.equals(cls)) {
            return this.resultDataBundle;
        }
        if (CharSequence.class.isAssignableFrom(cls)) {
            return this.resultDataString;
        }
        if (List.class.isAssignableFrom(cls)) {
            ArrayList A0A = A0A();
            if (A0A != null) {
                return A0A;
            }
        } else if (Map.class.isAssignableFrom(cls)) {
            Bundle bundle = this.resultDataBundle;
            if (bundle != null) {
                hashMap = (HashMap) bundle.getSerializable("result");
            } else {
                hashMap = null;
            }
            if (hashMap != null) {
                return hashMap;
            }
        } else {
            throw new IllegalArgumentException("Invalid result data type: " + cls);
        }
        throw new C103004vS();
    }

    public ArrayList A0A() {
        Object obj;
        Bundle bundle = this.resultDataBundle;
        if (bundle == null || bundle.getBoolean("resultNull")) {
            return null;
        }
        int i = this.resultDataBundle.getInt("resultSize");
        ArrayList arrayList = new ArrayList();
        Integer[] A002 = AnonymousClass07B.A00(3);
        for (int i2 = 0; i2 < i; i2++) {
            Integer num = A002[this.resultDataBundle.getInt(AnonymousClass08S.A09("resultType", i2))];
            if (AnonymousClass07B.A00.equals(num)) {
                arrayList.add(null);
            } else if (AnonymousClass07B.A0C.equals(num)) {
                arrayList.add(AnonymousClass324.A02(this.resultDataBundle, AnonymousClass08S.A09("result", i2)));
            } else {
                String A09 = AnonymousClass08S.A09("result", i2);
                Bundle bundle2 = this.resultDataBundle;
                if (bundle2 != null) {
                    bundle2.setClassLoader(getClass().getClassLoader());
                }
                Bundle bundle3 = this.resultDataBundle;
                if (bundle3 != null) {
                    obj = bundle3.get(A09);
                } else {
                    obj = null;
                }
                if (obj != null) {
                    arrayList.add(obj);
                } else {
                    throw new C103004vS();
                }
            }
        }
        return arrayList;
    }

    public String toString() {
        String bundle;
        String name;
        Boolean valueOf = Boolean.valueOf(this.success);
        String str = this.resultDataString;
        String str2 = BuildConfig.FLAVOR;
        if (str == null) {
            str = str2;
        }
        Bundle bundle2 = this.resultDataBundle;
        if (bundle2 == null) {
            bundle = str2;
        } else {
            bundle = bundle2.toString();
        }
        C14880uI r0 = this.errorCode;
        if (r0 == null) {
            name = str2;
        } else {
            name = r0.name();
        }
        String str3 = this.errorDescription;
        if (str3 == null) {
            str3 = str2;
        }
        Throwable th = this.errorThrowable;
        if (th != null) {
            str2 = AnonymousClass0D4.A01(th);
        }
        return StringFormatUtil.formatStrLocaleSafe("OperationResult success=%s, resultDataString=%s, resultDataBundle=%s, errorCode=%s, errorDescription=%s, exception=%s", valueOf, str, bundle, name, str3, str2);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.success ? 1 : 0);
        parcel.writeString(this.resultDataString);
        parcel.writeBundle(this.resultDataBundle);
        parcel.writeString(this.errorCode.toString());
        parcel.writeString(this.errorDescription);
        parcel.writeSerializable(this.errorThrowable);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.success = objectInputStream.readBoolean();
        this.resultDataString = (String) objectInputStream.readObject();
        Bundle bundle = new Bundle();
        this.resultDataBundle = bundle;
        bundle.putSerializable("result", (HashMap) objectInputStream.readObject());
        this.errorCode = (C14880uI) objectInputStream.readObject();
        this.errorDescription = (String) objectInputStream.readObject();
        this.errorThrowable = (Throwable) objectInputStream.readObject();
    }

    public Object A07() {
        Object A08 = A08();
        if (A08 != null) {
            return A08;
        }
        throw new C103004vS();
    }

    public OperationResult() {
        this.success = true;
        this.resultDataString = null;
        this.resultDataBundle = null;
        this.errorCode = C14880uI.A0B;
        this.errorDescription = null;
        this.errorThrowable = null;
    }

    private OperationResult(C14880uI r2, String str, Bundle bundle, Throwable th) {
        this.success = false;
        this.resultDataString = null;
        this.resultDataBundle = bundle;
        this.errorCode = r2;
        this.errorDescription = str;
        this.errorThrowable = th;
    }

    public OperationResult(Parcel parcel) {
        this.success = parcel.readInt() != 0;
        this.resultDataString = parcel.readString();
        this.resultDataBundle = parcel.readBundle(getClass().getClassLoader());
        this.errorCode = C14880uI.valueOf(parcel.readString());
        this.errorDescription = parcel.readString();
        this.errorThrowable = (Throwable) parcel.readSerializable();
    }

    public OperationResult(String str, Bundle bundle) {
        this.success = true;
        this.resultDataString = str;
        this.resultDataBundle = bundle;
        this.errorCode = C14880uI.A0B;
        this.errorDescription = null;
        this.errorThrowable = null;
    }

    public OperationResult(Throwable th) {
        this.success = false;
        this.resultDataString = null;
        this.errorCode = null;
        this.errorDescription = null;
        this.errorThrowable = th;
        Bundle bundle = new Bundle();
        this.resultDataBundle = bundle;
        bundle.putInt("resultType", AnonymousClass07B.A01.intValue());
    }
}
