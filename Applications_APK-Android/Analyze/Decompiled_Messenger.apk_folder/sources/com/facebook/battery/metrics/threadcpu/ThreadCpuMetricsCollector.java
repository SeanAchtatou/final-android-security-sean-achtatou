package com.facebook.battery.metrics.threadcpu;

import X.AnonymousClass08S;
import X.AnonymousClass0FM;
import X.AnonymousClass0FV;
import X.AnonymousClass0IO;
import X.AnonymousClass0KZ;
import X.AnonymousClass8TZ;
import X.AnonymousClass8Ta;
import X.C007907e;
import X.C010708t;
import X.C173757zo;
import android.os.Process;
import android.util.Pair;
import com.facebook.common.stringformat.StringFormatUtil;
import java.util.HashMap;
import java.util.Map;

public final class ThreadCpuMetricsCollector extends C007907e {
    private static AnonymousClass0FV A00(C173757zo r3) {
        AnonymousClass0FV r2 = new AnonymousClass0FV();
        r2.userTimeS = r3.A03;
        r2.systemTimeS = r3.A02;
        return r2;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A03() {
        return new AnonymousClass0IO();
    }

    public boolean A04(AnonymousClass0FM r9) {
        HashMap hashMap;
        AnonymousClass0IO r92 = (AnonymousClass0IO) r9;
        if (r92 != null) {
            try {
                Map A00 = AnonymousClass8Ta.A00();
                if (A00 == null) {
                    hashMap = null;
                } else {
                    hashMap = new HashMap();
                    for (Map.Entry entry : A00.entrySet()) {
                        C173757zo A01 = AnonymousClass8TZ.A01(AnonymousClass8TZ.A02(StringFormatUtil.formatStrLocaleSafe("/proc/%d/task/%s/stat", Integer.valueOf(Process.myPid()), entry.getKey())));
                        if (A01 != null) {
                            hashMap.put(entry.getKey(), new Pair(entry.getValue(), A01));
                        }
                    }
                }
            } catch (Exception e) {
                C010708t.A08(AnonymousClass8Ta.A00, "Error getting thread level CPU Usage data", e);
                hashMap = null;
            }
            if (hashMap == null) {
                return false;
            }
            r92.threadCpuMap.keySet().retainAll(hashMap.keySet());
            for (Map.Entry entry2 : hashMap.entrySet()) {
                try {
                    int parseInt = Integer.parseInt((String) entry2.getKey());
                    String str = (String) ((Pair) entry2.getValue()).first;
                    AnonymousClass0FV A002 = A00((C173757zo) ((Pair) entry2.getValue()).second);
                    HashMap hashMap2 = r92.threadCpuMap;
                    Integer valueOf = Integer.valueOf(parseInt);
                    if (hashMap2.containsKey(valueOf)) {
                        ((AnonymousClass0FV) ((Pair) r92.threadCpuMap.get(valueOf)).second).A0B(A002);
                    } else {
                        r92.threadCpuMap.put(valueOf, new Pair(str, A002));
                    }
                } catch (NumberFormatException e2) {
                    AnonymousClass0KZ.A00("com.facebook.battery.metrics.threadcpu.ThreadCpuMetricsCollector", AnonymousClass08S.A0J("Thread Id is not an integer: ", (String) entry2.getKey()), e2);
                }
            }
            return true;
        }
        throw new IllegalArgumentException("Null value passed to getSnapshot!");
    }
}
