package com.facebook.jni;

import X.AnonymousClass06F;
import X.AnonymousClass06G;
import X.AnonymousClass07A;
import X.AnonymousClass08S;
import X.AnonymousClass09P;
import X.AnonymousClass09Q;
import X.C03530Ok;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;

public final class NativeSoftErrorReporterProxy {
    public static ExecutorService sErrorReportingExecutorService;
    public static AnonymousClass09Q sErrorReportingGkReader;
    public static WeakReference sFbErrorReporterWeakReference;
    public static final LinkedList sSoftErrorCache = new LinkedList();

    public static native void generateNativeSoftError();

    public static synchronized void flushSoftErrorCacheAsync() {
        AnonymousClass09P r3;
        synchronized (NativeSoftErrorReporterProxy.class) {
            WeakReference weakReference = sFbErrorReporterWeakReference;
            if (!(weakReference == null || (r3 = (AnonymousClass09P) weakReference.get()) == null || sErrorReportingGkReader == null || sSoftErrorCache.isEmpty())) {
                ArrayList arrayList = new ArrayList();
                LinkedList linkedList = sSoftErrorCache;
                synchronized (linkedList) {
                    arrayList.addAll(linkedList);
                    linkedList.clear();
                }
                AnonymousClass07A.A04(sErrorReportingExecutorService, new C03530Ok(arrayList, r3), 576338814);
            }
        }
    }

    private NativeSoftErrorReporterProxy() {
    }

    public static void softReport(int i, String str, String str2, int i2) {
        softReport(i, str, str2, null, i2);
    }

    public static void softReport(int i, String str, String str2, Throwable th, int i2) {
        String str3;
        if (i != 1) {
            str3 = i != 2 ? "<level:unknown> " : "<level:mustfix> ";
        } else {
            str3 = "<level:warning> ";
        }
        String A0P = AnonymousClass08S.A0P("[Native] ", str3, str);
        synchronized (NativeSoftErrorReporterProxy.class) {
            LinkedList linkedList = sSoftErrorCache;
            synchronized (linkedList) {
                AnonymousClass06G A02 = AnonymousClass06F.A02(A0P, str2);
                A02.A03 = th;
                A02.A00 = i2;
                linkedList.addLast(A02.A00());
                while (linkedList.size() >= 50) {
                    linkedList.removeFirst();
                }
            }
        }
        flushSoftErrorCacheAsync();
    }
}
