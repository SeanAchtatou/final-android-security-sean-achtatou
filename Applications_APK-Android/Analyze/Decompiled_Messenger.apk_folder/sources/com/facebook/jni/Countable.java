package com.facebook.jni;

import X.AnonymousClass01q;
import X.C000700l;

public class Countable {
    private long mInstance = 0;

    public native void dispose();

    static {
        AnonymousClass01q.A08("fb");
    }

    public void finalize() {
        int A03 = C000700l.A03(-1205671323);
        dispose();
        super.finalize();
        C000700l.A09(-2037648308, A03);
    }
}
