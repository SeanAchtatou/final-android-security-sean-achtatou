package com.facebook.jni;

import X.AnonymousClass01q;

public class CpuCapabilitiesJni {
    public static native boolean nativeDeviceSupportsNeon();

    public static native boolean nativeDeviceSupportsVFPFP16();

    public static native boolean nativeDeviceSupportsX86();

    static {
        AnonymousClass01q.A08("fb");
    }
}
