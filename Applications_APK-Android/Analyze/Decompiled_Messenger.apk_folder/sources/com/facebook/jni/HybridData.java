package com.facebook.jni;

import X.AnonymousClass01u;
import X.C011208y;

public class HybridData {
    private Destructor mDestructor = new Destructor(this);

    public class Destructor extends C011208y {
        public volatile long mNativePointer;

        public static native void deleteNative(long j);

        public final void destruct() {
            deleteNative(this.mNativePointer);
            this.mNativePointer = 0;
        }

        public Destructor(Object obj) {
            super(obj);
        }
    }

    public synchronized void resetNative() {
        this.mDestructor.destruct();
    }

    static {
        AnonymousClass01u.A01("fbjni");
    }

    public boolean isValid() {
        if (this.mDestructor.mNativePointer != 0) {
            return true;
        }
        return false;
    }
}
