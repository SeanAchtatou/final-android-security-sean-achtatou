package com.facebook.jni;

import X.AnonymousClass01u;

public class ThreadScopeSupport {
    private static native void runStdFunctionImpl(long j);

    static {
        AnonymousClass01u.A01("fbjni");
    }

    private static void runStdFunction(long j) {
        runStdFunctionImpl(j);
    }
}
