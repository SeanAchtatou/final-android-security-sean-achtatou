package com.facebook.appcomponentmanager;

import java.io.Serializable;

public class AppComponentManagerProfiledRun implements Serializable {
    private static final long serialVersionUID = 1;
    private long mDurationInMilliseconds;
    private String mTrigger;
    private int mUpdatedComponents;

    public AppComponentManagerProfiledRun() {
    }

    public AppComponentManagerProfiledRun(String str, int i, long j) {
        this.mUpdatedComponents = i;
        this.mDurationInMilliseconds = j;
        this.mTrigger = str;
    }
}
