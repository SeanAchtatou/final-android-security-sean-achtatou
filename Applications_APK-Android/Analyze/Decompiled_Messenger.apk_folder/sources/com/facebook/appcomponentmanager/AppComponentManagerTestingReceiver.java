package com.facebook.appcomponentmanager;

import X.C000700l;
import X.C010708t;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class AppComponentManagerTestingReceiver extends BroadcastReceiver {
    private static void A00(PackageManager packageManager, ComponentInfo[] componentInfoArr) {
        for (ComponentInfo componentInfo : componentInfoArr) {
            int componentEnabledSetting = packageManager.getComponentEnabledSetting(new ComponentName(componentInfo.packageName, componentInfo.name));
            if (!(componentEnabledSetting == 0 || componentEnabledSetting == 1 || componentEnabledSetting == 2)) {
                C010708t.A0P("AppComponentManagerTestingReceiver", "%s is marked as currently in state %d, which is not an expected state. Conservatively assuming it's disabled.", componentInfo.name, Integer.valueOf(componentEnabledSetting));
            }
            C010708t.A01.BFj(4);
        }
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(925222215);
        String action = intent.getAction();
        if (action == null) {
            C010708t.A0J("AppComponentManagerTestingReceiver", "Intent Action was null. Please supply an action.");
            C000700l.A0D(intent, 161064986, A01);
            return;
        }
        char c = 65535;
        int hashCode = action.hashCode();
        if (hashCode != -1618603518) {
            if (hashCode == -405873831 && action.equals("com.facebook.appcomponentmanager.ACTION_PRINT_COMPONENTS")) {
                c = 0;
            }
        } else if (action.equals("com.facebook.appcomponentmanager.ACTION_GET_CURRENT_ENABLE_STAGE")) {
            c = 1;
        }
        if (c == 0) {
            PackageManager packageManager = context.getPackageManager();
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 33423);
                A00(packageManager, packageInfo.activities);
                A00(packageManager, packageInfo.receivers);
                A00(packageManager, packageInfo.services);
                A00(packageManager, packageInfo.providers);
            } catch (PackageManager.NameNotFoundException e) {
                C010708t.A0R("AppComponentManagerTestingReceiver", e, "Ran into NameNotFoundException");
            }
        } else if (c != 1) {
            C010708t.A0P("AppComponentManagerTestingReceiver", "Intent Action %s is not a known action.", action);
        } else {
            PackageManager packageManager2 = context.getPackageManager();
            if (packageManager2 == null) {
                C010708t.A0I("AppComponentManagerTestingReceiver", "PackageManager received from context was null. Aborting.");
            } else if (packageManager2.getComponentEnabledSetting(new ComponentName(context.getPackageName(), SecondEnableStageTestReceiver.class.getCanonicalName())) == 1) {
                C010708t.A0J("AppComponentManagerTestingReceiver", "Enable Stage: Warm Pre-TOS.");
            } else {
                C010708t.A0J("AppComponentManagerTestingReceiver", "Enable Stage: Cold Pre-TOS.");
            }
        }
        C000700l.A0D(intent, 1593703214, A01);
    }
}
