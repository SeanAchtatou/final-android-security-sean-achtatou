package com.facebook.appcomponentmanager.fb;

import X.AnonymousClass0KJ;
import X.C010708t;
import android.accounts.AccountManager;
import android.content.Context;
import io.card.payment.BuildConfig;

public class FbAppComponentReceiver extends AnonymousClass0KJ {
    public boolean A00(Context context) {
        String packageName = context.getPackageName();
        String str = "com.facebook.auth.login";
        if (!"com.facebook.wakizashi".equals(packageName) && !"com.facebook.katana".equals(packageName)) {
            str = "com.facebook.orca".equals(packageName) ? "com.facebook.messenger" : BuildConfig.FLAVOR;
        }
        try {
            if (AccountManager.get(context).getAccountsByType(str).length > 0) {
                return true;
            }
            return false;
        } catch (RuntimeException e) {
            C010708t.A0L("FbAppComponentReceiver", "Unexpected error while getting accounts", e);
            return false;
        }
    }
}
