package com.facebook.appcomponentmanager;

import X.AnonymousClass0E1;
import X.AnonymousClass0HH;
import X.AnonymousClass0KN;
import X.AnonymousClass0KP;
import X.C010708t;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.facebook.acra.ErrorReporter;
import java.io.File;

public class AppComponentManagerService extends AnonymousClass0E1 {
    public static final int A00 = 137875812;

    public void onHandleWork(Intent intent) {
        intent.getAction();
        if ("com.facebook.appcomponentmanager.ACTION_ENABLE_COMPONENTS".equals(intent.getAction())) {
            try {
                AnonymousClass0HH.A03(this, 3, "app_update");
                Intent intent2 = new Intent("com.facebook.appcomponentmanager.ENABLING_CMPS_DONE");
                intent2.setPackage(getPackageName());
                sendBroadcast(intent2);
            } catch (RuntimeException e) {
                e = e;
                C010708t.A0L("AppComponentManagerService", "Exception while enabling components. Aborting.", e);
                ErrorReporter.getInstance().handleException(e);
            }
        } else if ("com.facebook.appcomponentmanager.ACTION_EFNORCE_MANIFEST_CONSISTENCY".equals(intent.getAction())) {
            PackageManager packageManager = getPackageManager();
            AnonymousClass0KP r4 = new AnonymousClass0KP();
            File file = new File(getApplicationInfo().sourceDir);
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
                AnonymousClass0KN A04 = r4.A04(file);
                int i = packageInfo.versionCode;
                String num = Integer.toString(i);
                String str = A04.A01;
                if (!num.equals(str) || !packageInfo.versionName.equals(A04.A02) || !packageInfo.packageName.equals(A04.A00)) {
                    throw new IllegalStateException("PackageInfo{package=" + packageInfo.packageName + "," + "versionCode=" + i + "," + "versionName=" + packageInfo.versionName + "} ," + "Manifest{package=" + A04.A00 + ", " + "versionCode=" + str + ", " + "versionName=" + A04.A02 + ", " + "activities=" + A04.A03.size() + ", " + "receivers=" + A04.A05.size() + ", " + "services=" + A04.A06.size() + ", " + "providers=" + A04.A04.size() + "}");
                }
            } catch (Throwable th) {
                e = th;
                ErrorReporter.getInstance().handleException(e);
            }
        }
    }
}
