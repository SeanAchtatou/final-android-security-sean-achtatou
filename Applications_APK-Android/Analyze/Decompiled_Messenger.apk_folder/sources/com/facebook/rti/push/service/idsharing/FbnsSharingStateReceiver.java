package com.facebook.rti.push.service.idsharing;

import X.C000700l;
import X.C010708t;
import X.C012309k;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import io.card.payment.BuildConfig;

public class FbnsSharingStateReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(331409989);
        if (intent == null) {
            C000700l.A0D(intent, 814868148, A01);
            return;
        }
        String action = intent.getAction();
        if (!new C012309k(context, null).A04(intent)) {
            C010708t.A0I("FbnsSharingStateReceiver", "Rejecting device credentials sharing request due to failed auth");
            C000700l.A0D(intent, 1650811313, A01);
            return;
        }
        if ("com.facebook.rti.fbns.intent.SHARE_IDS".equals(action)) {
            Bundle resultExtras = getResultExtras(true);
            resultExtras.putString("/settings/mqtt/id/mqtt_device_id", BuildConfig.FLAVOR);
            resultExtras.putString("/settings/mqtt/id/mqtt_device_secret", BuildConfig.FLAVOR);
            resultExtras.putLong("/settings/mqtt/id/timestamp", Long.MAX_VALUE);
            setResult(-1, null, resultExtras);
        }
        C000700l.A0D(intent, -1840099475, A01);
    }
}
