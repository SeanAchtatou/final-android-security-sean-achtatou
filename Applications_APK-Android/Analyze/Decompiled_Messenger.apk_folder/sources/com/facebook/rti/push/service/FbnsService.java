package com.facebook.rti.push.service;

import X.AnonymousClass02C;
import X.AnonymousClass07B;
import X.AnonymousClass08E;
import X.AnonymousClass08I;
import X.AnonymousClass08J;
import X.AnonymousClass08S;
import X.AnonymousClass09P;
import X.AnonymousClass0A1;
import X.AnonymousClass0A9;
import X.AnonymousClass0AB;
import X.AnonymousClass0AC;
import X.AnonymousClass0AD;
import X.AnonymousClass0AG;
import X.AnonymousClass0AH;
import X.AnonymousClass0AX;
import X.AnonymousClass0AZ;
import X.AnonymousClass0B0;
import X.AnonymousClass0B2;
import X.AnonymousClass0B4;
import X.AnonymousClass0B7;
import X.AnonymousClass0CI;
import X.AnonymousClass0DD;
import X.AnonymousClass0DZ;
import X.AnonymousClass0ED;
import X.AnonymousClass0EE;
import X.AnonymousClass0H5;
import X.AnonymousClass0H6;
import X.AnonymousClass0HA;
import X.AnonymousClass0O0;
import X.AnonymousClass0O1;
import X.AnonymousClass0O2;
import X.AnonymousClass0O3;
import X.AnonymousClass0O7;
import X.AnonymousClass0OF;
import X.AnonymousClass0OG;
import X.AnonymousClass0OL;
import X.AnonymousClass0OM;
import X.AnonymousClass0ON;
import X.AnonymousClass0OO;
import X.AnonymousClass0OP;
import X.AnonymousClass0OQ;
import X.AnonymousClass0OT;
import X.AnonymousClass0OX;
import X.AnonymousClass0QF;
import X.AnonymousClass0QQ;
import X.AnonymousClass0QV;
import X.AnonymousClass0QW;
import X.AnonymousClass0RF;
import X.AnonymousClass0RG;
import X.AnonymousClass0RI;
import X.AnonymousClass0RJ;
import X.AnonymousClass0RK;
import X.AnonymousClass0RM;
import X.AnonymousClass0RN;
import X.AnonymousClass0RR;
import X.AnonymousClass0RS;
import X.AnonymousClass0RT;
import X.AnonymousClass0SB;
import X.C000700l;
import X.C009207y;
import X.C010708t;
import X.C012309k;
import X.C01410Ac;
import X.C01430Ae;
import X.C01460Ai;
import X.C01600Aw;
import X.C01740Bl;
import X.C01770Bo;
import X.C02020Cn;
import X.C03080Hx;
import X.C03430Ns;
import X.C03500Nz;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.push.fbns.ipc.FbnsAIDLRequest;
import com.facebook.push.fbns.ipc.FbnsAIDLResult;
import com.facebook.push.fbns.ipc.IFbnsAIDLService;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;
import org.json.JSONObject;

public class FbnsService extends AnonymousClass0AD {
    private static FbnsService A09;
    public static final List A0A = new AnonymousClass0H5();
    public static final List A0B = new AnonymousClass0H6();
    public C012309k A00;
    public AnonymousClass0RS A01;
    public AnonymousClass0OO A02;
    public AnonymousClass0ON A03;
    public AnonymousClass0RK A04;
    public AnonymousClass0RJ A05;
    public AnonymousClass0RF A06;
    public String A07;
    private final IFbnsAIDLService.Stub A08 = new IFbnsAIDLService.Stub() {
        private final Map A00;

        {
            int A03 = C000700l.A03(-1610380407);
            HashMap hashMap = new HashMap();
            this.A00 = hashMap;
            C03080Hx r0 = C03080Hx.GET_PREF_BASED_CONFIG;
            AnonymousClass0RR r2 = C03430Ns.A02;
            hashMap.put(r0, r2);
            this.A00.put(C03080Hx.SET_PREF_BASED_CONFIG, r2);
            Map map = this.A00;
            C03080Hx r1 = C03080Hx.GET_APPS_STATISTICS;
            map.put(r1, new AnonymousClass0OX());
            Map map2 = this.A00;
            AnonymousClass0RR r22 = C03430Ns.A01;
            map2.put(r1, r22);
            this.A00.put(C03080Hx.GET_ANALYTICS_CONFIG, r22);
            this.A00.put(C03080Hx.SET_ANALYTICS_CONFIG, r22);
            this.A00.put(C03080Hx.GET_FLYTRAP_REPORT, new AnonymousClass0O0());
            Map map3 = this.A00;
            C03080Hx r02 = C03080Hx.GET_PREF_IDS;
            AnonymousClass0RR r23 = C03430Ns.A03;
            map3.put(r02, r23);
            this.A00.put(C03080Hx.SET_PREF_IDS, r23);
            C000700l.A09(887030966, A03);
        }

        private AnonymousClass0RR A00(FbnsAIDLRequest fbnsAIDLRequest, boolean z) {
            int i;
            int A03 = C000700l.A03(-1209262015);
            if (fbnsAIDLRequest == null || (i = fbnsAIDLRequest.A00) < 0) {
                C010708t.A0I("FbnsService", "Invalid FbnsAIDLRequest");
                IllegalArgumentException illegalArgumentException = new IllegalArgumentException("FbnsService received invalid FbnsAIDLRequest");
                C000700l.A09(-81278191, A03);
                throw illegalArgumentException;
            }
            C03080Hx r4 = (C03080Hx) C03080Hx.A00.get(Integer.valueOf(i));
            if (r4 == null) {
                r4 = C03080Hx.NOT_EXIST;
            }
            if (r4 == C03080Hx.NOT_EXIST) {
                IllegalArgumentException illegalArgumentException2 = new IllegalArgumentException("FbnsService operation not found");
                C000700l.A09(1808603397, A03);
                throw illegalArgumentException2;
            } else if (r4.mHasReturn == z) {
                AnonymousClass0RR r1 = (AnonymousClass0RR) this.A00.get(r4);
                if (r1 != null) {
                    C000700l.A09(-2098459803, A03);
                    return r1;
                }
                IllegalArgumentException illegalArgumentException3 = new IllegalArgumentException("FbnsService does not implement operation" + r4);
                C000700l.A09(-1264357490, A03);
                throw illegalArgumentException3;
            } else {
                C010708t.A0I("FbnsService", "FbnsAIDLOperation incorrect return type");
                IllegalArgumentException illegalArgumentException4 = new IllegalArgumentException("FbnsService operation incorrect return type");
                C000700l.A09(1328292942, A03);
                throw illegalArgumentException4;
            }
        }

        public FbnsAIDLResult BzG(FbnsAIDLRequest fbnsAIDLRequest) {
            int A03 = C000700l.A03(-242637195);
            AnonymousClass0RR A002 = A00(fbnsAIDLRequest, true);
            FbnsService fbnsService = FbnsService.this;
            Bundle bundle = fbnsAIDLRequest.A00;
            if (bundle == null) {
                bundle = Bundle.EMPTY;
            }
            FbnsAIDLResult fbnsAIDLResult = new FbnsAIDLResult(A002.AXc(fbnsService, bundle));
            C000700l.A09(-1500551814, A03);
            return fbnsAIDLResult;
        }

        public void CK8(FbnsAIDLRequest fbnsAIDLRequest) {
            int A03 = C000700l.A03(809272888);
            AnonymousClass0RR A002 = A00(fbnsAIDLRequest, false);
            FbnsService fbnsService = FbnsService.this;
            Bundle bundle = fbnsAIDLRequest.A00;
            if (bundle == null) {
                bundle = Bundle.EMPTY;
            }
            A002.AXk(fbnsService, bundle);
            C000700l.A09(-1208654520, A03);
        }
    };

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A06(com.facebook.rti.push.service.FbnsService r11, java.lang.String r12, java.lang.String r13, X.AnonymousClass0SR r14) {
        /*
            r0 = 0
            if (r0 == 0) goto L_0x0064
            X.0RF r0 = r11.A06
            X.0AW r1 = r0.A00
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            X.0B0 r0 = r1.AbP(r0)
            r9 = r13
            X.0RG r0 = X.AnonymousClass0RF.A00(r13, r0)
            X.0RO r5 = new X.0RO
            r4 = 0
            if (r0 == 0) goto L_0x0025
            java.lang.String r7 = r0.A03
        L_0x0019:
            if (r0 == 0) goto L_0x0023
            java.lang.String r8 = r0.A01
        L_0x001d:
            r10 = r14
            r6 = r12
            r5.<init>(r6, r7, r8, r9, r10)
            goto L_0x0027
        L_0x0023:
            r8 = r4
            goto L_0x001d
        L_0x0025:
            r7 = r4
            goto L_0x0019
        L_0x0027:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0064 }
            r2.<init>()     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r1 = r5.A02     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r0 = "nid"
            r2.putOpt(r0, r1)     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r1 = r5.A04     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r0 = "t"
            r2.putOpt(r0, r1)     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r1 = r5.A01     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r0 = "aid"
            r2.putOpt(r0, r1)     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r1 = r5.A03     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r0 = "pn"
            r2.putOpt(r0, r1)     // Catch:{ JSONException -> 0x0064 }
            X.0SR r0 = r5.A00     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r1 = r0.name()     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r0 = "r"
            r2.putOpt(r0, r1)     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r0 = r2.toString()     // Catch:{ JSONException -> 0x0064 }
            X.0AH r3 = r11.A09     // Catch:{  }
            java.lang.String r2 = "/fbns_msg_ack"
            byte[] r1 = X.AnonymousClass0ED.A00(r0)     // Catch:{  }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{  }
            r3.A08(r2, r1, r0, r4)     // Catch:{  }
        L_0x0064:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.rti.push.service.FbnsService.A06(com.facebook.rti.push.service.FbnsService, java.lang.String, java.lang.String, X.0SR):void");
    }

    public String A0J() {
        return "FBNS_ALWAYS";
    }

    public void A0N() {
        Integer num = null;
        List<AnonymousClass0RG> A042 = this.A06.A04();
        this.A06.A05();
        this.A01.A02(AnonymousClass07B.A0B, String.valueOf(A042.size()));
        AnonymousClass0B0 AbP = this.A0A.A03.AbP(AnonymousClass07B.A04);
        if (AbP.contains("DELIVERY_RETRY_INTERVAL")) {
            num = Integer.valueOf(AbP.getInt("DELIVERY_RETRY_INTERVAL", 300));
        }
        A0S(C01770Bo.A06, new AnonymousClass08E(null, 0, null, num));
        for (AnonymousClass0RG r3 : A042) {
            Intent intent = new Intent("com.facebook.rti.fbns.intent.REGISTER");
            intent.putExtra("pkg_name", r3.A02);
            intent.putExtra("appid", r3.A01);
            intent.setClassName(getPackageName(), getClass().getName());
            A04(intent);
        }
    }

    public boolean A0a(Intent intent) {
        if (intent != null) {
            if (!getPackageName().equals(C012309k.A00(intent))) {
                this.A01.A06(intent.toString());
                return false;
            }
        }
        return true;
    }

    public synchronized void A0b(ArrayList arrayList) {
        for (AnonymousClass0RG r0 : this.A06.A04()) {
            arrayList.add(r0.A02);
        }
    }

    private static Intent A01(String str, String str2, String str3) {
        Intent intent = new Intent("com.facebook.rti.fbns.intent.RECEIVE");
        intent.setPackage(str);
        intent.addCategory(str);
        intent.putExtra("receive_type", str2);
        if (str3 != null) {
            intent.putExtra("data", str3);
        }
        return intent;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0142, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0145;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A04(android.content.Intent r10) {
        /*
            r9 = this;
            java.lang.String r0 = "pkg_name"
            java.lang.String r3 = r10.getStringExtra(r0)
            java.lang.String r0 = "appid"
            java.lang.String r2 = r10.getStringExtra(r0)
            java.lang.String r1 = "local_generation"
            r0 = 0
            boolean r6 = r10.getBooleanExtra(r1, r0)
            X.0RK r0 = r9.A04
            r0.A00(r3)
            java.util.concurrent.atomic.AtomicBoolean r0 = r9.A0C
            boolean r0 = r0.get()
            r4 = 0
            if (r0 != 0) goto L_0x002f
            java.lang.String r1 = "FbnsService"
            java.lang.String r0 = "service/register/not_started"
            X.C010708t.A0I(r1, r0)
            X.0RS r1 = r9.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            r1.A02(r0, r4)
        L_0x002f:
            X.0RS r1 = r9.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A07
            r1.A02(r0, r3)
            X.0RF r0 = r9.A06
            java.lang.String r5 = r0.A03(r3)
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 != 0) goto L_0x0058
            X.0RK r0 = r9.A04
            r0.A00(r3)
            java.lang.String r0 = "registered"
            android.content.Intent r0 = A01(r3, r0, r5)
            r9.A05(r0)
            X.0RS r1 = r9.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.A02(r0, r4)
            return
        L_0x0058:
            if (r6 == 0) goto L_0x015c
            java.lang.String r0 = r9.getPackageName()
            boolean r0 = X.AnonymousClass0A9.A01(r0)
            if (r0 == 0) goto L_0x015c
            java.util.List r0 = X.AnonymousClass0A9.A00
            boolean r0 = r0.contains(r3)
            if (r0 == 0) goto L_0x0144
            X.0AH r0 = r9.A09
            X.0AQ r0 = r0.A0E
            X.0Bu r0 = r0.Arn()
            java.lang.Object r1 = r0.first
            java.lang.String r1 = (java.lang.String) r1
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0144
            java.lang.String r0 = r9.A07
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0144
            X.0RU r8 = new X.0RU
            java.lang.String r0 = r9.A07
            r8.<init>(r0, r1)
            java.lang.String r0 = r8.A01
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00fc
            java.lang.String r0 = r8.A00
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00fc
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 != 0) goto L_0x00fc
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r0 = "{"
            r5.<init>(r0)
            java.lang.String r0 = "\"pn\":"
            r5.append(r0)
            java.lang.String r6 = "\""
            java.lang.String r0 = X.AnonymousClass08S.A0P(r6, r3, r6)
            r5.append(r0)
            java.lang.String r7 = ","
            r5.append(r7)
            java.lang.String r0 = "\"di\":"
            r5.append(r0)
            java.lang.String r1 = r8.A01
            java.lang.String r0 = X.AnonymousClass08S.A0P(r6, r1, r6)
            r5.append(r0)
            r5.append(r7)
            java.lang.String r0 = "\"ai\":"
            r5.append(r0)
            java.lang.String r0 = "567310203415052"
            r5.append(r0)
            r5.append(r7)
            java.lang.String r0 = "\"ck\":"
            r5.append(r0)
            java.lang.String r1 = r8.A00
            java.lang.String r0 = X.AnonymousClass08S.A0P(r6, r1, r6)
            r5.append(r0)
            java.lang.String r0 = "}"
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            byte[] r1 = r0.getBytes()     // Catch:{ AssertionError -> 0x00fc }
            r0 = 2
            java.lang.String r1 = android.util.Base64.encodeToString(r1, r0)     // Catch:{ AssertionError -> 0x00fc }
            goto L_0x00fd
        L_0x00fc:
            r1 = r4
        L_0x00fd:
            java.lang.String r7 = "fbns-b64"
            if (r1 == 0) goto L_0x015a
            if (r7 == 0) goto L_0x015a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r0 = "{"
            r6.<init>(r0)
            java.lang.String r0 = "\"k\":"
            r6.append(r0)
            java.lang.String r5 = "\""
            java.lang.String r0 = X.AnonymousClass08S.A0P(r5, r1, r5)
            r6.append(r0)
            java.lang.String r1 = ","
            r6.append(r1)
            java.lang.String r0 = "\"v\":"
            r6.append(r0)
            r0 = 0
            r6.append(r0)
            r6.append(r1)
            java.lang.String r0 = "\"t\":"
            r6.append(r0)
            java.lang.String r0 = X.AnonymousClass08S.A0P(r5, r7, r5)
            r6.append(r0)
            java.lang.String r0 = "}"
            r6.append(r0)
            java.lang.String r1 = r6.toString()
        L_0x013e:
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 == 0) goto L_0x0145
        L_0x0144:
            r1 = r4
        L_0x0145:
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x015c
            X.0RK r0 = r9.A04
            r0.A00(r3)
            java.lang.String r0 = "registered"
            android.content.Intent r0 = A01(r3, r0, r1)
            r9.A05(r0)
            return
        L_0x015a:
            r1 = r4
            goto L_0x013e
        L_0x015c:
            android.content.ComponentName r0 = r10.getComponent()
            java.lang.String r0 = r0.getClassName()
            r9.A08(r3, r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.rti.push.service.FbnsService.A04(android.content.Intent):void");
    }

    private void A07(Integer num, AnonymousClass0RN r16, String str) {
        AnonymousClass0RS r4 = this.A01;
        AnonymousClass0RN r0 = r16;
        String str2 = r0.A02;
        String str3 = r0.A04;
        long j = this.A00;
        boolean A002 = this.A07.A00();
        long j2 = this.A07.A04.get();
        Map A003 = C01740Bl.A00("event_type", AnonymousClass0RT.A00(num));
        String str4 = str;
        if (!TextUtils.isEmpty(str4)) {
            A003.put("event_extra_info", str4);
        }
        if (!TextUtils.isEmpty(str2)) {
            A003.put(TraceFieldType.IsBuffered, str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            A003.put("dpn", str3);
        }
        long elapsedRealtime = SystemClock.elapsedRealtime();
        A003.put("s_boot_ms", String.valueOf(elapsedRealtime));
        A003.put("s_svc_ms", String.valueOf(elapsedRealtime - r4.A00));
        A003.put("s_mqtt_ms", String.valueOf(elapsedRealtime - j));
        A003.put("s_net_ms", String.valueOf(elapsedRealtime - r4.A01.A02()));
        if (j2 > 0) {
            A003.put("is_scr_on", String.valueOf(A002));
            A003.put("s_scr_ms", String.valueOf(elapsedRealtime - j2));
        }
        AnonymousClass0RS.A01(r4, "fbns_message_event", A003);
    }

    private void A09(String str, String str2, String str3) {
        int i;
        AnonymousClass0RI r3 = new AnonymousClass0RI(str, str2, str3);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("tk", r3.A02);
            jSONObject.putOpt("pn", r3.A01);
            jSONObject.putOpt("aid", r3.A00);
            String jSONObject2 = jSONObject.toString();
            try {
                i = this.A09.A08("/fbns_unreg_req", AnonymousClass0ED.A00(jSONObject2), AnonymousClass07B.A01, new AnonymousClass0OM(this));
            } catch (AnonymousClass0CI unused) {
                i = -1;
            }
            if (i == -1) {
                this.A01.A02(AnonymousClass07B.A08, null);
            }
        } catch (JSONException e) {
            C010708t.A0R("FbnsService", e, "service/unregister/serialization_exception");
            this.A01.A02(AnonymousClass07B.A0o, null);
        }
    }

    public void A0F(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        try {
            printWriter.println(AnonymousClass08S.A0P("[ ", "FbnsService", " ]"));
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            A0c(arrayList, arrayList2);
            ArrayList arrayList3 = new ArrayList();
            A0b(arrayList3);
            printWriter.println("validCompatibleApps=" + arrayList);
            printWriter.println("enabledCompatibleApps=" + arrayList2);
            printWriter.println("registeredApps=" + arrayList3);
            printWriter.println("notificationCounter=" + this.A05.A01);
        } catch (Exception unused) {
        }
        super.A0F(fileDescriptor, printWriter, strArr);
    }

    public AnonymousClass0AX A0H() {
        long j;
        FbnsService fbnsService = A09;
        if (fbnsService != null) {
            fbnsService.A0K();
        }
        A09 = this;
        this.A02 = new AnonymousClass0OO(getApplicationContext());
        AnonymousClass0OL r39 = new AnonymousClass0OL();
        AnonymousClass0OG r47 = new AnonymousClass0OG();
        AnonymousClass0OF r46 = new AnonymousClass0OF();
        AnonymousClass09P A0G = A0G();
        C012309k r9 = new C012309k(this, A0G);
        AnonymousClass0QF r8 = new AnonymousClass0QF(this);
        AnonymousClass0O1 r10 = new AnonymousClass0O1(this, r8);
        this.A07 = r10.getDeviceId();
        AnonymousClass0OQ r7 = new AnonymousClass0OQ(this);
        SharedPreferences A002 = AnonymousClass0B2.A00(this, AnonymousClass07B.A01);
        int intValue = ((Integer) AnonymousClass0HA.A05.A00(A002, -1)).intValue();
        if (intValue < 0 || intValue > 10000) {
            boolean z = true;
            int i = 1;
            if (!(!C01600Aw.A00(this).A02)) {
                i = 10000;
            }
            if (new Random().nextInt(10000) >= i) {
                z = false;
            }
            Integer valueOf = Integer.valueOf(i);
            Boolean valueOf2 = Boolean.valueOf(z);
            SharedPreferences.Editor edit = A002.edit();
            AnonymousClass0HA.A05.A01(edit, valueOf);
            AnonymousClass0HA.A07.A01(edit, valueOf2);
            AnonymousClass0B4.A00(edit);
        }
        int intValue2 = ((Integer) AnonymousClass0HA.A06.A00(A002, -1)).intValue();
        int i2 = 10000;
        if (intValue2 < 0 || intValue2 > 10000) {
            if (!C01600Aw.A00(this).A02) {
                i2 = 1;
            }
            Integer valueOf3 = Integer.valueOf(i2);
            SharedPreferences.Editor edit2 = A002.edit();
            AnonymousClass0HA.A06.A01(edit2, valueOf3);
            AnonymousClass0B4.A00(edit2);
        } else {
            i2 = intValue2;
        }
        try {
            j = Long.valueOf(Long.parseLong(A002.getString(AnonymousClass0HA.A01.mPrefKey, null)));
        } catch (NumberFormatException unused) {
            j = -1L;
        }
        boolean z2 = false;
        if (new Random().nextInt(10000) < i2) {
            z2 = true;
        }
        AnonymousClass0O7 r6 = new AnonymousClass0O7(A002.getBoolean(AnonymousClass0HA.A07.mPrefKey, false), A002);
        String A003 = C01430Ae.A00(A0I());
        AnonymousClass0O3 r5 = new AnonymousClass0O3(r7);
        String deviceId = r10.getDeviceId();
        String AdI = r10.AdI();
        C01600Aw A004 = C01600Aw.A00(this);
        C01460Ai r12 = new C01460Ai(this, A004, AdI, null, null);
        AnonymousClass0QW r3 = new AnonymousClass0QW(deviceId);
        AnonymousClass0O3 r19 = r5;
        AnonymousClass0O7 r20 = r6;
        SharedPreferences sharedPreferences = A002;
        AnonymousClass0QW r22 = r3;
        AnonymousClass0QV r16 = new AnonymousClass0QV(this, A003, r19, r20, sharedPreferences, r22, r12.A01(), A004.A01, A004.A00, "725056107548211", "0e20c3123a90c76c02c901b7415ff67f", "567310203415052");
        AnonymousClass0EE r42 = new AnonymousClass0EE();
        Integer A0I = A0I();
        AnonymousClass0AH r32 = new AnonymousClass0AH();
        AnonymousClass0AG r62 = this.A0D;
        C03500Nz r4 = new C03500Nz(r8);
        AnonymousClass0DZ r36 = new AnonymousClass0DZ();
        Handler handler = new Handler(Looper.getMainLooper());
        AnonymousClass0QQ r41 = new AnonymousClass0QQ();
        AnonymousClass0OO r52 = this.A02;
        C01410Ac r29 = new C01410Ac(this, A0I, r32, r62, r10, r4, r36, r7, null, r39, handler, r41, r42, A0G, r16, null, r46, r47, false, new AnonymousClass0OP(r7), new AnonymousClass0AZ(), null, "567310203415052", r46, r8, false, z2, false, false, null, r52, false, null, false, false, 0, false, false, -1, -1, 0, -1, j, false, false, false, null);
        AnonymousClass0OT r1 = new AnonymousClass0OT();
        r1.A02(r9, r29);
        return r1;
    }

    public void A0Q(int i) {
        this.A03.A03().A00.set(((long) i) * 1000);
    }

    public void A0T(AnonymousClass0SB r7) {
        if (AnonymousClass0SB.A03.equals(r7)) {
            boolean z = false;
            if (System.currentTimeMillis() - this.A06.A00.AbP(AnonymousClass07B.A0N).getLong("auto_reg_retry", 0) > 86400000) {
                z = true;
            }
            if (z) {
                AnonymousClass0DD AY8 = this.A06.A00.AbP(AnonymousClass07B.A0N).AY8();
                AY8.BzB("auto_reg_retry", System.currentTimeMillis());
                AY8.commit();
                List<AnonymousClass0RG> A042 = this.A06.A04();
                this.A06.A05();
                this.A01.A02(AnonymousClass07B.A06, String.valueOf(A042.size()));
                for (AnonymousClass0RG r3 : A042) {
                    Intent intent = new Intent("com.facebook.rti.fbns.intent.REGISTER");
                    intent.putExtra("pkg_name", r3.A02);
                    intent.putExtra("appid", r3.A01);
                    intent.setClassName(getPackageName(), getClass().getName());
                    A04(intent);
                }
            }
        }
    }

    public void A0c(List list, List list2) {
        for (String A002 : AnonymousClass0A9.A00) {
            AnonymousClass0AC A003 = AnonymousClass0AB.A00(this, A002, 64, C009207y.A01);
            if (A003.A02 == AnonymousClass07B.A0Y || A003.A02 == AnonymousClass07B.A0C || A003.A02 == AnonymousClass07B.A0n) {
                list.add(A003.A00);
            }
            if (A003.A02 == AnonymousClass07B.A0n) {
                list2.add(A003.A00);
            }
        }
    }

    public IBinder onBind(Intent intent) {
        intent.toString();
        if (this.A00.A04(intent)) {
            return this.A08;
        }
        C010708t.A0O("FbnsService", "onBind invalid signature", intent.toString());
        this.A01.A06(intent.toString());
        return null;
    }

    public static String A03(String str) {
        if (AnonymousClass0A9.A01(str)) {
            return "com.facebook.oxygen.services.fbns.PreloadedFbnsService";
        }
        return FbnsService.class.getName();
    }

    private void A05(Intent intent) {
        String str = intent.getPackage();
        if (!TextUtils.isEmpty(str)) {
            if (!str.equals(getPackageName())) {
                C012309k r0 = this.A00;
                if (!AnonymousClass0AB.A01(r0.A00, str, r0.A01)) {
                    String A032 = this.A06.A03(str);
                    String A022 = this.A06.A02(str);
                    if (A032 != null && A022 != null) {
                        A09(A032, str, A022);
                        return;
                    }
                    return;
                }
            }
            this.A00.A03(intent, str);
        }
    }

    private void A08(String str, String str2, String str3) {
        int i;
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            this.A04.A01(str, str2, str3);
            AnonymousClass0RF r3 = this.A06;
            AnonymousClass0A1.A01(!TextUtils.isEmpty(str));
            AnonymousClass0A1.A01(!TextUtils.isEmpty(str2));
            AnonymousClass0RG r2 = new AnonymousClass0RG();
            r2.A02 = str;
            r2.A01 = str2;
            r2.A00 = Long.valueOf(System.currentTimeMillis());
            AnonymousClass0RF.A01(str, r2, r3.A00.AbP(AnonymousClass07B.A0o));
            AnonymousClass0RM r32 = new AnonymousClass0RM(str, str2);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.putOpt("pkg_name", r32.A01);
                jSONObject.putOpt("appid", r32.A00);
                String jSONObject2 = jSONObject.toString();
                try {
                    i = this.A09.A08("/fbns_reg_req", AnonymousClass0ED.A00(jSONObject2), AnonymousClass07B.A01, new AnonymousClass0O2(this));
                } catch (AnonymousClass0CI unused) {
                    i = -1;
                }
                if (i == -1) {
                    this.A01.A02(AnonymousClass07B.A0n, null);
                }
            } catch (JSONException e) {
                C010708t.A0R("FbnsService", e, "service/register/serialize_exception");
                this.A01.A02(AnonymousClass07B.A0o, null);
            }
        }
    }

    public void A0E() {
        super.A0E();
        if (A09 == this) {
            A09 = null;
        }
    }

    public Integer A0I() {
        return AnonymousClass07B.A01;
    }

    public void A0L() {
        super.A0L();
        AnonymousClass0B7 r1 = this.A05;
        this.A03.A03();
        r1.A0H = "S";
    }

    public void A0M() {
        super.A0M();
        AnonymousClass0OT r0 = (AnonymousClass0OT) this.A0A;
        AnonymousClass0RF r5 = r0.A03;
        AnonymousClass0RS r4 = r0.A01;
        AnonymousClass0RK r3 = r0.A02;
        C012309k r2 = r0.A00;
        AnonymousClass0ON r1 = new AnonymousClass0ON(this, r2, r0.A05);
        this.A06 = r5;
        this.A01 = r4;
        this.A04 = r3;
        this.A05 = new AnonymousClass0RJ();
        this.A00 = r2;
        this.A03 = r1;
    }

    public void A0O() {
        super.A0O();
        this.A03.A04(null);
    }

    public void A0P() {
        super.A0P();
        AnonymousClass0ON r3 = this.A03;
        BroadcastReceiver broadcastReceiver = r3.A00;
        if (broadcastReceiver != null) {
            C009207y.A01.A07(r3.A03, broadcastReceiver);
            r3.A00 = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0044 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0067  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0R(android.content.Intent r9, X.AnonymousClass08E r10) {
        /*
            r8 = this;
            super.A0R(r9, r10)
            java.lang.String r5 = r9.getAction()
            java.lang.String r0 = "com.facebook.rti.fbns.intent.REGISTER"
            boolean r0 = r0.equals(r5)
            java.lang.String r1 = "com.facebook.rti.fbns.intent.REGISTER_RETRY"
            if (r0 != 0) goto L_0x001f
            boolean r0 = r1.equals(r5)
            if (r0 != 0) goto L_0x001f
            java.lang.String r0 = "com.facebook.rti.fbns.intent.UNREGISTER"
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0065
        L_0x001f:
            java.lang.String r0 = "pkg_name"
            java.lang.String r4 = r9.getStringExtra(r0)
            java.lang.String r3 = X.C012309k.A00(r9)
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 == 0) goto L_0x0045
            java.lang.String r2 = "FbnsService"
            java.lang.Object[] r1 = new java.lang.Object[]{r5, r3}
            java.lang.String r0 = "Empty package name for %s from %s"
            X.C010708t.A0P(r2, r0, r1)
            X.0RS r1 = r8.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A04
        L_0x003e:
            r1.A05(r0, r5, r3, r4)
            r0 = 0
        L_0x0042:
            if (r0 != 0) goto L_0x0067
            return
        L_0x0045:
            boolean r0 = r1.equals(r5)
            if (r0 == 0) goto L_0x004f
            java.lang.String r4 = r8.getPackageName()
        L_0x004f:
            boolean r0 = r4.equals(r3)
            if (r0 != 0) goto L_0x0065
            java.lang.String r2 = "FbnsService"
            java.lang.Object[] r1 = new java.lang.Object[]{r5, r3, r4}
            java.lang.String r0 = "Package mismatch for %s from %s: packageName %s"
            X.C010708t.A0P(r2, r0, r1)
            X.0RS r1 = r8.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A03
            goto L_0x003e
        L_0x0065:
            r0 = 1
            goto L_0x0042
        L_0x0067:
            java.lang.String r1 = r9.getAction()
            java.lang.String r0 = "com.facebook.rti.fbns.intent.REGISTER"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00a5
            X.0Bo r0 = X.C01770Bo.A08
            r8.A0S(r0, r10)
            r8.A04(r9)
        L_0x007b:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r8.A0c(r0, r3)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r8.A0b(r2)
            X.0B7 r1 = r8.A05
            java.lang.String r0 = X.AnonymousClass0B7.A02(r0)
            r1.A0J = r0
            java.lang.String r0 = X.AnonymousClass0B7.A02(r3)
            r1.A0G = r0
            java.lang.String r0 = X.AnonymousClass0B7.A02(r2)
            r1.A0I = r0
            return
        L_0x00a5:
            java.lang.String r0 = "com.facebook.rti.fbns.intent.REGISTER_RETRY"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00ca
            X.0Bo r0 = X.C01770Bo.A09
            r8.A0S(r0, r10)
            java.lang.String r0 = "pkg_name"
            java.lang.String r2 = r9.getStringExtra(r0)
            java.lang.String r0 = "appid"
            java.lang.String r1 = r9.getStringExtra(r0)
            android.content.ComponentName r0 = r9.getComponent()
            java.lang.String r0 = r0.getClassName()
            r8.A08(r2, r1, r0)
            goto L_0x007b
        L_0x00ca:
            java.lang.String r0 = "com.facebook.rti.fbns.intent.UNREGISTER"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0125
            X.0Bo r0 = X.C01770Bo.A0A
            r8.A0S(r0, r10)
            java.lang.String r0 = "pkg_name"
            java.lang.String r7 = r9.getStringExtra(r0)
            X.0RF r0 = r8.A06
            java.lang.String r5 = r0.A03(r7)
            X.0RF r0 = r8.A06
            java.lang.String r4 = r0.A02(r7)
            X.0RF r1 = r8.A06
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            r3 = 1
            r0 = r0 ^ r3
            X.AnonymousClass0A1.A01(r0)
            X.0AW r1 = r1.A00
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            X.0B0 r2 = r1.AbP(r0)
            X.0RG r1 = X.AnonymousClass0RF.A00(r7, r2)
            if (r1 == 0) goto L_0x010b
            boolean r0 = r1.A04
            if (r0 != 0) goto L_0x010b
            r1.A04 = r3
            X.AnonymousClass0RF.A01(r7, r1, r2)
        L_0x010b:
            r2 = 0
            java.lang.String r0 = "unregistered"
            android.content.Intent r0 = A01(r7, r0, r2)
            r8.A05(r0)
            X.0RS r1 = r8.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A05
            r1.A02(r0, r2)
            if (r5 == 0) goto L_0x007b
            if (r4 == 0) goto L_0x007b
            r8.A09(r5, r7, r4)
            goto L_0x007b
        L_0x0125:
            java.lang.String r1 = "FbnsService"
            java.lang.String r0 = "service/doIntent/unrecognized_action"
            X.C010708t.A0I(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.rti.push.service.FbnsService.A0R(android.content.Intent, X.08E):void");
    }

    public void A0V(C02020Cn r5) {
        super.A0V(r5);
        ((AtomicLong) ((AnonymousClass08I) this.A05.A07(AnonymousClass08I.class)).A00(AnonymousClass08J.A02)).addAndGet((long) this.A03.A02());
    }

    public void onCreate() {
        int A002 = AnonymousClass02C.A00(this, -1022325934);
        super.onCreate();
        AnonymousClass02C.A02(-1752820605, A002);
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x0227 A[Catch:{ UnsupportedEncodingException -> 0x0344, JSONException -> 0x034c }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0246 A[Catch:{ UnsupportedEncodingException -> 0x0344, JSONException -> 0x034c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Y(java.lang.String r12, byte[] r13, int r14, long r15, X.AnonymousClass0CU r17) {
        /*
            r11 = this;
            super.A0Y(r12, r13, r14, r15, r17)
            if (r13 != 0) goto L_0x0014
            java.lang.String r2 = "FbnsService"
            java.lang.Object[] r1 = new java.lang.Object[]{r12}
            java.lang.String r0 = "receive/publish/empty_payload; topic=%s"
            X.C010708t.A0O(r2, r0, r1)
            r17.A00()
            return
        L_0x0014:
            java.lang.String r4 = "FbnsService"
            r3 = 0
            java.lang.String r2 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x0344 }
            java.lang.String r0 = "UTF-8"
            r2.<init>(r13, r0)     // Catch:{ UnsupportedEncodingException -> 0x0344 }
            r3 = r2
            java.lang.String r0 = "/fbns_msg"
            boolean r0 = r0.equals(r12)     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x0198
            java.lang.String r0 = "/fbns_msg_hp"
            boolean r0 = r0.equals(r12)     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x0198
            java.lang.String r0 = "/fbns_reg_resp"
            boolean r0 = r0.equals(r12)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x015a
            X.0RL r6 = new X.0RL     // Catch:{ JSONException -> 0x034c }
            r6.<init>()     // Catch:{ JSONException -> 0x034c }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x034c }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "pkg_name"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r6.A01 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "token"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r6.A02 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "error"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r6.A00 = r0     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x010d
            java.lang.String r0 = r6.A01     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x034c }
            r7 = 0
            if (r0 == 0) goto L_0x0076
            java.lang.String r0 = "service/register/response/invalid"
            X.C010708t.A0I(r4, r0)     // Catch:{ JSONException -> 0x034c }
            X.0RS r1 = r11.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A0q     // Catch:{ JSONException -> 0x034c }
            r1.A02(r0, r7)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x0076:
            java.lang.String r0 = r6.A02     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x008c
            java.lang.String r0 = "service/register/response/empty_token"
            X.C010708t.A0I(r4, r0)     // Catch:{ JSONException -> 0x034c }
            X.0RS r1 = r11.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A02     // Catch:{ JSONException -> 0x034c }
            r1.A02(r0, r7)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x008c:
            X.0RF r2 = r11.A06     // Catch:{ JSONException -> 0x034c }
            java.lang.String r8 = r6.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.String r10 = r6.A02     // Catch:{ JSONException -> 0x034c }
            java.lang.String r9 = "RegistrationState"
            boolean r0 = android.text.TextUtils.isEmpty(r8)     // Catch:{ JSONException -> 0x034c }
            r0 = r0 ^ 1
            X.AnonymousClass0A1.A01(r0)     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r10)     // Catch:{ JSONException -> 0x034c }
            r0 = r0 ^ 1
            X.AnonymousClass0A1.A01(r0)     // Catch:{ JSONException -> 0x034c }
            X.0AW r1 = r2.A00     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A0N     // Catch:{ JSONException -> 0x034c }
            X.0B0 r0 = r1.AbP(r0)     // Catch:{ JSONException -> 0x034c }
            X.0DD r1 = r0.AY8()     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "auto_reg_retry"
            r1.C1C(r0)     // Catch:{ JSONException -> 0x034c }
            r1.commit()     // Catch:{ JSONException -> 0x034c }
            X.0AW r1 = r2.A00     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A0o     // Catch:{ JSONException -> 0x034c }
            X.0B0 r5 = r1.AbP(r0)     // Catch:{ JSONException -> 0x034c }
            X.0RG r2 = X.AnonymousClass0RF.A00(r8, r5)     // Catch:{ JSONException -> 0x034c }
            if (r2 != 0) goto L_0x00ce
            java.lang.String r0 = "Missing entry"
            X.C010708t.A0I(r9, r0)     // Catch:{ JSONException -> 0x034c }
            goto L_0x00df
        L_0x00ce:
            r2.A03 = r10     // Catch:{ JSONException -> 0x034c }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x034c }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ JSONException -> 0x034c }
            r2.A00 = r0     // Catch:{ JSONException -> 0x034c }
            boolean r0 = X.AnonymousClass0RF.A01(r8, r2, r5)     // Catch:{ JSONException -> 0x034c }
            goto L_0x00e0
        L_0x00df:
            r0 = 0
        L_0x00e0:
            if (r0 == 0) goto L_0x00fd
            java.lang.String r5 = r6.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.String r2 = r6.A02     // Catch:{ JSONException -> 0x034c }
            X.0RK r0 = r11.A04     // Catch:{ JSONException -> 0x034c }
            r0.A00(r5)     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "registered"
            android.content.Intent r0 = A01(r5, r0, r2)     // Catch:{ JSONException -> 0x034c }
            r11.A05(r0)     // Catch:{ JSONException -> 0x034c }
            X.0RS r1 = r11.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A0N     // Catch:{ JSONException -> 0x034c }
            r1.A02(r0, r7)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x00fd:
            java.lang.String r0 = "service/register/response/cache_update_failed"
            X.C010708t.A0I(r4, r0)     // Catch:{ JSONException -> 0x034c }
            X.0RS r2 = r11.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r1 = X.AnonymousClass07B.A0Y     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r6.A01     // Catch:{ JSONException -> 0x034c }
            r2.A02(r1, r0)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x010d:
            java.lang.String r0 = r6.A01     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x0125
            java.lang.String r0 = "service/register/response/empty_package"
            X.C010708t.A0I(r4, r0)     // Catch:{ JSONException -> 0x034c }
        L_0x011a:
            X.0RS r2 = r11.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r1 = X.AnonymousClass07B.A0p     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r6.A00     // Catch:{ JSONException -> 0x034c }
            r2.A02(r1, r0)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x0125:
            X.0RF r1 = r11.A06     // Catch:{ JSONException -> 0x034c }
            java.lang.String r7 = r6.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.String r8 = "RegistrationState"
            boolean r0 = android.text.TextUtils.isEmpty(r7)     // Catch:{ JSONException -> 0x034c }
            r0 = r0 ^ 1
            X.AnonymousClass0A1.A01(r0)     // Catch:{ JSONException -> 0x034c }
            X.0AW r1 = r1.A00     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A0o     // Catch:{ JSONException -> 0x034c }
            X.0B0 r5 = r1.AbP(r0)     // Catch:{ JSONException -> 0x034c }
            X.0RG r2 = X.AnonymousClass0RF.A00(r7, r5)     // Catch:{ JSONException -> 0x034c }
            if (r2 != 0) goto L_0x0148
            java.lang.String r0 = "Missing entry"
            X.C010708t.A0I(r8, r0)     // Catch:{ JSONException -> 0x034c }
            goto L_0x011a
        L_0x0148:
            java.lang.String r0 = ""
            r2.A03 = r0     // Catch:{ JSONException -> 0x034c }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x034c }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ JSONException -> 0x034c }
            r2.A00 = r0     // Catch:{ JSONException -> 0x034c }
            X.AnonymousClass0RF.A01(r7, r2, r5)     // Catch:{ JSONException -> 0x034c }
            goto L_0x011a
        L_0x015a:
            java.lang.String r0 = "/fbns_exp_logging"
            boolean r0 = r0.equals(r12)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x0186
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            r1.<init>(r2)     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            java.lang.String r0 = "beacon_id"
            java.lang.String r1 = r1.optString(r0)     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            if (r0 != 0) goto L_0x0368
            java.lang.Long.parseLong(r1)     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            X.0AH r6 = r11.A09     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            java.lang.String r5 = "/fbns_exp_logging"
            byte[] r2 = X.AnonymousClass0ED.A00(r2)     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            java.lang.Integer r1 = X.AnonymousClass07B.A00     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            r0 = 0
            r6.A08(r5, r2, r1, r0)     // Catch:{ 0CI | NumberFormatException | JSONException -> 0x0368 }
            goto L_0x0368
        L_0x0186:
            java.lang.String r1 = "receive/publish/wrong_topic; topic=%s"
            java.lang.Object[] r0 = new java.lang.Object[]{r12}     // Catch:{ JSONException -> 0x034c }
            X.C010708t.A0O(r4, r1, r0)     // Catch:{ JSONException -> 0x034c }
            X.0RS r1 = r11.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ JSONException -> 0x034c }
            r1.A03(r0, r12)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x0198:
            X.0RN r7 = new X.0RN     // Catch:{ JSONException -> 0x034c }
            r7.<init>()     // Catch:{ JSONException -> 0x034c }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x034c }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "token"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r7.A06 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "ck"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r7.A01 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "pn"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r7.A04 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "cp"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r7.A00 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "fbpushnotif"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r7.A05 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "nid"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r7.A03 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "bu"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x034c }
            r7.A02 = r0     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r1 = X.AnonymousClass07B.A00     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r7.A03     // Catch:{ JSONException -> 0x034c }
            r11.A07(r1, r7, r0)     // Catch:{ JSONException -> 0x034c }
            X.0OO r6 = r11.A02     // Catch:{ JSONException -> 0x034c }
            java.lang.String r5 = "===Received Notif: target = "
            java.lang.String r2 = r7.A04     // Catch:{ JSONException -> 0x034c }
            java.lang.String r1 = "; notifId = "
            java.lang.String r0 = r7.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = X.AnonymousClass08S.A0S(r5, r2, r1, r0)     // Catch:{ JSONException -> 0x034c }
            r6.BIo(r0)     // Catch:{ JSONException -> 0x034c }
            X.0RJ r5 = r11.A05     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r7.A03     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x0222
            android.util.Pair r2 = new android.util.Pair     // Catch:{ JSONException -> 0x034c }
            java.lang.String r1 = r7.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r7.A04     // Catch:{ JSONException -> 0x034c }
            r2.<init>(r1, r0)     // Catch:{ JSONException -> 0x034c }
            java.util.LinkedList r0 = r5.A00     // Catch:{ JSONException -> 0x034c }
            boolean r0 = r0.contains(r2)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x020e
            goto L_0x0224
        L_0x020e:
            java.util.LinkedList r0 = r5.A00     // Catch:{ JSONException -> 0x034c }
            r0.add(r2)     // Catch:{ JSONException -> 0x034c }
            java.util.LinkedList r0 = r5.A00     // Catch:{ JSONException -> 0x034c }
            int r1 = r0.size()     // Catch:{ JSONException -> 0x034c }
            r0 = 100
            if (r1 <= r0) goto L_0x0222
            java.util.LinkedList r0 = r5.A00     // Catch:{ JSONException -> 0x034c }
            r0.removeFirst()     // Catch:{ JSONException -> 0x034c }
        L_0x0222:
            r0 = 0
            goto L_0x0225
        L_0x0224:
            r0 = 1
        L_0x0225:
            if (r0 == 0) goto L_0x0246
            java.lang.String r2 = r7.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r1 = r7.A04     // Catch:{ JSONException -> 0x034c }
            X.0SR r0 = X.AnonymousClass0SR.A06     // Catch:{ JSONException -> 0x034c }
            A06(r11, r2, r1, r0)     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r7.A03     // Catch:{ JSONException -> 0x034c }
            r11.A07(r1, r7, r0)     // Catch:{ JSONException -> 0x034c }
            X.0OO r2 = r11.A02     // Catch:{ JSONException -> 0x034c }
            java.lang.String r1 = "Duplicated Notif: notifId = "
            java.lang.String r0 = r7.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ JSONException -> 0x034c }
            r2.BIo(r0)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x0246:
            java.lang.String r2 = r7.A04     // Catch:{ JSONException -> 0x034c }
            java.lang.String r1 = r7.A05     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "message"
            android.content.Intent r9 = A01(r2, r0, r1)     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r7.A06     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x025f
            java.lang.String r1 = r7.A06     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "token"
            r9.putExtra(r0, r1)     // Catch:{ JSONException -> 0x034c }
        L_0x025f:
            java.lang.String r0 = r7.A00     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x026e
            java.lang.String r1 = r7.A00     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "collapse_key"
            r9.putExtra(r0, r1)     // Catch:{ JSONException -> 0x034c }
        L_0x026e:
            X.0ON r1 = r11.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r6 = r7.A03     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r6)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x02dd
            com.facebook.rti.push.service.FbnsService r0 = r1.A00     // Catch:{ JSONException -> 0x034c }
            X.0RS r2 = r0.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r1 = X.AnonymousClass07B.A0i     // Catch:{ JSONException -> 0x034c }
            r0 = 0
            r2.A04(r1, r0, r0)     // Catch:{ JSONException -> 0x034c }
            X.0SR r2 = X.AnonymousClass0SR.A04     // Catch:{ JSONException -> 0x034c }
        L_0x0284:
            boolean r0 = r2.A01()     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x02c2
            X.0SR r0 = X.AnonymousClass0SR.A0E     // Catch:{ JSONException -> 0x034c }
            if (r2 != r0) goto L_0x02aa
            android.content.Context r0 = r11.getBaseContext()     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ JSONException -> 0x034c }
            java.lang.String r1 = r7.A04     // Catch:{ JSONException -> 0x034c }
            boolean r0 = r0.equals(r1)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x02a3
            X.09k r0 = r11.A00     // Catch:{ JSONException -> 0x034c }
            r0.A03(r9, r1)     // Catch:{ JSONException -> 0x034c }
        L_0x02a3:
            java.lang.String r1 = r7.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = r7.A04     // Catch:{ JSONException -> 0x034c }
            A06(r11, r1, r0, r2)     // Catch:{ JSONException -> 0x034c }
        L_0x02aa:
            java.lang.String r6 = r2.name()     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ JSONException -> 0x034c }
            r11.A07(r0, r7, r6)     // Catch:{ JSONException -> 0x034c }
            X.0OO r5 = r11.A02     // Catch:{ JSONException -> 0x034c }
            java.lang.String r2 = "Error: Delivery helper failed notifId = "
            java.lang.String r1 = r7.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "; reason = "
            java.lang.String r0 = X.AnonymousClass08S.A0S(r2, r1, r0, r6)     // Catch:{ JSONException -> 0x034c }
            r5.BIo(r0)     // Catch:{ JSONException -> 0x034c }
        L_0x02c2:
            X.0B7 r5 = r11.A05     // Catch:{ JSONException -> 0x034c }
            java.lang.String r2 = r7.A04     // Catch:{ JSONException -> 0x034c }
            java.util.concurrent.ConcurrentMap r1 = r5.A01     // Catch:{ JSONException -> 0x034c }
            java.util.concurrent.atomic.AtomicLong r0 = new java.util.concurrent.atomic.AtomicLong     // Catch:{ JSONException -> 0x034c }
            r0.<init>()     // Catch:{ JSONException -> 0x034c }
            r1.putIfAbsent(r2, r0)     // Catch:{ JSONException -> 0x034c }
            java.util.concurrent.ConcurrentMap r0 = r5.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ JSONException -> 0x034c }
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0     // Catch:{ JSONException -> 0x034c }
            r0.incrementAndGet()     // Catch:{ JSONException -> 0x034c }
            goto L_0x0368
        L_0x02dd:
            java.lang.String r5 = r9.getPackage()     // Catch:{ JSONException -> 0x034c }
            boolean r0 = android.text.TextUtils.isEmpty(r5)     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x02fe
            com.facebook.rti.push.service.FbnsService r2 = r1.A00     // Catch:{ JSONException -> 0x034c }
            X.0RS r1 = r2.A01     // Catch:{ JSONException -> 0x034c }
            java.lang.Integer r0 = X.AnonymousClass07B.A0n     // Catch:{ JSONException -> 0x034c }
            r1.A04(r0, r6, r5)     // Catch:{ JSONException -> 0x034c }
            X.0OO r1 = r2.A02     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "Error: invalid receiver = "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r5)     // Catch:{ JSONException -> 0x034c }
            r1.BIo(r0)     // Catch:{ JSONException -> 0x034c }
            X.0SR r2 = X.AnonymousClass0SR.A0A     // Catch:{ JSONException -> 0x034c }
            goto L_0x0284
        L_0x02fe:
            java.util.List r0 = X.AnonymousClass0ON.A01     // Catch:{ JSONException -> 0x034c }
            boolean r0 = r0.contains(r5)     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x030a
            X.0SR r2 = X.AnonymousClass0SR.A0E     // Catch:{ JSONException -> 0x034c }
            goto L_0x0284
        L_0x030a:
            android.content.Context r0 = r1.A03     // Catch:{ JSONException -> 0x034c }
            java.lang.String r2 = r0.getPackageName()     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "extra_notification_sender"
            r9.putExtra(r0, r2)     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "extra_notification_id"
            r9.putExtra(r0, r6)     // Catch:{ JSONException -> 0x034c }
            X.0Ct r0 = r1.A03()     // Catch:{ JSONException -> 0x034c }
            r0.A02(r6, r9)     // Catch:{ JSONException -> 0x034c }
            X.0SR r2 = X.AnonymousClass0ON.A01(r1, r9)     // Catch:{ JSONException -> 0x034c }
            boolean r0 = r2.A00()     // Catch:{ JSONException -> 0x034c }
            if (r0 == 0) goto L_0x0337
            X.0Ct r0 = r1.A03()     // Catch:{ JSONException -> 0x034c }
            r0.A01(r6)     // Catch:{ JSONException -> 0x034c }
            r1.A07(r6, r5, r2)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0284
        L_0x0337:
            boolean r0 = r2.A01()     // Catch:{ JSONException -> 0x034c }
            if (r0 != 0) goto L_0x0284
            com.facebook.rti.push.service.FbnsService r1 = r1.A00     // Catch:{ JSONException -> 0x034c }
            A06(r1, r6, r5, r2)     // Catch:{ JSONException -> 0x034c }
            goto L_0x0284
        L_0x0344:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ JSONException -> 0x034c }
            java.lang.String r0 = "UTF-8 not supported"
            r1.<init>(r0)     // Catch:{ JSONException -> 0x034c }
            throw r1     // Catch:{ JSONException -> 0x034c }
        L_0x034c:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r12}
            java.lang.String r0 = "receive/publish/payload_exception; topic=%s"
            X.C010708t.A0U(r4, r2, r0, r1)
            X.0RS r1 = r11.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.A03(r0, r12)
            X.0OO r1 = r11.A02
            java.lang.String r0 = "Error: invalid payload = "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.BIo(r0)
        L_0x0368:
            r17.A00()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.rti.push.service.FbnsService.A0Y(java.lang.String, byte[], int, long, X.0CU):void");
    }
}
