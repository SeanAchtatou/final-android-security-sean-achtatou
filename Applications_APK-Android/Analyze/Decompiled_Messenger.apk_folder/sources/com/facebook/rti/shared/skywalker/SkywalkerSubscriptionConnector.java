package com.facebook.rti.shared.skywalker;

import X.AnonymousClass07A;
import X.AnonymousClass0GB;
import X.AnonymousClass0GC;
import X.AnonymousClass0GD;
import X.AnonymousClass0IF;
import X.AnonymousClass0IG;
import X.AnonymousClass0R9;
import X.AnonymousClass0UN;
import X.AnonymousClass0UX;
import X.AnonymousClass0Ud;
import X.AnonymousClass0WD;
import X.AnonymousClass0jJ;
import X.AnonymousClass195;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C010708t;
import X.C04430Uq;
import X.C04460Ut;
import X.C04810Wg;
import X.C05040Xk;
import X.C05460Za;
import X.C06600bl;
import X.C06850cB;
import X.C10370jz;
import X.C21171Fm;
import X.C24381Tk;
import X.C28081eE;
import X.C29281gA;
import X.C36991uR;
import X.C37561vs;
import X.C56632qX;
import android.content.Intent;
import android.os.RemoteException;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
public final class SkywalkerSubscriptionConnector implements C05460Za, C56632qX {
    public static final Class A08 = SkywalkerSubscriptionConnector.class;
    private static volatile SkywalkerSubscriptionConnector A09;
    private AnonymousClass0UN A00;
    public final C29281gA A01;
    public final AnonymousClass0jJ A02;
    public final Map A03 = new HashMap();
    public final Map A04 = new HashMap();
    public final ExecutorService A05;
    private final C21171Fm A06;
    private final C10370jz A07;

    public static void A05(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector) {
        ImmutableMap copyOf;
        synchronized (skywalkerSubscriptionConnector) {
            try {
                copyOf = ImmutableMap.copyOf(skywalkerSubscriptionConnector.A03);
                skywalkerSubscriptionConnector.A04.clear();
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        ArrayNode createArrayNode = skywalkerSubscriptionConnector.A02.createArrayNode();
        for (String add : copyOf.keySet()) {
            createArrayNode.add(add);
        }
        if (A08(skywalkerSubscriptionConnector, createArrayNode)) {
            synchronized (skywalkerSubscriptionConnector) {
                try {
                    skywalkerSubscriptionConnector.A04.putAll(copyOf);
                    skywalkerSubscriptionConnector.A03.clear();
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public static boolean A08(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector, ArrayNode arrayNode) {
        ObjectNode A022 = A02(arrayNode, null, null);
        C36991uR BvK = skywalkerSubscriptionConnector.A01.BvK();
        try {
            boolean A072 = BvK.A07("/pubsub", A022, LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT);
            BvK.A06();
            return A072;
        } catch (RemoteException e) {
            C010708t.A08(A08, "Remote exception for subscribe", e);
            BvK.A06();
            return false;
        } catch (Throwable th) {
            BvK.A06();
            throw th;
        }
    }

    public void clearUserData() {
        ImmutableSet<String> A0A;
        synchronized (this) {
            A0A = ImmutableSet.A0A(this.A04.keySet());
        }
        for (String r0 : A0A) {
            AnonymousClass07A.A04(this.A05, new AnonymousClass0GD(this, r0), -1289877389);
        }
    }

    public String getHandlerName() {
        return "SkywalkerSubscriptionConnector";
    }

    public static final SkywalkerSubscriptionConnector A01(AnonymousClass1XY r5) {
        if (A09 == null) {
            synchronized (SkywalkerSubscriptionConnector.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A09 = new SkywalkerSubscriptionConnector(applicationInjector, C04430Uq.A02(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    /* access modifiers changed from: private */
    public static ObjectNode A02(ArrayNode arrayNode, ArrayNode arrayNode2, JsonNode jsonNode) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        if (arrayNode != null) {
            objectNode.put("sub", arrayNode);
        }
        if (arrayNode2 != null) {
            objectNode.put("unsub", arrayNode2);
        }
        if (jsonNode != null) {
            objectNode.put("pub", jsonNode);
        }
        objectNode.put("version", 0);
        return objectNode;
    }

    private void A04() {
        SubscribeTopic subscribeTopic = new SubscribeTopic("/pubsub", 0);
        HashSet hashSet = new HashSet();
        hashSet.add(subscribeTopic);
        this.A06.A03(hashSet, RegularImmutableSet.A05);
    }

    public static void A06(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector, Intent intent) {
        AnonymousClass195 A002 = AnonymousClass195.A00(intent.getIntExtra("event", AnonymousClass195.UNKNOWN.value));
        if (A002 == AnonymousClass195.CHANNEL_CONNECTED) {
            synchronized (skywalkerSubscriptionConnector) {
                try {
                    if (!skywalkerSubscriptionConnector.A03.isEmpty() && !A07(skywalkerSubscriptionConnector)) {
                        AnonymousClass07A.A04(skywalkerSubscriptionConnector.A05, new AnonymousClass0IF(skywalkerSubscriptionConnector), 2012146702);
                    }
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
        } else {
            A002.toString();
            synchronized (skywalkerSubscriptionConnector) {
                try {
                    skywalkerSubscriptionConnector.A03.putAll(skywalkerSubscriptionConnector.A04);
                    skywalkerSubscriptionConnector.A04.clear();
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public static boolean A07(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector) {
        return ((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, skywalkerSubscriptionConnector.A00)).A0G();
    }

    public void A09(String str, JsonNode jsonNode, AnonymousClass0R9 r6) {
        AnonymousClass07A.A04(this.A05, new AnonymousClass0GC(this, str, jsonNode, r6), -552223622);
    }

    public void A0A(String str, C04810Wg r5, AnonymousClass0R9 r6) {
        AnonymousClass07A.A04(this.A05, new AnonymousClass0IG(this, str, r5, r6), -1984188221);
    }

    public void onMessage(String str, byte[] bArr, long j) {
        Class cls;
        String str2;
        if (str.startsWith("/pubsub")) {
            try {
                JsonNode jsonNode = (JsonNode) this.A07.createParser(((JsonNode) this.A07.createParser(bArr).readValueAsTree()).get("raw").asText()).readValueAsTree();
                String asText = jsonNode.get("topic").asText();
                JsonNode jsonNode2 = jsonNode.get("payload");
                if (C06850cB.A0B(asText)) {
                    C010708t.A06(A08, "Empty topic");
                    return;
                }
                if (jsonNode2 != null) {
                    jsonNode2.asText();
                }
                synchronized (this) {
                    if (this.A04.get(asText) != null) {
                        ((C04810Wg) this.A04.get(asText)).Bqf(jsonNode2);
                    } else if (this.A03.get(asText) != null) {
                        C010708t.A0C(A08, "No callback set for topic %s, fallback to pending topic map", asText);
                        ((C04810Wg) this.A03.get(asText)).Bqf(jsonNode2);
                    } else {
                        C010708t.A0C(A08, "No callback set for topic %s", asText);
                    }
                }
                return;
            } catch (C37561vs e) {
                e = e;
                cls = A08;
                str2 = "JsonParseException in onMessage";
            } catch (IOException e2) {
                e = e2;
                cls = A08;
                str2 = "IOException in onMessage";
            }
        } else {
            return;
        }
        C010708t.A08(cls, str2, e);
    }

    private SkywalkerSubscriptionConnector(AnonymousClass1XY r4, C04460Ut r5) {
        this.A00 = new AnonymousClass0UN(1, r4);
        this.A06 = C21171Fm.A00(r4);
        this.A07 = C05040Xk.A02();
        this.A01 = C24381Tk.A00(r4);
        this.A05 = AnonymousClass0UX.A0Z(r4);
        this.A02 = C05040Xk.A04();
        A04();
        C06600bl BMm = r5.BMm();
        BMm.A02("com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED", new AnonymousClass0GB(this));
        BMm.A00().A00();
        C28081eE.A00(SkywalkerSubscriptionConnector.class);
    }

    public static final SkywalkerSubscriptionConnector A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
