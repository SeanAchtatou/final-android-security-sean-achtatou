package com.facebook.rti.orca;

import X.AnonymousClass07A;
import X.AnonymousClass0GZ;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.C000700l;
import X.C07620dr;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UpdateQeBroadcastReceiver extends BroadcastReceiver {
    private AnonymousClass0GZ A00;

    private static final void A00(Context context, UpdateQeBroadcastReceiver updateQeBroadcastReceiver) {
        A01(AnonymousClass1XX.get(context), updateQeBroadcastReceiver);
    }

    private static final void A01(AnonymousClass1XY r0, UpdateQeBroadcastReceiver updateQeBroadcastReceiver) {
        updateQeBroadcastReceiver.A00 = AnonymousClass0GZ.A01(r0);
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(-1871418952);
        C07620dr.A00(context);
        A00(context, this);
        if (intent == null) {
            C000700l.A0D(intent, 1642406575, A01);
        } else if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) || "android.intent.action.MY_PACKAGE_REPLACED".equals(intent.getAction())) {
            AnonymousClass0GZ r0 = this.A00;
            AnonymousClass07A.A04(r0.A09, r0.A08, -1019195891);
            C000700l.A0D(intent, 1717474013, A01);
        } else {
            C000700l.A0D(intent, -1663257527, A01);
        }
    }
}
