package com.facebook.rti.orca;

import X.AnonymousClass02C;
import X.AnonymousClass03g;
import X.AnonymousClass07A;
import X.AnonymousClass0Re;
import X.AnonymousClass0UX;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import java.util.concurrent.ExecutorService;

public class MainService extends Service {
    public AnonymousClass03g A00;
    private ExecutorService A01;

    public IBinder onBind(Intent intent) {
        return null;
    }

    private static final void A00(Context context, MainService mainService) {
        A01(AnonymousClass1XX.get(context), mainService);
    }

    private static final void A01(AnonymousClass1XY r1, MainService mainService) {
        mainService.A00 = AnonymousClass03g.A00(r1);
        mainService.A01 = AnonymousClass0UX.A0a(r1);
    }

    public void onCreate() {
        int A002 = AnonymousClass02C.A00(this, -1756004822);
        super.onCreate();
        A00(this, this);
        AnonymousClass02C.A02(-274557715, A002);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int A012 = AnonymousClass02C.A01(this, 708886795);
        AnonymousClass07A.A04(this.A01, new AnonymousClass0Re(this), 854479358);
        AnonymousClass02C.A02(-1757932247, A012);
        return 1;
    }
}
