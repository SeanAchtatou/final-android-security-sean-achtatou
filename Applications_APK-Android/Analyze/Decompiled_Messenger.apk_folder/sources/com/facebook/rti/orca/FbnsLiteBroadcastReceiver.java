package com.facebook.rti.orca;

import X.AnonymousClass08S;
import X.AnonymousClass0H3;
import X.C000700l;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.rti.push.service.FbnsService;

public class FbnsLiteBroadcastReceiver extends BroadcastReceiver {
    public static void A00(Context context, String str, String str2) {
        String str3 = str2;
        Context context2 = context;
        if (str2 == null) {
            str3 = context.getPackageName();
        }
        AnonymousClass0H3.A03(context2, FbnsService.A03(str3), str, false, str3, "Orca.START", null);
    }

    public void onReceive(Context context, Intent intent) {
        String str;
        String str2;
        int A01 = C000700l.A01(-377315342);
        if (intent == null) {
            C000700l.A0D(intent, 1749805353, A01);
            return;
        }
        Context context2 = context;
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            A00(context, "USER_PRESENT", context.getPackageName());
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            if (networkInfo != null) {
                String upperCase = networkInfo.getTypeName().toUpperCase();
                if (networkInfo.isConnected()) {
                    str2 = ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL;
                } else {
                    str2 = "-0";
                }
                str = AnonymousClass08S.A0J(upperCase, str2);
            } else {
                str = "NET_NULL";
            }
            A00(context, str, context.getPackageName());
        } else if ("com.facebook.rti.mqtt.intent.ACTION_WAKEUP".equals(intent.getAction())) {
            String packageName = context.getPackageName();
            if (packageName == null) {
                packageName = context.getPackageName();
            }
            AnonymousClass0H3.A03(context2, FbnsService.A03(packageName), "GCM_WAKEUP", false, packageName, "Orca.PERSISTENT_KICK", null);
        }
        C000700l.A0D(intent, 1381417081, A01);
    }
}
