package com.facebook.rti.mqtt.protocol.messages;

import X.AnonymousClass08F;
import X.AnonymousClass0A1;
import X.AnonymousClass0A2;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class SubscribeTopic implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass08F();
    public final int A00;
    public final String A01;
    public volatile GqlsTopicExtraInfo A02;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            SubscribeTopic subscribeTopic = (SubscribeTopic) obj;
            if (!AnonymousClass0A2.A00(this.A01, subscribeTopic.A01) || this.A00 != subscribeTopic.A00) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, Integer.valueOf(this.A00)});
    }

    public String toString() {
        return String.format("{ name='%s', qos='%s', extra='%s' }", this.A01, Integer.valueOf(this.A00), this.A02);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeParcelable(this.A02, i);
    }

    public SubscribeTopic(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A02 = (GqlsTopicExtraInfo) parcel.readParcelable(GqlsTopicExtraInfo.class.getClassLoader());
    }

    public SubscribeTopic(String str, int i) {
        AnonymousClass0A1.A00(str);
        this.A01 = str;
        Integer valueOf = Integer.valueOf(i);
        AnonymousClass0A1.A00(valueOf);
        this.A00 = valueOf.intValue();
    }
}
