package com.facebook.rti.mqtt.protocol.messages;

import X.C04090Ru;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;

public final class GqlsTopicExtraInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C04090Ru();
    public final Boolean A00;
    public final String A01;
    public final Map A02;

    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1.equals(r5.A01) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0044
            r2 = 0
            if (r5 == 0) goto L_0x001e
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x001e
            com.facebook.rti.mqtt.protocol.messages.GqlsTopicExtraInfo r5 = (com.facebook.rti.mqtt.protocol.messages.GqlsTopicExtraInfo) r5
            java.lang.String r1 = r4.A01
            if (r1 == 0) goto L_0x001f
            java.lang.String r0 = r5.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0024
        L_0x001e:
            return r2
        L_0x001f:
            java.lang.String r0 = r5.A01
            if (r0 == 0) goto L_0x0024
            return r2
        L_0x0024:
            java.util.Map r1 = r4.A02
            if (r1 == 0) goto L_0x0031
            java.util.Map r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0036
            return r2
        L_0x0031:
            java.util.Map r0 = r5.A02
            if (r0 == 0) goto L_0x0036
            return r2
        L_0x0036:
            java.lang.Boolean r1 = r4.A00
            java.lang.Boolean r0 = r5.A00
            if (r1 == 0) goto L_0x0041
            boolean r3 = r1.equals(r0)
            return r3
        L_0x0041:
            if (r0 == 0) goto L_0x0044
            r3 = 0
        L_0x0044:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.rti.mqtt.protocol.messages.GqlsTopicExtraInfo.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i;
        int i2;
        String str = this.A01;
        int i3 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i4 = i * 31;
        Map map = this.A02;
        if (map != null) {
            i2 = map.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 31;
        Boolean bool = this.A00;
        if (bool != null) {
            i3 = bool.hashCode();
        }
        return i5 + i3;
    }

    public String toString() {
        return "GqlsTopicExtraInfo{DESCRIPTION='GraphQL Subscription Infomation', subscription='" + this.A01 + '\'' + ", queryParams=" + this.A02 + ", forceLogEnabled=" + this.A00 + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeMap(this.A02);
        parcel.writeValue(this.A00);
    }

    public GqlsTopicExtraInfo(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A02 = parcel.readHashMap(HashMap.class.getClassLoader());
        this.A00 = (Boolean) parcel.readValue(null);
    }

    public GqlsTopicExtraInfo(String str, Map map, Boolean bool) {
        this.A01 = str;
        this.A02 = map;
        this.A00 = bool;
    }
}
