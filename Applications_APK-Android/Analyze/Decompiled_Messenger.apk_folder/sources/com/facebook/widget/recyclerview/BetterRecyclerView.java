package com.facebook.widget.recyclerview;

import X.AnonymousClass0UN;
import X.AnonymousClass19T;
import X.AnonymousClass1AS;
import X.AnonymousClass1IN;
import X.AnonymousClass1Ri;
import X.AnonymousClass1XX;
import X.C005505z;
import X.C05280Yi;
import X.C15020ub;
import X.C15710vl;
import X.C16380wx;
import X.C16390wz;
import X.C16410x1;
import X.C16420x2;
import X.C195618t;
import X.C195718u;
import X.C197519n;
import X.C19931Ag;
import X.C20041Ar;
import X.C20831Dz;
import X.C20891Ef;
import X.C21721Il;
import X.C24151Sm;
import X.C26821DCx;
import X.C32831mP;
import X.C32841mQ;
import X.C84033yf;
import X.C96334ib;
import X.D61;
import X.D6O;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.common.dextricks.DexStore;
import com.facebook.resources.ui.FbRecyclerView;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class BetterRecyclerView extends FbRecyclerView implements C15020ub {
    public double A00 = 1.0d;
    public double A01 = 1.0d;
    public double A02 = 1.0d;
    public int A03 = 0;
    public View A04;
    public AnonymousClass0UN A05;
    public AnonymousClass1AS A06;
    public D61 A07;
    public D6O A08;
    public boolean A09 = false;
    public boolean A0A = false;
    public boolean A0B;
    private int A0C;
    private C19931Ag A0D;
    private List A0E;
    private boolean A0F;
    private boolean A0G;
    private boolean A0H;
    public final GestureDetector A0I = new GestureDetector(getContext(), new C195718u(this), this.A0M);
    public final GestureDetector A0J = new GestureDetector(getContext(), new C32841mQ(this), this.A0M);
    public final C195618t A0K = new C195618t();
    public final CopyOnWriteArrayList A0L = new CopyOnWriteArrayList();
    private final Handler A0M = new Handler(Looper.getMainLooper());
    private final C15710vl A0N = new C32831mP(this);
    private final C15710vl A0O = new C16380wx(this);
    private final C16420x2 A0P = new C16420x2(this);
    private final C21721Il A0Q = new C21721Il(this);
    private final AnonymousClass1IN A0R = new C16410x1(this);
    private final AnonymousClass1IN A0S = new C16390wz();

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        C84033yf r0 = null;
        if (r0 != null) {
            z = r0.onInterceptTouchEvent(motionEvent);
        } else {
            z = false;
        }
        if (!z) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return z;
    }

    public static void A01(BetterRecyclerView betterRecyclerView) {
        int i;
        if (betterRecyclerView.A04 == null) {
            super.setVisibility(betterRecyclerView.A0C);
            return;
        }
        C20831Dz r2 = betterRecyclerView.A0J;
        boolean z = false;
        if (r2 != null && (!(r2 instanceof C24151Sm) ? r2.ArU() > 0 : ((C24151Sm) r2).A03.ArU() > 0)) {
            z = true;
        }
        boolean z2 = !z;
        View view = betterRecyclerView.A04;
        int i2 = 8;
        if (z2) {
            i = betterRecyclerView.A0C;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        if (!z2) {
            i2 = betterRecyclerView.A0C;
        }
        super.setVisibility(i2);
    }

    public void A0i(int i) {
        C005505z.A03("BetterRecyclerView.offsetChildrenVertical", 1953632976);
        try {
            super.A0i(i);
        } finally {
            C005505z.A00(-2056748962);
        }
    }

    public void A0v(C20831Dz r3) {
        C20831Dz r1 = this.A0J;
        if (r1 != null) {
            r1.CJm(this.A0Q);
            r1.CJm(this.A0S);
            r1.CJm(this.A0R);
        }
        super.A0v(r3);
        if (r3 != null) {
            r3.C0O(this.A0S);
            r3.C0O(this.A0Q);
            r3.C0O(this.A0R);
        }
        A01(this);
    }

    public void A10(AnonymousClass19T r2) {
        if (r2 != null) {
            r2.A1R(false);
        }
        super.A10(r2);
    }

    public boolean A1C(int i, int i2) {
        double d;
        if (!this.A0A) {
            if (!this.A09) {
                this.A00 = 1.0d;
                this.A09 = true;
            }
            if (0 != 0) {
                d = this.A02 * this.A00;
            } else {
                d = this.A00;
            }
            this.A01 = d;
            this.A0A = true;
        }
        return super.A1C(i, (int) (((double) i2) * this.A01));
    }

    public C20891Ef A1F() {
        AnonymousClass19T r1 = this.A0L;
        Preconditions.checkState(r1 instanceof C20891Ef);
        return (C20891Ef) r1;
    }

    public void A1G(C20041Ar r3) {
        C195618t r0 = this.A0K;
        synchronized (r0.A00) {
            r0.A00.add(r3);
        }
    }

    public void A1H(D61 d61) {
        if (d61 == null && this.A0F) {
            A11(this.A0N);
        }
        if (!this.A0F && d61 != null) {
            this.A12.add(this.A0N);
        }
        this.A07 = d61;
        boolean z = false;
        if (d61 != null) {
            z = true;
        }
        this.A0F = z;
    }

    public void A1J(boolean z) {
        if (z) {
            if (this.A0D == null) {
                this.A0D = new C19931Ag(this);
            }
            this.A0L.add(this.A0D);
            return;
        }
        this.A0L.remove(this.A0D);
    }

    public void C0j(C197519n r2) {
        if (this.A0E == null) {
            this.A0E = new ArrayList();
        }
        this.A0E.add(r2);
    }

    public void CJu(C197519n r2) {
        List list = this.A0E;
        if (list != null) {
            list.remove(r2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (((X.C20891Ef) r1).AZp() <= 0) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003c, code lost:
        if (((X.AnonymousClass19S) r1).A1u() <= 0) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean canScrollVertically(int r5) {
        /*
            r4 = this;
            if (r5 >= 0) goto L_0x0044
            X.19T r0 = r4.A0L
            if (r0 == 0) goto L_0x0044
            int r0 = r4.getChildCount()
            if (r0 <= 0) goto L_0x0044
            boolean r0 = r4.getClipToPadding()
            r3 = 0
            if (r0 == 0) goto L_0x003f
            r1 = 0
        L_0x0014:
            android.view.View r0 = r4.getChildAt(r3)
            int r0 = r0.getTop()
            r2 = 0
            if (r0 < r1) goto L_0x0020
            r2 = 1
        L_0x0020:
            X.19T r1 = r4.A0L
            boolean r0 = r1 instanceof X.C20891Ef
            if (r0 == 0) goto L_0x0032
            X.1Ef r1 = (X.C20891Ef) r1
            int r0 = r1.AZp()
            if (r0 > 0) goto L_0x0030
        L_0x002e:
            if (r2 != 0) goto L_0x0031
        L_0x0030:
            r3 = 1
        L_0x0031:
            return r3
        L_0x0032:
            boolean r0 = r1 instanceof X.AnonymousClass19S
            if (r0 == 0) goto L_0x0044
            X.19S r1 = (X.AnonymousClass19S) r1
            int r0 = r1.A1u()
            if (r0 > 0) goto L_0x0030
            goto L_0x002e
        L_0x003f:
            int r1 = r4.getPaddingTop()
            goto L_0x0014
        L_0x0044:
            boolean r0 = super.canScrollVertically(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.recyclerview.BetterRecyclerView.canScrollVertically(int):boolean");
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        C20831Dz r1 = this.A0J;
        if ((r1 instanceof C05280Yi) && ((C05280Yi) r1).BEO()) {
            return false;
        }
        if (0 != 0 && motionEvent.getActionMasked() == 0) {
            C96334ib r0 = null;
            r0.onTouchDown(this, motionEvent);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void draw(Canvas canvas) {
        C005505z.A03("BetterRecyclerView.draw", -1552026474);
        try {
            super.draw(canvas);
            this.A0K.A00();
            C005505z.A00(1952474166);
        } catch (NullPointerException e) {
            int childCount = getChildCount();
            ArrayList arrayList = new ArrayList(childCount);
            for (int i = 0; i < childCount; i++) {
                arrayList.add(getChildAt(i));
            }
            throw new RuntimeException("Expected:" + childCount + " Children:" + arrayList, e);
        } catch (Throwable th) {
            C005505z.A00(678047310);
            throw th;
        }
    }

    public boolean getClipToPadding() {
        if (Build.VERSION.SDK_INT >= 21) {
            return super.getClipToPadding();
        }
        return this.A0H;
    }

    public int getVisibility() {
        return this.A0C;
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        AnonymousClass1AS r0 = this.A06;
        if (r0 != null) {
            r0.BPg(this);
        }
        super.onLayout(z, i, i2, i3, i4);
        this.A0B = true;
        AnonymousClass1AS r02 = this.A06;
        if (r02 != null) {
            r02.BNq(this);
        }
    }

    public void setClipToPadding(boolean z) {
        this.A0H = z;
        super.setClipToPadding(z);
    }

    public void setVisibility(int i) {
        this.A0C = i;
        A01(this);
    }

    private void A00() {
        this.A05 = new AnonymousClass0UN(1, AnonymousClass1XX.get(getContext()));
        this.A0C = super.getVisibility();
        setDescendantFocusability(DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP);
        A12(this.A0P);
    }

    public void A1I(D6O d6o) {
        boolean z = true;
        if (!isLongClickable()) {
            setLongClickable(true);
        }
        if (d6o == null && this.A0G) {
            A11(this.A0O);
        }
        if (!this.A0G && d6o != null) {
            this.A12.add(this.A0O);
        }
        this.A08 = d6o;
        if (d6o == null) {
            z = false;
        }
        this.A0G = z;
    }

    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        List list = this.A0E;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                AnonymousClass1Ri.A0H(((C197519n) this.A0E.get(i)).A00);
            }
        }
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        AnonymousClass1AS r0 = this.A06;
        if (r0 != null) {
            r0.BeZ();
        }
    }

    public void A15(C26821DCx dCx) {
        super.A15(dCx);
    }

    public BetterRecyclerView(Context context) {
        super(context);
        A00();
    }

    public BetterRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public BetterRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }
}
