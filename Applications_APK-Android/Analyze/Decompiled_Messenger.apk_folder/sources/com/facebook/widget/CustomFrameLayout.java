package com.facebook.widget;

import X.AnonymousClass08S;
import X.AnonymousClass8WA;
import X.C005505z;
import X.C008407o;
import X.C008707s;
import X.C05260Yg;
import X.C13580rg;
import X.C29201g2;
import X.C30400Evb;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import com.facebook.resources.ui.FbFrameLayout;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class CustomFrameLayout extends FbFrameLayout implements C13580rg, C29201g2 {
    private int A00;
    private String A01 = null;
    private String A02 = null;
    private String A03 = null;
    private boolean A04 = true;

    private final void A00(Context context, AttributeSet attributeSet, int i) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C008407o.A0Z, i, i);
            this.A03 = obtainStyledAttributes.getString(0);
            obtainStyledAttributes.recycle();
            String str = this.A03;
            if (str != null) {
                this.A02 = AnonymousClass08S.A0J(str, ".onMeasure");
                this.A01 = AnonymousClass08S.A0J(str, ".onLayout");
            }
        }
    }

    public void A0L(int i) {
        int i2;
        this.A00 = i;
        boolean A002 = C008707s.A00();
        if (A002) {
            String str = this.A03;
            if (str == null) {
                str = C05260Yg.A00(getClass());
            }
            C005505z.A05("%s.setContentView", str, 281185178);
        }
        try {
            LayoutInflater.from(getContext()).inflate(i, this);
            if (A002) {
                i2 = 379809121;
                C005505z.A00(i2);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A00, e);
            if (A002) {
                i2 = -1504204263;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A00, e2);
            if (A002) {
                i2 = -1896564610;
            }
        } catch (Throwable th) {
            if (A002) {
                C005505z.A00(1128137987);
            }
            throw th;
        }
    }

    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        if (this.A04) {
            super.dispatchRestoreInstanceState(sparseArray);
        }
    }

    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        if (this.A04) {
            super.dispatchSaveInstanceState(sparseArray);
        }
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        String str = this.A01;
        boolean z2 = false;
        if (str != null) {
            z2 = true;
        }
        if (z2) {
            C005505z.A03(str, 237563777);
        }
        try {
            super.onLayout(z, i, i2, i3, i4);
            if (z2) {
                i5 = -1760800179;
                C005505z.A00(i5);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A00, e);
            if (z2) {
                i5 = 568867183;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A00, e2);
            if (z2) {
                i5 = -790861244;
            }
        } catch (Throwable th) {
            if (z2) {
                C005505z.A00(1425475279);
            }
            throw th;
        }
    }

    public void onMeasure(int i, int i2) {
        int i3;
        String str = this.A02;
        boolean z = false;
        if (str != null) {
            z = true;
        }
        if (z) {
            C005505z.A03(str, -848304154);
        }
        try {
            super.onMeasure(i, i2);
            if (z) {
                i3 = 1675391452;
                C005505z.A00(i3);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A00, e);
            if (z) {
                i3 = 735485904;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A00, e2);
            if (z) {
                i3 = -972226976;
            }
        } catch (Throwable th) {
            if (z) {
                C005505z.A00(-1941686848);
            }
            throw th;
        }
    }

    public void detachRecyclableViewFromParent(View view) {
        super.detachViewFromParent(view);
        requestLayout();
    }

    public void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);
            CopyOnWriteArrayList copyOnWriteArrayList = null;
            if (copyOnWriteArrayList != null && !copyOnWriteArrayList.isEmpty()) {
                CopyOnWriteArrayList copyOnWriteArrayList2 = null;
                Iterator it = copyOnWriteArrayList2.iterator();
                while (it.hasNext()) {
                    C30400Evb evb = (C30400Evb) it.next();
                    if (evb.onDispatchDraw()) {
                        CopyOnWriteArrayList copyOnWriteArrayList3 = null;
                        copyOnWriteArrayList3.remove(evb);
                    }
                }
            }
        } catch (RuntimeException | StackOverflowError e) {
            AnonymousClass8WA.A00(this, this.A00, e);
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onAttachedToWindow() {
        /*
            r2 = this;
            r0 = 1337125802(0x4fb2ebaa, float:6.0035779E9)
            int r1 = X.C000700l.A06(r0)
            super.onAttachedToWindow()
            r0 = 0
            if (r0 == 0) goto L_0x0010
            r0.A00(r0)
        L_0x0010:
            r0 = -1525131183(0xffffffffa5185851, float:-1.3213821E-16)
            X.C000700l.A0C(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomFrameLayout.onAttachedToWindow():void");
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onDetachedFromWindow() {
        /*
            r2 = this;
            r0 = -1646192049(0xffffffff9de11a4f, float:-5.9584207E-21)
            int r1 = X.C000700l.A06(r0)
            super.onDetachedFromWindow()
            r0 = 0
            if (r0 == 0) goto L_0x0010
            r0.A01(r0)
        L_0x0010:
            r0 = 1938844599(0x73906bb7, float:2.2884383E31)
            X.C000700l.A0C(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomFrameLayout.onDetachedFromWindow():void");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onFinishTemporaryDetach() {
        /*
            r1 = this;
            super.onFinishTemporaryDetach()
            r0 = 0
            if (r0 == 0) goto L_0x0009
            r0.A00(r0)
        L_0x0009:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomFrameLayout.onFinishTemporaryDetach():void");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onStartTemporaryDetach() {
        /*
            r1 = this;
            super.onStartTemporaryDetach()
            r0 = 0
            if (r0 == 0) goto L_0x0009
            r0.A01(r0)
        L_0x0009:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomFrameLayout.onStartTemporaryDetach():void");
    }

    public void removeRecyclableViewFromParent(View view, boolean z) {
        super.removeDetachedView(view, z);
    }

    public void restoreHierarchyState(SparseArray sparseArray) {
        super.dispatchRestoreInstanceState(sparseArray);
    }

    public void saveHierarchyState(SparseArray sparseArray) {
        super.dispatchSaveInstanceState(sparseArray);
    }

    public CustomFrameLayout(Context context) {
        super(context);
        A00(context, null, 0);
    }

    public CustomFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context, attributeSet, 0);
    }

    public CustomFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00(context, attributeSet, i);
    }
}
