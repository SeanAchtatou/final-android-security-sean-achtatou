package com.facebook.widget.listview;

import X.AnonymousClass069;
import X.AnonymousClass0UN;
import X.AnonymousClass1L3;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass2T7;
import X.AnonymousClass2ZS;
import X.C008407o;
import X.C012809p;
import X.C11680nc;
import X.C37531vp;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.ViewStub;
import android.widget.TextView;
import com.facebook.widget.CustomRelativeLayout;
import com.google.common.base.Platform;

public class EmptyListViewItem extends CustomRelativeLayout {
    public AnonymousClass0UN A00;
    private long A01;
    private ViewStub A02;
    private TextView A03;

    public void A0I(boolean z) {
        boolean z2;
        int i = 0;
        if (z) {
            boolean z3 = false;
            if (this.A02.getVisibility() != 0) {
                z3 = true;
            }
            if (z3) {
                this.A01 = ((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, this.A00)).now();
            }
        }
        if (!z && this.A01 != 0) {
            int i2 = AnonymousClass1Y3.AMC;
            AnonymousClass0UN r1 = this.A00;
            C11680nc r8 = (C11680nc) AnonymousClass1XX.A02(0, i2, r1);
            long now = ((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, r1)).now() - this.A01;
            ViewStub viewStub = this.A02;
            if (viewStub == null || viewStub.getVisibility() != 0) {
                z2 = false;
            } else {
                if (now > 0) {
                    ((AnonymousClass2ZS) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AhD, r8.A00)).A04("progress_spinner_time", now);
                }
                z2 = true;
            }
            if (z2) {
                this.A01 = 0;
            }
        }
        ViewStub viewStub2 = this.A02;
        if (!z) {
            i = 8;
        }
        viewStub2.setVisibility(i);
    }

    private void A01() {
        boolean stringIsNullOrEmpty = Platform.stringIsNullOrEmpty(this.A03.getText().toString());
        TextView textView = this.A03;
        int i = 0;
        if (stringIsNullOrEmpty) {
            i = 8;
        }
        textView.setVisibility(i);
    }

    public void A0G(int i) {
        this.A03.setText(i);
        A01();
    }

    public void A0H(CharSequence charSequence) {
        this.A03.setText(charSequence);
        A01();
    }

    public void setTextColor(int i) {
        this.A03.setTextColor(i);
    }

    private void A00() {
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(getContext()));
        A0F(2132411598);
        this.A02 = (ViewStub) C012809p.A01(this, 2131297804);
        this.A03 = (TextView) C012809p.A01(this, 2131297805);
        if (getBackground() == null) {
            C37531vp.A02(this, new ColorDrawable(AnonymousClass2T7.A00(getContext(), AnonymousClass1L3.A1g)));
        }
    }

    public EmptyListViewItem(Context context) {
        super(context);
        A00();
    }

    public EmptyListViewItem(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public EmptyListViewItem(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, C008407o.A0s);
        if (obtainStyledAttributes.peekValue(0) != null) {
            this.A03.setTextColor(obtainStyledAttributes.getColor(0, 0));
        }
        obtainStyledAttributes.recycle();
    }
}
