package com.facebook.widget;

import X.AnonymousClass01R;
import X.C20871Ed;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

public class FbSwipeRefreshLayout extends C20871Ed {
    private float A00;
    private int A01;
    private boolean A02;
    private boolean A03;

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (this.A03 != z) {
            this.A03 = z;
            super.requestDisallowInterceptTouchEvent(z);
        }
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(z);
        }
    }

    private void A00(Context context) {
        this.A01 = ViewConfiguration.get(context).getScaledTouchSlop();
        int[] iArr = {2132082998};
        Context context2 = getContext();
        int[] iArr2 = new int[1];
        for (int i = 0; i < 1; i++) {
            iArr2[i] = AnonymousClass01R.A00(context2, iArr[i]);
        }
        A0D(iArr2);
        this.A0A.setBackgroundColor(AnonymousClass01R.A00(getContext(), 2132082727));
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (!this.A03 || !(action == 1 || action == 3)) {
            int action2 = motionEvent.getAction();
            if (action2 == 0) {
                this.A00 = motionEvent.getX();
                this.A02 = false;
            } else if (action2 == 2) {
                float abs = Math.abs(motionEvent.getX() - this.A00);
                if (this.A02 || abs > ((float) this.A01)) {
                    this.A02 = true;
                    return false;
                }
            }
            if (this.A03 || !super.onInterceptTouchEvent(motionEvent)) {
                return false;
            }
            return true;
        }
        this.A03 = false;
        return false;
    }

    public FbSwipeRefreshLayout(Context context) {
        this(context, null);
        A00(context);
    }

    public FbSwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context);
    }
}
