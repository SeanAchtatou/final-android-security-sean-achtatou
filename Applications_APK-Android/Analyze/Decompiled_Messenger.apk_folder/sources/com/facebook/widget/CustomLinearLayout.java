package com.facebook.widget;

import X.AnonymousClass08S;
import X.AnonymousClass8WA;
import X.C000700l;
import X.C005505z;
import X.C008407o;
import X.C008707s;
import X.C05260Yg;
import X.C13580rg;
import X.C25011Xz;
import X.C29201g2;
import X.C30400Evb;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import com.facebook.resources.ui.FbLinearLayout;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

public class CustomLinearLayout extends FbLinearLayout implements C13580rg, C29201g2 {
    private int A00;
    private Drawable A01;
    private String A02 = null;
    private String A03 = null;
    private String A04 = null;
    private boolean A05 = true;
    private boolean A06 = true;

    private final void A00(Context context, AttributeSet attributeSet, int i) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C008407o.A0b, i, 0);
            this.A04 = obtainStyledAttributes.getString(0);
            obtainStyledAttributes.recycle();
            String str = this.A04;
            if (str != null) {
                this.A03 = AnonymousClass08S.A0J(str, ".onMeasure");
                this.A02 = AnonymousClass08S.A0J(str, ".onLayout");
            }
        }
    }

    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        if (this.A06) {
            super.dispatchRestoreInstanceState(sparseArray);
        }
    }

    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        if (this.A06) {
            super.dispatchSaveInstanceState(sparseArray);
        }
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        String str = this.A02;
        boolean z2 = false;
        if (str != null) {
            z2 = true;
        }
        if (z2) {
            C005505z.A03(str, 1897558862);
        }
        try {
            super.onLayout(z, i, i2, i3, i4);
            if (z2) {
                i5 = -1135152635;
                C005505z.A00(i5);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A00, e);
            if (z2) {
                i5 = 776029012;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A00, e2);
            if (z2) {
                i5 = -1549704968;
            }
        } catch (Throwable th) {
            if (z2) {
                C005505z.A00(-1357054523);
            }
            throw th;
        }
    }

    public void onMeasure(int i, int i2) {
        int i3;
        String str = this.A03;
        boolean z = false;
        if (str != null) {
            z = true;
        }
        if (z) {
            C005505z.A03(str, -148907672);
        }
        try {
            super.onMeasure(i, i2);
            if (z) {
                i3 = -441725254;
                C005505z.A00(i3);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A00, e);
            if (z) {
                i3 = -1727231000;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A00, e2);
            if (z) {
                i3 = -1165488897;
            }
        } catch (Throwable th) {
            if (z) {
                C005505z.A00(420976946);
            }
            throw th;
        }
    }

    public void setForeground(Drawable drawable) {
        Drawable drawable2 = this.A01;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
                unscheduleDrawable(this.A01);
            }
            this.A01 = drawable;
            if (drawable != null) {
                setWillNotDraw(false);
                drawable.setCallback(this);
                if (drawable.isStateful()) {
                    drawable.setState(getDrawableState());
                }
            } else {
                setWillNotDraw(true);
            }
            this.A05 = true;
            invalidate();
        }
    }

    public void A0S(int i) {
        int i2;
        boolean A002 = C008707s.A00();
        this.A00 = i;
        if (A002) {
            String str = this.A04;
            if (str == null) {
                str = C05260Yg.A00(getClass());
            }
            if (getContext() == null || getContext().getResources() == null) {
                C005505z.A05("%s.setContentView", str, 1504134084);
            } else {
                C005505z.A06("%s.setContentView(%s)", str, getContext().getResources().getResourceName(i), 1369981503);
            }
        }
        try {
            LayoutInflater.from(getContext()).inflate(i, this);
            if (A002) {
                i2 = -794293868;
                C005505z.A00(i2);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A00, e);
            if (A002) {
                i2 = 864374650;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A00, e2);
            if (A002) {
                i2 = -1278705260;
            }
        } catch (Throwable th) {
            if (A002) {
                C005505z.A00(1363444684);
            }
            throw th;
        }
    }

    public void detachRecyclableViewFromParent(View view) {
        super.detachViewFromParent(view);
        requestLayout();
    }

    public void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);
            CopyOnWriteArraySet copyOnWriteArraySet = null;
            if (copyOnWriteArraySet != null) {
                HashSet A032 = C25011Xz.A03();
                Iterator it = copyOnWriteArraySet.iterator();
                while (it.hasNext()) {
                    C30400Evb evb = (C30400Evb) it.next();
                    if (evb.onDispatchDraw()) {
                        A032.add(evb);
                    }
                }
                copyOnWriteArraySet.removeAll(A032);
                copyOnWriteArraySet.isEmpty();
            }
        } catch (RuntimeException | StackOverflowError e) {
            AnonymousClass8WA.A00(this, this.A00, e);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Drawable drawable = this.A01;
        if (drawable != null) {
            if (this.A05) {
                this.A05 = false;
                drawable.setBounds(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
            }
            this.A01.draw(canvas);
        }
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.A01;
        if (drawable != null && drawable.isStateful()) {
            this.A01.setState(getDrawableState());
        }
    }

    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int A062 = C000700l.A06(-1915595218);
        super.onSizeChanged(i, i2, i3, i4);
        this.A05 = true;
        C000700l.A0C(454517865, A062);
    }

    public void removeRecyclableViewFromParent(View view, boolean z) {
        super.removeDetachedView(view, z);
    }

    public void restoreHierarchyState(SparseArray sparseArray) {
        try {
            super.dispatchRestoreInstanceState(sparseArray);
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A00, e);
        }
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        Drawable drawable = this.A01;
        if (drawable != null) {
            boolean z = false;
            if (i == 0) {
                z = true;
            }
            drawable.setVisible(z, false);
        }
    }

    public boolean verifyDrawable(Drawable drawable) {
        if (super.verifyDrawable(drawable) || drawable == this.A01) {
            return true;
        }
        return false;
    }

    public void saveHierarchyState(SparseArray sparseArray) {
        super.dispatchSaveInstanceState(sparseArray);
    }

    public CustomLinearLayout(Context context) {
        super(context);
        A00(context, null, 0);
    }

    public CustomLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context, attributeSet, 0);
    }

    public CustomLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00(context, attributeSet, i);
    }
}
