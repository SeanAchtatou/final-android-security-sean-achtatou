package com.facebook.widget;

import X.AnonymousClass08S;
import X.AnonymousClass59A;
import X.AnonymousClass7DA;
import X.AnonymousClass8WA;
import X.C005505z;
import X.C008407o;
import X.C008707s;
import X.C05260Yg;
import X.C13580rg;
import X.C25011Xz;
import X.C29201g2;
import X.C30400Evb;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import com.facebook.messaging.contacts.picker.ContactPickerSectionUpsellView;
import com.facebook.ui.compat.fbrelativelayout.FbRelativeLayout;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

public class CustomRelativeLayout extends FbRelativeLayout implements C13580rg, C29201g2 {
    public AnonymousClass7DA A00;
    private int A01;
    private String A02 = null;
    private String A03 = null;
    private String A04 = null;
    private boolean A05 = true;

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        AnonymousClass7DA r0;
        String str = this.A02;
        boolean z2 = false;
        if (str != null) {
            z2 = true;
        }
        if (z2) {
            C005505z.A03(str, -1195986438);
        }
        try {
            super.onLayout(z, i, i2, i3, i4);
            if (z && (r0 = this.A00) != null) {
                ContactPickerSectionUpsellView contactPickerSectionUpsellView = r0.A00;
                r0.A00.setTouchDelegate(AnonymousClass59A.A00(contactPickerSectionUpsellView.A01, contactPickerSectionUpsellView, 15, 15, 15, 15));
            }
            if (z2) {
                i5 = 1262114901;
                C005505z.A00(i5);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A01, e);
            if (z2) {
                i5 = -1713978233;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A01, e2);
            if (z2) {
                i5 = 819144979;
            }
        } catch (Throwable th) {
            if (z2) {
                C005505z.A00(-660026058);
            }
            throw th;
        }
    }

    private final void A00(Context context, AttributeSet attributeSet, int i) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C008407o.A0c, i, i);
            this.A04 = obtainStyledAttributes.getString(0);
            obtainStyledAttributes.recycle();
            String str = this.A04;
            if (str != null) {
                this.A03 = AnonymousClass08S.A0J(str, ".onMeasure");
                this.A02 = AnonymousClass08S.A0J(str, ".onLayout");
            }
        }
    }

    public void A0F(int i) {
        int i2;
        this.A01 = i;
        boolean A002 = C008707s.A00();
        if (A002) {
            String str = this.A04;
            if (str == null) {
                str = C05260Yg.A00(getClass());
            }
            if (getContext() == null || getContext().getResources() == null) {
                C005505z.A05("%s.setContentView", str, -836442554);
            } else {
                C005505z.A06("%s.setContentView(%s)", str, getContext().getResources().getResourceName(i), 1635835333);
            }
        }
        try {
            LayoutInflater.from(getContext()).inflate(i, this);
            if (A002) {
                i2 = -793650861;
                C005505z.A00(i2);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A01, e);
            if (A002) {
                i2 = -2092385761;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A01, e2);
            if (A002) {
                i2 = 1873287411;
            }
        } catch (Throwable th) {
            if (A002) {
                C005505z.A00(571194144);
            }
            throw th;
        }
    }

    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        if (this.A05) {
            super.dispatchRestoreInstanceState(sparseArray);
        }
    }

    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        if (this.A05) {
            super.dispatchSaveInstanceState(sparseArray);
        }
    }

    public void onMeasure(int i, int i2) {
        int i3;
        String str = this.A03;
        boolean z = false;
        if (str != null) {
            z = true;
        }
        if (z) {
            C005505z.A03(str, 1104401477);
        }
        try {
            super.onMeasure(i, i2);
            if (z) {
                i3 = -1469771637;
                C005505z.A00(i3);
            }
        } catch (RuntimeException e) {
            AnonymousClass8WA.A00(this, this.A01, e);
            if (z) {
                i3 = 1760426241;
            }
        } catch (StackOverflowError e2) {
            AnonymousClass8WA.A00(this, this.A01, e2);
            if (z) {
                i3 = -1053747978;
            }
        } catch (Throwable th) {
            if (z) {
                C005505z.A00(1192892084);
            }
            throw th;
        }
    }

    public void detachRecyclableViewFromParent(View view) {
        super.detachViewFromParent(view);
        requestLayout();
    }

    public void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);
            CopyOnWriteArraySet copyOnWriteArraySet = null;
            if (copyOnWriteArraySet != null) {
                HashSet A032 = C25011Xz.A03();
                Iterator it = copyOnWriteArraySet.iterator();
                while (it.hasNext()) {
                    C30400Evb evb = (C30400Evb) it.next();
                    if (evb.onDispatchDraw()) {
                        A032.add(evb);
                    }
                }
                copyOnWriteArraySet.removeAll(A032);
                copyOnWriteArraySet.isEmpty();
            }
        } catch (RuntimeException | StackOverflowError e) {
            AnonymousClass8WA.A00(this, this.A01, e);
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onAttachedToWindow() {
        /*
            r2 = this;
            r0 = -622227747(0xffffffffdae98edd, float:-3.28703747E16)
            int r1 = X.C000700l.A06(r0)
            super.onAttachedToWindow()
            r0 = 0
            if (r0 == 0) goto L_0x0010
            r0.A00(r0)
        L_0x0010:
            r0 = 1807901340(0x6bc2629c, float:4.6999456E26)
            X.C000700l.A0C(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomRelativeLayout.onAttachedToWindow():void");
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onDetachedFromWindow() {
        /*
            r2 = this;
            r0 = 1549641150(0x5c5da5be, float:2.49552821E17)
            int r1 = X.C000700l.A06(r0)
            super.onDetachedFromWindow()
            r0 = 0
            if (r0 == 0) goto L_0x0010
            r0.A01(r0)
        L_0x0010:
            r0 = -2144173716(0xffffffff8032816c, float:-4.638203E-39)
            X.C000700l.A0C(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomRelativeLayout.onDetachedFromWindow():void");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onFinishTemporaryDetach() {
        /*
            r1 = this;
            super.onFinishTemporaryDetach()
            r0 = 0
            if (r0 == 0) goto L_0x0009
            r0.A00(r0)
        L_0x0009:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomRelativeLayout.onFinishTemporaryDetach():void");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [X.0cG, X.6A4] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onStartTemporaryDetach() {
        /*
            r1 = this;
            super.onStartTemporaryDetach()
            r0 = 0
            if (r0 == 0) goto L_0x0009
            r0.A01(r0)
        L_0x0009:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.CustomRelativeLayout.onStartTemporaryDetach():void");
    }

    public void removeRecyclableViewFromParent(View view, boolean z) {
        super.removeDetachedView(view, z);
    }

    public void restoreHierarchyState(SparseArray sparseArray) {
        super.dispatchRestoreInstanceState(sparseArray);
    }

    public void saveHierarchyState(SparseArray sparseArray) {
        super.dispatchSaveInstanceState(sparseArray);
    }

    public CustomRelativeLayout(Context context) {
        super(context);
        A00(context, null, 0);
    }

    public CustomRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context, attributeSet, 0);
    }

    public CustomRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00(context, attributeSet, i);
    }
}
