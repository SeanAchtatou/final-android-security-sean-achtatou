package com.facebook.widget.text;

import X.AnonymousClass07B;
import X.AnonymousClass1I9;
import X.AnonymousClass24F;
import X.AnonymousClass3DP;
import X.AnonymousClass5X0;
import X.C000700l;
import X.C008407o;
import X.C013609x;
import X.C21651Ie;
import X.C21871Ja;
import X.C32031l0;
import X.CNK;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.text.Layout;
import android.text.Spanned;
import android.text.method.SingleLineTransformationMethod;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.resources.ui.FbTextView;
import java.util.ArrayList;

public class BetterTextView extends FbTextView {
    public Object A00;
    public boolean A01;
    private int A02;
    private boolean A03;
    private boolean A04;
    private boolean A05;

    private void A01(Context context, TypedArray typedArray) {
        Integer num;
        C32031l0 r1;
        this.A03 = typedArray.getBoolean(5, false);
        this.A04 = typedArray.getBoolean(6, false);
        this.A02 = typedArray.getDimensionPixelOffset(4, -1);
        this.A05 = typedArray.getBoolean(7, false);
        if (typedArray.getBoolean(8, false)) {
            C21651Ie.A01(this, AnonymousClass1I9.A02);
        }
        int i = typedArray.getInt(2, -1);
        int i2 = typedArray.getInt(3, -1);
        if (i == -1) {
            num = AnonymousClass07B.A01;
        } else {
            num = AnonymousClass24F.A00[i];
        }
        if (i2 == -1) {
            r1 = C32031l0.UNSET;
        } else {
            r1 = C32031l0.A00[i2];
        }
        C21871Ja.A02(this, num, r1, getTypeface());
        if (typedArray.getBoolean(1, false)) {
            setTransformationMethod(new CNK(context.getResources()));
        }
    }

    public void onStartTemporaryDetach() {
        AnonymousClass5X0 r0 = null;
        if (r0 != null) {
            r0.BW7(this);
        }
        super.onStartTemporaryDetach();
    }

    public void onDraw(Canvas canvas) {
        Object obj = this.A00;
        if (!(obj instanceof AnonymousClass3DP) && (obj instanceof ArrayList)) {
            ArrayList arrayList = (ArrayList) obj;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                arrayList.get(i);
            }
        }
        if ((getTransformationMethod() instanceof SingleLineTransformationMethod) && getMovementMethod() != null) {
            bringPointIntoView(0);
        }
        super.onDraw(canvas);
    }

    private int A00(boolean z) {
        float lineWidth;
        Layout layout = getLayout();
        int lineCount = layout.getLineCount();
        float f = 0.0f;
        for (int i = 0; i < lineCount; i++) {
            if (!z) {
                lineWidth = layout.getLineMax(i);
            } else {
                lineWidth = layout.getLineWidth(i);
            }
            f = Math.max(f, lineWidth);
        }
        return (int) f;
    }

    public void onAttachedToWindow() {
        int A06 = C000700l.A06(161653103);
        super.onAttachedToWindow();
        AnonymousClass5X0 r0 = null;
        if (r0 != null) {
            r0.BOa(this);
        }
        C000700l.A0C(-1943345237, A06);
    }

    public void onDetachedFromWindow() {
        int A06 = C000700l.A06(-343229399);
        AnonymousClass5X0 r0 = null;
        if (r0 != null) {
            r0.BW7(this);
        }
        super.onDetachedFromWindow();
        C000700l.A0C(-1480510811, A06);
    }

    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        AnonymousClass5X0 r0 = null;
        if (r0 != null) {
            r0.BOa(this);
        }
    }

    public void onMeasure(int i, int i2) {
        int A002;
        int A06 = C000700l.A06(1828579077);
        int mode = View.MeasureSpec.getMode(i2);
        if (this.A05 && (mode == Integer.MIN_VALUE || mode == 1073741824)) {
            setMaxLines(((View.MeasureSpec.getSize(i2) - getPaddingBottom()) - getPaddingTop()) / getLineHeight());
        }
        super.onMeasure(i, i2);
        boolean z = true;
        if (getLineCount() > 1) {
            int mode2 = View.MeasureSpec.getMode(i);
            if (this.A02 > 0 && mode2 == Integer.MIN_VALUE) {
                int size = View.MeasureSpec.getSize(i);
                int size2 = View.MeasureSpec.getSize(i);
                int A003 = A00(false);
                if (A003 < size && size - A003 < this.A02) {
                    if (size2 < size) {
                        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), i2);
                    }
                    if (this.A03 && ((mode2 == 1073741824 || mode2 == Integer.MIN_VALUE) && !z && (A002 = A00(this.A04) + getCompoundPaddingLeft() + getCompoundPaddingRight()) < getMeasuredWidth())) {
                        super.onMeasure(View.MeasureSpec.makeMeasureSpec(A002, 1073741824), i2);
                    }
                }
            }
            z = false;
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(A002, 1073741824), i2);
        }
        C000700l.A0C(213272482, A06);
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        if (charSequence instanceof Spanned) {
            for (ImageSpan drawable : (ImageSpan[]) ((Spanned) charSequence).getSpans(0, charSequence.length() - 1, ImageSpan.class)) {
                drawable.getDrawable().setCallback(this);
            }
        }
        this.A01 = C013609x.A03(this, charSequence, i3);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:3:0x0014 */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [int] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5, types: [boolean] */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            r0 = 2134529245(0x7f3a54dd, float:2.4767704E38)
            int r4 = X.C000700l.A05(r0)
            java.lang.Object r3 = r5.A00
            boolean r0 = r3 instanceof X.AnonymousClass3DP
            r2 = 0
            if (r0 == 0) goto L_0x0021
            X.3DP r3 = (X.AnonymousClass3DP) r3
            boolean r2 = r3.A07(r6)
        L_0x0014:
            if (r2 != 0) goto L_0x001a
            boolean r2 = super.onTouchEvent(r6)
        L_0x001a:
            r0 = 358378038(0x155c6a36, float:4.4512432E-26)
            X.C000700l.A0B(r0, r4)
            return r2
        L_0x0021:
            boolean r0 = r3 instanceof java.util.ArrayList
            if (r0 == 0) goto L_0x0014
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            int r1 = r3.size()
            r0 = 0
        L_0x002c:
            if (r2 >= r1) goto L_0x003c
            java.lang.Object r0 = r3.get(r2)
            X.3DP r0 = (X.AnonymousClass3DP) r0
            boolean r0 = r0.A07(r6)
            int r2 = r2 + 1
            if (r0 == 0) goto L_0x002c
        L_0x003c:
            r2 = r0
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.text.BetterTextView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, C008407o.A0G);
        A01(context, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }

    public BetterTextView(Context context) {
        this(context, null, 0);
    }

    public BetterTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BetterTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C008407o.A0G, i, 0);
        A01(context, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }
}
