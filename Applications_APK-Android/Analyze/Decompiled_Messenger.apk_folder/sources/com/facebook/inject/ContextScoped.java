package com.facebook.inject;

import javax.inject.Scope;

@Scope
public @interface ContextScoped {
}
