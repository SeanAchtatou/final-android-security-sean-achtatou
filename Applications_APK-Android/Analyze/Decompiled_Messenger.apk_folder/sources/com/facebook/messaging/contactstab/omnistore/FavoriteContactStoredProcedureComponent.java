package com.facebook.messaging.contactstab.omnistore;

import X.AnonymousClass07A;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1Uk;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2WT;
import X.AnonymousClass52R;
import X.AnonymousClass52c;
import X.C010708t;
import X.C04310Tq;
import X.C05350Yp;
import X.C06850cB;
import X.C188215g;
import X.C24971Xv;
import X.C851241v;
import com.facebook.acra.LogCatCollector;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.SettableFuture;
import io.card.payment.BuildConfig;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.concurrent.Executor;
import javax.inject.Singleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
public final class FavoriteContactStoredProcedureComponent implements OmnistoreStoredProcedureComponent {
    public static final Charset A04 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    private static volatile FavoriteContactStoredProcedureComponent A05;
    public AnonymousClass0UN A00;
    public AnonymousClass2WT A01;
    public SettableFuture A02 = null;
    public final C04310Tq A03;

    public int provideStoredProcedureId() {
        return 70;
    }

    public static final FavoriteContactStoredProcedureComponent A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (FavoriteContactStoredProcedureComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new FavoriteContactStoredProcedureComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public void A01(ImmutableList immutableList, ImmutableList immutableList2) {
        CollectionName collectionName;
        SettableFuture settableFuture;
        AnonymousClass2WT r0 = this.A01;
        if (r0 == null) {
            synchronized (this) {
                if (this.A02 == null) {
                    this.A02 = SettableFuture.create();
                    AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A1n, this.A00), new AnonymousClass52c(this), -55525751);
                }
                settableFuture = this.A02;
            }
            C05350Yp.A07(settableFuture, new C851241v(this, immutableList, immutableList2));
        } else if (r0 == null) {
            C010708t.A0I("com.facebook.messaging.contactstab.omnistore.FavoriteContactStoredProcedureComponent", " no sender available for setFavoriteContacts");
        } else {
            String str = BuildConfig.FLAVOR;
            try {
                JSONArray jSONArray = new JSONArray();
                C24971Xv it = immutableList.iterator();
                while (it.hasNext()) {
                    jSONArray.put(Long.parseLong((String) it.next()));
                }
                JSONArray jSONArray2 = new JSONArray();
                C24971Xv it2 = immutableList2.iterator();
                while (it2.hasNext()) {
                    jSONArray2.put(Long.parseLong((String) it2.next()));
                }
                JSONObject put = new JSONObject().put("client_mutation_id", C188215g.A00().toString()).put("actor_id", Long.parseLong(((UserKey) this.A03.get()).id));
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("data", put);
                str = StringFormatUtil.formatStrLocaleSafe(new JSONObject().put("query_params", jSONObject).put("favorite_messenger_contacts_added", jSONArray).put("favorite_messenger_contacts_removed", jSONArray2).toString());
            } catch (NumberFormatException | JSONException e) {
                C010708t.A0T("com.facebook.messaging.contactstab.omnistore.FavoriteContactStoredProcedureComponent", e, " error while building request");
            }
            if (!C06850cB.A0B(str) && (collectionName = ((AnonymousClass1Uk) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ac6, this.A00)).A00) != null) {
                String queueIdentifier = collectionName.getQueueIdentifier().toString();
                AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A1n, this.A00), new AnonymousClass52R(this, str, queueIdentifier + C188215g.A00(), queueIdentifier), 189824205);
            }
        }
    }

    public void onSenderInvalidated() {
    }

    public void onStoredProcedureResult(ByteBuffer byteBuffer) {
        new String(byteBuffer.array());
    }

    private FavoriteContactStoredProcedureComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A03 = AnonymousClass0XJ.A0I(r3);
    }

    public void onSenderAvailable(AnonymousClass2WT r1) {
        this.A01 = r1;
    }
}
