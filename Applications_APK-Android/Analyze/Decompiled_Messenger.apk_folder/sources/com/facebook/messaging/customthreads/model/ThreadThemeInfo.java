package com.facebook.messaging.customthreads.model;

import X.AnonymousClass12T;
import X.AnonymousClass12U;
import X.C22298Ase;
import X.C24971Xv;
import X.C28921fa;
import X.C28931fb;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphql.enums.GraphQLMessengerThreadViewMode;
import com.google.common.collect.ImmutableList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class ThreadThemeInfo implements Parcelable, C28921fa {
    private static volatile GraphQLMessengerThreadViewMode A0B;
    public static final Parcelable.Creator CREATOR = new AnonymousClass12T();
    public final int A00;
    public final long A01;
    public final Uri A02;
    public final Uri A03;
    public final Uri A04;
    public final GraphQLMessengerThreadViewMode A05;
    public final ImmutableList A06;
    public final ImmutableList A07;
    public final String A08;
    public final Set A09;
    public final boolean A0A;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ThreadThemeInfo) {
                ThreadThemeInfo threadThemeInfo = (ThreadThemeInfo) obj;
                if (!C28931fb.A07(this.A08, threadThemeInfo.A08) || this.A00 != threadThemeInfo.A00 || !C28931fb.A07(this.A06, threadThemeInfo.A06) || this.A0A != threadThemeInfo.A0A || !C28931fb.A07(this.A02, threadThemeInfo.A02) || !C28931fb.A07(this.A03, threadThemeInfo.A03) || !C28931fb.A07(this.A07, threadThemeInfo.A07) || !C28931fb.A07(this.A04, threadThemeInfo.A04) || this.A01 != threadThemeInfo.A01 || B61() != threadThemeInfo.B61()) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public static AnonymousClass12U A00() {
        return new AnonymousClass12U();
    }

    public String AbY() {
        return this.A08;
    }

    public int AmT() {
        return this.A00;
    }

    public ImmutableList Ao6() {
        return this.A06;
    }

    public Uri Art() {
        return this.A02;
    }

    public Uri Aru() {
        return this.A03;
    }

    public ImmutableList B01() {
        return this.A07;
    }

    public Uri B3J() {
        return this.A04;
    }

    public long B5o() {
        return this.A01;
    }

    public GraphQLMessengerThreadViewMode B61() {
        if (this.A09.contains("threadViewMode")) {
            return this.A05;
        }
        if (A0B == null) {
            synchronized (this) {
                if (A0B == null) {
                    A0B = GraphQLMessengerThreadViewMode.DEFAULT;
                }
            }
        }
        return A0B;
    }

    public boolean BGP() {
        return this.A0A;
    }

    public int hashCode() {
        int ordinal;
        int A022 = C28931fb.A02(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A04(C28931fb.A03((C28931fb.A03(1, this.A08) * 31) + this.A00, this.A06), this.A0A), this.A02), this.A03), this.A07), this.A04), this.A01);
        GraphQLMessengerThreadViewMode B61 = B61();
        if (B61 == null) {
            ordinal = -1;
        } else {
            ordinal = B61.ordinal();
        }
        return (A022 * 31) + ordinal;
    }

    public String toString() {
        return "ThreadThemeInfo{accessibilityLabel=" + AbY() + ", " + "fallbackColor=" + AmT() + ", " + "gradientColors=" + Ao6() + ", " + "isReverseGradientsForRadial=" + BGP() + ", " + "largeBackgroundImageUri=" + Art() + ", " + "largeIconAssetUri=" + Aru() + ", " + "reactionAssets=" + B01() + ", " + "smallIconAssetUri=" + B3J() + ", " + "themeId=" + B5o() + ", " + "threadViewMode=" + B61() + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A08);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A06.size());
        C24971Xv it = this.A06.iterator();
        while (it.hasNext()) {
            parcel.writeInt(((Integer) it.next()).intValue());
        }
        parcel.writeInt(this.A0A ? 1 : 0);
        if (this.A02 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A02, i);
        }
        if (this.A03 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A03, i);
        }
        parcel.writeInt(this.A07.size());
        C24971Xv it2 = this.A07.iterator();
        while (it2.hasNext()) {
            parcel.writeParcelable((ThreadThemeReactionAssetInfo) it2.next(), i);
        }
        if (this.A04 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A04, i);
        }
        parcel.writeLong(this.A01);
        if (this.A05 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A05.ordinal());
        }
        parcel.writeInt(this.A09.size());
        for (String writeString : this.A09) {
            parcel.writeString(writeString);
        }
    }

    public ThreadThemeInfo(AnonymousClass12U r3) {
        String str = r3.A08;
        C28931fb.A06(str, C22298Ase.$const$string(26));
        this.A08 = str;
        this.A00 = r3.A00;
        ImmutableList immutableList = r3.A06;
        C28931fb.A06(immutableList, "gradientColors");
        this.A06 = immutableList;
        this.A0A = r3.A0A;
        this.A02 = r3.A02;
        this.A03 = r3.A03;
        ImmutableList immutableList2 = r3.A07;
        C28931fb.A06(immutableList2, "reactionAssets");
        this.A07 = immutableList2;
        this.A04 = r3.A04;
        this.A01 = r3.A01;
        this.A05 = r3.A05;
        this.A09 = Collections.unmodifiableSet(r3.A09);
    }

    public ThreadThemeInfo(Parcel parcel) {
        this.A08 = parcel.readString();
        this.A00 = parcel.readInt();
        int readInt = parcel.readInt();
        Integer[] numArr = new Integer[readInt];
        for (int i = 0; i < readInt; i++) {
            numArr[i] = Integer.valueOf(parcel.readInt());
        }
        this.A06 = ImmutableList.copyOf(numArr);
        this.A0A = parcel.readInt() != 1 ? false : true;
        if (parcel.readInt() == 0) {
            this.A02 = null;
        } else {
            this.A02 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A03 = null;
        } else {
            this.A03 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        int readInt2 = parcel.readInt();
        ThreadThemeReactionAssetInfo[] threadThemeReactionAssetInfoArr = new ThreadThemeReactionAssetInfo[readInt2];
        for (int i2 = 0; i2 < readInt2; i2++) {
            threadThemeReactionAssetInfoArr[i2] = (ThreadThemeReactionAssetInfo) parcel.readParcelable(ThreadThemeReactionAssetInfo.class.getClassLoader());
        }
        this.A07 = ImmutableList.copyOf(threadThemeReactionAssetInfoArr);
        if (parcel.readInt() == 0) {
            this.A04 = null;
        } else {
            this.A04 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        this.A01 = parcel.readLong();
        if (parcel.readInt() == 0) {
            this.A05 = null;
        } else {
            this.A05 = GraphQLMessengerThreadViewMode.values()[parcel.readInt()];
        }
        HashSet hashSet = new HashSet();
        int readInt3 = parcel.readInt();
        for (int i3 = 0; i3 < readInt3; i3++) {
            hashSet.add(parcel.readString());
        }
        this.A09 = Collections.unmodifiableSet(hashSet);
    }
}
