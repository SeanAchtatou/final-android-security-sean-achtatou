package com.facebook.messaging.customthreads.model;

import X.C22298Ase;
import X.C28931fb;
import X.C37541vq;
import X.C37551vr;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public final class ThreadThemeReactionAssetInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C37551vr();
    public final Uri A00;
    public final Uri A01;
    public final Uri A02;
    public final String A03;
    public final String A04;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ThreadThemeReactionAssetInfo) {
                ThreadThemeReactionAssetInfo threadThemeReactionAssetInfo = (ThreadThemeReactionAssetInfo) obj;
                if (!C28931fb.A07(this.A03, threadThemeReactionAssetInfo.A03) || !C28931fb.A07(this.A00, threadThemeReactionAssetInfo.A00) || !C28931fb.A07(this.A01, threadThemeReactionAssetInfo.A01) || !C28931fb.A07(this.A04, threadThemeReactionAssetInfo.A04) || !C28931fb.A07(this.A02, threadThemeReactionAssetInfo.A02)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(1, this.A03), this.A00), this.A01), this.A04), this.A02);
    }

    public String toString() {
        return "ThreadThemeReactionAssetInfo{id=" + this.A03 + ", " + "keyframeAssetUri=" + this.A00 + ", " + "largeStaticAssetUri=" + this.A01 + ", " + "reactionEmoji=" + this.A04 + ", " + "smallStaticAssetUri=" + this.A02 + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A03);
        if (this.A00 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A00, i);
        }
        if (this.A01 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A01, i);
        }
        parcel.writeString(this.A04);
        if (this.A02 == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcel.writeParcelable(this.A02, i);
    }

    public ThreadThemeReactionAssetInfo(C37541vq r3) {
        String str = r3.A03;
        C28931fb.A06(str, "id");
        this.A03 = str;
        this.A00 = r3.A00;
        this.A01 = r3.A01;
        String str2 = r3.A04;
        C28931fb.A06(str2, C22298Ase.$const$string(40));
        this.A04 = str2;
        this.A02 = r3.A02;
    }

    public ThreadThemeReactionAssetInfo(Parcel parcel) {
        this.A03 = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A00 = null;
        } else {
            this.A00 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        this.A04 = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A02 = null;
        } else {
            this.A02 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
    }
}
