package com.facebook.messaging.browser.model;

import X.C30041hO;
import X.C30051hP;
import X.C30061hQ;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import java.util.Arrays;

public final class MessengerWebViewParams implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C30061hQ();
    public final double A00;
    public final C30051hP A01;
    public final String A02;
    public final String A03;
    public final boolean A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            MessengerWebViewParams messengerWebViewParams = (MessengerWebViewParams) obj;
            if (!Objects.equal(Double.valueOf(this.A00), Double.valueOf(messengerWebViewParams.A00)) || !Objects.equal(this.A03, messengerWebViewParams.A03) || !Objects.equal(this.A02, messengerWebViewParams.A02) || !Objects.equal(Boolean.valueOf(this.A05), Boolean.valueOf(messengerWebViewParams.A05)) || !Objects.equal(Boolean.valueOf(this.A04), Boolean.valueOf(messengerWebViewParams.A05)) || !Objects.equal(Boolean.valueOf(this.A07), Boolean.valueOf(messengerWebViewParams.A07)) || !Objects.equal(Boolean.valueOf(this.A08), Boolean.valueOf(messengerWebViewParams.A08)) || !Objects.equal(Boolean.valueOf(this.A09), Boolean.valueOf(messengerWebViewParams.A09)) || !Objects.equal(this.A01, messengerWebViewParams.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{Double.valueOf(this.A00), this.A03, this.A02, Boolean.valueOf(this.A05), Boolean.valueOf(this.A04), Boolean.valueOf(this.A07), Boolean.valueOf(this.A08), Boolean.valueOf(this.A09), this.A01});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.A00);
        parcel.writeString(this.A03);
        parcel.writeString(this.A02);
        parcel.writeInt(this.A05 ? 1 : 0);
        parcel.writeInt(this.A04 ? 1 : 0);
        parcel.writeInt(this.A08 ? 1 : 0);
        parcel.writeInt(this.A07 ? 1 : 0);
        parcel.writeInt(this.A09 ? 1 : 0);
        parcel.writeString(this.A01.name());
        parcel.writeInt(this.A06 ? 1 : 0);
    }

    public MessengerWebViewParams(C30041hO r8) {
        double d = r8.A00;
        this.A00 = (d < 0.25d || d > 1.0d) ? 1.0d : d;
        this.A03 = r8.A03;
        this.A02 = r8.A02;
        this.A05 = r8.A05;
        this.A04 = r8.A04;
        this.A08 = r8.A07;
        this.A07 = r8.A06;
        this.A09 = r8.A08;
        C30051hP r0 = r8.A01;
        this.A01 = r0 == null ? C30051hP.DEFAULT : r0;
        this.A06 = false;
    }

    public MessengerWebViewParams(Parcel parcel) {
        this.A00 = parcel.readDouble();
        this.A03 = parcel.readString();
        this.A02 = parcel.readString();
        boolean z = true;
        this.A05 = parcel.readInt() != 0;
        this.A04 = parcel.readInt() != 0;
        this.A08 = parcel.readInt() != 0;
        this.A07 = parcel.readInt() != 0;
        this.A09 = parcel.readInt() != 0;
        this.A01 = C30051hP.A00(parcel.readString());
        this.A06 = parcel.readInt() == 0 ? false : z;
    }
}
