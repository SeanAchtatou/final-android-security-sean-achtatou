package com.facebook.messaging.montage.omnistore.cache;

import X.AnonymousClass04a;
import X.AnonymousClass07B;
import X.AnonymousClass0UN;
import X.AnonymousClass0WY;
import X.AnonymousClass1XY;
import X.C04310Tq;
import X.C05540Zi;
import X.C21321Gd;
import com.facebook.auth.userscope.UserScoped;
import java.util.Map;

@UserScoped
public final class MontageCache {
    private static C05540Zi A06;
    public AnonymousClass0UN A00;
    public Integer A01 = AnonymousClass07B.A00;
    public final Map A02 = new AnonymousClass04a();
    public final Map A03 = new AnonymousClass04a();
    public final Map A04 = new AnonymousClass04a();
    public final C04310Tq A05;

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008f, code lost:
        if (r9 != null) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0094, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0C(java.lang.String r11, com.facebook.messaging.model.messages.Message r12) {
        /*
            r10 = this;
            r2 = 0
            if (r12 == 0) goto L_0x0014
            int r1 = X.AnonymousClass1Y3.A8n
            X.0UN r0 = r10.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1TU r0 = (X.AnonymousClass1TU) r0
            boolean r0 = r0.A0K(r12)
            if (r0 == 0) goto L_0x0014
        L_0x0013:
            return
        L_0x0014:
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BP2
            X.0UN r0 = r10.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r9 = r0.A02()
            if (r12 != 0) goto L_0x002b
            java.util.Map r0 = r10.A02     // Catch:{ all -> 0x008c }
            r0.remove(r11)     // Catch:{ all -> 0x008c }
            goto L_0x0086
        L_0x002b:
            java.util.Map r0 = r10.A02     // Catch:{ all -> 0x008c }
            X.AnonymousClass0qX.A00(r11)     // Catch:{ all -> 0x008c }
            r0.put(r11, r12)     // Catch:{ all -> 0x008c }
            r2 = 6
            int r1 = X.AnonymousClass1Y3.AcE     // Catch:{ all -> 0x008c }
            X.0UN r0 = r10.A00     // Catch:{ all -> 0x008c }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x008c }
            X.2VL r6 = (X.AnonymousClass2VL) r6     // Catch:{ all -> 0x008c }
            long r2 = r12.A03     // Catch:{ all -> 0x008c }
            long r0 = X.AnonymousClass1TU.A03(r12)     // Catch:{ all -> 0x008c }
            long r2 = r2 + r0
            java.lang.Long r0 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x008c }
            long r7 = r6.A00     // Catch:{ all -> 0x008c }
            long r3 = r0.longValue()     // Catch:{ all -> 0x008c }
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 > 0) goto L_0x005d
            X.06B r0 = r6.A03     // Catch:{ all -> 0x008c }
            long r1 = r0.now()     // Catch:{ all -> 0x008c }
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0086
        L_0x005d:
            X.06B r0 = r6.A03     // Catch:{ all -> 0x008c }
            long r1 = r0.now()     // Catch:{ all -> 0x008c }
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0086
            r6.A00 = r3     // Catch:{ all -> 0x008c }
            X.0ia r1 = r6.A02     // Catch:{ all -> 0x008c }
            java.lang.Runnable r0 = r6.A08     // Catch:{ all -> 0x008c }
            r1.A03(r0)     // Catch:{ all -> 0x008c }
            long r3 = r6.A00     // Catch:{ all -> 0x008c }
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0086
            X.0ia r5 = r6.A02     // Catch:{ all -> 0x008c }
            java.lang.Runnable r2 = r6.A08     // Catch:{ all -> 0x008c }
            X.06B r0 = r6.A03     // Catch:{ all -> 0x008c }
            long r0 = r0.now()     // Catch:{ all -> 0x008c }
            long r3 = r3 - r0
            r5.A04(r2, r3)     // Catch:{ all -> 0x008c }
        L_0x0086:
            if (r9 == 0) goto L_0x0013
            r9.close()
            return
        L_0x008c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x008e }
        L_0x008e:
            r0 = move-exception
            if (r9 == 0) goto L_0x0094
            r9.close()     // Catch:{ all -> 0x0094 }
        L_0x0094:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A0C(java.lang.String, com.facebook.messaging.model.messages.Message):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0053, code lost:
        if (r4 != null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0058, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.messaging.montage.model.BasicMontageThreadInfo A00(com.facebook.messaging.montage.omnistore.cache.MontageCache r6, com.facebook.user.model.UserKey r7) {
        /*
            int r2 = X.AnonymousClass1Y3.BHe
            X.0UN r1 = r6.A00
            r0 = 7
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.128 r1 = (X.AnonymousClass128) r1
            java.lang.String r0 = r7.id
            boolean r0 = r1.A04(r0)
            r5 = 0
            if (r0 != 0) goto L_0x0059
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BP2
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r4 = r0.A01()
            java.util.Map r0 = r6.A04     // Catch:{ all -> 0x0050 }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ all -> 0x0050 }
            X.1v1 r0 = (X.AnonymousClass1v1) r0     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x004a
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r3 = r0.A02     // Catch:{ all -> 0x0050 }
            if (r3 == 0) goto L_0x004a
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x0050 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x0050 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0050 }
            X.1TU r1 = (X.AnonymousClass1TU) r1     // Catch:{ all -> 0x0050 }
            com.facebook.messaging.model.messages.Message r0 = r3.A01     // Catch:{ all -> 0x0050 }
            boolean r0 = r1.A0K(r0)     // Catch:{ all -> 0x0050 }
            if (r0 != 0) goto L_0x004a
            if (r4 == 0) goto L_0x0049
            r4.close()
        L_0x0049:
            return r3
        L_0x004a:
            if (r4 == 0) goto L_0x0059
            r4.close()
            return r5
        L_0x0050:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
            if (r4 == 0) goto L_0x0058
            r4.close()     // Catch:{ all -> 0x0058 }
        L_0x0058:
            throw r0
        L_0x0059:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A00(com.facebook.messaging.montage.omnistore.cache.MontageCache, com.facebook.user.model.UserKey):com.facebook.messaging.montage.model.BasicMontageThreadInfo");
    }

    public static final MontageCache A01(AnonymousClass1XY r4) {
        MontageCache montageCache;
        synchronized (MontageCache.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r4)) {
                    A06.A00 = new MontageCache((AnonymousClass1XY) A06.A01());
                }
                C05540Zi r1 = A06;
                montageCache = (MontageCache) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return montageCache;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0050, code lost:
        if (r6 != null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0055, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Collection A02(com.facebook.messaging.montage.omnistore.cache.MontageCache r7) {
        /*
            int r2 = X.AnonymousClass1Y3.BP2
            X.0UN r1 = r7.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r6 = r0.A01()
            com.google.common.collect.ImmutableList$Builder r5 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x004d }
            r5.<init>()     // Catch:{ all -> 0x004d }
            java.util.Map r0 = r7.A03     // Catch:{ all -> 0x004d }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x004d }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x004d }
        L_0x001e:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x0043
            java.lang.Object r3 = r4.next()     // Catch:{ all -> 0x004d }
            X.1v1 r3 = (X.AnonymousClass1v1) r3     // Catch:{ all -> 0x004d }
            r2 = 7
            int r1 = X.AnonymousClass1Y3.BHe     // Catch:{ all -> 0x004d }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x004d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x004d }
            X.128 r1 = (X.AnonymousClass128) r1     // Catch:{ all -> 0x004d }
            com.facebook.user.model.UserKey r0 = r3.A05     // Catch:{ all -> 0x004d }
            java.lang.String r0 = r0.id     // Catch:{ all -> 0x004d }
            boolean r0 = r1.A04(r0)     // Catch:{ all -> 0x004d }
            if (r0 != 0) goto L_0x001e
            r5.add(r3)     // Catch:{ all -> 0x004d }
            goto L_0x001e
        L_0x0043:
            com.google.common.collect.ImmutableList r0 = r5.build()     // Catch:{ all -> 0x004d }
            if (r6 == 0) goto L_0x004c
            r6.close()
        L_0x004c:
            return r0
        L_0x004d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004f }
        L_0x004f:
            r0 = move-exception
            if (r6 == 0) goto L_0x0055
            r6.close()     // Catch:{ all -> 0x0055 }
        L_0x0055:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A02(com.facebook.messaging.montage.omnistore.cache.MontageCache):java.util.Collection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
        if (r1 != null) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003d, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.Message A03(java.lang.String r6) {
        /*
            r5 = this;
            int r2 = X.AnonymousClass1Y3.BP2
            X.0UN r1 = r5.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r1 = r0.A01()
            java.util.Map r0 = r5.A02     // Catch:{ all -> 0x0035 }
            java.lang.Object r4 = r0.get(r6)     // Catch:{ all -> 0x0035 }
            com.facebook.messaging.model.messages.Message r4 = (com.facebook.messaging.model.messages.Message) r4     // Catch:{ all -> 0x0035 }
            r3 = 0
            if (r4 != 0) goto L_0x001b
            r4 = r3
        L_0x001b:
            if (r1 == 0) goto L_0x0020
            r1.close()
        L_0x0020:
            if (r4 == 0) goto L_0x0034
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A8n
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1TU r0 = (X.AnonymousClass1TU) r0
            boolean r0 = r0.A0K(r4)
            if (r0 != 0) goto L_0x0034
            return r4
        L_0x0034:
            return r3
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ all -> 0x003d }
        L_0x003d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A03(java.lang.String):com.facebook.messaging.model.messages.Message");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        if (r1 != null) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.montage.model.BasicMontageThreadInfo A04(java.lang.Long r4) {
        /*
            r3 = this;
            int r2 = X.AnonymousClass1Y3.BHe
            X.0UN r1 = r3.A00
            r0 = 7
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.128 r1 = (X.AnonymousClass128) r1
            java.lang.String r0 = r4.toString()
            boolean r0 = r1.A04(r0)
            if (r0 == 0) goto L_0x0017
            r0 = 0
            return r0
        L_0x0017:
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BP2
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r1 = r0.A01()
            com.facebook.user.model.UserKey r0 = com.facebook.user.model.UserKey.A00(r4)     // Catch:{ all -> 0x0034 }
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = A00(r3, r0)     // Catch:{ all -> 0x0034 }
            if (r1 == 0) goto L_0x0033
            r1.close()
        L_0x0033:
            return r0
        L_0x0034:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ all -> 0x003c }
        L_0x003c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A04(java.lang.Long):com.facebook.messaging.montage.model.BasicMontageThreadInfo");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0055, code lost:
        if (r5 != null) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005a, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.montage.model.MontageThreadInfo A05(com.facebook.user.model.UserKey r8) {
        /*
            r7 = this;
            int r2 = X.AnonymousClass1Y3.BHe
            X.0UN r1 = r7.A00
            r0 = 7
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.128 r1 = (X.AnonymousClass128) r1
            java.lang.String r0 = r8.id
            boolean r0 = r1.A04(r0)
            r6 = 0
            if (r0 != 0) goto L_0x005b
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BP2
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r5 = r0.A01()
            java.util.Map r0 = r7.A04     // Catch:{ all -> 0x0052 }
            java.lang.Object r4 = r0.get(r8)     // Catch:{ all -> 0x0052 }
            X.1v1 r4 = (X.AnonymousClass1v1) r4     // Catch:{ all -> 0x0052 }
            if (r4 == 0) goto L_0x004c
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r3 = r4.A02     // Catch:{ all -> 0x0052 }
            if (r3 == 0) goto L_0x004c
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x0052 }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x0052 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0052 }
            X.1TU r1 = (X.AnonymousClass1TU) r1     // Catch:{ all -> 0x0052 }
            com.facebook.messaging.model.messages.Message r0 = r3.A01     // Catch:{ all -> 0x0052 }
            boolean r0 = r1.A0K(r0)     // Catch:{ all -> 0x0052 }
            if (r0 != 0) goto L_0x004c
            com.facebook.messaging.montage.model.MontageThreadInfo r0 = r4.A04     // Catch:{ all -> 0x0052 }
            if (r5 == 0) goto L_0x004b
            r5.close()
        L_0x004b:
            return r0
        L_0x004c:
            if (r5 == 0) goto L_0x005b
            r5.close()
            return r6
        L_0x0052:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0054 }
        L_0x0054:
            r0 = move-exception
            if (r5 == 0) goto L_0x005a
            r5.close()     // Catch:{ all -> 0x005a }
        L_0x005a:
            throw r0
        L_0x005b:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A05(com.facebook.user.model.UserKey):com.facebook.messaging.montage.model.MontageThreadInfo");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0020, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0025, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1v1 A06(com.facebook.messaging.model.threadkey.ThreadKey r4) {
        /*
            r3 = this;
            int r2 = X.AnonymousClass1Y3.BP2
            X.0UN r1 = r3.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r1 = r0.A01()
            java.util.Map r0 = r3.A03     // Catch:{ all -> 0x001d }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x001d }
            X.1v1 r0 = (X.AnonymousClass1v1) r0     // Catch:{ all -> 0x001d }
            if (r1 == 0) goto L_0x001c
            r1.close()
        L_0x001c:
            return r0
        L_0x001d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001f }
        L_0x001f:
            r0 = move-exception
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ all -> 0x0025 }
        L_0x0025:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A06(com.facebook.messaging.model.threadkey.ThreadKey):X.1v1");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        if (r3 != null) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A07() {
        /*
            r4 = this;
            int r2 = X.AnonymousClass1Y3.BP2
            X.0UN r1 = r4.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r3 = r0.A01()
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x0039 }
            java.util.Collection r0 = A02(r4)     // Catch:{ all -> 0x0039 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0039 }
        L_0x001b:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x002f
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0039 }
            X.1v1 r0 = (X.AnonymousClass1v1) r0     // Catch:{ all -> 0x0039 }
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = r0.A02     // Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x001b
            r2.add(r0)     // Catch:{ all -> 0x0039 }
            goto L_0x001b
        L_0x002f:
            com.google.common.collect.ImmutableList r0 = r2.build()     // Catch:{ all -> 0x0039 }
            if (r3 == 0) goto L_0x0038
            r3.close()
        L_0x0038:
            return r0
        L_0x0039:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003b }
        L_0x003b:
            r0 = move-exception
            if (r3 == 0) goto L_0x0041
            r3.close()     // Catch:{ all -> 0x0041 }
        L_0x0041:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A07():com.google.common.collect.ImmutableList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004a, code lost:
        if (r4 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004f, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A08(java.util.List r6) {
        /*
            r5 = this;
            int r2 = X.AnonymousClass1Y3.BP2
            X.0UN r1 = r5.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r4 = r0.A01()
            com.google.common.collect.ImmutableList$Builder r3 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x0047 }
            java.util.Iterator r2 = r6.iterator()     // Catch:{ all -> 0x0047 }
        L_0x0017:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x003d
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0047 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0047 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x0047 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = com.facebook.messaging.model.threadkey.ThreadKey.A01(r0)     // Catch:{ all -> 0x0047 }
            java.util.Map r0 = r5.A03     // Catch:{ all -> 0x0047 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0047 }
            X.1v1 r0 = (X.AnonymousClass1v1) r0     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0017
            com.facebook.messaging.montage.model.MontageThreadInfo r0 = r0.A04     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0017
            r3.add(r0)     // Catch:{ all -> 0x0047 }
            goto L_0x0017
        L_0x003d:
            com.google.common.collect.ImmutableList r0 = r3.build()     // Catch:{ all -> 0x0047 }
            if (r4 == 0) goto L_0x0046
            r4.close()
        L_0x0046:
            return r0
        L_0x0047:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            if (r4 == 0) goto L_0x004f
            r4.close()     // Catch:{ all -> 0x004f }
        L_0x004f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A08(java.util.List):com.google.common.collect.ImmutableList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0046, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0047, code lost:
        if (r4 != null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004c, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(com.facebook.messaging.model.threadkey.ThreadKey r6) {
        /*
            r5 = this;
            int r2 = X.AnonymousClass1Y3.BP2
            X.0UN r1 = r5.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r4 = r0.A02()
            java.util.Map r0 = r5.A03     // Catch:{ all -> 0x0044 }
            java.lang.Object r3 = r0.get(r6)     // Catch:{ all -> 0x0044 }
            X.1v1 r3 = (X.AnonymousClass1v1) r3     // Catch:{ all -> 0x0044 }
            java.util.Map r0 = r5.A03     // Catch:{ all -> 0x0044 }
            r0.remove(r6)     // Catch:{ all -> 0x0044 }
            if (r3 == 0) goto L_0x003e
            java.util.Map r1 = r5.A04     // Catch:{ all -> 0x0044 }
            com.facebook.user.model.UserKey r0 = r3.A05     // Catch:{ all -> 0x0044 }
            r1.remove(r0)     // Catch:{ all -> 0x0044 }
            r2 = 3
            int r1 = X.AnonymousClass1Y3.Avl     // Catch:{ all -> 0x0044 }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x0044 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0044 }
            X.2VO r1 = (X.AnonymousClass2VO) r1     // Catch:{ all -> 0x0044 }
            com.facebook.user.model.UserKey r0 = r3.A05     // Catch:{ all -> 0x0044 }
            X.0Xh r3 = r1.A00     // Catch:{ all -> 0x0044 }
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r0)     // Catch:{ all -> 0x0044 }
            java.util.concurrent.ExecutorService r1 = r1.A01     // Catch:{ all -> 0x0044 }
            r0 = 0
            r3.A04(r2, r0, r1)     // Catch:{ all -> 0x0044 }
        L_0x003e:
            if (r4 == 0) goto L_0x0043
            r4.close()
        L_0x0043:
            return
        L_0x0044:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0046 }
        L_0x0046:
            r0 = move-exception
            if (r4 == 0) goto L_0x004c
            r4.close()     // Catch:{ all -> 0x004c }
        L_0x004c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A09(com.facebook.messaging.model.threadkey.ThreadKey):void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:19|(2:21|22)|23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0074, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0075, code lost:
        if (r4 != null) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x007a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(X.AnonymousClass1v1 r7) {
        /*
            r6 = this;
            int r1 = X.AnonymousClass1Y3.BP2
            X.0UN r0 = r6.A00
            r3 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r5 = r0.A02()
            com.facebook.messaging.montage.model.MontageThreadInfo r0 = r7.A04     // Catch:{ all -> 0x007b }
            com.google.common.collect.ImmutableList r2 = r0.A02     // Catch:{ all -> 0x007b }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x007b }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x007b }
            X.1HC r0 = (X.AnonymousClass1HC) r0     // Catch:{ all -> 0x007b }
            X.1HE r4 = r0.A02()     // Catch:{ all -> 0x007b }
            java.util.Iterator r3 = r2.iterator()     // Catch:{ all -> 0x0072 }
        L_0x0023:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x0040
            java.lang.Object r2 = r3.next()     // Catch:{ all -> 0x0072 }
            com.facebook.messaging.model.messages.Message r2 = (com.facebook.messaging.model.messages.Message) r2     // Catch:{ all -> 0x0072 }
            java.lang.String r1 = r2.A0q     // Catch:{ all -> 0x0072 }
            X.AnonymousClass0qX.A00(r1)     // Catch:{ all -> 0x0072 }
            java.util.Map r0 = r6.A02     // Catch:{ all -> 0x0072 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x0072 }
            if (r0 != 0) goto L_0x0023
            r6.A0C(r1, r2)     // Catch:{ all -> 0x0072 }
            goto L_0x0023
        L_0x0040:
            if (r4 == 0) goto L_0x0045
            r4.close()     // Catch:{ all -> 0x007b }
        L_0x0045:
            java.util.Map r1 = r6.A03     // Catch:{ all -> 0x007b }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A01     // Catch:{ all -> 0x007b }
            r1.put(r0, r7)     // Catch:{ all -> 0x007b }
            java.util.Map r1 = r6.A04     // Catch:{ all -> 0x007b }
            com.facebook.user.model.UserKey r0 = r7.A05     // Catch:{ all -> 0x007b }
            r1.put(r0, r7)     // Catch:{ all -> 0x007b }
            r2 = 3
            int r1 = X.AnonymousClass1Y3.Avl     // Catch:{ all -> 0x007b }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x007b }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x007b }
            X.2VO r1 = (X.AnonymousClass2VO) r1     // Catch:{ all -> 0x007b }
            com.facebook.user.model.UserKey r0 = r7.A05     // Catch:{ all -> 0x007b }
            X.0Xh r3 = r1.A00     // Catch:{ all -> 0x007b }
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r0)     // Catch:{ all -> 0x007b }
            java.util.concurrent.ExecutorService r1 = r1.A01     // Catch:{ all -> 0x007b }
            r0 = 0
            r3.A04(r2, r0, r1)     // Catch:{ all -> 0x007b }
            if (r5 == 0) goto L_0x0071
            r5.close()
        L_0x0071:
            return
        L_0x0072:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x0074:
            r0 = move-exception
            if (r4 == 0) goto L_0x007a
            r4.close()     // Catch:{ all -> 0x007a }
        L_0x007a:
            throw r0     // Catch:{ all -> 0x007b }
        L_0x007b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007d }
        L_0x007d:
            r0 = move-exception
            if (r5 == 0) goto L_0x0083
            r5.close()     // Catch:{ all -> 0x0083 }
        L_0x0083:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A0A(X.1v1):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x012b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x012c, code lost:
        if (r12 != null) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0131, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0B(com.google.common.collect.ImmutableList r20) {
        /*
            r19 = this;
            int r2 = X.AnonymousClass1Y3.BP2
            r4 = r19
            X.0UN r1 = r4.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1HC r0 = (X.AnonymousClass1HC) r0
            X.1HE r12 = r0.A02()
            X.1Gd r5 = new X.1Gd     // Catch:{ all -> 0x0129 }
            r5.<init>()     // Catch:{ all -> 0x0129 }
            X.1Gd r3 = new X.1Gd     // Catch:{ all -> 0x0129 }
            r3.<init>()     // Catch:{ all -> 0x0129 }
            X.1Xv r2 = r20.iterator()     // Catch:{ all -> 0x0129 }
        L_0x001f:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0129 }
            if (r0 == 0) goto L_0x0036
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.messages.Message r1 = (com.facebook.messaging.model.messages.Message) r1     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0U     // Catch:{ all -> 0x0129 }
            r5.add(r0)     // Catch:{ all -> 0x0129 }
            java.lang.String r0 = r1.A0q     // Catch:{ all -> 0x0129 }
            r3.add(r0)     // Catch:{ all -> 0x0129 }
            goto L_0x001f
        L_0x0036:
            java.util.Iterator r11 = r5.iterator()     // Catch:{ all -> 0x0129 }
        L_0x003a:
            boolean r0 = r11.hasNext()     // Catch:{ all -> 0x0129 }
            if (r0 == 0) goto L_0x0123
            java.lang.Object r1 = r11.next()     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = (com.facebook.messaging.model.threadkey.ThreadKey) r1     // Catch:{ all -> 0x0129 }
            java.util.Map r0 = r4.A03     // Catch:{ all -> 0x0129 }
            java.lang.Object r1 = r0.get(r1)     // Catch:{ all -> 0x0129 }
            X.1v1 r1 = (X.AnonymousClass1v1) r1     // Catch:{ all -> 0x0129 }
            if (r1 == 0) goto L_0x0123
            com.facebook.messaging.montage.model.MontageThreadInfo r9 = r1.A04     // Catch:{ all -> 0x0129 }
            com.google.common.collect.ImmutableList$Builder r8 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x0129 }
            r8.<init>()     // Catch:{ all -> 0x0129 }
            com.google.common.collect.ImmutableList r0 = r9.A02     // Catch:{ all -> 0x0129 }
            X.1Xv r10 = r0.iterator()     // Catch:{ all -> 0x0129 }
        L_0x005d:
            boolean r0 = r10.hasNext()     // Catch:{ all -> 0x0129 }
            r7 = 0
            if (r0 == 0) goto L_0x00a1
            java.lang.Object r2 = r10.next()     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.messages.Message r2 = (com.facebook.messaging.model.messages.Message) r2     // Catch:{ all -> 0x0129 }
            java.lang.String r0 = r2.A0q     // Catch:{ all -> 0x0129 }
            boolean r0 = r3.contains(r0)     // Catch:{ all -> 0x0129 }
            if (r0 == 0) goto L_0x009d
            X.1TG r6 = com.facebook.messaging.model.messages.Message.A00()     // Catch:{ all -> 0x0129 }
            r6.A03(r2)     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.montagemetadata.MontageMetadata r0 = r2.A0O     // Catch:{ all -> 0x0129 }
            X.AnonymousClass0qX.A00(r0)     // Catch:{ all -> 0x0129 }
            X.1nd r5 = new X.1nd     // Catch:{ all -> 0x0129 }
            r5.<init>(r0)     // Catch:{ all -> 0x0129 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x0129 }
            r5.A0I = r2     // Catch:{ all -> 0x0129 }
            java.lang.String r0 = "isUnread"
            X.C28931fb.A06(r2, r0)     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.montagemetadata.MontageMetadata r0 = new com.facebook.messaging.model.montagemetadata.MontageMetadata     // Catch:{ all -> 0x0129 }
            r0.<init>(r5)     // Catch:{ all -> 0x0129 }
            r6.A0N = r0     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.messages.Message r0 = r6.A00()     // Catch:{ all -> 0x0129 }
            r8.add(r0)     // Catch:{ all -> 0x0129 }
            goto L_0x005d
        L_0x009d:
            r8.add(r2)     // Catch:{ all -> 0x0129 }
            goto L_0x005d
        L_0x00a1:
            X.2V7 r5 = new X.2V7     // Catch:{ all -> 0x0129 }
            r5.<init>(r9)     // Catch:{ all -> 0x0129 }
            com.google.common.collect.ImmutableList r2 = r8.build()     // Catch:{ all -> 0x0129 }
            r5.A02 = r2     // Catch:{ all -> 0x0129 }
            java.lang.String r0 = "messages"
            X.C28931fb.A06(r2, r0)     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.montage.model.MontageThreadInfo r14 = new com.facebook.messaging.montage.model.MontageThreadInfo     // Catch:{ all -> 0x0129 }
            r14.<init>(r5)     // Catch:{ all -> 0x0129 }
            int r2 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x0129 }
            X.0UN r0 = r4.A00     // Catch:{ all -> 0x0129 }
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r7, r2, r0)     // Catch:{ all -> 0x0129 }
            X.1TU r13 = (X.AnonymousClass1TU) r13     // Catch:{ all -> 0x0129 }
            java.lang.Boolean r0 = r1.A06     // Catch:{ all -> 0x0129 }
            boolean r15 = r0.booleanValue()     // Catch:{ all -> 0x0129 }
            r16 = 0
            r17 = 0
            int r5 = r14.A00     // Catch:{ all -> 0x0129 }
            r18 = r5
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = r13.A0G(r14, r15, r16, r17, r18)     // Catch:{ all -> 0x0129 }
            X.2V8 r6 = new X.2V8     // Catch:{ all -> 0x0129 }
            r6.<init>()     // Catch:{ all -> 0x0129 }
            r6.A01(r1)     // Catch:{ all -> 0x0129 }
            r6.A04 = r14     // Catch:{ all -> 0x0129 }
            r6.A02 = r0     // Catch:{ all -> 0x0129 }
            r8 = 2
            int r2 = X.AnonymousClass1Y3.BRG     // Catch:{ all -> 0x0129 }
            X.0UN r0 = r4.A00     // Catch:{ all -> 0x0129 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r2, r0)     // Catch:{ all -> 0x0129 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ all -> 0x0129 }
            boolean r0 = r0.A0J()     // Catch:{ all -> 0x0129 }
            if (r0 == 0) goto L_0x011a
            int r0 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x0129 }
            X.0UN r2 = r4.A00     // Catch:{ all -> 0x0129 }
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r7, r0, r2)     // Catch:{ all -> 0x0129 }
            X.1TU r13 = (X.AnonymousClass1TU) r13     // Catch:{ all -> 0x0129 }
            java.lang.Boolean r0 = r1.A06     // Catch:{ all -> 0x0129 }
            boolean r15 = r0.booleanValue()     // Catch:{ all -> 0x0129 }
            if (r15 == 0) goto L_0x0112
            r1 = 4
            int r0 = X.AnonymousClass1Y3.ApM     // Catch:{ all -> 0x0129 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r0, r2)     // Catch:{ all -> 0x0129 }
            X.1nU r2 = (X.C33381nU) r2     // Catch:{ all -> 0x0129 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r14.A01     // Catch:{ all -> 0x0129 }
            com.google.common.collect.ImmutableList r0 = r14.A02     // Catch:{ all -> 0x0129 }
            com.google.common.collect.ImmutableList r16 = r2.A02(r1, r0)     // Catch:{ all -> 0x0129 }
        L_0x0112:
            r17 = 1
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = r13.A0G(r14, r15, r16, r17, r18)     // Catch:{ all -> 0x0129 }
            r6.A03 = r0     // Catch:{ all -> 0x0129 }
        L_0x011a:
            X.1v1 r0 = r6.A00()     // Catch:{ all -> 0x0129 }
            r4.A0A(r0)     // Catch:{ all -> 0x0129 }
            goto L_0x003a
        L_0x0123:
            if (r12 == 0) goto L_0x0128
            r12.close()
        L_0x0128:
            return
        L_0x0129:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x012b }
        L_0x012b:
            r0 = move-exception
            if (r12 == 0) goto L_0x0131
            r12.close()     // Catch:{ all -> 0x0131 }
        L_0x0131:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.cache.MontageCache.A0B(com.google.common.collect.ImmutableList):void");
    }

    private MontageCache(AnonymousClass1XY r3) {
        new C21321Gd();
        this.A00 = new AnonymousClass0UN(8, r3);
        this.A05 = AnonymousClass0WY.A02(r3);
    }
}
