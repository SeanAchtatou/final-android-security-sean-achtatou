package com.facebook.messaging.montage.omnistore.storedprocedures;

import X.AnonymousClass067;
import X.AnonymousClass06B;
import X.AnonymousClass0UN;
import X.AnonymousClass0UX;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2WT;
import X.C04310Tq;
import X.C33451nb;
import com.facebook.acra.LogCatCollector;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
public final class MontageNonUserRefreshStoredProcedureComponent implements OmnistoreStoredProcedureComponent {
    public static final Charset A07 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    private static volatile MontageNonUserRefreshStoredProcedureComponent A08;
    public AnonymousClass0UN A00;
    public AnonymousClass2WT A01;
    public Long A02;
    public final AnonymousClass06B A03 = AnonymousClass067.A02();
    public final C33451nb A04;
    public final Executor A05;
    public final C04310Tq A06;

    public void onSenderInvalidated() {
        this.A01 = null;
    }

    public int provideStoredProcedureId() {
        return AnonymousClass1Y3.A3J;
    }

    public static final MontageNonUserRefreshStoredProcedureComponent A00(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (MontageNonUserRefreshStoredProcedureComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new MontageNonUserRefreshStoredProcedureComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public void onStoredProcedureResult(ByteBuffer byteBuffer) {
        new String(byteBuffer.array());
    }

    private MontageNonUserRefreshStoredProcedureComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A05 = AnonymousClass0UX.A0T(r3);
        this.A06 = AnonymousClass0XJ.A0I(r3);
        this.A04 = new C33451nb(r3);
    }

    public void onSenderAvailable(AnonymousClass2WT r1) {
        this.A01 = r1;
    }
}
