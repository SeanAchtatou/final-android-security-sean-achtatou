package com.facebook.messaging.montage.omnistore.storedprocedures;

import X.AnonymousClass07A;
import X.AnonymousClass09P;
import X.AnonymousClass0UN;
import X.AnonymousClass0V0;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2WT;
import X.AnonymousClass52Q;
import X.C010708t;
import X.C04310Tq;
import X.C05350Yp;
import X.C06850cB;
import X.C1053552b;
import X.C24971Xv;
import X.C33451nb;
import X.C98864nx;
import com.facebook.acra.LogCatCollector;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.SettableFuture;
import io.card.payment.BuildConfig;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;
import javax.inject.Singleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
public final class MontageReactionStoredProcedureComponent implements OmnistoreStoredProcedureComponent {
    public static final Charset A05 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    private static volatile MontageReactionStoredProcedureComponent A06;
    public AnonymousClass0UN A00;
    public AnonymousClass2WT A01;
    public SettableFuture A02 = null;
    public Long A03;
    public final C04310Tq A04;

    public void onSenderInvalidated() {
        this.A01 = null;
    }

    public int provideStoredProcedureId() {
        return 401;
    }

    public static final MontageReactionStoredProcedureComponent A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (MontageReactionStoredProcedureComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new MontageReactionStoredProcedureComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public void A01(String str, AnonymousClass0V0 r16, ImmutableMap immutableMap, String str2) {
        String str3;
        SettableFuture settableFuture;
        AnonymousClass0V0 r11 = r16;
        String str4 = str;
        String str5 = str2;
        ImmutableMap immutableMap2 = immutableMap;
        if (this.A01 == null) {
            synchronized (this) {
                if (this.A02 == null) {
                    this.A02 = SettableFuture.create();
                    AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, this.A00), new C1053552b(this), 508241642);
                }
                settableFuture = this.A02;
            }
            C05350Yp.A07(settableFuture, new C98864nx(this, str4, r11, immutableMap2, str5));
            return;
        }
        String str6 = BuildConfig.FLAVOR;
        try {
            JSONArray jSONArray = new JSONArray();
            for (Map.Entry entry : r11.AOp().entrySet()) {
                LinkedList linkedList = new LinkedList();
                for (Long intValue : (Collection) entry.getValue()) {
                    linkedList.add(Integer.valueOf(intValue.intValue()));
                }
                jSONArray.put(new JSONObject().put("reaction", entry.getKey()).put("offsets", new JSONArray((Collection) linkedList)));
            }
            JSONArray jSONArray2 = new JSONArray();
            C24971Xv it = immutableMap2.keySet().iterator();
            while (it.hasNext()) {
                String str7 = (String) it.next();
                if (immutableMap2.get(str7) != null) {
                    jSONArray2.put(new JSONObject().put("tag_key", str7).put("tag_value", immutableMap2.get(str7)));
                }
            }
            JSONObject put = new JSONObject().put("actor_id", Long.parseLong(((UserKey) this.A04.get()).id)).put("message_id", str).put("actions", jSONArray).put("client_tags", jSONArray2).put("client_mutation_id", UUID.randomUUID().toString()).put("offline_threading_id", str5);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("data", put);
            JSONObject jSONObject2 = new JSONObject();
            if (this.A03 == null) {
                this.A03 = ((C33451nb) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A6K, this.A00)).A01("FBMontageReactionMutation.params.json", "query_doc_id", "com.facebook.messaging.montage.omnistore.storedprocedures.MontageReactionStoredProcedureComponent");
            }
            Long l = this.A03;
            Preconditions.checkNotNull(l);
            str6 = StringFormatUtil.formatStrLocaleSafe(jSONObject2.put("doc_id", l).put("flat_buffer_idl", ((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANr, this.A00)).A03()).put("query_params", jSONObject).put("collection_label", ((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANr, this.A00)).getCollectionLabel()).toString());
        } catch (JSONException e) {
            C010708t.A0N("com.facebook.messaging.montage.omnistore.storedprocedures.MontageReactionStoredProcedureComponent", " error while building request", e);
            ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A00)).softReport("com.facebook.messaging.montage.omnistore.storedprocedures.MontageReactionStoredProcedureComponent", e.getMessage(), e);
        } catch (NumberFormatException e2) {
            C010708t.A0N("com.facebook.messaging.montage.omnistore.storedprocedures.MontageReactionStoredProcedureComponent", " error while building request", e2);
            ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A00)).softReport("com.facebook.messaging.montage.omnistore.storedprocedures.MontageReactionStoredProcedureComponent", e2.getMessage(), e2);
        }
        CollectionName collectionName = ((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANr, this.A00)).A01;
        if (collectionName != null) {
            str3 = collectionName.getQueueIdentifier().toString();
        } else {
            str3 = null;
        }
        if (!C06850cB.A0B(str6) && str3 != null) {
            AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, this.A00), new AnonymousClass52Q(this, str6, str3 + UUID.randomUUID(), str3), 661535169);
        }
    }

    public void onStoredProcedureResult(ByteBuffer byteBuffer) {
        new String(byteBuffer.array());
    }

    private MontageReactionStoredProcedureComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
        this.A04 = AnonymousClass0XJ.A0I(r3);
    }

    public void onSenderAvailable(AnonymousClass2WT r1) {
        this.A01 = r1;
    }
}
