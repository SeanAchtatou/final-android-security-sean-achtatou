package com.facebook.messaging.montage.inboxunit;

import X.AnonymousClass08S;
import X.AnonymousClass1HO;
import X.C27161ck;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.messaging.montage.model.MontageInboxNuxItem;
import com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem;
import com.facebook.user.model.LastActive;
import com.google.common.base.Preconditions;

public final class InboxMontageItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1HO();
    public final int A00;
    public final RankingLoggingItem A01;
    public final BasicMontageThreadInfo A02;
    public final MontageInboxNuxItem A03;
    public final UnifiedPresenceViewLoggerItem A04;
    public final LastActive A05;
    public final boolean A06;

    public static String A01(InboxMontageItem inboxMontageItem) {
        int i = inboxMontageItem.A00;
        if (i == 1) {
            return "myday";
        }
        if (i == 2) {
            Preconditions.checkNotNull(inboxMontageItem.A02);
            if (inboxMontageItem.A02.A07) {
                return "unseen";
            }
            return "seen";
        } else if (i == 3) {
            return "nux";
        } else {
            throw new IllegalStateException(AnonymousClass08S.A09("Unknown montage item type ", i));
        }
    }

    public String A0G() {
        int i = this.A00;
        if (i != 2) {
            if (i == 3) {
                Preconditions.checkNotNull(this.A03);
            }
            return super.A0G();
        }
        Preconditions.checkNotNull(this.A02);
        return AnonymousClass08S.A0O(this.A02.A0U(), ":", this.A02.A02.A00);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[type = ");
        sb.append(A01(this));
        BasicMontageThreadInfo basicMontageThreadInfo = this.A02;
        if (basicMontageThreadInfo != null) {
            sb.append(", user = ");
            sb.append(basicMontageThreadInfo.A03);
        }
        sb.append("]");
        return sb.toString();
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeInt(this.A00);
        parcel.writeValue(this.A02);
        parcel.writeValue(this.A03);
        parcel.writeValue(this.A01);
        C417826y.A0V(parcel, this.A06);
        parcel.writeValue(this.A05);
        parcel.writeValue(this.A04);
    }

    public InboxMontageItem(C27161ck r1, int i, BasicMontageThreadInfo basicMontageThreadInfo, MontageInboxNuxItem montageInboxNuxItem, boolean z, RankingLoggingItem rankingLoggingItem, LastActive lastActive, UnifiedPresenceViewLoggerItem unifiedPresenceViewLoggerItem) {
        super(r1);
        this.A00 = i;
        this.A02 = basicMontageThreadInfo;
        this.A03 = montageInboxNuxItem;
        this.A06 = z;
        this.A01 = rankingLoggingItem;
        this.A05 = lastActive;
        this.A04 = unifiedPresenceViewLoggerItem;
    }

    public InboxMontageItem(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
        this.A02 = (BasicMontageThreadInfo) C417826y.A00(parcel, BasicMontageThreadInfo.class);
        this.A03 = (MontageInboxNuxItem) C417826y.A00(parcel, MontageInboxNuxItem.class);
        this.A06 = C417826y.A0W(parcel);
        this.A01 = (RankingLoggingItem) C417826y.A00(parcel, RankingLoggingItem.class);
        this.A05 = (LastActive) C417826y.A00(parcel, LastActive.class);
        this.A04 = (UnifiedPresenceViewLoggerItem) C417826y.A00(parcel, UnifiedPresenceViewLoggerItem.class);
    }
}
