package com.facebook.messaging.montage.omnistore;

import X.AnonymousClass09P;
import X.AnonymousClass0UN;
import X.AnonymousClass0VG;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2UG;
import X.AnonymousClass2WQ;
import X.C04310Tq;
import X.C32151lI;
import X.C32161lJ;
import X.C32171lK;
import X.C33451nb;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Cursor;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.OmnistoreIOException;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import java.nio.ByteBuffer;
import java.util.List;
import javax.inject.Singleton;
import org.json.JSONObject;

@Singleton
public final class MontagePreferencesOmnistoreComponent implements OmnistoreComponent {
    private static volatile MontagePreferencesOmnistoreComponent A04;
    public AnonymousClass0UN A00;
    private Collection A01;
    private final C04310Tq A02;
    private final C04310Tq A03;

    public String getCollectionLabel() {
        return "messenger_montage_preferences";
    }

    public void onCollectionInvalidated() {
        this.A01 = null;
        Boolean bool = false;
        ((AnonymousClass2UG) this.A02.get()).A04 = bool.booleanValue();
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static final MontagePreferencesOmnistoreComponent A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (MontagePreferencesOmnistoreComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new MontagePreferencesOmnistoreComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private void A01() {
        Collection collection = this.A01;
        if (collection != null) {
            try {
                Cursor object = collection.getObject("audience_mode");
                if (object.step()) {
                    ((AnonymousClass2UG) this.A02.get()).A02 = AnonymousClass2WQ.A00(object.getBlob()).A06();
                }
                Cursor object2 = this.A01.getObject("story_archive_saving_mode");
                if (object2.step()) {
                    ((AnonymousClass2UG) this.A02.get()).A01 = AnonymousClass2WQ.A00(object2.getBlob()).A06();
                }
                Cursor object3 = this.A01.getObject("mention_reshare_state");
                if (object3.step()) {
                    ((AnonymousClass2UG) this.A02.get()).A03 = AnonymousClass2WQ.A00(object3.getBlob()).A06();
                }
                if (this.A01.getSnapshotState() == 2) {
                    Boolean bool = true;
                    ((AnonymousClass2UG) this.A02.get()).A04 = bool.booleanValue();
                }
            } catch (OmnistoreIOException e) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00)).softReport("com.facebook.messaging.montage.omnistore.MontagePreferencesOmnistoreComponent", e.getMessage(), e);
            }
        }
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    public void onCollectionAvailable(Collection collection) {
        this.A01 = collection;
        A01();
    }

    private MontagePreferencesOmnistoreComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A03 = AnonymousClass0XJ.A0L(r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.A3P, r3);
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        CollectionName.Builder createCollectionNameWithDomainBuilder = omnistore.createCollectionNameWithDomainBuilder(getCollectionLabel(), "messenger_user_sq");
        createCollectionNameWithDomainBuilder.addSegment((String) this.A03.get());
        CollectionName build = createCollectionNameWithDomainBuilder.build();
        C32151lI r4 = new C32151lI();
        r4.A02 = new JSONObject().toString();
        r4.A03 = ((C33451nb) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6K, this.A00)).A02("messenger_montage_preferences.fbs", "com.facebook.messaging.montage.omnistore.MontagePreferencesOmnistoreComponent");
        r4.A04 = ((C33451nb) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6K, this.A00)).A02("messenger_montage_preferences.idna", "com.facebook.messaging.montage.omnistore.MontagePreferencesOmnistoreComponent");
        r4.A00 = 2;
        return C32171lK.A00(build, new C32161lJ(r4));
    }

    public void BVs(List list) {
        A01();
    }

    public void Boz(int i) {
        A01();
    }
}
