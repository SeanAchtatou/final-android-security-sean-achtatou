package com.facebook.messaging.montage.omnistore.converter;

import X.AnonymousClass09P;
import X.AnonymousClass1TT;
import X.AnonymousClass1TU;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2UN;
import X.AnonymousClass2VW;
import X.C04750Wa;
import X.C15860w6;
import X.C16640xT;
import X.C17350yl;
import X.C24331Te;
import X.C36831tu;
import X.C47332Uk;
import X.C47382Up;
import X.C47392Ur;
import X.C47402Us;
import X.C47412Ut;
import X.C47422Uv;
import X.C47432Uw;
import X.C99084oO;
import com.facebook.graphql.enums.EnumHelper;
import com.facebook.graphql.enums.GraphQLMessageImageType;
import com.facebook.messaging.model.attachment.AttachmentImageMap;
import com.facebook.messaging.model.attachment.ImageData;
import com.facebook.messaging.model.attachment.ImageUrl;
import com.facebook.messaging.model.messages.MontageStickerOverlayBounds;
import com.facebook.messaging.montage.omnistore.cache.OptimisticReadCache;
import java.nio.ByteBuffer;

public final class MontageMessageFBConverter {
    public final AnonymousClass09P A00;
    public final C17350yl A01;
    public final OptimisticReadCache A02;
    public final AnonymousClass1TU A03;

    private static AttachmentImageMap A00(C24331Te r4, C24331Te r5, C24331Te r6, C24331Te r7, C24331Te r8) {
        if (r4 == null && r5 == null && r6 == null && r7 == null && r8 == null) {
            return null;
        }
        AnonymousClass1TT r3 = new AnonymousClass1TT();
        if (r4 != null) {
            r3.A01.put(C47422Uv.A02, A01(r4));
        }
        if (r5 != null) {
            r3.A01.put(C47422Uv.SMALL_PREVIEW, A01(r5));
        }
        if (r6 != null) {
            r3.A01.put(C47422Uv.MEDIUM_PREVIEW, A01(r6));
        }
        if (r7 != null) {
            r3.A01.put(C47422Uv.LARGE_PREVIEW, A01(r7));
        }
        if (r8 != null) {
            r3.A01.put(C47422Uv.BLURRED_PREVIEW, A01(r8));
        }
        return new AttachmentImageMap(r3);
    }

    private static ImageUrl A01(C24331Te r4) {
        int i;
        int i2;
        if (r4 == null) {
            return null;
        }
        C47432Uw r3 = new C47432Uw();
        int A022 = r4.A02(6);
        if (A022 != 0) {
            i = r4.A01.getInt(A022 + r4.A00);
        } else {
            i = 0;
        }
        r3.A00 = i;
        int A023 = r4.A02(8);
        if (A023 != 0) {
            i2 = r4.A01.getInt(A023 + r4.A00);
        } else {
            i2 = 0;
        }
        r3.A01 = i2;
        r3.A02 = AnonymousClass2UN.A00(r4.A06());
        return new ImageUrl(r3);
    }

    private static MontageStickerOverlayBounds A02(AnonymousClass2VW r6) {
        double A09;
        double A0A;
        double A08;
        double A06;
        MontageStickerOverlayBounds.MontageStickerOverlayBoundsBuilder montageStickerOverlayBoundsBuilder = new MontageStickerOverlayBounds.MontageStickerOverlayBoundsBuilder();
        double d = 0.0d;
        if (r6 == null) {
            A09 = 0.0d;
        } else {
            A09 = (double) r6.A09();
        }
        montageStickerOverlayBoundsBuilder.A00 = A09;
        if (r6 == null) {
            A0A = 0.0d;
        } else {
            A0A = (double) r6.A0A();
        }
        montageStickerOverlayBoundsBuilder.A01 = A0A;
        if (r6 == null) {
            A08 = 0.0d;
        } else {
            A08 = (double) r6.A08();
        }
        montageStickerOverlayBoundsBuilder.A04 = A08;
        if (r6 == null) {
            A06 = 0.0d;
        } else {
            A06 = (double) r6.A06();
        }
        montageStickerOverlayBoundsBuilder.A02 = A06;
        if (r6 != null) {
            d = (double) r6.A07();
        }
        montageStickerOverlayBoundsBuilder.A03 = d;
        return montageStickerOverlayBoundsBuilder.A00();
    }

    public static final MontageMessageFBConverter A03(AnonymousClass1XY r1) {
        return new MontageMessageFBConverter(r1);
    }

    private static void A04(C15860w6 r18, C47332Uk r19, C47382Up r20) {
        C24331Te A07;
        byte b;
        C47392Ur r14;
        int A06;
        int A072;
        boolean z;
        C24331Te r2;
        C24331Te r8;
        C24331Te r6 = new C24331Te();
        C47332Uk r7 = r19;
        int A022 = r7.A02(26);
        if (A022 != 0) {
            int A012 = r7.A01(A022 + r7.A00);
            ByteBuffer byteBuffer = r7.A01;
            r6.A00 = A012;
            r6.A01 = byteBuffer;
        } else {
            r6 = null;
        }
        C24331Te A073 = r7.A07();
        if (r7.A06() != null) {
            A07 = r7.A06();
        } else {
            A07 = r7.A07();
        }
        C24331Te r3 = null;
        C47382Up r82 = r20;
        if (r82 == C47382Up.ANIMATED_IMAGE) {
            r14 = C47392Ur.NONQUICKCAM;
            C47402Us A08 = r7.A08();
            C47412Ut.A00(A08);
            A06 = (int) A08.A06();
            C47402Us A082 = r7.A08();
            C47412Ut.A00(A082);
            A072 = (int) A082.A07();
            int A023 = r7.A02(20);
            z = false;
            if (!(A023 == 0 || r7.A01.get(A023 + r7.A00) == 0)) {
                z = true;
            }
            r8 = new C24331Te();
            int A024 = r7.A02(16);
            if (A024 != 0) {
                int A013 = r7.A01(A024 + r7.A00);
                ByteBuffer byteBuffer2 = r7.A01;
                r8.A00 = A013;
                r8.A01 = byteBuffer2;
            } else {
                r8 = null;
            }
            r2 = new C24331Te();
            int A025 = r7.A02(18);
            if (A025 != 0) {
                int A014 = r7.A01(A025 + r7.A00);
                ByteBuffer byteBuffer3 = r7.A01;
                r2.A00 = A014;
                r2.A01 = byteBuffer3;
            } else {
                r2 = null;
            }
        } else if (r82 == C47382Up.A02) {
            int A026 = r7.A02(24);
            if (A026 != 0) {
                b = r7.A01.get(A026 + r7.A00);
            } else {
                b = 0;
            }
            if (((GraphQLMessageImageType) EnumHelper.A00(C16640xT.A00[b], GraphQLMessageImageType.A02)) == GraphQLMessageImageType.A01) {
                r14 = C47392Ur.QUICKCAM;
            } else {
                r14 = C47392Ur.NONQUICKCAM;
            }
            C47402Us A083 = r7.A08();
            C47412Ut.A00(A083);
            A06 = (int) A083.A06();
            C47402Us A084 = r7.A08();
            C47412Ut.A00(A084);
            A072 = (int) A084.A07();
            int A027 = r7.A02(20);
            boolean z2 = false;
            if (!(A027 == 0 || r7.A01.get(A027 + r7.A00) == 0)) {
                z2 = true;
            }
            r2 = null;
            r8 = null;
            r3 = A073;
        } else {
            throw new UnsupportedOperationException(C99084oO.$const$string(AnonymousClass1Y3.A2z) + r82);
        }
        if (r3 == null) {
            r3 = A073;
        }
        r18.A03 = new ImageData(A06, A072, A00(r6, A073, A073, A07, r3), A00(r8, r2, r2, r8, r2), r14, z, null, null);
    }

    public MontageMessageFBConverter(AnonymousClass1XY r2) {
        this.A03 = AnonymousClass1TU.A05(r2);
        this.A01 = C17350yl.A00(r2);
        C36831tu.A00(r2);
        this.A00 = C04750Wa.A01(r2);
        this.A02 = OptimisticReadCache.A00(r2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Double]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setTime(java.lang.String, java.lang.Long):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(int, java.lang.Long):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(java.lang.String, java.lang.Long):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(int, java.lang.Long):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setTime(int, java.lang.Long):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setTime(java.lang.String, java.lang.Long):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX WARNING: Code restructure failed: missing block: B:1290:0x1fe3, code lost:
        if (r6 != 1) goto L_0x1fe5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1340:0x20c1, code lost:
        if (r6 != 2) goto L_0x1fe5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1347:0x20e1, code lost:
        r2 = X.C47342Ul.VIDEO;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1472:0x240b, code lost:
        if (X.C32741mG.A01(r2) != false) goto L_0x0cd8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1491:0x24b2, code lost:
        if (r0.A0A().A0A() != false) goto L_0x2457;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1678:0x295c, code lost:
        if (r5 == false) goto L_0x295e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:656:0x0fe7, code lost:
        if (r2.A02.contains(r4) == false) goto L_0x0fe9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:1248:0x1f01  */
    /* JADX WARNING: Removed duplicated region for block: B:1278:0x1fb0  */
    /* JADX WARNING: Removed duplicated region for block: B:1294:0x1ff5  */
    /* JADX WARNING: Removed duplicated region for block: B:1299:0x201a  */
    /* JADX WARNING: Removed duplicated region for block: B:1301:0x2020  */
    /* JADX WARNING: Removed duplicated region for block: B:1312:0x2054  */
    /* JADX WARNING: Removed duplicated region for block: B:1317:0x2062  */
    /* JADX WARNING: Removed duplicated region for block: B:1438:0x233c  */
    /* JADX WARNING: Removed duplicated region for block: B:1440:0x2342  */
    /* JADX WARNING: Removed duplicated region for block: B:1460:0x23b2  */
    /* JADX WARNING: Removed duplicated region for block: B:1462:0x23ca  */
    /* JADX WARNING: Removed duplicated region for block: B:1527:0x25a0  */
    /* JADX WARNING: Removed duplicated region for block: B:1530:0x25a9  */
    /* JADX WARNING: Removed duplicated region for block: B:1536:0x25c2 A[SYNTHETIC, Splitter:B:1536:0x25c2] */
    /* JADX WARNING: Removed duplicated region for block: B:1640:0x2894 A[Catch:{ Exception -> 0x2c90 }] */
    /* JADX WARNING: Removed duplicated region for block: B:1756:0x2b10 A[Catch:{ Exception -> 0x2c90 }] */
    /* JADX WARNING: Removed duplicated region for block: B:1760:0x2b23  */
    /* JADX WARNING: Removed duplicated region for block: B:1763:0x2b37  */
    /* JADX WARNING: Removed duplicated region for block: B:1820:0x2c6e  */
    /* JADX WARNING: Removed duplicated region for block: B:1823:0x2c7c  */
    /* JADX WARNING: Removed duplicated region for block: B:1826:0x2c8e  */
    /* JADX WARNING: Removed duplicated region for block: B:1840:0x0c6f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:1876:0x23b4 A[EDGE_INSN: B:1876:0x23b4->B:1461:0x23b4 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:502:0x0c52  */
    /* JADX WARNING: Removed duplicated region for block: B:535:0x0ce7  */
    /* JADX WARNING: Removed duplicated region for block: B:537:0x0cf6  */
    /* JADX WARNING: Removed duplicated region for block: B:540:0x0cff  */
    /* JADX WARNING: Removed duplicated region for block: B:572:0x0dbb  */
    /* JADX WARNING: Removed duplicated region for block: B:628:0x0f15  */
    /* JADX WARNING: Removed duplicated region for block: B:630:0x0f24  */
    /* JADX WARNING: Removed duplicated region for block: B:636:0x0f4e  */
    /* JADX WARNING: Removed duplicated region for block: B:639:0x0f61  */
    /* JADX WARNING: Removed duplicated region for block: B:642:0x0f74  */
    /* JADX WARNING: Removed duplicated region for block: B:655:0x0fd8  */
    /* JADX WARNING: Removed duplicated region for block: B:659:0x1013  */
    /* JADX WARNING: Removed duplicated region for block: B:661:0x101c  */
    /* JADX WARNING: Removed duplicated region for block: B:687:0x10c1  */
    /* JADX WARNING: Removed duplicated region for block: B:689:0x10ca  */
    /* JADX WARNING: Removed duplicated region for block: B:699:0x10f3  */
    /* JADX WARNING: Removed duplicated region for block: B:725:0x1176  */
    /* JADX WARNING: Removed duplicated region for block: B:766:0x121b  */
    /* JADX WARNING: Removed duplicated region for block: B:768:0x1221  */
    /* JADX WARNING: Removed duplicated region for block: B:770:0x1228  */
    /* JADX WARNING: Removed duplicated region for block: B:771:0x122b  */
    /* JADX WARNING: Removed duplicated region for block: B:772:0x122e  */
    /* JADX WARNING: Removed duplicated region for block: B:774:0x1234  */
    /* JADX WARNING: Removed duplicated region for block: B:775:0x1237  */
    /* JADX WARNING: Removed duplicated region for block: B:796:0x129f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.Message A05(com.facebook.messaging.model.threadkey.ThreadKey r24, X.C36841tx r25) {
        /*
            r23 = this;
            X.1TG r8 = com.facebook.messaging.model.messages.Message.A00()
            X.2UM r4 = new X.2UM
            r4.<init>()
            r1 = 10
            r0 = r25
            int r2 = r0.A02(r1)
            if (r2 == 0) goto L_0x0c91
            int r1 = r0.A00
            int r2 = r2 + r1
            int r2 = r0.A01(r2)
            java.nio.ByteBuffer r1 = r0.A01
            r4.A00 = r2
            r4.A01 = r1
        L_0x0020:
            r1 = 4
            int r2 = r4.A02(r1)
            if (r2 == 0) goto L_0x0c8e
            int r1 = r4.A00
            int r2 = r2 + r1
            java.lang.String r1 = r4.A05(r2)
        L_0x002e:
            java.lang.String r1 = X.AnonymousClass2UN.A01(r1)
            X.1aC r17 = X.C25661aC.A00(r1)
            r1 = 6
            int r2 = r4.A02(r1)
            if (r2 == 0) goto L_0x0c8b
            int r1 = r4.A00
            int r2 = r2 + r1
            java.lang.String r1 = r4.A05(r2)
        L_0x0044:
            java.lang.String r3 = X.AnonymousClass2UN.A00(r1)
            r1 = 8
            int r2 = r4.A02(r1)
            if (r2 == 0) goto L_0x0c88
            int r1 = r4.A00
            int r2 = r2 + r1
            java.lang.String r1 = r4.A05(r2)
        L_0x0057:
            java.lang.String r12 = X.AnonymousClass2UN.A01(r1)
            com.facebook.messaging.model.messages.ParticipantInfo r10 = new com.facebook.messaging.model.messages.ParticipantInfo
            com.facebook.user.model.UserKey r11 = new com.facebook.user.model.UserKey
            X.1aB r1 = X.C25651aB.A03
            r11.<init>(r1, r3)
            r1 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r13 = r1
            r10.<init>(r11, r12, r13, r14, r15, r16, r17)
            r8.A0J = r10
            X.1V7 r2 = X.AnonymousClass1V7.A0K
            r8.A0C = r2
            r2 = 18
            int r3 = r0.A02(r2)
            if (r3 == 0) goto L_0x0c85
            int r2 = r0.A00
            int r3 = r3 + r2
            java.lang.String r2 = r0.A05(r3)
        L_0x0082:
            r8.A0y = r2
            r2 = r24
            r8.A0T = r2
            java.lang.String r2 = r0.A0D()
            r8.A06(r2)
            java.lang.String r2 = r0.A0E()
            long r2 = java.lang.Long.parseLong(r2)
            r8.A04 = r2
            r2 = 8
            int r3 = r0.A02(r2)
            if (r3 == 0) goto L_0x0c82
            int r2 = r0.A00
            int r3 = r3 + r2
            java.lang.String r2 = r0.A05(r3)
        L_0x00a8:
            r8.A0u = r2
            r2 = 16
            int r3 = r0.A02(r2)
            if (r3 == 0) goto L_0x0c7f
            int r2 = r0.A00
            int r3 = r3 + r2
            java.lang.String r2 = r0.A05(r3)
        L_0x00b9:
            java.lang.String r2 = X.AnonymousClass2UN.A00(r2)
            r8.A0w = r2
            r2 = 14
            int r4 = r0.A02(r2)
            if (r4 == 0) goto L_0x0c7c
            java.nio.ByteBuffer r3 = r0.A01
            int r2 = r0.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x00d0:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r8.A0i = r2
            r2 = r16
            r8.A15 = r2
            X.2UQ r2 = r0.A07()
            if (r2 == 0) goto L_0x0c79
            X.2UQ r4 = r0.A07()
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0c79
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x00f2:
            r8.A0o = r2
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder
            r9.<init>()
            r10 = 0
        L_0x00fa:
            r2 = 52
            int r2 = r0.A02(r2)
            if (r2 == 0) goto L_0x0c76
            int r2 = r0.A04(r2)
        L_0x0106:
            if (r10 >= r2) goto L_0x0c94
            X.2VP r4 = new X.2VP
            r4.<init>()
            r2 = 52
            int r2 = r0.A02(r2)
            if (r2 == 0) goto L_0x0c73
            int r3 = r0.A03(r2)
            int r2 = r10 << 2
            int r3 = r3 + r2
            int r3 = r0.A01(r3)
            java.nio.ByteBuffer r2 = r0.A01
            r4.A00 = r3
            r4.A01 = r2
        L_0x0126:
            X.AnonymousClass0qX.A00(r4)
            X.2VP r4 = (X.AnonymousClass2VP) r4
            X.2VQ r2 = r4.A0A()
            if (r2 == 0) goto L_0x028f
            X.2VQ r6 = r4.A0A()
            com.google.common.base.Preconditions.checkNotNull(r6)
            X.3el r5 = new X.3el
            r5.<init>()
            X.2VW r12 = r4.A08()
            r2 = 4
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x028c
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r2 = r6.A05(r3)
        L_0x014f:
            r5.A03 = r2
            r2 = 16
            int r7 = r4.A02(r2)
            if (r7 == 0) goto L_0x0289
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r7 = r7 + r2
            byte r3 = r3.get(r7)
        L_0x0162:
            java.lang.String[] r2 = X.C72173dm.A00
            r3 = r2[r3]
            r5.A05 = r3
            X.2xn r2 = r4.A09()
            if (r2 != 0) goto L_0x027f
            java.lang.String r3 = ""
        L_0x0170:
            r5.A04 = r3
            r2 = 10
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x027c
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r3 = r6.A05(r3)
        L_0x0181:
            r5.A06 = r3
            r2 = 6
            int r11 = r6.A02(r2)
            r7 = 0
            if (r11 == 0) goto L_0x0197
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r11 = r11 + r2
            byte r2 = r3.get(r11)
            if (r2 == 0) goto L_0x0197
            r7 = 1
        L_0x0197:
            r5.A08 = r7
            r2 = 18
            int r11 = r4.A02(r2)
            r7 = 0
            if (r11 == 0) goto L_0x01ae
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r11 = r11 + r2
            byte r2 = r3.get(r11)
            if (r2 == 0) goto L_0x01ae
            r7 = 1
        L_0x01ae:
            r5.A09 = r7
            r2 = 8
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x0279
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x01c1:
            r5.A00 = r2
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = A02(r12)
            r5.A02 = r2
            com.google.common.collect.ImmutableList$Builder r11 = new com.google.common.collect.ImmutableList$Builder
            r11.<init>()
            r7 = 0
        L_0x01cf:
            r2 = 12
            int r2 = r6.A02(r2)
            if (r2 == 0) goto L_0x0276
            int r2 = r6.A04(r2)
        L_0x01db:
            if (r7 >= r2) goto L_0x025d
            X.3dn r13 = new X.3dn
            r13.<init>()
            r2 = 12
            int r2 = r6.A02(r2)
            if (r2 == 0) goto L_0x025b
            int r3 = r6.A03(r2)
            int r2 = r7 << 2
            int r3 = r3 + r2
            int r3 = r6.A01(r3)
            java.nio.ByteBuffer r2 = r6.A01
            r13.A00 = r3
            r13.A01 = r2
        L_0x01fb:
            X.3em r12 = new X.3em
            r12.<init>()
            r2 = 6
            int r3 = r13.A02(r2)
            if (r3 == 0) goto L_0x0259
            int r2 = r13.A00
            int r3 = r3 + r2
            java.lang.String r2 = r13.A05(r3)
        L_0x020e:
            r12.A04 = r2
            r2 = 4
            int r4 = r13.A02(r2)
            if (r4 == 0) goto L_0x0257
            java.nio.ByteBuffer r3 = r13.A01
            int r2 = r13.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x0220:
            r12.A00 = r2
            r2 = 10
            int r4 = r13.A02(r2)
            if (r4 == 0) goto L_0x0255
            java.nio.ByteBuffer r3 = r13.A01
            int r2 = r13.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x0233:
            r12.A01 = r2
            r2 = 8
            int r4 = r13.A02(r2)
            if (r4 == 0) goto L_0x0253
            java.nio.ByteBuffer r3 = r13.A01
            int r2 = r13.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x0246:
            r12.A02 = r2
            com.facebook.messaging.model.messages.MontageFeedbackPollOption r2 = r12.A00()
            r11.add(r2)
            int r7 = r7 + 1
            goto L_0x01cf
        L_0x0253:
            r2 = 0
            goto L_0x0246
        L_0x0255:
            r2 = 0
            goto L_0x0233
        L_0x0257:
            r2 = 0
            goto L_0x0220
        L_0x0259:
            r2 = 0
            goto L_0x020e
        L_0x025b:
            r13 = 0
            goto L_0x01fb
        L_0x025d:
            com.google.common.collect.ImmutableList r3 = r11.build()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r3)
            r5.A07 = r2
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r3 = new com.facebook.messaging.model.messages.MontageFeedbackOverlay
            com.facebook.messaging.model.messages.MontageFeedbackPoll r2 = r5.A00()
            r3.<init>(r2)
            r9.add(r3)
            goto L_0x0c6f
        L_0x0276:
            r2 = 0
            goto L_0x01db
        L_0x0279:
            r2 = 0
            goto L_0x01c1
        L_0x027c:
            r3 = 0
            goto L_0x0181
        L_0x027f:
            X.2xn r2 = r4.A09()
            java.lang.String r3 = r2.A06()
            goto L_0x0170
        L_0x0289:
            r3 = 0
            goto L_0x0162
        L_0x028c:
            r2 = 0
            goto L_0x014f
        L_0x028f:
            X.2VR r2 = r4.A0B()
            if (r2 == 0) goto L_0x03cd
            X.2VR r6 = r4.A0B()
            com.google.common.base.Preconditions.checkNotNull(r6)
            X.2VW r7 = r4.A07()
            X.2xp r5 = new X.2xp
            r5.<init>()
            r2 = 8
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x03ca
            int r2 = r6.A00
            int r3 = r3 + r2
            int r3 = r6.A01(r3)
            java.nio.ByteBuffer r2 = r6.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x02ba:
            com.facebook.messaging.model.messages.MontageReactionSticker$MontageReactionStickerBuilder r4 = new com.facebook.messaging.model.messages.MontageReactionSticker$MontageReactionStickerBuilder
            r4.<init>()
            r2 = 4
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x03c7
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r2 = r6.A05(r3)
        L_0x02cd:
            r4.A02 = r2
            java.lang.String r6 = ""
            if (r5 != 0) goto L_0x03b4
            r2 = r6
        L_0x02d4:
            r4.A04 = r2
            if (r5 == 0) goto L_0x02e6
            r2 = 6
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x03b1
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r6 = r5.A05(r3)
        L_0x02e6:
            r4.A03 = r6
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = A02(r7)
            r4.A00 = r2
            if (r5 == 0) goto L_0x0c63
            r6 = 0
        L_0x02f1:
            r2 = 8
            int r2 = r5.A02(r2)
            if (r2 == 0) goto L_0x03ae
            int r2 = r5.A04(r2)
        L_0x02fd:
            if (r6 >= r2) goto L_0x0c63
            X.2xq r14 = new X.2xq
            r14.<init>()
            r2 = 8
            int r2 = r5.A02(r2)
            if (r2 == 0) goto L_0x03ab
            int r3 = r5.A03(r2)
            int r2 = r6 << 2
            int r3 = r3 + r2
            int r3 = r5.A01(r3)
            java.nio.ByteBuffer r2 = r5.A01
            r14.A00 = r3
            r14.A01 = r2
        L_0x031d:
            X.32l r7 = new X.32l
            r7.<init>()
            r2 = 6
            int r3 = r14.A02(r2)
            if (r3 == 0) goto L_0x03a9
            int r2 = r14.A00
            int r3 = r3 + r2
            int r3 = r14.A01(r3)
            java.nio.ByteBuffer r2 = r14.A01
            r7.A00 = r3
            r7.A01 = r2
        L_0x0336:
            r12 = r7
            if (r7 == 0) goto L_0x039d
            X.2VW r11 = new X.2VW
            r11.<init>()
            r2 = 6
            int r3 = r7.A02(r2)
            if (r3 == 0) goto L_0x03a7
            int r2 = r7.A00
            int r3 = r3 + r2
            int r3 = r7.A01(r3)
            java.nio.ByteBuffer r2 = r7.A01
            r11.A00 = r3
            r11.A01 = r2
        L_0x0352:
            com.facebook.messaging.model.messages.MontageStickerAnimationAsset$StickerAnimationAssetBuilder r7 = new com.facebook.messaging.model.messages.MontageStickerAnimationAsset$StickerAnimationAssetBuilder
            r7.<init>()
            r2 = 4
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x03a5
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r2 = r12.A05(r3)
        L_0x0365:
            r7.A01 = r2
            r2 = 4
            int r13 = r14.A02(r2)
            if (r13 == 0) goto L_0x03a3
            java.nio.ByteBuffer r3 = r14.A01
            int r2 = r14.A00
            int r13 = r13 + r2
            byte r3 = r3.get(r13)
        L_0x0377:
            java.lang.String[] r2 = X.C626132m.A00
            r2 = r2[r3]
            r7.A03 = r2
            r2 = 8
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x03a1
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r2 = r12.A05(r3)
        L_0x038c:
            r7.A02 = r2
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = A02(r11)
            r7.A00 = r2
            com.facebook.messaging.model.messages.MontageStickerAnimationAsset r3 = r7.A00()
            java.util.List r2 = r4.A05
            r2.add(r3)
        L_0x039d:
            int r6 = r6 + 1
            goto L_0x02f1
        L_0x03a1:
            r2 = 0
            goto L_0x038c
        L_0x03a3:
            r3 = 0
            goto L_0x0377
        L_0x03a5:
            r2 = 0
            goto L_0x0365
        L_0x03a7:
            r11 = 0
            goto L_0x0352
        L_0x03a9:
            r7 = 0
            goto L_0x0336
        L_0x03ab:
            r14 = 0
            goto L_0x031d
        L_0x03ae:
            r2 = 0
            goto L_0x02fd
        L_0x03b1:
            r6 = 0
            goto L_0x02e6
        L_0x03b4:
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x03c4
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
            goto L_0x02d4
        L_0x03c4:
            r2 = 0
            goto L_0x02d4
        L_0x03c7:
            r2 = 0
            goto L_0x02cd
        L_0x03ca:
            r5 = 0
            goto L_0x02ba
        L_0x03cd:
            r2 = 6
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x04ca
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x03db:
            if (r2 == 0) goto L_0x04cd
            r2 = 6
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x04c7
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r6 = r4.A05(r3)
        L_0x03eb:
            if (r6 != 0) goto L_0x03ef
            java.lang.String r6 = ""
        L_0x03ef:
            r2 = 8
            int r5 = r4.A02(r2)
            if (r5 == 0) goto L_0x04c4
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r5 = r5 + r2
            byte r3 = r3.get(r5)
        L_0x0400:
            java.lang.String[] r2 = X.C47552Vr.A00
            r7 = r2[r3]
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder r2 = new com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder
            r2.<init>()
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = r2.A00()
            X.2VW r11 = r4.A07()
            if (r11 == 0) goto L_0x043f
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder r5 = new com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder
            r5.<init>()
            float r2 = r11.A09()
            double r2 = (double) r2
            r5.A00 = r2
            float r2 = r11.A0A()
            double r2 = (double) r2
            r5.A01 = r2
            float r2 = r11.A08()
            double r2 = (double) r2
            r5.A04 = r2
            float r2 = r11.A06()
            double r2 = (double) r2
            r5.A02 = r2
            float r2 = r11.A07()
            double r2 = (double) r2
            r5.A03 = r2
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = r5.A00()
        L_0x043f:
            X.2Vu r5 = com.facebook.messaging.model.messages.MontageLinkSticker.A00(r2, r7, r6)
            X.2Vv r2 = r4.A06()
            if (r2 == 0) goto L_0x04ad
            X.2Vv r2 = r4.A06()
            X.2Vw r2 = r2.A06()
            if (r2 == 0) goto L_0x04ad
            X.2Vv r2 = r4.A06()
            X.2Vw r3 = r2.A06()
            r2 = 6
            int r2 = r3.A02(r2)
            if (r2 == 0) goto L_0x04c2
            int r2 = r3.A04(r2)
        L_0x0466:
            if (r2 == 0) goto L_0x04ad
            X.2Vv r2 = r4.A06()
            X.2Vw r6 = r2.A06()
            X.7Wc r4 = new X.7Wc
            r4.<init>()
            r2 = 6
            int r2 = r6.A02(r2)
            if (r2 == 0) goto L_0x04c0
            int r2 = r6.A03(r2)
            int r2 = r2 + r16
            int r3 = r6.A01(r2)
            java.nio.ByteBuffer r2 = r6.A01
            r4.A00 = r3
            r4.A01 = r2
        L_0x048c:
            r2 = 6
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x04be
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x049a:
            r5.A02 = r2
            r2 = 8
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x04bc
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x04ab:
            r5.A01 = r2
        L_0x04ad:
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r3 = new com.facebook.messaging.model.messages.MontageFeedbackOverlay
            com.facebook.messaging.model.messages.MontageLinkSticker r2 = new com.facebook.messaging.model.messages.MontageLinkSticker
            r2.<init>(r5)
            r3.<init>(r2)
            r9.add(r3)
            goto L_0x0c6f
        L_0x04bc:
            r2 = 0
            goto L_0x04ab
        L_0x04be:
            r2 = 0
            goto L_0x049a
        L_0x04c0:
            r4 = 0
            goto L_0x048c
        L_0x04c2:
            r2 = 0
            goto L_0x0466
        L_0x04c4:
            r3 = 0
            goto L_0x0400
        L_0x04c7:
            r6 = 0
            goto L_0x03eb
        L_0x04ca:
            r2 = 0
            goto L_0x03db
        L_0x04cd:
            X.2VS r2 = r4.A0D()
            if (r2 == 0) goto L_0x071a
            com.facebook.graphservice.factory.GraphQLServiceFactory r3 = X.C05850aR.A02()
            r2 = 127(0x7f, float:1.78E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r6 = X.C73513gF.A00(r2, r3)
            r2 = 52
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0717
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r3 = r4.A05(r3)
        L_0x04f0:
            r2 = 12
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r6.setString(r2, r3)
            r2 = 54
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0714
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r3 = r4.A05(r3)
        L_0x0508:
            java.lang.String r2 = "emoji"
            r6.setString(r2, r3)
            r2 = 56
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0711
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r3 = r4.A05(r3)
        L_0x051c:
            r2 = 209(0xd1, float:2.93E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r6.setString(r2, r3)
            X.2VS r11 = r4.A0D()
            if (r11 != 0) goto L_0x0540
            com.facebook.graphservice.factory.GraphQLServiceFactory r2 = X.C05850aR.A02()
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.A05(r2)
            java.lang.String r2 = "dummy_id"
            r5.A0I(r2)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 864418276(0x3385f9e4, float:6.23875E-8)
            r5.getResult(r3, r2)
        L_0x0540:
            com.facebook.graphservice.factory.GraphQLServiceFactory r2 = X.C05850aR.A02()
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.A05(r2)
            r2 = 4
            int r3 = r11.A02(r2)
            if (r3 == 0) goto L_0x070e
            int r2 = r11.A00
            int r3 = r3 + r2
            java.lang.String r2 = r11.A05(r3)
        L_0x0556:
            r5.A0I(r2)
            r2 = 6
            int r12 = r11.A02(r2)
            r7 = 0
            if (r12 == 0) goto L_0x056d
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r12 = r12 + r2
            byte r2 = r3.get(r12)
            if (r2 == 0) goto L_0x056d
            r7 = 1
        L_0x056d:
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r7)
            java.lang.String r2 = "can_viewer_vote"
            r5.setBoolean(r2, r3)
            r2 = 12
            int r7 = r11.A02(r2)
            if (r7 == 0) goto L_0x070b
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r7 = r7 + r2
            float r2 = r3.getFloat(r7)
        L_0x0587:
            double r2 = (double) r2
            java.lang.Double r3 = java.lang.Double.valueOf(r2)
            java.lang.String r2 = "slider_vote_average"
            r5.setDouble(r2, r3)
            r2 = 8
            int r7 = r11.A02(r2)
            if (r7 == 0) goto L_0x0708
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r7 = r7 + r2
            float r2 = r3.getFloat(r7)
        L_0x05a2:
            double r2 = (double) r2
            java.lang.Double r3 = java.lang.Double.valueOf(r2)
            java.lang.String r2 = "viewer_vote"
            r5.setDouble(r2, r3)
            r2 = 10
            int r7 = r11.A02(r2)
            if (r7 == 0) goto L_0x0705
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r7 = r7 + r2
            int r2 = r3.getInt(r7)
        L_0x05bd:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "slider_vote_count"
            r5.setInt(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 864418276(0x3385f9e4, float:6.23875E-8)
            com.facebook.graphservice.interfaces.Tree r3 = r5.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.String r2 = "slider_poll"
            r6.setTree(r2, r3)
            r2 = 60
            int r5 = r4.A02(r2)
            if (r5 == 0) goto L_0x0702
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r5 = r5 + r2
            byte r3 = r3.get(r5)
        L_0x05e7:
            java.lang.String[] r2 = X.AnonymousClass65A.A00
            r3 = r2[r3]
            com.facebook.graphql.enums.GraphQLStoryOverlaySliderStyle r2 = com.facebook.graphql.enums.GraphQLStoryOverlaySliderStyle.A01
            java.lang.Enum r3 = com.facebook.graphql.enums.EnumHelper.A00(r3, r2)
            com.facebook.graphql.enums.GraphQLStoryOverlaySliderStyle r3 = (com.facebook.graphql.enums.GraphQLStoryOverlaySliderStyle) r3
            r2 = 223(0xdf, float:3.12E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r6.A01(r2, r3)
            X.2VW r7 = r4.A08()
            X.AnonymousClass0qX.A00(r7)
            com.facebook.graphservice.factory.GraphQLServiceFactory r2 = X.C05850aR.A02()
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.A00(r2)
            float r2 = r7.A06()
            double r2 = (double) r2
            r5.A0C(r2)
            float r2 = r7.A08()
            double r2 = (double) r2
            r5.A0E(r2)
            float r2 = r7.A07()
            double r2 = (double) r2
            r5.A0D(r2)
            float r2 = r7.A09()
            double r2 = (double) r2
            r5.A0F(r2)
            float r2 = r7.A0A()
            double r2 = (double) r2
            r5.A0G(r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r5.A03()
            r2 = 235(0xeb, float:3.3E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r6.setTree(r2, r3)
            com.facebook.graphservice.factory.GraphQLServiceFactory r7 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r5 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "TextWithEntities"
            r2 = -1672642741(0xffffffff9c4d7f4b, float:-6.7993333E-22)
            X.11R r5 = r7.newTreeBuilder(r3, r5, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r5
            X.2xn r2 = r4.A09()
            java.lang.String r3 = r2.A06()
            java.lang.String r2 = "text"
            r5.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1672642741(0xffffffff9c4d7f4b, float:-6.7993333E-22)
            com.facebook.graphservice.interfaces.Tree r3 = r5.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.String r2 = "question_text"
            r6.setTree(r2, r3)
            X.3gF r6 = r6.A07()
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder r2 = new com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder
            r2.<init>()
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r20 = r2.A00()
            X.2VW r7 = r4.A07()
            if (r7 == 0) goto L_0x06ad
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder r5 = new com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder
            r5.<init>()
            float r2 = r7.A09()
            double r2 = (double) r2
            r5.A00 = r2
            float r2 = r7.A0A()
            double r2 = (double) r2
            r5.A01 = r2
            float r2 = r7.A08()
            double r2 = (double) r2
            r5.A04 = r2
            float r2 = r7.A06()
            double r2 = (double) r2
            r5.A02 = r2
            float r2 = r7.A07()
            double r2 = (double) r2
            r5.A03 = r2
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r20 = r5.A00()
        L_0x06ad:
            X.2xn r2 = r4.A09()
            java.lang.String r19 = r2.A06()
            r2 = 54
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x06ff
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r18 = r4.A05(r3)
        L_0x06c4:
            r2 = 56
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x06fc
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r22 = r4.A05(r3)
        L_0x06d3:
            r2 = 52
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x06f9
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r17 = r4.A05(r3)
        L_0x06e2:
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r4 = new com.facebook.messaging.model.messages.MontageFeedbackOverlay
            java.lang.String r21 = "ig_slider"
            X.Cql r3 = com.facebook.messaging.model.messages.MontageSliderSticker.A00(r17, r18, r19, r20, r21, r22)
            r3.A00 = r6
            com.facebook.messaging.model.messages.MontageSliderSticker r2 = new com.facebook.messaging.model.messages.MontageSliderSticker
            r2.<init>(r3)
            r4.<init>(r2)
            r9.add(r4)
            goto L_0x0c6f
        L_0x06f9:
            r17 = 0
            goto L_0x06e2
        L_0x06fc:
            r22 = 0
            goto L_0x06d3
        L_0x06ff:
            r18 = 0
            goto L_0x06c4
        L_0x0702:
            r3 = 0
            goto L_0x05e7
        L_0x0705:
            r2 = 0
            goto L_0x05bd
        L_0x0708:
            r2 = 0
            goto L_0x05a2
        L_0x070b:
            r2 = 0
            goto L_0x0587
        L_0x070e:
            r2 = 0
            goto L_0x0556
        L_0x0711:
            r3 = 0
            goto L_0x051c
        L_0x0714:
            r3 = 0
            goto L_0x0508
        L_0x0717:
            r3 = 0
            goto L_0x04f0
        L_0x071a:
            X.2VT r5 = new X.2VT
            r5.<init>()
            r2 = 48
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0966
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x0734:
            if (r5 == 0) goto L_0x0969
            X.35x r5 = new X.35x
            r5.<init>()
            X.2VW r6 = new X.2VW
            r6.<init>()
            r2 = 42
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0963
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r6.A00 = r3
            r6.A01 = r2
        L_0x0755:
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = A02(r6)
            r5.A00 = r2
            r2 = 44
            int r6 = r4.A02(r2)
            if (r6 == 0) goto L_0x0960
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r6 = r6 + r2
            byte r3 = r3.get(r6)
        L_0x076c:
            java.lang.String[] r2 = X.C621130b.A00
            r2 = r2[r3]
            if (r2 == 0) goto L_0x0774
            r5.A0A = r2
        L_0x0774:
            r2 = 46
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x095d
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0783:
            if (r2 == 0) goto L_0x0796
            r2 = 46
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x095a
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0794:
            r5.A09 = r2
        L_0x0796:
            r2 = 38
            int r6 = r4.A02(r2)
            if (r6 == 0) goto L_0x0957
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r6 = r6 + r2
            byte r3 = r3.get(r6)
        L_0x07a7:
            java.lang.String[] r2 = X.C634135y.A00
            r2 = r2[r3]
            if (r2 == 0) goto L_0x07af
            r5.A05 = r2
        L_0x07af:
            X.35z r6 = new X.35z
            r6.<init>()
            r2 = 40
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0954
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r6.A00 = r3
            r6.A01 = r2
        L_0x07c9:
            if (r6 == 0) goto L_0x094c
            r2 = 6
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x0949
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r2 = r6.A05(r3)
        L_0x07d9:
            r5.A06 = r2
            r2 = 4
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x0946
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r3 = r6.A05(r3)
        L_0x07e9:
            r5.A04 = r3
            r2 = 8
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x0943
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r3 = r6.A05(r3)
        L_0x07fa:
            r5.A03 = r3
            r2 = 16
            int r7 = r6.A02(r2)
            r4 = 0
            if (r7 == 0) goto L_0x0811
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r7 = r7 + r2
            byte r2 = r3.get(r7)
            if (r2 == 0) goto L_0x0811
            r4 = 1
        L_0x0811:
            r5.A0E = r4
            r2 = 18
            int r7 = r6.A02(r2)
            r4 = 0
            if (r7 == 0) goto L_0x0828
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r7 = r7 + r2
            byte r2 = r3.get(r7)
            if (r2 == 0) goto L_0x0828
            r4 = 1
        L_0x0828:
            r5.A0D = r4
            r2 = 14
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x0940
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r4 = r4 + r2
            byte r3 = r3.get(r4)
        L_0x083b:
            java.lang.String[] r2 = X.AnonymousClass360.A00
            r2 = r2[r3]
            if (r2 == 0) goto L_0x0843
            r5.A01 = r2
        L_0x0843:
            X.361 r4 = new X.361
            r4.<init>()
            r2 = 12
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x093d
            int r2 = r6.A00
            int r3 = r3 + r2
            int r3 = r6.A01(r3)
            java.nio.ByteBuffer r2 = r6.A01
            r4.A00 = r3
            r4.A01 = r2
        L_0x085d:
            if (r4 == 0) goto L_0x0880
            r2 = 8
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x093a
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x086e:
            r5.A08 = r2
            r2 = 6
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0937
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r3 = r4.A05(r3)
        L_0x087e:
            r5.A07 = r3
        L_0x0880:
            r2 = 20
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x0934
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r4 = r4 + r2
            byte r3 = r3.get(r4)
        L_0x0891:
            java.lang.String[] r2 = X.AnonymousClass362.A00
            r2 = r2[r3]
            if (r2 == 0) goto L_0x0899
            r5.A0C = r2
        L_0x0899:
            r2 = 22
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x0931
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r4 = r4 + r2
            byte r3 = r3.get(r4)
        L_0x08aa:
            java.lang.String[] r2 = X.C48882bI.A00
            r2 = r2[r3]
            if (r2 == 0) goto L_0x08b2
            r5.A0B = r2
        L_0x08b2:
            X.363 r4 = new X.363
            r4.<init>()
            r2 = 10
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x092f
            int r2 = r6.A00
            int r3 = r3 + r2
            int r3 = r6.A01(r3)
            java.nio.ByteBuffer r2 = r6.A01
            r4.A00 = r3
            r4.A01 = r2
        L_0x08cc:
            if (r4 == 0) goto L_0x091a
            X.364 r6 = new X.364
            r6.<init>()
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x092d
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r6.A00 = r3
            r6.A01 = r2
        L_0x08e7:
            if (r6 == 0) goto L_0x091a
            X.365 r4 = new X.365
            r4.<init>()
            r2 = 4
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x092b
            int r2 = r6.A00
            int r3 = r3 + r2
            int r3 = r6.A01(r3)
            java.nio.ByteBuffer r2 = r6.A01
            r4.A00 = r3
            r4.A01 = r2
        L_0x0902:
            if (r4 == 0) goto L_0x0929
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0929
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r3 = r4.A05(r3)
        L_0x0912:
            boolean r2 = android.text.TextUtils.isEmpty(r3)
            if (r2 != 0) goto L_0x091a
            r5.A02 = r3
        L_0x091a:
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r3 = new com.facebook.messaging.model.messages.MontageFeedbackOverlay
            com.facebook.messaging.model.messages.MontageEventsSticker r2 = new com.facebook.messaging.model.messages.MontageEventsSticker
            r2.<init>(r5)
            r3.<init>(r2)
            r9.add(r3)
            goto L_0x0c6f
        L_0x0929:
            r3 = 0
            goto L_0x0912
        L_0x092b:
            r4 = 0
            goto L_0x0902
        L_0x092d:
            r6 = 0
            goto L_0x08e7
        L_0x092f:
            r4 = 0
            goto L_0x08cc
        L_0x0931:
            r3 = 0
            goto L_0x08aa
        L_0x0934:
            r3 = 0
            goto L_0x0891
        L_0x0937:
            r3 = 0
            goto L_0x087e
        L_0x093a:
            r2 = 0
            goto L_0x086e
        L_0x093d:
            r4 = 0
            goto L_0x085d
        L_0x0940:
            r3 = 0
            goto L_0x083b
        L_0x0943:
            r3 = 0
            goto L_0x07fa
        L_0x0946:
            r3 = 0
            goto L_0x07e9
        L_0x0949:
            r2 = 0
            goto L_0x07d9
        L_0x094c:
            java.lang.String r3 = "com.facebook.messaging.montage.omnistore.converter.MontageMessageFBConverter"
            java.lang.String r2 = "event info bar event is null"
            X.C010708t.A0I(r3, r2)
            goto L_0x091a
        L_0x0954:
            r6 = 0
            goto L_0x07c9
        L_0x0957:
            r3 = 0
            goto L_0x07a7
        L_0x095a:
            r2 = 0
            goto L_0x0794
        L_0x095d:
            r2 = 0
            goto L_0x0783
        L_0x0960:
            r3 = 0
            goto L_0x076c
        L_0x0963:
            r6 = 0
            goto L_0x0755
        L_0x0966:
            r5 = 0
            goto L_0x0734
        L_0x0969:
            X.2VU r2 = r4.A0C()
            if (r2 == 0) goto L_0x0a24
            X.2VU r7 = r4.A0C()
            com.google.common.base.Preconditions.checkNotNull(r7)
            X.2VV r6 = new X.2VV
            r6.<init>()
            X.2VW r11 = r4.A07()
            if (r11 == 0) goto L_0x09af
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder r5 = new com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder
            r5.<init>()
            float r2 = r11.A09()
            double r2 = (double) r2
            r5.A00 = r2
            float r2 = r11.A0A()
            double r2 = (double) r2
            r5.A01 = r2
            float r2 = r11.A08()
            double r2 = (double) r2
            r5.A04 = r2
            float r2 = r11.A06()
            double r2 = (double) r2
            r5.A02 = r2
            float r2 = r11.A07()
            double r2 = (double) r2
            r5.A03 = r2
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = r5.A00()
            r6.A02 = r2
        L_0x09af:
            r2 = 34
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0a22
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x09be:
            if (r2 == 0) goto L_0x09c2
            r6.A08 = r2
        L_0x09c2:
            if (r7 == 0) goto L_0x0a06
            r2 = 6
            int r3 = r7.A02(r2)
            if (r3 == 0) goto L_0x0a20
            int r2 = r7.A00
            int r3 = r3 + r2
            java.lang.String r2 = r7.A05(r3)
        L_0x09d2:
            if (r2 == 0) goto L_0x09e4
            r2 = 6
            int r3 = r7.A02(r2)
            if (r3 == 0) goto L_0x0a1e
            int r2 = r7.A00
            int r3 = r3 + r2
            java.lang.String r2 = r7.A05(r3)
        L_0x09e2:
            r6.A06 = r2
        L_0x09e4:
            r2 = 8
            int r3 = r7.A02(r2)
            if (r3 == 0) goto L_0x0a1c
            int r2 = r7.A00
            int r3 = r3 + r2
            java.lang.String r2 = r7.A05(r3)
        L_0x09f3:
            if (r2 == 0) goto L_0x0a06
            r2 = 8
            int r3 = r7.A02(r2)
            if (r3 == 0) goto L_0x0a1a
            int r2 = r7.A00
            int r3 = r3 + r2
            java.lang.String r2 = r7.A05(r3)
        L_0x0a04:
            r6.A07 = r2
        L_0x0a06:
            X.2Va r2 = X.AnonymousClass2Va.POST
        L_0x0a08:
            r6.A00(r2)
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r3 = new com.facebook.messaging.model.messages.MontageFeedbackOverlay
            com.facebook.messaging.model.messages.MontageReshareContentSticker r2 = new com.facebook.messaging.model.messages.MontageReshareContentSticker
            r2.<init>(r6)
            r3.<init>(r2)
            r9.add(r3)
            goto L_0x0c6f
        L_0x0a1a:
            r2 = 0
            goto L_0x0a04
        L_0x0a1c:
            r2 = 0
            goto L_0x09f3
        L_0x0a1e:
            r2 = 0
            goto L_0x09e2
        L_0x0a20:
            r2 = 0
            goto L_0x09d2
        L_0x0a22:
            r2 = 0
            goto L_0x09be
        L_0x0a24:
            java.lang.String r2 = r4.A0E()
            if (r2 == 0) goto L_0x0b9b
            java.lang.String r3 = r4.A0E()
            java.lang.String r2 = "StoryOverlayResharedContent"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x0b9b
            X.7Wb r7 = new X.7Wb
            r7.<init>()
            r2 = 32
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0b8b
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r7.A00 = r3
            r7.A01 = r2
        L_0x0a50:
            X.2VV r6 = new X.2VV
            r6.<init>()
            X.2VW r11 = r4.A07()
            if (r11 == 0) goto L_0x0a89
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder r5 = new com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder
            r5.<init>()
            float r2 = r11.A09()
            double r2 = (double) r2
            r5.A00 = r2
            float r2 = r11.A0A()
            double r2 = (double) r2
            r5.A01 = r2
            float r2 = r11.A08()
            double r2 = (double) r2
            r5.A04 = r2
            float r2 = r11.A06()
            double r2 = (double) r2
            r5.A02 = r2
            float r2 = r11.A07()
            double r2 = (double) r2
            r5.A03 = r2
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r2 = r5.A00()
            r6.A02 = r2
        L_0x0a89:
            if (r7 == 0) goto L_0x0b30
            X.7WZ r11 = new X.7WZ
            r11.<init>()
            r2 = 6
            int r3 = r7.A02(r2)
            if (r3 == 0) goto L_0x0b88
            int r2 = r7.A00
            int r3 = r3 + r2
            int r3 = r7.A01(r3)
            java.nio.ByteBuffer r2 = r7.A01
            r11.A00 = r3
            r11.A01 = r2
        L_0x0aa4:
            if (r11 == 0) goto L_0x0ad2
            r2 = 6
            int r5 = r11.A02(r2)
            if (r5 == 0) goto L_0x0b85
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r5 = r5 + r2
            byte r3 = r3.get(r5)
        L_0x0ab6:
            java.lang.String[] r2 = X.C30306Etp.A00
            r3 = r2[r3]
            com.facebook.graphql.enums.GraphQLStoryCardTypes r2 = com.facebook.graphql.enums.GraphQLStoryCardTypes.A0F
            java.lang.Enum r5 = com.facebook.graphql.enums.EnumHelper.A00(r3, r2)
            com.facebook.graphql.enums.GraphQLStoryCardTypes r5 = (com.facebook.graphql.enums.GraphQLStoryCardTypes) r5
            r6.A00 = r5
            r2 = 328(0x148, float:4.6E-43)
            java.lang.String r3 = X.AnonymousClass24B.$const$string(r2)
            X.C28931fb.A06(r5, r3)
            java.util.Set r2 = r6.A09
            r2.add(r3)
        L_0x0ad2:
            r2 = 8
            int r2 = r7.A02(r2)
            if (r2 == 0) goto L_0x0b82
            int r2 = r7.A04(r2)
        L_0x0ade:
            if (r2 <= 0) goto L_0x0b30
            X.7Wa r5 = new X.7Wa
            r5.<init>()
            r2 = 8
            int r2 = r7.A02(r2)
            if (r2 == 0) goto L_0x0b7f
            int r2 = r7.A03(r2)
            int r2 = r2 + r16
            int r3 = r7.A01(r2)
            java.nio.ByteBuffer r2 = r7.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x0afd:
            if (r5 == 0) goto L_0x0b30
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x0b7d
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x0b0d:
            r6.A05 = r2
            r2 = 6
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x0b7b
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x0b1d:
            r6.A03 = r2
            r2 = 8
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x0b79
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x0b2e:
            r6.A04 = r2
        L_0x0b30:
            r2 = 28
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0b77
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0b3f:
            r6.A08 = r2
            r2 = 30
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0b75
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0b50:
            r6.A06 = r2
            r2 = 26
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0b73
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0b61:
            if (r2 == 0) goto L_0x0b97
            r2 = 26
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0b8e
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r3 = r4.A05(r3)
            goto L_0x0b8f
        L_0x0b73:
            r2 = 0
            goto L_0x0b61
        L_0x0b75:
            r2 = 0
            goto L_0x0b50
        L_0x0b77:
            r2 = 0
            goto L_0x0b3f
        L_0x0b79:
            r2 = 0
            goto L_0x0b2e
        L_0x0b7b:
            r2 = 0
            goto L_0x0b1d
        L_0x0b7d:
            r2 = 0
            goto L_0x0b0d
        L_0x0b7f:
            r5 = 0
            goto L_0x0afd
        L_0x0b82:
            r2 = 0
            goto L_0x0ade
        L_0x0b85:
            r3 = 0
            goto L_0x0ab6
        L_0x0b88:
            r11 = 0
            goto L_0x0aa4
        L_0x0b8b:
            r7 = 0
            goto L_0x0a50
        L_0x0b8e:
            r3 = 0
        L_0x0b8f:
            java.lang.String r2 = "UTF-8"
            java.lang.String r3 = java.net.URLDecoder.decode(r3, r2)     // Catch:{ UnsupportedEncodingException -> 0x0b95 }
        L_0x0b95:
            r6.A07 = r3
        L_0x0b97:
            X.2Va r2 = X.AnonymousClass2Va.STORY
            goto L_0x0a08
        L_0x0b9b:
            java.lang.String r2 = r4.A0E()
            if (r2 == 0) goto L_0x0c6f
            java.lang.String r3 = r4.A0E()
            java.lang.String r2 = "StoryOverlayTagSticker"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x0c6f
            r2 = 66
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0c60
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0bbc:
            if (r2 == 0) goto L_0x0c6f
            X.2VW r6 = r4.A07()
            if (r6 == 0) goto L_0x0c5e
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder r5 = new com.facebook.messaging.model.messages.MontageStickerOverlayBounds$MontageStickerOverlayBoundsBuilder
            r5.<init>()
            float r2 = r6.A09()
            double r2 = (double) r2
            r5.A00 = r2
            float r2 = r6.A0A()
            double r2 = (double) r2
            r5.A01 = r2
            float r2 = r6.A08()
            double r2 = (double) r2
            r5.A04 = r2
            float r2 = r6.A06()
            double r2 = (double) r2
            r5.A02 = r2
            float r2 = r6.A07()
            double r2 = (double) r2
            r5.A03 = r2
            com.facebook.messaging.model.messages.MontageStickerOverlayBounds r11 = r5.A00()
            X.2bJ r6 = new X.2bJ
            r6.<init>()
            r2 = 64
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0c5c
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r6.A00 = r3
            r6.A01 = r2
        L_0x0c0a:
            if (r6 == 0) goto L_0x0c5e
            r2 = 66
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x0c5a
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r7 = r4.A05(r3)
        L_0x0c1b:
            if (r7 == 0) goto L_0x0c5e
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r5 = new com.facebook.messaging.model.messages.MontageFeedbackOverlay
            r2 = 68
            int r12 = r4.A02(r2)
            if (r12 == 0) goto L_0x0c58
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r12 = r12 + r2
            byte r3 = r3.get(r12)
        L_0x0c30:
            java.lang.String[] r2 = X.C48902bK.A00
            r2 = r2[r3]
            X.2bM r4 = com.facebook.messaging.model.messages.MontageTagSticker.A00(r11, r7, r2)
            r2 = 6
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x0c56
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r2 = r6.A05(r3)
        L_0x0c46:
            r4.A01 = r2
            com.facebook.messaging.model.messages.MontageTagSticker r2 = new com.facebook.messaging.model.messages.MontageTagSticker
            r2.<init>(r4)
            r5.<init>(r2)
        L_0x0c50:
            if (r5 == 0) goto L_0x0c6f
            r9.add(r5)
            goto L_0x0c6f
        L_0x0c56:
            r2 = 0
            goto L_0x0c46
        L_0x0c58:
            r3 = 0
            goto L_0x0c30
        L_0x0c5a:
            r7 = 0
            goto L_0x0c1b
        L_0x0c5c:
            r6 = 0
            goto L_0x0c0a
        L_0x0c5e:
            r5 = r1
            goto L_0x0c50
        L_0x0c60:
            r2 = 0
            goto L_0x0bbc
        L_0x0c63:
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r3 = new com.facebook.messaging.model.messages.MontageFeedbackOverlay
            com.facebook.messaging.model.messages.MontageReactionSticker r2 = r4.A00()
            r3.<init>(r2)
            r9.add(r3)
        L_0x0c6f:
            int r10 = r10 + 1
            goto L_0x00fa
        L_0x0c73:
            r4 = 0
            goto L_0x0126
        L_0x0c76:
            r2 = 0
            goto L_0x0106
        L_0x0c79:
            r2 = 0
            goto L_0x00f2
        L_0x0c7c:
            r2 = 0
            goto L_0x00d0
        L_0x0c7f:
            r2 = 0
            goto L_0x00b9
        L_0x0c82:
            r2 = 0
            goto L_0x00a8
        L_0x0c85:
            r2 = 0
            goto L_0x0082
        L_0x0c88:
            r1 = 0
            goto L_0x0057
        L_0x0c8b:
            r1 = 0
            goto L_0x0044
        L_0x0c8e:
            r1 = 0
            goto L_0x002e
        L_0x0c91:
            r4 = 0
            goto L_0x0020
        L_0x0c94:
            com.google.common.collect.ImmutableList r2 = r9.build()
            r8.A0b = r2
            X.2UR r2 = r0.A0B()
            r7 = r23
            if (r2 == 0) goto L_0x2457
            X.2UR r2 = r0.A0B()
            java.lang.String r2 = r2.A0A()
            if (r2 == 0) goto L_0x23cd
            X.2UR r2 = r0.A0B()
            java.lang.String r3 = r2.A0A()
            X.1uC r2 = X.AnonymousClass1uC.Photo
            java.lang.String r2 = r2.toString()
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x23cd
            X.2UR r2 = r0.A0B()
            X.2US r2 = r2.A07()
            if (r2 == 0) goto L_0x23cd
            X.2UR r2 = r0.A0B()
            X.2US r2 = r2.A07()
            java.lang.String r2 = r2.A06()
            if (r2 == 0) goto L_0x23cd
        L_0x0cd8:
            r17 = 1
        L_0x0cda:
            X.2UT r11 = new X.2UT
            r11.<init>()
            r2 = 50
            int r3 = r0.A02(r2)
            if (r3 == 0) goto L_0x23ca
            int r2 = r0.A00
            int r3 = r3 + r2
            int r3 = r0.A01(r3)
            java.nio.ByteBuffer r2 = r0.A01
            r11.A00 = r3
            r11.A01 = r2
        L_0x0cf4:
            if (r11 != 0) goto L_0x0dbb
            r3 = r1
        L_0x0cf7:
            r8.A0N = r3
            X.2Uo r2 = r0.A08()
            if (r2 == 0) goto L_0x25a0
            X.2Uo r10 = r0.A08()
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder
            r9.<init>()
            if (r10 == 0) goto L_0x24b5
            r6 = 0
        L_0x0d0b:
            r2 = 6
            int r2 = r10.A02(r2)
            if (r2 == 0) goto L_0x0db8
            int r2 = r10.A04(r2)
        L_0x0d16:
            if (r6 >= r2) goto L_0x24b5
            X.84l r11 = new X.84l
            r11.<init>()
            r2 = 6
            int r2 = r10.A02(r2)
            if (r2 == 0) goto L_0x0db5
            int r3 = r10.A03(r2)
            int r2 = r6 << 2
            int r3 = r3 + r2
            int r3 = r10.A01(r3)
            java.nio.ByteBuffer r2 = r10.A01
            r11.A00 = r3
            r11.A01 = r2
        L_0x0d35:
            X.AnonymousClass0qX.A00(r11)
            X.84l r11 = (X.C1747884l) r11
            X.84m r2 = r11.A06()
            if (r2 == 0) goto L_0x0da9
            X.8yQ r5 = new X.8yQ
            r5.<init>()
            X.4x5 r4 = new X.4x5
            r4.<init>()
            X.84m r12 = r11.A06()
            r2 = 6
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x0db3
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r2 = r12.A05(r3)
        L_0x0d5c:
            r4.A01 = r2
            X.84m r12 = r11.A06()
            r2 = 4
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x0db1
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r2 = r12.A05(r3)
        L_0x0d70:
            java.lang.String r2 = X.AnonymousClass2UN.A00(r2)
            r4.A00 = r2
            com.facebook.messaging.model.messages.montageattribution.Entity r2 = new com.facebook.messaging.model.messages.montageattribution.Entity
            r2.<init>(r4)
            r5.A02 = r2
            r2 = 6
            int r4 = r11.A02(r2)
            if (r4 == 0) goto L_0x0daf
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x0d8d:
            r5.A00 = r2
            r2 = 4
            int r4 = r11.A02(r2)
            if (r4 == 0) goto L_0x0dad
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x0d9f:
            r5.A01 = r2
            com.facebook.messaging.model.messages.montageattribution.EntityAtRange r2 = new com.facebook.messaging.model.messages.montageattribution.EntityAtRange
            r2.<init>(r5)
            r9.add(r2)
        L_0x0da9:
            int r6 = r6 + 1
            goto L_0x0d0b
        L_0x0dad:
            r2 = 0
            goto L_0x0d9f
        L_0x0daf:
            r2 = 0
            goto L_0x0d8d
        L_0x0db1:
            r2 = 0
            goto L_0x0d70
        L_0x0db3:
            r2 = 0
            goto L_0x0d5c
        L_0x0db5:
            r11 = 0
            goto L_0x0d35
        L_0x0db8:
            r2 = 0
            goto L_0x0d16
        L_0x0dbb:
            X.1nd r19 = new X.1nd
            r19.<init>()
            r18 = 1
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r18)
            r2 = r19
            r2.A0J = r3
            X.2UU r12 = new X.2UU
            r12.<init>()
            r2 = 4
            int r3 = r11.A02(r2)
            if (r3 == 0) goto L_0x1eb0
            int r2 = r11.A00
            int r3 = r3 + r2
            int r3 = r11.A01(r3)
            java.nio.ByteBuffer r2 = r11.A01
            r12.A00 = r3
            r12.A01 = r2
        L_0x0de3:
            if (r12 != 0) goto L_0x1a93
            r3 = r1
        L_0x0de6:
            r2 = r19
            r2.A01 = r3
            r2 = 6
            int r2 = r11.A02(r2)
            if (r2 == 0) goto L_0x1a90
            int r12 = r11.A04(r2)
        L_0x0df5:
            if (r12 != 0) goto L_0x16ab
            r3 = r1
        L_0x0df8:
            r2 = r19
            r2.A09 = r3
            X.2UV r15 = new X.2UV
            r15.<init>()
            r2 = 14
            int r3 = r11.A02(r2)
            if (r3 == 0) goto L_0x16a8
            int r2 = r11.A00
            int r3 = r3 + r2
            int r3 = r11.A01(r3)
            java.nio.ByteBuffer r2 = r11.A01
            r15.A00 = r3
            r15.A01 = r2
        L_0x0e16:
            if (r15 != 0) goto L_0x12fe
            r3 = r1
        L_0x0e19:
            r2 = r19
            r2.A00 = r3
            X.2UW r2 = r11.A06()
            r9 = 0
            if (r2 == 0) goto L_0x12fb
            X.2UW r3 = r11.A06()
            r2 = 4
            int r2 = r3.A02(r2)
            if (r2 == 0) goto L_0x12f8
            int r2 = r3.A04(r2)
        L_0x0e33:
            if (r2 <= 0) goto L_0x12fb
            X.2UW r5 = r11.A06()
            X.85E r4 = new X.85E
            r4.<init>()
            r2 = 4
            int r2 = r5.A02(r2)
            if (r2 == 0) goto L_0x12f5
            int r2 = r5.A03(r2)
            int r2 = r2 + r16
            int r3 = r5.A01(r2)
            java.nio.ByteBuffer r2 = r5.A01
            r4.A00 = r3
            r4.A01 = r2
        L_0x0e55:
            if (r4 == 0) goto L_0x12f2
            X.85G r2 = r4.A08()
            if (r2 == 0) goto L_0x12f2
            X.85G r2 = r4.A08()
            java.lang.String r2 = r2.A06()
            if (r2 == 0) goto L_0x12f2
            X.85F r2 = r4.A06()
            if (r2 == 0) goto L_0x1261
            X.85F r5 = r4.A06()
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x125e
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x0e7f:
            if (r2 == 0) goto L_0x1261
            X.85G r2 = r4.A08()
            java.lang.String r5 = r2.A06()
            X.85F r6 = r4.A06()
            r2 = 4
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x125b
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r2 = r6.A05(r3)
        L_0x0e9b:
            X.EmB r3 = com.facebook.ipc.stories.model.InlineActivityInfo.A00(r5, r2)
            X.85F r6 = r4.A06()
            r2 = 6
            int r5 = r6.A02(r2)
            if (r5 == 0) goto L_0x1258
            int r2 = r6.A00
            int r5 = r5 + r2
            java.lang.String r2 = r6.A05(r5)
        L_0x0eb1:
            r3.A02 = r2
            X.85F r6 = r4.A06()
            r2 = 8
            int r5 = r6.A02(r2)
            if (r5 == 0) goto L_0x1255
            int r2 = r6.A00
            int r5 = r5 + r2
            java.lang.String r2 = r6.A05(r5)
        L_0x0ec6:
            r3.A03 = r2
            X.85F r2 = r4.A06()
            X.85J r2 = r2.A06()
            if (r2 != 0) goto L_0x123a
            r2 = r1
        L_0x0ed3:
            r3.A04 = r2
            X.85C r2 = r4.A09()
            if (r2 == 0) goto L_0x0ef3
            X.85C r2 = r4.A09()
            X.85B r2 = r2.A06()
            if (r2 == 0) goto L_0x0ef1
            X.85C r2 = r4.A09()
            X.85B r2 = r2.A06()
            java.lang.String r9 = r2.A06()
        L_0x0ef1:
            r3.A01 = r9
        L_0x0ef3:
            com.facebook.ipc.stories.model.InlineActivityInfo r4 = new com.facebook.ipc.stories.model.InlineActivityInfo
            r4.<init>(r3)
        L_0x0ef8:
            r2 = r19
            r2.A02 = r4
            X.1T9 r2 = r0.A0A()
            X.1ty r2 = r2.A07()
            X.2UX r4 = r2.A06()
            X.1Te r6 = new X.1Te
            r6.<init>()
            r2 = 10
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x1237
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r6.A00 = r3
            r6.A01 = r2
        L_0x0f22:
            if (r6 == 0) goto L_0x1234
            com.facebook.user.profilepic.PicSquare r9 = new com.facebook.user.profilepic.PicSquare
            com.facebook.user.profilepic.PicSquareUrlWithSize r5 = new com.facebook.user.profilepic.PicSquareUrlWithSize
            r2 = 8
            int r10 = r6.A02(r2)
            if (r10 == 0) goto L_0x1231
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r10 = r10 + r2
            int r3 = r3.getInt(r10)
        L_0x0f39:
            java.lang.String r2 = r6.A06()
            java.lang.String r2 = X.AnonymousClass2UN.A00(r2)
            r5.<init>(r3, r2)
            r9.<init>(r5)
        L_0x0f47:
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x122e
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0f55:
            java.lang.String r6 = X.AnonymousClass2UN.A00(r2)
            r2 = 8
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x122b
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0f68:
            java.lang.String r5 = X.AnonymousClass2UN.A01(r2)
            r2 = 12
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x1228
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x0f7b:
            java.lang.String r3 = X.AnonymousClass2UN.A01(r2)
            X.2UY r4 = new X.2UY
            r4.<init>()
            r4.A01 = r6
            java.lang.String r2 = "actorType"
            X.C28931fb.A06(r6, r2)
            r4.A00 = r9
            r4.A02 = r5
            r4.A03 = r3
            com.facebook.messaging.model.montagemetadata.MontageActorInfo r3 = new com.facebook.messaging.model.montagemetadata.MontageActorInfo
            r3.<init>(r4)
            r2 = r19
            r2.A04 = r3
            r2 = 12
            int r5 = r11.A02(r2)
            r4 = 0
            if (r5 == 0) goto L_0x0faf
            java.nio.ByteBuffer r3 = r11.A01
            int r2 = r11.A00
            int r5 = r5 + r2
            byte r2 = r3.get(r5)
            if (r2 == 0) goto L_0x0faf
            r4 = 1
        L_0x0faf:
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r4)
            r2 = r19
            r2.A0D = r3
            r2 = 13
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            X.C28931fb.A06(r3, r2)
            r2 = 22
            int r5 = r0.A02(r2)
            r4 = 0
            if (r5 == 0) goto L_0x0fd5
            java.nio.ByteBuffer r3 = r0.A01
            int r2 = r0.A00
            int r5 = r5 + r2
            byte r2 = r3.get(r5)
            if (r2 == 0) goto L_0x0fd5
            r4 = 1
        L_0x0fd5:
            r3 = 0
            if (r4 == 0) goto L_0x1224
            com.facebook.messaging.montage.omnistore.cache.OptimisticReadCache r2 = r7.A02
            java.lang.String r4 = r0.A0D()
            X.AnonymousClass0qX.A00(r4)
            java.util.Set r2 = r2.A02
            boolean r2 = r2.contains(r4)
            if (r2 != 0) goto L_0x1224
        L_0x0fe9:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r18)
            r2 = r19
            r2.A0I = r4
            java.lang.String r2 = "isUnread"
            X.C28931fb.A06(r4, r2)
            X.1T9 r2 = r0.A0A()
            boolean r2 = r2.A0A()
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r2)
            r2 = r19
            r2.A0G = r4
            java.lang.String r2 = "isMyMontage"
            X.C28931fb.A06(r4, r2)
            r2 = 36
            int r4 = r0.A02(r2)
            if (r4 == 0) goto L_0x1221
            int r2 = r0.A00
            int r4 = r4 + r2
            java.lang.String r2 = r0.A05(r4)
        L_0x101a:
            if (r2 == 0) goto L_0x102f
            r2 = 36
            int r4 = r0.A02(r2)
            if (r4 == 0) goto L_0x121e
            int r2 = r0.A00
            int r4 = r4 + r2
            java.lang.String r4 = r0.A05(r4)
        L_0x102b:
            r2 = r19
            r2.A0R = r4
        L_0x102f:
            r2 = 38
            int r6 = r0.A02(r2)
            r5 = 0
            if (r6 == 0) goto L_0x1044
            java.nio.ByteBuffer r4 = r0.A01
            int r2 = r0.A00
            int r6 = r6 + r2
            byte r2 = r4.get(r6)
            if (r2 == 0) goto L_0x1044
            r5 = 1
        L_0x1044:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r5)
            r2 = r19
            r2.A0A = r4
            java.lang.String r2 = "canMute"
            X.C28931fb.A06(r4, r2)
            r2 = 40
            int r6 = r0.A02(r2)
            r5 = 0
            if (r6 == 0) goto L_0x1066
            java.nio.ByteBuffer r4 = r0.A01
            int r2 = r0.A00
            int r6 = r6 + r2
            byte r2 = r4.get(r6)
            if (r2 == 0) goto L_0x1066
            r5 = 1
        L_0x1066:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r5)
            r2 = r19
            r2.A0C = r4
            java.lang.String r2 = "canReport"
            X.C28931fb.A06(r4, r2)
            r2 = 30
            int r6 = r0.A02(r2)
            r5 = 0
            if (r6 == 0) goto L_0x1088
            java.nio.ByteBuffer r4 = r0.A01
            int r2 = r0.A00
            int r6 = r6 + r2
            byte r2 = r4.get(r6)
            if (r2 == 0) goto L_0x1088
            r5 = 1
        L_0x1088:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r5)
            r2 = r19
            r2.A0B = r4
            java.lang.String r2 = "canReply"
            X.C28931fb.A06(r4, r2)
            X.1T9 r2 = r0.A0A()
            if (r2 == 0) goto L_0x10b9
            X.1T9 r2 = r0.A0A()
            java.lang.String r2 = r2.A09()
            if (r2 == 0) goto L_0x10b9
            X.1T9 r2 = r0.A0A()
            java.lang.String r2 = r2.A09()
            long r4 = java.lang.Long.parseLong(r2)
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r2 = r19
            r2.A0L = r4
        L_0x10b9:
            r2 = 28
            int r4 = r0.A02(r2)
            if (r4 == 0) goto L_0x121b
            int r2 = r0.A00
            int r4 = r4 + r2
            java.lang.String r2 = r0.A05(r4)
        L_0x10c8:
            if (r2 == 0) goto L_0x10dd
            r2 = 28
            int r4 = r0.A02(r2)
            if (r4 == 0) goto L_0x1218
            int r2 = r0.A00
            int r4 = r4 + r2
            java.lang.String r4 = r0.A05(r4)
        L_0x10d9:
            r2 = r19
            r2.A0N = r4
        L_0x10dd:
            X.2UZ r2 = r11.A08()
            if (r2 != 0) goto L_0x10e9
            X.2Ua r2 = r11.A07()
            if (r2 == 0) goto L_0x1efb
        L_0x10e9:
            X.2Ua r12 = r11.A07()
            X.2UZ r5 = r11.A08()
            if (r5 == 0) goto L_0x1215
            r2 = 4
            int r2 = r5.A02(r2)
            if (r2 == 0) goto L_0x1212
            int r4 = r5.A04(r2)
        L_0x10fe:
            r2 = 1
            if (r4 < r2) goto L_0x1215
            X.2Ub r6 = new X.2Ub
            r6.<init>()
            r2 = 4
            int r2 = r5.A02(r2)
            if (r2 == 0) goto L_0x120f
            int r2 = r5.A03(r2)
            int r2 = r2 + r16
            int r4 = r5.A01(r2)
            java.nio.ByteBuffer r2 = r5.A01
            r6.A00 = r4
            r6.A01 = r2
        L_0x111d:
            if (r6 == 0) goto L_0x1215
            r2 = 4
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x120c
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r2 = r6.A05(r4)
        L_0x112d:
            java.lang.String r5 = X.C47272Uc.A00(r2)
            r2 = 6
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x1209
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r2 = r6.A05(r4)
        L_0x113f:
            java.lang.String r2 = X.C47272Uc.A00(r2)
            if (r5 == 0) goto L_0x1215
            if (r2 == 0) goto L_0x1215
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r5, r2)
            X.2Uf r5 = com.facebook.ipc.stories.model.StoryBackgroundInfo.A00(r2)
            com.facebook.graphql.enums.GraphQLPostGradientDirection r2 = com.facebook.graphql.enums.GraphQLPostGradientDirection.TOP_BOTTOM
            r5.A00(r2)
            r2 = 8
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x1206
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r2 = r6.A05(r4)
        L_0x1163:
            java.lang.String r2 = X.C47272Uc.A00(r2)
            if (r2 == 0) goto L_0x116f
            java.lang.String r2 = X.AnonymousClass2UN.A00(r2)
            r5.A02 = r2
        L_0x116f:
            com.facebook.ipc.stories.model.StoryBackgroundInfo r4 = new com.facebook.ipc.stories.model.StoryBackgroundInfo
            r4.<init>(r5)
        L_0x1174:
            if (r4 != 0) goto L_0x1ef7
            if (r12 == 0) goto L_0x1ec5
            java.lang.String r2 = r12.A07()
            if (r2 == 0) goto L_0x1ec5
            java.lang.String r2 = r12.A07()
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x1ec5
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()
            X.2Vf r2 = r12.A06()
            if (r2 == 0) goto L_0x1eb3
            X.2Vf r4 = r12.A06()
            r2 = 6
            int r2 = r4.A02(r2)
            if (r2 == 0) goto L_0x1204
            int r2 = r4.A04(r2)
        L_0x11a1:
            if (r2 <= 0) goto L_0x1eb3
            r5 = 0
        L_0x11a4:
            X.2Vf r4 = r12.A06()
            r2 = 6
            int r2 = r4.A02(r2)
            if (r2 == 0) goto L_0x1202
            int r2 = r4.A04(r2)
        L_0x11b3:
            if (r5 >= r2) goto L_0x1ec7
            X.2Vf r10 = r12.A06()
            X.2Vg r9 = new X.2Vg
            r9.<init>()
            r2 = 6
            int r2 = r10.A02(r2)
            if (r2 == 0) goto L_0x1200
            int r4 = r10.A03(r2)
            int r2 = r5 << 2
            int r4 = r4 + r2
            int r4 = r10.A01(r4)
            java.nio.ByteBuffer r2 = r10.A01
            r9.A00 = r4
            r9.A01 = r2
        L_0x11d6:
            if (r9 == 0) goto L_0x11f9
            r2 = 4
            int r4 = r9.A02(r2)
            if (r4 == 0) goto L_0x11fe
            int r2 = r9.A00
            int r4 = r4 + r2
            java.lang.String r2 = r9.A05(r4)
        L_0x11e6:
            if (r2 == 0) goto L_0x11f9
            r2 = 4
            int r4 = r9.A02(r2)
            if (r4 == 0) goto L_0x11fc
            int r2 = r9.A00
            int r4 = r4 + r2
            java.lang.String r2 = r9.A05(r4)
        L_0x11f6:
            r6.add(r2)
        L_0x11f9:
            int r5 = r5 + 1
            goto L_0x11a4
        L_0x11fc:
            r2 = 0
            goto L_0x11f6
        L_0x11fe:
            r2 = 0
            goto L_0x11e6
        L_0x1200:
            r9 = 0
            goto L_0x11d6
        L_0x1202:
            r2 = 0
            goto L_0x11b3
        L_0x1204:
            r2 = 0
            goto L_0x11a1
        L_0x1206:
            r2 = 0
            goto L_0x1163
        L_0x1209:
            r2 = 0
            goto L_0x113f
        L_0x120c:
            r2 = 0
            goto L_0x112d
        L_0x120f:
            r6 = 0
            goto L_0x111d
        L_0x1212:
            r4 = 0
            goto L_0x10fe
        L_0x1215:
            r4 = r1
            goto L_0x1174
        L_0x1218:
            r4 = 0
            goto L_0x10d9
        L_0x121b:
            r2 = 0
            goto L_0x10c8
        L_0x121e:
            r4 = 0
            goto L_0x102b
        L_0x1221:
            r2 = 0
            goto L_0x101a
        L_0x1224:
            r18 = 0
            goto L_0x0fe9
        L_0x1228:
            r2 = 0
            goto L_0x0f7b
        L_0x122b:
            r2 = 0
            goto L_0x0f68
        L_0x122e:
            r2 = 0
            goto L_0x0f55
        L_0x1231:
            r3 = 0
            goto L_0x0f39
        L_0x1234:
            r9 = 0
            goto L_0x0f47
        L_0x1237:
            r6 = 0
            goto L_0x0f22
        L_0x123a:
            X.85F r2 = r4.A06()
            X.85J r6 = r2.A06()
            r2 = 4
            int r5 = r6.A02(r2)
            if (r5 == 0) goto L_0x1252
            int r2 = r6.A00
            int r5 = r5 + r2
            java.lang.String r2 = r6.A05(r5)
            goto L_0x0ed3
        L_0x1252:
            r2 = 0
            goto L_0x0ed3
        L_0x1255:
            r2 = 0
            goto L_0x0ec6
        L_0x1258:
            r2 = 0
            goto L_0x0eb1
        L_0x125b:
            r2 = 0
            goto L_0x0e9b
        L_0x125e:
            r2 = 0
            goto L_0x0e7f
        L_0x1261:
            X.85H r2 = r4.A07()
            if (r2 == 0) goto L_0x12c9
            X.85H r5 = r4.A07()
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x12c7
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x1279:
            if (r2 == 0) goto L_0x12c9
            X.85G r2 = r4.A08()
            java.lang.String r6 = r2.A06()
            X.85H r5 = r4.A07()
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x12c5
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x1295:
            X.EmB r3 = com.facebook.ipc.stories.model.InlineActivityInfo.A00(r6, r2)
        L_0x1299:
            X.85C r2 = r4.A09()
            if (r2 == 0) goto L_0x0ef3
            X.85C r2 = r4.A09()
            X.85D r2 = r2.A07()
            if (r2 == 0) goto L_0x12d8
            X.85C r2 = r4.A09()
            X.85D r5 = r2.A07()
            r2 = 4
            int r4 = r5.A02(r2)
            if (r4 == 0) goto L_0x12c3
            int r2 = r5.A00
            int r4 = r4 + r2
            java.lang.String r2 = r5.A05(r4)
        L_0x12bf:
            r3.A04 = r2
            goto L_0x0ef3
        L_0x12c3:
            r2 = 0
            goto L_0x12bf
        L_0x12c5:
            r2 = 0
            goto L_0x1295
        L_0x12c7:
            r2 = 0
            goto L_0x1279
        L_0x12c9:
            X.85G r2 = r4.A08()
            java.lang.String r3 = r2.A06()
            java.lang.String r2 = ""
            X.EmB r3 = com.facebook.ipc.stories.model.InlineActivityInfo.A00(r3, r2)
            goto L_0x1299
        L_0x12d8:
            X.85C r2 = r4.A09()
            X.85B r2 = r2.A06()
            if (r2 == 0) goto L_0x0ef3
            X.85C r2 = r4.A09()
            X.85B r2 = r2.A06()
            java.lang.String r2 = r2.A06()
            r3.A04 = r2
            goto L_0x0ef3
        L_0x12f2:
            r4 = r1
            goto L_0x0ef8
        L_0x12f5:
            r4 = 0
            goto L_0x0e55
        L_0x12f8:
            r2 = 0
            goto L_0x0e33
        L_0x12fb:
            r4 = r1
            goto L_0x0e55
        L_0x12fe:
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "TextWithEntities"
            r2 = -684097254(0xffffffffd739811a, float:-2.03964138E14)
            X.11R r14 = r5.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r14 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r14
            r2 = 6
            int r3 = r15.A02(r2)
            if (r3 == 0) goto L_0x16a5
            int r2 = r15.A00
            int r3 = r3 + r2
            java.lang.String r3 = r15.A05(r3)
        L_0x131d:
            java.lang.String r2 = "text"
            r14.setString(r2, r3)
            r2 = 8
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x16a2
            int r2 = r15.A04(r2)
        L_0x132e:
            if (r2 > 0) goto L_0x1593
            r3 = r1
        L_0x1331:
            java.lang.String r2 = "ranges"
            r14.setTreeList(r2, r3)
            r2 = 10
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x1590
            int r2 = r15.A04(r2)
        L_0x1342:
            if (r2 > 0) goto L_0x14fc
            r3 = r1
        L_0x1345:
            java.lang.String r2 = "aggregated_ranges"
            r14.setTreeList(r2, r3)
            r2 = 12
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x14f9
            int r2 = r15.A04(r2)
        L_0x1356:
            if (r2 > 0) goto L_0x136f
            r3 = r1
        L_0x1359:
            r2 = 519(0x207, float:7.27E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r14.setTreeList(r2, r3)
            java.lang.Class<X.4gE> r3 = X.C95324gE.class
            r2 = -684097254(0xffffffffd739811a, float:-2.03964138E14)
            com.facebook.graphservice.interfaces.Tree r3 = r14.getResult(r3, r2)
            X.4gE r3 = (X.C95324gE) r3
            goto L_0x0e19
        L_0x136f:
            com.google.common.collect.ImmutableList$Builder r13 = new com.google.common.collect.ImmutableList$Builder
            r13.<init>()
            r12 = 0
        L_0x1375:
            r2 = 12
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x14f0
            int r2 = r15.A04(r2)
        L_0x1381:
            if (r12 >= r2) goto L_0x14f3
            X.7WX r5 = new X.7WX
            r5.<init>()
            r2 = 12
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x14ed
            int r2 = r15.A03(r2)
            int r3 = r12 << 2
            int r2 = r2 + r3
            int r3 = r15.A01(r2)
            java.nio.ByteBuffer r2 = r15.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x13a1:
            if (r5 == 0) goto L_0x14d1
            com.facebook.graphservice.factory.GraphQLServiceFactory r6 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 276(0x114, float:3.87E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = -1027164784(0xffffffffc2c6b590, float:-99.354614)
            X.11R r10 = r6.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r10 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r10
            r2 = 4
            int r4 = r5.A02(r2)
            if (r4 == 0) goto L_0x14ea
            java.nio.ByteBuffer r3 = r5.A01
            int r2 = r5.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x13c8:
            r10.A0H(r2)
            r2 = 6
            int r4 = r5.A02(r2)
            if (r4 == 0) goto L_0x14e7
            java.nio.ByteBuffer r3 = r5.A01
            int r2 = r5.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x13db:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "length"
            r10.setInt(r2, r3)
            X.7WW r4 = new X.7WW
            r4.<init>()
            r2 = 8
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x14e4
            int r2 = r5.A00
            int r3 = r3 + r2
            int r2 = r5.A01(r3)
            java.nio.ByteBuffer r3 = r5.A01
            r4.A00 = r2
            r4.A01 = r3
        L_0x13fe:
            if (r4 == 0) goto L_0x14c3
            r2 = 6
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x14e1
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r6 = r4.A05(r3)
        L_0x140e:
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r3 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = -580851045(0xffffffffdd60ea9b, float:-1.01293354E18)
            X.11R r9 = r5.newTreeBuilder(r6, r3, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r9 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r9
            X.7WV r6 = new X.7WV
            r6.<init>()
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x14de
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r6.A00 = r3
            r6.A01 = r2
        L_0x1436:
            if (r6 == 0) goto L_0x14b3
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "Image"
            r2 = -1725656266(0xffffffff99249336, float:-8.50833E-24)
            X.11R r5 = r5.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r5
            r2 = 4
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x14db
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r3 = r6.A05(r3)
        L_0x1457:
            java.lang.String r2 = "uri"
            r5.setString(r2, r3)
            r2 = 6
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x14d9
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x146c:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "width"
            r5.setInt(r2, r3)
            r2 = 8
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x14d7
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x1486:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "height"
            r5.setInt(r2, r3)
            r2 = 10
            int r3 = r6.A02(r2)
            if (r3 == 0) goto L_0x14d5
            int r2 = r6.A00
            int r3 = r3 + r2
            java.lang.String r3 = r6.A05(r3)
        L_0x149e:
            java.lang.String r2 = "name"
            r5.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1725656266(0xffffffff99249336, float:-8.50833E-24)
            com.facebook.graphservice.interfaces.Tree r3 = r5.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.String r2 = "image"
            r9.setTree(r2, r3)
        L_0x14b3:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -580851045(0xffffffffdd60ea9b, float:-1.01293354E18)
            com.facebook.graphservice.interfaces.Tree r3 = r9.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.String r2 = "entity_with_image"
            r10.setTree(r2, r3)
        L_0x14c3:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1027164784(0xffffffffc2c6b590, float:-99.354614)
            com.facebook.graphservice.interfaces.Tree r2 = r10.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            r13.add(r2)
        L_0x14d1:
            int r12 = r12 + 1
            goto L_0x1375
        L_0x14d5:
            r3 = 0
            goto L_0x149e
        L_0x14d7:
            r2 = 0
            goto L_0x1486
        L_0x14d9:
            r2 = 0
            goto L_0x146c
        L_0x14db:
            r3 = 0
            goto L_0x1457
        L_0x14de:
            r6 = 0
            goto L_0x1436
        L_0x14e1:
            r6 = 0
            goto L_0x140e
        L_0x14e4:
            r4 = 0
            goto L_0x13fe
        L_0x14e7:
            r2 = 0
            goto L_0x13db
        L_0x14ea:
            r2 = 0
            goto L_0x13c8
        L_0x14ed:
            r5 = 0
            goto L_0x13a1
        L_0x14f0:
            r2 = 0
            goto L_0x1381
        L_0x14f3:
            com.google.common.collect.ImmutableList r3 = r13.build()
            goto L_0x1359
        L_0x14f9:
            r2 = 0
            goto L_0x1356
        L_0x14fc:
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder
            r9.<init>()
            r6 = 0
        L_0x1502:
            r2 = 10
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x1588
            int r2 = r15.A04(r2)
        L_0x150e:
            if (r6 >= r2) goto L_0x158a
            X.7WY r10 = new X.7WY
            r10.<init>()
            r2 = 10
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x1586
            int r2 = r15.A03(r2)
            int r3 = r6 << 2
            int r2 = r2 + r3
            int r3 = r15.A01(r2)
            java.nio.ByteBuffer r2 = r15.A01
            r10.A00 = r3
            r10.A01 = r2
        L_0x152e:
            if (r10 == 0) goto L_0x157f
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 214(0xd6, float:3.0E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = 197574311(0xbc6bea7, float:7.655369E-32)
            X.11R r5 = r5.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r5
            r2 = 6
            int r4 = r10.A02(r2)
            if (r4 == 0) goto L_0x1584
            java.nio.ByteBuffer r3 = r10.A01
            int r2 = r10.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x1555:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "length"
            r5.setInt(r2, r3)
            r2 = 4
            int r4 = r10.A02(r2)
            if (r4 == 0) goto L_0x1582
            java.nio.ByteBuffer r3 = r10.A01
            int r2 = r10.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x156e:
            r5.A0H(r2)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 197574311(0xbc6bea7, float:7.655369E-32)
            com.facebook.graphservice.interfaces.Tree r2 = r5.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            r9.add(r2)
        L_0x157f:
            int r6 = r6 + 1
            goto L_0x1502
        L_0x1582:
            r2 = 0
            goto L_0x156e
        L_0x1584:
            r2 = 0
            goto L_0x1555
        L_0x1586:
            r10 = 0
            goto L_0x152e
        L_0x1588:
            r2 = 0
            goto L_0x150e
        L_0x158a:
            com.google.common.collect.ImmutableList r3 = r9.build()
            goto L_0x1345
        L_0x1590:
            r2 = 0
            goto L_0x1342
        L_0x1593:
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder
            r9.<init>()
            r6 = 0
        L_0x1599:
            r2 = 8
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x1699
            int r2 = r15.A04(r2)
        L_0x15a5:
            if (r6 >= r2) goto L_0x169c
            X.7WU r4 = new X.7WU
            r4.<init>()
            r2 = 8
            int r2 = r15.A02(r2)
            if (r2 == 0) goto L_0x1696
            int r2 = r15.A03(r2)
            int r3 = r6 << 2
            int r2 = r2 + r3
            int r3 = r15.A01(r2)
            java.nio.ByteBuffer r2 = r15.A01
            r4.A00 = r3
            r4.A01 = r2
        L_0x15c5:
            if (r4 == 0) goto L_0x1684
            com.facebook.graphservice.factory.GraphQLServiceFactory r10 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r5 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 30
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = 1277079995(0x4c1eb1bb, float:4.1600748E7)
            X.11R r5 = r10.newTreeBuilder(r3, r5, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r5
            r2 = 6
            int r10 = r4.A02(r2)
            if (r10 == 0) goto L_0x1693
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r10 = r10 + r2
            int r2 = r3.getInt(r10)
        L_0x15ec:
            r5.A0H(r2)
            r2 = 4
            int r10 = r4.A02(r2)
            if (r10 == 0) goto L_0x1690
            java.nio.ByteBuffer r3 = r4.A01
            int r2 = r4.A00
            int r10 = r10 + r2
            int r2 = r3.getInt(r10)
        L_0x15ff:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "length"
            r5.setInt(r2, r3)
            X.7WT r10 = new X.7WT
            r10.<init>()
            r2 = 8
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x168e
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r10.A00 = r3
            r10.A01 = r2
        L_0x1622:
            if (r10 == 0) goto L_0x1676
            r2 = 4
            int r3 = r10.A02(r2)
            if (r3 == 0) goto L_0x168c
            int r2 = r10.A00
            int r3 = r3 + r2
            java.lang.String r12 = r10.A05(r3)
        L_0x1632:
            com.facebook.graphservice.factory.GraphQLServiceFactory r4 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r3 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 380387876(0x16ac4224, float:2.782984E-25)
            X.11R r4 = r4.newTreeBuilder(r12, r3, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 6
            int r3 = r10.A02(r2)
            if (r3 == 0) goto L_0x168a
            int r2 = r10.A00
            int r3 = r3 + r2
            java.lang.String r2 = r10.A05(r3)
        L_0x164f:
            r4.A0I(r2)
            r2 = 8
            int r3 = r10.A02(r2)
            if (r3 == 0) goto L_0x1688
            int r2 = r10.A00
            int r3 = r3 + r2
            java.lang.String r3 = r10.A05(r3)
        L_0x1661:
            java.lang.String r2 = "url"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 380387876(0x16ac4224, float:2.782984E-25)
            com.facebook.graphservice.interfaces.Tree r3 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.String r2 = "entity"
            r5.setTree(r2, r3)
        L_0x1676:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 1277079995(0x4c1eb1bb, float:4.1600748E7)
            com.facebook.graphservice.interfaces.Tree r2 = r5.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            r9.add(r2)
        L_0x1684:
            int r6 = r6 + 1
            goto L_0x1599
        L_0x1688:
            r3 = 0
            goto L_0x1661
        L_0x168a:
            r2 = 0
            goto L_0x164f
        L_0x168c:
            r12 = 0
            goto L_0x1632
        L_0x168e:
            r10 = 0
            goto L_0x1622
        L_0x1690:
            r2 = 0
            goto L_0x15ff
        L_0x1693:
            r2 = 0
            goto L_0x15ec
        L_0x1696:
            r4 = 0
            goto L_0x15c5
        L_0x1699:
            r2 = 0
            goto L_0x15a5
        L_0x169c:
            com.google.common.collect.ImmutableList r3 = r9.build()
            goto L_0x1331
        L_0x16a2:
            r2 = 0
            goto L_0x132e
        L_0x16a5:
            r3 = 0
            goto L_0x131d
        L_0x16a8:
            r15 = 0
            goto L_0x0e16
        L_0x16ab:
            com.google.common.collect.ImmutableList$Builder r20 = new com.google.common.collect.ImmutableList$Builder
            r20.<init>()
            r10 = 0
        L_0x16b1:
            if (r10 >= r12) goto L_0x1a8a
            X.2Vx r9 = new X.2Vx
            r9.<init>()
            r2 = 6
            int r2 = r11.A02(r2)
            if (r2 == 0) goto L_0x1a87
            int r3 = r11.A03(r2)
            int r2 = r10 << 2
            int r3 = r3 + r2
            int r3 = r11.A01(r3)
            java.nio.ByteBuffer r2 = r11.A01
            r9.A00 = r3
            r9.A01 = r2
        L_0x16d0:
            if (r9 == 0) goto L_0x17c7
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 359(0x167, float:5.03E-43)
            java.lang.String r3 = X.C99084oO.$const$string(r2)
            r2 = -978801287(0xffffffffc5a8ad79, float:-5397.684)
            X.11R r6 = r5.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r6 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r6
            X.2Vy r5 = new X.2Vy
            r5.<init>()
            r2 = 4
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1a84
            int r2 = r9.A00
            int r3 = r3 + r2
            int r2 = r9.A01(r3)
            java.nio.ByteBuffer r3 = r9.A01
            r5.A00 = r2
            r5.A01 = r3
        L_0x1700:
            if (r5 != 0) goto L_0x1a51
            r2 = r1
        L_0x1703:
            java.lang.String r3 = "title_with_entities"
            r6.setTree(r3, r2)
            X.2Vz r5 = new X.2Vz
            r5.<init>()
            r2 = 6
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1a4e
            int r2 = r9.A00
            int r3 = r3 + r2
            int r2 = r9.A01(r3)
            java.nio.ByteBuffer r3 = r9.A01
            r5.A00 = r2
            r5.A01 = r3
        L_0x1721:
            if (r5 != 0) goto L_0x1a1b
            r2 = r1
        L_0x1724:
            java.lang.String r3 = "description"
            r6.setTree(r3, r2)
            X.2W0 r5 = new X.2W0
            r5.<init>()
            r2 = 8
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1a18
            int r2 = r9.A00
            int r3 = r3 + r2
            int r2 = r9.A01(r3)
            java.nio.ByteBuffer r3 = r9.A01
            r5.A00 = r2
            r5.A01 = r3
        L_0x1743:
            if (r5 != 0) goto L_0x19e5
            r2 = r1
        L_0x1746:
            java.lang.String r3 = "source"
            r6.setTree(r3, r2)
            X.2W1 r4 = new X.2W1
            r4.<init>()
            r2 = 10
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x19e2
            int r2 = r9.A00
            int r3 = r3 + r2
            int r2 = r9.A01(r3)
            java.nio.ByteBuffer r3 = r9.A01
            r4.A00 = r2
            r4.A01 = r3
        L_0x1765:
            if (r4 != 0) goto L_0x1961
            r2 = r1
        L_0x1768:
            java.lang.String r3 = "target"
            r6.setTree(r3, r2)
            r2 = 12
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x195e
            int r2 = r9.A00
            int r3 = r3 + r2
            java.lang.String r3 = r9.A05(r3)
        L_0x177c:
            java.lang.String r2 = "url"
            r6.setString(r2, r3)
            X.2W3 r5 = new X.2W3
            r5.<init>()
            r2 = 14
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x195b
            int r2 = r9.A00
            int r3 = r3 + r2
            int r2 = r9.A01(r3)
            java.nio.ByteBuffer r3 = r9.A01
            r5.A00 = r2
            r5.A01 = r3
        L_0x179b:
            if (r5 != 0) goto L_0x1878
            r2 = r1
        L_0x179e:
            java.lang.String r3 = "media"
            r6.setTree(r3, r2)
            r2 = 16
            int r2 = r9.A02(r2)
            if (r2 == 0) goto L_0x1875
            int r2 = r9.A04(r2)
        L_0x17af:
            if (r2 > 0) goto L_0x17cb
            r3 = r1
        L_0x17b2:
            java.lang.String r2 = "action_links"
            r6.setTreeList(r2, r3)
            java.lang.Class<X.2W5> r3 = X.AnonymousClass2W5.class
            r2 = -978801287(0xffffffffc5a8ad79, float:-5397.684)
            com.facebook.graphservice.interfaces.Tree r3 = r6.getResult(r3, r2)
            X.2W5 r3 = (X.AnonymousClass2W5) r3
            r2 = r20
            r2.add(r3)
        L_0x17c7:
            int r10 = r10 + 1
            goto L_0x16b1
        L_0x17cb:
            com.google.common.collect.ImmutableList$Builder r13 = new com.google.common.collect.ImmutableList$Builder
            r13.<init>()
            r5 = 0
        L_0x17d1:
            r2 = 16
            int r2 = r9.A02(r2)
            if (r2 == 0) goto L_0x186c
            int r2 = r9.A04(r2)
        L_0x17dd:
            if (r5 >= r2) goto L_0x186f
            X.2bG r14 = new X.2bG
            r14.<init>()
            r2 = 16
            int r2 = r9.A02(r2)
            if (r2 == 0) goto L_0x186a
            int r2 = r9.A03(r2)
            int r3 = r5 << 2
            int r2 = r2 + r3
            int r3 = r9.A01(r2)
            java.nio.ByteBuffer r2 = r9.A01
            r14.A00 = r3
            r14.A01 = r2
        L_0x17fd:
            if (r14 == 0) goto L_0x1860
            com.facebook.graphservice.factory.GraphQLServiceFactory r15 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "ArticleContextActionLink"
            r2 = 1153377331(0x44bf2433, float:1529.1312)
            X.11R r4 = r15.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r14.A02(r2)
            if (r3 == 0) goto L_0x1868
            int r2 = r14.A00
            int r3 = r3 + r2
            java.lang.String r3 = r14.A05(r3)
        L_0x181e:
            r2 = 527(0x20f, float:7.38E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r4.setString(r2, r3)
            r2 = 6
            int r3 = r14.A02(r2)
            if (r3 == 0) goto L_0x1866
            int r2 = r14.A00
            int r3 = r3 + r2
            java.lang.String r3 = r14.A05(r3)
        L_0x1835:
            r2 = 528(0x210, float:7.4E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r4.setString(r2, r3)
            r2 = 8
            int r3 = r14.A02(r2)
            if (r3 == 0) goto L_0x1864
            int r2 = r14.A00
            int r3 = r3 + r2
            java.lang.String r3 = r14.A05(r3)
        L_0x184d:
            java.lang.String r2 = "integrity_context_extra_data"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 1153377331(0x44bf2433, float:1529.1312)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            r13.add(r2)
        L_0x1860:
            int r5 = r5 + 1
            goto L_0x17d1
        L_0x1864:
            r3 = 0
            goto L_0x184d
        L_0x1866:
            r3 = 0
            goto L_0x1835
        L_0x1868:
            r3 = 0
            goto L_0x181e
        L_0x186a:
            r14 = 0
            goto L_0x17fd
        L_0x186c:
            r2 = 0
            goto L_0x17dd
        L_0x186f:
            com.google.common.collect.ImmutableList r3 = r13.build()
            goto L_0x17b2
        L_0x1875:
            r2 = 0
            goto L_0x17af
        L_0x1878:
            r2 = 12
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1958
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r13 = r5.A05(r3)
        L_0x1887:
            com.facebook.graphservice.factory.GraphQLServiceFactory r4 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r3 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 1758109738(0x68caa02a, float:7.65498E24)
            X.11R r4 = r4.newTreeBuilder(r13, r3, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1955
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x18a4:
            r4.A0I(r2)
            X.2W4 r14 = new X.2W4
            r14.<init>()
            r2 = 6
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1952
            int r2 = r5.A00
            int r3 = r3 + r2
            int r3 = r5.A01(r3)
            java.nio.ByteBuffer r2 = r5.A01
            r14.A00 = r3
            r14.A01 = r2
        L_0x18c0:
            if (r14 == 0) goto L_0x1929
            com.facebook.graphservice.factory.GraphQLServiceFactory r15 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r13 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "Image"
            r2 = 1974585833(0x75b1c9e9, float:4.5074793E32)
            X.11R r13 = r15.newTreeBuilder(r3, r13, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r13 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r13
            r2 = 4
            int r15 = r14.A02(r2)
            if (r15 == 0) goto L_0x1950
            java.nio.ByteBuffer r3 = r14.A01
            int r2 = r14.A00
            int r15 = r15 + r2
            int r2 = r3.getInt(r15)
        L_0x18e3:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "width"
            r13.setInt(r2, r3)
            r2 = 6
            int r15 = r14.A02(r2)
            if (r15 == 0) goto L_0x194e
            java.nio.ByteBuffer r3 = r14.A01
            int r2 = r14.A00
            int r15 = r15 + r2
            int r2 = r3.getInt(r15)
        L_0x18fc:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "height"
            r13.setInt(r2, r3)
            r2 = 8
            int r3 = r14.A02(r2)
            if (r3 == 0) goto L_0x194c
            int r2 = r14.A00
            int r3 = r3 + r2
            java.lang.String r3 = r14.A05(r3)
        L_0x1914:
            java.lang.String r2 = "uri"
            r13.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 1974585833(0x75b1c9e9, float:4.5074793E32)
            com.facebook.graphservice.interfaces.Tree r3 = r13.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.String r2 = "image"
            r4.setTree(r2, r3)
        L_0x1929:
            r2 = 8
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x194a
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1938:
            java.lang.String r2 = "playable_url"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 1758109738(0x68caa02a, float:7.65498E24)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x179e
        L_0x194a:
            r3 = 0
            goto L_0x1938
        L_0x194c:
            r3 = 0
            goto L_0x1914
        L_0x194e:
            r2 = 0
            goto L_0x18fc
        L_0x1950:
            r2 = 0
            goto L_0x18e3
        L_0x1952:
            r14 = 0
            goto L_0x18c0
        L_0x1955:
            r2 = 0
            goto L_0x18a4
        L_0x1958:
            r13 = 0
            goto L_0x1887
        L_0x195b:
            r5 = 0
            goto L_0x179b
        L_0x195e:
            r3 = 0
            goto L_0x177c
        L_0x1961:
            r2 = 6
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x19e0
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r13 = r4.A05(r3)
        L_0x196f:
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r3 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = -671771564(0xffffffffd7f59454, float:-5.40034827E14)
            X.11R r5 = r5.newTreeBuilder(r13, r3, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r5 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r5
            X.2W2 r13 = new X.2W2
            r13.<init>()
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x19de
            int r2 = r4.A00
            int r3 = r3 + r2
            int r3 = r4.A01(r3)
            java.nio.ByteBuffer r2 = r4.A01
            r13.A00 = r3
            r13.A01 = r2
        L_0x1997:
            if (r13 == 0) goto L_0x19cf
            com.facebook.graphservice.factory.GraphQLServiceFactory r14 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 279(0x117, float:3.91E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = -367490715(0xffffffffea188965, float:-4.610139E25)
            X.11R r4 = r14.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r13.A02(r2)
            if (r3 == 0) goto L_0x19dc
            int r2 = r13.A00
            int r3 = r3 + r2
            java.lang.String r2 = r13.A05(r3)
        L_0x19bc:
            r4.A0I(r2)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -367490715(0xffffffffea188965, float:-4.610139E25)
            com.facebook.graphservice.interfaces.Tree r3 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            java.lang.String r2 = "instant_article"
            r5.setTree(r2, r3)
        L_0x19cf:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -671771564(0xffffffffd7f59454, float:-5.40034827E14)
            com.facebook.graphservice.interfaces.Tree r2 = r5.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1768
        L_0x19dc:
            r2 = 0
            goto L_0x19bc
        L_0x19de:
            r13 = 0
            goto L_0x1997
        L_0x19e0:
            r13 = 0
            goto L_0x196f
        L_0x19e2:
            r4 = 0
            goto L_0x1765
        L_0x19e5:
            com.facebook.graphservice.factory.GraphQLServiceFactory r13 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "TextWithEntities"
            r2 = 620886187(0x2501f8ab, float:1.1273218E-16)
            X.11R r4 = r13.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1a16
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1a04:
            java.lang.String r2 = "text"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 620886187(0x2501f8ab, float:1.1273218E-16)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1746
        L_0x1a16:
            r3 = 0
            goto L_0x1a04
        L_0x1a18:
            r5 = 0
            goto L_0x1743
        L_0x1a1b:
            com.facebook.graphservice.factory.GraphQLServiceFactory r13 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "TextWithEntities"
            r2 = -1247889134(0xffffffffb59eb912, float:-1.182578E-6)
            X.11R r4 = r13.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1a4c
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1a3a:
            java.lang.String r2 = "text"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1247889134(0xffffffffb59eb912, float:-1.182578E-6)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1724
        L_0x1a4c:
            r3 = 0
            goto L_0x1a3a
        L_0x1a4e:
            r5 = 0
            goto L_0x1721
        L_0x1a51:
            com.facebook.graphservice.factory.GraphQLServiceFactory r13 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "TextWithEntities"
            r2 = 35134020(0x2181a44, float:1.1174734E-37)
            X.11R r4 = r13.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1a82
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1a70:
            java.lang.String r2 = "text"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 35134020(0x2181a44, float:1.1174734E-37)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1703
        L_0x1a82:
            r3 = 0
            goto L_0x1a70
        L_0x1a84:
            r5 = 0
            goto L_0x1700
        L_0x1a87:
            r9 = 0
            goto L_0x16d0
        L_0x1a8a:
            com.google.common.collect.ImmutableList r3 = r20.build()
            goto L_0x0df8
        L_0x1a90:
            r12 = 0
            goto L_0x0df5
        L_0x1a93:
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 121(0x79, float:1.7E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = 1530798005(0x5b3e1fb5, float:5.3515108E16)
            X.11R r10 = r5.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r10 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r10
            r2 = 4
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1ead
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1ab6:
            java.lang.String r2 = "color"
            r10.setString(r2, r3)
            r2 = 6
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1eaa
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1ac9:
            r2 = 12
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r10.setString(r2, r3)
            r2 = 8
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1ea7
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1ae1:
            r2 = 414(0x19e, float:5.8E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r10.setString(r2, r3)
            r2 = 10
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1ea4
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1af9:
            r2 = 415(0x19f, float:5.82E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r10.setString(r2, r3)
            X.2Vi r5 = new X.2Vi
            r5.<init>()
            r2 = 12
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1ea1
            int r2 = r12.A00
            int r3 = r3 + r2
            int r3 = r12.A01(r3)
            java.nio.ByteBuffer r2 = r12.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x1b1c:
            if (r5 != 0) goto L_0x1e6e
            r2 = r1
        L_0x1b1f:
            java.lang.String r3 = "background_image"
            r10.setTree(r3, r2)
            r2 = 14
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1e6b
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1b33:
            java.lang.String r2 = "background_image_name"
            r10.setString(r2, r3)
            X.2Vj r5 = new X.2Vj
            r5.<init>()
            r2 = 16
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1e68
            int r2 = r12.A00
            int r3 = r3 + r2
            int r3 = r12.A01(r3)
            java.nio.ByteBuffer r2 = r12.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x1b52:
            if (r5 != 0) goto L_0x1dd4
            r2 = r1
        L_0x1b55:
            java.lang.String r3 = "font_object"
            r10.setTree(r3, r2)
            r2 = 18
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1dd1
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1b69:
            java.lang.String r2 = "font_style"
            r10.setString(r2, r3)
            r2 = 20
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1dce
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1b7d:
            r2 = 173(0xad, float:2.42E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r10.setString(r2, r3)
            X.2Vk r9 = new X.2Vk
            r9.<init>()
            r2 = 22
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1dcb
            int r2 = r12.A00
            int r3 = r3 + r2
            int r3 = r12.A01(r3)
            java.nio.ByteBuffer r2 = r12.A01
            r9.A00 = r3
            r9.A01 = r2
        L_0x1ba0:
            if (r9 != 0) goto L_0x1cbe
            r2 = r1
        L_0x1ba3:
            java.lang.String r3 = "inspirations_custom_font_object"
            r10.setTree(r3, r2)
            r2 = 24
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1cbb
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1bb7:
            java.lang.String r2 = "preset_id"
            r10.setString(r2, r3)
            X.2Vl r5 = new X.2Vl
            r5.<init>()
            r2 = 26
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1cb8
            int r2 = r12.A00
            int r3 = r3 + r2
            int r3 = r12.A01(r3)
            java.nio.ByteBuffer r2 = r12.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x1bd6:
            if (r5 != 0) goto L_0x1c85
            r2 = r1
        L_0x1bd9:
            java.lang.String r3 = "portrait_background_image"
            r10.setTree(r3, r2)
            X.2Vm r5 = new X.2Vm
            r5.<init>()
            r2 = 28
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1c82
            int r2 = r12.A00
            int r3 = r3 + r2
            int r3 = r12.A01(r3)
            java.nio.ByteBuffer r2 = r12.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x1bf8:
            if (r5 != 0) goto L_0x1c23
            r2 = r1
        L_0x1bfb:
            java.lang.String r3 = "portrait_keyframes_animation"
            r10.setTree(r3, r2)
            r2 = 30
            int r3 = r12.A02(r2)
            if (r3 == 0) goto L_0x1c21
            int r2 = r12.A00
            int r3 = r3 + r2
            java.lang.String r3 = r12.A05(r3)
        L_0x1c0f:
            java.lang.String r2 = "text_align"
            r10.setString(r2, r3)
            java.lang.Class<X.2Vn> r3 = X.AnonymousClass2Vn.class
            r2 = 1530798005(0x5b3e1fb5, float:5.3515108E16)
            com.facebook.graphservice.interfaces.Tree r3 = r10.getResult(r3, r2)
            X.2Vn r3 = (X.AnonymousClass2Vn) r3
            goto L_0x0de6
        L_0x1c21:
            r3 = 0
            goto L_0x1c0f
        L_0x1c23:
            com.facebook.graphservice.factory.GraphQLServiceFactory r6 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 243(0xf3, float:3.4E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = -1039116531(0xffffffffc210570d, float:-36.08501)
            X.11R r4 = r6.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1c80
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x1c46:
            r4.A0I(r2)
            r2 = 6
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1c7e
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1c57:
            java.lang.String r2 = "name"
            r4.setString(r2, r3)
            r2 = 8
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1c7c
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1c6b:
            java.lang.String r2 = "animation_uri"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -1039116531(0xffffffffc210570d, float:-36.08501)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1bfb
        L_0x1c7c:
            r3 = 0
            goto L_0x1c6b
        L_0x1c7e:
            r3 = 0
            goto L_0x1c57
        L_0x1c80:
            r2 = 0
            goto L_0x1c46
        L_0x1c82:
            r5 = 0
            goto L_0x1bf8
        L_0x1c85:
            com.facebook.graphservice.factory.GraphQLServiceFactory r6 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "Image"
            r2 = -758954775(0xffffffffd2c344e9, float:-4.19337372E11)
            X.11R r4 = r6.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1cb6
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1ca4:
            java.lang.String r2 = "uri"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -758954775(0xffffffffd2c344e9, float:-4.19337372E11)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1bd9
        L_0x1cb6:
            r3 = 0
            goto L_0x1ca4
        L_0x1cb8:
            r5 = 0
            goto L_0x1bd6
        L_0x1cbb:
            r3 = 0
            goto L_0x1bb7
        L_0x1cbe:
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 278(0x116, float:3.9E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = 1696781739(0x6522d5ab, float:4.8060303E22)
            X.11R r6 = r5.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r6 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r6
            r2 = 4
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1dc8
            int r2 = r9.A00
            int r3 = r3 + r2
            java.lang.String r2 = r9.A05(r3)
        L_0x1ce1:
            r6.A0I(r2)
            r2 = 6
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1dc5
            int r2 = r9.A00
            int r3 = r3 + r2
            java.lang.String r3 = r9.A05(r3)
        L_0x1cf2:
            java.lang.String r2 = "font_url"
            r6.setString(r2, r3)
            r2 = 8
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1dc2
            int r2 = r9.A00
            int r3 = r3 + r2
            java.lang.String r3 = r9.A05(r3)
        L_0x1d06:
            java.lang.String r2 = "font_name"
            r6.setString(r2, r3)
            r2 = 10
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1dbf
            int r2 = r9.A00
            int r3 = r3 + r2
            java.lang.String r3 = r9.A05(r3)
        L_0x1d1a:
            java.lang.String r2 = "font_icon_url"
            r6.setString(r2, r3)
            r2 = 12
            int r3 = r9.A02(r2)
            if (r3 == 0) goto L_0x1dbc
            int r2 = r9.A00
            int r3 = r3 + r2
            java.lang.String r3 = r9.A05(r3)
        L_0x1d2e:
            java.lang.String r2 = "font_postscript_name"
            r6.setString(r2, r3)
            r2 = 14
            int r4 = r9.A02(r2)
            if (r4 == 0) goto L_0x1dba
            java.nio.ByteBuffer r3 = r9.A01
            int r2 = r9.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x1d44:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "maximum_font_size"
            r6.setInt(r2, r3)
            r2 = 16
            int r4 = r9.A02(r2)
            if (r4 == 0) goto L_0x1db8
            java.nio.ByteBuffer r3 = r9.A01
            int r2 = r9.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x1d5e:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "minimum_font_size"
            r6.setInt(r2, r3)
            X.32p r2 = r9.A06()
            if (r2 == 0) goto L_0x1da9
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "TextWithEntities"
            r2 = 356273527(0x153c4d77, float:3.8027407E-26)
            X.11R r4 = r5.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            X.32p r5 = r9.A06()
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1db6
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1d90:
            java.lang.String r2 = "text"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 356273527(0x153c4d77, float:3.8027407E-26)
            com.facebook.graphservice.interfaces.Tree r3 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r2 = 145(0x91, float:2.03E-43)
            java.lang.String r2 = X.ECX.$const$string(r2)
            r6.setTree(r2, r3)
        L_0x1da9:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 1696781739(0x6522d5ab, float:4.8060303E22)
            com.facebook.graphservice.interfaces.Tree r2 = r6.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1ba3
        L_0x1db6:
            r3 = 0
            goto L_0x1d90
        L_0x1db8:
            r2 = 0
            goto L_0x1d5e
        L_0x1dba:
            r2 = 0
            goto L_0x1d44
        L_0x1dbc:
            r3 = 0
            goto L_0x1d2e
        L_0x1dbf:
            r3 = 0
            goto L_0x1d1a
        L_0x1dc2:
            r3 = 0
            goto L_0x1d06
        L_0x1dc5:
            r3 = 0
            goto L_0x1cf2
        L_0x1dc8:
            r2 = 0
            goto L_0x1ce1
        L_0x1dcb:
            r9 = 0
            goto L_0x1ba0
        L_0x1dce:
            r3 = 0
            goto L_0x1b7d
        L_0x1dd1:
            r3 = 0
            goto L_0x1b69
        L_0x1dd4:
            com.facebook.graphservice.factory.GraphQLServiceFactory r6 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 352(0x160, float:4.93E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r2)
            r2 = -967701493(0xffffffffc6520c0b, float:-13443.011)
            X.11R r4 = r6.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r6 = r5.A02(r2)
            if (r6 == 0) goto L_0x1e66
            java.nio.ByteBuffer r3 = r5.A01
            int r2 = r5.A00
            int r6 = r6 + r2
            int r2 = r3.getInt(r6)
        L_0x1df9:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = "default_font_size"
            r4.setInt(r2, r3)
            r2 = 6
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1e64
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1e10:
            java.lang.String r2 = "font_name"
            r4.setString(r2, r3)
            r2 = 8
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1e62
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1e24:
            java.lang.String r2 = "font_version"
            r4.setString(r2, r3)
            r2 = 10
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1e60
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1e38:
            java.lang.String r2 = "resource_name"
            r4.setString(r2, r3)
            r2 = 12
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1e5e
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1e4c:
            java.lang.String r2 = "url"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -967701493(0xffffffffc6520c0b, float:-13443.011)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1b55
        L_0x1e5e:
            r3 = 0
            goto L_0x1e4c
        L_0x1e60:
            r3 = 0
            goto L_0x1e38
        L_0x1e62:
            r3 = 0
            goto L_0x1e24
        L_0x1e64:
            r3 = 0
            goto L_0x1e10
        L_0x1e66:
            r2 = 0
            goto L_0x1df9
        L_0x1e68:
            r5 = 0
            goto L_0x1b52
        L_0x1e6b:
            r3 = 0
            goto L_0x1b33
        L_0x1e6e:
            com.facebook.graphservice.factory.GraphQLServiceFactory r6 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "Image"
            r2 = 1953763184(0x74740f70, float:7.7345797E31)
            X.11R r4 = r6.newTreeBuilder(r3, r4, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x1e9f
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r3 = r5.A05(r3)
        L_0x1e8d:
            java.lang.String r2 = "uri"
            r4.setString(r2, r3)
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 1953763184(0x74740f70, float:7.7345797E31)
            com.facebook.graphservice.interfaces.Tree r2 = r4.getResult(r3, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            goto L_0x1b1f
        L_0x1e9f:
            r3 = 0
            goto L_0x1e8d
        L_0x1ea1:
            r5 = 0
            goto L_0x1b1c
        L_0x1ea4:
            r3 = 0
            goto L_0x1af9
        L_0x1ea7:
            r3 = 0
            goto L_0x1ae1
        L_0x1eaa:
            r3 = 0
            goto L_0x1ac9
        L_0x1ead:
            r3 = 0
            goto L_0x1ab6
        L_0x1eb0:
            r12 = 0
            goto L_0x0de3
        L_0x1eb3:
            java.lang.String r2 = r12.A07()
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r2)
            X.2Uf r2 = com.facebook.ipc.stories.model.StoryBackgroundInfo.A00(r2)
            com.facebook.ipc.stories.model.StoryBackgroundInfo r4 = new com.facebook.ipc.stories.model.StoryBackgroundInfo
            r4.<init>(r2)
            goto L_0x1ef7
        L_0x1ec5:
            r4 = r1
            goto L_0x1ef7
        L_0x1ec7:
            com.google.common.collect.ImmutableList r2 = r6.build()
            X.2Uf r5 = com.facebook.ipc.stories.model.StoryBackgroundInfo.A00(r2)
            X.2Vf r9 = r12.A06()
            r2 = 4
            int r6 = r9.A02(r2)
            if (r6 == 0) goto L_0x231e
            java.nio.ByteBuffer r4 = r9.A01
            int r2 = r9.A00
            int r6 = r6 + r2
            byte r4 = r4.get(r6)
        L_0x1ee3:
            java.lang.String[] r2 = X.C47472Vh.A00
            r4 = r2[r4]
            com.facebook.graphql.enums.GraphQLPostGradientDirection r2 = com.facebook.graphql.enums.GraphQLPostGradientDirection.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            java.lang.Enum r2 = com.facebook.graphql.enums.EnumHelper.A00(r4, r2)
            com.facebook.graphql.enums.GraphQLPostGradientDirection r2 = (com.facebook.graphql.enums.GraphQLPostGradientDirection) r2
            r5.A00(r2)
            com.facebook.ipc.stories.model.StoryBackgroundInfo r4 = new com.facebook.ipc.stories.model.StoryBackgroundInfo
            r4.<init>(r5)
        L_0x1ef7:
            r2 = r19
            r2.A03 = r4
        L_0x1efb:
            X.2UR r2 = r0.A0B()
            if (r2 == 0) goto L_0x1f8c
            X.2UR r2 = r0.A0B()
            X.2Uh r2 = r2.A09()
            if (r2 == 0) goto L_0x1f34
            X.2UR r2 = r0.A0B()
            X.2Uh r9 = r2.A09()
            X.7WS r6 = new X.7WS
            r6.<init>()
            r2 = 10
            int r4 = r9.A02(r2)
            if (r4 == 0) goto L_0x231b
            int r2 = r9.A00
            int r4 = r4 + r2
            int r4 = r9.A01(r4)
            java.nio.ByteBuffer r2 = r9.A01
            r6.A00 = r4
            r6.A01 = r2
        L_0x1f2d:
            if (r6 != 0) goto L_0x20ef
            r4 = r1
        L_0x1f30:
            r2 = r19
            r2.A05 = r4
        L_0x1f34:
            X.2UR r2 = r0.A0B()
            X.2US r2 = r2.A07()
            if (r2 == 0) goto L_0x1f5c
            X.2UR r2 = r0.A0B()
            X.2US r2 = r2.A07()
            java.lang.String r2 = r2.A06()
            if (r2 == 0) goto L_0x1f5c
            X.2UR r2 = r0.A0B()
            X.2US r2 = r2.A07()
            java.lang.String r4 = r2.A06()
            r2 = r19
            r2.A0O = r4
        L_0x1f5c:
            X.2Ui r2 = r0.A0C()
            if (r2 == 0) goto L_0x1f8c
            X.2Ui r5 = r0.A0C()
            r2 = 4
            int r4 = r5.A02(r2)
            if (r4 == 0) goto L_0x20ec
            int r2 = r5.A00
            int r4 = r4 + r2
            java.lang.String r2 = r5.A05(r4)
        L_0x1f74:
            if (r2 == 0) goto L_0x1f8c
            X.2Ui r5 = r0.A0C()
            r2 = 4
            int r4 = r5.A02(r2)
            if (r4 == 0) goto L_0x20e9
            int r2 = r5.A00
            int r4 = r4 + r2
            java.lang.String r4 = r5.A05(r4)
        L_0x1f88:
            r2 = r19
            r2.A0Q = r4
        L_0x1f8c:
            r2 = 34
            int r6 = r0.A02(r2)
            r5 = 0
            if (r6 == 0) goto L_0x1fa1
            java.nio.ByteBuffer r4 = r0.A01
            int r2 = r0.A00
            int r6 = r6 + r2
            byte r2 = r4.get(r6)
            if (r2 == 0) goto L_0x1fa1
            r5 = 1
        L_0x1fa1:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r5)
            r2 = r19
            r2.A0H = r4
            java.lang.String r2 = "isReshareable"
            X.C28931fb.A06(r4, r2)
            if (r17 == 0) goto L_0x2062
            X.2UR r2 = r0.A0B()
            if (r2 == 0) goto L_0x1fe5
            X.2UR r2 = r0.A0B()
            java.lang.String r2 = r2.A0A()
            if (r2 == 0) goto L_0x1fe5
            X.2UR r2 = r0.A0B()
            java.lang.String r9 = r2.A0A()
            r6 = -1
            int r5 = r9.hashCode()
            r2 = 77090322(0x4984e12, float:3.5806725E-36)
            r4 = 1
            if (r5 == r2) goto L_0x2058
            r2 = 82650203(0x4ed245b, float:5.575182E-36)
            if (r5 != r2) goto L_0x1fe1
            java.lang.String r2 = "Video"
            boolean r2 = r9.equals(r2)
            if (r2 == 0) goto L_0x1fe1
            r6 = 1
        L_0x1fe1:
            if (r6 == 0) goto L_0x20e5
            if (r6 == r4) goto L_0x20e1
        L_0x1fe5:
            X.2Ul r2 = X.C47342Ul.TEXT
        L_0x1fe7:
            java.lang.String r4 = r2.name()
            r2 = r19
            r2.A0P = r4
            X.1T9 r2 = r0.A0A()
            if (r2 == 0) goto L_0x2056
            X.1T9 r6 = r0.A0A()
            r2 = 20
            int r5 = r6.A02(r2)
            if (r5 == 0) goto L_0x2056
            java.nio.ByteBuffer r4 = r6.A01
            int r2 = r6.A00
            int r5 = r5 + r2
            float r2 = r4.getFloat(r5)
        L_0x200a:
            java.lang.Float r4 = java.lang.Float.valueOf(r2)
            r2 = r19
            r2.A0K = r4
            r2 = 32
            int r2 = r0.A02(r2)
            if (r2 == 0) goto L_0x2054
            int r2 = r0.A04(r2)
        L_0x201e:
            if (r2 <= 0) goto L_0x232e
            com.google.common.collect.ImmutableList$Builder r5 = new com.google.common.collect.ImmutableList$Builder
            r5.<init>()
        L_0x2025:
            r2 = 32
            int r2 = r0.A02(r2)
            if (r2 == 0) goto L_0x2052
            int r2 = r0.A04(r2)
        L_0x2031:
            if (r3 >= r2) goto L_0x2321
            r2 = 32
            int r2 = r0.A02(r2)
            if (r2 == 0) goto L_0x2050
            java.nio.ByteBuffer r4 = r0.A01
            int r2 = r0.A03(r2)
            int r2 = r2 + r3
            byte r4 = r4.get(r2)
        L_0x2046:
            java.lang.String[] r2 = X.C82903wj.A00
            r2 = r2[r4]
            r5.add(r2)
            int r3 = r3 + 1
            goto L_0x2025
        L_0x2050:
            r4 = 0
            goto L_0x2046
        L_0x2052:
            r2 = 0
            goto L_0x2031
        L_0x2054:
            r2 = 0
            goto L_0x201e
        L_0x2056:
            r2 = 0
            goto L_0x200a
        L_0x2058:
            java.lang.String r2 = "Photo"
            boolean r2 = r9.equals(r2)
            if (r2 == 0) goto L_0x1fe1
            r6 = 0
            goto L_0x1fe1
        L_0x2062:
            X.2Uj r2 = r0.A09()
            if (r2 == 0) goto L_0x2082
            X.2Uj r5 = r0.A09()
            r2 = 4
            int r4 = r5.A02(r2)
            if (r4 == 0) goto L_0x2080
            int r2 = r5.A00
            int r4 = r4 + r2
            java.lang.String r2 = r5.A05(r4)
        L_0x207a:
            if (r2 == 0) goto L_0x2082
            X.2Ul r2 = X.C47342Ul.STICKER
            goto L_0x1fe7
        L_0x2080:
            r2 = 0
            goto L_0x207a
        L_0x2082:
            X.2Uk r2 = r0.A06()
            if (r2 == 0) goto L_0x1fe5
            X.2Uk r2 = r0.A06()
            java.lang.String r2 = r2.A09()
            if (r2 == 0) goto L_0x1fe5
            X.2Uk r2 = r0.A06()
            java.lang.String r9 = r2.A09()
            r6 = -1
            int r10 = r9.hashCode()
            r2 = -1142294092(0xffffffffbbe9f9b4, float:-0.0071403626)
            r5 = 2
            r4 = 1
            if (r10 == r2) goto L_0x20d3
            r2 = -1130404652(0xffffffffbc9f64d4, float:-0.019457258)
            if (r10 == r2) goto L_0x20c5
            r2 = -702610223(0xffffffffd61f04d1, float:-4.3710759E13)
            if (r10 != r2) goto L_0x20bd
            r2 = 35
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            boolean r2 = r9.equals(r2)
            if (r2 == 0) goto L_0x20bd
            r6 = 0
        L_0x20bd:
            if (r6 == 0) goto L_0x20e5
            if (r6 == r4) goto L_0x20e5
            if (r6 == r5) goto L_0x20e1
            goto L_0x1fe5
        L_0x20c5:
            r2 = 37
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            boolean r2 = r9.equals(r2)
            if (r2 == 0) goto L_0x20bd
            r6 = 2
            goto L_0x20bd
        L_0x20d3:
            r2 = 36
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            boolean r2 = r9.equals(r2)
            if (r2 == 0) goto L_0x20bd
            r6 = 1
            goto L_0x20bd
        L_0x20e1:
            X.2Ul r2 = X.C47342Ul.VIDEO
            goto L_0x1fe7
        L_0x20e5:
            X.2Ul r2 = X.C47342Ul.PHOTO
            goto L_0x1fe7
        L_0x20e9:
            r4 = 0
            goto L_0x1f88
        L_0x20ec:
            r2 = 0
            goto L_0x1f74
        L_0x20ef:
            com.google.common.collect.ImmutableList$Builder r10 = new com.google.common.collect.ImmutableList$Builder
            r10.<init>()
            r5 = 0
        L_0x20f5:
            r2 = 4
            int r2 = r9.A02(r2)
            if (r2 == 0) goto L_0x2128
            int r2 = r9.A04(r2)
        L_0x2100:
            if (r5 >= r2) goto L_0x212a
            r2 = 4
            int r2 = r9.A02(r2)
            if (r2 == 0) goto L_0x2126
            java.nio.ByteBuffer r4 = r9.A01
            int r2 = r9.A03(r2)
            int r2 = r2 + r5
            byte r4 = r4.get(r2)
        L_0x2114:
            java.lang.String[] r2 = X.C30277EtE.A00
            r4 = r2[r4]
            com.facebook.graphql.enums.GraphQLObjectionableContentCategory r2 = com.facebook.graphql.enums.GraphQLObjectionableContentCategory.A01
            java.lang.Enum r2 = com.facebook.graphql.enums.EnumHelper.A00(r4, r2)
            com.facebook.graphql.enums.GraphQLObjectionableContentCategory r2 = (com.facebook.graphql.enums.GraphQLObjectionableContentCategory) r2
            r10.add(r2)
            int r5 = r5 + 1
            goto L_0x20f5
        L_0x2126:
            r4 = 0
            goto L_0x2114
        L_0x2128:
            r2 = 0
            goto L_0x2100
        L_0x212a:
            r2 = 6
            int r5 = r9.A02(r2)
            if (r5 == 0) goto L_0x2318
            java.nio.ByteBuffer r4 = r9.A01
            int r2 = r9.A00
            int r5 = r5 + r2
            byte r4 = r4.get(r5)
        L_0x213a:
            java.lang.String[] r2 = X.Al0.A00
            r4 = r2[r4]
            com.facebook.graphql.enums.GraphQLObjectionableContentReportFPAction r2 = com.facebook.graphql.enums.GraphQLObjectionableContentReportFPAction.A01
            java.lang.Enum r5 = com.facebook.graphql.enums.EnumHelper.A00(r4, r2)
            com.facebook.graphql.enums.GraphQLObjectionableContentReportFPAction r5 = (com.facebook.graphql.enums.GraphQLObjectionableContentReportFPAction) r5
            com.facebook.graphservice.factory.GraphQLServiceFactory r12 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r11 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 314(0x13a, float:4.4E-43)
            java.lang.String r4 = X.AnonymousClass80H.$const$string(r2)
            r2 = -750386191(0xffffffffd34603f1, float:-8.5046965E11)
            X.11R r11 = r12.newTreeBuilder(r4, r11, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r11 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r11
            r2 = 4
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x2315
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x2169:
            java.lang.String r2 = "blur_title"
            r11.setString(r2, r4)
            r2 = 6
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x2312
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x217c:
            java.lang.String r2 = "blur_subtitle"
            r11.setString(r2, r4)
            r2 = 14
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x230f
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x2190:
            java.lang.String r2 = "cover_media_link"
            r11.setString(r2, r4)
            r2 = 18
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x230c
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x21a4:
            java.lang.String r2 = "edit_preferences_link"
            r11.setString(r2, r4)
            r2 = 24
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x2309
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x21b8:
            java.lang.String r2 = "feedback_flow_body"
            r11.setString(r2, r4)
            r2 = 26
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x2306
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x21cc:
            java.lang.String r2 = "feedback_flow_freeform_text_placeholder"
            r11.setString(r2, r4)
            r2 = 30
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x2303
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x21e0:
            java.lang.String r2 = "feedback_response_body"
            r11.setString(r2, r4)
            r2 = 32
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x2300
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x21f4:
            java.lang.String r2 = "feedback_response_on_warning_screen"
            r11.setString(r2, r4)
            r2 = 28
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x22fd
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x2208:
            java.lang.String r2 = "feedback_response_title"
            r11.setString(r2, r4)
            r2 = 12
            int r13 = r6.A02(r2)
            r12 = 0
            if (r13 == 0) goto L_0x2222
            java.nio.ByteBuffer r4 = r6.A01
            int r2 = r6.A00
            int r13 = r13 + r2
            byte r2 = r4.get(r13)
            if (r2 == 0) goto L_0x2222
            r12 = 1
        L_0x2222:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r12)
            java.lang.String r2 = "has_edit_preferences_link"
            r11.setBoolean(r2, r4)
            r2 = 20
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x22fa
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x223a:
            java.lang.String r2 = "learn_more_desc"
            r11.setString(r2, r4)
            r2 = 22
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x22f7
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x224e:
            java.lang.String r2 = "learn_more_uri"
            r11.setString(r2, r4)
            r2 = 10
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x22f4
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x2262:
            java.lang.String r2 = "report_mistake_link"
            r11.setString(r2, r4)
            r2 = 8
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x22f2
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x2276:
            java.lang.String r2 = "show_media_desc"
            r11.setString(r2, r4)
            r2 = 16
            int r4 = r6.A02(r2)
            if (r4 == 0) goto L_0x22f0
            int r2 = r6.A00
            int r4 = r4 + r2
            java.lang.String r4 = r6.A05(r4)
        L_0x228a:
            java.lang.String r2 = "uncover_media_link"
            r11.setString(r2, r4)
            com.facebook.graphservice.factory.GraphQLServiceFactory r12 = X.C05850aR.A02()
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r6 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            r2 = 313(0x139, float:4.39E-43)
            java.lang.String r4 = X.AnonymousClass80H.$const$string(r2)
            r2 = -1378521345(0xffffffffadd56eff, float:-2.4264589E-11)
            X.11R r6 = r12.newTreeBuilder(r4, r6, r2)
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r6 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r6
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r4 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = -750386191(0xffffffffd34603f1, float:-8.5046965E11)
            com.facebook.graphservice.interfaces.Tree r4 = r11.getResult(r4, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            java.lang.String r2 = "strings"
            r6.setTree(r2, r4)
            com.google.common.collect.ImmutableList r4 = r10.build()
            r2 = 46
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            r6.A00(r2, r4)
            java.lang.String r2 = "report_mistake_action"
            r6.A01(r2, r5)
            r2 = 8
            int r5 = r9.A02(r2)
            if (r5 == 0) goto L_0x22ed
            java.nio.ByteBuffer r4 = r9.A01
            int r2 = r9.A00
            int r5 = r5 + r2
            long r4 = r4.getLong(r5)
        L_0x22d7:
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            java.lang.String r2 = "timestamp"
            r6.setTime(r2, r4)
            java.lang.Class<X.4BB> r4 = X.AnonymousClass4BB.class
            r2 = -1378521345(0xffffffffadd56eff, float:-2.4264589E-11)
            com.facebook.graphservice.interfaces.Tree r4 = r6.getResult(r4, r2)
            X.4BB r4 = (X.AnonymousClass4BB) r4
            goto L_0x1f30
        L_0x22ed:
            r4 = 0
            goto L_0x22d7
        L_0x22f0:
            r4 = 0
            goto L_0x228a
        L_0x22f2:
            r4 = 0
            goto L_0x2276
        L_0x22f4:
            r4 = 0
            goto L_0x2262
        L_0x22f7:
            r4 = 0
            goto L_0x224e
        L_0x22fa:
            r4 = 0
            goto L_0x223a
        L_0x22fd:
            r4 = 0
            goto L_0x2208
        L_0x2300:
            r4 = 0
            goto L_0x21f4
        L_0x2303:
            r4 = 0
            goto L_0x21e0
        L_0x2306:
            r4 = 0
            goto L_0x21cc
        L_0x2309:
            r4 = 0
            goto L_0x21b8
        L_0x230c:
            r4 = 0
            goto L_0x21a4
        L_0x230f:
            r4 = 0
            goto L_0x2190
        L_0x2312:
            r4 = 0
            goto L_0x217c
        L_0x2315:
            r4 = 0
            goto L_0x2169
        L_0x2318:
            r4 = 0
            goto L_0x213a
        L_0x231b:
            r6 = 0
            goto L_0x1f2d
        L_0x231e:
            r4 = 0
            goto L_0x1ee3
        L_0x2321:
            com.google.common.collect.ImmutableList r3 = r5.build()
            r2 = r19
            r2.A07 = r3
            java.lang.String r2 = "reshareIntents"
            X.C28931fb.A06(r3, r2)
        L_0x232e:
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder
            r9.<init>()
            r6 = 0
        L_0x2334:
            r2 = 44
            int r2 = r0.A02(r2)
            if (r2 == 0) goto L_0x23b2
            int r2 = r0.A04(r2)
        L_0x2340:
            if (r6 >= r2) goto L_0x23b4
            X.30l r5 = new X.30l
            r5.<init>()
            r2 = 44
            int r2 = r0.A02(r2)
            if (r2 == 0) goto L_0x23b0
            int r3 = r0.A03(r2)
            int r2 = r6 << 2
            int r3 = r3 + r2
            int r3 = r0.A01(r3)
            java.nio.ByteBuffer r2 = r0.A01
            r5.A00 = r3
            r5.A01 = r2
        L_0x2360:
            if (r5 == 0) goto L_0x23a7
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x23ae
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r2 = r5.A05(r3)
        L_0x2370:
            if (r2 == 0) goto L_0x23a7
            r2 = 4
            int r3 = r5.A02(r2)
            if (r3 == 0) goto L_0x23ac
            int r2 = r5.A00
            int r3 = r3 + r2
            java.lang.String r10 = r5.A05(r3)
        L_0x2380:
            r2 = 6
            int r4 = r5.A02(r2)
            if (r4 == 0) goto L_0x23aa
            java.nio.ByteBuffer r3 = r5.A01
            int r2 = r5.A00
            int r4 = r4 + r2
            float r2 = r3.getFloat(r4)
        L_0x2390:
            double r2 = (double) r2
            X.30m r5 = new X.30m
            r5.<init>()
            r5.A01 = r10
            java.lang.String r4 = "name"
            X.C28931fb.A06(r10, r4)
            r5.A00 = r2
            com.facebook.messaging.model.montagemetadata.MontageXRaySmartFeature r2 = new com.facebook.messaging.model.montagemetadata.MontageXRaySmartFeature
            r2.<init>(r5)
            r9.add(r2)
        L_0x23a7:
            int r6 = r6 + 1
            goto L_0x2334
        L_0x23aa:
            r2 = 0
            goto L_0x2390
        L_0x23ac:
            r10 = 0
            goto L_0x2380
        L_0x23ae:
            r2 = 0
            goto L_0x2370
        L_0x23b0:
            r5 = 0
            goto L_0x2360
        L_0x23b2:
            r2 = 0
            goto L_0x2340
        L_0x23b4:
            com.google.common.collect.ImmutableList r3 = r9.build()
            r2 = r19
            r2.A06 = r3
            java.lang.String r2 = "montageXRaySmartFeature"
            X.C28931fb.A06(r3, r2)
            com.facebook.messaging.model.montagemetadata.MontageMetadata r3 = new com.facebook.messaging.model.montagemetadata.MontageMetadata
            r2 = r19
            r3.<init>(r2)
            goto L_0x0cf7
        L_0x23ca:
            r11 = 0
            goto L_0x0cf4
        L_0x23cd:
            X.0yl r6 = r7.A01
            int r4 = X.AnonymousClass1Y3.ADT
            X.0UN r3 = r6.A00
            r2 = 3
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.0xE r2 = (X.C16510xE) r2
            boolean r2 = r2.A0B()
            r5 = 1
            if (r2 == 0) goto L_0x2411
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r6.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r3, r2)
            X.1Yd r4 = (X.C25051Yd) r4
            r2 = 282501480515079(0x100ef00650607, double:1.39574276421791E-309)
            boolean r2 = r4.Aem(r2)
            if (r2 == 0) goto L_0x2411
        L_0x23f6:
            if (r5 == 0) goto L_0x2413
            r2 = 24
            int r3 = r0.A02(r2)
            if (r3 == 0) goto L_0x240f
            int r2 = r0.A00
            int r3 = r3 + r2
            java.lang.String r2 = r0.A05(r3)
        L_0x2407:
            boolean r2 = X.C32741mG.A01(r2)
            if (r2 == 0) goto L_0x2413
            goto L_0x0cd8
        L_0x240f:
            r2 = 0
            goto L_0x2407
        L_0x2411:
            r5 = 0
            goto L_0x23f6
        L_0x2413:
            X.0yl r2 = r7.A01
            int r4 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r2.A00
            r2 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1Yd r4 = (X.C25051Yd) r4
            r2 = 282501476451805(0x100ef002705dd, double:1.395742744142667E-309)
            boolean r2 = r4.Aem(r2)
            if (r2 == 0) goto L_0x2457
            X.0yl r2 = r7.A01
            int r4 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r2.A00
            r2 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1Yd r4 = (X.C25051Yd) r4
            r2 = 282501484578365(0x100ef00a3063d, double:1.39574278429321E-309)
            boolean r2 = r4.Aem(r2)
            if (r2 == 0) goto L_0x245b
            X.1uC r2 = X.AnonymousClass1uC.Video
            java.lang.String r3 = r2.toString()
            X.2UR r2 = r0.A0B()
            java.lang.String r2 = r2.A0A()
            boolean r2 = r3.equals(r2)
            if (r2 != 0) goto L_0x245b
        L_0x2457:
            r17 = 0
            goto L_0x0cda
        L_0x245b:
            X.0yl r2 = r7.A01
            int r4 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r2.A00
            r2 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1Yd r4 = (X.C25051Yd) r4
            r2 = 282501484447291(0x100ef00a1063b, double:1.395742783645617E-309)
            boolean r2 = r4.Aem(r2)
            if (r2 == 0) goto L_0x2492
            X.1uC r2 = X.AnonymousClass1uC.Video
            java.lang.String r3 = r2.toString()
            X.2UR r2 = r0.A0B()
            java.lang.String r2 = r2.A0A()
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x2492
            X.2UR r2 = r0.A0B()
            java.lang.String r2 = r2.A0B()
            if (r2 != 0) goto L_0x2492
            goto L_0x2457
        L_0x2492:
            X.0yl r2 = r7.A01
            int r4 = X.AnonymousClass1Y3.AOJ
            X.0UN r3 = r2.A00
            r2 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1Yd r4 = (X.C25051Yd) r4
            r2 = 282501484512828(0x100ef00a2063c, double:1.395742783969413E-309)
            boolean r2 = r4.Aem(r2)
            if (r2 == 0) goto L_0x0cd8
            X.1T9 r2 = r0.A0A()
            boolean r2 = r2.A0A()
            if (r2 == 0) goto L_0x0cd8
            goto L_0x2457
        L_0x24b5:
            com.google.common.collect.ImmutableList r9 = r9.build()
            X.2Uo r11 = r0.A08()
            com.google.common.collect.ImmutableList$Builder r10 = new com.google.common.collect.ImmutableList$Builder
            r10.<init>()
            if (r11 == 0) goto L_0x2577
            r6 = 0
        L_0x24c5:
            r2 = 8
            int r2 = r11.A02(r2)
            if (r2 == 0) goto L_0x2574
            int r2 = r11.A04(r2)
        L_0x24d1:
            if (r6 >= r2) goto L_0x2577
            X.84n r12 = new X.84n
            r12.<init>()
            r2 = 8
            int r2 = r11.A02(r2)
            if (r2 == 0) goto L_0x2571
            int r3 = r11.A03(r2)
            int r2 = r6 << 2
            int r3 = r3 + r2
            int r3 = r11.A01(r3)
            java.nio.ByteBuffer r2 = r11.A01
            r12.A00 = r3
            r12.A01 = r2
        L_0x24f1:
            X.AnonymousClass0qX.A00(r12)
            X.84n r12 = (X.C1748084n) r12
            X.84o r2 = r12.A06()
            if (r2 == 0) goto L_0x2567
            X.84o r2 = r12.A06()
            X.84p r2 = r2.A06()
            if (r2 == 0) goto L_0x2567
            X.8x4 r5 = new X.8x4
            r5.<init>()
            X.8x6 r3 = new X.8x6
            r3.<init>()
            X.8xA r4 = new X.8xA
            r4.<init>()
            X.84o r2 = r12.A06()
            X.84p r14 = r2.A06()
            r2 = 4
            int r13 = r14.A02(r2)
            if (r13 == 0) goto L_0x256f
            int r2 = r14.A00
            int r13 = r13 + r2
            java.lang.String r2 = r14.A05(r13)
        L_0x252b:
            r4.A00 = r2
            com.facebook.messaging.model.messages.montageattribution.Image r2 = new com.facebook.messaging.model.messages.montageattribution.Image
            r2.<init>(r4)
            r3.A00 = r2
            com.facebook.messaging.model.messages.montageattribution.EntityWithImage r2 = new com.facebook.messaging.model.messages.montageattribution.EntityWithImage
            r2.<init>(r3)
            r5.A02 = r2
            r2 = 6
            int r4 = r12.A02(r2)
            if (r4 == 0) goto L_0x256d
            java.nio.ByteBuffer r3 = r12.A01
            int r2 = r12.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x254b:
            r5.A00 = r2
            r2 = 4
            int r4 = r12.A02(r2)
            if (r4 == 0) goto L_0x256b
            java.nio.ByteBuffer r3 = r12.A01
            int r2 = r12.A00
            int r4 = r4 + r2
            int r2 = r3.getInt(r4)
        L_0x255d:
            r5.A01 = r2
            com.facebook.messaging.model.messages.montageattribution.ImageAtRange r2 = new com.facebook.messaging.model.messages.montageattribution.ImageAtRange
            r2.<init>(r5)
            r10.add(r2)
        L_0x2567:
            int r6 = r6 + 1
            goto L_0x24c5
        L_0x256b:
            r2 = 0
            goto L_0x255d
        L_0x256d:
            r2 = 0
            goto L_0x254b
        L_0x256f:
            r2 = 0
            goto L_0x252b
        L_0x2571:
            r12 = 0
            goto L_0x24f1
        L_0x2574:
            r2 = 0
            goto L_0x24d1
        L_0x2577:
            com.google.common.collect.ImmutableList r6 = r10.build()
            X.4wy r5 = new X.4wy
            r5.<init>()
            X.2Uo r4 = r0.A08()
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x259e
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x2592:
            r5.A02 = r2
            r5.A01 = r9
            r5.A00 = r6
            com.facebook.messaging.model.messages.montageattribution.MontageAttributionData r2 = new com.facebook.messaging.model.messages.montageattribution.MontageAttributionData
            r2.<init>(r5)
            goto L_0x25a1
        L_0x259e:
            r2 = 0
            goto L_0x2592
        L_0x25a0:
            r2 = r1
        L_0x25a1:
            r8.A0L = r2
            X.2Uj r2 = r0.A09()
            if (r2 == 0) goto L_0x25bd
            X.2Uj r4 = r0.A09()
            r2 = 4
            int r3 = r4.A02(r2)
            if (r3 == 0) goto L_0x25c0
            int r2 = r4.A00
            int r3 = r3 + r2
            java.lang.String r2 = r4.A05(r3)
        L_0x25bb:
            r8.A0x = r2
        L_0x25bd:
            if (r17 == 0) goto L_0x2894
            goto L_0x25c2
        L_0x25c0:
            r2 = 0
            goto L_0x25bb
        L_0x25c2:
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x2c90 }
            r4.<init>()     // Catch:{ Exception -> 0x2c90 }
            X.2UR r3 = r0.A0B()     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r2 = r0.A0D()     // Catch:{ Exception -> 0x2c90 }
            if (r2 == 0) goto L_0x2b13
            if (r3 == 0) goto L_0x2b13
            java.lang.String r2 = r3.A0A()     // Catch:{ Exception -> 0x2c90 }
            com.google.common.base.Preconditions.checkNotNull(r2)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r6 = r0.A0D()     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r2 = r3.A0C()     // Catch:{ Exception -> 0x2c90 }
            if (r2 != 0) goto L_0x25ed
            java.lang.String r3 = "graphql_media_id_null"
            java.lang.String r2 = "Graphql ent media id is null."
            X.C010708t.A0I(r3, r2)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2b0e
        L_0x25ed:
            X.0w6 r2 = new X.0w6     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r5 = r3.A0C()     // Catch:{ Exception -> 0x2c90 }
            r2.<init>(r5, r6)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r5 = r3.A0C()     // Catch:{ Exception -> 0x2c90 }
            r2.A06 = r5     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r5 = r3.A0A()     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r6 = com.google.common.base.Strings.nullToEmpty(r5)     // Catch:{ Exception -> 0x2c90 }
            X.1uC r5 = X.AnonymousClass1uC.Photo     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x2c90 }
            boolean r5 = r6.equals(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r5 == 0) goto L_0x27b0
            java.lang.String r5 = "image/jpeg"
            r2.A09 = r5     // Catch:{ Exception -> 0x2c90 }
            X.1TT r5 = new X.1TT     // Catch:{ Exception -> 0x2c90 }
            r5.<init>()     // Catch:{ Exception -> 0x2c90 }
            X.2Vo r10 = new X.2Vo     // Catch:{ Exception -> 0x2c90 }
            r10.<init>()     // Catch:{ Exception -> 0x2c90 }
            r6 = 12
            int r7 = r3.A02(r6)     // Catch:{ Exception -> 0x2c90 }
            if (r7 == 0) goto L_0x26d5
            int r6 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r7 = r7 + r6
            int r7 = r3.A01(r7)     // Catch:{ Exception -> 0x2c90 }
            java.nio.ByteBuffer r6 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            r10.A00 = r7     // Catch:{ Exception -> 0x2c90 }
            r10.A01 = r6     // Catch:{ Exception -> 0x2c90 }
        L_0x2633:
            X.C47412Ut.A00(r10)     // Catch:{ Exception -> 0x2c90 }
            X.2Vo r10 = (X.C47522Vo) r10     // Catch:{ Exception -> 0x2c90 }
            r6 = 6
            int r9 = r10.A02(r6)     // Catch:{ Exception -> 0x2c90 }
            if (r9 == 0) goto L_0x26d2
            java.nio.ByteBuffer r7 = r10.A01     // Catch:{ Exception -> 0x2c90 }
            int r6 = r10.A00     // Catch:{ Exception -> 0x2c90 }
            int r9 = r9 + r6
            int r6 = r7.getInt(r9)     // Catch:{ Exception -> 0x2c90 }
        L_0x2648:
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x2c90 }
            X.C47412Ut.A00(r6)     // Catch:{ Exception -> 0x2c90 }
            int r12 = r6.intValue()     // Catch:{ Exception -> 0x2c90 }
            r6 = 8
            int r9 = r10.A02(r6)     // Catch:{ Exception -> 0x2c90 }
            if (r9 == 0) goto L_0x26d0
            java.nio.ByteBuffer r7 = r10.A01     // Catch:{ Exception -> 0x2c90 }
            int r6 = r10.A00     // Catch:{ Exception -> 0x2c90 }
            int r9 = r9 + r6
            int r6 = r7.getInt(r9)     // Catch:{ Exception -> 0x2c90 }
        L_0x2664:
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x2c90 }
            X.C47412Ut.A00(r6)     // Catch:{ Exception -> 0x2c90 }
            int r13 = r6.intValue()     // Catch:{ Exception -> 0x2c90 }
            X.2Uv r7 = X.C47422Uv.A02     // Catch:{ Exception -> 0x2c90 }
            if (r10 == 0) goto L_0x26b6
            X.2Uw r9 = new X.2Uw     // Catch:{ Exception -> 0x2c90 }
            r9.<init>()     // Catch:{ Exception -> 0x2c90 }
            r1 = 8
            int r11 = r10.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r11 == 0) goto L_0x26ce
            java.nio.ByteBuffer r6 = r10.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r10.A00     // Catch:{ Exception -> 0x2c90 }
            int r11 = r11 + r1
            int r1 = r6.getInt(r11)     // Catch:{ Exception -> 0x2c90 }
        L_0x2689:
            r9.A00 = r1     // Catch:{ Exception -> 0x2c90 }
            r1 = 6
            int r11 = r10.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r11 == 0) goto L_0x26cc
            java.nio.ByteBuffer r6 = r10.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r10.A00     // Catch:{ Exception -> 0x2c90 }
            int r11 = r11 + r1
            int r1 = r6.getInt(r11)     // Catch:{ Exception -> 0x2c90 }
        L_0x269b:
            r9.A01 = r1     // Catch:{ Exception -> 0x2c90 }
            r1 = 4
            int r6 = r10.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x26ca
            int r1 = r10.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r1
            java.lang.String r1 = r10.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x26ab:
            java.lang.String r1 = X.AnonymousClass2UN.A00(r1)     // Catch:{ Exception -> 0x2c90 }
            r9.A02 = r1     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.ImageUrl r1 = new com.facebook.messaging.model.attachment.ImageUrl     // Catch:{ Exception -> 0x2c90 }
            r1.<init>(r9)     // Catch:{ Exception -> 0x2c90 }
        L_0x26b6:
            java.util.Map r6 = r5.A01     // Catch:{ Exception -> 0x2c90 }
            r6.put(r7, r1)     // Catch:{ Exception -> 0x2c90 }
            X.2Vp r6 = r3.A08()     // Catch:{ Exception -> 0x2c90 }
            X.C47412Ut.A00(r6)     // Catch:{ Exception -> 0x2c90 }
            X.2Vq r7 = r3.A06()     // Catch:{ Exception -> 0x2c90 }
            X.C47412Ut.A00(r7)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x26d8
        L_0x26ca:
            r1 = 0
            goto L_0x26ab
        L_0x26cc:
            r1 = 0
            goto L_0x269b
        L_0x26ce:
            r1 = 0
            goto L_0x2689
        L_0x26d0:
            r6 = 0
            goto L_0x2664
        L_0x26d2:
            r6 = 0
            goto L_0x2648
        L_0x26d5:
            r10 = 0
            goto L_0x2633
        L_0x26d8:
            if (r6 != 0) goto L_0x26dc
            r6 = 0
            goto L_0x271f
        L_0x26dc:
            X.2Uw r9 = new X.2Uw     // Catch:{ Exception -> 0x2c90 }
            r9.<init>()     // Catch:{ Exception -> 0x2c90 }
            r1 = 8
            int r10 = r6.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r10 == 0) goto L_0x272b
            java.nio.ByteBuffer r3 = r6.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r6.A00     // Catch:{ Exception -> 0x2c90 }
            int r10 = r10 + r1
            int r1 = r3.getInt(r10)     // Catch:{ Exception -> 0x2c90 }
        L_0x26f2:
            r9.A00 = r1     // Catch:{ Exception -> 0x2c90 }
            r1 = 6
            int r10 = r6.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r10 == 0) goto L_0x2729
            java.nio.ByteBuffer r3 = r6.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r6.A00     // Catch:{ Exception -> 0x2c90 }
            int r10 = r10 + r1
            int r1 = r3.getInt(r10)     // Catch:{ Exception -> 0x2c90 }
        L_0x2704:
            r9.A01 = r1     // Catch:{ Exception -> 0x2c90 }
            r1 = 4
            int r3 = r6.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r3 == 0) goto L_0x2727
            int r1 = r6.A00     // Catch:{ Exception -> 0x2c90 }
            int r3 = r3 + r1
            java.lang.String r1 = r6.A05(r3)     // Catch:{ Exception -> 0x2c90 }
        L_0x2714:
            java.lang.String r1 = X.AnonymousClass2UN.A00(r1)     // Catch:{ Exception -> 0x2c90 }
            r9.A02 = r1     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.ImageUrl r6 = new com.facebook.messaging.model.attachment.ImageUrl     // Catch:{ Exception -> 0x2c90 }
            r6.<init>(r9)     // Catch:{ Exception -> 0x2c90 }
        L_0x271f:
            X.C47412Ut.A00(r6)     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.ImageUrl r6 = (com.facebook.messaging.model.attachment.ImageUrl) r6     // Catch:{ Exception -> 0x2c90 }
            if (r7 != 0) goto L_0x272f
            goto L_0x272d
        L_0x2727:
            r1 = 0
            goto L_0x2714
        L_0x2729:
            r1 = 0
            goto L_0x2704
        L_0x272b:
            r1 = 0
            goto L_0x26f2
        L_0x272d:
            r7 = 0
            goto L_0x2772
        L_0x272f:
            X.2Uw r9 = new X.2Uw     // Catch:{ Exception -> 0x2c90 }
            r9.<init>()     // Catch:{ Exception -> 0x2c90 }
            r1 = 8
            int r10 = r7.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r10 == 0) goto L_0x27ae
            java.nio.ByteBuffer r3 = r7.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r7.A00     // Catch:{ Exception -> 0x2c90 }
            int r10 = r10 + r1
            int r1 = r3.getInt(r10)     // Catch:{ Exception -> 0x2c90 }
        L_0x2745:
            r9.A00 = r1     // Catch:{ Exception -> 0x2c90 }
            r1 = 6
            int r10 = r7.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r10 == 0) goto L_0x27ac
            java.nio.ByteBuffer r3 = r7.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r7.A00     // Catch:{ Exception -> 0x2c90 }
            int r10 = r10 + r1
            int r1 = r3.getInt(r10)     // Catch:{ Exception -> 0x2c90 }
        L_0x2757:
            r9.A01 = r1     // Catch:{ Exception -> 0x2c90 }
            r1 = 4
            int r3 = r7.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r3 == 0) goto L_0x27aa
            int r1 = r7.A00     // Catch:{ Exception -> 0x2c90 }
            int r3 = r3 + r1
            java.lang.String r1 = r7.A05(r3)     // Catch:{ Exception -> 0x2c90 }
        L_0x2767:
            java.lang.String r1 = X.AnonymousClass2UN.A00(r1)     // Catch:{ Exception -> 0x2c90 }
            r9.A02 = r1     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.ImageUrl r7 = new com.facebook.messaging.model.attachment.ImageUrl     // Catch:{ Exception -> 0x2c90 }
            r7.<init>(r9)     // Catch:{ Exception -> 0x2c90 }
        L_0x2772:
            X.C47412Ut.A00(r7)     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.ImageUrl r7 = (com.facebook.messaging.model.attachment.ImageUrl) r7     // Catch:{ Exception -> 0x2c90 }
            X.2Uv r3 = X.C47422Uv.SMALL_PREVIEW     // Catch:{ Exception -> 0x2c90 }
            java.util.Map r1 = r5.A01     // Catch:{ Exception -> 0x2c90 }
            r1.put(r3, r6)     // Catch:{ Exception -> 0x2c90 }
            X.2Uv r3 = X.C47422Uv.MEDIUM_PREVIEW     // Catch:{ Exception -> 0x2c90 }
            java.util.Map r1 = r5.A01     // Catch:{ Exception -> 0x2c90 }
            r1.put(r3, r6)     // Catch:{ Exception -> 0x2c90 }
            X.2Uv r3 = X.C47422Uv.LARGE_PREVIEW     // Catch:{ Exception -> 0x2c90 }
            java.util.Map r1 = r5.A01     // Catch:{ Exception -> 0x2c90 }
            r1.put(r3, r7)     // Catch:{ Exception -> 0x2c90 }
            X.2Uv r3 = X.C47422Uv.BLURRED_PREVIEW     // Catch:{ Exception -> 0x2c90 }
            java.util.Map r1 = r5.A01     // Catch:{ Exception -> 0x2c90 }
            r1.put(r3, r6)     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.AttachmentImageMap r14 = new com.facebook.messaging.model.attachment.AttachmentImageMap     // Catch:{ Exception -> 0x2c90 }
            r14.<init>(r5)     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.ImageData r11 = new com.facebook.messaging.model.attachment.ImageData     // Catch:{ Exception -> 0x2c90 }
            X.2Ur r16 = X.C47392Ur.NONQUICKCAM     // Catch:{ Exception -> 0x2c90 }
            r15 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ Exception -> 0x2c90 }
            r2.A03 = r11     // Catch:{ Exception -> 0x2c90 }
            goto L_0x286e
        L_0x27aa:
            r1 = 0
            goto L_0x2767
        L_0x27ac:
            r1 = 0
            goto L_0x2757
        L_0x27ae:
            r1 = 0
            goto L_0x2745
        L_0x27b0:
            X.1uC r5 = X.AnonymousClass1uC.Video     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x2c90 }
            boolean r5 = r6.equals(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r5 == 0) goto L_0x2883
            r1 = 342(0x156, float:4.79E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r1)     // Catch:{ Exception -> 0x2c90 }
            r2.A09 = r1     // Catch:{ Exception -> 0x2c90 }
            X.2Vp r9 = r3.A08()     // Catch:{ Exception -> 0x2c90 }
            X.C47412Ut.A00(r9)     // Catch:{ Exception -> 0x2c90 }
            X.2Vq r7 = r3.A06()     // Catch:{ Exception -> 0x2c90 }
            X.C47412Ut.A00(r7)     // Catch:{ Exception -> 0x2c90 }
            r1 = 24
            int r5 = r3.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r5 == 0) goto L_0x2880
            int r1 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r5 = r5 + r1
            java.lang.String r10 = r3.A05(r5)     // Catch:{ Exception -> 0x2c90 }
        L_0x27e1:
            X.C47412Ut.A00(r10)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ Exception -> 0x2c90 }
            r1 = 22
            int r6 = r3.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x287d
            java.nio.ByteBuffer r5 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r1
            int r11 = r5.getInt(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x27f7:
            r1 = 26
            int r6 = r3.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x287b
            java.nio.ByteBuffer r5 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r1
            int r12 = r5.getInt(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x2808:
            r1 = 28
            int r6 = r3.A02(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x2879
            java.nio.ByteBuffer r5 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            int r1 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r1
            int r13 = r5.getInt(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x2819:
            int r1 = r11 / 1000
            android.net.Uri r18 = android.net.Uri.parse(r10)     // Catch:{ Exception -> 0x2c90 }
            r5 = 4
            int r6 = r9.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x2877
            int r5 = r9.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r5 = r9.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x282d:
            android.net.Uri r19 = android.net.Uri.parse(r5)     // Catch:{ Exception -> 0x2c90 }
            r5 = 4
            int r6 = r7.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x2875
            int r5 = r7.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r5 = r7.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x283f:
            android.net.Uri r21 = android.net.Uri.parse(r5)     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.VideoData r11 = new com.facebook.messaging.model.attachment.VideoData     // Catch:{ Exception -> 0x2c90 }
            X.2VB r17 = X.AnonymousClass2VB.VIDEO_ATTACHMENT     // Catch:{ Exception -> 0x2c90 }
            r14 = 0
            r20 = 0
            r15 = r1
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ Exception -> 0x2c90 }
            r2.A04 = r11     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r1 = r3.A0B()     // Catch:{ Exception -> 0x2c90 }
            if (r1 == 0) goto L_0x286e
            java.util.Map r5 = r2.A0A     // Catch:{ Exception -> 0x2c90 }
            if (r5 != 0) goto L_0x285f
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ Exception -> 0x2c90 }
            r5.<init>()     // Catch:{ Exception -> 0x2c90 }
        L_0x285f:
            java.lang.String r1 = r3.A0B()     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r3 = X.AnonymousClass2UN.A00(r1)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r1 = "dash_manifest"
            r5.put(r1, r3)     // Catch:{ Exception -> 0x2c90 }
            r2.A0A = r5     // Catch:{ Exception -> 0x2c90 }
        L_0x286e:
            com.facebook.messaging.model.attachment.Attachment r1 = new com.facebook.messaging.model.attachment.Attachment     // Catch:{ Exception -> 0x2c90 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2b0e
        L_0x2875:
            r5 = 0
            goto L_0x283f
        L_0x2877:
            r5 = 0
            goto L_0x282d
        L_0x2879:
            r13 = 0
            goto L_0x2819
        L_0x287b:
            r12 = 0
            goto L_0x2808
        L_0x287d:
            r11 = 0
            goto L_0x27f7
        L_0x2880:
            r10 = 0
            goto L_0x27e1
        L_0x2883:
            java.lang.Object[] r5 = new java.lang.Object[]{r6}     // Catch:{ Exception -> 0x2c90 }
            r2 = 177(0xb1, float:2.48E-43)
            java.lang.String r3 = X.C99084oO.$const$string(r2)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r2 = "Failed to support EntMedia of type %s"
            X.C010708t.A0O(r3, r2, r5)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2b0e
        L_0x2894:
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x2c90 }
            r4.<init>()     // Catch:{ Exception -> 0x2c90 }
            X.2Uk r3 = r0.A06()     // Catch:{ Exception -> 0x2c90 }
            if (r3 == 0) goto L_0x2b13
            java.lang.String r2 = r3.A09()     // Catch:{ Exception -> 0x2c90 }
            com.google.common.base.Preconditions.checkNotNull(r2)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r9 = r0.A0D()     // Catch:{ Exception -> 0x2c90 }
            X.0w6 r2 = new X.0w6     // Catch:{ Exception -> 0x2c90 }
            r5 = 22
            int r6 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x291f
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r5 = r3.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x28bb:
            r2.<init>(r5, r9)     // Catch:{ Exception -> 0x2c90 }
            r5 = 6
            int r6 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x291d
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r5 = r3.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x28cc:
            r2.A06 = r5     // Catch:{ Exception -> 0x2c90 }
            r5 = 12
            int r6 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x291b
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r5 = r3.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x28dd:
            r2.A09 = r5     // Catch:{ Exception -> 0x2c90 }
            r5 = 8
            int r6 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x2919
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r5 = r3.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x28ee:
            r2.A07 = r5     // Catch:{ Exception -> 0x2c90 }
            r5 = 10
            int r9 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r9 == 0) goto L_0x2917
            java.nio.ByteBuffer r6 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r9 = r9 + r5
            int r5 = r6.getInt(r9)     // Catch:{ Exception -> 0x2c90 }
        L_0x2901:
            r2.A00 = r5     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r11 = r3.A09()     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r13 = com.google.common.base.Strings.nullToEmpty(r11)     // Catch:{ Exception -> 0x2c90 }
            int r5 = r13.hashCode()     // Catch:{ Exception -> 0x2c90 }
            r12 = 4
            r10 = 3
            r9 = 2
            r6 = 1
            switch(r5) {
                case -1149440977: goto L_0x2921;
                case -1142294092: goto L_0x292b;
                case -1130404652: goto L_0x2939;
                case -702610223: goto L_0x2947;
                case 794342915: goto L_0x2955;
                default: goto L_0x2916;
            }     // Catch:{ Exception -> 0x2c90 }
        L_0x2916:
            goto L_0x295e
        L_0x2917:
            r5 = 0
            goto L_0x2901
        L_0x2919:
            r5 = 0
            goto L_0x28ee
        L_0x291b:
            r5 = 0
            goto L_0x28dd
        L_0x291d:
            r5 = 0
            goto L_0x28cc
        L_0x291f:
            r5 = 0
            goto L_0x28bb
        L_0x2921:
            java.lang.String r5 = "MessageAudio"
            boolean r5 = r13.equals(r5)     // Catch:{ Exception -> 0x2c90 }
            r13 = 0
            if (r5 != 0) goto L_0x295f
            goto L_0x295e
        L_0x292b:
            r5 = 36
            java.lang.String r5 = X.C99084oO.$const$string(r5)     // Catch:{ Exception -> 0x2c90 }
            boolean r5 = r13.equals(r5)     // Catch:{ Exception -> 0x2c90 }
            r13 = 3
            if (r5 != 0) goto L_0x295f
            goto L_0x295e
        L_0x2939:
            r5 = 37
            java.lang.String r5 = X.C99084oO.$const$string(r5)     // Catch:{ Exception -> 0x2c90 }
            boolean r5 = r13.equals(r5)     // Catch:{ Exception -> 0x2c90 }
            r13 = 4
            if (r5 != 0) goto L_0x295f
            goto L_0x295e
        L_0x2947:
            r5 = 35
            java.lang.String r5 = X.C99084oO.$const$string(r5)     // Catch:{ Exception -> 0x2c90 }
            boolean r5 = r13.equals(r5)     // Catch:{ Exception -> 0x2c90 }
            r13 = 2
            if (r5 != 0) goto L_0x295f
            goto L_0x295e
        L_0x2955:
            java.lang.String r5 = "MessageFile"
            boolean r5 = r13.equals(r5)     // Catch:{ Exception -> 0x2c90 }
            r13 = 1
            if (r5 != 0) goto L_0x295f
        L_0x295e:
            r13 = -1
        L_0x295f:
            if (r13 == 0) goto L_0x2b09
            if (r13 == r6) goto L_0x2b09
            if (r13 == r9) goto L_0x2ad1
            if (r13 == r10) goto L_0x2abf
            if (r13 == r12) goto L_0x297a
            java.lang.String r2 = "Failed to support graphql attachment of type "
            java.lang.String r3 = X.AnonymousClass08S.A0J(r2, r11)     // Catch:{ Exception -> 0x2c90 }
            r2 = 177(0xb1, float:2.48E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)     // Catch:{ Exception -> 0x2c90 }
            X.C010708t.A0K(r2, r3)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2b0e
        L_0x297a:
            X.1Te r6 = new X.1Te     // Catch:{ Exception -> 0x2c90 }
            r6.<init>()     // Catch:{ Exception -> 0x2c90 }
            r5 = 42
            int r9 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r9 == 0) goto L_0x29d7
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r9 = r9 + r5
            int r9 = r3.A01(r9)     // Catch:{ Exception -> 0x2c90 }
            java.nio.ByteBuffer r5 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            r6.A00 = r9     // Catch:{ Exception -> 0x2c90 }
            r6.A01 = r5     // Catch:{ Exception -> 0x2c90 }
        L_0x2994:
            X.1Te r5 = new X.1Te     // Catch:{ Exception -> 0x2c90 }
            r5.<init>()     // Catch:{ Exception -> 0x2c90 }
            r9 = 44
            int r10 = r3.A02(r9)     // Catch:{ Exception -> 0x2c90 }
            if (r10 == 0) goto L_0x29d5
            int r9 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r10 = r10 + r9
            int r10 = r3.A01(r10)     // Catch:{ Exception -> 0x2c90 }
            java.nio.ByteBuffer r9 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            r5.A00 = r10     // Catch:{ Exception -> 0x2c90 }
            r5.A01 = r9     // Catch:{ Exception -> 0x2c90 }
        L_0x29ae:
            r9 = 32
            int r10 = r3.A02(r9)     // Catch:{ Exception -> 0x2c90 }
            if (r10 == 0) goto L_0x29d3
            int r9 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r10 = r10 + r9
            java.lang.String r9 = r3.A05(r10)     // Catch:{ Exception -> 0x2c90 }
        L_0x29bd:
            X.2Us r11 = r3.A08()     // Catch:{ Exception -> 0x2c90 }
            r10 = 40
            int r13 = r3.A02(r10)     // Catch:{ Exception -> 0x2c90 }
            if (r13 == 0) goto L_0x29d9
            java.nio.ByteBuffer r12 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            int r10 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r13 = r13 + r10
            int r10 = r12.getInt(r13)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x29da
        L_0x29d3:
            r9 = 0
            goto L_0x29bd
        L_0x29d5:
            r5 = 0
            goto L_0x29ae
        L_0x29d7:
            r6 = 0
            goto L_0x2994
        L_0x29d9:
            r10 = 0
        L_0x29da:
            int r10 = r10 * 1000
            if (r6 == 0) goto L_0x2ad7
            if (r9 == 0) goto L_0x2ad7
            if (r11 == 0) goto L_0x2ad7
            if (r10 <= 0) goto L_0x2ad7
            r7 = 36
            int r13 = r3.A02(r7)     // Catch:{ Exception -> 0x2c90 }
            if (r13 == 0) goto L_0x2a93
            java.nio.ByteBuffer r12 = r3.A01     // Catch:{ Exception -> 0x2c90 }
            int r7 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r13 = r13 + r7
            byte r12 = r12.get(r13)     // Catch:{ Exception -> 0x2c90 }
        L_0x29f5:
            java.lang.String[] r7 = X.AnonymousClass2V9.A00     // Catch:{ Exception -> 0x2c90 }
            r12 = r7[r12]     // Catch:{ Exception -> 0x2c90 }
            com.facebook.graphql.enums.GraphQLMessageVideoType r7 = com.facebook.graphql.enums.GraphQLMessageVideoType.A04     // Catch:{ Exception -> 0x2c90 }
            java.lang.Enum r12 = com.facebook.graphql.enums.EnumHelper.A00(r12, r7)     // Catch:{ Exception -> 0x2c90 }
            com.facebook.graphql.enums.GraphQLMessageVideoType r12 = (com.facebook.graphql.enums.GraphQLMessageVideoType) r12     // Catch:{ Exception -> 0x2c90 }
            com.facebook.graphql.enums.GraphQLMessageVideoType r7 = com.facebook.graphql.enums.GraphQLMessageVideoType.A01     // Catch:{ Exception -> 0x2c90 }
            if (r12 != r7) goto L_0x2a48
            X.2VB r17 = X.AnonymousClass2VB.VIDEO_ATTACHMENT     // Catch:{ Exception -> 0x2c90 }
        L_0x2a07:
            float r7 = r11.A06()     // Catch:{ Exception -> 0x2c90 }
            int r12 = (int) r7     // Catch:{ Exception -> 0x2c90 }
            float r7 = r11.A07()     // Catch:{ Exception -> 0x2c90 }
            int r13 = (int) r7     // Catch:{ Exception -> 0x2c90 }
            r14 = 0
            int r15 = r10 / 1000
            android.net.Uri r18 = android.net.Uri.parse(r9)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r7 = r6.A06()     // Catch:{ Exception -> 0x2c90 }
            android.net.Uri r19 = android.net.Uri.parse(r7)     // Catch:{ Exception -> 0x2c90 }
            if (r5 == 0) goto L_0x2a43
            java.lang.String r5 = r5.A06()     // Catch:{ Exception -> 0x2c90 }
        L_0x2a26:
            android.net.Uri r21 = android.net.Uri.parse(r5)     // Catch:{ Exception -> 0x2c90 }
            com.facebook.messaging.model.attachment.VideoData r11 = new com.facebook.messaging.model.attachment.VideoData     // Catch:{ Exception -> 0x2c90 }
            r20 = 0
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ Exception -> 0x2c90 }
            r2.A04 = r11     // Catch:{ Exception -> 0x2c90 }
            r5 = 34
            int r6 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x2a96
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r5 = r3.A05(r6)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2a97
        L_0x2a43:
            java.lang.String r5 = r6.A06()     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2a26
        L_0x2a48:
            com.facebook.graphql.enums.GraphQLMessageVideoType r7 = com.facebook.graphql.enums.GraphQLMessageVideoType.A03     // Catch:{ Exception -> 0x2c90 }
            if (r12 != r7) goto L_0x2a4f
            X.2VB r17 = X.AnonymousClass2VB.QUICKCAM     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2a07
        L_0x2a4f:
            com.facebook.graphql.enums.GraphQLMessageVideoType r7 = com.facebook.graphql.enums.GraphQLMessageVideoType.A02     // Catch:{ Exception -> 0x2c90 }
            if (r12 != r7) goto L_0x2a56
            X.2VB r17 = X.AnonymousClass2VB.VIDEO_STICKER     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2a07
        L_0x2a56:
            com.facebook.graphql.enums.GraphQLMessageVideoType r7 = com.facebook.graphql.enums.GraphQLMessageVideoType.A05     // Catch:{ Exception -> 0x2c90 }
            if (r12 != r7) goto L_0x2a5d
            X.2VB r17 = X.AnonymousClass2VB.VIDEO_MAIL     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2a07
        L_0x2a5d:
            r7 = 292(0x124, float:4.09E-43)
            java.lang.String r13 = X.C99084oO.$const$string(r7)     // Catch:{ Exception -> 0x2c90 }
            if (r12 != 0) goto L_0x2a85
            r7 = 6
            int r12 = r3.A02(r7)     // Catch:{ Exception -> 0x2c90 }
            if (r12 == 0) goto L_0x2a83
            int r7 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r12 = r12 + r7
            java.lang.String r7 = r3.A05(r12)     // Catch:{ Exception -> 0x2c90 }
        L_0x2a73:
            java.lang.String r12 = X.AnonymousClass08S.A0J(r13, r7)     // Catch:{ Exception -> 0x2c90 }
            r7 = 502(0x1f6, float:7.03E-43)
            java.lang.String r7 = X.C99084oO.$const$string(r7)     // Catch:{ Exception -> 0x2c90 }
            X.C010708t.A0K(r7, r12)     // Catch:{ Exception -> 0x2c90 }
        L_0x2a80:
            X.2VB r17 = X.AnonymousClass2VB.VIDEO_ATTACHMENT     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2a07
        L_0x2a83:
            r7 = 0
            goto L_0x2a73
        L_0x2a85:
            java.lang.String r7 = r12.toString()     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r12 = X.AnonymousClass08S.A0J(r13, r7)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r7 = "graphql_video_type_null"
            X.C010708t.A0K(r7, r12)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2a80
        L_0x2a93:
            r12 = 0
            goto L_0x29f5
        L_0x2a96:
            r5 = 0
        L_0x2a97:
            if (r5 == 0) goto L_0x2b09
            java.util.Map r7 = r2.A0A     // Catch:{ Exception -> 0x2c90 }
            if (r7 != 0) goto L_0x2aa2
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ Exception -> 0x2c90 }
            r7.<init>()     // Catch:{ Exception -> 0x2c90 }
        L_0x2aa2:
            r5 = 34
            int r6 = r3.A02(r5)     // Catch:{ Exception -> 0x2c90 }
            if (r6 == 0) goto L_0x2abd
            int r5 = r3.A00     // Catch:{ Exception -> 0x2c90 }
            int r6 = r6 + r5
            java.lang.String r3 = r3.A05(r6)     // Catch:{ Exception -> 0x2c90 }
        L_0x2ab1:
            java.lang.String r5 = X.AnonymousClass2UN.A00(r3)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r3 = "dash_manifest"
            r7.put(r3, r5)     // Catch:{ Exception -> 0x2c90 }
            r2.A0A = r7     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2b09
        L_0x2abd:
            r3 = 0
            goto L_0x2ab1
        L_0x2abf:
            X.2Up r1 = X.C47382Up.A02     // Catch:{ Exception -> 0x2c90 }
            A04(r2, r3, r1)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r1 = r2.A09     // Catch:{ Exception -> 0x2c90 }
            boolean r1 = X.C06850cB.A0B(r1)     // Catch:{ Exception -> 0x2c90 }
            if (r1 == 0) goto L_0x2b09
            java.lang.String r1 = "image/jpeg"
            r2.A09 = r1     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2b09
        L_0x2ad1:
            X.2Up r1 = X.C47382Up.ANIMATED_IMAGE     // Catch:{ Exception -> 0x2c90 }
            A04(r2, r3, r1)     // Catch:{ Exception -> 0x2c90 }
            goto L_0x2b09
        L_0x2ad7:
            X.09P r7 = r7.A00     // Catch:{ Exception -> 0x2c90 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x2c90 }
            r3.<init>()     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r1 = "Got an incomplete video attachment model. streamingImageThumbnail="
            r3.append(r1)     // Catch:{ Exception -> 0x2c90 }
            r3.append(r6)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r1 = ", attachmentVideoUrl="
            r3.append(r1)     // Catch:{ Exception -> 0x2c90 }
            r3.append(r9)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r1 = ", originalDimensions="
            r3.append(r1)     // Catch:{ Exception -> 0x2c90 }
            r3.append(r11)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r1 = ", playableDurationInMs="
            r3.append(r1)     // Catch:{ Exception -> 0x2c90 }
            r3.append(r10)     // Catch:{ Exception -> 0x2c90 }
            java.lang.String r5 = r3.toString()     // Catch:{ Exception -> 0x2c90 }
            r3 = 20000(0x4e20, float:2.8026E-41)
            java.lang.String r1 = "graphql_video_incomplete_model"
            r7.CGT(r1, r5, r3)     // Catch:{ Exception -> 0x2c90 }
        L_0x2b09:
            com.facebook.messaging.model.attachment.Attachment r1 = new com.facebook.messaging.model.attachment.Attachment     // Catch:{ Exception -> 0x2c90 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x2c90 }
        L_0x2b0e:
            if (r1 == 0) goto L_0x2b13
            r4.add(r1)     // Catch:{ Exception -> 0x2c90 }
        L_0x2b13:
            r8.A08(r4)     // Catch:{ Exception -> 0x2c90 }
            X.2V0 r9 = new X.2V0
            r9.<init>()
            r1 = 46
            int r2 = r0.A02(r1)
            if (r2 == 0) goto L_0x2c6e
            int r1 = r0.A00
            int r2 = r2 + r1
            int r2 = r0.A01(r2)
            java.nio.ByteBuffer r1 = r0.A01
            r9.A00 = r2
            r9.A01 = r1
        L_0x2b30:
            com.google.common.collect.HashMultimap r7 = new com.google.common.collect.HashMultimap
            r7.<init>()
            if (r9 == 0) goto L_0x2c71
            r6 = 0
        L_0x2b38:
            r1 = 4
            int r1 = r9.A02(r1)
            if (r1 == 0) goto L_0x2c6b
            int r1 = r9.A04(r1)
        L_0x2b43:
            if (r6 >= r1) goto L_0x2c71
            X.367 r3 = new X.367
            r3.<init>()
            r1 = 4
            int r1 = r9.A02(r1)
            if (r1 == 0) goto L_0x2c68
            int r2 = r9.A03(r1)
            int r1 = r6 << 2
            int r2 = r2 + r1
            int r2 = r9.A01(r2)
            java.nio.ByteBuffer r1 = r9.A01
            r3.A00 = r2
            r3.A01 = r1
        L_0x2b62:
            X.368 r4 = new X.368
            r4.<init>()
            r1 = 4
            int r2 = r3.A02(r1)
            if (r2 == 0) goto L_0x2c65
            int r1 = r3.A00
            int r2 = r2 + r1
            int r2 = r3.A01(r2)
            java.nio.ByteBuffer r1 = r3.A01
            r4.A00 = r2
            r4.A01 = r1
        L_0x2b7b:
            X.1tz r3 = new X.1tz
            r3.<init>()
            r1 = 4
            int r2 = r4.A02(r1)
            if (r2 == 0) goto L_0x2c62
            int r1 = r4.A00
            int r2 = r2 + r1
            int r2 = r4.A01(r2)
            java.nio.ByteBuffer r1 = r4.A01
            r3.A00 = r2
            r3.A01 = r1
        L_0x2b94:
            X.369 r5 = new X.369
            r5.<init>()
            r1 = 6
            int r2 = r4.A02(r1)
            if (r2 == 0) goto L_0x2c5f
            int r1 = r4.A00
            int r2 = r2 + r1
            int r2 = r4.A01(r2)
            java.nio.ByteBuffer r1 = r4.A01
            r5.A00 = r2
            r5.A01 = r1
        L_0x2bad:
            if (r3 == 0) goto L_0x2c5b
            if (r5 == 0) goto L_0x2c5b
            r1 = 4
            int r1 = r5.A02(r1)
            if (r1 == 0) goto L_0x2c58
            int r1 = r5.A04(r1)
        L_0x2bbc:
            if (r1 <= 0) goto L_0x2c5b
            java.lang.String r1 = r3.A06()
            com.facebook.user.model.UserKey r4 = com.facebook.user.model.UserKey.A01(r1)
            r3 = 0
        L_0x2bc7:
            r1 = 4
            int r1 = r5.A02(r1)
            if (r1 == 0) goto L_0x2c55
            int r1 = r5.A04(r1)
        L_0x2bd2:
            if (r3 >= r1) goto L_0x2c5b
            X.36A r10 = new X.36A
            r10.<init>()
            r1 = 4
            int r1 = r5.A02(r1)
            if (r1 == 0) goto L_0x2c53
            int r2 = r5.A03(r1)
            int r1 = r3 << 2
            int r2 = r2 + r1
            int r2 = r5.A01(r2)
            java.nio.ByteBuffer r1 = r5.A01
            r10.A00 = r2
            r10.A01 = r1
        L_0x2bf1:
            X.36B r11 = new X.36B
            r11.<init>()
            r1 = 4
            int r2 = r10.A02(r1)
            if (r2 == 0) goto L_0x2c51
            int r1 = r10.A00
            int r2 = r2 + r1
            int r2 = r10.A01(r2)
            java.nio.ByteBuffer r1 = r10.A01
            r11.A00 = r2
            r11.A01 = r1
        L_0x2c0a:
            com.facebook.messaging.model.messages.MontageMessageReaction r12 = new com.facebook.messaging.model.messages.MontageMessageReaction
            r1 = 4
            int r2 = r11.A02(r1)
            if (r2 == 0) goto L_0x2c4f
            int r1 = r11.A00
            int r2 = r2 + r1
            java.lang.String r13 = r11.A05(r2)
        L_0x2c1a:
            r1 = 6
            int r10 = r11.A02(r1)
            if (r10 == 0) goto L_0x2c4d
            java.nio.ByteBuffer r2 = r11.A01
            int r1 = r11.A00
            int r10 = r10 + r1
            int r1 = r2.getInt(r10)
        L_0x2c2a:
            long r14 = (long) r1
            r1 = 8
            int r10 = r11.A02(r1)
            if (r10 == 0) goto L_0x2c4a
            java.nio.ByteBuffer r2 = r11.A01
            int r1 = r11.A00
            int r10 = r10 + r1
            long r1 = r2.getLong(r10)
        L_0x2c3c:
            r16 = 1000(0x3e8, double:4.94E-321)
            long r16 = r16 * r1
            r12.<init>(r13, r14, r16)
            r7.Byx(r4, r12)
            int r3 = r3 + 1
            goto L_0x2bc7
        L_0x2c4a:
            r1 = 0
            goto L_0x2c3c
        L_0x2c4d:
            r1 = 0
            goto L_0x2c2a
        L_0x2c4f:
            r13 = 0
            goto L_0x2c1a
        L_0x2c51:
            r11 = 0
            goto L_0x2c0a
        L_0x2c53:
            r10 = 0
            goto L_0x2bf1
        L_0x2c55:
            r1 = 0
            goto L_0x2bd2
        L_0x2c58:
            r1 = 0
            goto L_0x2bbc
        L_0x2c5b:
            int r6 = r6 + 1
            goto L_0x2b38
        L_0x2c5f:
            r5 = 0
            goto L_0x2bad
        L_0x2c62:
            r3 = 0
            goto L_0x2b94
        L_0x2c65:
            r4 = 0
            goto L_0x2b7b
        L_0x2c68:
            r3 = 0
            goto L_0x2b62
        L_0x2c6b:
            r1 = 0
            goto L_0x2b43
        L_0x2c6e:
            r9 = 0
            goto L_0x2b30
        L_0x2c71:
            r8.A05(r7)
            r1 = 24
            int r2 = r0.A02(r1)
            if (r2 == 0) goto L_0x2c8e
            int r1 = r0.A00
            int r2 = r2 + r1
            java.lang.String r0 = r0.A05(r2)
        L_0x2c83:
            java.lang.String r0 = X.AnonymousClass2UN.A00(r0)
            r8.A0r = r0
            com.facebook.messaging.model.messages.Message r0 = r8.A00()
            return r0
        L_0x2c8e:
            r0 = 0
            goto L_0x2c83
        L_0x2c90:
            r2 = move-exception
            java.lang.String r1 = "com.facebook.messaging.montage.omnistore.converter.MontageMessageFBConverter"
            java.lang.String r0 = "Failed to convert attachment"
            X.C010708t.A0N(r1, r0, r2)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.converter.MontageMessageFBConverter.A05(com.facebook.messaging.model.threadkey.ThreadKey, X.1tx):com.facebook.messaging.model.messages.Message");
    }
}
