package com.facebook.messaging.montage.inboxunit.activenow;

import X.AnonymousClass08S;
import X.AnonymousClass1H4;
import X.C27161ck;
import X.C417826y;
import X.C73963h3;
import X.C74553i4;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem;
import com.facebook.messaging.inbox2.activenow.loader.Entity;
import com.facebook.messaging.inbox2.activenow.model.GroupPresenceInfo;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;

public final class InboxUnitMontageActiveNowItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new C73963h3();
    public final RankingLoggingItem A00;
    public final Entity A01;
    public final UnifiedPresenceViewLoggerItem A02;
    public final AnonymousClass1H4 A03;
    public final String A04;
    public final boolean A05;

    public static long A01(Entity entity) {
        GroupPresenceInfo groupPresenceInfo;
        if (entity.A00 == C74553i4.GROUP && (groupPresenceInfo = entity.A01) != null) {
            return groupPresenceInfo.A00.A0S.A03;
        }
        User user = entity.A02;
        if (user != null) {
            return Long.parseLong(user.A0j);
        }
        return 0;
    }

    public String A0G() {
        Preconditions.checkNotNull(this.A01);
        return AnonymousClass08S.A0O(this.A02.A0U(), ":", A01(this.A01));
    }

    public ThreadSummary A0I() {
        GroupPresenceInfo groupPresenceInfo;
        Preconditions.checkNotNull(this.A01);
        Entity entity = this.A01;
        if (entity.A00 != C74553i4.GROUP || (groupPresenceInfo = entity.A01) == null) {
            return null;
        }
        return groupPresenceInfo.A00;
    }

    public User A0J() {
        User user;
        Preconditions.checkNotNull(this.A01);
        Entity entity = this.A01;
        if (entity.A00 != C74553i4.USER || (user = entity.A02) == null) {
            return null;
        }
        return user;
    }

    public String A0L() {
        ThreadKey threadKey;
        Preconditions.checkNotNull(this.A01);
        if (this.A01.A00 == C74553i4.USER) {
            return String.valueOf(A0K());
        }
        ThreadSummary A0I = A0I();
        if (A0I != null) {
            threadKey = A0I.A0S;
        } else {
            threadKey = null;
        }
        return String.valueOf(threadKey);
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        String A0L = A0L();
        if (A0L != null) {
            if (this.A01.A00 == C74553i4.USER) {
                str = ", user = ";
            } else {
                str = ", group = ";
            }
            sb.append(str);
            sb.append(A0L);
        }
        sb.append("]");
        return sb.toString();
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeParcelable(this.A01, i);
        C417826y.A0V(parcel, this.A05);
        parcel.writeValue(this.A00);
        parcel.writeString(this.A04);
        parcel.writeValue(this.A02);
    }

    public UserKey A0K() {
        User A0J = A0J();
        if (A0J != null) {
            return A0J.A0Q;
        }
        return null;
    }

    public InboxUnitMontageActiveNowItem(C27161ck r1, Entity entity, AnonymousClass1H4 r3, boolean z, RankingLoggingItem rankingLoggingItem, String str, UnifiedPresenceViewLoggerItem unifiedPresenceViewLoggerItem) {
        super(r1);
        this.A01 = entity;
        this.A03 = r3;
        this.A05 = z;
        this.A00 = rankingLoggingItem;
        this.A04 = str;
        this.A02 = unifiedPresenceViewLoggerItem;
    }

    public InboxUnitMontageActiveNowItem(Parcel parcel) {
        super(parcel);
        this.A01 = (Entity) C417826y.A00(parcel, Entity.class);
        this.A05 = C417826y.A0W(parcel);
        this.A03 = null;
        this.A00 = (RankingLoggingItem) C417826y.A00(parcel, RankingLoggingItem.class);
        this.A04 = parcel.readString();
        this.A02 = (UnifiedPresenceViewLoggerItem) C417826y.A00(parcel, UnifiedPresenceViewLoggerItem.class);
    }
}
