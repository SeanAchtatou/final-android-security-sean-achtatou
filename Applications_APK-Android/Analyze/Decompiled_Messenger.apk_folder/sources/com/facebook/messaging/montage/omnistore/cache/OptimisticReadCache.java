package com.facebook.messaging.montage.omnistore.cache;

import X.AnonymousClass1XY;
import X.C05540Zi;
import X.C189216c;
import X.C21321Gd;
import X.C24971Xv;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.Message;
import com.google.common.collect.ImmutableList;
import java.util.Collections;
import java.util.Set;

@UserScoped
public final class OptimisticReadCache {
    private static C05540Zi A03;
    public final C189216c A00;
    public final MontageCache A01;
    public final Set A02 = Collections.synchronizedSet(new C21321Gd());

    public static final OptimisticReadCache A00(AnonymousClass1XY r4) {
        OptimisticReadCache optimisticReadCache;
        synchronized (OptimisticReadCache.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new OptimisticReadCache((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                optimisticReadCache = (OptimisticReadCache) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return optimisticReadCache;
    }

    private OptimisticReadCache(AnonymousClass1XY r2) {
        this.A01 = MontageCache.A01(r2);
        this.A00 = C189216c.A02(r2);
    }

    public void A01(ImmutableList immutableList) {
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            this.A02.add(((Message) it.next()).A0q);
        }
        this.A01.A0B(immutableList);
        this.A00.A0N("com.facebook.messaging.montage.omnistore.cache.OptimisticReadCache");
    }
}
