package com.facebook.messaging.montage.omnistore.storedprocedures;

import X.AnonymousClass07A;
import X.AnonymousClass08S;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2WT;
import X.AnonymousClass41J;
import X.AnonymousClass52P;
import X.C010708t;
import X.C04310Tq;
import X.C05350Yp;
import X.C06850cB;
import X.C1053452a;
import X.C24971Xv;
import X.C33451nb;
import X.C99084oO;
import com.facebook.acra.LogCatCollector;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.SettableFuture;
import io.card.payment.BuildConfig;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.UUID;
import java.util.concurrent.Executor;
import javax.inject.Singleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
public final class MontageMarkReadStoredProcedureComponent implements OmnistoreStoredProcedureComponent {
    public static final Charset A05 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    private static volatile MontageMarkReadStoredProcedureComponent A06;
    public AnonymousClass0UN A00;
    public AnonymousClass2WT A01;
    public SettableFuture A02 = null;
    public Long A03;
    public final C04310Tq A04;

    public void onSenderInvalidated() {
        this.A01 = null;
    }

    public int provideStoredProcedureId() {
        return AnonymousClass1Y3.A3I;
    }

    public static final MontageMarkReadStoredProcedureComponent A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (MontageMarkReadStoredProcedureComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new MontageMarkReadStoredProcedureComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public void A01(ImmutableList immutableList) {
        String str;
        SettableFuture settableFuture;
        if (this.A01 == null) {
            synchronized (this) {
                if (this.A02 == null) {
                    this.A02 = SettableFuture.create();
                    AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, this.A00), new C1053452a(this), 1423593585);
                }
                settableFuture = this.A02;
            }
            C05350Yp.A07(settableFuture, new AnonymousClass41J(this, immutableList));
            return;
        }
        String str2 = BuildConfig.FLAVOR;
        try {
            JSONArray jSONArray = new JSONArray();
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                jSONArray.put(Long.parseLong((String) it.next()));
            }
            JSONObject put = new JSONObject().put("client_mutation_id", UUID.randomUUID().toString()).put("actor_id", Long.parseLong(((UserKey) this.A04.get()).id)).put(C99084oO.$const$string(16), jSONArray);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("data", put);
            JSONObject jSONObject2 = new JSONObject();
            if (this.A03 == null) {
                this.A03 = ((C33451nb) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A6K, this.A00)).A01("FBMMontageMarkReadMutation.params.json", "query_doc_id", "com.facebook.messaging.montage.omnistore.storedprocedures.MontageMarkReadStoredProcedureComponent");
            }
            Long l = this.A03;
            Preconditions.checkNotNull(l);
            str2 = StringFormatUtil.formatStrLocaleSafe(jSONObject2.put("doc_id", l).put("flat_buffer_idl", ((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANr, this.A00)).A03()).put("query_params", jSONObject).put("collection_label", ((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANr, this.A00)).getCollectionLabel()).toString());
        } catch (NumberFormatException | JSONException e) {
            C010708t.A0T("com.facebook.messaging.montage.omnistore.storedprocedures.MontageMarkReadStoredProcedureComponent", e, " error while building request");
        }
        CollectionName collectionName = ((MontageOmnistoreComponent) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANr, this.A00)).A01;
        if (collectionName != null) {
            str = collectionName.getQueueIdentifier().toString();
        } else {
            str = null;
        }
        if (!C06850cB.A0B(str2) && str != null) {
            AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, this.A00), new AnonymousClass52P(this, str2, AnonymousClass08S.A0J(str, C06850cB.A06(",", immutableList)), str), 105158856);
        }
    }

    public void onStoredProcedureResult(ByteBuffer byteBuffer) {
        new String(byteBuffer.array());
    }

    private MontageMarkReadStoredProcedureComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A04 = AnonymousClass0XJ.A0I(r3);
    }

    public void onSenderAvailable(AnonymousClass2WT r1) {
        this.A01 = r1;
    }
}
