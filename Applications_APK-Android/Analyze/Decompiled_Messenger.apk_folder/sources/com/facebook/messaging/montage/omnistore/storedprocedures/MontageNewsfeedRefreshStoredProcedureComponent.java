package com.facebook.messaging.montage.omnistore.storedprocedures;

import X.AnonymousClass067;
import X.AnonymousClass06B;
import X.AnonymousClass07A;
import X.AnonymousClass0UX;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2WT;
import X.AnonymousClass52V;
import X.AnonymousClass80H;
import X.C010708t;
import X.C06850cB;
import com.facebook.acra.LogCatCollector;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.concurrent.Executor;
import javax.inject.Singleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
public final class MontageNewsfeedRefreshStoredProcedureComponent implements OmnistoreStoredProcedureComponent {
    public static final Charset A04 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    private static volatile MontageNewsfeedRefreshStoredProcedureComponent A05;
    public AnonymousClass2WT A00;
    public final MontageOmnistoreComponent A01;
    private final AnonymousClass06B A02 = AnonymousClass067.A02();
    private final Executor A03;

    public void onSenderInvalidated() {
        this.A00 = null;
    }

    public int provideStoredProcedureId() {
        return 409;
    }

    public static final MontageNewsfeedRefreshStoredProcedureComponent A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (MontageNewsfeedRefreshStoredProcedureComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new MontageNewsfeedRefreshStoredProcedureComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public void A01() {
        String str;
        if (this.A00 == null) {
            C010708t.A0I("com.facebook.messaging.montage.omnistore.storedprocedures.MontageNewsfeedRefreshStoredProcedureComponent", " no sender available for RefreshNewsfeed");
            return;
        }
        String str2 = BuildConfig.FLAVOR;
        try {
            str2 = StringFormatUtil.formatStrLocaleSafe(new JSONObject().put("collection_name", this.A01.A01).put("story_types", new JSONArray((Collection) ImmutableList.of(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1J)))).toString());
        } catch (NumberFormatException | JSONException e) {
            C010708t.A0T("com.facebook.messaging.montage.omnistore.storedprocedures.MontageNewsfeedRefreshStoredProcedureComponent", e, " error while building request");
        }
        if (!C06850cB.A0B(str2)) {
            String valueOf = String.valueOf(this.A02.now() / 1000);
            CollectionName collectionName = this.A01.A01;
            if (collectionName != null) {
                str = collectionName.getQueueIdentifier().toString();
            } else {
                str = null;
            }
            AnonymousClass07A.A04(this.A03, new AnonymousClass52V(this, str, str2, valueOf), 550734489);
        }
    }

    public void onStoredProcedureResult(ByteBuffer byteBuffer) {
        new String(byteBuffer.array());
    }

    private MontageNewsfeedRefreshStoredProcedureComponent(AnonymousClass1XY r2) {
        this.A03 = AnonymousClass0UX.A0T(r2);
        this.A01 = MontageOmnistoreComponent.A00(r2);
    }

    public void onSenderAvailable(AnonymousClass2WT r1) {
        this.A00 = r1;
    }
}
