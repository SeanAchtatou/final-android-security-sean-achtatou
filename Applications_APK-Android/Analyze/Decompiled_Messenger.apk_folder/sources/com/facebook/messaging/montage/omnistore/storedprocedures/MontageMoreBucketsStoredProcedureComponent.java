package com.facebook.messaging.montage.omnistore.storedprocedures;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.AnonymousClass2WT;
import com.facebook.acra.LogCatCollector;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import javax.inject.Singleton;

@Singleton
public final class MontageMoreBucketsStoredProcedureComponent implements OmnistoreStoredProcedureComponent {
    public static final Charset A03 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    private static volatile MontageMoreBucketsStoredProcedureComponent A04;
    public AnonymousClass0UN A00;
    public AnonymousClass2WT A01;
    public Long A02;

    public int provideStoredProcedureId() {
        return 410;
    }

    public static final MontageMoreBucketsStoredProcedureComponent A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (MontageMoreBucketsStoredProcedureComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new MontageMoreBucketsStoredProcedureComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void onSenderInvalidated() {
    }

    public void onStoredProcedureResult(ByteBuffer byteBuffer) {
        new String(byteBuffer.array());
    }

    private MontageMoreBucketsStoredProcedureComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }

    public void onSenderAvailable(AnonymousClass2WT r1) {
        this.A01 = r1;
    }
}
