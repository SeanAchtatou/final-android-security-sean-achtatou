package com.facebook.messaging.montage.omnistore;

import X.AnonymousClass07B;
import X.AnonymousClass09P;
import X.AnonymousClass0UN;
import X.AnonymousClass0VB;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass0qX;
import X.AnonymousClass12F;
import X.AnonymousClass12H;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2UE;
import X.C010708t;
import X.C04310Tq;
import X.C24321Td;
import X.C24971Xv;
import X.C32601m1;
import X.C32741mG;
import X.C33531nj;
import X.C36841tx;
import X.C74463ht;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.montagemetadata.MontageMetadata;
import com.facebook.messaging.montage.omnistore.util.MontageThreadViewUpdater;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;
import javax.inject.Singleton;

@Singleton
public final class MontageNonUserOmnistoreComponent implements OmnistoreComponent {
    private static volatile MontageNonUserOmnistoreComponent A05;
    public AnonymousClass0UN A00;
    public CollectionName A01;
    public final C04310Tq A02;
    private final AnonymousClass12H A03 = new C32601m1(this);
    private final C04310Tq A04;

    public String getCollectionLabel() {
        return "messenger_montage_non_user";
    }

    public void onCollectionAvailable(Collection collection) {
        String str;
        MontageMetadata montageMetadata;
        if (collection == null) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).CGS("com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent", "Null collection on onCollectionAvailable");
            return;
        }
        Integer num = AnonymousClass07B.A01;
        boolean A042 = ((C24321Td) this.A02.get()).A04(num);
        ((C24321Td) this.A02.get()).A02(collection, num);
        if (!A042) {
            try {
                AnonymousClass2UE r3 = (AnonymousClass2UE) AnonymousClass1XX.A02(9, AnonymousClass1Y3.B7y, this.A00);
                C24971Xv it = r3.A00.A03(num).iterator();
                while (it.hasNext()) {
                    C24971Xv it2 = ((C33531nj) it.next()).A02.iterator();
                    while (it2.hasNext()) {
                        try {
                            Message A022 = r3.A03.A02((C36841tx) it2.next());
                            if (A022 != null) {
                                if (!C32741mG.A00(A022.A0u) || (montageMetadata = A022.A0O) == null || montageMetadata.Av0() == null) {
                                    str = A022.A0q;
                                    AnonymousClass0qX.A00(str);
                                } else {
                                    str = montageMetadata.Av0();
                                }
                                r3.A02.A0C(str, A022);
                            }
                        } catch (Exception e) {
                            C010708t.A0N(r3.A04, "Failed to parse non user story", e);
                        }
                    }
                }
            } catch (Exception e2) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent", "onCollectionAvailable", e2);
            }
        }
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static final MontageNonUserOmnistoreComponent A00(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (MontageNonUserOmnistoreComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new MontageNonUserOmnistoreComponent(applicationInjector, AnonymousClass12F.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public void onCollectionInvalidated() {
        ((C24321Td) this.A02.get()).A03(AnonymousClass07B.A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0235, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0236, code lost:
        if (r2 != null) goto L_0x0238;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x027c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x027d, code lost:
        if (r2 != null) goto L_0x027f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x023b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x0282 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C32171lK provideSubscriptionInfo(com.facebook.omnistore.Omnistore r22) {
        /*
            r21 = this;
            int r1 = X.AnonymousClass1Y3.BRG
            r5 = r21
            X.0UN r0 = r5.A00
            r4 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0yl r0 = (X.C17350yl) r0
            boolean r0 = r0.A0C()
            if (r0 != 0) goto L_0x0016
            X.1lK r0 = X.C32171lK.A03
            return r0
        L_0x0016:
            java.lang.String r0 = r5.getCollectionLabel()
            r1 = r22
            com.facebook.omnistore.CollectionName$Builder r1 = r1.createCollectionNameBuilder(r0)
            X.0Tq r0 = r5.A04
            java.lang.Object r0 = r0.get()
            java.lang.String r0 = (java.lang.String) r0
            r1.addSegment(r0)
            r1.addDeviceId()
            com.facebook.omnistore.CollectionName r6 = r1.build()
            r5.A01 = r6
            X.1lI r3 = new X.1lI
            r3.<init>()
            java.lang.String r2 = ""
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ JSONException -> 0x020a }
            r10.<init>()     // Catch:{ JSONException -> 0x020a }
            java.lang.String r9 = "num_reactions"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x020a }
            int r7 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x020a }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x020a }
            r0 = 1
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x020a }
            X.1Yd r8 = (X.C25051Yd) r8     // Catch:{ JSONException -> 0x020a }
            r0 = 563976455520913(0x200ef004b0291, double:2.78641391736197E-309)
            r7 = 10
            int r0 = r8.AqL(r0, r7)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r10 = r10.put(r9, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r9 = "num_reaction_actions"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x020a }
            int r7 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x020a }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x020a }
            r0 = 1
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x020a }
            X.1Yd r8 = (X.C25051Yd) r8     // Catch:{ JSONException -> 0x020a }
            r0 = 563976455455376(0x200ef004a0290, double:2.786413917038174E-309)
            r7 = 10
            int r0 = r8.AqL(r0, r7)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r8 = r10.put(r9, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r7 = "image_full_screen_size"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x020a }
            boolean r0 = r0.A0D()     // Catch:{ JSONException -> 0x020a }
            r9 = 4
            if (r0 == 0) goto L_0x01fa
            int r1 = X.AnonymousClass1Y3.BCz     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x020a }
            int r0 = r0.A05()     // Catch:{ JSONException -> 0x020a }
        L_0x00ad:
            org.json.JSONObject r8 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r7 = "image_preview_size"
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x020a }
            int r0 = r0.A0E()     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r8 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r7 = "image_large_preview_size"
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x020a }
            int r0 = r0.A0C()     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r9 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r8 = "preset_image_scale"
            r7 = 1
            int r1 = X.AnonymousClass1Y3.A6S     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ JSONException -> 0x020a }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ JSONException -> 0x020a }
            android.content.res.Resources r0 = r0.getResources()     // Catch:{ JSONException -> 0x020a }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ JSONException -> 0x020a }
            float r0 = r0.density     // Catch:{ JSONException -> 0x020a }
            double r0 = (double) r0     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r12 = r9.put(r8, r0)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x020a }
            r7.<init>()     // Catch:{ JSONException -> 0x020a }
            java.lang.String r1 = "top_level_list_path"
            java.lang.String r0 = "viewer.non_user_montage_messages.nodes"
            org.json.JSONObject r1 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = "object_path"
            org.json.JSONObject r7 = r1.put(r0, r2)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r1 = "primary_key_path"
            java.lang.String r0 = "id"
            org.json.JSONObject r8 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = r12.toString()     // Catch:{ JSONException -> 0x020a }
            r7.<init>(r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r1 = "supported_story_types"
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x020a }
            java.lang.String r13 = "GROUP"
            java.lang.String r14 = "PAGE"
            java.lang.String r15 = "BIRTHDAY"
            java.lang.String r16 = "CHANNEL"
            java.lang.String r17 = "EVENT"
            java.lang.String r18 = "GOODWILL"
            java.lang.String r19 = "HIGHLIGHT"
            java.lang.String r20 = "ARCHIVED"
            com.google.common.collect.ImmutableList r9 = com.google.common.collect.ImmutableList.of(r13, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ JSONException -> 0x020a }
            r0.<init>(r9)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r11 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = r12.toString()     // Catch:{ JSONException -> 0x020a }
            r7.<init>(r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r1 = "story_id"
            java.lang.String r0 = "<ID>"
            org.json.JSONObject r10 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = r12.toString()     // Catch:{ JSONException -> 0x020a }
            r7.<init>(r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r1 = "story_ids"
            java.lang.String r0 = "<IDs>"
            org.json.JSONObject r9 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x020a }
            r1.<init>()     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = "render_object_list_query_params"
            org.json.JSONObject r1 = r1.put(r0, r11)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = "render_object_list_graphql_params"
            org.json.JSONObject r12 = r1.put(r0, r8)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r11 = "render_object_list_query_id"
            int r7 = X.AnonymousClass1Y3.A6K     // Catch:{ JSONException -> 0x020a }
            X.0UN r1 = r5.A00     // Catch:{ JSONException -> 0x020a }
            r0 = 8
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x020a }
            X.1nb r8 = (X.C33451nb) r8     // Catch:{ JSONException -> 0x020a }
            java.lang.String r7 = "com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent"
            java.lang.String r1 = "OmnistoreMontageNonUserListQuery.params.json"
            java.lang.String r0 = "query_doc_id"
            java.lang.Long r0 = r8.A01(r1, r0, r7)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r12 = r12.put(r11, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r11 = "render_object_query_id"
            int r7 = X.AnonymousClass1Y3.A6K     // Catch:{ JSONException -> 0x020a }
            X.0UN r1 = r5.A00     // Catch:{ JSONException -> 0x020a }
            r0 = 8
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x020a }
            X.1nb r8 = (X.C33451nb) r8     // Catch:{ JSONException -> 0x020a }
            java.lang.String r7 = "com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent"
            java.lang.String r1 = "OmnistoreMontageListQuery.params.json"
            java.lang.Long r0 = r8.A01(r1, r11, r7)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r1 = r12.put(r11, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = "render_object_query_params"
            org.json.JSONObject r11 = r1.put(r0, r10)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r10 = "render_multi_objects_query_id"
            int r7 = X.AnonymousClass1Y3.A6K     // Catch:{ JSONException -> 0x020a }
            X.0UN r1 = r5.A00     // Catch:{ JSONException -> 0x020a }
            r0 = 8
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x020a }
            X.1nb r8 = (X.C33451nb) r8     // Catch:{ JSONException -> 0x020a }
            java.lang.String r7 = "com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent"
            java.lang.String r1 = "OmnistoreMontageMultiObjectsQuery.params.json"
            java.lang.String r0 = "query_doc_id"
            java.lang.Long r0 = r8.A01(r1, r0, r7)     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r1 = r11.put(r10, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = "render_multi_objects_query_params"
            org.json.JSONObject r9 = r1.put(r0, r9)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r8 = "app_id"
            r7 = 6
            int r1 = X.AnonymousClass1Y3.B3a     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.00x r0 = (X.C001300x) r0     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = r0.A04     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r9 = r9.put(r8, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r8 = "app_version"
            r7 = 7
            int r1 = X.AnonymousClass1Y3.BMV     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.0hF r0 = (X.C09400hF) r0     // Catch:{ JSONException -> 0x020a }
            java.lang.String r0 = r0.A02()     // Catch:{ JSONException -> 0x020a }
            org.json.JSONObject r7 = r9.put(r8, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r1 = "trigger_delete_field"
            java.lang.String r0 = "1"
            org.json.JSONObject r0 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x020a }
            java.lang.String r2 = r0.toString()     // Catch:{ JSONException -> 0x020a }
            goto L_0x020a
        L_0x01fa:
            int r1 = X.AnonymousClass1Y3.BCz     // Catch:{ JSONException -> 0x020a }
            X.0UN r0 = r5.A00     // Catch:{ JSONException -> 0x020a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x020a }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x020a }
            int r0 = X.C36821tr.A01(r0)     // Catch:{ JSONException -> 0x020a }
            goto L_0x00ad
        L_0x020a:
            r3.A02 = r2
            r2 = 1
            int r1 = X.AnonymousClass1Y3.A6S     // Catch:{ IOException -> 0x023c }
            X.0UN r0 = r5.A00     // Catch:{ IOException -> 0x023c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IOException -> 0x023c }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ IOException -> 0x023c }
            android.content.res.AssetManager r1 = r0.getAssets()     // Catch:{ IOException -> 0x023c }
            java.lang.String r0 = "FBMMontageMessageInfo.fbs"
            java.io.InputStream r2 = r1.open(r0)     // Catch:{ IOException -> 0x023c }
            int r0 = r2.available()     // Catch:{ all -> 0x0233 }
            byte[] r1 = new byte[r0]     // Catch:{ all -> 0x0233 }
            r2.read(r1)     // Catch:{ all -> 0x0233 }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0233 }
            r0.<init>(r1)     // Catch:{ all -> 0x0233 }
            r2.close()     // Catch:{ IOException -> 0x023c }
            goto L_0x0251
        L_0x0233:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0235 }
        L_0x0235:
            r0 = move-exception
            if (r2 == 0) goto L_0x023b
            r2.close()     // Catch:{ all -> 0x023b }
        L_0x023b:
            throw r0     // Catch:{ IOException -> 0x023c }
        L_0x023c:
            r7 = move-exception
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent"
            java.lang.String r0 = "Failed to read idl from file"
            r2.softReport(r1, r0, r7)
            java.lang.String r0 = ""
        L_0x0251:
            r3.A03 = r0
            r2 = 1
            int r1 = X.AnonymousClass1Y3.A6S     // Catch:{ IOException -> 0x0283 }
            X.0UN r0 = r5.A00     // Catch:{ IOException -> 0x0283 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IOException -> 0x0283 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ IOException -> 0x0283 }
            android.content.res.AssetManager r1 = r0.getAssets()     // Catch:{ IOException -> 0x0283 }
            java.lang.String r0 = "FBMMontageMessageInfo.idna"
            java.io.InputStream r2 = r1.open(r0)     // Catch:{ IOException -> 0x0283 }
            int r0 = r2.available()     // Catch:{ all -> 0x027a }
            byte[] r1 = new byte[r0]     // Catch:{ all -> 0x027a }
            r2.read(r1)     // Catch:{ all -> 0x027a }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x027a }
            r0.<init>(r1)     // Catch:{ all -> 0x027a }
            r2.close()     // Catch:{ IOException -> 0x0283 }
            goto L_0x0298
        L_0x027a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x027c }
        L_0x027c:
            r0 = move-exception
            if (r2 == 0) goto L_0x0282
            r2.close()     // Catch:{ all -> 0x0282 }
        L_0x0282:
            throw r0     // Catch:{ IOException -> 0x0283 }
        L_0x0283:
            r7 = move-exception
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent"
            java.lang.String r0 = "Failed to read dna from file"
            r2.softReport(r1, r0, r7)
            java.lang.String r0 = ""
        L_0x0298:
            r3.A04 = r0
            r3.A00 = r4
            X.1lJ r0 = new X.1lJ
            r0.<init>(r3)
            X.1lK r0 = X.C32171lK.A00(r6, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent.provideSubscriptionInfo(com.facebook.omnistore.Omnistore):X.1lK");
    }

    private MontageNonUserOmnistoreComponent(AnonymousClass1XY r3, AnonymousClass12F r4) {
        this.A00 = new AnonymousClass0UN(10, r3);
        this.A02 = AnonymousClass0VB.A00(AnonymousClass1Y3.AOY, r3);
        this.A04 = AnonymousClass0XJ.A0L(r3);
        AnonymousClass12H r1 = this.A03;
        synchronized (r4) {
            r4.A01.add(r1);
        }
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        try {
            return C74463ht.A01(byteBuffer);
        } catch (Exception e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("com.facebook.messaging.montage.omnistore.MontageNonUserOmnistoreComponent", "indexObject", e);
            return new IndexedFields();
        }
    }

    public void BVs(List list) {
        String str;
        MontageMetadata montageMetadata;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Delta delta = (Delta) it.next();
            if (delta.getType() == 1) {
                ByteBuffer blob = delta.getBlob();
                AnonymousClass0qX.A00(blob);
                C36841tx A002 = C36841tx.A00(blob);
                AnonymousClass2UE r3 = (AnonymousClass2UE) AnonymousClass1XX.A02(9, AnonymousClass1Y3.B7y, this.A00);
                try {
                    Message A022 = r3.A03.A02(A002);
                    if (A022 != null) {
                        if (!C32741mG.A00(A022.A0u) || (montageMetadata = A022.A0O) == null || montageMetadata.Av0() == null) {
                            str = A022.A0q;
                            AnonymousClass0qX.A00(str);
                        } else {
                            str = montageMetadata.Av0();
                        }
                        r3.A02.A0C(str, A022);
                        if (r3.A02.A03(str) != null) {
                            r3.A01.A01(A002);
                        }
                    }
                } catch (Exception e) {
                    C010708t.A0N(r3.A04, "Failed to parse non user story", e);
                }
            } else if (delta.getType() == 2) {
                AnonymousClass2UE r2 = (AnonymousClass2UE) AnonymousClass1XX.A02(9, AnonymousClass1Y3.B7y, this.A00);
                String primaryKey = delta.getPrimaryKey();
                r2.A02.A0C(primaryKey, null);
                ((MontageThreadViewUpdater) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Axc, r2.A01.A00)).A01(primaryKey);
            }
        }
    }

    public void Boz(int i) {
    }
}
