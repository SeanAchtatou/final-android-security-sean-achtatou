package com.facebook.messaging.montage.omnistore;

import X.AnonymousClass0UN;
import X.AnonymousClass0WY;
import X.AnonymousClass1XY;
import X.C04310Tq;
import X.C05540Zi;
import X.C189216c;
import X.C21321Gd;
import X.C32451lm;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.messaging.montage.omnistore.cache.MontageCache;
import com.facebook.messaging.montage.omnistore.converter.MontageFBConverter;
import java.util.Set;

@UserScoped
public final class MontageOmnistoreCacheUpdater implements CallerContextable {
    private static C05540Zi A09;
    public AnonymousClass0UN A00;
    public Set A01 = new C21321Gd();
    public Set A02 = new C21321Gd();
    public Set A03 = new C21321Gd();
    public final C189216c A04;
    public final C32451lm A05;
    public final MontageCache A06;
    public final C04310Tq A07;
    private final MontageFBConverter A08;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r11v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r2v18, types: [com.google.common.collect.ImmutableList] */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:33|(2:35|36)|37|38) */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x012c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x012d, code lost:
        if (r1 != null) goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0170, code lost:
        return r5;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0132 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0139 A[SYNTHETIC, Splitter:B:44:0x0139] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized X.AnonymousClass1v1 A01(com.facebook.messaging.montage.omnistore.MontageOmnistoreCacheUpdater r12, X.C33531nj r13) {
        /*
            r3 = r12
            monitor-enter(r3)
            com.facebook.messaging.montage.omnistore.converter.MontageFBConverter r1 = r12.A08     // Catch:{ all -> 0x0171 }
            java.lang.String r0 = X.AnonymousClass2UL.A00(r13)     // Catch:{ all -> 0x0171 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.montage.model.MontageThreadInfo r9 = r1.A03(r13)     // Catch:{ all -> 0x0171 }
            X.1nn r6 = r1.A01     // Catch:{ all -> 0x0171 }
            X.1T9 r4 = r13.A01     // Catch:{ all -> 0x0171 }
            boolean r7 = r4.A0A()     // Catch:{ all -> 0x0171 }
            com.facebook.user.model.UserKey r5 = com.facebook.user.model.UserKey.A01(r0)     // Catch:{ all -> 0x0171 }
            r0 = 6
            int r2 = r4.A02(r0)     // Catch:{ all -> 0x0171 }
            if (r2 == 0) goto L_0x00c0
            java.nio.ByteBuffer r1 = r4.A01     // Catch:{ all -> 0x0171 }
            int r0 = r4.A00     // Catch:{ all -> 0x0171 }
            int r2 = r2 + r0
            int r13 = r1.getInt(r2)     // Catch:{ all -> 0x0171 }
        L_0x002b:
            X.1TU r8 = r6.A01     // Catch:{ all -> 0x0171 }
            r2 = 0
            if (r7 == 0) goto L_0x00bd
            X.1nU r4 = r6.A02     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r9.A01     // Catch:{ all -> 0x0171 }
            com.google.common.collect.ImmutableList r0 = r9.A02     // Catch:{ all -> 0x0171 }
            com.google.common.collect.ImmutableList r11 = r4.A02(r1, r0)     // Catch:{ all -> 0x0171 }
        L_0x003a:
            X.0yl r0 = r6.A00     // Catch:{ all -> 0x0171 }
            boolean r12 = r0.A0H()     // Catch:{ all -> 0x0171 }
            r10 = r7
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r4 = r8.A0G(r9, r10, r11, r12, r13)     // Catch:{ all -> 0x0171 }
            X.0yl r0 = r6.A00     // Catch:{ all -> 0x0171 }
            boolean r0 = r0.A0J()     // Catch:{ all -> 0x0171 }
            if (r0 == 0) goto L_0x0061
            X.1TU r8 = r6.A01     // Catch:{ all -> 0x0171 }
            if (r7 == 0) goto L_0x005b
            X.1nU r2 = r6.A02     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r9.A01     // Catch:{ all -> 0x0171 }
            com.google.common.collect.ImmutableList r0 = r9.A02     // Catch:{ all -> 0x0171 }
            com.google.common.collect.ImmutableList r2 = r2.A02(r1, r0)     // Catch:{ all -> 0x0171 }
        L_0x005b:
            r12 = 1
            r11 = r2
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r2 = r8.A0G(r9, r10, r11, r12, r13)     // Catch:{ all -> 0x0171 }
        L_0x0061:
            X.2V8 r1 = new X.2V8     // Catch:{ all -> 0x0171 }
            r1.<init>()     // Catch:{ all -> 0x0171 }
            r1.A04 = r9     // Catch:{ all -> 0x0171 }
            r1.A05 = r5     // Catch:{ all -> 0x0171 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x0171 }
            r1.A06 = r0     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r9.A01     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S     // Catch:{ all -> 0x0171 }
            r1.A01 = r0     // Catch:{ all -> 0x0171 }
            r1.A00 = r13     // Catch:{ all -> 0x0171 }
            r1.A02 = r4     // Catch:{ all -> 0x0171 }
            r1.A03 = r2     // Catch:{ all -> 0x0171 }
            X.1v1 r5 = r1.A00()     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r5.A01     // Catch:{ all -> 0x0171 }
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ all -> 0x0171 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x0171 }
            r7 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0171 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ all -> 0x0171 }
            int r2 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x0171 }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x0171 }
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x0171 }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ all -> 0x0171 }
            r0 = 282501475403217(0x100ef001705d1, double:1.395742738961954E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ all -> 0x0171 }
            if (r0 == 0) goto L_0x0133
            com.facebook.messaging.montage.omnistore.cache.MontageCache r0 = r3.A06     // Catch:{ all -> 0x0171 }
            X.1v1 r0 = r0.A06(r4)     // Catch:{ all -> 0x0171 }
            if (r0 != 0) goto L_0x0133
            com.facebook.messaging.montage.omnistore.cache.MontageCache r4 = r3.A06     // Catch:{ all -> 0x0171 }
            int r2 = X.AnonymousClass1Y3.BP2     // Catch:{ all -> 0x0171 }
            X.0UN r1 = r4.A00     // Catch:{ all -> 0x0171 }
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x0171 }
            X.1HC r0 = (X.AnonymousClass1HC) r0     // Catch:{ all -> 0x0171 }
            X.1HE r1 = r0.A01()     // Catch:{ all -> 0x0171 }
            goto L_0x00c3
        L_0x00bd:
            r11 = r2
            goto L_0x003a
        L_0x00c0:
            r13 = 0
            goto L_0x002b
        L_0x00c3:
            java.util.Map r0 = r4.A03     // Catch:{ all -> 0x012a }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x012a }
            int r6 = r0.size()     // Catch:{ all -> 0x012a }
            if (r1 == 0) goto L_0x00d2
            r1.close()     // Catch:{ all -> 0x0171 }
        L_0x00d2:
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ all -> 0x0171 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x0171 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0171 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ all -> 0x0171 }
            int r2 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x0171 }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x0171 }
            r0 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x0171 }
            X.1Yd r4 = (X.C25051Yd) r4     // Catch:{ all -> 0x0171 }
            r1 = 563976455586450(0x200ef004c0292, double:2.786413917685765E-309)
            r0 = 40
            int r4 = r4.AqL(r1, r0)     // Catch:{ all -> 0x0171 }
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ all -> 0x0171 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x0171 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0171 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ all -> 0x0171 }
            int r2 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x0171 }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x0171 }
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x0171 }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ all -> 0x0171 }
            r0 = 563976451719811(0x200ef00110283, double:2.78641389858203E-309)
            long r1 = r2.At0(r0)     // Catch:{ all -> 0x0171 }
            int r0 = (int) r1     // Catch:{ all -> 0x0171 }
            int r4 = r4 + r0
            if (r6 <= r4) goto L_0x0133
            r2 = 7
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0171 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x0171 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0171 }
            X.09P r4 = (X.AnonymousClass09P) r4     // Catch:{ all -> 0x0171 }
            java.lang.String r2 = "com.facebook.messaging.montage.omnistore.MontageOmnistoreCacheUpdater"
            r1 = 10000(0x2710, float:1.4013E-41)
            java.lang.String r0 = "Dropping update to omnistore cache, cache is full"
            r4.CGT(r2, r0, r1)     // Catch:{ all -> 0x0171 }
            r0 = 1
            goto L_0x0134
        L_0x012a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x012c }
        L_0x012c:
            r0 = move-exception
            if (r1 == 0) goto L_0x0132
            r1.close()     // Catch:{ all -> 0x0132 }
        L_0x0132:
            throw r0     // Catch:{ all -> 0x0171 }
        L_0x0133:
            r0 = 0
        L_0x0134:
            if (r0 == 0) goto L_0x0139
            r0 = 0
            monitor-exit(r3)
            return r0
        L_0x0139:
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r2 = r5.A02     // Catch:{ all -> 0x0171 }
            r4 = 1
            if (r2 == 0) goto L_0x0150
            int r1 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x0171 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x0171 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x0171 }
            X.1TU r1 = (X.AnonymousClass1TU) r1     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.model.messages.Message r0 = r2.A01     // Catch:{ all -> 0x0171 }
            boolean r0 = r1.A0K(r0)     // Catch:{ all -> 0x0171 }
            if (r0 == 0) goto L_0x0168
        L_0x0150:
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r2 = r5.A03     // Catch:{ all -> 0x0171 }
            if (r2 == 0) goto L_0x0167
            int r1 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x0171 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x0171 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x0171 }
            X.1TU r1 = (X.AnonymousClass1TU) r1     // Catch:{ all -> 0x0171 }
            com.facebook.messaging.model.messages.Message r0 = r2.A01     // Catch:{ all -> 0x0171 }
            boolean r0 = r1.A0K(r0)     // Catch:{ all -> 0x0171 }
            if (r0 != 0) goto L_0x0167
            goto L_0x0168
        L_0x0167:
            r4 = 0
        L_0x0168:
            if (r4 == 0) goto L_0x016f
            com.facebook.messaging.montage.omnistore.cache.MontageCache r0 = r3.A06     // Catch:{ all -> 0x0171 }
            r0.A0A(r5)     // Catch:{ all -> 0x0171 }
        L_0x016f:
            monitor-exit(r3)
            return r5
        L_0x0171:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.MontageOmnistoreCacheUpdater.A01(com.facebook.messaging.montage.omnistore.MontageOmnistoreCacheUpdater, X.1nj):X.1v1");
    }

    public static final MontageOmnistoreCacheUpdater A00(AnonymousClass1XY r4) {
        MontageOmnistoreCacheUpdater montageOmnistoreCacheUpdater;
        synchronized (MontageOmnistoreCacheUpdater.class) {
            C05540Zi A002 = C05540Zi.A00(A09);
            A09 = A002;
            try {
                if (A002.A03(r4)) {
                    A09.A00 = new MontageOmnistoreCacheUpdater((AnonymousClass1XY) A09.A01());
                }
                C05540Zi r1 = A09;
                montageOmnistoreCacheUpdater = (MontageOmnistoreCacheUpdater) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A09.A02();
                throw th;
            }
        }
        return montageOmnistoreCacheUpdater;
    }

    private MontageOmnistoreCacheUpdater(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(9, r3);
        this.A06 = MontageCache.A01(r3);
        this.A05 = C32451lm.A00(r3);
        this.A04 = C189216c.A02(r3);
        this.A08 = MontageFBConverter.A01(r3);
        this.A07 = AnonymousClass0WY.A02(r3);
    }
}
