package com.facebook.messaging.montage.omnistore;

import X.AnonymousClass07A;
import X.AnonymousClass0UN;
import X.AnonymousClass0VG;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y6;
import X.AnonymousClass2UG;
import X.C04310Tq;
import X.C100784rP;
import X.C100794rQ;
import X.C100814rS;
import X.C158717Wd;
import X.C32151lI;
import X.C32161lJ;
import X.C32171lK;
import X.C33451nb;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.concurrent.Executor;
import javax.inject.Singleton;
import org.json.JSONObject;

@Singleton
public final class MontageParticipantOmnistoreComponent implements OmnistoreComponent {
    private static volatile MontageParticipantOmnistoreComponent A04;
    public AnonymousClass0UN A00;
    public Collection A01;
    private final C04310Tq A02;
    private final C04310Tq A03;

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0076, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        ((X.AnonymousClass09P) X.AnonymousClass1XX.A02(1, X.AnonymousClass1Y3.Amr, r8.A00)).softReport("com.facebook.messaging.montage.omnistore.MontageParticipantOmnistoreComponent", "updatePreferenceCache", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0089, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008a, code lost:
        X.C010708t.A0R("com.facebook.messaging.montage.omnistore.MontageParticipantOmnistoreComponent", r2, "IO error while reading messenger_montage_participant_attributes collection");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void A01() {
        /*
            r8 = this;
            monitor-enter(r8)
            com.facebook.omnistore.Collection r0 = r8.A01     // Catch:{ all -> 0x0093 }
            if (r0 == 0) goto L_0x0091
            r5 = 0
            r4 = 1
            java.lang.String r0 = "is_custom_participant"
            java.lang.String r7 = java.lang.Boolean.toString(r4)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r6 = 3
            com.facebook.omnistore.IndexQuery r1 = com.facebook.omnistore.IndexQuery.predicate(r0, r6, r7)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            com.facebook.omnistore.Collection r0 = r8.A01     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r2 = -1
            com.facebook.omnistore.Cursor r1 = r1.queryWithIndex(r0, r2)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
        L_0x0019:
            boolean r0 = r1.step()     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            if (r0 == 0) goto L_0x0022
            int r5 = r5 + 1
            goto L_0x0019
        L_0x0022:
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r3.<init>()     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            java.lang.String r0 = "is_muted_participant"
            com.facebook.omnistore.IndexQuery r1 = com.facebook.omnistore.IndexQuery.predicate(r0, r6, r7)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            com.facebook.omnistore.Collection r0 = r8.A01     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            com.facebook.omnistore.Cursor r1 = r1.queryWithIndex(r0, r2)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
        L_0x0033:
            boolean r0 = r1.step()     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            if (r0 == 0) goto L_0x0041
            java.lang.String r0 = r1.getPrimaryKey()     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r3.add(r0)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            goto L_0x0033
        L_0x0041:
            com.facebook.omnistore.Collection r0 = r8.A01     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            int r0 = r0.getSnapshotState()     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r2 = 2
            if (r0 != r2) goto L_0x0091
            X.0Tq r0 = r8.A02     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            java.lang.Object r1 = r0.get()     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            X.2UG r1 = (X.AnonymousClass2UG) r1     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            monitor-enter(r1)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r1.A00 = r5     // Catch:{ all -> 0x0073 }
            monitor-exit(r1)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            X.0Tq r0 = r8.A02     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            java.lang.Object r1 = r0.get()     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            X.2UG r1 = (X.AnonymousClass2UG) r1     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r1.A01(r0)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            int r1 = X.AnonymousClass1Y3.AJ2     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            X.0UN r0 = r8.A00     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            X.2UH r0 = (X.AnonymousClass2UH) r0     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            r0.A01(r3)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            goto L_0x0091
        L_0x0073:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
            throw r0     // Catch:{ OmnistoreIOException -> 0x0089, Exception -> 0x0076 }
        L_0x0076:
            r3 = move-exception
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0093 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x0093 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x0093 }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x0093 }
            java.lang.String r1 = "com.facebook.messaging.montage.omnistore.MontageParticipantOmnistoreComponent"
            java.lang.String r0 = "updatePreferenceCache"
            r2.softReport(r1, r0, r3)     // Catch:{ all -> 0x0093 }
            goto L_0x0091
        L_0x0089:
            r2 = move-exception
            java.lang.String r1 = "com.facebook.messaging.montage.omnistore.MontageParticipantOmnistoreComponent"
            java.lang.String r0 = "IO error while reading messenger_montage_participant_attributes collection"
            X.C010708t.A0R(r1, r2, r0)     // Catch:{ all -> 0x0093 }
        L_0x0091:
            monitor-exit(r8)
            return
        L_0x0093:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.MontageParticipantOmnistoreComponent.A01():void");
    }

    public String getCollectionLabel() {
        return "messenger_montage_participant_attributes";
    }

    public void onCollectionInvalidated() {
        this.A01 = null;
        ((AnonymousClass2UG) this.A02.get()).A01(false);
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static final MontageParticipantOmnistoreComponent A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (MontageParticipantOmnistoreComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new MontageParticipantOmnistoreComponent(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static void A02(MontageParticipantOmnistoreComponent montageParticipantOmnistoreComponent, C100794rQ r4, Throwable th) {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(3, AnonymousClass1Y3.APr, montageParticipantOmnistoreComponent.A00)).BxR(new C100784rP(r4, th));
    }

    public void A03(String str, C100794rQ r5) {
        AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A1n, this.A00), new C100814rS(this, r5, str), -563872155);
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        IndexedFields indexedFields = new IndexedFields();
        C158717Wd r5 = new C158717Wd();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int i = byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position();
        r5.A00 = i;
        r5.A01 = byteBuffer;
        int A022 = r5.A02(4);
        boolean z = false;
        if (!(A022 == 0 || byteBuffer.get(A022 + i) == 0)) {
            z = true;
        }
        indexedFields.addFieldValue("is_custom_participant", Boolean.toString(z));
        int A023 = r5.A02(6);
        boolean z2 = false;
        if (!(A023 == 0 || r5.A01.get(A023 + r5.A00) == 0)) {
            z2 = true;
        }
        indexedFields.addFieldValue("is_blocked_participant", Boolean.toString(z2));
        int A024 = r5.A02(8);
        boolean z3 = false;
        if (!(A024 == 0 || r5.A01.get(A024 + r5.A00) == 0)) {
            z3 = true;
        }
        indexedFields.addFieldValue("is_muted_participant", Boolean.toString(z3));
        return indexedFields;
    }

    public void onCollectionAvailable(Collection collection) {
        this.A01 = collection;
        if (collection.getObjectCount() != this.A01.getIndexCollectionUniqueCount()) {
            this.A01.reindexAllObjects();
        }
        A01();
    }

    private MontageParticipantOmnistoreComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A03 = AnonymousClass0XJ.A0L(r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.A3P, r3);
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
        A01();
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        CollectionName.Builder createCollectionNameWithDomainBuilder = omnistore.createCollectionNameWithDomainBuilder(getCollectionLabel(), "messenger_user_sq");
        createCollectionNameWithDomainBuilder.addSegment((String) this.A03.get());
        CollectionName build = createCollectionNameWithDomainBuilder.build();
        C32151lI r4 = new C32151lI();
        r4.A02 = new JSONObject().toString();
        r4.A03 = ((C33451nb) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6K, this.A00)).A02("messenger_montage_participant_attributes.fbs", "com.facebook.messaging.montage.omnistore.MontageParticipantOmnistoreComponent");
        r4.A04 = ((C33451nb) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6K, this.A00)).A02("messenger_montage_participant_attributes.idna", "com.facebook.messaging.montage.omnistore.MontageParticipantOmnistoreComponent");
        r4.A00 = 2;
        return C32171lK.A00(build, new C32161lJ(r4));
    }

    public void BVs(List list) {
    }

    public void Boz(int i) {
        A01();
    }
}
