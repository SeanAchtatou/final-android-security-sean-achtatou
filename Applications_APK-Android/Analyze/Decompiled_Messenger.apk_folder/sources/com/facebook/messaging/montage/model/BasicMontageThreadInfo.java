package com.facebook.messaging.montage.model;

import X.AnonymousClass2VI;
import X.C28841fS;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.UserKey;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Arrays;

public final class BasicMontageThreadInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass2VI();
    public final int A00;
    public final Message A01;
    public final MontageBucketKey A02;
    public final UserKey A03;
    public final ImmutableList A04;
    public final ImmutableList A05;
    public final String A06;
    public final boolean A07;
    public final boolean A08;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            BasicMontageThreadInfo basicMontageThreadInfo = (BasicMontageThreadInfo) obj;
            if (this.A07 != basicMontageThreadInfo.A07 || this.A08 != basicMontageThreadInfo.A08 || !Objects.equal(this.A03, basicMontageThreadInfo.A03) || !Objects.equal(this.A02, basicMontageThreadInfo.A02) || !Objects.equal(this.A01.toString(), basicMontageThreadInfo.A01.toString()) || !Objects.equal(this.A04, basicMontageThreadInfo.A04) || !Objects.equal(this.A05, basicMontageThreadInfo.A05) || !Objects.equal(this.A06, basicMontageThreadInfo.A06)) {
                return false;
            }
            return !this.A08 || Objects.equal(this.A01.A0j, basicMontageThreadInfo.A01.A0j);
        }
    }

    public static BasicMontageThreadInfo A00(Message message, String str, boolean z, boolean z2, int i) {
        long j;
        Message message2 = message;
        UserKey userKey = message.A0K.A01;
        ThreadKey threadKey = message.A0U;
        if (threadKey == null) {
            j = 0;
        } else {
            j = threadKey.A03;
        }
        return new BasicMontageThreadInfo(userKey, new MontageBucketKey(j), message2, null, null, str, z, z2, i);
    }

    public boolean A01() {
        Message message;
        if (!this.A08 || (message = this.A01) == null || !C28841fS.A0V(message)) {
            return false;
        }
        return true;
    }

    public boolean A02() {
        Message message;
        if (!this.A08 || (message = this.A01) == null || !C28841fS.A14(message)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, this.A02, this.A01, this.A04, this.A05, this.A06, Boolean.valueOf(this.A07), Boolean.valueOf(this.A08), Integer.valueOf(this.A00)});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A03, 0);
        parcel.writeParcelable(this.A02, 0);
        parcel.writeParcelable(this.A01, 0);
        parcel.writeList(this.A04);
        parcel.writeList(this.A05);
        parcel.writeString(this.A06);
        C417826y.A0V(parcel, this.A07);
        C417826y.A0V(parcel, this.A08);
        parcel.writeInt(this.A00);
    }

    public BasicMontageThreadInfo(UserKey userKey, MontageBucketKey montageBucketKey, Message message, ImmutableList immutableList, ImmutableList immutableList2, String str, boolean z, boolean z2, int i) {
        Preconditions.checkNotNull(userKey);
        this.A03 = userKey;
        Preconditions.checkNotNull(montageBucketKey);
        this.A02 = montageBucketKey;
        Preconditions.checkNotNull(message);
        this.A01 = message;
        this.A05 = immutableList2 == null ? RegularImmutableList.A02 : immutableList2;
        this.A06 = str;
        this.A07 = z;
        this.A08 = z2;
        this.A04 = immutableList == null ? RegularImmutableList.A02 : immutableList;
        this.A00 = i;
        if (z2) {
            Preconditions.checkArgument(!z, "Can't have unread message for myMontage item");
        }
    }

    public int describeContents() {
        return hashCode();
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("userKey", this.A03);
        stringHelper.add("montageBucketKey", this.A02);
        stringHelper.add("previewMessage", this.A01);
        stringHelper.add("previewMessages", this.A04);
        stringHelper.add("seenByUsers", this.A05);
        stringHelper.add("hasUnreadMessages", this.A07);
        stringHelper.add("isForMyMontage", this.A08);
        stringHelper.add("pageNum", this.A00);
        return stringHelper.toString();
    }
}
