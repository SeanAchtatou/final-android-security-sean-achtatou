package com.facebook.messaging.montage.omnistore;

import X.AnonymousClass07A;
import X.AnonymousClass07B;
import X.AnonymousClass09P;
import X.AnonymousClass0UN;
import X.AnonymousClass0VB;
import X.AnonymousClass0VG;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass0g4;
import X.AnonymousClass108;
import X.AnonymousClass12F;
import X.AnonymousClass12G;
import X.AnonymousClass12H;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y6;
import X.C005505z;
import X.C04310Tq;
import X.C06850cB;
import X.C17350yl;
import X.C188715q;
import X.C195518s;
import X.C24321Td;
import X.C25051Yd;
import X.C32751mH;
import X.C36841tx;
import X.C74463ht;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.google.common.base.Preconditions;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
public final class MontageOmnistoreComponent implements OmnistoreComponent {
    private static volatile MontageOmnistoreComponent A0A;
    public AnonymousClass0UN A00;
    public CollectionName A01;
    private long A02 = 0;
    public final AtomicBoolean A03 = new AtomicBoolean(false);
    public final AtomicBoolean A04 = new AtomicBoolean(false);
    public final C04310Tq A05;
    private final AnonymousClass12H A06 = new AnonymousClass12G(this);
    private final C04310Tq A07;
    private final C04310Tq A08;
    private final C04310Tq A09;

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        if (r2 != null) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x002f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String A01() {
        /*
            r4 = this;
            r2 = 1
            int r1 = X.AnonymousClass1Y3.A6S     // Catch:{ IOException -> 0x0030 }
            X.0UN r0 = r4.A00     // Catch:{ IOException -> 0x0030 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IOException -> 0x0030 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ IOException -> 0x0030 }
            android.content.res.AssetManager r1 = r0.getAssets()     // Catch:{ IOException -> 0x0030 }
            java.lang.String r0 = "FBMMontageMessageInfo.idna"
            java.io.InputStream r2 = r1.open(r0)     // Catch:{ IOException -> 0x0030 }
            int r0 = r2.available()     // Catch:{ all -> 0x0027 }
            byte[] r1 = new byte[r0]     // Catch:{ all -> 0x0027 }
            r2.read(r1)     // Catch:{ all -> 0x0027 }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0027 }
            r0.<init>(r1)     // Catch:{ all -> 0x0027 }
            r2.close()     // Catch:{ IOException -> 0x0030 }
            return r0
        L_0x0027:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0029 }
        L_0x0029:
            r0 = move-exception
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ all -> 0x002f }
        L_0x002f:
            throw r0     // Catch:{ IOException -> 0x0030 }
        L_0x0030:
            r3 = move-exception
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent"
            java.lang.String r0 = "Failed to read dna from file"
            r2.softReport(r1, r0, r3)
            java.lang.String r0 = ""
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent.A01():java.lang.String");
    }

    public String getCollectionLabel() {
        return "messenger_montage_v2";
    }

    public static final MontageOmnistoreComponent A00(AnonymousClass1XY r5) {
        if (A0A == null) {
            synchronized (MontageOmnistoreComponent.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0A = new MontageOmnistoreComponent(applicationInjector, AnonymousClass12F.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static boolean A02(MontageOmnistoreComponent montageOmnistoreComponent) {
        if (!((C17350yl) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BRG, montageOmnistoreComponent.A00)).A09() || montageOmnistoreComponent.A05()) {
            return false;
        }
        return true;
    }

    public boolean A05() {
        return ((C24321Td) this.A08.get()).A04(AnonymousClass07B.A00);
    }

    public void BVs(List list) {
        String A082;
        try {
            MontageOmnistoreDeltaHandler montageOmnistoreDeltaHandler = (MontageOmnistoreDeltaHandler) this.A07.get();
            HashSet hashSet = new HashSet();
            HashSet hashSet2 = new HashSet();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Delta delta = (Delta) it.next();
                if (delta.getType() == 2) {
                    MontageOmnistoreCacheUpdater montageOmnistoreCacheUpdater = montageOmnistoreDeltaHandler.A02;
                    String primaryKey = delta.getPrimaryKey();
                    synchronized (montageOmnistoreCacheUpdater) {
                        try {
                            montageOmnistoreCacheUpdater.A01.add(primaryKey);
                        } catch (Throwable th) {
                            th = th;
                            throw th;
                        }
                    }
                } else if (delta.getType() == 1) {
                    C36841tx A002 = C36841tx.A00(delta.getBlob());
                    if (A002.A0A().A0A()) {
                        A082 = A002.A0A().A09();
                    } else {
                        A082 = A002.A0A().A08();
                    }
                    hashSet.add(A082);
                    String A0D = A002.A0D();
                    Preconditions.checkNotNull(A0D);
                    if (montageOmnistoreDeltaHandler.A03.A03(A0D) == null) {
                        hashSet2.add(A002);
                    }
                }
            }
            MontageOmnistoreCacheUpdater montageOmnistoreCacheUpdater2 = montageOmnistoreDeltaHandler.A02;
            synchronized (montageOmnistoreCacheUpdater2) {
                try {
                    montageOmnistoreCacheUpdater2.A03.addAll(hashSet);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
            MontageOmnistoreCacheUpdater montageOmnistoreCacheUpdater3 = montageOmnistoreDeltaHandler.A02;
            synchronized (montageOmnistoreCacheUpdater3) {
                montageOmnistoreCacheUpdater3.A02.addAll(hashSet2);
            }
            if (!montageOmnistoreDeltaHandler.A00) {
                MontageOmnistoreDeltaHandler.A01(montageOmnistoreDeltaHandler);
            }
        } catch (Exception e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent", "onDeltasReceived", e);
        }
    }

    public void onCollectionAvailable(Collection collection) {
        int i;
        String str;
        C005505z.A03("MontageOmnistoreComponent:onCollectionAvailable", -1479465674);
        if (collection == null) {
            try {
                ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).CGS("com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent", "Null collection on onCollectionAvailable");
                i = -1362490704;
            } catch (Throwable th) {
                C005505z.A00(-409895312);
                throw th;
            }
        } else {
            boolean A052 = A05();
            ((C24321Td) this.A08.get()).A02(collection, AnonymousClass07B.A00);
            if (A052) {
                i = -96398313;
            } else {
                if (this.A03.compareAndSet(false, true)) {
                    C32751mH r4 = new C32751mH(this);
                    long At0 = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C195518s) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AQC, this.A00)).A00)).At0(563976455651987L);
                    if (At0 == 3 && ((AnonymousClass1Y6) AnonymousClass1XX.A02(10, AnonymousClass1Y3.APr, this.A00)).BHM()) {
                        At0 = 2;
                    }
                    if (At0 == 1) {
                        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B7B, this.A00), r4, 1986037788);
                    } else if (At0 == 3) {
                        r4.run();
                    } else {
                        AnonymousClass108 r3 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, this.A00);
                        AnonymousClass0g4 r2 = (AnonymousClass0g4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ASI, this.A00);
                        r3.A02(r4);
                        r3.A02 = "MontageOmnistoreLoadAllStories";
                        if (At0 == 2) {
                            str = "Foreground";
                        } else {
                            str = "ForNonUiThread";
                        }
                        r3.A03(str);
                        r2.A04(r3.A01(), "KeepExisting");
                    }
                }
                C005505z.A00(1748627688);
                return;
            }
        }
        C005505z.A00(i);
    }

    public void onCollectionInvalidated() {
        ((C24321Td) this.A08.get()).A03(AnonymousClass07B.A00);
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
        MontageOmnistoreDeltaHandler montageOmnistoreDeltaHandler = (MontageOmnistoreDeltaHandler) this.A07.get();
        montageOmnistoreDeltaHandler.A00 = false;
        MontageOmnistoreDeltaHandler.A01(montageOmnistoreDeltaHandler);
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
        ((MontageOmnistoreDeltaHandler) this.A07.get()).A00 = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0321, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0322, code lost:
        if (r2 != null) goto L_0x0324;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0327 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C32171lK provideSubscriptionInfo(com.facebook.omnistore.Omnistore r14) {
        /*
            r13 = this;
            int r1 = X.AnonymousClass1Y3.BRG
            X.0UN r0 = r13.A00
            r3 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0yl r0 = (X.C17350yl) r0
            boolean r0 = r0.A09()
            if (r0 != 0) goto L_0x0014
            X.1lK r0 = X.C32171lK.A03
            return r0
        L_0x0014:
            java.lang.String r0 = r13.getCollectionLabel()
            com.facebook.omnistore.CollectionName$Builder r1 = r14.createCollectionNameBuilder(r0)
            X.0Tq r0 = r13.A09
            java.lang.Object r0 = r0.get()
            java.lang.String r0 = (java.lang.String) r0
            r1.addSegment(r0)
            r1.addDeviceId()
            com.facebook.omnistore.CollectionName r6 = r1.build()
            r13.A01 = r6
            X.1lI r4 = new X.1lI
            r4.<init>()
            java.lang.String r2 = ""
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02f6 }
            r10.<init>()     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r9 = "num_reactions"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            int r7 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 1
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1Yd r8 = (X.C25051Yd) r8     // Catch:{ JSONException -> 0x02f6 }
            r0 = 563976455520913(0x200ef004b0291, double:2.78641391736197E-309)
            r7 = 10
            int r0 = r8.AqL(r0, r7)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r10 = r10.put(r9, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r9 = "num_reaction_actions"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            int r7 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 1
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1Yd r8 = (X.C25051Yd) r8     // Catch:{ JSONException -> 0x02f6 }
            r0 = 563976455455376(0x200ef004a0290, double:2.786413917038174E-309)
            r7 = 10
            int r0 = r8.AqL(r0, r7)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r8 = r10.put(r9, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r7 = "image_full_screen_size"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            boolean r0 = r0.A0D()     // Catch:{ JSONException -> 0x02f6 }
            r9 = 6
            if (r0 == 0) goto L_0x02e6
            int r1 = X.AnonymousClass1Y3.BCz     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x02f6 }
            int r0 = r0.A05()     // Catch:{ JSONException -> 0x02f6 }
        L_0x00a9:
            org.json.JSONObject r8 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r7 = "image_preview_size"
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x02f6 }
            int r0 = r0.A0E()     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r8 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r7 = "image_large_preview_size"
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x02f6 }
            int r0 = r0.A0C()     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r9 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r8 = "shouldFetchEntMedia"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            int r7 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 1
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1Yd r7 = (X.C25051Yd) r7     // Catch:{ JSONException -> 0x02f6 }
            r0 = 2306125510701549164(0x200100ef00d5066c, double:1.5852374031824215E-154)
            boolean r0 = r7.Aem(r0)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r9 = r9.put(r8, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r8 = "shouldFetchLargePreview"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            int r7 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 1
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r7, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1Yd r7 = (X.C25051Yd) r7     // Catch:{ JSONException -> 0x02f6 }
            r0 = 2306125510701614701(0x200100ef00d6066d, double:1.5852374032041285E-154)
            boolean r0 = r7.Aem(r0)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r8 = r9.put(r8, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r7 = "preset_image_scale"
            r9 = 1
            int r1 = X.AnonymousClass1Y3.A6S     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ JSONException -> 0x02f6 }
            android.content.res.Resources r0 = r0.getResources()     // Catch:{ JSONException -> 0x02f6 }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ JSONException -> 0x02f6 }
            float r0 = r0.density     // Catch:{ JSONException -> 0x02f6 }
            double r0 = (double) r0     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r8 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x02f6 }
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            boolean r0 = r0.A0E()     // Catch:{ JSONException -> 0x02f6 }
            if (r0 == 0) goto L_0x0152
            com.facebook.omnistore.CollectionName r0 = r13.A01     // Catch:{ JSONException -> 0x02f6 }
            if (r0 == 0) goto L_0x0152
            java.lang.String r1 = "collection_name"
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x02f6 }
            r8.put(r1, r0)     // Catch:{ JSONException -> 0x02f6 }
        L_0x0152:
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02f6 }
            r7.<init>()     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r1 = "top_level_list_path"
            java.lang.String r0 = "viewer.montage_threads.nodes"
            org.json.JSONObject r7 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r1 = "internal_level_list_path"
            java.lang.String r0 = "montage_messages.nodes"
            org.json.JSONObject r1 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = "object_path"
            org.json.JSONObject r7 = r1.put(r0, r2)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r1 = "primary_key_path"
            java.lang.String r0 = "id"
            org.json.JSONObject r7 = r7.put(r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r12 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r8.toString()     // Catch:{ JSONException -> 0x02f6 }
            r12.<init>(r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r11 = "num_threads"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            int r9 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 1
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r0, r9, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1Yd r10 = (X.C25051Yd) r10     // Catch:{ JSONException -> 0x02f6 }
            r0 = 563976455586450(0x200ef004c0292, double:2.786413917685765E-309)
            r9 = 40
            int r0 = r10.AqL(r0, r9)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r11 = r12.put(r11, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r10 = "num_messages_in_thread"
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            int r5 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 1
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r0, r5, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1Yd r9 = (X.C25051Yd) r9     // Catch:{ JSONException -> 0x02f6 }
            r0 = 563976455324302(0x200ef0048028e, double:2.78641391639058E-309)
            r5 = 100
            int r0 = r9.AqL(r0, r5)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r10 = r11.put(r10, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r9 = "supported_story_types"
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ JSONException -> 0x02f6 }
            com.google.common.collect.ImmutableList$Builder r11 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ JSONException -> 0x02f6 }
            r11.<init>()     // Catch:{ JSONException -> 0x02f6 }
            X.1mG r0 = X.C32741mG.A04     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x02f6 }
            r11.add(r0)     // Catch:{ JSONException -> 0x02f6 }
            int r1 = X.AnonymousClass1Y3.BRG     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            boolean r0 = r0.A0F()     // Catch:{ JSONException -> 0x02f6 }
            if (r0 == 0) goto L_0x01f5
            X.1mG r0 = X.C32741mG.A03     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x02f6 }
            r11.add(r0)     // Catch:{ JSONException -> 0x02f6 }
        L_0x01f5:
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ JSONException -> 0x02f6 }
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r0.A00     // Catch:{ JSONException -> 0x02f6 }
            r12 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r12, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ JSONException -> 0x02f6 }
            r0 = 296(0x128, float:4.15E-43)
            boolean r0 = r1.AbO(r0, r12)     // Catch:{ JSONException -> 0x02f6 }
            if (r0 == 0) goto L_0x0219
            X.1mG r0 = X.C32741mG.A02     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x02f6 }
            r11.add(r0)     // Catch:{ JSONException -> 0x02f6 }
        L_0x0219:
            com.google.common.collect.ImmutableList r0 = r11.build()     // Catch:{ JSONException -> 0x02f6 }
            r5.<init>(r0)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r10 = r10.put(r9, r5)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r8.toString()     // Catch:{ JSONException -> 0x02f6 }
            r5.<init>(r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r1 = "story_id"
            java.lang.String r0 = "<ID>"
            org.json.JSONObject r9 = r5.put(r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r8.toString()     // Catch:{ JSONException -> 0x02f6 }
            r5.<init>(r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r1 = "story_ids"
            java.lang.String r0 = "<IDs>"
            org.json.JSONObject r8 = r5.put(r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02f6 }
            r1.<init>()     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = "render_object_list_query_params"
            org.json.JSONObject r1 = r1.put(r0, r10)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = "render_object_list_graphql_params"
            org.json.JSONObject r11 = r1.put(r0, r7)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r10 = "render_object_list_query_id"
            int r5 = X.AnonymousClass1Y3.A6K     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 5
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r5, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1nb r7 = (X.C33451nb) r7     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r5 = "com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent"
            java.lang.String r1 = "OmnistoreMontageListQuery.params.json"
            java.lang.Long r0 = r7.A01(r1, r10, r5)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r11 = r11.put(r10, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r10 = "render_object_query_id"
            int r5 = X.AnonymousClass1Y3.A6K     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 5
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r5, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1nb r7 = (X.C33451nb) r7     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r5 = "com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent"
            java.lang.String r1 = "OmnistoreMontageListQuery.params.json"
            java.lang.Long r0 = r7.A01(r1, r10, r5)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r1 = r11.put(r10, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = "render_object_query_params"
            org.json.JSONObject r10 = r1.put(r0, r9)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r9 = "render_multi_objects_query_id"
            int r5 = X.AnonymousClass1Y3.A6K     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r1 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            r0 = 5
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r5, r1)     // Catch:{ JSONException -> 0x02f6 }
            X.1nb r7 = (X.C33451nb) r7     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r5 = "com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent"
            java.lang.String r1 = "OmnistoreMontageMultiObjectsQuery.params.json"
            java.lang.String r0 = "query_doc_id"
            java.lang.Long r0 = r7.A01(r1, r0, r5)     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r1 = r10.put(r9, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = "render_multi_objects_query_params"
            org.json.JSONObject r8 = r1.put(r0, r8)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r7 = "app_id"
            r5 = 7
            int r1 = X.AnonymousClass1Y3.B3a     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.00x r0 = (X.C001300x) r0     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r0.A04     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r8 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r7 = "app_version"
            r5 = 8
            int r1 = X.AnonymousClass1Y3.BMV     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.0hF r0 = (X.C09400hF) r0     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r0 = r0.A02()     // Catch:{ JSONException -> 0x02f6 }
            org.json.JSONObject r5 = r8.put(r7, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r1 = "trigger_delete_field"
            java.lang.String r0 = "1"
            org.json.JSONObject r0 = r5.put(r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            java.lang.String r2 = r0.toString()     // Catch:{ JSONException -> 0x02f6 }
            goto L_0x02f6
        L_0x02e6:
            int r1 = X.AnonymousClass1Y3.BCz     // Catch:{ JSONException -> 0x02f6 }
            X.0UN r0 = r13.A00     // Catch:{ JSONException -> 0x02f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ JSONException -> 0x02f6 }
            X.1tr r0 = (X.C36821tr) r0     // Catch:{ JSONException -> 0x02f6 }
            int r0 = X.C36821tr.A01(r0)     // Catch:{ JSONException -> 0x02f6 }
            goto L_0x00a9
        L_0x02f6:
            r4.A02 = r2
            r2 = 1
            int r1 = X.AnonymousClass1Y3.A6S     // Catch:{ IOException -> 0x0328 }
            X.0UN r0 = r13.A00     // Catch:{ IOException -> 0x0328 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IOException -> 0x0328 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ IOException -> 0x0328 }
            android.content.res.AssetManager r1 = r0.getAssets()     // Catch:{ IOException -> 0x0328 }
            java.lang.String r0 = "FBMMontageMessageInfo.fbs"
            java.io.InputStream r2 = r1.open(r0)     // Catch:{ IOException -> 0x0328 }
            int r0 = r2.available()     // Catch:{ all -> 0x031f }
            byte[] r1 = new byte[r0]     // Catch:{ all -> 0x031f }
            r2.read(r1)     // Catch:{ all -> 0x031f }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x031f }
            r0.<init>(r1)     // Catch:{ all -> 0x031f }
            r2.close()     // Catch:{ IOException -> 0x0328 }
            goto L_0x033d
        L_0x031f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0321 }
        L_0x0321:
            r0 = move-exception
            if (r2 == 0) goto L_0x0327
            r2.close()     // Catch:{ all -> 0x0327 }
        L_0x0327:
            throw r0     // Catch:{ IOException -> 0x0328 }
        L_0x0328:
            r5 = move-exception
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r13.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent"
            java.lang.String r0 = "Failed to read idl from file"
            r2.softReport(r1, r0, r5)
            java.lang.String r0 = ""
        L_0x033d:
            r4.A03 = r0
            java.lang.String r0 = r13.A01()
            r4.A04 = r0
            r4.A00 = r3
            X.1lJ r0 = new X.1lJ
            r0.<init>(r4)
            X.1lK r0 = X.C32171lK.A00(r6, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent.provideSubscriptionInfo(com.facebook.omnistore.Omnistore):X.1lK");
    }

    private MontageOmnistoreComponent(AnonymousClass1XY r3, AnonymousClass12F r4) {
        this.A00 = new AnonymousClass0UN(14, r3);
        this.A09 = AnonymousClass0XJ.A0L(r3);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.BTa, r3);
        this.A08 = AnonymousClass0VB.A00(AnonymousClass1Y3.AOY, r3);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.BCG, r3);
        AnonymousClass12H r1 = this.A06;
        synchronized (r4) {
            r4.A01.add(r1);
        }
    }

    public Long A03() {
        int length;
        String A012 = A01();
        if (this.A02 == 0) {
            String[] split = A012.split(":");
            if (!C06850cB.A0B(A012) && (length = split.length) >= 1) {
                this.A02 = Long.parseLong(split[length - 1]);
            }
        }
        return Long.valueOf(this.A02);
    }

    public void A04() {
        String str;
        if (A02(this) && this.A04.compareAndSet(false, true)) {
            C188715q r4 = new C188715q(this);
            long At0 = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C195518s) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AQC, this.A00)).A00)).At0(563976454996617L);
            if (At0 == 3 && ((AnonymousClass1Y6) AnonymousClass1XX.A02(10, AnonymousClass1Y3.APr, this.A00)).BHM()) {
                At0 = 2;
            }
            if (At0 == 1) {
                AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B7B, this.A00), r4, -1659457548);
            } else if (At0 == 3) {
                r4.run();
            } else {
                AnonymousClass108 r3 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, this.A00);
                AnonymousClass0g4 r2 = (AnonymousClass0g4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ASI, this.A00);
                r3.A02(r4);
                r3.A02 = "MontageOmnistoreTryToGetCollection";
                if (At0 == 2) {
                    str = "Foreground";
                } else {
                    str = "ForNonUiThread";
                }
                r3.A03(str);
                r2.A04(r3.A01(), "KeepExisting");
            }
        }
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        try {
            return C74463ht.A01(byteBuffer);
        } catch (Exception e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).softReport("com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent", "indexObject", e);
            return new IndexedFields();
        }
    }

    public void Boz(int i) {
    }
}
