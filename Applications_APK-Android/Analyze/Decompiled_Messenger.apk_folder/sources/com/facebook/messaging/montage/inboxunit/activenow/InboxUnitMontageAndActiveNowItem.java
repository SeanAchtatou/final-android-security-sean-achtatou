package com.facebook.messaging.montage.inboxunit.activenow;

import X.AnonymousClass1I5;
import X.C24971Xv;
import X.C27161ck;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.messaging.montage.model.MontageInboxNuxItem;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;

public final class InboxUnitMontageAndActiveNowItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1I5();
    public final MontageInboxNuxItem A00;
    public final ImmutableList A01;
    public final ImmutableList A02;
    public final ImmutableList A03;
    public final ImmutableList A04;
    public final boolean A05;

    public void A0B(int i) {
        super.A0B(i);
        C24971Xv it = this.A03.iterator();
        while (it.hasNext()) {
            ((InboxMontageItem) it.next()).A0B(i);
        }
        C24971Xv it2 = this.A01.iterator();
        while (it2.hasNext()) {
            ((InboxUnitMontageActiveNowItem) it2.next()).A0B(i);
        }
    }

    public void A0D(int i) {
        super.A0D(i);
        C24971Xv it = this.A03.iterator();
        while (it.hasNext()) {
            ((InboxMontageItem) it.next()).A0D(i);
        }
        C24971Xv it2 = this.A01.iterator();
        while (it2.hasNext()) {
            ((InboxUnitMontageActiveNowItem) it2.next()).A0D(i);
        }
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeList(this.A03);
        parcel.writeList(this.A01);
        parcel.writeList(this.A04);
        parcel.writeList(this.A02);
        C417826y.A0V(parcel, this.A05);
        MontageInboxNuxItem montageInboxNuxItem = this.A00;
        if (montageInboxNuxItem != null) {
            montageInboxNuxItem.writeToParcel(parcel, i);
        }
    }

    public InboxUnitMontageAndActiveNowItem(C27161ck r1, ImmutableList immutableList, ImmutableList immutableList2, ImmutableList immutableList3, ImmutableList immutableList4, MontageInboxNuxItem montageInboxNuxItem, boolean z) {
        super(r1);
        this.A03 = immutableList == null ? RegularImmutableList.A02 : immutableList;
        this.A01 = immutableList2 == null ? RegularImmutableList.A02 : immutableList2;
        this.A04 = immutableList3 == null ? RegularImmutableList.A02 : immutableList3;
        this.A02 = immutableList4 == null ? RegularImmutableList.A02 : immutableList4;
        this.A00 = montageInboxNuxItem;
        this.A05 = z;
    }

    public InboxUnitMontageAndActiveNowItem(Parcel parcel) {
        super(parcel);
        this.A03 = C417826y.A06(parcel, InboxMontageItem.CREATOR);
        this.A01 = C417826y.A06(parcel, InboxUnitMontageActiveNowItem.CREATOR);
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, Integer.class.getClassLoader());
        this.A04 = ImmutableList.copyOf((Collection) arrayList);
        this.A02 = C417826y.A06(parcel, UserKey.CREATOR);
        this.A05 = C417826y.A0W(parcel);
        this.A00 = (MontageInboxNuxItem) MontageInboxNuxItem.CREATOR.createFromParcel(parcel);
    }
}
