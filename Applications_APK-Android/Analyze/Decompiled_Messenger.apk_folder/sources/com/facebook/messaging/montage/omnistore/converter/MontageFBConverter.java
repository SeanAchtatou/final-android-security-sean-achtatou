package com.facebook.messaging.montage.omnistore.converter;

import X.AnonymousClass09P;
import X.AnonymousClass1T9;
import X.AnonymousClass1TU;
import X.AnonymousClass1XY;
import X.AnonymousClass2UN;
import X.AnonymousClass2UX;
import X.AnonymousClass2V7;
import X.C04750Wa;
import X.C05540Zi;
import X.C10950l8;
import X.C17920zh;
import X.C24971Xv;
import X.C28831fR;
import X.C28931fb;
import X.C33531nj;
import X.C33571nn;
import X.C33801oA;
import X.C33881oI;
import X.C36841tx;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.MontageThreadPreview;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.model.MontageThreadInfo;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

@UserScoped
public final class MontageFBConverter {
    private static C05540Zi A05;
    public final AnonymousClass09P A00;
    public final C33571nn A01;
    public final C33801oA A02;
    public final MontageMessageFBConverter A03;
    public final AnonymousClass1TU A04;

    private static ParticipantInfo A00(C33531nj r4) {
        String str;
        String str2;
        AnonymousClass2UX A06 = r4.A01.A07().A06();
        int A022 = A06.A02(6);
        if (A022 != 0) {
            str = A06.A05(A022 + A06.A00);
        } else {
            str = null;
        }
        UserKey A012 = UserKey.A01(AnonymousClass2UN.A00(str));
        int A023 = A06.A02(8);
        if (A023 != 0) {
            str2 = A06.A05(A023 + A06.A00);
        } else {
            str2 = null;
        }
        return new ParticipantInfo(A012, AnonymousClass2UN.A01(str2));
    }

    public static final MontageFBConverter A01(AnonymousClass1XY r4) {
        MontageFBConverter montageFBConverter;
        synchronized (MontageFBConverter.class) {
            C05540Zi A002 = C05540Zi.A00(A05);
            A05 = A002;
            try {
                if (A002.A03(r4)) {
                    A05.A00 = new MontageFBConverter((AnonymousClass1XY) A05.A01());
                }
                C05540Zi r1 = A05;
                montageFBConverter = (MontageFBConverter) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A05.A02();
                throw th;
            }
        }
        return montageFBConverter;
    }

    public Message A02(C36841tx r3) {
        C33801oA r1 = this.A02;
        Preconditions.checkNotNull(r3);
        return this.A03.A05(C33801oA.A00(r1, r3.A0A()), r3);
    }

    private MontageFBConverter(AnonymousClass1XY r2) {
        this.A00 = C04750Wa.A01(r2);
        this.A03 = new MontageMessageFBConverter(r2);
        this.A01 = new C33571nn(r2);
        this.A04 = AnonymousClass1TU.A05(r2);
        this.A02 = new C33801oA(r2);
    }

    public MontageThreadInfo A03(C33531nj r8) {
        ImmutableList immutableList;
        int i;
        ImmutableList build = ImmutableList.builder().build();
        ThreadKey A002 = C33801oA.A00(this.A02, r8.A01);
        try {
            MontageMessageFBConverter montageMessageFBConverter = this.A03;
            ImmutableList immutableList2 = r8.A02;
            ImmutableList.Builder builder = new ImmutableList.Builder();
            C24971Xv it = immutableList2.iterator();
            while (it.hasNext()) {
                Message A052 = montageMessageFBConverter.A05(A002, (C36841tx) it.next());
                if (A052 != null && !montageMessageFBConverter.A03.A0K(A052)) {
                    builder.add((Object) A052);
                }
            }
            ImmutableList reverse = builder.build().reverse();
            C33881oI r1 = new C33881oI();
            r1.A00 = A002;
            r1.A01(reverse);
            r1.A03 = true;
            immutableList = r1.A00().A01.reverse();
        } catch (Exception e) {
            this.A00.softReport("com.facebook.messaging.montage.omnistore.converter.MontageFBConverter", e.getMessage(), e);
            immutableList = RegularImmutableList.A02;
        }
        AnonymousClass1T9 r4 = r8.A01;
        int A022 = r4.A02(6);
        if (A022 != 0) {
            i = r4.A01.getInt(A022 + r4.A00);
        } else {
            i = 0;
        }
        MontageThreadPreview A0F = this.A04.A0F(immutableList);
        C17920zh A003 = ThreadSummary.A00();
        A003.A0N = C10950l8.A06;
        A003.A0A = r8.A00;
        A003.A0R = C33801oA.A00(this.A02, r8.A01);
        C28831fR r12 = new C28831fR();
        r12.A04 = A00(r8);
        A003.A03(ImmutableList.of(new ThreadParticipant(r12)));
        A003.A0v = ImmutableList.of(A00(r8));
        A003.A0W = A0F;
        AnonymousClass2V7 A004 = MontageThreadInfo.A00(immutableList, i, A003.A00());
        A004.A03 = build;
        C28931fb.A06(build, "seenByUserList");
        A004.A04.add("seenByUserList");
        return new MontageThreadInfo(A004);
    }
}
