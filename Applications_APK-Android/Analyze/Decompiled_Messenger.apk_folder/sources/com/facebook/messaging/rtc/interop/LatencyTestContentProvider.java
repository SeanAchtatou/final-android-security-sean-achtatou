package com.facebook.messaging.rtc.interop;

import X.AnonymousClass04e;
import X.AnonymousClass09C;
import X.AnonymousClass09P;
import X.AnonymousClass0TM;
import X.AnonymousClass0TN;
import X.AnonymousClass0TV;
import X.AnonymousClass0TW;
import X.AnonymousClass0TX;
import X.AnonymousClass0UN;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.net.Uri;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.util.concurrent.atomic.AtomicBoolean;

public final class LatencyTestContentProvider extends ContentProvider implements AnonymousClass09C {
    public AnonymousClass09P A00;
    public AnonymousClass0UN A01;
    public AnonymousClass04e A02;
    public AnonymousClass0TW A03;
    public final AtomicBoolean A04 = new AtomicBoolean();
    private final String[] A05 = {"user_id"};

    public int delete(Uri uri, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }

    public String getType(Uri uri) {
        throw new UnsupportedOperationException();
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException();
    }

    public boolean onCreate() {
        this.A02 = new AnonymousClass0TM(this);
        this.A03 = new AnonymousClass0TW(AnonymousClass0TX.A00(AnonymousClass0TN.A0l, AnonymousClass0TV.A00(TurboLoader.Locator.$const$string(18))));
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0335, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007f, code lost:
        if (X.C185528jI.A00(r4, r1).contains(r5) == false) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01da, code lost:
        if (r1 != false) goto L_0x01dd;
     */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0374 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0238 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.database.Cursor query(android.net.Uri r25, java.lang.String[] r26, java.lang.String r27, java.lang.String[] r28, java.lang.String r29) {
        /*
            r24 = this;
            r2 = r24
            android.content.Context r4 = r2.getContext()
            X.0TW r1 = r2.A03
            if (r4 != 0) goto L_0x0043
            r0 = 0
        L_0x000b:
            if (r0 == 0) goto L_0x01dc
            X.0bo r0 = X.AnonymousClass0TW.A01(r4)
            java.util.List r1 = r0.A04
            if (r1 == 0) goto L_0x01a2
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x01a2
            java.util.Iterator r5 = r1.iterator()
        L_0x001f:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x01a2
            java.lang.Object r1 = r5.next()
            java.lang.String r1 = (java.lang.String) r1
            boolean r0 = X.C185528jI.A01(r4, r1)
            if (r0 == 0) goto L_0x0037
            X.04e r6 = r2.A02
            java.lang.Class<X.8jI> r5 = X.C185528jI.class
            monitor-enter(r5)
            goto L_0x004c
        L_0x0037:
            X.04e r3 = r2.A02
            java.lang.String r0 = "App %s is not FbPermission signed"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1)
            r3.C2S(r0)
            goto L_0x001f
        L_0x0043:
            X.0bo r0 = X.AnonymousClass0TW.A01(r4)
            boolean r0 = X.AnonymousClass0TW.A04(r1, r0, r4)
            goto L_0x000b
        L_0x004c:
            java.lang.Class<X.8jI> r3 = X.C185528jI.class
            monitor-enter(r3)     // Catch:{ all -> 0x019f }
            X.8jI r0 = X.C185528jI.A01     // Catch:{ all -> 0x019c }
            if (r0 != 0) goto L_0x005a
            X.8jI r0 = new X.8jI     // Catch:{ all -> 0x019c }
            r0.<init>()     // Catch:{ all -> 0x019c }
            X.C185528jI.A01 = r0     // Catch:{ all -> 0x019c }
        L_0x005a:
            X.8jI r23 = X.C185528jI.A01     // Catch:{ all -> 0x019c }
            monitor-exit(r3)     // Catch:{ all -> 0x019f }
            r0 = r23
            r0.A00 = r6     // Catch:{ all -> 0x019f }
            monitor-exit(r5)
            java.lang.String r22 = "com.facebook.fbpermission.MN_PROVIDER_LATENCY_TEST"
            r5 = r22
            java.util.Set r3 = X.AnonymousClass0TN.A0e
            java.lang.String r0 = r4.getPackageName()
            X.0TO r0 = X.C28061eC.A02(r4, r0)
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0081
            java.util.List r0 = X.C185528jI.A00(r4, r1)
            boolean r3 = r0.contains(r5)
            r0 = 1
            if (r3 != 0) goto L_0x0082
        L_0x0081:
            r0 = 0
        L_0x0082:
            r21 = 1
            if (r0 != 0) goto L_0x01d8
            r6 = r5
            boolean r0 = X.C185528jI.A01(r4, r1)
            if (r0 == 0) goto L_0x0399
            java.lang.String r12 = r4.getPackageName()
            java.lang.String r9 = "signatures"
            X.444 r10 = new X.444
            r10.<init>()
            r3 = 0
            r0 = 0
            android.content.Context r0 = r4.createPackageContext(r1, r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            android.content.res.AssetManager r7 = r0.getAssets()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = "fbpermissions.json"
            java.io.InputStream r0 = r7.open(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r5.<init>(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r8.<init>(r5)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r7.<init>()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
        L_0x00b7:
            java.lang.String r0 = r8.readLine()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            if (r0 == 0) goto L_0x00c1
            r7.append(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            goto L_0x00b7
        L_0x00c1:
            r8.close()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            int r0 = r7.length()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            if (r0 != 0) goto L_0x00dd
            r0 = r23
            X.04e r7 = r0.A00     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r5 = "Consumer app '%s' has an empty FbPermissions asset file"
            java.lang.Object[] r0 = new java.lang.Object[]{r1}     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = java.lang.String.format(r5, r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r7.C2S(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            goto L_0x01a5
        L_0x00dd:
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = r7.toString()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r5.<init>(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            boolean r0 = r5.has(r12)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            if (r0 == 0) goto L_0x01a5
            org.json.JSONObject r11 = r5.getJSONObject(r12)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = "permissions"
            org.json.JSONArray r8 = r11.getJSONArray(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            int r0 = r8.length()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            if (r0 != 0) goto L_0x010f
            r0 = r23
            X.04e r7 = r0.A00     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r5 = "Consumer app '%s' has an empty permissions list for '%s' provider"
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r12}     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = java.lang.String.format(r5, r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r7.C2S(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            goto L_0x01a5
        L_0x010f:
            r7 = 0
        L_0x0110:
            int r0 = r8.length()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            if (r7 >= r0) goto L_0x0122
            java.util.Set r5 = r10.A00     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = r8.getString(r7)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r5.add(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            int r7 = r7 + 1
            goto L_0x0110
        L_0x0122:
            boolean r0 = r11.has(r9)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            if (r0 == 0) goto L_0x012d
            org.json.JSONArray r9 = r11.getJSONArray(r9)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            goto L_0x013b
        L_0x012d:
            org.json.JSONArray r9 = new org.json.JSONArray     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r9.<init>()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = "signature"
            org.json.JSONObject r0 = r11.getJSONObject(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r9.put(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
        L_0x013b:
            r8 = 0
        L_0x013c:
            int r0 = r9.length()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            if (r8 >= r0) goto L_0x01a4
            org.json.JSONObject r7 = r9.getJSONObject(r8)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            X.2N5 r5 = new X.2N5     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r5.<init>()     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = "algorithm"
            java.lang.String r0 = r7.getString(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r5.A00 = r0     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.lang.String r0 = "value"
            java.lang.String r0 = r7.getString(r0)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r5.A01 = r0     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            java.util.Set r0 = r10.A01     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            r0.add(r5)     // Catch:{ NameNotFoundException -> 0x018a, IOException -> 0x0163, JSONException -> 0x0173 }
            int r8 = r8 + 1
            goto L_0x013c
        L_0x0163:
            r5 = move-exception
            r0 = r23
            X.04e r7 = r0.A00
            java.lang.String r0 = r5.getMessage()
            java.lang.Object[] r5 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Failed to read FBPermission asset file from package '%s': %s"
            goto L_0x0182
        L_0x0173:
            r5 = move-exception
            r0 = r23
            X.04e r7 = r0.A00
            java.lang.String r0 = r5.getMessage()
            java.lang.Object[] r5 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Failed to decode FBPermission asset file from package '%s' due to JSON exception: %s"
        L_0x0182:
            java.lang.String r0 = java.lang.String.format(r0, r5)
            r7.C2S(r0)
            goto L_0x01a5
        L_0x018a:
            r0 = r23
            X.04e r7 = r0.A00
            java.lang.Object[] r5 = new java.lang.Object[]{r1}
            java.lang.String r0 = "Cannot create package context for '%s'"
            java.lang.String r0 = java.lang.String.format(r0, r5)
            r7.C2S(r0)
            goto L_0x01a5
        L_0x019c:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x019f }
            throw r0     // Catch:{ all -> 0x019f }
        L_0x019f:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x01a2:
            r1 = 0
            goto L_0x01d9
        L_0x01a4:
            r3 = r10
        L_0x01a5:
            if (r3 != 0) goto L_0x01e1
            r0 = r23
            X.04e r4 = r0.A00
            java.lang.Object[] r3 = new java.lang.Object[]{r1}
            java.lang.String r0 = "Failed to read fb permissions from '%s' asset"
        L_0x01b1:
            java.lang.String r0 = java.lang.String.format(r0, r3)
            r4.C2S(r0)
            r0 = 0
        L_0x01b9:
            if (r0 != 0) goto L_0x01d8
            r0 = r22
            java.lang.Object[] r1 = new java.lang.Object[]{r0, r1}
            java.lang.String r0 = "FBPermission '%s' was not granted to package '%s'"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "%s; request is allowed for fail-open"
            java.lang.String r1 = java.lang.String.format(r0, r1)
            r0 = r23
            X.04e r0 = r0.A00
            r0.C2S(r1)
        L_0x01d8:
            r1 = 1
        L_0x01d9:
            r0 = 1
            if (r1 != 0) goto L_0x01dd
        L_0x01dc:
            r0 = 0
        L_0x01dd:
            if (r0 != 0) goto L_0x03be
            r0 = 0
            return r0
        L_0x01e1:
            java.util.Set r0 = r3.A00
            boolean r0 = r0.contains(r6)
            if (r0 != 0) goto L_0x01f4
            r0 = r23
            X.04e r4 = r0.A00
            java.lang.Object[] r3 = new java.lang.Object[]{r22}
            java.lang.String r0 = "Missing FBPermission '%s'"
            goto L_0x01b1
        L_0x01f4:
            java.util.Set r0 = r3.A01
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0207
            r0 = r23
            X.04e r4 = r0.A00
            java.lang.Object[] r3 = new java.lang.Object[]{r6, r1}
            java.lang.String r0 = "Missing signature entry while verifying '%s' from package '%s'"
            goto L_0x01b1
        L_0x0207:
            X.0TO r0 = X.C28061eC.A02(r4, r1)     // Catch:{ SecurityException -> 0x03aa }
            java.lang.String r0 = r0.sha256Hash     // Catch:{ SecurityException -> 0x03aa }
            r20 = r0
            r8 = 0
            android.content.pm.PackageManager r5 = r4.getPackageManager()     // Catch:{ NameNotFoundException | RuntimeException -> 0x0222 }
            r0 = 0
            android.content.pm.PackageInfo r7 = r5.getPackageInfo(r1, r0)     // Catch:{ NameNotFoundException | RuntimeException -> 0x0222 }
            int r6 = android.os.Build.VERSION.SDK_INT
            r5 = 28
            r0 = 0
            if (r6 < r5) goto L_0x0226
            goto L_0x0225
        L_0x0222:
            r17 = r8
            goto L_0x022c
        L_0x0225:
            r0 = 1
        L_0x0226:
            if (r0 == 0) goto L_0x038c
            long r17 = r7.getLongVersionCode()
        L_0x022c:
            if (r20 == 0) goto L_0x039c
            int r0 = (r17 > r8 ? 1 : (r17 == r8 ? 0 : -1))
            if (r0 <= 0) goto L_0x039c
            java.util.Set r0 = r3.A01
            java.util.Iterator r19 = r0.iterator()
        L_0x0238:
            boolean r0 = r19.hasNext()
            if (r0 == 0) goto L_0x0399
            java.lang.Object r0 = r19.next()
            X.2N5 r0 = (X.AnonymousClass2N5) r0
            java.lang.String r8 = r0.A00
            java.lang.String r5 = r0.A01
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 != 0) goto L_0x0377
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 != 0) goto L_0x0377
            java.lang.String r11 = java.lang.String.valueOf(r17)
            java.util.Set r12 = r3.A00
            java.lang.String r10 = r4.getPackageName()
            android.content.pm.PackageInfo r0 = X.C28061eC.A00(r4, r10)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            android.content.pm.Signature r9 = X.C28061eC.A01(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.lang.String r0 = "X.509"
            java.security.cert.CertificateFactory r7 = java.security.cert.CertificateFactory.getInstance(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.io.ByteArrayInputStream r6 = new java.io.ByteArrayInputStream     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            byte[] r0 = r9.toByteArray()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r6.<init>(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.security.cert.Certificate r0 = r7.generateCertificate(r6)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.security.PublicKey r7 = r0.getPublicKey()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            X.8iQ r9 = new X.8iQ     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r9.<init>()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r6 = 1
            int r0 = r9.A00     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r13 = 255(0xff, float:3.57E-43)
            if (r0 >= r13) goto L_0x0326
            java.util.TreeSet r14 = new java.util.TreeSet     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r14.<init>(r12)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            int r0 = r14.size()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            if (r0 > r13) goto L_0x032e
            java.util.ArrayList r12 = new java.util.ArrayList     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r12.<init>()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.util.Iterator r16 = r14.iterator()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
        L_0x029d:
            boolean r0 = r16.hasNext()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            if (r0 == 0) goto L_0x02be
            java.lang.Object r15 = r16.next()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.lang.String r15 = (java.lang.String) r15     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.nio.charset.Charset r0 = X.C185058iQ.A02     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            byte[] r15 = r15.getBytes(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            int r0 = r15.length     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            if (r0 > r13) goto L_0x02b6
            r12.add(r15)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            goto L_0x029d
        L_0x02b6:
            X.1wa r5 = new X.1wa     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.lang.String r0 = "String size (UTF-8 encoded) cannot exceed 255"
            r5.<init>(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            goto L_0x0335
        L_0x02be:
            java.io.ByteArrayOutputStream r0 = r9.A01     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0.write(r6)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.io.ByteArrayOutputStream r6 = r9.A01     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            int r0 = r14.size()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = r0 & r13
            r6.write(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.util.Iterator r14 = r12.iterator()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
        L_0x02d1:
            boolean r0 = r14.hasNext()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            if (r0 == 0) goto L_0x02ec
            java.lang.Object r13 = r14.next()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            byte[] r13 = (byte[]) r13     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.io.ByteArrayOutputStream r6 = r9.A01     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            int r12 = r13.length     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = r12 & 255(0xff, float:3.57E-43)
            r6.write(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.io.ByteArrayOutputStream r6 = r9.A01     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = 0
            r6.write(r13, r0, r12)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            goto L_0x02d1
        L_0x02ec:
            int r0 = r9.A00     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            int r0 = r0 + 1
            r9.A00 = r0     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = 2
            r9.A00(r1, r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = 3
            r9.A00(r11, r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r6 = 4
            r0 = r20
            r9.A00(r0, r6)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = 5
            r9.A00(r10, r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.io.ByteArrayOutputStream r0 = r9.A01     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            byte[] r6 = r0.toByteArray()     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            int r0 = r9.A00     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            byte r0 = (byte) r0     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r6[r21] = r0     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0 = 10
            byte[] r5 = android.util.Base64.decode(r5, r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.security.Signature r0 = java.security.Signature.getInstance(r8)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0.initVerify(r7)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            r0.update(r6)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            boolean r0 = r0.verify(r5)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            goto L_0x0372
        L_0x0326:
            X.1wa r5 = new X.1wa     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.lang.String r0 = "Total number of entries cannot exceed 255"
            r5.<init>(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            goto L_0x0335
        L_0x032e:
            X.1wa r5 = new X.1wa     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
            java.lang.String r0 = "Collection size (duplicates removed) cannot exceed 255"
            r5.<init>(r0)     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
        L_0x0335:
            throw r5     // Catch:{ SecurityException -> 0x035e, 1wa -> 0x0354, CertificateException -> 0x034a, InvalidKeyException -> 0x0340, NoSuchAlgorithmException -> 0x0336, SignatureException -> 0x0368 }
        L_0x0336:
            r0 = r23
            X.04e r5 = r0.A00
            java.lang.String r0 = "Unsupported signature algorithm"
            r5.C2S(r0)
            goto L_0x0371
        L_0x0340:
            r0 = r23
            X.04e r5 = r0.A00
            java.lang.String r0 = "Invalid public key"
            r5.C2S(r0)
            goto L_0x0371
        L_0x034a:
            r0 = r23
            X.04e r5 = r0.A00
            java.lang.String r0 = "Failed to get public key due to certificate exception"
            r5.C2S(r0)
            goto L_0x0371
        L_0x0354:
            r0 = r23
            X.04e r5 = r0.A00
            java.lang.String r0 = "Failed to encode data using FbPermissionEncoder"
            r5.C2S(r0)
            goto L_0x0371
        L_0x035e:
            r0 = r23
            X.04e r5 = r0.A00
            java.lang.String r0 = "Failed to get provider package signature"
            r5.C2S(r0)
            goto L_0x0371
        L_0x0368:
            r0 = r23
            X.04e r5 = r0.A00
            java.lang.String r0 = "Signature type wrong or improperly encoded"
            r5.C2S(r0)
        L_0x0371:
            r0 = 0
        L_0x0372:
            if (r0 == 0) goto L_0x0238
            r0 = 1
            goto L_0x01b9
        L_0x0377:
            r0 = r23
            X.04e r6 = r0.A00
            r0 = r22
            java.lang.Object[] r5 = new java.lang.Object[]{r1, r0, r8, r5}
            java.lang.String r0 = "Invalid signature for package '%s' while verifying '%s': algorithm(%s), value(%s)"
            java.lang.String r0 = java.lang.String.format(r0, r5)
            r6.C2S(r0)
            goto L_0x0238
        L_0x038c:
            int r0 = r7.versionCode
            long r5 = (long) r0
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            long r17 = r0.longValue()
            goto L_0x022c
        L_0x0399:
            r0 = 0
            goto L_0x01b9
        L_0x039c:
            r0 = r23
            X.04e r4 = r0.A00
            r0 = r22
            java.lang.Object[] r3 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Invalid key hash or version code for package '%s' while verifying '%s'"
            goto L_0x01b1
        L_0x03aa:
            r0 = r23
            X.04e r4 = r0.A00
            java.lang.Object[] r3 = new java.lang.Object[]{r1, r6}
            java.lang.String r0 = "Invalid developer key for package '%s' while verifying '%s'"
            java.lang.String r0 = java.lang.String.format(r0, r3)
            r4.C2S(r0)
            r0 = 0
            goto L_0x01b9
        L_0x03be:
            java.util.concurrent.atomic.AtomicBoolean r4 = r2.A04
            monitor-enter(r4)
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.A04     // Catch:{ all -> 0x040c }
            boolean r0 = r0.get()     // Catch:{ all -> 0x040c }
            if (r0 != 0) goto L_0x03ec
            android.content.Context r0 = r2.getContext()     // Catch:{ all -> 0x040c }
            X.C07620dr.A00(r0)     // Catch:{ all -> 0x040c }
            android.content.Context r0 = r2.getContext()     // Catch:{ all -> 0x040c }
            X.1XX r3 = X.AnonymousClass1XX.get(r0)     // Catch:{ all -> 0x040c }
            X.0UN r1 = new X.0UN     // Catch:{ all -> 0x040c }
            r0 = 0
            r1.<init>(r0, r3)     // Catch:{ all -> 0x040c }
            r2.A01 = r1     // Catch:{ all -> 0x040c }
            X.09P r0 = X.C04750Wa.A01(r3)     // Catch:{ all -> 0x040c }
            r2.A00 = r0     // Catch:{ all -> 0x040c }
            java.util.concurrent.atomic.AtomicBoolean r1 = r2.A04     // Catch:{ all -> 0x040c }
            r0 = 1
            r1.set(r0)     // Catch:{ all -> 0x040c }
        L_0x03ec:
            monitor-exit(r4)     // Catch:{ all -> 0x040c }
            android.database.MatrixCursor r3 = new android.database.MatrixCursor
            java.lang.String[] r0 = r2.A05
            r3.<init>(r0)
            int r1 = X.AnonymousClass1Y3.Awq
            X.0UN r0 = r2.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            java.lang.String r1 = (java.lang.String) r1
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x040b
            java.lang.String[] r0 = new java.lang.String[]{r1}
            r3.addRow(r0)
        L_0x040b:
            return r3
        L_0x040c:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x040c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.rtc.interop.LatencyTestContentProvider.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor");
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException();
    }
}
