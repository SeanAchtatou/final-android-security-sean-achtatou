package com.facebook.messaging.integrity.featurelimits.omnistore;

import X.AnonymousClass0UN;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C04310Tq;
import X.C21491Hf;
import X.C21501Hg;
import X.C30281hn;
import X.C32151lI;
import X.C32161lJ;
import X.C32171lK;
import X.C33451nb;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class FeatureLimitsOmnistoreComponent implements OmnistoreComponent {
    public AnonymousClass0UN A00;
    private Collection A01;
    private final C04310Tq A02;

    public void Boz(int i) {
        if (i == 2) {
            A01();
        }
    }

    public String getCollectionLabel() {
        return "messenger_integrity_feature_limits";
    }

    public void onCollectionInvalidated() {
        this.A01 = null;
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static final FeatureLimitsOmnistoreComponent A00(AnonymousClass1XY r1) {
        return new FeatureLimitsOmnistoreComponent(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0103, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0104, code lost:
        if (r12 != null) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r12.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x0109 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A01() {
        /*
            r13 = this;
            com.facebook.omnistore.Collection r2 = r13.A01
            if (r2 == 0) goto L_0x0112
            java.lang.String r1 = ""
            r0 = -1
            r7 = 1
            com.facebook.omnistore.Cursor r12 = r2.query(r1, r0, r7)     // Catch:{ OmnistoreIOException -> 0x010a }
        L_0x000c:
            boolean r0 = r12.step()     // Catch:{ all -> 0x0101 }
            if (r0 == 0) goto L_0x00fd
            java.nio.ByteBuffer r3 = r12.getBlob()     // Catch:{ all -> 0x0101 }
            java.lang.String r4 = r12.getPrimaryKey()     // Catch:{ all -> 0x0101 }
            if (r3 == 0) goto L_0x000c
            X.7Wf r2 = new X.7Wf     // Catch:{ all -> 0x0101 }
            r2.<init>()     // Catch:{ all -> 0x0101 }
            java.nio.ByteOrder r0 = java.nio.ByteOrder.LITTLE_ENDIAN     // Catch:{ all -> 0x0101 }
            r3.order(r0)     // Catch:{ all -> 0x0101 }
            int r0 = r3.position()     // Catch:{ all -> 0x0101 }
            int r1 = r3.getInt(r0)     // Catch:{ all -> 0x0101 }
            int r0 = r3.position()     // Catch:{ all -> 0x0101 }
            int r1 = r1 + r0
            r2.A00 = r1     // Catch:{ all -> 0x0101 }
            r2.A01 = r3     // Catch:{ all -> 0x0101 }
            r0 = 4
            int r0 = r2.A02(r0)     // Catch:{ all -> 0x0101 }
            if (r0 == 0) goto L_0x00f9
            int r0 = r0 + r1
            long r2 = r3.getLong(r0)     // Catch:{ all -> 0x0101 }
        L_0x0043:
            r5 = 2
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x0101 }
            X.0UN r0 = r13.A00     // Catch:{ all -> 0x0101 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0101 }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x0101 }
            long r5 = r0.now()     // Catch:{ all -> 0x0101 }
            r10 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r10
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x006c
            int r1 = X.AnonymousClass1Y3.BR1     // Catch:{ all -> 0x0101 }
            X.0UN r0 = r13.A00     // Catch:{ all -> 0x0101 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0101 }
            X.1Hf r0 = (X.C21491Hf) r0     // Catch:{ all -> 0x0101 }
            r0.A01(r4)     // Catch:{ all -> 0x0101 }
            com.facebook.omnistore.Collection r0 = r13.A01     // Catch:{ all -> 0x0101 }
            r0.deleteObject(r4)     // Catch:{ all -> 0x0101 }
            goto L_0x000c
        L_0x006c:
            int r1 = X.AnonymousClass1Y3.BR1     // Catch:{ all -> 0x0101 }
            X.0UN r0 = r13.A00     // Catch:{ all -> 0x0101 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0101 }
            X.1Hf r1 = (X.C21491Hf) r1     // Catch:{ all -> 0x0101 }
            X.1Y8 r0 = X.C21501Hg.A00     // Catch:{ all -> 0x0101 }
            X.1Y8 r6 = r0.A09(r4)     // Catch:{ all -> 0x0101 }
            int r5 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0101 }
            X.0UN r1 = r1.A00     // Catch:{ all -> 0x0101 }
            r0 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r5, r1)     // Catch:{ all -> 0x0101 }
            com.facebook.prefs.shared.FbSharedPreferences r5 = (com.facebook.prefs.shared.FbSharedPreferences) r5     // Catch:{ all -> 0x0101 }
            r0 = 0
            long r5 = r5.At2(r6, r0)     // Catch:{ all -> 0x0101 }
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x000c
            int r1 = X.AnonymousClass1Y3.BR1     // Catch:{ all -> 0x0101 }
            X.0UN r0 = r13.A00     // Catch:{ all -> 0x0101 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0101 }
            X.1Hf r6 = (X.C21491Hf) r6     // Catch:{ all -> 0x0101 }
            X.1Y8 r5 = X.C21501Hg.A00     // Catch:{ all -> 0x0101 }
            X.1Y8 r9 = r5.A09(r4)     // Catch:{ all -> 0x0101 }
            int r8 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0101 }
            X.0UN r6 = r6.A00     // Catch:{ all -> 0x0101 }
            r5 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r8, r6)     // Catch:{ all -> 0x0101 }
            com.facebook.prefs.shared.FbSharedPreferences r5 = (com.facebook.prefs.shared.FbSharedPreferences) r5     // Catch:{ all -> 0x0101 }
            X.1hn r5 = r5.edit()     // Catch:{ all -> 0x0101 }
            r5.BzA(r9, r2)     // Catch:{ all -> 0x0101 }
            r5.commit()     // Catch:{ all -> 0x0101 }
            r5 = 3
            int r1 = X.AnonymousClass1Y3.B10     // Catch:{ all -> 0x0101 }
            X.0UN r0 = r13.A00     // Catch:{ all -> 0x0101 }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0101 }
            X.4i7 r8 = (X.C96054i7) r8     // Catch:{ all -> 0x0101 }
            boolean r0 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0101 }
            r0 = r0 ^ r7
            X.AnonymousClass0qX.A02(r0)     // Catch:{ all -> 0x0101 }
            X.0ap r1 = r8.A02     // Catch:{ all -> 0x0101 }
            java.lang.String r0 = "ENFORCE_FEATURE_LIMIT_ACTION"
            java.lang.String r0 = r1.A02(r0)     // Catch:{ all -> 0x0101 }
            android.content.Intent r5 = new android.content.Intent     // Catch:{ all -> 0x0101 }
            r5.<init>(r0)     // Catch:{ all -> 0x0101 }
            android.content.Context r0 = r8.A00     // Catch:{ all -> 0x0101 }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ all -> 0x0101 }
            android.content.Intent r1 = r5.setPackage(r0)     // Catch:{ all -> 0x0101 }
            java.lang.String r0 = "feature_limit_name"
            r1.putExtra(r0, r4)     // Catch:{ all -> 0x0101 }
            int r4 = r4.hashCode()     // Catch:{ all -> 0x0101 }
            android.content.Context r1 = r8.A00     // Catch:{ all -> 0x0101 }
            r0 = 134217728(0x8000000, float:3.85186E-34)
            android.app.PendingIntent r1 = X.C64633Cq.A01(r1, r4, r5, r0)     // Catch:{ all -> 0x0101 }
            X.25T r0 = r8.A01     // Catch:{ all -> 0x0101 }
            long r2 = r2 * r10
            r0.A03(r7, r2, r1)     // Catch:{ all -> 0x0101 }
            goto L_0x000c
        L_0x00f9:
            r2 = 0
            goto L_0x0043
        L_0x00fd:
            r12.close()     // Catch:{ OmnistoreIOException -> 0x010a }
            return
        L_0x0101:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0103 }
        L_0x0103:
            r0 = move-exception
            if (r12 == 0) goto L_0x0109
            r12.close()     // Catch:{ all -> 0x0109 }
        L_0x0109:
            throw r0     // Catch:{ OmnistoreIOException -> 0x010a }
        L_0x010a:
            r2 = move-exception
            java.lang.String r1 = "com.facebook.messaging.integrity.featurelimits.omnistore.FeatureLimitsOmnistoreComponent"
            java.lang.String r0 = "IO error while reading messenger_integrity_feature_limits collection"
            X.C010708t.A0R(r1, r2, r0)
        L_0x0112:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.integrity.featurelimits.omnistore.FeatureLimitsOmnistoreComponent.A01():void");
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    public void onCollectionAvailable(Collection collection) {
        this.A01 = collection;
        A01();
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        int i = AnonymousClass1Y3.BAo;
        AnonymousClass0UN r2 = this.A00;
        if (((Boolean) AnonymousClass1XX.A02(4, i, r2)).booleanValue()) {
            C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, ((C21491Hf) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BR1, r2)).A00)).edit();
            edit.C24(C21501Hg.A00);
            edit.commit();
            return C32171lK.A03;
        }
        CollectionName.Builder createCollectionNameWithDomainBuilder = omnistore.createCollectionNameWithDomainBuilder(getCollectionLabel(), "messenger_user_sq");
        createCollectionNameWithDomainBuilder.addSegment((String) this.A02.get());
        CollectionName build = createCollectionNameWithDomainBuilder.build();
        C32151lI r4 = new C32151lI();
        r4.A02 = new JSONObject().toString();
        r4.A03 = ((C33451nb) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6K, this.A00)).A02("messenger_integrity_feature_limits.fbs", "com.facebook.messaging.integrity.featurelimits.omnistore.FeatureLimitsOmnistoreComponent");
        r4.A04 = ((C33451nb) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6K, this.A00)).A02("messenger_integrity_feature_limits.idna", "com.facebook.messaging.integrity.featurelimits.omnistore.FeatureLimitsOmnistoreComponent");
        r4.A00 = 2;
        return C32171lK.A00(build, new C32161lJ(r4));
    }

    private FeatureLimitsOmnistoreComponent(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A02 = AnonymousClass0XJ.A0L(r3);
    }

    public void BVs(List list) {
        Iterator it = list.iterator();
        boolean z = false;
        while (it.hasNext()) {
            Delta delta = (Delta) it.next();
            if (delta.getType() == 1) {
                z = true;
            } else {
                ((C21491Hf) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BR1, this.A00)).A01(delta.getPrimaryKey());
            }
        }
        if (z) {
            A01();
        }
    }
}
