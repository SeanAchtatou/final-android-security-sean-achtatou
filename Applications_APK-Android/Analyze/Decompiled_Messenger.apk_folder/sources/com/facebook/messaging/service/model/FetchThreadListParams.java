package com.facebook.messaging.service.model;

import X.AnonymousClass07B;
import X.AnonymousClass1Y3;
import X.AnonymousClass80H;
import X.C09510hU;
import X.C10670kf;
import X.C10680kg;
import X.C10700ki;
import X.C10950l8;
import X.C417826y;
import X.C50452e3;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.http.interfaces.RequestPriority;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;

public final class FetchThreadListParams implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C10670kf();
    public final int A00;
    public final C09510hU A01;
    public final RequestPriority A02;
    public final C10950l8 A03;
    public final C10700ki A04;
    public final ImmutableSet A05;
    public final Integer A06;
    public final String A07;
    private final int A08;

    public static String A00(Integer num) {
        return 1 - num.intValue() != 0 ? "NONE" : "STANDARD_GROUP";
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        String str;
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            FetchThreadListParams fetchThreadListParams = (FetchThreadListParams) obj;
            return this.A08 == fetchThreadListParams.A08 && this.A01 == fetchThreadListParams.A01 && this.A03 == fetchThreadListParams.A03 && this.A04 == fetchThreadListParams.A04 && this.A05.equals(fetchThreadListParams.A05) && this.A06 == fetchThreadListParams.A06 && this.A02 == fetchThreadListParams.A02 && ((str = this.A07) == null || str.equals(fetchThreadListParams.A07)) && ((str != null || fetchThreadListParams.A07 == null) && this.A00 == fetchThreadListParams.A00);
        }
        return false;
    }

    public int A01() {
        return Math.max(1, this.A08);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        C09510hU r0 = this.A01;
        int i7 = 0;
        if (r0 != null) {
            i = r0.hashCode();
        } else {
            i = 0;
        }
        int i8 = i * 31;
        C10950l8 r02 = this.A03;
        if (r02 != null) {
            i2 = r02.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 31;
        C10700ki r03 = this.A04;
        if (r03 != null) {
            i3 = r03.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 31;
        ImmutableSet immutableSet = this.A05;
        if (immutableSet != null) {
            i4 = immutableSet.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 31;
        Integer num = this.A06;
        if (num != null) {
            i5 = A00(num).hashCode() + num.intValue();
        } else {
            i5 = 0;
        }
        int i12 = (((i11 + i5) * 31) + this.A08) * 31;
        RequestPriority requestPriority = this.A02;
        if (requestPriority != null) {
            i6 = requestPriority.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (((i12 + i6) * 31) + this.A00) * 31;
        String str = this.A07;
        if (str != null) {
            i7 = str.hashCode();
        }
        return i13 + i7;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01.toString());
        parcel.writeString(this.A03.dbName);
        parcel.writeString(this.A04.name());
        C417826y.A0U(parcel, this.A05);
        parcel.writeString(A00(this.A06));
        parcel.writeInt(this.A08);
        parcel.writeParcelable(this.A02, i);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A07);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add(AnonymousClass80H.$const$string(AnonymousClass1Y3.A17), this.A01.name());
        stringHelper.add("folder", this.A03.name());
        stringHelper.add(AnonymousClass80H.$const$string(61), this.A04.name());
        stringHelper.add("smsAggregationTypes", this.A05);
        stringHelper.add("groupFilterType", A00(this.A06));
        stringHelper.add("maxToFetch", this.A08);
        stringHelper.add("requestPriority", this.A02.name());
        stringHelper.add("minToFetch", this.A00);
        stringHelper.add("pageAssignedAdminId", this.A07);
        return stringHelper.toString();
    }

    public FetchThreadListParams(C10680kg r2) {
        this.A01 = r2.A02;
        this.A03 = r2.A04;
        this.A04 = r2.A05;
        this.A05 = r2.A06;
        this.A06 = r2.A07;
        this.A08 = r2.A00;
        this.A02 = r2.A03;
        this.A00 = r2.A01;
        this.A07 = r2.A08;
    }

    public FetchThreadListParams(Parcel parcel) {
        Integer num;
        this.A01 = C09510hU.valueOf(parcel.readString());
        this.A03 = C10950l8.A00(parcel.readString());
        this.A04 = C10700ki.valueOf(parcel.readString());
        this.A05 = C417826y.A0A(parcel, C50452e3.class.getClassLoader());
        String readString = parcel.readString();
        if (readString.equals("NONE")) {
            num = AnonymousClass07B.A00;
        } else if (readString.equals("STANDARD_GROUP")) {
            num = AnonymousClass07B.A01;
        } else {
            throw new IllegalArgumentException(readString);
        }
        this.A06 = num;
        this.A08 = parcel.readInt();
        this.A02 = (RequestPriority) parcel.readParcelable(RequestPriority.class.getClassLoader());
        this.A00 = parcel.readInt();
        this.A07 = parcel.readString();
    }
}
