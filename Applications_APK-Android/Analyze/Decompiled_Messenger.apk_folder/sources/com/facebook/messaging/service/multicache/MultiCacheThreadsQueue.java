package com.facebook.messaging.service.multicache;

import com.google.inject.BindingAnnotation;

@BindingAnnotation
public @interface MultiCacheThreadsQueue {
}
