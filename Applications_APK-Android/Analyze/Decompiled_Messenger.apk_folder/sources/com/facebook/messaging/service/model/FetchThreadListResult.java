package com.facebook.messaging.service.model;

import X.AnonymousClass161;
import X.C10950l8;
import X.C17280ye;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threads.NotificationSetting;
import com.facebook.messaging.model.threads.ThreadMetadata;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.user.model.User;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.Collection;

public final class FetchThreadListResult implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C17280ye();
    public final long A00;
    public final long A01;
    public final DataFetchDisposition A02;
    public final FolderCounts A03;
    public final C10950l8 A04;
    public final NotificationSetting A05;
    public final ThreadsCollection A06;
    public final ImmutableList A07;
    public final ImmutableList A08;
    public final ImmutableList A09;

    public int describeContents() {
        return 0;
    }

    public static FetchThreadListResult A00(C10950l8 r2) {
        AnonymousClass161 r1 = new AnonymousClass161();
        r1.A02 = DataFetchDisposition.A0I;
        r1.A04 = r2;
        return new FetchThreadListResult(r1);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A04.dbName);
        parcel.writeParcelable(this.A06, i);
        parcel.writeList(this.A08);
        parcel.writeList(this.A09);
        parcel.writeList(this.A07);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A05, i);
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A00);
    }

    public FetchThreadListResult(AnonymousClass161 r3) {
        Preconditions.checkNotNull(r3.A02);
        this.A02 = r3.A02;
        this.A04 = r3.A04;
        this.A06 = r3.A06;
        this.A08 = ImmutableList.copyOf((Collection) r3.A08);
        this.A09 = ImmutableList.copyOf((Collection) r3.A09);
        this.A07 = r3.A07;
        this.A03 = r3.A03;
        this.A05 = r3.A05;
        this.A01 = r3.A01;
        this.A00 = r3.A00;
    }

    public FetchThreadListResult(Parcel parcel) {
        this.A02 = (DataFetchDisposition) parcel.readParcelable(DataFetchDisposition.class.getClassLoader());
        this.A04 = C10950l8.A00(parcel.readString());
        this.A06 = (ThreadsCollection) parcel.readParcelable(ThreadsCollection.class.getClassLoader());
        this.A08 = ImmutableList.copyOf((Collection) parcel.readArrayList(ThreadMetadata.class.getClassLoader()));
        this.A09 = ImmutableList.copyOf((Collection) parcel.readArrayList(User.class.getClassLoader()));
        this.A07 = ImmutableList.copyOf((Collection) parcel.readArrayList(MessagesCollection.class.getClassLoader()));
        this.A03 = (FolderCounts) parcel.readParcelable(FolderCounts.class.getClassLoader());
        this.A05 = (NotificationSetting) parcel.readParcelable(NotificationSetting.class.getClassLoader());
        this.A01 = parcel.readLong();
        this.A00 = parcel.readLong();
    }
}
