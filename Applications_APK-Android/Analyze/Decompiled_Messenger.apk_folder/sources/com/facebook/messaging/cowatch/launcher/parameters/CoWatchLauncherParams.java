package com.facebook.messaging.cowatch.launcher.parameters;

import X.AnonymousClass0r0;
import X.C1293964q;
import X.C28931fb;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphql.enums.GraphQLLivingRoomEntrySource;
import com.facebook.messaging.model.threadkey.ThreadKey;

public final class CoWatchLauncherParams implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C1293964q();
    public final GraphQLLivingRoomEntrySource A00;
    public final VideoInfo A01;
    public final ThreadKey A02;
    public final String A03;
    public final boolean A04;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CoWatchLauncherParams) {
                CoWatchLauncherParams coWatchLauncherParams = (CoWatchLauncherParams) obj;
                if (this.A00 != coWatchLauncherParams.A00 || !C28931fb.A07(this.A03, coWatchLauncherParams.A03) || this.A04 != coWatchLauncherParams.A04 || !C28931fb.A07(this.A02, coWatchLauncherParams.A02) || !C28931fb.A07(this.A01, coWatchLauncherParams.A01)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public static AnonymousClass0r0 A00(ThreadKey threadKey) {
        AnonymousClass0r0 r1 = new AnonymousClass0r0();
        r1.A02 = threadKey;
        C28931fb.A06(threadKey, "threadKey");
        return r1;
    }

    public int hashCode() {
        int ordinal;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource = this.A00;
        if (graphQLLivingRoomEntrySource == null) {
            ordinal = -1;
        } else {
            ordinal = graphQLLivingRoomEntrySource.ordinal();
        }
        return C28931fb.A03(C28931fb.A03(C28931fb.A04(C28931fb.A03(31 + ordinal, this.A03), this.A04), this.A02), this.A01);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A00 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A00.ordinal());
        }
        if (this.A03 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A03);
        }
        parcel.writeInt(this.A04 ? 1 : 0);
        this.A02.writeToParcel(parcel, i);
        if (this.A01 == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcel.writeParcelable(this.A01, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001f, code lost:
        if (r1 != null) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CoWatchLauncherParams(X.AnonymousClass0r0 r4) {
        /*
            r3 = this;
            r3.<init>()
            com.facebook.graphql.enums.GraphQLLivingRoomEntrySource r0 = r4.A00
            r3.A00 = r0
            java.lang.String r2 = r4.A03
            r3.A03 = r2
            boolean r0 = r4.A04
            r3.A04 = r0
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A02
            java.lang.String r0 = "threadKey"
            X.C28931fb.A06(r1, r0)
            r3.A02 = r1
            com.facebook.messaging.cowatch.launcher.parameters.VideoInfo r1 = r4.A01
            r3.A01 = r1
            if (r2 != 0) goto L_0x0021
            r0 = 0
            if (r1 == 0) goto L_0x0022
        L_0x0021:
            r0 = 1
        L_0x0022:
            com.google.common.base.Preconditions.checkArgument(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.cowatch.launcher.parameters.CoWatchLauncherParams.<init>(X.0r0):void");
    }

    public CoWatchLauncherParams(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A00 = null;
        } else {
            this.A00 = GraphQLLivingRoomEntrySource.values()[parcel.readInt()];
        }
        if (parcel.readInt() == 0) {
            this.A03 = null;
        } else {
            this.A03 = parcel.readString();
        }
        this.A04 = parcel.readInt() != 1 ? false : true;
        this.A02 = (ThreadKey) ThreadKey.CREATOR.createFromParcel(parcel);
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = (VideoInfo) parcel.readParcelable(VideoInfo.class.getClassLoader());
        }
    }
}
