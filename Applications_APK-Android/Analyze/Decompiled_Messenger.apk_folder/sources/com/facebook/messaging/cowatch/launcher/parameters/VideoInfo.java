package com.facebook.messaging.cowatch.launcher.parameters;

import X.C1294064r;
import X.C13280r9;
import X.C28931fb;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.video.engine.api.VideoDataSource;

public final class VideoInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C1294064r();
    public final VideoDataSource A00;
    public final String A01;
    public final String A02;
    public final String A03;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof VideoInfo) {
                VideoInfo videoInfo = (VideoInfo) obj;
                if (!C28931fb.A07(this.A00, videoInfo.A00) || !C28931fb.A07(this.A01, videoInfo.A01) || !C28931fb.A07(this.A02, videoInfo.A02) || !C28931fb.A07(this.A03, videoInfo.A03)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(1, this.A00), this.A01), this.A02), this.A03);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A00 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A00, i);
        }
        if (this.A01 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A01);
        }
        parcel.writeString(this.A02);
        if (this.A03 == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcel.writeString(this.A03);
    }

    public VideoInfo(C13280r9 r3) {
        this.A00 = r3.A00;
        this.A01 = r3.A01;
        String str = r3.A02;
        C28931fb.A06(str, "videoId");
        this.A02 = str;
        this.A03 = r3.A03;
    }

    public VideoInfo(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A00 = null;
        } else {
            this.A00 = (VideoDataSource) parcel.readParcelable(VideoDataSource.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = parcel.readString();
        }
        this.A02 = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A03 = null;
        } else {
            this.A03 = parcel.readString();
        }
    }
}
