package com.facebook.messaging.platform;

import X.AnonymousClass0TE;
import X.AnonymousClass1XX;
import X.AnonymousClass26S;
import X.C07620dr;
import android.content.UriMatcher;

public class MessengerPlatformProvider extends AnonymousClass0TE {
    public static final UriMatcher A01 = new UriMatcher(-1);
    public AnonymousClass26S A00;

    public void A0G() {
        super.A0G();
        C07620dr.A00(getContext());
        this.A00 = AnonymousClass26S.A00(AnonymousClass1XX.get(getContext()));
        A01.addURI("com.facebook.orca.provider.MessengerPlatformProvider", "versions", 1);
    }
}
