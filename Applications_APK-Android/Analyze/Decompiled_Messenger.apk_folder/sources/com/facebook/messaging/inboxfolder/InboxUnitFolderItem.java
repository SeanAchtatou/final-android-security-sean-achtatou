package com.facebook.messaging.inboxfolder;

import X.C64643Cr;
import X.C64653Cs;
import X.C64663Ct;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.collect.ImmutableList;
import java.util.Collection;

public final class InboxUnitFolderItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new C64643Cr();
    public final C64663Ct A00;
    public final ThreadSummary A01;
    public final ImmutableList A02;

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeParcelable(this.A01, i);
        parcel.writeTypedList(this.A02);
        parcel.writeString(this.A00.name());
    }

    public InboxUnitFolderItem(C64653Cs r2) {
        super(r2.A00);
        this.A00 = r2.A01;
        this.A01 = r2.A02;
        this.A02 = ImmutableList.copyOf((Collection) r2.A03);
    }

    public InboxUnitFolderItem(Parcel parcel) {
        super(parcel);
        this.A00 = C64663Ct.valueOf(parcel.readString());
        this.A01 = (ThreadSummary) parcel.readParcelable(ThreadSummary.class.getClassLoader());
        this.A02 = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ThreadSummary.CREATOR));
    }
}
