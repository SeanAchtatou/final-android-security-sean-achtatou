package com.facebook.messaging.discovery.model;

import X.C144806oY;
import X.C27161ck;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class MessengerDiscoveryCategoryInboxItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new C144806oY();
    public final String A00;
    public final String A01;

    public void writeToParcel(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }

    public MessengerDiscoveryCategoryInboxItem(C27161ck r2, GSTModelShape1S0000000 gSTModelShape1S0000000, String str, String str2) {
        super(r2, gSTModelShape1S0000000, null);
        this.A00 = str;
        this.A01 = str2;
    }

    public MessengerDiscoveryCategoryInboxItem(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
    }
}
