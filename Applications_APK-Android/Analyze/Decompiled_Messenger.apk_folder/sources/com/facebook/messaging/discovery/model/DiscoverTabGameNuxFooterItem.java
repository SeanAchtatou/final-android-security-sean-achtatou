package com.facebook.messaging.discovery.model;

import X.AnonymousClass44D;
import X.C27161ck;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class DiscoverTabGameNuxFooterItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44D();
    public final String A00;

    public void writeToParcel(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeString(this.A00);
    }

    public DiscoverTabGameNuxFooterItem(C27161ck r2) {
        super(r2);
        GSTModelShape1S0000000 A0S = r2.A0S();
        this.A00 = A0S != null ? A0S.A3y() : null;
    }

    public DiscoverTabGameNuxFooterItem(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readString();
    }
}
