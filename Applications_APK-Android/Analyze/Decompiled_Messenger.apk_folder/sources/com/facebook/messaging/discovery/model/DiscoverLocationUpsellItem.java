package com.facebook.messaging.discovery.model;

import X.C114725ch;
import X.C27161ck;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class DiscoverLocationUpsellItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new C114725ch();
    public final String A00;
    public final String A01;

    public void writeToParcel(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }

    public DiscoverLocationUpsellItem(C27161ck r5) {
        super(r5);
        String str;
        GSTModelShape1S0000000 A0S = r5.A0S();
        String str2 = null;
        if (A0S != null) {
            str = A0S.A3y();
        } else {
            str = null;
        }
        this.A01 = str;
        GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) r5.A0J(704850766, GSTModelShape1S0000000.class, -1971421990);
        this.A00 = gSTModelShape1S0000000 != null ? gSTModelShape1S0000000.A3y() : str2;
    }

    public DiscoverLocationUpsellItem(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
    }
}
