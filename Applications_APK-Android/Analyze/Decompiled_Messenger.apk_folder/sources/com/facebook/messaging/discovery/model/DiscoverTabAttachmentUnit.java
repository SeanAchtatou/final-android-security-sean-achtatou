package com.facebook.messaging.discovery.model;

import X.C114745cj;
import X.C1522572v;
import X.C24971Xv;
import X.C27161ck;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.collect.ImmutableList;

public final class DiscoverTabAttachmentUnit extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new C114745cj();
    public final C1522572v A00;
    public final ImmutableList A01;
    public final String A02;

    public void A0B(int i) {
        super.A0B(i);
        C24971Xv it = this.A01.iterator();
        while (it.hasNext()) {
            ((DiscoverTabAttachmentItem) it.next()).A0B(i);
        }
    }

    public void A0D(int i) {
        super.A0D(i);
        C24971Xv it = this.A01.iterator();
        while (it.hasNext()) {
            ((DiscoverTabAttachmentItem) it.next()).A0D(i);
        }
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeString(this.A00.name());
        parcel.writeList(this.A01);
        parcel.writeString(this.A02);
    }

    public DiscoverTabAttachmentUnit(C27161ck r4, C1522572v r5, ImmutableList immutableList) {
        super(r4);
        String A3y;
        this.A00 = r5;
        this.A01 = immutableList;
        GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) r4.A0J(704850766, GSTModelShape1S0000000.class, -1971421990);
        if (gSTModelShape1S0000000 == null) {
            A3y = null;
        } else {
            A3y = gSTModelShape1S0000000.A3y();
        }
        this.A02 = A3y;
    }

    public DiscoverTabAttachmentUnit(Parcel parcel) {
        super(parcel);
        this.A00 = C1522572v.valueOf(parcel.readString());
        this.A01 = C417826y.A05(parcel, DiscoverTabAttachmentItem.CREATOR);
        this.A02 = parcel.readString();
    }
}
