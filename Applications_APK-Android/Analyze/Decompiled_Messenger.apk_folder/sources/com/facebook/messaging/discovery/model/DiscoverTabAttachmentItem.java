package com.facebook.messaging.discovery.model;

import X.AnonymousClass07B;
import X.C013509w;
import X.C114735ci;
import X.C1522472u;
import X.C24971Xv;
import X.C29597Ee1;
import X.C417826y;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public final class DiscoverTabAttachmentItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new C114735ci();
    public final double A00;
    public final Uri A01;
    public final Uri A02;
    public final CallToAction A03;
    public final CallToAction A04;
    public final DiscoverTabAppVideo A05;
    public final C1522472u A06;
    public final DiscoverTabGameExtraData A07;
    public final ImmutableList A08;
    public final Integer A09;
    public final Integer A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;
    public final boolean A0J;
    public final boolean A0K;

    public void writeToParcel(Parcel parcel, int i) {
        String str;
        String str2;
        parcel.writeString(this.A0F);
        parcel.writeString(this.A0I);
        parcel.writeString(this.A0H);
        parcel.writeParcelable(this.A02, i);
        parcel.writeInt(this.A0J ? 1 : 0);
        parcel.writeInt(this.A0K ? 1 : 0);
        parcel.writeDouble(this.A00);
        parcel.writeString(this.A0D);
        parcel.writeString(this.A0E);
        parcel.writeString(this.A0G);
        parcel.writeString(this.A0B);
        parcel.writeString(this.A0C);
        if (1 - this.A09.intValue() != 0) {
            str = "PAGE";
        } else {
            str = "GAME";
        }
        parcel.writeString(str);
        parcel.writeParcelable(this.A01, i);
        parcel.writeString(this.A06.name());
        if (1 - this.A0A.intValue() != 0) {
            str2 = "RATING";
        } else {
            str2 = "DEFAULT";
        }
        parcel.writeString(str2);
        parcel.writeParcelable(this.A04, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A07, i);
        C417826y.A0I(parcel, this.A08);
    }

    public void A0B(int i) {
        super.A0B(i);
        if (!C013509w.A02(this.A08)) {
            C24971Xv it = this.A08.iterator();
            while (it.hasNext()) {
                ((DiscoverTabAttachmentItem) it.next()).A0B(i);
            }
        }
    }

    public void A0C(int i) {
        super.A0C(i);
        if (!C013509w.A02(this.A08)) {
            for (int i2 = 0; i2 < this.A08.size(); i2++) {
                ((DiscoverTabAttachmentItem) this.A08.get(i2)).A0C(i + i2 + 1);
            }
        }
    }

    public void A0D(int i) {
        super.A0D(i);
        if (!C013509w.A02(this.A08)) {
            C24971Xv it = this.A08.iterator();
            while (it.hasNext()) {
                ((DiscoverTabAttachmentItem) it.next()).A0D(i);
            }
        }
    }

    public DiscoverTabAttachmentItem(C29597Ee1 ee1) {
        super(ee1.A09, ee1.A03, null);
        String str = ee1.A0H;
        Preconditions.checkNotNull(str);
        this.A0F = str;
        String str2 = ee1.A0K;
        Preconditions.checkNotNull(str2);
        this.A0I = str2;
        String str3 = ee1.A0J;
        Preconditions.checkNotNull(str3);
        this.A0H = str3;
        Uri uri = ee1.A02;
        Preconditions.checkNotNull(uri);
        this.A02 = uri;
        this.A0J = ee1.A0L;
        this.A0K = ee1.A0M;
        this.A00 = ee1.A00;
        this.A0D = ee1.A0F;
        this.A0E = ee1.A0G;
        this.A0G = ee1.A0I;
        this.A0B = ee1.A0D;
        this.A0C = ee1.A0E;
        Integer num = ee1.A0B;
        Preconditions.checkNotNull(num);
        this.A09 = num;
        this.A01 = ee1.A01;
        C1522472u r0 = ee1.A07;
        Preconditions.checkNotNull(r0);
        this.A06 = r0;
        Integer num2 = ee1.A0C;
        this.A0A = num2 == null ? AnonymousClass07B.A01 : num2;
        this.A04 = ee1.A05;
        this.A03 = ee1.A04;
        this.A05 = ee1.A06;
        this.A07 = ee1.A08;
        this.A08 = ee1.A0A;
    }

    public DiscoverTabAttachmentItem(Parcel parcel) {
        super(parcel);
        Integer num;
        Integer num2;
        this.A0F = parcel.readString();
        this.A0I = parcel.readString();
        this.A0H = parcel.readString();
        Class<Uri> cls = Uri.class;
        this.A02 = (Uri) parcel.readParcelable(cls.getClassLoader());
        boolean z = false;
        this.A0J = parcel.readInt() == 1;
        this.A0K = parcel.readInt() == 1 ? true : z;
        this.A00 = parcel.readDouble();
        this.A0D = parcel.readString();
        this.A0E = parcel.readString();
        this.A0G = parcel.readString();
        this.A0B = parcel.readString();
        this.A0C = parcel.readString();
        String readString = parcel.readString();
        if (readString.equals("PAGE")) {
            num = AnonymousClass07B.A00;
        } else {
            num = readString.equals("GAME") ? AnonymousClass07B.A01 : num;
            throw new IllegalArgumentException(readString);
        }
        this.A09 = num;
        this.A01 = (Uri) parcel.readParcelable(cls.getClassLoader());
        this.A06 = C1522472u.valueOf(parcel.readString());
        readString = parcel.readString();
        if (readString.equals("RATING")) {
            num2 = AnonymousClass07B.A00;
        } else {
            num2 = readString.equals("DEFAULT") ? AnonymousClass07B.A01 : num2;
            throw new IllegalArgumentException(readString);
        }
        this.A0A = num2;
        Class<CallToAction> cls2 = CallToAction.class;
        this.A04 = (CallToAction) parcel.readParcelable(cls2.getClassLoader());
        this.A03 = (CallToAction) parcel.readParcelable(cls2.getClassLoader());
        this.A05 = (DiscoverTabAppVideo) parcel.readParcelable(DiscoverTabAppVideo.class.getClassLoader());
        this.A07 = (DiscoverTabGameExtraData) parcel.readParcelable(DiscoverTabGameExtraData.class.getClassLoader());
        this.A08 = C417826y.A06(parcel, CREATOR);
    }
}
