package com.facebook.messaging.presence.unifiedpresence;

import X.AnonymousClass1HV;
import X.C06610bn;
import X.C06850cB;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import java.util.Arrays;

public final class UnifiedPresenceViewLoggerItem implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1HV();
    public final ImmutableMap A00;
    public final Long A01;
    public final String A02;
    public final boolean A03;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof UnifiedPresenceViewLoggerItem)) {
            return false;
        }
        UnifiedPresenceViewLoggerItem unifiedPresenceViewLoggerItem = (UnifiedPresenceViewLoggerItem) obj;
        return Objects.equal(Boolean.valueOf(this.A03), Boolean.valueOf(unifiedPresenceViewLoggerItem.A03)) && Objects.equal(this.A01, unifiedPresenceViewLoggerItem.A01) && C06850cB.A0C(this.A02, unifiedPresenceViewLoggerItem.A02) && Objects.equal(this.A00, unifiedPresenceViewLoggerItem.A00);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Boolean.valueOf(this.A03), this.A01, this.A02, this.A00});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(Boolean.valueOf(this.A03));
        parcel.writeValue(this.A01);
        parcel.writeValue(this.A02);
        parcel.writeMap(this.A00);
    }

    public UnifiedPresenceViewLoggerItem(C06610bn r2) {
        this.A03 = r2.A03;
        this.A01 = r2.A01;
        this.A02 = r2.A02;
        Preconditions.checkNotNull(r2.A00);
        this.A00 = r2.A00;
    }

    public UnifiedPresenceViewLoggerItem(Parcel parcel) {
        this.A03 = ((Boolean) parcel.readValue(Boolean.class.getClassLoader())).booleanValue();
        this.A01 = (Long) parcel.readValue(Long.class.getClassLoader());
        this.A02 = (String) parcel.readValue(String.class.getClassLoader());
        this.A00 = C417826y.A09(parcel);
    }
}
