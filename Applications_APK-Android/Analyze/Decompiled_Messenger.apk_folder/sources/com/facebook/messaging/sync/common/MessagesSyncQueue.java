package com.facebook.messaging.sync.common;

import com.google.inject.BindingAnnotation;

@BindingAnnotation
public @interface MessagesSyncQueue {
}
