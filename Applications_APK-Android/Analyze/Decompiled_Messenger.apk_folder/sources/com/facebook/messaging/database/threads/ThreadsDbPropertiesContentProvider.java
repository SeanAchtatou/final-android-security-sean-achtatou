package com.facebook.messaging.database.threads;

import X.AnonymousClass08S;
import X.AnonymousClass0TB;
import X.AnonymousClass0TF;
import X.AnonymousClass0VG;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.C005505z;
import X.C04310Tq;
import X.C12730ps;
import X.C25771aN;
import X.C28051eB;

public class ThreadsDbPropertiesContentProvider extends AnonymousClass0TB {
    public AnonymousClass0TF A00 = new AnonymousClass0TF();
    public C04310Tq A01;
    public C04310Tq A02;

    public synchronized void A0G() {
        super.A0G();
        C005505z.A03("ThreadsDbPropertiesContentProvider.onInitialize", 338286375);
        try {
            AnonymousClass1XX r1 = AnonymousClass1XX.get(getContext());
            this.A01 = C25771aN.A02(r1);
            AnonymousClass0VG A002 = AnonymousClass0VG.A00(AnonymousClass1Y3.AZA, r1);
            this.A02 = A002;
            AnonymousClass0TF r3 = new AnonymousClass0TF();
            this.A00 = r3;
            r3.A01(AnonymousClass08S.A0P(((C12730ps) A002.get()).A00.getPackageName(), ".", "threads_properties"), "properties", new C28051eB(this));
            C005505z.A00(-1062098419);
        } finally {
            C005505z.A00(1932651530);
        }
    }
}
