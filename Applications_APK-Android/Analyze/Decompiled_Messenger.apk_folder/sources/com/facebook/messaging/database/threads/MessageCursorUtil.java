package com.facebook.messaging.database.threads;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass0jC;
import X.AnonymousClass0jD;
import X.AnonymousClass0jE;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass24B;
import X.C05040Xk;
import X.C08690fm;
import X.C11920oF;
import X.C11930oG;
import X.C11940oH;
import X.C12000oN;
import X.C12030oQ;
import X.C12050oS;
import X.C12060oT;
import X.C12070oU;
import X.C12080oV;
import X.C12090oW;
import X.C12100oX;
import com.facebook.proxygen.TraceFieldType;
import javax.inject.Singleton;

@Singleton
public final class MessageCursorUtil {
    public static final String[] A0F;
    private static volatile MessageCursorUtil A0G;
    public AnonymousClass0UN A00;
    public final AnonymousClass0jE A01 = C05040Xk.A00();
    public final C11930oG A02;
    public final C11940oH A03;
    public final C11920oF A04;
    public final C12030oQ A05;
    public final C12050oS A06;
    public final C12100oX A07;
    public final AnonymousClass0jC A08;
    public final C12060oT A09;
    public final C12070oU A0A;
    public final C12000oN A0B;
    public final C08690fm A0C;
    public final C12090oW A0D;
    public final C12080oV A0E;

    static {
        String[] strArr = new String[81];
        System.arraycopy(new String[]{"thread_key", TraceFieldType.MsgId, "text", "is_not_forwardable", "sender", "timestamp_ms", "timestamp_sent_ms", TraceFieldType.MsgType, "affected_users", "attachments", "shares", "sticker_id", "offline_threading_id", "source", "channel_source", "is_non_authoritative", "pending_send_media_attachment", "sent_share_attachment", "client_tags", "extensible_message_data", "send_error", "send_error_message", "send_error_detail", "send_error_original_exception", "send_error_number", "send_error_timestamp_ms", "send_error_error_url"}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{"send_channel", "publicity", AnonymousClass24B.$const$string(62), "send_queue_type", "payment_transaction", "payment_request", "has_unavailable_attachment", "app_attribution", "content_app_attribution", AnonymousClass24B.$const$string(338), "admin_text_type", "admin_text_theme_color", AnonymousClass24B.$const$string(AnonymousClass1Y3.A1P), AnonymousClass24B.$const$string(53), "admin_text_thread_icon_emoji", "admin_text_nickname", "admin_text_target_id", "admin_text_thread_message_lifetime", "message_lifetime", "admin_text_thread_rtc_event", "admin_text_thread_rtc_server_info_data", "admin_text_thread_rtc_is_video_call", "is_sponsored", AnonymousClass24B.$const$string(339), "unsendability_status", "admin_text_thread_ad_properties", "admin_text_game_score_data"}, 0, strArr, 27, 27);
        System.arraycopy(new String[]{"admin_text_thread_event_reminder_properties", "commerce_message_type", "admin_text_joinable_event_type", "metadata_at_text_ranges", "platform_metadata", "admin_text_is_joinable_promo", "montage_reply_message_id", "montage_reply_action", AnonymousClass24B.$const$string(23), AnonymousClass24B.$const$string(24), AnonymousClass24B.$const$string(25), AnonymousClass24B.$const$string(26), AnonymousClass24B.$const$string(85), AnonymousClass24B.$const$string(81), "generic_admin_message_extensible_data", "reactions", "profile_ranges", AnonymousClass24B.$const$string(103), AnonymousClass24B.$const$string(77), AnonymousClass24B.$const$string(78), AnonymousClass24B.$const$string(80), AnonymousClass24B.$const$string(84), AnonymousClass24B.$const$string(AnonymousClass1Y3.A0m), AnonymousClass24B.$const$string(31), "snippet", AnonymousClass24B.$const$string(AnonymousClass1Y3.A1y), AnonymousClass24B.$const$string(336)}, 0, strArr, 54, 27);
        A0F = strArr;
    }

    public static final MessageCursorUtil A00(AnonymousClass1XY r4) {
        if (A0G == null) {
            synchronized (MessageCursorUtil.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0G, r4);
                if (A002 != null) {
                    try {
                        A0G = new MessageCursorUtil(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0G;
    }

    private MessageCursorUtil(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A08 = new AnonymousClass0jC(r3);
        this.A04 = C11920oF.A00(r3);
        this.A03 = new C11940oH(r3);
        this.A0C = new C08690fm(r3);
        this.A0B = new C12000oN(r3);
        this.A05 = new C12030oQ(AnonymousClass0jD.A00(r3));
        this.A06 = new C12050oS(r3);
        this.A09 = new C12060oT(r3);
        this.A0A = new C12070oU(r3);
        this.A02 = new C11930oG(AnonymousClass0jD.A00(r3));
        this.A0E = C12080oV.A00(r3);
        this.A0D = C12090oW.A00(r3);
        this.A07 = new C12100oX(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002b, code lost:
        if (r3.equals("M") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0054, code lost:
        if (r3.equals("L") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x005e, code lost:
        if (r3.equals("S") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0068, code lost:
        if (r3.equals("P") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0072, code lost:
        if (r3.equals("F") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x007c, code lost:
        if (r3.equals("C") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0086, code lost:
        if (r3.equals("A") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0090, code lost:
        if (r3.equals("2") == false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C36881u2 A01(java.lang.String r3) {
        /*
            int r1 = r3.hashCode()
            r0 = 50
            if (r1 == r0) goto L_0x0089
            r0 = 65
            if (r1 == r0) goto L_0x007f
            r0 = 67
            if (r1 == r0) goto L_0x0075
            r0 = 70
            if (r1 == r0) goto L_0x006b
            r0 = 80
            if (r1 == r0) goto L_0x0061
            r0 = 83
            if (r1 == r0) goto L_0x0057
            r0 = 76
            if (r1 == r0) goto L_0x004d
            r0 = 77
            if (r1 != r0) goto L_0x002d
            java.lang.String r0 = "M"
            boolean r0 = r3.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x002e
        L_0x002d:
            r1 = -1
        L_0x002e:
            switch(r1) {
                case 0: goto L_0x004a;
                case 1: goto L_0x0047;
                case 2: goto L_0x0044;
                case 3: goto L_0x0041;
                case 4: goto L_0x003e;
                case 5: goto L_0x003b;
                case 6: goto L_0x0038;
                case 7: goto L_0x0035;
                default: goto L_0x0031;
            }
        L_0x0031:
            r0 = 0
        L_0x0032:
            if (r0 == 0) goto L_0x0093
            return r0
        L_0x0035:
            X.1u2 r0 = X.C36881u2.FBNS_LITE
            goto L_0x0032
        L_0x0038:
            X.1u2 r0 = X.C36881u2.FBNS
            goto L_0x0032
        L_0x003b:
            X.1u2 r0 = X.C36881u2.PUSH
            goto L_0x0032
        L_0x003e:
            X.1u2 r0 = X.C36881u2.SEND
            goto L_0x0032
        L_0x0041:
            X.1u2 r0 = X.C36881u2.C2DM
            goto L_0x0032
        L_0x0044:
            X.1u2 r0 = X.C36881u2.CALL_LOG
            goto L_0x0032
        L_0x0047:
            X.1u2 r0 = X.C36881u2.API
            goto L_0x0032
        L_0x004a:
            X.1u2 r0 = X.C36881u2.MQTT
            goto L_0x0032
        L_0x004d:
            java.lang.String r0 = "L"
            boolean r0 = r3.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x002e
            goto L_0x002d
        L_0x0057:
            java.lang.String r0 = "S"
            boolean r0 = r3.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x002e
            goto L_0x002d
        L_0x0061:
            java.lang.String r0 = "P"
            boolean r0 = r3.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x002e
            goto L_0x002d
        L_0x006b:
            java.lang.String r0 = "F"
            boolean r0 = r3.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x002e
            goto L_0x002d
        L_0x0075:
            java.lang.String r0 = "C"
            boolean r0 = r3.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x002e
            goto L_0x002d
        L_0x007f:
            java.lang.String r0 = "A"
            boolean r0 = r3.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x002e
            goto L_0x002d
        L_0x0089:
            java.lang.String r0 = "2"
            boolean r0 = r3.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x002e
            goto L_0x002d
        L_0x0093:
            java.lang.Class<X.1u2> r0 = X.C36881u2.class
            java.lang.Enum r0 = java.lang.Enum.valueOf(r0, r3)     // Catch:{ IllegalArgumentException -> 0x009c }
            X.1u2 r0 = (X.C36881u2) r0     // Catch:{ IllegalArgumentException -> 0x009c }
            return r0
        L_0x009c:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Invalid ChannelSource string: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.<init>(r0, r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.database.threads.MessageCursorUtil.A01(java.lang.String):X.1u2");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        if (r5.equals("U") == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0049, code lost:
        if (r5.equals("S") == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0053, code lost:
        if (r5.equals("M") == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005d, code lost:
        if (r5.equals("G") == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1R6 A02(java.lang.String r5) {
        /*
            boolean r0 = X.C06850cB.A0B(r5)
            if (r0 == 0) goto L_0x0009
            X.1R6 r0 = X.AnonymousClass1R6.UNKNOWN
            return r0
        L_0x0009:
            int r1 = r5.hashCode()
            r0 = 71
            r4 = 3
            r3 = 2
            r2 = 1
            if (r1 == r0) goto L_0x0056
            r0 = 77
            if (r1 == r0) goto L_0x004c
            r0 = 83
            if (r1 == r0) goto L_0x0042
            r0 = 85
            if (r1 != r0) goto L_0x0029
            java.lang.String r0 = "U"
            boolean r0 = r5.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x002a
        L_0x0029:
            r1 = -1
        L_0x002a:
            if (r1 == 0) goto L_0x003f
            if (r1 == r2) goto L_0x003c
            if (r1 == r3) goto L_0x0039
            if (r1 == r4) goto L_0x0036
            r0 = 0
        L_0x0033:
            if (r0 == 0) goto L_0x0060
            return r0
        L_0x0036:
            X.1R6 r0 = X.AnonymousClass1R6.SMS
            goto L_0x0033
        L_0x0039:
            X.1R6 r0 = X.AnonymousClass1R6.GRAPH
            goto L_0x0033
        L_0x003c:
            X.1R6 r0 = X.AnonymousClass1R6.UNKNOWN
            goto L_0x0033
        L_0x003f:
            X.1R6 r0 = X.AnonymousClass1R6.MQTT
            goto L_0x0033
        L_0x0042:
            java.lang.String r0 = "S"
            boolean r0 = r5.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x002a
            goto L_0x0029
        L_0x004c:
            java.lang.String r0 = "M"
            boolean r0 = r5.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x002a
            goto L_0x0029
        L_0x0056:
            java.lang.String r0 = "G"
            boolean r0 = r5.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x002a
            goto L_0x0029
        L_0x0060:
            java.lang.Class<X.1R6> r0 = X.AnonymousClass1R6.class
            java.lang.Enum r0 = java.lang.Enum.valueOf(r0, r5)     // Catch:{ IllegalArgumentException -> 0x0069 }
            X.1R6 r0 = (X.AnonymousClass1R6) r0     // Catch:{ IllegalArgumentException -> 0x0069 }
            return r0
        L_0x0069:
            X.1R6 r0 = X.AnonymousClass1R6.UNKNOWN
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.database.threads.MessageCursorUtil.A02(java.lang.String):X.1R6");
    }
}
