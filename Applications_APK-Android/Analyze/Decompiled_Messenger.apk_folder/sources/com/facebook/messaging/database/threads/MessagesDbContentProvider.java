package com.facebook.messaging.database.threads;

import X.AnonymousClass0TB;
import X.AnonymousClass0TF;
import X.AnonymousClass0VG;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass4XE;
import X.C005505z;
import X.C04310Tq;
import X.C11300mg;
import X.C25771aN;

public class MessagesDbContentProvider extends AnonymousClass0TB {
    public AnonymousClass0TF A00;
    public C04310Tq A01;
    public C04310Tq A02;
    public C04310Tq A03;
    public C04310Tq A04;

    public synchronized void A0G() {
        super.A0G();
        C005505z.A03("MessagesDbContentProvider.onInitialize", 1455216131);
        try {
            AnonymousClass1XX r1 = AnonymousClass1XX.get(getContext());
            this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.BSj, r1);
            this.A03 = C25771aN.A02(r1);
            this.A04 = AnonymousClass0VG.A00(AnonymousClass1Y3.AnN, r1);
            this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.Adk, r1);
            AnonymousClass4XE r5 = (AnonymousClass4XE) this.A01.get();
            AnonymousClass0TF r4 = new AnonymousClass0TF();
            this.A00 = r4;
            r4.A01(r5.A01, "thread_summaries", new C11300mg(this.A04));
            this.A00.A01(r5.A01, "messages", new C11300mg(this.A02));
            C005505z.A00(-1113753044);
        } finally {
            C005505z.A00(-1511327477);
        }
    }
}
