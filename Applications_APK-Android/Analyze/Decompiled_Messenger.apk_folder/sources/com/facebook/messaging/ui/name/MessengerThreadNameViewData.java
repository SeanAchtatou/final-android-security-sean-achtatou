package com.facebook.messaging.ui.name;

import com.facebook.messaging.model.messages.ParticipantInfo;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;

public final class MessengerThreadNameViewData extends ThreadNameViewData {
    public final long A00;
    public final ParticipantInfo A01;

    public String toString() {
        long j;
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(MessengerThreadNameViewData.class);
        stringHelper.add("name", this.A01);
        stringHelper.add("memberNames", this.A00);
        stringHelper.add("canonicalThreadParticipantInfo", this.A01);
        if (this.A01 != null) {
            j = this.A00;
        } else {
            j = -1;
        }
        stringHelper.add("canonicalThreadParticipantLastReadTime", j);
        return stringHelper.toString();
    }

    public MessengerThreadNameViewData(boolean z, String str, ImmutableList immutableList, ParticipantInfo participantInfo, long j) {
        super(z, str, immutableList);
        this.A01 = participantInfo;
        this.A00 = j;
    }
}
