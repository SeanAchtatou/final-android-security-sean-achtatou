package com.facebook.messaging.ui.name;

import X.AnonymousClass07K;
import X.C22201Kh;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

public class ThreadNameViewData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C22201Kh();
    public final ImmutableList A00;
    public final String A01;
    public final boolean A02;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ThreadNameViewData threadNameViewData = (ThreadNameViewData) obj;
            if (this.A02 != threadNameViewData.A02 || !Objects.equal(this.A01, threadNameViewData.A01) || !Objects.equal(this.A00, threadNameViewData.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return AnonymousClass07K.A02(Boolean.valueOf(this.A02), this.A01, this.A00);
    }

    public String toString() {
        return this.A01;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C417826y.A0V(parcel, this.A02);
        parcel.writeString(this.A01);
        parcel.writeStringList(this.A00);
    }

    public ThreadNameViewData(Parcel parcel) {
        this.A02 = C417826y.A0W(parcel);
        this.A01 = parcel.readString();
        this.A00 = C417826y.A04(parcel);
    }

    public ThreadNameViewData(boolean z, String str, ImmutableList immutableList) {
        this.A02 = z;
        this.A01 = str;
        this.A00 = immutableList;
    }
}
