package com.facebook.messaging.analytics.perf;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C08990gJ;
import X.C27021cW;
import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
public final class PostStartupTracker {
    private static volatile PostStartupTracker A03;
    public int A00 = 0;
    public AnonymousClass0UN A01;
    public final C08990gJ A02;

    public static final PostStartupTracker A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (PostStartupTracker.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new PostStartupTracker(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private PostStartupTracker(AnonymousClass1XY r6) {
        AnonymousClass0UN r4 = new AnonymousClass0UN(2, r6);
        this.A01 = r4;
        this.A02 = new C08990gJ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r4), (C27021cW) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Alk, r4));
    }
}
