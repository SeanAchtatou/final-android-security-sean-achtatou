package com.facebook.messaging.analytics.reliability;

import X.AnonymousClass067;
import X.AnonymousClass06B;
import X.AnonymousClass07B;
import X.AnonymousClass09P;
import X.AnonymousClass0US;
import X.AnonymousClass0UX;
import X.AnonymousClass0VK;
import X.AnonymousClass0WA;
import X.AnonymousClass0WD;
import X.AnonymousClass0WT;
import X.AnonymousClass0XJ;
import X.AnonymousClass18P;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y7;
import X.AnonymousClass1YA;
import X.AnonymousClass1YQ;
import X.C000700l;
import X.C04750Wa;
import X.C05690aA;
import X.C06920cI;
import X.C11670nb;
import X.C202889hN;
import X.C25051Yd;
import X.C28711fF;
import X.C28761fK;
import X.C30281hn;
import X.C36651tV;
import X.C36671tX;
import X.C36701ta;
import android.content.Context;
import android.util.Base64;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.collect.ImmutableMap;
import io.card.payment.BuildConfig;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
public class AggregatedReliabilityLogger implements AnonymousClass1YQ {
    public static final AnonymousClass1Y7 A0C = ((AnonymousClass1Y7) C05690aA.A0m.A09("reliability_serialized"));
    private static volatile AggregatedReliabilityLogger A0D;
    public LinkedHashMap A00 = null;
    public final Context A01;
    public final DeprecatedAnalyticsLogger A02;
    public final AnonymousClass06B A03;
    public final AnonymousClass0US A04;
    public final C28761fK A05;
    public final C36651tV A06;
    public final AnonymousClass18P A07;
    public final C25051Yd A08;
    public final FbSharedPreferences A09;
    public final AnonymousClass0VK A0A;
    public final String A0B;

    public class ReliabilityInfo implements Serializable {
        private static final long serialVersionUID = -7196522877148772764L;
        public int graphAttempts = 0;
        public final String messageType;
        public int mqttAttempts = 0;
        public Outcome outcome = Outcome.UNKNOWN;
        public final long sendAttemptTimestamp;
        public final String threadType;
        public long timeSinceFirstSendAttempt = -1;

        public enum Outcome {
            UNKNOWN("u"),
            SUCCESS_MQTT("m"),
            SUCCESS_GRAPH("g"),
            FAILURE_RETRYABLE("f"),
            FAILURE_PERMANENT("p");
            
            public final String rawValue;

            private Outcome(String str) {
                this.rawValue = str;
            }
        }

        public ReliabilityInfo(long j, String str, String str2) {
            this.sendAttemptTimestamp = j;
            this.messageType = str;
            this.threadType = str2;
        }
    }

    private synchronized void A01() {
        if (this.A00 != null) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(this.A00);
                objectOutputStream.flush();
                String str = new String(Base64.encode(byteArrayOutputStream.toByteArray(), 0));
                objectOutputStream.close();
                C30281hn edit = this.A09.edit();
                edit.BzC(A0C, str);
                edit.commit();
            } catch (IOException e) {
                ((AnonymousClass09P) this.A04.get()).softReport("reliabilities_serialization_failed", e);
                C30281hn edit2 = this.A09.edit();
                edit2.C1B(A0C);
                edit2.commit();
            }
        }
        return;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0008 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A02(com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger r3) {
        /*
            monitor-enter(r3)
            r3.A03()     // Catch:{ Exception -> 0x0008 }
            r3.A01()     // Catch:{ Exception -> 0x0008 }
            goto L_0x0017
        L_0x0008:
            X.0US r0 = r3.A04     // Catch:{ all -> 0x0019 }
            java.lang.Object r2 = r0.get()     // Catch:{ all -> 0x0019 }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x0019 }
            java.lang.String r1 = "reliability_logger_on_reliability_cata_changed_fail"
            java.lang.String r0 = "Failed to update aggregated reliability data"
            r2.CGS(r1, r0)     // Catch:{ all -> 0x0019 }
        L_0x0017:
            monitor-exit(r3)
            return
        L_0x0019:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.A02(com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0148, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean A03() {
        /*
            r15 = this;
            monitor-enter(r15)
            boolean r0 = A04(r15)     // Catch:{ all -> 0x0149 }
            r14 = 0
            if (r0 == 0) goto L_0x0147
            java.util.LinkedHashMap r0 = r15.A00     // Catch:{ all -> 0x0149 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0149 }
            if (r0 != 0) goto L_0x0147
            r13 = r15
            monitor-enter(r13)     // Catch:{ all -> 0x0149 }
            java.util.LinkedHashMap r0 = r15.A00     // Catch:{ all -> 0x0141 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x0141 }
            java.util.Iterator r12 = r0.iterator()     // Catch:{ all -> 0x0141 }
            java.lang.Object r9 = r12.next()     // Catch:{ all -> 0x0141 }
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ all -> 0x0141 }
            java.lang.Object r5 = r9.getValue()     // Catch:{ all -> 0x0141 }
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo r5 = (com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo) r5     // Catch:{ all -> 0x0141 }
            java.util.LinkedHashMap r0 = r15.A00     // Catch:{ all -> 0x0141 }
            int r0 = r0.size()     // Catch:{ all -> 0x0141 }
            long r3 = (long) r0     // Catch:{ all -> 0x0141 }
            X.1Yd r6 = r15.A08     // Catch:{ all -> 0x0141 }
            r0 = 563727342568000(0x200b500010240, double:2.785183135842286E-309)
            r2 = 500(0x1f4, float:7.0E-43)
            int r0 = r6.AqL(r0, r2)     // Catch:{ all -> 0x0141 }
            long r1 = (long) r0     // Catch:{ all -> 0x0141 }
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0062
            long r6 = r5.sendAttemptTimestamp     // Catch:{ all -> 0x0141 }
            X.06B r0 = r15.A03     // Catch:{ all -> 0x0141 }
            long r10 = r0.now()     // Catch:{ all -> 0x0141 }
            X.1Yd r4 = r15.A08     // Catch:{ all -> 0x0141 }
            r2 = 563727342633537(0x200b500020241, double:2.78518313616608E-309)
            r0 = 21600(0x5460, double:1.0672E-319)
            long r2 = r4.At1(r2, r0)     // Catch:{ all -> 0x0141 }
            r0 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r0
            long r10 = r10 - r2
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x0062
            r3 = 0
            monitor-exit(r13)     // Catch:{ all -> 0x0149 }
            goto L_0x0124
        L_0x0062:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0141 }
            r4.<init>()     // Catch:{ all -> 0x0141 }
        L_0x0067:
            java.util.LinkedHashMap r0 = r15.A00     // Catch:{ all -> 0x0141 }
            int r0 = r0.size()     // Catch:{ all -> 0x0141 }
            long r2 = (long) r0     // Catch:{ all -> 0x0141 }
            X.1Yd r7 = r15.A08     // Catch:{ all -> 0x0141 }
            r0 = 563727342568000(0x200b500010240, double:2.785183135842286E-309)
            r6 = 500(0x1f4, float:7.0E-43)
            int r0 = r7.AqL(r0, r6)     // Catch:{ all -> 0x0141 }
            long r0 = (long) r0     // Catch:{ all -> 0x0141 }
            int r6 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r6 > 0) goto L_0x00a7
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r1 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.UNKNOWN     // Catch:{ all -> 0x0141 }
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = r5.outcome     // Catch:{ all -> 0x0141 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0141 }
            if (r0 == 0) goto L_0x00a7
            long r6 = r5.sendAttemptTimestamp     // Catch:{ all -> 0x0141 }
            X.06B r0 = r15.A03     // Catch:{ all -> 0x0141 }
            long r10 = r0.now()     // Catch:{ all -> 0x0141 }
            X.1Yd r8 = r15.A08     // Catch:{ all -> 0x0141 }
            r2 = 563727342699074(0x200b500030242, double:2.785183136489877E-309)
            r0 = 10800(0x2a30, double:5.336E-320)
            long r2 = r8.At1(r2, r0)     // Catch:{ all -> 0x0141 }
            r0 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r0
            long r10 = r10 - r2
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x011f
        L_0x00a7:
            java.lang.Object r1 = r9.getKey()     // Catch:{ all -> 0x0141 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0141 }
            int r0 = r4.length()     // Catch:{ all -> 0x0141 }
            if (r0 <= 0) goto L_0x00b8
            r0 = 44
            r4.append(r0)     // Catch:{ all -> 0x0141 }
        L_0x00b8:
            r4.append(r1)     // Catch:{ all -> 0x0141 }
            java.lang.String r0 = "="
            r4.append(r0)     // Catch:{ all -> 0x0141 }
            java.lang.String r0 = r5.messageType     // Catch:{ all -> 0x0141 }
            r4.append(r0)     // Catch:{ all -> 0x0141 }
            java.lang.String r3 = ":"
            r4.append(r3)     // Catch:{ all -> 0x0141 }
            int r0 = r5.mqttAttempts     // Catch:{ all -> 0x0141 }
            r4.append(r0)     // Catch:{ all -> 0x0141 }
            r4.append(r3)     // Catch:{ all -> 0x0141 }
            int r0 = r5.graphAttempts     // Catch:{ all -> 0x0141 }
            r4.append(r0)     // Catch:{ all -> 0x0141 }
            r4.append(r3)     // Catch:{ all -> 0x0141 }
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r2 = r5.outcome     // Catch:{ all -> 0x0141 }
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.FAILURE_PERMANENT     // Catch:{ all -> 0x0141 }
            if (r2 == r0) goto L_0x0119
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.FAILURE_RETRYABLE     // Catch:{ all -> 0x0141 }
            if (r2 == r0) goto L_0x0119
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.UNKNOWN     // Catch:{ all -> 0x0141 }
            if (r2 == r0) goto L_0x0119
            long r0 = r5.timeSinceFirstSendAttempt     // Catch:{ all -> 0x0141 }
            r4.append(r0)     // Catch:{ all -> 0x0141 }
        L_0x00ed:
            r4.append(r3)     // Catch:{ all -> 0x0141 }
            if (r2 != 0) goto L_0x00f5
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.UNKNOWN     // Catch:{ all -> 0x0141 }
            goto L_0x00f7
        L_0x00f5:
            java.lang.String r0 = r2.rawValue     // Catch:{ all -> 0x0141 }
        L_0x00f7:
            r4.append(r0)     // Catch:{ all -> 0x0141 }
            r4.append(r3)     // Catch:{ all -> 0x0141 }
            java.lang.String r0 = r5.threadType     // Catch:{ all -> 0x0141 }
            r4.append(r0)     // Catch:{ all -> 0x0141 }
            r12.remove()     // Catch:{ all -> 0x0141 }
            boolean r0 = r12.hasNext()     // Catch:{ all -> 0x0141 }
            if (r0 == 0) goto L_0x011f
            java.lang.Object r9 = r12.next()     // Catch:{ all -> 0x0141 }
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ all -> 0x0141 }
            java.lang.Object r5 = r9.getValue()     // Catch:{ all -> 0x0141 }
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo r5 = (com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo) r5     // Catch:{ all -> 0x0141 }
            goto L_0x0067
        L_0x0119:
            long r0 = r5.sendAttemptTimestamp     // Catch:{ all -> 0x0141 }
            r4.append(r0)     // Catch:{ all -> 0x0141 }
            goto L_0x00ed
        L_0x011f:
            java.lang.String r3 = r4.toString()     // Catch:{ all -> 0x0141 }
            monitor-exit(r13)     // Catch:{ all -> 0x0149 }
        L_0x0124:
            boolean r0 = X.C06850cB.A0A(r3)     // Catch:{ all -> 0x0149 }
            if (r0 != 0) goto L_0x0147
            r2 = r15
            monitor-enter(r2)     // Catch:{ all -> 0x0149 }
            X.0nb r1 = new X.0nb     // Catch:{ all -> 0x0144 }
            java.lang.String r0 = "msg_reliability"
            r1.<init>(r0)     // Catch:{ all -> 0x0144 }
            java.lang.String r0 = "reliabilities_map"
            r1.A0D(r0, r3)     // Catch:{ all -> 0x0144 }
            com.facebook.analytics.DeprecatedAnalyticsLogger r0 = r15.A02     // Catch:{ all -> 0x0144 }
            r0.A09(r1)     // Catch:{ all -> 0x0144 }
            monitor-exit(r2)     // Catch:{ all -> 0x0149 }
            r0 = 1
            monitor-exit(r15)
            return r0
        L_0x0141:
            r0 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x0149 }
            goto L_0x0146
        L_0x0144:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0149 }
        L_0x0146:
            throw r0     // Catch:{ all -> 0x0149 }
        L_0x0147:
            monitor-exit(r15)
            return r14
        L_0x0149:
            r0 = move-exception
            monitor-exit(r15)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.A03():boolean");
    }

    public static synchronized boolean A04(AggregatedReliabilityLogger aggregatedReliabilityLogger) {
        boolean z;
        synchronized (aggregatedReliabilityLogger) {
            if (aggregatedReliabilityLogger.A00 == null) {
                synchronized (aggregatedReliabilityLogger) {
                    LinkedHashMap linkedHashMap = null;
                    if (aggregatedReliabilityLogger.A09.BFQ()) {
                        String B4F = aggregatedReliabilityLogger.A09.B4F(A0C, null);
                        if (B4F == null) {
                            linkedHashMap = new LinkedHashMap();
                        } else {
                            try {
                                linkedHashMap = (LinkedHashMap) new ObjectInputStream(new ByteArrayInputStream(Base64.decode(B4F, 0))).readObject();
                            } catch (Exception e) {
                                ((AnonymousClass09P) aggregatedReliabilityLogger.A04.get()).softReport("bad_reliabilities_deserialization", e);
                                C30281hn edit = aggregatedReliabilityLogger.A09.edit();
                                edit.C1B(A0C);
                                edit.commit();
                                linkedHashMap = new LinkedHashMap();
                            }
                        }
                    }
                    aggregatedReliabilityLogger.A00 = linkedHashMap;
                }
            }
            z = false;
            if (aggregatedReliabilityLogger.A00 != null) {
                z = true;
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        ((X.AnonymousClass09P) r3.A04.get()).CGS("reliability_logger_on_periodic_check_for_stale_data_fail", X.C22298Ase.$const$string(X.AnonymousClass1Y3.A0z));
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x000b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A05() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.A03()     // Catch:{ Exception -> 0x000b }
            if (r0 == 0) goto L_0x001e
            r3.A01()     // Catch:{ Exception -> 0x000b }
            goto L_0x001e
        L_0x000b:
            X.0US r0 = r3.A04     // Catch:{ all -> 0x0020 }
            java.lang.Object r2 = r0.get()     // Catch:{ all -> 0x0020 }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x0020 }
            java.lang.String r1 = "reliability_logger_on_periodic_check_for_stale_data_fail"
            r0 = 131(0x83, float:1.84E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)     // Catch:{ all -> 0x0020 }
            r2.CGS(r1, r0)     // Catch:{ all -> 0x0020 }
        L_0x001e:
            monitor-exit(r3)
            return
        L_0x0020:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.A05():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005b, code lost:
        if (r8.equals("f") == false) goto L_0x005d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A06(com.facebook.messaging.model.messages.Message r7, java.lang.String r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r0 = A04(r6)     // Catch:{ all -> 0x0083 }
            if (r0 != 0) goto L_0x0014
            java.lang.String r1 = r7.A0w     // Catch:{ all -> 0x0083 }
            if (r1 == 0) goto L_0x0081
            java.util.LinkedHashMap r0 = r6.A00     // Catch:{ all -> 0x0083 }
            boolean r0 = r0.containsValue(r1)     // Catch:{ all -> 0x0083 }
            if (r0 == 0) goto L_0x0014
            goto L_0x0081
        L_0x0014:
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo r2 = new com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo     // Catch:{ all -> 0x0083 }
            long r0 = r7.A02     // Catch:{ all -> 0x0083 }
            java.lang.String r4 = "e"
            java.lang.String r3 = "s"
            r2.<init>(r0, r4, r3)     // Catch:{ all -> 0x0083 }
            int r5 = r8.hashCode()     // Catch:{ all -> 0x0083 }
            r0 = 102(0x66, float:1.43E-43)
            r4 = 3
            r3 = 2
            r1 = 1
            if (r5 == r0) goto L_0x0054
            r0 = 103(0x67, float:1.44E-43)
            if (r5 == r0) goto L_0x004a
            r0 = 109(0x6d, float:1.53E-43)
            if (r5 == r0) goto L_0x0040
            r0 = 112(0x70, float:1.57E-43)
            if (r5 != r0) goto L_0x005d
            java.lang.String r0 = "p"
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x0083 }
            r5 = 3
            if (r0 != 0) goto L_0x005e
            goto L_0x005d
        L_0x0040:
            java.lang.String r0 = "m"
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x0083 }
            r5 = 0
            if (r0 != 0) goto L_0x005e
            goto L_0x005d
        L_0x004a:
            java.lang.String r0 = "g"
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x0083 }
            r5 = 1
            if (r0 != 0) goto L_0x005e
            goto L_0x005d
        L_0x0054:
            java.lang.String r0 = "f"
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x0083 }
            r5 = 2
            if (r0 != 0) goto L_0x005e
        L_0x005d:
            r5 = -1
        L_0x005e:
            if (r5 == 0) goto L_0x0070
            if (r5 == r1) goto L_0x006d
            if (r5 == r3) goto L_0x006a
            if (r5 == r4) goto L_0x0067
            goto L_0x0073
        L_0x0067:
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.FAILURE_PERMANENT     // Catch:{ all -> 0x0083 }
            goto L_0x0075
        L_0x006a:
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.FAILURE_RETRYABLE     // Catch:{ all -> 0x0083 }
            goto L_0x0075
        L_0x006d:
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.SUCCESS_GRAPH     // Catch:{ all -> 0x0083 }
            goto L_0x0075
        L_0x0070:
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.SUCCESS_MQTT     // Catch:{ all -> 0x0083 }
            goto L_0x0075
        L_0x0073:
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.UNKNOWN     // Catch:{ all -> 0x0083 }
        L_0x0075:
            r2.outcome = r0     // Catch:{ all -> 0x0083 }
            java.util.LinkedHashMap r1 = r6.A00     // Catch:{ all -> 0x0083 }
            java.lang.String r0 = r7.A0w     // Catch:{ all -> 0x0083 }
            r1.put(r0, r2)     // Catch:{ all -> 0x0083 }
            A02(r6)     // Catch:{ all -> 0x0083 }
        L_0x0081:
            monitor-exit(r6)
            return
        L_0x0083:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.A06(com.facebook.messaging.model.messages.Message, java.lang.String):void");
    }

    public synchronized void A07(Integer num, Message message) {
        boolean z;
        String str;
        C28711fF r1;
        C28761fK r12 = this.A05;
        synchronized (r12) {
            if (C28761fK.A04(r12) && C28761fK.A06(message) && !r12.A02.contains(message.A0w)) {
                AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = (AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) r12.A01.get(message.A0w);
                if (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo == null) {
                    aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = C28761fK.A00(r12, message);
                    if (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo != null) {
                        r12.A01.put(message.A0w, aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo);
                    }
                }
                if (C28761fK.A05(message) && ((MediaResource) message.A0b.get(0)).A0O != null) {
                    aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mediaDurationMs = ((MediaResource) message.A0b.get(0)).A07;
                    aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.downsizedHeight = ((MediaResource) message.A0b.get(0)).A0O.A00;
                    aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.downsizedWidth = ((MediaResource) message.A0b.get(0)).A0O.A01;
                }
                if (num == AnonymousClass07B.A00) {
                    aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mqttAttempts++;
                } else {
                    aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.graphAttempts++;
                }
                C28761fK.A02(r12);
            }
        }
        if (A04(this)) {
            ThreadKey threadKey = message.A0U;
            if (threadKey == null || !((r1 = threadKey.A05) == C28711fF.ONE_TO_ONE || r1 == C28711fF.GROUP)) {
                z = false;
            } else {
                z = true;
            }
            if (z) {
                ReliabilityInfo reliabilityInfo = (ReliabilityInfo) this.A00.get(message.A0w);
                if (reliabilityInfo == null) {
                    long now = this.A03.now();
                    String A032 = this.A07.A03(message);
                    if (message.A0U.A0L()) {
                        str = "g";
                    } else {
                        str = "c";
                    }
                    reliabilityInfo = new ReliabilityInfo(now, A032, str);
                    this.A00.put(message.A0w, reliabilityInfo);
                }
                if (num == AnonymousClass07B.A00) {
                    reliabilityInfo.mqttAttempts++;
                } else {
                    reliabilityInfo.graphAttempts++;
                }
                A02(this);
            }
        }
    }

    public synchronized void A09(Integer num, Message message, long j) {
        A08(num, message, 0, null, null, null, j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        if (r0 == false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A0A(java.lang.Integer r6, java.lang.String r7, com.facebook.messaging.model.threadkey.ThreadKey r8) {
        /*
            r5 = this;
            monitor-enter(r5)
            X.1fK r0 = r5.A05     // Catch:{ all -> 0x005c }
            r0.A08(r6, r7)     // Catch:{ all -> 0x005c }
            X.1tV r0 = r5.A06     // Catch:{ all -> 0x005c }
            X.1tX r0 = r0.A00     // Catch:{ all -> 0x005c }
            r0.A03(r7)     // Catch:{ all -> 0x005c }
            boolean r0 = A04(r5)     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0026
            if (r8 == 0) goto L_0x0028
            if (r8 == 0) goto L_0x0023
            X.1fF r1 = r8.A05     // Catch:{ all -> 0x005c }
            X.1fF r0 = X.C28711fF.ONE_TO_ONE     // Catch:{ all -> 0x005c }
            if (r1 == r0) goto L_0x0021
            X.1fF r0 = X.C28711fF.GROUP     // Catch:{ all -> 0x005c }
            if (r1 != r0) goto L_0x0023
        L_0x0021:
            r0 = 1
            goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 != 0) goto L_0x0028
        L_0x0026:
            monitor-exit(r5)
            return
        L_0x0028:
            java.util.LinkedHashMap r0 = r5.A00     // Catch:{ all -> 0x005c }
            java.lang.Object r4 = r0.get(r7)     // Catch:{ all -> 0x005c }
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo r4 = (com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo) r4     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x0026
            int r1 = r4.graphAttempts     // Catch:{ all -> 0x005c }
            int r0 = r4.mqttAttempts     // Catch:{ all -> 0x005c }
            int r1 = r1 + r0
            r0 = 1
            if (r1 != r0) goto L_0x0043
            java.util.LinkedHashMap r0 = r5.A00     // Catch:{ all -> 0x005c }
            r0.remove(r7)     // Catch:{ all -> 0x005c }
        L_0x003f:
            A02(r5)     // Catch:{ all -> 0x005c }
            goto L_0x0026
        L_0x0043:
            X.06B r0 = r5.A03     // Catch:{ all -> 0x005c }
            long r2 = r0.now()     // Catch:{ all -> 0x005c }
            long r0 = r4.sendAttemptTimestamp     // Catch:{ all -> 0x005c }
            long r2 = r2 - r0
            r4.timeSinceFirstSendAttempt = r2     // Catch:{ all -> 0x005c }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x005c }
            if (r6 != r0) goto L_0x0057
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.SUCCESS_MQTT     // Catch:{ all -> 0x005c }
            r4.outcome = r0     // Catch:{ all -> 0x005c }
            goto L_0x003f
        L_0x0057:
            com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger$ReliabilityInfo$Outcome r0 = com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.ReliabilityInfo.Outcome.SUCCESS_GRAPH     // Catch:{ all -> 0x005c }
            r4.outcome = r0     // Catch:{ all -> 0x005c }
            goto L_0x003f
        L_0x005c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger.A0A(java.lang.Integer, java.lang.String, com.facebook.messaging.model.threadkey.ThreadKey):void");
    }

    public String getSimpleName() {
        return "AggregatedReliabilityLogger";
    }

    public static final AggregatedReliabilityLogger A00(AnonymousClass1XY r14) {
        if (A0D == null) {
            synchronized (AggregatedReliabilityLogger.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0D, r14);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r14.getApplicationInjector();
                        AnonymousClass06B A022 = AnonymousClass067.A02();
                        AnonymousClass18P A003 = AnonymousClass18P.A00(applicationInjector);
                        DeprecatedAnalyticsLogger A004 = C06920cI.A00(applicationInjector);
                        AnonymousClass0WA.A00(applicationInjector);
                        A0D = new AggregatedReliabilityLogger(A022, A003, A004, FbSharedPreferencesModule.A00(applicationInjector), C04750Wa.A03(applicationInjector), AnonymousClass0WT.A00(applicationInjector), C28761fK.A01(applicationInjector), C36651tV.A00(applicationInjector), AnonymousClass1YA.A00(applicationInjector), AnonymousClass0XJ.A0F(applicationInjector), AnonymousClass0UX.A0Q(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0D;
    }

    public synchronized void A08(Integer num, Message message, int i, String str, String str2, String str3, long j) {
        String str4;
        C36651tV r7 = this.A06;
        String str5 = str3;
        C36651tV.A04 = str3;
        C36671tX r15 = r7.A00;
        Message message2 = message;
        String str6 = message2.A0w;
        boolean z = false;
        if (num == AnonymousClass07B.A00) {
            z = true;
        }
        String str7 = str;
        int i2 = i;
        C36701ta r8 = (C36701ta) r15.A04(str6, j, i2, str7, z);
        if (r8 != null && !r7.A00.A07()) {
            int AqL = r7.A03.AqL(568318663002389L, 10);
            int i3 = r8.A02;
            Integer num2 = 10;
            Integer[] numArr = {Integer.valueOf(AqL)};
            for (int i4 = 0; i4 < 1; i4++) {
                Integer num3 = numArr[i4];
                if (num2.compareTo((Object) num3) > 0) {
                    num2 = num3;
                }
            }
            if (i3 < num2.intValue()) {
                r8.A02++;
                ThreadKey threadKey = message2.A0U;
                String l = Long.toString(threadKey.A0G());
                String str8 = message2.A0w;
                String lowerCase = threadKey.A05.toString().toLowerCase(Locale.US);
                ImmutableMap immutableMap = message2.A0g;
                if (immutableMap != null) {
                    str4 = immutableMap.toString();
                } else {
                    str4 = "null";
                }
                C11670nb r4 = new C11670nb("message_send_failure");
                r4.A0D("thread_key", l);
                r4.A0D("thread_type", lowerCase);
                r4.A0D(TraceFieldType.MsgType, r8.A00);
                r4.A0D("offline_threading_key", str8);
                r4.A0A("latency", (r7.A01.now() - r8.A07) / 1000);
                r4.A09("has_failed", 0);
                r4.A0D("error_type", BuildConfig.FLAVOR);
                r4.A0D("error_detail", str2);
                r4.A09(TraceFieldType.ErrorCode, i2);
                r4.A0D("error_msg", str7);
                r4.A0D("exception", str5);
                r4.A0A("attempt_id", r8.A06);
                r4.A0D("client_tags", str4);
                r7.A00.A05(r8, r4);
            }
        }
    }

    private AggregatedReliabilityLogger(AnonymousClass06B r2, AnonymousClass18P r3, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, FbSharedPreferences fbSharedPreferences, AnonymousClass0US r6, C25051Yd r7, C28761fK r8, C36651tV r9, Context context, String str, AnonymousClass0VK r12) {
        this.A03 = r2;
        this.A07 = r3;
        this.A02 = deprecatedAnalyticsLogger;
        this.A09 = fbSharedPreferences;
        this.A04 = r6;
        this.A08 = r7;
        this.A05 = r8;
        this.A06 = r9;
        this.A01 = context;
        this.A0B = str;
        this.A0A = r12;
    }

    public void init() {
        int A032 = C000700l.A03(-1803462056);
        A05();
        if (this.A0B != null) {
            this.A0A.C4Y(new C202889hN(this), 45, TimeUnit.SECONDS);
        }
        C000700l.A09(40293460, A032);
    }
}
