package com.facebook.messaging.contacts.ranking.logging;

import X.AnonymousClass12J;
import X.C24971Xv;
import X.C28931fb;
import X.C33591np;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.collect.ImmutableList;

public final class RankingLoggingItem implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C33591np();
    public final float A00;
    public final ImmutableList A01;
    public final String A02;
    public final String A03;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof RankingLoggingItem) {
                RankingLoggingItem rankingLoggingItem = (RankingLoggingItem) obj;
                if (!C28931fb.A07(this.A02, rankingLoggingItem.A02) || !C28931fb.A07(this.A01, rankingLoggingItem.A01) || !C28931fb.A07(this.A03, rankingLoggingItem.A03) || this.A00 != rankingLoggingItem.A00) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A01(C28931fb.A03(C28931fb.A03(C28931fb.A03(1, this.A02), this.A01), this.A03), this.A00);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A02 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A02);
        }
        parcel.writeInt(this.A01.size());
        C24971Xv it = this.A01.iterator();
        while (it.hasNext()) {
            parcel.writeParcelable((ScoreLoggingItem) it.next(), i);
        }
        if (this.A03 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A03);
        }
        parcel.writeFloat(this.A00);
    }

    public RankingLoggingItem(AnonymousClass12J r3) {
        this.A02 = r3.A02;
        ImmutableList immutableList = r3.A01;
        C28931fb.A06(immutableList, "rawScoreItems");
        this.A01 = immutableList;
        this.A03 = r3.A03;
        this.A00 = r3.A00;
    }

    public RankingLoggingItem(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A02 = null;
        } else {
            this.A02 = parcel.readString();
        }
        int readInt = parcel.readInt();
        ScoreLoggingItem[] scoreLoggingItemArr = new ScoreLoggingItem[readInt];
        for (int i = 0; i < readInt; i++) {
            scoreLoggingItemArr[i] = (ScoreLoggingItem) parcel.readParcelable(ScoreLoggingItem.class.getClassLoader());
        }
        this.A01 = ImmutableList.copyOf(scoreLoggingItemArr);
        if (parcel.readInt() == 0) {
            this.A03 = null;
        } else {
            this.A03 = parcel.readString();
        }
        this.A00 = parcel.readFloat();
    }
}
