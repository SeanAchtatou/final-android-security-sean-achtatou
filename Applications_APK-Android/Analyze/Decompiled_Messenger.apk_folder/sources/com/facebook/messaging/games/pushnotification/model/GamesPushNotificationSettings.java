package com.facebook.messaging.games.pushnotification.model;

import X.AnonymousClass7R8;
import X.C184613o;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.facebook.common.json.AutoGenJsonSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@AutoGenJsonDeserializer
@AutoGenJsonSerializer
@JsonDeserialize(using = GamesPushNotificationSettingsDeserializer.class)
@JsonSerialize(using = GamesPushNotificationSettingsSerializer.class)
public class GamesPushNotificationSettings implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C184613o();
    @JsonProperty("mute_until_seconds")
    public final long muteUntilSeconds;
    @JsonProperty("push_notification_states")
    public final ImmutableList<InstantGamePushNotificationState> pushNotificationStates;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof GamesPushNotificationSettings)) {
            if (this == obj) {
                return true;
            }
            GamesPushNotificationSettings gamesPushNotificationSettings = (GamesPushNotificationSettings) obj;
            if (this.muteUntilSeconds == gamesPushNotificationSettings.muteUntilSeconds) {
                ImmutableList<InstantGamePushNotificationState> immutableList = this.pushNotificationStates;
                ImmutableList<InstantGamePushNotificationState> immutableList2 = gamesPushNotificationSettings.pushNotificationStates;
                if (immutableList == null) {
                    if (immutableList2 == null) {
                        return true;
                    }
                } else if (immutableList.equals(immutableList2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.muteUntilSeconds), this.pushNotificationStates});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.muteUntilSeconds);
        parcel.writeList(this.pushNotificationStates);
    }

    private GamesPushNotificationSettings() {
        this.muteUntilSeconds = 0;
        this.pushNotificationStates = null;
    }

    public GamesPushNotificationSettings(AnonymousClass7R8 r3) {
        this.muteUntilSeconds = r3.A00;
        this.pushNotificationStates = r3.A01;
    }

    public GamesPushNotificationSettings(Parcel parcel) {
        this.muteUntilSeconds = parcel.readLong();
        ArrayList readArrayList = parcel.readArrayList(InstantGamePushNotificationState.class.getClassLoader());
        this.pushNotificationStates = readArrayList != null ? ImmutableList.copyOf((Collection) readArrayList) : null;
    }
}
