package com.facebook.messaging.games.pushnotification.model;

import X.AnonymousClass7RD;
import X.C103424x9;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.facebook.common.json.AutoGenJsonSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
import java.util.Arrays;

@AutoGenJsonDeserializer
@AutoGenJsonSerializer
@JsonDeserialize(using = InstantGamePushNotificationStateDeserializer.class)
@JsonSerialize(using = InstantGamePushNotificationStateSerializer.class)
public class InstantGamePushNotificationState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C103424x9();
    @JsonProperty("game_id")
    public final String gameId;
    @JsonProperty("mute_until_seconds")
    public final long muteUntilSeconds;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof InstantGamePushNotificationState)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        InstantGamePushNotificationState instantGamePushNotificationState = (InstantGamePushNotificationState) obj;
        return Objects.equal(this.gameId, instantGamePushNotificationState.gameId) && Objects.equal(Long.valueOf(this.muteUntilSeconds), Long.valueOf(instantGamePushNotificationState.muteUntilSeconds));
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.gameId, Long.valueOf(this.muteUntilSeconds)});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.muteUntilSeconds);
        parcel.writeString(this.gameId);
    }

    private InstantGamePushNotificationState() {
        this.muteUntilSeconds = 0;
        this.gameId = null;
    }

    public InstantGamePushNotificationState(AnonymousClass7RD r3) {
        this.muteUntilSeconds = r3.A00;
        this.gameId = r3.A01;
    }

    public InstantGamePushNotificationState(Parcel parcel) {
        this.muteUntilSeconds = parcel.readLong();
        this.gameId = parcel.readString();
    }
}
