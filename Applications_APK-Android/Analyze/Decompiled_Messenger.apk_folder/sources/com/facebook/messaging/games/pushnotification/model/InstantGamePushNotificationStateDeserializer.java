package com.facebook.messaging.games.pushnotification.model;

import com.facebook.common.json.FbJsonDeserializer;
import java.util.Map;

public class InstantGamePushNotificationStateDeserializer extends FbJsonDeserializer {
    private static Map sSchema;

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0061, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r1 != null) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.common.json.FbJsonField getField(java.lang.String r6) {
        /*
            r5 = this;
            java.lang.Class<com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationStateDeserializer> r4 = com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationStateDeserializer.class
            monitor-enter(r4)
            java.util.Map r0 = com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationStateDeserializer.sSchema     // Catch:{ all -> 0x006e }
            if (r0 != 0) goto L_0x000f
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x006e }
            r0.<init>()     // Catch:{ all -> 0x006e }
            com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationStateDeserializer.sSchema = r0     // Catch:{ all -> 0x006e }
            goto L_0x0019
        L_0x000f:
            java.lang.Object r1 = r0.get(r6)     // Catch:{ all -> 0x006e }
            com.facebook.common.json.FbJsonField r1 = (com.facebook.common.json.FbJsonField) r1     // Catch:{ all -> 0x006e }
            if (r1 == 0) goto L_0x0019
        L_0x0017:
            monitor-exit(r4)     // Catch:{ all -> 0x006e }
            goto L_0x0061
        L_0x0019:
            r3 = -1
            int r2 = r6.hashCode()     // Catch:{ Exception -> 0x0068 }
            r0 = -1819658504(0xffffffff938a36f8, float:-3.48903E-27)
            r1 = 1
            if (r2 == r0) goto L_0x0032
            r0 = -195606392(0xfffffffff4574888, float:-6.822601E31)
            if (r2 != r0) goto L_0x003d
            java.lang.String r0 = "game_id"
            boolean r0 = r6.equals(r0)     // Catch:{ Exception -> 0x0068 }
            if (r0 == 0) goto L_0x003d
            goto L_0x003c
        L_0x0032:
            java.lang.String r0 = "mute_until_seconds"
            boolean r0 = r6.equals(r0)     // Catch:{ Exception -> 0x0068 }
            if (r0 == 0) goto L_0x003d
            r3 = 0
            goto L_0x003d
        L_0x003c:
            r3 = 1
        L_0x003d:
            if (r3 == 0) goto L_0x004f
            if (r3 == r1) goto L_0x0042
            goto L_0x0062
        L_0x0042:
            java.lang.Class<com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationState> r1 = com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationState.class
            java.lang.String r0 = "gameId"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch:{ Exception -> 0x0068 }
            com.facebook.common.json.FbJsonField r1 = com.facebook.common.json.FbJsonField.jsonField(r0)     // Catch:{ Exception -> 0x0068 }
            goto L_0x005b
        L_0x004f:
            java.lang.Class<com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationState> r1 = com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationState.class
            java.lang.String r0 = "muteUntilSeconds"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch:{ Exception -> 0x0068 }
            com.facebook.common.json.FbJsonField r1 = com.facebook.common.json.FbJsonField.jsonField(r0)     // Catch:{ Exception -> 0x0068 }
        L_0x005b:
            java.util.Map r0 = com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationStateDeserializer.sSchema     // Catch:{ all -> 0x006e }
            r0.put(r6, r1)     // Catch:{ all -> 0x006e }
            goto L_0x0017
        L_0x0061:
            return r1
        L_0x0062:
            com.facebook.common.json.FbJsonField r0 = super.getField(r6)     // Catch:{ Exception -> 0x0068 }
            monitor-exit(r4)     // Catch:{ all -> 0x006e }
            return r0
        L_0x0068:
            r0 = move-exception
            java.lang.RuntimeException r0 = com.google.common.base.Throwables.propagate(r0)     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x006e:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x006e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.games.pushnotification.model.InstantGamePushNotificationStateDeserializer.getField(java.lang.String):com.facebook.common.json.FbJsonField");
    }

    public InstantGamePushNotificationStateDeserializer() {
        init(InstantGamePushNotificationState.class);
    }
}
