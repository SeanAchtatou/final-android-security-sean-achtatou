package com.facebook.messaging.tincan.attachments;

import X.AnonymousClass0TB;
import X.AnonymousClass0UX;
import X.AnonymousClass0WT;
import X.AnonymousClass1XX;
import X.AnonymousClass29Z;
import X.AnonymousClass29s;
import X.AnonymousClass29u;
import X.C25051Yd;
import X.C30441i3;
import X.C423029r;
import java.util.concurrent.ExecutorService;

public class DecryptedAttachmentProvider extends AnonymousClass0TB {
    public static final Class A07 = DecryptedAttachmentProvider.class;
    public AnonymousClass29Z A00;
    public AnonymousClass29u A01;
    public AnonymousClass29s A02;
    public C423029r A03;
    public C30441i3 A04;
    public C25051Yd A05;
    public ExecutorService A06;

    public void A0G() {
        super.A0G();
        AnonymousClass1XX r7 = AnonymousClass1XX.get(getContext());
        ExecutorService A0a = AnonymousClass0UX.A0a(r7);
        AnonymousClass29s A002 = AnonymousClass29s.A00(r7);
        AnonymousClass29Z r4 = new AnonymousClass29Z(r7);
        C30441i3 A042 = C30441i3.A04(r7);
        AnonymousClass29u A003 = AnonymousClass29u.A00(r7);
        C25051Yd A004 = AnonymousClass0WT.A00(r7);
        C423029r r0 = new C423029r(r7);
        this.A06 = A0a;
        this.A02 = A002;
        this.A00 = r4;
        this.A04 = A042;
        this.A01 = A003;
        this.A05 = A004;
        this.A03 = r0;
    }
}
