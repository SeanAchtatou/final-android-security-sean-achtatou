package com.facebook.messaging.tincan.messenger;

import X.AnonymousClass067;
import X.AnonymousClass06A;
import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass0UX;
import X.AnonymousClass0VG;
import X.AnonymousClass0VL;
import X.AnonymousClass0WD;
import X.AnonymousClass0WT;
import X.AnonymousClass0XM;
import X.AnonymousClass0XN;
import X.AnonymousClass183;
import X.AnonymousClass197;
import X.AnonymousClass19A;
import X.AnonymousClass1CJ;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass94R;
import X.AoD;
import X.AoN;
import X.AoT;
import X.Ap6;
import X.Aw8;
import X.C007406x;
import X.C010708t;
import X.C04310Tq;
import X.C04490Ux;
import X.C05350Yp;
import X.C06140av;
import X.C12420pJ;
import X.C193917y;
import X.C194718h;
import X.C194818i;
import X.C22030An2;
import X.C22036AnC;
import X.C22039AnF;
import X.C22055Anb;
import X.C22087AoC;
import X.C22110Aoj;
import X.C22335Aw3;
import X.C22338Aw9;
import X.C22345AwP;
import X.C25051Yd;
import X.C30111hV;
import X.C36471tB;
import X.C36501tE;
import X.C36521tG;
import X.C36571tL;
import X.C36601tP;
import X.C36611tQ;
import X.C36621tR;
import X.C36631tS;
import X.C36641tT;
import X.C36721tc;
import X.C36731td;
import X.C36741te;
import X.C48942bO;
import X.C55592oK;
import X.C88254Jj;
import android.content.ContentValues;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
public final class TincanPreKeyManager implements CallerContextable {
    public static final AnonymousClass19A A0P = new AnonymousClass19A("last_prekey_upload_timestamp_ms");
    public static final Class A0Q = TincanPreKeyManager.class;
    public static final AtomicBoolean A0R = new AtomicBoolean(false);
    private static final AnonymousClass19A A0S = new AnonymousClass19A("prekey_upload_state");
    private static volatile TincanPreKeyManager A0T;
    public ListenableFuture A00;
    public final AnonymousClass06A A01;
    public final C36721tc A02;
    public final C194718h A03;
    public final C12420pJ A04;
    public final C36601tP A05;
    public final C36621tR A06;
    public final C36611tQ A07;
    public final C04310Tq A08;
    public final C04310Tq A09;
    public final C04310Tq A0A;
    private final Resources A0B;
    private final AnonymousClass0XN A0C;
    private final BlueServiceOperationFactory A0D;
    private final C36521tG A0E;
    private final C36571tL A0F;
    private final C193917y A0G;
    private final C36501tE A0H;
    private final C36731td A0I;
    private final C36641tT A0J;
    private final C36741te A0K;
    private final C36631tS A0L;
    private final C25051Yd A0M;
    private final AnonymousClass0VL A0N;
    private final Map A0O = Collections.synchronizedMap(new HashMap());

    public static synchronized void A01(TincanPreKeyManager tincanPreKeyManager, Integer num) {
        int i;
        synchronized (tincanPreKeyManager) {
            C55592oK r2 = (C55592oK) tincanPreKeyManager.A08.get();
            AnonymousClass19A r1 = A0S;
            switch (num.intValue()) {
                case 1:
                    i = 1;
                    break;
                case 2:
                    i = 2;
                    break;
                case 3:
                    i = 3;
                    break;
                case 4:
                    i = 4;
                    break;
                case 5:
                    i = 5;
                    break;
                default:
                    i = 0;
                    break;
            }
            r2.A06(r1, Integer.toString(i));
        }
    }

    public synchronized C22110Aoj A05() {
        int i;
        C22110Aoj A022;
        AnonymousClass19A r3 = AnonymousClass197.A07;
        synchronized (r3) {
            String A052 = ((C55592oK) this.A08.get()).A05(r3);
            if (A052 == null) {
                i = 1;
            } else {
                try {
                    i = Integer.parseInt(A052);
                } catch (NumberFormatException unused) {
                    i = 1;
                }
            }
            try {
                AoN aoN = (AoN) this.A0A.get();
                while (aoN.AUE(i)) {
                    i++;
                }
                try {
                    C36601tP r1 = this.A05;
                    synchronized (r1) {
                        A022 = AoT.A02(((C48942bO) r1.A01.get()).ApI(), i);
                    }
                    int i2 = i + 1;
                    while (aoN.AUE(i2)) {
                        i2++;
                    }
                    ((C55592oK) this.A08.get()).A06(r3, Integer.toString(i2));
                } catch (Ap6 e) {
                    C010708t.A08(A0Q, "Failed to generate signed pre-key", e);
                    throw new RuntimeException(e);
                }
            } catch (Throwable th) {
                th = th;
                throw th;
            }
        }
        AoD aoD = (AoD) this.A0A.get();
        int i3 = A022.A00.id_;
        synchronized (aoD) {
            try {
                aoD.A03.A00(i3, A022.A00.CJ3(), aoD.A01.now());
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
        return A022;
    }

    public synchronized void A06() {
        if (!this.A0C.A0I()) {
            A0R.set(true);
        } else if (this.A00 == null) {
            ListenableFuture CIC = this.A0N.CIC(new C22087AoC(this));
            this.A00 = CIC;
            C05350Yp.A08(CIC, new C88254Jj(this), this.A0N);
        }
    }

    public synchronized void A08(ThreadKey threadKey) {
        Bundle bundle = new Bundle();
        bundle.putString("thread_key", threadKey.A0J());
        this.A0D.newInstance("CompletePrekeyDelivery", bundle, 1, CallerContext.A04(TincanPreKeyManager.class)).CGe();
    }

    public synchronized void A09(ThreadKey threadKey, AnonymousClass94R r6) {
        int i = 2131832770;
        switch (r6.ordinal()) {
            case 4:
                this.A0F.A02(threadKey, false, false);
                break;
            case 5:
                this.A0H.A05(threadKey, true);
                break;
            case AnonymousClass1Y3.A01:
                i = 2131821344;
                break;
        }
        this.A0J.A02(threadKey, this.A0B.getString(i), AnonymousClass07B.A0p, String.valueOf(r6));
        A0A(threadKey, AnonymousClass07B.A0N);
    }

    public static final TincanPreKeyManager A00(AnonymousClass1XY r28) {
        if (A0T == null) {
            synchronized (TincanPreKeyManager.class) {
                AnonymousClass1XY r1 = r28;
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0T, r1);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r1.getApplicationInjector();
                        A0T = new TincanPreKeyManager(AnonymousClass0UX.A0N(applicationInjector), C30111hV.A00(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.AZ9, applicationInjector), C193917y.A00(applicationInjector), C194718h.A06(applicationInjector), C36521tG.A03(applicationInjector), AnonymousClass067.A0A(applicationInjector), new C36601tP(AnonymousClass0VG.A00(AnonymousClass1Y3.As6, applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.AZ9, applicationInjector)), AnonymousClass0VG.A00(AnonymousClass1Y3.B1A, applicationInjector), C36611tQ.A00(applicationInjector), C36621tR.A00(applicationInjector), C36631tS.A00(applicationInjector), C36641tT.A00(applicationInjector), C04490Ux.A0L(applicationInjector), C36721tc.A00(applicationInjector), AnonymousClass0XM.A00(applicationInjector), C12420pJ.A00(applicationInjector), AnonymousClass0WT.A00(applicationInjector), C36571tL.A00(applicationInjector), C36501tE.A00(applicationInjector), new C36731td(applicationInjector), C36741te.A00(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.As6, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0T;
    }

    private boolean A02(ThreadKey threadKey) {
        int AqL = this.A0M.AqL(564801085375426L, 0);
        if (AqL == 0) {
            return false;
        }
        if (!this.A0O.containsKey(threadKey) || this.A01.now() - ((Long) this.A0O.get(threadKey)).longValue() > ((long) AqL)) {
            return true;
        }
        return false;
    }

    public Integer A04(ThreadKey threadKey) {
        Integer A072;
        synchronized (AnonymousClass08S.A0J("com.facebook.messaging.tincan.messenger.TincanPreKeyManager", threadKey.A0J()).intern()) {
            A072 = this.A0G.A07(threadKey);
        }
        return A072;
    }

    public void A07(ThreadKey threadKey) {
        if (threadKey != null && this.A0M.AqL(564801084326847L, 0) > 0) {
            A0A(threadKey, AnonymousClass07B.A0N);
        }
    }

    public void A0A(ThreadKey threadKey, Integer num) {
        synchronized (AnonymousClass08S.A0J("com.facebook.messaging.tincan.messenger.TincanPreKeyManager", threadKey.A0J()).intern()) {
            if (AnonymousClass07B.A0Y.equals(num) || AnonymousClass07B.A01.equals(num) || AnonymousClass07B.A0n.equals(num) || AnonymousClass07B.A0i.equals(num)) {
                this.A0O.put(threadKey, Long.valueOf(this.A01.now()));
            }
            this.A0E.A0F(threadKey, num);
        }
    }

    /* JADX INFO: finally extract failed */
    public synchronized void A0B(ThreadKey threadKey, Long l, Aw8 aw8, boolean z) {
        Bundle bundle = new Bundle();
        Bundle bundle2 = new Bundle();
        Aw8 aw82 = aw8;
        bundle2.putString("codename", aw82.suggested_codename);
        bundle2.putLong("user_id_to", aw82.msg_to.user_id.longValue());
        bundle2.putString("device_id_to", aw82.msg_to.instance_id);
        bundle2.putInt("prekey_id", aw82.pre_key_with_id.id.intValue());
        bundle2.putByteArray("prekey", aw82.pre_key_with_id.public_key);
        bundle2.putInt("signed_prekey_id", aw82.signed_pre_key_with_id.public_key_with_id.id.intValue());
        bundle2.putByteArray("signed_prekey", aw82.signed_pre_key_with_id.public_key_with_id.public_key);
        bundle2.putByteArray("signed_prekey_signature", aw82.signed_pre_key_with_id.signature);
        bundle2.putByteArray("identity_key", aw82.identity_key);
        bundle.putBundle("prekey_bundle", bundle2);
        bundle.putBoolean("is_multidevice", z);
        this.A0D.newInstance("TincanProcessNewPreKey", bundle, 1, CallerContext.A04(TincanPreKeyManager.class)).CGe();
        C194718h r4 = this.A03;
        C22338Aw9 aw9 = aw82.msg_to;
        String l2 = aw9.user_id.toString();
        String str = aw9.instance_id;
        String str2 = aw82.suggested_codename;
        SQLiteDatabase A012 = ((AnonymousClass183) r4.A03.get()).A01();
        ThreadKey threadKey2 = threadKey;
        C06140av A013 = C194718h.A01(threadKey2, l2, str);
        Cursor query = A012.query("thread_devices", C194718h.A08, A013.A02(), A013.A04(), null, null, null);
        try {
            Long l3 = null;
            if (query.moveToNext()) {
                l3 = Long.valueOf(query.getLong(0));
            }
            query.close();
            Long l4 = l;
            if (l3 == null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(C194818i.A0A.A00, threadKey2.A0J());
                contentValues.put(C194818i.A01.A00, l2);
                contentValues.put(C194818i.A05.A00, str);
                contentValues.put(C194818i.A04.A00, str2);
                contentValues.put(C194818i.A03.A00, l4);
                C007406x.A00(-1294258325);
                A012.insert("thread_devices", null, contentValues);
                C007406x.A00(-1134665043);
            } else if (l4.longValue() < l3.longValue()) {
                C010708t.A06(C194718h.class, "Dropping update which is older than current entry.");
            } else {
                C06140av A014 = C194718h.A01(threadKey2, l2, str);
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put(C194818i.A04.A00, str2);
                contentValues2.put(C194818i.A03.A00, l4);
                A012.update("thread_devices", contentValues2, A014.A02(), A014.A04());
            }
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        X.AnonymousClass07A.A04(r3.A0N, new X.AnonymousClass2HO(r3, r4, r5), 2098728563);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0032, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D(com.facebook.messaging.model.threadkey.ThreadKey r4, java.util.List r5, boolean r6) {
        /*
            r3 = this;
            java.lang.String r1 = "com.facebook.messaging.tincan.messenger.TincanPreKeyManager"
            java.lang.String r0 = r4.A0J()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.String r1 = r0.intern()
            monitor-enter(r1)
            boolean r0 = r3.A03(r4)     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x0017
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            return
        L_0x0017:
            if (r6 == 0) goto L_0x001f
            java.lang.Integer r0 = X.AnonymousClass07B.A0n     // Catch:{ all -> 0x0033 }
            r3.A0A(r4, r0)     // Catch:{ all -> 0x0033 }
            goto L_0x0024
        L_0x001f:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0033 }
            r3.A0A(r4, r0)     // Catch:{ all -> 0x0033 }
        L_0x0024:
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            X.0VL r2 = r3.A0N
            X.2HO r1 = new X.2HO
            r1.<init>(r3, r4, r5)
            r0 = 2098728563(0x7d180e73, float:1.2632355E37)
            X.AnonymousClass07A.A04(r2, r1, r0)
            return
        L_0x0033:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.tincan.messenger.TincanPreKeyManager.A0D(com.facebook.messaging.model.threadkey.ThreadKey, java.util.List, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        X.AnonymousClass07A.A04(r3.A0N, new X.C22032An6(r3, r4), -2136933270);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0032, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0E(com.facebook.messaging.model.threadkey.ThreadKey r4, boolean r5) {
        /*
            r3 = this;
            java.lang.String r1 = "com.facebook.messaging.tincan.messenger.TincanPreKeyManager"
            java.lang.String r0 = r4.A0J()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.String r1 = r0.intern()
            monitor-enter(r1)
            boolean r0 = r3.A03(r4)     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x0017
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            return
        L_0x0017:
            if (r5 == 0) goto L_0x001f
            java.lang.Integer r0 = X.AnonymousClass07B.A0n     // Catch:{ all -> 0x0033 }
            r3.A0A(r4, r0)     // Catch:{ all -> 0x0033 }
            goto L_0x0024
        L_0x001f:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0033 }
            r3.A0A(r4, r0)     // Catch:{ all -> 0x0033 }
        L_0x0024:
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            X.0VL r2 = r3.A0N
            X.An6 r1 = new X.An6
            r1.<init>(r3, r4)
            r0 = -2136933270(0xffffffff80a0fc6a, float:-1.4784228E-38)
            X.AnonymousClass07A.A04(r2, r1, r0)
            return
        L_0x0033:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.tincan.messenger.TincanPreKeyManager.A0E(com.facebook.messaging.model.threadkey.ThreadKey, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        if (r3 == X.AnonymousClass07B.A0i) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0F(com.facebook.messaging.model.threadkey.ThreadKey r6) {
        /*
            r5 = this;
            java.lang.String r1 = "com.facebook.messaging.tincan.messenger.TincanPreKeyManager"
            java.lang.String r0 = r6.A0J()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.String r4 = r0.intern()
            monitor-enter(r4)
            java.lang.Integer r3 = r5.A04(r6)     // Catch:{ all -> 0x0036 }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0036 }
            if (r3 == r0) goto L_0x0033
            java.lang.Integer r0 = X.AnonymousClass07B.A0n     // Catch:{ all -> 0x0036 }
            if (r3 == r0) goto L_0x0033
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y     // Catch:{ all -> 0x0036 }
            if (r3 != r0) goto L_0x002e
            X.1td r0 = r5.A0I     // Catch:{ all -> 0x0036 }
            X.1Yd r2 = r0.A00     // Catch:{ all -> 0x0036 }
            r0 = 283326108010823(0x101af00060947, double:1.39981696538054E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ all -> 0x0036 }
            if (r0 == 0) goto L_0x0033
        L_0x002e:
            java.lang.Integer r1 = X.AnonymousClass07B.A0i     // Catch:{ all -> 0x0036 }
            r0 = 0
            if (r3 != r1) goto L_0x0034
        L_0x0033:
            r0 = 1
        L_0x0034:
            monitor-exit(r4)     // Catch:{ all -> 0x0036 }
            return r0
        L_0x0036:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0036 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.tincan.messenger.TincanPreKeyManager.A0F(com.facebook.messaging.model.threadkey.ThreadKey):boolean");
    }

    public boolean A0G(ThreadKey threadKey) {
        synchronized (AnonymousClass08S.A0J("com.facebook.messaging.tincan.messenger.TincanPreKeyManager", threadKey.A0J()).intern()) {
            if (!A0F(threadKey)) {
                return true;
            }
            boolean A022 = A02(threadKey);
            return A022;
        }
    }

    public boolean A0H(ThreadKey threadKey) {
        Integer A042;
        if ((!this.A03.A0J(threadKey) && this.A03.A0I(threadKey)) || (A042 = A04(threadKey)) == AnonymousClass07B.A00 || A042 == AnonymousClass07B.A0N) {
            return true;
        }
        return false;
    }

    private boolean A03(ThreadKey threadKey) {
        Integer A042 = A04(threadKey);
        if ((A042 == AnonymousClass07B.A01 || A042 == AnonymousClass07B.A0i || A042 == AnonymousClass07B.A0n) && !A02(threadKey)) {
            threadKey.toString();
            return false;
        } else if (!threadKey.A0K()) {
            C010708t.A0B(A0Q, "Unable to look up keys for thread type %s", threadKey.A05.toString());
            return false;
        } else {
            Preconditions.checkArgument(ThreadKey.A0F(threadKey));
            return true;
        }
    }

    public void A0C(ThreadKey threadKey, List list) {
        ImmutableList A062 = AnonymousClass1CJ.A00(list).A05(new C22055Anb(this, threadKey)).A06();
        C36631tS r1 = this.A0L;
        synchronized (r1) {
            if (!r1.A0C()) {
                C010708t.A05(C36631tS.A03, "Stored procedure sender not available for batch lookup");
            } else if (r1.A01.A03()) {
                C010708t.A05(C36631tS.A03, "Invalid device id");
            } else {
                byte[] bytes = C36471tB.A03(threadKey.A0J()).getBytes(Charset.defaultCharset());
                C22345AwP awP = new C22345AwP(A062);
                C22036AnC anC = C22036AnC.A03;
                C22335Aw3 aw3 = new C22335Aw3();
                aw3.setField_ = 21;
                aw3.value_ = awP;
                r1.A0B(C22030An2.A01(C22039AnF.A01(null, new C22338Aw9(Long.valueOf(Long.parseLong((String) r1.A02.get())), r1.A01.A02()), r1.A00.now() * 1000, anC, aw3, bytes, null)));
            }
        }
    }

    private TincanPreKeyManager(AnonymousClass0VL r2, BlueServiceOperationFactory blueServiceOperationFactory, C04310Tq r4, C193917y r5, C194718h r6, C36521tG r7, AnonymousClass06A r8, C36601tP r9, C04310Tq r10, C36611tQ r11, C36621tR r12, C36631tS r13, C36641tT r14, Resources resources, C36721tc r16, AnonymousClass0XN r17, C12420pJ r18, C25051Yd r19, C36571tL r20, C36501tE r21, C36731td r22, C36741te r23, C04310Tq r24) {
        this.A0N = r2;
        this.A0D = blueServiceOperationFactory;
        this.A08 = r4;
        this.A0G = r5;
        this.A03 = r6;
        this.A0E = r7;
        this.A01 = r8;
        this.A05 = r9;
        this.A0A = r10;
        this.A07 = r11;
        this.A06 = r12;
        this.A0L = r13;
        this.A0J = r14;
        this.A0B = resources;
        this.A02 = r16;
        this.A0C = r17;
        this.A04 = r18;
        this.A0M = r19;
        this.A0F = r20;
        this.A0H = r21;
        this.A0I = r22;
        this.A0K = r23;
        this.A09 = r24;
        r11.A0A(this);
        this.A06.A0A(this);
        this.A0L.A0A(this);
        this.A0K.A0A(this);
    }
}
