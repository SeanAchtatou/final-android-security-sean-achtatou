package com.facebook.messaging.business.common.calltoaction.model;

import X.C30071hR;
import X.C30081hS;
import android.os.Parcel;
import android.os.Parcelable;

public final class CTAPaymentInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C30081hS();
    public final String A00;
    public final String A01;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }

    public CTAPaymentInfo(C30071hR r2) {
        this.A01 = r2.A01;
        this.A00 = r2.A00;
    }

    public CTAPaymentInfo(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
    }
}
