package com.facebook.messaging.business.inboxads.common;

import X.C27161ck;
import X.C28931fb;
import X.C36021sC;
import X.C36031sD;
import X.C36051sF;
import X.C36071sH;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;

public final class InboxAdsItem extends InboxUnitItem implements C36031sD, Parcelable {
    public static final Parcelable.Creator CREATOR = new C36071sH();
    public InboxAdsData A00;

    public String Ac7() {
        return null;
    }

    public String Ac6() {
        return this.A00.A0E;
    }

    public String AcF() {
        return this.A00.A0F;
    }

    public ImmutableList AvU() {
        return this.A00.A07();
    }

    public int B7T() {
        return this.A00.A0D.intValue();
    }

    public void A0B(int i) {
        super.A0B(i);
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < AvU().size(); i2++) {
            C36021sC r1 = new C36021sC((InboxAdsMediaInfo) AvU().get(i2));
            r1.A01 = i;
            arrayList.add(i2, new InboxAdsMediaInfo(r1));
        }
        C36051sF r2 = new C36051sF(this.A00);
        ImmutableList copyOf = ImmutableList.copyOf((Collection) arrayList);
        r2.A07 = copyOf;
        C28931fb.A06(copyOf, "adMediaInfos");
        r2.A0J.add("adMediaInfos");
        this.A00 = new InboxAdsData(r2);
    }

    public int Ac9() {
        return A07().A00;
    }

    public long AcG() {
        return A07().ArZ();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.A00, i);
    }

    public InboxAdsItem(C27161ck r2, GSTModelShape1S0000000 gSTModelShape1S0000000, InboxAdsData inboxAdsData) {
        super(r2, gSTModelShape1S0000000, null);
        this.A00 = inboxAdsData;
    }

    public InboxAdsItem(Parcel parcel) {
        super(parcel);
        this.A00 = (InboxAdsData) parcel.readParcelable(InboxAdsData.class.getClassLoader());
    }
}
