package com.facebook.messaging.business.common.calltoaction.model;

import X.C30011hL;
import X.C30021hM;
import android.os.Parcel;
import android.os.Parcelable;

public final class CTAUserConfirmation implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C30021hM();
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeString(this.A03);
        parcel.writeString(this.A00);
    }

    public CTAUserConfirmation(C30011hL r2) {
        this.A02 = r2.A02;
        this.A01 = r2.A01;
        this.A03 = r2.A03;
        this.A00 = r2.A00;
    }

    public CTAUserConfirmation(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A03 = parcel.readString();
        this.A00 = parcel.readString();
    }
}
