package com.facebook.messaging.business.common.calltoaction.model;

import X.C04160So;
import X.C30031hN;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.informationidentify.model.PIISinglePage;

public final class CTAInformationIdentify implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C30031hN();
    public final PIISinglePage A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A05);
        parcel.writeString(this.A03);
        parcel.writeString(this.A04);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeParcelable(this.A00, i);
    }

    public CTAInformationIdentify(C04160So r2) {
        this.A05 = r2.A05;
        this.A03 = r2.A03;
        this.A04 = r2.A04;
        this.A02 = r2.A02;
        this.A01 = r2.A01;
        this.A00 = r2.A00;
    }

    public CTAInformationIdentify(Parcel parcel) {
        this.A05 = parcel.readString();
        this.A03 = parcel.readString();
        this.A04 = parcel.readString();
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A00 = (PIISinglePage) parcel.readParcelable(PIISinglePage.class.getClassLoader());
    }
}
