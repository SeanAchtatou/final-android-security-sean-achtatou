package com.facebook.messaging.business.inboxads.common;

import X.C24971Xv;
import X.C28931fb;
import X.C35861rw;
import X.C35951s5;
import X.C36021sC;
import X.C36031sD;
import X.C36041sE;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.common.calltoaction.model.AdCallToAction;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class InboxAdsMediaInfo implements C36031sD, Parcelable {
    private static volatile ImmutableList A0L;
    private static volatile ImmutableList A0M;
    private static volatile ImmutableList A0N;
    public static final Parcelable.Creator CREATOR = new C36041sE();
    public final double A00;
    public final int A01;
    public final int A02;
    public final long A03;
    public final AdCallToAction A04;
    public final InboxAdsImage A05;
    public final InboxAdsVideo A06;
    public final ImmutableList A07;
    public final ImmutableList A08;
    public final ImmutableList A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;
    public final Set A0J;
    public final boolean A0K;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof InboxAdsMediaInfo) {
                InboxAdsMediaInfo inboxAdsMediaInfo = (InboxAdsMediaInfo) obj;
                if (!C28931fb.A07(A00(), inboxAdsMediaInfo.A00()) || !C28931fb.A07(this.A0A, inboxAdsMediaInfo.A0A) || !C28931fb.A07(this.A0B, inboxAdsMediaInfo.A0B) || this.A01 != inboxAdsMediaInfo.A01 || !C28931fb.A07(this.A0C, inboxAdsMediaInfo.A0C) || !C28931fb.A07(this.A0D, inboxAdsMediaInfo.A0D) || this.A03 != inboxAdsMediaInfo.A03 || !C28931fb.A07(A01(), inboxAdsMediaInfo.A01()) || this.A00 != inboxAdsMediaInfo.A00 || !C28931fb.A07(this.A04, inboxAdsMediaInfo.A04) || !C28931fb.A07(this.A0E, inboxAdsMediaInfo.A0E) || !C28931fb.A07(this.A0F, inboxAdsMediaInfo.A0F) || !C28931fb.A07(this.A05, inboxAdsMediaInfo.A05) || this.A0K != inboxAdsMediaInfo.A0K || !C28931fb.A07(AvU(), inboxAdsMediaInfo.AvU()) || !C28931fb.A07(this.A0G, inboxAdsMediaInfo.A0G) || !C28931fb.A07(this.A0H, inboxAdsMediaInfo.A0H) || !C28931fb.A07(this.A0I, inboxAdsMediaInfo.A0I) || this.A02 != inboxAdsMediaInfo.A02 || !C28931fb.A07(this.A06, inboxAdsMediaInfo.A06)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public ImmutableList A00() {
        if (this.A0J.contains("adCardTypes")) {
            return this.A07;
        }
        if (A0L == null) {
            synchronized (this) {
                if (A0L == null) {
                    A0L = RegularImmutableList.A02;
                }
            }
        }
        return A0L;
    }

    public ImmutableList A01() {
        if (this.A0J.contains("adTypes")) {
            return this.A08;
        }
        if (A0M == null) {
            synchronized (this) {
                if (A0M == null) {
                    A0M = RegularImmutableList.A02;
                }
            }
        }
        return A0M;
    }

    public String Ac6() {
        return this.A0A;
    }

    public String Ac7() {
        return this.A0B;
    }

    public int Ac9() {
        return this.A01;
    }

    public String AcF() {
        return this.A0D;
    }

    public long AcG() {
        return this.A03;
    }

    public ImmutableList AvU() {
        if (this.A0J.contains("nestedAdItems")) {
            return this.A09;
        }
        if (A0N == null) {
            synchronized (this) {
                if (A0N == null) {
                    A0N = RegularImmutableList.A02;
                }
            }
        }
        return A0N;
    }

    public int B7T() {
        return this.A02;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A07 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A07.size());
            C24971Xv it = this.A07.iterator();
            while (it.hasNext()) {
                parcel.writeInt(((C35861rw) it.next()).ordinal());
            }
        }
        parcel.writeString(this.A0A);
        parcel.writeString(this.A0B);
        parcel.writeInt(this.A01);
        parcel.writeString(this.A0C);
        parcel.writeString(this.A0D);
        parcel.writeLong(this.A03);
        if (this.A08 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A08.size());
            C24971Xv it2 = this.A08.iterator();
            while (it2.hasNext()) {
                parcel.writeInt(((C35951s5) it2.next()).ordinal());
            }
        }
        parcel.writeDouble(this.A00);
        if (this.A04 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A04, i);
        }
        parcel.writeString(this.A0E);
        parcel.writeString(this.A0F);
        if (this.A05 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A05, i);
        }
        parcel.writeInt(this.A0K ? 1 : 0);
        if (this.A09 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A09.size());
            C24971Xv it3 = this.A09.iterator();
            while (it3.hasNext()) {
                parcel.writeParcelable((InboxAdsMediaInfo) it3.next(), i);
            }
        }
        parcel.writeString(this.A0G);
        parcel.writeString(this.A0H);
        parcel.writeString(this.A0I);
        parcel.writeInt(this.A02);
        if (this.A06 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A06, i);
        }
        parcel.writeInt(this.A0J.size());
        for (String writeString : this.A0J) {
            parcel.writeString(writeString);
        }
    }

    public int hashCode() {
        return C28931fb.A03((C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A04(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A00(C28931fb.A03(C28931fb.A02(C28931fb.A03(C28931fb.A03((C28931fb.A03(C28931fb.A03(C28931fb.A03(1, A00()), this.A0A), this.A0B) * 31) + this.A01, this.A0C), this.A0D), this.A03), A01()), this.A00), this.A04), this.A0E), this.A0F), this.A05), this.A0K), AvU()), this.A0G), this.A0H), this.A0I) * 31) + this.A02, this.A06);
    }

    public InboxAdsMediaInfo(C36021sC r3) {
        this.A07 = r3.A07;
        String str = r3.A0A;
        C28931fb.A06(str, "adId");
        this.A0A = str;
        String str2 = r3.A0B;
        C28931fb.A06(str2, "adItemId");
        this.A0B = str2;
        this.A01 = r3.A01;
        String str3 = r3.A0C;
        C28931fb.A06(str3, "adTitle");
        this.A0C = str3;
        String str4 = r3.A0D;
        C28931fb.A06(str4, "adToken");
        this.A0D = str4;
        this.A03 = r3.A03;
        this.A08 = r3.A08;
        this.A00 = r3.A00;
        this.A04 = r3.A04;
        String str5 = r3.A0E;
        C28931fb.A06(str5, "cardDescription");
        this.A0E = str5;
        String str6 = r3.A0F;
        C28931fb.A06(str6, "description");
        this.A0F = str6;
        this.A05 = r3.A05;
        this.A0K = r3.A0K;
        this.A09 = r3.A09;
        String str7 = r3.A0G;
        C28931fb.A06(str7, "pageId");
        this.A0G = str7;
        String str8 = r3.A0H;
        C28931fb.A06(str8, "photoDescription");
        this.A0H = str8;
        String str9 = r3.A0I;
        C28931fb.A06(str9, "title");
        this.A0I = str9;
        this.A02 = r3.A02;
        this.A06 = r3.A06;
        this.A0J = Collections.unmodifiableSet(r3.A0J);
    }

    public InboxAdsMediaInfo(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A07 = null;
        } else {
            int readInt = parcel.readInt();
            C35861rw[] r3 = new C35861rw[readInt];
            for (int i = 0; i < readInt; i++) {
                r3[i] = C35861rw.values()[parcel.readInt()];
            }
            this.A07 = ImmutableList.copyOf(r3);
        }
        this.A0A = parcel.readString();
        this.A0B = parcel.readString();
        this.A01 = parcel.readInt();
        this.A0C = parcel.readString();
        this.A0D = parcel.readString();
        this.A03 = parcel.readLong();
        if (parcel.readInt() == 0) {
            this.A08 = null;
        } else {
            int readInt2 = parcel.readInt();
            C35951s5[] r32 = new C35951s5[readInt2];
            for (int i2 = 0; i2 < readInt2; i2++) {
                r32[i2] = C35951s5.values()[parcel.readInt()];
            }
            this.A08 = ImmutableList.copyOf(r32);
        }
        this.A00 = parcel.readDouble();
        if (parcel.readInt() == 0) {
            this.A04 = null;
        } else {
            this.A04 = (AdCallToAction) parcel.readParcelable(AdCallToAction.class.getClassLoader());
        }
        this.A0E = parcel.readString();
        this.A0F = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A05 = null;
        } else {
            this.A05 = (InboxAdsImage) parcel.readParcelable(InboxAdsImage.class.getClassLoader());
        }
        this.A0K = parcel.readInt() != 1 ? false : true;
        if (parcel.readInt() == 0) {
            this.A09 = null;
        } else {
            int readInt3 = parcel.readInt();
            InboxAdsMediaInfo[] inboxAdsMediaInfoArr = new InboxAdsMediaInfo[readInt3];
            for (int i3 = 0; i3 < readInt3; i3++) {
                inboxAdsMediaInfoArr[i3] = (InboxAdsMediaInfo) parcel.readParcelable(InboxAdsMediaInfo.class.getClassLoader());
            }
            this.A09 = ImmutableList.copyOf(inboxAdsMediaInfoArr);
        }
        this.A0G = parcel.readString();
        this.A0H = parcel.readString();
        this.A0I = parcel.readString();
        this.A02 = parcel.readInt();
        if (parcel.readInt() == 0) {
            this.A06 = null;
        } else {
            this.A06 = (InboxAdsVideo) parcel.readParcelable(InboxAdsVideo.class.getClassLoader());
        }
        HashSet hashSet = new HashSet();
        int readInt4 = parcel.readInt();
        for (int i4 = 0; i4 < readInt4; i4++) {
            hashSet.add(parcel.readString());
        }
        this.A0J = Collections.unmodifiableSet(hashSet);
    }
}
