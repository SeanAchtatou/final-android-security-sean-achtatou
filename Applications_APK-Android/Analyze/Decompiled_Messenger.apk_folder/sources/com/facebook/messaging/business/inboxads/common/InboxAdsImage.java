package com.facebook.messaging.business.inboxads.common;

import X.C28931fb;
import X.C36001sA;
import X.C36011sB;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.proxygen.TraceFieldType;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class InboxAdsImage implements Parcelable {
    private static volatile Uri A04;
    public static final Parcelable.Creator CREATOR = new C36011sB();
    private final int A00;
    private final int A01;
    private final Uri A02;
    private final Set A03;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof InboxAdsImage) {
                InboxAdsImage inboxAdsImage = (InboxAdsImage) obj;
                if (!(this.A00 == inboxAdsImage.A00 && C28931fb.A07(A00(), inboxAdsImage.A00()) && this.A01 == inboxAdsImage.A01)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public Uri A00() {
        if (this.A03.contains(TraceFieldType.Uri)) {
            return this.A02;
        }
        if (A04 == null) {
            synchronized (this) {
                if (A04 == null) {
                    A04 = Uri.EMPTY;
                }
            }
        }
        return A04;
    }

    public int hashCode() {
        return (C28931fb.A03(31 + this.A00, A00()) * 31) + this.A01;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        if (this.A02 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A02, i);
        }
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A03.size());
        for (String writeString : this.A03) {
            parcel.writeString(writeString);
        }
    }

    public InboxAdsImage(C36001sA r2) {
        this.A00 = r2.A00;
        this.A02 = r2.A02;
        this.A01 = r2.A01;
        this.A03 = Collections.unmodifiableSet(r2.A03);
    }

    public InboxAdsImage(Parcel parcel) {
        this.A00 = parcel.readInt();
        if (parcel.readInt() == 0) {
            this.A02 = null;
        } else {
            this.A02 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        this.A01 = parcel.readInt();
        HashSet hashSet = new HashSet();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            hashSet.add(parcel.readString());
        }
        this.A03 = Collections.unmodifiableSet(hashSet);
    }
}
