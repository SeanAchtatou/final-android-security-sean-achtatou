package com.facebook.messaging.business.inboxads.common;

import X.C07220cv;
import X.C24971Xv;
import X.C25651aB;
import X.C28931fb;
import X.C35951s5;
import X.C36001sA;
import X.C36051sF;
import X.C36061sG;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class InboxAdsData implements Parcelable {
    private static volatile Uri A0K;
    private static volatile Uri A0L;
    private static volatile Uri A0M;
    private static volatile Uri A0N;
    private static volatile Uri A0O;
    private static volatile InboxAdsImage A0P;
    private static volatile User A0Q;
    private static volatile ImmutableList A0R;
    private static volatile ImmutableList A0S;
    private static volatile ImmutableList A0T;
    public static final Parcelable.Creator CREATOR = new C36061sG();
    public final Uri A00;
    public final Uri A01;
    public final Uri A02;
    public final Uri A03;
    public final Uri A04;
    public final InboxAdsImage A05;
    public final User A06;
    public final ImmutableList A07;
    public final ImmutableList A08;
    public final ImmutableList A09;
    public final Boolean A0A;
    public final Boolean A0B;
    public final Boolean A0C;
    public final Integer A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;
    public final Set A0J;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof InboxAdsData) {
                InboxAdsData inboxAdsData = (InboxAdsData) obj;
                if (!C28931fb.A07(A00(), inboxAdsData.A00()) || !C28931fb.A07(A01(), inboxAdsData.A01()) || !C28931fb.A07(A02(), inboxAdsData.A02()) || !C28931fb.A07(this.A0E, inboxAdsData.A0E) || !C28931fb.A07(A07(), inboxAdsData.A07()) || !C28931fb.A07(A03(), inboxAdsData.A03()) || !C28931fb.A07(A04(), inboxAdsData.A04()) || !C28931fb.A07(A08(), inboxAdsData.A08()) || !C28931fb.A07(this.A0F, inboxAdsData.A0F) || !C28931fb.A07(this.A0G, inboxAdsData.A0G) || !C28931fb.A07(this.A0A, inboxAdsData.A0A) || !C28931fb.A07(this.A0B, inboxAdsData.A0B) || !C28931fb.A07(A06(), inboxAdsData.A06()) || !C28931fb.A07(this.A0H, inboxAdsData.A0H) || !C28931fb.A07(A09(), inboxAdsData.A09()) || !C28931fb.A07(this.A0C, inboxAdsData.A0C) || !C28931fb.A07(A05(), inboxAdsData.A05()) || !C28931fb.A07(this.A0I, inboxAdsData.A0I) || !C28931fb.A07(this.A0D, inboxAdsData.A0D)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public Uri A00() {
        if (this.A0J.contains("adHelpUri")) {
            return this.A00;
        }
        if (A0K == null) {
            synchronized (this) {
                if (A0K == null) {
                    A0K = Uri.EMPTY;
                }
            }
        }
        return A0K;
    }

    public Uri A01() {
        if (this.A0J.contains("adHideUri")) {
            return this.A01;
        }
        if (A0L == null) {
            synchronized (this) {
                if (A0L == null) {
                    A0L = Uri.EMPTY;
                }
            }
        }
        return A0L;
    }

    public Uri A02() {
        if (this.A0J.contains("adIconUri")) {
            return this.A02;
        }
        if (A0M == null) {
            synchronized (this) {
                if (A0M == null) {
                    A0M = Uri.EMPTY;
                }
            }
        }
        return A0M;
    }

    public Uri A03() {
        if (this.A0J.contains("adPreferenceUri")) {
            return this.A03;
        }
        if (A0N == null) {
            synchronized (this) {
                if (A0N == null) {
                    A0N = Uri.EMPTY;
                }
            }
        }
        return A0N;
    }

    public Uri A04() {
        if (this.A0J.contains("adReportUri")) {
            return this.A04;
        }
        if (A0O == null) {
            synchronized (this) {
                if (A0O == null) {
                    A0O = Uri.EMPTY;
                }
            }
        }
        return A0O;
    }

    public InboxAdsImage A05() {
        if (this.A0J.contains("thumbnailImage")) {
            return this.A05;
        }
        if (A0P == null) {
            synchronized (this) {
                if (A0P == null) {
                    A0P = new InboxAdsImage(new C36001sA());
                }
            }
        }
        return A0P;
    }

    public User A06() {
        if (this.A0J.contains("page")) {
            return this.A06;
        }
        if (A0Q == null) {
            synchronized (this) {
                if (A0Q == null) {
                    C07220cv r2 = new C07220cv();
                    r2.A03(C25651aB.A03, ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL);
                    A0Q = r2.A02();
                }
            }
        }
        return A0Q;
    }

    public ImmutableList A07() {
        if (this.A0J.contains("adMediaInfos")) {
            return this.A07;
        }
        if (A0R == null) {
            synchronized (this) {
                if (A0R == null) {
                    A0R = RegularImmutableList.A02;
                }
            }
        }
        return A0R;
    }

    public ImmutableList A08() {
        if (this.A0J.contains("adTypes")) {
            return this.A08;
        }
        if (A0S == null) {
            synchronized (this) {
                if (A0S == null) {
                    A0S = RegularImmutableList.A02;
                }
            }
        }
        return A0S;
    }

    public ImmutableList A09() {
        if (this.A0J.contains("quickReplies")) {
            return this.A09;
        }
        if (A0T == null) {
            synchronized (this) {
                if (A0T == null) {
                    A0T = RegularImmutableList.A02;
                }
            }
        }
        return A0T;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A00 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A00, i);
        }
        if (this.A01 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A01, i);
        }
        if (this.A02 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A02, i);
        }
        parcel.writeString(this.A0E);
        if (this.A07 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A07.size());
            C24971Xv it = this.A07.iterator();
            while (it.hasNext()) {
                parcel.writeParcelable((InboxAdsMediaInfo) it.next(), i);
            }
        }
        if (this.A03 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A03, i);
        }
        if (this.A04 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A04, i);
        }
        if (this.A08 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A08.size());
            C24971Xv it2 = this.A08.iterator();
            while (it2.hasNext()) {
                parcel.writeInt(((C35951s5) it2.next()).ordinal());
            }
        }
        parcel.writeString(this.A0F);
        parcel.writeString(this.A0G);
        parcel.writeInt(this.A0A.booleanValue() ? 1 : 0);
        parcel.writeInt(this.A0B.booleanValue() ? 1 : 0);
        if (this.A06 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A06, i);
        }
        parcel.writeString(this.A0H);
        if (this.A09 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A09.size());
            C24971Xv it3 = this.A09.iterator();
            while (it3.hasNext()) {
                parcel.writeParcelable((InboxAdsQuickReply) it3.next(), i);
            }
        }
        parcel.writeInt(this.A0C.booleanValue() ? 1 : 0);
        if (this.A05 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A05, i);
        }
        parcel.writeString(this.A0I);
        parcel.writeInt(this.A0D.intValue());
        parcel.writeInt(this.A0J.size());
        for (String writeString : this.A0J) {
            parcel.writeString(writeString);
        }
    }

    public int hashCode() {
        return C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(1, A00()), A01()), A02()), this.A0E), A07()), A03()), A04()), A08()), this.A0F), this.A0G), this.A0A), this.A0B), A06()), this.A0H), A09()), this.A0C), A05()), this.A0I), this.A0D);
    }

    public InboxAdsData(C36051sF r3) {
        this.A00 = r3.A00;
        this.A01 = r3.A01;
        this.A02 = r3.A02;
        String str = r3.A0E;
        C28931fb.A06(str, "adId");
        this.A0E = str;
        this.A07 = r3.A07;
        this.A03 = r3.A03;
        this.A04 = r3.A04;
        this.A08 = r3.A08;
        String str2 = r3.A0F;
        C28931fb.A06(str2, "clientToken");
        this.A0F = str2;
        String str3 = r3.A0G;
        C28931fb.A06(str3, "description");
        this.A0G = str3;
        Boolean bool = r3.A0A;
        C28931fb.A06(bool, "isBusinessActive");
        this.A0A = bool;
        Boolean bool2 = r3.A0B;
        C28931fb.A06(bool2, "isDefaultWelcomeMessage");
        this.A0B = bool2;
        this.A06 = r3.A06;
        String str4 = r3.A0H;
        C28931fb.A06(str4, "postclickSocialContext");
        this.A0H = str4;
        this.A09 = r3.A09;
        Boolean bool3 = r3.A0C;
        C28931fb.A06(bool3, "shouldShowGetStarted");
        this.A0C = bool3;
        this.A05 = r3.A05;
        String str5 = r3.A0I;
        C28931fb.A06(str5, "title");
        this.A0I = str5;
        Integer num = r3.A0D;
        C28931fb.A06(num, "uiFormat");
        this.A0D = num;
        this.A0J = Collections.unmodifiableSet(r3.A0J);
    }

    public InboxAdsData(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A00 = null;
        } else {
            this.A00 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A02 = null;
        } else {
            this.A02 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        this.A0E = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A07 = null;
        } else {
            int readInt = parcel.readInt();
            InboxAdsMediaInfo[] inboxAdsMediaInfoArr = new InboxAdsMediaInfo[readInt];
            for (int i = 0; i < readInt; i++) {
                inboxAdsMediaInfoArr[i] = (InboxAdsMediaInfo) parcel.readParcelable(InboxAdsMediaInfo.class.getClassLoader());
            }
            this.A07 = ImmutableList.copyOf(inboxAdsMediaInfoArr);
        }
        if (parcel.readInt() == 0) {
            this.A03 = null;
        } else {
            this.A03 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A04 = null;
        } else {
            this.A04 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A08 = null;
        } else {
            int readInt2 = parcel.readInt();
            C35951s5[] r4 = new C35951s5[readInt2];
            for (int i2 = 0; i2 < readInt2; i2++) {
                r4[i2] = C35951s5.values()[parcel.readInt()];
            }
            this.A08 = ImmutableList.copyOf(r4);
        }
        this.A0F = parcel.readString();
        this.A0G = parcel.readString();
        boolean z = true;
        this.A0A = Boolean.valueOf(parcel.readInt() == 1);
        this.A0B = Boolean.valueOf(parcel.readInt() == 1);
        if (parcel.readInt() == 0) {
            this.A06 = null;
        } else {
            this.A06 = (User) parcel.readParcelable(User.class.getClassLoader());
        }
        this.A0H = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A09 = null;
        } else {
            int readInt3 = parcel.readInt();
            InboxAdsQuickReply[] inboxAdsQuickReplyArr = new InboxAdsQuickReply[readInt3];
            for (int i3 = 0; i3 < readInt3; i3++) {
                inboxAdsQuickReplyArr[i3] = (InboxAdsQuickReply) parcel.readParcelable(InboxAdsQuickReply.class.getClassLoader());
            }
            this.A09 = ImmutableList.copyOf(inboxAdsQuickReplyArr);
        }
        this.A0C = Boolean.valueOf(parcel.readInt() != 1 ? false : z);
        if (parcel.readInt() == 0) {
            this.A05 = null;
        } else {
            this.A05 = (InboxAdsImage) parcel.readParcelable(InboxAdsImage.class.getClassLoader());
        }
        this.A0I = parcel.readString();
        this.A0D = Integer.valueOf(parcel.readInt());
        HashSet hashSet = new HashSet();
        int readInt4 = parcel.readInt();
        for (int i4 = 0; i4 < readInt4; i4++) {
            hashSet.add(parcel.readString());
        }
        this.A0J = Collections.unmodifiableSet(hashSet);
    }
}
