package com.facebook.messaging.business.common.calltoaction.model;

import X.C16550xJ;
import X.C188515m;
import X.C30101hU;
import X.C30121hW;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.util.StringLocaleUtil;
import com.facebook.messaging.browser.model.MessengerWebViewParams;
import com.facebook.messaging.business.mdotme.model.PlatformRefParams;
import com.google.common.base.Objects;
import com.google.common.base.Platform;
import io.card.payment.BuildConfig;
import java.util.Arrays;

public class CallToAction implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C30121hW();
    public final Uri A00;
    public final Uri A01;
    public final MessengerWebViewParams A02;
    public final CTABrandedCameraParams A03;
    public final CTAInformationIdentify A04;
    public final CTAPaymentInfo A05;
    public final CTAUserConfirmation A06;
    public final C30101hU A07;
    public final C16550xJ A08;
    public final CallToActionTarget A09;
    public final PlatformRefParams A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            CallToAction callToAction = (CallToAction) obj;
            if (!Objects.equal(Boolean.valueOf(this.A0G), Boolean.valueOf(callToAction.A0G)) || !Objects.equal(Boolean.valueOf(this.A0F), Boolean.valueOf(callToAction.A0F)) || !Objects.equal(Boolean.valueOf(this.A0H), Boolean.valueOf(callToAction.A0H)) || !Objects.equal(this.A0B, callToAction.A0B) || !Objects.equal(this.A0E, callToAction.A0E) || !Objects.equal(this.A00, callToAction.A00) || !Objects.equal(this.A01, callToAction.A01) || !Objects.equal(this.A08, callToAction.A08) || !Objects.equal(this.A09, callToAction.A09) || !Objects.equal(this.A06, callToAction.A06) || !Objects.equal(this.A05, callToAction.A05) || !Objects.equal(this.A04, callToAction.A04) || !Objects.equal(this.A0C, callToAction.A0C) || !Objects.equal(this.A02, callToAction.A02) || !Objects.equal(this.A03, callToAction.A03) || !Objects.equal(this.A0D, callToAction.A0D) || !Objects.equal(this.A07, callToAction.A07)) {
                return false;
            }
        }
        return true;
    }

    public String A00() {
        String str = this.A0E;
        if (str == null) {
            return BuildConfig.FLAVOR;
        }
        return StringLocaleUtil.toUpperCaseLocaleSafe(str);
    }

    public int hashCode() {
        String str = this.A0B;
        String str2 = this.A0E;
        Uri uri = this.A00;
        Uri uri2 = this.A01;
        C16550xJ r12 = this.A08;
        CallToActionTarget callToActionTarget = this.A09;
        Boolean valueOf = Boolean.valueOf(this.A0G);
        Boolean valueOf2 = Boolean.valueOf(this.A0F);
        Boolean valueOf3 = Boolean.valueOf(this.A0H);
        CTAUserConfirmation cTAUserConfirmation = this.A06;
        CTAPaymentInfo cTAPaymentInfo = this.A05;
        CTAInformationIdentify cTAInformationIdentify = this.A04;
        String str3 = this.A0C;
        MessengerWebViewParams messengerWebViewParams = this.A02;
        CTABrandedCameraParams cTABrandedCameraParams = this.A03;
        return Arrays.deepHashCode(new Object[]{str, str2, uri, uri2, r12, callToActionTarget, valueOf, valueOf2, valueOf3, cTAUserConfirmation, cTAPaymentInfo, cTAInformationIdentify, str3, messengerWebViewParams, cTABrandedCameraParams, this.A0D, this.A07});
    }

    public void writeToParcel(Parcel parcel, int i) {
        String str;
        parcel.writeString(this.A0B);
        parcel.writeString(this.A0E);
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
        C16550xJ r0 = this.A08;
        if (r0 != null) {
            str = r0.name();
        } else {
            str = null;
        }
        parcel.writeString(str);
        parcel.writeParcelable(this.A09, i);
        parcel.writeInt(this.A0G ? 1 : 0);
        parcel.writeInt(this.A0F ? 1 : 0);
        parcel.writeInt(this.A0H ? 1 : 0);
        parcel.writeParcelable(this.A06, i);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A04, i);
        parcel.writeParcelable(this.A0A, i);
        parcel.writeString(this.A0C);
        parcel.writeParcelable(this.A02, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeString(this.A0D);
        parcel.writeSerializable(this.A07);
    }

    public CallToAction(C188515m r2) {
        this.A0B = r2.A0B;
        this.A0E = r2.A0E;
        this.A00 = r2.A00;
        this.A01 = r2.A01;
        this.A08 = r2.A08;
        this.A09 = r2.A09;
        this.A0G = r2.A0G;
        this.A0F = r2.A0F;
        this.A0H = r2.A0H;
        this.A06 = r2.A06;
        this.A05 = r2.A05;
        this.A04 = r2.A04;
        this.A0A = r2.A0A;
        this.A0C = r2.A0C;
        this.A02 = r2.A02;
        this.A03 = r2.A03;
        this.A0D = r2.A0D;
        this.A07 = r2.A07;
    }

    public CallToAction(Parcel parcel) {
        C16550xJ r0;
        this.A0B = parcel.readString();
        this.A0E = parcel.readString();
        Class<Uri> cls = Uri.class;
        this.A00 = (Uri) parcel.readParcelable(cls.getClassLoader());
        this.A01 = (Uri) parcel.readParcelable(cls.getClassLoader());
        String readString = parcel.readString();
        if (!Platform.stringIsNullOrEmpty(readString)) {
            r0 = null;
            if (readString != null) {
                try {
                    r0 = C16550xJ.valueOf(readString);
                } catch (IllegalArgumentException unused) {
                }
            }
        } else {
            r0 = null;
        }
        this.A08 = r0;
        this.A09 = (CallToActionTarget) parcel.readParcelable(CallToActionTarget.class.getClassLoader());
        boolean z = false;
        this.A0G = parcel.readInt() == 1;
        this.A0F = parcel.readInt() == 1;
        this.A0H = parcel.readInt() == 1 ? true : z;
        this.A06 = (CTAUserConfirmation) parcel.readParcelable(CTAUserConfirmation.class.getClassLoader());
        this.A05 = (CTAPaymentInfo) parcel.readParcelable(CTAPaymentInfo.class.getClassLoader());
        this.A04 = (CTAInformationIdentify) parcel.readParcelable(CTAInformationIdentify.class.getClassLoader());
        this.A0A = (PlatformRefParams) parcel.readParcelable(PlatformRefParams.class.getClassLoader());
        this.A0C = parcel.readString();
        this.A02 = (MessengerWebViewParams) parcel.readParcelable(MessengerWebViewParams.class.getClassLoader());
        this.A03 = (CTABrandedCameraParams) parcel.readParcelable(CTABrandedCameraParams.class.getClassLoader());
        this.A0D = parcel.readString();
        this.A07 = (C30101hU) parcel.readSerializable();
    }
}
