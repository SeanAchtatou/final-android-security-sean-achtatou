package com.facebook.messaging.business.common.calltoaction.model;

import X.AnonymousClass285;
import X.C30131hX;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public final class NestedCallToAction implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass285();
    public final CallToAction A00;
    public final ImmutableList A01;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
        C417826y.A0I(parcel, this.A01);
    }

    public NestedCallToAction(C30131hX r2) {
        Preconditions.checkNotNull(r2.A00);
        this.A00 = r2.A00;
        this.A01 = r2.A01;
    }

    public NestedCallToAction(Parcel parcel) {
        this.A00 = (CallToAction) parcel.readParcelable(CallToAction.class.getClassLoader());
        this.A01 = C417826y.A06(parcel, CREATOR);
    }
}
