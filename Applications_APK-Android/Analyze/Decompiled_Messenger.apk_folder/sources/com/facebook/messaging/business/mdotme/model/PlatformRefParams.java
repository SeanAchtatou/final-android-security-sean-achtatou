package com.facebook.messaging.business.mdotme.model;

import X.C188415k;
import X.C30091hT;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import com.google.common.base.Platform;
import java.util.Arrays;

public final class PlatformRefParams implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C188415k();
    public String A00;
    public String A01;
    public String A02;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            PlatformRefParams platformRefParams = (PlatformRefParams) obj;
            if (!Objects.equal(this.A00, platformRefParams.A00) || !Objects.equal(this.A01, platformRefParams.A01) || !Objects.equal(this.A02, platformRefParams.A02)) {
                return false;
            }
        }
        return true;
    }

    public boolean A00() {
        if (!Platform.stringIsNullOrEmpty(this.A00) || "branded_camera".equals(this.A01)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01, this.A02});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
    }

    private PlatformRefParams() {
    }

    public PlatformRefParams(C30091hT r2) {
        this.A00 = r2.A00;
        this.A01 = r2.A01;
        this.A02 = r2.A02;
    }

    public PlatformRefParams(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
        this.A02 = parcel.readString();
    }
}
