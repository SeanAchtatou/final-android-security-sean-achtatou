package com.facebook.messaging.business.common.calltoaction.model;

import X.C35981s8;
import X.C35991s9;
import android.os.Parcel;
import android.os.Parcelable;

public final class AdCallToAction extends CallToAction {
    public static final Parcelable.Creator CREATOR = new C35991s9();
    public final String A00;
    public final String A01;

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }

    public AdCallToAction(C35981s8 r2) {
        super(r2);
        this.A00 = r2.A00;
        this.A01 = r2.A01;
    }

    public AdCallToAction(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
    }
}
