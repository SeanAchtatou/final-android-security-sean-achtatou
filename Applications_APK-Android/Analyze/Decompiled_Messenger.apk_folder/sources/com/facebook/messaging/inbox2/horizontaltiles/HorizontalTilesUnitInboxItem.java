package com.facebook.messaging.inbox2.horizontaltiles;

import X.AnonymousClass44E;
import X.C24971Xv;
import X.C27161ck;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.collect.ImmutableList;

public final class HorizontalTilesUnitInboxItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44E();
    public final ImmutableList A00;

    public void A0B(int i) {
        super.A0B(i);
        C24971Xv it = this.A00.iterator();
        while (it.hasNext()) {
            ((HorizontalTileInboxItem) it.next()).A0B(i);
        }
    }

    public void A0D(int i) {
        super.A0D(i);
        C24971Xv it = this.A00.iterator();
        while (it.hasNext()) {
            ((HorizontalTileInboxItem) it.next()).A0D(i);
        }
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeList(this.A00);
    }

    public HorizontalTilesUnitInboxItem(C27161ck r1, ImmutableList immutableList) {
        super(r1);
        this.A00 = immutableList;
    }

    public HorizontalTilesUnitInboxItem(Parcel parcel) {
        super(parcel);
        this.A00 = C417826y.A05(parcel, HorizontalTileInboxItem.CREATOR);
    }
}
