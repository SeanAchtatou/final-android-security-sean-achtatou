package com.facebook.messaging.inbox2.items;

import X.C33901oK;
import X.C33941oO;
import X.C33951oP;
import X.C417826y;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.collect.ImmutableMap;

public final class InboxTrackableItem implements Parcelable, C33941oO {
    public static final Parcelable.Creator CREATOR = new C33951oP();
    public int A00 = -1;
    public int A01 = -1;
    public int A02 = -1;
    public final Bundle A03;
    public final C33901oK A04;
    public final ImmutableMap A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final boolean A0B;
    private final long A0C;

    public int describeContents() {
        return 0;
    }

    public long ArZ() {
        return this.A0C;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A0C);
        parcel.writeString(this.A08);
        parcel.writeString(this.A07);
        parcel.writeString(this.A0A);
        parcel.writeSerializable(this.A04);
        parcel.writeString(this.A09);
        parcel.writeString(this.A06);
        parcel.writeMap(this.A05);
        parcel.writeBundle(this.A03);
        C417826y.A0V(parcel, this.A0B);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A00);
    }

    public InboxTrackableItem(long j, String str, String str2, String str3, C33901oK r7, String str4, String str5, ImmutableMap immutableMap, Bundle bundle, boolean z) {
        this.A0C = j;
        this.A08 = str;
        this.A07 = str2;
        this.A0A = str3;
        this.A04 = r7;
        this.A09 = str4;
        this.A06 = str5;
        this.A05 = immutableMap;
        this.A03 = bundle;
        this.A0B = z;
    }

    public InboxTrackableItem(Parcel parcel) {
        this.A0C = parcel.readLong();
        this.A08 = parcel.readString();
        this.A07 = parcel.readString();
        this.A0A = parcel.readString();
        this.A04 = (C33901oK) parcel.readSerializable();
        this.A09 = parcel.readString();
        this.A06 = parcel.readString();
        this.A05 = C417826y.A09(parcel);
        this.A03 = parcel.readBundle();
        this.A0B = C417826y.A0W(parcel);
        this.A01 = parcel.readInt();
        this.A02 = parcel.readInt();
        this.A00 = parcel.readInt();
    }
}
