package com.facebook.messaging.inbox2.morefooter;

import X.AnonymousClass07B;
import X.AnonymousClass1GI;
import X.C192517k;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class InboxMoreThreadsItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1GI();
    public final C192517k A00;
    public final String A01;

    public Integer A0I() {
        switch (this.A00.ordinal()) {
            case 0:
                return AnonymousClass07B.A01;
            case 1:
            default:
                return AnonymousClass07B.A00;
            case 2:
                return AnonymousClass07B.A0C;
        }
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeSerializable(this.A00);
        parcel.writeString(this.A01);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public InboxMoreThreadsItem(X.C27161ck r3, X.C192517k r4, java.lang.String r5) {
        /*
            r2 = this;
            X.1GH r1 = X.AnonymousClass1GH.A01
            r0 = 0
            r2.<init>(r3, r0, r1)
            com.google.common.base.Preconditions.checkNotNull(r1)
            r2.A00 = r4
            r2.A01 = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem.<init>(X.1ck, X.17k, java.lang.String):void");
    }

    public InboxMoreThreadsItem(Parcel parcel) {
        super(parcel);
        this.A00 = (C192517k) parcel.readSerializable();
        this.A01 = parcel.readString();
    }
}
