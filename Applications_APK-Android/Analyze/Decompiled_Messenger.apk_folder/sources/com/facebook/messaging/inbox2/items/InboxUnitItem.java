package com.facebook.messaging.inbox2.items;

import X.AnonymousClass07K;
import X.AnonymousClass08S;
import X.AnonymousClass1GH;
import X.AnonymousClass1HT;
import X.AnonymousClass1IY;
import X.AnonymousClass324;
import X.C07160cj;
import X.C07180co;
import X.C07190cq;
import X.C27161ck;
import X.C33901oK;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.business.inboxads.common.InboxAdsItem;
import com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem;
import com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit;
import com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem;
import com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem;
import com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem;
import com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem;
import com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem;
import com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem;
import com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem;
import com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem;
import com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem;
import com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem;
import com.facebook.messaging.inboxfolder.InboxUnitFolderItem;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem;
import com.facebook.workchat.inbox.invites.bridge.WorkChatInviteCoworkersUnitInboxItem;
import com.google.common.base.Charsets;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public abstract class InboxUnitItem implements Parcelable {
    public static int A04 = 1;
    public static int A05 = 2;
    public static final C07160cj A06 = new AnonymousClass1HT(InboxUnitItem.class.hashCode());
    private InboxTrackableItem A00;
    public final GSTModelShape1S0000000 A01;
    public final C27161ck A02;
    private final AnonymousClass1GH A03;

    public int A02() {
        int i = 0;
        GSTModelShape1S0000000 gSTModelShape1S0000000 = this.A01;
        if (gSTModelShape1S0000000 != null && gSTModelShape1S0000000.getIntValue(-1374423487) > 0) {
            i = 0 | A04;
        }
        if (this.A02.getIntValue(-23571790) > 0) {
            return i | A05;
        }
        return i;
    }

    public Bundle A04() {
        if (!(this instanceof InboxUnitThreadItem)) {
            return null;
        }
        InboxUnitThreadItem inboxUnitThreadItem = (InboxUnitThreadItem) this;
        if (Platform.stringIsNullOrEmpty(inboxUnitThreadItem.A01.A0s)) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putLong("thread_summary", inboxUnitThreadItem.A01.A0S.A0G());
        bundle.putString("message_id", inboxUnitThreadItem.A01.A0s);
        bundle.putLong("timestamp", inboxUnitThreadItem.A01.A0B);
        return bundle;
    }

    public C33901oK A05() {
        return !(this instanceof WorkChatInviteCoworkersUnitInboxItem) ? !(this instanceof InboxLoadMorePlaceholderItem) ? !(this instanceof InboxUnitMontageAndActiveNowItem) ? !(this instanceof InboxUnitMontageActiveNowItem) ? !(this instanceof InboxMontageItem) ? !(this instanceof InboxUnitFolderItem) ? ((this instanceof NonInboxServiceSectionHeaderItem) || (this instanceof InboxUnitSectionHeaderItem)) ? C33901oK.A0Z : !(this instanceof PlatformExtensionsVerticalInboxItem) ? !(this instanceof InboxUnitSeeAllItem) ? !(this instanceof InboxMoreThreadsItem) ? !(this instanceof MessageRequestsHeaderInboxItem) ? !(this instanceof MessageRequestsBannerInboxItem) ? !(this instanceof InboxUnitThreadItem) ? !(this instanceof HorizontalTilesUnitInboxItem) ? !(this instanceof MessengerDiscoveryCategoryInboxItem) ? !(this instanceof DiscoverTabGameNuxFooterItem) ? !(this instanceof DiscoverTabAttachmentUnit) ? !(this instanceof DiscoverTabAttachmentItem) ? !(this instanceof DiscoverLocationUpsellItem) ? !(this instanceof InboxUnitConversationStarterItem) ? C33901oK.A0M : C33901oK.A05 : C33901oK.A0O : ((DiscoverTabAttachmentItem) this).A06.itemType : ((DiscoverTabAttachmentUnit) this).A00.unitType : C33901oK.A08 : C33901oK.A0N : C33901oK.A0F : C33901oK.A0b : C33901oK.A0L : C33901oK.A0K : C33901oK.A0S : C33901oK.A0a : C33901oK.A0P : ((InboxUnitFolderItem) this).A00.itemType : C33901oK.A0R : C33901oK.A0G : C33901oK.A0Q : C33901oK.A0I : C33901oK.A0H;
    }

    public AnonymousClass1IY A06() {
        return !(this instanceof WorkChatInviteCoworkersUnitInboxItem) ? !(this instanceof InboxLoadMorePlaceholderItem) ? !(this instanceof InboxUnitMontageAndActiveNowItem) ? !(this instanceof InboxUnitMontageActiveNowItem) ? !(this instanceof InboxMontageItem) ? !(this instanceof InboxUnitFolderItem) ? !(this instanceof NonInboxServiceSectionHeaderItem) ? !(this instanceof InboxUnitSectionHeaderItem) ? !(this instanceof PlatformExtensionsVerticalInboxItem) ? !(this instanceof InboxUnitSeeAllItem) ? !(this instanceof InboxMoreThreadsItem) ? !(this instanceof MessageRequestsHeaderInboxItem) ? !(this instanceof MessageRequestsBannerInboxItem) ? !(this instanceof InboxUnitThreadItem) ? !(this instanceof HorizontalTilesUnitInboxItem) ? !(this instanceof MessengerDiscoveryCategoryInboxItem) ? !(this instanceof DiscoverTabGameNuxFooterItem) ? !(this instanceof DiscoverTabAttachmentUnit) ? !(this instanceof DiscoverTabAttachmentItem) ? !(this instanceof DiscoverLocationUpsellItem) ? !(this instanceof InboxUnitConversationStarterItem) ? AnonymousClass1IY.A0O : AnonymousClass1IY.A06 : AnonymousClass1IY.A0R : ((DiscoverTabAttachmentItem) this).A06.viewType : ((DiscoverTabAttachmentUnit) this).A00.viewType : AnonymousClass1IY.A09 : AnonymousClass1IY.A0Q : AnonymousClass1IY.A0H : AnonymousClass1IY.A0b : AnonymousClass1IY.A0N : AnonymousClass1IY.A0M : AnonymousClass1IY.A0W : AnonymousClass1IY.A0a : AnonymousClass1IY.A0S : AnonymousClass1IY.A0Z : AnonymousClass1IY.A0X : ((InboxUnitFolderItem) this).A00.inboxItemViewType : AnonymousClass1IY.A0V : AnonymousClass1IY.A0I : AnonymousClass1IY.A0T : AnonymousClass1IY.A0K : AnonymousClass1IY.A0J;
    }

    public synchronized InboxTrackableItem A07() {
        String str;
        if (this.A00 == null) {
            long A032 = A03();
            String A0U = this.A02.A0U();
            String A0G = A0G();
            GraphQLMessengerInboxUnitType A0Q = this.A02.A0Q();
            Preconditions.checkNotNull(A0Q);
            String name = A0Q.name();
            C33901oK A052 = A05();
            String A0P = this.A02.A0P(392918208);
            GSTModelShape1S0000000 gSTModelShape1S0000000 = this.A01;
            if (gSTModelShape1S0000000 == null) {
                str = null;
            } else {
                str = gSTModelShape1S0000000.A0P(73536401);
            }
            this.A00 = new InboxTrackableItem(A032, A0U, A0G, name, A052, A0P, str, A08(), A04(), this.A02.getBooleanValue(1890603023));
        }
        return this.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0070, code lost:
        if (r3.A09 == false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007e, code lost:
        if (r3.A0H == false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008e, code lost:
        if (r4.A0B() != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009c, code lost:
        if (r3.A0D == false) goto L_0x009e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableMap A08() {
        /*
            r12 = this;
            boolean r0 = r12 instanceof com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem
            if (r0 != 0) goto L_0x0148
            boolean r0 = r12 instanceof com.facebook.messaging.montage.inboxunit.InboxMontageItem
            if (r0 != 0) goto L_0x0184
            boolean r0 = r12 instanceof com.facebook.messaging.inbox2.items.InboxUnitThreadItem
            if (r0 != 0) goto L_0x0012
            boolean r0 = r12 instanceof com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem
            if (r0 != 0) goto L_0x0135
            r0 = 0
            return r0
        L_0x0012:
            r3 = r12
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r3 = (com.facebook.messaging.inbox2.items.InboxUnitThreadItem) r3
            com.google.common.collect.ImmutableMap$Builder r2 = com.google.common.collect.ImmutableMap.builder()
            com.facebook.messaging.model.threads.ThreadSummary r0 = r3.A01
            boolean r0 = r0.A0B()
            java.lang.String r1 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "unr"
            r2.put(r0, r1)
            X.1H4 r0 = r3.A05
            if (r0 == 0) goto L_0x003b
            X.1Gs r0 = r0.B6D()
            if (r0 == 0) goto L_0x003b
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = "bt"
            r2.put(r0, r1)
        L_0x003b:
            X.1nO r1 = r3.A00
            X.1nO r0 = X.C33321nO.A01
            if (r1 == r0) goto L_0x004a
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "pt"
            r2.put(r0, r1)
        L_0x004a:
            X.1Qt r0 = r3.A04
            if (r0 == 0) goto L_0x005d
            java.lang.String r4 = r0.A02
            java.lang.String r1 = ":"
            java.lang.String r0 = r0.A03
            java.lang.String r1 = X.AnonymousClass08S.A0P(r4, r1, r0)
            java.lang.String r0 = "cta"
            r2.put(r0, r1)
        L_0x005d:
            boolean r11 = r3.A0I()
            com.facebook.messaging.model.threads.ThreadSummary r4 = r3.A01
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A0S
            com.facebook.messaging.model.threadkey.ThreadKey r0 = X.C34001oU.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0072
            boolean r0 = r3.A09
            r10 = 1
            if (r0 != 0) goto L_0x0073
        L_0x0072:
            r10 = 0
        L_0x0073:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = X.C34001oU.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0080
            boolean r0 = r3.A0H
            r9 = 1
            if (r0 != 0) goto L_0x0081
        L_0x0080:
            r9 = 0
        L_0x0081:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = X.C34001oU.A00
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0090
            boolean r0 = r4.A0B()
            r8 = 0
            if (r0 == 0) goto L_0x0091
        L_0x0090:
            r8 = 1
        L_0x0091:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = X.C34001oU.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x009e
            boolean r0 = r3.A0D
            r7 = 1
            if (r0 != 0) goto L_0x009f
        L_0x009e:
            r7 = 0
        L_0x009f:
            boolean r6 = r3.A0C
            com.facebook.messaging.model.threadkey.ThreadKey r0 = X.C34001oU.A00
            boolean r0 = r1.equals(r0)
            r5 = r0 ^ 1
            boolean r1 = r3.A0E
            boolean r0 = r3.A0F
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            X.1Gm[] r1 = X.C21351Gl.A01(r11, r10, r9, r1, r0)
            java.lang.String r0 = "inboxLeft:"
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem.A01(r4, r1, r0)
            X.1Gm[] r1 = X.C21351Gl.A00(r8, r7, r6, r5)
            java.lang.String r0 = "inboxRight:"
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem.A01(r4, r1, r0)
            int r0 = r4.length()
            if (r0 <= 0) goto L_0x0133
            java.lang.String r0 = r4.toString()
        L_0x00ce:
            r1 = r0
            if (r0 == 0) goto L_0x00d6
            java.lang.String r0 = "sa"
            r2.put(r0, r1)
        L_0x00d6:
            java.lang.String r1 = r3.A07
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x00e3
            java.lang.String r0 = "ra"
            r2.put(r0, r1)
        L_0x00e3:
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r3.A03
            boolean r0 = r0.A03
            if (r0 == 0) goto L_0x0130
            java.lang.String r1 = "1"
        L_0x00eb:
            java.lang.String r0 = "an"
            r2.put(r0, r1)
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r3.A03
            java.lang.Long r0 = r0.A01
            if (r0 == 0) goto L_0x0103
            long r0 = r0.longValue()
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "lat"
            r2.put(r0, r1)
        L_0x0103:
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r3.A03
            java.lang.String r1 = r0.A02
            if (r1 == 0) goto L_0x010e
            java.lang.String r0 = "as"
            r2.put(r0, r1)
        L_0x010e:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r3.A01
            java.lang.String r1 = r0.A0v
            if (r1 == 0) goto L_0x0119
            java.lang.String r0 = "trri"
            r2.put(r0, r1)
        L_0x0119:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r3.A01
            com.facebook.messaging.model.threads.ThreadConnectivityData r0 = r0.A0d
            if (r0 == 0) goto L_0x012d
            com.facebook.graphql.enums.GraphQLThreadConnectivityStatus r0 = r0.A00()
            java.lang.String r1 = r0.toString()
        L_0x0127:
            java.lang.String r0 = "cs"
            r2.put(r0, r1)
            goto L_0x0143
        L_0x012d:
            java.lang.String r1 = "unknown"
            goto L_0x0127
        L_0x0130:
            java.lang.String r1 = "0"
            goto L_0x00eb
        L_0x0133:
            r0 = 0
            goto L_0x00ce
        L_0x0135:
            r0 = r12
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r0 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r0
            com.google.common.collect.ImmutableMap$Builder r2 = com.google.common.collect.ImmutableMap.builder()
            java.lang.String r1 = "userId"
            java.lang.String r0 = r0.A0I
            r2.put(r1, r0)
        L_0x0143:
            com.google.common.collect.ImmutableMap r0 = r2.build()
            return r0
        L_0x0148:
            r4 = r12
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r4 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem) r4
            com.google.common.collect.ImmutableMap$Builder r3 = com.google.common.collect.ImmutableMap.builder()
            boolean r0 = r4.A05
            java.lang.String r2 = "1"
            java.lang.String r1 = "an"
            if (r0 == 0) goto L_0x016f
            r3.put(r1, r2)
        L_0x015a:
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r0 = r4.A00
            X.C22401Le.A00(r0, r3)
            java.lang.String r0 = r4.A04
            boolean r0 = X.C06850cB.A0B(r0)
            if (r0 != 0) goto L_0x01b5
            java.lang.String r1 = r4.A04
            java.lang.String r0 = "iir"
            r3.put(r0, r1)
            goto L_0x01b5
        L_0x016f:
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r4.A02
            if (r0 == 0) goto L_0x015a
            r3.put(r1, r2)
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r4.A02
            java.lang.Long r0 = r0.A01
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "lat"
            r3.put(r0, r1)
            goto L_0x015a
        L_0x0184:
            r4 = r12
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r4 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r4
            com.google.common.collect.ImmutableMap$Builder r3 = com.google.common.collect.ImmutableMap.builder()
            java.lang.String r1 = com.facebook.messaging.montage.inboxunit.InboxMontageItem.A01(r4)
            java.lang.String r0 = "mt"
            r3.put(r0, r1)
            boolean r0 = r4.A06
            java.lang.String r2 = "1"
            java.lang.String r1 = "an"
            if (r0 == 0) goto L_0x01ba
            r3.put(r1, r2)
        L_0x019f:
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = r4.A02
            if (r0 == 0) goto L_0x01b0
            com.facebook.messaging.model.messages.Message r0 = r0.A01
            if (r0 == 0) goto L_0x01b0
            java.lang.String r1 = r0.A0q
            if (r1 == 0) goto L_0x01b0
            java.lang.String r0 = "msg_id"
            r3.put(r0, r1)
        L_0x01b0:
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r0 = r4.A01
            X.C22401Le.A00(r0, r3)
        L_0x01b5:
            com.google.common.collect.ImmutableMap r0 = r3.build()
            return r0
        L_0x01ba:
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r4.A04
            if (r0 == 0) goto L_0x019f
            r3.put(r1, r2)
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r4.A04
            java.lang.Long r0 = r0.A01
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "lat"
            r3.put(r0, r1)
            goto L_0x019f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.inbox2.items.InboxUnitItem.A08():com.google.common.collect.ImmutableMap");
    }

    public String A09() {
        if ((this instanceof WorkChatInviteCoworkersUnitInboxItem) || (this instanceof InboxLoadMorePlaceholderItem)) {
            return null;
        }
        if (this instanceof InboxUnitMontageAndActiveNowItem) {
            return "tap_montage_and_active_now";
        }
        if (this instanceof InboxUnitMontageActiveNowItem) {
            return "tap_horizontal_tile_unit";
        }
        if (this instanceof InboxMontageItem) {
            return "tap_montage_carousel_item";
        }
        if (this instanceof InboxUnitFolderItem) {
            return ((InboxUnitFolderItem) this).A00.analyticsNavigationTapPoints;
        }
        if ((this instanceof NonInboxServiceSectionHeaderItem) || (this instanceof InboxUnitSectionHeaderItem)) {
            return null;
        }
        if (this instanceof PlatformExtensionsVerticalInboxItem) {
            return "tap_messenger_extension_item";
        }
        if (this instanceof InboxUnitSeeAllItem) {
            return "tap_see_all";
        }
        if (this instanceof InboxMoreThreadsItem) {
            return "tap_load_more";
        }
        if (!(this instanceof MessageRequestsHeaderInboxItem)) {
            return !(this instanceof MessageRequestsBannerInboxItem) ? !(this instanceof InboxUnitThreadItem) ? !(this instanceof HorizontalTilesUnitInboxItem) ? !(this instanceof MessengerDiscoveryCategoryInboxItem) ? !(this instanceof DiscoverTabGameNuxFooterItem) ? !(this instanceof DiscoverTabAttachmentUnit) ? !(this instanceof DiscoverTabAttachmentItem) ? !(this instanceof DiscoverLocationUpsellItem) ? !(this instanceof InboxUnitConversationStarterItem) ? "tap_messenger_ads_item" : "tap_conversation_starter" : "tap_messenger_discovery_location_upsell" : ((DiscoverTabAttachmentItem) this).A06.analyticsTapPoint : ((DiscoverTabAttachmentUnit) this).A00.analyticsTapPoint : "tap_discover_game_nux_footer" : "tap_messenger_discovery_category" : "tap_horizontal_tile_unit" : "tap_conversation_thread" : "tap_message_request";
        }
        return null;
    }

    public String A0A() {
        GSTModelShape1S0000000 AtV;
        if ((this instanceof InboxUnitConversationStarterItem) && (AtV = ((InboxUnitConversationStarterItem) this).A00.AtV()) != null) {
            return AtV.A3y();
        }
        return null;
    }

    public boolean A0E() {
        if ((this instanceof WorkChatInviteCoworkersUnitInboxItem) || (this instanceof InboxLoadMorePlaceholderItem) || (this instanceof InboxUnitMontageAndActiveNowItem)) {
            return true;
        }
        if (this instanceof InboxUnitMontageActiveNowItem) {
            return false;
        }
        if (this instanceof InboxMontageItem) {
            return true;
        }
        if ((this instanceof InboxUnitFolderItem) || (this instanceof NonInboxServiceSectionHeaderItem) || (this instanceof InboxUnitSectionHeaderItem)) {
            return false;
        }
        if (this instanceof PlatformExtensionsVerticalInboxItem) {
            return true;
        }
        if (this instanceof InboxUnitSeeAllItem) {
            return false;
        }
        if (this instanceof InboxMoreThreadsItem) {
            return true;
        }
        if (this instanceof MessageRequestsHeaderInboxItem) {
            return false;
        }
        if ((this instanceof MessageRequestsBannerInboxItem) || (this instanceof InboxUnitThreadItem)) {
            return true;
        }
        if (this instanceof HorizontalTilesUnitInboxItem) {
            return false;
        }
        if ((this instanceof MessengerDiscoveryCategoryInboxItem) || (this instanceof DiscoverTabGameNuxFooterItem)) {
            return true;
        }
        if (this instanceof DiscoverTabAttachmentUnit) {
            return false;
        }
        if ((this instanceof DiscoverTabAttachmentItem) || (this instanceof DiscoverLocationUpsellItem)) {
            return true;
        }
        boolean z = this instanceof InboxUnitConversationStarterItem;
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0204, code lost:
        if (r6.A17 != r5.A17) goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x03fc, code lost:
        if (r12.A01 == null) goto L_0x03fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x0497, code lost:
        if (r1.A0Q.equals(r0.A0Q) != false) goto L_0x0499;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:315:0x04eb, code lost:
        if (r0 != false) goto L_0x0499;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:339:0x0561, code lost:
        if (r3 == r2) goto L_0x0499;
     */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x03e8 A[PHI: r3 
      PHI: (r3v7 boolean) = (r3v8 boolean), (r3v8 boolean), (r3v8 boolean), (r3v8 boolean), (r3v8 boolean), (r3v8 boolean), (r3v8 boolean), (r3v8 boolean), (r3v8 boolean), (r3v14 boolean), (r3v14 boolean), (r3v14 boolean), (r3v14 boolean), (r3v14 boolean), (r3v14 boolean), (r3v14 boolean), (r3v14 boolean), (r3v14 boolean), (r3v17 boolean), (r3v17 boolean), (r3v17 boolean), (r3v17 boolean), (r3v17 boolean) binds: [B:213:0x036b, B:215:0x0373, B:217:0x0379, B:219:0x0383, B:231:0x03b3, B:225:0x0395, B:227:0x039f, B:229:0x03a9, B:223:0x038b, B:233:0x03c0, B:235:0x03c8, B:237:0x03ce, B:239:0x03d4, B:241:0x03da, B:268:0x041b, B:255:0x03fc, B:248:0x03eb, B:245:0x03e6, B:270:0x0428, B:272:0x0438, B:274:0x0446, B:276:0x0454, B:278:0x0462] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0F(com.facebook.messaging.inbox2.items.InboxUnitItem r12) {
        /*
            r11 = this;
            boolean r0 = r11 instanceof com.facebook.workchat.inbox.invites.bridge.WorkChatInviteCoworkersUnitInboxItem
            if (r0 != 0) goto L_0x05b1
            boolean r0 = r11 instanceof com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem
            if (r0 != 0) goto L_0x006e
            boolean r0 = r11 instanceof com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem
            if (r0 != 0) goto L_0x0568
            boolean r0 = r11 instanceof com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem
            if (r0 != 0) goto L_0x0466
            boolean r0 = r11 instanceof com.facebook.messaging.montage.inboxunit.InboxMontageItem
            if (r0 != 0) goto L_0x0361
            boolean r0 = r11 instanceof com.facebook.messaging.inboxfolder.InboxUnitFolderItem
            if (r0 != 0) goto L_0x02f8
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem
            if (r0 != 0) goto L_0x02cf
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem
            if (r0 != 0) goto L_0x03b6
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem
            if (r0 != 0) goto L_0x041e
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem
            if (r0 != 0) goto L_0x02b9
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem
            if (r0 != 0) goto L_0x02a3
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem
            if (r0 != 0) goto L_0x028d
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem
            if (r0 != 0) goto L_0x024f
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.items.InboxUnitThreadItem
            if (r0 != 0) goto L_0x0107
            boolean r0 = r11 instanceof com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem
            if (r0 != 0) goto L_0x00ef
            boolean r0 = r11 instanceof com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem
            if (r0 != 0) goto L_0x00cb
            boolean r0 = r11 instanceof com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem
            if (r0 != 0) goto L_0x00c9
            boolean r0 = r11 instanceof com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit
            if (r0 != 0) goto L_0x00b7
            boolean r0 = r11 instanceof com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem
            if (r0 != 0) goto L_0x032b
            boolean r0 = r11 instanceof com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem
            if (r0 != 0) goto L_0x00c9
            boolean r0 = r11 instanceof com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem
            if (r0 != 0) goto L_0x0079
            r2 = r11
            com.facebook.messaging.business.inboxads.common.InboxAdsItem r2 = (com.facebook.messaging.business.inboxads.common.InboxAdsItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.business.inboxads.common.InboxAdsItem> r0 = com.facebook.messaging.business.inboxads.common.InboxAdsItem.class
            if (r1 != r0) goto L_0x0105
            com.facebook.messaging.business.inboxads.common.InboxAdsItem r12 = (com.facebook.messaging.business.inboxads.common.InboxAdsItem) r12
            com.facebook.messaging.business.inboxads.common.InboxAdsData r0 = r2.A00
            java.lang.String r1 = r0.A0E
            com.facebook.messaging.business.inboxads.common.InboxAdsData r0 = r12.A00
            java.lang.String r0 = r0.A0E
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
        L_0x006d:
            return r0
        L_0x006e:
            java.lang.Class r2 = r12.getClass()
            java.lang.Class<com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem> r1 = com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem.class
            r0 = 0
            if (r2 != r1) goto L_0x006d
            r0 = 1
            return r0
        L_0x0079:
            r2 = r11
            com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem r2 = (com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem> r0 = com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem.class
            r5 = 0
            if (r1 != r0) goto L_0x00b1
            com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem r12 = (com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem) r12
            X.0xv r1 = r2.A00
            X.0xv r0 = r12.A00
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00b1
            com.facebook.messaging.model.threads.ThreadSummary r4 = r2.A01
            com.facebook.messaging.model.threads.ThreadSummary r3 = r12.A01
            r2 = 1
            if (r4 == 0) goto L_0x00b2
            if (r3 == 0) goto L_0x00b2
            android.net.Uri r1 = r4.A0E
            android.net.Uri r0 = r3.A0E
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x00b5
            com.google.common.collect.ImmutableList r1 = r4.A0m
            com.google.common.collect.ImmutableList r0 = r3.A0m
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x00b5
        L_0x00ae:
            if (r2 == 0) goto L_0x00b1
            r5 = 1
        L_0x00b1:
            return r5
        L_0x00b2:
            if (r4 != r3) goto L_0x00b5
            goto L_0x00ae
        L_0x00b5:
            r2 = 0
            goto L_0x00ae
        L_0x00b7:
            r2 = r11
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r2 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit> r0 = com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit.class
            if (r1 != r0) goto L_0x0105
            com.google.common.collect.ImmutableList r1 = r2.A01
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r12 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit) r12
            com.google.common.collect.ImmutableList r0 = r12.A01
            goto L_0x0100
        L_0x00c9:
            r0 = 0
            return r0
        L_0x00cb:
            r3 = r11
            com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem r3 = (com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem) r3
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem> r0 = com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem.class
            r2 = 0
            if (r1 != r0) goto L_0x0360
            com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem r12 = (com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem) r12
            java.lang.String r1 = r3.A00
            java.lang.String r0 = r12.A00
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0360
            java.lang.String r1 = r3.A01
            java.lang.String r0 = r12.A01
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0360
            goto L_0x035f
        L_0x00ef:
            r2 = r11
            com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem r2 = (com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem> r0 = com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem.class
            if (r1 != r0) goto L_0x0105
            com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem r12 = (com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem) r12
            com.google.common.collect.ImmutableList r1 = r2.A00
            com.google.common.collect.ImmutableList r0 = r12.A00
        L_0x0100:
            boolean r0 = A00(r1, r0)
            return r0
        L_0x0105:
            r0 = 0
            return r0
        L_0x0107:
            r2 = r11
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r2 = (com.facebook.messaging.inbox2.items.InboxUnitThreadItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.items.InboxUnitThreadItem> r0 = com.facebook.messaging.inbox2.items.InboxUnitThreadItem.class
            r8 = 0
            if (r1 != r0) goto L_0x024e
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r12 = (com.facebook.messaging.inbox2.items.InboxUnitThreadItem) r12
            boolean r1 = r2.A0C
            boolean r0 = r12.A0C
            if (r1 != r0) goto L_0x024e
            boolean r1 = r2.A0A
            boolean r0 = r12.A0A
            if (r1 != r0) goto L_0x024e
            boolean r1 = r2.A09
            boolean r0 = r12.A09
            if (r1 != r0) goto L_0x024e
            boolean r1 = r2.A0H
            boolean r0 = r12.A0H
            if (r1 != r0) goto L_0x024e
            boolean r1 = r2.A0G
            boolean r0 = r12.A0G
            if (r1 != r0) goto L_0x024e
            boolean r1 = r2.A0B
            boolean r0 = r12.A0B
            if (r1 != r0) goto L_0x024e
            com.facebook.messaging.model.threads.ThreadSummary r6 = r2.A01
            com.facebook.messaging.model.threads.ThreadSummary r5 = r12.A01
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r6.A0S
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0S
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0206
            long r3 = r6.A0B
            long r0 = r5.A0B
            int r7 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r7 != 0) goto L_0x0206
            boolean r1 = r6.A0B()
            boolean r0 = r5.A0B()
            if (r1 != r0) goto L_0x0206
            boolean r1 = r6.A10
            boolean r0 = r5.A10
            if (r1 != r0) goto L_0x0206
            boolean r1 = r6.A0z
            boolean r0 = r5.A0z
            if (r1 != r0) goto L_0x0206
            boolean r1 = r6.A12
            boolean r0 = r5.A12
            if (r1 != r0) goto L_0x0206
            X.105 r1 = r6.A0Z
            X.105 r0 = r5.A0Z
            if (r1 != r0) goto L_0x0206
            com.google.common.collect.ImmutableList r1 = r6.A0m
            com.google.common.collect.ImmutableList r0 = r5.A0m
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            java.lang.String r1 = r6.A0t
            java.lang.String r0 = r5.A0t
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            X.0l8 r1 = r6.A0O
            X.0l8 r0 = r5.A0O
            if (r1 != r0) goto L_0x0206
            java.lang.String r1 = r6.A0p
            java.lang.String r0 = r5.A0p
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            java.lang.String r1 = r6.A0w
            java.lang.String r0 = r5.A0w
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            com.facebook.messaging.model.messages.ParticipantInfo r1 = r6.A0Q
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r5.A0Q
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r1 = r6.A0L
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r5.A0L
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            java.lang.String r1 = r6.A0s
            java.lang.String r0 = r5.A0s
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r1 = r6.A0h
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r0 = r5.A0h
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            X.0zi r1 = r6.A0i
            X.0zi r0 = r5.A0i
            if (r1 != r0) goto L_0x0206
            com.google.common.collect.ImmutableList r1 = r6.A0l
            com.google.common.collect.ImmutableList r0 = r5.A0l
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r6.A0R
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0R
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            com.facebook.messaging.model.threads.MontageThreadPreview r1 = r6.A0X
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r5.A0X
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            com.facebook.messaging.model.threads.ThreadMediaPreview r1 = r6.A0f
            com.facebook.messaging.model.threads.ThreadMediaPreview r0 = r5.A0f
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            com.google.common.collect.ImmutableList r1 = r6.A0o
            com.google.common.collect.ImmutableList r0 = r5.A0o
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0206
            boolean r3 = r6.A17
            boolean r1 = r5.A17
            r0 = 1
            if (r3 == r1) goto L_0x0207
        L_0x0206:
            r0 = 0
        L_0x0207:
            if (r0 == 0) goto L_0x024e
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r1 = r2.A02
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = r12.A02
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x024e
            X.1H4 r1 = r2.A05
            X.1H4 r0 = r12.A05
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x024e
            java.lang.String r1 = r2.A06
            java.lang.String r0 = r12.A06
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x024e
            java.lang.String r1 = r2.A07
            java.lang.String r0 = r12.A07
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x024e
            boolean r1 = r2.A0F
            boolean r0 = r12.A0F
            if (r1 != r0) goto L_0x024e
            boolean r1 = r2.A0E
            boolean r0 = r12.A0E
            if (r1 != r0) goto L_0x024e
            java.lang.String r1 = r2.A08
            java.lang.String r0 = r12.A08
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x024e
            boolean r1 = r2.A0D
            boolean r0 = r12.A0D
            if (r1 != r0) goto L_0x024e
            r8 = 1
        L_0x024e:
            return r8
        L_0x024f:
            r2 = r11
            com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem r2 = (com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem> r0 = com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem.class
            r4 = 0
            if (r1 != r0) goto L_0x05a7
            com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem r12 = (com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem) r12
            java.lang.String r1 = r2.A01
            java.lang.String r0 = r12.A01
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x05a7
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r0 = r2.A00
            java.lang.String r1 = r0.A03
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r0 = r12.A00
            java.lang.String r0 = r0.A03
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x05a7
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r3 = r2.A00
            int r1 = r3.A01
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r2 = r12.A00
            int r0 = r2.A01
            if (r1 != r0) goto L_0x05a7
            int r1 = r3.A02
            int r0 = r2.A02
            if (r1 != r0) goto L_0x05a7
            int r1 = r3.A00
            int r0 = r2.A00
            if (r1 != r0) goto L_0x05a7
            r4 = 1
            return r4
        L_0x028d:
            r3 = r11
            com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem r3 = (com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem) r3
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem> r0 = com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem.class
            r2 = 0
            if (r1 != r0) goto L_0x0360
            com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem r12 = (com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem) r12
            int r1 = r3.A00
            int r0 = r12.A00
            if (r1 != r0) goto L_0x0360
            goto L_0x035f
        L_0x02a3:
            r3 = r11
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r3 = (com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem) r3
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem> r0 = com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem.class
            r2 = 0
            if (r1 != r0) goto L_0x0360
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r12 = (com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem) r12
            X.17k r1 = r3.A00
            X.17k r0 = r12.A00
            if (r1 != r0) goto L_0x0360
            goto L_0x035f
        L_0x02b9:
            r3 = r11
            com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem r3 = (com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem) r3
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem> r0 = com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem.class
            r2 = 0
            if (r1 != r0) goto L_0x0360
            com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem r12 = (com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem) r12
            int r1 = r3.A00
            int r0 = r12.A00
            if (r1 != r0) goto L_0x0360
            goto L_0x035f
        L_0x02cf:
            r3 = r11
            com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem r3 = (com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem) r3
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem> r0 = com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem.class
            r2 = 0
            if (r1 != r0) goto L_0x0360
            com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem r12 = (com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem) r12
            boolean r1 = r3.A02
            boolean r0 = r12.A02
            if (r1 != r0) goto L_0x0360
            java.lang.String r1 = r3.A01
            java.lang.String r0 = r12.A01
            boolean r0 = X.C06850cB.A0C(r1, r0)
            if (r0 == 0) goto L_0x0360
            java.lang.String r1 = r3.A00
            java.lang.String r0 = r12.A00
            boolean r0 = X.C06850cB.A0C(r1, r0)
            if (r0 == 0) goto L_0x0360
            goto L_0x035f
        L_0x02f8:
            r3 = r11
            com.facebook.messaging.inboxfolder.InboxUnitFolderItem r3 = (com.facebook.messaging.inboxfolder.InboxUnitFolderItem) r3
            boolean r0 = r12 instanceof com.facebook.messaging.inboxfolder.InboxUnitFolderItem
            r2 = 0
            if (r0 == 0) goto L_0x0360
            com.facebook.messaging.inboxfolder.InboxUnitFolderItem r12 = (com.facebook.messaging.inboxfolder.InboxUnitFolderItem) r12
            X.3Ct r1 = r12.A00
            X.3Ct r0 = r3.A00
            if (r1 != r0) goto L_0x0360
            com.google.common.collect.ImmutableList r0 = r12.A02
            boolean r0 = r0.isEmpty()
            r1 = r0 ^ 1
            com.google.common.collect.ImmutableList r0 = r3.A02
            boolean r0 = r0.isEmpty()
            r0 = r0 ^ 1
            if (r1 != r0) goto L_0x0360
            com.google.common.collect.ImmutableList r1 = r12.A02
            com.google.common.collect.ImmutableList r0 = r3.A02
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0360
            com.facebook.messaging.model.threads.ThreadSummary r1 = r12.A01
            com.facebook.messaging.model.threads.ThreadSummary r0 = r3.A01
            if (r1 != r0) goto L_0x0360
            goto L_0x035f
        L_0x032b:
            r3 = r11
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r3 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r3
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem> r0 = com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem.class
            r2 = 0
            if (r1 != r0) goto L_0x0360
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r12 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r12
            java.lang.String r1 = r3.A0F
            java.lang.String r0 = r12.A0F
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0360
            java.lang.String r1 = r3.A0H
            java.lang.String r0 = r12.A0H
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0360
            java.lang.String r1 = r3.A0I
            java.lang.String r0 = r12.A0I
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0360
            boolean r0 = r3.A0J
            if (r0 != 0) goto L_0x0360
            boolean r0 = r12.A0J
            if (r0 != 0) goto L_0x0360
        L_0x035f:
            r2 = 1
        L_0x0360:
            return r2
        L_0x0361:
            r2 = r11
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r2 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.montage.inboxunit.InboxMontageItem> r0 = com.facebook.messaging.montage.inboxunit.InboxMontageItem.class
            r3 = 0
            if (r1 != r0) goto L_0x03e8
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r12 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r12
            int r1 = r2.A00
            int r0 = r12.A00
            if (r1 != r0) goto L_0x03e8
            boolean r1 = r2.A06
            boolean r0 = r12.A06
            if (r1 != r0) goto L_0x03e8
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r1 = r2.A02
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = r12.A02
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
            com.facebook.messaging.montage.model.MontageInboxNuxItem r1 = r2.A03
            if (r1 != 0) goto L_0x03ad
            com.facebook.messaging.montage.model.MontageInboxNuxItem r0 = r12.A03
            if (r0 != 0) goto L_0x03e8
        L_0x038d:
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r1 = r2.A01
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r0 = r12.A01
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
            com.facebook.user.model.LastActive r1 = r2.A05
            com.facebook.user.model.LastActive r0 = r12.A05
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r1 = r2.A04
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r12.A04
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
            goto L_0x0464
        L_0x03ad:
            com.facebook.messaging.montage.model.MontageInboxNuxItem r0 = r12.A03
            boolean r0 = r1.A00(r0)
            if (r0 == 0) goto L_0x03e8
            goto L_0x038d
        L_0x03b6:
            r2 = r11
            com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem r2 = (com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem> r0 = com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem.class
            r3 = 0
            if (r1 != r0) goto L_0x03e8
            com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem r12 = (com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem) r12
            int r1 = r2.A00
            int r0 = r12.A00
            if (r1 != r0) goto L_0x03e8
            boolean r1 = r2.A05
            boolean r0 = r12.A05
            if (r1 != r0) goto L_0x03e8
            boolean r1 = r2.A06
            boolean r0 = r12.A06
            if (r1 != r0) goto L_0x03e8
            boolean r1 = r2.A07
            boolean r0 = r12.A07
            if (r1 != r0) goto L_0x03e8
            java.lang.String r1 = r2.A03
            if (r1 == 0) goto L_0x03e9
            java.lang.String r0 = r12.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03ed
        L_0x03e8:
            return r3
        L_0x03e9:
            java.lang.String r0 = r12.A03
            if (r0 != 0) goto L_0x03e8
        L_0x03ed:
            java.lang.String r1 = r2.A01
            if (r1 == 0) goto L_0x03fa
            java.lang.String r0 = r12.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03fe
            return r3
        L_0x03fa:
            java.lang.String r0 = r12.A01
            if (r0 != 0) goto L_0x03e8
        L_0x03fe:
            java.lang.String r1 = r2.A02
            if (r1 == 0) goto L_0x040b
            java.lang.String r0 = r12.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0410
            return r3
        L_0x040b:
            java.lang.String r0 = r12.A02
            if (r0 == 0) goto L_0x0410
            return r3
        L_0x0410:
            java.lang.String r1 = r2.A04
            java.lang.String r0 = r12.A04
            if (r1 == 0) goto L_0x041b
            boolean r3 = r1.equals(r0)
            return r3
        L_0x041b:
            if (r0 != 0) goto L_0x03e8
            goto L_0x0464
        L_0x041e:
            r2 = r11
            com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem r2 = (com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem) r2
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem> r0 = com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem.class
            r3 = 0
            if (r1 != r0) goto L_0x03e8
            com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem r12 = (com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem) r12
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r2.A00
            java.lang.String r1 = r0.A02
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r12.A00
            java.lang.String r0 = r0.A02
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r2.A00
            java.lang.String r1 = r0.A01
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r12.A00
            java.lang.String r0 = r0.A01
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r2.A00
            java.lang.String r1 = r0.A03
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r12.A00
            java.lang.String r0 = r0.A03
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r2.A00
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r1 = r0.A00
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = r12.A00
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r0.A00
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x03e8
        L_0x0464:
            r3 = 1
            return r3
        L_0x0466:
            r6 = r11
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r6 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem) r6
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem> r0 = com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem.class
            r10 = 0
            if (r1 != r0) goto L_0x04c0
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r12 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem) r12
            com.facebook.messaging.inbox2.activenow.loader.Entity r3 = r6.A01
            com.facebook.messaging.inbox2.activenow.loader.Entity r2 = r12.A01
            r9 = 1
            if (r3 == 0) goto L_0x0561
            if (r2 == 0) goto L_0x0561
            X.3i4 r0 = r3.A00
            X.3i4 r1 = X.C74553i4.USER
            if (r0 != r1) goto L_0x04c1
            X.3i4 r0 = r2.A00
            if (r0 != r1) goto L_0x0565
            com.facebook.user.model.User r1 = r3.A02
            if (r1 == 0) goto L_0x0565
            com.facebook.user.model.User r0 = r2.A02
            if (r0 == 0) goto L_0x0565
            com.facebook.user.model.UserKey r1 = r1.A0Q
            com.facebook.user.model.UserKey r0 = r0.A0Q
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0565
        L_0x0499:
            if (r9 == 0) goto L_0x04c0
            X.1H4 r1 = r6.A03
            X.1H4 r0 = r12.A03
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x04c0
            boolean r1 = r6.A05
            boolean r0 = r12.A05
            if (r1 != r0) goto L_0x04c0
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r1 = r6.A00
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r0 = r12.A00
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x04c0
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r1 = r6.A02
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r0 = r12.A02
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x04c0
            r10 = 1
        L_0x04c0:
            return r10
        L_0x04c1:
            X.3i4 r1 = r2.A00
            X.3i4 r0 = X.C74553i4.GROUP
            if (r1 != r0) goto L_0x0565
            com.facebook.messaging.inbox2.activenow.model.GroupPresenceInfo r8 = r3.A01
            if (r8 == 0) goto L_0x0565
            com.facebook.messaging.inbox2.activenow.model.GroupPresenceInfo r5 = r2.A01
            if (r5 == 0) goto L_0x0565
            com.facebook.messaging.model.threads.ThreadSummary r0 = r8.A00
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r0.A0S
            com.facebook.messaging.model.threads.ThreadSummary r0 = r5.A00
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0565
            com.google.common.collect.ImmutableList r0 = r8.A01
            boolean r0 = X.C013509w.A02(r0)
            if (r0 == 0) goto L_0x04ee
            com.google.common.collect.ImmutableList r0 = r5.A01
            boolean r0 = X.C013509w.A02(r0)
        L_0x04eb:
            if (r0 == 0) goto L_0x0565
            goto L_0x0499
        L_0x04ee:
            com.google.common.collect.ImmutableList r0 = r8.A01
            X.1Xv r7 = r0.iterator()
        L_0x04f4:
            boolean r0 = r7.hasNext()
            r4 = 1
            if (r0 == 0) goto L_0x0523
            java.lang.Object r3 = r7.next()
            com.facebook.user.model.User r3 = (com.facebook.user.model.User) r3
            com.google.common.collect.ImmutableList r0 = r5.A01
            X.1Xv r2 = r0.iterator()
        L_0x0507:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0521
            java.lang.Object r0 = r2.next()
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0
            com.facebook.user.model.UserKey r1 = r3.A0Q
            com.facebook.user.model.UserKey r0 = r0.A0Q
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0507
        L_0x051d:
            if (r4 != 0) goto L_0x04f4
        L_0x051f:
            r0 = 0
            goto L_0x04eb
        L_0x0521:
            r4 = 0
            goto L_0x051d
        L_0x0523:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r8.A00
            com.google.common.collect.ImmutableList r0 = r0.A0m
            X.1Xv r4 = r0.iterator()
        L_0x052b:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x055f
            java.lang.Object r3 = r4.next()
            com.facebook.messaging.model.threads.ThreadParticipant r3 = (com.facebook.messaging.model.threads.ThreadParticipant) r3
            com.facebook.messaging.model.threads.ThreadSummary r0 = r5.A00
            com.google.common.collect.ImmutableList r0 = r0.A0m
            X.1Xv r2 = r0.iterator()
        L_0x053f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x055d
            java.lang.Object r0 = r2.next()
            com.facebook.messaging.model.threads.ThreadParticipant r0 = (com.facebook.messaging.model.threads.ThreadParticipant) r0
            com.facebook.user.model.UserKey r1 = r3.A00()
            com.facebook.user.model.UserKey r0 = r0.A00()
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x053f
            r0 = 1
        L_0x055a:
            if (r0 != 0) goto L_0x052b
            goto L_0x051f
        L_0x055d:
            r0 = 0
            goto L_0x055a
        L_0x055f:
            r0 = 1
            goto L_0x04eb
        L_0x0561:
            if (r3 != r2) goto L_0x0565
            goto L_0x0499
        L_0x0565:
            r9 = 0
            goto L_0x0499
        L_0x0568:
            r3 = r11
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem r3 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem) r3
            java.lang.Class r1 = r12.getClass()
            java.lang.Class<com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem> r0 = com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem.class
            r4 = 0
            if (r1 != r0) goto L_0x05a7
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem r12 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem) r12
            com.facebook.messaging.montage.model.MontageInboxNuxItem r1 = r3.A00
            r4 = 1
            if (r1 != 0) goto L_0x05a8
            com.facebook.messaging.montage.model.MontageInboxNuxItem r0 = r12.A00
            r2 = 0
            if (r0 != 0) goto L_0x0581
            r2 = 1
        L_0x0581:
            com.google.common.collect.ImmutableList r1 = r3.A04
            com.google.common.collect.ImmutableList r0 = r12.A04
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x05af
            com.google.common.collect.ImmutableList r1 = r3.A03
            com.google.common.collect.ImmutableList r0 = r12.A03
            boolean r0 = A00(r1, r0)
            if (r0 == 0) goto L_0x05af
            com.google.common.collect.ImmutableList r1 = r3.A01
            com.google.common.collect.ImmutableList r0 = r12.A01
            boolean r0 = A00(r1, r0)
            if (r0 == 0) goto L_0x05af
            if (r2 == 0) goto L_0x05af
            boolean r1 = r3.A05
            boolean r0 = r12.A05
            if (r1 != r0) goto L_0x05af
        L_0x05a7:
            return r4
        L_0x05a8:
            com.facebook.messaging.montage.model.MontageInboxNuxItem r0 = r12.A00
            boolean r2 = r1.A00(r0)
            goto L_0x0581
        L_0x05af:
            r4 = 0
            return r4
        L_0x05b1:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.inbox2.items.InboxUnitItem.A0F(com.facebook.messaging.inbox2.items.InboxUnitItem):boolean");
    }

    public int describeContents() {
        if ((this instanceof PlatformExtensionsVerticalInboxItem) || (this instanceof MessengerDiscoveryCategoryInboxItem) || (this instanceof DiscoverTabGameNuxFooterItem) || (this instanceof DiscoverTabAttachmentItem) || (this instanceof DiscoverLocationUpsellItem)) {
            return 0;
        }
        boolean z = this instanceof InboxAdsItem;
        return 0;
    }

    public long A03() {
        int i;
        C07180co A012;
        C07190cq r0;
        String str;
        if (!(this instanceof InboxUnitMontageActiveNowItem)) {
            if (this instanceof InboxMontageItem) {
                InboxMontageItem inboxMontageItem = (InboxMontageItem) this;
                A012 = A06.A01();
                A012.A05(inboxMontageItem.A02.A0U(), Charsets.UTF_8);
                A012.A03(inboxMontageItem.A00);
                BasicMontageThreadInfo basicMontageThreadInfo = inboxMontageItem.A02;
                if (basicMontageThreadInfo != null) {
                    A012.A04(basicMontageThreadInfo.A02.A00);
                }
            } else if (this instanceof InboxUnitThreadItem) {
                InboxUnitThreadItem inboxUnitThreadItem = (InboxUnitThreadItem) this;
                C07180co A013 = A06.A01();
                A013.A05(inboxUnitThreadItem.A02.A0U(), Charsets.UTF_8);
                ThreadKey threadKey = inboxUnitThreadItem.A01.A0S;
                A013.A04((((((((((long) threadKey.A05.hashCode()) * 63) + threadKey.A03) * 63) + threadKey.A01) * 63) + threadKey.A04) * 63) + threadKey.A02);
                r0 = A013.A09();
                return r0.A02();
            } else if (!(this instanceof DiscoverTabAttachmentItem)) {
                A012 = A06.A01();
                GSTModelShape1S0000000 gSTModelShape1S0000000 = this.A01;
                if (gSTModelShape1S0000000 != null) {
                    str = gSTModelShape1S0000000.A3m();
                } else {
                    A012.A05(this.A02.A0U(), Charsets.UTF_8);
                    AnonymousClass1GH r02 = this.A03;
                    if (r02 != null) {
                        str = r02.analyticsString;
                    }
                }
                A012.A05(str, Charsets.UTF_8);
            } else {
                DiscoverTabAttachmentItem discoverTabAttachmentItem = (DiscoverTabAttachmentItem) this;
                i = AnonymousClass07K.A00(AnonymousClass08S.A0J(discoverTabAttachmentItem.A0F, discoverTabAttachmentItem.A0I));
            }
            r0 = A012.A09();
            return r0.A02();
        }
        InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem = (InboxUnitMontageActiveNowItem) this;
        Preconditions.checkNotNull(inboxUnitMontageActiveNowItem.A01);
        i = AnonymousClass07K.A01(inboxUnitMontageActiveNowItem.A02.A0U(), Long.valueOf(InboxUnitMontageActiveNowItem.A01(inboxUnitMontageActiveNowItem.A01)));
        return (long) i;
    }

    public String A0G() {
        GSTModelShape1S0000000 gSTModelShape1S0000000 = this.A01;
        if (gSTModelShape1S0000000 != null) {
            return gSTModelShape1S0000000.A3m();
        }
        if (this.A03 != null) {
            return AnonymousClass08S.A0P(this.A02.A0U(), ":", this.A03.analyticsString);
        }
        return this.A02.A0U();
    }

    public void A0H(Parcel parcel, int i) {
        InboxTrackableItem inboxTrackableItem;
        Bundle bundle = new Bundle();
        AnonymousClass324.A09(bundle, "node", this.A02);
        AnonymousClass324.A09(bundle, "node_item", this.A01);
        parcel.writeBundle(bundle);
        parcel.writeSerializable(this.A03);
        synchronized (this) {
            inboxTrackableItem = this.A00;
        }
        parcel.writeParcelable(inboxTrackableItem, i);
    }

    public static boolean A00(ImmutableList immutableList, ImmutableList immutableList2) {
        if (immutableList.size() == immutableList2.size()) {
            int size = immutableList.size();
            int i = 0;
            while (i < size) {
                if (((InboxUnitItem) immutableList.get(i)).A0F((InboxUnitItem) immutableList2.get(i))) {
                    i++;
                }
            }
            return true;
        }
        return false;
    }

    public void A0B(int i) {
        A07().A00 = i;
    }

    public void A0C(int i) {
        A07().A01 = i;
    }

    public void A0D(int i) {
        A07().A02 = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        A0H(parcel, i);
    }

    public InboxUnitItem(C27161ck r2) {
        this(r2, null, null);
    }

    public InboxUnitItem(C27161ck r1, GSTModelShape1S0000000 gSTModelShape1S0000000, AnonymousClass1GH r3) {
        Preconditions.checkNotNull(r1);
        this.A02 = r1;
        this.A01 = gSTModelShape1S0000000;
        this.A03 = r3;
    }

    public InboxUnitItem(Parcel parcel) {
        Bundle readBundle = parcel.readBundle();
        readBundle.setClassLoader(InboxUnitItem.class.getClassLoader());
        C27161ck r2 = (C27161ck) AnonymousClass324.A02(readBundle, "node");
        Preconditions.checkNotNull(r2);
        this.A02 = r2;
        this.A01 = (GSTModelShape1S0000000) AnonymousClass324.A02(readBundle, "node_item");
        this.A03 = (AnonymousClass1GH) parcel.readSerializable();
        this.A00 = (InboxTrackableItem) parcel.readParcelable(InboxTrackableItem.class.getClassLoader());
    }
}
