package com.facebook.messaging.inbox2.messagerequests;

import X.AnonymousClass44J;
import X.C27161ck;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class MessageRequestsHeaderInboxItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44J();
    public final int A00;

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeInt(this.A00);
    }

    public MessageRequestsHeaderInboxItem(C27161ck r1, int i) {
        super(r1);
        this.A00 = i;
    }

    public MessageRequestsHeaderInboxItem(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
    }
}
