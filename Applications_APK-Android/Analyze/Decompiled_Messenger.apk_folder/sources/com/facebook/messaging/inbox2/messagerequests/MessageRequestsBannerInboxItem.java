package com.facebook.messaging.inbox2.messagerequests;

import X.AnonymousClass44I;
import X.C27161ck;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet;

public final class MessageRequestsBannerInboxItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44I();
    public final MessageRequestsSnippet A00;
    public final String A01;

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeParcelable(this.A00, i);
    }

    public MessageRequestsBannerInboxItem(C27161ck r1, String str, MessageRequestsSnippet messageRequestsSnippet) {
        super(r1);
        this.A01 = str;
        this.A00 = messageRequestsSnippet;
    }

    public MessageRequestsBannerInboxItem(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        this.A00 = (MessageRequestsSnippet) parcel.readParcelable(MessageRequestsSnippet.class.getClassLoader());
    }
}
