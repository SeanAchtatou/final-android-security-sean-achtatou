package com.facebook.messaging.inbox2.morefooter;

import X.AnonymousClass44K;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class InboxUnitSeeAllItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44K();
    public final int A00;

    public InboxUnitSeeAllItem(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i) {
        A0H(parcel, i);
        parcel.writeInt(this.A00);
    }
}
