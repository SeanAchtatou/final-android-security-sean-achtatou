package com.facebook.messaging.inbox2.sectionheader;

import X.AnonymousClass79G;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class InboxUnitSectionHeaderItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass79G();
    public final int A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;

    public InboxUnitSectionHeaderItem(Parcel parcel) {
        super(parcel);
        this.A03 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A01 = parcel.readString();
        this.A05 = C417826y.A0W(parcel);
        this.A06 = C417826y.A0W(parcel);
        this.A07 = C417826y.A0W(parcel);
        this.A04 = parcel.readString();
        this.A02 = parcel.readString();
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeString(this.A03);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A01);
        C417826y.A0V(parcel, this.A05);
        C417826y.A0V(parcel, this.A06);
        C417826y.A0V(parcel, this.A07);
        parcel.writeString(this.A04);
        parcel.writeString(this.A02);
    }
}
