package com.facebook.messaging.inbox2.data.graphql;

import X.AnonymousClass0UN;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C05540Zi;
import X.C05850aR;
import X.C07900eM;
import X.C21899AkD;
import X.C26931cN;
import X.C27161ck;
import X.C82583wD;
import android.net.NetworkInfo;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000;
import com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;

@UserScoped
public final class InboxUnitGraphQLQueryExecutorHelper {
    private static C05540Zi A02;
    private AnonymousClass0UN A00;
    private final C07900eM A01;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, int]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI */
    public static C21899AkD A00(C27161ck r6, GSTModelShape1S0000000 gSTModelShape1S0000000) {
        C82583wD r4 = new C82583wD();
        GQLCallInputCInputShape1S0000000 gQLCallInputCInputShape1S0000000 = new GQLCallInputCInputShape1S0000000(AnonymousClass1Y3.A1C);
        gQLCallInputCInputShape1S0000000.A09("action_type", "HIDE");
        String A3m = gSTModelShape1S0000000.A3m();
        gQLCallInputCInputShape1S0000000.A09("item_id", A3m);
        gQLCallInputCInputShape1S0000000.A09("item_logging_data", r6.A0P(392918208));
        r4.A04("input", gQLCallInputCInputShape1S0000000);
        GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MessengerInboxItem", GSMBuilderShape0S0000000.class, -923939885);
        gSMBuilderShape0S0000000.A0I(A3m);
        gSMBuilderShape0S0000000.setInt("messenger_inbox_item_clicks_remaining", Integer.valueOf(gSTModelShape1S0000000.getIntValue(1050673705)));
        gSMBuilderShape0S0000000.setInt("messenger_inbox_item_hides_remaining", (Integer) 0);
        C21899AkD A012 = C26931cN.A01(r4);
        A012.A00((GSTModelShape1S0000000) gSMBuilderShape0S0000000.getResult(GSTModelShape1S0000000.class, -923939885));
        return A012;
    }

    public static final InboxUnitGraphQLQueryExecutorHelper A01(AnonymousClass1XY r4) {
        InboxUnitGraphQLQueryExecutorHelper inboxUnitGraphQLQueryExecutorHelper;
        synchronized (InboxUnitGraphQLQueryExecutorHelper.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new InboxUnitGraphQLQueryExecutorHelper((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                inboxUnitGraphQLQueryExecutorHelper = (InboxUnitGraphQLQueryExecutorHelper) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return inboxUnitGraphQLQueryExecutorHelper;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x00e8, code lost:
        if (r9.equals("MESSENGER_DISCOVERY_BOTS") == false) goto L_0x00ea;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.graphql.query.GQSQStringShape0S0000000_I0 A02(java.lang.String r9) {
        /*
            r8 = this;
            X.0eM r3 = r8.A01
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            int r1 = X.AnonymousClass1Y3.BHQ
            X.0UN r0 = r3.A00
            r4 = 7
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1cI r0 = (X.C26881cI) r0
            int r0 = r0.A0A()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "montage_nux_image_width"
            r2.put(r0, r1)
            int r1 = X.AnonymousClass1Y3.BHQ
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1cI r0 = (X.C26881cI) r0
            int r0 = r0.A08()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "montage_nux_image_height"
            r2.put(r0, r1)
            com.facebook.graphql.querybuilder.common.ScaleInputPixelRatio r1 = X.C26901cK.A03()
            r0 = 218(0xda, float:3.05E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.put(r0, r1)
            int r4 = X.AnonymousClass1Y3.ARr
            X.0UN r1 = r3.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r4, r1)
            X.0kz r1 = (X.C10870kz) r1
            r0 = 80
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r0 = r1.A01(r0)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            r0 = 742(0x2e6, float:1.04E-42)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.put(r0, r1)
            r0 = 3
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "friend_count"
            r2.put(r0, r1)
            android.content.res.Resources r1 = r3.A01
            r0 = 2132148529(0x7f160131, float:1.9939039E38)
            int r0 = r1.getDimensionPixelSize(r0)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "suggested_room_image_width"
            r2.put(r0, r1)
            java.lang.String r0 = "suggested_room_image_height"
            r2.put(r0, r1)
            android.content.res.Resources r1 = r3.A01
            r0 = 2132148269(0x7f16002d, float:1.9938511E38)
            int r0 = r1.getDimensionPixelSize(r0)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "page_pic_size"
            r2.put(r0, r1)
            X.1ac r0 = r3.A02
            java.lang.String r1 = r0.B7Z()
            java.lang.String r0 = "deviceId"
            r2.put(r0, r1)
            java.lang.String r1 = "specific_units"
            r0 = 0
            r2.put(r1, r0)
            com.facebook.graphql.query.GQSQStringShape0S0000000_I0 r5 = new com.facebook.graphql.query.GQSQStringShape0S0000000_I0
            r0 = 2
            r5.<init>(r0)
            java.util.Set r0 = r2.entrySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x00b7:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00d1
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.getValue()
            r5.A08(r1, r0)
            goto L_0x00b7
        L_0x00d1:
            X.0eM r3 = r8.A01
            X.04a r4 = new X.04a
            r4.<init>()
            int r1 = r9.hashCode()
            r0 = -1413460057(0xffffffffabc04fa7, float:-1.3664528E-12)
            if (r1 != r0) goto L_0x00ea
            java.lang.String r0 = "MESSENGER_DISCOVERY_BOTS"
            boolean r0 = r9.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x00eb
        L_0x00ea:
            r1 = -1
        L_0x00eb:
            if (r1 != 0) goto L_0x0157
            int r2 = X.AnonymousClass1Y3.AjZ
            X.0UN r1 = r3.A00
            r0 = 9
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.6ob r7 = (X.C144836ob) r7
            X.25O r2 = r7.A01
            long r0 = r7.A00
            X.8pw r6 = r2.A02(r0)
            if (r6 == 0) goto L_0x012b
            com.facebook.prefs.shared.FbSharedPreferences r0 = r7.A03
            X.1hn r3 = r0.edit()
            X.1Y7 r2 = X.AnonymousClass4V0.A01
            android.location.Location r0 = r6.A00
            double r0 = r0.getLatitude()
            r3.Bz4(r2, r0)
            r3.commit()
            com.facebook.prefs.shared.FbSharedPreferences r0 = r7.A03
            X.1hn r3 = r0.edit()
            X.1Y7 r2 = X.AnonymousClass4V0.A02
            android.location.Location r0 = r6.A00
            double r0 = r0.getLongitude()
            r3.Bz4(r2, r0)
            r3.commit()
        L_0x012b:
            if (r6 == 0) goto L_0x0157
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r2 = new com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000
            r0 = 47
            r2.<init>(r0)
            android.location.Location r0 = r6.A00
            double r0 = r0.getLatitude()
            java.lang.Double r1 = java.lang.Double.valueOf(r0)
            java.lang.String r0 = "latitude"
            r2.A07(r0, r1)
            android.location.Location r0 = r6.A00
            double r0 = r0.getLongitude()
            java.lang.Double r1 = java.lang.Double.valueOf(r0)
            java.lang.String r0 = "longitude"
            r2.A07(r0, r1)
            java.lang.String r0 = "viewerLocation"
            r4.put(r0, r2)
        L_0x0157:
            java.util.Set r0 = r4.entrySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x015f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0179
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.getValue()
            r5.A08(r1, r0)
            goto L_0x015f
        L_0x0179:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper.A02(java.lang.String):com.facebook.graphql.query.GQSQStringShape0S0000000_I0");
    }

    public boolean A03() {
        NetworkInfo networkInfo = (NetworkInfo) AnonymousClass1XX.A03(AnonymousClass1Y3.A8e, this.A00);
        if (networkInfo == null || !networkInfo.isConnected() || !networkInfo.isAvailable() || networkInfo.getType() != 1) {
            return false;
        }
        return true;
    }

    private InboxUnitGraphQLQueryExecutorHelper(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = C07900eM.A00(r3);
    }
}
