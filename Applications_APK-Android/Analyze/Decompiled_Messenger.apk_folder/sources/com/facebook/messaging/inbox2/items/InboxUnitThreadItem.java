package com.facebook.messaging.inbox2.items;

import X.AnonymousClass08S;
import X.AnonymousClass1H4;
import X.C21361Gm;
import X.C23761Qt;
import X.C27161ck;
import X.C33311nN;
import X.C33321nO;
import X.C34001oU;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem;

public final class InboxUnitThreadItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new C33311nN();
    public final C33321nO A00;
    public final ThreadSummary A01;
    public final BasicMontageThreadInfo A02;
    public final UnifiedPresenceViewLoggerItem A03;
    public final C23761Qt A04;
    public final AnonymousClass1H4 A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;
    private final int A0I;

    public static void A01(StringBuilder sb, C21361Gm[] r5, String str) {
        if (r5 != null) {
            for (C21361Gm r1 : r5) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(str);
                sb.append(r1.mLoggingTag);
            }
        }
    }

    public String A0G() {
        GSTModelShape1S0000000 gSTModelShape1S0000000 = this.A01;
        if (gSTModelShape1S0000000 != null) {
            return gSTModelShape1S0000000.A3m();
        }
        return AnonymousClass08S.A0O(this.A02.A0U(), ":", this.A01.A0S.A0G());
    }

    public boolean A0I() {
        ThreadKey threadKey = this.A01.A0S;
        if (threadKey.equals(C34001oU.A00) || ThreadKey.A0E(threadKey) || !this.A0B) {
            return false;
        }
        return true;
    }

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A02, i);
        C417826y.A0L(parcel, this.A00);
        C417826y.A0V(parcel, this.A0C);
        C417826y.A0V(parcel, this.A0A);
        C417826y.A0V(parcel, this.A0B);
        C417826y.A0V(parcel, this.A0G);
        parcel.writeInt(this.A0I);
        C417826y.A0V(parcel, this.A0F);
        C417826y.A0V(parcel, this.A09);
        C417826y.A0V(parcel, this.A0H);
        C417826y.A0V(parcel, this.A0E);
        parcel.writeString(this.A08);
        C417826y.A0V(parcel, this.A0D);
    }

    public InboxUnitThreadItem(C27161ck r3, GSTModelShape1S0000000 gSTModelShape1S0000000, ThreadSummary threadSummary, BasicMontageThreadInfo basicMontageThreadInfo, AnonymousClass1H4 r7, C23761Qt r8, C33321nO r9, String str, String str2, UnifiedPresenceViewLoggerItem unifiedPresenceViewLoggerItem, boolean z, boolean z2, boolean z3, boolean z4, int i, boolean z5, boolean z6, boolean z7, boolean z8, String str3, boolean z9) {
        super(r3, gSTModelShape1S0000000, null);
        this.A01 = threadSummary;
        this.A02 = basicMontageThreadInfo;
        this.A05 = r7;
        this.A04 = r8;
        this.A06 = str;
        this.A07 = str2;
        this.A03 = unifiedPresenceViewLoggerItem;
        this.A00 = r9;
        this.A0C = z;
        this.A0A = z2;
        this.A0B = z3;
        this.A0G = z4;
        this.A0I = i;
        this.A0F = z5;
        this.A09 = z6;
        this.A0H = z7;
        this.A0E = z8;
        this.A08 = str3;
        this.A0D = z9;
    }

    public InboxUnitThreadItem(Parcel parcel) {
        super(parcel);
        this.A01 = (ThreadSummary) parcel.readParcelable(ThreadSummary.class.getClassLoader());
        this.A02 = (BasicMontageThreadInfo) parcel.readParcelable(BasicMontageThreadInfo.class.getClassLoader());
        this.A05 = null;
        this.A04 = null;
        this.A06 = null;
        this.A07 = null;
        this.A03 = (UnifiedPresenceViewLoggerItem) parcel.readParcelable(UnifiedPresenceViewLoggerItem.class.getClassLoader());
        this.A00 = (C33321nO) C417826y.A0C(parcel, C33321nO.class);
        this.A0C = C417826y.A0W(parcel);
        this.A0A = C417826y.A0W(parcel);
        this.A0B = C417826y.A0W(parcel);
        this.A0G = C417826y.A0W(parcel);
        this.A0I = parcel.readInt();
        this.A0F = C417826y.A0W(parcel);
        this.A09 = C417826y.A0W(parcel);
        this.A0H = C417826y.A0W(parcel);
        this.A0E = C417826y.A0W(parcel);
        this.A08 = parcel.readString();
        this.A0D = C417826y.A0W(parcel);
    }
}
