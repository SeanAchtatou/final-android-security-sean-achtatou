package com.facebook.messaging.inbox2.analytics;

import X.AnonymousClass07B;
import X.AnonymousClass21J;
import X.C06850cB;
import X.C20191Bg;
import X.C32861mS;
import android.os.Parcel;
import android.os.Parcelable;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Objects;

public final class InboxSourceLoggingData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C32861mS();
    public final Integer A00;
    public final String A01;
    public final String A02;

    public int describeContents() {
        return 0;
    }

    public static void A00(ObjectNode objectNode, InboxSourceLoggingData inboxSourceLoggingData) {
        objectNode.put("st", inboxSourceLoggingData.A02);
        if (!C06850cB.A0B(inboxSourceLoggingData.A01)) {
            objectNode.put("ci", inboxSourceLoggingData.A01);
        }
        Integer num = AnonymousClass07B.A00;
        Integer num2 = inboxSourceLoggingData.A00;
        if (!num.equals(num2)) {
            objectNode.put("src", AnonymousClass21J.A00(num2));
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(AnonymousClass21J.A00(this.A00));
        parcel.writeString(this.A01);
    }

    public InboxSourceLoggingData(C20191Bg r2) {
        this.A02 = r2.A02;
        this.A00 = r2.A00;
        this.A01 = r2.A01;
    }

    public InboxSourceLoggingData(Parcel parcel) {
        Integer num;
        this.A02 = parcel.readString();
        String readString = parcel.readString();
        Integer[] A002 = AnonymousClass07B.A00(12);
        int length = A002.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                num = null;
                break;
            }
            num = A002[i];
            if (Objects.equal(AnonymousClass21J.A00(num), readString)) {
                break;
            }
            i++;
        }
        this.A00 = num;
        this.A01 = parcel.readString();
    }
}
