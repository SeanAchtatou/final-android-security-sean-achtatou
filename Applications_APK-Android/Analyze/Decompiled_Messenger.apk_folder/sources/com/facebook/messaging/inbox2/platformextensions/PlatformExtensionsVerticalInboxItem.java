package com.facebook.messaging.inbox2.platformextensions;

import X.AnonymousClass44F;
import X.C27161ck;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class PlatformExtensionsVerticalInboxItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44F();
    public final InboxPlatformExtensionsBasicData A00;

    public void writeToParcel(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeParcelable(this.A00, i);
    }

    public PlatformExtensionsVerticalInboxItem(C27161ck r2, GSTModelShape1S0000000 gSTModelShape1S0000000, InboxPlatformExtensionsBasicData inboxPlatformExtensionsBasicData) {
        super(r2, gSTModelShape1S0000000, null);
        this.A00 = inboxPlatformExtensionsBasicData;
    }

    public PlatformExtensionsVerticalInboxItem(Parcel parcel) {
        super(parcel);
        this.A00 = (InboxPlatformExtensionsBasicData) parcel.readParcelable(InboxPlatformExtensionsBasicData.class.getClassLoader());
    }
}
