package com.facebook.messaging.inbox2.sectionheader;

import X.AnonymousClass44L;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class NonInboxServiceSectionHeaderItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44L();
    public final String A00;
    public final String A01;
    public final boolean A02;

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
        C417826y.A0V(parcel, this.A02);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NonInboxServiceSectionHeaderItem(X.C27161ck r3, java.lang.String r4, java.lang.String r5, boolean r6) {
        /*
            r2 = this;
            X.1GH r1 = X.AnonymousClass1GH.A02
            r0 = 0
            r2.<init>(r3, r0, r1)
            com.google.common.base.Preconditions.checkNotNull(r1)
            r2.A01 = r4
            r2.A00 = r5
            r2.A02 = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem.<init>(X.1ck, java.lang.String, java.lang.String, boolean):void");
    }

    public NonInboxServiceSectionHeaderItem(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
        this.A02 = C417826y.A0W(parcel);
    }
}
