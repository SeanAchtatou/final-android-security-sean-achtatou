package com.facebook.messaging.messengerprefs.tincan;

import X.AnonymousClass1XX;
import X.AnonymousClass51L;
import X.AnonymousClass51O;
import X.C000700l;
import X.C12570pc;
import X.C16290wo;
import X.C67803Qg;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.facebook.resources.ui.FbFrameLayout;

public class TincanNuxFragment extends C12570pc {
    public C67803Qg A00;

    public void A1h(Bundle bundle) {
        int A02 = C000700l.A02(731520960);
        super.A1h(bundle);
        this.A00 = C67803Qg.A00(AnonymousClass1XX.get(A1j()));
        A23(2, 2132476937);
        C000700l.A08(-815298157, A02);
    }

    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int A02 = C000700l.A02(640968647);
        FbFrameLayout fbFrameLayout = new FbFrameLayout(A1j());
        fbFrameLayout.setId(16908290);
        C000700l.A08(2121453889, A02);
        return fbFrameLayout;
    }

    public void A1v(View view, Bundle bundle) {
        AnonymousClass51L r2;
        super.A1v(view, bundle);
        Fragment A0Q = A17().A0Q("M4TincanNuxFragment");
        if (A0Q == null) {
            r2 = new AnonymousClass51L();
        } else {
            r2 = (AnonymousClass51L) A0Q;
        }
        r2.A01 = new AnonymousClass51O(this);
        Dialog dialog = this.A09;
        if (dialog != null) {
            r2.A00 = dialog.getWindow();
        }
        if (A0Q == null) {
            C16290wo A0T = A17().A0T();
            A0T.A0A(16908290, r2, "M4TincanNuxFragment");
            A0T.A02();
        }
    }
}
