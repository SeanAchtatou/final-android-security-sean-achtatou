package com.facebook.messaging.sms.defaultapp;

import X.AnonymousClass08S;
import X.AnonymousClass0TE;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import com.google.common.base.Preconditions;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class MmsFileProvider extends AnonymousClass0TE {
    public static final UriMatcher A00 = new UriMatcher(-1);
    private static final Random A01 = new Random();

    public static Uri A00() {
        return new Uri.Builder().authority("com.facebook.messaging.sms.MmsFileProvider").scheme("content").path(String.valueOf(Math.abs(A01.nextLong()))).build();
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:156:0x03a7 */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x03c2 A[SYNTHETIC, Splitter:B:170:0x03c2] */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x03cd A[SYNTHETIC, Splitter:B:175:0x03cd] */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x03d4 A[SYNTHETIC, Splitter:B:179:0x03d4] */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x03f1 A[Catch:{ all -> 0x0420 }] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0426 A[SYNTHETIC, Splitter:B:205:0x0426] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:195:0x0402=Splitter:B:195:0x0402, B:189:0x03eb=Splitter:B:189:0x03eb} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.net.Uri A01(android.content.Context r18, X.C196849Nr r19) {
        /*
            java.lang.String r8 = "MmsFileProvider"
            android.net.Uri r7 = A00()
            r1 = r18
            java.io.File r12 = A02(r1, r7)
            r3 = 0
            java.io.BufferedOutputStream r6 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0401, OutOfMemoryError -> 0x03ea }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0401, OutOfMemoryError -> 0x03ea }
            r0.<init>(r12)     // Catch:{ IOException -> 0x0401, OutOfMemoryError -> 0x03ea }
            r6.<init>(r0)     // Catch:{ IOException -> 0x0401, OutOfMemoryError -> 0x03ea }
            X.9Nc r5 = new X.9Nc     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = r19
            r5.<init>(r1, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Nr r0 = r5.A01     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ne r1 = r0.A00     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 140(0x8c, float:1.96E-43)
            int r1 = r1.A00(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 128(0x80, float:1.794E-43)
            if (r1 == r0) goto L_0x013b
            r0 = 133(0x85, float:1.86E-43)
            if (r1 == r0) goto L_0x010c
            r0 = 135(0x87, float:1.89E-43)
            if (r1 == r0) goto L_0x00c5
            r0 = 130(0x82, float:1.82E-43)
            if (r1 == r0) goto L_0x006e
            r0 = 131(0x83, float:1.84E-43)
            if (r1 != r0) goto L_0x03c8
            java.io.ByteArrayOutputStream r0 = r5.A04     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r1 = 0
            if (r0 != 0) goto L_0x004a
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.<init>()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A04 = r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A00 = r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x004a:
            r0 = 140(0x8c, float:1.96E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 131(0x83, float:1.84E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 152(0x98, float:2.13E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 141(0x8d, float:1.98E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 149(0x95, float:2.09E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == 0) goto L_0x03bb
            goto L_0x03bd
        L_0x006e:
            java.io.ByteArrayOutputStream r0 = r5.A04     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r1 = 0
            if (r0 != 0) goto L_0x007c
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.<init>()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A04 = r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A00 = r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x007c:
            r0 = 140(0x8c, float:1.96E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 130(0x82, float:1.82E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 152(0x98, float:2.13E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 141(0x8d, float:1.98E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 137(0x89, float:1.92E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 150(0x96, float:2.1E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 138(0x8a, float:1.93E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 142(0x8e, float:1.99E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 136(0x88, float:1.9E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 131(0x83, float:1.84E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == 0) goto L_0x03bb
            goto L_0x03bd
        L_0x00c5:
            java.io.ByteArrayOutputStream r0 = r5.A04     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r1 = 0
            if (r0 != 0) goto L_0x00d3
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.<init>()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A04 = r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A00 = r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x00d3:
            r0 = 140(0x8c, float:1.96E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 135(0x87, float:1.89E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 141(0x8d, float:1.98E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 139(0x8b, float:1.95E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 151(0x97, float:2.12E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 137(0x89, float:1.92E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 133(0x85, float:1.86E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 155(0x9b, float:2.17E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == 0) goto L_0x03bb
            goto L_0x03bd
        L_0x010c:
            java.io.ByteArrayOutputStream r0 = r5.A04     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r1 = 0
            if (r0 != 0) goto L_0x011a
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.<init>()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A04 = r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A00 = r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x011a:
            r0 = 140(0x8c, float:1.96E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 133(0x85, float:1.86E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 152(0x98, float:2.13E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 141(0x8d, float:1.98E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 145(0x91, float:2.03E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            goto L_0x03bb
        L_0x013b:
            java.io.ByteArrayOutputStream r0 = r5.A04     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r2 = 0
            if (r0 != 0) goto L_0x0149
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.<init>()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A04 = r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A00 = r2     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x0149:
            r0 = 140(0x8c, float:1.96E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 128(0x80, float:1.794E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r1 = 152(0x98, float:2.13E-43)
            X.C196709Nc.A04(r5, r1)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ne r0 = r5.A03     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r0 = r0.A08(r1)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == 0) goto L_0x03dc
            X.C196709Nc.A07(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 141(0x8d, float:1.98E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r1 = 1
            if (r0 != 0) goto L_0x03bd
            r0 = 133(0x85, float:1.86E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 137(0x89, float:1.92E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x03bd
            r0 = 151(0x97, float:2.12E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == r1) goto L_0x0182
            r2 = 1
        L_0x0182:
            r0 = 130(0x82, float:1.82E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == r1) goto L_0x018b
            r2 = 1
        L_0x018b:
            r0 = 129(0x81, float:1.81E-43)
            int r0 = X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == r1) goto L_0x0194
            r2 = 1
        L_0x0194:
            if (r2 == 0) goto L_0x03bd
            r0 = 150(0x96, float:2.1E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 138(0x8a, float:1.93E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 136(0x88, float:1.9E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 143(0x8f, float:2.0E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 134(0x86, float:1.88E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 144(0x90, float:2.02E-43)
            X.C196709Nc.A00(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 132(0x84, float:1.85E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.String r19 = ">"
            java.lang.String r18 = "<"
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A02()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Nq r13 = r0.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ne r1 = r5.A03     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 132(0x84, float:1.85E-43)
            byte[] r0 = r1.A08(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.04b r0 = X.C196709Nc.A06     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r17 = 1
            if (r0 == 0) goto L_0x03bd
            int r0 = r0.intValue()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = r0 | 128(0x80, float:1.794E-43)
            r0 = r0 & 255(0xff, float:3.57E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Nr r0 = r5.A01     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Nf r0 = (X.C196729Nf) r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9NH r4 = r0.A00     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r3 = 0
            if (r4 == 0) goto L_0x03ae
            java.util.Vector r0 = r4.A00     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            int r0 = r0.size()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == 0) goto L_0x03ae
            r11 = 62
            r2 = 60
            X.9KT r10 = r4.A00(r3)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            android.util.SparseArray r1 = r10.A01     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            r0 = 192(0xc0, float:2.69E-43)
            java.lang.Object r1 = r1.get(r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            byte[] r1 = (byte[]) r1     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            if (r1 == 0) goto L_0x0224
            r0 = 138(0x8a, float:1.93E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            byte r0 = r1[r3]     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            if (r2 != r0) goto L_0x0231
            int r0 = r1.length     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            int r0 = r0 - r17
            byte r0 = r1[r0]     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            if (r11 != r0) goto L_0x0231
            X.C196709Nc.A07(r5, r1)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
        L_0x0224:
            r0 = 137(0x89, float:1.92E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            byte[] r0 = r10.A06()     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            X.C196709Nc.A07(r5, r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            goto L_0x024a
        L_0x0231:
            java.lang.String r9 = new java.lang.String     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            r9.<init>(r1)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            r1 = r18
            r0 = r19
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r9, r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            byte[] r0 = r0.getBytes()     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            X.C196709Nc.A07(r5, r0)     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0246 }
            goto L_0x0224
        L_0x0246:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x024a:
            int r1 = r13.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A03()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            long r0 = (long) r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.C196709Nc.A06(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A01()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.util.Vector r0 = r4.A00     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            int r9 = r0.size()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            long r0 = (long) r9     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.C196709Nc.A05(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r10 = 0
        L_0x0267:
            if (r10 >= r9) goto L_0x03bb
            X.9KT r13 = r4.A00(r10)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A02()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Nq r16 = r0.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A02()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Nq r15 = r0.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r14 = r13.A06()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r14 == 0) goto L_0x03bd
            X.04b r1 = X.C196709Nc.A06     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.String r0 = new java.lang.String     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.<init>(r14)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 != 0) goto L_0x0386
            X.C196709Nc.A07(r5, r14)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x0299:
            byte[] r1 = r13.A07()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r1 != 0) goto L_0x02bd
            android.util.SparseArray r1 = r13.A01     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 152(0x98, float:2.13E-43)
            java.lang.Object r1 = r1.get(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r1 = (byte[]) r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r1 != 0) goto L_0x02bd
            android.util.SparseArray r1 = r13.A01     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 142(0x8e, float:1.99E-43)
            java.lang.Object r1 = r1.get(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r1 = (byte[]) r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r1 != 0) goto L_0x02bd
            java.lang.String r0 = "smil.xml"
            byte[] r1 = r0.getBytes()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x02bd:
            r0 = 133(0x85, float:1.86E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.C196709Nc.A07(r5, r1)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            int r14 = r13.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r14 == 0) goto L_0x02d7
            r0 = 129(0x81, float:1.81E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = r14 | 128(0x80, float:1.794E-43)
            r0 = r0 & 255(0xff, float:3.57E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x02d7:
            int r1 = r15.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A03()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            long r0 = (long) r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.C196709Nc.A06(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A01()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            android.util.SparseArray r1 = r13.A01     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 192(0xc0, float:2.69E-43)
            java.lang.Object r1 = r1.get(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r1 = (byte[]) r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r1 == 0) goto L_0x030e
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte r0 = r1[r3]     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r2 != r0) goto L_0x036b
            int r14 = r1.length     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            int r0 = r14 - r17
            byte r0 = r1[r0]     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r11 != r0) goto L_0x036b
            r0 = 34
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A08(r1, r3, r14)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x030b:
            X.C196709Nc.A04(r5, r3)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x030e:
            android.util.SparseArray r1 = r13.A01     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 142(0x8e, float:1.99E-43)
            java.lang.Object r1 = r1.get(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r1 = (byte[]) r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r1 == 0) goto L_0x0320
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.C196709Nc.A07(r5, r1)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x0320:
            int r14 = r16.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r0 = r13.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            if (r0 == 0) goto L_0x0346
            int r13 = r0.length     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A08(r0, r3, r13)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x032c:
            int r0 = r16.A00()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            int r0 = r0 - r14
            if (r13 != r0) goto L_0x0399
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A03()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            long r0 = (long) r14     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.C196709Nc.A05(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            long r0 = (long) r13     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.C196709Nc.A05(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A01()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            goto L_0x0393
        L_0x0346:
            r11 = 0
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r0]     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            android.content.ContentResolver r15 = r5.A05     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            android.net.Uri r0 = r13.A00     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            java.io.InputStream r11 = r15.openInputStream(r0)     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            r13 = 0
        L_0x0354:
            int r15 = r11.read(r1)     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            r0 = -1
            if (r15 == r0) goto L_0x0367
            java.io.ByteArrayOutputStream r0 = r5.A04     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            r0.write(r1, r3, r15)     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            int r0 = r5.A00     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            int r0 = r0 + r15
            r5.A00 = r0     // Catch:{ FileNotFoundException | IOException | RuntimeException -> 0x03a8, all -> 0x03a1 }
            int r13 = r13 + r15
            goto L_0x0354
        L_0x0367:
            r11.close()     // Catch:{ IOException -> 0x032c }
            goto L_0x032c
        L_0x036b:
            java.lang.String r11 = new java.lang.String     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r11.<init>(r1)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r1 = r18
            r0 = r19
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r11, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            byte[] r11 = r0.getBytes()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = 34
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            int r1 = r11.length     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r5.A08(r11, r3, r1)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            goto L_0x030b
        L_0x0386:
            int r0 = r0.intValue()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0 = r0 | 128(0x80, float:1.794E-43)
            r0 = r0 & 255(0xff, float:3.57E-43)
            X.C196709Nc.A04(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            goto L_0x0299
        L_0x0393:
            int r10 = r10 + 1
            r11 = 62
            goto L_0x0267
        L_0x0399:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.String r0 = "BUG: Length sanity check failed"
            r1.<init>(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            throw r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x03a1:
            r0 = move-exception
            if (r11 == 0) goto L_0x03a7
            r11.close()     // Catch:{ IOException -> 0x03a7 }
        L_0x03a7:
            throw r0     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x03a8:
            if (r11 == 0) goto L_0x03bf
            r11.close()     // Catch:{ IOException -> 0x03bf }
            goto L_0x03bf
        L_0x03ae:
            r0 = 0
            X.C196709Nc.A05(r5, r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            X.9Ng r0 = r5.A02     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A03()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.A01()     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x03bb:
            r0 = 0
            goto L_0x03c0
        L_0x03bd:
            r0 = 1
            goto L_0x03c0
        L_0x03bf:
            r0 = 1
        L_0x03c0:
            if (r0 != 0) goto L_0x03c8
            java.io.ByteArrayOutputStream r0 = r5.A04     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            r0.writeTo(r6)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            goto L_0x03ca
        L_0x03c8:
            r0 = 0
            goto L_0x03cb
        L_0x03ca:
            r0 = 1
        L_0x03cb:
            if (r0 == 0) goto L_0x03d4
            r6.flush()     // Catch:{ IOException -> 0x03d3 }
            r6.close()     // Catch:{ IOException -> 0x03d3 }
        L_0x03d3:
            return r7
        L_0x03d4:
            X.9N8 r1 = new X.9N8     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.String r0 = "Failure writing pdu to file"
            r1.<init>(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            throw r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x03dc:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            java.lang.String r0 = "Transaction-ID is null."
            r1.<init>(r0)     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
            throw r1     // Catch:{ IOException -> 0x03e7, OutOfMemoryError -> 0x03e4, all -> 0x0422 }
        L_0x03e4:
            r1 = move-exception
            r3 = r6
            goto L_0x03eb
        L_0x03e7:
            r2 = move-exception
            r3 = r6
            goto L_0x0402
        L_0x03ea:
            r1 = move-exception
        L_0x03eb:
            boolean r0 = r12.exists()     // Catch:{ all -> 0x0420 }
            if (r0 == 0) goto L_0x03f4
            r12.delete()     // Catch:{ all -> 0x0420 }
        L_0x03f4:
            java.lang.String r0 = "Out of memory in composing PDU"
            X.C010708t.A0L(r8, r0, r1)     // Catch:{ all -> 0x0420 }
            X.9N8 r1 = new X.9N8     // Catch:{ all -> 0x0420 }
            java.lang.String r0 = "Cannot create raw mms file because out of memory"
            r1.<init>(r0)     // Catch:{ all -> 0x0420 }
            goto L_0x041f
        L_0x0401:
            r2 = move-exception
        L_0x0402:
            boolean r0 = r12.exists()     // Catch:{ all -> 0x0420 }
            if (r0 == 0) goto L_0x040b
            r12.delete()     // Catch:{ all -> 0x0420 }
        L_0x040b:
            java.lang.String r1 = "Cannot create temporary file %s"
            java.lang.String r0 = r12.getAbsolutePath()     // Catch:{ all -> 0x0420 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x0420 }
            X.C010708t.A0U(r8, r2, r1, r0)     // Catch:{ all -> 0x0420 }
            X.9N8 r1 = new X.9N8     // Catch:{ all -> 0x0420 }
            java.lang.String r0 = "Cannot create raw mms file"
            r1.<init>(r0)     // Catch:{ all -> 0x0420 }
        L_0x041f:
            throw r1     // Catch:{ all -> 0x0420 }
        L_0x0420:
            r0 = move-exception
            goto L_0x0424
        L_0x0422:
            r0 = move-exception
            r3 = r6
        L_0x0424:
            if (r3 == 0) goto L_0x042c
            r3.flush()     // Catch:{ IOException -> 0x042c }
            r3.close()     // Catch:{ IOException -> 0x042c }
        L_0x042c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.sms.defaultapp.MmsFileProvider.A01(android.content.Context, X.9Nr):android.net.Uri");
    }

    public static File A02(Context context, Uri uri) {
        boolean z;
        File file = new File(context.getCacheDir(), "rawmms");
        file.mkdirs();
        File file2 = new File(file, AnonymousClass08S.A0J(uri.getPath(), ".mms"));
        try {
            File canonicalFile = file.getCanonicalFile();
            File canonicalFile2 = file2.getCanonicalFile();
            while (true) {
                if (canonicalFile2 == null) {
                    z = false;
                    break;
                } else if (canonicalFile2.equals(canonicalFile)) {
                    z = true;
                    break;
                } else {
                    canonicalFile2 = canonicalFile2.getParentFile();
                }
            }
            Preconditions.checkArgument(z);
            return file2;
        } catch (IOException e) {
            throw new RuntimeException("Error checking paths", e);
        }
    }

    public void A0G() {
        A00.addURI("com.facebook.messaging.sms.MmsFileProvider", "#", 1);
    }
}
