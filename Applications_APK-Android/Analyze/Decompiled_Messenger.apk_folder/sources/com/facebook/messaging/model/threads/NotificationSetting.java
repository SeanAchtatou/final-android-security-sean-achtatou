package com.facebook.messaging.model.threads;

import X.AnonymousClass07B;
import X.C17940zk;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.MoreObjects;

public final class NotificationSetting implements Parcelable {
    public static final NotificationSetting A04 = new NotificationSetting(true, 0, false, true);
    public static final NotificationSetting A05 = new NotificationSetting(false, 0, false, false);
    public static final NotificationSetting A06 = new NotificationSetting(true, 0, false, false);
    public static final Parcelable.Creator CREATOR = new C17940zk();
    public final long A00;
    public final boolean A01;
    public final boolean A02;
    public final boolean A03;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                NotificationSetting notificationSetting = (NotificationSetting) obj;
                if (!(this.A01 == notificationSetting.A01 && this.A03 == notificationSetting.A03 && this.A00 == notificationSetting.A00 && this.A02 == notificationSetting.A02)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public static NotificationSetting A00(long j) {
        long j2 = j;
        if (j == -1) {
            return A05;
        }
        if (j == -2) {
            return A04;
        }
        if (j == 0) {
            return A06;
        }
        if (j < 0) {
            return new NotificationSetting(true, -j, true, false);
        }
        return new NotificationSetting(true, j2, false, false);
    }

    public long A01() {
        if (!this.A03) {
            return -1;
        }
        if (this.A02) {
            return -2;
        }
        return this.A00;
    }

    public Integer A02() {
        if (!this.A03) {
            return AnonymousClass07B.A01;
        }
        if (this.A00 > System.currentTimeMillis() / 1000) {
            return AnonymousClass07B.A0C;
        }
        return AnonymousClass07B.A00;
    }

    public int hashCode() {
        long j = this.A00;
        return ((((((this.A03 ? 1 : 0) * true) + ((int) (j ^ (j >>> 32)))) * 31) + (this.A01 ? 1 : 0)) * 31) + (this.A02 ? 1 : 0);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A03 ? 1 : 0);
        parcel.writeLong(this.A00);
        parcel.writeInt(this.A01 ? 1 : 0);
        parcel.writeInt(this.A02 ? 1 : 0);
    }

    public boolean A03() {
        if (A02() == AnonymousClass07B.A00) {
            return true;
        }
        return false;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("enabled", this.A03);
        stringHelper.add("mutedUntilSeconds", this.A00);
        stringHelper.add("automaticallyMuted", this.A01);
        stringHelper.add("chatheadDisabled", this.A02);
        return stringHelper.toString();
    }

    public NotificationSetting(Parcel parcel) {
        boolean z = true;
        this.A03 = parcel.readInt() != 0;
        this.A00 = parcel.readLong();
        this.A01 = parcel.readInt() != 0;
        this.A02 = parcel.readInt() == 0 ? false : z;
    }

    public NotificationSetting(boolean z, long j, boolean z2, boolean z3) {
        this.A03 = z;
        this.A00 = j;
        this.A01 = z2;
        this.A02 = z3;
    }
}
