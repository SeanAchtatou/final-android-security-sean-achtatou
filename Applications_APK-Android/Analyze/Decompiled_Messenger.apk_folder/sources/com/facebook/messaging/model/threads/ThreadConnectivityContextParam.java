package com.facebook.messaging.model.threads;

import X.AnonymousClass0tY;
import X.C11260mU;
import X.C11710np;
import X.C14550ta;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C28931fb;
import X.C52222ii;
import X.C59992wN;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleParamIdentifier;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@JsonDeserialize(using = Deserializer.class)
@JsonSerialize(using = Serializer.class)
public final class ThreadConnectivityContextParam implements Parcelable {
    private static volatile GraphQLThreadConnectivityStatusSubtitleParamIdentifier A03;
    public static final Parcelable.Creator CREATOR = new C59992wN();
    public final String A00;
    private final GraphQLThreadConnectivityStatusSubtitleParamIdentifier A01;
    private final Set A02;

    public class Deserializer extends JsonDeserializer {
        public /* bridge */ /* synthetic */ Object deserialize(C28271eX r7, C26791c3 r8) {
            C52222ii r2 = new C52222ii();
            do {
                try {
                    if (r7.getCurrentToken() == C182811d.FIELD_NAME) {
                        String currentName = r7.getCurrentName();
                        r7.nextToken();
                        char c = 65535;
                        int hashCode = currentName.hashCode();
                        if (hashCode != 3575610) {
                            if (hashCode == 111972721 && currentName.equals("value")) {
                                c = 1;
                            }
                        } else if (currentName.equals("type")) {
                            c = 0;
                        }
                        if (c == 0) {
                            GraphQLThreadConnectivityStatusSubtitleParamIdentifier graphQLThreadConnectivityStatusSubtitleParamIdentifier = (GraphQLThreadConnectivityStatusSubtitleParamIdentifier) C14550ta.A01(GraphQLThreadConnectivityStatusSubtitleParamIdentifier.class, r7, r8);
                            r2.A00 = graphQLThreadConnectivityStatusSubtitleParamIdentifier;
                            C28931fb.A06(graphQLThreadConnectivityStatusSubtitleParamIdentifier, "type");
                            r2.A02.add("type");
                        } else if (c != 1) {
                            r7.skipChildren();
                        } else {
                            String A02 = C14550ta.A02(r7);
                            r2.A01 = A02;
                            C28931fb.A06(A02, "value");
                        }
                    }
                } catch (Exception e) {
                    C14550ta.A0D(ThreadConnectivityContextParam.class, r7, e);
                }
            } while (AnonymousClass0tY.A00(r7) != C182811d.END_OBJECT);
            return new ThreadConnectivityContextParam(r2);
        }
    }

    public class Serializer extends JsonSerializer {
        public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
            ThreadConnectivityContextParam threadConnectivityContextParam = (ThreadConnectivityContextParam) obj;
            r4.writeStartObject();
            C14550ta.A04(r4, r5, "type", threadConnectivityContextParam.A01());
            C14550ta.A0B(r4, "value", threadConnectivityContextParam.A00);
            r4.writeEndObject();
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ThreadConnectivityContextParam) {
                ThreadConnectivityContextParam threadConnectivityContextParam = (ThreadConnectivityContextParam) obj;
                if (A01() != threadConnectivityContextParam.A01() || !C28931fb.A07(this.A00, threadConnectivityContextParam.A00)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public static C52222ii A00(GraphQLThreadConnectivityStatusSubtitleParamIdentifier graphQLThreadConnectivityStatusSubtitleParamIdentifier, String str) {
        C52222ii r2 = new C52222ii();
        r2.A00 = graphQLThreadConnectivityStatusSubtitleParamIdentifier;
        C28931fb.A06(graphQLThreadConnectivityStatusSubtitleParamIdentifier, "type");
        r2.A02.add("type");
        r2.A01 = str;
        C28931fb.A06(str, "value");
        return r2;
    }

    public GraphQLThreadConnectivityStatusSubtitleParamIdentifier A01() {
        if (this.A02.contains("type")) {
            return this.A01;
        }
        if (A03 == null) {
            synchronized (this) {
                if (A03 == null) {
                    A03 = GraphQLThreadConnectivityStatusSubtitleParamIdentifier.UNSET_OR_UNRECOGNIZED_ENUM_VALUE;
                }
            }
        }
        return A03;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A01 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A01.ordinal());
        }
        parcel.writeString(this.A00);
        parcel.writeInt(this.A02.size());
        for (String writeString : this.A02) {
            parcel.writeString(writeString);
        }
    }

    public int hashCode() {
        int ordinal;
        GraphQLThreadConnectivityStatusSubtitleParamIdentifier A012 = A01();
        if (A012 == null) {
            ordinal = -1;
        } else {
            ordinal = A012.ordinal();
        }
        return C28931fb.A03(31 + ordinal, this.A00);
    }

    public ThreadConnectivityContextParam(C52222ii r3) {
        this.A01 = r3.A00;
        String str = r3.A01;
        C28931fb.A06(str, "value");
        this.A00 = str;
        this.A02 = Collections.unmodifiableSet(r3.A02);
    }

    public ThreadConnectivityContextParam(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = GraphQLThreadConnectivityStatusSubtitleParamIdentifier.values()[parcel.readInt()];
        }
        this.A00 = parcel.readString();
        HashSet hashSet = new HashSet();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            hashSet.add(parcel.readString());
        }
        this.A02 = Collections.unmodifiableSet(hashSet);
    }
}
