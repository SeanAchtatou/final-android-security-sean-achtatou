package com.facebook.messaging.model.threads;

import X.AnonymousClass0tY;
import X.AnonymousClass1Y3;
import X.C05360Yq;
import X.C11260mU;
import X.C11710np;
import X.C14540tX;
import X.C14550ta;
import X.C182811d;
import X.C24971Xv;
import X.C26791c3;
import X.C28271eX;
import X.C28931fb;
import X.C29191g1;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphql.enums.GraphQLThreadConnectivityStatus;
import com.facebook.graphql.enums.GraphQLThreadConnectivityStatusSubtitleType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@JsonDeserialize(using = Deserializer.class)
@JsonSerialize(using = Serializer.class)
public final class ThreadConnectivityData implements Parcelable {
    private static volatile GraphQLThreadConnectivityStatus A05;
    private static volatile GraphQLThreadConnectivityStatusSubtitleType A06;
    public static final Parcelable.Creator CREATOR = new C29191g1();
    public final GraphQLThreadConnectivityStatus A00;
    public final GraphQLThreadConnectivityStatusSubtitleType A01;
    public final ImmutableList A02;
    public final String A03;
    public final Set A04;

    public class Deserializer extends JsonDeserializer {
        public /* bridge */ /* synthetic */ Object deserialize(C28271eX r9, C26791c3 r10) {
            C14540tX r2 = new C14540tX();
            do {
                try {
                    if (r9.getCurrentToken() == C182811d.FIELD_NAME) {
                        String currentName = r9.getCurrentName();
                        r9.nextToken();
                        char c = 65535;
                        switch (currentName.hashCode()) {
                            case -491008490:
                                if (currentName.equals("context_params")) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case -375826566:
                                if (currentName.equals("connectivity_status")) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case 1029136534:
                                if (currentName.equals("first_sender_id")) {
                                    c = 3;
                                    break;
                                }
                                break;
                            case 1116948426:
                                if (currentName.equals("context_type")) {
                                    c = 2;
                                    break;
                                }
                                break;
                        }
                        if (c == 0) {
                            GraphQLThreadConnectivityStatus graphQLThreadConnectivityStatus = (GraphQLThreadConnectivityStatus) C14550ta.A01(GraphQLThreadConnectivityStatus.class, r9, r10);
                            r2.A00 = graphQLThreadConnectivityStatus;
                            C28931fb.A06(graphQLThreadConnectivityStatus, "connectivityStatus");
                            r2.A04.add("connectivityStatus");
                        } else if (c == 1) {
                            ImmutableList A00 = C14550ta.A00(r9, r10, ThreadConnectivityContextParam.class, null);
                            r2.A02 = A00;
                            C28931fb.A06(A00, "contextParams");
                        } else if (c == 2) {
                            GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType = (GraphQLThreadConnectivityStatusSubtitleType) C14550ta.A01(GraphQLThreadConnectivityStatusSubtitleType.class, r9, r10);
                            r2.A01 = graphQLThreadConnectivityStatusSubtitleType;
                            C28931fb.A06(graphQLThreadConnectivityStatusSubtitleType, "contextType");
                            r2.A04.add("contextType");
                        } else if (c != 3) {
                            r9.skipChildren();
                        } else {
                            String A02 = C14550ta.A02(r9);
                            r2.A03 = A02;
                            C28931fb.A06(A02, "firstSenderId");
                        }
                    }
                } catch (Exception e) {
                    C14550ta.A0D(ThreadConnectivityData.class, r9, e);
                }
            } while (AnonymousClass0tY.A00(r9) != C182811d.END_OBJECT);
            return new ThreadConnectivityData(r2);
        }
    }

    public class Serializer extends JsonSerializer {
        public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
            ThreadConnectivityData threadConnectivityData = (ThreadConnectivityData) obj;
            r4.writeStartObject();
            C14550ta.A04(r4, r5, C05360Yq.$const$string(AnonymousClass1Y3.A3U), threadConnectivityData.A00());
            C14550ta.A05(r4, r5, C05360Yq.$const$string(AnonymousClass1Y3.AB5), threadConnectivityData.A02);
            C14550ta.A04(r4, r5, C05360Yq.$const$string(31), threadConnectivityData.A01());
            C14550ta.A0B(r4, C05360Yq.$const$string(AnonymousClass1Y3.ACd), threadConnectivityData.A03);
            r4.writeEndObject();
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ThreadConnectivityData) {
                ThreadConnectivityData threadConnectivityData = (ThreadConnectivityData) obj;
                if (A00() != threadConnectivityData.A00() || !C28931fb.A07(this.A02, threadConnectivityData.A02) || A01() != threadConnectivityData.A01() || !C28931fb.A07(this.A03, threadConnectivityData.A03)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public GraphQLThreadConnectivityStatus A00() {
        if (this.A04.contains("connectivityStatus")) {
            return this.A00;
        }
        if (A05 == null) {
            synchronized (this) {
                if (A05 == null) {
                    A05 = GraphQLThreadConnectivityStatus.A01;
                }
            }
        }
        return A05;
    }

    public GraphQLThreadConnectivityStatusSubtitleType A01() {
        if (this.A04.contains("contextType")) {
            return this.A01;
        }
        if (A06 == null) {
            synchronized (this) {
                if (A06 == null) {
                    A06 = GraphQLThreadConnectivityStatusSubtitleType.A04;
                }
            }
        }
        return A06;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A00 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A00.ordinal());
        }
        parcel.writeInt(this.A02.size());
        C24971Xv it = this.A02.iterator();
        while (it.hasNext()) {
            parcel.writeParcelable((ThreadConnectivityContextParam) it.next(), i);
        }
        if (this.A01 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A01.ordinal());
        }
        parcel.writeString(this.A03);
        parcel.writeInt(this.A04.size());
        for (String writeString : this.A04) {
            parcel.writeString(writeString);
        }
    }

    public int hashCode() {
        int ordinal;
        GraphQLThreadConnectivityStatus A002 = A00();
        int i = -1;
        if (A002 == null) {
            ordinal = -1;
        } else {
            ordinal = A002.ordinal();
        }
        int A032 = C28931fb.A03(31 + ordinal, this.A02);
        GraphQLThreadConnectivityStatusSubtitleType A012 = A01();
        if (A012 != null) {
            i = A012.ordinal();
        }
        return C28931fb.A03((A032 * 31) + i, this.A03);
    }

    public ThreadConnectivityData(C14540tX r3) {
        this.A00 = r3.A00;
        ImmutableList immutableList = r3.A02;
        C28931fb.A06(immutableList, "contextParams");
        this.A02 = immutableList;
        this.A01 = r3.A01;
        String str = r3.A03;
        C28931fb.A06(str, "firstSenderId");
        this.A03 = str;
        this.A04 = Collections.unmodifiableSet(r3.A04);
    }

    public ThreadConnectivityData(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A00 = null;
        } else {
            this.A00 = GraphQLThreadConnectivityStatus.values()[parcel.readInt()];
        }
        int readInt = parcel.readInt();
        ThreadConnectivityContextParam[] threadConnectivityContextParamArr = new ThreadConnectivityContextParam[readInt];
        for (int i = 0; i < readInt; i++) {
            threadConnectivityContextParamArr[i] = (ThreadConnectivityContextParam) parcel.readParcelable(ThreadConnectivityContextParam.class.getClassLoader());
        }
        this.A02 = ImmutableList.copyOf(threadConnectivityContextParamArr);
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = GraphQLThreadConnectivityStatusSubtitleType.values()[parcel.readInt()];
        }
        this.A03 = parcel.readString();
        HashSet hashSet = new HashSet();
        int readInt2 = parcel.readInt();
        for (int i2 = 0; i2 < readInt2; i2++) {
            hashSet.add(parcel.readString());
        }
        this.A04 = Collections.unmodifiableSet(hashSet);
    }
}
