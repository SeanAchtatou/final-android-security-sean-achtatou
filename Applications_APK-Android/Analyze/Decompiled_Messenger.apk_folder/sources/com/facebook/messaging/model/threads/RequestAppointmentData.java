package com.facebook.messaging.model.threads;

import X.AnonymousClass1Lu;
import X.AnonymousClass1Lv;
import X.C28931fb;
import android.os.Parcel;
import android.os.Parcelable;

public final class RequestAppointmentData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1Lu();
    public final boolean A00;
    public final boolean A01;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof RequestAppointmentData) {
                RequestAppointmentData requestAppointmentData = (RequestAppointmentData) obj;
                if (!(this.A00 == requestAppointmentData.A00 && this.A01 == requestAppointmentData.A01)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A04(C28931fb.A04(1, this.A00), this.A01);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00 ? 1 : 0);
        parcel.writeInt(this.A01 ? 1 : 0);
    }

    public RequestAppointmentData(AnonymousClass1Lv r2) {
        this.A00 = r2.A00;
        this.A01 = r2.A01;
    }

    public RequestAppointmentData(Parcel parcel) {
        boolean z = false;
        this.A00 = parcel.readInt() == 1;
        this.A01 = parcel.readInt() == 1 ? true : z;
    }
}
