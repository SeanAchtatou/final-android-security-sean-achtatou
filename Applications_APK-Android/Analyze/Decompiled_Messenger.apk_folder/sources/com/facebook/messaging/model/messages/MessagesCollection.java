package com.facebook.messaging.model.messages;

import X.AnonymousClass08S;
import X.C010708t;
import X.C24971Xv;
import X.C28841fS;
import X.C33871oH;
import X.C33881oI;
import X.C417826y;
import X.CoM;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Arrays;
import java.util.Collection;

public final class MessagesCollection implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C33871oH();
    public final ThreadKey A00;
    public final ImmutableList A01;
    public final boolean A02;
    public final boolean A03;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            MessagesCollection messagesCollection = (MessagesCollection) obj;
            if (this.A02 != messagesCollection.A02 || this.A03 != messagesCollection.A03 || !Objects.equal(this.A00, messagesCollection.A00) || !Objects.equal(this.A01, messagesCollection.A01)) {
                return false;
            }
        }
        return true;
    }

    public static C33881oI A00(MessagesCollection messagesCollection) {
        C33881oI r1 = new C33881oI();
        r1.A00 = messagesCollection.A00;
        r1.A01(messagesCollection.A01);
        r1.A03 = messagesCollection.A02;
        r1.A04 = messagesCollection.A03;
        return r1;
    }

    public static MessagesCollection A01(Message message) {
        C33881oI r1 = new C33881oI();
        r1.A00 = message.A0U;
        r1.A01(ImmutableList.of(message));
        r1.A03 = false;
        return r1.A00();
    }

    public static MessagesCollection A02(ThreadKey threadKey) {
        C33881oI r1 = new C33881oI();
        r1.A00 = threadKey;
        r1.A01(RegularImmutableList.A02);
        r1.A03 = true;
        return r1.A00();
    }

    public int A04() {
        return this.A01.size();
    }

    public Message A05() {
        if (this.A01.isEmpty()) {
            return null;
        }
        return (Message) this.A01.get(0);
    }

    public Message A06() {
        if (this.A01.isEmpty()) {
            return null;
        }
        ImmutableList immutableList = this.A01;
        return (Message) immutableList.get(immutableList.size() - 1);
    }

    public Message A07(int i) {
        return (Message) this.A01.get(i);
    }

    public boolean A08() {
        return this.A01.isEmpty();
    }

    public boolean A09(int i) {
        if (this.A02 || i <= this.A01.size()) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01, Boolean.valueOf(this.A02), Boolean.valueOf(this.A03)});
    }

    public String toString() {
        int A002;
        StringBuilder sb = new StringBuilder("[\n");
        int min = Math.min(this.A01.size(), 10) - 1;
        int i = 0;
        while (i < min) {
            sb.append(AnonymousClass08S.A0J(Message.A01((Message) this.A01.get(i)), "\n"));
            i++;
        }
        int min2 = Math.min(this.A01.size(), 50) - 1;
        while (i < min2) {
            int i2 = i + 1;
            Message message = (Message) this.A01.get(i);
            StringBuilder sb2 = new StringBuilder();
            sb2.append(message.A0q);
            sb2.append(" ");
            sb2.append(message.A03);
            sb2.append(" ");
            String str = message.A10;
            if (str == null) {
                A002 = -1;
            } else {
                A002 = CoM.A00(str);
            }
            sb2.append(A002);
            sb2.append("\n");
            sb.append(sb2.toString());
            i = i2;
        }
        if ((this.A01.size() - i) - 1 > 0) {
            sb.append(AnonymousClass08S.A01((this.A01.size() - i) - 1, " more...\n"));
        }
        if (!this.A01.isEmpty()) {
            ImmutableList immutableList = this.A01;
            sb.append(AnonymousClass08S.A0J(Message.A01((Message) immutableList.get(immutableList.size() - 1)), "\n"));
        }
        sb.append("]");
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("threadKey", this.A00);
        stringHelper.add("includesFirstMessageInThread", this.A02);
        stringHelper.add("includesLastMessageInThread", this.A03);
        stringHelper.add("numberOfMessages", this.A01.size());
        stringHelper.add("messages", sb.toString());
        return stringHelper.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
        parcel.writeTypedList(this.A01);
        C417826y.A0V(parcel, this.A02);
        C417826y.A0V(parcel, this.A03);
    }

    public static ImmutableList A03(ImmutableList immutableList) {
        int size = immutableList.size();
        long j = Long.MAX_VALUE;
        Message message = null;
        long j2 = Long.MAX_VALUE;
        for (int i = 0; i < size; i++) {
            Message message2 = (Message) immutableList.get(i);
            if (!message2.A14) {
                long j3 = message2.A03;
                if (j3 <= j || C28841fS.A00(message2) <= j2) {
                    j = j3;
                    j2 = C28841fS.A00(message2);
                    message = message2;
                } else {
                    ImmutableList.Builder builder = ImmutableList.builder();
                    Preconditions.checkNotNull(message, "moreRecentMessage cannot be null because the if condition is always false for the first loop");
                    builder.add((Object) message);
                    builder.add((Object) message2);
                    int i2 = i + 1;
                    if (i2 < size) {
                        builder.add(immutableList.get(i2));
                    }
                    return builder.build();
                }
            }
        }
        return null;
    }

    public MessagesCollection(C33881oI r5) {
        ImmutableList A032;
        ThreadKey threadKey = r5.A00;
        this.A00 = threadKey;
        this.A01 = r5.A01;
        this.A02 = r5.A03;
        this.A03 = r5.A04;
        if (threadKey == null) {
            C010708t.A0K("MessagesCollection", "Null thread key");
        }
        if (r5.A02 && (A032 = A03(this.A01)) != null) {
            StringBuilder sb = new StringBuilder("[");
            C24971Xv it = A032.iterator();
            while (it.hasNext()) {
                sb.append(Message.A01((Message) it.next()));
            }
            sb.append("]");
            C010708t.A0K("MessagesCollection", AnonymousClass08S.A0J("Thread messages were not in order, messages=", sb.toString()));
        }
    }

    public MessagesCollection(Parcel parcel) {
        this.A00 = (ThreadKey) parcel.readParcelable(ThreadKey.class.getClassLoader());
        this.A01 = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(Message.CREATOR));
        this.A02 = C417826y.A0W(parcel);
        this.A03 = C417826y.A0W(parcel);
    }
}
