package com.facebook.messaging.model.threads;

import X.AnonymousClass0jJ;
import X.AnonymousClass13A;
import X.AnonymousClass1NN;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.util.JSONUtil;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableMap;
import java.io.IOException;
import java.util.Map;

public final class NicknamesMap implements Parcelable {
    private static final ImmutableMap A02 = RegularImmutableMap.A03;
    public static final Parcelable.Creator CREATOR = new AnonymousClass13A();
    public ImmutableMap A00;
    public String A01;

    public int describeContents() {
        return 0;
    }

    public ImmutableMap A00(AnonymousClass0jJ r3) {
        String str;
        if (this.A00 == null && (str = this.A01) != null) {
            try {
                this.A00 = (ImmutableMap) r3.readValue(str, new AnonymousClass1NN(this));
            } catch (IOException unused) {
            }
        }
        ImmutableMap immutableMap = this.A00;
        if (immutableMap == null) {
            return A02;
        }
        return immutableMap;
    }

    public String A01() {
        ImmutableMap immutableMap;
        if (this.A01 == null && (immutableMap = this.A00) != null && !immutableMap.isEmpty()) {
            this.A01 = JSONUtil.A0I(this.A00).toString();
        }
        return this.A01;
    }

    public String A02(String str, AnonymousClass0jJ r4) {
        String str2;
        if (this.A00 == null && (str2 = this.A01) != null) {
            try {
                this.A00 = (ImmutableMap) r4.readValue(str2, new AnonymousClass1NN(this));
            } catch (IOException unused) {
            }
        }
        ImmutableMap immutableMap = this.A00;
        if (immutableMap == null) {
            return null;
        }
        return (String) immutableMap.get(str);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof NicknamesMap)) {
            return false;
        }
        NicknamesMap nicknamesMap = (NicknamesMap) obj;
        String str = this.A01;
        if ((str == null || !str.equals(nicknamesMap.A01)) && !Objects.equal(this.A00, nicknamesMap.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        String str = this.A01;
        int i2 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        ImmutableMap immutableMap = this.A00;
        if (immutableMap != null) {
            i2 = immutableMap.hashCode();
        }
        return i3 + i2;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        int i3 = 0;
        if (this.A01 != null) {
            i3 = 1;
        }
        parcel.writeInt(i3);
        String str = this.A01;
        if (str != null) {
            parcel.writeString(str);
        }
        if (this.A00 == null) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        ImmutableMap immutableMap = this.A00;
        if (immutableMap != null) {
            C417826y.A0S(parcel, immutableMap);
        }
    }

    public String toString() {
        String A012 = A01();
        if (A012 == null) {
            return "{}";
        }
        return A012;
    }

    public NicknamesMap() {
    }

    public NicknamesMap(String str) {
        if (!"{}".equals(str)) {
            this.A01 = str;
        } else {
            this.A01 = null;
        }
    }

    public NicknamesMap(String str, ImmutableMap immutableMap) {
        this.A01 = str;
        this.A00 = immutableMap;
    }

    public NicknamesMap(Map map) {
        this.A00 = map != null ? ImmutableMap.copyOf(map) : null;
    }
}
