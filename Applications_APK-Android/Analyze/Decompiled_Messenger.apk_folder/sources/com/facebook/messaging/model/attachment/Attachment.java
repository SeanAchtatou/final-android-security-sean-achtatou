package com.facebook.messaging.model.attachment;

import X.C15860w6;
import X.C32371le;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.collect.ImmutableMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public final class Attachment implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C32371le();
    public final int A00;
    public final long A01;
    public final AudioData A02;
    public final ImageData A03;
    public final VideoData A04;
    public final ImmutableMap A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final byte[] A0D;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        ImageData imageData;
        VideoData videoData;
        AudioData audioData;
        String str6;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Attachment attachment = (Attachment) obj;
        if (this.A00 == attachment.A00 && ((str = this.A09) == null ? attachment.A09 == null : str.equals(attachment.A09)) && ((str2 = this.A0B) == null ? attachment.A0B == null : str2.equals(attachment.A0B)) && ((str3 = this.A07) == null ? attachment.A07 == null : str3.equals(attachment.A07)) && ((str4 = this.A0C) == null ? attachment.A0C == null : str4.equals(attachment.A0C)) && ((str5 = this.A08) == null ? attachment.A08 == null : str5.equals(attachment.A08)) && ((imageData = this.A03) == null ? attachment.A03 == null : imageData.equals(attachment.A03)) && ((videoData = this.A04) == null ? attachment.A04 == null : videoData.equals(attachment.A04)) && ((audioData = this.A02) == null ? attachment.A02 == null : audioData.equals(attachment.A02)) && ((str6 = this.A06) == null ? attachment.A06 == null : str6.equals(attachment.A06)) && Arrays.equals(this.A0D, attachment.A0D)) {
            String str7 = this.A0A;
            String str8 = attachment.A0A;
            if (str7 != null) {
                if (str7.equals(str8)) {
                    return true;
                }
            } else if (str8 == null) {
                return true;
            }
        }
        return false;
    }

    public boolean A00() {
        ImmutableMap immutableMap = this.A05;
        if (immutableMap == null) {
            return false;
        }
        return "1".equals(immutableMap.get("is_preview"));
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        String str = this.A09;
        int i10 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i11 = i * 31;
        String str2 = this.A0B;
        if (str2 != null) {
            i2 = str2.hashCode();
        } else {
            i2 = 0;
        }
        int i12 = (i11 + i2) * 31;
        String str3 = this.A07;
        if (str3 != null) {
            i3 = str3.hashCode();
        } else {
            i3 = 0;
        }
        int i13 = (i12 + i3) * 31;
        String str4 = this.A0C;
        if (str4 != null) {
            i4 = str4.hashCode();
        } else {
            i4 = 0;
        }
        int i14 = (i13 + i4) * 31;
        String str5 = this.A08;
        if (str5 != null) {
            i5 = str5.hashCode();
        } else {
            i5 = 0;
        }
        int i15 = (((i14 + i5) * 31) + this.A00) * 31;
        ImageData imageData = this.A03;
        if (imageData != null) {
            i6 = imageData.hashCode();
        } else {
            i6 = 0;
        }
        int i16 = (i15 + i6) * 31;
        VideoData videoData = this.A04;
        if (videoData != null) {
            i7 = videoData.hashCode();
        } else {
            i7 = 0;
        }
        int i17 = (i16 + i7) * 31;
        AudioData audioData = this.A02;
        if (audioData != null) {
            i8 = audioData.hashCode();
        } else {
            i8 = 0;
        }
        int i18 = (i17 + i8) * 31;
        String str6 = this.A06;
        if (str6 != null) {
            i9 = str6.hashCode();
        } else {
            i9 = 0;
        }
        int hashCode = (((i18 + i9) * 31) + Arrays.hashCode(this.A0D)) * 31;
        String str7 = this.A0A;
        if (str7 != null) {
            i10 = str7.hashCode();
        }
        return hashCode + i10;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A09);
        parcel.writeString(this.A0B);
        parcel.writeString(this.A07);
        parcel.writeString(this.A0C);
        parcel.writeString(this.A08);
        parcel.writeInt(this.A00);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A04, i);
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A06);
        parcel.writeValue(this.A0D);
        parcel.writeString(this.A0A);
        parcel.writeMap(this.A05);
        parcel.writeLong(this.A01);
    }

    public Attachment(C15860w6 r3) {
        this.A09 = r3.A0C;
        this.A0B = r3.A0D;
        this.A07 = r3.A06;
        this.A0C = r3.A09;
        this.A08 = r3.A07;
        this.A00 = r3.A00;
        this.A03 = r3.A03;
        this.A04 = r3.A04;
        this.A02 = r3.A02;
        this.A06 = r3.A05;
        this.A0D = r3.A0B;
        this.A0A = r3.A08;
        Map map = r3.A0A;
        this.A05 = ImmutableMap.copyOf(map == null ? new HashMap() : map);
        this.A01 = r3.A01;
    }

    public Attachment(Parcel parcel) {
        this.A09 = parcel.readString();
        this.A0B = parcel.readString();
        this.A07 = parcel.readString();
        this.A0C = parcel.readString();
        this.A08 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A03 = (ImageData) parcel.readParcelable(ImageData.class.getClassLoader());
        this.A04 = (VideoData) parcel.readParcelable(VideoData.class.getClassLoader());
        this.A02 = (AudioData) parcel.readParcelable(AudioData.class.getClassLoader());
        this.A06 = parcel.readString();
        this.A0D = (byte[]) parcel.readValue(null);
        this.A0A = parcel.readString();
        this.A05 = ImmutableMap.copyOf(parcel.readHashMap(Attachment.class.getClassLoader()));
        this.A01 = parcel.readLong();
    }
}
