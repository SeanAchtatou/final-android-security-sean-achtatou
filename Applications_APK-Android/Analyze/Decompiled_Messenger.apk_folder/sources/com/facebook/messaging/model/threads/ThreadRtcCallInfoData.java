package com.facebook.messaging.model.threads;

import X.C06850cB;
import X.C29251g7;
import X.C29261g8;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;

public final class ThreadRtcCallInfoData implements Parcelable {
    public static final ThreadRtcCallInfoData A04 = new ThreadRtcCallInfoData("UNKNOWN", null, null, true);
    public static final Parcelable.Creator CREATOR = new C29261g8();
    public final String A00;
    public final String A01;
    public final String A02;
    public final boolean A03;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ThreadRtcCallInfoData threadRtcCallInfoData = (ThreadRtcCallInfoData) obj;
            if (this.A00 != threadRtcCallInfoData.A00 || !C06850cB.A0C(this.A02, threadRtcCallInfoData.A02) || !C06850cB.A0C(this.A01, threadRtcCallInfoData.A01) || this.A03 != threadRtcCallInfoData.A03) {
                return false;
            }
        }
        return true;
    }

    public boolean A00() {
        String str = this.A00;
        if (str == "AUDIO_GROUP_CALL" || str == "VIDEO_GROUP_CALL") {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.A00;
        int i = 0;
        if (str != null) {
            i = 0 + str.hashCode();
        }
        String str2 = this.A02;
        if (str2 != null) {
            i = (i * 31) + str2.hashCode();
        }
        String str3 = this.A01;
        if (str3 != null) {
            i = (i * 31) + str3.hashCode();
        }
        return (i * 31) + Boolean.valueOf(this.A03).hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        C417826y.A0V(parcel, this.A03);
    }

    public ThreadRtcCallInfoData(C29251g7 r2) {
        this.A00 = r2.A00;
        this.A02 = r2.A02;
        this.A01 = r2.A01;
        this.A03 = r2.A03;
    }

    public ThreadRtcCallInfoData(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A03 = C417826y.A0W(parcel);
    }

    private ThreadRtcCallInfoData(String str, String str2, String str3, boolean z) {
        this.A00 = str;
        this.A02 = str2;
        this.A01 = str3;
        this.A03 = z;
    }
}
