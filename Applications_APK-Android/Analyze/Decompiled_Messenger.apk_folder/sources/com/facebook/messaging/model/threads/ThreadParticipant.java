package com.facebook.messaging.model.threads;

import X.AnonymousClass114;
import X.AnonymousClass116;
import X.C000300h;
import X.C22298Ase;
import X.C28831fR;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.user.model.UserKey;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class ThreadParticipant implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass116();
    public final int A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final ParticipantInfo A04;
    public final AnonymousClass114 A05;
    public final UserKey A06;
    public final String A07;
    public final boolean A08;
    public final boolean A09;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        ParticipantInfo participantInfo;
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            ThreadParticipant threadParticipant = (ThreadParticipant) obj;
            if (this.A02 == threadParticipant.A02 && this.A03 == threadParticipant.A03 && this.A01 == threadParticipant.A01 && this.A09 == threadParticipant.A09 && this.A05 == threadParticipant.A05 && this.A08 == threadParticipant.A08 && ((participantInfo = this.A04) == null ? threadParticipant.A04 == null : participantInfo.equals(threadParticipant.A04)) && Objects.equal(this.A06, threadParticipant.A06) && this.A00 == threadParticipant.A00) {
                String str = this.A07;
                String str2 = threadParticipant.A07;
                if (str != null) {
                    return str.equals(str2);
                }
                return str2 == null;
            }
        }
        return false;
    }

    public UserKey A00() {
        return this.A04.A01;
    }

    public String A01() {
        return this.A04.A00();
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        ParticipantInfo participantInfo = this.A04;
        int i4 = 0;
        if (participantInfo != null) {
            i = participantInfo.hashCode();
        } else {
            i = 0;
        }
        long j = this.A02;
        long j2 = this.A03;
        int i5 = ((((i * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str = this.A07;
        if (str != null) {
            i2 = str.hashCode();
        } else {
            i2 = 0;
        }
        long j3 = this.A01;
        int i6 = (((((i5 + i2) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + (this.A09 ? 1 : 0)) * 31;
        AnonymousClass114 r0 = this.A05;
        if (r0 != null) {
            i3 = r0.dbValue;
        } else {
            i3 = -2;
        }
        int i7 = (((i6 + i3) * 31) + (this.A08 ? 1 : 0)) * 31;
        UserKey userKey = this.A06;
        if (userKey != null) {
            i4 = userKey.hashCode();
        }
        return ((i7 + i4) * 31) + this.A00;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(ThreadParticipant.class);
        stringHelper.add(C22298Ase.$const$string(13), this.A04);
        stringHelper.add("lastReadReceiptActionTimestampMs", this.A02);
        stringHelper.add("lastReadReceiptWatermarkTimestampMs", this.A03);
        stringHelper.add("lastDeliveredReceiptMsgId", this.A07);
        stringHelper.add("lastDeliveredReceiptTimestampMs", this.A01);
        stringHelper.add("isAdmin", this.A09);
        stringHelper.add("adminType", this.A05);
        stringHelper.add("canViewerMessage", this.A08);
        stringHelper.add("inviterUserKey", this.A06);
        stringHelper.add("source", this.A00);
        return stringHelper.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A04, i);
        parcel.writeLong(this.A02);
        parcel.writeString(this.A07);
        parcel.writeLong(this.A01);
        C417826y.A0V(parcel, this.A09);
        C417826y.A0L(parcel, this.A05);
        parcel.writeLong(this.A03);
        C417826y.A0V(parcel, this.A08);
        parcel.writeParcelable(this.A06, i);
        parcel.writeInt(this.A00);
    }

    public ThreadParticipant(C28831fR r3) {
        ParticipantInfo participantInfo = r3.A04;
        C000300h.A01(participantInfo);
        this.A04 = participantInfo;
        this.A02 = r3.A02;
        this.A03 = r3.A03;
        this.A07 = r3.A07;
        this.A01 = r3.A01;
        this.A09 = r3.A09;
        this.A05 = r3.A05;
        this.A08 = r3.A08;
        this.A06 = r3.A06;
        this.A00 = r3.A00;
    }

    public ThreadParticipant(Parcel parcel) {
        this.A04 = (ParticipantInfo) parcel.readParcelable(ParticipantInfo.class.getClassLoader());
        this.A02 = parcel.readLong();
        this.A07 = parcel.readString();
        this.A01 = parcel.readLong();
        this.A09 = C417826y.A0W(parcel);
        this.A05 = (AnonymousClass114) C417826y.A0C(parcel, AnonymousClass114.class);
        this.A03 = parcel.readLong();
        this.A08 = C417826y.A0W(parcel);
        this.A06 = (UserKey) parcel.readParcelable(UserKey.class.getClassLoader());
        this.A00 = parcel.readInt();
    }
}
