package com.facebook.messaging.model.threads;

import X.AnonymousClass13e;
import X.C184413f;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public final class AdContextData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C184413f();
    public final Uri A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeParcelable(this.A00, i);
        parcel.writeString(this.A02);
        parcel.writeString(this.A03);
        parcel.writeByte(this.A04 ? (byte) 1 : 0);
    }

    public AdContextData(AnonymousClass13e r2) {
        this.A01 = r2.A01;
        this.A00 = r2.A00;
        this.A02 = r2.A02;
        this.A03 = r2.A03;
        this.A04 = r2.A04;
    }

    public AdContextData(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.A02 = parcel.readString();
        this.A03 = parcel.readString();
        this.A04 = parcel.readByte() != 0;
    }
}
