package com.facebook.messaging.model.threads;

import X.AnonymousClass163;
import X.C010708t;
import X.C05180Xy;
import X.C24971Xv;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;

public final class ThreadsCollection implements Parcelable {
    public static final ThreadsCollection A02 = new ThreadsCollection(RegularImmutableList.A02, true);
    public static final Parcelable.Creator CREATOR = new AnonymousClass163();
    public final ImmutableList A00;
    public final boolean A01;

    public int describeContents() {
        return 0;
    }

    public static ThreadsCollection A00(ThreadsCollection threadsCollection, ThreadsCollection threadsCollection2) {
        if (!threadsCollection.A00.isEmpty() || !threadsCollection2.A00.isEmpty()) {
            if (threadsCollection.A00.isEmpty()) {
                return threadsCollection2;
            }
            if (!threadsCollection2.A00.isEmpty()) {
                C05180Xy r4 = new C05180Xy();
                ImmutableList.Builder builder = ImmutableList.builder();
                C24971Xv it = threadsCollection.A00.iterator();
                while (it.hasNext()) {
                    ThreadSummary threadSummary = (ThreadSummary) it.next();
                    builder.add((Object) threadSummary);
                    r4.add(threadSummary.A0S);
                }
                C24971Xv it2 = threadsCollection2.A00.iterator();
                while (it2.hasNext()) {
                    ThreadSummary threadSummary2 = (ThreadSummary) it2.next();
                    if (!r4.contains(threadSummary2.A0S)) {
                        builder.add((Object) threadSummary2);
                    }
                }
                return new ThreadsCollection(builder.build(), threadsCollection2.A01);
            }
        }
        return threadsCollection;
    }

    public int A02() {
        return this.A00.size();
    }

    public ThreadSummary A03(int i) {
        return (ThreadSummary) this.A00.get(i);
    }

    public String toString() {
        ArrayList arrayList = new ArrayList();
        C24971Xv it = this.A00.iterator();
        while (it.hasNext()) {
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            arrayList.add(threadSummary.A0S + ":" + threadSummary.A0B());
        }
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("includesFirstThreadInFolder", this.A01);
        stringHelper.add("threadInfo", arrayList);
        return stringHelper.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.A00);
        parcel.writeInt(this.A01 ? 1 : 0);
    }

    public static ImmutableList A01(ImmutableList immutableList) {
        ImmutableList.Builder builder = ImmutableList.builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            if (!ThreadKey.A0F(threadSummary.A0S)) {
                builder.add((Object) threadSummary);
            }
        }
        return builder.build();
    }

    public ThreadsCollection(Parcel parcel) {
        this.A00 = ImmutableList.copyOf((Collection) parcel.readArrayList(ThreadSummary.class.getClassLoader()));
        this.A01 = parcel.readInt() != 0;
    }

    public ThreadsCollection(ImmutableList immutableList, boolean z) {
        this.A00 = immutableList;
        this.A01 = z;
        C24971Xv it = immutableList.iterator();
        boolean z2 = false;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            z2 = !threadSummary.A17 ? true : z2;
            if (threadSummary.A0v != null) {
                z2 = false;
                break;
            }
        }
        if (z2) {
            C24971Xv it2 = immutableList.iterator();
            ThreadSummary threadSummary2 = null;
            while (it2.hasNext()) {
                ThreadSummary threadSummary3 = (ThreadSummary) it2.next();
                if (threadSummary2 != null) {
                    long j = threadSummary3.A0B;
                    long j2 = threadSummary2.A0B;
                    if (j > j2) {
                        C010708t.A0K("ThreadsCollection", String.format("Threads were not in order, this[%s] timestamp=%d, last[%s] timestampMs=%d", threadSummary3.A0S, Long.valueOf(j), threadSummary2.A0S, Long.valueOf(j2)));
                        return;
                    }
                }
                threadSummary2 = threadSummary3;
            }
        }
    }
}
