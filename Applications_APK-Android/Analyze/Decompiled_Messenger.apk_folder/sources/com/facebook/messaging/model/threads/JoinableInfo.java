package com.facebook.messaging.model.threads;

import X.C17990zt;
import X.C28681fC;
import X.C28691fD;
import X.C417826y;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import java.util.Arrays;

public final class JoinableInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C28691fD();
    public final Uri A00;
    public final Uri A01;
    public final GroupApprovalInfo A02;
    public final C17990zt A03;
    public final String A04;
    public final boolean A05;
    public final boolean A06;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            JoinableInfo joinableInfo = (JoinableInfo) obj;
            if (this.A06 != joinableInfo.A06 || this.A05 != joinableInfo.A05 || !Objects.equal(this.A00, joinableInfo.A00) || !Objects.equal(this.A01, joinableInfo.A01) || this.A03 != joinableInfo.A03 || !Objects.equal(this.A02, joinableInfo.A02) || !Objects.equal(this.A04, joinableInfo.A04)) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r1 != false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.net.Uri A00() {
        /*
            r2 = this;
            android.net.Uri r0 = r2.A01
            if (r0 == 0) goto L_0x000f
            java.lang.String r0 = r0.toString()
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            r0 = 1
            if (r1 == 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x0015
            android.net.Uri r0 = r2.A01
            return r0
        L_0x0015:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.threads.JoinableInfo.A00():android.net.Uri");
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01, Boolean.valueOf(this.A06), Boolean.valueOf(this.A05), this.A03, this.A02, this.A04});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
        C417826y.A0V(parcel, this.A06);
        C417826y.A0V(parcel, this.A05);
        parcel.writeInt(this.A03.dbValue);
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A04);
    }

    public JoinableInfo(C28681fC r2) {
        this.A00 = r2.A00;
        this.A01 = r2.A01;
        this.A06 = r2.A06;
        this.A05 = r2.A05;
        this.A03 = r2.A03;
        this.A02 = r2.A02;
        this.A04 = r2.A04;
    }

    public JoinableInfo(Parcel parcel) {
        Class<Uri> cls = Uri.class;
        this.A00 = (Uri) parcel.readParcelable(cls.getClassLoader());
        this.A01 = (Uri) parcel.readParcelable(cls.getClassLoader());
        this.A06 = C417826y.A0W(parcel);
        this.A05 = C417826y.A0W(parcel);
        this.A03 = C17990zt.A00(parcel.readInt());
        this.A02 = (GroupApprovalInfo) parcel.readParcelable(GroupApprovalInfo.class.getClassLoader());
        this.A04 = parcel.readString();
    }
}
