package com.facebook.messaging.model.send;

import X.C21461Hb;
import X.C21471Hc;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;

public final class PendingSendQueueKey implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C21461Hb();
    public final C21471Hc A00;
    public final ThreadKey A01;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                PendingSendQueueKey pendingSendQueueKey = (PendingSendQueueKey) obj;
                if (this.A00 != pendingSendQueueKey.A00 || !this.A01.equals(pendingSendQueueKey.A01)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + this.A00.hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A01, i);
        parcel.writeSerializable(this.A00);
    }

    public PendingSendQueueKey(Parcel parcel) {
        this.A01 = (ThreadKey) parcel.readParcelable(ThreadKey.class.getClassLoader());
        this.A00 = (C21471Hc) parcel.readSerializable();
    }

    public PendingSendQueueKey(ThreadKey threadKey, C21471Hc r2) {
        Preconditions.checkNotNull(threadKey);
        Preconditions.checkNotNull(r2);
        this.A01 = threadKey;
        this.A00 = r2;
    }
}
