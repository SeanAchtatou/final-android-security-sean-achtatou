package com.facebook.messaging.model.threads;

import X.AnonymousClass104;
import X.AnonymousClass6YD;
import X.C17960zn;
import X.C17980zq;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.workshared.syncedgroups.model.WorkSyncGroupModelData;
import com.google.common.base.Objects;
import java.util.Arrays;

public final class GroupThreadData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C17960zn();
    public final long A00;
    public final long A01;
    public final GroupThreadAssociatedObject A02;
    public final AnonymousClass104 A03;
    public final JoinableInfo A04;
    public final SyncedGroupData A05;
    public final WorkSyncGroupModelData A06;
    public final String A07;
    public final String A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            GroupThreadData groupThreadData = (GroupThreadData) obj;
            if (!(this.A0B == groupThreadData.A0B && this.A00 == groupThreadData.A00 && Objects.equal(this.A07, groupThreadData.A07) && Objects.equal(this.A02, groupThreadData.A02) && Objects.equal(this.A04, groupThreadData.A04) && Objects.equal(this.A08, groupThreadData.A08) && this.A09 == groupThreadData.A09 && this.A01 == groupThreadData.A01 && this.A0A == groupThreadData.A0A && this.A03 == groupThreadData.A03 && Objects.equal(this.A06, groupThreadData.A06))) {
                return false;
            }
        }
        return true;
    }

    public AnonymousClass6YD A00() {
        return AnonymousClass6YD.A00(this.A08);
    }

    public boolean A01() {
        GroupThreadAssociatedObject groupThreadAssociatedObject = this.A02;
        if (groupThreadAssociatedObject == null || groupThreadAssociatedObject.A00() == null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Boolean.valueOf(this.A0B), this.A07, this.A02, this.A04, Long.valueOf(this.A00), this.A08, Boolean.valueOf(this.A09), Long.valueOf(this.A01), Boolean.valueOf(this.A0A), this.A03, this.A06});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A07);
        parcel.writeParcelable(this.A02, i);
        C417826y.A0V(parcel, this.A0B);
        parcel.writeLong(this.A00);
        parcel.writeParcelable(this.A04, i);
        parcel.writeString(this.A08);
        C417826y.A0V(parcel, this.A09);
        parcel.writeLong(this.A01);
        C417826y.A0V(parcel, this.A0A);
        C417826y.A0L(parcel, this.A03);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A06, i);
    }

    public GroupThreadData(C17980zq r3) {
        this.A07 = r3.A07;
        this.A02 = r3.A02;
        this.A0B = r3.A09;
        this.A00 = r3.A00;
        this.A04 = r3.A04;
        this.A08 = r3.A08;
        this.A09 = r3.A0A;
        this.A01 = r3.A01;
        this.A0A = r3.A0B;
        this.A03 = r3.A03;
        this.A05 = r3.A05;
        this.A06 = r3.A06;
    }

    public GroupThreadData(Parcel parcel) {
        this.A07 = parcel.readString();
        this.A02 = (GroupThreadAssociatedObject) parcel.readParcelable(GroupThreadAssociatedObject.class.getClassLoader());
        this.A0B = C417826y.A0W(parcel);
        this.A00 = parcel.readLong();
        this.A04 = (JoinableInfo) parcel.readParcelable(JoinableInfo.class.getClassLoader());
        this.A08 = parcel.readString();
        this.A09 = C417826y.A0W(parcel);
        this.A01 = parcel.readLong();
        this.A0A = C417826y.A0W(parcel);
        this.A03 = (AnonymousClass104) C417826y.A0C(parcel, AnonymousClass104.class);
        this.A05 = (SyncedGroupData) parcel.readParcelable(SyncedGroupData.class.getClassLoader());
        this.A06 = (WorkSyncGroupModelData) parcel.readParcelable(WorkSyncGroupModelData.class.getClassLoader());
    }
}
