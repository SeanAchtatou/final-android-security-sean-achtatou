package com.facebook.messaging.model.montagemetadata;

import X.AnonymousClass0tY;
import X.AnonymousClass1Y3;
import X.AnonymousClass24B;
import X.AnonymousClass2Vn;
import X.AnonymousClass2W5;
import X.AnonymousClass324;
import X.AnonymousClass4BB;
import X.C05360Yq;
import X.C11260mU;
import X.C11710np;
import X.C14550ta;
import X.C182811d;
import X.C24971Xv;
import X.C26791c3;
import X.C28271eX;
import X.C28931fb;
import X.C33471nd;
import X.C47352Um;
import X.C47362Un;
import X.C95324gE;
import X.C99084oO;
import X.ECX;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.ipc.stories.model.InlineActivityInfo;
import com.facebook.ipc.stories.model.StoryBackgroundInfo;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableList;
import org.webrtc.audio.WebRtcAudioRecord;

@JsonDeserialize(using = Deserializer.class)
@JsonSerialize(using = Serializer.class)
public final class MontageMetadata implements C47352Um, Parcelable {
    public static final Parcelable.Creator CREATOR = new C47362Un();
    public final C95324gE A00;
    public final AnonymousClass2Vn A01;
    public final InlineActivityInfo A02;
    public final StoryBackgroundInfo A03;
    public final MontageActorInfo A04;
    public final AnonymousClass4BB A05;
    public final ImmutableList A06;
    public final ImmutableList A07;
    public final ImmutableList A08;
    public final ImmutableList A09;
    public final Boolean A0A;
    public final Boolean A0B;
    public final Boolean A0C;
    public final Boolean A0D;
    public final Boolean A0E;
    public final Boolean A0F;
    public final Boolean A0G;
    public final Boolean A0H;
    public final Boolean A0I;
    public final Boolean A0J;
    public final Float A0K;
    public final Long A0L;
    public final Long A0M;
    public final String A0N;
    public final String A0O;
    public final String A0P;
    public final String A0Q;
    public final String A0R;

    public class Deserializer extends JsonDeserializer {
        public /* bridge */ /* synthetic */ Object deserialize(C28271eX r6, C26791c3 r7) {
            C33471nd r2 = new C33471nd();
            do {
                try {
                    if (r6.getCurrentToken() == C182811d.FIELD_NAME) {
                        String currentName = r6.getCurrentName();
                        r6.nextToken();
                        char c = 65535;
                        switch (currentName.hashCode()) {
                            case -2077023236:
                                if (currentName.equals("montage_x_ray_smart_feature")) {
                                    c = 20;
                                    break;
                                }
                                break;
                            case -1988814966:
                                if (currentName.equals("montage_original_data_id")) {
                                    c = 19;
                                    break;
                                }
                                break;
                            case -1956965529:
                                if (currentName.equals("can_show_story_in_thread")) {
                                    c = 4;
                                    break;
                                }
                                break;
                            case -1294833533:
                                if (currentName.equals("can_report")) {
                                    c = 3;
                                    break;
                                }
                                break;
                            case -1071752347:
                                if (currentName.equals(C99084oO.$const$string(AnonymousClass1Y3.A6B))) {
                                    c = 26;
                                    break;
                                }
                                break;
                            case -1032493414:
                                if (currentName.equals("montage_message_type")) {
                                    c = 17;
                                    break;
                                }
                                break;
                            case -960432552:
                                if (currentName.equals("montage_objectionable_content_info")) {
                                    c = 18;
                                    break;
                                }
                                break;
                            case -775506228:
                                if (currentName.equals(ECX.$const$string(29))) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case -709951264:
                                if (currentName.equals("montage_actor_info")) {
                                    c = 16;
                                    break;
                                }
                                break;
                            case -492033556:
                                if (currentName.equals("encoded_id")) {
                                    c = 5;
                                    break;
                                }
                                break;
                            case -476980957:
                                if (currentName.equals("metadata_from_omnistore")) {
                                    c = 15;
                                    break;
                                }
                                break;
                            case -376233884:
                                if (currentName.equals("is_unread")) {
                                    c = 12;
                                    break;
                                }
                                break;
                            case -196405935:
                                if (currentName.equals("is_reshareable")) {
                                    c = 11;
                                    break;
                                }
                                break;
                            case -126620376:
                                if (currentName.equals("can_mute")) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 185017562:
                                if (currentName.equals("legacy_thread_key_id")) {
                                    c = 13;
                                    break;
                                }
                                break;
                            case 373873083:
                                if (currentName.equals("can_reply")) {
                                    c = 2;
                                    break;
                                }
                                break;
                            case 417210788:
                                if (currentName.equals("reshare_intents")) {
                                    c = 22;
                                    break;
                                }
                                break;
                            case 472834883:
                                if (currentName.equals("has_long_text_metadata")) {
                                    c = 6;
                                    break;
                                }
                                break;
                            case 478518140:
                                if (currentName.equals("story_viewer_background_info")) {
                                    c = 25;
                                    break;
                                }
                                break;
                            case 1090794272:
                                if (currentName.equals("integrity_score")) {
                                    c = 9;
                                    break;
                                }
                                break;
                            case 1259002733:
                                if (currentName.equals("has_media_text")) {
                                    c = 7;
                                    break;
                                }
                                break;
                            case 1261153850:
                                if (currentName.equals("original_post_permalink")) {
                                    c = 21;
                                    break;
                                }
                                break;
                            case 1278858501:
                                if (currentName.equals("text_format_preset_id")) {
                                    c = 27;
                                    break;
                                }
                                break;
                            case 1419219041:
                                if (currentName.equals("media_caption_text")) {
                                    c = 14;
                                    break;
                                }
                                break;
                            case 1469264056:
                                if (currentName.equals("inline_activity_info")) {
                                    c = 8;
                                    break;
                                }
                                break;
                            case 1696201478:
                                if (currentName.equals("share_story_attachments")) {
                                    c = 24;
                                    break;
                                }
                                break;
                            case 2001471417:
                                if (currentName.equals("is_my_montage")) {
                                    c = 10;
                                    break;
                                }
                                break;
                            case 2022463996:
                                if (currentName.equals("share_attachment_ids")) {
                                    c = 23;
                                    break;
                                }
                                break;
                        }
                        switch (c) {
                            case 0:
                                r2.A00 = (C95324gE) C14550ta.A01(C95324gE.class, r6, r7);
                                break;
                            case 1:
                                Boolean bool = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0A = bool;
                                C28931fb.A06(bool, "canMute");
                                break;
                            case 2:
                                Boolean bool2 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0B = bool2;
                                C28931fb.A06(bool2, "canReply");
                                break;
                            case 3:
                                Boolean bool3 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0C = bool3;
                                C28931fb.A06(bool3, "canReport");
                                break;
                            case 4:
                                Boolean bool4 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0D = bool4;
                                C28931fb.A06(bool4, C99084oO.$const$string(13));
                                break;
                            case 5:
                                r2.A0N = C14550ta.A02(r6);
                                break;
                            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                                Boolean bool5 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0E = bool5;
                                C28931fb.A06(bool5, C99084oO.$const$string(60));
                                break;
                            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                                Boolean bool6 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0F = bool6;
                                C28931fb.A06(bool6, C99084oO.$const$string(61));
                                break;
                            case 8:
                                r2.A02 = (InlineActivityInfo) C14550ta.A01(InlineActivityInfo.class, r6, r7);
                                break;
                            case Process.SIGKILL:
                                r2.A0K = (Float) C14550ta.A01(Float.class, r6, r7);
                                break;
                            case AnonymousClass1Y3.A01:
                                Boolean bool7 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0G = bool7;
                                C28931fb.A06(bool7, "isMyMontage");
                                break;
                            case AnonymousClass1Y3.A02:
                                Boolean bool8 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0H = bool8;
                                C28931fb.A06(bool8, C05360Yq.$const$string(1672));
                                break;
                            case AnonymousClass1Y3.A03:
                                Boolean bool9 = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                r2.A0I = bool9;
                                C28931fb.A06(bool9, C05360Yq.$const$string(AnonymousClass1Y3.A4B));
                                break;
                            case 13:
                                r2.A0L = (Long) C14550ta.A01(Long.class, r6, r7);
                                break;
                            case 14:
                                r2.A0O = C14550ta.A02(r6);
                                break;
                            case 15:
                                r2.A0J = (Boolean) C14550ta.A01(Boolean.class, r6, r7);
                                break;
                            case 16:
                                r2.A04 = (MontageActorInfo) C14550ta.A01(MontageActorInfo.class, r6, r7);
                                break;
                            case 17:
                                r2.A0P = C14550ta.A02(r6);
                                break;
                            case Process.SIGCONT:
                                r2.A05 = (AnonymousClass4BB) C14550ta.A01(AnonymousClass4BB.class, r6, r7);
                                break;
                            case Process.SIGSTOP:
                                r2.A0Q = C14550ta.A02(r6);
                                break;
                            case 20:
                                ImmutableList A00 = C14550ta.A00(r6, r7, MontageXRaySmartFeature.class, null);
                                r2.A06 = A00;
                                C28931fb.A06(A00, C05360Yq.$const$string(1917));
                                break;
                            case AnonymousClass1Y3.A05:
                                r2.A0R = C14550ta.A02(r6);
                                break;
                            case AnonymousClass1Y3.A06:
                                ImmutableList A002 = C14550ta.A00(r6, r7, String.class, null);
                                r2.A07 = A002;
                                C28931fb.A06(A002, C05360Yq.$const$string(2189));
                                break;
                            case 23:
                                r2.A08 = C14550ta.A00(r6, r7, Long.class, null);
                                break;
                            case AnonymousClass1Y3.A07:
                                r2.A09 = C14550ta.A00(r6, r7, AnonymousClass2W5.class, null);
                                break;
                            case 25:
                                r2.A03 = (StoryBackgroundInfo) C14550ta.A01(StoryBackgroundInfo.class, r6, r7);
                                break;
                            case AnonymousClass1Y3.A08:
                                r2.A01 = (AnonymousClass2Vn) C14550ta.A01(AnonymousClass2Vn.class, r6, r7);
                                break;
                            case AnonymousClass1Y3.A09:
                                r2.A0M = (Long) C14550ta.A01(Long.class, r6, r7);
                                break;
                            default:
                                r6.skipChildren();
                                break;
                        }
                    }
                } catch (Exception e) {
                    C14550ta.A0D(MontageMetadata.class, r6, e);
                }
            } while (AnonymousClass0tY.A00(r6) != C182811d.END_OBJECT);
            return new MontageMetadata(r2);
        }
    }

    public class Serializer extends JsonSerializer {
        public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
            MontageMetadata montageMetadata = (MontageMetadata) obj;
            r4.writeStartObject();
            C14550ta.A04(r4, r5, ECX.$const$string(29), montageMetadata.Abu());
            C14550ta.A09(r4, "can_mute", montageMetadata.Ag6());
            C14550ta.A09(r4, AnonymousClass24B.$const$string(204), montageMetadata.Ag8());
            C14550ta.A09(r4, AnonymousClass24B.$const$string(205), montageMetadata.Ag9());
            C14550ta.A09(r4, AnonymousClass24B.$const$string(AnonymousClass1Y3.A71), montageMetadata.AgB());
            C14550ta.A0B(r4, "encoded_id", montageMetadata.AlJ());
            C14550ta.A09(r4, AnonymousClass24B.$const$string(AnonymousClass1Y3.A8q), montageMetadata.AoQ());
            C14550ta.A09(r4, AnonymousClass24B.$const$string(AnonymousClass1Y3.A8r), montageMetadata.AoR());
            C14550ta.A04(r4, r5, AnonymousClass24B.$const$string(AnonymousClass1Y3.A99), montageMetadata.Aq5());
            Float AqQ = montageMetadata.AqQ();
            String $const$string = AnonymousClass24B.$const$string(AnonymousClass1Y3.A9A);
            if (AqQ != null) {
                r4.writeFieldName($const$string);
                r4.writeNumber(AqQ.floatValue());
            }
            C14550ta.A09(r4, AnonymousClass24B.$const$string(1135), montageMetadata.ArB());
            C14550ta.A09(r4, AnonymousClass24B.$const$string(1136), montageMetadata.ArK());
            C14550ta.A09(r4, "is_unread", montageMetadata.ArM());
            Long AsJ = montageMetadata.AsJ();
            String $const$string2 = AnonymousClass24B.$const$string(1149);
            if (AsJ != null) {
                r4.writeFieldName($const$string2);
                r4.writeNumber(AsJ.longValue());
            }
            C14550ta.A0B(r4, AnonymousClass24B.$const$string(AnonymousClass1Y3.A9a), montageMetadata.Atb());
            C14550ta.A09(r4, AnonymousClass24B.$const$string(1208), montageMetadata.AuT());
            C14550ta.A04(r4, r5, AnonymousClass24B.$const$string(AnonymousClass1Y3.A9u), montageMetadata.Auw());
            C14550ta.A0B(r4, AnonymousClass24B.$const$string(AnonymousClass1Y3.A9v), montageMetadata.Auy());
            C14550ta.A04(r4, r5, AnonymousClass24B.$const$string(AnonymousClass1Y3.A9w), montageMetadata.Auz());
            C14550ta.A0B(r4, AnonymousClass24B.$const$string(AnonymousClass1Y3.A9x), montageMetadata.Av0());
            C14550ta.A05(r4, r5, AnonymousClass24B.$const$string(AnonymousClass1Y3.AA0), montageMetadata.Av3());
            C14550ta.A0B(r4, AnonymousClass24B.$const$string(1273), montageMetadata.Awb());
            C14550ta.A05(r4, r5, AnonymousClass24B.$const$string(1345), montageMetadata.B14());
            C14550ta.A05(r4, r5, AnonymousClass24B.$const$string(1374), montageMetadata.B2p());
            C14550ta.A05(r4, r5, AnonymousClass24B.$const$string(AnonymousClass1Y3.ABJ), montageMetadata.B2s());
            C14550ta.A04(r4, r5, AnonymousClass24B.$const$string(AnonymousClass1Y3.ABc), montageMetadata.B47());
            C14550ta.A04(r4, r5, "text_format_metadata", montageMetadata.B5f());
            Long B5g = montageMetadata.B5g();
            String $const$string3 = AnonymousClass24B.$const$string(1425);
            if (B5g != null) {
                r4.writeFieldName($const$string3);
                r4.writeNumber(B5g.longValue());
            }
            r4.writeEndObject();
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MontageMetadata) {
                MontageMetadata montageMetadata = (MontageMetadata) obj;
                if (!C28931fb.A07(this.A00, montageMetadata.A00) || !C28931fb.A07(this.A0A, montageMetadata.A0A) || !C28931fb.A07(this.A0B, montageMetadata.A0B) || !C28931fb.A07(this.A0C, montageMetadata.A0C) || !C28931fb.A07(this.A0D, montageMetadata.A0D) || !C28931fb.A07(this.A0N, montageMetadata.A0N) || !C28931fb.A07(this.A0E, montageMetadata.A0E) || !C28931fb.A07(this.A0F, montageMetadata.A0F) || !C28931fb.A07(this.A02, montageMetadata.A02) || !C28931fb.A07(this.A0K, montageMetadata.A0K) || !C28931fb.A07(this.A0G, montageMetadata.A0G) || !C28931fb.A07(this.A0H, montageMetadata.A0H) || !C28931fb.A07(this.A0I, montageMetadata.A0I) || !C28931fb.A07(this.A0L, montageMetadata.A0L) || !C28931fb.A07(this.A0O, montageMetadata.A0O) || !C28931fb.A07(this.A0J, montageMetadata.A0J) || !C28931fb.A07(this.A04, montageMetadata.A04) || !C28931fb.A07(this.A0P, montageMetadata.A0P) || !C28931fb.A07(this.A05, montageMetadata.A05) || !C28931fb.A07(this.A0Q, montageMetadata.A0Q) || !C28931fb.A07(this.A06, montageMetadata.A06) || !C28931fb.A07(this.A0R, montageMetadata.A0R) || !C28931fb.A07(this.A07, montageMetadata.A07) || !C28931fb.A07(this.A08, montageMetadata.A08) || !C28931fb.A07(this.A09, montageMetadata.A09) || !C28931fb.A07(this.A03, montageMetadata.A03) || !C28931fb.A07(this.A01, montageMetadata.A01) || !C28931fb.A07(this.A0M, montageMetadata.A0M)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public C95324gE Abu() {
        return this.A00;
    }

    public Boolean Ag6() {
        return this.A0A;
    }

    public Boolean Ag8() {
        return this.A0B;
    }

    public Boolean Ag9() {
        return this.A0C;
    }

    public Boolean AgB() {
        return this.A0D;
    }

    public String AlJ() {
        return this.A0N;
    }

    public Boolean AoQ() {
        return this.A0E;
    }

    public Boolean AoR() {
        return this.A0F;
    }

    public InlineActivityInfo Aq5() {
        return this.A02;
    }

    public Float AqQ() {
        return this.A0K;
    }

    public Boolean ArB() {
        return this.A0G;
    }

    public Boolean ArK() {
        return this.A0H;
    }

    public Boolean ArM() {
        return this.A0I;
    }

    public Long AsJ() {
        return this.A0L;
    }

    public String Atb() {
        return this.A0O;
    }

    public Boolean AuT() {
        return this.A0J;
    }

    public MontageActorInfo Auw() {
        return this.A04;
    }

    public String Auy() {
        return this.A0P;
    }

    public AnonymousClass4BB Auz() {
        return this.A05;
    }

    public String Av0() {
        return this.A0Q;
    }

    public ImmutableList Av3() {
        return this.A06;
    }

    public String Awb() {
        return this.A0R;
    }

    public ImmutableList B14() {
        return this.A07;
    }

    public ImmutableList B2p() {
        return this.A08;
    }

    public ImmutableList B2s() {
        return this.A09;
    }

    public StoryBackgroundInfo B47() {
        return this.A03;
    }

    public AnonymousClass2Vn B5f() {
        return this.A01;
    }

    public Long B5g() {
        return this.A0M;
    }

    public int hashCode() {
        return C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(1, this.A00), this.A0A), this.A0B), this.A0C), this.A0D), this.A0N), this.A0E), this.A0F), this.A02), this.A0K), this.A0G), this.A0H), this.A0I), this.A0L), this.A0O), this.A0J), this.A04), this.A0P), this.A05), this.A0Q), this.A06), this.A0R), this.A07), this.A08), this.A09), this.A03), this.A01), this.A0M);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.A00 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            AnonymousClass324.A0B(parcel, this.A00);
        }
        parcel.writeInt(this.A0A.booleanValue() ? 1 : 0);
        parcel.writeInt(this.A0B.booleanValue() ? 1 : 0);
        parcel.writeInt(this.A0C.booleanValue() ? 1 : 0);
        parcel.writeInt(this.A0D.booleanValue() ? 1 : 0);
        if (this.A0N == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A0N);
        }
        parcel.writeInt(this.A0E.booleanValue() ? 1 : 0);
        parcel.writeInt(this.A0F.booleanValue() ? 1 : 0);
        if (this.A02 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            this.A02.writeToParcel(parcel, i);
        }
        if (this.A0K == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeFloat(this.A0K.floatValue());
        }
        parcel.writeInt(this.A0G.booleanValue() ? 1 : 0);
        parcel.writeInt(this.A0H.booleanValue() ? 1 : 0);
        parcel.writeInt(this.A0I.booleanValue() ? 1 : 0);
        if (this.A0L == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeLong(this.A0L.longValue());
        }
        if (this.A0O == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A0O);
        }
        if (this.A0J == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A0J.booleanValue() ? 1 : 0);
        }
        if (this.A04 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A04, i);
        }
        if (this.A0P == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A0P);
        }
        if (this.A05 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            AnonymousClass324.A0B(parcel, this.A05);
        }
        if (this.A0Q == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A0Q);
        }
        parcel.writeInt(this.A06.size());
        C24971Xv it = this.A06.iterator();
        while (it.hasNext()) {
            parcel.writeParcelable((MontageXRaySmartFeature) it.next(), i);
        }
        if (this.A0R == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A0R);
        }
        parcel.writeInt(this.A07.size());
        C24971Xv it2 = this.A07.iterator();
        while (it2.hasNext()) {
            parcel.writeString((String) it2.next());
        }
        if (this.A08 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A08.size());
            C24971Xv it3 = this.A08.iterator();
            while (it3.hasNext()) {
                parcel.writeLong(((Long) it3.next()).longValue());
            }
        }
        if (this.A09 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeInt(this.A09.size());
            C24971Xv it4 = this.A09.iterator();
            while (it4.hasNext()) {
                AnonymousClass324.A0B(parcel, (AnonymousClass2W5) it4.next());
            }
        }
        if (this.A03 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            this.A03.writeToParcel(parcel, i);
        }
        if (this.A01 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            AnonymousClass324.A0B(parcel, this.A01);
        }
        if (this.A0M == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcel.writeLong(this.A0M.longValue());
    }

    public MontageMetadata(C33471nd r3) {
        this.A00 = r3.A00;
        Boolean bool = r3.A0A;
        C28931fb.A06(bool, "canMute");
        this.A0A = bool;
        Boolean bool2 = r3.A0B;
        C28931fb.A06(bool2, "canReply");
        this.A0B = bool2;
        Boolean bool3 = r3.A0C;
        C28931fb.A06(bool3, "canReport");
        this.A0C = bool3;
        Boolean bool4 = r3.A0D;
        C28931fb.A06(bool4, C99084oO.$const$string(13));
        this.A0D = bool4;
        this.A0N = r3.A0N;
        Boolean bool5 = r3.A0E;
        C28931fb.A06(bool5, C99084oO.$const$string(60));
        this.A0E = bool5;
        Boolean bool6 = r3.A0F;
        C28931fb.A06(bool6, C99084oO.$const$string(61));
        this.A0F = bool6;
        this.A02 = r3.A02;
        this.A0K = r3.A0K;
        Boolean bool7 = r3.A0G;
        C28931fb.A06(bool7, "isMyMontage");
        this.A0G = bool7;
        Boolean bool8 = r3.A0H;
        C28931fb.A06(bool8, "isReshareable");
        this.A0H = bool8;
        Boolean bool9 = r3.A0I;
        C28931fb.A06(bool9, "isUnread");
        this.A0I = bool9;
        this.A0L = r3.A0L;
        this.A0O = r3.A0O;
        this.A0J = r3.A0J;
        this.A04 = r3.A04;
        this.A0P = r3.A0P;
        this.A05 = r3.A05;
        this.A0Q = r3.A0Q;
        ImmutableList immutableList = r3.A06;
        C28931fb.A06(immutableList, "montageXRaySmartFeature");
        this.A06 = immutableList;
        this.A0R = r3.A0R;
        ImmutableList immutableList2 = r3.A07;
        C28931fb.A06(immutableList2, "reshareIntents");
        this.A07 = immutableList2;
        this.A08 = r3.A08;
        this.A09 = r3.A09;
        this.A03 = r3.A03;
        this.A01 = r3.A01;
        this.A0M = r3.A0M;
    }

    public MontageMetadata(Parcel parcel) {
        if (parcel.readInt() == 0) {
            this.A00 = null;
        } else {
            this.A00 = (C95324gE) AnonymousClass324.A03(parcel);
        }
        boolean z = true;
        this.A0A = Boolean.valueOf(parcel.readInt() == 1);
        this.A0B = Boolean.valueOf(parcel.readInt() == 1);
        this.A0C = Boolean.valueOf(parcel.readInt() == 1);
        this.A0D = Boolean.valueOf(parcel.readInt() == 1);
        if (parcel.readInt() == 0) {
            this.A0N = null;
        } else {
            this.A0N = parcel.readString();
        }
        this.A0E = Boolean.valueOf(parcel.readInt() == 1);
        this.A0F = Boolean.valueOf(parcel.readInt() == 1);
        if (parcel.readInt() == 0) {
            this.A02 = null;
        } else {
            this.A02 = (InlineActivityInfo) InlineActivityInfo.CREATOR.createFromParcel(parcel);
        }
        if (parcel.readInt() == 0) {
            this.A0K = null;
        } else {
            this.A0K = Float.valueOf(parcel.readFloat());
        }
        this.A0G = Boolean.valueOf(parcel.readInt() == 1);
        this.A0H = Boolean.valueOf(parcel.readInt() == 1);
        this.A0I = Boolean.valueOf(parcel.readInt() == 1);
        if (parcel.readInt() == 0) {
            this.A0L = null;
        } else {
            this.A0L = Long.valueOf(parcel.readLong());
        }
        if (parcel.readInt() == 0) {
            this.A0O = null;
        } else {
            this.A0O = parcel.readString();
        }
        if (parcel.readInt() == 0) {
            this.A0J = null;
        } else {
            this.A0J = Boolean.valueOf(parcel.readInt() != 1 ? false : z);
        }
        if (parcel.readInt() == 0) {
            this.A04 = null;
        } else {
            this.A04 = (MontageActorInfo) parcel.readParcelable(MontageActorInfo.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A0P = null;
        } else {
            this.A0P = parcel.readString();
        }
        if (parcel.readInt() == 0) {
            this.A05 = null;
        } else {
            this.A05 = (AnonymousClass4BB) AnonymousClass324.A03(parcel);
        }
        if (parcel.readInt() == 0) {
            this.A0Q = null;
        } else {
            this.A0Q = parcel.readString();
        }
        int readInt = parcel.readInt();
        MontageXRaySmartFeature[] montageXRaySmartFeatureArr = new MontageXRaySmartFeature[readInt];
        for (int i = 0; i < readInt; i++) {
            montageXRaySmartFeatureArr[i] = (MontageXRaySmartFeature) parcel.readParcelable(MontageXRaySmartFeature.class.getClassLoader());
        }
        this.A06 = ImmutableList.copyOf(montageXRaySmartFeatureArr);
        if (parcel.readInt() == 0) {
            this.A0R = null;
        } else {
            this.A0R = parcel.readString();
        }
        int readInt2 = parcel.readInt();
        String[] strArr = new String[readInt2];
        for (int i2 = 0; i2 < readInt2; i2++) {
            strArr[i2] = parcel.readString();
        }
        this.A07 = ImmutableList.copyOf(strArr);
        if (parcel.readInt() == 0) {
            this.A08 = null;
        } else {
            int readInt3 = parcel.readInt();
            Long[] lArr = new Long[readInt3];
            for (int i3 = 0; i3 < readInt3; i3++) {
                lArr[i3] = Long.valueOf(parcel.readLong());
            }
            this.A08 = ImmutableList.copyOf(lArr);
        }
        if (parcel.readInt() == 0) {
            this.A09 = null;
        } else {
            int readInt4 = parcel.readInt();
            AnonymousClass2W5[] r1 = new AnonymousClass2W5[readInt4];
            for (int i4 = 0; i4 < readInt4; i4++) {
                r1[i4] = (AnonymousClass2W5) AnonymousClass324.A03(parcel);
            }
            this.A09 = ImmutableList.copyOf(r1);
        }
        if (parcel.readInt() == 0) {
            this.A03 = null;
        } else {
            this.A03 = (StoryBackgroundInfo) StoryBackgroundInfo.CREATOR.createFromParcel(parcel);
        }
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = (AnonymousClass2Vn) AnonymousClass324.A03(parcel);
        }
        if (parcel.readInt() == 0) {
            this.A0M = null;
        } else {
            this.A0M = Long.valueOf(parcel.readLong());
        }
    }
}
