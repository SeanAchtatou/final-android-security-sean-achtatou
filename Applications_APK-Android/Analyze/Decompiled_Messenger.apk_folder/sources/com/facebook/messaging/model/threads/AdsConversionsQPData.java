package com.facebook.messaging.model.threads;

import X.AnonymousClass0tY;
import X.C11260mU;
import X.C11710np;
import X.C14550ta;
import X.C182811d;
import X.C184513k;
import X.C22298Ase;
import X.C26791c3;
import X.C28271eX;
import X.C28931fb;
import X.C29561gc;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.acra.ACRA;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.webrtc.audio.WebRtcAudioRecord;

@JsonDeserialize(using = Deserializer.class)
@JsonSerialize(using = Serializer.class)
public final class AdsConversionsQPData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C184513k();
    public final double A00;
    public final long A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final boolean A07;

    public class Deserializer extends JsonDeserializer {
        public /* bridge */ /* synthetic */ Object deserialize(C28271eX r5, C26791c3 r6) {
            C29561gc r2 = new C29561gc();
            do {
                try {
                    if (r5.getCurrentToken() == C182811d.FIELD_NAME) {
                        String currentName = r5.getCurrentName();
                        r5.nextToken();
                        char c = 65535;
                        switch (currentName.hashCode()) {
                            case -634286772:
                                if (currentName.equals("is_eligible")) {
                                    c = 5;
                                    break;
                                }
                                break;
                            case 55126294:
                                if (currentName.equals("timestamp")) {
                                    c = 7;
                                    break;
                                }
                                break;
                            case 277108806:
                                if (currentName.equals(C22298Ase.$const$string(9))) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 624279747:
                                if (currentName.equals("conversion_type")) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case 928726729:
                                if (currentName.equals("icebreaker_message")) {
                                    c = 4;
                                    break;
                                }
                                break;
                            case 1108728155:
                                if (currentName.equals(C22298Ase.$const$string(10))) {
                                    c = 2;
                                    break;
                                }
                                break;
                            case 1548979745:
                                if (currentName.equals("icebreaker_key")) {
                                    c = 3;
                                    break;
                                }
                                break;
                            case 1628467514:
                                if (currentName.equals("page_reply")) {
                                    c = 6;
                                    break;
                                }
                                break;
                        }
                        switch (c) {
                            case 0:
                                String A02 = C14550ta.A02(r5);
                                r2.A02 = A02;
                                C28931fb.A06(A02, "conversionType");
                                break;
                            case 1:
                                r2.A00 = r5.getValueAsDouble();
                                break;
                            case 2:
                                String A022 = C14550ta.A02(r5);
                                r2.A03 = A022;
                                C28931fb.A06(A022, "currencyCode");
                                break;
                            case 3:
                                r2.A04 = C14550ta.A02(r5);
                                break;
                            case 4:
                                r2.A05 = C14550ta.A02(r5);
                                break;
                            case 5:
                                r2.A07 = r5.getValueAsBoolean();
                                break;
                            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                                r2.A06 = C14550ta.A02(r5);
                                break;
                            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                                r2.A01 = r5.getValueAsLong();
                                break;
                            default:
                                r5.skipChildren();
                                break;
                        }
                    }
                } catch (Exception e) {
                    C14550ta.A0D(AdsConversionsQPData.class, r5, e);
                }
            } while (AnonymousClass0tY.A00(r5) != C182811d.END_OBJECT);
            return new AdsConversionsQPData(r2);
        }
    }

    public class Serializer extends JsonSerializer {
        public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r5, C11260mU r6) {
            AdsConversionsQPData adsConversionsQPData = (AdsConversionsQPData) obj;
            r5.writeStartObject();
            C14550ta.A0B(r5, "conversion_type", adsConversionsQPData.A02);
            double d = adsConversionsQPData.A00;
            r5.writeFieldName(C22298Ase.$const$string(9));
            r5.writeNumber(d);
            C14550ta.A0B(r5, C22298Ase.$const$string(10), adsConversionsQPData.A03);
            C14550ta.A0B(r5, "icebreaker_key", adsConversionsQPData.A04);
            C14550ta.A0B(r5, "icebreaker_message", adsConversionsQPData.A05);
            C14550ta.A0C(r5, "is_eligible", adsConversionsQPData.A07);
            C14550ta.A0B(r5, "page_reply", adsConversionsQPData.A06);
            long j = adsConversionsQPData.A01;
            r5.writeFieldName("timestamp");
            r5.writeNumber(j);
            r5.writeEndObject();
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AdsConversionsQPData) {
                AdsConversionsQPData adsConversionsQPData = (AdsConversionsQPData) obj;
                if (!C28931fb.A07(this.A02, adsConversionsQPData.A02) || this.A00 != adsConversionsQPData.A00 || !C28931fb.A07(this.A03, adsConversionsQPData.A03) || !C28931fb.A07(this.A04, adsConversionsQPData.A04) || !C28931fb.A07(this.A05, adsConversionsQPData.A05) || this.A07 != adsConversionsQPData.A07 || !C28931fb.A07(this.A06, adsConversionsQPData.A06) || this.A01 != adsConversionsQPData.A01) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A02(C28931fb.A03(C28931fb.A04(C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A00(C28931fb.A03(1, this.A02), this.A00), this.A03), this.A04), this.A05), this.A07), this.A06), this.A01);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeDouble(this.A00);
        parcel.writeString(this.A03);
        if (this.A04 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A04);
        }
        if (this.A05 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A05);
        }
        parcel.writeInt(this.A07 ? 1 : 0);
        if (this.A06 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A06);
        }
        parcel.writeLong(this.A01);
    }

    public AdsConversionsQPData(C29561gc r3) {
        String str = r3.A02;
        C28931fb.A06(str, "conversionType");
        this.A02 = str;
        this.A00 = r3.A00;
        String str2 = r3.A03;
        C28931fb.A06(str2, "currencyCode");
        this.A03 = str2;
        this.A04 = r3.A04;
        this.A05 = r3.A05;
        this.A07 = r3.A07;
        this.A06 = r3.A06;
        this.A01 = r3.A01;
    }

    public AdsConversionsQPData(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A00 = parcel.readDouble();
        this.A03 = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A04 = null;
        } else {
            this.A04 = parcel.readString();
        }
        if (parcel.readInt() == 0) {
            this.A05 = null;
        } else {
            this.A05 = parcel.readString();
        }
        this.A07 = parcel.readInt() != 1 ? false : true;
        if (parcel.readInt() == 0) {
            this.A06 = null;
        } else {
            this.A06 = parcel.readString();
        }
        this.A01 = parcel.readLong();
    }
}
