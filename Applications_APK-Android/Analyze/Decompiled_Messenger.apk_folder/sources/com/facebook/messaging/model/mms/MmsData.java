package com.facebook.messaging.model.mms;

import X.C24971Xv;
import X.C36921u9;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Collection;

public final class MmsData implements Parcelable {
    public static final MmsData A04;
    public static final ImmutableList A05;
    public static final Parcelable.Creator CREATOR = new C36921u9();
    public final long A00;
    public final long A01;
    public final ImmutableList A02;
    public final ImmutableList A03;

    public static MmsData A00(ImmutableList immutableList, ImmutableList immutableList2) {
        int i = 0;
        boolean z = false;
        if (immutableList != null) {
            z = true;
        }
        Preconditions.checkArgument(z);
        Preconditions.checkArgument(true ^ immutableList.isEmpty());
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            i = (int) (((long) i) + ((MediaResource) it.next()).A06);
        }
        return new MmsData(0, (long) i, immutableList, immutableList2);
    }

    public int describeContents() {
        return 0;
    }

    static {
        ImmutableList immutableList = RegularImmutableList.A02;
        A05 = immutableList;
        A04 = new MmsData(-1, -1, immutableList, immutableList);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r6.A02.isEmpty() == false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01() {
        /*
            r6 = this;
            long r3 = r6.A00
            com.facebook.messaging.model.mms.MmsData r5 = com.facebook.messaging.model.mms.MmsData.A04
            long r1 = r5.A00
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x001b
            long r3 = r6.A01
            long r1 = r5.A01
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x001b
            com.google.common.collect.ImmutableList r0 = r6.A02
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 != 0) goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            if (r0 != 0) goto L_0x0027
            com.google.common.collect.ImmutableList r0 = r6.A02
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 != 0) goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.mms.MmsData.A01():boolean");
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A00);
        parcel.writeLong(this.A01);
        parcel.writeList(this.A02);
        C417826y.A0I(parcel, this.A03);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0016, code lost:
        if (r9.size() == r8.size()) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MmsData(long r4, long r6, com.google.common.collect.ImmutableList r8, com.google.common.collect.ImmutableList r9) {
        /*
            r3 = this;
            r3.<init>()
            r3.A00 = r4
            r3.A01 = r6
            r3.A02 = r8
            r3.A03 = r9
            if (r9 == 0) goto L_0x0018
            int r2 = r9.size()
            int r1 = r8.size()
            r0 = 0
            if (r2 != r1) goto L_0x0019
        L_0x0018:
            r0 = 1
        L_0x0019:
            com.google.common.base.Preconditions.checkArgument(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.mms.MmsData.<init>(long, long, com.google.common.collect.ImmutableList, com.google.common.collect.ImmutableList):void");
    }

    public MmsData(Parcel parcel) {
        this.A00 = parcel.readLong();
        this.A01 = parcel.readLong();
        this.A02 = ImmutableList.copyOf((Collection) parcel.readArrayList(MediaResource.class.getClassLoader()));
        this.A03 = C417826y.A06(parcel, MediaResource.CREATOR);
    }
}
