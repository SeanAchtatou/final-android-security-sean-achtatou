package com.facebook.messaging.model.threads;

import X.AnonymousClass103;
import X.C18050zz;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.collect.ImmutableList;
import java.util.Collection;

public final class GroupApprovalInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C18050zz();
    public final ImmutableList A00;
    public final boolean A01;
    public final boolean A02;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            GroupApprovalInfo groupApprovalInfo = (GroupApprovalInfo) obj;
            if (!(this.A02 == groupApprovalInfo.A02 && this.A01 == groupApprovalInfo.A01 && this.A00.equals(groupApprovalInfo.A00))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        boolean z = this.A02;
        boolean z2 = this.A01;
        return (((((z ? 1 : 0) + true) * 31) + (z2 ? 1 : 0)) * 31) + this.A00.hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        C417826y.A0V(parcel, this.A02);
        C417826y.A0V(parcel, this.A01);
        parcel.writeTypedList(this.A00);
    }

    public GroupApprovalInfo(AnonymousClass103 r2) {
        this.A02 = r2.A02;
        this.A01 = r2.A01;
        this.A00 = r2.A00;
    }

    public GroupApprovalInfo(Parcel parcel) {
        this.A02 = C417826y.A0W(parcel);
        this.A01 = C417826y.A0W(parcel);
        this.A00 = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ThreadJoinRequest.CREATOR));
    }
}
