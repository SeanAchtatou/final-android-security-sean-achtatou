package com.facebook.messaging.model.threads;

import X.C06850cB;
import X.C29231g5;
import X.C29241g6;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.MoreObjects;

public final class ThreadCustomization implements Parcelable {
    public static final ThreadCustomization A02 = new ThreadCustomization();
    public static final Parcelable.Creator CREATOR = new C29241g6();
    public final NicknamesMap A00;
    public final String A01;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ThreadCustomization threadCustomization = (ThreadCustomization) obj;
            if (!C06850cB.A0C(this.A01, threadCustomization.A01) || !this.A00.equals(threadCustomization.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i;
        String str = this.A01;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        return (i * 31) + this.A00.hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeParcelable(this.A00, i);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("emojilikeString", this.A01);
        stringHelper.add("nicknamesMap", this.A00);
        return stringHelper.toString();
    }

    public ThreadCustomization() {
        this.A01 = null;
        this.A00 = new NicknamesMap();
    }

    public ThreadCustomization(C29231g5 r2) {
        this.A01 = r2.A01;
        this.A00 = r2.A00;
    }

    public ThreadCustomization(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = (NicknamesMap) parcel.readParcelable(NicknamesMap.class.getClassLoader());
    }
}
