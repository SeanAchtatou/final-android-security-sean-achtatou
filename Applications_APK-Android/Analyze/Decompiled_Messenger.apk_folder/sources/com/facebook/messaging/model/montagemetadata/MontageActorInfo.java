package com.facebook.messaging.model.montagemetadata;

import X.AnonymousClass0tY;
import X.AnonymousClass1uE;
import X.AnonymousClass2UY;
import X.C11260mU;
import X.C11710np;
import X.C14550ta;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C28931fb;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.profilepic.PicSquare;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonDeserialize(using = Deserializer.class)
@JsonSerialize(using = Serializer.class)
public final class MontageActorInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1uE();
    public final PicSquare A00;
    public final String A01;
    public final String A02;
    public final String A03;

    public class Deserializer extends JsonDeserializer {
        public /* bridge */ /* synthetic */ Object deserialize(C28271eX r8, C26791c3 r9) {
            AnonymousClass2UY r2 = new AnonymousClass2UY();
            do {
                try {
                    if (r8.getCurrentToken() == C182811d.FIELD_NAME) {
                        String currentName = r8.getCurrentName();
                        r8.nextToken();
                        char c = 65535;
                        switch (currentName.hashCode()) {
                            case -1340043452:
                                if (currentName.equals("actor_type")) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case 3373707:
                                if (currentName.equals("name")) {
                                    c = 2;
                                    break;
                                }
                                break;
                            case 1561734725:
                                if (currentName.equals("fallback_profile_pic_square")) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 1565793390:
                                if (currentName.equals("short_name")) {
                                    c = 3;
                                    break;
                                }
                                break;
                        }
                        if (c == 0) {
                            String A02 = C14550ta.A02(r8);
                            r2.A01 = A02;
                            C28931fb.A06(A02, "actorType");
                        } else if (c == 1) {
                            r2.A00 = (PicSquare) C14550ta.A01(PicSquare.class, r8, r9);
                        } else if (c == 2) {
                            r2.A02 = C14550ta.A02(r8);
                        } else if (c != 3) {
                            r8.skipChildren();
                        } else {
                            r2.A03 = C14550ta.A02(r8);
                        }
                    }
                } catch (Exception e) {
                    C14550ta.A0D(MontageActorInfo.class, r8, e);
                }
            } while (AnonymousClass0tY.A00(r8) != C182811d.END_OBJECT);
            return new MontageActorInfo(r2);
        }
    }

    public class Serializer extends JsonSerializer {
        public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
            MontageActorInfo montageActorInfo = (MontageActorInfo) obj;
            r4.writeStartObject();
            C14550ta.A0B(r4, "actor_type", montageActorInfo.A01);
            C14550ta.A04(r4, r5, "fallback_profile_pic_square", montageActorInfo.A00);
            C14550ta.A0B(r4, "name", montageActorInfo.A02);
            C14550ta.A0B(r4, "short_name", montageActorInfo.A03);
            r4.writeEndObject();
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MontageActorInfo) {
                MontageActorInfo montageActorInfo = (MontageActorInfo) obj;
                if (!C28931fb.A07(this.A01, montageActorInfo.A01) || !C28931fb.A07(this.A00, montageActorInfo.A00) || !C28931fb.A07(this.A02, montageActorInfo.A02) || !C28931fb.A07(this.A03, montageActorInfo.A03)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A03(C28931fb.A03(C28931fb.A03(C28931fb.A03(1, this.A01), this.A00), this.A02), this.A03);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        if (this.A00 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeParcelable(this.A00, i);
        }
        if (this.A02 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A02);
        }
        if (this.A03 == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcel.writeString(this.A03);
    }

    public MontageActorInfo(AnonymousClass2UY r3) {
        String str = r3.A01;
        C28931fb.A06(str, "actorType");
        this.A01 = str;
        this.A00 = r3.A00;
        this.A02 = r3.A02;
        this.A03 = r3.A03;
    }

    public MontageActorInfo(Parcel parcel) {
        this.A01 = parcel.readString();
        if (parcel.readInt() == 0) {
            this.A00 = null;
        } else {
            this.A00 = (PicSquare) parcel.readParcelable(PicSquare.class.getClassLoader());
        }
        if (parcel.readInt() == 0) {
            this.A02 = null;
        } else {
            this.A02 = parcel.readString();
        }
        if (parcel.readInt() == 0) {
            this.A03 = null;
        } else {
            this.A03 = parcel.readString();
        }
    }
}
