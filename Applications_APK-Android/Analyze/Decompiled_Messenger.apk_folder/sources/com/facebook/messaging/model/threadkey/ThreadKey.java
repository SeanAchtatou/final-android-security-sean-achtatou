package com.facebook.messaging.model.threadkey;

import X.AnonymousClass08S;
import X.C28701fE;
import X.C28711fF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.facebook.acra.ACRA;
import com.facebook.user.model.UserKey;
import org.webrtc.audio.WebRtcAudioRecord;

public final class ThreadKey implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C28701fE();
    private String A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final C28711fF A05;

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.messaging.model.threadkey.ThreadKey A06(java.lang.String r11) {
        /*
            r0 = 0
            if (r11 == 0) goto L_0x00ce
            java.lang.String r1 = ":"
            java.lang.String[] r2 = r11.split(r1)
            int r7 = r2.length
            r6 = 1
            if (r7 < r6) goto L_0x00ce
            r1 = 0
            r1 = r2[r1]     // Catch:{ Exception -> 0x00ca }
            X.1fF r4 = X.C28711fF.valueOf(r1)     // Catch:{ Exception -> 0x00ca }
            X.1fF r1 = X.C28711fF.ONE_TO_ONE     // Catch:{ Exception -> 0x00ca }
            r3 = 3
            r5 = 2
            if (r4 != r1) goto L_0x002e
            if (r7 != r3) goto L_0x002e
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r3 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            r1 = r2[r5]     // Catch:{ Exception -> 0x00ca }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = A04(r3, r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00ca
        L_0x002e:
            X.1fF r1 = X.C28711fF.GROUP     // Catch:{ Exception -> 0x00ca }
            if (r4 != r1) goto L_0x0040
            if (r7 != r5) goto L_0x0040
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = A00(r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00ca
        L_0x0040:
            X.1fF r1 = X.C28711fF.MONTAGE     // Catch:{ Exception -> 0x00ca }
            if (r4 != r1) goto L_0x0051
            if (r7 != r5) goto L_0x0051
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = A01(r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00ca
        L_0x0051:
            X.1fF r1 = X.C28711fF.TINCAN     // Catch:{ Exception -> 0x00ca }
            if (r4 == r1) goto L_0x0059
            X.1fF r1 = X.C28711fF.TINCAN_MULTI_ENDPOINT     // Catch:{ Exception -> 0x00ca }
            if (r4 != r1) goto L_0x0074
        L_0x0059:
            if (r7 == r5) goto L_0x005d
            if (r7 != r3) goto L_0x0074
        L_0x005d:
            if (r7 != r3) goto L_0x0060
            goto L_0x0063
        L_0x0060:
            r3 = 0
            goto L_0x0069
        L_0x0063:
            r1 = r2[r5]     // Catch:{ Exception -> 0x00ca }
            long r3 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
        L_0x0069:
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = A05(r1, r3)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00ca
        L_0x0074:
            X.1fF r1 = X.C28711fF.OPTIMISTIC_GROUP_THREAD     // Catch:{ Exception -> 0x00ca }
            if (r4 != r1) goto L_0x0085
            if (r7 != r5) goto L_0x0085
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = A02(r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00ca
        L_0x0085:
            X.1fF r1 = X.C28711fF.PENDING_THREAD     // Catch:{ Exception -> 0x00ca }
            if (r4 != r1) goto L_0x009f
            if (r7 != r5) goto L_0x009f
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r9 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = new com.facebook.messaging.model.threadkey.ThreadKey     // Catch:{ Exception -> 0x00ca }
            X.1fF r2 = X.C28711fF.PENDING_THREAD     // Catch:{ Exception -> 0x00ca }
            r3 = -1
            r5 = -1
            r7 = -1
            r1.<init>(r2, r3, r5, r7, r9)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00c9
        L_0x009f:
            X.1fF r1 = X.C28711fF.SMS     // Catch:{ Exception -> 0x00ca }
            if (r4 != r1) goto L_0x00b0
            if (r7 != r5) goto L_0x00b0
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = A03(r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x00ca
        L_0x00b0:
            X.1fF r1 = X.C28711fF.PENDING_GENERAL_THREAD     // Catch:{ Exception -> 0x00ca }
            if (r4 != r1) goto L_0x00ca
            if (r7 != r5) goto L_0x00ca
            r1 = r2[r6]     // Catch:{ Exception -> 0x00ca }
            long r9 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x00ca }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = new com.facebook.messaging.model.threadkey.ThreadKey     // Catch:{ Exception -> 0x00ca }
            X.1fF r2 = X.C28711fF.PENDING_GENERAL_THREAD     // Catch:{ Exception -> 0x00ca }
            r3 = -1
            r5 = -1
            r7 = -1
            r1.<init>(r2, r3, r5, r7, r9)     // Catch:{ Exception -> 0x00ca }
        L_0x00c9:
            r0 = r1
        L_0x00ca:
            if (r0 == 0) goto L_0x00ce
            r0.A00 = r11
        L_0x00ce:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.threadkey.ThreadKey.A06(java.lang.String):com.facebook.messaging.model.threadkey.ThreadKey");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                ThreadKey threadKey = (ThreadKey) obj;
                if (!(this.A05 == threadKey.A05 && this.A01 == threadKey.A01 && this.A03 == threadKey.A03 && this.A04 == threadKey.A04 && this.A02 == threadKey.A02)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public static ThreadKey A00(long j) {
        return new ThreadKey(C28711fF.GROUP, j, -1, -1, -1);
    }

    public static ThreadKey A01(long j) {
        return new ThreadKey(C28711fF.MONTAGE, j, -1, -1, -1);
    }

    public static ThreadKey A02(long j) {
        return new ThreadKey(C28711fF.OPTIMISTIC_GROUP_THREAD, -1, -1, -1, j);
    }

    public static ThreadKey A03(long j) {
        return new ThreadKey(C28711fF.SMS, j, -1, -1, -1);
    }

    public static ThreadKey A04(long j, long j2) {
        return new ThreadKey(C28711fF.ONE_TO_ONE, -1, j, j2, -1);
    }

    public static ThreadKey A05(long j, long j2) {
        return new ThreadKey(C28711fF.TINCAN, j, j, j2, -1);
    }

    public static UserKey A07(ThreadKey threadKey) {
        if (threadKey == null) {
            return null;
        }
        if (threadKey.A05 == C28711fF.ONE_TO_ONE || A0F(threadKey)) {
            return UserKey.A01(Long.toString(threadKey.A01));
        }
        return null;
    }

    public static String A08(ThreadKey threadKey) {
        return Base64.encodeToString(AnonymousClass08S.A0G("message_thread:", threadKey.A0G()).getBytes(), 11);
    }

    public static boolean A09(ThreadKey threadKey) {
        if (threadKey == null || threadKey.A05 != C28711fF.GROUP) {
            return false;
        }
        return true;
    }

    public static boolean A0A(ThreadKey threadKey) {
        if (threadKey == null) {
            return false;
        }
        C28711fF r1 = threadKey.A05;
        if (r1 == C28711fF.GROUP || r1 == C28711fF.PENDING_THREAD || r1 == C28711fF.OPTIMISTIC_GROUP_THREAD) {
            return true;
        }
        return false;
    }

    public static boolean A0B(ThreadKey threadKey) {
        if (threadKey == null || threadKey.A05 != C28711fF.ONE_TO_ONE) {
            return false;
        }
        return true;
    }

    public static boolean A0C(ThreadKey threadKey) {
        if (threadKey == null || threadKey.A05 != C28711fF.OPTIMISTIC_GROUP_THREAD) {
            return false;
        }
        return true;
    }

    public static boolean A0D(ThreadKey threadKey) {
        if (threadKey == null) {
            return false;
        }
        C28711fF r1 = threadKey.A05;
        if (r1 == C28711fF.PENDING_THREAD || r1 == C28711fF.PENDING_GENERAL_THREAD) {
            return true;
        }
        return false;
    }

    public static boolean A0E(ThreadKey threadKey) {
        if (threadKey == null || threadKey.A05 != C28711fF.SMS) {
            return false;
        }
        return true;
    }

    public static boolean A0F(ThreadKey threadKey) {
        if (threadKey == null) {
            return false;
        }
        C28711fF r1 = threadKey.A05;
        if (r1 == C28711fF.TINCAN || r1 == C28711fF.TINCAN_MULTI_ENDPOINT) {
            return true;
        }
        return false;
    }

    public long A0G() {
        if (this.A05 == C28711fF.ONE_TO_ONE) {
            return this.A01;
        }
        return this.A03;
    }

    public String A0H() {
        String str;
        switch (this.A05.ordinal()) {
            case 0:
                long j = this.A04;
                long j2 = this.A01;
                return AnonymousClass08S.A05(Math.min(j, j2), "u", Math.max(j, j2));
            case 1:
                str = "g";
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            default:
                return "unknown";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                str = "s";
                break;
            case 8:
                str = "m";
                break;
        }
        return AnonymousClass08S.A0G(str, this.A03);
    }

    public String A0J() {
        StringBuilder sb;
        String str;
        String str2;
        long j;
        if (this.A00 == null) {
            C28711fF r1 = this.A05;
            switch (r1.ordinal()) {
                case 0:
                    sb = new StringBuilder();
                    str = r1.name();
                    sb.append(str);
                    sb.append(":");
                    sb.append(this.A01);
                    sb.append(":");
                    sb.append(this.A04);
                    this.A00 = sb.toString();
                    break;
                case 1:
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                case 8:
                    str2 = r1.name();
                    j = this.A03;
                    this.A00 = AnonymousClass08S.A0O(str2, ":", j);
                    break;
                case 2:
                case 3:
                    sb = new StringBuilder();
                    str = C28711fF.TINCAN.name();
                    sb.append(str);
                    sb.append(":");
                    sb.append(this.A01);
                    sb.append(":");
                    sb.append(this.A04);
                    this.A00 = sb.toString();
                    break;
                case 4:
                case 5:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    str2 = r1.name();
                    j = this.A02;
                    this.A00 = AnonymousClass08S.A0O(str2, ":", j);
                    break;
                default:
                    this.A00 = "UNKNOWN_TYPE";
                    break;
            }
        }
        return this.A00;
    }

    public boolean A0K() {
        C28711fF r2 = this.A05;
        if (r2 == C28711fF.TINCAN || r2 == C28711fF.TINCAN_MULTI_ENDPOINT) {
            return true;
        }
        return false;
    }

    public boolean A0L() {
        if (this.A05 == C28711fF.GROUP) {
            return true;
        }
        return false;
    }

    public boolean A0M() {
        if (this.A05 == C28711fF.MONTAGE) {
            return true;
        }
        return false;
    }

    public boolean A0N() {
        if (this.A05 == C28711fF.ONE_TO_ONE) {
            return true;
        }
        return false;
    }

    public boolean A0O() {
        if (this.A05 == C28711fF.OPTIMISTIC_GROUP_THREAD) {
            return true;
        }
        return false;
    }

    public boolean A0P() {
        C28711fF r2 = this.A05;
        if (r2 != C28711fF.PENDING_THREAD) {
            boolean z = false;
            if (r2 == C28711fF.PENDING_GENERAL_THREAD) {
                z = true;
            }
            if (z) {
                return true;
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        long j = this.A03;
        long j2 = this.A01;
        long j3 = this.A04;
        long j4 = this.A02;
        return (((((((this.A05.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)));
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A05.name());
        parcel.writeLong(this.A03);
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A04);
        parcel.writeLong(this.A02);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003b, code lost:
        if (r9 > -1) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0049, code lost:
        if (r9 == -1) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        com.google.common.base.Preconditions.checkArgument(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        if (r11 != -1) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0061, code lost:
        if (r9 == r7) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0063, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        com.google.common.base.Preconditions.checkArgument(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0069, code lost:
        if (r11 <= -1) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006b, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006c, code lost:
        com.google.common.base.Preconditions.checkArgument(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0071, code lost:
        if (r13 != -1) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0074, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0076, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0095, code lost:
        if (r13 > -1) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009b, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002d, code lost:
        if (r9 == -1) goto L_0x004b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ThreadKey(X.C28711fF r6, long r7, long r9, long r11, long r13) {
        /*
            r5 = this;
            r5.<init>()
            r5.A05 = r6
            r5.A03 = r7
            r5.A01 = r9
            r5.A04 = r11
            r5.A02 = r13
            int r0 = r6.ordinal()
            r3 = -1
            r2 = 1
            switch(r0) {
                case 0: goto L_0x0030;
                case 1: goto L_0x003e;
                case 2: goto L_0x0056;
                case 3: goto L_0x0017;
                case 4: goto L_0x0078;
                case 5: goto L_0x0078;
                case 6: goto L_0x002b;
                case 7: goto L_0x0078;
                case 8: goto L_0x003e;
                default: goto L_0x0017;
            }
        L_0x0017:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Unsupported ThreadKey type: "
            r1.<init>(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x002b:
            int r0 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0054
            goto L_0x004b
        L_0x0030:
            int r1 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x0036
            r0 = 1
        L_0x0036:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r0 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0076
            goto L_0x0063
        L_0x003e:
            int r1 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x0044
            r0 = 1
        L_0x0044:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r0 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0054
        L_0x004b:
            r0 = 1
        L_0x004c:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r0 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0074
            goto L_0x006b
        L_0x0054:
            r0 = 0
            goto L_0x004c
        L_0x0056:
            int r1 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x005c
            r0 = 1
        L_0x005c:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r0 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x0076
        L_0x0063:
            r0 = 1
        L_0x0064:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r0 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0074
        L_0x006b:
            r0 = 1
        L_0x006c:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r0 = (r13 > r3 ? 1 : (r13 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x009b
            goto L_0x0097
        L_0x0074:
            r0 = 0
            goto L_0x006c
        L_0x0076:
            r0 = 0
            goto L_0x0064
        L_0x0078:
            int r1 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x007e
            r0 = 1
        L_0x007e:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r1 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x0087
            r0 = 1
        L_0x0087:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r1 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x0090
            r0 = 1
        L_0x0090:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r0 = (r13 > r3 ? 1 : (r13 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x009b
        L_0x0097:
            com.google.common.base.Preconditions.checkArgument(r2)
            return
        L_0x009b:
            r2 = 0
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.threadkey.ThreadKey.<init>(X.1fF, long, long, long, long):void");
    }

    public String A0I() {
        return String.valueOf(A0G());
    }

    public boolean A0Q() {
        if (A0L() || A0M() || this.A01 != this.A04) {
            return false;
        }
        return true;
    }

    public String toString() {
        return A0J();
    }
}
