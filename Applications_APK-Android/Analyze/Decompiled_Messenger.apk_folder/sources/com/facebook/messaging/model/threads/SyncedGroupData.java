package com.facebook.messaging.model.threads;

import X.C16850xs;
import X.C16860xt;
import X.C28931fb;
import android.os.Parcel;
import android.os.Parcelable;

public final class SyncedGroupData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C16850xs();
    public final long A00;
    public final String A01;
    public final boolean A02;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SyncedGroupData) {
                SyncedGroupData syncedGroupData = (SyncedGroupData) obj;
                if (!(this.A00 == syncedGroupData.A00 && C28931fb.A07(this.A01, syncedGroupData.A01) && this.A02 == syncedGroupData.A02)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A04(C28931fb.A03(C28931fb.A02(1, this.A00), this.A01), this.A02);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A00);
        if (this.A01 == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            parcel.writeString(this.A01);
        }
        parcel.writeInt(this.A02 ? 1 : 0);
    }

    public SyncedGroupData(C16860xt r3) {
        this.A00 = r3.A00;
        this.A01 = r3.A01;
        this.A02 = r3.A02;
    }

    public SyncedGroupData(Parcel parcel) {
        this.A00 = parcel.readLong();
        if (parcel.readInt() == 0) {
            this.A01 = null;
        } else {
            this.A01 = parcel.readString();
        }
        this.A02 = parcel.readInt() != 1 ? false : true;
    }
}
