package com.facebook.messaging.model.messages;

import X.AnonymousClass08S;
import X.AnonymousClass0TG;
import X.AnonymousClass1R6;
import X.AnonymousClass1TG;
import X.AnonymousClass1V7;
import X.AnonymousClass324;
import X.B5H;
import X.C04300To;
import X.C06850cB;
import X.C117335hD;
import X.C21936Al6;
import X.C24971Xv;
import X.C28841fS;
import X.C33391nV;
import X.C36871u1;
import X.C36881u2;
import X.C417826y;
import X.C61192yU;
import X.C69333Wq;
import X.C72033dY;
import X.C98894o1;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.messaging.business.commerce.model.retail.CommerceData;
import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.attribution.ContentAppAttribution;
import com.facebook.messaging.model.messagemetadata.MessageMetadataAtTextRange;
import com.facebook.messaging.model.messagemetadata.PlatformMetadata;
import com.facebook.messaging.model.messages.montageattribution.MontageAttributionData;
import com.facebook.messaging.model.mms.MmsData;
import com.facebook.messaging.model.montagemetadata.MontageMetadata;
import com.facebook.messaging.model.payment.PaymentRequestData;
import com.facebook.messaging.model.payment.PaymentTransactionData;
import com.facebook.messaging.model.send.PendingSendQueueKey;
import com.facebook.messaging.model.send.SendError;
import com.facebook.messaging.model.share.SentShareAttachment;
import com.facebook.messaging.model.share.Share;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.translation.model.MessageTranslation;
import com.facebook.share.model.ComposerAppAttribution;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class Message implements Parcelable, C36871u1 {
    public static final Parcelable.Creator CREATOR = new C33391nV();
    public final int A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final CommerceData A05;
    public final C61192yU A06;
    public final ContentAppAttribution A07;
    public final C72033dY A08;
    public final GenericAdminMessageInfo A09;
    public final C36881u2 A0A;
    public final AnonymousClass1R6 A0B;
    public final MessageRepliedTo A0C;
    public final AnonymousClass1V7 A0D;
    public final MontageBrandedCameraAttributionData A0E;
    public final C98894o1 A0F;
    public final C117335hD A0G;
    public final C117335hD A0H;
    public final C21936Al6 A0I;
    public final ParticipantInfo A0J;
    public final ParticipantInfo A0K;
    public final Publicity A0L;
    public final MontageAttributionData A0M;
    public final MmsData A0N;
    public final MontageMetadata A0O;
    public final PaymentRequestData A0P;
    public final PaymentTransactionData A0Q;
    public final PendingSendQueueKey A0R;
    public final SendError A0S;
    public final SentShareAttachment A0T;
    public final ThreadKey A0U;
    public final ComposerAppAttribution A0V;
    public final ImmutableList A0W;
    public final ImmutableList A0X;
    public final ImmutableList A0Y;
    public final ImmutableList A0Z;
    public final ImmutableList A0a;
    public final ImmutableList A0b;
    public final ImmutableList A0c;
    public final ImmutableList A0d;
    public final ImmutableList A0e;
    public final ImmutableMap A0f;
    public final ImmutableMap A0g;
    public final ImmutableMap A0h;
    public final ImmutableMap A0i;
    public final ImmutableMultimap A0j;
    public final ImmutableMultimap A0k;
    public final Integer A0l;
    public final Long A0m;
    public final String A0n;
    public final String A0o;
    public final String A0p;
    public final String A0q;
    public final String A0r;
    public final String A0s;
    public final String A0t;
    public final String A0u;
    public final String A0v;
    public final String A0w;
    public final String A0x;
    public final String A0y;
    public final String A0z;
    public final String A10;
    public final String A11;
    public final boolean A12;
    public final boolean A13;
    public final boolean A14;
    public final boolean A15;
    public final boolean A16;

    public int describeContents() {
        return 0;
    }

    public static AnonymousClass1TG A00() {
        return new AnonymousClass1TG();
    }

    public static String A01(Message message) {
        int length;
        if (message == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder("{");
        String str = message.A0q;
        sb.append(str);
        if (ThreadKey.A0F(message.A0U)) {
            return AnonymousClass08S.A0J("{", str);
        }
        A03(message, sb);
        sb.append(" rm: ");
        sb.append(message.A01);
        sb.append(" na: ");
        sb.append(message.A14);
        sb.append(" ua: ");
        sb.append(message.A12);
        sb.append(" len: ");
        String str2 = message.A10;
        if (str2 == null) {
            length = -1;
        } else {
            length = str2.length();
        }
        sb.append(length);
        sb.append("}");
        return sb.toString();
    }

    public static String A02(Message message) {
        Object valueOf;
        Object valueOf2;
        String str;
        int length;
        if (message == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder("{");
        String str2 = message.A0q;
        sb.append(str2);
        if (ThreadKey.A0F(message.A0U)) {
            return AnonymousClass08S.A0J("{", str2);
        }
        A03(message, sb);
        sb.append(" tk: ");
        sb.append(message.A0U);
        sb.append(" ua: ");
        sb.append(message.A12);
        sb.append(" smac: ");
        ImmutableList immutableList = message.A0b;
        Object obj = ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL;
        if (immutableList == null) {
            valueOf = obj;
        } else {
            valueOf = Integer.valueOf(immutableList.size());
        }
        sb.append(valueOf);
        sb.append(" atc: ");
        ImmutableList immutableList2 = message.A0X;
        if (immutableList2 == null) {
            valueOf2 = obj;
        } else {
            valueOf2 = Integer.valueOf(immutableList2.size());
        }
        sb.append(valueOf2);
        sb.append(" ssa: ");
        if (message.A0T == null) {
            str = "N";
        } else {
            str = "Y";
        }
        sb.append(str);
        sb.append(" sc: ");
        ImmutableList immutableList3 = message.A0c;
        if (immutableList3 != null) {
            obj = Integer.valueOf(immutableList3.size());
        }
        sb.append(obj);
        sb.append(" len: ");
        String str3 = message.A10;
        if (str3 == null) {
            length = -1;
        } else {
            length = str3.length();
        }
        sb.append(length);
        sb.append(" tags: ");
        sb.append(message.A0g);
        sb.append("}");
        return sb.toString();
    }

    private static void A03(Message message, StringBuilder sb) {
        if (!C06850cB.A0B(message.A0w)) {
            sb.append(" (");
            sb.append(message.A0w);
            sb.append(")");
        }
        sb.append(" ");
        sb.append(message.A0A);
        sb.append(" t: ");
        sb.append(message.A03);
        sb.append(" st: ");
        sb.append(message.A02);
    }

    public ImmutableList A04() {
        if (!ThreadKey.A0E(this.A0U)) {
            return this.A0b;
        }
        return this.A0N.A02;
    }

    public boolean A05() {
        if (this.A04 != 0) {
            return true;
        }
        return false;
    }

    public CommerceData Ahe() {
        return this.A05;
    }

    public PaymentRequestData Axe() {
        return this.A0P;
    }

    public PaymentTransactionData Axg() {
        return this.A0Q;
    }

    public SentShareAttachment B2Y() {
        return this.A0T;
    }

    public ImmutableList B2v() {
        return this.A0c;
    }

    public C61192yU BAB() {
        return this.A06;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A0q);
        if (!ThreadKey.A0F(this.A0U)) {
            A03(this, sb);
            sb.append(" rm: ");
            sb.append(this.A01);
            sb.append(" na: ");
            sb.append(this.A14);
            sb.append(" ua: ");
            sb.append(this.A12);
            sb.append(": ");
            String str = this.A10;
            if (C06850cB.A0B(str)) {
                sb.append("[empty]");
            } else {
                sb.append(C28841fS.A0B(str));
            }
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A0q);
        parcel.writeParcelable(this.A0U, i);
        parcel.writeLong(this.A03);
        parcel.writeLong(this.A02);
        parcel.writeLong(this.A01);
        parcel.writeParcelable(this.A0K, i);
        parcel.writeString(this.A10);
        parcel.writeInt(this.A15 ? 1 : 0);
        parcel.writeList(this.A0X);
        parcel.writeList(this.A0c);
        parcel.writeString(this.A0z);
        parcel.writeInt(this.A0D.dbKeyValue);
        parcel.writeList(this.A0W);
        parcel.writeString(this.A0w);
        parcel.writeInt(this.A14 ? 1 : 0);
        parcel.writeString(this.A0y);
        parcel.writeString(this.A0A.name());
        parcel.writeList(this.A0b);
        parcel.writeParcelable(this.A0T, i);
        parcel.writeMap(this.A0g);
        parcel.writeMap(this.A0h);
        parcel.writeParcelable(this.A0S, i);
        parcel.writeParcelable(this.A0L, i);
        parcel.writeString(this.A0p);
        parcel.writeString(this.A0o);
        C417826y.A0Q(parcel, this.A0f);
        parcel.writeParcelable(this.A0R, i);
        parcel.writeParcelable(this.A0Q, i);
        parcel.writeParcelable(this.A0P, i);
        parcel.writeInt(this.A12 ? 1 : 0);
        parcel.writeParcelable(this.A0V, i);
        parcel.writeParcelable(this.A07, i);
        AnonymousClass324.A0B(parcel, this.A06);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A09, i);
        parcel.writeValue(this.A0l);
        parcel.writeString(this.A0B.name());
        parcel.writeParcelable(this.A0N, i);
        parcel.writeInt(this.A16 ? 1 : 0);
        parcel.writeLong(this.A04);
        parcel.writeString(this.A11);
        parcel.writeString(this.A0n);
        parcel.writeString(this.A0s);
        parcel.writeSerializable(this.A0F);
        parcel.writeSerializable(this.A0G);
        parcel.writeSerializable(this.A0H);
        parcel.writeString(this.A0t);
        parcel.writeSerializable(this.A0I);
        parcel.writeString(this.A0u);
        parcel.writeValue(this.A0m);
        parcel.writeList(this.A0Y);
        ImmutableMap immutableMap = this.A0i;
        ArrayList arrayList = new ArrayList();
        C24971Xv it = immutableMap.keySet().asList().iterator();
        while (it.hasNext()) {
            arrayList.add(((C69333Wq) it.next()).value);
        }
        parcel.writeStringList(arrayList);
        parcel.writeList(immutableMap.values().asList());
        C417826y.A0V(parcel, this.A13);
        ImmutableMultimap immutableMultimap = this.A0k;
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : immutableMultimap.AOp().entrySet()) {
            hashMap.put(entry.getKey(), new ArrayList((Collection) entry.getValue()));
        }
        C417826y.A0R(parcel, hashMap);
        C417826y.A0J(parcel, this.A0j);
        parcel.writeInt(this.A00);
        C417826y.A0I(parcel, this.A0a);
        C417826y.A0I(parcel, this.A0Z);
        parcel.writeParcelable(this.A0E, i);
        parcel.writeParcelable(this.A0C, i);
        parcel.writeSerializable(this.A08);
        parcel.writeParcelable(this.A0M, i);
        parcel.writeParcelable(this.A0O, i);
        parcel.writeList(this.A0e);
        parcel.writeString(this.A0x);
        parcel.writeParcelable(this.A0J, i);
        parcel.writeList(this.A0d);
        parcel.writeString(this.A0r);
        parcel.writeString(this.A0v);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00b8, code lost:
        if (r1 != false) goto L_0x00ba;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Message(X.AnonymousClass1TG r6) {
        /*
            r5 = this;
            r5.<init>()
            java.lang.String r0 = r6.A0m
            r5.A0q = r0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r6.A0T
            r5.A0U = r0
            long r0 = r6.A04
            r5.A03 = r0
            long r0 = r6.A03
            r5.A02 = r0
            long r0 = r6.A02
            r5.A01 = r0
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r6.A0J
            r5.A0K = r0
            java.lang.String r0 = r6.A0y
            r5.A10 = r0
            java.lang.String r0 = r6.A0v
            r5.A0x = r0
            boolean r0 = r6.A13
            r5.A15 = r0
            com.google.common.collect.ImmutableList r0 = r6.A0W
            r5.A0X = r0
            com.google.common.collect.ImmutableList r0 = r6.B2v()
            r5.A0c = r0
            java.lang.String r0 = r6.A0x
            r5.A0z = r0
            X.1V7 r0 = r6.A0C
            com.google.common.base.Preconditions.checkNotNull(r0)
            r5.A0D = r0
            com.google.common.collect.ImmutableList r0 = r6.A0V
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
            r5.A0W = r0
            java.lang.String r0 = r6.A0u
            r5.A0w = r0
            boolean r0 = r6.A12
            r5.A14 = r0
            java.lang.String r0 = r6.A0w
            r5.A0y = r0
            X.1u2 r0 = r6.A09
            r5.A0A = r0
            X.1R6 r0 = r6.A0A
            r5.A0B = r0
            com.google.common.collect.ImmutableList r0 = r6.A0c
            if (r0 != 0) goto L_0x005e
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
        L_0x005e:
            r5.A0b = r0
            com.facebook.messaging.model.share.SentShareAttachment r0 = r6.B2Y()
            r5.A0T = r0
            java.util.Map r0 = r6.A0z
            com.google.common.collect.ImmutableMap r0 = com.google.common.collect.ImmutableMap.copyOf(r0)
            r5.A0g = r0
            java.util.Map r0 = r6.A10
            com.google.common.collect.ImmutableMap r0 = com.google.common.collect.ImmutableMap.copyOf(r0)
            r5.A0h = r0
            com.facebook.messaging.model.send.SendError r2 = r6.A0R
            r5.A0S = r2
            com.facebook.messaging.model.messages.Publicity r0 = r6.A0K
            r5.A0L = r0
            java.lang.String r0 = r6.A0l
            r5.A0p = r0
            java.lang.String r0 = r6.A0k
            r5.A0o = r0
            com.google.common.collect.ImmutableMap r0 = r6.A0e
            if (r0 != 0) goto L_0x008c
            com.google.common.collect.ImmutableMap r0 = com.google.common.collect.RegularImmutableMap.A03
        L_0x008c:
            r5.A0f = r0
            com.facebook.messaging.model.send.PendingSendQueueKey r0 = r6.A0Q
            r5.A0R = r0
            boolean r0 = r6.A11
            r5.A12 = r0
            X.1V7 r1 = r5.A0D
            X.1V7 r0 = X.AnonymousClass1V7.A0A
            r4 = 1
            r3 = 0
            if (r1 != r0) goto L_0x009f
            r3 = 1
        L_0x009f:
            X.1u4 r2 = r2.A02
            X.1u4 r1 = X.C36891u4.A07
            r0 = 0
            if (r2 != r1) goto L_0x00a7
            r0 = 1
        L_0x00a7:
            r3 = r3 ^ r0
            com.google.common.base.Preconditions.checkArgument(r3)
            com.facebook.messaging.model.send.PendingSendQueueKey r0 = r5.A0R
            if (r0 == 0) goto L_0x00ba
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r0.A01
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0U
            boolean r1 = com.google.common.base.Objects.equal(r1, r0)
            r0 = 0
            if (r1 == 0) goto L_0x00bb
        L_0x00ba:
            r0 = 1
        L_0x00bb:
            com.google.common.base.Preconditions.checkArgument(r0)
            com.facebook.messaging.model.payment.PaymentTransactionData r0 = r6.Axg()
            r5.A0Q = r0
            com.facebook.messaging.model.payment.PaymentRequestData r0 = r6.Axe()
            r5.A0P = r0
            com.facebook.messaging.business.commerce.model.retail.CommerceData r0 = r6.Ahe()
            r5.A05 = r0
            com.facebook.share.model.ComposerAppAttribution r1 = r6.A0U
            r5.A0V = r1
            com.facebook.messaging.model.attribution.ContentAppAttribution r0 = r6.A06
            r5.A07 = r0
            if (r1 == 0) goto L_0x00dd
            if (r0 == 0) goto L_0x00dd
            r4 = 0
        L_0x00dd:
            com.google.common.base.Preconditions.checkArgument(r4)
            X.2yU r0 = r6.BAB()
            r5.A06 = r0
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r6.A08
            r5.A09 = r0
            java.lang.Integer r0 = r6.A0i
            r5.A0l = r0
            java.lang.Long r0 = r6.A0j
            r5.A0m = r0
            com.facebook.messaging.model.mms.MmsData r0 = r6.A0M
            r5.A0N = r0
            boolean r0 = r6.A15
            r5.A16 = r0
            long r0 = r6.A01
            r5.A04 = r0
            java.lang.String r0 = r6.A0t
            r5.A11 = r0
            java.lang.String r0 = r6.A0n
            r5.A0n = r0
            java.lang.String r0 = r6.A0p
            r5.A0s = r0
            X.4o1 r0 = r6.A0E
            r5.A0F = r0
            X.5hD r0 = r6.A0F
            r5.A0G = r0
            X.5hD r0 = r6.A0G
            r5.A0H = r0
            java.lang.String r0 = r6.A0q
            r5.A0t = r0
            X.Al6 r0 = r6.A0H
            r5.A0I = r0
            java.lang.String r0 = r6.A0r
            r5.A0u = r0
            com.google.common.collect.ImmutableList r0 = r6.A0X
            r5.A0Y = r0
            com.google.common.collect.ImmutableMap r0 = r6.A0f
            r5.A0i = r0
            boolean r0 = r6.A14
            r5.A13 = r0
            X.0V0 r0 = r6.A0h
            com.google.common.collect.ImmutableMultimap r0 = com.google.common.collect.ImmutableMultimap.A00(r0)
            r5.A0k = r0
            X.0V0 r0 = r6.A0g
            com.google.common.collect.ImmutableMultimap r0 = com.google.common.collect.ImmutableMultimap.A00(r0)
            r5.A0j = r0
            int r0 = r6.A00
            r5.A00 = r0
            com.google.common.collect.ImmutableList r0 = r6.A0Y
            r5.A0a = r0
            com.google.common.collect.ImmutableList r0 = r6.A0b
            r5.A0Z = r0
            com.facebook.messaging.model.messages.MontageBrandedCameraAttributionData r0 = r6.A0D
            r5.A0E = r0
            com.facebook.messaging.model.messages.MessageRepliedTo r0 = r6.A0B
            r5.A0C = r0
            X.3dY r0 = r6.A07
            r5.A08 = r0
            com.facebook.messaging.model.messages.montageattribution.MontageAttributionData r0 = r6.A0L
            r5.A0M = r0
            com.facebook.messaging.model.montagemetadata.MontageMetadata r0 = r6.A0N
            r5.A0O = r0
            com.google.common.collect.ImmutableList r0 = r6.A0a
            r5.A0e = r0
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r6.A0I
            r5.A0J = r0
            com.google.common.collect.ImmutableList r0 = r6.A0Z
            r5.A0d = r0
            java.lang.String r0 = r6.A0o
            r5.A0r = r0
            java.lang.String r0 = r6.A0s
            r5.A0v = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.messages.Message.<init>(X.1TG):void");
    }

    public Message(Parcel parcel) {
        AnonymousClass1R6 valueOf;
        Class<Message> cls = Message.class;
        this.A0q = parcel.readString();
        Class<ThreadKey> cls2 = ThreadKey.class;
        this.A0U = (ThreadKey) parcel.readParcelable(cls2.getClassLoader());
        this.A03 = parcel.readLong();
        this.A02 = parcel.readLong();
        this.A01 = parcel.readLong();
        Class<ParticipantInfo> cls3 = ParticipantInfo.class;
        this.A0K = (ParticipantInfo) parcel.readParcelable(cls3.getClassLoader());
        this.A10 = parcel.readString();
        boolean z = true;
        this.A15 = parcel.readInt() != 0;
        this.A0X = ImmutableList.copyOf((Collection) parcel.readArrayList(Attachment.class.getClassLoader()));
        this.A0c = ImmutableList.copyOf((Collection) parcel.readArrayList(Share.class.getClassLoader()));
        this.A0z = parcel.readString();
        this.A0D = AnonymousClass1V7.A00(parcel.readInt());
        this.A0W = ImmutableList.copyOf((Collection) parcel.readArrayList(cls3.getClassLoader()));
        this.A0w = parcel.readString();
        this.A14 = parcel.readInt() != 0;
        this.A0y = parcel.readString();
        this.A0A = C36881u2.valueOf(parcel.readString());
        this.A0b = ImmutableList.copyOf((Collection) parcel.readArrayList(MediaResource.class.getClassLoader()));
        this.A0T = (SentShareAttachment) parcel.readParcelable(SentShareAttachment.class.getClassLoader());
        this.A0g = ImmutableMap.copyOf(parcel.readHashMap(cls.getClassLoader()));
        this.A0h = ImmutableMap.copyOf(parcel.readHashMap(cls.getClassLoader()));
        this.A0S = (SendError) parcel.readParcelable(SendError.class.getClassLoader());
        this.A0L = (Publicity) parcel.readParcelable(Publicity.class.getClassLoader());
        this.A0p = parcel.readString();
        this.A0o = parcel.readString();
        HashMap A042 = AnonymousClass0TG.A04();
        C417826y.A0T(parcel, A042, cls2);
        this.A0f = ImmutableMap.copyOf(A042);
        this.A0R = (PendingSendQueueKey) parcel.readParcelable(PendingSendQueueKey.class.getClassLoader());
        this.A0Q = (PaymentTransactionData) parcel.readParcelable(PaymentTransactionData.class.getClassLoader());
        this.A0P = (PaymentRequestData) parcel.readParcelable(PaymentRequestData.class.getClassLoader());
        this.A12 = parcel.readInt() != 0;
        this.A0V = (ComposerAppAttribution) parcel.readParcelable(ComposerAppAttribution.class.getClassLoader());
        this.A07 = (ContentAppAttribution) parcel.readParcelable(ContentAppAttribution.class.getClassLoader());
        this.A06 = (C61192yU) AnonymousClass324.A03(parcel);
        this.A05 = (CommerceData) parcel.readParcelable(CommerceData.class.getClassLoader());
        this.A09 = (GenericAdminMessageInfo) parcel.readParcelable(GenericAdminMessageInfo.class.getClassLoader());
        this.A0l = (Integer) parcel.readValue(Integer.class.getClassLoader());
        String readString = parcel.readString();
        if (C06850cB.A0B(readString)) {
            valueOf = AnonymousClass1R6.UNKNOWN;
        } else {
            valueOf = AnonymousClass1R6.valueOf(readString);
        }
        this.A0B = valueOf;
        this.A0N = (MmsData) parcel.readParcelable(MmsData.class.getClassLoader());
        this.A16 = parcel.readInt() == 0 ? false : z;
        this.A04 = parcel.readLong();
        this.A11 = parcel.readString();
        this.A0n = parcel.readString();
        this.A0s = parcel.readString();
        this.A0F = (C98894o1) parcel.readSerializable();
        this.A0G = (C117335hD) parcel.readSerializable();
        this.A0H = (C117335hD) parcel.readSerializable();
        this.A0t = parcel.readString();
        this.A0I = (C21936Al6) parcel.readSerializable();
        this.A0u = parcel.readString();
        this.A0m = (Long) parcel.readValue(Long.class.getClassLoader());
        this.A0Y = ImmutableList.copyOf((Collection) parcel.readArrayList(MessageMetadataAtTextRange.class.getClassLoader()));
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        ArrayList readArrayList = parcel.readArrayList(PlatformMetadata.class.getClassLoader());
        ImmutableMap.Builder builder = ImmutableMap.builder();
        for (int i = 0; i < arrayList.size(); i++) {
            builder.put(C69333Wq.A00((String) arrayList.get(i)), readArrayList.get(i));
        }
        this.A0i = builder.build();
        this.A13 = C417826y.A0W(parcel);
        HashMultimap hashMultimap = new HashMultimap();
        HashMap hashMap = new HashMap();
        C417826y.A0O(parcel, hashMap);
        for (Map.Entry entry : hashMap.entrySet()) {
            hashMultimap.Byz(entry.getKey(), (Iterable) entry.getValue());
        }
        this.A0k = ImmutableMultimap.A00(hashMultimap);
        HashMultimap hashMultimap2 = new HashMultimap();
        Class<C417826y> cls4 = C417826y.class;
        int readInt = parcel.readInt();
        for (int i2 = 0; i2 < readInt; i2++) {
            Parcelable readParcelable = parcel.readParcelable(cls4.getClassLoader());
            ArrayList A002 = C04300To.A00();
            parcel.readList(A002, cls4.getClassLoader());
            hashMultimap2.Byz(readParcelable, A002);
        }
        this.A0j = ImmutableMultimap.A00(hashMultimap2);
        this.A00 = parcel.readInt();
        this.A0a = C417826y.A06(parcel, ProfileRange.CREATOR);
        this.A0Z = C417826y.A06(parcel, MontageFeedbackOverlay.CREATOR);
        Class<MontageBrandedCameraAttributionData> cls5 = MontageBrandedCameraAttributionData.class;
        this.A0E = (MontageBrandedCameraAttributionData) parcel.readParcelable(cls5.getClassLoader());
        this.A0C = (MessageRepliedTo) parcel.readParcelable(MessageRepliedTo.class.getClassLoader());
        this.A08 = (C72033dY) parcel.readSerializable();
        this.A0M = (MontageAttributionData) parcel.readParcelable(cls5.getClassLoader());
        this.A0O = (MontageMetadata) parcel.readParcelable(MontageMetadata.class.getClassLoader());
        this.A0e = ImmutableList.copyOf((Collection) parcel.readArrayList(MessageTranslation.class.getClassLoader()));
        this.A0x = parcel.readString();
        this.A0J = (ParticipantInfo) parcel.readParcelable(cls3.getClassLoader());
        this.A0d = ImmutableList.copyOf((Collection) parcel.readArrayList(B5H.class.getClassLoader()));
        this.A0r = parcel.readString();
        this.A0v = parcel.readString();
    }
}
