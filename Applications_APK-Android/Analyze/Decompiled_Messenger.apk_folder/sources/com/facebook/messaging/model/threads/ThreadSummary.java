package com.facebook.messaging.model.threads;

import X.AnonymousClass0TG;
import X.AnonymousClass105;
import X.AnonymousClass1Y3;
import X.AnonymousClass24B;
import X.AnonymousClass80H;
import X.C06850cB;
import X.C10950l8;
import X.C17920zh;
import X.C17930zi;
import X.C17950zm;
import X.C22298Ase;
import X.C24971Xv;
import X.C25661aC;
import X.C417826y;
import X.C61822zY;
import X.C99084oO;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.util.TriState;
import com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType;
import com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason;
import com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType;
import com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType;
import com.facebook.graphql.enums.GraphQLThreadConnectivityStatus;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.customthreads.model.ThreadThemeInfo;
import com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessageDraft;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel;
import com.facebook.user.model.UserKey;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.card.payment.BuildConfig;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public final class ThreadSummary implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C17950zm();
    private ImmutableMap A00;
    public final float A01;
    public final int A02;
    public final long A03;
    public final long A04;
    public final long A05;
    public final long A06;
    public final long A07;
    public final long A08;
    public final long A09;
    public final long A0A;
    public final long A0B;
    public final long A0C;
    public final Uri A0D;
    public final Uri A0E;
    public final TriState A0F;
    public final GraphQLExtensibleMessageAdminTextType A0G;
    public final GraphQLMessageThreadCannotReplyReason A0H;
    public final GraphQLMessengerGroupThreadSubType A0I;
    public final GraphQLMessengerXMAGroupingType A0J;
    public final CallToAction A0K;
    public final CallToAction A0L;
    public final ThreadThemeInfo A0M;
    public final GamesPushNotificationSettings A0N;
    public final C10950l8 A0O;
    public final MessageDraft A0P;
    public final ParticipantInfo A0Q;
    public final ThreadKey A0R;
    public final ThreadKey A0S;
    public final AdContextData A0T;
    public final AdsConversionsQPData A0U;
    public final GroupThreadData A0V;
    public final MarketplaceThreadData A0W;
    public final MontageThreadPreview A0X;
    public final NotificationSetting A0Y;
    public final AnonymousClass105 A0Z;
    public final RelatedPageThreadData A0a;
    public final RequestAppointmentData A0b;
    public final ThreadBookingRequests A0c;
    public final ThreadConnectivityData A0d;
    public final ThreadCustomization A0e;
    public final ThreadMediaPreview A0f;
    public final ThreadPageMessageAssignedAdmin A0g;
    public final ThreadRtcCallInfoData A0h;
    public final C17930zi A0i;
    public final AnimatedThreadActivityBannerDataModel A0j;
    public final ImmutableList A0k;
    public final ImmutableList A0l;
    public final ImmutableList A0m;
    public final ImmutableList A0n;
    public final ImmutableList A0o;
    public final String A0p;
    public final String A0q;
    public final String A0r;
    public final String A0s;
    public final String A0t;
    public final String A0u;
    public final String A0v;
    public final String A0w;
    public final String A0x;
    public final boolean A0y;
    public final boolean A0z;
    public final boolean A10;
    public final boolean A11;
    public final boolean A12;
    public final boolean A13;
    public final boolean A14;
    public final boolean A15;
    public final boolean A16;
    public final boolean A17;
    public final boolean A18;

    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0126, code lost:
        if (r8.A0u == null) goto L_0x0128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0137, code lost:
        if (r8.A0E == null) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0148, code lost:
        if (r8.A0f == null) goto L_0x014a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x016b, code lost:
        if (r8.A0P == null) goto L_0x016d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x017c, code lost:
        if (r8.A0Y == null) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0193, code lost:
        if (r8.A0e == null) goto L_0x0195;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x01a4, code lost:
        if (r8.A0h == null) goto L_0x01a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x01b5, code lost:
        if (r8.A0l == null) goto L_0x01b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x01c6, code lost:
        if (r8.A0L == null) goto L_0x01c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x01d7, code lost:
        if (r8.A0q == null) goto L_0x01d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x01ee, code lost:
        if (r8.A00 == null) goto L_0x01f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x01ff, code lost:
        if (r8.A0c == null) goto L_0x0201;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0210, code lost:
        if (r8.A0X == null) goto L_0x0212;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x0221, code lost:
        if (r8.A0o == null) goto L_0x0223;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x0232, code lost:
        if (r8.A0R == null) goto L_0x0234;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0243, code lost:
        if (r8.A0W == null) goto L_0x0245;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x0254, code lost:
        if (r8.A0T == null) goto L_0x0256;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x0265, code lost:
        if (r8.A0U == null) goto L_0x0267;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x0282, code lost:
        if (r8.A0N == null) goto L_0x0284;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x0293, code lost:
        if (r8.A0D == null) goto L_0x0295;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x02c2, code lost:
        if (r8.A0j == null) goto L_0x02c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x02dd, code lost:
        if (r8.A0K == null) goto L_0x02df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:0x02fb, code lost:
        if (r8.A0x == null) goto L_0x02fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:297:0x0312, code lost:
        if (r8.A0r == null) goto L_0x0314;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x0323, code lost:
        if (r8.A0b == null) goto L_0x0325;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x009e, code lost:
        if (r8.A0S == null) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00af, code lost:
        if (r8.A0t == null) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c0, code lost:
        if (r8.A0m == null) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00d1, code lost:
        if (r8.A0k == null) goto L_0x00d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00e2, code lost:
        if (r8.A0n == null) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00f3, code lost:
        if (r8.A0w == null) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0104, code lost:
        if (r8.A0p == null) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0115, code lost:
        if (r8.A0Q == null) goto L_0x0117;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 1
            if (r7 == r8) goto L_0x035f
            r5 = 0
            if (r8 == 0) goto L_0x0078
            java.lang.Class r1 = r7.getClass()
            java.lang.Class r0 = r8.getClass()
            if (r1 != r0) goto L_0x0078
            com.facebook.messaging.model.threads.ThreadSummary r8 = (com.facebook.messaging.model.threads.ThreadSummary) r8
            long r3 = r7.A09
            long r1 = r8.A09
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            long r3 = r7.A0B
            long r1 = r8.A0B
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            long r3 = r7.A07
            long r1 = r8.A07
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            long r3 = r7.A03
            long r1 = r8.A03
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            long r3 = r7.A0C
            long r1 = r8.A0C
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            boolean r1 = r7.A0y
            boolean r0 = r8.A0y
            if (r1 != r0) goto L_0x0078
            boolean r1 = r7.A16
            boolean r0 = r8.A16
            if (r1 != r0) goto L_0x0078
            boolean r1 = r7.A10
            boolean r0 = r8.A10
            if (r1 != r0) goto L_0x0078
            boolean r1 = r7.A0z
            boolean r0 = r8.A0z
            if (r1 != r0) goto L_0x0078
            boolean r1 = r7.A12
            boolean r0 = r8.A12
            if (r1 != r0) goto L_0x0078
            long r3 = r7.A05
            long r1 = r8.A05
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            boolean r1 = r7.A14
            boolean r0 = r8.A14
            if (r1 != r0) goto L_0x0078
            int r1 = r7.A02
            int r0 = r8.A02
            if (r1 != r0) goto L_0x0078
            java.lang.String r1 = r7.A0s
            if (r1 == 0) goto L_0x0079
            java.lang.String r0 = r8.A0s
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x007d
        L_0x0078:
            return r5
        L_0x0079:
            java.lang.String r0 = r8.A0s
            if (r0 != 0) goto L_0x0078
        L_0x007d:
            long r3 = r7.A08
            long r1 = r8.A08
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            float r1 = r8.A01
            float r0 = r7.A01
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0078
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r7.A0S
            if (r1 == 0) goto L_0x009c
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0S
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00a0
            return r5
        L_0x009c:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0S
            if (r0 != 0) goto L_0x0078
        L_0x00a0:
            java.lang.String r1 = r7.A0t
            if (r1 == 0) goto L_0x00ad
            java.lang.String r0 = r8.A0t
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00b1
            return r5
        L_0x00ad:
            java.lang.String r0 = r8.A0t
            if (r0 != 0) goto L_0x0078
        L_0x00b1:
            com.google.common.collect.ImmutableList r1 = r7.A0m
            if (r1 == 0) goto L_0x00be
            com.google.common.collect.ImmutableList r0 = r8.A0m
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00c2
            return r5
        L_0x00be:
            com.google.common.collect.ImmutableList r0 = r8.A0m
            if (r0 != 0) goto L_0x0078
        L_0x00c2:
            com.google.common.collect.ImmutableList r1 = r7.A0k
            if (r1 == 0) goto L_0x00cf
            com.google.common.collect.ImmutableList r0 = r8.A0k
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00d3
            return r5
        L_0x00cf:
            com.google.common.collect.ImmutableList r0 = r8.A0k
            if (r0 != 0) goto L_0x0078
        L_0x00d3:
            com.google.common.collect.ImmutableList r1 = r7.A0n
            if (r1 == 0) goto L_0x00e0
            com.google.common.collect.ImmutableList r0 = r8.A0n
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00e4
            return r5
        L_0x00e0:
            com.google.common.collect.ImmutableList r0 = r8.A0n
            if (r0 != 0) goto L_0x0078
        L_0x00e4:
            java.lang.String r1 = r7.A0w
            if (r1 == 0) goto L_0x00f1
            java.lang.String r0 = r8.A0w
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00f5
            return r5
        L_0x00f1:
            java.lang.String r0 = r8.A0w
            if (r0 != 0) goto L_0x0078
        L_0x00f5:
            java.lang.String r1 = r7.A0p
            if (r1 == 0) goto L_0x0102
            java.lang.String r0 = r8.A0p
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0106
            return r5
        L_0x0102:
            java.lang.String r0 = r8.A0p
            if (r0 != 0) goto L_0x0078
        L_0x0106:
            com.facebook.messaging.model.messages.ParticipantInfo r1 = r7.A0Q
            if (r1 == 0) goto L_0x0113
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r8.A0Q
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0117
            return r5
        L_0x0113:
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r8.A0Q
            if (r0 != 0) goto L_0x0078
        L_0x0117:
            java.lang.String r1 = r7.A0u
            if (r1 == 0) goto L_0x0124
            java.lang.String r0 = r8.A0u
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0128
            return r5
        L_0x0124:
            java.lang.String r0 = r8.A0u
            if (r0 != 0) goto L_0x0078
        L_0x0128:
            android.net.Uri r1 = r7.A0E
            if (r1 == 0) goto L_0x0135
            android.net.Uri r0 = r8.A0E
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0139
            return r5
        L_0x0135:
            android.net.Uri r0 = r8.A0E
            if (r0 != 0) goto L_0x0078
        L_0x0139:
            com.facebook.messaging.model.threads.ThreadMediaPreview r1 = r7.A0f
            if (r1 == 0) goto L_0x0146
            com.facebook.messaging.model.threads.ThreadMediaPreview r0 = r8.A0f
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x014a
            return r5
        L_0x0146:
            com.facebook.messaging.model.threads.ThreadMediaPreview r0 = r8.A0f
            if (r0 != 0) goto L_0x0078
        L_0x014a:
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r1 = r7.A0H
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r0 = r8.A0H
            if (r1 != r0) goto L_0x0078
            X.0zi r1 = r7.A0i
            X.0zi r0 = r8.A0i
            if (r1 != r0) goto L_0x0078
            X.0l8 r1 = r7.A0O
            X.0l8 r0 = r8.A0O
            if (r1 != r0) goto L_0x0078
            com.facebook.messaging.model.messages.MessageDraft r1 = r7.A0P
            if (r1 == 0) goto L_0x0169
            com.facebook.messaging.model.messages.MessageDraft r0 = r8.A0P
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x016d
            return r5
        L_0x0169:
            com.facebook.messaging.model.messages.MessageDraft r0 = r8.A0P
            if (r0 != 0) goto L_0x0078
        L_0x016d:
            com.facebook.messaging.model.threads.NotificationSetting r1 = r7.A0Y
            if (r1 == 0) goto L_0x017a
            com.facebook.messaging.model.threads.NotificationSetting r0 = r8.A0Y
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x017e
            return r5
        L_0x017a:
            com.facebook.messaging.model.threads.NotificationSetting r0 = r8.A0Y
            if (r0 != 0) goto L_0x0078
        L_0x017e:
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = r7.A0G
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r0 = r8.A0G
            if (r1 != r0) goto L_0x0078
            com.facebook.messaging.model.threads.ThreadCustomization r1 = r7.A0e
            if (r1 == 0) goto L_0x0191
            com.facebook.messaging.model.threads.ThreadCustomization r0 = r8.A0e
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0195
            return r5
        L_0x0191:
            com.facebook.messaging.model.threads.ThreadCustomization r0 = r8.A0e
            if (r0 != 0) goto L_0x0078
        L_0x0195:
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r1 = r7.A0h
            if (r1 == 0) goto L_0x01a2
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r0 = r8.A0h
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01a6
            return r5
        L_0x01a2:
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r0 = r8.A0h
            if (r0 != 0) goto L_0x0078
        L_0x01a6:
            com.google.common.collect.ImmutableList r1 = r7.A0l
            if (r1 == 0) goto L_0x01b3
            com.google.common.collect.ImmutableList r0 = r8.A0l
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01b7
            return r5
        L_0x01b3:
            com.google.common.collect.ImmutableList r0 = r8.A0l
            if (r0 != 0) goto L_0x0078
        L_0x01b7:
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r1 = r7.A0L
            if (r1 == 0) goto L_0x01c4
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r8.A0L
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01c8
            return r5
        L_0x01c4:
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r8.A0L
            if (r0 != 0) goto L_0x0078
        L_0x01c8:
            java.lang.String r1 = r7.A0q
            if (r1 == 0) goto L_0x01d5
            java.lang.String r0 = r8.A0q
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01d9
            return r5
        L_0x01d5:
            java.lang.String r0 = r8.A0q
            if (r0 != 0) goto L_0x0078
        L_0x01d9:
            com.facebook.common.util.TriState r1 = r7.A0F
            com.facebook.common.util.TriState r0 = r8.A0F
            if (r1 != r0) goto L_0x0078
            com.google.common.collect.ImmutableMap r1 = r7.A00
            if (r1 == 0) goto L_0x01ec
            com.google.common.collect.ImmutableMap r0 = r8.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01f0
            return r5
        L_0x01ec:
            com.google.common.collect.ImmutableMap r0 = r8.A00
            if (r0 != 0) goto L_0x0078
        L_0x01f0:
            com.facebook.messaging.model.threads.ThreadBookingRequests r1 = r7.A0c
            if (r1 == 0) goto L_0x01fd
            com.facebook.messaging.model.threads.ThreadBookingRequests r0 = r8.A0c
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0201
            return r5
        L_0x01fd:
            com.facebook.messaging.model.threads.ThreadBookingRequests r0 = r8.A0c
            if (r0 != 0) goto L_0x0078
        L_0x0201:
            com.facebook.messaging.model.threads.MontageThreadPreview r1 = r7.A0X
            if (r1 == 0) goto L_0x020e
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r8.A0X
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0212
            return r5
        L_0x020e:
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r8.A0X
            if (r0 != 0) goto L_0x0078
        L_0x0212:
            com.google.common.collect.ImmutableList r1 = r7.A0o
            if (r1 == 0) goto L_0x021f
            com.google.common.collect.ImmutableList r0 = r8.A0o
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0223
            return r5
        L_0x021f:
            com.google.common.collect.ImmutableList r0 = r8.A0o
            if (r0 != 0) goto L_0x0078
        L_0x0223:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r7.A0R
            if (r1 == 0) goto L_0x0230
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0R
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0234
            return r5
        L_0x0230:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0R
            if (r0 != 0) goto L_0x0078
        L_0x0234:
            com.facebook.messaging.model.threads.MarketplaceThreadData r1 = r7.A0W
            if (r1 == 0) goto L_0x0241
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r8.A0W
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0245
            return r5
        L_0x0241:
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r8.A0W
            if (r0 != 0) goto L_0x0078
        L_0x0245:
            com.facebook.messaging.model.threads.AdContextData r1 = r7.A0T
            if (r1 == 0) goto L_0x0252
            com.facebook.messaging.model.threads.AdContextData r0 = r8.A0T
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0256
            return r5
        L_0x0252:
            com.facebook.messaging.model.threads.AdContextData r0 = r8.A0T
            if (r0 != 0) goto L_0x0078
        L_0x0256:
            com.facebook.messaging.model.threads.AdsConversionsQPData r1 = r7.A0U
            if (r1 == 0) goto L_0x0263
            com.facebook.messaging.model.threads.AdsConversionsQPData r0 = r8.A0U
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0267
            return r5
        L_0x0263:
            com.facebook.messaging.model.threads.AdsConversionsQPData r0 = r8.A0U
            if (r0 != 0) goto L_0x0078
        L_0x0267:
            X.105 r1 = r7.A0Z
            X.105 r0 = r8.A0Z
            if (r1 != r0) goto L_0x0078
            boolean r1 = r7.A18
            boolean r0 = r8.A18
            if (r1 != r0) goto L_0x0078
            com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings r1 = r7.A0N
            if (r1 == 0) goto L_0x0280
            com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings r0 = r8.A0N
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0284
            return r5
        L_0x0280:
            com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings r0 = r8.A0N
            if (r0 != 0) goto L_0x0078
        L_0x0284:
            android.net.Uri r1 = r7.A0D
            if (r1 == 0) goto L_0x0291
            android.net.Uri r0 = r8.A0D
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0295
            return r5
        L_0x0291:
            android.net.Uri r0 = r8.A0D
            if (r0 != 0) goto L_0x0078
        L_0x0295:
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r1 = r7.A0M
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r0 = r8.A0M
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0078
            boolean r1 = r7.A13
            boolean r0 = r8.A13
            if (r1 != r0) goto L_0x0078
            boolean r1 = r7.A17
            boolean r0 = r8.A17
            if (r1 != r0) goto L_0x0078
            long r3 = r7.A0A
            long r1 = r8.A0A
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel r1 = r7.A0j
            if (r1 == 0) goto L_0x02c0
            com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel r0 = r8.A0j
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02c4
            return r5
        L_0x02c0:
            com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel r0 = r8.A0j
            if (r0 != 0) goto L_0x0078
        L_0x02c4:
            com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType r1 = r7.A0J
            com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType r0 = r8.A0J
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0078
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r1 = r7.A0K
            if (r1 == 0) goto L_0x02db
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r8.A0K
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02df
            return r5
        L_0x02db:
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r8.A0K
            if (r0 != 0) goto L_0x0078
        L_0x02df:
            com.facebook.messaging.model.threads.ThreadConnectivityData r1 = r7.A0d
            if (r1 == 0) goto L_0x02ec
            com.facebook.messaging.model.threads.ThreadConnectivityData r0 = r8.A0d
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02ec
            return r5
        L_0x02ec:
            java.lang.String r1 = r7.A0x
            if (r1 == 0) goto L_0x02f9
            java.lang.String r0 = r8.A0x
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02fd
            return r5
        L_0x02f9:
            java.lang.String r0 = r8.A0x
            if (r0 != 0) goto L_0x0078
        L_0x02fd:
            boolean r1 = r7.A15
            boolean r0 = r8.A15
            if (r1 != r0) goto L_0x0078
            java.lang.String r1 = r7.A0r
            if (r1 == 0) goto L_0x0310
            java.lang.String r0 = r8.A0r
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0314
            return r5
        L_0x0310:
            java.lang.String r0 = r8.A0r
            if (r0 != 0) goto L_0x0078
        L_0x0314:
            com.facebook.messaging.model.threads.RequestAppointmentData r1 = r7.A0b
            if (r1 == 0) goto L_0x0321
            com.facebook.messaging.model.threads.RequestAppointmentData r0 = r8.A0b
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0325
            return r5
        L_0x0321:
            com.facebook.messaging.model.threads.RequestAppointmentData r0 = r8.A0b
            if (r0 != 0) goto L_0x0078
        L_0x0325:
            com.facebook.messaging.model.threads.RelatedPageThreadData r1 = r7.A0a
            com.facebook.messaging.model.threads.RelatedPageThreadData r0 = r8.A0a
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0078
            com.facebook.messaging.model.threads.ThreadPageMessageAssignedAdmin r1 = r7.A0g
            com.facebook.messaging.model.threads.ThreadPageMessageAssignedAdmin r0 = r8.A0g
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0078
            java.lang.String r1 = r7.A0v
            java.lang.String r0 = r8.A0v
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x0078
            long r3 = r7.A04
            long r1 = r8.A04
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            boolean r1 = r7.A11
            boolean r0 = r8.A11
            if (r1 != r0) goto L_0x0078
            com.facebook.messaging.model.threads.GroupThreadData r1 = r7.A0V
            com.facebook.messaging.model.threads.GroupThreadData r0 = r8.A0V
            if (r1 == 0) goto L_0x035c
            boolean r6 = r1.equals(r0)
            return r6
        L_0x035c:
            if (r0 == 0) goto L_0x035f
            r6 = 0
        L_0x035f:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.threads.ThreadSummary.equals(java.lang.Object):boolean");
    }

    public static C17920zh A00() {
        return new C17920zh();
    }

    public static String A01(ThreadSummary threadSummary) {
        int length;
        Object obj;
        String str = "null";
        if (threadSummary == null) {
            return str;
        }
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(ThreadSummary.class);
        stringHelper.add("threadKey", threadSummary.A0S);
        stringHelper.add(AnonymousClass80H.$const$string(74), threadSummary.A09);
        stringHelper.add("folder", threadSummary.A0O);
        stringHelper.add("name", threadSummary.A0t);
        stringHelper.add("timestampMs", threadSummary.A0B);
        String str2 = threadSummary.A0w;
        int i = -1;
        if (str2 == null) {
            length = -1;
        } else {
            length = str2.length();
        }
        stringHelper.add("snippet length", length);
        String str3 = threadSummary.A0p;
        if (str3 != null) {
            i = str3.length();
        }
        stringHelper.add("adminSnippet length", i);
        stringHelper.add("optimisticGroupState", threadSummary.A0Z);
        stringHelper.add("useExistingGroup", threadSummary.A18);
        stringHelper.add("isUnread", threadSummary.A0B());
        stringHelper.add("lastReadTimestampMs", threadSummary.A07);
        stringHelper.add("lastMessageTimestampMs", threadSummary.A06);
        stringHelper.add("isFussRedPage", threadSummary.A13);
        stringHelper.add("isPinned", threadSummary.A17);
        ThreadCustomization threadCustomization = threadSummary.A0e;
        if (threadCustomization != null) {
            str = threadCustomization.toString();
        }
        stringHelper.add("customization", str);
        boolean isEmpty = threadSummary.A0m.isEmpty();
        Object obj2 = BuildConfig.FLAVOR;
        if (isEmpty) {
            obj = obj2;
        } else {
            obj = threadSummary.A0m.get(0);
        }
        stringHelper.add("participantOne", obj);
        if (threadSummary.A0m.size() >= 2) {
            obj2 = threadSummary.A0m.get(1);
        }
        stringHelper.add("participantTwo", obj2);
        stringHelper.add(C22298Ase.$const$string(239), threadSummary.A0I);
        stringHelper.add("hasNonAdminMessage", threadSummary.A11);
        return stringHelper.toString();
    }

    public ThreadParticipant A03() {
        ImmutableList immutableList;
        if (ThreadKey.A0E(this.A0S) && (immutableList = this.A0m) != null && immutableList.size() == 2) {
            C24971Xv it = this.A0m.iterator();
            while (it.hasNext()) {
                ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
                if (threadParticipant.A00() != null && threadParticipant.A00().A08()) {
                    return threadParticipant;
                }
            }
        }
        return null;
    }

    public ThreadParticipant A04(UserKey userKey) {
        if (this.A00 == null) {
            ImmutableList immutableList = this.A0m;
            ImmutableList immutableList2 = this.A0k;
            HashMap A042 = AnonymousClass0TG.A04();
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
                A042.put(threadParticipant.A00(), threadParticipant);
            }
            C24971Xv it2 = immutableList2.iterator();
            while (it2.hasNext()) {
                ThreadParticipant threadParticipant2 = (ThreadParticipant) it2.next();
                A042.put(threadParticipant2.A00(), threadParticipant2);
            }
            this.A00 = ImmutableMap.copyOf(A042);
        }
        return (ThreadParticipant) this.A00.get(userKey);
    }

    public List A06() {
        return new C61822zY(this.A0m, this.A0k);
    }

    public boolean A07() {
        return !C06850cB.A0B(this.A0t);
    }

    public boolean A08() {
        ImmutableList immutableList = this.A0m;
        if (immutableList != null) {
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
                if (threadParticipant != null) {
                    C25661aC r2 = threadParticipant.A04.A00;
                    boolean z = false;
                    if (r2 == C25661aC.PARENT_APPROVED_USER) {
                        z = true;
                    }
                    if (z) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean A09() {
        ImmutableList immutableList;
        if (!ThreadKey.A0E(this.A0S) || (immutableList = this.A0m) == null || immutableList.size() <= 2) {
            return false;
        }
        return true;
    }

    public boolean A0A() {
        if (this.A0O.A01()) {
            return true;
        }
        ThreadConnectivityData threadConnectivityData = this.A0d;
        if (threadConnectivityData == null || !threadConnectivityData.A00().equals(GraphQLThreadConnectivityStatus.A01)) {
            return false;
        }
        return true;
    }

    public boolean A0B() {
        if (!ThreadKey.A0E(this.A0S)) {
            long j = this.A06;
            if (j != -1) {
                if (this.A07 < j) {
                    return true;
                }
                return false;
            }
        }
        if (this.A07 < this.A0B) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        int i24;
        int i25;
        int i26;
        int i27;
        int i28;
        int i29;
        int i30;
        int i31;
        int i32;
        int i33;
        int i34;
        int i35;
        int i36;
        int i37;
        int i38;
        int i39;
        int i40;
        int i41;
        int i42;
        int i43;
        int i44;
        int i45;
        ThreadKey threadKey = this.A0S;
        int i46 = 0;
        if (threadKey != null) {
            i = threadKey.hashCode();
        } else {
            i = 0;
        }
        long j = this.A09;
        int i47 = ((i * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str = this.A0t;
        if (str != null) {
            i2 = str.hashCode();
        } else {
            i2 = 0;
        }
        int i48 = (i47 + i2) * 31;
        ImmutableList immutableList = this.A0m;
        if (immutableList != null) {
            i3 = immutableList.hashCode();
        } else {
            i3 = 0;
        }
        int i49 = (i48 + i3) * 31;
        ImmutableList immutableList2 = this.A0k;
        if (immutableList2 != null) {
            i4 = immutableList2.hashCode();
        } else {
            i4 = 0;
        }
        long j2 = this.A0B;
        long j3 = this.A07;
        long j4 = this.A03;
        long j5 = this.A0C;
        int i50 = (((((((((i49 + i4) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31;
        ImmutableList immutableList3 = this.A0n;
        if (immutableList3 != null) {
            i5 = immutableList3.hashCode();
        } else {
            i5 = 0;
        }
        int i51 = (i50 + i5) * 31;
        String str2 = this.A0w;
        if (str2 != null) {
            i6 = str2.hashCode();
        } else {
            i6 = 0;
        }
        int i52 = (i51 + i6) * 31;
        String str3 = this.A0p;
        if (str3 != null) {
            i7 = str3.hashCode();
        } else {
            i7 = 0;
        }
        int i53 = (i52 + i7) * 31;
        ParticipantInfo participantInfo = this.A0Q;
        if (participantInfo != null) {
            i8 = participantInfo.hashCode();
        } else {
            i8 = 0;
        }
        int i54 = (i53 + i8) * 31;
        String str4 = this.A0u;
        if (str4 != null) {
            i9 = str4.hashCode();
        } else {
            i9 = 0;
        }
        int i55 = (i54 + i9) * 31;
        Uri uri = this.A0E;
        if (uri != null) {
            i10 = uri.hashCode();
        } else {
            i10 = 0;
        }
        int i56 = (i55 + i10) * 31;
        ThreadMediaPreview threadMediaPreview = this.A0f;
        if (threadMediaPreview != null) {
            i11 = threadMediaPreview.hashCode();
        } else {
            i11 = 0;
        }
        int i57 = (((i56 + i11) * 31) + (this.A0y ? 1 : 0)) * 31;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason = this.A0H;
        if (graphQLMessageThreadCannotReplyReason != null) {
            i12 = graphQLMessageThreadCannotReplyReason.hashCode();
        } else {
            i12 = 0;
        }
        int i58 = (((((i57 + i12) * 31) + (this.A16 ? 1 : 0)) * 31) + (this.A10 ? 1 : 0)) * 31;
        C17930zi r0 = this.A0i;
        if (r0 != null) {
            i13 = r0.hashCode();
        } else {
            i13 = 0;
        }
        int i59 = (((i58 + i13) * 31) + (this.A0z ? 1 : 0)) * 31;
        C10950l8 r02 = this.A0O;
        if (r02 != null) {
            i14 = r02.hashCode();
        } else {
            i14 = 0;
        }
        int i60 = (i59 + i14) * 31;
        MessageDraft messageDraft = this.A0P;
        if (messageDraft != null) {
            i15 = messageDraft.hashCode();
        } else {
            i15 = 0;
        }
        int i61 = (i60 + i15) * 31;
        NotificationSetting notificationSetting = this.A0Y;
        if (notificationSetting != null) {
            i16 = notificationSetting.hashCode();
        } else {
            i16 = 0;
        }
        int i62 = (i61 + i16) * 31;
        GraphQLExtensibleMessageAdminTextType graphQLExtensibleMessageAdminTextType = this.A0G;
        if (graphQLExtensibleMessageAdminTextType != null) {
            i17 = graphQLExtensibleMessageAdminTextType.hashCode();
        } else {
            i17 = 0;
        }
        int i63 = (i62 + i17) * 31;
        ThreadCustomization threadCustomization = this.A0e;
        if (threadCustomization != null) {
            i18 = threadCustomization.hashCode();
        } else {
            i18 = 0;
        }
        int i64 = (i63 + i18) * 31;
        ThreadRtcCallInfoData threadRtcCallInfoData = this.A0h;
        if (threadRtcCallInfoData != null) {
            i19 = threadRtcCallInfoData.hashCode();
        } else {
            i19 = 0;
        }
        long j6 = this.A05;
        int i65 = (((((((i64 + i19) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + (this.A14 ? 1 : 0)) * 31) + this.A02) * 31;
        ImmutableList immutableList4 = this.A0l;
        if (immutableList4 != null) {
            i20 = immutableList4.hashCode();
        } else {
            i20 = 0;
        }
        int i66 = (i65 + i20) * 31;
        String str5 = this.A0s;
        if (str5 != null) {
            i21 = str5.hashCode();
        } else {
            i21 = 0;
        }
        int i67 = (i66 + i21) * 31;
        CallToAction callToAction = this.A0L;
        if (callToAction != null) {
            i22 = callToAction.hashCode();
        } else {
            i22 = 0;
        }
        long j7 = this.A08;
        int i68 = (((i67 + i22) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31;
        float f = this.A01;
        if (f != 0.0f) {
            i23 = Float.floatToIntBits(f);
        } else {
            i23 = 0;
        }
        int i69 = (i68 + i23) * 31;
        String str6 = this.A0q;
        if (str6 != null) {
            i24 = str6.hashCode();
        } else {
            i24 = 0;
        }
        int i70 = (i69 + i24) * 31;
        TriState triState = this.A0F;
        if (triState != null) {
            i25 = triState.hashCode();
        } else {
            i25 = 0;
        }
        int i71 = (i70 + i25) * 31;
        ImmutableMap immutableMap = this.A00;
        if (immutableMap != null) {
            i26 = immutableMap.hashCode();
        } else {
            i26 = 0;
        }
        int i72 = (i71 + i26) * 31;
        ThreadBookingRequests threadBookingRequests = this.A0c;
        if (threadBookingRequests != null) {
            i27 = threadBookingRequests.hashCode();
        } else {
            i27 = 0;
        }
        int i73 = (i72 + i27) * 31;
        MontageThreadPreview montageThreadPreview = this.A0X;
        if (montageThreadPreview != null) {
            i28 = montageThreadPreview.hashCode();
        } else {
            i28 = 0;
        }
        int i74 = (i73 + i28) * 31;
        ImmutableList immutableList5 = this.A0o;
        if (immutableList5 != null) {
            i29 = immutableList5.hashCode();
        } else {
            i29 = 0;
        }
        int i75 = (i74 + i29) * 31;
        ThreadKey threadKey2 = this.A0R;
        if (threadKey2 != null) {
            i30 = threadKey2.hashCode();
        } else {
            i30 = 0;
        }
        int i76 = (i75 + i30) * 31;
        GroupThreadData groupThreadData = this.A0V;
        if (groupThreadData != null) {
            i31 = groupThreadData.hashCode();
        } else {
            i31 = 0;
        }
        int i77 = (i76 + i31) * 31;
        MarketplaceThreadData marketplaceThreadData = this.A0W;
        if (marketplaceThreadData != null) {
            i32 = marketplaceThreadData.hashCode();
        } else {
            i32 = 0;
        }
        int i78 = (i77 + i32) * 31;
        AdContextData adContextData = this.A0T;
        if (adContextData != null) {
            i33 = adContextData.hashCode();
        } else {
            i33 = 0;
        }
        int i79 = (i78 + i33) * 31;
        AdsConversionsQPData adsConversionsQPData = this.A0U;
        if (adsConversionsQPData != null) {
            i34 = adsConversionsQPData.hashCode();
        } else {
            i34 = 0;
        }
        int hashCode = (((((i79 + i34) * 31) + this.A0Z.hashCode()) * 31) + (this.A18 ? 1 : 0)) * 31;
        GamesPushNotificationSettings gamesPushNotificationSettings = this.A0N;
        if (gamesPushNotificationSettings != null) {
            i35 = gamesPushNotificationSettings.hashCode();
        } else {
            i35 = 0;
        }
        int i80 = (hashCode + i35) * 31;
        Uri uri2 = this.A0D;
        if (uri2 != null) {
            i36 = uri2.hashCode();
        } else {
            i36 = 0;
        }
        long j8 = this.A0A;
        int hashCode2 = (((((((((((i80 + i36) * 31) + Arrays.hashCode(new Object[]{this.A0M})) * 31) + (this.A13 ? 1 : 0)) * 31) + (this.A17 ? 1 : 0)) * 31) + ((int) (j8 ^ (j8 >>> 32)))) * 31) + (this.A12 ? 1 : 0)) * 31;
        AnimatedThreadActivityBannerDataModel animatedThreadActivityBannerDataModel = this.A0j;
        if (animatedThreadActivityBannerDataModel != null) {
            i37 = animatedThreadActivityBannerDataModel.hashCode();
        } else {
            i37 = 0;
        }
        int hashCode3 = (((hashCode2 + i37) * 31) + this.A0J.hashCode()) * 31;
        CallToAction callToAction2 = this.A0K;
        if (callToAction2 != null) {
            i38 = callToAction2.hashCode();
        } else {
            i38 = 0;
        }
        int i81 = (hashCode3 + i38) * 31;
        ThreadConnectivityData threadConnectivityData = this.A0d;
        if (threadConnectivityData != null) {
            i39 = threadConnectivityData.hashCode();
        } else {
            i39 = 0;
        }
        int i82 = (i81 + i39) * 31;
        String str7 = this.A0x;
        if (str7 != null) {
            i40 = str7.hashCode();
        } else {
            i40 = 0;
        }
        int i83 = (i82 + i40) * 31;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType = this.A0I;
        if (graphQLMessengerGroupThreadSubType != null) {
            i41 = graphQLMessengerGroupThreadSubType.hashCode();
        } else {
            i41 = 0;
        }
        int i84 = (((i83 + i41) * 31) + (this.A15 ? 1 : 0)) * 31;
        String str8 = this.A0r;
        if (str8 != null) {
            i42 = str8.hashCode();
        } else {
            i42 = 0;
        }
        int i85 = (i84 + i42) * 31;
        RequestAppointmentData requestAppointmentData = this.A0b;
        if (requestAppointmentData != null) {
            i43 = requestAppointmentData.hashCode();
        } else {
            i43 = 0;
        }
        int i86 = (i85 + i43) * 31;
        RelatedPageThreadData relatedPageThreadData = this.A0a;
        if (relatedPageThreadData != null) {
            i44 = relatedPageThreadData.hashCode();
        } else {
            i44 = 0;
        }
        int i87 = (i86 + i44) * 31;
        ThreadPageMessageAssignedAdmin threadPageMessageAssignedAdmin = this.A0g;
        if (threadPageMessageAssignedAdmin != null) {
            i45 = threadPageMessageAssignedAdmin.hashCode();
        } else {
            i45 = 0;
        }
        int i88 = (i87 + i45) * 31;
        String str9 = this.A0v;
        if (str9 != null) {
            i46 = str9.hashCode();
        }
        return ((i88 + i46) * 31) + (this.A11 ? 1 : 0);
    }

    public String toString() {
        String replace;
        String replace2;
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(ThreadSummary.class);
        stringHelper.add("threadKey", this.A0S);
        stringHelper.add("folder", this.A0O.toString());
        stringHelper.add("name", this.A0t);
        stringHelper.add("unread", A0B());
        stringHelper.add("timestampMs", this.A0B);
        stringHelper.add("lastReadWatermarkTimestampMs", this.A07);
        stringHelper.add("lastMessageTimestampMs", this.A06);
        stringHelper.add(C99084oO.$const$string(77), this.A0m);
        stringHelper.add("senders", this.A0n);
        String str = this.A0w;
        if (str == null) {
            replace = null;
        } else {
            replace = str.replace("\n", " ");
        }
        stringHelper.add("snippet", replace);
        String str2 = this.A0p;
        if (str2 == null) {
            replace2 = null;
        } else {
            replace2 = str2.replace("\n", " ");
        }
        stringHelper.add("adminSnippet", replace2);
        stringHelper.add("threadCustomization", this.A0e);
        stringHelper.add("outgoingMessageLifetime", this.A02);
        stringHelper.add("subscribed", this.A16);
        stringHelper.add(AnonymousClass24B.$const$string(AnonymousClass1Y3.A1e), this.A0y);
        stringHelper.add("lastCallMs", this.A05);
        stringHelper.add("missedCallStatus", this.A0i.name());
        stringHelper.add("optimisticGroupState", this.A0Z);
        stringHelper.add("useExistingGroup", this.A18);
        stringHelper.add(C99084oO.$const$string(AnonymousClass1Y3.A1n), this.A0M);
        stringHelper.add("isFussRedPage", this.A13);
        stringHelper.add("isPinned", this.A17);
        return stringHelper.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A0S, i);
        parcel.writeLong(this.A09);
        parcel.writeString(this.A0t);
        parcel.writeTypedList(this.A0m);
        parcel.writeTypedList(this.A0k);
        parcel.writeLong(this.A0B);
        parcel.writeLong(this.A07);
        parcel.writeLong(this.A03);
        parcel.writeLong(this.A0C);
        parcel.writeTypedList(this.A0n);
        parcel.writeString(this.A0w);
        parcel.writeString(this.A0p);
        parcel.writeParcelable(this.A0Q, i);
        parcel.writeString(this.A0u);
        parcel.writeParcelable(this.A0E, i);
        parcel.writeParcelable(this.A0f, i);
        C417826y.A0V(parcel, this.A0y);
        C417826y.A0L(parcel, this.A0H);
        C417826y.A0L(parcel, this.A0G);
        C417826y.A0V(parcel, this.A16);
        C417826y.A0V(parcel, this.A10);
        C417826y.A0L(parcel, this.A0i);
        C417826y.A0V(parcel, this.A0z);
        C417826y.A0V(parcel, this.A12);
        parcel.writeString(this.A0O.dbName);
        parcel.writeParcelable(this.A0P, i);
        parcel.writeParcelable(this.A0Y, i);
        parcel.writeParcelable(this.A0e, i);
        C417826y.A0V(parcel, this.A14);
        parcel.writeInt(this.A02);
        parcel.writeTypedList(this.A0l);
        parcel.writeString(this.A0s);
        parcel.writeLong(this.A08);
        parcel.writeFloat(this.A01);
        parcel.writeParcelable(this.A0h, i);
        parcel.writeString(this.A0q);
        C417826y.A0E(parcel, this.A0F);
        parcel.writeParcelable(this.A0c, i);
        parcel.writeLong(this.A05);
        parcel.writeParcelable(this.A0L, i);
        parcel.writeParcelable(this.A0X, i);
        parcel.writeTypedList(this.A0o);
        parcel.writeParcelable(this.A0R, i);
        parcel.writeParcelable(this.A0V, i);
        parcel.writeParcelable(this.A0W, i);
        parcel.writeParcelable(this.A0T, i);
        parcel.writeParcelable(this.A0U, i);
        C417826y.A0L(parcel, this.A0Z);
        C417826y.A0V(parcel, this.A18);
        parcel.writeParcelable(this.A0N, i);
        parcel.writeParcelable(this.A0D, i);
        parcel.writeParcelable(this.A0M, i);
        C417826y.A0V(parcel, this.A13);
        C417826y.A0V(parcel, this.A17);
        parcel.writeLong(this.A0A);
        parcel.writeParcelable(this.A0j, i);
        C417826y.A0L(parcel, this.A0J);
        parcel.writeParcelable(this.A0K, i);
        parcel.writeParcelable(this.A0d, i);
        parcel.writeString(this.A0x);
        C417826y.A0L(parcel, this.A0I);
        C417826y.A0V(parcel, this.A15);
        parcel.writeString(this.A0r);
        parcel.writeLong(this.A06);
        parcel.writeParcelable(this.A0b, i);
        parcel.writeParcelable(this.A0a, i);
        parcel.writeParcelable(this.A0g, i);
        parcel.writeString(this.A0v);
        parcel.writeLong(this.A04);
        C417826y.A0V(parcel, this.A11);
    }

    private void A02(GroupThreadData groupThreadData) {
        Preconditions.checkNotNull(groupThreadData);
        if (!this.A0S.A0L()) {
            Preconditions.checkArgument(!groupThreadData.A04.A06);
            Preconditions.checkArgument(!groupThreadData.A04.A02.A02);
        }
    }

    public UserKey A05() {
        String str;
        ThreadParticipant A032 = A03();
        if (A032 == null || (str = A032.A04.A05) == null) {
            return null;
        }
        return UserKey.A01(str);
    }

    public ThreadSummary(C17920zh r4) {
        Preconditions.checkNotNull(r4.A0N);
        Preconditions.checkNotNull(r4.A0R);
        this.A0S = r4.A0R;
        this.A09 = r4.A08;
        this.A0t = r4.A0p;
        this.A0m = r4.A0k;
        this.A0k = r4.A0j;
        this.A0B = r4.A0A;
        this.A07 = r4.A06;
        this.A06 = r4.A05;
        this.A03 = r4.A02;
        this.A0C = r4.A0B;
        this.A0n = ImmutableList.copyOf((Collection) r4.A0v);
        this.A0w = r4.A0s;
        this.A0p = r4.A0l;
        this.A0Q = r4.A0P;
        this.A0u = r4.A0q;
        this.A0E = r4.A0D;
        this.A0f = r4.A0e;
        this.A0y = r4.A0x;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason = r4.A0G;
        this.A0H = graphQLMessageThreadCannotReplyReason == null ? GraphQLMessageThreadCannotReplyReason.A07 : graphQLMessageThreadCannotReplyReason;
        GraphQLExtensibleMessageAdminTextType graphQLExtensibleMessageAdminTextType = r4.A0F;
        this.A0G = graphQLExtensibleMessageAdminTextType == null ? GraphQLExtensibleMessageAdminTextType.A3z : graphQLExtensibleMessageAdminTextType;
        this.A16 = r4.A15;
        this.A10 = r4.A0z;
        this.A0i = r4.A0h;
        this.A0z = r4.A0y;
        this.A12 = r4.A11;
        this.A0O = r4.A0N;
        this.A0P = r4.A0O;
        this.A0Y = r4.A0X;
        this.A0e = r4.A0d;
        this.A14 = r4.A13;
        this.A02 = r4.A01;
        this.A0l = ImmutableList.copyOf((Collection) r4.A0u);
        this.A0s = r4.A0o;
        this.A0L = r4.A0K;
        this.A08 = r4.A07;
        this.A01 = r4.A00;
        this.A0h = r4.A0g;
        this.A0q = r4.A0m;
        TriState triState = r4.A0E;
        Preconditions.checkNotNull(triState);
        if (!this.A0S.A0L()) {
            Preconditions.checkArgument(triState != TriState.YES);
        }
        this.A0F = triState;
        this.A0c = r4.A0b;
        this.A05 = r4.A04;
        this.A0X = r4.A0W;
        this.A0o = ImmutableList.copyOf((Collection) r4.A0w);
        this.A0R = r4.A0Q;
        GroupThreadData groupThreadData = r4.A0U;
        A02(groupThreadData);
        this.A0V = groupThreadData;
        this.A0W = r4.A0V;
        this.A0T = r4.A0S;
        this.A0U = r4.A0T;
        this.A0Z = r4.A0Y;
        this.A18 = r4.A17;
        this.A0N = r4.A0M;
        this.A0D = r4.A0C;
        this.A0M = r4.A0L;
        this.A13 = r4.A12;
        this.A17 = r4.A16;
        this.A0A = r4.A09;
        this.A0j = r4.A0i;
        this.A0J = r4.A0I;
        this.A0K = r4.A0J;
        this.A0d = r4.A0c;
        this.A0x = r4.A0t;
        this.A0I = r4.A0H;
        this.A15 = r4.A14;
        this.A0r = r4.A0n;
        this.A0b = r4.A0a;
        this.A0a = r4.A0Z;
        this.A0g = r4.A0f;
        this.A0v = r4.A0r;
        this.A04 = r4.A03;
        this.A11 = r4.A10;
    }

    public ThreadSummary(Parcel parcel) {
        Class<ThreadKey> cls = ThreadKey.class;
        this.A0S = (ThreadKey) parcel.readParcelable(cls.getClassLoader());
        this.A09 = parcel.readLong();
        this.A0t = parcel.readString();
        this.A0m = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ThreadParticipant.CREATOR));
        this.A0k = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ThreadParticipant.CREATOR));
        this.A0B = parcel.readLong();
        this.A07 = parcel.readLong();
        this.A03 = parcel.readLong();
        this.A0C = parcel.readLong();
        this.A0n = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ParticipantInfo.CREATOR));
        this.A0w = parcel.readString();
        this.A0p = parcel.readString();
        this.A0Q = (ParticipantInfo) parcel.readParcelable(ParticipantInfo.class.getClassLoader());
        this.A0u = parcel.readString();
        this.A0E = (Uri) parcel.readParcelable(null);
        this.A0f = (ThreadMediaPreview) parcel.readParcelable(ThreadMediaPreview.class.getClassLoader());
        this.A0y = C417826y.A0W(parcel);
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason = (GraphQLMessageThreadCannotReplyReason) C417826y.A0C(parcel, GraphQLMessageThreadCannotReplyReason.class);
        this.A0H = graphQLMessageThreadCannotReplyReason == null ? GraphQLMessageThreadCannotReplyReason.A07 : graphQLMessageThreadCannotReplyReason;
        GraphQLExtensibleMessageAdminTextType graphQLExtensibleMessageAdminTextType = (GraphQLExtensibleMessageAdminTextType) C417826y.A0C(parcel, GraphQLExtensibleMessageAdminTextType.class);
        this.A0G = graphQLExtensibleMessageAdminTextType == null ? GraphQLExtensibleMessageAdminTextType.A3z : graphQLExtensibleMessageAdminTextType;
        this.A16 = C417826y.A0W(parcel);
        this.A10 = C417826y.A0W(parcel);
        this.A0i = (C17930zi) C417826y.A0C(parcel, C17930zi.class);
        this.A0z = C417826y.A0W(parcel);
        this.A12 = C417826y.A0W(parcel);
        this.A0O = C10950l8.A00(parcel.readString());
        this.A0P = (MessageDraft) parcel.readParcelable(MessageDraft.class.getClassLoader());
        this.A0Y = (NotificationSetting) parcel.readParcelable(NotificationSetting.class.getClassLoader());
        this.A0e = (ThreadCustomization) parcel.readParcelable(ThreadCustomization.class.getClassLoader());
        this.A14 = C417826y.A0W(parcel);
        this.A02 = parcel.readInt();
        this.A0l = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ThreadEventReminder.CREATOR));
        this.A0s = parcel.readString();
        this.A08 = parcel.readLong();
        this.A01 = parcel.readFloat();
        this.A0h = (ThreadRtcCallInfoData) parcel.readParcelable(ThreadRtcCallInfoData.class.getClassLoader());
        this.A0q = parcel.readString();
        TriState A012 = C417826y.A01(parcel);
        Preconditions.checkNotNull(A012);
        if (!this.A0S.A0L()) {
            Preconditions.checkArgument(A012 != TriState.YES);
        }
        this.A0F = A012;
        this.A0c = (ThreadBookingRequests) parcel.readParcelable(ThreadBookingRequests.class.getClassLoader());
        this.A05 = parcel.readLong();
        Class<CallToAction> cls2 = CallToAction.class;
        this.A0L = (CallToAction) parcel.readParcelable(cls2.getClassLoader());
        this.A0X = (MontageThreadPreview) parcel.readParcelable(MontageThreadPreview.class.getClassLoader());
        this.A0o = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(Message.CREATOR));
        this.A0R = (ThreadKey) parcel.readParcelable(cls.getClassLoader());
        GroupThreadData groupThreadData = (GroupThreadData) parcel.readParcelable(GroupThreadData.class.getClassLoader());
        A02(groupThreadData);
        this.A0V = groupThreadData;
        this.A0W = (MarketplaceThreadData) parcel.readParcelable(MarketplaceThreadData.class.getClassLoader());
        this.A0T = (AdContextData) parcel.readParcelable(AdContextData.class.getClassLoader());
        this.A0U = (AdsConversionsQPData) parcel.readParcelable(AdsConversionsQPData.class.getClassLoader());
        this.A0Z = (AnonymousClass105) C417826y.A0C(parcel, AnonymousClass105.class);
        this.A18 = C417826y.A0W(parcel);
        this.A0N = (GamesPushNotificationSettings) parcel.readParcelable(GamesPushNotificationSettings.class.getClassLoader());
        this.A0D = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.A0M = (ThreadThemeInfo) parcel.readParcelable(ThreadThemeInfo.class.getClassLoader());
        this.A13 = C417826y.A0W(parcel);
        this.A17 = C417826y.A0W(parcel);
        this.A0A = parcel.readLong();
        this.A0j = (AnimatedThreadActivityBannerDataModel) parcel.readParcelable(AnimatedThreadActivityBannerDataModel.class.getClassLoader());
        GraphQLMessengerXMAGroupingType graphQLMessengerXMAGroupingType = (GraphQLMessengerXMAGroupingType) C417826y.A0C(parcel, GraphQLMessengerXMAGroupingType.class);
        this.A0J = graphQLMessengerXMAGroupingType == null ? GraphQLMessengerXMAGroupingType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE : graphQLMessengerXMAGroupingType;
        this.A0K = (CallToAction) parcel.readParcelable(cls2.getClassLoader());
        this.A0d = (ThreadConnectivityData) parcel.readParcelable(ThreadConnectivityData.class.getClassLoader());
        this.A0x = parcel.readString();
        this.A0I = (GraphQLMessengerGroupThreadSubType) C417826y.A0C(parcel, GraphQLMessengerGroupThreadSubType.class);
        this.A15 = C417826y.A0W(parcel);
        this.A0r = parcel.readString();
        this.A06 = parcel.readLong();
        this.A0b = (RequestAppointmentData) parcel.readParcelable(RequestAppointmentData.class.getClassLoader());
        this.A0a = (RelatedPageThreadData) parcel.readParcelable(RelatedPageThreadData.class.getClassLoader());
        this.A0g = (ThreadPageMessageAssignedAdmin) parcel.readParcelable(ThreadPageMessageAssignedAdmin.class.getClassLoader());
        this.A0v = parcel.readString();
        this.A04 = parcel.readLong();
        this.A11 = C417826y.A0W(parcel);
    }
}
