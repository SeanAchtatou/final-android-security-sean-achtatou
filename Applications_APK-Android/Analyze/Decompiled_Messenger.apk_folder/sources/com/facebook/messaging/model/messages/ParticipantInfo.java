package com.facebook.messaging.model.messages;

import X.AnonymousClass1Y3;
import X.C181810n;
import X.C25651aB;
import X.C25661aC;
import X.C28811fP;
import X.C28821fQ;
import X.C417826y;
import X.C99084oO;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.util.Comparator;

public final class ParticipantInfo implements Parcelable {
    public static final C181810n A07 = C181810n.A00(String.CASE_INSENSITIVE_ORDER).A03();
    public static final Comparator A08 = new C28811fP();
    public static final Parcelable.Creator CREATOR = new C28821fQ();
    public final C25661aC A00;
    public final UserKey A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final boolean A06;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ParticipantInfo participantInfo = (ParticipantInfo) obj;
            if (!Objects.equal(this.A02, participantInfo.A02) || !Objects.equal(this.A04, participantInfo.A04) || !Objects.equal(this.A05, participantInfo.A05) || !Objects.equal(this.A03, participantInfo.A03) || !Objects.equal(this.A01, participantInfo.A01) || this.A06 != participantInfo.A06 || this.A00 != participantInfo.A00) {
                return false;
            }
        }
        return true;
    }

    public String A00() {
        UserKey userKey = this.A01;
        if (userKey.A08()) {
            return this.A05;
        }
        return userKey.id;
    }

    public boolean A01() {
        if (this.A01.type == C25651aB.A03) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        UserKey userKey = this.A01;
        if (userKey != null) {
            return userKey.hashCode();
        }
        return 0;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(ParticipantInfo.class);
        stringHelper.add("userKey", this.A01.A06());
        stringHelper.add("name", this.A03);
        stringHelper.add("emailAddress", this.A02);
        stringHelper.add(C99084oO.$const$string(AnonymousClass1Y3.A1e), this.A04);
        stringHelper.add("smsParticipantFbid", this.A05);
        stringHelper.add("isCommerce", this.A06);
        stringHelper.add("messagingActorType", this.A00.name());
        return stringHelper.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A04);
        parcel.writeString(this.A05);
        parcel.writeString(this.A03);
        parcel.writeString(this.A01.A06());
        C417826y.A0V(parcel, this.A06);
        parcel.writeString(this.A00.name());
    }

    public ParticipantInfo(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A04 = parcel.readString();
        this.A05 = parcel.readString();
        this.A03 = parcel.readString();
        UserKey A022 = UserKey.A02(parcel.readString());
        Preconditions.checkNotNull(A022, "Attempting to create ParticipantInfo with a null UserKey");
        this.A01 = A022;
        this.A06 = C417826y.A0W(parcel);
        this.A00 = C25661aC.valueOf(parcel.readString());
    }

    public ParticipantInfo(User user) {
        this.A01 = user.A0Q;
        this.A03 = user.A09();
        this.A02 = user.A0E();
        this.A04 = user.A0C();
        this.A05 = null;
        this.A06 = user.A13;
        this.A00 = user.A0I;
    }

    public ParticipantInfo(UserKey userKey, String str) {
        this(userKey, str, null, null, null, false, C25661aC.UNSET);
    }

    public ParticipantInfo(UserKey userKey, String str, String str2, C25661aC r12) {
        this(userKey, str, str2, null, null, false, r12);
    }

    public ParticipantInfo(UserKey userKey, String str, String str2, String str3, String str4, boolean z, C25661aC r8) {
        Preconditions.checkNotNull(userKey, "Attempting to create ParticipantInfo with a null UserKey");
        this.A01 = userKey;
        this.A03 = str;
        this.A02 = str2;
        this.A04 = str3;
        this.A05 = str4;
        this.A06 = z;
        Preconditions.checkNotNull(r8, "Attempting to create ParticipantInfo with a null MessagingActorType");
        this.A00 = r8;
    }
}
