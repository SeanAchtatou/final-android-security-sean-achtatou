package com.facebook.messaging.model.threads;

import X.AnonymousClass1TG;
import X.AnonymousClass1V7;
import X.AnonymousClass1Y3;
import X.AnonymousClass2VF;
import X.C36891u4;
import X.C417826y;
import X.C42742Bq;
import X.C42752Br;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.montagemetadata.MontageMetadata;
import com.facebook.messaging.model.send.SendError;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

public final class MontageThreadPreview implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C42742Bq();
    public final long A00;
    public final Attachment A01;
    public final AnonymousClass1V7 A02;
    public final ParticipantInfo A03;
    public final MontageMetadata A04;
    public final MediaResource A05;
    public final String A06;
    public final String A07;
    public final String A08;

    public static C42752Br A00(AnonymousClass1V7 r2) {
        if (r2 != null) {
            switch (r2.ordinal()) {
                case 1:
                    return C42752Br.REGULAR;
                case AnonymousClass1Y3.A07:
                    return C42752Br.BLOCKED;
                case 25:
                    return C42752Br.PENDING;
                case AnonymousClass1Y3.A08:
                    return C42752Br.FAILED;
            }
        }
        return null;
    }

    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        if (r1.equals(r8.A01) == false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 1
            if (r7 == r8) goto L_0x008a
            r5 = 0
            if (r8 == 0) goto L_0x0030
            java.lang.Class r1 = r7.getClass()
            java.lang.Class r0 = r8.getClass()
            if (r1 != r0) goto L_0x0030
            com.facebook.messaging.model.threads.MontageThreadPreview r8 = (com.facebook.messaging.model.threads.MontageThreadPreview) r8
            long r3 = r7.A00
            long r1 = r8.A00
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0030
            java.lang.String r1 = r7.A06
            java.lang.String r0 = r8.A06
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0030
            com.facebook.messaging.model.attachment.Attachment r1 = r7.A01
            if (r1 == 0) goto L_0x0031
            com.facebook.messaging.model.attachment.Attachment r0 = r8.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0036
        L_0x0030:
            return r5
        L_0x0031:
            com.facebook.messaging.model.attachment.Attachment r0 = r8.A01
            if (r0 == 0) goto L_0x0036
            return r5
        L_0x0036:
            java.lang.String r1 = r7.A07
            if (r1 == 0) goto L_0x0043
            java.lang.String r0 = r8.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0048
            return r5
        L_0x0043:
            java.lang.String r0 = r8.A07
            if (r0 == 0) goto L_0x0048
            return r5
        L_0x0048:
            X.1V7 r1 = r7.A02
            X.1V7 r0 = r8.A02
            if (r1 != r0) goto L_0x0030
            com.facebook.ui.media.attachments.model.MediaResource r1 = r7.A05
            if (r1 == 0) goto L_0x005b
            com.facebook.ui.media.attachments.model.MediaResource r0 = r8.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0060
            return r5
        L_0x005b:
            com.facebook.ui.media.attachments.model.MediaResource r0 = r8.A05
            if (r0 == 0) goto L_0x0060
            return r5
        L_0x0060:
            com.facebook.messaging.model.messages.ParticipantInfo r1 = r7.A03
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r8.A03
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0030
            com.facebook.messaging.model.montagemetadata.MontageMetadata r1 = r7.A04
            if (r1 == 0) goto L_0x0077
            com.facebook.messaging.model.montagemetadata.MontageMetadata r0 = r8.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x007c
            return r5
        L_0x0077:
            com.facebook.messaging.model.montagemetadata.MontageMetadata r0 = r8.A04
            if (r0 == 0) goto L_0x007c
            return r5
        L_0x007c:
            java.lang.String r1 = r7.A08
            java.lang.String r0 = r8.A08
            if (r1 == 0) goto L_0x0087
            boolean r6 = r1.equals(r0)
            return r6
        L_0x0087:
            if (r0 == 0) goto L_0x008a
            r6 = 0
        L_0x008a:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.model.threads.MontageThreadPreview.equals(java.lang.Object):boolean");
    }

    public boolean A02() {
        MontageMetadata montageMetadata = this.A04;
        if (montageMetadata == null || !montageMetadata.ArM().booleanValue()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        long j = this.A00;
        int hashCode = ((this.A06.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        Attachment attachment = this.A01;
        int i7 = 0;
        if (attachment != null) {
            i = attachment.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 31;
        String str = this.A07;
        if (str != null) {
            i2 = str.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 31;
        String str2 = this.A08;
        if (str2 != null) {
            i3 = str2.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 31;
        AnonymousClass1V7 r0 = this.A02;
        if (r0 != null) {
            i4 = r0.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 31;
        MediaResource mediaResource = this.A05;
        if (mediaResource != null) {
            i5 = mediaResource.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 31;
        ParticipantInfo participantInfo = this.A03;
        if (participantInfo != null) {
            i6 = participantInfo.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 31;
        MontageMetadata montageMetadata = this.A04;
        if (montageMetadata != null) {
            i7 = montageMetadata.hashCode();
        }
        return i13 + i7;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A00);
        parcel.writeString(this.A06);
        parcel.writeParcelable(this.A01, i);
        parcel.writeString(this.A07);
        parcel.writeString(this.A08);
        C417826y.A0L(parcel, this.A02);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A04, i);
    }

    public Message A01(ThreadKey threadKey) {
        ImmutableList immutableList;
        ImmutableList immutableList2;
        AnonymousClass1TG A002 = Message.A00();
        A002.A06(this.A06);
        A002.A0C = this.A02;
        A002.A0T = threadKey;
        A002.A0x = this.A07;
        A002.A0y = this.A08;
        Attachment attachment = this.A01;
        if (attachment != null) {
            immutableList = ImmutableList.of(attachment);
        } else {
            immutableList = RegularImmutableList.A02;
        }
        A002.A08(immutableList);
        MediaResource mediaResource = this.A05;
        if (mediaResource != null) {
            immutableList2 = ImmutableList.of(mediaResource);
        } else {
            immutableList2 = RegularImmutableList.A02;
        }
        A002.A09(immutableList2);
        A002.A0J = this.A03;
        A002.A0N = this.A04;
        A002.A04 = this.A00;
        if (this.A02 == AnonymousClass1V7.A0A) {
            A002.A0R = new SendError(C36891u4.A08);
        }
        return A002.A00();
    }

    public MontageThreadPreview(AnonymousClass2VF r3) {
        this.A00 = r3.A00;
        this.A06 = r3.A05;
        this.A02 = r3.A02;
        this.A03 = r3.A08;
        this.A01 = r3.A01;
        this.A07 = r3.A06;
        this.A08 = r3.A07;
        this.A05 = r3.A04;
        this.A04 = r3.A03;
    }

    public MontageThreadPreview(Parcel parcel) {
        this.A00 = parcel.readLong();
        this.A06 = parcel.readString();
        this.A01 = (Attachment) parcel.readParcelable(Attachment.class.getClassLoader());
        this.A07 = parcel.readString();
        this.A08 = parcel.readString();
        this.A02 = (AnonymousClass1V7) C417826y.A0C(parcel, AnonymousClass1V7.class);
        this.A05 = (MediaResource) parcel.readParcelable(MediaResource.class.getClassLoader());
        this.A03 = (ParticipantInfo) parcel.readParcelable(ParticipantInfo.class.getClassLoader());
        this.A04 = (MontageMetadata) parcel.readParcelable(MontageMetadata.class.getClassLoader());
    }
}
