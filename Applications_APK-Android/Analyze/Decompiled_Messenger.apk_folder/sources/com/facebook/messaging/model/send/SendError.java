package com.facebook.messaging.model.send;

import X.AnonymousClass1Y3;
import X.AnonymousClass24B;
import X.AnonymousClass80H;
import X.C196899Nw;
import X.C36891u4;
import X.C36901u5;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.MoreObjects;

public final class SendError implements Parcelable {
    public static final SendError A08 = new SendError(C36891u4.A07);
    public static final Parcelable.Creator CREATOR = new C36901u5();
    public final int A00;
    public final long A01;
    public final C36891u4 A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        C417826y.A0L(parcel, this.A02);
        parcel.writeString(this.A06);
        parcel.writeString(this.A03);
        parcel.writeString(this.A07);
        parcel.writeLong(this.A01);
        parcel.writeString(this.A04);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A05);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("timeStamp", this.A01);
        stringHelper.add("type", this.A02.serializedString);
        stringHelper.add(AnonymousClass24B.$const$string(231), this.A06);
        stringHelper.add(AnonymousClass80H.$const$string(AnonymousClass1Y3.A19), this.A00);
        stringHelper.add("errorUrl", this.A04);
        return stringHelper.toString();
    }

    public SendError(C36891u4 r4) {
        this.A02 = r4;
        this.A06 = null;
        this.A03 = null;
        this.A07 = null;
        this.A01 = -1;
        this.A04 = null;
        this.A00 = -1;
        this.A05 = null;
    }

    public SendError(C196899Nw r3) {
        this.A02 = r3.A02;
        this.A06 = r3.A06;
        this.A03 = r3.A03;
        this.A07 = r3.A07;
        this.A01 = r3.A01;
        this.A04 = r3.A04;
        this.A00 = r3.A00;
        this.A05 = r3.A05;
    }

    public SendError(Parcel parcel) {
        this.A02 = (C36891u4) C417826y.A0C(parcel, C36891u4.class);
        this.A06 = parcel.readString();
        this.A03 = parcel.readString();
        this.A07 = parcel.readString();
        this.A01 = parcel.readLong();
        this.A04 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A05 = parcel.readString();
    }
}
