package com.facebook.messaging.conversationstarters;

import X.AnonymousClass324;
import X.AnonymousClass44C;
import X.C16880xv;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.model.threads.ThreadSummary;

public final class InboxUnitConversationStarterItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44C();
    public final C16880xv A00;
    public final ThreadSummary A01;

    public void A0H(Parcel parcel, int i) {
        super.A0H(parcel, i);
        Bundle bundle = new Bundle();
        AnonymousClass324.A09(bundle, "fields", this.A02);
        parcel.writeBundle(bundle);
        parcel.writeParcelable(this.A01, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (r4 != null) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public InboxUnitConversationStarterItem(X.C27161ck r2, com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3, com.facebook.messaging.model.threads.ThreadSummary r4, X.C16880xv r5) {
        /*
            r1 = this;
            r0 = 0
            r1.<init>(r2, r3, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r5.Are()
            if (r0 != 0) goto L_0x000d
            r0 = 0
            if (r4 == 0) goto L_0x000e
        L_0x000d:
            r0 = 1
        L_0x000e:
            com.google.common.base.Preconditions.checkArgument(r0)
            r1.A01 = r4
            r1.A00 = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem.<init>(X.1ck, com.facebook.graphservice.modelutil.GSTModelShape1S0000000, com.facebook.messaging.model.threads.ThreadSummary, X.0xv):void");
    }

    public InboxUnitConversationStarterItem(Parcel parcel) {
        super(parcel);
        Bundle readBundle = parcel.readBundle();
        readBundle.setClassLoader(InboxUnitItem.class.getClassLoader());
        this.A00 = (C16880xv) AnonymousClass324.A02(readBundle, "fields");
        this.A01 = (ThreadSummary) parcel.readParcelable(ThreadSummary.class.getClassLoader());
    }
}
