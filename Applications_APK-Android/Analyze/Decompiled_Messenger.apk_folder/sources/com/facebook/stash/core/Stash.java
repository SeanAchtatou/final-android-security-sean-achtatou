package com.facebook.stash.core;

import java.io.File;
import java.util.Set;

public interface Stash {
    Set getAllKeys();

    File getResource(String str);

    File getResourcePath(String str);

    long getSizeBytes();

    boolean hasKey(String str);

    File insert(String str);

    boolean remove(String str);

    boolean removeAll();
}
