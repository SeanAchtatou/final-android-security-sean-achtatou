package com.facebook.messenger.splashscreen;

import X.AnonymousClass01E;
import X.AnonymousClass01R;
import X.AnonymousClass01S;
import X.AnonymousClass1Y3;
import X.C000700l;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.FrameLayout;
import com.facebook.base.app.SplashScreenActivity;
import com.facebook.common.asyncview.SpriteView;

public class MessengerSplashScreenActivity extends SplashScreenActivity {
    public SpriteView A00;
    public AnonymousClass01S A01;

    private void A01(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(i);
            window.getDecorView().setSystemUiVisibility(AnonymousClass1Y3.AAV);
        }
    }

    public void onCreate(Bundle bundle) {
        int A002 = C000700l.A00(-1948617271);
        super.onCreate(bundle);
        if (isFinishing()) {
            C000700l.A07(928768493, A002);
            return;
        }
        if (getFileStreamPath("orca_splash_screen2.enabled").exists()) {
            AnonymousClass01E r0 = new AnonymousClass01E(this);
            this.A00 = r0;
            setContentView(r0);
            A01(AnonymousClass01R.A00(this, 2132083190));
        } else if (getFileStreamPath("orca_splash_screen.enabled").exists()) {
            FrameLayout frameLayout = new FrameLayout(this);
            AnonymousClass01S r2 = new AnonymousClass01S(this, BitmapFactory.decodeResource(getResources(), 2132346704));
            this.A01 = r2;
            frameLayout.addView(r2);
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.A01.getLayoutParams();
            layoutParams.gravity = 17;
            this.A01.setLayoutParams(layoutParams);
            setContentView(frameLayout);
            A01(-16750641);
        }
        C000700l.A07(-80908058, A002);
    }

    public void onDestroy() {
        int A002 = C000700l.A00(1444278661);
        AnonymousClass01S r0 = this.A01;
        if (r0 != null) {
            r0.A0I();
        }
        SpriteView spriteView = this.A00;
        if (spriteView != null) {
            spriteView.A0I();
        }
        super.onDestroy();
        C000700l.A07(-1228900451, A002);
    }
}
