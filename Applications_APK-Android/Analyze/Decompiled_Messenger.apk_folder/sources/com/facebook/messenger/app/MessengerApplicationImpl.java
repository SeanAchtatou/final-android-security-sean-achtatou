package com.facebook.messenger.app;

import X.AnonymousClass00M;
import X.AnonymousClass00T;
import X.AnonymousClass04W;
import X.AnonymousClass08W;
import X.AnonymousClass0UN;
import X.AnonymousClass0Ud;
import X.AnonymousClass0WE;
import X.AnonymousClass0WP;
import X.AnonymousClass0XN;
import X.AnonymousClass0g4;
import X.AnonymousClass108;
import X.AnonymousClass1MZ;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass2AI;
import X.C001300x;
import X.C04460Ut;
import X.C08790fx;
import X.C179688Sa;
import X.C50362dr;
import X.C91694Zm;
import android.app.Application;
import com.facebook.base.app.AbstractApplicationLike;
import com.facebook.common.callercontext.CallerContextable;

public class MessengerApplicationImpl extends AbstractApplicationLike implements AnonymousClass04W, CallerContextable {
    public long A00;
    public C04460Ut A01;
    public AnonymousClass0Ud A02;
    public AnonymousClass0WE A03;
    public AnonymousClass0UN A04;
    private AnonymousClass08W A05;
    private AnonymousClass00T A06;

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x040c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x040d, code lost:
        X.C005505z.A00(-923420670);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0413, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005b, code lost:
        if ((r4.lastModified() / 1000) < (r7.getPackageManager().getPackageInfo(r7.getPackageName(), 0).lastUpdateTime / 1000)) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0219, code lost:
        if (r4.getAction().equals("android.intent.action.MAIN") == false) goto L_0x021b;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ba A[Catch:{ all -> 0x040c }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bd A[Catch:{ all -> 0x040c }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0119 A[Catch:{ all -> 0x040c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r18 = this;
            r3 = r18
            android.app.Application r7 = r3.A02
            r6 = 3
            java.lang.String r1 = "appcomponents"
            r0 = 0
            java.io.File r2 = r7.getDir(r1, r0)
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "versions"
            r1.<init>(r2, r0)
            int r8 = com.facebook.common.build.BuildConstants.A00()
            java.io.File r4 = new java.io.File
            java.lang.String r0 = java.lang.Integer.toString(r8)
            r4.<init>(r1, r0)
            boolean r1 = r4.exists()
            java.lang.String r5 = "Can't get package info for this package."
            r2 = 0
            r0 = 1
            if (r1 != 0) goto L_0x0041
            java.lang.String r4 = "AppComponentManager"
            android.content.pm.PackageManager r1 = r7.getPackageManager()
            java.lang.String r0 = r7.getPackageName()     // Catch:{ NameNotFoundException -> 0x005e }
            android.content.pm.PackageInfo r0 = r1.getPackageInfo(r0, r2)     // Catch:{ NameNotFoundException -> 0x005e }
            int r1 = r0.versionCode
            int r0 = com.facebook.common.build.BuildConstants.A00()
            if (r1 == r0) goto L_0x0081
            goto L_0x0066
        L_0x0041:
            if (r8 != r0) goto L_0x0064
            android.content.pm.PackageManager r1 = r7.getPackageManager()
            java.lang.String r0 = r7.getPackageName()     // Catch:{ NameNotFoundException -> 0x005e }
            android.content.pm.PackageInfo r0 = r1.getPackageInfo(r0, r2)     // Catch:{ NameNotFoundException -> 0x005e }
            long r8 = r4.lastModified()
            r4 = 1000(0x3e8, double:4.94E-321)
            long r8 = r8 / r4
            long r1 = r0.lastUpdateTime
            long r1 = r1 / r4
            int r0 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0064
            goto L_0x0081
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r5)
            throw r0
        L_0x0064:
            r0 = 0
            goto L_0x0082
        L_0x0066:
            java.util.Locale r2 = java.util.Locale.US
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            int r0 = com.facebook.common.build.BuildConstants.A00()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Android PackageManager returned version code: %d, apk version code is: %d"
            java.lang.String r0 = java.lang.String.format(r2, r0, r1)
            X.C010708t.A0K(r4, r0)
        L_0x0081:
            r0 = 1
        L_0x0082:
            if (r0 == 0) goto L_0x0089
            java.lang.String r0 = "cold_start"
            X.AnonymousClass0HH.A03(r7, r6, r0)
        L_0x0089:
            android.app.Application r0 = r3.A02
            X.AnonymousClass04h.A00(r0)
            super.A02()
            java.lang.String r1 = "MessengerApplicationImpl.onCreate"
            r0 = 196677423(0xbb90f2f, float:7.1282254E-32)
            X.C005505z.A03(r1, r0)
            com.facebook.acra.ErrorReporter r1 = com.facebook.acra.ErrorReporter.getInstance()     // Catch:{ all -> 0x040c }
            X.0ga r0 = new X.0ga     // Catch:{ all -> 0x040c }
            r0.<init>()     // Catch:{ all -> 0x040c }
            r1.setLogBridge(r0)     // Catch:{ all -> 0x040c }
            com.facebook.acra.ErrorReporter r2 = com.facebook.acra.ErrorReporter.getInstance()     // Catch:{ all -> 0x040c }
            java.lang.String r1 = "app_background_stats"
            X.0gg r0 = new X.0gg     // Catch:{ all -> 0x040c }
            r0.<init>()     // Catch:{ all -> 0x040c }
            r2.putLazyCustomData(r1, r0)     // Catch:{ all -> 0x040c }
            android.app.Application r2 = r3.A02     // Catch:{ all -> 0x040c }
            int r1 = com.facebook.redex.dynamicanalysis.DynamicAnalysis.sNumStaticallyInstrumented     // Catch:{ all -> 0x040c }
            r0 = 0
            if (r1 <= 0) goto L_0x00bb
            r0 = 1
        L_0x00bb:
            if (r0 == 0) goto L_0x00d4
            android.content.Context r0 = r2.getApplicationContext()     // Catch:{ all -> 0x040c }
            if (r0 == 0) goto L_0x00c7
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ all -> 0x040c }
        L_0x00c7:
            java.lang.String r1 = "DYNA|TraceManager"
            java.lang.String r0 = "Setting context!"
            X.C010708t.A0J(r1, r0)     // Catch:{ all -> 0x040c }
            android.content.Context r0 = X.C02470Ey.A00     // Catch:{ all -> 0x040c }
            if (r0 != 0) goto L_0x00d4
            X.C02470Ey.A00 = r2     // Catch:{ all -> 0x040c }
        L_0x00d4:
            r2 = 17
            int r1 = X.AnonymousClass1Y3.Awo     // Catch:{ all -> 0x040c }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x040c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x040c }
            X.0gb r0 = (X.C09140gb) r0     // Catch:{ all -> 0x040c }
            int r1 = X.AnonymousClass1Y3.A3l     // Catch:{ all -> 0x040c }
            X.0UN r4 = r0.A00     // Catch:{ all -> 0x040c }
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r4)     // Catch:{ all -> 0x040c }
            X.0cV r2 = (X.C07030cV) r2     // Catch:{ all -> 0x040c }
            int r1 = X.AnonymousClass1Y3.AmR     // Catch:{ all -> 0x040c }
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r1, r4)     // Catch:{ all -> 0x040c }
            X.0kL r0 = (X.C10530kL) r0     // Catch:{ all -> 0x040c }
            r2.A00 = r0     // Catch:{ all -> 0x040c }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x040c }
            X.0UN r0 = r2.A01     // Catch:{ all -> 0x040c }
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x040c }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x040c }
            r0.updateListenerMarkers()     // Catch:{ all -> 0x040c }
            int r1 = X.AnonymousClass1Y3.B9x     // Catch:{ all -> 0x040c }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x040c }
            r2 = 1
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x040c }
            X.0fv r6 = (X.C08770fv) r6     // Catch:{ all -> 0x040c }
            long r0 = r3.A00     // Catch:{ all -> 0x040c }
            X.00T r8 = r3.A06     // Catch:{ all -> 0x040c }
            X.08W r7 = r3.A05     // Catch:{ all -> 0x040c }
            r6.A01 = r0     // Catch:{ all -> 0x040c }
            com.facebook.perf.startupstatemachine.StartupStateMachine r5 = r6.A0N     // Catch:{ all -> 0x040c }
            monitor-enter(r5)     // Catch:{ all -> 0x040c }
            r4 = 3
            r5.A00 = r4     // Catch:{ all -> 0x0409 }
            monitor-exit(r5)     // Catch:{ all -> 0x040c }
            com.facebook.quicklog.QuickPerformanceLogger r9 = r6.A05     // Catch:{ all -> 0x040c }
            r10 = 5505027(0x540003, float:7.714186E-39)
            r11 = 0
            r12 = 0
            r15 = 0
            r17 = 0
            r13 = r0
            r16 = r8
            r9.markerStartWithCounterForLegacy(r10, r11, r12, r13, r15, r16, r17)     // Catch:{ all -> 0x040c }
            com.facebook.quicklog.QuickPerformanceLogger r10 = r6.A05     // Catch:{ all -> 0x040c }
            r9 = 5505203(0x5400b3, float:7.714433E-39)
            r10.markerStart(r9, r11, r0)     // Catch:{ all -> 0x040c }
            int r8 = X.AnonymousClass1Y3.Azd     // Catch:{ all -> 0x040c }
            X.0UN r5 = r6.A03     // Catch:{ all -> 0x040c }
            r4 = 4
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r4, r8, r5)     // Catch:{ all -> 0x040c }
            java.util.concurrent.ScheduledExecutorService r10 = (java.util.concurrent.ScheduledExecutorService) r10     // Catch:{ all -> 0x040c }
            X.0gE r9 = new X.0gE     // Catch:{ all -> 0x040c }
            r9.<init>(r6)     // Catch:{ all -> 0x040c }
            java.util.concurrent.TimeUnit r8 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x040c }
            r4 = 120(0x78, double:5.93E-322)
            r10.schedule(r9, r4, r8)     // Catch:{ all -> 0x040c }
            com.facebook.quicklog.QuickPerformanceLogger r8 = r6.A05     // Catch:{ all -> 0x040c }
            r5 = 5505190(0x5400a6, float:7.714414E-39)
            r8.markerStart(r5, r11, r0)     // Catch:{ all -> 0x040c }
            X.0g6 r0 = r6.A0K     // Catch:{ all -> 0x040c }
            r8 = 5505027(0x540003, float:7.714186E-39)
            r0.A01(r8)     // Catch:{ all -> 0x040c }
            X.0ko r1 = r6.A0L     // Catch:{ all -> 0x040c }
            r1.A02(r2)     // Catch:{ all -> 0x040c }
            r6.A02 = r7     // Catch:{ all -> 0x040c }
            X.0fx r1 = r6.A0H     // Catch:{ all -> 0x040c }
            X.C08790fx.A04(r1, r8)     // Catch:{ all -> 0x040c }
            X.C08770fv.A03(r6)     // Catch:{ all -> 0x040c }
            int r0 = com.facebook.acra.StartTimeBlockedRecorder.sTotalCrashesUploaded     // Catch:{ all -> 0x040c }
            if (r0 <= 0) goto L_0x0181
            com.facebook.quicklog.QuickPerformanceLogger r5 = r6.A05     // Catch:{ all -> 0x040c }
            long r0 = com.facebook.acra.StartTimeBlockedRecorder.sDurationStartupBlocked     // Catch:{ all -> 0x040c }
            java.lang.String r4 = "crash_upload_blocking_time"
            r5.markerAnnotate(r8, r4, r0)     // Catch:{ all -> 0x040c }
            com.facebook.quicklog.QuickPerformanceLogger r4 = r6.A05     // Catch:{ all -> 0x040c }
            int r1 = com.facebook.acra.StartTimeBlockedRecorder.sTotalCrashesUploaded     // Catch:{ all -> 0x040c }
            java.lang.String r0 = "crash_upload_count"
            r4.markerAnnotate(r8, r0, r1)     // Catch:{ all -> 0x040c }
        L_0x0181:
            X.0gK r0 = new X.0gK     // Catch:{ all -> 0x040c }
            r0.<init>()     // Catch:{ all -> 0x040c }
            X.C27041cY.A00 = r0     // Catch:{ all -> 0x040c }
            r4 = 11
            int r1 = X.AnonymousClass1Y3.ANx     // Catch:{ all -> 0x040c }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x040c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x040c }
            X.1cZ r0 = (X.C27051cZ) r0     // Catch:{ all -> 0x040c }
            X.C09070gU.A00 = r0     // Catch:{ all -> 0x040c }
            java.lang.String r1 = "InitThreadsQueue"
            r0 = -1372403060(0xffffffffae32ca8c, float:-4.0652412E-11)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x040c }
            X.0WE r0 = r3.A03     // Catch:{ all -> 0x0401 }
            r0.A03()     // Catch:{ all -> 0x0401 }
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            r7 = 6
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0401 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x0401 }
            r0 = 198(0xc6, float:2.77E-43)
            boolean r0 = r1.AbO(r0, r11)     // Catch:{ all -> 0x0401 }
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0401 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x0401 }
            r0 = 702(0x2be, float:9.84E-43)
            boolean r4 = r1.AbO(r0, r11)     // Catch:{ all -> 0x0401 }
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0401 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x0401 }
            r0 = 703(0x2bf, float:9.85E-43)
            boolean r5 = r1.AbO(r0, r11)     // Catch:{ all -> 0x0401 }
            if (r4 != 0) goto L_0x01d8
            if (r5 == 0) goto L_0x01ef
        L_0x01d8:
            r4 = 15
            int r1 = X.AnonymousClass1Y3.BIG     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x0401 }
            java.util.concurrent.ExecutorService r4 = (java.util.concurrent.ExecutorService) r4     // Catch:{ all -> 0x0401 }
            X.1uf r1 = new X.1uf     // Catch:{ all -> 0x0401 }
            r1.<init>(r5)     // Catch:{ all -> 0x0401 }
            r0 = -938231362(0xffffffffc813b9be, float:-151270.97)
            X.AnonymousClass07A.A04(r4, r1, r0)     // Catch:{ all -> 0x0401 }
        L_0x01ef:
            r4 = 0
            android.app.Application r1 = r3.A02     // Catch:{ all -> 0x0401 }
            boolean r0 = r1 instanceof com.facebook.messenger.app.MessengerApplication     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x01fa
            com.facebook.messenger.app.MessengerApplication r1 = (com.facebook.messenger.app.MessengerApplication) r1     // Catch:{ all -> 0x0401 }
            android.content.Intent r4 = r1.A00     // Catch:{ all -> 0x0401 }
        L_0x01fa:
            if (r4 == 0) goto L_0x021b
            java.lang.String r0 = r4.getAction()     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x021b
            r0 = 14
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ all -> 0x0401 }
            boolean r0 = r4.hasCategory(r0)     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x021b
            java.lang.String r1 = r4.getAction()     // Catch:{ all -> 0x0401 }
            java.lang.String r0 = "android.intent.action.MAIN"
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0401 }
            r11 = 1
            if (r0 != 0) goto L_0x021c
        L_0x021b:
            r11 = 0
        L_0x021c:
            if (r4 == 0) goto L_0x021f
            goto L_0x0221
        L_0x021f:
            r2 = 0
            goto L_0x0237
        L_0x0221:
            android.content.ComponentName r0 = r4.getComponent()     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x021f
            android.content.ComponentName r0 = r4.getComponent()     // Catch:{ all -> 0x0401 }
            java.lang.String r1 = r0.getClassName()     // Catch:{ all -> 0x0401 }
            java.lang.String r0 = "com.facebook.orca.auth.StartScreenActivity"
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x021f
        L_0x0237:
            r11 = r11 | r2
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0401 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x0401 }
            r0 = 782(0x30e, float:1.096E-42)
            boolean r0 = r1.AbO(r0, r15)     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x024c
            if (r11 == 0) goto L_0x02cc
        L_0x024c:
            int r1 = X.AnonymousClass1Y3.AGW     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r9 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0401 }
            X.0gV r9 = (X.C09080gV) r9     // Catch:{ all -> 0x0401 }
            int r1 = X.AnonymousClass1Y3.B1W     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r15, r1, r0)     // Catch:{ all -> 0x0401 }
            com.facebook.fbservice.ops.BlueServiceOperationFactory r4 = (com.facebook.fbservice.ops.BlueServiceOperationFactory) r4     // Catch:{ all -> 0x0401 }
            java.lang.Class r1 = r3.getClass()     // Catch:{ all -> 0x0401 }
            java.lang.String r0 = "fg_prefetch"
            com.facebook.common.callercontext.CallerContext r10 = com.facebook.common.callercontext.CallerContext.A07(r1, r0)     // Catch:{ all -> 0x0401 }
            java.lang.String r1 = "KickOffThreadPrefetcher"
            r0 = 1689021738(0x64ac6d2a, float:2.5445649E22)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x0401 }
            X.0kg r5 = new X.0kg     // Catch:{ all -> 0x03f9 }
            r5.<init>()     // Catch:{ all -> 0x03f9 }
            X.0hU r0 = X.C09510hU.DO_NOT_CHECK_SERVER     // Catch:{ all -> 0x03f9 }
            r5.A02 = r0     // Catch:{ all -> 0x03f9 }
            r0 = 20
            r5.A00 = r0     // Catch:{ all -> 0x03f9 }
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ all -> 0x03f9 }
            r5.A04 = r0     // Catch:{ all -> 0x03f9 }
            int r2 = X.AnonymousClass1Y3.BOk     // Catch:{ all -> 0x03f9 }
            X.0UN r1 = r9.A00     // Catch:{ all -> 0x03f9 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r15, r2, r1)     // Catch:{ all -> 0x03f9 }
            X.0l9 r0 = (X.C10960l9) r0     // Catch:{ all -> 0x03f9 }
            com.google.common.collect.ImmutableSet r0 = r0.A04()     // Catch:{ all -> 0x03f9 }
            r5.A06 = r0     // Catch:{ all -> 0x03f9 }
            com.facebook.messaging.service.model.FetchThreadListParams r8 = new com.facebook.messaging.service.model.FetchThreadListParams     // Catch:{ all -> 0x03f9 }
            r8.<init>(r5)     // Catch:{ all -> 0x03f9 }
            android.os.Bundle r2 = new android.os.Bundle     // Catch:{ all -> 0x03f9 }
            r2.<init>()     // Catch:{ all -> 0x03f9 }
            java.lang.String r0 = "fetchThreadListParams"
            r2.putParcelable(r0, r8)     // Catch:{ all -> 0x03f9 }
            java.lang.String r1 = "fetch_thread_list"
            r0 = 770957219(0x2df3dfa3, float:2.7725216E-11)
            X.0lL r1 = X.AnonymousClass07W.A01(r4, r1, r2, r10, r0)     // Catch:{ all -> 0x03f9 }
            r0 = 1
            r1.C6o(r0)     // Catch:{ all -> 0x03f9 }
            X.1cp r5 = r1.CHd()     // Catch:{ all -> 0x03f9 }
            X.0li r4 = new X.0li     // Catch:{ all -> 0x03f9 }
            r4.<init>(r9, r8, r10)     // Catch:{ all -> 0x03f9 }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Aoc     // Catch:{ all -> 0x03f9 }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x03f9 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x03f9 }
            java.util.concurrent.ExecutorService r0 = (java.util.concurrent.ExecutorService) r0     // Catch:{ all -> 0x03f9 }
            X.C05350Yp.A08(r5, r4, r0)     // Catch:{ all -> 0x03f9 }
            r0 = -649768564(0xffffffffd945518c, float:-3.47126451E15)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0401 }
        L_0x02cc:
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0401 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x0401 }
            r0 = 781(0x30d, float:1.094E-42)
            boolean r0 = r1.AbO(r0, r15)     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x02e0
            if (r11 == 0) goto L_0x02f8
        L_0x02e0:
            int r1 = X.AnonymousClass1Y3.AFN     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0401 }
            X.0Zh r5 = (X.C05530Zh) r5     // Catch:{ all -> 0x0401 }
            X.0hT r4 = new X.0hT     // Catch:{ all -> 0x0401 }
            X.0hU r2 = X.C09510hU.DO_NOT_CHECK_SERVER     // Catch:{ all -> 0x0401 }
            X.1a7 r1 = X.C25611a7.TOP     // Catch:{ all -> 0x0401 }
            java.lang.String r0 = "REGULAR"
            r4.<init>(r2, r1, r0)     // Catch:{ all -> 0x0401 }
            r5.A03(r4)     // Catch:{ all -> 0x0401 }
        L_0x02f8:
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x0401 }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x0401 }
            r0 = 241(0xf1, float:3.38E-43)
            boolean r0 = r1.AbO(r0, r15)     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x0321
            int r1 = X.AnonymousClass1Y3.ArG     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0401 }
            X.2bA r0 = (X.C48802bA) r0     // Catch:{ all -> 0x0401 }
            java.util.concurrent.ExecutorService r2 = r0.A04     // Catch:{ all -> 0x0401 }
            X.2bB r1 = new X.2bB     // Catch:{ all -> 0x0401 }
            r1.<init>(r0)     // Catch:{ all -> 0x0401 }
            r0 = -2059706267(0xffffffff853b6065, float:-8.8104026E-36)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ all -> 0x0401 }
        L_0x0321:
            X.0gD r2 = new X.0gD     // Catch:{ all -> 0x0401 }
            r2.<init>(r3)     // Catch:{ all -> 0x0401 }
            X.0Ut r0 = r3.A01     // Catch:{ all -> 0x0401 }
            X.0bl r1 = r0.BMm()     // Catch:{ all -> 0x0401 }
            java.lang.String r0 = "background_started"
            r1.A02(r0, r2)     // Catch:{ all -> 0x0401 }
            X.0c5 r0 = r1.A00()     // Catch:{ all -> 0x0401 }
            r0.A00()     // Catch:{ all -> 0x0401 }
            r2 = 3
            int r1 = X.AnonymousClass1Y3.ArW     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0401 }
            X.0XN r0 = (X.AnonymousClass0XN) r0     // Catch:{ all -> 0x0401 }
            boolean r0 = r0.A0I()     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x0358
            r2 = 16
            int r1 = X.AnonymousClass1Y3.A3Y     // Catch:{ all -> 0x0401 }
            X.0UN r0 = r3.A04     // Catch:{ all -> 0x0401 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0401 }
            X.1cu r0 = (X.C27261cu) r0     // Catch:{ all -> 0x0401 }
            X.C27261cu.A03(r0)     // Catch:{ all -> 0x0401 }
        L_0x0358:
            android.app.Application r0 = r3.A02     // Catch:{ all -> 0x0401 }
            X.00K r0 = X.AnonymousClass00J.A01(r0)     // Catch:{ all -> 0x0401 }
            boolean r7 = r0.A1R     // Catch:{ all -> 0x0401 }
            long r2 = r0.A0k     // Catch:{ all -> 0x0401 }
            boolean r6 = r0.A1S     // Catch:{ all -> 0x0401 }
            boolean r5 = r0.A1T     // Catch:{ all -> 0x0401 }
            java.util.concurrent.atomic.AtomicBoolean r4 = com.facebook.common.fury.Fury.A00     // Catch:{ all -> 0x0401 }
            r0 = 1
            boolean r0 = r4.compareAndSet(r15, r0)     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x03ec
            com.facebook.common.fury.Fury.A02 = r7     // Catch:{ all -> 0x0401 }
            boolean r0 = com.facebook.common.fury.Fury.A02     // Catch:{ all -> 0x0401 }
            if (r0 == 0) goto L_0x03ec
            com.facebook.common.fury.Fury.A01 = r2     // Catch:{ all -> 0x0401 }
            X.1d5 r4 = new X.1d5     // Catch:{ all -> 0x0401 }
            long r0 = com.facebook.common.fury.Fury.A01     // Catch:{ all -> 0x0401 }
            r4.<init>(r0)     // Catch:{ all -> 0x0401 }
            X.C001000r.A00(r4)     // Catch:{ all -> 0x0401 }
            if (r5 != 0) goto L_0x0385
            X.C27401d8.A03 = r12     // Catch:{ all -> 0x0401 }
        L_0x0385:
            if (r6 != 0) goto L_0x0389
            X.C27401d8.A01 = r12     // Catch:{ all -> 0x0401 }
        L_0x0389:
            X.04r r3 = X.AnonymousClass04r.A01     // Catch:{ all -> 0x0401 }
            X.1XU[] r0 = new X.AnonymousClass1XU[]{r4, r3}     // Catch:{ all -> 0x0401 }
            X.1d6 r2 = new X.1d6     // Catch:{ all -> 0x0401 }
            r2.<init>(r0)     // Catch:{ all -> 0x0401 }
            X.1XU r0 = X.C25231Yv.A00     // Catch:{ all -> 0x0401 }
            if (r0 != 0) goto L_0x03de
            X.C25231Yv.A00 = r2     // Catch:{ all -> 0x0401 }
        L_0x039a:
            X.1XT[] r0 = new X.AnonymousClass1XT[]{r4, r3}     // Catch:{ all -> 0x0401 }
            X.1d7 r2 = new X.1d7     // Catch:{ all -> 0x0401 }
            r2.<init>(r0)     // Catch:{ all -> 0x0401 }
            X.1XT r0 = X.C27401d8.A02     // Catch:{ all -> 0x0401 }
            if (r0 != 0) goto L_0x03d0
            X.C27401d8.A02 = r2     // Catch:{ all -> 0x0401 }
        L_0x03a9:
            X.1dA r0 = new X.1dA     // Catch:{ all -> 0x0401 }
            r0.<init>()     // Catch:{ all -> 0x0401 }
            X.C21851Iy.A00 = r0     // Catch:{ all -> 0x0401 }
            X.1dB r2 = new X.1dB     // Catch:{ all -> 0x0401 }
            java.lang.String r1 = "Litho_"
            r0 = 1
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0401 }
            X.C27461dE.A00 = r2     // Catch:{ all -> 0x0401 }
            X.1dF r0 = new X.1dF     // Catch:{ all -> 0x0401 }
            r0.<init>()     // Catch:{ all -> 0x0401 }
            X.C07840eF.A03 = r0     // Catch:{ all -> 0x0401 }
            X.1dG r0 = new X.1dG     // Catch:{ all -> 0x0401 }
            r0.<init>()     // Catch:{ all -> 0x0401 }
            X.C27491dH.A00 = r0     // Catch:{ all -> 0x0401 }
            X.1dI r0 = new X.1dI     // Catch:{ all -> 0x0401 }
            r0.<init>()     // Catch:{ all -> 0x0401 }
            X.AnonymousClass1QN.A06 = r0     // Catch:{ all -> 0x0401 }
            goto L_0x03ec
        L_0x03d0:
            X.1d7 r1 = new X.1d7     // Catch:{ all -> 0x0401 }
            X.1XT r0 = X.C27401d8.A02     // Catch:{ all -> 0x0401 }
            X.1XT[] r0 = new X.AnonymousClass1XT[]{r0, r2}     // Catch:{ all -> 0x0401 }
            r1.<init>(r0)     // Catch:{ all -> 0x0401 }
            X.C27401d8.A02 = r1     // Catch:{ all -> 0x0401 }
            goto L_0x03a9
        L_0x03de:
            X.1d6 r1 = new X.1d6     // Catch:{ all -> 0x0401 }
            X.1XU r0 = X.C25231Yv.A00     // Catch:{ all -> 0x0401 }
            X.1XU[] r0 = new X.AnonymousClass1XU[]{r0, r2}     // Catch:{ all -> 0x0401 }
            r1.<init>(r0)     // Catch:{ all -> 0x0401 }
            X.C25231Yv.A00 = r1     // Catch:{ all -> 0x0401 }
            goto L_0x039a
        L_0x03ec:
            r0 = -762881048(0xffffffffd2875be8, float:-2.90681258E11)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x040c }
            r0 = 1632020852(0x6146a974, float:2.290416E20)
            X.C005505z.A00(r0)
            return
        L_0x03f9:
            r1 = move-exception
            r0 = 1937504663(0x737bf997, float:1.9963513E31)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0401 }
            throw r1     // Catch:{ all -> 0x0401 }
        L_0x0401:
            r1 = move-exception
            r0 = 1164087470(0x456290ae, float:3625.0425)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x040c }
            throw r1     // Catch:{ all -> 0x040c }
        L_0x0409:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x040c }
            throw r0     // Catch:{ all -> 0x040c }
        L_0x040c:
            r1 = move-exception
            r0 = -923420670(0xffffffffc8f5b802, float:-503232.06)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messenger.app.MessengerApplicationImpl.A02():void");
    }

    public void A03(int i) {
        C50362dr r4 = new C50362dr(this, i);
        if (((C08790fx) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AsC, this.A04)).A09()) {
            AnonymousClass108 r3 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, this.A04);
            r3.A02(r4);
            r3.A02 = "TrimMemory";
            r3.A03("ForUiThread");
            r3.A04 = String.valueOf(i);
            ((AnonymousClass0g4) AnonymousClass1XX.A02(13, AnonymousClass1Y3.ASI, this.A04)).A04(r3.A01(), "KeepExisting");
            return;
        }
        r4.run();
    }

    public MessengerApplicationImpl(Application application, C001300x r2, long j, AnonymousClass08W r5, AnonymousClass00T r6) {
        super(application, r2);
        this.A00 = j;
        this.A05 = r5;
        this.A06 = r6;
    }

    public static void A00(MessengerApplicationImpl messengerApplicationImpl, int i) {
        super.A03(i);
        if (messengerApplicationImpl.A02.A07() >= 1000 && ((AnonymousClass0WP) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A8a, messengerApplicationImpl.A04)).BDj() && AnonymousClass00M.A00().A05()) {
            if (i > 5) {
                AnonymousClass1MZ r2 = (AnonymousClass1MZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AF0, messengerApplicationImpl.A04);
                C179688Sa r1 = new C179688Sa();
                r2.A07.C1M(r1);
                r2.A08.C1M(r1);
            }
            if ((i > 20 || i == 15) && ((AnonymousClass0XN) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ArW, messengerApplicationImpl.A04)).A0I()) {
                ((C91694Zm) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BSF, messengerApplicationImpl.A04)).A02();
            }
            if (i == 10 || i == 15 || i >= 80) {
                ((AnonymousClass2AI) AnonymousClass1XX.A02(8, AnonymousClass1Y3.B4N, messengerApplicationImpl.A04)).A00.clear();
            }
        }
    }
}
