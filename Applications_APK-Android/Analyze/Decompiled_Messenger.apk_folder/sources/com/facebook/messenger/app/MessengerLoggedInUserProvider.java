package com.facebook.messenger.app;

import X.AnonymousClass0TA;
import X.AnonymousClass0VG;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.C04310Tq;

public class MessengerLoggedInUserProvider extends AnonymousClass0TA {
    public C04310Tq A00;

    public void A0G() {
        super.A0G();
        this.A00 = AnonymousClass0VG.A00(AnonymousClass1Y3.AUM, AnonymousClass1XX.get(getContext()));
    }
}
