package com.facebook.messenger.app;

import X.AnonymousClass001;
import X.AnonymousClass005;
import X.AnonymousClass00M;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass07c;
import X.AnonymousClass08W;
import X.C000000c;
import X.C001300x;
import X.C006006f;
import X.C009508d;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.facebook.breakpad.internal.BreakpadCompatible;
import com.facebook.messenger.splashscreen.MessengerSplashScreenActivity;
import com.facebook.sosource.bsod.BSODActivity;
import java.io.File;

public class MessengerApplication extends AnonymousClass001 implements AnonymousClass005, BreakpadCompatible {
    private static final String[] A05 = {"videoplayer"};
    public Intent A00;
    private long A01;
    private AnonymousClass08W A02;
    private AnonymousClass00T A03;
    private C001300x A04;

    public Context getApplicationContext() {
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0070, code lost:
        if (r0 != false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0082, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0083, code lost:
        android.util.Log.w("MessengerApplication", "error enabling jvm stream", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0164, code lost:
        if (r10.startsWith("/odm") != false) goto L_0x0166;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doBreakpadInitialization_INTERNAL() {
        /*
            r14 = this;
            r7 = 0
            r6 = 1536000(0x177000, float:2.152394E-39)
            java.lang.Class<com.facebook.breakpad.BreakpadManager> r5 = com.facebook.breakpad.BreakpadManager.class
            monitor-enter(r5)
            r4 = 0
            java.lang.String r0 = com.facebook.breakpad.BreakpadManager.mNativeLibraryName     // Catch:{ all -> 0x01f0 }
            if (r0 != 0) goto L_0x0014
            java.lang.String r0 = "breakpad"
            X.AnonymousClass01q.A08(r0)     // Catch:{ all -> 0x01f0 }
            com.facebook.breakpad.BreakpadManager.mNativeLibraryName = r0     // Catch:{ all -> 0x01f0 }
        L_0x0014:
            java.io.File r0 = com.facebook.breakpad.BreakpadManager.mCrashDirectory     // Catch:{ all -> 0x01f0 }
            if (r0 != 0) goto L_0x0054
            java.lang.String r0 = "minidumps"
            java.io.File r3 = r14.getDir(r0, r4)     // Catch:{ all -> 0x01f0 }
            if (r3 == 0) goto L_0x004c
            java.lang.String r2 = r3.getAbsolutePath()     // Catch:{ all -> 0x01f0 }
            java.lang.String r1 = ""
            com.facebook.breakpad.BreakpadManager.install(r2, r1, r1, r6)     // Catch:{ all -> 0x01f0 }
            com.facebook.breakpad.BreakpadManager.mCrashDirectory = r3     // Catch:{ all -> 0x01f0 }
            long r0 = com.facebook.breakpad.BreakpadManager.getMinidumpFlags()     // Catch:{ all -> 0x01f0 }
            long r7 = r7 | r0
            r0 = 2
            long r7 = r7 | r0
            r0 = 4
            long r7 = r7 | r0
            com.facebook.breakpad.BreakpadManager.setMinidumpFlags(r7)     // Catch:{ all -> 0x01f0 }
            int r1 = com.facebook.common.build.BuildConstants.A00()     // Catch:{ all -> 0x01f0 }
            java.lang.String r0 = "253.0.0.17.117"
            com.facebook.breakpad.BreakpadManager.setVersionInfo(r1, r0)     // Catch:{ all -> 0x01f0 }
            java.lang.String r2 = "Fingerprint"
            java.lang.String r1 = android.os.Build.FINGERPRINT     // Catch:{ all -> 0x01f0 }
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x01f0 }
            com.facebook.breakpad.BreakpadManager.setCustomData(r2, r1, r0)     // Catch:{ all -> 0x01f0 }
            goto L_0x0054
        L_0x004c:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x01f0 }
            java.lang.String r0 = "Breakpad init failed to create crash directory: minidumps"
            r1.<init>(r0)     // Catch:{ all -> 0x01f0 }
            throw r1     // Catch:{ all -> 0x01f0 }
        L_0x0054:
            monitor-exit(r5)
            r3 = 1
            java.lang.Class<com.facebook.breakpad.BreakpadManager> r2 = com.facebook.breakpad.BreakpadManager.class
            monitor-enter(r2)     // Catch:{ Exception | UnsatisfiedLinkError -> 0x0082 }
            java.lang.String r0 = "java.vm.version"
            java.lang.String r1 = java.lang.System.getProperty(r0)     // Catch:{ all -> 0x007f }
            if (r1 == 0) goto L_0x0072
            java.lang.String r0 = "1."
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x007f }
            if (r0 != 0) goto L_0x0072
            java.lang.String r0 = "0."
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x007f }
            r1 = 1
            if (r0 == 0) goto L_0x0073
        L_0x0072:
            r1 = 0
        L_0x0073:
            if (r1 == 0) goto L_0x007a
            java.lang.String r0 = "breakpad_cpp_helper"
            X.AnonymousClass01q.A08(r0)     // Catch:{ all -> 0x007f }
        L_0x007a:
            com.facebook.breakpad.BreakpadManager.nativeSetJvmStreamEnabled(r1, r3)     // Catch:{ all -> 0x007f }
            monitor-exit(r2)     // Catch:{ Exception | UnsatisfiedLinkError -> 0x0082 }
            goto L_0x008a
        L_0x007f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ Exception | UnsatisfiedLinkError -> 0x0082 }
            throw r0     // Catch:{ Exception | UnsatisfiedLinkError -> 0x0082 }
        L_0x0082:
            r2 = move-exception
            java.lang.String r1 = "MessengerApplication"
            java.lang.String r0 = "error enabling jvm stream"
            android.util.Log.w(r1, r0, r2)
        L_0x008a:
            boolean r0 = com.facebook.breakpad.BreakpadManager.isActive()
            if (r0 == 0) goto L_0x01ef
            java.lang.String r0 = "breakpad_libunwindstack_enabled"
            boolean r0 = X.AnonymousClass08X.A07(r14, r0)
            if (r0 == 0) goto L_0x01ca
            long r12 = com.facebook.breakpad.BreakpadManager.getMinidumpFlags()
            r0 = 2048(0x800, double:1.0118E-320)
            long r12 = r12 | r0
            java.lang.String r0 = "libunwindstack_binary.so"
            java.lang.String r3 = X.AnonymousClass01q.A04(r0)     // Catch:{ IOException -> 0x01bf }
            if (r3 != 0) goto L_0x00b0
            java.lang.String r1 = "UnwindstackStreamManager"
            java.lang.String r0 = "Unable to find libunwindstack_binary.so"
            X.C010708t.A0I(r1, r0)     // Catch:{ IOException -> 0x01bf }
            goto L_0x01c7
        L_0x00b0:
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ IOException -> 0x01bf }
            r9.<init>()     // Catch:{ IOException -> 0x01bf }
            r9.add(r0)     // Catch:{ IOException -> 0x01bf }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ IOException -> 0x01bf }
            r4.<init>()     // Catch:{ IOException -> 0x01bf }
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x01bf }
            r0.<init>(r3)     // Catch:{ IOException -> 0x01bf }
            java.io.File r0 = r0.getParentFile()     // Catch:{ IOException -> 0x01bf }
            java.lang.String r0 = r0.getCanonicalPath()     // Catch:{ IOException -> 0x01bf }
            r4.add(r0)     // Catch:{ IOException -> 0x01bf }
            r8 = 0
            r7 = 0
        L_0x00cf:
            int r0 = r9.size()     // Catch:{ IOException -> 0x01bf }
            if (r7 >= r0) goto L_0x0194
            java.lang.Object r5 = r9.get(r7)     // Catch:{ IOException -> 0x01bf }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x01bf }
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = X.AnonymousClass01q.A06     // Catch:{ IOException -> 0x01bf }
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()     // Catch:{ IOException -> 0x01bf }
            r0.lock()     // Catch:{ IOException -> 0x01bf }
            X.025[] r0 = X.AnonymousClass01q.A04     // Catch:{ all -> 0x0189 }
            r6 = 0
            if (r0 == 0) goto L_0x00fa
            r2 = 0
        L_0x00ea:
            if (r6 != 0) goto L_0x00fa
            X.025[] r1 = X.AnonymousClass01q.A04     // Catch:{ all -> 0x0189 }
            int r0 = r1.length     // Catch:{ all -> 0x0189 }
            if (r2 >= r0) goto L_0x00fa
            r0 = r1[r2]     // Catch:{ all -> 0x0189 }
            java.lang.String[] r6 = r0.A08(r5)     // Catch:{ all -> 0x0189 }
            int r2 = r2 + 1
            goto L_0x00ea
        L_0x00fa:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = X.AnonymousClass01q.A06     // Catch:{ IOException -> 0x01bf }
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()     // Catch:{ IOException -> 0x01bf }
            r0.unlock()     // Catch:{ IOException -> 0x01bf }
            if (r6 != 0) goto L_0x0117
            java.lang.String r2 = "UnwindstackStreamManager"
            java.lang.String r1 = "unable to find dependencies for "
            java.lang.Object r0 = r9.get(r7)     // Catch:{ IOException -> 0x01bf }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x01bf }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ IOException -> 0x01bf }
            X.C010708t.A0J(r2, r0)     // Catch:{ IOException -> 0x01bf }
            goto L_0x0185
        L_0x0117:
            int r5 = r6.length     // Catch:{ IOException -> 0x01bf }
            r2 = 0
        L_0x0119:
            if (r2 >= r5) goto L_0x0185
            r11 = r6[r2]     // Catch:{ IOException -> 0x01bf }
            java.lang.String r10 = X.AnonymousClass01q.A04(r11)     // Catch:{ IOException -> 0x01bf }
            if (r10 != 0) goto L_0x012f
            java.lang.String r1 = "UnwindstackStreamManager"
            java.lang.String r0 = "unable to find path for "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r11)     // Catch:{ IOException -> 0x01bf }
            X.C010708t.A0J(r1, r0)     // Catch:{ IOException -> 0x01bf }
            goto L_0x0182
        L_0x012f:
            boolean r0 = r9.contains(r11)     // Catch:{ IOException -> 0x01bf }
            if (r0 != 0) goto L_0x0182
            java.lang.String r0 = "/system"
            boolean r0 = r10.startsWith(r0)     // Catch:{ IOException -> 0x01bf }
            if (r0 != 0) goto L_0x0166
            java.lang.String r0 = "/lib"
            boolean r0 = r10.startsWith(r0)     // Catch:{ IOException -> 0x01bf }
            if (r0 != 0) goto L_0x0166
            java.lang.String r0 = "/lib64"
            boolean r0 = r10.startsWith(r0)     // Catch:{ IOException -> 0x01bf }
            if (r0 != 0) goto L_0x0166
            java.lang.String r0 = "/vendor"
            boolean r0 = r10.startsWith(r0)     // Catch:{ IOException -> 0x01bf }
            if (r0 != 0) goto L_0x0166
            java.lang.String r0 = "/apex"
            boolean r0 = r10.startsWith(r0)     // Catch:{ IOException -> 0x01bf }
            if (r0 != 0) goto L_0x0166
            java.lang.String r0 = "/odm"
            boolean r1 = r10.startsWith(r0)     // Catch:{ IOException -> 0x01bf }
            r0 = 0
            if (r1 == 0) goto L_0x0167
        L_0x0166:
            r0 = 1
        L_0x0167:
            if (r0 != 0) goto L_0x0182
            r9.add(r11)     // Catch:{ IOException -> 0x01bf }
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x01bf }
            r0.<init>(r10)     // Catch:{ IOException -> 0x01bf }
            java.io.File r0 = r0.getParentFile()     // Catch:{ IOException -> 0x01bf }
            java.lang.String r1 = r0.getCanonicalPath()     // Catch:{ IOException -> 0x01bf }
            boolean r0 = r4.contains(r1)     // Catch:{ IOException -> 0x01bf }
            if (r0 != 0) goto L_0x0182
            r4.add(r1)     // Catch:{ IOException -> 0x01bf }
        L_0x0182:
            int r2 = r2 + 1
            goto L_0x0119
        L_0x0185:
            int r7 = r7 + 1
            goto L_0x00cf
        L_0x0189:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = X.AnonymousClass01q.A06     // Catch:{ IOException -> 0x01bf }
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()     // Catch:{ IOException -> 0x01bf }
            r0.unlock()     // Catch:{ IOException -> 0x01bf }
            throw r1     // Catch:{ IOException -> 0x01bf }
        L_0x0194:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01bf }
            java.lang.Object r0 = r4.get(r8)     // Catch:{ IOException -> 0x01bf }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x01bf }
            r2.<init>(r0)     // Catch:{ IOException -> 0x01bf }
            r1 = 1
        L_0x01a0:
            int r0 = r4.size()     // Catch:{ IOException -> 0x01bf }
            if (r1 >= r0) goto L_0x01b7
            java.lang.String r0 = ":"
            r2.append(r0)     // Catch:{ IOException -> 0x01bf }
            java.lang.Object r0 = r4.get(r1)     // Catch:{ IOException -> 0x01bf }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x01bf }
            r2.append(r0)     // Catch:{ IOException -> 0x01bf }
            int r1 = r1 + 1
            goto L_0x01a0
        L_0x01b7:
            java.lang.String r0 = r2.toString()     // Catch:{ IOException -> 0x01bf }
            com.facebook.breakpad.UnwindstackStreamManager.register(r3, r0)     // Catch:{ IOException -> 0x01bf }
            goto L_0x01c7
        L_0x01bf:
            r2 = move-exception
            java.lang.String r1 = "UnwindstackStreamManager"
            java.lang.String r0 = "Error registering unwindstack stream"
            X.C010708t.A0L(r1, r0, r2)
        L_0x01c7:
            com.facebook.breakpad.BreakpadManager.setMinidumpFlags(r12)
        L_0x01ca:
            X.02L r0 = X.AnonymousClass02L.A05
            java.lang.Boolean r0 = r0.A01
            boolean r0 = r0.booleanValue()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r3 = 0
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            java.lang.String r1 = "APP_STARTED_IN_BACKGROUND"
            java.lang.String r0 = "%b"
            com.facebook.breakpad.BreakpadManager.setCustomData(r1, r0, r2)
            com.facebook.acra.config.AcraReportingConfig r0 = com.facebook.acra.ACRA.mConfig
            java.lang.String r2 = r0.getSessionId()
            java.lang.Object[] r1 = new java.lang.Object[r3]
            java.lang.String r0 = "session_id"
            com.facebook.breakpad.BreakpadManager.setCustomData(r0, r2, r1)
        L_0x01ef:
            return
        L_0x01f0:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messenger.app.MessengerApplication.doBreakpadInitialization_INTERNAL():void");
    }

    public File getDir(String str, int i) {
        if (C009508d.A00 && "webview".equals(str)) {
            str = "browser_proc_webview";
        }
        return super.getDir(str, i);
    }

    /* JADX WARN: Type inference failed for: r1v78, types: [java.lang.Throwable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x034a, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x034b, code lost:
        android.util.Log.e("MessengerApplication", "Unable to install logcat interceptor", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0238, code lost:
        if (io.card.payment.BuildConfig.FLAVOR.equals(r3) != false) goto L_0x023a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003c, code lost:
        if (r3.equals(io.card.payment.BuildConfig.FLAVOR) != false) goto L_0x003e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.base.app.ApplicationLike A05() {
        /*
            r40 = this;
            boolean r0 = X.AnonymousClass00M.A02()
            if (r0 != 0) goto L_0x02f3
            r0 = r40
            java.lang.String r1 = "instacrash_threshold"
            r13 = 2
            int r8 = X.AnonymousClass08X.A00(r0, r1, r13)
            java.lang.String r2 = "instacrash_interval"
            r1 = 45000(0xafc8, float:6.3058E-41)
            int r19 = X.AnonymousClass08X.A00(r0, r2, r1)
            X.00x r5 = X.C009308a.A00
            X.00x r4 = X.C009308a.A01
            X.00x r3 = X.C009308a.A02
            java.lang.String r2 = "prod"
            java.lang.String r1 = "inhouse"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0048
            r5 = r4
        L_0x0029:
            r0.A04 = r5
            X.00M r1 = X.AnonymousClass00M.A00()
            java.lang.String r3 = r1.A04()
            if (r3 == 0) goto L_0x003e
            java.lang.String r1 = ""
            boolean r1 = r3.equals(r1)
            r5 = 0
            if (r1 == 0) goto L_0x003f
        L_0x003e:
            r5 = 2
        L_0x003f:
            X.011 r4 = new X.011
            r14 = 0
            r1 = r19
            r4.<init>(r8, r14, r14, r1)
            goto L_0x0052
        L_0x0048:
            java.lang.String r1 = "debug"
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x0029
            r5 = r3
            goto L_0x0029
        L_0x0052:
            java.lang.Class<X.012> r1 = X.AnonymousClass012.class
            java.lang.String r1 = r1.getName()     // Catch:{ IOException -> 0x02df }
            X.AnonymousClass014.A0D(r0, r5, r1, r4)     // Catch:{ IOException -> 0x02df }
            X.02L r2 = X.AnonymousClass02L.A05
            java.lang.Boolean r1 = r2.A01
            if (r1 != 0) goto L_0x02d7
            boolean r25 = X.AnonymousClass02L.A03(r2)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r25)
            r2.A01 = r1
            X.00M r1 = X.AnonymousClass00M.A00()
            long r17 = android.os.SystemClock.uptimeMillis()
            X.01T r34 = X.AnonymousClass01D.A00
            r26 = r17
            X.01F r7 = X.AnonymousClass01F.A00()
            java.lang.String r6 = r1.A01
            if (r6 == 0) goto L_0x00fb
        L_0x007f:
            if (r40 == 0) goto L_0x02cf
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            X.01I r1 = new X.01I
            r1.<init>()
            r5.add(r1)
            X.01K r1 = new X.01K
            r1.<init>()
            r5.add(r1)
            X.01L r1 = new X.01L
            r1.<init>()
            r5.add(r1)
            X.01N r1 = new X.01N
            r1.<init>()
            r5.add(r1)
            X.01O r1 = new X.01O
            r1.<init>()
            r5.add(r1)
            boolean r1 = r7.A0T(r0)
            if (r1 == 0) goto L_0x00bc
            X.0Mk r1 = new X.0Mk
            r1.<init>(r0)
            r5.add(r1)
        L_0x00bc:
            boolean r1 = r7.A0O(r0)
            if (r1 == 0) goto L_0x00ca
            X.0N7 r1 = new X.0N7
            r1.<init>()
            r5.add(r1)
        L_0x00ca:
            boolean r1 = r7.A0S(r0)
            if (r1 == 0) goto L_0x00d8
            X.0Mv r1 = new X.0Mv
            r1.<init>()
            r5.add(r1)
        L_0x00d8:
            boolean r1 = r7.A0K(r0)
            if (r1 == 0) goto L_0x00e6
            X.0N2 r1 = new X.0N2
            r1.<init>()
            r5.add(r1)
        L_0x00e6:
            java.util.List r1 = r7.A09()
            r5.addAll(r1)
            java.lang.String r1 = "app_state_log_write_free_internal_disk_space"
            boolean r37 = X.AnonymousClass08X.A07(r0, r1)
            r39 = 0
            r28 = r0
            java.lang.Object r2 = X.AnonymousClass01P.A0Z
            monitor-enter(r2)
            goto L_0x00fe
        L_0x00fb:
            java.lang.String r6 = "unknown"
            goto L_0x007f
        L_0x00fe:
            X.01P r1 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x02cb }
            if (r1 != 0) goto L_0x02c3
            monitor-exit(r2)     // Catch:{ all -> 0x02cb }
            java.lang.String r1 = "activity"
            java.lang.Object r4 = r0.getSystemService(r1)
            android.app.ActivityManager r4 = (android.app.ActivityManager) r4
            java.security.SecureRandom r2 = new java.security.SecureRandom
            r2.<init>()
            r11 = -1
            java.lang.String r1 = com.facebook.acra.criticaldata.CriticalAppData.getUserId(r0)     // Catch:{ NumberFormatException -> 0x011c }
            if (r1 == 0) goto L_0x011c
            long r11 = java.lang.Long.parseLong(r1)     // Catch:{ NumberFormatException -> 0x011c }
        L_0x011c:
            java.util.UUID r16 = new java.util.UUID
            long r9 = r2.nextLong()
            long r9 = r9 ^ r17
            long r1 = r2.nextLong()
            long r11 = r11 ^ r1
            r20 = r16
            r21 = r9
            r23 = r11
            r20.<init>(r21, r23)
            java.lang.String r1 = "state_logs"
            java.io.File r12 = r0.getDir(r1, r14)
            int r21 = android.os.Process.myPid()
            r2 = 58
            r1 = 95
            java.lang.String r11 = r6.replace(r2, r1)
            java.lang.String r9 = "_"
            java.lang.String r2 = r16.toString()
            java.lang.Integer r1 = X.AnonymousClass01V.A00
            java.lang.String r1 = X.AnonymousClass01V.A01(r1)
            java.lang.String r1 = X.AnonymousClass08S.A0S(r11, r9, r2, r1)
            java.io.File r15 = new java.io.File
            r15.<init>(r12, r1)
            android.content.pm.PackageManager r2 = r28.getPackageManager()
            android.content.pm.ApplicationInfo r1 = r28.getApplicationInfo()
            X.01W r9 = new X.01W
            r9.<init>(r2, r1)
            java.lang.String r1 = r28.getPackageName()
            android.content.pm.PackageInfo r9 = r9.A04(r1, r14)
            if (r9 == 0) goto L_0x01bb
            java.lang.String r12 = r9.versionName
            int r11 = r9.versionCode
            long r9 = r9.lastUpdateTime
        L_0x0176:
            if (r2 == 0) goto L_0x017e
            java.lang.String r24 = r2.getInstallerPackageName(r1)
            if (r24 != 0) goto L_0x0180
        L_0x017e:
            java.lang.String r24 = ""
        L_0x0180:
            X.01P r1 = new X.01P
            r2 = r16
            java.lang.String r35 = r2.toString()
            r33 = r0
            r28 = r9
            r30 = r7
            r31 = r15
            r32 = r4
            r36 = r6
            r38 = r5
            r20 = r1
            r22 = r12
            r23 = r11
            r20.<init>(r21, r22, r23, r24, r25, r26, r28, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39)
            X.021 r2 = new X.021
            r2.<init>(r1)
            r0.registerActivityLifecycleCallbacks(r2)
            X.01m r2 = X.C002201m.A00
            if (r2 != 0) goto L_0x02bb
            X.01m r4 = new X.01m
            r4.<init>()
            X.C002201m.A00 = r4
            r2 = 100
            X.AnonymousClass016.A02(r4, r2)
            java.lang.Object r2 = X.AnonymousClass01P.A0Z
            monitor-enter(r2)
            goto L_0x01c8
        L_0x01bb:
            java.lang.String r10 = "AppStateLoggerCore"
            java.lang.String r9 = "Could not find package info"
            X.C010708t.A0J(r10, r9)
            java.lang.String r12 = "UNKNOWN"
            r9 = 0
            r11 = -1
            goto L_0x0176
        L_0x01c8:
            X.AnonymousClass01P.A0Y = r1     // Catch:{ all -> 0x02b7 }
            monitor-exit(r2)     // Catch:{ all -> 0x02b7 }
            r0.A08(r13)
            java.lang.String r1 = "https://b-www.facebook.com/mobile/orca_android_crash_logs/"
            X.02M r2 = new X.02M
            r2.<init>(r8, r1, r0)
            long r4 = r0.A01
            X.06H r1 = new X.06H
            r1.<init>()
            com.facebook.acra.ErrorReporter r4 = com.facebook.acra.ACRA.init(r2, r4, r1)
            X.00x r1 = r0.A04
            java.lang.String r2 = r1.A06
            java.lang.String r1 = "app"
            com.facebook.acra.ErrorReporter.putCustomData(r1, r2)
            X.00x r1 = r0.A04
            java.lang.String r2 = r1.A04
            java.lang.String r1 = "fb_app_id"
            com.facebook.acra.ErrorReporter.putCustomData(r1, r2)
            X.02S r2 = new X.02S
            r2.<init>()
            java.lang.String r1 = "app_backgrounded"
            r4.putLazyCustomData(r1, r2)
            X.08u r2 = new X.08u
            r2.<init>()
            java.lang.String r1 = "asl_app_in_foreground"
            r4.putLazyCustomData(r1, r2)
            X.08v r2 = new X.08v
            r2.<init>()
            java.lang.String r1 = "asl_app_in_foreground_v2"
            r4.putLazyCustomData(r1, r2)
            X.03I r2 = new X.03I
            r2.<init>()
            java.lang.String r1 = "nav_module"
            r4.putLazyCustomData(r1, r2)
            X.03J r2 = new X.03J
            r2.<init>()
            java.lang.String r1 = "granular_exposures"
            r4.putLazyCustomData(r1, r2)
            X.08w r2 = new X.08w
            r2.<init>()
            java.lang.String r1 = "chat_head_status"
            r4.putLazyCustomData(r1, r2)
            java.lang.String r10 = ""
            r9 = 1
            if (r3 == 0) goto L_0x023a
            boolean r1 = r10.equals(r3)
            r4 = 0
            if (r1 == 0) goto L_0x023b
        L_0x023a:
            r4 = 1
        L_0x023b:
            if (r4 != 0) goto L_0x0246
            X.0Km r1 = X.C03210Km.A05
            r1.A00 = r0
            java.lang.Thread r1 = r1.A03
            r1.start()
        L_0x0246:
            com.facebook.common.dextricks.ClassFailureStapler.tryInstall()
            X.00M r1 = X.AnonymousClass00M.A00()
            java.lang.String r2 = r1.toString()
            java.lang.String r1 = "classtracinglogger_enable_"
            java.lang.String r2 = X.AnonymousClass08S.A0J(r1, r2)
            boolean r1 = X.AnonymousClass08X.A08(r0, r2, r14)
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sLoggerEnabled = r1
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.initialize()
            java.lang.String r8 = "MessengerApplication"
            if (r3 == 0) goto L_0x02e6
            if (r4 != 0) goto L_0x02e6
            java.lang.String[] r7 = com.facebook.messenger.app.MessengerApplication.A05
            int r6 = r7.length
            r5 = 0
        L_0x026a:
            if (r5 >= r6) goto L_0x02e6
            r2 = r7[r5]
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r1 = r3.toLowerCase(r1)
            boolean r1 = r1.contains(r2)
            if (r1 == 0) goto L_0x02b4
            X.00x r1 = r0.A04     // Catch:{ all -> 0x02ae }
            X.010 r1 = r1.A00     // Catch:{ all -> 0x02ae }
            java.lang.String r4 = r1.A00()     // Catch:{ all -> 0x02ae }
            android.util.SparseArray r1 = new android.util.SparseArray     // Catch:{ all -> 0x02ae }
            r1.<init>(r9)     // Catch:{ all -> 0x02ae }
            int r12 = X.C04110Rx.A00     // Catch:{ all -> 0x02ae }
            X.0Rx r11 = new X.0Rx     // Catch:{ all -> 0x02ae }
            r11.<init>()     // Catch:{ all -> 0x02ae }
            r1.put(r12, r11)     // Catch:{ all -> 0x02ae }
            r21 = 0
            X.00o[] r24 = X.AnonymousClass05A.A00(r0)     // Catch:{ all -> 0x02ae }
            r26 = 0
            r27 = 0
            r20 = r0
            r23 = 0
            r22 = r3
            r25 = r1
            X.AnonymousClass05e.A00(r20, r21, r22, r23, r24, r25, r26, r27)     // Catch:{ all -> 0x02ae }
            com.facebook.profilo.logger.api.ProfiloLogger.sHasProfilo = r9     // Catch:{ all -> 0x02ae }
            X.C004004z.A00 = r9     // Catch:{ all -> 0x02ae }
            X.AnonymousClass073.A00(r0, r14, r4)     // Catch:{ all -> 0x02ae }
            goto L_0x02b4
        L_0x02ae:
            r2 = move-exception
            java.lang.String r1 = "It was not possible to init TraceOrchestrator"
            android.util.Log.e(r8, r1, r2)
        L_0x02b4:
            int r5 = r5 + 1
            goto L_0x026a
        L_0x02b7:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x02b7 }
            goto L_0x0549
        L_0x02bb:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "AppStateLoggerExceptionHandler can only be initialized once"
            r1.<init>(r0)
            throw r1
        L_0x02c3:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x02cb }
            java.lang.String r0 = "An application has already been registered with AppStateLogger"
            r1.<init>(r0)     // Catch:{ all -> 0x02cb }
            throw r1     // Catch:{ all -> 0x02cb }
        L_0x02cb:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x02cb }
            goto L_0x0549
        L_0x02cf:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Must provide a valid context"
            r1.<init>(r0)
            throw r1
        L_0x02d7:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "checkIfStartupWasInTheBackground has already been called!"
            r1.<init>(r0)
            throw r1
        L_0x02df:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        L_0x02e6:
            com.facebook.common.dextricks.verifier.Verifier.disableRuntimeVerification(r0)
            if (r3 == 0) goto L_0x02f9
            java.lang.String r1 = "bsod"
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x02f9
        L_0x02f3:
            com.facebook.base.app.ApplicationLike r0 = new com.facebook.base.app.ApplicationLike
            r0.<init>()
            return r0
        L_0x02f9:
            r0.doBreakpadInitialization_INTERNAL()
            java.lang.String r1 = "acraconfig_logcat_native_crash_enabled_enabled"
            boolean r1 = X.AnonymousClass08X.A07(r0, r1)
            if (r1 == 0) goto L_0x0350
            java.lang.String r2 = "acraconfig_logcat_interceptor_enabled"
            boolean r1 = X.AnonymousClass08X.A08(r0, r2, r14)
            if (r1 == 0) goto L_0x0350
            java.lang.String r2 = "acraconfig_logcat_interceptor_ring_size"
            r1 = 204800(0x32000, float:2.86986E-40)
            int r6 = X.AnonymousClass08X.A00(r0, r2, r1)     // Catch:{ all -> 0x034a }
            java.lang.Class<com.facebook.logcatinterceptor.LogcatInterceptor> r4 = com.facebook.logcatinterceptor.LogcatInterceptor.class
            monitor-enter(r4)     // Catch:{ all -> 0x034a }
            boolean r1 = com.facebook.logcatinterceptor.LogcatInterceptor.sInstalled     // Catch:{ all -> 0x0347 }
            if (r1 != 0) goto L_0x0336
            java.lang.String r2 = "minidumps"
            java.io.File r1 = r0.getDir(r2, r14)     // Catch:{ all -> 0x0347 }
            if (r1 == 0) goto L_0x032e
            java.lang.String r1 = r1.getAbsolutePath()     // Catch:{ all -> 0x0347 }
            com.facebook.logcatinterceptor.LogcatInterceptor.nativeInstall(r1, r6, r14)     // Catch:{ all -> 0x0347 }
            com.facebook.logcatinterceptor.LogcatInterceptor.sInstalled = r9     // Catch:{ all -> 0x0347 }
            goto L_0x0336
        L_0x032e:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x0347 }
            java.lang.String r1 = "Logcat Install failed to create crash directory: minidumps"
            r2.<init>(r1)     // Catch:{ all -> 0x0347 }
            throw r2     // Catch:{ all -> 0x0347 }
        L_0x0336:
            monitor-exit(r4)     // Catch:{ all -> 0x034a }
            boolean r1 = com.facebook.logcatinterceptor.LogcatInterceptor.sInstalled     // Catch:{ all -> 0x034a }
            if (r1 == 0) goto L_0x033f
            com.facebook.logcatinterceptor.LogcatInterceptor.nativeIntegrateWithBreakpad(r14)     // Catch:{ all -> 0x034a }
            goto L_0x0350
        L_0x033f:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x034a }
            java.lang.String r1 = "Logcat interceptor not installed"
            r2.<init>(r1)     // Catch:{ all -> 0x034a }
            throw r2     // Catch:{ all -> 0x034a }
        L_0x0347:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x034a }
            throw r1     // Catch:{ all -> 0x034a }
        L_0x034a:
            r2 = move-exception
            java.lang.String r1 = "Unable to install logcat interceptor"
            android.util.Log.e(r8, r1, r2)
        L_0x0350:
            java.lang.String r1 = "android_aborthooks_enabled"
            boolean r1 = X.AnonymousClass08X.A08(r0, r1, r9)
            if (r1 == 0) goto L_0x036e
            com.facebook.aborthooks.AbortHooks.install()     // Catch:{ all -> 0x0368 }
            com.facebook.aborthooks.AbortHooks.hookAbort()     // Catch:{ all -> 0x0368 }
            com.facebook.aborthooks.AbortHooks.hookAndroidLogAssert()     // Catch:{ all -> 0x0368 }
            com.facebook.aborthooks.AbortHooks.setGlogFatalHandler()     // Catch:{ all -> 0x0368 }
            com.facebook.aborthooks.AbortHooks.hookAndroidSetAbortMessage()     // Catch:{ all -> 0x0368 }
            goto L_0x036e
        L_0x0368:
            r2 = move-exception
            java.lang.String r1 = "Unable to install abort hooks"
            android.util.Log.e(r8, r1, r2)
        L_0x036e:
            android.content.Context r6 = r0.getBaseContext()
            java.lang.String r1 = "fb.running_e2e"
            java.lang.String r2 = java.lang.System.getProperty(r1)
            java.lang.String r1 = "true"
            boolean r4 = r1.equals(r2)
            java.lang.String r2 = "android_crash_lyra_hook_cxa_throw"
            int r1 = X.AnonymousClass08X.A00(r6, r2, r4)
            r5 = 1
            if (r1 == r9) goto L_0x0388
            r5 = 0
        L_0x0388:
            java.lang.String r1 = "android_crash_lyra_enable_backtraces"
            int r1 = X.AnonymousClass08X.A00(r6, r1, r4)
            r4 = 1
            if (r1 == r9) goto L_0x0392
            r4 = 0
        L_0x0392:
            java.lang.String r1 = "os.arch"
            java.lang.String r2 = java.lang.System.getProperty(r1)
            if (r5 == 0) goto L_0x03b0
            java.lang.String r1 = "x86_64"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 != 0) goto L_0x03b0
            boolean r1 = com.facebook.common.lyra.LyraManager.nativeInstallLyraHook(r4)
            if (r1 != 0) goto L_0x03b0
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Installing lyra hook failed. Try reinstalling the app."
            r1.<init>(r0)
            throw r1
        L_0x03b0:
            android.content.Context r4 = r0.getBaseContext()
            java.lang.String r2 = "terminate_handler_flags_store"
            android.content.SharedPreferences r4 = r4.getSharedPreferences(r2, r14)
            java.lang.String r2 = "android_enable_terminate_handler"
            boolean r1 = r4.getBoolean(r2, r9)
            if (r1 == 0) goto L_0x03c5
            com.facebook.common.terminate_handler.TerminateHandlerManager.nativeInstallTerminateHandler()
        L_0x03c5:
            java.lang.Object r2 = X.AnonymousClass01P.A0Z
            monitor-enter(r2)
            X.01P r1 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x0547 }
            if (r1 == 0) goto L_0x053f
            monitor-exit(r2)     // Catch:{ all -> 0x0547 }
            X.01P r7 = X.AnonymousClass01P.A0Y
            X.01G r2 = r7.A0C
            android.content.Context r1 = r7.A08
            boolean r1 = r2.A0J(r1)
            if (r1 != 0) goto L_0x044f
            java.io.File r4 = r7.A0E
            java.lang.String r2 = r4.getAbsolutePath()
            java.lang.String r1 = "_native"
            java.lang.String r12 = X.AnonymousClass08S.A0J(r2, r1)
            java.lang.String r2 = r4.getAbsolutePath()
            java.lang.String r1 = "_anr"
            java.lang.String r11 = X.AnonymousClass08S.A0J(r2, r1)
            java.lang.String r2 = r4.getAbsolutePath()
            java.lang.String r1 = "_wrotedump"
            java.lang.String r2 = X.AnonymousClass08S.A0J(r2, r1)
            android.content.Context r8 = r7.A08
            boolean r6 = X.AnonymousClass01P.A0F(r7)
            boolean r5 = X.AnonymousClass01P.A0G(r7)
            X.01G r4 = r7.A0C
            java.lang.String r1 = "appstatelogger"
            X.AnonymousClass01q.A08(r1)
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.registerWithNativeCrashHandler(r12, r11, r2)
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.registerStreamWithBreakpad()
            boolean r1 = r4.A0H(r8)
            if (r1 == 0) goto L_0x0419
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.registerOomHandler()
        L_0x0419:
            boolean r1 = r4.A0F(r8)
            if (r1 == 0) goto L_0x042a
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.registerSelfSigkillHandlers()
            X.092 r1 = new X.092
            r1.<init>()
            r4.A0A(r1)
        L_0x042a:
            boolean r2 = r4.A0N(r8)
            boolean r1 = r4.A0M(r8)
            if (r2 != 0) goto L_0x0436
            if (r1 == 0) goto L_0x0439
        L_0x0436:
            com.facebook.analytics.appstatelogger.EarlyActivityTransitionMonitor.start(r2, r1)
        L_0x0439:
            java.lang.Class<com.facebook.analytics.appstatelogger.AppStateLoggerNative> r1 = com.facebook.analytics.appstatelogger.AppStateLoggerNative.class
            monitor-enter(r1)
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.appInForeground(r6, r5)     // Catch:{ all -> 0x0443 }
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited = r9     // Catch:{ all -> 0x0443 }
            monitor-exit(r1)     // Catch:{ all -> 0x0443 }
            goto L_0x0447
        L_0x0443:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0443 }
            goto L_0x0549
        L_0x0447:
            X.01i r2 = r7.A0B
            boolean r1 = r2.A0J
            if (r1 != 0) goto L_0x0537
            r2.A0J = r9
        L_0x044f:
            com.facebook.common.dextricks.DalvikInternals$CrashLogParameters r4 = new com.facebook.common.dextricks.DalvikInternals$CrashLogParameters
            r1 = r19
            r4.<init>(r1)
            java.lang.String r2 = X.AnonymousClass014.A06(r0)
            java.lang.String r1 = X.AnonymousClass014.A07(r0)
            com.facebook.common.dextricks.DalvikInternals.integrateWithCrashLog(r2, r1, r4)
            com.facebook.acra.ACRA.safeToLoadNativeLibraries(r0)
            r4 = 0
            r7 = 4
            if (r3 == 0) goto L_0x04ab
            boolean r1 = r10.equals(r3)     // Catch:{ all -> 0x0533 }
            if (r1 != 0) goto L_0x04ab
            java.lang.String r1 = "optsvc"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0533 }
            if (r1 == 0) goto L_0x0483
            r2 = 6
            X.08W r1 = r0.A02     // Catch:{ all -> 0x0533 }
            com.facebook.common.dextricks.DexLibLoader.loadAll(r0, r2, r1)     // Catch:{ all -> 0x0533 }
            com.facebook.base.app.ApplicationLike r1 = new com.facebook.base.app.ApplicationLike     // Catch:{ all -> 0x0533 }
            r1.<init>()     // Catch:{ all -> 0x0533 }
            goto L_0x0514
        L_0x0483:
            java.lang.String[] r6 = com.facebook.common.build.BuildConstants.A00     // Catch:{ all -> 0x0533 }
            int r5 = r6.length     // Catch:{ all -> 0x0533 }
            r2 = 0
        L_0x0487:
            if (r2 >= r5) goto L_0x0497
            r1 = r6[r2]     // Catch:{ all -> 0x0533 }
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x0533 }
            if (r1 == 0) goto L_0x0492
            goto L_0x0495
        L_0x0492:
            int r2 = r2 + 1
            goto L_0x0487
        L_0x0495:
            r1 = 1
            goto L_0x0498
        L_0x0497:
            r1 = 0
        L_0x0498:
            if (r1 == 0) goto L_0x04a5
            X.08W r1 = r0.A02     // Catch:{ all -> 0x0533 }
            com.facebook.common.dextricks.DexLibLoader.loadAll(r0, r7, r1)     // Catch:{ all -> 0x0533 }
            com.facebook.base.app.ApplicationLike r1 = new com.facebook.base.app.ApplicationLike     // Catch:{ all -> 0x0533 }
            r1.<init>()     // Catch:{ all -> 0x0533 }
            goto L_0x0514
        L_0x04a5:
            com.facebook.base.app.ApplicationLike r1 = new com.facebook.base.app.ApplicationLike     // Catch:{ all -> 0x0533 }
            r1.<init>()     // Catch:{ all -> 0x0533 }
            goto L_0x0514
        L_0x04ab:
            com.facebook.common.dextricks.LargeHeapHelper.maybeEnableLargeHeap(r0)     // Catch:{ all -> 0x0533 }
            X.08W r1 = r0.A02     // Catch:{ all -> 0x0533 }
            com.facebook.common.dextricks.DexLibLoader.loadAll(r0, r7, r1)     // Catch:{ all -> 0x0533 }
            com.facebook.common.dextricks.MemoryReductionHack.freeApkZip(r0)     // Catch:{ all -> 0x0533 }
            com.facebook.common.dextricks.DalvikLinearAllocType r1 = com.facebook.common.dextricks.DalvikLinearAllocType.MESSENGER_RELEASE     // Catch:{ all -> 0x0533 }
            com.facebook.common.dextricks.DalvikReplaceBuffer.replaceBufferIfNecessary(r1)     // Catch:{ all -> 0x0533 }
            r3 = 8
            java.lang.Class<X.00x> r5 = X.C001300x.class
            X.00x r6 = r0.A04     // Catch:{ all -> 0x0533 }
            java.lang.Class r7 = java.lang.Long.TYPE     // Catch:{ all -> 0x0533 }
            long r1 = r0.A01     // Catch:{ all -> 0x0533 }
            java.lang.Long r8 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0533 }
            java.lang.Class<X.08W> r9 = X.AnonymousClass08W.class
            X.08W r10 = r0.A02     // Catch:{ all -> 0x0533 }
            java.lang.Class<X.00T> r11 = X.AnonymousClass00T.class
            X.00T r12 = r0.A03     // Catch:{ all -> 0x0533 }
            java.lang.Object[] r8 = new java.lang.Object[]{r5, r6, r7, r8, r9, r10, r11, r12}     // Catch:{ all -> 0x0533 }
            int r7 = r3 / 2
            int r1 = r7 + 1
            java.lang.Class[] r6 = new java.lang.Class[r1]     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.Object[] r5 = new java.lang.Object[r1]     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.Class<android.app.Application> r2 = android.app.Application.class
            r1 = 0
            r6[r14] = r2     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            r5[r14] = r40     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
        L_0x04e4:
            if (r1 >= r7) goto L_0x04f8
            int r3 = r1 + 1
            int r2 = r1 << 1
            r1 = r8[r2]     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.Class r1 = (java.lang.Class) r1     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            r6[r3] = r1     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            int r1 = r2 + 1
            r1 = r8[r1]     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            r5[r3] = r1     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            r1 = r3
            goto L_0x04e4
        L_0x04f8:
            java.lang.Class r1 = r0.getClass()     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.String r2 = r1.getName()     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.String r1 = "Impl"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r2, r1)     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.reflect.Constructor r1 = r1.getDeclaredConstructor(r6)     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            java.lang.Object r1 = r1.newInstance(r5)     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
            com.facebook.base.app.ApplicationLike r1 = (com.facebook.base.app.ApplicationLike) r1     // Catch:{ InvocationTargetException -> 0x051e, ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException -> 0x0517 }
        L_0x0514:
            r0.A03 = r4
            return r1
        L_0x0517:
            r2 = move-exception
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0533 }
            r1.<init>(r2)     // Catch:{ all -> 0x0533 }
            goto L_0x0532
        L_0x051e:
            r2 = move-exception
            java.lang.Throwable r1 = r2.getCause()     // Catch:{ all -> 0x0533 }
            if (r1 == 0) goto L_0x0526
            r2 = r1
        L_0x0526:
            boolean r1 = r2 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0533 }
            if (r1 == 0) goto L_0x052d
            java.lang.RuntimeException r2 = (java.lang.RuntimeException) r2     // Catch:{ all -> 0x0533 }
            throw r2     // Catch:{ all -> 0x0533 }
        L_0x052d:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0533 }
            r1.<init>(r2)     // Catch:{ all -> 0x0533 }
        L_0x0532:
            throw r1     // Catch:{ all -> 0x0533 }
        L_0x0533:
            r1 = move-exception
            r0.A03 = r4
            throw r1
        L_0x0537:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Native crash reporting is already initialized"
            r1.<init>(r0)
            throw r1
        L_0x053f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0547 }
            java.lang.String r0 = "Application needs to be registered before native crash reporting"
            r1.<init>(r0)     // Catch:{ all -> 0x0547 }
            throw r1     // Catch:{ all -> 0x0547 }
        L_0x0547:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0547 }
        L_0x0549:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messenger.app.MessengerApplication.A05():com.facebook.base.app.ApplicationLike");
    }

    public C000000c A06() {
        return this.A02;
    }

    public void A09(Throwable th) {
        String A042 = AnonymousClass00M.A00().A04();
        if (A042 == null || !A042.equals("bsod")) {
            BSODActivity.A00(this, 2131231430, 2131820629, 2131820620, 2131820617, 2131821487);
            AnonymousClass016.A03(Thread.currentThread(), new RuntimeException("Not enough disk space", th));
        }
    }

    public void A0A(Throwable th) {
        String A042 = AnonymousClass00M.A00().A04();
        if (A042 == null || !A042.equals("bsod")) {
            BSODActivity.A00(this, 2131231430, 2131820629, 2131820625, 2131820624, 2131821487);
            AnonymousClass016.A03(Thread.currentThread(), new RuntimeException("Files not found", th));
        }
    }

    public void A0B(Throwable th) {
        String A042 = AnonymousClass00M.A00().A04();
        if (A042 == null || !A042.equals("bsod")) {
            BSODActivity.A00(this, 2131231430, 2131820629, 2131820636, 2131820630, 2131821487);
            AnonymousClass016.A03(Thread.currentThread(), new RuntimeException("Architecture mismatch", th));
        }
    }

    public void A0I(Intent intent) {
        super.A0I(intent);
        if (this.A00 == null) {
            this.A00 = intent;
        }
    }

    public void attachBaseContext(Context context) {
        this.A01 = SystemClock.uptimeMillis();
        AnonymousClass00T r0 = new AnonymousClass00T();
        r0.A02();
        this.A03 = r0;
        this.A02 = new AnonymousClass08W();
        super.attachBaseContext(context);
    }

    public File getCacheDir() {
        File cacheDir = super.getCacheDir();
        if (C009508d.A00) {
            File file = new File(cacheDir, "browser_proc");
            if (file.isDirectory() || file.mkdirs()) {
                return file;
            }
        }
        return cacheDir;
    }

    public void onCreate() {
        super.onCreate();
        AnonymousClass07c.isEndToEndTestRun = C006006f.A01();
    }

    public Class A0G(Intent intent) {
        return MessengerSplashScreenActivity.class;
    }
}
