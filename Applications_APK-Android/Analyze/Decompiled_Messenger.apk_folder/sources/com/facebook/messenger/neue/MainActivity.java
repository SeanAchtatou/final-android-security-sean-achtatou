package com.facebook.messenger.neue;

import X.AnonymousClass07B;
import X.AnonymousClass0UN;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.C08770fv;
import X.C11570nO;
import X.C11580nP;
import X.C12980qK;
import X.C13090qd;
import X.C13100qf;
import X.C17060yG;
import X.C17080yI;
import X.C32281lV;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.common.classmarkers.ClassMarkerLoader;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends InterfaceDelegatingMainActivity implements C11570nO, C11580nP, CallerContextable {
    public AnonymousClass0UN A00;
    private int A01;

    public MainActivity() {
        super(new C12980qK());
    }

    public Map Acr() {
        HashMap hashMap = new HashMap();
        hashMap.put("badge_number", Integer.valueOf(this.A01));
        return hashMap;
    }

    public Integer Agn() {
        return AnonymousClass07B.A00;
    }

    public void onMultiWindowModeChanged(boolean z, Configuration configuration) {
        AnonymousClass1XX.A03(AnonymousClass1Y3.AR0, this.A00);
    }

    public void A17() {
        super.A17();
        ((C08770fv) AnonymousClass1XX.A03(AnonymousClass1Y3.B9x, this.A00)).A0S = true;
    }

    public void A18(Context context) {
        super.A18(context);
        ClassMarkerLoader.loadIsMessengerStartupBeginMarker();
        AnonymousClass0UN r1 = new AnonymousClass0UN(3, AnonymousClass1XX.get(this));
        this.A00 = r1;
        C13100qf r12 = (C13100qf) AnonymousClass1XX.A03(AnonymousClass1Y3.APU, r1);
        C32281lV r0 = new C32281lV(this);
        r12.A00 = this;
        r12.A01 = r0;
        this.A00 = new C13090qd(r12);
        this.A00 = r12;
    }

    public void A1B(Bundle bundle) {
        super.A1B(bundle);
        this.A01 = ((C17060yG) AnonymousClass1XX.A03(AnonymousClass1Y3.AaH, this.A00)).A03();
        getWindow().getDecorView().setAccessibilityDelegate(new C17080yI(this));
    }
}
