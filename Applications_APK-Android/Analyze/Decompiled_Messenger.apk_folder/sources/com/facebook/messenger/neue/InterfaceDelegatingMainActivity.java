package com.facebook.messenger.neue;

import X.AnonymousClass0nG;
import X.C12980qK;
import X.C13110qg;
import android.view.Menu;
import com.facebook.base.activity.DelegatingFbFragmentFrameworkActivity;
import com.facebook.messaging.model.threadkey.ThreadKey;
import java.util.Map;

public class InterfaceDelegatingMainActivity extends DelegatingFbFragmentFrameworkActivity implements AnonymousClass0nG {
    public C13110qg A00;

    public boolean ARW() {
        return this.A00.ARW();
    }

    public String Act() {
        return this.A00.Act();
    }

    public ThreadKey AjF() {
        return this.A00.AjF();
    }

    public Map Ajk() {
        return this.A00.Ajk();
    }

    public void BPe(int i) {
        this.A00.BPe(i);
    }

    public void BpD(int i, int i2, int i3, int i4, boolean z) {
        this.A00.BpD(i, i2, i3, i4, z);
    }

    public void Bxr(Menu menu) {
        this.A00.Bxr(menu);
    }

    public InterfaceDelegatingMainActivity(C12980qK r1) {
        super(r1);
    }
}
