package com.facebook.messenger.msys.securemessage;

import X.AnonymousClass064;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass0WY;
import X.AnonymousClass1TG;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass4C0;
import X.C010708t;
import X.C04310Tq;
import X.C10950l8;
import X.C157187On;
import X.C17920zh;
import X.C200699cX;
import X.C28831fR;
import X.C29069EKc;
import X.C29070EKf;
import X.C29071EKg;
import X.C29072EKh;
import X.C30457Ewo;
import X.C33881oI;
import X.C415526a;
import X.C421828p;
import X.C57262rh;
import X.EJ9;
import X.EKS;
import X.EKU;
import X.EKW;
import X.EKX;
import X.EKa;
import X.EKd;
import android.net.Uri;
import android.util.Pair;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messenger.msys.securemessage.exceptions.SecureMessageSendingException;
import com.facebook.messenger.securemessage.interfaces.SecureMessageMasterKeyProvider;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import javax.inject.Singleton;

@Singleton
public class MessengerMsysSecureMessage {
    private static volatile MessengerMsysSecureMessage $ul_$xXXcom_facebook_messenger_msys_securemessage_MessengerMsysSecureMessage$xXXINSTANCE;
    public static C29069EKc mMailboxSecureMessage;
    public static boolean sInitialized;
    public static SecureMessageMasterKeyProvider sSecureMessageMasterKeyProvider;
    public AnonymousClass0UN $ul_mInjectionContext;
    @LoggedInUser
    public final C04310Tq mLoggedInUserProvider;

    /* JADX WARN: Failed to insert an additional move for type inference into block B:29:? */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r4v2, types: [com.google.common.util.concurrent.ListenableFuture] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void ensureSecureMessageInitialized() {
        /*
            r8 = this;
            r7 = r8
            monitor-enter(r7)     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            boolean r0 = com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.sInitialized     // Catch:{ all -> 0x00c7 }
            r5 = 1
            if (r0 == 0) goto L_0x0012
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)     // Catch:{ all -> 0x00c7 }
            com.google.common.util.concurrent.ListenableFuture r4 = X.C05350Yp.A03(r0)     // Catch:{ all -> 0x00c7 }
            monitor-exit(r7)     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            goto L_0x00ae
        L_0x0012:
            com.google.common.util.concurrent.SettableFuture r4 = com.google.common.util.concurrent.SettableFuture.create()     // Catch:{ all -> 0x00c7 }
            X.EKc r2 = new X.EKc     // Catch:{ all -> 0x00c7 }
            int r1 = X.AnonymousClass1Y3.AbT     // Catch:{ all -> 0x00c7 }
            X.0UN r0 = r8.$ul_mInjectionContext     // Catch:{ all -> 0x00c7 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x00c7 }
            X.999 r0 = (X.AnonymousClass999) r0     // Catch:{ all -> 0x00c7 }
            X.EJJ r0 = r0.A01()     // Catch:{ all -> 0x00c7 }
            r2.<init>(r0)     // Catch:{ all -> 0x00c7 }
            com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.mMailboxSecureMessage = r2     // Catch:{ all -> 0x00c7 }
            r1 = 2
            int r0 = X.AnonymousClass1Y3.AaK     // Catch:{ all -> 0x00c7 }
            X.0UN r2 = r8.$ul_mInjectionContext     // Catch:{ all -> 0x00c7 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r2)     // Catch:{ all -> 0x00c7 }
            com.facebook.messenger.securemessage.interfaces.SecureMessageMasterKeyProvider r0 = (com.facebook.messenger.securemessage.interfaces.SecureMessageMasterKeyProvider) r0     // Catch:{ all -> 0x00c7 }
            com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.sSecureMessageMasterKeyProvider = r0     // Catch:{ all -> 0x00c7 }
            X.8db r1 = new X.8db     // Catch:{ all -> 0x00c7 }
            int r0 = X.AnonymousClass1Y3.A6S     // Catch:{ all -> 0x00c7 }
            r3 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r0, r2)     // Catch:{ all -> 0x00c7 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x00c7 }
            r1.<init>(r0)     // Catch:{ all -> 0x00c7 }
            r6 = 1
            r1.A00 = r5     // Catch:{ all -> 0x00c7 }
            X.0XQ r5 = r1.A00()     // Catch:{ all -> 0x00c7 }
            com.facebook.messenger.securemessage.orca.SecureMessageCryptoProviderImpl r1 = new com.facebook.messenger.securemessage.orca.SecureMessageCryptoProviderImpl     // Catch:{ all -> 0x00c7 }
            r1.<init>()     // Catch:{ all -> 0x00c7 }
            monitor-enter(r1)     // Catch:{ all -> 0x00c7 }
            boolean r0 = com.facebook.messenger.securemessage.orca.SecureMessageCryptoProviderImpl.sInitialized     // Catch:{ all -> 0x00c1 }
            if (r0 != 0) goto L_0x005c
            com.facebook.messenger.securemessage.orca.SecureMessageCryptoProviderImpl.nativeRegisterCryptoProviderHandler()     // Catch:{ all -> 0x00c1 }
            com.facebook.messenger.securemessage.orca.SecureMessageCryptoProviderImpl.sInitialized = r6     // Catch:{ all -> 0x00c1 }
        L_0x005c:
            monitor-exit(r1)     // Catch:{ all -> 0x00c7 }
            com.facebook.messenger.securemessage.interfaces.SecureMessageMasterKeyProvider r2 = com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.sSecureMessageMasterKeyProvider     // Catch:{ all -> 0x00c7 }
            int r1 = X.AnonymousClass1Y3.A6S     // Catch:{ all -> 0x00c7 }
            X.0UN r0 = r8.$ul_mInjectionContext     // Catch:{ all -> 0x00c7 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x00c7 }
            android.content.Context r1 = (android.content.Context) r1     // Catch:{ all -> 0x00c7 }
            X.0Tq r0 = r8.mLoggedInUserProvider     // Catch:{ all -> 0x00c7 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00c7 }
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0     // Catch:{ all -> 0x00c7 }
            java.lang.String r0 = r0.A0j     // Catch:{ all -> 0x00c7 }
            r2.initialize(r1, r0, r5)     // Catch:{ all -> 0x00c7 }
            X.0Tq r0 = r8.mLoggedInUserProvider     // Catch:{ all -> 0x00c7 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00c7 }
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0     // Catch:{ all -> 0x00c7 }
            java.lang.String r3 = r0.A0j     // Catch:{ all -> 0x00c7 }
            com.facebook.messenger.securemessage.interfaces.SecureMessageMasterKeyProvider r2 = com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.sSecureMessageMasterKeyProvider     // Catch:{ all -> 0x00c7 }
            java.lang.Class<X.9cS> r1 = X.C200659cS.class
            monitor-enter(r1)     // Catch:{ all -> 0x00c7 }
            boolean r0 = X.C200659cS.A03     // Catch:{ all -> 0x00c4 }
            if (r0 != 0) goto L_0x0091
            X.C200659cS.A02 = r3     // Catch:{ all -> 0x00c4 }
            X.C200659cS.A00 = r5     // Catch:{ all -> 0x00c4 }
            X.C200659cS.A01 = r2     // Catch:{ all -> 0x00c4 }
            X.C200659cS.A03 = r6     // Catch:{ all -> 0x00c4 }
        L_0x0091:
            monitor-exit(r1)     // Catch:{ all -> 0x00c7 }
            X.EKc r3 = com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.mMailboxSecureMessage     // Catch:{ all -> 0x00c7 }
            X.9cU r1 = new X.9cU     // Catch:{ all -> 0x00c7 }
            r1.<init>(r4)     // Catch:{ all -> 0x00c7 }
            X.EJ9 r2 = new X.EJ9     // Catch:{ all -> 0x00c7 }
            X.EJJ r0 = r3.A00     // Catch:{ all -> 0x00c7 }
            r2.<init>(r0)     // Catch:{ all -> 0x00c7 }
            r2.A02(r1)     // Catch:{ all -> 0x00c7 }
            X.EJJ r1 = r3.A00     // Catch:{ all -> 0x00c7 }
            X.EKm r0 = new X.EKm     // Catch:{ all -> 0x00c7 }
            r0.<init>(r2)     // Catch:{ all -> 0x00c7 }
            r1.A02(r0)     // Catch:{ all -> 0x00c7 }
            monitor-exit(r7)     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
        L_0x00ae:
            java.lang.Object r0 = r4.get()     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            boolean r0 = r0.booleanValue()     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            X.AnonymousClass064.A03(r0)     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            X.EKc r0 = com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.mMailboxSecureMessage     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            X.AnonymousClass064.A00(r0)     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            return
        L_0x00c1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00c7 }
            goto L_0x00c6
        L_0x00c4:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00c7 }
        L_0x00c6:
            throw r0     // Catch:{ all -> 0x00c7 }
        L_0x00c7:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
            throw r0     // Catch:{ InterruptedException | ExecutionException -> 0x00ca }
        L_0x00ca:
            r2 = move-exception
            java.lang.String r1 = "com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage"
            java.lang.String r0 = "Error during secure message initialization: "
            X.C010708t.A0L(r1, r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage.ensureSecureMessageInitialized():void");
    }

    public static final MessengerMsysSecureMessage $ul_$xXXcom_facebook_messenger_msys_securemessage_MessengerMsysSecureMessage$xXXFACTORY_METHOD(AnonymousClass1XY r4) {
        if ($ul_$xXXcom_facebook_messenger_msys_securemessage_MessengerMsysSecureMessage$xXXINSTANCE == null) {
            synchronized (MessengerMsysSecureMessage.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_messenger_msys_securemessage_MessengerMsysSecureMessage$xXXINSTANCE, r4);
                if (A00 != null) {
                    try {
                        $ul_$xXXcom_facebook_messenger_msys_securemessage_MessengerMsysSecureMessage$xXXINSTANCE = new MessengerMsysSecureMessage(r4.getApplicationInjector());
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_messenger_msys_securemessage_MessengerMsysSecureMessage$xXXINSTANCE;
    }

    public static ThreadParticipant A00(User user, long j, long j2) {
        ParticipantInfo participantInfo = new ParticipantInfo(user.A0Q, user.A08());
        C28831fR r1 = new C28831fR();
        r1.A04 = participantInfo;
        r1.A01 = j2;
        r1.A02 = j;
        r1.A03 = j;
        return new ThreadParticipant(r1);
    }

    public Pair fetchThreadsAndUsers(ThreadKey threadKey, long j, int i) {
        long j2;
        long j3;
        long j4;
        ensureSecureMessageInitialized();
        try {
            User user = (User) this.mLoggedInUserProvider.get();
            if (user == null) {
                return Pair.create(ThreadsCollection.A02, RegularImmutableList.A02);
            }
            ThreadKey threadKey2 = threadKey;
            if (threadKey != null) {
                j2 = threadKey2.A01;
            } else {
                j2 = -1;
            }
            C29069EKc eKc = mMailboxSecureMessage;
            EJ9 ej9 = new EJ9(eKc.A00);
            int i2 = i;
            eKc.A00.A02(new EKd(ej9, i2, j2, j));
            EKa eKa = (EKa) ((C30457Ewo) ej9.get()).A00;
            if (eKa == null) {
                return Pair.create(ThreadsCollection.A02, RegularImmutableList.A02);
            }
            ImmutableList.Builder builder = new ImmutableList.Builder();
            ImmutableList.Builder builder2 = new ImmutableList.Builder();
            int count = eKa.A00.getCount();
            boolean z = false;
            for (int i3 = 0; i3 < count; i3++) {
                User A01 = ((C157187On) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ATU, this.$ul_mInjectionContext)).A01(UserKey.A01(String.valueOf(eKa.A00.getLong(i3, 0))));
                if (A01 != null) {
                    ThreadKey A05 = ThreadKey.A05(eKa.A00.getLong(i3, 0), Long.parseLong(user.A0j));
                    AnonymousClass064.A00(A05);
                    AnonymousClass064.A04(A05.A0K());
                    C17920zh A00 = ThreadSummary.A00();
                    A00.A0N = C10950l8.A05;
                    A00.A15 = true;
                    A00.A0x = true;
                    A00.A0R = A05;
                    A00.A0A = eKa.A00.getLong(i3, 2);
                    A00.A06 = eKa.A00.getLong(i3, 3);
                    if (A01.A08() != null) {
                        A00.A0p = A01.A08();
                    }
                    if (eKa.A00.getNullableLong(i3, 9) != null) {
                        j3 = eKa.A00.getNullableLong(i3, 9).longValue();
                    } else {
                        j3 = 0;
                    }
                    if (eKa.A00.getNullableLong(i3, 10) != null) {
                        j4 = eKa.A00.getNullableLong(i3, 10).longValue();
                    } else {
                        j4 = 0;
                    }
                    ThreadParticipant A002 = A00(A01, j3, j4);
                    ThreadParticipant A003 = A00(user, 0, 0);
                    A00.A03(ImmutableList.of(A002, A003));
                    String string = eKa.A00.getString(i3, 5);
                    if (eKa.A00.getInteger(i3, 6) != 0) {
                        A00.A0l = string;
                    } else {
                        A00.A0s = string;
                        if (String.valueOf(eKa.A00.getNullableLong(i3, 7)).equals(A003.A00().id)) {
                            A00.A0P = A003.A04;
                        } else {
                            A00.A0P = A002.A04;
                        }
                    }
                    ThreadSummary A004 = A00.A00();
                    if (A004 != null) {
                        builder.add((Object) A004);
                        builder2.add((Object) A01);
                    }
                }
            }
            builder2.add((Object) user);
            ImmutableList build = builder.build();
            if (i <= 0 || build.size() < i2) {
                z = true;
            }
            return Pair.create(new ThreadsCollection(build, z), builder2.build());
        } catch (InterruptedException | ExecutionException e) {
            C010708t.A0L("com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage", "Failed to fetch secure threads from msys db", e);
            return Pair.create(ThreadsCollection.A02, RegularImmutableList.A02);
        }
    }

    public void sendMessage(Message message, ThreadKey threadKey) {
        String str;
        ensureSecureMessageInitialized();
        Message message2 = message;
        ImmutableList<MediaResource> immutableList = message2.A0b;
        String str2 = message2.A0x;
        Long l = null;
        if (str2 == null) {
            str2 = ((C57262rh) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AnD, this.$ul_mInjectionContext)).A09(message2, null, false);
        }
        ThreadKey threadKey2 = threadKey;
        if (immutableList.isEmpty()) {
            String str3 = message2.A0z;
            C29069EKc eKc = mMailboxSecureMessage;
            Long valueOf = Long.valueOf(threadKey2.A01);
            Long valueOf2 = Long.valueOf(threadKey2.A04);
            String str4 = message2.A10;
            if (str3 != null) {
                l = Long.valueOf(Long.parseLong(str3));
            }
            eKc.A00.A02(new C29071EKg(new EJ9(eKc.A00), valueOf, valueOf2, str4, l, str2));
            return;
        }
        for (MediaResource mediaResource : immutableList) {
            C421828p r3 = mediaResource.A0L;
            if (r3.ordinal() != 5) {
                throw new SecureMessageSendingException(StringFormatUtil.formatStrLocaleSafe("The attachment type %s is not supported yet", r3.toString()));
            }
            String path = mediaResource.A0D.getPath();
            if (path != null) {
                File file = new File(path);
                Uri uri = mediaResource.A0C;
                if (uri != null) {
                    str = uri.getPath();
                } else {
                    str = mediaResource.A0D.getPath();
                }
                try {
                    byte[] bArr = new byte[((int) file.length())];
                    new DataInputStream(new BufferedInputStream(new FileInputStream(file))).readFully(bArr);
                    String str5 = mediaResource.A0b;
                    if (str5 == null) {
                        str5 = Long.toString(((C415526a) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BRT, this.$ul_mInjectionContext)).A01());
                    }
                    C29069EKc eKc2 = mMailboxSecureMessage;
                    Long valueOf3 = Long.valueOf(threadKey2.A01);
                    Long valueOf4 = Long.valueOf(threadKey2.A04);
                    String str6 = mediaResource.A0a;
                    String path2 = mediaResource.A0D.getPath();
                    String str7 = mediaResource.A0a;
                    String str8 = mediaResource.A0Y;
                    if (str8 == null) {
                        str8 = file.getName();
                    }
                    Integer valueOf5 = Integer.valueOf(mediaResource.A00);
                    Integer valueOf6 = Integer.valueOf(mediaResource.A04);
                    Long valueOf7 = Long.valueOf(mediaResource.A07);
                    C200699cX r5 = new C200699cX();
                    EJ9 ej9 = new EJ9(eKc2.A00);
                    ej9.A02(r5);
                    eKc2.A00.A02(new C29070EKf(ej9, valueOf3, valueOf4, str2, 2, str5, str6, path2, str7, str, str8, bArr, null, valueOf5, valueOf6, valueOf7));
                } catch (IOException e) {
                    throw new SecureMessageSendingException(e.toString());
                }
            } else {
                throw new SecureMessageSendingException("Failed to find attachment local path");
            }
        }
    }

    public MessengerMsysSecureMessage(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(10, r3);
        this.mLoggedInUserProvider = AnonymousClass0WY.A01(r3);
    }

    public MessagesCollection loadSecureMessages(ThreadKey threadKey, Long l, Long l2, int i, int i2, boolean z, boolean z2, boolean z3) {
        ensureSecureMessageInitialized();
        EKS eks = (EKS) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AMy, this.$ul_mInjectionContext);
        ThreadKey threadKey2 = threadKey;
        long j = threadKey2.A03;
        EJ9 ej9 = new EJ9(eks.A00);
        int i3 = i;
        long j2 = j;
        eks.A00.A02(new EKU(ej9, 1, i3, i2, j2, j, null, l, l2, false, false, true, z, z2, z3));
        try {
            EKX ekx = ((EKW) ej9.get()).A00;
            AnonymousClass064.A00(ekx);
            ekx.A00.getCount();
            Integer valueOf = Integer.valueOf(((EKW) ej9.get()).A01.intValue());
            AnonymousClass064.A00(valueOf);
            int intValue = valueOf.intValue();
            if (ekx == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int count = ekx.A00.getCount();
            for (int i4 = 0; i4 < count; i4++) {
                AnonymousClass1TG A00 = Message.A00();
                A00.A06(ekx.A00.getString(i4, 12));
                A00.A0u = ekx.A00.getString(i4, 21);
                A00.A0T = threadKey2;
                A00.A04 = ekx.A00.getLong(i4, 52);
                A00.A0y = ekx.A00.getString(i4, 48);
                A00.A0J = new ParticipantInfo(UserKey.A00(Long.valueOf(ekx.A00.getLong(i4, 46))), null);
                arrayList.add(A00.A00());
            }
            Collections.sort(arrayList, new AnonymousClass4C0());
            boolean z4 = false;
            if (intValue == 4) {
                z4 = true;
            }
            C33881oI r1 = new C33881oI();
            r1.A03 = z4;
            r1.A04 = true;
            r1.A02 = false;
            r1.A00 = threadKey2;
            r1.A01(ImmutableList.copyOf((Collection) arrayList));
            return r1.A00();
        } catch (InterruptedException | ExecutionException e) {
            C010708t.A0L("com.facebook.messenger.msys.securemessage.MessengerMsysSecureMessage", "Failed to load secure message from msys db", e);
            return null;
        }
    }

    public void registerDevice(boolean z) {
        ensureSecureMessageInitialized();
        C29069EKc eKc = mMailboxSecureMessage;
        Long valueOf = Long.valueOf(Long.parseLong(((User) this.mLoggedInUserProvider.get()).A0j));
        eKc.A00.A02(new C29072EKh(new EJ9(eKc.A00), valueOf, z, true));
    }
}
