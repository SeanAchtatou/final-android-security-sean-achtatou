package com.facebook.secure.webkit;

import X.AnonymousClass0R7;
import android.content.Context;
import android.util.AttributeSet;

public class WebView extends android.webkit.WebView {
    public WebView(Context context) {
        super(context);
        AnonymousClass0R7.A00(getSettings());
    }

    public WebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        AnonymousClass0R7.A00(getSettings());
    }

    public WebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        AnonymousClass0R7.A00(getSettings());
    }

    public WebView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        AnonymousClass0R7.A00(getSettings());
    }

    public WebView(Context context, AttributeSet attributeSet, int i, boolean z) {
        super(context, attributeSet, i, z);
        AnonymousClass0R7.A00(getSettings());
    }
}
