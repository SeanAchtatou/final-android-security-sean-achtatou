package com.facebook.secure.fileprovider;

import X.AnonymousClass04e;
import X.AnonymousClass08S;
import X.AnonymousClass1XR;
import X.AnonymousClass1XS;
import X.AnonymousClass24B;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.webkit.MimeTypeMap;
import io.card.payment.BuildConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class SecureFileProvider extends ContentProvider {
    private static final AnonymousClass04e A01 = new AnonymousClass1XR();
    private static final String[] A02 = {"_display_name", "_size"};
    private AnonymousClass1XS A00;

    public int delete(Uri uri, String str, String[] strArr) {
        try {
            File A06 = this.A00.A06(uri);
            if (A06 == null) {
                return 0;
            }
            boolean z = false;
            if (AnonymousClass1XS.A03(this.A00, A06) != null) {
                z = true;
            }
            return (!z || !A06.delete()) ? 0 : 1;
        } catch (IOException unused) {
            return 0;
        }
    }

    public boolean onCreate() {
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        int i;
        try {
            File A06 = this.A00.A06(uri);
            if (strArr == null) {
                strArr = A02;
            }
            String[] strArr3 = new String[r8];
            Object[] objArr = new Object[r8];
            int i2 = 0;
            for (String str3 : strArr) {
                if ("_display_name".equals(str3)) {
                    strArr3[i2] = "_display_name";
                    i = i2 + 1;
                    objArr[i2] = A06.getName();
                } else if ("_size".equals(str3)) {
                    strArr3[i2] = "_size";
                    i = i2 + 1;
                    objArr[i2] = Long.valueOf(A06.length());
                }
                i2 = i;
            }
            Object[] copyOf = Arrays.copyOf(objArr, i2);
            MatrixCursor matrixCursor = new MatrixCursor((String[]) Arrays.copyOf(strArr3, i2), 0);
            if (i2 > 0) {
                matrixCursor.addRow(copyOf);
            }
            return matrixCursor;
        } catch (IOException e) {
            A01.C2T("SecureFileProvider", "Query incurred an IOException", e);
            return new MatrixCursor(new String[strArr.length], 0);
        }
    }

    public static Uri A00(Context context, File file) {
        return AnonymousClass1XS.A01(context, null, new AnonymousClass1XR()).A05(file);
    }

    public String getType(Uri uri) {
        String substring;
        String mimeTypeFromExtension;
        try {
            File A06 = this.A00.A06(uri);
            int lastIndexOf = A06.getName().lastIndexOf(46);
            if (lastIndexOf == -1) {
                substring = BuildConfig.FLAVOR;
            } else {
                substring = A06.getName().substring(lastIndexOf + 1);
            }
            if (substring.length() <= 0 || (mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(substring)) == null) {
                return AnonymousClass24B.$const$string(8);
            }
            return mimeTypeFromExtension;
        } catch (IOException e) {
            A01.C2T("SecureFileProvider", "Could not resolve file type.", e);
            return BuildConfig.FLAVOR;
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException("No external inserts");
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) {
        int i;
        try {
            File A06 = this.A00.A06(uri);
            if ("r".equals(str)) {
                i = 268435456;
            } else if ("w".equals(str) || "wt".equals(str)) {
                i = 738197504;
            } else if ("wa".equals(str)) {
                i = 704643072;
            } else if ("rw".equals(str)) {
                i = 939524096;
            } else if ("rwt".equals(str)) {
                i = 1006632960;
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Invalid mode: ", str));
            }
            return ParcelFileDescriptor.open(A06, i);
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e2) {
            A01.C2T("SecureFileProvider", "IOException during file opening.", e2);
            throw new FileNotFoundException("Could not open file");
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException("No external updates");
    }

    public void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
        if (!providerInfo.exported) {
            this.A00 = AnonymousClass1XS.A01(context, providerInfo, new AnonymousClass1XR());
            return;
        }
        throw new SecurityException("Provider must not be exported.");
    }
}
