package com.facebook.secure.intentswitchoff;

import X.AnonymousClass09P;
import X.AnonymousClass0WD;
import X.AnonymousClass0WT;
import X.AnonymousClass1XY;
import X.AnonymousClass1ZD;
import X.AnonymousClass1ZE;
import X.C04750Wa;
import X.C06630bp;
import X.C06650br;
import X.C28121eI;
import X.C49822cv;
import android.app.Activity;
import android.content.Intent;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import javax.inject.Singleton;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
public final class ActivityIntentSwitchOffDI extends C28121eI {
    private static volatile ActivityIntentSwitchOffDI A02;
    public final AnonymousClass1ZE A00;
    public final AnonymousClass09P A01;

    public static final ActivityIntentSwitchOffDI A00(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (ActivityIntentSwitchOffDI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = new ActivityIntentSwitchOffDI(applicationInjector, C06650br.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public void A05(Activity activity, Intent intent) {
        JSONObject jSONObject;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(this.A00.A01("android_security_fb4a_killed_intent_logging"), 37);
        if (uSLEBaseShape0S0000000.A0G()) {
            C06630bp r5 = null;
            try {
                r5 = C49822cv.A00(intent, null, null);
            } catch (JSONException e) {
                this.A01.softReport("com.facebook.secure.intentswitchoff.ActivityIntentSwitchOffDI", "Error parsing killed intent.", e);
            }
            if (!(r5 == null || (jSONObject = r5.A01) == null)) {
                uSLEBaseShape0S0000000.A0D("activity", String.format("%s/%s", activity.getPackageName(), activity.getClass().getName()));
                uSLEBaseShape0S0000000.A0D("intent", jSONObject.toString());
                uSLEBaseShape0S0000000.A06();
            }
        }
        super.A05(activity, intent);
    }

    private ActivityIntentSwitchOffDI(AnonymousClass1XY r2, C06650br r3) {
        super(r3);
        C04750Wa.A01(r2);
        AnonymousClass0WT.A00(r2);
        this.A00 = AnonymousClass1ZD.A00(r2);
        this.A01 = C04750Wa.A01(r2);
    }
}
