package com.facebook.secure.intentswitchoff;

import X.AnonymousClass06j;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.C06650br;
import android.content.Intent;
import javax.inject.Singleton;

@Singleton
public final class FbReceiverSwitchOffDI extends AnonymousClass06j {
    private static volatile FbReceiverSwitchOffDI A00;

    public static final FbReceiverSwitchOffDI A01(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (FbReceiverSwitchOffDI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new FbReceiverSwitchOffDI(C06650br.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final FbReceiverSwitchOffDI A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public void A04(Object obj, Intent intent) {
    }

    private FbReceiverSwitchOffDI(C06650br r1) {
        super(r1);
    }
}
