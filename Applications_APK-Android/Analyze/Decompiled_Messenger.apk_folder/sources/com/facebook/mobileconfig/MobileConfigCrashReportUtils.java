package com.facebook.mobileconfig;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import java.util.Map;

public class MobileConfigCrashReportUtils {
    private static MobileConfigCrashReportUtils sInstance;
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public native void addCanaryData(String str, String str2);

    public native void clear();

    public native long count();

    public native Map getAllCanaryData();

    public native Map getAllLastFetchTimestamps();

    static {
        AnonymousClass01q.A08("mobileconfig-jni");
    }

    public static synchronized MobileConfigCrashReportUtils getInstance() {
        MobileConfigCrashReportUtils mobileConfigCrashReportUtils;
        synchronized (MobileConfigCrashReportUtils.class) {
            if (sInstance == null) {
                sInstance = new MobileConfigCrashReportUtils();
            }
            mobileConfigCrashReportUtils = sInstance;
        }
        return mobileConfigCrashReportUtils;
    }

    private MobileConfigCrashReportUtils() {
    }

    public String getSerializedCanaryData() {
        Map allCanaryData = getAllCanaryData();
        StringBuilder sb = new StringBuilder((allCanaryData.size() * 100) + 50);
        sb.append("[");
        boolean z = true;
        for (Map.Entry entry : allCanaryData.entrySet()) {
            if (!z) {
                sb.append(",");
            }
            z = false;
            sb.append("\"");
            sb.append((String) entry.getKey());
            sb.append("\"");
        }
        sb.append("]");
        return sb.toString();
    }
}
