package com.facebook.mobileconfig;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class MobileConfigCoreParams {
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public native boolean isCanaryEnabled();

    public native void setIsCanaryEnabled(boolean z);

    public native void setSkipBufferVerification(boolean z);

    public native boolean skipBufferVerification();

    static {
        AnonymousClass01q.A08("mobileconfig-jni");
    }
}
