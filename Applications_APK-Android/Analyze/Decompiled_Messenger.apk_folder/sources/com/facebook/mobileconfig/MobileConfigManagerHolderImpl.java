package com.facebook.mobileconfig;

import X.AnonymousClass01q;
import X.AnonymousClass0WV;
import X.AnonymousClass8ZO;
import X.C06190b2;
import X.C25851aV;
import android.content.res.AssetManager;
import com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory;
import com.facebook.jni.HybridData;
import com.facebook.tigon.iface.TigonServiceHolder;
import com.facebook.xanalytics.XAnalyticsHolder;
import java.util.Map;

public class MobileConfigManagerHolderImpl implements AnonymousClass0WV {
    private String mDataDirPath;
    private boolean mHasSessionId;
    private final HybridData mHybridData;

    public static native String getParamMapContentFromAsset(AssetManager assetManager);

    private static native HybridData initHybrid(AndroidAsyncExecutorFactory androidAsyncExecutorFactory, String str, String str2, String str3, TigonServiceHolder tigonServiceHolder, boolean z, XAnalyticsHolder xAnalyticsHolder, String str4, String str5, AssetManager assetManager, boolean z2, MobileConfigManagerParamsHolder mobileConfigManagerParamsHolder, Map map);

    public native void clearAlternativeUpdater();

    public native void clearCurrentUserData();

    public native void clearOverrides();

    public native void deleteOldUserData(int i);

    public native String getConsistencyLoggingFlagsJSON();

    public native String getFrameworkStatus();

    public native MobileConfigMmapHandleHolder getLatestHandleHolder();

    public native MobileConfigOverridesTableHolder getNewOverridesTableHolder();

    public native MobileConfigOverridesTableHolder getNewOverridesTableHolderIfExists();

    public native String getSchemaString();

    public native boolean isConsistencyLoggingNeeded(int i);

    public native boolean isFetchNeeded();

    public native boolean isTigonServiceSet();

    public native boolean isValid();

    public native void logConfigs(String str, int i, Map map);

    public native void logExposure(String str, String str2, String str3);

    public native void logShadowResult(String str, String str2, String str3, String str4, String str5, String str6);

    public native void logStorageConsistency();

    public native boolean registerConfigChangeListener(MobileConfigCxxChangeListener mobileConfigCxxChangeListener);

    public native boolean setEpHandler(MobileConfigEmergencyPushChangeListener mobileConfigEmergencyPushChangeListener);

    public native boolean setSandboxURL(String str);

    public native void setTigonService(TigonServiceHolder tigonServiceHolder, boolean z);

    public native String syncFetchReason();

    public native boolean tryUpdateConfigs();

    public native boolean tryUpdateConfigsSynchronously(int i);

    public native boolean updateConfigs();

    public native boolean updateConfigsSynchronouslyWithDefaultUpdater(int i);

    public native boolean updateEmergencyPushConfigs();

    public native boolean updateEmergencyPushConfigsSynchronously(int i);

    static {
        AnonymousClass01q.A08("mobileconfig-jni");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
        if (r7.length() == 0) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MobileConfigManagerHolderImpl(java.io.File r14, com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory r15, java.lang.String r16, java.lang.String r17, com.facebook.tigon.iface.TigonServiceHolder r18, boolean r19, com.facebook.xanalytics.XAnalyticsHolder r20, java.lang.String r21, java.lang.String r22, android.content.res.AssetManager r23, boolean r24, com.facebook.mobileconfig.MobileConfigManagerParamsHolder r25, java.util.Map r26) {
        /*
            r13 = this;
            r13.<init>()
            java.lang.String r0 = r14.getAbsolutePath()
            r13.mDataDirPath = r0
            r7 = r21
            if (r21 == 0) goto L_0x0014
            int r1 = r7.length()
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            r13.mHasSessionId = r0
            java.lang.String r3 = r14.getPath()
            r6 = r20
            r5 = r19
            r4 = r18
            r2 = r17
            r0 = r15
            r1 = r16
            r11 = r25
            r12 = r26
            r10 = r24
            r9 = r23
            r8 = r22
            com.facebook.jni.HybridData r0 = initHybrid(r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r13.mHybridData = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.mobileconfig.MobileConfigManagerHolderImpl.<init>(java.io.File, com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory, java.lang.String, java.lang.String, com.facebook.tigon.iface.TigonServiceHolder, boolean, com.facebook.xanalytics.XAnalyticsHolder, java.lang.String, java.lang.String, android.content.res.AssetManager, boolean, com.facebook.mobileconfig.MobileConfigManagerParamsHolder, java.util.Map):void");
    }

    public C06190b2 getLatestHandle() {
        return getLatestHandleHolder();
    }

    public C25851aV getNewOverridesTable() {
        return getNewOverridesTableHolder();
    }

    public C25851aV getNewOverridesTableIfExists() {
        return getNewOverridesTableHolderIfExists();
    }

    public boolean isConsistencyLoggingNeeded(AnonymousClass8ZO r2) {
        return isConsistencyLoggingNeeded(r2.mValue);
    }

    public void logConfigs(String str, AnonymousClass8ZO r3, Map map) {
        logConfigs(str, r3.mValue, map);
    }
}
