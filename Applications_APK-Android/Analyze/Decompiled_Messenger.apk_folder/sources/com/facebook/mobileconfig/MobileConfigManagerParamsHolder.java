package com.facebook.mobileconfig;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class MobileConfigManagerParamsHolder {
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public native void setBufferPathPostfix(String str);

    public native void setConsistencyLoggingEnabled(boolean z);

    public native void setConsistencyLoggingEveryNSec(long j);

    public native void setOTAParamsMapPath(String str);

    public native void setOmnistoreUpdaterExpected(boolean z);

    public native void setQueryHashOptimization(boolean z);

    public native void setResponseCompressionEnabled(boolean z);

    public native void setShouldParseAdditionalParamsMaps(boolean z);

    public native void setShouldUseOTAResource(boolean z);

    public native boolean setUniverseType(int i);

    public native void setUsePartialAndFullSyncFetch(boolean z);

    public native void setUsePlatformEpHandler(boolean z);

    static {
        AnonymousClass01q.A08("mobileconfig-jni");
    }
}
