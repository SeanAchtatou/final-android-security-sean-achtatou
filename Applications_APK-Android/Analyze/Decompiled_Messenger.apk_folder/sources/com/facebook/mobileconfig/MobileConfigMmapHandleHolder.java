package com.facebook.mobileconfig;

import X.AnonymousClass01q;
import X.C06190b2;
import com.facebook.jni.HybridData;
import java.nio.ByteBuffer;

public class MobileConfigMmapHandleHolder extends C06190b2 {
    private final HybridData mHybridData;

    public native String getFilename();

    static {
        AnonymousClass01q.A08("mobileconfig-jni");
    }

    private MobileConfigMmapHandleHolder(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public ByteBuffer getJavaByteBuffer() {
        return C06190b2.A00(getFilename());
    }
}
