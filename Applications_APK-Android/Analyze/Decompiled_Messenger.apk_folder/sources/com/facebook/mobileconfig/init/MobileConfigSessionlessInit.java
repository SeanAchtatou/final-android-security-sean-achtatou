package com.facebook.mobileconfig.init;

import X.AnonymousClass0UN;
import X.AnonymousClass0VG;
import X.AnonymousClass0WD;
import X.AnonymousClass0WZ;
import X.AnonymousClass0Wl;
import X.AnonymousClass0ZD;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C04310Tq;
import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
public class MobileConfigSessionlessInit extends AnonymousClass0Wl {
    public static final Class A05 = MobileConfigSessionlessInit.class;
    private static volatile MobileConfigSessionlessInit A06;
    public AnonymousClass0UN A00;
    public final Boolean A01 = true;
    public final C04310Tq A02;
    private final AnonymousClass0WZ A03;
    private final QuickPerformanceLogger A04;

    public String getSimpleName() {
        return "MobileConfigSessionlessInit";
    }

    public static final MobileConfigSessionlessInit A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (MobileConfigSessionlessInit.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new MobileConfigSessionlessInit(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private MobileConfigSessionlessInit(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.AWK, r3);
        this.A03 = AnonymousClass0WZ.A00(r3);
        this.A04 = AnonymousClass0ZD.A03(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0076, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0079, code lost:
        if ((r2 instanceof java.io.IOException) == false) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007b, code lost:
        ((X.AnonymousClass09P) X.AnonymousClass1XX.A02(3, X.AnonymousClass1Y3.Amr, r10.A00)).CGa(com.facebook.mobileconfig.init.MobileConfigSessionlessInit.A05.toString(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009e, code lost:
        r0 = r10.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a0, code lost:
        if (r9 == false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a2, code lost:
        r5 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a3, code lost:
        r0.markerEnd(13631492, 1, r5);
        X.C000700l.A09(-467799906, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ac, code lost:
        throw r1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0092  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void init() {
        /*
            r10 = this;
            r0 = -798567349(0xffffffffd066d44b, float:-1.549069E10)
            int r4 = X.C000700l.A03(r0)
            r6 = 1
            r5 = 2
            r7 = 3
            r3 = 13631492(0xd00004, float:1.9101789E-38)
            r9 = 0
            com.facebook.quicklog.QuickPerformanceLogger r0 = r10.A04     // Catch:{ Exception -> 0x0076 }
            r0.markerStart(r3, r6)     // Catch:{ Exception -> 0x0076 }
            X.0WZ r1 = r10.A03     // Catch:{ Exception -> 0x0076 }
            java.lang.String r0 = ""
            r1.A03(r0)     // Catch:{ Exception -> 0x0076 }
            X.0Tq r0 = r10.A02     // Catch:{ Exception -> 0x0076 }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x0076 }
            X.1Yc r0 = (X.C25041Yc) r0     // Catch:{ Exception -> 0x0076 }
            X.0WV r2 = r0.A09     // Catch:{ Exception -> 0x0076 }
            boolean r9 = r2.isValid()     // Catch:{ Exception -> 0x0076 }
            boolean r0 = r2 instanceof X.C25031Yb     // Catch:{ Exception -> 0x0076 }
            if (r0 == 0) goto L_0x0067
            r0 = r2
            X.1Yb r0 = (X.C25031Yb) r0     // Catch:{ Exception -> 0x0076 }
            X.0WV r0 = r0.A00()     // Catch:{ Exception -> 0x0076 }
            boolean r0 = r0 instanceof com.facebook.mobileconfig.MobileConfigManagerHolderImpl     // Catch:{ Exception -> 0x0076 }
            if (r0 == 0) goto L_0x0067
            X.6G1 r8 = new X.6G1     // Catch:{ Exception -> 0x0076 }
            r8.<init>(r10, r2)     // Catch:{ Exception -> 0x0076 }
            java.lang.Boolean r0 = r10.A01     // Catch:{ Exception -> 0x0076 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x0076 }
            if (r0 == 0) goto L_0x0056
            r2 = 0
            int r1 = X.AnonymousClass1Y3.ACz     // Catch:{ Exception -> 0x0076 }
            X.0UN r0 = r10.A00     // Catch:{ Exception -> 0x0076 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ Exception -> 0x0076 }
            X.0bZ r1 = (X.C06480bZ) r1     // Catch:{ Exception -> 0x0076 }
            r0 = 1417382022(0x547b8886, float:4.3213091E12)
            X.AnonymousClass07A.A04(r1, r8, r0)     // Catch:{ Exception -> 0x0076 }
            goto L_0x008e
        L_0x0056:
            int r1 = X.AnonymousClass1Y3.BKH     // Catch:{ Exception -> 0x0076 }
            X.0UN r0 = r10.A00     // Catch:{ Exception -> 0x0076 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ Exception -> 0x0076 }
            java.util.concurrent.ExecutorService r1 = (java.util.concurrent.ExecutorService) r1     // Catch:{ Exception -> 0x0076 }
            r0 = -713128795(0xffffffffd57e84a5, float:-1.74903536E13)
            X.AnonymousClass07A.A04(r1, r8, r0)     // Catch:{ Exception -> 0x0076 }
            goto L_0x008e
        L_0x0067:
            java.lang.Class<X.0iV> r1 = X.AnonymousClass0iV.class
            monitor-enter(r1)     // Catch:{ Exception -> 0x0076 }
            X.0iV r0 = X.C26661bo.A00     // Catch:{ all -> 0x0073 }
            monitor-exit(r1)     // Catch:{ Exception -> 0x0076 }
            java.util.concurrent.CountDownLatch r0 = r0.A00     // Catch:{ Exception -> 0x0076 }
            r0.countDown()     // Catch:{ Exception -> 0x0076 }
            goto L_0x008e
        L_0x0073:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ Exception -> 0x0076 }
            throw r0     // Catch:{ Exception -> 0x0076 }
        L_0x0076:
            r2 = move-exception
            boolean r0 = r2 instanceof java.io.IOException     // Catch:{ all -> 0x009d }
            if (r0 != 0) goto L_0x008e
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x009d }
            X.0UN r0 = r10.A00     // Catch:{ all -> 0x009d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x009d }
            X.09P r1 = (X.AnonymousClass09P) r1     // Catch:{ all -> 0x009d }
            java.lang.Class r0 = com.facebook.mobileconfig.init.MobileConfigSessionlessInit.A05     // Catch:{ all -> 0x009d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009d }
            r1.CGa(r0, r2)     // Catch:{ all -> 0x009d }
        L_0x008e:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r10.A04
            if (r9 != 0) goto L_0x0093
            r5 = 3
        L_0x0093:
            r0.markerEnd(r3, r6, r5)
            r0 = -1112864118(0xffffffffbdab0a8a, float:-0.083516195)
            X.C000700l.A09(r0, r4)
            return
        L_0x009d:
            r1 = move-exception
            com.facebook.quicklog.QuickPerformanceLogger r0 = r10.A04
            if (r9 != 0) goto L_0x00a3
            r5 = 3
        L_0x00a3:
            r0.markerEnd(r3, r6, r5)
            r0 = -467799906(0xffffffffe41df09e, float:-1.1653908E22)
            X.C000700l.A09(r0, r4)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.mobileconfig.init.MobileConfigSessionlessInit.init():void");
    }
}
