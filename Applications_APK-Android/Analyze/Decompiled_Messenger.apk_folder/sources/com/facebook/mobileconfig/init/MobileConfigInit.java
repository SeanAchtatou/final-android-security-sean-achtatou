package com.facebook.mobileconfig.init;

import X.AnonymousClass00J;
import X.AnonymousClass07A;
import X.AnonymousClass09P;
import X.AnonymousClass0UN;
import X.AnonymousClass0VG;
import X.AnonymousClass0WD;
import X.AnonymousClass0WV;
import X.AnonymousClass0WZ;
import X.AnonymousClass0Wl;
import X.AnonymousClass0XE;
import X.AnonymousClass0XG;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C000700l;
import X.C010708t;
import X.C04310Tq;
import X.C05480Zc;
import X.C06480bZ;
import X.C08830g1;
import X.C25041Yc;
import X.C25051Yd;
import X.C25951af;
import android.content.ComponentName;
import android.content.Context;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.mobileconfig.MobileConfigCxxChangeListener;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.tigon.iface.TigonServiceHolder;
import io.card.payment.BuildConfig;
import java.io.IOException;
import javax.inject.Singleton;

@Singleton
public class MobileConfigInit extends AnonymousClass0Wl {
    public static final Class A05 = MobileConfigInit.class;
    private static volatile MobileConfigInit A06;
    public AnonymousClass0UN A00;
    public final C04310Tq A01;
    public final C04310Tq A02;
    private final C04310Tq A03;
    private final C04310Tq A04;

    public synchronized void A08(ViewerContext viewerContext) {
        int i;
        int i2;
        if (viewerContext != null) {
            String str = viewerContext.mUserId;
            if (A06(this, str)) {
                AnonymousClass0WZ r3 = (AnonymousClass0WZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AHH, this.A00);
                synchronized (r3) {
                    if (AnonymousClass0WZ.A01(str)) {
                        i = 12;
                        i2 = AnonymousClass1Y3.AWK;
                    } else {
                        i = 13;
                        i2 = AnonymousClass1Y3.AOJ;
                    }
                    C25051Yd r1 = (C25051Yd) AnonymousClass1XX.A02(i, i2, r3.A00);
                    if (r1 instanceof C25041Yc) {
                        ((C25041Yc) r1).A08();
                    }
                }
            }
        }
    }

    public String getSimpleName() {
        return "MobileConfigInit";
    }

    public static final MobileConfigInit A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (MobileConfigInit.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new MobileConfigInit(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static boolean A06(MobileConfigInit mobileConfigInit, String str) {
        Context context = (Context) mobileConfigInit.A01.get();
        if (2 == context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, MobileConfigEnableReceiver.class)) || str == null || str.isEmpty() || str.equals("0")) {
            return false;
        }
        return true;
    }

    public void A07() {
        ((C25051Yd) this.A02.get()).Aki(1128489772122303L);
        ((C25051Yd) this.A02.get()).Aki(1127192692064334L);
        ((C25051Yd) this.A02.get()).Aki(1128472592253115L);
        ((C25051Yd) this.A02.get()).Aki(1128481182187709L);
        ((C25051Yd) this.A02.get()).Aki(1128476887220412L);
        C010708t.A01.BFj(4);
        if (Math.random() < 0.5d) {
            ((C25051Yd) this.A02.get()).Aki(1128494067089600L);
        }
    }

    public void A0A(AnonymousClass0WV r2) {
        if (r2.registerConfigChangeListener((C25041Yc) this.A02.get())) {
            r2.registerConfigChangeListener((MobileConfigCxxChangeListener) this.A04.get());
        }
    }

    private MobileConfigInit(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(9, r3);
        this.A04 = AnonymousClass0VG.A00(AnonymousClass1Y3.AAe, r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.AOJ, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.BCt, r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.Af3, r3);
    }

    public static void A01(C25041Yc r3, long j, String str) {
        boolean A032 = AnonymousClass0XG.A03(j);
        AnonymousClass0XE A033 = new AnonymousClass0XE().A03();
        A05(Boolean.valueOf(A032), Boolean.valueOf(r3.Aer(j, A033)), str, A033);
    }

    public static void A02(C25041Yc r7, long j, String str) {
        double A002 = C05480Zc.A00(j);
        AnonymousClass0XE A032 = new AnonymousClass0XE().A03();
        A05(Double.valueOf(A002), Double.valueOf(r7.Akm(j, A032)), str, A032);
    }

    public static void A03(C25041Yc r7, long j, String str) {
        long A012 = C05480Zc.A01(j);
        AnonymousClass0XE A032 = new AnonymousClass0XE().A03();
        A05(Long.valueOf(A012), Long.valueOf(r7.At4(j, A032)), str, A032);
    }

    public static void A04(C25041Yc r3, long j, String str) {
        String A022 = C05480Zc.A02(j);
        AnonymousClass0XE A032 = new AnonymousClass0XE().A03();
        A05(A022, r3.B4J(j, A032), str, A032);
    }

    public static void A05(Object obj, Object obj2, String str, AnonymousClass0XE r4) {
        if (!obj.equals(obj2)) {
            C010708t.A0Q("MobileConfigTranslationTableVerifier", "Failed to verify mobileconfig_verify_tt.%s, expected:%s, actual:%s. actual_src:%s", str, obj, obj2, r4.A00);
        }
    }

    public void A09(AnonymousClass0WV r5) {
        if (!r5.isTigonServiceSet()) {
            r5.setTigonService((TigonServiceHolder) this.A03.get(), true);
        }
        String B4F = ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, this.A00)).B4F(C25951af.A0J, null);
        if (B4F != null) {
            String replace = B4F.replace("facebook.com", BuildConfig.FLAVOR).replace("facebook.com", BuildConfig.FLAVOR);
            if (!replace.isEmpty()) {
                int length = replace.length() - 1;
                if (replace.charAt(length) == '.') {
                    replace = replace.substring(0, length);
                }
            }
            r5.setSandboxURL(replace);
        }
    }

    public void init() {
        boolean z;
        int A032 = C000700l.A03(-1967092383);
        AnonymousClass00J.A01((Context) this.A01.get());
        short s = 3;
        boolean z2 = false;
        try {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, this.A00)).markerStart(13631491);
            C25041Yc r7 = (C25041Yc) this.A02.get();
            r7.A09.syncFetchReason();
            A01(r7, 285108519048572L, "bool1");
            A01(r7, 2306128117732808061L, "bool2");
            A01(r7, 2306128117732873598L, "bool3");
            A01(r7, 285108519245183L, "bool4");
            A03(r7, 566583496214222L, "int1");
            A03(r7, 566583496279759L, "int2");
            A03(r7, 566583496345296L, "int3");
            A02(r7, 1129533449437475L, "double1");
            A02(r7, 1129533449503012L, "double2");
            A02(r7, 1129533449568549L, "double3");
            A04(r7, 848058473120296L, "string1");
            A04(r7, 848058473185833L, "string2");
            A04(r7, 848058473251370L, "string3");
            ViewerContext viewerContext = (ViewerContext) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AfE, this.A00);
            if (viewerContext != null) {
                z = A06(this, viewerContext.mUserId);
            } else {
                z = false;
            }
            if (z) {
                int i = AnonymousClass1Y3.AfE;
                AnonymousClass0UN r9 = this.A00;
                ((AnonymousClass0WZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AHH, r9)).A03(((ViewerContext) AnonymousClass1XX.A02(0, i, r9)).mUserId);
            }
            z2 = r7.A09.isValid();
            if (z2) {
                AnonymousClass07A.A04((C06480bZ) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ACz, this.A00), new C08830g1(this), 977314233);
            }
        } catch (Exception e) {
            if (!(e instanceof IOException)) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Amr, this.A00)).CGa(A05.toString(), e);
            }
        } catch (Throwable th) {
            QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, this.A00);
            if (0 != 0) {
                s = 2;
            }
            quickPerformanceLogger.markerEnd(13631491, s);
            C000700l.A09(-592666325, A032);
            throw th;
        }
        QuickPerformanceLogger quickPerformanceLogger2 = (QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, this.A00);
        if (z2) {
            s = 2;
        }
        quickPerformanceLogger2.markerEnd(13631491, s);
        C000700l.A09(-2050834736, A032);
    }
}
