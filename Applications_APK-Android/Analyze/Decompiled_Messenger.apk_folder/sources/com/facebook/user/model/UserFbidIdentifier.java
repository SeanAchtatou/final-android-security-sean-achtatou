package com.facebook.user.model;

import X.C25701aG;
import android.os.Parcel;
import android.os.Parcelable;

public final class UserFbidIdentifier implements Parcelable, UserIdentifier {
    public static final Parcelable.Creator CREATOR = new C25701aG();
    private final String A00;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof UserFbidIdentifier)) {
                return false;
            }
            String str = this.A00;
            String str2 = ((UserFbidIdentifier) obj).A00;
            if (str == null) {
                return str2 == null;
            }
            if (!str.equals(str2)) {
                return false;
            }
        }
    }

    public String getId() {
        return this.A00;
    }

    public int hashCode() {
        String str = this.A00;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getId());
    }

    public UserFbidIdentifier(Parcel parcel) {
        this.A00 = parcel.readString();
    }

    public UserFbidIdentifier(String str) {
        this.A00 = str;
    }
}
