package com.facebook.user.model;

import X.AnonymousClass168;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.util.TriState;

public final class UserPhoneNumber implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass168();
    public final int A00;
    public final TriState A01;
    public final String A02;
    public final String A03;
    public final String A04;

    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        if (r1.equals(r5.A02) == false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0052
            r2 = 0
            if (r5 == 0) goto L_0x0024
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x0024
            com.facebook.user.model.UserPhoneNumber r5 = (com.facebook.user.model.UserPhoneNumber) r5
            int r1 = r4.A00
            int r0 = r5.A00
            if (r1 != r0) goto L_0x0024
            java.lang.String r1 = r4.A02
            if (r1 == 0) goto L_0x0025
            java.lang.String r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x002a
        L_0x0024:
            return r2
        L_0x0025:
            java.lang.String r0 = r5.A02
            if (r0 == 0) goto L_0x002a
            return r2
        L_0x002a:
            com.facebook.common.util.TriState r1 = r4.A01
            com.facebook.common.util.TriState r0 = r5.A01
            if (r1 != r0) goto L_0x0024
            java.lang.String r1 = r4.A03
            if (r1 == 0) goto L_0x003d
            java.lang.String r0 = r5.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0042
            return r2
        L_0x003d:
            java.lang.String r0 = r5.A03
            if (r0 == 0) goto L_0x0042
            return r2
        L_0x0042:
            java.lang.String r1 = r4.A04
            java.lang.String r0 = r5.A04
            if (r1 == 0) goto L_0x004f
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0052
            return r2
        L_0x004f:
            if (r0 == 0) goto L_0x0052
            return r2
        L_0x0052:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.user.model.UserPhoneNumber.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        String str = this.A03;
        int i4 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i5 = i * 31;
        String str2 = this.A02;
        if (str2 != null) {
            i2 = str2.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        String str3 = this.A04;
        if (str3 != null) {
            i3 = str3.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (((i6 + i3) * 31) + this.A00) * 31;
        TriState triState = this.A01;
        if (triState != null) {
            i4 = triState.hashCode();
        }
        return i7 + i4;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A01.name());
    }

    public UserPhoneNumber(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A04 = parcel.readString();
        this.A03 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A01 = TriState.valueOf(parcel.readString());
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public UserPhoneNumber(java.lang.String r7, java.lang.String r8, java.lang.String r9, int r10) {
        /*
            r6 = this;
            com.facebook.common.util.TriState r5 = com.facebook.common.util.TriState.UNSET
            r0 = r6
            r2 = r8
            r1 = r7
            r4 = r10
            r3 = r9
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.user.model.UserPhoneNumber.<init>(java.lang.String, java.lang.String, java.lang.String, int):void");
    }

    public UserPhoneNumber(String str, String str2, String str3, int i, TriState triState) {
        this.A02 = str;
        this.A04 = str2;
        this.A03 = str3;
        this.A00 = i;
        this.A01 = triState;
    }
}
