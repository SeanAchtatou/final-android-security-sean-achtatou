package com.facebook.user.model;

import X.AnonymousClass08S;
import X.C06850cB;
import X.C25671aD;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.facebook.common.json.AutoGenJsonSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import io.card.payment.BuildConfig;

@AutoGenJsonDeserializer
@AutoGenJsonSerializer
@JsonDeserialize(using = NameDeserializer.class)
@JsonSerialize(using = NameSerializer.class)
public class Name implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C25671aD();
    @JsonIgnore
    private String A00;
    @JsonProperty("displayName")
    public final String displayName;
    @JsonProperty("firstName")
    public final String firstName;
    @JsonProperty("lastName")
    public final String lastName;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Name name = (Name) obj;
            if (!Objects.equal(this.displayName, name.displayName) || !Objects.equal(this.firstName, name.firstName) || !Objects.equal(this.lastName, name.lastName)) {
                return false;
            }
        }
        return true;
    }

    @JsonIgnore
    public String A00() {
        String str = this.displayName;
        if (str == null) {
            return A01();
        }
        return str;
    }

    @JsonIgnore
    public String A01() {
        if (this.A00 == null) {
            String str = this.firstName;
            String str2 = this.lastName;
            if (str != null && str.length() > 0 && str2 != null && str2.length() > 0) {
                str = AnonymousClass08S.A0P(str, " ", str2);
            } else if (str == null || str.length() <= 0) {
                if (str2 == null || str2.length() <= 0) {
                    str = BuildConfig.FLAVOR;
                } else {
                    str = str2;
                }
            }
            this.A00 = str;
        }
        return this.A00;
    }

    @JsonIgnore
    public String A02() {
        String str = this.firstName;
        if (C06850cB.A0B(str)) {
            return A00();
        }
        return str;
    }

    public int hashCode() {
        int i;
        int i2;
        String str = this.firstName;
        int i3 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i4 = i * 31;
        String str2 = this.lastName;
        if (str2 != null) {
            i2 = str2.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 31;
        String str3 = this.displayName;
        if (str3 != null) {
            i3 = str3.hashCode();
        }
        return i5 + i3;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.displayName);
    }

    public String toString() {
        return A01();
    }

    private Name() {
        this.firstName = null;
        this.lastName = null;
        this.displayName = null;
    }

    public Name(Parcel parcel) {
        this.firstName = parcel.readString();
        this.lastName = parcel.readString();
        this.displayName = parcel.readString();
    }

    public Name(String str) {
        this(null, null, str);
    }

    public Name(String str, String str2, String str3) {
        this.firstName = Strings.emptyToNull(str);
        this.lastName = Strings.emptyToNull(str2);
        this.displayName = Strings.emptyToNull(str3);
    }
}
