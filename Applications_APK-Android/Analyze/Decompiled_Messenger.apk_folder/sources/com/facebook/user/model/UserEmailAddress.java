package com.facebook.user.model;

import X.C25681aE;
import android.os.Parcel;
import android.os.Parcelable;

public final class UserEmailAddress implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C25681aE();
    public final String A00;
    private final int A01;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeInt(this.A01);
    }

    public UserEmailAddress(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A01 = parcel.readInt();
    }

    public UserEmailAddress(String str, int i) {
        this(str, i, true);
    }

    public UserEmailAddress(String str, int i, boolean z) {
        this.A00 = z ? str.toLowerCase() : str;
        this.A01 = i;
    }
}
