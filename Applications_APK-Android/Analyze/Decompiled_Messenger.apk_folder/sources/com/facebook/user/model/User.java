package com.facebook.user.model;

import X.AnonymousClass07B;
import X.AnonymousClass0j0;
import X.AnonymousClass1BW;
import X.AnonymousClass80H;
import X.C013509w;
import X.C07220cv;
import X.C07230cz;
import X.C25651aB;
import X.C25661aC;
import X.C25691aF;
import X.C29751gv;
import X.C37891wW;
import X.C40011zy;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.common.util.TriState;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.business.common.calltoaction.model.NestedCallToAction;
import com.facebook.messaging.business.messengerextensions.model.MessengerExtensionProperties;
import com.facebook.messaging.games.model.InstantGameChannel;
import com.facebook.user.profilepic.PicSquare;
import com.facebook.user.profilepic.PicSquareUrlWithSize;
import com.facebook.user.profilepic.ProfilePicUriWithFilePath;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class User implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C25691aF();
    public long A00;
    public ImmutableList A01;
    public String A02;
    public final float A03;
    public final float A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final long A0C;
    public final long A0D;
    public final long A0E;
    public final long A0F;
    public final TriState A0G;
    public final TriState A0H;
    public final C25661aC A0I;
    public final MessengerExtensionProperties A0J;
    public final InstantGameChannel A0K;
    public final Name A0L;
    public final C25651aB A0M;
    public final User A0N;
    public final User A0O;
    public final UserIdentifier A0P;
    public final UserKey A0Q;
    public final WorkUserInfo A0R;
    public final ImmutableList A0S;
    public final ImmutableList A0T;
    public final ImmutableList A0U;
    public final ImmutableList A0V;
    public final ImmutableList A0W;
    public final ImmutableList A0X;
    public final ImmutableList A0Y;
    public final ImmutableList A0Z;
    public final ImmutableList A0a;
    public final ImmutableList A0b;
    public final Integer A0c;
    public final Integer A0d;
    public final Integer A0e;
    public final Integer A0f;
    public final String A0g;
    public final String A0h;
    public final String A0i;
    public final String A0j;
    public final String A0k;
    public final String A0l;
    public final String A0m;
    public final String A0n;
    public final String A0o;
    public final String A0p;
    public final String A0q;
    public final String A0r;
    public final String A0s;
    public final String A0t;
    public final String A0u;
    public final String A0v;
    public final String A0w;
    public final String A0x;
    public final String A0y;
    public final String A0z;
    public final String A10;
    public final boolean A11;
    public final boolean A12;
    public final boolean A13;
    public final boolean A14;
    public final boolean A15;
    public final boolean A16;
    public final boolean A17;
    public final boolean A18;
    public final boolean A19;
    public final boolean A1A;
    public final boolean A1B;
    public final boolean A1C;
    public final boolean A1D;
    public final boolean A1E;
    public final boolean A1F;
    public final boolean A1G;
    public final boolean A1H;
    public final boolean A1I;
    public final boolean A1J;
    public final boolean A1K;
    public final boolean A1L;
    public final boolean A1M;
    public final boolean A1N;
    public final boolean A1O;
    public final boolean A1P;
    public final boolean A1Q;
    public final boolean A1R;
    public final boolean A1S;
    public final boolean A1T;
    public final boolean A1U;
    public final boolean A1V;
    public final boolean A1W;
    public final boolean A1X;
    public final boolean A1Y;
    public final boolean A1Z;
    public final boolean A1a;
    public final boolean A1b;
    public final boolean A1c;
    public final boolean A1d;
    public final boolean A1e;
    public final boolean A1f;
    public final boolean A1g;
    public final boolean A1h;
    private final Name A1i;
    private final Integer A1j;
    private final String A1k;
    public volatile PicSquare A1l;
    public volatile ProfilePicUriWithFilePath A1m;
    public volatile String A1n;

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "FRIENDS";
            case 2:
                return "FOLLOWER";
            case 3:
                return "CONNECTION";
            default:
                return "UNSET";
        }
    }

    public int describeContents() {
        return 0;
    }

    private UserIdentifier A00() {
        C25651aB r1 = this.A0M;
        if (r1 == C25651aB.A03) {
            return new UserFbidIdentifier(this.A0j);
        }
        if (!A02(r1)) {
            return null;
        }
        UserPhoneNumber A032 = A03();
        String A0E2 = A0E();
        if (A032 != null) {
            return new UserSmsIdentifier(A032.A03, A032.A04);
        }
        if (A0E2 != null) {
            return new UserSmsIdentifier(A0E2);
        }
        return null;
    }

    public static boolean A02(C25651aB r2) {
        if (r2 == C25651aB.A01 || r2 == C25651aB.A05 || r2 == C25651aB.A02) {
            return true;
        }
        return false;
    }

    public UserPhoneNumber A03() {
        if (this.A01.isEmpty()) {
            return null;
        }
        return (UserPhoneNumber) this.A01.get(0);
    }

    public PicSquare A04() {
        if (this.A1l == null) {
            synchronized (this) {
                if (this.A1l == null) {
                    PicSquare picSquare = null;
                    if (!TextUtils.isEmpty(this.A1n)) {
                        try {
                            JSONArray jSONArray = new JSONArray(this.A1n);
                            ImmutableList.Builder builder = ImmutableList.builder();
                            int length = jSONArray.length();
                            for (int i = 0; i < length; i++) {
                                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                                builder.add((Object) new PicSquareUrlWithSize(optJSONObject.optInt("profile_pic_size"), optJSONObject.optString("profile_pic_url")));
                            }
                            picSquare = new PicSquare(builder.build());
                        } catch (JSONException unused) {
                        }
                    }
                    this.A1l = picSquare;
                }
            }
        }
        return this.A1l;
    }

    public ImmutableList A05() {
        ImmutableList build;
        TriState triState;
        ImmutableList immutableList = this.A01;
        if (immutableList == null || immutableList.isEmpty()) {
            if (TextUtils.isEmpty(this.A02)) {
                build = null;
            } else {
                try {
                    JSONArray jSONArray = new JSONArray(this.A02);
                    ImmutableList.Builder builder = ImmutableList.builder();
                    int length = jSONArray.length();
                    for (int i = 0; i < length; i++) {
                        JSONObject optJSONObject = jSONArray.optJSONObject(i);
                        String optString = optJSONObject.optString("phone_full_number");
                        String optString2 = optJSONObject.optString("phone_display_number");
                        if (optJSONObject.has("phone_is_verified")) {
                            triState = TriState.valueOf(optJSONObject.optBoolean("phone_is_verified"));
                        } else {
                            triState = TriState.UNSET;
                        }
                        builder.add((Object) new UserPhoneNumber(optString2, optString, optString, optJSONObject.optInt("phone_android_type"), triState));
                    }
                    build = builder.build();
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }
            if (build != null) {
                this.A01 = build;
            }
        }
        if (this.A01 == null) {
            this.A01 = RegularImmutableList.A02;
        }
        return this.A01;
    }

    public String A07() {
        return this.A0Q.A04();
    }

    public String A08() {
        return this.A0L.displayName;
    }

    public String A09() {
        return this.A0L.A00();
    }

    public String A0A() {
        return this.A0L.firstName;
    }

    public String A0B() {
        return this.A0L.lastName;
    }

    public String A0C() {
        String jSONArray;
        if (this.A02 == null) {
            if (this.A01 == null) {
                jSONArray = null;
            } else {
                JSONArray jSONArray2 = new JSONArray();
                int size = this.A01.size();
                int i = 0;
                while (i < size) {
                    UserPhoneNumber userPhoneNumber = (UserPhoneNumber) this.A01.get(i);
                    try {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("phone_full_number", userPhoneNumber.A03);
                        jSONObject.put("phone_display_number", userPhoneNumber.A02);
                        TriState triState = userPhoneNumber.A01;
                        if (triState != TriState.UNSET) {
                            jSONObject.put("phone_is_verified", triState.asBoolean(false));
                        }
                        jSONObject.put("phone_android_type", userPhoneNumber.A00);
                        jSONArray2.put(jSONObject);
                        i++;
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }
                jSONArray = jSONArray2.toString();
            }
            this.A02 = jSONArray;
        }
        return this.A02;
    }

    public String A0D() {
        String str = this.A1k;
        if (str != null) {
            return str;
        }
        if (this.A1l != null) {
            return this.A1l.mPicSquareUrlsWithSizes.get(0).url;
        }
        return null;
    }

    public String A0E() {
        if (this.A0T.isEmpty()) {
            return null;
        }
        return ((UserEmailAddress) this.A0T.get(0)).A00;
    }

    public boolean A0F() {
        if ("page".equals(this.A0x) || this.A0I == C25661aC.PAGE) {
            return true;
        }
        return false;
    }

    public boolean A0G() {
        if (!C013509w.A01(this.A0b)) {
            if (Objects.equal(AnonymousClass80H.$const$string(10), this.A0x) || this.A0I == C25661aC.PARENT_APPROVED_USER) {
                return true;
            }
            return false;
        }
        return true;
    }

    public boolean A0H() {
        return this.A0Q.A08();
    }

    public boolean A0I() {
        C25661aC r2;
        if ("user".equals(this.A0x) || (r2 = this.A0I) == C25661aC.FACEBOOK || r2 == C25661aC.SMS_MESSAGING_PARTICIPANT || r2 == C25661aC.INSTAGRAM) {
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A0L);
        sb.append(" ");
        sb.append(this.A0j);
        sb.append(" [");
        sb.append(this.A0M.name());
        sb.append("] ");
        if (!this.A0T.isEmpty()) {
            sb.append(((UserEmailAddress) this.A0T.get(0)).A00);
            sb.append(" ");
        }
        if (!this.A0b.isEmpty()) {
            sb.append(((ManagingParent) this.A0b.get(0)).mId);
            sb.append(" ");
        }
        if (!this.A01.isEmpty()) {
            sb.append(((UserPhoneNumber) this.A01.get(0)).A04);
            sb.append(" ");
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        String A012;
        String str;
        String A013;
        parcel.writeString(this.A0j);
        parcel.writeString(this.A0M.name());
        parcel.writeString(this.A0w);
        parcel.writeList(this.A0T);
        parcel.writeList(this.A0b);
        parcel.writeList(this.A01);
        parcel.writeParcelable(this.A0L, i);
        parcel.writeParcelable(this.A1i, i);
        parcel.writeString(this.A0s);
        parcel.writeString(C07230cz.A01(this.A08));
        parcel.writeString(this.A1k);
        parcel.writeString(this.A0u);
        parcel.writeString(this.A0h);
        parcel.writeParcelable(this.A1l, i);
        parcel.writeParcelable(this.A1m, i);
        parcel.writeString(this.A0z);
        parcel.writeString(this.A0t);
        parcel.writeFloat(this.A04);
        parcel.writeString(this.A0G.name());
        parcel.writeInt(this.A14 ? 1 : 0);
        parcel.writeInt(this.A1A ? 1 : 0);
        parcel.writeString(this.A10);
        parcel.writeString(this.A0x);
        parcel.writeInt(this.A18 ? 1 : 0);
        parcel.writeLong(this.A0F);
        parcel.writeLong(this.A0C);
        parcel.writeInt(this.A16 ? 1 : 0);
        parcel.writeInt(this.A15 ? 1 : 0);
        parcel.writeInt(this.A17 ? 1 : 0);
        parcel.writeInt(this.A19 ? 1 : 0);
        parcel.writeInt(this.A07);
        parcel.writeInt(this.A06);
        parcel.writeInt(this.A05);
        parcel.writeString(this.A0g);
        parcel.writeString(this.A0v);
        parcel.writeString(this.A0y);
        parcel.writeString(this.A0i);
        parcel.writeInt(this.A1c ? 1 : 0);
        parcel.writeInt(this.A1g ? 1 : 0);
        parcel.writeInt(this.A1b ? 1 : 0);
        parcel.writeInt(this.A0H.getDbValue());
        parcel.writeInt(this.A1J ? 1 : 0);
        parcel.writeInt(this.A12 ? 1 : 0);
        parcel.writeInt(this.A13 ? 1 : 0);
        parcel.writeInt(this.A1L ? 1 : 0);
        parcel.writeInt(this.A1V ? 1 : 0);
        parcel.writeInt(this.A1Z ? 1 : 0);
        Integer num = this.A0c;
        String str2 = null;
        if (num == null) {
            A012 = null;
        } else {
            A012 = AnonymousClass1BW.A01(num);
        }
        parcel.writeString(A012);
        parcel.writeInt(this.A1D ? 1 : 0);
        parcel.writeList(this.A0S);
        parcel.writeLong(this.A00);
        parcel.writeLong(this.A0D);
        parcel.writeInt(this.A1C ? 1 : 0);
        parcel.writeInt(this.A11 ? 1 : 0);
        parcel.writeInt(this.A1Q ? 1 : 0);
        parcel.writeString(this.A02);
        parcel.writeString(this.A1n);
        parcel.writeFloat(this.A03);
        parcel.writeList(this.A0a);
        parcel.writeInt(this.A1Y ? 1 : 0);
        parcel.writeInt(this.A1E ? 1 : 0);
        parcel.writeInt(this.A1X ? 1 : 0);
        parcel.writeInt(this.A1F ? 1 : 0);
        Integer num2 = this.A0d;
        if (num2 != null) {
            str2 = AnonymousClass0j0.A01(num2);
        }
        parcel.writeString(str2);
        parcel.writeInt(this.A1h ? 1 : 0);
        parcel.writeInt(this.A1H ? 1 : 0);
        parcel.writeInt(this.A1G ? 1 : 0);
        parcel.writeInt(this.A1I ? 1 : 0);
        parcel.writeParcelable(this.A0J, i);
        parcel.writeParcelable(this.A0O, i);
        parcel.writeString(C29751gv.A01(this.A0f));
        switch (this.A1j.intValue()) {
            case 1:
                str = "INSTAGRAM";
                break;
            case 2:
                str = "PHONE_NUMBER";
                break;
            default:
                str = "UNSET";
                break;
        }
        parcel.writeString(str);
        parcel.writeParcelable(this.A0N, i);
        parcel.writeInt(this.A1U ? 1 : 0);
        parcel.writeString(this.A0q);
        parcel.writeList(this.A0Z);
        parcel.writeString(this.A0l);
        parcel.writeInt(this.A1S ? 1 : 0);
        parcel.writeInt(this.A1B ? 1 : 0);
        parcel.writeParcelable(this.A0K, i);
        parcel.writeInt(this.A0A);
        parcel.writeString(this.A0p);
        parcel.writeString("tmp_value");
        parcel.writeInt(this.A1K ? 1 : 0);
        parcel.writeList(this.A0U);
        parcel.writeInt(this.A1a ? 1 : 0);
        parcel.writeInt(this.A1W ? 1 : 0);
        parcel.writeList(this.A0V);
        parcel.writeInt(this.A1f ? 1 : 0);
        parcel.writeInt(this.A1N ? 1 : 0);
        parcel.writeList(this.A0X);
        parcel.writeInt(this.A1d ? 1 : 0);
        parcel.writeLong(this.A0E);
        C417826y.A0V(parcel, this.A1O);
        parcel.writeString(this.A0o);
        parcel.writeParcelable(this.A0R, i);
        parcel.writeString(this.A0m);
        parcel.writeList(this.A0W);
        parcel.writeList(this.A0Y);
        C417826y.A0V(parcel, this.A1e);
        parcel.writeString(this.A0n);
        parcel.writeString(this.A0k);
        parcel.writeString(this.A0r);
        Integer num3 = this.A0e;
        if (num3 != null) {
            A013 = A01(num3);
        } else {
            A013 = A01(AnonymousClass07B.A00);
        }
        parcel.writeString(A013);
        C417826y.A0V(parcel, this.A1T);
        parcel.writeInt(this.A0B);
        parcel.writeString(this.A0I.name());
        C417826y.A0V(parcel, this.A1P);
        C417826y.A0V(parcel, this.A1M);
        parcel.writeInt(this.A09);
        C417826y.A0V(parcel, this.A1R);
    }

    public Integer A06() {
        if (!A0F() && this.A1L) {
            return AnonymousClass07B.A0C;
        }
        if (this.A1V) {
            return AnonymousClass07B.A01;
        }
        return AnonymousClass07B.A00;
    }

    public User(C07220cv r5) {
        String str = r5.A0i;
        Preconditions.checkNotNull(str, "id must not be null");
        this.A0j = str;
        C25651aB r2 = r5.A0L;
        Preconditions.checkNotNull(r2, "type must not be null");
        this.A0M = r2;
        this.A0Q = new UserKey(r2, this.A0j);
        this.A0w = r5.A12;
        List list = r5.A16;
        if (list == null) {
            this.A0T = RegularImmutableList.A02;
        } else {
            this.A0T = ImmutableList.copyOf((Collection) list);
        }
        ImmutableList immutableList = r5.A0Z;
        if (immutableList == null) {
            this.A0b = RegularImmutableList.A02;
        } else {
            this.A0b = immutableList;
        }
        List list2 = r5.A17;
        if (list2 == null) {
            this.A01 = RegularImmutableList.A02;
        } else {
            this.A01 = ImmutableList.copyOf((Collection) list2);
        }
        ImmutableList immutableList2 = r5.A0S;
        if (immutableList2 == null) {
            this.A0U = RegularImmutableList.A02;
        } else {
            this.A0U = immutableList2;
        }
        ImmutableList immutableList3 = r5.A0T;
        this.A0V = immutableList3 == null ? RegularImmutableList.A02 : immutableList3;
        Name name = r5.A0J;
        this.A0L = name == null ? new Name(r5.A0h, r5.A0j, r5.A0g) : name;
        this.A1i = r5.A0K;
        this.A0s = r5.A0x;
        this.A08 = r5.A02;
        this.A1k = r5.A11;
        this.A0u = r5.A10;
        this.A0h = r5.A0f;
        this.A1l = r5.A0P;
        this.A1m = r5.A0Q;
        this.A0z = r5.A14;
        this.A0t = r5.A0y;
        this.A04 = r5.A01;
        this.A0G = r5.A0E;
        this.A14 = r5.A1A;
        this.A1A = r5.A1C;
        this.A10 = r5.A15;
        this.A0x = r5.A13;
        this.A18 = r5.A1B;
        this.A12 = r5.A18;
        this.A13 = r5.A19;
        this.A0c = r5.A0a;
        this.A0S = r5.A0R;
        this.A0F = r5.A0B;
        this.A0C = r5.A09;
        this.A16 = r5.A1W;
        this.A15 = r5.A1V;
        this.A17 = r5.A1c;
        this.A19 = r5.A1j;
        this.A0P = A00();
        this.A07 = r5.A06;
        this.A06 = r5.A05;
        this.A05 = r5.A04;
        this.A0g = r5.A0l;
        this.A0y = r5.A0w;
        this.A0v = r5.A0u;
        this.A0i = r5.A0o;
        this.A1c = r5.A1i;
        this.A1g = r5.A1n;
        this.A1b = r5.A1h;
        this.A0H = r5.A0F;
        this.A1J = r5.A1L;
        this.A1L = r5.A1N;
        this.A1V = r5.A1a;
        this.A1Z = r5.A1f;
        this.A1D = r5.A1F;
        this.A00 = r5.A0A;
        this.A0D = r5.A0C;
        this.A1C = r5.A1E;
        this.A11 = r5.A1O;
        this.A1Q = r5.A1T;
        this.A02 = r5.A0z;
        this.A1n = r5.A0v;
        this.A03 = r5.A00;
        this.A0a = r5.A0Y;
        this.A1Y = r5.A1e;
        this.A1E = r5.A1G;
        this.A1X = r5.A1d;
        this.A1F = r5.A1H;
        this.A0d = r5.A0c;
        this.A1h = r5.A1o;
        this.A1H = r5.A1J;
        this.A1G = r5.A1I;
        this.A1I = r5.A1K;
        this.A0J = r5.A0H;
        this.A0O = r5.A0N;
        this.A0f = r5.A0e;
        this.A1j = r5.A0b;
        this.A0N = r5.A0M;
        this.A1U = r5.A1Z;
        this.A0q = r5.A0s;
        this.A0Z = r5.A0X;
        this.A0l = r5.A0m;
        this.A1S = r5.A1X;
        this.A1B = r5.A1D;
        this.A0K = r5.A0I;
        this.A0A = r5.A07;
        this.A0p = r5.A0r;
        this.A1K = r5.A1M;
        this.A1a = r5.A1g;
        this.A1W = r5.A1b;
        this.A1f = r5.A1m;
        this.A1N = r5.A1Q;
        ImmutableList immutableList4 = r5.A0V;
        this.A0X = immutableList4 == null ? RegularImmutableList.A02 : immutableList4;
        this.A1d = r5.A1k;
        this.A0E = r5.A0D;
        this.A1O = r5.A1R;
        this.A0o = r5.A0q;
        this.A0R = r5.A0O;
        this.A0m = r5.A0n;
        this.A0W = r5.A0U;
        this.A0Y = r5.A0W;
        this.A1e = r5.A1l;
        this.A0n = r5.A0p;
        this.A0k = r5.A0k;
        this.A0r = r5.A0t;
        Integer num = r5.A0d;
        this.A0e = num == null ? AnonymousClass07B.A00 : num;
        this.A1T = r5.A1Y;
        this.A0B = r5.A08;
        this.A0I = r5.A0G;
        this.A1P = r5.A1S;
        this.A1M = r5.A1P;
        this.A09 = r5.A03;
        this.A1R = r5.A1U;
    }

    public User(Parcel parcel) {
        Integer num;
        ImmutableList copyOf;
        ImmutableList copyOf2;
        Integer num2;
        Integer num3;
        ImmutableList copyOf3;
        ImmutableList copyOf4;
        ImmutableList copyOf5;
        C25661aC valueOf;
        Class<User> cls = User.class;
        this.A0j = parcel.readString();
        C25651aB valueOf2 = C25651aB.valueOf(parcel.readString());
        this.A0M = valueOf2;
        this.A0Q = new UserKey(valueOf2, this.A0j);
        this.A0w = parcel.readString();
        this.A0T = ImmutableList.copyOf((Collection) parcel.readArrayList(UserEmailAddress.class.getClassLoader()));
        this.A0b = ImmutableList.copyOf((Collection) parcel.readArrayList(ManagingParent.class.getClassLoader()));
        this.A01 = ImmutableList.copyOf((Collection) parcel.readArrayList(UserPhoneNumber.class.getClassLoader()));
        Class<Name> cls2 = Name.class;
        this.A0L = (Name) parcel.readParcelable(cls2.getClassLoader());
        this.A1i = (Name) parcel.readParcelable(cls2.getClassLoader());
        this.A0s = parcel.readString();
        this.A08 = C07230cz.A00(parcel.readString());
        this.A1k = parcel.readString();
        this.A0u = parcel.readString();
        this.A0h = parcel.readString();
        this.A1l = (PicSquare) parcel.readParcelable(PicSquare.class.getClassLoader());
        this.A1m = (ProfilePicUriWithFilePath) parcel.readParcelable(ProfilePicUriWithFilePath.class.getClassLoader());
        this.A0z = parcel.readString();
        this.A0t = parcel.readString();
        this.A04 = parcel.readFloat();
        this.A0G = TriState.valueOf(parcel.readString());
        boolean z = true;
        this.A14 = parcel.readInt() != 0;
        this.A1A = parcel.readInt() != 0;
        this.A10 = parcel.readString();
        this.A0x = parcel.readString();
        this.A18 = parcel.readInt() != 0;
        this.A0F = parcel.readLong();
        this.A0C = parcel.readLong();
        this.A16 = parcel.readInt() != 0;
        this.A15 = parcel.readInt() != 0;
        this.A17 = parcel.readInt() != 0;
        this.A19 = parcel.readInt() != 0;
        this.A07 = parcel.readInt();
        this.A06 = parcel.readInt();
        this.A05 = parcel.readInt();
        this.A0g = parcel.readString();
        this.A0v = parcel.readString();
        this.A0y = parcel.readString();
        this.A0i = parcel.readString();
        this.A0P = A00();
        this.A1c = parcel.readInt() != 0;
        this.A1g = parcel.readInt() != 0;
        this.A1b = parcel.readInt() != 0;
        this.A0H = TriState.fromDbValue(parcel.readInt());
        this.A1J = parcel.readInt() != 0;
        this.A12 = parcel.readInt() != 0;
        this.A13 = parcel.readInt() != 0;
        this.A1L = parcel.readInt() != 0;
        this.A1V = parcel.readInt() != 0;
        this.A1Z = parcel.readInt() != 0;
        String readString = parcel.readString();
        ImmutableList immutableList = null;
        if (readString == null) {
            num = null;
        } else {
            try {
                num = AnonymousClass1BW.A00(readString);
            } catch (IllegalArgumentException unused) {
                num = null;
            }
        }
        this.A0c = num;
        this.A1D = parcel.readInt() != 0;
        ArrayList readArrayList = parcel.readArrayList(C37891wW.class.getClassLoader());
        ImmutableList immutableList2 = null;
        if (readArrayList == null) {
            copyOf = null;
        } else {
            copyOf = ImmutableList.copyOf((Collection) readArrayList);
        }
        this.A0S = copyOf;
        this.A00 = parcel.readLong();
        this.A0D = parcel.readLong();
        this.A1C = parcel.readInt() != 0;
        this.A11 = parcel.readInt() != 0;
        this.A1Q = parcel.readInt() != 0;
        this.A02 = parcel.readString();
        this.A1n = parcel.readString();
        this.A03 = parcel.readFloat();
        ArrayList readArrayList2 = parcel.readArrayList(CallToAction.class.getClassLoader());
        if (readArrayList2 == null) {
            copyOf2 = null;
        } else {
            copyOf2 = ImmutableList.copyOf((Collection) readArrayList2);
        }
        this.A0a = copyOf2;
        this.A1Y = parcel.readInt() != 0;
        this.A1E = parcel.readInt() != 0;
        this.A1X = parcel.readInt() != 0;
        this.A1F = parcel.readInt() != 0;
        try {
            num2 = AnonymousClass0j0.A00(parcel.readString());
        } catch (IllegalArgumentException | NullPointerException unused2) {
            num2 = null;
        }
        this.A0d = num2;
        this.A1h = parcel.readInt() != 0;
        this.A1H = parcel.readInt() != 0;
        this.A1G = parcel.readInt() != 0;
        this.A1I = parcel.readInt() != 0;
        this.A0J = (MessengerExtensionProperties) parcel.readParcelable(MessengerExtensionProperties.class.getClassLoader());
        this.A0O = (User) parcel.readParcelable(cls.getClassLoader());
        this.A0f = C29751gv.A00(parcel.readString());
        String readString2 = parcel.readString();
        if (readString2.equals("UNSET")) {
            num3 = AnonymousClass07B.A00;
        } else if (readString2.equals("INSTAGRAM")) {
            num3 = AnonymousClass07B.A01;
        } else if (readString2.equals("PHONE_NUMBER")) {
            num3 = AnonymousClass07B.A0C;
        } else {
            throw new IllegalArgumentException(readString2);
        }
        this.A1j = num3;
        this.A0N = (User) parcel.readParcelable(cls.getClassLoader());
        this.A1U = parcel.readInt() != 0;
        this.A0q = parcel.readString();
        ArrayList readArrayList3 = parcel.readArrayList(NestedCallToAction.class.getClassLoader());
        if (readArrayList3 == null) {
            copyOf3 = null;
        } else {
            copyOf3 = ImmutableList.copyOf((Collection) readArrayList3);
        }
        this.A0Z = copyOf3;
        this.A0l = parcel.readString();
        this.A1S = parcel.readInt() != 0;
        this.A1B = parcel.readInt() != 0;
        this.A0K = (InstantGameChannel) parcel.readParcelable(InstantGameChannel.class.getClassLoader());
        this.A0A = parcel.readInt();
        this.A0p = parcel.readString();
        parcel.readString();
        this.A1K = parcel.readInt() != 0;
        ArrayList readArrayList4 = parcel.readArrayList(AlohaUser.class.getClassLoader());
        if (readArrayList4 == null) {
            copyOf4 = RegularImmutableList.A02;
        } else {
            copyOf4 = ImmutableList.copyOf((Collection) readArrayList4);
        }
        this.A0U = copyOf4;
        this.A1a = parcel.readInt() != 0;
        this.A1W = parcel.readInt() != 0;
        this.A0V = ImmutableList.copyOf((Collection) parcel.readArrayList(AlohaProxyUser.class.getClassLoader()));
        this.A1f = parcel.readInt() != 0;
        this.A1N = parcel.readInt() != 0;
        Class<String> cls3 = String.class;
        ArrayList readArrayList5 = parcel.readArrayList(cls3.getClassLoader());
        if (readArrayList5 == null) {
            copyOf5 = RegularImmutableList.A02;
        } else {
            copyOf5 = ImmutableList.copyOf((Collection) readArrayList5);
        }
        this.A0X = copyOf5;
        this.A1d = parcel.readInt() == 0 ? false : z;
        this.A0E = parcel.readLong();
        this.A1O = C417826y.A0W(parcel);
        this.A0o = parcel.readString();
        this.A0R = (WorkUserInfo) parcel.readParcelable(WorkUserInfo.class.getClassLoader());
        this.A0m = parcel.readString();
        ArrayList readArrayList6 = parcel.readArrayList(cls3.getClassLoader());
        this.A0W = readArrayList6 != null ? ImmutableList.copyOf((Collection) readArrayList6) : immutableList;
        ArrayList readArrayList7 = parcel.readArrayList(cls3.getClassLoader());
        this.A0Y = readArrayList7 != null ? ImmutableList.copyOf((Collection) readArrayList7) : immutableList2;
        this.A1e = C417826y.A0W(parcel);
        this.A0n = parcel.readString();
        this.A0k = parcel.readString();
        this.A0r = parcel.readString();
        this.A0e = C40011zy.A00(parcel.readString());
        this.A1T = C417826y.A0W(parcel);
        this.A0B = parcel.readInt();
        String readString3 = parcel.readString();
        if (readString3 == null) {
            valueOf = C25661aC.UNSET;
        } else {
            valueOf = C25661aC.valueOf(readString3);
        }
        this.A0I = valueOf;
        this.A1P = C417826y.A0W(parcel);
        this.A1M = C417826y.A0W(parcel);
        this.A09 = parcel.readInt();
        this.A1R = C417826y.A0W(parcel);
    }
}
