package com.facebook.user.model;

import X.AnonymousClass08S;
import X.AnonymousClass16Q;
import X.C07220cv;
import X.C25651aB;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Objects;

@AutoGenJsonDeserializer
@JsonDeserialize(using = UserKeyDeserializer.class)
public class UserKey implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass16Q();
    @JsonIgnore
    private String A00;
    @JsonProperty("id")
    public final String id;
    @JsonProperty("type")
    public final C25651aB type;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                UserKey userKey = (UserKey) obj;
                if (!Objects.equal(this.id, userKey.id) || this.type != userKey.type) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public static UserKey A01(String str) {
        return new UserKey(C25651aB.A03, str);
    }

    public static UserKey A02(String str) {
        if (str == null) {
            return null;
        }
        String[] split = str.split(":", 2);
        if (split.length == 2) {
            return new UserKey(C25651aB.valueOf(split[0]), split[1]);
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Cannot parse user key: ", str));
    }

    public String A03() {
        C25651aB r1 = this.type;
        if (r1 == C25651aB.A02 || r1 == C25651aB.A05) {
            return C07220cv.A01(this.id);
        }
        return null;
    }

    public String A04() {
        C25651aB r1 = this.type;
        if (r1 == C25651aB.A01) {
            return this.id;
        }
        if (r1 == C25651aB.A05 || r1 == C25651aB.A02) {
            return C07220cv.A00(this.id);
        }
        return null;
    }

    public String A05() {
        if (this.type == C25651aB.A02) {
            return C07220cv.A01(this.id);
        }
        return null;
    }

    public String A06() {
        String str;
        if (this.A00 == null && (str = this.id) != null) {
            this.A00 = AnonymousClass08S.A0P(this.type.name(), ":", str);
        }
        return this.A00;
    }

    public String A07() {
        if (this.type == C25651aB.A05) {
            return C07220cv.A01(this.id);
        }
        return null;
    }

    public boolean A08() {
        C25651aB r2 = this.type;
        if (User.A02(r2) || r2 == C25651aB.A02) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode;
        String str = this.id;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        return (hashCode * 31) + this.type.hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.type.name());
        parcel.writeString(this.id);
    }

    public UserKey(C25651aB r1, String str) {
        this.type = r1;
        this.id = str;
    }

    public static UserKey A00(Long l) {
        return A01(Long.toString(l.longValue()));
    }

    public String toString() {
        return A06();
    }
}
