package com.facebook.user.model;

import android.os.Parcelable;

public interface UserIdentifier extends Parcelable {
    String getId();
}
