package com.facebook.user.profilepic;

import X.AnonymousClass07K;
import X.C04300To;
import X.C29731gt;
import X.C29741gu;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

@AutoGenJsonDeserializer
@JsonDeserialize(using = PicSquareDeserializer.class)
public class PicSquare implements Parcelable {
    private static final Comparator A00 = new C29731gt();
    public static final Parcelable.Creator CREATOR = new C29741gu();
    @JsonProperty("picSquareUrls")
    public final ImmutableList<PicSquareUrlWithSize> mPicSquareUrlsWithSizes;

    public int describeContents() {
        return 0;
    }

    public PicSquareUrlWithSize A00(int i) {
        int size = this.mPicSquareUrlsWithSizes.size();
        for (int i2 = 0; i2 < size; i2++) {
            PicSquareUrlWithSize picSquareUrlWithSize = this.mPicSquareUrlsWithSizes.get(i2);
            if (i <= picSquareUrlWithSize.size) {
                return picSquareUrlWithSize;
            }
        }
        return this.mPicSquareUrlsWithSizes.get(size - 1);
    }

    public PicSquareUrlWithSize A01(int i) {
        int size = this.mPicSquareUrlsWithSizes.size();
        int i2 = 0;
        int abs = Math.abs(i - this.mPicSquareUrlsWithSizes.get(0).size);
        for (int i3 = 1; i3 < size; i3++) {
            if (Math.abs(i - this.mPicSquareUrlsWithSizes.get(i3).size) <= abs) {
                i2 = i3;
            }
        }
        return this.mPicSquareUrlsWithSizes.get(i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equal(this.mPicSquareUrlsWithSizes, ((PicSquare) obj).mPicSquareUrlsWithSizes);
    }

    public int hashCode() {
        return AnonymousClass07K.A00(this.mPicSquareUrlsWithSizes);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(PicSquare.class);
        stringHelper.add("mPicSquareUrlsWithSizes", this.mPicSquareUrlsWithSizes);
        return stringHelper.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.mPicSquareUrlsWithSizes);
    }

    private PicSquare() {
        this.mPicSquareUrlsWithSizes = null;
    }

    public PicSquare(Parcel parcel) {
        this.mPicSquareUrlsWithSizes = ImmutableList.copyOf((Collection) parcel.readArrayList(PicSquareUrlWithSize.class.getClassLoader()));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public PicSquare(PicSquareUrlWithSize picSquareUrlWithSize) {
        this(picSquareUrlWithSize, null, null);
        Preconditions.checkNotNull(picSquareUrlWithSize);
    }

    public PicSquare(PicSquareUrlWithSize picSquareUrlWithSize, PicSquareUrlWithSize picSquareUrlWithSize2, PicSquareUrlWithSize picSquareUrlWithSize3) {
        boolean z = false;
        int i = picSquareUrlWithSize != null ? 1 : 0;
        i = picSquareUrlWithSize2 != null ? i + 1 : i;
        i = picSquareUrlWithSize3 != null ? i + 1 : i;
        Preconditions.checkArgument(i != 0 ? true : z);
        if (i != 1) {
            if (i != 2) {
                if (picSquareUrlWithSize.size <= picSquareUrlWithSize2.size) {
                    PicSquareUrlWithSize picSquareUrlWithSize4 = picSquareUrlWithSize2;
                    picSquareUrlWithSize2 = picSquareUrlWithSize;
                    picSquareUrlWithSize = picSquareUrlWithSize4;
                }
                if (picSquareUrlWithSize.size > picSquareUrlWithSize3.size) {
                    PicSquareUrlWithSize picSquareUrlWithSize5 = picSquareUrlWithSize3;
                    picSquareUrlWithSize3 = picSquareUrlWithSize;
                    picSquareUrlWithSize = picSquareUrlWithSize5;
                }
                this.mPicSquareUrlsWithSizes = ImmutableList.of(picSquareUrlWithSize2, picSquareUrlWithSize, picSquareUrlWithSize3);
                return;
            }
            if (picSquareUrlWithSize == null) {
                picSquareUrlWithSize = picSquareUrlWithSize3;
            } else if (picSquareUrlWithSize2 == null) {
                picSquareUrlWithSize2 = picSquareUrlWithSize3;
            }
            if (picSquareUrlWithSize.size < picSquareUrlWithSize2.size) {
                this.mPicSquareUrlsWithSizes = ImmutableList.of(picSquareUrlWithSize, picSquareUrlWithSize2);
            } else {
                this.mPicSquareUrlsWithSizes = ImmutableList.of(picSquareUrlWithSize2, picSquareUrlWithSize);
            }
        } else if (picSquareUrlWithSize != null) {
            this.mPicSquareUrlsWithSizes = ImmutableList.of(picSquareUrlWithSize);
        } else if (picSquareUrlWithSize2 != null) {
            this.mPicSquareUrlsWithSizes = ImmutableList.of(picSquareUrlWithSize2);
        } else {
            this.mPicSquareUrlsWithSizes = ImmutableList.of(picSquareUrlWithSize3);
        }
    }

    public PicSquare(ImmutableList immutableList) {
        Preconditions.checkArgument(!immutableList.isEmpty());
        ArrayList A03 = C04300To.A03(immutableList);
        Collections.sort(A03, A00);
        this.mPicSquareUrlsWithSizes = ImmutableList.copyOf((Collection) A03);
    }
}
