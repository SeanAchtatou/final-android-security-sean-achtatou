package com.facebook.user.profilepic;

import X.C29721gs;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.util.Arrays;

@AutoGenJsonDeserializer
@JsonDeserialize(using = PicSquareUrlWithSizeDeserializer.class)
public class PicSquareUrlWithSize implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C29721gs();
    @JsonProperty("size")
    public final int size;
    @JsonProperty("url")
    public final String url;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            PicSquareUrlWithSize picSquareUrlWithSize = (PicSquareUrlWithSize) obj;
            if (this.size != picSquareUrlWithSize.size || !Objects.equal(this.url, picSquareUrlWithSize.url)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.size), this.url});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.size);
        parcel.writeString(this.url);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("size", this.size);
        stringHelper.add("url", this.url);
        return stringHelper.toString();
    }

    private PicSquareUrlWithSize() {
        this.size = 0;
        this.url = null;
    }

    public PicSquareUrlWithSize(int i, String str) {
        this.size = i;
        Preconditions.checkNotNull(str);
        this.url = str;
    }

    public PicSquareUrlWithSize(Parcel parcel) {
        this.size = parcel.readInt();
        this.url = parcel.readString();
    }
}
