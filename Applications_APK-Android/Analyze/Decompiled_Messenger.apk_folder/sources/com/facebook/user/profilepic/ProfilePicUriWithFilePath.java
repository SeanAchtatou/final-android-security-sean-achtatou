package com.facebook.user.profilepic;

import X.C08090ef;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.facebook.common.json.AutoGenJsonSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
import com.google.common.base.Strings;

@AutoGenJsonDeserializer
@AutoGenJsonSerializer
@JsonDeserialize(using = ProfilePicUriWithFilePathDeserializer.class)
@JsonSerialize(using = ProfilePicUriWithFilePathSerializer.class)
public class ProfilePicUriWithFilePath implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C08090ef();
    @JsonProperty("filePath")
    public final String filePath;
    @JsonProperty("profilePicUri")
    public final String profilePicUri;

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ProfilePicUriWithFilePath profilePicUriWithFilePath = (ProfilePicUriWithFilePath) obj;
            if (!Objects.equal(this.profilePicUri, profilePicUriWithFilePath.profilePicUri) || !Objects.equal(this.filePath, profilePicUriWithFilePath.filePath)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i;
        String str = this.profilePicUri;
        int i2 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        String str2 = this.filePath;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return i3 + i2;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.profilePicUri);
        parcel.writeString(this.filePath);
    }

    public ProfilePicUriWithFilePath(Parcel parcel) {
        this.profilePicUri = parcel.readString();
        this.filePath = parcel.readString();
    }

    public ProfilePicUriWithFilePath(String str, String str2) {
        this.profilePicUri = Strings.emptyToNull(str);
        this.filePath = Strings.emptyToNull(str2);
    }
}
