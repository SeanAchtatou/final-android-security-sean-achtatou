package com.facebook.mqtt.debug;

import X.AnonymousClass067;
import X.AnonymousClass069;
import X.AnonymousClass0TG;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.C99244oi;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
public class MqttStats {
    private static volatile MqttStats A03;
    public long A00;
    public final AnonymousClass069 A01;
    public final Map A02 = AnonymousClass0TG.A04();

    public synchronized void A01(String str, long j, boolean z) {
        C99244oi r3;
        if (str == null) {
            str = "<not-specified>";
        }
        synchronized (this) {
            if (str == null) {
                str = "<not-specified>";
            }
            r3 = (C99244oi) this.A02.get(str);
            if (r3 == null) {
                r3 = new C99244oi(str);
                this.A02.put(str, r3);
            }
        }
        if (z) {
            r3.data.sent += j;
        } else {
            r3.data.recvd += j;
        }
        r3.count++;
    }

    public static final MqttStats A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (MqttStats.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new MqttStats(AnonymousClass067.A03(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private MqttStats(AnonymousClass069 r3) {
        this.A01 = r3;
        this.A00 = r3.now();
    }
}
