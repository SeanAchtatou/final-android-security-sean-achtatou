package com.facebook.workchat.inbox.invites.bridge;

import X.AnonymousClass44H;
import X.C27161ck;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitItem;

public final class WorkChatInviteCoworkersUnitInboxItem extends InboxUnitItem {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44H();

    public WorkChatInviteCoworkersUnitInboxItem(C27161ck r1) {
        super(r1);
    }

    public WorkChatInviteCoworkersUnitInboxItem(Parcel parcel) {
        super(parcel);
    }
}
