package com.facebook.redex.dynamicanalysis;

import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public final class DynamicAnalysisTraceManager {
    public static String A00 = "ColdStart";
    public static HashMap A01 = new HashMap();
    public static Semaphore A02 = new Semaphore(0);
    public static AtomicBoolean A03 = new AtomicBoolean(false);
}
