package com.facebook.redex.dynamicanalysis;

import X.AnonymousClass00M;
import X.AnonymousClass0I1;
import X.C010708t;
import X.C04270Tg;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class DynamicAnalysis {
    public static boolean A00 = true;
    public static int A01 = 0;
    public static int A02 = 0;
    public static int A03 = 0;
    public static String A04 = null;
    public static String A05 = null;
    public static AtomicInteger A06 = new AtomicInteger(0);
    public static boolean A07 = false;
    public static final short[] sBasicBlockStats = new short[0];
    private static short[] sMethodStats = new short[0];
    public static short[][] sMethodStatsArray = new short[0][];
    public static int sNumStaticallyInstrumented = 0;
    public static int sTraceType = 1;

    static {
        new DynamicAnalysis();
    }

    public static void A00(C04270Tg r5) {
        A02 = A06.incrementAndGet();
        A03 = (int) TimeUnit.NANOSECONDS.toMillis(r5.A09);
        A05 = AnonymousClass0I1.A00(r5.A0M);
        List A022 = r5.A02();
        StringBuilder sb = new StringBuilder();
        if (A022 != null && A022.size() % 2 == 0) {
            for (int i = 0; i < A022.size(); i += 2) {
                sb.append((String) A022.get(i));
                sb.append(':');
                int i2 = i + 1;
                sb.append((String) A022.get(i2));
                if (i2 != A022.size() - 1) {
                    sb.append(',');
                }
            }
        }
        String sb2 = sb.toString();
        A04 = sb2;
        C010708t.A0P("DYNA", "Init.COLD_START: %s, %d ms, cut order: %d, extra: \"%s\"", A05, Integer.valueOf(A03), Integer.valueOf(A02), sb2);
    }

    public static void onMethodBeginBasicGated(int i) {
        if (A00) {
            short[] sArr = sMethodStats;
            sArr[i] = (short) (sArr[i] + 1);
            int i2 = i + 1;
            if (sArr[i2] == 0) {
                sArr[i2] = (short) A06.incrementAndGet();
            }
        }
    }

    public DynamicAnalysis() {
        if (!AnonymousClass00M.A00().A05()) {
            A00 = false;
            sNumStaticallyInstrumented = 0;
            int i = 0;
            while (true) {
                short[][] sArr = sMethodStatsArray;
                if (i < sArr.length) {
                    sArr[i] = new short[0];
                    i++;
                } else {
                    return;
                }
            }
        }
    }
}
