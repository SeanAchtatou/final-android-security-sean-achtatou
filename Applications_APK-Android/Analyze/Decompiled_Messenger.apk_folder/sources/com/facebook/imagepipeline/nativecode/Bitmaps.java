package com.facebook.imagepipeline.nativecode;

import X.AnonymousClass01u;
import X.C05520Zg;
import android.graphics.Bitmap;

public class Bitmaps {
    private static native void nativeCopyBitmap(Bitmap bitmap, int i, Bitmap bitmap2, int i2, int i3);

    static {
        AnonymousClass01u.A01("imagepipeline");
    }

    public static void copyBitmap(Bitmap bitmap, Bitmap bitmap2) {
        boolean z = true;
        boolean z2 = false;
        if (bitmap2.getConfig() == bitmap.getConfig()) {
            z2 = true;
        }
        C05520Zg.A04(z2);
        C05520Zg.A04(bitmap.isMutable());
        boolean z3 = false;
        if (bitmap.getWidth() == bitmap2.getWidth()) {
            z3 = true;
        }
        C05520Zg.A04(z3);
        if (bitmap.getHeight() != bitmap2.getHeight()) {
            z = false;
        }
        C05520Zg.A04(z);
        nativeCopyBitmap(bitmap, bitmap.getRowBytes(), bitmap2, bitmap2.getRowBytes(), bitmap.getHeight());
    }
}
