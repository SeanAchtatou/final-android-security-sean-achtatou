package com.facebook.imagepipeline.nativecode;

import X.AnonymousClass1O3;
import X.AnonymousClass1SI;
import X.AnonymousClass1T5;
import X.C23351Pe;

public class NativeJpegTranscoderFactory implements C23351Pe {
    private final int A00;
    private final boolean A01;

    public AnonymousClass1T5 createImageTranscoder(AnonymousClass1O3 r4, boolean z) {
        if (r4 != AnonymousClass1SI.A06) {
            return null;
        }
        return new NativeJpegTranscoder(z, this.A00, this.A01);
    }

    public NativeJpegTranscoderFactory(int i, boolean z) {
        this.A00 = i;
        this.A01 = z;
    }
}
