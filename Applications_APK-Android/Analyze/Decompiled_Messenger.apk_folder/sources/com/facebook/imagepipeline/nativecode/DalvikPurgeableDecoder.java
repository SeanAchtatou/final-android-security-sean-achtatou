package com.facebook.imagepipeline.nativecode;

import X.AnonymousClass01u;
import X.AnonymousClass1NV;
import X.AnonymousClass1NW;
import X.AnonymousClass1NX;
import X.AnonymousClass1NY;
import X.AnonymousClass1PS;
import X.AnonymousClass1SJ;
import X.AnonymousClass1SS;
import X.AnonymousClass95E;
import X.C05520Zg;
import X.C30591iK;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.os.Build;
import java.util.Locale;

public abstract class DalvikPurgeableDecoder implements C30591iK {
    public static final byte[] EOI = {-1, -39};
    private final AnonymousClass1NW mUnpooledBitmapsCounter;

    private static native void nativePinBitmap(Bitmap bitmap);

    public abstract Bitmap decodeByteArrayAsPurgeable(AnonymousClass1PS r1, BitmapFactory.Options options);

    public abstract Bitmap decodeJPEGByteArrayAsPurgeable(AnonymousClass1PS r1, int i, BitmapFactory.Options options);

    public AnonymousClass1PS decodeJPEGFromEncodedImage(AnonymousClass1NY r7, Bitmap.Config config, Rect rect, int i) {
        return decodeJPEGFromEncodedImageWithColorSpace(r7, config, rect, i, null);
    }

    static {
        AnonymousClass01u.A01("imagepipeline");
    }

    public static void A00(BitmapFactory.Options options, ColorSpace colorSpace) {
        if (colorSpace == null) {
            colorSpace = ColorSpace.get(ColorSpace.Named.SRGB);
        }
        options.inPreferredColorSpace = colorSpace;
    }

    public static BitmapFactory.Options getBitmapFactoryOptions(int i, Bitmap.Config config) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = true;
        options.inPreferredConfig = config;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inSampleSize = i;
        if (Build.VERSION.SDK_INT >= 11) {
            options.inMutable = true;
        }
        return options;
    }

    public AnonymousClass1PS decodeFromEncodedImageWithColorSpace(AnonymousClass1NY r4, Bitmap.Config config, Rect rect, ColorSpace colorSpace) {
        BitmapFactory.Options bitmapFactoryOptions = getBitmapFactoryOptions(r4.A03, config);
        if (Build.VERSION.SDK_INT >= 26) {
            A00(bitmapFactoryOptions, colorSpace);
        }
        AnonymousClass1PS A00 = AnonymousClass1PS.A00(r4.A0A);
        C05520Zg.A02(A00);
        try {
            return pinBitmap(decodeByteArrayAsPurgeable(A00, bitmapFactoryOptions));
        } finally {
            AnonymousClass1PS.A05(A00);
        }
    }

    public AnonymousClass1PS decodeJPEGFromEncodedImageWithColorSpace(AnonymousClass1NY r4, Bitmap.Config config, Rect rect, int i, ColorSpace colorSpace) {
        BitmapFactory.Options bitmapFactoryOptions = getBitmapFactoryOptions(r4.A03, config);
        if (Build.VERSION.SDK_INT >= 26) {
            A00(bitmapFactoryOptions, colorSpace);
        }
        AnonymousClass1PS A00 = AnonymousClass1PS.A00(r4.A0A);
        C05520Zg.A02(A00);
        try {
            return pinBitmap(decodeJPEGByteArrayAsPurgeable(A00, i, bitmapFactoryOptions));
        } finally {
            AnonymousClass1PS.A05(A00);
        }
    }

    public DalvikPurgeableDecoder() {
        if (AnonymousClass1NV.A02 == null) {
            synchronized (AnonymousClass1NV.class) {
                if (AnonymousClass1NV.A02 == null) {
                    AnonymousClass1NV.A02 = new AnonymousClass1NW(AnonymousClass1NV.A00, AnonymousClass1NV.A01);
                }
            }
        }
        this.mUnpooledBitmapsCounter = AnonymousClass1NV.A02;
    }

    public static boolean endsWithEOI(AnonymousClass1PS r3, int i) {
        AnonymousClass1SS r32 = (AnonymousClass1SS) r3.A0A();
        if (i >= 2 && r32.read(i - 2) == -1 && r32.read(i - 1) == -39) {
            return true;
        }
        return false;
    }

    public AnonymousClass1PS pinBitmap(Bitmap bitmap) {
        boolean z;
        int i;
        long j;
        int i2;
        int i3;
        C05520Zg.A02(bitmap);
        try {
            nativePinBitmap(bitmap);
            AnonymousClass1NW r6 = this.mUnpooledBitmapsCounter;
            synchronized (r6) {
                int A01 = AnonymousClass1SJ.A01(bitmap);
                int i4 = r6.A00;
                if (i4 < r6.A02) {
                    long j2 = r6.A01 + ((long) A01);
                    if (j2 <= ((long) r6.A03)) {
                        r6.A00 = i4 + 1;
                        r6.A01 = j2;
                        z = true;
                    }
                }
                z = false;
            }
            if (z) {
                return AnonymousClass1PS.A02(bitmap, this.mUnpooledBitmapsCounter.A04);
            }
            int A012 = AnonymousClass1SJ.A01(bitmap);
            bitmap.recycle();
            Locale locale = Locale.US;
            Integer valueOf = Integer.valueOf(A012);
            AnonymousClass1NW r1 = this.mUnpooledBitmapsCounter;
            synchronized (r1) {
                i = r1.A00;
            }
            Integer valueOf2 = Integer.valueOf(i);
            AnonymousClass1NW r2 = this.mUnpooledBitmapsCounter;
            synchronized (r2) {
                j = r2.A01;
            }
            Long valueOf3 = Long.valueOf(j);
            AnonymousClass1NW r12 = this.mUnpooledBitmapsCounter;
            synchronized (r12) {
                i2 = r12.A02;
            }
            Integer valueOf4 = Integer.valueOf(i2);
            AnonymousClass1NW r13 = this.mUnpooledBitmapsCounter;
            synchronized (r13) {
                i3 = r13.A03;
            }
            throw new AnonymousClass1NX(String.format(locale, "Attempted to pin a bitmap of size %d bytes. The current pool count is %d, the current pool size is %d bytes. The current pool max count is %d, the current pool max size is %d bytes.", valueOf, valueOf2, valueOf3, valueOf4, Integer.valueOf(i3)));
        } catch (Exception e) {
            bitmap.recycle();
            throw AnonymousClass95E.A00(e);
        }
    }
}
