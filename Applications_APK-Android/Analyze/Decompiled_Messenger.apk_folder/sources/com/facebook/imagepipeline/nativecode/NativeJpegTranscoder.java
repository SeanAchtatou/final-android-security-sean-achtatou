package com.facebook.imagepipeline.nativecode;

import X.AnonymousClass1NY;
import X.AnonymousClass1O3;
import X.AnonymousClass1Q1;
import X.AnonymousClass1SI;
import X.AnonymousClass1T5;
import X.AnonymousClass1T6;
import X.AnonymousClass1T7;
import X.AnonymousClass36w;
import java.io.InputStream;
import java.io.OutputStream;

public class NativeJpegTranscoder implements AnonymousClass1T5 {
    private int mMaxBitmapSize;
    private boolean mResizingEnabled;
    private boolean mUseDownsamplingRatio;

    private static native void nativeTranscodeJpeg(InputStream inputStream, OutputStream outputStream, int i, int i2, int i3);

    private static native void nativeTranscodeJpegWithExifOrientation(InputStream inputStream, OutputStream outputStream, int i, int i2, int i3);

    public String getIdentifier() {
        return "NativeJpegTranscoder";
    }

    public boolean canResize(AnonymousClass1NY r4, AnonymousClass1Q1 r5, AnonymousClass36w r6) {
        if (r5 == null) {
            r5 = AnonymousClass1Q1.A02;
        }
        if (AnonymousClass1T7.A00(r5, r6, r4, this.mResizingEnabled) < 8) {
            return true;
        }
        return false;
    }

    public boolean canTranscode(AnonymousClass1O3 r3) {
        if (r3 == AnonymousClass1SI.A06) {
            return true;
        }
        return false;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00bd, code lost:
        if ((r6 % 90) != 0) goto L_0x00bf;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C80293sC transcode(X.AnonymousClass1NY r10, java.io.OutputStream r11, X.AnonymousClass1Q1 r12, X.AnonymousClass36w r13, X.AnonymousClass1O3 r14, java.lang.Integer r15) {
        /*
            r9 = this;
            if (r15 != 0) goto L_0x0008
            r0 = 85
            java.lang.Integer r15 = java.lang.Integer.valueOf(r0)
        L_0x0008:
            if (r12 != 0) goto L_0x000c
            X.1Q1 r12 = X.AnonymousClass1Q1.A02
        L_0x000c:
            int r0 = r9.mMaxBitmapSize
            int r2 = X.C52932jv.A00(r12, r13, r10, r0)
            r3 = 0
            boolean r0 = r9.mResizingEnabled     // Catch:{ all -> 0x00e5 }
            int r4 = X.AnonymousClass1T7.A00(r12, r13, r10, r0)     // Catch:{ all -> 0x00e5 }
            r1 = 8
            int r1 = r1 / r2
            r0 = 1
            int r1 = java.lang.Math.max(r0, r1)     // Catch:{ all -> 0x00e5 }
            boolean r0 = r9.mUseDownsamplingRatio     // Catch:{ all -> 0x00e5 }
            if (r0 == 0) goto L_0x0026
            r4 = r1
        L_0x0026:
            java.io.InputStream r3 = r10.A09()     // Catch:{ all -> 0x00e5 }
            X.0yS r1 = X.AnonymousClass1T7.A00     // Catch:{ all -> 0x00e5 }
            X.AnonymousClass1NY.A05(r10)     // Catch:{ all -> 0x00e5 }
            int r0 = r10.A00     // Catch:{ all -> 0x00e5 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00e5 }
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x00e5 }
            if (r0 == 0) goto L_0x0087
            int r6 = X.AnonymousClass1T7.A01(r12, r10)     // Catch:{ all -> 0x00e5 }
            int r7 = r15.intValue()     // Catch:{ all -> 0x00e5 }
            X.AnonymousClass1T6.A00()     // Catch:{ all -> 0x00e5 }
            r5 = 0
            r8 = 1
            r0 = 0
            if (r4 < r8) goto L_0x004c
            r0 = 1
        L_0x004c:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r1 = 16
            r0 = 0
            if (r4 > r1) goto L_0x0055
            r0 = 1
        L_0x0055:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r0 = 0
            if (r7 < 0) goto L_0x005c
            r0 = 1
        L_0x005c:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r1 = 100
            r0 = 0
            if (r7 > r1) goto L_0x0065
            r0 = 1
        L_0x0065:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            switch(r6) {
                case 1: goto L_0x006d;
                case 2: goto L_0x006d;
                case 3: goto L_0x006d;
                case 4: goto L_0x006d;
                case 5: goto L_0x006d;
                case 6: goto L_0x006d;
                case 7: goto L_0x006d;
                case 8: goto L_0x006d;
                default: goto L_0x006b;
            }     // Catch:{ all -> 0x00e5 }
        L_0x006b:
            r0 = 0
            goto L_0x006e
        L_0x006d:
            r0 = 1
        L_0x006e:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r0 = 8
            if (r4 != r0) goto L_0x0077
            if (r6 == r8) goto L_0x0078
        L_0x0077:
            r5 = 1
        L_0x0078:
            java.lang.String r0 = "no transformation requested"
            X.C05520Zg.A06(r5, r0)     // Catch:{ all -> 0x00e5 }
            X.C05520Zg.A02(r3)     // Catch:{ all -> 0x00e5 }
            X.C05520Zg.A02(r11)     // Catch:{ all -> 0x00e5 }
            nativeTranscodeJpegWithExifOrientation(r3, r11, r6, r4, r7)     // Catch:{ all -> 0x00e5 }
            goto L_0x00d8
        L_0x0087:
            int r6 = X.AnonymousClass1T7.A02(r12, r10)     // Catch:{ all -> 0x00e5 }
            int r7 = r15.intValue()     // Catch:{ all -> 0x00e5 }
            X.AnonymousClass1T6.A00()     // Catch:{ all -> 0x00e5 }
            r5 = 0
            r1 = 1
            r0 = 0
            if (r4 < r1) goto L_0x0098
            r0 = 1
        L_0x0098:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r1 = 16
            r0 = 0
            if (r4 > r1) goto L_0x00a1
            r0 = 1
        L_0x00a1:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r0 = 0
            if (r7 < 0) goto L_0x00a8
            r0 = 1
        L_0x00a8:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r1 = 100
            r0 = 0
            if (r7 > r1) goto L_0x00b1
            r0 = 1
        L_0x00b1:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            if (r6 < 0) goto L_0x00bf
            r0 = 270(0x10e, float:3.78E-43)
            if (r6 > r0) goto L_0x00bf
            int r1 = r6 % 90
            r0 = 1
            if (r1 == 0) goto L_0x00c0
        L_0x00bf:
            r0 = 0
        L_0x00c0:
            X.C05520Zg.A04(r0)     // Catch:{ all -> 0x00e5 }
            r0 = 8
            if (r4 != r0) goto L_0x00c9
            if (r6 == 0) goto L_0x00ca
        L_0x00c9:
            r5 = 1
        L_0x00ca:
            java.lang.String r0 = "no transformation requested"
            X.C05520Zg.A06(r5, r0)     // Catch:{ all -> 0x00e5 }
            X.C05520Zg.A02(r3)     // Catch:{ all -> 0x00e5 }
            X.C05520Zg.A02(r11)     // Catch:{ all -> 0x00e5 }
            nativeTranscodeJpeg(r3, r11, r6, r4, r7)     // Catch:{ all -> 0x00e5 }
        L_0x00d8:
            X.AnonymousClass2R2.A01(r3)
            X.3sC r1 = new X.3sC
            r0 = 1
            if (r2 == r0) goto L_0x00e1
            r0 = 0
        L_0x00e1:
            r1.<init>(r0)
            return r1
        L_0x00e5:
            r0 = move-exception
            X.AnonymousClass2R2.A01(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.nativecode.NativeJpegTranscoder.transcode(X.1NY, java.io.OutputStream, X.1Q1, X.36w, X.1O3, java.lang.Integer):X.3sC");
    }

    public NativeJpegTranscoder(boolean z, int i, boolean z2) {
        this.mResizingEnabled = z;
        this.mMaxBitmapSize = i;
        this.mUseDownsamplingRatio = z2;
    }

    static {
        AnonymousClass1T6.A00();
    }
}
