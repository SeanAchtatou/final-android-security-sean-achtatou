package com.facebook.imagepipeline.memory;

import X.AnonymousClass01u;
import X.AnonymousClass08S;
import X.AnonymousClass1NZ;
import X.AnonymousClass1SB;
import X.AnonymousClass80H;
import X.C000700l;
import X.C05520Zg;
import android.util.Log;
import java.io.Closeable;
import java.nio.ByteBuffer;

public class NativeMemoryChunk implements Closeable, AnonymousClass1NZ {
    private boolean mIsClosed;
    private final long mNativePtr;
    private final int mSize;

    private static native long nativeAllocate(int i);

    private static native void nativeCopyFromByteArray(long j, byte[] bArr, int i, int i2);

    private static native void nativeCopyToByteArray(long j, byte[] bArr, int i, int i2);

    private static native void nativeFree(long j);

    private static native void nativeMemcpy(long j, long j2, int i);

    private static native byte nativeReadByte(long j);

    public synchronized void close() {
        if (!this.mIsClosed) {
            this.mIsClosed = true;
            nativeFree(this.mNativePtr);
        }
    }

    public ByteBuffer getByteBuffer() {
        return null;
    }

    public synchronized boolean isClosed() {
        return this.mIsClosed;
    }

    public synchronized int write(int i, byte[] bArr, int i2, int i3) {
        int min;
        C05520Zg.A02(bArr);
        boolean z = false;
        if (!isClosed()) {
            z = true;
        }
        C05520Zg.A05(z);
        int i4 = this.mSize;
        min = Math.min(Math.max(0, i4 - i), i3);
        AnonymousClass1SB.A00(i, bArr.length, i2, min, i4);
        nativeCopyFromByteArray(this.mNativePtr + ((long) i), bArr, i2, min);
        return min;
    }

    static {
        AnonymousClass01u.A01("imagepipeline");
    }

    private void doCopy(int i, AnonymousClass1NZ r8, int i2, int i3) {
        if (r8 instanceof NativeMemoryChunk) {
            C05520Zg.A05(!isClosed());
            C05520Zg.A05(!r8.isClosed());
            AnonymousClass1SB.A00(i, r8.getSize(), i2, i3, this.mSize);
            nativeMemcpy(r8.getNativePtr() + ((long) i2), this.mNativePtr + ((long) i), i3);
            return;
        }
        throw new IllegalArgumentException(AnonymousClass80H.$const$string(232));
    }

    public long getNativePtr() {
        return this.mNativePtr;
    }

    public int getSize() {
        return this.mSize;
    }

    public long getUniqueId() {
        return this.mNativePtr;
    }

    public void copy(int i, AnonymousClass1NZ r10, int i2, int i3) {
        C05520Zg.A02(r10);
        if (r10.getUniqueId() == getUniqueId()) {
            Log.w("NativeMemoryChunk", AnonymousClass08S.A0U("Copying from NativeMemoryChunk ", Integer.toHexString(System.identityHashCode(this)), " to NativeMemoryChunk ", Integer.toHexString(System.identityHashCode(r10)), " which share the same address ", Long.toHexString(this.mNativePtr)));
            C05520Zg.A04(false);
        }
        if (r10.getUniqueId() < getUniqueId()) {
            synchronized (r10) {
                try {
                    synchronized (this) {
                        doCopy(i, r10, i2, i3);
                    }
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
            return;
        }
        synchronized (this) {
            try {
                synchronized (r10) {
                    doCopy(i, r10, i2, i3);
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public void finalize() {
        int A03 = C000700l.A03(-2103824331);
        if (isClosed()) {
            C000700l.A09(357646775, A03);
            return;
        }
        Log.w("NativeMemoryChunk", AnonymousClass08S.A0P("finalize: Chunk ", Integer.toHexString(System.identityHashCode(this)), " still active. "));
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(1541628182, A03);
        }
    }

    public NativeMemoryChunk() {
        this.mSize = 0;
        this.mNativePtr = 0;
        this.mIsClosed = true;
    }

    public NativeMemoryChunk(int i) {
        C05520Zg.A04(i > 0);
        this.mSize = i;
        this.mNativePtr = nativeAllocate(i);
        this.mIsClosed = false;
    }

    public synchronized byte read(int i) {
        boolean z = true;
        boolean z2 = false;
        if (!isClosed()) {
            z2 = true;
        }
        C05520Zg.A05(z2);
        boolean z3 = false;
        if (i >= 0) {
            z3 = true;
        }
        C05520Zg.A04(z3);
        if (i >= this.mSize) {
            z = false;
        }
        C05520Zg.A04(z);
        return nativeReadByte(this.mNativePtr + ((long) i));
    }

    public synchronized int read(int i, byte[] bArr, int i2, int i3) {
        int min;
        C05520Zg.A02(bArr);
        boolean z = false;
        if (!isClosed()) {
            z = true;
        }
        C05520Zg.A05(z);
        int i4 = this.mSize;
        min = Math.min(Math.max(0, i4 - i), i3);
        AnonymousClass1SB.A00(i, bArr.length, i2, min, i4);
        nativeCopyToByteArray(this.mNativePtr + ((long) i), bArr, i2, min);
        return min;
    }
}
