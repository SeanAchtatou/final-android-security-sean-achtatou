package com.facebook.imagepipeline.platform;

import X.AnonymousClass1PS;
import X.AnonymousClass1SS;
import X.C05520Zg;
import X.C22841Na;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder;

public class KitKatPurgeableDecoder extends DalvikPurgeableDecoder {
    private final C22841Na A00;

    public KitKatPurgeableDecoder(C22841Na r1) {
        this.A00 = r1;
    }

    public Bitmap decodeByteArrayAsPurgeable(AnonymousClass1PS r6, BitmapFactory.Options options) {
        AnonymousClass1SS r4 = (AnonymousClass1SS) r6.A0A();
        int size = r4.size();
        C22841Na r2 = this.A00;
        AnonymousClass1PS A02 = AnonymousClass1PS.A02(r2.A01.get(size), r2.A00);
        try {
            byte[] bArr = (byte[]) A02.A0A();
            r4.read(0, bArr, 0, size);
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, size, options);
            C05520Zg.A03(decodeByteArray, "BitmapFactory returned null");
            return decodeByteArray;
        } finally {
            AnonymousClass1PS.A05(A02);
        }
    }

    public Bitmap decodeJPEGByteArrayAsPurgeable(AnonymousClass1PS r9, int i, BitmapFactory.Options options) {
        byte[] bArr;
        if (DalvikPurgeableDecoder.endsWithEOI(r9, i)) {
            bArr = null;
        } else {
            bArr = DalvikPurgeableDecoder.EOI;
        }
        AnonymousClass1SS r6 = (AnonymousClass1SS) r9.A0A();
        boolean z = false;
        if (i <= r6.size()) {
            z = true;
        }
        C05520Zg.A04(z);
        C22841Na r2 = this.A00;
        int i2 = i + 2;
        AnonymousClass1PS A02 = AnonymousClass1PS.A02(r2.A01.get(i2), r2.A00);
        try {
            byte[] bArr2 = (byte[]) A02.A0A();
            r6.read(0, bArr2, 0, i);
            if (bArr != null) {
                bArr2[i] = -1;
                bArr2[i + 1] = -39;
                i = i2;
            }
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr2, 0, i, options);
            C05520Zg.A03(decodeByteArray, "BitmapFactory returned null");
            return decodeByteArray;
        } finally {
            AnonymousClass1PS.A05(A02);
        }
    }
}
