package com.facebook.livefeed.service.common;

import X.AnonymousClass0UN;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;

public class LiveFeedServiceFactory {
    private AnonymousClass0UN $ul_mInjectionContext;

    public static final LiveFeedServiceFactory $ul_$xXXcom_facebook_livefeed_service_common_LiveFeedServiceFactory$xXXFACTORY_METHOD(AnonymousClass1XY r1) {
        return new LiveFeedServiceFactory(r1);
    }

    public LiveFeedService build() {
        return (LiveFeedService) AnonymousClass1XX.A03(AnonymousClass1Y3.Aw3, this.$ul_mInjectionContext);
    }

    public LiveFeedServiceFactory(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(0, r3);
    }
}
