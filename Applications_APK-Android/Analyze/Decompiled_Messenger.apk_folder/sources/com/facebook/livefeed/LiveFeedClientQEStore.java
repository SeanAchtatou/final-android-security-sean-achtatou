package com.facebook.livefeed;

import X.AnonymousClass0UN;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C05540Zi;
import X.C25051Yd;
import com.facebook.auth.userscope.UserScoped;

@UserScoped
public class LiveFeedClientQEStore {
    private static C05540Zi $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXINSTANCE;
    private AnonymousClass0UN $ul_mInjectionContext;

    public static final LiveFeedClientQEStore $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXFACTORY_METHOD(AnonymousClass1XY r4) {
        LiveFeedClientQEStore liveFeedClientQEStore;
        synchronized (LiveFeedClientQEStore.class) {
            C05540Zi A00 = C05540Zi.A00($ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXINSTANCE);
            $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXINSTANCE = A00;
            try {
                if (A00.A03(r4)) {
                    $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXINSTANCE.A00 = new LiveFeedClientQEStore((AnonymousClass1XY) $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXINSTANCE.A01());
                }
                C05540Zi r1 = $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXINSTANCE;
                liveFeedClientQEStore = (LiveFeedClientQEStore) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXINSTANCE.A02();
                throw th;
            }
        }
        return liveFeedClientQEStore;
    }

    public long getKeepAliveInterval() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).At0(565479689946482L);
    }

    public String getLiveFeedUrl() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).B4C(846954666131829L);
    }

    public boolean isLiveFeedEnabled() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004712648419L);
    }

    public boolean isLiveFeedFetchEnabled() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004713172714L);
    }

    public boolean shouldReconnectOnDisconnect() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004713303787L);
    }

    public boolean shouldReconnectOnForeground() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004712976103L);
    }

    public boolean shouldSendKeepAliveAfterFirstSignal() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004713041640L);
    }

    public boolean shouldUseBidi() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004712582882L);
    }

    public boolean shouldUseGatewayStaging() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004712451808L);
    }

    public boolean shouldUseHyperThrift() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004713434861L);
    }

    public boolean shouldUseLiveFeedServer() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284004712517345L);
    }

    public LiveFeedClientQEStore(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(1, r3);
    }
}
