package com.facebook.runtimepermissions;

import X.AnonymousClass0UN;
import X.AnonymousClass3At;
import X.AnonymousClass7x4;
import X.AnonymousClass7x5;
import X.C04230Sz;
import X.C12570pc;
import X.C12960qH;
import X.C13500rX;
import X.C27622Dg5;
import X.C27623Dg6;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.resources.ui.FbTextView;
import io.card.payment.BuildConfig;
import java.util.HashSet;

public class RuntimePermissionsNeverAskAgainDialogFragment extends C12570pc {
    public Activity A00;
    public AnonymousClass3At A01;
    public AnonymousClass0UN A02;
    public C12960qH A03;
    public C27623Dg6 A04;
    public RequestPermissionsConfig A05;
    public C27622Dg5 A06;
    public Integer A07;
    public Integer A08;
    public Integer A09;
    public Integer A0A;
    public Integer A0B;
    public String A0C;
    public String[] A0D;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void A00(LinearLayout linearLayout, CharSequence charSequence) {
        FbTextView fbTextView = (FbTextView) this.A00.getLayoutInflater().inflate(2132412050, (ViewGroup) linearLayout, false);
        fbTextView.setText(charSequence);
        linearLayout.addView(fbTextView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public Dialog A20(Bundle bundle) {
        String str;
        Activity activity;
        int A022;
        String str2;
        CharSequence string;
        String[] strArr;
        String str3;
        C13500rX r10 = new C13500rX(A1j());
        RequestPermissionsConfig requestPermissionsConfig = this.A05;
        if (requestPermissionsConfig == null) {
            str = null;
        } else {
            str = requestPermissionsConfig.A01;
        }
        AnonymousClass7x5 r8 = new AnonymousClass7x5(this);
        View inflate = this.A00.getLayoutInflater().inflate(2132412051, (ViewGroup) this.A00.getWindow().getDecorView(), false);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(2131300327);
        FbTextView fbTextView = (FbTextView) this.A00.getLayoutInflater().inflate(2132412052, (ViewGroup) linearLayout, false);
        RequestPermissionsConfig requestPermissionsConfig2 = this.A05;
        if (requestPermissionsConfig2 == null || (str3 = requestPermissionsConfig2.A02) == null) {
            if (this.A04.A04(this.A0D).size() > 1) {
                activity = this.A00;
                A022 = this.A09.intValue();
            } else {
                activity = this.A00;
                A022 = this.A04.A02(this.A0D[0]);
            }
            fbTextView.setText(activity.getString(A022, new Object[]{this.A0C}));
        } else {
            fbTextView.setText(str3);
        }
        linearLayout.addView(fbTextView);
        RequestPermissionsConfig requestPermissionsConfig3 = this.A05;
        if (requestPermissionsConfig3 == null || (strArr = requestPermissionsConfig3.A05) == null) {
            if (this.A04.A04(this.A0D).size() > 1) {
                string = this.A06.A02(this.A0C, this.A0D, this.A00.getResources(), this.A08.intValue());
            } else {
                string = this.A00.getString(this.A04.A01(this.A0D[0]), new Object[]{this.A0C});
            }
            A00(linearLayout, string);
        } else {
            for (String A002 : strArr) {
                A00(linearLayout, A002);
            }
        }
        Resources resources = this.A00.getResources();
        int intValue = this.A07.intValue();
        C27622Dg5 dg5 = this.A06;
        String[] strArr2 = this.A0D;
        Resources resources2 = this.A00.getResources();
        HashSet A042 = dg5.A00.A04(strArr2);
        String[] strArr3 = (String[]) A042.toArray(new String[A042.size()]);
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int length = strArr3.length;
            if (i >= length) {
                break;
            }
            sb.append(resources2.getString(dg5.A00.A03(strArr3[i])));
            int i2 = length - i;
            if (i2 > 2) {
                str2 = ", ";
            } else if (i2 == 2) {
                str2 = " and ";
            } else {
                str2 = BuildConfig.FLAVOR;
            }
            sb.append(str2);
            i++;
        }
        A00(linearLayout, resources.getString(intValue, sb.toString()));
        r10.A0A(inflate);
        r10.A02(this.A0B.intValue(), new AnonymousClass7x4(this));
        if (str == null) {
            r10.A00(this.A0A.intValue(), r8);
        } else {
            r10.A03(str, r8);
        }
        AnonymousClass3At A062 = r10.A06();
        this.A01 = A062;
        return A062;
    }

    public void onCancel(DialogInterface dialogInterface) {
        C12960qH r3 = this.A03;
        if (r3 != null) {
            C04230Sz.A02(r3.A00, r3.A02, "CANCEL");
            C04230Sz r0 = r3.A00;
            r0.A03.BiP(r3.A03, r3.A02);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x009b, code lost:
        if (r1 == null) goto L_0x009d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1h(android.os.Bundle r5) {
        /*
            r4 = this;
            r0 = 1809882780(0x6be09e9c, float:5.430968E26)
            int r2 = X.C000700l.A02(r0)
            super.A1h(r5)
            android.content.Context r0 = r4.A1j()
            X.1XX r3 = X.AnonymousClass1XX.get(r0)
            X.0UN r1 = new X.0UN
            r0 = 0
            r1.<init>(r0, r3)
            r4.A02 = r1
            r0 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r4.A08 = r0
            r0 = 2131832344(0x7f112e18, float:1.929774E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r4.A0B = r0
            r0 = 2131832370(0x7f112e32, float:1.9297792E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r4.A0A = r0
            r0 = 2131832369(0x7f112e31, float:1.929779E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r4.A09 = r0
            r0 = 2131832363(0x7f112e2b, float:1.9297778E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r4.A07 = r0
            X.Dg6 r0 = new X.Dg6
            r0.<init>()
            r4.A04 = r0
            X.Dg5 r0 = new X.Dg5
            r0.<init>()
            r4.A06 = r0
            int r0 = X.AnonymousClass1Y3.BH4
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r0, r1)
            X.00z r3 = (X.C001500z) r3
            int r1 = X.AnonymousClass1Y3.A1G
            X.0UN r0 = r4.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.0sy r1 = (X.C14290sy) r1
            android.content.Context r0 = r4.A1j()
            java.lang.String r0 = X.C121665oc.A02(r0, r3, r1)
            r4.A0C = r0
            android.os.Bundle r1 = r4.A0G
            if (r1 != 0) goto L_0x007a
            r0 = -225708850(0xfffffffff28bf4ce, float:-5.544239E30)
            X.C000700l.A08(r0, r2)
            return
        L_0x007a:
            java.lang.String r0 = "config"
            android.os.Parcelable r0 = r1.getParcelable(r0)
            com.facebook.runtimepermissions.RequestPermissionsConfig r0 = (com.facebook.runtimepermissions.RequestPermissionsConfig) r0
            r4.A05 = r0
            java.lang.String r0 = "permissions_never_ask_again"
            java.lang.String[] r0 = r1.getStringArray(r0)
            r4.A0D = r0
            android.app.Activity r1 = r4.A2B()
            r4.A00 = r1
            com.facebook.runtimepermissions.RequestPermissionsConfig r0 = r4.A05
            if (r0 != 0) goto L_0x009a
            java.lang.String[] r0 = r4.A0D
            if (r0 == 0) goto L_0x009d
        L_0x009a:
            r0 = 1
            if (r1 != 0) goto L_0x009e
        L_0x009d:
            r0 = 0
        L_0x009e:
            com.google.common.base.Preconditions.checkArgument(r0)
            r0 = 2005920392(0x778fea88, float:5.837932E33)
            X.C000700l.A08(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.runtimepermissions.RuntimePermissionsNeverAskAgainDialogFragment.A1h(android.os.Bundle):void");
    }
}
