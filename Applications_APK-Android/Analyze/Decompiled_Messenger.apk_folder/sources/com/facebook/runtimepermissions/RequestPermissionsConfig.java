package com.facebook.runtimepermissions;

import X.AnonymousClass3Ce;
import X.C417826y;
import X.C64513Cd;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Preconditions;

public final class RequestPermissionsConfig implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass3Ce();
    public final int A00;
    public final String A01;
    public final String A02;
    public final boolean A03;
    public final boolean A04;
    public final String[] A05;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int length;
        parcel.writeString(this.A02);
        String[] strArr = this.A05;
        if (strArr == null || (length = strArr.length) <= 0) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(length);
            parcel.writeStringArray(this.A05);
        }
        parcel.writeString(this.A01);
        parcel.writeInt(this.A00);
        C417826y.A0V(parcel, this.A04);
        C417826y.A0V(parcel, this.A03);
    }

    public RequestPermissionsConfig(C64513Cd r3) {
        String[] strArr;
        this.A02 = r3.A01;
        if (r3.A03.isEmpty()) {
            strArr = null;
        } else {
            strArr = (String[]) r3.A03.toArray(new String[0]);
        }
        this.A05 = strArr;
        this.A01 = null;
        Preconditions.checkNotNull(r3.A00);
        this.A00 = r3.A00.intValue();
        this.A04 = r3.A02;
        this.A03 = false;
    }

    public RequestPermissionsConfig(Parcel parcel) {
        this.A02 = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            String[] strArr = new String[readInt];
            this.A05 = strArr;
            parcel.readStringArray(strArr);
        } else {
            this.A05 = null;
        }
        this.A01 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A04 = C417826y.A0W(parcel);
        this.A03 = C417826y.A0W(parcel);
    }
}
