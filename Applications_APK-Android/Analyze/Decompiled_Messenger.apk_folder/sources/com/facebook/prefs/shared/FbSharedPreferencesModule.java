package com.facebook.prefs.shared;

import X.AnonymousClass062;
import X.AnonymousClass0UN;
import X.AnonymousClass0UV;
import X.AnonymousClass0VD;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import android.content.Context;
import com.facebook.inject.InjectorModule;

@InjectorModule
public class FbSharedPreferencesModule extends AnonymousClass0UV {

    public class FbSharedPreferencesModuleSelendroidInjector implements AnonymousClass062 {
        public AnonymousClass0UN A00;

        public FbSharedPreferences getFbSharedPreferences() {
            return (FbSharedPreferences) AnonymousClass1XX.A03(AnonymousClass1Y3.B6q, this.A00);
        }

        public FbSharedPreferencesModuleSelendroidInjector(Context context) {
            this.A00 = new AnonymousClass0UN(0, AnonymousClass1XX.get(context));
        }
    }

    public static FbSharedPreferences getInstanceForTest_FbSharedPreferences(AnonymousClass1XX r1) {
        return (FbSharedPreferences) r1.getInstance(FbSharedPreferences.class);
    }

    public static final FbSharedPreferences A00(AnonymousClass1XY r0) {
        return AnonymousClass0VD.A00(r0);
    }
}
