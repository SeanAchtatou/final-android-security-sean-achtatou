package com.facebook.prefs.shared;

import X.AnonymousClass1Y7;
import X.AnonymousClass1Y8;
import X.AnonymousClass1ZH;
import X.C30281hn;
import com.facebook.common.util.TriState;
import java.util.Set;
import java.util.SortedMap;

public interface FbSharedPreferences {
    void AQP();

    void ASu(Set set);

    boolean Aep(AnonymousClass1Y7 r1, boolean z);

    TriState Aeq(AnonymousClass1Y7 r1);

    double Akk(AnonymousClass1Y7 r1, double d);

    SortedMap AlW(AnonymousClass1Y7 r1);

    float AnA(AnonymousClass1Y7 r1, float f);

    int AqN(AnonymousClass1Y7 r1, int i);

    Set Arq(AnonymousClass1Y7 r1);

    long At2(AnonymousClass1Y7 r1, long j);

    String B4F(AnonymousClass1Y7 r1, String str);

    Set B87(AnonymousClass1Y8 r1);

    Object B8F(AnonymousClass1Y7 r1);

    boolean BBh(AnonymousClass1Y7 r1);

    void BCy();

    boolean BFQ();

    void C0e(Runnable runnable);

    void C0f(AnonymousClass1Y7 r1, AnonymousClass1ZH r2);

    void C0g(String str, AnonymousClass1ZH r2);

    void C0h(Set set, AnonymousClass1ZH r2);

    void C0i(AnonymousClass1Y7 r1, AnonymousClass1ZH r2);

    void CJr(AnonymousClass1Y7 r1, AnonymousClass1ZH r2);

    void CJs(Set set, AnonymousClass1ZH r2);

    void CJt(AnonymousClass1Y7 r1, AnonymousClass1ZH r2);

    C30281hn edit();
}
