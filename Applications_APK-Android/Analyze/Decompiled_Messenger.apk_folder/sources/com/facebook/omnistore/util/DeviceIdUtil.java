package com.facebook.omnistore.util;

import X.AnonymousClass08S;
import X.AnonymousClass1Uf;
import X.AnonymousClass1Ug;
import X.AnonymousClass1Uh;
import X.C013109s;
import X.C06850cB;
import android.content.Context;
import android.provider.Settings;
import android.util.Base64;
import io.card.payment.BuildConfig;
import java.util.Map;
import java.util.Set;

public class DeviceIdUtil {
    public static final Long AI_DEMOS_SAMPLE_APP_ID = 1613547858907322L;
    public static final Map APP_ID_MAP = new AnonymousClass1Uf();
    public static final Long APP_MANAGER_APP_ID = 659091980805095L;
    public static final Long CREATOR_APP_ID = 256624908153128L;
    public static final Long GRAPHQL_TEST_APP_ID = 512266575476403L;
    public static final Long INSTANT_ARTICLES_SAMPLE_APP_ID = 1410841939220564L;
    public static final Map PACKAGE_NAME_MAP = new AnonymousClass1Uh();
    public static final Long PAGES_MANAGER_APP_ID = 121876164619130L;
    public static final Long PAGES_MANAGER_DEV_APP_ID = 310060339087338L;
    public static final Long PAGES_MANAGER_IN_HOUSE_APP_ID = 288852714521348L;
    public static final Set REQUIRE_PACKAGENAME_CHECK_APP_ID = new AnonymousClass1Ug();

    public static boolean isCreatorApp(String str) {
        return CREATOR_APP_ID.toString().equals(str);
    }

    public static boolean isPagesManager(String str) {
        if (PAGES_MANAGER_APP_ID.toString().equals(str) || PAGES_MANAGER_IN_HOUSE_APP_ID.toString().equals(str) || PAGES_MANAGER_DEV_APP_ID.toString().equals(str)) {
            return true;
        }
        return false;
    }

    public static boolean isSupportedApp(String str) {
        return APP_ID_MAP.containsKey(Long.valueOf(Long.parseLong(str)));
    }

    public static String getDeviceId(Context context, Long l) {
        Object obj;
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (!C06850cB.A0A(string)) {
            try {
                string = Base64.encodeToString(C013109s.A01(string), 17);
            } catch (IllegalArgumentException unused) {
            }
            if (REQUIRE_PACKAGENAME_CHECK_APP_ID.contains(l)) {
                obj = PACKAGE_NAME_MAP.get(context.getPackageName());
            } else {
                obj = APP_ID_MAP.get(l);
            }
            String str = (String) obj;
            if (str != null) {
                String replaceAll = string.replaceAll("[^a-zA-Z0-9]", BuildConfig.FLAVOR);
                if (!C06850cB.A0A(replaceAll)) {
                    return AnonymousClass08S.A0J(str, replaceAll);
                }
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Invalid app ID: ", String.valueOf(l)));
            }
        }
        return null;
    }
}
