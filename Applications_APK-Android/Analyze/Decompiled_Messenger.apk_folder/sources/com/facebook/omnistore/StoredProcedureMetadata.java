package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class StoredProcedureMetadata {
    private final HybridData mHybridData;

    public static native StoredProcedureMetadata makeStoredProcedureMetadata(int i);

    public native int getSendAttempts();

    static {
        AnonymousClass01q.A08("omnistore");
    }

    private StoredProcedureMetadata(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
