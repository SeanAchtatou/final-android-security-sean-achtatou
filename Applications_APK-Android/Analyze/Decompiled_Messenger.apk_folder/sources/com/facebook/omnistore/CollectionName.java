package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class CollectionName {
    private final HybridData mHybridData;

    public class Builder {
        private final HybridData mHybridData;

        private native void addDeviceIdNative();

        private native void addSegmentNative(String str);

        public native CollectionName build();

        static {
            AnonymousClass01q.A08("omnistore");
        }

        private Builder(HybridData hybridData) {
            this.mHybridData = hybridData;
        }

        public Builder addSegment(String str) {
            addSegmentNative(str);
            return this;
        }

        public Builder addDeviceId() {
            addDeviceIdNative();
            return this;
        }
    }

    public native boolean equalsNative(CollectionName collectionName);

    public native String getLabel();

    public native QueueIdentifier getQueueIdentifier();

    public native String toString();

    static {
        AnonymousClass01q.A08("omnistore");
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof CollectionName)) {
            return false;
        }
        if (this == obj || equalsNative((CollectionName) obj)) {
            return true;
        }
        return false;
    }

    private CollectionName(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public int hashCode() {
        return toString().hashCode();
    }
}
