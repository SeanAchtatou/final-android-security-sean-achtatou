package com.facebook.omnistore;

public class OmnistoreSettings {
    public long dbVacuumInterval = 0;
    public boolean deleteDbIfDbHealthTrackerIsCorrupt;
    public boolean deleteDbIfDbIsCorrupt;
    public boolean deleteDbOnOpenError = true;
    public boolean deleteObjectsBeforeResnapshot = true;
    private boolean disableCheckpointOnDBInit;
    public boolean enableApiEventLogger = false;
    public boolean enableDatabaseHealthTracker;
    private boolean enableDelayIntegrityProcessWithRetries;
    public boolean enableFlatbufferRuntimeVerifier;
    private boolean enableFlatbufferRuntimeVerifierForDelta;
    public boolean enableIrisAckOptimization;
    private boolean enablePerCollectionIntegrityProcessTiming;
    public boolean enableReportChangedBlob;
    public boolean enableSelfCheck = false;
    public boolean enableServerSideUnsubscribe;
    private boolean enableServerSync = false;
    private boolean enableStrongConsistencyOnQueueBasedSubscription = false;
    private boolean enableThrowExceptionOnOpening = false;
    public long minDeleteDBSizeMB = 0;
    private long sendBaseRetryTimeoutInSecond = -1;
    public boolean sendCollectionWithConnectSubscription = false;
    public boolean shouldSkipConnectForPreviousSession;
}
