package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import java.io.Closeable;
import java.nio.ByteBuffer;

public class SendQueueCursor implements Closeable {
    private final HybridData mHybridData;

    public native void close();

    public native long getClientVersionId();

    public native long getEnqueueTimestampMs();

    public native String getQueueName();

    public native long getSendAttempts();

    public native long getStoredProcedureId();

    public native ByteBuffer getStoredProcedureParams();

    public native String getUniqueKey();

    public native boolean step();

    static {
        AnonymousClass01q.A08("omnistore");
    }

    private SendQueueCursor(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
