package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import java.io.Closeable;
import java.nio.ByteBuffer;

public class Cursor implements Closeable {
    private final HybridData mHybridData;

    public native void close();

    public native ByteBuffer getBlob();

    public native String getPrimaryKey();

    public native String getSortKey();

    public native boolean step();

    static {
        AnonymousClass01q.A08("omnistorecollections");
    }

    private Cursor(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
