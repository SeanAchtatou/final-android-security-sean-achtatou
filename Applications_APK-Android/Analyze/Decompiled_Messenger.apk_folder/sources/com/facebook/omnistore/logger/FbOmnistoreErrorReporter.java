package com.facebook.omnistore.logger;

import X.AnonymousClass09P;
import X.AnonymousClass0US;
import X.AnonymousClass0VB;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C04310Tq;
import X.C04750Wa;
import com.facebook.omnistore.OmnistoreErrorReporter;
import javax.inject.Singleton;

@Singleton
public class FbOmnistoreErrorReporter implements OmnistoreErrorReporter {
    private static volatile FbOmnistoreErrorReporter $ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXINSTANCE;
    private final AnonymousClass09P mFbErrorReporter;

    public static final AnonymousClass0US $ul_$xXXcom_facebook_inject_Lazy$x3Ccom_facebook_omnistore_logger_FbOmnistoreErrorReporter$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.Aeh, r1);
    }

    public static final FbOmnistoreErrorReporter $ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXFACTORY_METHOD(AnonymousClass1XY r4) {
        if ($ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXINSTANCE == null) {
            synchronized (FbOmnistoreErrorReporter.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXINSTANCE, r4);
                if (A00 != null) {
                    try {
                        $ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXINSTANCE = new FbOmnistoreErrorReporter(r4.getApplicationInjector());
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXINSTANCE;
    }

    public static final C04310Tq $ul_$xXXjavax_inject_Provider$x3Ccom_facebook_omnistore_logger_FbOmnistoreErrorReporter$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.Aeh, r1);
    }

    public void reportError(String str, String str2, Throwable th) {
        this.mFbErrorReporter.softReport(str, str2, th);
    }

    public static final FbOmnistoreErrorReporter $ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXACCESS_METHOD(AnonymousClass1XY r0) {
        return $ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXFACTORY_METHOD(r0);
    }

    public FbOmnistoreErrorReporter(AnonymousClass1XY r2) {
        this.mFbErrorReporter = C04750Wa.A01(r2);
    }
}
