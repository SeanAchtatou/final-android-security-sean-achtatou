package com.facebook.omnistore.module;

import X.C14430tJ;
import X.C32171lK;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.Omnistore;

public interface OmnistoreComponent extends C14430tJ {
    String getCollectionLabel();

    void onCollectionAvailable(Collection collection);

    void onCollectionInvalidated();

    C32171lK provideSubscriptionInfo(Omnistore omnistore);
}
