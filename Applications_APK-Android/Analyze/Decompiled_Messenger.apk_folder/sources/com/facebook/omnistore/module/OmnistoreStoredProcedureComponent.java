package com.facebook.omnistore.module;

import X.AnonymousClass2WT;
import java.nio.ByteBuffer;

public interface OmnistoreStoredProcedureComponent {
    void onSenderAvailable(AnonymousClass2WT r1);

    void onSenderInvalidated();

    void onStoredProcedureResult(ByteBuffer byteBuffer);

    int provideStoredProcedureId();
}
