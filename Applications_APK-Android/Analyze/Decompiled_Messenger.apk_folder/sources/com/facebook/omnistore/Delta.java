package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import java.nio.ByteBuffer;

public class Delta {
    private final HybridData mHybridData;

    public static native Delta makeDelta(CollectionName collectionName, int i, String str, String str2, byte[] bArr);

    public native ByteBuffer getBlob();

    public native CollectionName getCollectionName();

    public native String getPrimaryKey();

    public native String getSortKey();

    public native int getStatus();

    public native int getType();

    static {
        AnonymousClass01q.A08("omnistore");
    }

    private Delta(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
