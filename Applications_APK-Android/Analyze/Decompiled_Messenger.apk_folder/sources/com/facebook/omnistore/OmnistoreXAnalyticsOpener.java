package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory;
import com.facebook.jni.HybridData;
import com.facebook.tigon.iface.TigonServiceHolder;
import com.facebook.xanalytics.XAnalyticsHolder;

public class OmnistoreXAnalyticsOpener {
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public static native Omnistore open(OmnistoreDatabaseCreator omnistoreDatabaseCreator, String str, MqttProtocolProvider mqttProtocolProvider, OmnistoreErrorReporter omnistoreErrorReporter, XAnalyticsHolder xAnalyticsHolder, OmnistoreSettings omnistoreSettings, TigonServiceHolder tigonServiceHolder, String str2, AndroidAsyncExecutorFactory androidAsyncExecutorFactory, String str3, OmnistoreCollectionFrontendHolder omnistoreCollectionFrontendHolder, OmnistoreCustomLogger omnistoreCustomLogger);

    static {
        AnonymousClass01q.A08("omnistoreopener");
    }
}
