package com.facebook.omnistore;

import X.AnonymousClass01q;
import X.C32151lI;
import X.C32161lJ;
import com.facebook.jni.HybridData;

public class OmnistoreCollections {
    private final HybridData mHybridData = initHybrid();

    private native Collection doSubscribeCollection(Omnistore omnistore, CollectionName collectionName, String str, String str2, String str3, byte[] bArr, long j, boolean z, int i);

    private static native HybridData initHybrid();

    public native OmnistoreCollectionFrontendHolder getFrontend();

    public Collection subscribeCollection(Omnistore omnistore, CollectionName collectionName, C32161lJ r15) {
        boolean z;
        Omnistore omnistore2 = omnistore;
        synchronized (omnistore2) {
            synchronized (omnistore2) {
                z = omnistore.mIsClosed;
            }
            return r0;
        }
        if (!z) {
            C32151lI r0 = r15.A00;
            Collection doSubscribeCollection = doSubscribeCollection(omnistore2, collectionName, r0.A02, r0.A03, r0.A04, null, r0.A01, r0.A05, r0.A00);
            return doSubscribeCollection;
        }
        throw new OmnistoreException("Called subscribeCollection after close");
    }

    static {
        AnonymousClass01q.A08("omnistorecollections");
    }
}
