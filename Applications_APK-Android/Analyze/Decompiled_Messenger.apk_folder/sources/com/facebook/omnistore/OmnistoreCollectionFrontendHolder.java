package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class OmnistoreCollectionFrontendHolder {
    private final HybridData mHybridData;

    static {
        AnonymousClass01q.A08("omnistore");
    }

    private OmnistoreCollectionFrontendHolder(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
