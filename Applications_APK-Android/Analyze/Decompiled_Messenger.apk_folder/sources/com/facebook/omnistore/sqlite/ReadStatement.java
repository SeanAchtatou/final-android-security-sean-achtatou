package com.facebook.omnistore.sqlite;

import X.C32131lC;
import X.C32141lE;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.facebook.omnistore.OmnistoreIOException;
import java.io.Closeable;

public class ReadStatement implements Closeable {
    private static final byte[] EMPTY_BLOB = new byte[0];
    private C32141lE mCursorWrapper;
    private final C32131lC mDatabaseWrapper;
    private final String mSql;

    private Cursor assertCursor() {
        C32141lE r0 = this.mCursorWrapper;
        if (r0 != null) {
            return r0.A00();
        }
        throw new RuntimeException("Tried to read from null cursor. Did you forget to step()?");
    }

    public void close() {
        try {
            C32141lE r1 = this.mCursorWrapper;
            if (r1 != null) {
                if (!r1.A00.isClosed()) {
                    r1.A00.close();
                }
                this.mCursorWrapper = null;
            }
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public void reset() {
        try {
            C32141lE r1 = this.mCursorWrapper;
            if (r1 != null) {
                if (!r1.A00.isClosed()) {
                    r1.A00.close();
                }
                this.mCursorWrapper = null;
            }
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public boolean step(String[] strArr) {
        try {
            if (this.mCursorWrapper == null) {
                this.mCursorWrapper = new C32141lE(this.mDatabaseWrapper.A00().rawQuery(this.mSql, strArr));
            }
            if (!this.mCursorWrapper.A00().moveToNext()) {
                return false;
            }
            if (!this.mCursorWrapper.A00().isNull(0)) {
                return true;
            }
            try {
                this.mCursorWrapper.A00().getBlob(0);
                return true;
            } catch (IllegalStateException unused) {
                return false;
            }
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public ReadStatement(String str, C32131lC r2) {
        this.mSql = str;
        this.mDatabaseWrapper = r2;
    }

    public byte[] getBlob(int i) {
        try {
            byte[] blob = assertCursor().getBlob(i);
            if (blob != null || isNull(i)) {
                return blob;
            }
            return EMPTY_BLOB;
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public int getInt(int i) {
        try {
            return assertCursor().getInt(i);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public long getLong(int i) {
        try {
            return assertCursor().getLong(i);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public String getString(int i) {
        try {
            return assertCursor().getString(i);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public boolean isNull(int i) {
        try {
            return assertCursor().isNull(i);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }
}
