package com.facebook.omnistore.sqlite;

import X.C007406x;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import com.facebook.omnistore.OmnistoreIOException;
import java.io.Closeable;

public class WriteStatement implements Closeable {
    private final SQLiteDatabase mDatabase;
    private final SQLiteStatement mStatementUnsafe;

    private SQLiteStatement getStatement() {
        if (this.mDatabase.isOpen()) {
            return this.mStatementUnsafe;
        }
        throw new OmnistoreIOException("SQLite Database is closed");
    }

    public void close() {
        try {
            this.mStatementUnsafe.close();
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public WriteStatement(SQLiteStatement sQLiteStatement, SQLiteDatabase sQLiteDatabase) {
        this.mDatabase = sQLiteDatabase;
        this.mStatementUnsafe = sQLiteStatement;
    }

    public void bindBlob(int i, byte[] bArr) {
        try {
            getStatement().bindBlob(i, bArr);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public void bindLong(int i, long j) {
        try {
            getStatement().bindLong(i, j);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public void bindNull(int i) {
        try {
            getStatement().bindNull(i);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public void bindString(int i, String str) {
        try {
            getStatement().bindString(i, str);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public void execute() {
        try {
            SQLiteStatement statement = getStatement();
            C007406x.A00(1371922506);
            statement.execute();
            C007406x.A00(-2078742041);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public void reset() {
        try {
            getStatement().clearBindings();
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }
}
