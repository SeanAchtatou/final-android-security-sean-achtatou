package com.facebook.omnistore.sqlite;

import X.AnonymousClass4X4;
import X.C007406x;
import X.C32131lC;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.facebook.omnistore.OmnistoreIOException;
import java.io.Closeable;

public class Database implements Closeable {
    private final C32131lC mDatabaseWrapper;
    private int mTransactionNestingDepth;

    public synchronized void execExclusiveTransaction(String str) {
        try {
            SQLiteDatabase A00 = this.mDatabaseWrapper.A00();
            C007406x.A00(715657945);
            A00.execSQL(str);
            C007406x.A00(439105871);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public synchronized boolean isInTransaction() {
        boolean z;
        z = false;
        if (this.mTransactionNestingDepth != 0) {
            z = true;
        }
        return z;
    }

    public synchronized void runInRootTransaction(Runnable runnable) {
        if (this.mTransactionNestingDepth == 0) {
            runInTransaction(runnable);
        } else {
            throw new AnonymousClass4X4();
        }
    }

    public synchronized void runInTransaction(Runnable runnable) {
        try {
            C007406x.A01(this.mDatabaseWrapper.A00(), -1481933718);
            this.mTransactionNestingDepth++;
            runnable.run();
            this.mDatabaseWrapper.A00().setTransactionSuccessful();
            this.mTransactionNestingDepth--;
            C007406x.A02(this.mDatabaseWrapper.A00(), -1064148207);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        } catch (Throwable th) {
            this.mTransactionNestingDepth--;
            C007406x.A02(this.mDatabaseWrapper.A00(), 520471587);
            throw th;
        }
    }

    public void close() {
        this.mDatabaseWrapper.A00.close();
    }

    public void exec(String str) {
        try {
            SQLiteDatabase A00 = this.mDatabaseWrapper.A00();
            C007406x.A00(-326336547);
            A00.execSQL(str);
            C007406x.A00(-1501505647);
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public String getDatabaseFilename() {
        return this.mDatabaseWrapper.A00().getPath();
    }

    public ReadStatement prepareRead(String str) {
        return new ReadStatement(str, this.mDatabaseWrapper);
    }

    public WriteStatement prepareWrite(String str) {
        try {
            return new WriteStatement(this.mDatabaseWrapper.A00().compileStatement(str), this.mDatabaseWrapper.A00());
        } catch (SQLiteException e) {
            throw new OmnistoreIOException(e);
        }
    }

    public Database(SQLiteDatabase sQLiteDatabase) {
        this.mDatabaseWrapper = new C32131lC(sQLiteDatabase);
    }
}
