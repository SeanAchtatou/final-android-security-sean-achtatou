package com.facebook.omnistore.sqlite;

import X.AnonymousClass01q;
import X.C66633La;
import com.facebook.jni.HybridData;
import com.facebook.omnistore.OmnistoreDatabaseCreator;

public class AndroidSqliteOmnistoreDatabaseCreator {

    public interface DatabaseOpener {
        void deleteDatabaseFiles();

        String getHealthTrackerAbsoluteFilename();

        PreparedDatabase openDatabase(SchemaUpdater schemaUpdater);
    }

    public static native OmnistoreDatabaseCreator makeDatabaseCreator(DatabaseOpener databaseOpener);

    public class PreparedDatabase {
        private final HybridData mHybridData;

        private PreparedDatabase(HybridData hybridData) {
            this.mHybridData = hybridData;
        }
    }

    public class SchemaUpdater {
        private final HybridData mHybridData;

        public native PreparedDatabase ensureDbSchema(Database database);

        private SchemaUpdater(HybridData hybridData) {
            this.mHybridData = hybridData;
        }
    }

    static {
        AnonymousClass01q.A08("omnistoresqliteandroid");
    }

    public class OmnistoreDowngradeException extends C66633La {
        public OmnistoreDowngradeException(String str) {
            super(str);
        }
    }
}
