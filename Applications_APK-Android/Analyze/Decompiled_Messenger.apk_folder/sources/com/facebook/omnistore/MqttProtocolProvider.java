package com.facebook.omnistore;

import com.facebook.jni.HybridData;

public final class MqttProtocolProvider {
    private final HybridData mHybridData;

    public MqttProtocolProvider(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
