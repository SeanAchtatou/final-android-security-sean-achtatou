package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class OmnistoreOpener {
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public static native Omnistore open(OmnistoreDatabaseCreator omnistoreDatabaseCreator, String str, MqttProtocolProvider mqttProtocolProvider, OmnistoreCustomLogger omnistoreCustomLogger, OmnistoreSettings omnistoreSettings, OmnistoreCollectionFrontendHolder omnistoreCollectionFrontendHolder);

    static {
        AnonymousClass01q.A08("omnistore");
    }
}
