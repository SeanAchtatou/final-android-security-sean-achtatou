package com.facebook.omnistore.mqtt;

import X.AnonymousClass07B;
import X.AnonymousClass08G;
import X.AnonymousClass0US;
import X.AnonymousClass0VB;
import X.AnonymousClass0WD;
import X.AnonymousClass1FI;
import X.AnonymousClass1FP;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C04310Tq;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;
import javax.inject.Singleton;

@Singleton
public class OmnistoreMqttTopicsSetProvider implements AnonymousClass1FI {
    private static volatile OmnistoreMqttTopicsSetProvider $ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXINSTANCE = null;
    public static final String OMNISTORE_SYNC_LOW_PRI_TOPIC = "/t_omnistore_sync_low_pri";
    public static final String OMNISTORE_SYNC_TOPIC = "/t_omnistore_sync";

    public static final AnonymousClass0US $ul_$xXXcom_facebook_inject_Lazy$x3Ccom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.Ac4, r1);
    }

    public static final OmnistoreMqttTopicsSetProvider $ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXFACTORY_METHOD(AnonymousClass1XY r3) {
        if ($ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXINSTANCE == null) {
            synchronized (OmnistoreMqttTopicsSetProvider.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXINSTANCE, r3);
                if (A00 != null) {
                    try {
                        r3.getApplicationInjector();
                        $ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXINSTANCE = new OmnistoreMqttTopicsSetProvider();
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXINSTANCE;
    }

    public static final C04310Tq $ul_$xXXjavax_inject_Provider$x3Ccom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.Ac4, r1);
    }

    public static boolean isOmnistoreTopic(String str) {
        if (OMNISTORE_SYNC_TOPIC.equals(str) || OMNISTORE_SYNC_LOW_PRI_TOPIC.equals(str)) {
            return true;
        }
        return false;
    }

    public ImmutableMap get() {
        SubscribeTopic subscribeTopic = new SubscribeTopic(OMNISTORE_SYNC_TOPIC, AnonymousClass08G.A00(AnonymousClass07B.A01));
        AnonymousClass1FP r3 = AnonymousClass1FP.ALWAYS;
        return ImmutableMap.of(subscribeTopic, r3, new SubscribeTopic(OMNISTORE_SYNC_LOW_PRI_TOPIC, AnonymousClass08G.A00(AnonymousClass07B.A01)), r3);
    }

    public static final OmnistoreMqttTopicsSetProvider $ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXACCESS_METHOD(AnonymousClass1XY r0) {
        return $ul_$xXXcom_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider$xXXFACTORY_METHOD(r0);
    }
}
