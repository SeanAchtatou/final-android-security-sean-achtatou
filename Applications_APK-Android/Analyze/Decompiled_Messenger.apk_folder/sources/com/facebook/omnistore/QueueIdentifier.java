package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class QueueIdentifier {
    private final HybridData mHybridData;

    private static native HybridData initHybrid(String str, String str2);

    public native boolean equalsNative(QueueIdentifier queueIdentifier);

    public native String getDomain();

    public native String getTopic();

    public native String toString();

    static {
        AnonymousClass01q.A08("omnistore");
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof QueueIdentifier)) {
            return false;
        }
        if (this == obj || equalsNative((QueueIdentifier) obj)) {
            return true;
        }
        return false;
    }

    private QueueIdentifier(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public int hashCode() {
        return toString().hashCode();
    }
}
