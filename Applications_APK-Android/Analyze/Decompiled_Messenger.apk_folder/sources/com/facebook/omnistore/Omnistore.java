package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import com.facebook.omnistore.CollectionName;
import java.io.Closeable;
import java.nio.ByteBuffer;

public class Omnistore implements Closeable {
    private final HybridData mHybridData;
    public boolean mIsClosed = false;

    public interface CollectionIndexerFunction {
        IndexedFields getIndexedFields(CollectionName collectionName, String str, String str2, ByteBuffer byteBuffer);
    }

    public interface DeltaClusterCallback {
        void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier);

        void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier);
    }

    public interface DeltaReceivedCallback {
        void onDeltaReceived(Delta[] deltaArr);
    }

    public interface SnapshotStateChangedCallback {
        void onSnapshotStateChanged(CollectionName collectionName, int i);
    }

    public interface StoredProcedureResultCallback {
        void onStoredProcedureResult(int i, ByteBuffer byteBuffer);
    }

    public interface StoredProcedureInternalErrorCallback {
        void onStoredProcedureInternalError(int i, ByteBuffer byteBuffer);
    }

    public interface StoredProcedureResultWithMetadataCallback {
        void onStoredProcedureResult(int i, ByteBuffer byteBuffer, String str, StoredProcedureMetadata storedProcedureMetadata);
    }

    public interface StoredProcedureResultWithUniqueKeyCallback {
        void onStoredProcedureResult(int i, ByteBuffer byteBuffer, String str);
    }

    public interface SubscriptionParamsErrorCallback {
        void onSubscriptionParamsError(CollectionName collectionName, String str, String str2, String str3);
    }

    public interface DeltaInternalErrorCallback {
        int onDeltaInternalError(Delta[] deltaArr, int i);
    }

    private native void doApplyStoredProcedure(int i, byte[] bArr, String str, String str2);

    private native boolean doCancelStoredProcedure(int i, String str);

    private native void doClose();

    private native String doGetDebugInfo();

    private native String[] doGetSubscriptionCollectionNames();

    private native SendQueueCursor doQuerySendQueue(String str);

    private native void doStart();

    private native void doUnsubscribeCollection(CollectionName collectionName);

    private native void doUnsubscribeQueue(QueueIdentifier queueIdentifier);

    private native String[] doWriteBugReport(String str);

    public native void addDeltaClusterCallback(DeltaClusterCallback deltaClusterCallback);

    public native void addDeltaReceivedCallback(DeltaReceivedCallback deltaReceivedCallback);

    public native void addSnapshotStateChangedCallback(SnapshotStateChangedCallback snapshotStateChangedCallback);

    public native void addStoredProcedureInternalErrorCallback(StoredProcedureInternalErrorCallback storedProcedureInternalErrorCallback);

    public native void addStoredProcedureResultCallback(StoredProcedureResultCallback storedProcedureResultCallback);

    public native void addStoredProcedureResultWithMetadataCallback(StoredProcedureResultWithMetadataCallback storedProcedureResultWithMetadataCallback);

    public native void addStoredProcedureResultWithUniqueKeyCallback(StoredProcedureResultWithUniqueKeyCallback storedProcedureResultWithUniqueKeyCallback);

    public native void addSubscriptionParamsErrorCallback(SubscriptionParamsErrorCallback subscriptionParamsErrorCallback);

    public synchronized boolean cancelStoredProcedure(int i, String str) {
        if (this.mIsClosed) {
            return false;
        }
        return doCancelStoredProcedure(i, str);
    }

    public synchronized void close() {
        if (!this.mIsClosed) {
            this.mIsClosed = true;
            doClose();
        }
    }

    public native CollectionName.Builder createCollectionNameBuilder(String str);

    public native CollectionName.Builder createCollectionNameWithDomainBuilder(String str, String str2);

    public synchronized String getDebugInfo() {
        if (this.mIsClosed) {
            return "Omnistore removed";
        }
        return doGetDebugInfo();
    }

    public native void setCollectionIndexerFunction(CollectionIndexerFunction collectionIndexerFunction);

    public native void setDeltaInternalErrorCallback(DeltaInternalErrorCallback deltaInternalErrorCallback);

    public synchronized void start() {
        if (!this.mIsClosed) {
            doStart();
        }
    }

    public native void subscribeQueue(QueueIdentifier queueIdentifier, String str, long j);

    public synchronized void unsubscribeCollection(CollectionName collectionName) {
        if (!this.mIsClosed) {
            doUnsubscribeCollection(collectionName);
        }
    }

    public synchronized String[] writeBugReport(String str) {
        if (this.mIsClosed) {
            return new String[0];
        }
        return doWriteBugReport(str);
    }

    static {
        AnonymousClass01q.A08("omnistore");
    }

    private Omnistore(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public synchronized void applyStoredProcedure(int i, byte[] bArr) {
        applyStoredProcedure(i, bArr, null, null);
    }

    public synchronized void applyStoredProcedure(int i, byte[] bArr, String str, String str2) {
        if (!this.mIsClosed) {
            doApplyStoredProcedure(i, bArr, str, str2);
        }
    }
}
