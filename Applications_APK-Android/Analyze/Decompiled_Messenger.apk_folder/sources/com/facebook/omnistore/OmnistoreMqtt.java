package com.facebook.omnistore;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class OmnistoreMqtt {
    private final HybridData mHybridData;

    public class PublishCallback {
        private final HybridData mHybridData;

        public native void onFailure();

        public native void onSuccess();

        static {
            AnonymousClass01q.A08("omnistore");
        }

        private PublishCallback(HybridData hybridData) {
            this.mHybridData = hybridData;
        }
    }

    public interface Publisher {
        void ensureConnection();

        void publishMessage(String str, byte[] bArr, PublishCallback publishCallback);
    }

    private static native HybridData initHybrid(Publisher publisher, OmnistoreCustomLogger omnistoreCustomLogger);

    public native MqttProtocolProvider getProtocolProvider();

    public native void handleOmnistoreSyncMessage(byte[] bArr);

    public native void onConnectionEstablished();

    public native void onConnectionLost();

    static {
        AnonymousClass01q.A08("omnistore");
    }

    public OmnistoreMqtt(Publisher publisher, OmnistoreCustomLogger omnistoreCustomLogger) {
        this.mHybridData = initHybrid(publisher, omnistoreCustomLogger);
    }
}
