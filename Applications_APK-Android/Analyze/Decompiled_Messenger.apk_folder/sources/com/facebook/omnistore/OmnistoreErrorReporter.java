package com.facebook.omnistore;

public interface OmnistoreErrorReporter {
    void reportError(String str, String str2, Throwable th);
}
