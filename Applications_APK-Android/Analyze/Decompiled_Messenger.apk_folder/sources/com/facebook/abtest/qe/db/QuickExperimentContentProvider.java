package com.facebook.abtest.qe.db;

import X.AnonymousClass0TB;
import X.AnonymousClass0TF;
import X.AnonymousClass1XX;
import X.AnonymousClass4XI;
import X.C158667Vw;

public class QuickExperimentContentProvider extends AnonymousClass0TB {
    public C158667Vw A00;
    public AnonymousClass4XI A01;
    public AnonymousClass0TF A02;

    public void A0G() {
        super.A0G();
        AnonymousClass1XX r1 = AnonymousClass1XX.get(getContext());
        this.A01 = AnonymousClass4XI.A00(r1);
        this.A00 = new C158667Vw(r1);
        AnonymousClass0TF r3 = new AnonymousClass0TF();
        this.A02 = r3;
        r3.A01(this.A01.A00, "metainfo", this.A00);
    }
}
