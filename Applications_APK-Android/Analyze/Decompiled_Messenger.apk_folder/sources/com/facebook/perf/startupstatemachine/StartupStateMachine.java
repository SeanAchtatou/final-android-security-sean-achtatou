package com.facebook.perf.startupstatemachine;

import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import javax.inject.Singleton;

@Singleton
public final class StartupStateMachine {
    private static volatile StartupStateMachine A04;
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = 0;
    public int A03 = 0;

    public synchronized int A01() {
        return this.A00;
    }

    public synchronized int A02() {
        return this.A02;
    }

    public synchronized int A03() {
        return this.A03;
    }

    public synchronized String toString() {
        return Integer.toString(this.A00) + ':' + Integer.toString(this.A02) + ':' + Integer.toString(this.A03) + ':' + Integer.toString(this.A01);
    }

    public static final StartupStateMachine A00(AnonymousClass1XY r3) {
        if (A04 == null) {
            synchronized (StartupStateMachine.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A04 = new StartupStateMachine();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }
}
