package com.facebook.systrace;

import X.AnonymousClass00I;
import X.AnonymousClass01q;
import X.AnonymousClass09R;
import X.AnonymousClass0HL;
import android.os.Process;
import android.util.Log;
import com.facebook.common.util.TriState;

public class TraceDirect {
    private static final boolean sForceJavaImpl = "true".equals(AnonymousClass00I.A02("debug.fbsystrace.force_java"));
    private static volatile TriState sNativeAvailable = TriState.UNSET;
    private static volatile int sPrevSoLoaderSourcesVersion = -1;
    private static final boolean sTraceLoad = "true".equals(AnonymousClass00I.A02("debug.fbsystrace.trace_load"));

    private static native void nativeAsyncTraceBegin(String str, int i, long j);

    public static native void nativeAsyncTraceCancel(String str, int i);

    private static native void nativeAsyncTraceEnd(String str, int i, long j);

    public static native void nativeAsyncTraceRename(String str, String str2, int i);

    private static native void nativeAsyncTraceStageBegin(String str, int i, long j, String str2);

    public static native void nativeBeginSection(String str);

    public static native void nativeBeginSectionWithArgs(String str, String[] strArr, int i);

    public static native void nativeEndAsyncFlow(String str, int i);

    public static native void nativeEndSection();

    public static native void nativeEndSectionWithArgs(String[] strArr, int i);

    public static native void nativeSetEnabledTags(long j);

    public static native void nativeStartAsyncFlow(String str, int i);

    private static native void nativeStepAsyncFlow(String str, int i);

    public static native void nativeTraceCounter(String str, int i);

    public static native void nativeTraceInstant(String str, String str2, char c);

    public static native void nativeTraceMetadata(String str, String str2, int i);

    public static boolean checkNative() {
        if (sNativeAvailable == TriState.UNSET) {
            if (sForceJavaImpl) {
                sNativeAvailable = TriState.NO;
            } else {
                int i = AnonymousClass01q.A0B;
                if (i != sPrevSoLoaderSourcesVersion) {
                    sPrevSoLoaderSourcesVersion = i;
                    if (sTraceLoad) {
                        new Exception();
                    }
                    try {
                        AnonymousClass01q.A08("fbsystrace");
                        sNativeAvailable = TriState.YES;
                        nativeSetEnabledTags(AnonymousClass00I.A00("debug.fbsystrace.tags", 0));
                    } catch (UnsatisfiedLinkError unused) {
                        Log.w("TraceDirect", "fbsystrace.so could not be loaded - switching to Java implementation.");
                    }
                }
            }
        }
        if (sNativeAvailable != TriState.YES) {
            return false;
        }
        return true;
    }

    public static void asyncTraceBegin(String str, int i, long j) {
        if (checkNative()) {
            nativeAsyncTraceBegin(str, i, j);
            return;
        }
        AnonymousClass09R r3 = new AnonymousClass09R('S');
        r3.A01(Process.myPid());
        r3.A03(str);
        r3.A02("<0>");
        if (j != 0) {
            r3.A02("<T");
            r3.A02(Long.toString(j));
            r3.A02(">");
        }
        r3.A01(i);
        AnonymousClass0HL.A00(r3.toString());
    }

    public static void asyncTraceEnd(String str, int i, long j) {
        if (checkNative()) {
            nativeAsyncTraceEnd(str, i, j);
            return;
        }
        AnonymousClass09R r3 = new AnonymousClass09R('F');
        r3.A01(Process.myPid());
        r3.A03(str);
        if (j != 0) {
            r3.A02("<T");
            r3.A02(Long.toString(j));
            r3.A02(">");
        }
        r3.A01(i);
        AnonymousClass0HL.A00(r3.toString());
    }

    public static void asyncTraceStageBegin(String str, int i, long j, String str2) {
        if (checkNative()) {
            nativeAsyncTraceStageBegin(str, i, j, str2);
            return;
        }
        AnonymousClass09R r3 = new AnonymousClass09R('T');
        r3.A01(Process.myPid());
        r3.A03(str);
        if (j != 0) {
            r3.A02("<T");
            r3.A02(Long.toString(j));
            r3.A02(">");
        }
        r3.A01(i);
        r3.A03(str2);
        AnonymousClass0HL.A00(r3.toString());
    }
}
