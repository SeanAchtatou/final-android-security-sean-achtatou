package com.facebook.soloader;

public class MergedSoMapping$Invoke_JNI_OnLoad {
    public static native int libandroid_live_streaming_so();

    public static native int libandroid_reachability_announcer_so();

    public static native int libandroid_rtmpssl_so();

    public static native int libandroid_video_protocol_so();

    public static native int libappstatelogger_so();

    public static native int libarclass_graphql_so();

    public static native int libarclass_so();

    public static native int libardcache_jni_so();

    public static native int libardfilecache_jni_so();

    public static native int libarpersistenceservice_so();

    public static native int libasyncexecutor_so();

    public static native int libasyncgraphstoreservice_so();

    public static native int libaudiographservice_so();

    public static native int libbase64_so();

    public static native int libcaffe2_core_ops_so();

    public static native int libchipset_so();

    public static native int libclassid_so();

    public static native int libclasstracing_so();

    public static native int libcoldstart_pgo_so();

    public static native int libcompactdisk_analytics_jni_so();

    public static native int libcompactdisk_common_jni_so();

    public static native int libcompactdisk_current_jni_so();

    public static native int libconcealcpp_so();

    public static native int libconcealjni_so();

    public static native int libcryptojni_so();

    public static native int libcryptox_so();

    public static native int libdalviksmartgc_so();

    public static native int libdextricks_so();

    public static native int libdouble_conversion_so();

    public static native int libdownloadservice_jni_so();

    public static native int libfacetracker_model_cache_native_android_so();

    public static native int libfb_ffmpeg_jni_so();

    public static native int libfb_ffmpeg_so();

    public static native int libfb_jpegturbo_so();

    public static native int libfb_mozjpeg_so();

    public static native int libfb_sqlite_omnistore_so();

    public static native int libfbacore_jni_so();

    public static native int libfbacoreimpl_so();

    public static native int libfbandroid_java_com_facebook_cameracore_mediapipeline_effectasyncassetfetcher_jni_jni_so();

    public static native int libfbandroid_java_com_facebook_cameracore_mediapipeline_engine_jni_textures_textures_so();

    public static native int libfbandroid_java_com_facebook_cameracore_mediapipeline_services_persistence_config_fb4a_remote_jni_jni_so();

    public static native int libfbandroid_java_com_facebook_common_dextricks_classtracing_logger_native_so();

    public static native int libfbandroid_java_com_facebook_omnistore_jni_android_logger_so();

    public static native int libfbandroid_java_com_facebook_omnistore_jni_jni_so();

    public static native int libfbandroid_java_com_facebook_tigon_iface_jni_iface_jni_so();

    public static native int libfbandroid_libraries_profilo_cpp_generated_cpp_so();

    public static native int libfbandroid_libraries_profilo_cpp_jni_jni_so();

    public static native int libfbandroid_libraries_profilo_cpp_jni_writer_callbacks_so();

    public static native int libfbandroid_libraries_profilo_cpp_logger_buffer_buffer_static_so();

    public static native int libfbandroid_libraries_profilo_cpp_logger_lfrb_lfrb_so();

    public static native int libfbandroid_libraries_profilo_cpp_logger_logger_static_so();

    public static native int libfbandroid_libraries_profilo_cpp_profiler_external_tracer_manager_so();

    public static native int libfbandroid_libraries_profilo_cpp_profiler_external_tracer_so();

    public static native int libfbandroid_libraries_profilo_cpp_profiler_js_tracer_so();

    public static native int libfbandroid_libraries_profilo_cpp_profiler_tracer_utils_so();

    public static native int libfbandroid_libraries_profilo_cpp_providers_so();

    public static native int libfbandroid_libraries_profilo_cpp_util_hooks_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_delta_visitor_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_packet_reassembler_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_print_visitor_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_stack_visitor_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_timestamp_truncating_visitor_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_trace_backwards_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_trace_headers_so();

    public static native int libfbandroid_libraries_profilo_cpp_writer_writer_so();

    public static native int libfbandroid_libraries_profilo_facebook_cpp_blackbox_crash_dump_header_so();

    public static native int libfbandroid_native_redex_shared_dex_so();

    public static native int libfbandroid_native_redex_shared_mmap_so();

    public static native int libfbandroid_native_redex_shared_quickening_so();

    public static native int libfbandroid_native_redex_shared_util_so();

    public static native int libfbandroid_native_redex_tools_oatmeal_oatmeal_src_so();

    public static native int libfbandroid_xplat_RtmpSSL_LigerRtmpSSL_so();

    public static native int libfbjs_empty_so();

    public static native int libfbrtmp_so();

    public static native int libfbsystrace_so();

    public static native int libfeatureconfig_so();

    public static native int libfilters_native_android_so();

    public static native int libfittedexpressiontracker_model_cache_native_android_so();

    public static native int libfizz_so();

    public static native int libflatbuffers_so();

    public static native int libflatbuffersflatc_so();

    public static native int libfmt_so();

    public static native int libfolly_extended_light_so();

    public static native int libfolly_extended_so();

    public static native int libgazecorrection_model_cache_native_android_so();

    public static native int libgputimer_jni_so();

    public static native int libgraphbase_so();

    public static native int libgraphicsengine_arengineservices_messengereffectservicehost_native_so();

    public static native int libgraphicsengine_asyncscripting_native_so();

    public static native int libgraphicsengine_messenger_native_so();

    public static native int libgraphqlservice_so();

    public static native int libgraphservice_jni_asset_so();

    public static native int libgraphservice_jni_factory_so();

    public static native int libgraphservice_jni_mutations_so();

    public static native int libgraphservice_jni_nativeconfig_so();

    public static native int libgraphservice_jni_nativeconfigloader_so();

    public static native int libgraphservice_jni_nativeutil_so();

    public static native int libgraphservice_jni_serialization_so();

    public static native int libgraphservice_jni_so();

    public static native int libgraphservice_jni_tree_so();

    public static native int libgraphstore_so();

    public static native int libgraphstorecache_so();

    public static native int libgraphstorecachecurrent_so();

    public static native int libgraphstorecereal_so();

    public static native int libgraphstoresqlite_so();

    public static native int libgraphutil_so();

    public static native int libhairsegmentation_model_cache_native_android_so();

    public static native int libiopri_jni_so();

    public static native int libjniexecutors_so();

    public static native int libliger_native_so();

    public static native int libliger_so();

    public static native int liblive_query_jni_so();

    public static native int liblivefeedcommon_jni_so();

    public static native int liblivefeedservice_common_jni_so();

    public static native int liblocationdataprovider_so();

    public static native int libmediapipeline_filterlib_so();

    public static native int libmobileconfig_jni_so();

    public static native int libmodels_caffe2_so();

    public static native int libmodels_core_so();

    public static native int libmodels_evaluator_so();

    public static native int libmodels_gbdt_so();

    public static native int libmodels_so();

    public static native int libmqttlib_so();

    public static native int libmsuggestionscore_model_cache_native_android_so();

    public static native int libmultiplayerdataprovider_so();

    public static native int libmultiplayerservice_so();

    public static native int libomnistore_so();

    public static native int libomnistorecollections_so();

    public static native int libomnistoreexceptions_so();

    public static native int libomnistoreindexquery_so();

    public static native int libomnistoreopener_so();

    public static native int libomnistoresqliteandroid_so();

    public static native int libopus1_3_1_so();

    public static native int libperftestlogger_so();

    public static native int libprofilo_atrace_so();

    public static native int libprofilo_binder_so();

    public static native int libprofilo_breakpad_so();

    public static native int libprofilo_config_so();

    public static native int libprofilo_configjni_so();

    public static native int libprofilo_crash_dump_writer_so();

    public static native int libprofilo_dalvik_tracer_so();

    public static native int libprofilo_jni_helpers_so();

    public static native int libprofilo_libcio_so();

    public static native int libprofilo_mappings_so();

    public static native int libprofilo_memory_so();

    public static native int libprofilo_mmap_file_writer_so();

    public static native int libprofilo_mmapbuf_so();

    public static native int libprofilo_network_so();

    public static native int libprofilo_perfevents_so();

    public static native int libprofilo_so();

    public static native int libprofilo_stacktrace_artcompat_so();

    public static native int libprofilo_stacktrace_so();

    public static native int libprofilo_systemcounters_so();

    public static native int libprofilo_threadmetadata_so();

    public static native int libprofilo_util_so();

    public static native int libprofilomemory500_so();

    public static native int libprofilomemory511_so();

    public static native int libprofilomemory601_so();

    public static native int libprofilomemory700_so();

    public static native int libprofilomemory712_so();

    public static native int libprofilomemory800_so();

    public static native int libprofilomemory810_so();

    public static native int libprofiloprofilerunwindc500_so();

    public static native int libprofiloprofilerunwindc510_so();

    public static native int libprofiloprofilerunwindc600_so();

    public static native int libprofiloprofilerunwindc700_so();

    public static native int libprofiloprofilerunwindc710_so();

    public static native int libprofiloprofilerunwindc711_so();

    public static native int libprofiloprofilerunwindc712_so();

    public static native int libprofiloprofilerunwindc800_so();

    public static native int libprofiloprofilerunwindc810_so();

    public static native int libproxygen_http_so();

    public static native int libproxygen_lib_utils_compression_so();

    public static native int libproxygen_lib_utils_crypt_so();

    public static native int libproxygen_lib_utils_logging_so();

    public static native int libquic_so();

    public static native int librewritenativeinterceptor_so();

    public static native int librtc_analytics_so();

    public static native int librtcactivity_so();

    public static native int libsegmentation_model_cache_native_android_so();

    public static native int libsegmentationdataprovider_so();

    public static native int libsodium_so();

    public static native int libspatialaudio_so();

    public static native int libspectrum_so();

    public static native int libspectrumpluginjpeg_so();

    public static native int libspectrumpluginpng_so();

    public static native int libspectrumpluginwebp_so();

    public static native int libspeeddataprovider_so();

    public static native int libsslx_so();

    public static native int libtar_brotli_archive_native_so();

    public static native int libtargetrecognition_model_cache_native_android_so();

    public static native int libthird_party_LibSignalProtocolCV2_LibSignalProtocolCV2Android_so();

    public static native int libthird_party_librtmp_librtmpAndroid_so();

    public static native int libthird_party_microtar_microtarAndroid_so();

    public static native int libthird_party_ne10_ne10_dspAndroid_so();

    public static native int libtigonapi_so();

    public static native int libtigonliger_so();

    public static native int libtigonnativeservice_so();

    public static native int libtigonvideo_so();

    public static native int libusercrypto_so();

    public static native int libvalue_model_holder_jni_so();

    public static native int libverifier_so();

    public static native int libversioned_model_cache_native_android_so();

    public static native int libwangle_so();

    public static native int libworldtrackerdataprovider_so();

    public static native int libxanalyticsadapter_jni_so();

    public static native int libxplat_CompactDisk_CompactDiskAnalytics_CompactDiskAnalyticsAndroid_so();

    public static native int libxplat_CompactDisk_CompactDiskCommon_CompactDiskCommonAndroid_so();

    public static native int libxplat_CompactDisk_CompactDiskCurrent_CompactDiskCurrentAndroid_so();

    public static native int libxplat_DownloadService_DownloadRetryServiceAndroid_so();

    public static native int libxplat_DownloadService_DownloadServiceAndroid_so();

    public static native int libxplat_DownloadService_TigonDownloadServiceAndroid_so();

    public static native int libxplat_FBTarArchive_FBTarArchiveAndroid_so();

    public static native int libxplat_FBTarBrotliArchive_FBTarBrotliArchiveAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_AdaptiveGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_AnalyticsGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_AssetProvidingGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_BlackBoxGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_Fb4aApplicationHackGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLAnalyticsUtilsAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLAssetFactoryAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLBaseAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLCacheAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLConnectionsAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLConsistencyAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLFacebookAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLFlatbufferConnectionConfigurationResolverAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLGeneratedAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLOptimizerAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLParsingAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLSecretaryAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLSerializationAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_GraphQLServiceUtilAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_JsonParsingAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_LiveFeedGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_LiveGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_TigonGraphQLAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_TigonGraphQLBatchAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_TigonGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_UsedFieldTrackingAndroid_so();

    public static native int libxplat_GraphServices_GraphQL_UsedFieldTrackingGraphQLServiceAndroid_so();

    public static native int libxplat_GraphServices_GraphStore_GraphStoreParserAndroid_so();

    public static native int libxplat_IGL_src_igl_iglAndroid_so();

    public static native int libxplat_IGL_src_igl_opengl_openglAndroid_so();

    public static native int libxplat_LiveStreaming_LiveStreamingAdaptiveBitrateAndroid_so();

    public static native int libxplat_LiveStreaming_LiveStreamingAndroid_so();

    public static native int libxplat_LiveStreaming_LiveStreamingRTMPAndroid_so();

    public static native int libxplat_LiveStreaming_LiveStreamingTslogAndroid_so();

    public static native int libxplat_LiveStreaming_LiveStreamingVideoProtocolAndroid_so();

    public static native int libxplat_MobileCoreHealth_fbsofterror_fbsofterrorAndroid_so();

    public static native int libxplat_MobileCoreHealth_memorydebug_memorydebugAndroid_so();

    public static native int libxplat_Reachability_ReachabilityAndroid_so();

    public static native int libxplat_Statistics_StatisticsAndroid_so();

    public static native int libxplat_TigonLiger_TigonLigerAndroid_so();

    public static native int libxplat_TigonLiger_VPSTraceEventLoggerAndroid_so();

    public static native int libxplat_Tigon_TigonAndroid_so();

    public static native int libxplat_Tigon_TigonBackupHostServiceAndroid_so();

    public static native int libxplat_Tigon_TigonBodyProvidersAndroid_so();

    public static native int libxplat_Tigon_TigonCancellerAndroid_so();

    public static native int libxplat_Tigon_TigonIdServiceAndroid_so();

    public static native int libxplat_Tigon_TigonIgnoreCancelAndroid_so();

    public static native int libxplat_Tigon_TigonLayerSwitcherAndroid_so();

    public static native int libxplat_Tigon_TigonPoliciesAndroid_so();

    public static native int libxplat_Tigon_TigonPrimitivesAndroid_so();

    public static native int libxplat_Tigon_TigonPrivacyEnforcerAndroid_so();

    public static native int libxplat_Tigon_TigonQueuesAndroid_so();

    public static native int libxplat_Tigon_TigonRedirectorAndroid_so();

    public static native int libxplat_Tigon_TigonReliableMediaServiceAndroid_so();

    public static native int libxplat_Tigon_TigonRequestInterceptorsAndroid_so();

    public static native int libxplat_Tigon_TigonRetrierAndroid_so();

    public static native int libxplat_Tigon_TigonSecretaryAndroid_so();

    public static native int libxplat_Tigon_TigonStackAndroid_so();

    public static native int libxplat_Tigon_TigonSwapperAndroid_so();

    public static native int libxplat_Tigon_TigonSwitcherAndroid_so();

    public static native int libxplat_Tigon_TigonTimersAndroid_so();

    public static native int libxplat_arfx_delivery_AssetLoader_AsyncAssetLoader_AsyncAssetLoaderAndroid_so();

    public static native int libxplat_arfx_engine_animsamplers_animsamplersAndroid_so();

    public static native int libxplat_arfx_engine_asset_assetAndroid_so();

    public static native int libxplat_arfx_engine_asset_loader_utils_utilsAndroid_so();

    public static native int libxplat_arfx_engine_audio_audioAndroid_so();

    public static native int libxplat_arfx_engine_augmentations_augmentationsAndroid_so();

    public static native int libxplat_arfx_engine_common_composer_composerAndroid_so();

    public static native int libxplat_arfx_engine_common_geometry_geometryAndroid_so();

    public static native int libxplat_arfx_engine_common_reflection_reflectionAndroid_so();

    public static native int libxplat_arfx_engine_font_texture_provider_font_texture_providerAndroid_so();

    public static native int libxplat_arfx_engine_fx_fxAndroid_so();

    public static native int libxplat_arfx_engine_model_modelAndroid_so();

    public static native int libxplat_arfx_engine_model_v2_model2Android_so();

    public static native int libxplat_arfx_engine_model_v2_reactiveAndroid_so();

    public static native int libxplat_arfx_engine_platform_platformAndroid_so();

    public static native int libxplat_arfx_engine_plugins_configuration_configurationAndroid_so();

    public static native int libxplat_arfx_engine_plugins_configuration_messenger_messengerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_core_coreAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_async_assets_ARAssetParserAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_async_assets_ARAssetRequestResponseAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_async_assets_ARAsyncAssetControllersAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_async_assets_async_assetsAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_audio_delay_audio_delayAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_bit_crusher_bit_crusherAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_camera_control_camera_controlAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_deep_link_deep_linkAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_denoiser_denoiserAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_device_environment_device_environmentAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_device_motion_device_motionAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_distortion_distortionAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_dynamic_extrusion_dynamic_extrusionAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_dynamic_text_dynamic_textAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_extrusion_extrusionAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_face_movement_face_movementAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_face_tracking_2d_face_tracking_2dAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_face_tracking_face_trackingAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_fbidentity_fbidentityAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_font_fontAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_graphql_graphqlAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_hand_tracker_hand_trackerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_instruction_instructionAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_inter_effect_linking_inter_effect_linkingAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_iris_tracking_iris_trackingAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_ktx_ktxAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_lighting_estimation_lighting_estimationAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_modulator_modulatorAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_moving_target_tracker_moving_target_trackerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_msdf_texture_msdf_textureAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_multiplayer_multiplayerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_native_uicontrol_native_uicontrolAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_networking_networkingAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_object_tracker_object_trackerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_parametric_equalizer_parametric_equalizerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_particle_system_particle_systemAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_persistence_persistenceAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_personalization_personalizationAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_pitch_shifter_pitch_shifterAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_pkm_pkmAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_planar_object_planar_objectAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_planar_text_planar_textAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_plane_tracker_plane_trackerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_platform_events_platform_eventsAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_png_pngAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_point_tracker_point_trackerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_pvr_pvrAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_recognition_tracking_recognition_trackingAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_reverb_reverbAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_script_analytics_script_analyticsAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_scripting_scriptingAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_segmentation_segmentationAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_svg_svgAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_target_tracker_target_trackerAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_text_extrusion_text_extrusionAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_text_textAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_touch_gestures_touch_gesturesAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_vocoder_vocoderAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_world_tracker_utils_world_tracker_utilsAndroid_so();

    public static native int libxplat_arfx_engine_plugins_impl_zip_zipAndroid_so();

    public static native int libxplat_arfx_engine_reactive_reactiveAndroid_so();

    public static native int libxplat_arfx_engine_renderer_constant_constantAndroid_so();

    public static native int libxplat_arfx_engine_renderer_rendererAndroid_so();

    public static native int libxplat_arfx_engine_renderer_shaders_component_componentAndroid_so();

    public static native int libxplat_arfx_engine_renderer_shaders_components_componentsAndroid_so();

    public static native int libxplat_arfx_engine_renderer_shaders_types_typesAndroid_so();

    public static native int libxplat_arfx_engine_renderer_shaders_utils_utilsAndroid_so();

    public static native int libxplat_arfx_engine_scene_sceneAndroid_so();

    public static native int libxplat_arfx_engine_scene_sceneYogaNodeAndroid_so();

    public static native int libxplat_arfx_engine_shader_evaluator_evaluatorAndroid_so();

    public static native int libxplat_arfx_engine_shader_model_modelAndroid_so();

    public static native int libxplat_arfx_services_impl_Multiplayer_ARMultiplayerDataProviderAndroid_so();

    public static native int libxplat_arfx_services_impl_Multiplayer_ARMultiplayerServiceAndroid_so();

    public static native int libxplat_arfx_services_impl_WorldTracking_ARWorldTrackingDataProviderAndroid_so();

    public static native int libxplat_arfx_support_arclass_CommonAndroid_so();

    public static native int libxplat_arfx_support_arclass_GraphQLSourceAndroid_so();

    public static native int libxplat_arfx_support_feature_config_feature_configAndroid_so();

    public static native int libxplat_arfx_support_igl_android_device_android_deviceAndroid_so();

    public static native int libxplat_arfx_tracking_artech_modules_worldtracker_worldtrackerAndroid_so();

    public static native int libxplat_arfx_versioning_migrations_migrationsAndroid_so();

    public static native int libxplat_caffe2_caffe2Android_so();

    public static native int libxplat_caffe2_caffe2_mobile_contrib_libopencl_stub_libopencl_stubAndroid_so();

    public static native int libxplat_caffe2_caffe2_mobile_contrib_libvulkan_stub_libvulkan_stubAndroid_so();

    public static native int libxplat_caffe2_caffe2_mobile_fb_qpl_jni_jni_qplAndroid_so();

    public static native int libxplat_caffe2_nomnigraphAndroid_so();

    public static native int libxplat_compphoto_ocean_ocean_baseAndroid_so();

    public static native int libxplat_compphoto_ocean_ocean_geometryAndroid_so();

    public static native int libxplat_compphoto_ocean_ocean_mathAndroid_so();

    public static native int libxplat_compphoto_ocean_ocean_systemAndroid_so();

    public static native int libxplat_fbaudio_audio360Android_so();

    public static native int libxplat_fbaudio_audio360_coreAndroid_so();

    public static native int libxplat_fbaudio_audio360_decoderAndroid_so();

    public static native int libxplat_fbaudio_core_ambisonic_baseAndroid_so();

    public static native int libxplat_fbaudio_core_ambisonic_encodersAndroid_so();

    public static native int libxplat_fbaudio_core_ambisonic_renderersAndroid_so();

    public static native int libxplat_fbaudio_core_baseAndroid_so();

    public static native int libxplat_fbaudio_core_effectsAndroid_so();

    public static native int libxplat_fbaudio_core_spatial_effectsAndroid_so();

    public static native int libxplat_fbaudio_dspAndroid_so();

    public static native int libxplat_fbaudio_dsp_commonAndroid_so();

    public static native int libxplat_fbaudio_effectsAndroid_so();

    public static native int libxplat_fbaudio_fbaAndroid_so();

    public static native int libxplat_fbaudio_fba_asset_mgrAndroid_so();

    public static native int libxplat_fbaudio_fba_decoderAndroid_so();

    public static native int libxplat_fbaudio_fba_deviceAndroid_so();

    public static native int libxplat_fbaudio_fba_managerAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_ambisonicsAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_analyzerAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_bit_crusherAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_delayAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_denoiserAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_distortionAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_energy_meterAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_eqAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_pitch_shifterAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_reverbAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_stack_ring_modulatorAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_audio_toolboxAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_coreAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_effectsAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_fileAndroid_so();

    public static native int libxplat_fbaudio_fba_plugin_streamAndroid_so();

    public static native int libxplat_fbaudio_liaAndroid_so();

    public static native int libxplat_fbaudio_magic_headersAndroid_so();

    public static native int libxplat_fbaudio_oboeAndroid_so();

    public static native int libxplat_fbaudio_oggAndroid_so();

    public static native int libxplat_fbaudio_opusfileAndroid_so();

    public static native int libxplat_fbaudio_rendererAndroid_so();

    public static native int libxplat_fbaudio_utilsAndroid_so();

    public static native int libxplat_fbbrotli_fbbrotliAndroid_so();

    public static native int libxplat_folly_concurrency_cache_localityAndroid_so();

    public static native int libxplat_folly_container_detail_f14_hash_detailAndroid_so();

    public static native int libxplat_folly_convAndroid_so();

    public static native int libxplat_folly_demangleAndroid_so();

    public static native int libxplat_folly_detail_at_forkAndroid_so();

    public static native int libxplat_folly_detail_demangleAndroid_so();

    public static native int libxplat_folly_detail_futexAndroid_so();

    public static native int libxplat_folly_detail_ip_addressAndroid_so();

    public static native int libxplat_folly_detail_memory_idlerAndroid_so();

    public static native int libxplat_folly_detail_range_commonAndroid_so();

    public static native int libxplat_folly_detail_range_sse42Android_so();

    public static native int libxplat_folly_detail_singleton_stack_traceAndroid_so();

    public static native int libxplat_folly_detail_socket_fast_openAndroid_so();

    public static native int libxplat_folly_detail_sseAndroid_so();

    public static native int libxplat_folly_detail_static_singleton_managerAndroid_so();

    public static native int libxplat_folly_detail_thread_local_detailAndroid_so();

    public static native int libxplat_folly_detail_unique_instanceAndroid_so();

    public static native int libxplat_folly_dynamicAndroid_so();

    public static native int libxplat_folly_exception_wrapperAndroid_so();

    public static native int libxplat_folly_executorAndroid_so();

    public static native int libxplat_folly_executors_executor_with_priorityAndroid_so();

    public static native int libxplat_folly_executors_global_executorAndroid_so();

    public static native int libxplat_folly_executors_global_thread_pool_listAndroid_so();

    public static native int libxplat_folly_executors_inline_executorAndroid_so();

    public static native int libxplat_folly_executors_io_thread_pool_executorAndroid_so();

    public static native int libxplat_folly_executors_manual_executorAndroid_so();

    public static native int libxplat_folly_executors_queued_immediate_executorAndroid_so();

    public static native int libxplat_folly_executors_serial_executorAndroid_so();

    public static native int libxplat_folly_executors_thread_pool_executorAndroid_so();

    public static native int libxplat_folly_executors_timed_drivable_executorAndroid_so();

    public static native int libxplat_folly_experimental_io_fs_utilAndroid_so();

    public static native int libxplat_folly_experimental_test_utilAndroid_so();

    public static native int libxplat_folly_fileAndroid_so();

    public static native int libxplat_folly_file_utilAndroid_so();

    public static native int libxplat_folly_formatAndroid_so();

    public static native int libxplat_folly_futures_barrierAndroid_so();

    public static native int libxplat_folly_futures_coreAndroid_so();

    public static native int libxplat_folly_hash_spooky_hash_v1Android_so();

    public static native int libxplat_folly_hash_spooky_hash_v2Android_so();

    public static native int libxplat_folly_io_async_async_baseAndroid_so();

    public static native int libxplat_folly_io_async_async_pipeAndroid_so();

    public static native int libxplat_folly_io_async_async_signal_handlerAndroid_so();

    public static native int libxplat_folly_io_async_async_socketAndroid_so();

    public static native int libxplat_folly_io_async_async_socket_exceptionAndroid_so();

    public static native int libxplat_folly_io_async_async_ssl_socketAndroid_so();

    public static native int libxplat_folly_io_async_async_udp_socketAndroid_so();

    public static native int libxplat_folly_io_async_event_base_localAndroid_so();

    public static native int libxplat_folly_io_async_event_base_managerAndroid_so();

    public static native int libxplat_folly_io_async_request_contextAndroid_so();

    public static native int libxplat_folly_io_async_ssl_contextAndroid_so();

    public static native int libxplat_folly_io_async_ssl_openssl_utilsAndroid_so();

    public static native int libxplat_folly_io_async_ssl_optionsAndroid_so();

    public static native int libxplat_folly_io_async_ssl_ssl_errorsAndroid_so();

    public static native int libxplat_folly_io_iobufAndroid_so();

    public static native int libxplat_folly_io_shutdown_socket_setAndroid_so();

    public static native int libxplat_folly_jsonAndroid_so();

    public static native int libxplat_folly_json_pointerAndroid_so();

    public static native int libxplat_folly_lang_assumeAndroid_so();

    public static native int libxplat_folly_lang_safe_assertAndroid_so();

    public static native int libxplat_folly_memory_detail_malloc_implAndroid_so();

    public static native int libxplat_folly_memory_mallctl_helperAndroid_so();

    public static native int libxplat_folly_memory_sanitize_leakAndroid_so();

    public static native int libxplat_folly_micro_lockAndroid_so();

    public static native int libxplat_folly_net_detail_socket_file_descriptor_mapAndroid_so();

    public static native int libxplat_folly_net_net_opsAndroid_so();

    public static native int libxplat_folly_network_addressAndroid_so();

    public static native int libxplat_folly_portability_builtinsAndroid_so();

    public static native int libxplat_folly_portability_direntAndroid_so();

    public static native int libxplat_folly_portability_fcntlAndroid_so();

    public static native int libxplat_folly_portability_opensslAndroid_so();

    public static native int libxplat_folly_portability_pthreadAndroid_so();

    public static native int libxplat_folly_portability_schedAndroid_so();

    public static native int libxplat_folly_portability_socketsAndroid_so();

    public static native int libxplat_folly_portability_stringAndroid_so();

    public static native int libxplat_folly_portability_sys_fileAndroid_so();

    public static native int libxplat_folly_portability_sys_membarrierAndroid_so();

    public static native int libxplat_folly_portability_sys_mmanAndroid_so();

    public static native int libxplat_folly_portability_sys_resourceAndroid_so();

    public static native int libxplat_folly_portability_sys_statAndroid_so();

    public static native int libxplat_folly_portability_sys_timeAndroid_so();

    public static native int libxplat_folly_portability_sys_uioAndroid_so();

    public static native int libxplat_folly_portability_timeAndroid_so();

    public static native int libxplat_folly_portability_unistdAndroid_so();

    public static native int libxplat_folly_randomAndroid_so();

    public static native int libxplat_folly_shared_mutexAndroid_so();

    public static native int libxplat_folly_singletonAndroid_so();

    public static native int libxplat_folly_ssl_detail_openssl_threadingAndroid_so();

    public static native int libxplat_folly_ssl_initAndroid_so();

    public static native int libxplat_folly_ssl_openssl_cert_utilsAndroid_so();

    public static native int libxplat_folly_ssl_openssl_hashAndroid_so();

    public static native int libxplat_folly_stringAndroid_so();

    public static native int libxplat_folly_synchronization_asymmetric_memory_barrierAndroid_so();

    public static native int libxplat_folly_synchronization_hazptrAndroid_so();

    public static native int libxplat_folly_synchronization_parking_lotAndroid_so();

    public static native int libxplat_folly_system_hardware_concurrencyAndroid_so();

    public static native int libxplat_folly_system_thread_nameAndroid_so();

    public static native int libxplat_folly_unicodeAndroid_so();

    public static native int libxplat_folly_uriAndroid_so();

    public static native int libxplat_lithium_client_utilAndroid_so();

    public static native int libxplat_lithium_lithium_config_sourceAndroid_so();

    public static native int libxplat_lithium_live_query_client_pollerAndroid_so();

    public static native int libxplat_lithium_live_query_commonAndroid_so();

    public static native int libxplat_mobileconfig_FBMobileConfigCore_FBMobileConfigCoreAndroid_so();

    public static native int libxplat_mobileconfig_FBMobileConfigDefaultImpl_FBMobileConfigDefaultImplAndroid_so();

    public static native int libxplat_models_modelsAndroid_so();

    public static native int libxplat_models_models_cacheAndroid_so();

    public static native int libxplat_models_models_caffe2Android_so();

    public static native int libxplat_models_models_evaluatorAndroid_so();

    public static native int libxplat_models_models_evaluator_caffe2Android_so();

    public static native int libxplat_models_models_evaluator_gbdtAndroid_so();

    public static native int libxplat_models_models_graphqlAndroid_so();

    public static native int libxplat_models_models_tigonAndroid_so();

    public static native int libxplat_models_models_xanalyticsAndroid_so();

    public static native int libxplat_multifeed_ranking_core_libs_boolean_testAndroid_so();

    public static native int libxplat_multifeed_ranking_core_libs_fastfxlAndroid_so();

    public static native int libxplat_multifeed_ranking_core_value_model_value_modelAndroid_so();

    public static native int libxplat_multifeed_shared_sharedAndroid_so();

    public static native int libxplat_omnistore_client_collection_collectionAndroid_so();

    public static native int libxplat_omnistore_client_common_commonAndroid_so();

    public static native int libxplat_omnistore_client_fbsparserschema_fbsparserpoolschemaAndroid_so();

    public static native int libxplat_omnistore_client_fbsparserschema_fbsparserschema_commonAndroid_so();

    public static native int libxplat_omnistore_client_hash_hashAndroid_so();

    public static native int libxplat_omnistore_client_indexquery_indexqueryAndroid_so();

    public static native int libxplat_omnistore_client_nocollection_nocollectionAndroid_so();

    public static native int libxplat_omnistore_client_protocol_tigon_omnistore_tigonAndroid_so();

    public static native int libxplat_perflogger_hybrid_perfloggerAndroid_so();

    public static native int libxplat_perflogger_jniperfloggerAndroid_so();

    public static native int libxplat_perflogger_perfloggerAndroid_so();

    public static native int libxplat_perflogger_perfloggerinterfacesAndroid_so();

    public static native int libxplat_perflogger_structured_dataAndroid_so();

    public static native int libxplat_perfloggerbase_perfloggerbaseAndroid_so();

    public static native int libxplat_perfloggerbase_perfloggerutilAndroid_so();

    public static native int libxplat_proxygen_proxygen_parse_urlAndroid_so();

    public static native int libxplat_rsocket_rsocketAndroid_so();

    public static native int libxplat_rsocket_rsocket_tcpAndroid_so();

    public static native int libxplat_rtc_diagnostics_diagnosticsAndroid_so();

    public static native int libxplat_rtc_diagnostics_qpl_qplAndroid_so();

    public static native int libxplat_rtc_platform_turn_turnAndroid_so();

    public static native int libxplat_rtc_third_party_libvpx_libvpxAndroid_so();

    public static native int libxplat_sonar_xplat_FlipperAndroid_so();

    public static native int libxplat_sonar_xplat_plugins_facebook_SonarGraphQLPlugin_SonarGraphQLPluginAndroid_so();

    public static native int libxplat_spectrum_cpp_spectrumAndroid_so();

    public static native int libxplat_spectrum_cpp_spectrumJpegAndroid_so();

    public static native int libxplat_spectrum_cpp_spectrumPngAndroid_so();

    public static native int libxplat_spectrum_cpp_spectrumWebpDecodeAndroid_so();

    public static native int libxplat_sqlite_sqlite_commonAndroid_so();

    public static native int libxplat_structuredlogger_events_TigonRequestEventNoBuilderAndroid_so();

    public static native int libxplat_third_party_event_eventAndroid_so();

    public static native int libxplat_third_party_jpeg_jpeg_simd_neonAndroid_so();

    public static native int libxplat_third_party_lapack_lapackAndroid_so();

    public static native int libxplat_third_party_lapack_lapack_f2cAndroid_so();

    public static native int libxplat_third_party_mozjpeg_mozjpeg_simdAndroid_so();

    public static native int libxplat_third_party_opencv_opencv_coreAndroid_so();

    public static native int libxplat_third_party_opencv_opencv_imgprocAndroid_so();

    public static native int libxplat_third_party_opencv_opencv_photoAndroid_so();

    public static native int libxplat_third_party_opencv_opencv_videoAndroid_so();

    public static native int libxplat_third_party_yajl_yajlAndroid_so();

    public static native int libxplat_tslog_tslogAndroid_so();

    public static native int libxplat_tslog_tslog_follyAndroid_so();

    public static native int libxplat_tvm_tvm_runtimeAndroid_so();

    public static native int libxplat_wangle_acceptor_managedAndroid_so();

    public static native int libxplat_wangle_acceptor_socket_optionsAndroid_so();

    public static native int libxplat_wangle_acceptor_transport_infoAndroid_so();

    public static native int libxplat_wangle_ssl_ssl_utilAndroid_so();

    public static native int libxplat_yarpl_yarpl_internalAndroid_so();
}
