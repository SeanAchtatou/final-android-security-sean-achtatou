package com.facebook.soloader;

import X.AnonymousClass07B;
import android.os.Build;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeSet;

public final class SysUtil$LollipopSysdeps {
    public static void fallocateIfSupported(FileDescriptor fileDescriptor, long j) {
        try {
            Os.posix_fallocate(fileDescriptor, 0, j);
        } catch (ErrnoException e) {
            if (e.errno != OsConstants.EOPNOTSUPP && e.errno != OsConstants.ENOSYS && e.errno != OsConstants.EINVAL) {
                throw new IOException(e.toString(), e);
            }
        }
    }

    public static String[] getSupportedAbis() {
        String[] strArr = Build.SUPPORTED_ABIS;
        TreeSet treeSet = new TreeSet();
        try {
            if (Os.readlink("/proc/self/exe").contains("64")) {
                treeSet.add(A00(AnonymousClass07B.A0Y));
                treeSet.add(A00(AnonymousClass07B.A0N));
            } else {
                treeSet.add(A00(AnonymousClass07B.A0C));
                treeSet.add(A00(AnonymousClass07B.A01));
            }
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                if (treeSet.contains(str)) {
                    arrayList.add(str);
                }
            }
            return (String[]) arrayList.toArray(new String[arrayList.size()]);
        } catch (ErrnoException e) {
            Log.e("SysUtil", String.format("Could not read /proc/self/exe. Falling back to default ABI list: %s. errno: %d Err msg: %s", Arrays.toString(strArr), Integer.valueOf(e.errno), e.getMessage()));
            String[] strArr2 = Build.SUPPORTED_ABIS;
            return strArr;
        }
    }

    private SysUtil$LollipopSysdeps() {
    }

    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "x86";
            case 2:
                return "armeabi-v7a";
            case 3:
                return "x86_64";
            case 4:
                return "arm64-v8a";
            case 5:
                return "others";
            default:
                return "not_so";
        }
    }
}
