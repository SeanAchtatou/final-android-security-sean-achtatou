package com.facebook.soloader;

import X.AnonymousClass01q;
import android.util.Log;
import java.util.List;

public abstract class NativeLibrary {
    private Boolean A00 = true;
    private List A01;
    private boolean A02 = false;
    private final Object A03 = new Object();
    private volatile UnsatisfiedLinkError A04 = null;

    public void A02() {
    }

    public boolean A03() {
        synchronized (this.A03) {
            if (!this.A00.booleanValue()) {
                boolean z = this.A02;
                return z;
            }
            try {
                List<String> list = this.A01;
                if (list != null) {
                    for (String A08 : list) {
                        AnonymousClass01q.A08(A08);
                    }
                }
                A02();
                this.A02 = true;
                this.A01 = null;
            } catch (UnsatisfiedLinkError e) {
                Log.e("com.facebook.soloader.NativeLibrary", "Failed to load native lib (initial check): ", e);
                this.A04 = e;
                this.A02 = false;
                this.A00 = false;
                boolean z2 = this.A02;
                return z2;
            } catch (Throwable th) {
                Log.e("com.facebook.soloader.NativeLibrary", "Failed to load native lib (other error): ", th);
                this.A04 = new UnsatisfiedLinkError("Failed loading libraries");
                this.A04.initCause(th);
                this.A02 = false;
                this.A00 = false;
                boolean z22 = this.A02;
                return z22;
            }
            this.A00 = false;
            boolean z222 = this.A02;
            return z222;
        }
    }

    public NativeLibrary(List list) {
        this.A01 = list;
    }

    public void A01() {
        if (!A03()) {
            throw this.A04;
        }
    }
}
