package com.facebook.analytics.structuredlogger.base;

import X.AnonymousClass1Y3;
import X.AnonymousClass1ZE;
import X.AnonymousClass24B;
import X.C05100Xq;
import X.C06450bW;
import com.facebook.acra.ACRA;

public class USLEBaseShape0S0000000 extends C05100Xq {
    public final int A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public USLEBaseShape0S0000000(C06450bW r1, int i) {
        super(r1);
        this.A00 = i;
    }

    public static USLEBaseShape0S0000000 A00(AnonymousClass1ZE r2) {
        return new USLEBaseShape0S0000000(r2.A01("living_room"), 305);
    }

    public static USLEBaseShape0S0000000 A01(AnonymousClass1ZE r2) {
        return new USLEBaseShape0S0000000(r2.A01("mn_cowatch_event"), 418);
    }

    public static USLEBaseShape0S0000000 A02(AnonymousClass1ZE r2) {
        return new USLEBaseShape0S0000000(r2.A01("msgr_account_login_flow"), AnonymousClass1Y3.A3n);
    }

    public static USLEBaseShape0S0000000 A03(AnonymousClass1ZE r2) {
        return new USLEBaseShape0S0000000(r2.A01("notif_abnormal"), AnonymousClass1Y3.A3q);
    }

    public static USLEBaseShape0S0000000 A04(AnonymousClass1ZE r2) {
        return new USLEBaseShape0S0000000(r2.A01("rti_living_room"), 524);
    }

    public static USLEBaseShape0S0000000 A05(AnonymousClass1ZE r2) {
        return new USLEBaseShape0S0000000(r2.A01("stories_interactive_feedback"), AnonymousClass1Y3.A4k);
    }

    public USLEBaseShape0S0000000 A0H(Boolean bool) {
        A08("is_viewer_mo", bool);
        return this;
    }

    public USLEBaseShape0S0000000 A0I(String str) {
        A0D("client_token", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0J(String str) {
        A0D("entry_point", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0K(String str) {
        A0D("event", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0L(String str) {
        A0D("event_name", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0M(String str) {
        A0D(AnonymousClass24B.$const$string(87), str);
        return this;
    }

    public USLEBaseShape0S0000000 A0N(String str) {
        A0D("page_id", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0O(String str) {
        A0D("pigeon_reserved_keyword_module", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0P(String str) {
        A0D(ACRA.SESSION_ID_KEY, str);
        return this;
    }

    public USLEBaseShape0S0000000 A0Q(String str) {
        A0D("source", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0R(String str) {
        A0D("surface", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0S(String str) {
        A0D("thread_id", str);
        return this;
    }

    public USLEBaseShape0S0000000 A0T(String str) {
        A0D("thread_type", str);
        return this;
    }

    public void A0U(String str) {
        A0D("links_link_url", str);
    }
}
