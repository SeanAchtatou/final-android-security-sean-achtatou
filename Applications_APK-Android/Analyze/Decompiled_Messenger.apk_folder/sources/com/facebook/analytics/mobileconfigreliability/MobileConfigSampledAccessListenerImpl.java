package com.facebook.analytics.mobileconfigreliability;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1ZE;
import X.C04970Xc;
import X.C25071Yf;
import X.C84543zd;
import com.facebook.acra.ACRA;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.stringformat.StringFormatUtil;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
public final class MobileConfigSampledAccessListenerImpl implements C25071Yf {
    private static volatile MobileConfigSampledAccessListenerImpl A06;
    public AtomicReference A00 = new AtomicReference(BuildConfig.FLAVOR);
    public AnonymousClass0UN A01;
    public boolean A02;
    public final Object A03 = new Object();
    public final ArrayList A04 = new ArrayList();
    public volatile boolean A05;

    public static final MobileConfigSampledAccessListenerImpl A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (MobileConfigSampledAccessListenerImpl.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new MobileConfigSampledAccessListenerImpl(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static void A01(MobileConfigSampledAccessListenerImpl mobileConfigSampledAccessListenerImpl, long j, int i, boolean z, String str, String str2, String str3, boolean z2) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ap7, mobileConfigSampledAccessListenerImpl.A01)).A02("mobile_config_sampled_access", C04970Xc.A02), AnonymousClass1Y3.A3e);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0B("internal_sampling_rate", Integer.valueOf(i));
            uSLEBaseShape0S0000000.A08("is_default_fallback", Boolean.valueOf(z));
            uSLEBaseShape0S0000000.A0D("param_specifier", Long.toString(j));
            uSLEBaseShape0S0000000.A0D("status", str);
            uSLEBaseShape0S0000000.A0D("start_type", str2);
            uSLEBaseShape0S0000000.A0D("client_value", str3);
            uSLEBaseShape0S0000000.A08("is_using_translation_table", Boolean.valueOf(z2));
            uSLEBaseShape0S0000000.A0D("markers", (String) mobileConfigSampledAccessListenerImpl.A00.get());
            uSLEBaseShape0S0000000.A06();
        }
    }

    public void BfI(long j, int i, boolean z, String str, String str2, String str3, boolean z2) {
        if (!this.A02) {
            String str4 = str2;
            String str5 = str;
            String str6 = str3;
            boolean z3 = z2;
            long j2 = j;
            int i2 = i;
            boolean z4 = z;
            if (this.A05) {
                A01(this, j2, i2, z4, str5, str4, str6, z3);
                return;
            }
            synchronized (this.A03) {
                this.A04.add(new C84543zd(this, j2, i2, z4, str5, str4, str6, z3));
            }
        }
    }

    public void C9N(Integer num) {
        int i;
        synchronized (this.A03) {
            AtomicReference atomicReference = this.A00;
            Object obj = atomicReference.get();
            switch (num.intValue()) {
                case 1:
                    i = 2;
                    break;
                case 2:
                    i = 3;
                    break;
                case 3:
                    i = 4;
                    break;
                case 4:
                    i = 5;
                    break;
                case 5:
                    i = 6;
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    i = 7;
                    break;
                default:
                    i = 1;
                    break;
            }
            atomicReference.set(StringFormatUtil.formatStrLocaleSafe("%s%d,", obj, Integer.valueOf(i)));
        }
    }

    private MobileConfigSampledAccessListenerImpl(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(2, r3);
    }
}
