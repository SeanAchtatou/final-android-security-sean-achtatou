package com.facebook.analytics.timespent;

import X.AnonymousClass06B;
import X.AnonymousClass0UN;
import X.AnonymousClass0UQ;
import X.AnonymousClass0US;
import X.AnonymousClass0Ud;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1FM;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C04310Tq;
import X.C06230bA;
import X.C14250su;
import X.C24441Tq;
import X.C24451Tr;
import X.C25051Yd;
import javax.inject.Singleton;

@Singleton
public final class TimeSpentEventReporter implements C14250su, C24441Tq {
    private static volatile TimeSpentEventReporter A06;
    public AnonymousClass0UN A00;
    private long A01 = 0;
    private Boolean A02;
    private Boolean A03;
    private final AnonymousClass1FM A04;
    private final C04310Tq A05;

    public void onAfterDialtoneStateChanged(boolean z) {
    }

    public static final TimeSpentEventReporter A00(AnonymousClass1XY r5) {
        if (A06 == null) {
            synchronized (TimeSpentEventReporter.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A06 = new TimeSpentEventReporter(applicationInjector, AnonymousClass0UQ.A00(AnonymousClass1Y3.AR0, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static void A01(TimeSpentEventReporter timeSpentEventReporter) {
        A03(timeSpentEventReporter, 2, ((AnonymousClass0Ud) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B5j, timeSpentEventReporter.A00)).A0P);
    }

    public static void A02(TimeSpentEventReporter timeSpentEventReporter) {
        A03(timeSpentEventReporter, 0, ((AnonymousClass0Ud) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B5j, timeSpentEventReporter.A00)).A0K);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0024, code lost:
        if (r10 != 7) goto L_0x0026;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(com.facebook.analytics.timespent.TimeSpentEventReporter r9, int r10, long r11) {
        /*
            X.1FM r3 = r9.A04
            monitor-enter(r3)
            X.1FM r6 = r9.A04     // Catch:{ all -> 0x010a }
            r1 = 1000(0x3e8, double:4.94E-321)
            if (r10 == 0) goto L_0x000a
            goto L_0x0014
        L_0x000a:
            int[] r0 = r6.A07     // Catch:{ all -> 0x010a }
            if (r0 == 0) goto L_0x0026
            long r11 = r11 / r1
            X.4WG r4 = X.AnonymousClass1FM.A00(r6, r11, r10)     // Catch:{ all -> 0x010a }
            goto L_0x0027
        L_0x0014:
            r0 = 1
            if (r10 == r0) goto L_0x002a
            r0 = 2
            if (r10 == r0) goto L_0x000a
            r0 = 3
            if (r10 == r0) goto L_0x000a
            r0 = 5
            if (r10 == r0) goto L_0x000a
            r0 = 6
            if (r10 == r0) goto L_0x000a
            r0 = 7
            if (r10 == r0) goto L_0x000a
        L_0x0026:
            r4 = 0
        L_0x0027:
            if (r4 == 0) goto L_0x0108
            goto L_0x0089
        L_0x002a:
            long r11 = r11 / r1
            long r0 = r6.A03     // Catch:{ all -> 0x010a }
            int r2 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0026
            long r4 = r6.A02     // Catch:{ all -> 0x010a }
            long r0 = r11 - r4
            r7 = 1
            r4 = 0
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 < 0) goto L_0x0044
            r4 = 64
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x0044
            r4 = 0
            goto L_0x0048
        L_0x0044:
            X.4WG r4 = X.AnonymousClass1FM.A00(r6, r11, r7)     // Catch:{ all -> 0x010a }
        L_0x0048:
            int[] r8 = r6.A07     // Catch:{ all -> 0x010a }
            if (r8 != 0) goto L_0x0075
            r6.A03 = r11     // Catch:{ all -> 0x010a }
            r6.A02 = r11     // Catch:{ all -> 0x010a }
            r0 = 2
            int[] r5 = new int[r0]     // Catch:{ all -> 0x010a }
            r6.A07 = r5     // Catch:{ all -> 0x010a }
            r2 = 0
            r5[r2] = r7     // Catch:{ all -> 0x010a }
            r1 = 1
        L_0x0059:
            if (r1 >= r0) goto L_0x0060
            r5[r1] = r2     // Catch:{ all -> 0x010a }
            int r1 = r1 + 1
            goto L_0x0059
        L_0x0060:
            X.0bA r0 = r6.A04     // Catch:{ all -> 0x010a }
            if (r0 == 0) goto L_0x006a
            java.lang.String r0 = r0.A06()     // Catch:{ all -> 0x010a }
            r6.A05 = r0     // Catch:{ all -> 0x010a }
        L_0x006a:
            int r0 = r6.A01     // Catch:{ all -> 0x010a }
            int r0 = r0 + r7
            r6.A01 = r0     // Catch:{ all -> 0x010a }
            int r0 = r6.A00     // Catch:{ all -> 0x010a }
            int r0 = r0 + r7
            r6.A00 = r0     // Catch:{ all -> 0x010a }
            goto L_0x0027
        L_0x0075:
            int r5 = (int) r0     // Catch:{ all -> 0x010a }
            int r2 = r5 >> 5
            r1 = r8[r2]     // Catch:{ all -> 0x010a }
            r0 = r5 & 31
            int r0 = r7 << r0
            r0 = r0 | r1
            r8[r2] = r0     // Catch:{ all -> 0x010a }
            r6.A03 = r11     // Catch:{ all -> 0x010a }
            int r0 = r6.A00     // Catch:{ all -> 0x010a }
            int r0 = r0 + r7
            r6.A00 = r0     // Catch:{ all -> 0x010a }
            goto L_0x0027
        L_0x0089:
            X.0Tq r0 = r9.A05     // Catch:{ all -> 0x010a }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x010a }
            r5 = 1
            if (r0 == 0) goto L_0x0095
            r0 = 5
            if (r10 != r0) goto L_0x009c
        L_0x0095:
            java.lang.String r2 = "pre_login"
            X.0nb r0 = r4.A00     // Catch:{ all -> 0x010a }
            r0.A0E(r2, r5)     // Catch:{ all -> 0x010a }
        L_0x009c:
            r2 = 3
            int r1 = X.AnonymousClass1Y3.B5E     // Catch:{ all -> 0x010a }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x010a }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x010a }
            X.9yd r6 = (X.C211189yd) r6     // Catch:{ all -> 0x010a }
            java.lang.String r0 = r6.A01     // Catch:{ all -> 0x010a }
            if (r0 != 0) goto L_0x00da
            boolean r0 = r6.A03     // Catch:{ all -> 0x010a }
            if (r0 != 0) goto L_0x00da
            java.lang.Integer r1 = r6.A02     // Catch:{ all -> 0x010a }
            monitor-enter(r1)     // Catch:{ all -> 0x010a }
            java.lang.String r0 = r6.A01     // Catch:{ all -> 0x00d7 }
            if (r0 != 0) goto L_0x00d3
            boolean r0 = r6.A03     // Catch:{ all -> 0x00d7 }
            if (r0 != 0) goto L_0x00d3
            r6.A03 = r5     // Catch:{ all -> 0x00d7 }
            monitor-exit(r1)     // Catch:{ all -> 0x00d7 }
            int r1 = X.AnonymousClass1Y3.BKH     // Catch:{ all -> 0x010a }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x010a }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x010a }
            java.util.concurrent.ExecutorService r2 = (java.util.concurrent.ExecutorService) r2     // Catch:{ all -> 0x010a }
            X.9yc r1 = new X.9yc     // Catch:{ all -> 0x010a }
            r1.<init>(r6)     // Catch:{ all -> 0x010a }
            r0 = -1176567773(0xffffffffb9df0023, float:-4.2533976E-4)
            X.AnonymousClass07A.A02(r2, r1, r0)     // Catch:{ all -> 0x010a }
            goto L_0x00dd
        L_0x00d3:
            java.lang.String r2 = r6.A01     // Catch:{ all -> 0x00d7 }
            monitor-exit(r1)     // Catch:{ all -> 0x00d7 }
            goto L_0x00de
        L_0x00d7:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00d7 }
            throw r0     // Catch:{ all -> 0x010a }
        L_0x00da:
            java.lang.String r2 = r6.A01     // Catch:{ all -> 0x010a }
            goto L_0x00de
        L_0x00dd:
            r2 = 0
        L_0x00de:
            if (r2 == 0) goto L_0x00e7
            java.lang.String r1 = "google_ad_id"
            X.0nb r0 = r4.A00     // Catch:{ all -> 0x010a }
            r0.A0D(r1, r2)     // Catch:{ all -> 0x010a }
        L_0x00e7:
            r0 = r4
            X.4WG r0 = (X.AnonymousClass4WG) r0     // Catch:{ all -> 0x010a }
            X.0nb r2 = r0.A00     // Catch:{ all -> 0x010a }
            int r1 = X.AnonymousClass1Y3.BFX     // Catch:{ all -> 0x010a }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x010a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x010a }
            X.0tZ r0 = (X.AnonymousClass0tZ) r0     // Catch:{ all -> 0x010a }
            r0.A03(r2)     // Catch:{ all -> 0x010a }
            X.1Tr r0 = r4.A01     // Catch:{ all -> 0x010a }
            X.0US r0 = r0.A00     // Catch:{ all -> 0x010a }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x010a }
            com.facebook.analytics.DeprecatedAnalyticsLogger r1 = (com.facebook.analytics.DeprecatedAnalyticsLogger) r1     // Catch:{ all -> 0x010a }
            X.0nb r0 = r4.A00     // Catch:{ all -> 0x010a }
            r1.A07(r0)     // Catch:{ all -> 0x010a }
        L_0x0108:
            monitor-exit(r3)     // Catch:{ all -> 0x010a }
            return
        L_0x010a:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x010a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.analytics.timespent.TimeSpentEventReporter.A03(com.facebook.analytics.timespent.TimeSpentEventReporter, int, long):void");
    }

    public static boolean A04(TimeSpentEventReporter timeSpentEventReporter) {
        if (timeSpentEventReporter.A02 == null) {
            timeSpentEventReporter.A02 = Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, timeSpentEventReporter.A00)).Aem(285224483231244L));
        }
        return timeSpentEventReporter.A02.booleanValue();
    }

    public static boolean A05(TimeSpentEventReporter timeSpentEventReporter) {
        if (timeSpentEventReporter.A03 == null) {
            timeSpentEventReporter.A03 = Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, timeSpentEventReporter.A00)).Aem(285224483165707L));
        }
        return timeSpentEventReporter.A03.booleanValue();
    }

    public void Bra(long j) {
        long j2 = j / 1000;
        if (j2 > this.A01) {
            A03(this, 1, j);
            this.A01 = j2;
        }
    }

    public void BsO(long j) {
        long j2 = j / 1000;
        if (j2 > this.A01) {
            A03(this, 1, j);
            this.A01 = j2;
        }
    }

    public void onBeforeDialtoneStateChanged(boolean z) {
        A03(this, 6, ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A00)).now());
    }

    private TimeSpentEventReporter(AnonymousClass1XY r6, AnonymousClass0US r7) {
        this.A00 = new AnonymousClass0UN(6, r6);
        this.A05 = AnonymousClass0XJ.A0K(r6);
        this.A04 = new AnonymousClass1FM(new C24451Tr(r7), (C06230bA) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BTX, this.A00));
    }
}
