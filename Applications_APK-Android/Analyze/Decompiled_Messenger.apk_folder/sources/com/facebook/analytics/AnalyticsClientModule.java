package com.facebook.analytics;

import X.AnonymousClass00J;
import X.AnonymousClass062;
import X.AnonymousClass067;
import X.AnonymousClass069;
import X.AnonymousClass0UN;
import X.AnonymousClass0UQ;
import X.AnonymousClass0UV;
import X.AnonymousClass0UX;
import X.AnonymousClass0V4;
import X.AnonymousClass0V7;
import X.AnonymousClass0VB;
import X.AnonymousClass0VM;
import X.AnonymousClass0VS;
import X.AnonymousClass0WD;
import X.AnonymousClass0WT;
import X.AnonymousClass146;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YA;
import X.AnonymousClass1ZE;
import X.AnonymousClass1ZF;
import X.C06210b4;
import X.C06310bI;
import X.C06340bL;
import X.C06470bY;
import X.C06480bZ;
import X.C06830c9;
import X.C06840cA;
import X.C06920cI;
import X.C08720fq;
import X.C1064257g;
import X.C25051Yd;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.facebook.analytics.counterlogger.CommunicationScheduler;
import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.concurrent.ScheduledExecutorService;

@InjectorModule
public class AnalyticsClientModule extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static final Object A02 = new Object();
    private static final Object A03 = new Object();
    private static volatile C06340bL A04;
    private static volatile C06340bL A05;
    private static volatile DeprecatedAnalyticsLogger A06;
    private static volatile CommunicationScheduler A07;
    private static volatile AnonymousClass1ZE A08;
    private static volatile C06840cA A09;
    private static volatile C06480bZ A0A;
    private static volatile ScheduledExecutorService A0B;

    public class AnalyticsClientModuleSelendroidInjector implements AnonymousClass062 {
        public AnonymousClass0UN A00;

        public DeprecatedAnalyticsLogger getAnalyticsLogger() {
            return (DeprecatedAnalyticsLogger) AnonymousClass1XX.A03(AnonymousClass1Y3.AR0, this.A00);
        }

        public AnalyticsClientModuleSelendroidInjector(Context context) {
            this.A00 = new AnonymousClass0UN(0, AnonymousClass1XX.get(context));
        }
    }

    public static final C06340bL A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C06340bL(AnonymousClass0VB.A00(AnonymousClass1Y3.APl, r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final C06340bL A01(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C06340bL(AnonymousClass0VB.A00(AnonymousClass1Y3.APl, r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final DeprecatedAnalyticsLogger A02(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (DeprecatedAnalyticsLogger.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = (DeprecatedAnalyticsLogger) AnonymousClass0VB.A00(AnonymousClass1Y3.Avo, r4.getApplicationInjector()).get();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static final CommunicationScheduler A03(AnonymousClass1XY r6) {
        if (A07 == null) {
            synchronized (CommunicationScheduler.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        DeprecatedAnalyticsLogger A003 = C06920cI.A00(applicationInjector);
                        AnonymousClass069 A042 = AnonymousClass067.A04(applicationInjector);
                        AnonymousClass069 A052 = AnonymousClass067.A05(applicationInjector);
                        A0B(applicationInjector);
                        A07 = new CommunicationScheduler(A003, A042, A052);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static final AnonymousClass1ZE A04(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass1ZE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass1ZE(AnonymousClass1ZF.A02(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static final C06840cA A06(AnonymousClass1XY r5) {
        if (A09 == null) {
            synchronized (C06840cA.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        A09 = new C06210b4(AnonymousClass00J.A01(A003), AnonymousClass0UQ.A00(AnonymousClass1Y3.BT8, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static final C06480bZ A07(AnonymousClass1XY r5) {
        if (A0A == null) {
            synchronized (A02) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0A = new C06480bZ(C06470bY.A00(applicationInjector).A00, AnonymousClass0UX.A0V(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static final Class A09() {
        return NewAnalyticsSamplingPolicyConfig.class;
    }

    public static final ScheduledExecutorService A0B(AnonymousClass1XY r5) {
        if (A0B == null) {
            synchronized (A03) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0B, r5);
                if (A002 != null) {
                    try {
                        A0B = AnonymousClass0VM.A00(r5.getApplicationInjector()).A04(AnonymousClass0VS.NORMAL, "CounterLogger-");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0B;
    }

    public static final C06830c9 A05(AnonymousClass1XY r7) {
        C1064257g r0;
        C25051Yd A012 = AnonymousClass0WT.A01(r7);
        AnonymousClass0V4 A002 = AnonymousClass0V4.A00(r7);
        if (!A012.Aem(18296552091092121L)) {
            return new C06310bI();
        }
        HandlerThread A022 = A002.A02("event-throttler", AnonymousClass0V7.NORMAL);
        A022.start();
        Handler handler = new Handler(A022.getLooper());
        long At0 = A012.At0(18578027067933191L);
        int At02 = (int) A012.At0(18578027067867654L);
        boolean Aem = A012.Aem(18296552091026584L);
        synchronized (C1064257g.class) {
            if (C1064257g.A08 == null) {
                C1064257g.A08 = new C1064257g(handler, At0, At02, Aem);
            }
            r0 = C1064257g.A08;
        }
        return r0;
    }

    public static final Boolean A08(AnonymousClass1XY r0) {
        return Boolean.valueOf(AnonymousClass146.A00(r0).A08);
    }

    public static final Long A0A(AnonymousClass1XY r3) {
        return Long.valueOf(FbSharedPreferencesModule.A00(r3).At2(C08720fq.A09, 3600000));
    }
}
