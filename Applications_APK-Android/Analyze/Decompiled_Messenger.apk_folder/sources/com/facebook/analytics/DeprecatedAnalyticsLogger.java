package com.facebook.analytics;

import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass0WY;
import X.AnonymousClass0Xd;
import X.AnonymousClass0ZF;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YI;
import X.C010708t;
import X.C04310Tq;
import X.C04980Xe;
import X.C06230bA;
import X.C06390bQ;
import X.C11670nb;
import X.C14700tq;
import X.C183011q;
import X.C22361La;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
public final class DeprecatedAnalyticsLogger {
    public static final C06390bQ A04 = C06390bQ.A00();
    private static volatile DeprecatedAnalyticsLogger A05;
    public AnonymousClass0UN A00;
    public String A01;
    public final C04310Tq A02;
    private final C04980Xe A03;

    public static final DeprecatedAnalyticsLogger A00(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (DeprecatedAnalyticsLogger.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new DeprecatedAnalyticsLogger(applicationInjector, AnonymousClass0Xd.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private AnonymousClass0ZF A01(C11670nb r4, boolean z, boolean z2) {
        return ((C06230bA) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BTX, this.A00)).A05(r4.A05, z, AnonymousClass07B.A00, z2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00cd, code lost:
        if (X.AnonymousClass0YO.A00.contains(r1) == false) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0142, code lost:
        r0 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A02(X.C11670nb r9, X.AnonymousClass0ZF r10) {
        /*
            r8 = this;
            java.lang.String r1 = "buildAndDispatch"
            r0 = -1030526884(0xffffffffc293685c, float:-73.70383)
            X.AnonymousClass06K.A01(r1, r0)
            int r2 = X.AnonymousClass1Y3.Aug     // Catch:{ all -> 0x0155 }
            X.0UN r1 = r8.A00     // Catch:{ all -> 0x0155 }
            r0 = 7
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x0155 }
            X.0oI r0 = (X.C11950oI) r0     // Catch:{ all -> 0x0155 }
            boolean r0 = r0.A02     // Catch:{ all -> 0x0155 }
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0065
            r4 = r8
            monitor-enter(r4)     // Catch:{ all -> 0x0155 }
            java.lang.String r1 = r8.A01     // Catch:{ all -> 0x0142 }
            if (r9 == 0) goto L_0x0026
            java.lang.String r3 = r9.A07     // Catch:{ all -> 0x0142 }
            java.lang.String r0 = "AUTO_SET"
            if (r3 == r0) goto L_0x0026
            goto L_0x002e
        L_0x0026:
            X.0Tq r0 = r8.A02     // Catch:{ all -> 0x0142 }
            java.lang.Object r3 = r0.get()     // Catch:{ all -> 0x0142 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0142 }
        L_0x002e:
            boolean r0 = com.google.common.base.Objects.equal(r1, r3)     // Catch:{ all -> 0x0142 }
            if (r0 != 0) goto L_0x0064
            r8.A01 = r3     // Catch:{ all -> 0x0142 }
            r2 = 2
            if (r3 != 0) goto L_0x004e
            int r1 = X.AnonymousClass1Y3.AuX     // Catch:{ all -> 0x0142 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x0142 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0142 }
            X.0bD r1 = (X.C06260bD) r1     // Catch:{ all -> 0x0142 }
            monitor-enter(r1)     // Catch:{ all -> 0x0142 }
            X.0bE r0 = r1.A02     // Catch:{ all -> 0x004b }
            r0.A02()     // Catch:{ all -> 0x004b }
            monitor-exit(r1)     // Catch:{ all -> 0x0142 }
            goto L_0x0064
        L_0x004b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0142 }
            throw r0     // Catch:{ all -> 0x0142 }
        L_0x004e:
            int r1 = X.AnonymousClass1Y3.AuX     // Catch:{ all -> 0x0142 }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x0142 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0142 }
            X.0bD r2 = (X.C06260bD) r2     // Catch:{ all -> 0x0142 }
            r1 = 0
            if (r3 == 0) goto L_0x0061
            X.23I r0 = new X.23I     // Catch:{ all -> 0x0142 }
            r0.<init>(r3, r3, r1)     // Catch:{ all -> 0x0142 }
            r1 = r0
        L_0x0061:
            r2.A02(r1)     // Catch:{ all -> 0x0142 }
        L_0x0064:
            monitor-exit(r4)     // Catch:{ all -> 0x0155 }
        L_0x0065:
            java.lang.String r1 = r9.A06     // Catch:{ all -> 0x0155 }
            java.lang.String r0 = "AUTO_SET"
            if (r1 == r0) goto L_0x006e
            r10.A07(r1)     // Catch:{ all -> 0x0155 }
        L_0x006e:
            long r1 = r9.A02     // Catch:{ all -> 0x0155 }
            r3 = -1
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0079
            r10.A06(r1)     // Catch:{ all -> 0x0155 }
        L_0x0079:
            X.0os r1 = r10.A0B()     // Catch:{ all -> 0x0155 }
            com.fasterxml.jackson.databind.node.ObjectNode r0 = r9.A04     // Catch:{ all -> 0x0155 }
            if (r0 == 0) goto L_0x0092
            X.C11960oJ.A01(r0, r1)     // Catch:{ IllegalArgumentException -> 0x0085 }
            goto L_0x0092
        L_0x0085:
            r2 = move-exception
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0155 }
            com.fasterxml.jackson.databind.node.ObjectNode r0 = r9.A04     // Catch:{ all -> 0x0155 }
            java.lang.String r0 = r0.asText()     // Catch:{ all -> 0x0155 }
            r1.<init>(r0, r2)     // Catch:{ all -> 0x0155 }
            throw r1     // Catch:{ all -> 0x0155 }
        L_0x0092:
            long r1 = r9.A01     // Catch:{ all -> 0x0155 }
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x009d
            r10.A05 = r1     // Catch:{ all -> 0x0155 }
            r0 = 1
            r10.A0E = r0     // Catch:{ all -> 0x0155 }
        L_0x009d:
            com.fasterxml.jackson.databind.node.ArrayNode r4 = r9.A03     // Catch:{ all -> 0x0155 }
            if (r4 == 0) goto L_0x00c0
            int r3 = r4.size()     // Catch:{ all -> 0x0155 }
            X.0os r1 = r10.A0B()     // Catch:{ all -> 0x0155 }
            java.lang.String r0 = "enabled_features"
            X.0xz r2 = r1.A0E(r0)     // Catch:{ all -> 0x0155 }
            r1 = 0
        L_0x00b0:
            if (r1 >= r3) goto L_0x00c0
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r1)     // Catch:{ all -> 0x0155 }
            java.lang.String r0 = r0.asText()     // Catch:{ all -> 0x0155 }
            X.C16910xz.A00(r2, r0)     // Catch:{ all -> 0x0155 }
            int r1 = r1 + 1
            goto L_0x00b0
        L_0x00c0:
            java.lang.String r1 = r9.A05     // Catch:{ all -> 0x0155 }
            boolean r0 = X.AnonymousClass0YO.A01     // Catch:{ all -> 0x0155 }
            if (r0 == 0) goto L_0x00cf
            java.util.List r0 = X.AnonymousClass0YO.A00     // Catch:{ all -> 0x0155 }
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x0155 }
            r0 = 1
            if (r1 != 0) goto L_0x00d0
        L_0x00cf:
            r0 = 0
        L_0x00d0:
            if (r0 == 0) goto L_0x014b
            java.lang.String r4 = "CTScanV2Event"
            X.0bQ r0 = com.facebook.analytics.DeprecatedAnalyticsLogger.A04     // Catch:{ all -> 0x0155 }
            X.0os r3 = r0.A02()     // Catch:{ all -> 0x0155 }
            java.lang.String r1 = "name"
            java.lang.String r0 = r9.A05     // Catch:{ IOException -> 0x0136 }
            r3.A0K(r1, r0)     // Catch:{ IOException -> 0x0136 }
            java.lang.String r7 = "time"
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ IOException -> 0x0136 }
            X.0UN r0 = r8.A00     // Catch:{ IOException -> 0x0136 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IOException -> 0x0136 }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ IOException -> 0x0136 }
            long r5 = r0.now()     // Catch:{ IOException -> 0x0136 }
            r0 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r0
            java.lang.Long r0 = java.lang.Long.valueOf(r5)     // Catch:{ IOException -> 0x0136 }
            r3.A0J(r7, r0)     // Catch:{ IOException -> 0x0136 }
            java.lang.String r0 = r10.A0D()     // Catch:{ IOException -> 0x0136 }
            if (r0 == 0) goto L_0x010b
            java.lang.String r1 = "module"
            java.lang.String r0 = r10.A0D()     // Catch:{ IOException -> 0x0136 }
            r3.A0K(r1, r0)     // Catch:{ IOException -> 0x0136 }
        L_0x010b:
            java.io.StringWriter r2 = new java.io.StringWriter     // Catch:{ IOException -> 0x0136 }
            r2.<init>()     // Catch:{ IOException -> 0x0136 }
            java.lang.String r0 = "{"
            r2.append(r0)     // Catch:{ IOException -> 0x0136 }
            X.0oA r0 = X.C11910oA.A00()     // Catch:{ IOException -> 0x0136 }
            r0.A04(r2, r3)     // Catch:{ IOException -> 0x0136 }
            java.lang.String r0 = ",\"extra\":{"
            r2.append(r0)     // Catch:{ IOException -> 0x0136 }
            X.0oA r1 = X.C11910oA.A00()     // Catch:{ IOException -> 0x0136 }
            X.0os r0 = r10.A0B()     // Catch:{ IOException -> 0x0136 }
            r1.A04(r2, r0)     // Catch:{ IOException -> 0x0136 }
            java.lang.String r0 = "} }"
            r2.append(r0)     // Catch:{ IOException -> 0x0136 }
            java.lang.String r0 = r2.toString()     // Catch:{ IOException -> 0x0136 }
            goto L_0x0145
        L_0x0136:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x013d }
            r0.<init>(r1)     // Catch:{ all -> 0x013d }
            throw r0     // Catch:{ all -> 0x013d }
        L_0x013d:
            r0 = move-exception
            r3.A06()     // Catch:{ all -> 0x0155 }
            goto L_0x0144
        L_0x0142:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0155 }
        L_0x0144:
            throw r0     // Catch:{ all -> 0x0155 }
        L_0x0145:
            r3.A06()     // Catch:{ all -> 0x0155 }
            X.C010708t.A0J(r4, r0)     // Catch:{ all -> 0x0155 }
        L_0x014b:
            r10.A0E()     // Catch:{ all -> 0x0155 }
            r0 = -335795448(0xffffffffebfc2b08, float:-6.0970503E26)
            X.AnonymousClass06K.A00(r0)
            return
        L_0x0155:
            r1 = move-exception
            r0 = 1718224972(0x666a084c, float:2.762967E23)
            X.AnonymousClass06K.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.analytics.DeprecatedAnalyticsLogger.A02(X.0nb, X.0ZF):void");
    }

    private boolean A03(String str, boolean z) {
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A0t, false)) {
            return true;
        }
        if (this.A03.A02(str, z).A00 == 0) {
            return false;
        }
        boolean contains = C14700tq.A00.contains(str);
        if (!contains) {
            C010708t.A0K("com.facebook.analytics.DeprecatedAnalyticsLogger", AnonymousClass08S.A0J(str, " is not allowed to log through legacy framework (https://fburl.com/69wk2eki). Please use USL fburl.com/usl."));
        }
        return contains;
    }

    public C22361La A04(String str, boolean z) {
        return new C183011q(((C06230bA) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BTX, this.A00)).A05(str, z, AnonymousClass07B.A00, false), this.A03.A02(str, z).A00);
    }

    public void A05(C11670nb r2) {
        if (r2 == null) {
            return;
        }
        if (r2.A08) {
            A06(r2);
        } else {
            A07(r2);
        }
    }

    public void A06(C11670nb r3) {
        if (r3 != null && A03(r3.A05, true)) {
            AnonymousClass0ZF A012 = A01(r3, true, true);
            if (A012.A0G()) {
                A02(r3, A012);
            }
        }
    }

    public void A07(C11670nb r4) {
        boolean z;
        if (r4 != null && A03(r4.A05, true)) {
            Map A002 = C11670nb.A00(r4, false);
            if (A002 != null) {
                z = A002.containsKey("upload_this_event_now");
            } else {
                z = false;
            }
            AnonymousClass0ZF A012 = A01(r4, true, z);
            if (A012.A0G()) {
                A02(r4, A012);
            }
        }
    }

    public void A08(C11670nb r5) {
        boolean z;
        if (r5 != null && A03(r5.A05, true)) {
            Map A002 = C11670nb.A00(r5, false);
            if (A002 != null) {
                z = A002.containsKey("upload_this_event_now");
            } else {
                z = false;
            }
            A02(r5, ((C06230bA) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BTX, this.A00)).A04(r5.A05, AnonymousClass07B.A00, z));
        }
    }

    public void A09(C11670nb r4) {
        boolean z;
        if (r4 != null && A03(r4.A05, false)) {
            Map A002 = C11670nb.A00(r4, false);
            if (A002 != null) {
                z = A002.containsKey("upload_this_event_now");
            } else {
                z = false;
            }
            AnonymousClass0ZF A012 = A01(r4, false, z);
            if (A012.A0G()) {
                A02(r4, A012);
            }
        }
    }

    private DeprecatedAnalyticsLogger(AnonymousClass1XY r3, C04980Xe r4) {
        this.A00 = new AnonymousClass0UN(8, r3);
        this.A02 = AnonymousClass0WY.A02(r3);
        this.A03 = r4;
    }
}
