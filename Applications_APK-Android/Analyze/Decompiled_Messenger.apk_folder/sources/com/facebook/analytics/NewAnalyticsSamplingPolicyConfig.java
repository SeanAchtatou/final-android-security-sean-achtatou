package com.facebook.analytics;

import X.AnonymousClass062;
import X.AnonymousClass0UN;
import X.AnonymousClass0WY;
import X.AnonymousClass0d3;
import X.AnonymousClass0jJ;
import X.AnonymousClass16O;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YI;
import X.AnonymousClass829;
import X.AnonymousClass99A;
import X.C010708t;
import X.C04310Tq;
import X.C05030Xj;
import X.C06500bb;
import X.C08720fq;
import X.C09400hF;
import X.C09420hL;
import X.C101434sX;
import X.C12240os;
import X.C188215g;
import X.C56192pV;
import X.C633935w;
import android.content.Context;
import android.text.TextUtils;
import com.facebook.flexiblesampling.SamplingPolicyConfig;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.google.common.base.Objects;
import io.card.payment.BuildConfig;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Set;

public class NewAnalyticsSamplingPolicyConfig implements SamplingPolicyConfig, AnonymousClass062 {
    public AnonymousClass0UN A00;
    public C04310Tq A01;

    public void BDO() {
        AnonymousClass16O A06 = ((C05030Xj) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A73, this.A00)).A02.A06();
        A06.A08("__fs_policy_config_checksum__");
        A06.A05();
    }

    public String ByW() {
        return ((C09400hF) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BMV, ((AnonymousClass0d3) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AwX, this.A00)).A00)).A02();
    }

    public String ByY() {
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B50, this.A00)).AbO(0, false) || Objects.equal(((C05030Xj) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A73, this.A00)).A01.A02(C08720fq.A0H.A05(), null), this.A01.get())) {
            return ((C05030Xj) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A73, this.A00)).A01.A02("__fs_policy_config_checksum__", BuildConfig.FLAVOR);
        }
        return BuildConfig.FLAVOR;
    }

    public void ByZ(C12240os r5) {
        r5.A0K("config_version", "v2");
        Set set = (Set) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BJo, this.A00);
        if (set != null) {
            Iterator it = set.iterator();
            while (it.hasNext()) {
                it.next();
                r5.A0K("qpl_config_version", "v2");
            }
        }
    }

    public String Byb() {
        return (String) this.A01.get();
    }

    public void CKg(InputStream inputStream) {
        AnonymousClass0jJ r2 = (AnonymousClass0jJ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AmL, this.A00);
        JsonNode jsonNode = (JsonNode) r2._readMapAndClose(r2._jsonFactory.createParser(inputStream), AnonymousClass0jJ.JSON_NODE_TYPE);
        if (jsonNode == null) {
            jsonNode = NullNode.instance;
        }
        if (jsonNode == null) {
            C010708t.A0K("NewAnalyticsSamplingPolicyConfig", "No content from Http response");
            return;
        }
        JsonNode jsonNode2 = jsonNode.get("checksum");
        JsonNode jsonNode3 = jsonNode.get("config");
        if (jsonNode2 == null || jsonNode3 == null) {
            C010708t.A0Q("NewAnalyticsSamplingPolicyConfig", "Incomplete response: %s", jsonNode.toString());
            return;
        }
        JsonNode jsonNode4 = jsonNode.get("config_owner_id");
        C05030Xj r4 = (C05030Xj) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A73, this.A00);
        String asText = jsonNode2.asText();
        String asText2 = jsonNode3.asText();
        String str = null;
        if (jsonNode.get("app_data") != null) {
            jsonNode.get("app_data").asText();
        }
        if (jsonNode4 != null) {
            str = jsonNode4.asText();
        }
        C633935w r7 = new C633935w(asText, asText2, str);
        if (!TextUtils.isEmpty(r7.A00)) {
            C06500bb r8 = r4.A00;
            if (!TextUtils.isEmpty(r8.A00.A02("__fs_policy_config_checksum__", BuildConfig.FLAVOR))) {
                AnonymousClass829 r22 = new AnonymousClass829();
                if (r8.A02 != null) {
                    AnonymousClass829 r1 = r8.A02;
                    r8.A01.A08();
                    synchronized (r1) {
                    }
                }
                r8.A02 = r22;
            }
            r4.A04(r7.A00, r7.A02, r7.A01);
        }
        JsonNode jsonNode5 = jsonNode.get("app_data");
        if (jsonNode5 != null) {
            JsonNode jsonNode6 = ((AnonymousClass0jJ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AmL, this.A00)).readTree(jsonNode5.asText()).get("pigeon_internal");
            if (jsonNode6 != null && jsonNode6.get("regenerate_deviceid") != null) {
                C101434sX r6 = (C101434sX) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A2y, this.A00);
                C09420hL B6J = r6.A01.B6J();
                C09420hL r42 = new C09420hL(C188215g.A00().toString(), r6.A00.now());
                r6.A01.CCU(new C09420hL(r42.A01, (r6.A02.nextLong() % 86400000) + 1262376061000L));
                for (C56192pV onChanged : r6.A03) {
                    onChanged.onChanged(B6J, r42, AnonymousClass99A.A02, null);
                }
            }
        }
    }

    public NewAnalyticsSamplingPolicyConfig(Context context) {
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A00 = new AnonymousClass0UN(6, r2);
        this.A01 = AnonymousClass0WY.A02(r2);
    }
}
