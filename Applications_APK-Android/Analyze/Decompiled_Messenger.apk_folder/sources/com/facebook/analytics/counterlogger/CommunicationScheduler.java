package com.facebook.analytics.counterlogger;

import X.AnonymousClass069;
import X.AnonymousClass80H;
import X.C192417j;
import X.C22361La;
import X.C50412dw;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.HashMap;
import java.util.Map;

public final class CommunicationScheduler {
    public int A00 = 0;
    public long A01;
    public long A02;
    public final AnonymousClass069 A03;
    public final AnonymousClass069 A04;
    public final Object A05 = new Object();
    public final Map A06 = new HashMap();
    public final Map A07 = new HashMap();
    private final DeprecatedAnalyticsLogger A08;

    public static void A01(CommunicationScheduler communicationScheduler, boolean z) {
        C50412dw r4;
        synchronized (communicationScheduler.A05) {
            synchronized (communicationScheduler.A05) {
                try {
                    long now = communicationScheduler.A04.now();
                    long j = communicationScheduler.A02;
                    communicationScheduler.A02 = now;
                    long now2 = communicationScheduler.A03.now();
                    long j2 = communicationScheduler.A01;
                    communicationScheduler.A01 = now2;
                    int i = communicationScheduler.A00;
                    communicationScheduler.A00 = i + 1;
                    r4 = new C50412dw(j, now, j2, now2, z, i);
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                    throw th;
                }
            }
            synchronized (communicationScheduler.A05) {
                try {
                    communicationScheduler.A00(r4, communicationScheduler.A07, false);
                    communicationScheduler.A00(r4, communicationScheduler.A06, true);
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
        }
        return;
        throw th;
    }

    public CommunicationScheduler(DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, AnonymousClass069 r4, AnonymousClass069 r5) {
        this.A08 = deprecatedAnalyticsLogger;
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r5.now();
        this.A01 = this.A03.now();
    }

    private void A00(C50412dw r9, Map map, boolean z) {
        for (Map.Entry entry : map.entrySet()) {
            C22361La A042 = this.A08.A04((String) entry.getKey(), z);
            boolean z2 = false;
            for (Map.Entry entry2 : ((Map) entry.getValue()).entrySet()) {
                if (A042.A0B()) {
                    JsonNode A002 = ((C192417j) entry2.getValue()).A00(true);
                    if (A002 != null) {
                        A042.A04((String) entry2.getKey(), A002);
                        z2 = true;
                    }
                } else {
                    ((C192417j) entry2.getValue()).A00(false);
                }
            }
            if (z2) {
                A042.A03("period_start", r9.A03);
                A042.A03("period_end", r9.A01);
                A042.A03("real_start", r9.A04);
                A042.A03("real_end", r9.A02);
                A042.A07(AnonymousClass80H.$const$string(158), r9.A05);
                A042.A02("session_count", r9.A00);
                A042.A02(ErrorReportingConstants.SOFT_ERROR_OCCURRENCE_COUNT, A042.A00());
                A042.A0A();
            }
        }
    }
}
