package com.facebook.analytics.appstatelogger;

import X.AnonymousClass01P;
import X.AnonymousClass01Y;
import X.C010708t;

public class EarlyActivityTransitionMonitor {
    public static native void start(boolean z, boolean z2);

    private static void onPendingLaunch(int i) {
        synchronized (AnonymousClass01P.A0Z) {
            if (AnonymousClass01P.A0Y == null) {
                C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
                return;
            }
            AnonymousClass01P r2 = AnonymousClass01P.A0Y;
            AnonymousClass01P.A09(r2, r2.A0J, AnonymousClass01Y.IN_FOREGROUND);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0015, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r3.A0M.offer(java.lang.Integer.valueOf(r4));
        r1 = r3.A0M.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0025, code lost:
        if (r1 <= 0) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0027, code lost:
        r0 = ((java.lang.Integer) r3.A0M.peek()).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0037, code lost:
        X.AnonymousClass01P.A08(r3, r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        r3 = X.AnonymousClass01P.A0Y;
        r2 = r3.A0M;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void onPendingStop(int r4) {
        /*
            java.lang.Object r2 = X.AnonymousClass01P.A0Z
            monitor-enter(r2)
            X.01P r0 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x003e }
            if (r0 != 0) goto L_0x0010
            java.lang.String r1 = "AppStateLoggerCore"
            java.lang.String r0 = "AppStateLogger is not ready yet"
            X.C010708t.A0J(r1, r0)     // Catch:{ all -> 0x003e }
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            return
        L_0x0010:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            X.01P r3 = X.AnonymousClass01P.A0Y
            java.util.Queue r2 = r3.A0M
            monitor-enter(r2)
            java.util.Queue r1 = r3.A0M     // Catch:{ all -> 0x003b }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x003b }
            r1.offer(r0)     // Catch:{ all -> 0x003b }
            java.util.Queue r0 = r3.A0M     // Catch:{ all -> 0x003b }
            int r1 = r0.size()     // Catch:{ all -> 0x003b }
            if (r1 <= 0) goto L_0x0035
            java.util.Queue r0 = r3.A0M     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x003b }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x003b }
            int r0 = r0.intValue()     // Catch:{ all -> 0x003b }
        L_0x0033:
            monitor-exit(r2)     // Catch:{ all -> 0x003b }
            goto L_0x0037
        L_0x0035:
            r0 = 0
            goto L_0x0033
        L_0x0037:
            X.AnonymousClass01P.A08(r3, r1, r0)
            return
        L_0x003b:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003b }
            goto L_0x0040
        L_0x003e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
        L_0x0040:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.analytics.appstatelogger.EarlyActivityTransitionMonitor.onPendingStop(int):void");
    }
}
