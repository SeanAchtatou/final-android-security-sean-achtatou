package com.facebook.analytics.appstatelogger;

import X.AnonymousClass08S;
import X.AnonymousClass0E0;
import X.AnonymousClass0K4;
import X.C006506l;
import android.content.Intent;

public class AppStateIntentService extends AnonymousClass0E0 {
    public static final String A00;
    public static final String A01;

    static {
        Class<AppStateBroadcastReceiver> cls = AppStateBroadcastReceiver.class;
        A00 = AnonymousClass08S.A0J(cls.getCanonicalName(), ".LOG_TO_SHARED_PREFS");
        A01 = AnonymousClass08S.A0J(cls.getPackage().getName(), ".FRAMEWORK_TIME");
    }

    public void onHandleWork(Intent intent) {
        if (intent != null && C006506l.A01().A02(this, this, intent) && A00.equals(intent.getAction())) {
            AnonymousClass0K4.A00(getApplicationContext()).A00.edit().putLong("frameworkStartTime", intent.getLongExtra(A01, System.currentTimeMillis() / 1000)).apply();
        }
    }
}
