package com.facebook.analytics.appstatelogger;

import X.AnonymousClass01Y;
import X.AnonymousClass01q;
import X.AnonymousClass0EA;
import X.C002101k;
import X.C010208o;
import com.facebook.acra.util.StatFsUtil;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.concurrent.atomic.AtomicReference;

public class AppStateLogFile {
    public static final byte[] HEX_CHARACTERS = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70};
    private static final AtomicReference sInstance = new AtomicReference();
    public static volatile boolean sLibraryLoaded;
    private final FileChannel mAppStateLogFileChannel;
    private final FileLock mAppStateLogFileLock;
    public MessageDigest mDigest;
    public boolean mIsContentOutputStreamOpen;
    public boolean mIsEnabled;
    public MappedByteBuffer mMappedByteBuffer;
    private final Object mPendingStopLock = new Object();
    public final Object mPositionLock = new Object();
    private final Object mStatusLock = new Object();

    public static void init() {
        AppStateLogFile appStateLogFile;
        try {
            AnonymousClass01q.A08("appstatelogger");
            appStateLogFile = (AppStateLogFile) sInstance.get();
            sLibraryLoaded = true;
            if (appStateLogFile == null) {
                return;
            }
        } catch (RuntimeException | UnsatisfiedLinkError unused) {
            appStateLogFile = (AppStateLogFile) sInstance.get();
            sLibraryLoaded = false;
            if (appStateLogFile == null) {
                return;
            }
        } catch (Throwable th) {
            AppStateLogFile appStateLogFile2 = (AppStateLogFile) sInstance.get();
            sLibraryLoaded = true;
            if (appStateLogFile2 != null) {
                appStateLogFile2.mlockBuffer();
            }
            throw th;
        }
        appStateLogFile.mlockBuffer();
    }

    private static native void mlockBuffer(ByteBuffer byteBuffer);

    private static native void munlockBuffer(ByteBuffer byteBuffer);

    public static void assertIsAscii(char c) {
        if (c < 0 || c > 127) {
            throw new IllegalStateException("State byte should be ASCII");
        }
    }

    public static void ensureMappedByteBufferSizeRemaining(AppStateLogFile appStateLogFile, int i) {
        if (appStateLogFile.mAppStateLogFileChannel != null) {
            synchronized (appStateLogFile.mPositionLock) {
                if (appStateLogFile.mMappedByteBuffer.remaining() < i) {
                    appStateLogFile.mMappedByteBuffer.force();
                    int position = appStateLogFile.mMappedByteBuffer.position();
                    int i2 = ((((position + i) - 1) / 1024) + 1) << 10;
                    if (appStateLogFile.mIsEnabled && sLibraryLoaded) {
                        munlockBuffer(appStateLogFile.mMappedByteBuffer);
                    }
                    appStateLogFile.mMappedByteBuffer = appStateLogFile.mAppStateLogFileChannel.map(FileChannel.MapMode.READ_WRITE, 0, (long) i2);
                    appStateLogFile.mlockBuffer();
                    appStateLogFile.mMappedByteBuffer.position(position);
                }
            }
            return;
        }
        throw new IllegalStateException("In bad state");
    }

    private void throwIfContentOutputStreamIsOpen() {
        if (this.mIsEnabled) {
            synchronized (this.mPositionLock) {
                if (this.mIsContentOutputStreamOpen) {
                    throw new IllegalStateException("Cannot modify log file while content output stream is open");
                }
            }
        }
    }

    public void close() {
        boolean z = this.mIsEnabled;
        if (z) {
            if (z && sLibraryLoaded) {
                munlockBuffer(this.mMappedByteBuffer);
            }
            FileLock fileLock = this.mAppStateLogFileLock;
            if (fileLock != null) {
                fileLock.release();
            }
        }
    }

    public OutputStream createContentOutputStream() {
        if (!this.mIsEnabled) {
            return new AnonymousClass0EA();
        }
        throwIfContentOutputStreamIsOpen();
        synchronized (this.mPositionLock) {
            try {
                this.mIsContentOutputStreamOpen = true;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (this.mIsEnabled) {
            synchronized (this.mPositionLock) {
                try {
                    if (this.mIsEnabled) {
                        this.mMappedByteBuffer.position(5);
                    }
                    for (int i = 0; i < 4; i++) {
                        this.mMappedByteBuffer.put((byte) 0);
                    }
                    if (this.mIsEnabled) {
                        this.mMappedByteBuffer.position(37);
                    }
                    this.mMappedByteBuffer.put((byte) 0);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        if (this.mIsEnabled) {
            this.mMappedByteBuffer.position(37);
        }
        this.mDigest.reset();
        return new DigestOutputStream(new C010208o(this), this.mDigest);
    }

    public byte[] getLogData() {
        byte[] bArr;
        if (!this.mIsEnabled) {
            return null;
        }
        throwIfContentOutputStreamIsOpen();
        synchronized (this.mPositionLock) {
            if (this.mIsEnabled) {
                this.mMappedByteBuffer.position(0);
            }
            bArr = new byte[this.mMappedByteBuffer.remaining()];
            this.mMappedByteBuffer.get(bArr);
        }
        return bArr;
    }

    public void updateForegroundEntityInfo(AnonymousClass01Y r4, char c) {
        if (this.mIsEnabled) {
            char c2 = r4.mLogSymbol;
            assertIsAscii(c2);
            this.mMappedByteBuffer.put(1, (byte) c2);
            assertIsAscii(c);
            this.mMappedByteBuffer.put(2, (byte) c);
        }
    }

    public void updatePendingStopTracking(char c) {
        if (this.mIsEnabled) {
            assertIsAscii(c);
            synchronized (this.mPendingStopLock) {
                this.mMappedByteBuffer.put(3, (byte) c);
            }
        }
    }

    public void updateStatus(C002101k r5) {
        if (this.mIsEnabled) {
            char c = r5.mSymbol;
            assertIsAscii(c);
            synchronized (this.mStatusLock) {
                this.mMappedByteBuffer.put(0, (byte) c);
            }
        }
    }

    public AppStateLogFile(File file, boolean z) {
        this.mIsEnabled = z;
        if (!z) {
            this.mAppStateLogFileChannel = null;
            this.mAppStateLogFileLock = null;
            return;
        }
        FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
        this.mAppStateLogFileChannel = channel;
        this.mMappedByteBuffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, StatFsUtil.IN_KILO_BYTE);
        FileLock tryLock = this.mAppStateLogFileChannel.tryLock();
        this.mAppStateLogFileLock = tryLock;
        if (tryLock != null) {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            this.mDigest = instance;
            int digestLength = instance.getDigestLength() << 1;
            if (digestLength == 32) {
                if (sInstance.compareAndSet(null, this)) {
                    char c = AnonymousClass01Y.BYTE_NOT_USED.mLogSymbol;
                    assertIsAscii(c);
                    this.mMappedByteBuffer.put(1, (byte) c);
                    updatePendingStopTracking(' ');
                }
                mlockBuffer();
                return;
            }
            throw new IllegalArgumentException(String.format("Expected digest to have length %d; found %d", Integer.valueOf(digestLength), 32));
        }
        throw new IOException(String.format("Unable to acquire lock for app state log file: %s", file.getAbsolutePath()));
    }

    private void mlockBuffer() {
        if (this.mIsEnabled && sLibraryLoaded) {
            mlockBuffer(this.mMappedByteBuffer);
        }
    }
}
