package com.facebook.analytics.appstatelogger;

import X.AnonymousClass01P;
import X.AnonymousClass0E1;
import X.AnonymousClass0Jm;
import X.AnonymousClass0K4;
import X.C000700l;
import X.C001901i;
import X.C002001j;
import X.C006506l;
import X.C010708t;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AppStateBroadcastReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(-1656640902);
        if (!C006506l.A01().A02(context, this, intent)) {
            C000700l.A0D(intent, 853075440, A01);
            return;
        }
        String action = intent.getAction();
        if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            Intent intent2 = new Intent(context, AppStateIntentService.class);
            intent2.setAction(AppStateIntentService.A00);
            intent2.putExtra(AppStateIntentService.A01, System.currentTimeMillis() / 1000);
            try {
                AnonymousClass0E1.enqueueWork(context, AppStateIntentService.class, "AppStateIntentService".hashCode(), intent2);
            } catch (IllegalStateException | SecurityException e) {
                C002001j A00 = AnonymousClass01P.A00();
                if (A00 != null) {
                    A00.A00("Could not start framework start intent service", e);
                }
            }
        } else if (action.equals("android.intent.action.ACTION_SHUTDOWN")) {
            synchronized (AnonymousClass01P.A0Z) {
                try {
                    if (AnonymousClass01P.A0Y == null) {
                        C010708t.A0J("AppStateLoggerCore", "No application has been registered with AppStateLogger");
                    } else {
                        AnonymousClass01P r1 = AnonymousClass01P.A0Y;
                        boolean z = r1.A0O;
                        C001901i r2 = r1.A0B;
                        synchronized (r2) {
                            try {
                                r2.A0G = true;
                                r2.A0Q = z;
                                C001901i.A03(r2);
                            } catch (Throwable th) {
                                while (true) {
                                    th = th;
                                    break;
                                }
                            }
                        }
                        C001901i.A02(r2);
                        if (z) {
                            try {
                                AnonymousClass01P.A0Y.A0B.join();
                            } catch (InterruptedException e2) {
                                C010708t.A0R("AppStateLoggerCore", e2, "Interrupted joining worker thread");
                            }
                        }
                    }
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                    }
                }
            }
            AnonymousClass0Jm.A00 = true;
            AnonymousClass0K4.A00(context).A00.edit().putLong("deviceShutdown", System.currentTimeMillis() / 1000).apply();
        }
        C000700l.A0D(intent, 483118374, A01);
        return;
        throw th;
    }
}
