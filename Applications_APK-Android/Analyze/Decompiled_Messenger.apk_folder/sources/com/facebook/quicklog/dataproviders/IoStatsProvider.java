package com.facebook.quicklog.dataproviders;

import X.AnonymousClass00U;
import X.AnonymousClass00Y;
import X.AnonymousClass00Z;
import X.AnonymousClass03b;
import X.AnonymousClass077;
import X.AnonymousClass078;
import X.AnonymousClass0UN;
import X.AnonymousClass0Ud;
import X.AnonymousClass0WD;
import X.AnonymousClass0f8;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C012009h;
import X.C08350fD;
import X.C08360fE;
import X.C12810q0;
import android.os.Process;
import com.facebook.common.dextricks.stats.ClassLoadingStats;
import com.facebook.quicklog.PerformanceLoggingEvent;
import java.util.concurrent.atomic.AtomicLong;
import javax.inject.Singleton;

@Singleton
public final class IoStatsProvider extends AnonymousClass0f8 {
    private static volatile IoStatsProvider A04;
    private AnonymousClass0UN A00;
    private final AtomicLong A01 = new AtomicLong(-1);
    private final AtomicLong A02 = new AtomicLong(-1);
    private final AtomicLong A03 = new AtomicLong(-1);

    public String Azg() {
        return "io_stats";
    }

    public static C12810q0 A00(IoStatsProvider ioStatsProvider) {
        C12810q0 r5 = new C12810q0();
        r5.A00 = Process.myTid();
        long[] A012 = AnonymousClass00U.A01("/proc/self/stat");
        r5.A06 = A012[0];
        r5.A05 = A012[2];
        r5.A07 = AnonymousClass00U.A00();
        AnonymousClass00Z A002 = AnonymousClass00Y.A00();
        r5.A01 = A002.A00;
        r5.A03 = A002.A02;
        r5.A04 = A002.A04;
        long j = A002.A01;
        r5.A02 = j;
        ioStatsProvider.A01.compareAndSet(-1, j);
        long j2 = ((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, ioStatsProvider.A00)).A0L;
        if (ioStatsProvider.A03.get() != j2) {
            ioStatsProvider.A03.set(j2);
            ioStatsProvider.A02.set(r5.A02);
        }
        r5.A0A = ClassLoadingStats.A00().A01();
        r5.A09 = new AnonymousClass03b();
        r5.A08 = C012009h.A00();
        return r5;
    }

    public static final IoStatsProvider A01(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (IoStatsProvider.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new IoStatsProvider(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        C012009h r0;
        C12810q0 r9 = (C12810q0) obj;
        C12810q0 r10 = (C12810q0) obj2;
        if (r9 != null && r10 != null && performanceLoggingEvent.A0F == null) {
            performanceLoggingEvent.A06("ps_flt", r10.A05 - r9.A05);
            if (r9.A00 == r10.A00) {
                performanceLoggingEvent.A06("th_flt", r10.A07 - r9.A07);
            }
            ClassLoadingStats.SnapshotStats snapshotStats = r9.A0A;
            ClassLoadingStats.SnapshotStats snapshotStats2 = r10.A0A;
            performanceLoggingEvent.A04("class_load_attempts", snapshotStats2.A00 - snapshotStats.A00);
            performanceLoggingEvent.A04("dex_queries", snapshotStats2.A02 - snapshotStats.A02);
            performanceLoggingEvent.A04("class_loads_failed", snapshotStats2.A01 - snapshotStats.A01);
            performanceLoggingEvent.A04("locator_assists", snapshotStats2.A04 - snapshotStats.A04);
            performanceLoggingEvent.A04("wrong_dfa_guesses", snapshotStats2.A03 - snapshotStats.A03);
            performanceLoggingEvent.A04("class_hashmap_generate_successes", snapshotStats2.A08 - snapshotStats.A08);
            performanceLoggingEvent.A04("class_hashmap_generate_failures", snapshotStats2.A07 - snapshotStats.A07);
            performanceLoggingEvent.A04("class_hashmap_load_successes", snapshotStats2.A06 - snapshotStats.A06);
            performanceLoggingEvent.A04("class_hashmap_load_failures", snapshotStats2.A05 - snapshotStats.A05);
            C012009h r1 = r9.A08;
            C012009h r2 = null;
            if (r1 != null) {
                r0 = C012009h.A00();
            } else {
                r0 = null;
            }
            if (!(r1 == null || r0 == null)) {
                r2 = r0.A01(r1);
            }
            if (r2 != null) {
                performanceLoggingEvent.A06("io_cancelledwb", r2.A00);
                performanceLoggingEvent.A06("io_readbytes", r2.A01);
                performanceLoggingEvent.A06("io_readchars", r2.A02);
                performanceLoggingEvent.A06("io_readsyscalls", r2.A03);
                performanceLoggingEvent.A06("io_writebytes", r2.A04);
                performanceLoggingEvent.A06("io_writechars", r2.A05);
                performanceLoggingEvent.A06("io_writesyscalls", r2.A06);
            }
            long j = r9.A01;
            long j2 = -1;
            if (j != -1) {
                long j3 = r10.A01;
                if (j3 != -1) {
                    performanceLoggingEvent.A06("allocstall", j3 - j);
                }
            }
            long j4 = r9.A03;
            if (j4 != -1) {
                long j5 = r10.A03;
                if (j5 != -1) {
                    performanceLoggingEvent.A06("pages_in", j5 - j4);
                }
            }
            long j6 = r9.A04;
            if (j6 != -1) {
                long j7 = r10.A04;
                if (j7 != -1) {
                    performanceLoggingEvent.A06("pages_out", j7 - j6);
                }
            }
            long j8 = r9.A02;
            if (j8 != -1) {
                long j9 = r10.A02;
                if (j9 != -1) {
                    performanceLoggingEvent.A06("pages_steals", j9 - j8);
                    performanceLoggingEvent.A06("page_steals_since_cold_start", r10.A02 - this.A01.get());
                    performanceLoggingEvent.A06("page_steals_since_foreground", r10.A02 - this.A02.get());
                }
            }
            performanceLoggingEvent.A06("ps_min_flt", r10.A06 - r9.A06);
            if (r10.A09 != null) {
                AnonymousClass078 r12 = AnonymousClass077.A00;
                r12.A00.block();
                j2 = r12.A03.get();
            }
            performanceLoggingEvent.A06("avail_disk_spc_kb", j2);
        }
    }

    public long Azh() {
        return C08350fD.A07;
    }

    public Class B3O() {
        return C12810q0.class;
    }

    private IoStatsProvider(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(1, r4);
    }

    public /* bridge */ /* synthetic */ Object CGO() {
        return A00(this);
    }

    public boolean BEZ(C08360fE r2) {
        return true;
    }
}
