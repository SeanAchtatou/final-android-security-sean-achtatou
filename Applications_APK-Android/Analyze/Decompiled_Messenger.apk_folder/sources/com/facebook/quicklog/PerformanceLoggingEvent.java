package com.facebook.quicklog;

import X.AnonymousClass00T;
import X.AnonymousClass0Tj;
import X.AnonymousClass194;
import X.AnonymousClass1GD;
import X.AnonymousClass1J8;
import X.C04280Th;
import X.C08920gC;
import X.C11100lt;
import android.util.SparseArray;
import com.facebook.common.util.TriState;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PerformanceLoggingEvent implements Runnable, C04280Th {
    public static final C08920gC A0c = new AnonymousClass194(500);
    public List A00 = new ArrayList();
    public boolean A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07 = (1 << 24);
    public long A08;
    public long A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public SparseArray A0D;
    public SparseArray A0E;
    public AnonymousClass00T A0F;
    public TriState A0G;
    public TriState A0H;
    public AnonymousClass0Tj A0I = new AnonymousClass0Tj();
    public C11100lt A0J;
    public AnonymousClass1GD A0K;
    public AnonymousClass1J8 A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public ArrayList A0P = new ArrayList();
    public List A0Q = new ArrayList();
    public short A0R;
    public short A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    private PerformanceLoggingEvent A0b;

    public void A02(AnonymousClass0Tj r6) {
        this.A01 = false;
        this.A0T = false;
        AnonymousClass0Tj r3 = this.A0I;
        r3.A05.clear();
        r3.A05.addAll(r6.A05);
        r3.A01 = r6.A01;
        r3.A04 = r6.A04;
        r3.A06.clear();
        r3.A06.addAll(r6.A06);
        double[] dArr = r3.A08;
        int length = dArr.length;
        int i = r6.A00;
        if (length < i) {
            r3.A08 = Arrays.copyOf(r6.A08, i);
        } else {
            System.arraycopy(r6.A08, 0, dArr, 0, i);
        }
        long[] jArr = r3.A09;
        int length2 = jArr.length;
        int i2 = r6.A02;
        if (length2 < i2) {
            r3.A09 = Arrays.copyOf(r6.A09, i2);
        } else {
            System.arraycopy(r6.A09, 0, jArr, 0, i2);
        }
        byte[] bArr = r3.A07;
        int length3 = bArr.length;
        int i3 = r6.A03;
        if (length3 < i3) {
            r3.A07 = Arrays.copyOf(r6.A07, i3);
        } else {
            System.arraycopy(r6.A07, 0, bArr, 0, i3);
        }
        r3.A00 = r6.A00;
        r3.A02 = r6.A02;
        r3.A03 = r6.A03;
        this.A0a = true;
    }

    public void A05(String str, long j) {
        this.A01 = false;
        this.A0T = false;
        if (this.A0a) {
            AnonymousClass0Tj r1 = this.A0I;
            r1.A05.add(str);
            AnonymousClass0Tj.A01(r1, j);
            AnonymousClass0Tj.A00(r1, (byte) 3);
            return;
        }
        this.A00.add(str);
        this.A00.add(String.valueOf(j));
        this.A0Q.add(2);
    }

    public void A0A(String str, String str2) {
        this.A01 = false;
        this.A0T = false;
        if (this.A0a) {
            AnonymousClass0Tj r1 = this.A0I;
            r1.A05.add(str);
            r1.A06.add(str2);
            AnonymousClass0Tj.A00(r1, (byte) 1);
            return;
        }
        this.A00.add(str);
        this.A00.add(str2);
        this.A0Q.add(1);
    }

    public void A0C(String str, boolean z) {
        long j;
        this.A01 = false;
        this.A0T = false;
        if (this.A0a) {
            AnonymousClass0Tj r2 = this.A0I;
            r2.A05.add(str);
            if (z) {
                j = 1;
            } else {
                j = 0;
            }
            AnonymousClass0Tj.A01(r2, j);
            AnonymousClass0Tj.A00(r2, (byte) 8);
            return;
        }
        this.A00.add(str);
        this.A00.add(String.valueOf(z));
        this.A0Q.add(7);
    }

    public void BqE() {
    }

    public void clear() {
        this.A06 = 0;
        this.A0M = null;
        this.A04 = 0;
        this.A03 = 0;
        this.A0O = null;
        this.A0U = false;
        this.A0Z = false;
        this.A00.clear();
        this.A0Q.clear();
        this.A0I.A04();
        this.A01 = false;
        this.A0T = false;
        this.A0P.clear();
        this.A0J = null;
        this.A0b = null;
        this.A0F = null;
        this.A0D = null;
        this.A0E = null;
        this.A0R = 2;
        this.A08 = 0;
        this.A09 = 0;
        this.A0B = 0;
        this.A0A = 0;
        this.A0W = false;
        AnonymousClass1GD r1 = this.A0K;
        if (r1 != null) {
            r1.A00.clear();
            r1.A01.clear();
        }
    }

    public String A00() {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> arrayList = this.A0P;
        int size = arrayList.size();
        for (String append : arrayList) {
            sb.append(append);
            if (size > 1) {
                sb.append(",");
            }
            size--;
        }
        return sb.toString();
    }

    public List A01() {
        if (!this.A0a || this.A01) {
            return this.A00;
        }
        List A032 = this.A0I.A03();
        this.A00 = A032;
        this.A01 = true;
        return A032;
    }

    public void A03(String str) {
        if (this.A0K == null) {
            this.A0K = new AnonymousClass1GD();
        }
        AnonymousClass1GD r2 = this.A0K;
        int size = r2.A01.size() - 1;
        if (size >= 0 && r2.A01.get(size) != null) {
            r2.A01.remove(size);
        }
        r2.A01.add(str);
    }

    public void A04(String str, int i) {
        if (this.A0K == null) {
            this.A0K = new AnonymousClass1GD();
        }
        AnonymousClass1GD.A00(this.A0K, str, Integer.valueOf(i));
    }

    public void A06(String str, long j) {
        if (this.A0K == null) {
            this.A0K = new AnonymousClass1GD();
        }
        AnonymousClass1GD.A00(this.A0K, str, Long.valueOf(j));
    }

    public void A07(String str, Boolean bool) {
        if (bool != null) {
            if (this.A0K == null) {
                this.A0K = new AnonymousClass1GD();
            }
            AnonymousClass1GD.A00(this.A0K, str, Boolean.valueOf(bool.booleanValue()));
        }
    }

    public void A08(String str, Double d) {
        if (d != null) {
            if (this.A0K == null) {
                this.A0K = new AnonymousClass1GD();
            }
            AnonymousClass1GD.A00(this.A0K, str, Double.valueOf(d.doubleValue()));
        }
    }

    public void A09(String str, Long l) {
        if (l != null) {
            if (this.A0K == null) {
                this.A0K = new AnonymousClass1GD();
            }
            AnonymousClass1GD.A00(this.A0K, str, Long.valueOf(l.longValue()));
        }
    }

    public void A0B(String str, String str2) {
        if (this.A0K == null) {
            this.A0K = new AnonymousClass1GD();
        }
        AnonymousClass1GD.A00(this.A0K, str, str2);
    }

    public void A0D(String str, boolean z) {
        if (this.A0K == null) {
            this.A0K = new AnonymousClass1GD();
        }
        AnonymousClass1GD.A00(this.A0K, str, Boolean.valueOf(z));
    }

    public void A0E(List list, List list2) {
        this.A00.clear();
        this.A0Q.clear();
        this.A0a = false;
        this.A00.addAll(list);
        this.A0Q.addAll(list2);
    }

    public Object Avh() {
        return this.A0b;
    }

    public void C9o(Object obj) {
        this.A0b = (PerformanceLoggingEvent) obj;
    }

    public short getActionId() {
        return this.A0R;
    }

    public int getEventId() {
        return this.A06;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00be, code lost:
        if (r2.isSet() == false) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00d9, code lost:
        if (r6.BCO() == false) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x009e, code lost:
        if (r2.isSet() == false) goto L_0x00a0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r27 = this;
            r3 = r27
            X.1J8 r5 = r3.A0L
            X.0bA r4 = r5.A00
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            java.lang.String r1 = "perf"
            r23 = 0
            r0 = r23
            X.0ZF r4 = r4.A04(r1, r2, r0)
            java.lang.String r1 = r3.A0M
            java.lang.String r0 = "pigeon_reserved_keyword_module"
            r4.A0A(r0, r1)
            int r0 = r3.A02
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "pigeon_reserved_keyword_uuid"
            r4.A0A(r0, r1)
            long r0 = r3.A0C
            r4.A06(r0)
            X.0op r6 = r5.A01
            int r0 = r3.A06
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "marker_id"
            r4.A09(r0, r1)
            int r0 = r3.A02
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "instance_id"
            r4.A09(r0, r1)
            int r0 = r3.A05
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "sample_rate"
            r4.A09(r0, r1)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r0 = r3.A0B
            long r0 = r2.toMillis(r0)
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "time_since_boot_ms"
            r4.A09(r0, r1)
            long r0 = r3.A08
            long r1 = r2.toMillis(r0)
            int r0 = (int) r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "duration_ms"
            r4.A09(r0, r1)
            short r0 = r3.A0R
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "action_id"
            r4.A09(r0, r1)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r0 = r3.A09
            long r1 = r2.toMillis(r0)
            int r0 = (int) r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "duration_since_prev_action_ms"
            r4.A09(r0, r1)
            short r0 = r3.A0S
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "prev_action_id"
            r4.A09(r0, r1)
            com.facebook.common.util.TriState r2 = r3.A0G
            if (r2 == 0) goto L_0x00a0
            boolean r1 = r2.isSet()
            r0 = 1
            if (r1 != 0) goto L_0x00a1
        L_0x00a0:
            r0 = 0
        L_0x00a1:
            if (r0 == 0) goto L_0x00b1
            r0 = 0
            boolean r0 = r2.asBoolean(r0)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "was_backgrounded"
            r4.A08(r0, r1)
        L_0x00b1:
            boolean r0 = r3.A0V
            if (r0 == 0) goto L_0x00d1
            com.facebook.common.util.TriState r2 = r3.A0H
            if (r2 == 0) goto L_0x00c0
            boolean r1 = r2.isSet()
            r0 = 1
            if (r1 != 0) goto L_0x00c1
        L_0x00c0:
            r0 = 0
        L_0x00c1:
            if (r0 == 0) goto L_0x00d1
            r0 = 0
            boolean r0 = r2.asBoolean(r0)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "app_started_in_bg"
            r4.A08(r0, r1)
        L_0x00d1:
            r5 = 1
            if (r6 == 0) goto L_0x00db
            boolean r0 = r6.BCO()
            r2 = 1
            if (r0 != 0) goto L_0x00dc
        L_0x00db:
            r2 = 0
        L_0x00dc:
            boolean r1 = r3.A0Y
            boolean r0 = r3.A0X
            if (r2 == 0) goto L_0x025d
            java.lang.String r0 = "perf_qe"
        L_0x00e4:
            java.lang.String r1 = "method"
            r4.A0A(r1, r0)
            java.lang.String r1 = r3.A0N
            java.lang.String r0 = "qpl_lib_ver"
            r4.A0A(r0, r1)
            int r0 = r3.A03
            if (r0 == 0) goto L_0x00fd
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "da_level"
            r4.A09(r0, r1)
        L_0x00fd:
            java.lang.String r1 = r3.A0O
            if (r1 == 0) goto L_0x0106
            java.lang.String r0 = "da_type"
            r4.A0A(r0, r1)
        L_0x0106:
            java.util.List r6 = r3.A01()
            boolean r0 = r3.A0a
            if (r0 == 0) goto L_0x0258
            boolean r0 = r3.A0T
            if (r0 != 0) goto L_0x0258
            X.0Tj r0 = r3.A0I
            java.util.List r1 = r0.A02()
            r3.A0Q = r1
            r3.A0T = r5
        L_0x011c:
            X.0os r10 = r4.A0B()
            int r22 = r6.size()
            int r22 = r22 - r5
            r9 = 0
            r21 = 0
            r20 = 0
            r19 = 0
            r18 = 0
            r17 = 0
            r16 = 0
            r15 = 0
            r14 = 0
        L_0x0135:
            r0 = r22
            if (r9 >= r0) goto L_0x026d
            java.lang.Object r11 = r6.get(r9)
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            r11 = r0
            int r0 = r9 + 1
            java.lang.Object r2 = r6.get(r0)
            java.lang.String r2 = (java.lang.String) r2
            int r0 = r9 >> 1
            java.lang.Object r0 = r1.get(r0)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.lang.String r5 = ",,,"
            switch(r0) {
                case 1: goto L_0x0245;
                case 2: goto L_0x022a;
                case 3: goto L_0x020c;
                case 4: goto L_0x01e0;
                case 5: goto L_0x01c5;
                case 6: goto L_0x0199;
                case 7: goto L_0x0183;
                case 8: goto L_0x015d;
                default: goto L_0x015a;
            }
        L_0x015a:
            int r9 = r9 + 2
            goto L_0x0135
        L_0x015d:
            if (r14 != 0) goto L_0x0165
            java.lang.String r0 = "annotations_bool_array"
            X.0os r14 = r10.A0F(r0)
        L_0x0165:
            X.0xz r11 = r14.A0E(r11)
            java.lang.String[] r8 = r2.split(r5)
            int r7 = r8.length
            r5 = 0
        L_0x016f:
            if (r5 >= r7) goto L_0x015a
            r2 = r8[r5]
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0180
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            X.C16910xz.A00(r11, r0)
        L_0x0180:
            int r5 = r5 + 1
            goto L_0x016f
        L_0x0183:
            if (r15 != 0) goto L_0x018b
            java.lang.String r0 = "annotations_bool"
            X.0os r15 = r10.A0F(r0)
        L_0x018b:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            r24 = r15
            r25 = r11
            r26 = r0
            X.C12240os.A01(r24, r25, r26)
            goto L_0x015a
        L_0x0199:
            if (r16 != 0) goto L_0x01a1
            java.lang.String r0 = "annotations_double_array"
            X.0os r16 = r10.A0F(r0)
        L_0x01a1:
            r7 = r16
            X.0xz r13 = r7.A0E(r11)
            java.lang.String[] r12 = r2.split(r5)
            int r11 = r12.length
            r5 = 0
        L_0x01ad:
            if (r5 >= r11) goto L_0x015a
            r2 = r12[r5]
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x01c2
            double r7 = java.lang.Double.parseDouble(r2)
            java.lang.Double r0 = java.lang.Double.valueOf(r7)
            X.C16910xz.A00(r13, r0)
        L_0x01c2:
            int r5 = r5 + 1
            goto L_0x01ad
        L_0x01c5:
            if (r17 != 0) goto L_0x01cd
            java.lang.String r0 = "annotations_double"
            X.0os r17 = r10.A0F(r0)
        L_0x01cd:
            double r7 = java.lang.Double.parseDouble(r2)
            java.lang.Double r0 = java.lang.Double.valueOf(r7)
            r24 = r17
            r25 = r11
            r26 = r0
            r24.A0J(r25, r26)
            goto L_0x015a
        L_0x01e0:
            if (r18 != 0) goto L_0x01e8
            java.lang.String r0 = "annotations_int_array"
            X.0os r18 = r10.A0F(r0)
        L_0x01e8:
            r7 = r18
            X.0xz r13 = r7.A0E(r11)
            java.lang.String[] r12 = r2.split(r5)
            int r11 = r12.length
            r5 = 0
        L_0x01f4:
            if (r5 >= r11) goto L_0x015a
            r2 = r12[r5]
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0209
            long r7 = java.lang.Long.parseLong(r2)
            java.lang.Long r0 = java.lang.Long.valueOf(r7)
            X.C16910xz.A00(r13, r0)
        L_0x0209:
            int r5 = r5 + 1
            goto L_0x01f4
        L_0x020c:
            if (r19 != 0) goto L_0x0214
            java.lang.String r0 = "annotations_string_array"
            X.0os r19 = r10.A0F(r0)
        L_0x0214:
            r7 = r19
            X.0xz r8 = r7.A0E(r11)
            java.lang.String[] r7 = r2.split(r5)
            int r5 = r7.length
            r2 = 0
        L_0x0220:
            if (r2 >= r5) goto L_0x015a
            r0 = r7[r2]
            X.C16910xz.A00(r8, r0)
            int r2 = r2 + 1
            goto L_0x0220
        L_0x022a:
            if (r20 != 0) goto L_0x0232
            java.lang.String r0 = "annotations_int"
            X.0os r20 = r10.A0F(r0)
        L_0x0232:
            long r7 = java.lang.Long.parseLong(r2)
            java.lang.Long r0 = java.lang.Long.valueOf(r7)
            r24 = r20
            r25 = r11
            r26 = r0
            r24.A0J(r25, r26)
            goto L_0x015a
        L_0x0245:
            if (r21 != 0) goto L_0x024d
            java.lang.String r0 = "annotations"
            X.0os r21 = r10.A0F(r0)
        L_0x024d:
            r24 = r21
            r25 = r11
            r26 = r2
            r24.A0K(r25, r26)
            goto L_0x015a
        L_0x0258:
            java.util.List r0 = r3.A0Q
            r1 = r0
            goto L_0x011c
        L_0x025d:
            if (r1 == 0) goto L_0x0263
            java.lang.String r0 = "missing_config"
            goto L_0x00e4
        L_0x0263:
            if (r0 == 0) goto L_0x0269
            java.lang.String r0 = "always_on"
            goto L_0x00e4
        L_0x0269:
            java.lang.String r0 = "random_sampling"
            goto L_0x00e4
        L_0x026d:
            java.lang.String r1 = r3.A00()
            java.lang.String r0 = "trace_tags"
            r4.A0A(r0, r1)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r0 = r3.A08
            long r1 = r2.toMillis(r0)
            int r0 = (int) r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "value"
            r4.A09(r0, r1)
            java.lang.String r1 = X.AnonymousClass1J8.A02
            if (r1 == 0) goto L_0x0291
            java.lang.String r0 = "scenario"
            r4.A0A(r0, r1)
        L_0x0291:
            boolean r0 = r3.A0U
            r2 = 1
            if (r0 == 0) goto L_0x029f
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r2)
            java.lang.String r0 = "cancelled"
            r4.A08(r0, r1)
        L_0x029f:
            boolean r0 = r3.A0Z
            if (r0 == 0) goto L_0x02ac
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r2)
            java.lang.String r0 = "tracked_for_loss"
            r4.A08(r0, r1)
        L_0x02ac:
            if (r23 == 0) goto L_0x02b7
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r2)
            java.lang.String r0 = "is_note"
            r4.A08(r0, r1)
        L_0x02b7:
            int r0 = r3.A04
            if (r0 == 0) goto L_0x0326
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r0 = "markerStart called multiple times without end or cancel"
            r8.<init>(r0)
        L_0x02c2:
            X.0lt r6 = r3.A0J
            if (r6 == 0) goto L_0x0328
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r0 = r3.A08
            long r1 = r2.toMillis(r0)
            int r0 = (int) r1
            long r1 = (long) r0
            X.0os r5 = r4.A0B()
            java.lang.String r0 = "points"
            X.0xz r5 = r5.A0E(r0)
            X.1Jw r0 = new X.1Jw
            r0.<init>(r1, r5)
            r6.A01(r0)
            java.util.ArrayList r7 = r6.A02
            if (r7 == 0) goto L_0x0328
            java.util.Collections.sort(r7)
            r6 = 0
            int r5 = r7.size()
            r2 = 0
        L_0x02ef:
            if (r2 >= r5) goto L_0x0328
            java.lang.Object r1 = r7.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            boolean r0 = r1.equals(r6)
            if (r0 != 0) goto L_0x031d
            if (r8 != 0) goto L_0x0320
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
        L_0x0304:
            java.lang.String r0 = "intermediatePoint called multiple times for the same key: "
            r8.append(r0)
            r0 = 60
            r8.append(r0)
            java.lang.Object r0 = r7.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            r8.append(r0)
            r0 = 62
            r8.append(r0)
            r6 = r1
        L_0x031d:
            int r2 = r2 + 1
            goto L_0x02ef
        L_0x0320:
            r0 = 44
            r8.append(r0)
            goto L_0x0304
        L_0x0326:
            r8 = 0
            goto L_0x02c2
        L_0x0328:
            if (r8 == 0) goto L_0x0337
            X.0os r2 = r4.A0B()
            java.lang.String r1 = r8.toString()
            java.lang.String r0 = "error"
            r2.A0K(r0, r1)
        L_0x0337:
            X.1GD r2 = r3.A0K
            if (r2 == 0) goto L_0x0355
            java.util.ArrayList r0 = r2.A00
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0355
            X.0os r1 = r4.A0B()
            java.lang.String r0 = "metadata"
            X.0os r1 = r1.A0F(r0)
            X.1jE r0 = new X.1jE
            r0.<init>(r1)
            r2.A01(r0)
        L_0x0355:
            int r1 = r3.A06
            r0 = 196678(0x30046, float:2.75605E-40)
            if (r1 == r0) goto L_0x035f
            r4.A0E()
        L_0x035f:
            X.0gC r0 = com.facebook.quicklog.PerformanceLoggingEvent.A0c
            r0.A02(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.quicklog.PerformanceLoggingEvent.run():void");
    }

    public PerformanceLoggingEvent() {
        clear();
    }
}
