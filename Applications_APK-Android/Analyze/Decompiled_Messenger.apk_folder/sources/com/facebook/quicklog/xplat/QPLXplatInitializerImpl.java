package com.facebook.quicklog.xplat;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import com.facebook.xanalytics.XAnalyticsHolder;

public class QPLXplatInitializerImpl {
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public static native void setupNativeQPLWithXAnalyticsHolder(XAnalyticsHolder xAnalyticsHolder);

    static {
        AnonymousClass01q.A08("perfloggerxplat_init");
    }
}
