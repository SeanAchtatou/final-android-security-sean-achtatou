package com.facebook.quicklog.module;

import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1YA;
import X.AnonymousClass1ZF;
import X.AnonymousClass960;
import X.C000700l;
import X.C006506l;
import X.C06230bA;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

public class QPLEventFlushActivity extends Activity {
    private static final String TAG = "QPLEventFlushActivity";
    private Context mContext;
    private C06230bA mLogger;

    private static final void $ul_injectMe(Context context, QPLEventFlushActivity qPLEventFlushActivity) {
        $ul_staticInjectMe(AnonymousClass1XX.get(context), qPLEventFlushActivity);
    }

    public static final void $ul_staticInjectMe(AnonymousClass1XY r1, QPLEventFlushActivity qPLEventFlushActivity) {
        qPLEventFlushActivity.mLogger = AnonymousClass1ZF.A02(r1);
        qPLEventFlushActivity.mContext = AnonymousClass1YA.A00(r1);
    }

    public void onCreate(Bundle bundle) {
        int A00 = C000700l.A00(1034148874);
        if (!C006506l.A01().A02(this, this, getIntent())) {
            finish();
            C000700l.A07(-1159136977, A00);
            return;
        }
        super.onCreate(bundle);
        $ul_injectMe(this, this);
        new AnonymousClass960(this.mLogger).A00(1);
        finish();
        C000700l.A07(-758441869, A00);
    }
}
