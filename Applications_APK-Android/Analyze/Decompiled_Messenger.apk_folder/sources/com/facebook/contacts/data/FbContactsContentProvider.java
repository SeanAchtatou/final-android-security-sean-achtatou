package com.facebook.contacts.data;

import X.AnonymousClass08S;
import X.AnonymousClass0TB;
import X.AnonymousClass0TF;
import X.AnonymousClass1XX;
import X.AnonymousClass24B;
import X.AnonymousClass2B6;
import X.AnonymousClass2BO;
import X.AnonymousClass2BW;
import X.AnonymousClass2BX;
import X.AnonymousClass7Vk;
import X.C005505z;
import X.C05330Yn;
import X.C06850cB;
import X.C158507Vf;
import X.C158547Vj;
import X.C158637Vt;
import X.C24971Xv;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

public class FbContactsContentProvider extends AnonymousClass0TB {
    public AnonymousClass2BO A00;
    public AnonymousClass2B6 A01;
    public C05330Yn A02;
    public AnonymousClass0TF A03;
    public AnonymousClass2BX A04;
    private C158547Vj A05;
    private C158637Vt A06;
    private C158507Vf A07;
    private AnonymousClass7Vk A08;
    public volatile ImmutableMap A09;
    public volatile ImmutableMap A0A;
    private volatile ImmutableMap A0B;

    public synchronized void A0G() {
        C005505z.A03("ContactsContentProvider.onInitialize", 1684832346);
        try {
            AnonymousClass1XX r1 = AnonymousClass1XX.get(getContext());
            this.A02 = C05330Yn.A00(r1);
            this.A04 = AnonymousClass2BW.A00(r1);
            this.A01 = AnonymousClass2B6.A00(r1);
            this.A00 = new AnonymousClass2BO();
            this.A05 = new C158547Vj(this);
            this.A08 = new AnonymousClass7Vk(this);
            this.A07 = new C158507Vf(this);
            this.A06 = new C158637Vt(this);
            AnonymousClass0TF r3 = new AnonymousClass0TF();
            this.A03 = r3;
            r3.A01(this.A01.A05, "contacts_with_fbids", this.A05);
            this.A03.A01(this.A01.A05, "sms_favorites", this.A08);
            this.A03.A01(this.A01.A05, "search", this.A07);
            this.A03.A01(this.A01.A05, "search/", this.A07);
            this.A03.A01(this.A01.A05, "search/*", this.A07);
            this.A03.A01(this.A01.A05, "search/*/*", this.A07);
            this.A03.A01(this.A01.A05, "contact_index", this.A06);
            C005505z.A00(-1751968473);
        } finally {
            C005505z.A00(-223388754);
        }
    }

    public static ImmutableMap A00(FbContactsContentProvider fbContactsContentProvider) {
        if (fbContactsContentProvider.A0B == null) {
            ImmutableMap.Builder builder = ImmutableMap.builder();
            builder.put("_id", "_id");
            builder.put("fbid", "fbid");
            builder.put("type", "type");
            builder.put("link_type", "link_type");
            builder.put("communication_rank", "communication_rank");
            String $const$string = AnonymousClass24B.$const$string(347);
            builder.put($const$string, $const$string);
            builder.put("is_messenger_user", "is_messenger_user");
            builder.put("is_on_viewer_contact_list", "is_on_viewer_contact_list");
            builder.put("viewer_connection_status", "viewer_connection_status");
            builder.put("add_source", "add_source");
            builder.put("is_broadcast_recipient_holdout", "is_broadcast_recipient_holdout");
            String $const$string2 = AnonymousClass24B.$const$string(94);
            builder.put($const$string2, $const$string2);
            builder.put("phonebook_section_key", "phonebook_section_key");
            builder.put("data", "data");
            builder.put("first_name", "first_name");
            builder.put("last_name", "last_name");
            builder.put("display_name", "display_name");
            builder.put("small_picture_url", "small_picture_url");
            builder.put("big_picture_url", "big_picture_url");
            builder.put("huge_picture_url", "huge_picture_url");
            builder.put("small_picture_size", "small_picture_size");
            builder.put("big_picture_size", "big_picture_size");
            builder.put("huge_picture_size", "huge_picture_size");
            builder.put("is_mobile_pushable", "is_mobile_pushable");
            builder.put("messenger_install_time_ms", "messenger_install_time_ms");
            builder.put("added_time_ms", "added_time_ms");
            builder.put("last_fetch_time_ms", "last_fetch_time_ms");
            builder.put("is_indexed", "is_indexed");
            builder.put("bday_month", "bday_month");
            builder.put("bday_day", "bday_day");
            builder.put("is_partial", "is_partial");
            builder.put("messenger_invite_priority", "messenger_invite_priority");
            builder.put("is_memorialized", "is_memorialized");
            builder.put("is_aloha_proxy_confirmed", "is_aloha_proxy_confirmed");
            builder.put("aloha_proxy_user_owners", "aloha_proxy_user_owners");
            builder.put("is_message_ignored_by_viewer", "is_message_ignored_by_viewer");
            builder.put("favorite_color", "favorite_color");
            builder.put("is_viewer_managing_parent", "is_viewer_managing_parent");
            builder.put("work_info", "work_info");
            builder.put("is_managing_parent_approved_user", "is_managing_parent_approved_user");
            fbContactsContentProvider.A0B = builder.build();
        }
        return fbContactsContentProvider.A0B;
    }

    public static /* synthetic */ String A02(String str, String str2) {
        if (str2 != null && !"_id".equals(str2)) {
            return str;
        }
        if (C06850cB.A0B(str)) {
            return "is_indexed = 1";
        }
        return AnonymousClass08S.A0J(str, " AND is_indexed = 1");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003e, code lost:
        if (X.AnonymousClass2B8.A01.contains(r13) != false) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A0I(java.lang.String r10, java.lang.String[] r11, java.lang.String r12, java.lang.String r13) {
        /*
            r9 = this;
            java.util.LinkedHashSet r1 = new java.util.LinkedHashSet
            r1.<init>()
            java.util.Collections.addAll(r1, r11)
            java.lang.String r4 = com.google.common.base.Strings.nullToEmpty(r12)
            com.google.common.collect.ImmutableMap r0 = A00(r9)
            com.google.common.collect.ImmutableSet r0 = r0.keySet()
            X.1Xv r3 = r0.iterator()
        L_0x0018:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x002e
            java.lang.Object r2 = r3.next()
            java.lang.String r2 = (java.lang.String) r2
            boolean r0 = r4.contains(r2)
            if (r0 == 0) goto L_0x0018
            r1.add(r2)
            goto L_0x0018
        L_0x002e:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "c.internal_id AS _id"
            r2.<init>(r0)
            if (r13 == 0) goto L_0x0040
            com.google.common.collect.ImmutableSet r0 = X.AnonymousClass2B8.A01
            boolean r3 = r0.contains(r13)
            r0 = 0
            if (r3 == 0) goto L_0x0041
        L_0x0040:
            r0 = 1
        L_0x0041:
            com.google.common.base.Preconditions.checkState(r0)
            r3 = r10
            if (r13 == 0) goto L_0x00bf
            java.lang.String r0 = "_id"
            boolean r0 = r0.equals(r13)
            if (r0 != 0) goto L_0x00bf
            com.google.common.collect.ImmutableMap r0 = A00(r9)
            java.lang.Object r7 = r0.get(r13)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r0 = ", idx.indexed_data AS "
            r2.append(r0)
            r2.append(r7)
            java.lang.String r4 = " AS c INNER JOIN contacts_indexed_data AS idx"
            java.lang.String r5 = " ON (c.internal_id = idx.contact_internal_id AND "
            java.lang.String r6 = "idx.type = '"
            java.lang.String r8 = "') "
            java.lang.String r3 = X.AnonymousClass08S.A0U(r3, r4, r5, r6, r7, r8)
        L_0x006d:
            if (r13 == 0) goto L_0x0072
            r1.remove(r13)
        L_0x0072:
            java.util.Iterator r1 = r1.iterator()
        L_0x0076:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00c6
            java.lang.Object r4 = r1.next()
            java.lang.String r4 = (java.lang.String) r4
            java.lang.String r0 = "_id"
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x0076
            java.lang.String r0 = "data"
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x00ae
            java.lang.String r0 = "is_indexed"
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x00ae
            com.google.common.collect.ImmutableSet r0 = X.AnonymousClass2B8.A00
            boolean r0 = r0.contains(r4)
            if (r0 != 0) goto L_0x00ae
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unknown field: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r4)
            r1.<init>(r0)
            throw r1
        L_0x00ae:
            java.lang.String r0 = ", c."
            r2.append(r0)
            r2.append(r4)
            java.lang.String r0 = " AS "
            r2.append(r0)
            r2.append(r4)
            goto L_0x0076
        L_0x00bf:
            java.lang.String r0 = " AS c "
            java.lang.String r3 = X.AnonymousClass08S.A0J(r10, r0)
            goto L_0x006d
        L_0x00c6:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "(SELECT "
            r1.<init>(r0)
            r1.append(r2)
            java.lang.String r0 = " FROM "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = ")"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.contacts.data.FbContactsContentProvider.A0I(java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.lang.String");
    }

    public static String A01(FbContactsContentProvider fbContactsContentProvider, String str) {
        String nullToEmpty = Strings.nullToEmpty(str);
        C24971Xv it = A00(fbContactsContentProvider).keySet().iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (nullToEmpty.contains(str2)) {
                return str2;
            }
        }
        return null;
    }
}
