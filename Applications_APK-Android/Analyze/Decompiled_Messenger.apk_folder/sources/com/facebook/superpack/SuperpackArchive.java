package com.facebook.superpack;

import X.AnonymousClass01q;
import X.C000700l;
import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SuperpackArchive implements Iterator, Closeable {
    public long A00;

    private static native void appendNative(long j, long j2);

    private static native void closeNative(long j);

    private static native long createNative();

    private static native boolean isEmptyNative(long j);

    private static native long nextNative(long j);

    private static native long readNative(InputStream inputStream, String str);

    private static native void writeNative(long j, OutputStream outputStream);

    /* renamed from: A01 */
    public synchronized SuperpackFile next() {
        long nextNative;
        long j = this.A00;
        if (j != 0) {
            nextNative = nextNative(j);
            if (nextNative != 0) {
            } else {
                throw new NoSuchElementException();
            }
        } else {
            throw new IllegalStateException();
        }
        return new SuperpackFile(nextNative);
    }

    public synchronized void close() {
        long j = this.A00;
        if (j != 0) {
            closeNative(j);
            this.A00 = 0;
        } else {
            throw new IllegalStateException();
        }
    }

    public boolean hasNext() {
        boolean isEmptyNative;
        synchronized (this) {
            long j = this.A00;
            if (j != 0) {
                isEmptyNative = isEmptyNative(j);
            } else {
                throw new IllegalStateException();
            }
        }
        return !isEmptyNative;
    }

    static {
        AnonymousClass01q.A08("superpack-jni");
    }

    public static SuperpackArchive A00(InputStream inputStream, String str) {
        if (inputStream != null) {
            return new SuperpackArchive(readNative(inputStream, str));
        }
        throw new NullPointerException();
    }

    private SuperpackArchive(long j) {
        if (j != 0) {
            this.A00 = j;
            return;
        }
        throw new IllegalArgumentException();
    }

    public void finalize() {
        int A03 = C000700l.A03(1818645622);
        long j = this.A00;
        if (j == 0) {
            C000700l.A09(-1684098993, A03);
            return;
        }
        closeNative(j);
        this.A00 = 0;
        IllegalStateException illegalStateException = new IllegalStateException();
        C000700l.A09(1146585758, A03);
        throw illegalStateException;
    }
}
