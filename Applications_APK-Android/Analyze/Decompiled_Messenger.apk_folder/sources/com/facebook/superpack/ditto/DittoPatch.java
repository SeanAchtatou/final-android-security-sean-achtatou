package com.facebook.superpack.ditto;

import X.AnonymousClass01q;
import com.facebook.superpack.SuperpackFile;
import java.io.Closeable;
import java.io.InputStream;

public final class DittoPatch implements Closeable {
    private static native long applyNative(long j, long j2);

    private static native void closeNative(long j);

    private static native long readNative(InputStream inputStream);

    public SuperpackFile A00(SuperpackFile superpackFile) {
        long j;
        synchronized (this) {
            if (0 == 0) {
                throw new IllegalStateException();
            }
        }
        synchronized (superpackFile) {
            j = superpackFile.A01;
            if (j == 0) {
                throw new IllegalStateException();
            }
        }
        return new SuperpackFile(applyNative(0, j));
    }

    public synchronized void close() {
        if (0 != 0) {
            closeNative(0);
        } else {
            throw new IllegalStateException();
        }
    }

    static {
        AnonymousClass01q.A08("ditto-jni");
    }
}
