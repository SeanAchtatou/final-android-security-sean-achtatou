package com.facebook.superpack;

import X.AnonymousClass01q;
import X.C000700l;
import java.io.Closeable;
import java.io.InputStream;

public final class SuperpackFile implements Closeable {
    public int A00;
    public long A01;

    private static native void closeNative(long j);

    public static native long createSuperpackFileNative(String str, InputStream inputStream);

    private static native long createSuperpackFileNative(String str, byte[] bArr);

    private static native int getLengthNative(long j);

    private static native String getNameNative(long j);

    public static native void readBytesNative(long j, int i, int i2, byte[] bArr, int i3);

    public synchronized int A00() {
        if (this.A01 != 0) {
        } else {
            throw new IllegalStateException();
        }
        return this.A00;
    }

    public synchronized String A01() {
        long j;
        j = this.A01;
        if (j != 0) {
        } else {
            throw new IllegalStateException();
        }
        return getNameNative(j);
    }

    public synchronized void close() {
        long j = this.A01;
        if (j != 0) {
            closeNative(j);
            this.A01 = 0;
        } else {
            throw new IllegalStateException();
        }
    }

    static {
        AnonymousClass01q.A08("superpack-jni");
    }

    public SuperpackFile(long j) {
        if (j != 0) {
            this.A01 = j;
            this.A00 = getLengthNative(j);
            return;
        }
        throw new NullPointerException();
    }

    public void finalize() {
        int A03 = C000700l.A03(-2000321027);
        long j = this.A01;
        if (j == 0) {
            C000700l.A09(-158776507, A03);
            return;
        }
        closeNative(j);
        this.A01 = 0;
        IllegalStateException illegalStateException = new IllegalStateException();
        C000700l.A09(1774343014, A03);
        throw illegalStateException;
    }
}
