package com.facebook.superpack;

import java.io.InputStream;

public final class SuperpackFileInputStream extends InputStream {
    private int A00;
    private int A01;
    private int A02;
    private Boolean A03;
    private byte[] A04;
    private final SuperpackFile A05;

    public synchronized void mark(int i) {
        this.A01 = this.A02;
    }

    public boolean markSupported() {
        return true;
    }

    public synchronized void reset() {
        this.A02 = this.A01;
    }

    public synchronized long skip(long j) {
        if (j < 0) {
            return 0;
        }
        int i = this.A02;
        long j2 = (long) i;
        int i2 = this.A00;
        if (j2 + j > ((long) i2)) {
            j = (long) (i2 - i);
        }
        this.A02 = (int) (j2 + j);
        return j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003b, code lost:
        if (r3 != null) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0040, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.superpack.SuperpackFileInputStream createFromSingletonArchiveInputStream(java.io.InputStream r2, java.lang.String r3) {
        /*
            if (r2 == 0) goto L_0x0041
            com.facebook.superpack.SuperpackArchive r3 = com.facebook.superpack.SuperpackArchive.A00(r2, r3)
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0032
            com.facebook.superpack.SuperpackFile r2 = r3.next()     // Catch:{ all -> 0x0038 }
            if (r2 == 0) goto L_0x002c
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x0026
            com.facebook.superpack.SuperpackFileInputStream r1 = new com.facebook.superpack.SuperpackFileInputStream     // Catch:{ all -> 0x0038 }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0038 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0038 }
            r3.close()
            return r1
        L_0x0026:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0038 }
            r0.<init>()     // Catch:{ all -> 0x0038 }
            throw r0     // Catch:{ all -> 0x0038 }
        L_0x002c:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x0038 }
            r0.<init>()     // Catch:{ all -> 0x0038 }
            throw r0     // Catch:{ all -> 0x0038 }
        L_0x0032:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0038 }
            r0.<init>()     // Catch:{ all -> 0x0038 }
            throw r0     // Catch:{ all -> 0x0038 }
        L_0x0038:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003a }
        L_0x003a:
            r0 = move-exception
            if (r3 == 0) goto L_0x0040
            r3.close()     // Catch:{ all -> 0x0040 }
        L_0x0040:
            throw r0
        L_0x0041:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.superpack.SuperpackFileInputStream.createFromSingletonArchiveInputStream(java.io.InputStream, java.lang.String):com.facebook.superpack.SuperpackFileInputStream");
    }

    public void close() {
        if (this.A03.booleanValue()) {
            this.A05.close();
        }
    }

    public SuperpackFileInputStream(SuperpackFile superpackFile) {
        if (superpackFile != null) {
            this.A05 = superpackFile;
            this.A02 = 0;
            this.A00 = superpackFile.A00();
            this.A01 = 0;
            this.A04 = null;
            this.A03 = false;
            return;
        }
        throw new NullPointerException();
    }

    public SuperpackFileInputStream(SuperpackFile superpackFile, Boolean bool) {
        this(superpackFile);
        this.A03 = bool;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int read() {
        /*
            r3 = this;
            monitor-enter(r3)
            byte[] r0 = r3.A04     // Catch:{ all -> 0x0029 }
            r2 = 1
            if (r0 != 0) goto L_0x000a
            byte[] r0 = new byte[r2]     // Catch:{ all -> 0x0029 }
            r3.A04 = r0     // Catch:{ all -> 0x0029 }
        L_0x000a:
            byte[] r0 = r3.A04     // Catch:{ all -> 0x0029 }
            int r1 = r3.read(r0)     // Catch:{ all -> 0x0029 }
            r0 = -1
            if (r1 == r0) goto L_0x0027
            if (r1 != r2) goto L_0x001d
            byte[] r1 = r3.A04     // Catch:{ all -> 0x0029 }
            r0 = 0
            byte r0 = r1[r0]     // Catch:{ all -> 0x0029 }
            if (r0 >= 0) goto L_0x0027
            goto L_0x0025
        L_0x001d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = "Unexpected number of bytes read"
            r1.<init>(r0)     // Catch:{ all -> 0x0029 }
            throw r1     // Catch:{ all -> 0x0029 }
        L_0x0025:
            int r0 = r0 + 256
        L_0x0027:
            monitor-exit(r3)
            return r0
        L_0x0029:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.superpack.SuperpackFileInputStream.read():int");
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:49:0x0062=Splitter:B:49:0x0062, B:19:0x001e=Splitter:B:19:0x001e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int read(byte[] r13, int r14, int r15) {
        /*
            r12 = this;
            r9 = r15
            monitor-enter(r12)
            r10 = r13
            if (r13 == 0) goto L_0x0068
            r11 = r14
            if (r14 < 0) goto L_0x0062
            if (r15 < 0) goto L_0x0062
            int r0 = r15 + r14
            int r5 = r13.length     // Catch:{ all -> 0x0071 }
            if (r0 > r5) goto L_0x0062
            int r8 = r12.A02     // Catch:{ all -> 0x0071 }
            int r1 = r12.A00     // Catch:{ all -> 0x0071 }
            if (r8 != r1) goto L_0x0018
            r0 = -1
            monitor-exit(r12)
            return r0
        L_0x0018:
            int r0 = r8 + r15
            if (r0 <= r1) goto L_0x001e
            int r9 = r1 - r8
        L_0x001e:
            com.facebook.superpack.SuperpackFile r2 = r12.A05     // Catch:{ all -> 0x0071 }
            monitor-enter(r2)     // Catch:{ all -> 0x0071 }
            long r6 = r2.A01     // Catch:{ all -> 0x006e }
            r3 = 0
            int r0 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x005c
            if (r13 == 0) goto L_0x0056
            if (r8 < 0) goto L_0x0050
            if (r9 < 0) goto L_0x0050
            int r0 = r14 + r9
            if (r0 > r5) goto L_0x004a
            int r1 = r8 + r9
            int r0 = r2.A00     // Catch:{ all -> 0x006e }
            if (r1 > r0) goto L_0x0044
            com.facebook.superpack.SuperpackFile.readBytesNative(r6, r8, r9, r10, r11)     // Catch:{ all -> 0x006e }
            monitor-exit(r2)     // Catch:{ all -> 0x0071 }
            int r0 = r12.A02     // Catch:{ all -> 0x0071 }
            int r0 = r0 + r9
            r12.A02 = r0     // Catch:{ all -> 0x0071 }
            monitor-exit(r12)
            return r9
        L_0x0044:
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException     // Catch:{ all -> 0x006e }
            r0.<init>()     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x004a:
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException     // Catch:{ all -> 0x006e }
            r0.<init>()     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x0050:
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException     // Catch:{ all -> 0x006e }
            r0.<init>()     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x0056:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x006e }
            r0.<init>()     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x005c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x006e }
            r0.<init>()     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x0062:
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException     // Catch:{ all -> 0x0071 }
            r0.<init>()     // Catch:{ all -> 0x0071 }
            goto L_0x0070
        L_0x0068:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x0071 }
            r0.<init>()     // Catch:{ all -> 0x0071 }
            goto L_0x0070
        L_0x006e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0071 }
        L_0x0070:
            throw r0     // Catch:{ all -> 0x0071 }
        L_0x0071:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.superpack.SuperpackFileInputStream.read(byte[], int, int):int");
    }
}
