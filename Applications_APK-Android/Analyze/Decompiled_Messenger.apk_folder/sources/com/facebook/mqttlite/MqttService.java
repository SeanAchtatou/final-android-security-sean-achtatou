package com.facebook.mqttlite;

import X.AnonymousClass00S;
import X.AnonymousClass02C;
import X.AnonymousClass067;
import X.AnonymousClass069;
import X.AnonymousClass07A;
import X.AnonymousClass07B;
import X.AnonymousClass07g;
import X.AnonymousClass089;
import X.AnonymousClass08G;
import X.AnonymousClass08I;
import X.AnonymousClass08J;
import X.AnonymousClass08S;
import X.AnonymousClass09P;
import X.AnonymousClass0A1;
import X.AnonymousClass0AD;
import X.AnonymousClass0AI;
import X.AnonymousClass0AQ;
import X.AnonymousClass0AX;
import X.AnonymousClass0B6;
import X.AnonymousClass0CI;
import X.AnonymousClass0CU;
import X.AnonymousClass0CW;
import X.AnonymousClass0ED;
import X.AnonymousClass0PF;
import X.AnonymousClass0SB;
import X.AnonymousClass0UU;
import X.AnonymousClass0UX;
import X.AnonymousClass0VG;
import X.AnonymousClass0WA;
import X.AnonymousClass0X5;
import X.AnonymousClass0X6;
import X.AnonymousClass0ZM;
import X.AnonymousClass1CU;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y7;
import X.AnonymousClass1YI;
import X.C000700l;
import X.C001500z;
import X.C009207y;
import X.C010708t;
import X.C01430Ae;
import X.C01470Aj;
import X.C01540Aq;
import X.C01660Bc;
import X.C01700Bh;
import X.C01760Bn;
import X.C01830Bu;
import X.C02020Cn;
import X.C04140Sd;
import X.C04310Tq;
import X.C04430Uq;
import X.C04460Ut;
import X.C04500Uy;
import X.C04750Wa;
import X.C06550bg;
import X.C07390dL;
import X.C07620dr;
import X.C10930l6;
import X.C11330mk;
import X.C11560nN;
import X.C13840sA;
import X.C15610vZ;
import X.C193517u;
import X.C206729on;
import X.C21041Eu;
import X.C21171Fm;
import X.C29321gE;
import X.C33641nu;
import X.C33771o7;
import X.C33821oC;
import X.C34261pE;
import X.C34301pL;
import X.C34311pM;
import X.C34321pO;
import X.C34331pP;
import X.C34341pQ;
import X.C34351pR;
import X.C34581pq;
import X.C36101sK;
import X.C36111sL;
import X.C36121sM;
import X.C56612qU;
import X.C621430e;
import X.C70873bQ;
import X.EY0;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.gk.sessionless.GkSessionlessModule;
import com.facebook.mqtt.debug.MqttStats;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.push.mqtt.ipc.IMqttPushService;
import com.facebook.push.mqtt.ipc.MqttPubAckCallback;
import com.facebook.push.mqtt.ipc.MqttPublishListener;
import io.card.payment.BuildConfig;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;
import org.json.JSONObject;

public class MqttService extends AnonymousClass0AD implements C04500Uy, AnonymousClass0ZM {
    private static MqttService A0U;
    private static Boolean A0V = false;
    public Handler A00;
    public C04460Ut A01;
    public C04460Ut A02;
    public AnonymousClass09P A03;
    public AnonymousClass069 A04;
    public C001500z A05;
    public AnonymousClass1YI A06;
    public C33821oC A07;
    public C21041Eu A08;
    public MqttStats A09;
    public C36121sM A0A;
    public C34321pO A0B;
    public C34331pP A0C;
    public C34261pE A0D = new C34261pE();
    public C34351pR A0E;
    public C34301pL A0F;
    public FbSharedPreferences A0G;
    public C11330mk A0H;
    public AnonymousClass1CU A0I;
    public C193517u A0J;
    public C21171Fm A0K;
    public C34311pM A0L;
    public AnonymousClass0AQ A0M;
    public Set A0N;
    public ExecutorService A0O;
    public C04310Tq A0P;
    private Looper A0Q;
    public C33771o7 A0R;
    private final C11560nN A0S = new C11560nN();
    private final IMqttPushService.Stub A0T = new IMqttPushService.Stub() {
        {
            C000700l.A09(-1048805209, C000700l.A03(1592728115));
        }

        public boolean Byl(String str, byte[] bArr, long j, MqttPublishListener mqttPublishListener, long j2, String str2) {
            C621430e r10;
            int A03 = C000700l.A03(1536626883);
            MqttService.this.A0A();
            try {
                long j3 = j2;
                if (AnonymousClass07g.A00.contains(str)) {
                    MqttService.this.A0H.BIo(String.format(null, "publishAndWaitForAckWithRefCode/topic:%s/startTime:%d", str, Long.valueOf(j3)));
                }
                C34261pE r5 = MqttService.this.A0D;
                MqttPublishListener mqttPublishListener2 = mqttPublishListener;
                if (mqttPublishListener != null) {
                    r10 = new C621430e(mqttPublishListener2);
                } else {
                    r10 = null;
                }
                boolean A0T = r5.A0T(str, bArr, j, r10, j3, str2);
                C000700l.A09(1258510726, A03);
                return A0T;
            } catch (AnonymousClass0CI | InterruptedException | ExecutionException | TimeoutException e) {
                C010708t.A0U("MqttService", e, "send/publish/exception; topic=%s", str);
                RemoteException remoteException = new RemoteException(e.toString());
                C000700l.A09(-1228980586, A03);
                throw remoteException;
            }
        }

        public boolean AU4(long j) {
            int A03 = C000700l.A03(-1391005948);
            MqttService.this.A0A();
            boolean A0S = MqttService.this.A0D.A0S(j);
            C000700l.A09(1122802416, A03);
            return A0S;
        }

        public String Ae1() {
            int A03 = C000700l.A03(1581062708);
            MqttService.this.A0A();
            String A002 = C206729on.A00(MqttService.this.A0E.A07.A02());
            C000700l.A09(871631665, A03);
            return A002;
        }

        public String AiF() {
            int A03 = C000700l.A03(-1464835529);
            MqttService.this.A0A();
            String name = MqttService.this.A0D.A0A().name();
            C000700l.A09(-691994809, A03);
            return name;
        }

        public String Av6() {
            String str;
            int A03 = C000700l.A03(-156849938);
            MqttService.this.A0A();
            try {
                C04140Sd A06 = MqttService.this.A05.A06(MqttService.this.A0D.A09(), true);
                try {
                    str = C04140Sd.A00(A06, A06.A00).toString();
                } catch (JSONException unused) {
                    str = BuildConfig.FLAVOR;
                }
                C000700l.A09(-78313967, A03);
                return str;
            } catch (Throwable th) {
                String th2 = th.toString();
                C000700l.A09(2092961264, A03);
                return th2;
            }
        }

        public boolean BEB() {
            int A03 = C000700l.A03(2129105304);
            MqttService.this.A0A();
            boolean A0R = MqttService.this.A0D.A0R();
            C000700l.A09(527388292, A03);
            return A0R;
        }

        public int Byg(String str, byte[] bArr, int i, MqttPublishListener mqttPublishListener) {
            C621430e r0;
            int A03 = C000700l.A03(68436753);
            MqttService.this.A0A();
            Integer A01 = AnonymousClass08G.A01(i);
            try {
                C34261pE r1 = MqttService.this.A0D;
                if (mqttPublishListener != null) {
                    r0 = new C621430e(mqttPublishListener);
                } else {
                    r0 = null;
                }
                int A08 = r1.A08(str, bArr, A01, r0);
                C000700l.A09(1705877043, A03);
                return A08;
            } catch (AnonymousClass0CI e) {
                RemoteException remoteException = new RemoteException(e.toString());
                C000700l.A09(46170699, A03);
                throw remoteException;
            }
        }

        public boolean Byj(String str, byte[] bArr, long j, MqttPublishListener mqttPublishListener, long j2) {
            int A03 = C000700l.A03(702873191);
            MqttService.this.A0A();
            boolean Byl = Byl(str, bArr, j, mqttPublishListener, j2, null);
            C000700l.A09(-1631083569, A03);
            return Byl;
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x007b  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int Bym(java.lang.String r19, byte[] r20, long r21, com.facebook.push.mqtt.ipc.MqttPubAckCallback r23) {
            /*
                r18 = this;
                r0 = 913936154(0x36798f1a, float:3.7187187E-6)
                int r3 = X.C000700l.A03(r0)
                r1 = r18
                com.facebook.mqttlite.MqttService r0 = com.facebook.mqttlite.MqttService.this
                r0.A0A()
                com.facebook.mqttlite.MqttService r0 = com.facebook.mqttlite.MqttService.this
                X.1pE r8 = r0.A0D
                X.0PF r6 = new X.0PF
                r0 = r23
                r6.<init>(r0)
                java.lang.String r2 = "FbnsConnectionManager"
                r1 = -1
                r7 = 0
                r9 = r19
                java.lang.Integer r11 = X.AnonymousClass07B.A01     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                r12 = 0
                int r13 = r8.A07()     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                r14 = 0
                r16 = 0
                r17 = 0
                r10 = r20
                X.0Aq r4 = r8.A0U(r9, r10, r11, r12, r13, r14, r16, r17)     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                boolean r0 = r4.A02()     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                if (r0 == 0) goto L_0x004b
                java.lang.Object r0 = r4.A01()     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                X.0CW r0 = (X.AnonymousClass0CW) r0     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                r4 = r21
                r0.CMt(r4)     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0055, TimeoutException -> 0x006e }
                int r1 = r0.AwM()     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0055, TimeoutException -> 0x006e }
                r6.A02(r1)     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0055, TimeoutException -> 0x006e }
                goto L_0x0082
            L_0x004b:
                X.0CI r0 = new X.0CI     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                r0.<init>()     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
                throw r0     // Catch:{ 0CI | InterruptedException | NullPointerException | ExecutionException -> 0x0053, TimeoutException -> 0x0051 }
            L_0x0051:
                r5 = move-exception
                goto L_0x0070
            L_0x0053:
                r5 = move-exception
                goto L_0x0057
            L_0x0055:
                r5 = move-exception
                r7 = r0
            L_0x0057:
                java.lang.String r0 = r5.toString()
                java.lang.Object[] r4 = new java.lang.Object[]{r9, r0}
                java.lang.String r0 = "send/publishAndWaitWithPubAckCallbacks/failed; topic=%s, error=%s"
                X.C010708t.A0U(r2, r5, r0, r4)
                if (r7 == 0) goto L_0x006a
                int r1 = r7.AwM()
            L_0x006a:
                r6.A00(r1)
                goto L_0x0082
            L_0x006e:
                r5 = move-exception
                r7 = r0
            L_0x0070:
                java.lang.Object[] r4 = new java.lang.Object[]{r9}
                java.lang.String r0 = "send/publishAndWaitWithPubAckCallbacks/failed; topic=%s, error=timeoutException"
                X.C010708t.A0U(r2, r5, r0, r4)
                if (r7 == 0) goto L_0x007f
                int r1 = r7.AwM()
            L_0x007f:
                r6.A01(r1)
            L_0x0082:
                r0 = -1025640854(0xffffffffc2ddf66a, float:-110.98128)
                X.C000700l.A09(r0, r3)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.mqttlite.MqttService.AnonymousClass1.Bym(java.lang.String, byte[], long, com.facebook.push.mqtt.ipc.MqttPubAckCallback):int");
        }

        public int Byo(String str, byte[] bArr, int i, MqttPubAckCallback mqttPubAckCallback) {
            int i2;
            int A03 = C000700l.A03(-1650023206);
            MqttService.this.A0A();
            String str2 = str;
            AnonymousClass0A1.A00(str);
            byte[] bArr2 = bArr;
            AnonymousClass0A1.A00(bArr);
            AnonymousClass0A1.A00(Integer.valueOf(i));
            MqttPubAckCallback mqttPubAckCallback2 = mqttPubAckCallback;
            AnonymousClass0A1.A00(mqttPubAckCallback2);
            try {
                C34261pE r3 = MqttService.this.A0D;
                C01540Aq A0U = r3.A0U(str2, bArr2, AnonymousClass08G.A01(i), null, r3.A07(), 0, null, new AnonymousClass0PF(mqttPubAckCallback2));
                if (!A0U.A02()) {
                    i2 = -1;
                } else {
                    i2 = ((AnonymousClass0CW) A0U.A01()).AwM();
                }
                C000700l.A09(-569570851, A03);
                return i2;
            } catch (AnonymousClass0CI unused) {
                RemoteException remoteException = new RemoteException();
                C000700l.A09(-274395773, A03);
                throw remoteException;
            }
        }

        public boolean CIO(List list, int i) {
            int A03 = C000700l.A03(-1374653588);
            MqttService.this.A0A();
            boolean A04 = AnonymousClass00S.A04(MqttService.this.A00, new EY0(this, list, i), -1565511493);
            C000700l.A09(-176962168, A03);
            return A04;
        }

        public boolean isConnected() {
            int A03 = C000700l.A03(-1651839481);
            MqttService.this.A0A();
            boolean A0Q = MqttService.this.A0D.A0Q();
            C000700l.A09(964640106, A03);
            return A0Q;
        }
    };

    private C34581pq A01(AnonymousClass089 r11) {
        long now = this.A04.now();
        C34261pE r0 = this.A0D;
        return new C34581pq(r11, now, r0.A01, r0.A02, r0.A0V);
    }

    public Looper A0B() {
        if (this.A0Q == null) {
            this.A0Q = new C36101sK(this).A00;
        }
        return this.A0Q;
    }

    public void A0E() {
        C21171Fm r0 = this.A0K;
        AnonymousClass07A.A04(r0.A02, new C70873bQ(r0), -1920570291);
        super.A0E();
        if (A0V != null) {
            A0V = false;
        }
        if (A0U == this) {
            A0U = null;
        }
    }

    public void A0F(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        try {
            printWriter.println("[ MqttService ]");
            printWriter.println("notificationCounter=" + ((AnonymousClass08I) this.A05.A07(AnonymousClass08I.class)).A00(AnonymousClass08J.A05));
            if (this.A06.AbO(76, false)) {
                printWriter.println(AnonymousClass08S.A0J("appId=", this.A0E.A08.AdF()));
                printWriter.println(AnonymousClass08S.A0J("userId=", (String) this.A0E.A09.Arn().first));
                String B4F = this.A0G.B4F(C10930l6.A01, BuildConfig.FLAVOR);
                if (!TextUtils.isEmpty(B4F)) {
                    printWriter.println(AnonymousClass08S.A0P("fbnsToken=\"", B4F, "\""));
                }
                try {
                    printWriter.println(AnonymousClass08S.A0P("fbnsliteToken=\"", new JSONObject(this.A0G.B4F(C10930l6.A00, BuildConfig.FLAVOR)).getString("k"), "\""));
                } catch (JSONException unused) {
                }
                printWriter.println(AnonymousClass08S.A0J("deviceId=", this.A0E.A08.getDeviceId()));
            }
        } catch (Exception unused2) {
        }
        super.A0F(fileDescriptor, printWriter, strArr);
    }

    public AnonymousClass09P A0G() {
        return this.A03;
    }

    public AnonymousClass0AX A0H() {
        MqttService mqttService = A0U;
        if (mqttService != null) {
            mqttService.A0K();
            this.A02.C4y("ACTION_MQTT_FORCE_REBIND");
        }
        A0U = this;
        this.A0J.A04(A01(AnonymousClass089.DISCONNECTED));
        AnonymousClass0AX r2 = new AnonymousClass0AX();
        this.A0E.A00(this, r2, A0I(), this.A0D, this.A0D);
        return r2;
    }

    public Integer A0I() {
        return AnonymousClass07B.A00;
    }

    public String A0J() {
        return ((C06550bg) this.A0P.get()).name();
    }

    public void A0T(AnonymousClass0SB r3) {
        if (r3 == AnonymousClass0SB.A04) {
            this.A01.C4x(new Intent("ACTION_MQTT_NO_AUTH"));
        }
    }

    public void A0W(String str, long j, boolean z) {
        if (str.startsWith(TurboLoader.Locator.$const$string(51))) {
            str = AnonymousClass0AI.A01(str.substring(8));
        }
        this.A09.A01(str, j, z);
    }

    public void A0X(String str, String str2, Throwable th) {
        this.A03.softReport(str, str2, th);
    }

    public void A0Y(String str, byte[] bArr, int i, long j, AnonymousClass0CU r23) {
        String str2 = str;
        byte[] bArr2 = bArr;
        int i2 = i;
        AnonymousClass0CU r8 = r23;
        long j2 = j;
        super.A0Y(str2, bArr2, i2, j2, r8);
        AnonymousClass07A.A04(this.A0O, new C56612qU(this, str2, bArr2, j2, i2, r8), 115990033);
    }

    public Object Aze(Object obj) {
        return this.A0S.A00(obj);
    }

    public void CB0(Object obj, Object obj2) {
        this.A0S.A01(obj, obj2);
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r9) {
        Long l;
        long j;
        boolean Aep = this.A0G.Aep(C34341pQ.A00, true);
        JSONObject jSONObject = new JSONObject();
        try {
            C21041Eu r3 = this.A08;
            if (r3.A04.A00.AbO(AnonymousClass1Y3.A6h, false)) {
                if (Aep) {
                    j = r3.A04(false);
                } else {
                    j = r3.A03(false);
                }
                l = Long.valueOf(j);
            } else {
                l = null;
            }
            if (l != null) {
                jSONObject.put(TraceFieldType.RequestID, l);
            }
            jSONObject.put("make_user_available_when_in_foreground", Aep);
            C34261pE r4 = this.A0D;
            String jSONObject2 = jSONObject.toString();
            r4.A08("/set_client_settings", AnonymousClass0ED.A00(jSONObject2), AnonymousClass07B.A00, null);
            C33821oC r2 = this.A07;
            if (Aep) {
                C33821oC.A02(r2, "client_presence_availability_preference_switch_on_published", C33821oC.A01(r2.A01.A04(false)));
            } else {
                C33821oC.A02(r2, "client_presence_availability_preference_switch_off_published", C33821oC.A01(r2.A01.A03(false)));
            }
        } catch (AnonymousClass0CI | JSONException unused) {
        }
    }

    public void A0C() {
        C07620dr.A00(this);
        AnonymousClass1XX r1 = AnonymousClass1XX.get(this);
        this.A0M = new C36111sL(r1);
        this.A0F = new C34301pL(r1);
        this.A09 = MqttStats.A00(r1);
        this.A0N = new AnonymousClass0X5(r1, AnonymousClass0X6.A2R);
        this.A0L = C34311pM.A00(r1);
        this.A04 = AnonymousClass067.A03(r1);
        this.A0G = FbSharedPreferencesModule.A00(r1);
        this.A03 = C04750Wa.A01(r1);
        this.A01 = C07390dL.A00(r1);
        this.A02 = C04430Uq.A02(r1);
        this.A0O = AnonymousClass0UX.A0d(r1);
        this.A06 = AnonymousClass0WA.A00(r1);
        GkSessionlessModule.A00(r1);
        this.A0A = C36121sM.A01(r1);
        this.A0P = AnonymousClass0VG.A00(AnonymousClass1Y3.A5z, r1);
        this.A05 = AnonymousClass0UU.A05(r1);
        this.A0J = C193517u.A00(r1);
        this.A0K = C21171Fm.A00(r1);
        this.A0I = AnonymousClass1CU.A00(r1);
        this.A07 = C33821oC.A00(r1);
        this.A08 = C21041Eu.A00(r1);
        this.A00 = C29321gE.A00(r1);
        this.A0B = C34321pO.A00(r1);
        this.A0C = C34331pP.A00(r1);
        this.A0E = new C34351pR(this.A0F, this.A0M);
        this.A0H = this.A0I.A01("mqtt_instance");
        super.A0C();
    }

    public void A0L() {
        super.A0L();
        C21171Fm r3 = this.A0K;
        AnonymousClass07A.A04(r3.A02, new C15610vZ(r3, new C33641nu(this)), 678352645);
    }

    public void A0M() {
        super.A0M();
        this.A0R = new C33771o7(this.A0D, this.A0L, this.A05, this.A0N, this.A03, this.A0E);
        Boolean bool = A0V;
        if (bool == null) {
            return;
        }
        if (bool.booleanValue()) {
            A0V = null;
            AnonymousClass0B6 r2 = this.A0E;
            String A0J2 = AnonymousClass08S.A0J(C01430Ae.A00(A0I()), ".SERVICE_DOUBLE_BOOTSTRAP");
            String A0J3 = A0J();
            C01660Bc r6 = C01660Bc.A00;
            r2.A06(A0J2, A0J3, null, r6, r6, this.A0C.get(), -1, 0, null);
            return;
        }
        A0V = true;
    }

    public void A0O() {
        super.A0O();
        if (this.A05 == C001500z.A07) {
            C13840sA r3 = new C13840sA(this);
            C01470Aj r5 = this.A07;
            boolean equals = r5.A03.getLooper().equals(Looper.myLooper());
            C01700Bh.A00(equals, "ScreenStateListener registration should be called on MqttThread. Current Looper:" + Looper.myLooper());
            r5.A00 = r3;
            r5.A02.registerReceiver(r5.A01, C01470Aj.A07, null, r5.A03);
        }
        this.A0G.C0f(C34341pQ.A00, this);
        this.A0A.A04(this.A00);
    }

    public void A0P() {
        super.A0P();
        this.A0G.CJr(C34341pQ.A00, this);
        C36121sM r3 = this.A0A;
        BroadcastReceiver broadcastReceiver = r3.A00;
        if (broadcastReceiver != null) {
            C009207y.A01.A07(r3.A03, broadcastReceiver);
            r3.A00 = null;
        }
    }

    public void A0U(AnonymousClass089 r3) {
        this.A0J.A04(A01(r3));
    }

    public void A0V(C02020Cn r5) {
        super.A0V(r5);
        ((AtomicLong) ((AnonymousClass08I) this.A05.A07(AnonymousClass08I.class)).A00(AnonymousClass08J.A04)).addAndGet((long) this.A0A.A02());
    }

    public boolean A0Z() {
        if (!super.A0Z() || this.A0E.A09.Arn() == C01830Bu.A00) {
            return false;
        }
        return true;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        boolean booleanValue;
        int A012 = AnonymousClass02C.A01(this, -2102351232);
        int onStartCommand = super.onStartCommand(intent, i, i2);
        if (onStartCommand != 1) {
            AnonymousClass02C.A02(-1774946580, A012);
            return onStartCommand;
        }
        synchronized (C01760Bn.class) {
            if (C01760Bn.A00 == null) {
                C01760Bn.A00 = Boolean.valueOf(getSharedPreferences("mqtt_stickiness_controller", 0).getBoolean("mqtt_service_nonsticky", false));
            }
            booleanValue = C01760Bn.A00.booleanValue();
        }
        if (booleanValue) {
            AnonymousClass02C.A02(2077879395, A012);
            return 2;
        }
        AnonymousClass02C.A02(-1045700754, A012);
        return 1;
    }

    public IBinder onBind(Intent intent) {
        return this.A0T;
    }
}
