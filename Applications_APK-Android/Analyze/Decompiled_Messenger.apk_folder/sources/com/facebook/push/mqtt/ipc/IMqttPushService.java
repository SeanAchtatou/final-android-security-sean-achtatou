package com.facebook.push.mqtt.ipc;

import X.AnonymousClass1Y3;
import X.AnonymousClass80H;
import X.C000700l;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.push.mqtt.ipc.MqttPublishListener;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.util.List;
import org.webrtc.audio.WebRtcAudioRecord;

public interface IMqttPushService extends IInterface {

    public abstract class Stub extends Binder implements IMqttPushService {

        public final class Proxy implements IMqttPushService {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(-413674820);
                this.A00 = iBinder;
                C000700l.A09(-1948480499, A03);
            }

            public boolean AU4(long j) {
                int A03 = C000700l.A03(-255580311);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    obtain.writeLong(j);
                    boolean z = false;
                    this.A00.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1222900924, A03);
                }
            }

            public String Ae1() {
                int A03 = C000700l.A03(1619972093);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    this.A00.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-950538132, A03);
                }
            }

            public String AiF() {
                int A03 = C000700l.A03(-2143373483);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    this.A00.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(126253573, A03);
                }
            }

            public String Av6() {
                int A03 = C000700l.A03(-1007651026);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    this.A00.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1684819821, A03);
                }
            }

            public boolean BEB() {
                int A03 = C000700l.A03(-2001661702);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    boolean z = false;
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(989161270, A03);
                }
            }

            public int Byg(String str, byte[] bArr, int i, MqttPublishListener mqttPublishListener) {
                IBinder iBinder;
                int A03 = C000700l.A03(-638605197);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeInt(i);
                    if (mqttPublishListener != null) {
                        iBinder = mqttPublishListener.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.A00.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1149736483, A03);
                }
            }

            public boolean Byj(String str, byte[] bArr, long j, MqttPublishListener mqttPublishListener, long j2) {
                IBinder iBinder;
                int A03 = C000700l.A03(-1435416255);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeLong(j);
                    if (mqttPublishListener != null) {
                        iBinder = mqttPublishListener.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeLong(j2);
                    boolean z = false;
                    this.A00.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1487467057, A03);
                }
            }

            public boolean Byl(String str, byte[] bArr, long j, MqttPublishListener mqttPublishListener, long j2, String str2) {
                IBinder iBinder;
                int A03 = C000700l.A03(-549674108);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeLong(j);
                    if (mqttPublishListener != null) {
                        iBinder = mqttPublishListener.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeLong(j2);
                    obtain.writeString(str2);
                    boolean z = false;
                    this.A00.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-2130862898, A03);
                }
            }

            public int Bym(String str, byte[] bArr, long j, MqttPubAckCallback mqttPubAckCallback) {
                IBinder iBinder;
                int A03 = C000700l.A03(2019364500);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeLong(j);
                    if (mqttPubAckCallback != null) {
                        iBinder = mqttPubAckCallback.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.A00.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1840624454, A03);
                }
            }

            public int Byo(String str, byte[] bArr, int i, MqttPubAckCallback mqttPubAckCallback) {
                IBinder iBinder;
                int A03 = C000700l.A03(-776136342);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    obtain.writeString(str);
                    obtain.writeByteArray(bArr);
                    obtain.writeInt(i);
                    if (mqttPubAckCallback != null) {
                        iBinder = mqttPubAckCallback.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.A00.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1324392521, A03);
                }
            }

            public boolean CIO(List list, int i) {
                int A03 = C000700l.A03(225596749);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    obtain.writeTypedList(list);
                    obtain.writeInt(i);
                    boolean z = false;
                    this.A00.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1099498243, A03);
                }
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(1725548853);
                IBinder iBinder = this.A00;
                C000700l.A09(193674799, A03);
                return iBinder;
            }

            public boolean isConnected() {
                int A03 = C000700l.A03(-1864955878);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.mqtt.ipc.IMqttPushService");
                    boolean z = false;
                    this.A00.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(343413505, A03);
                }
            }
        }

        public static MqttPubAckCallback A00(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(AnonymousClass80H.$const$string(AnonymousClass1Y3.A3W));
            if (queryLocalInterface == null || !(queryLocalInterface instanceof MqttPubAckCallback)) {
                return new MqttPubAckCallback$Stub$Proxy(iBinder);
            }
            return (MqttPubAckCallback) queryLocalInterface;
        }

        public Stub() {
            int A03 = C000700l.A03(865785400);
            attachInterface(this, AnonymousClass80H.$const$string(56));
            C000700l.A09(1835247252, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(-83868798, C000700l.A03(1162142793));
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            int A03 = C000700l.A03(1543883801);
            String $const$string = AnonymousClass80H.$const$string(56);
            int i3 = i;
            Parcel parcel3 = parcel2;
            if (i3 != 1598968902) {
                Parcel parcel4 = parcel;
                switch (i) {
                    case 1:
                        parcel4.enforceInterface($const$string);
                        boolean BEB = BEB();
                        parcel3.writeNoException();
                        parcel3.writeInt(BEB ? 1 : 0);
                        C000700l.A09(843190737, A03);
                        return true;
                    case 2:
                        parcel4.enforceInterface($const$string);
                        boolean isConnected = isConnected();
                        parcel3.writeNoException();
                        parcel3.writeInt(isConnected ? 1 : 0);
                        C000700l.A09(-175619006, A03);
                        return true;
                    case 3:
                        parcel4.enforceInterface($const$string);
                        boolean AU4 = AU4(parcel4.readLong());
                        parcel3.writeNoException();
                        parcel3.writeInt(AU4 ? 1 : 0);
                        C000700l.A09(-801639485, A03);
                        return true;
                    case 4:
                        parcel4.enforceInterface($const$string);
                        boolean CIO = CIO(parcel4.createTypedArrayList(SubscribeTopic.CREATOR), parcel4.readInt());
                        parcel3.writeNoException();
                        parcel3.writeInt(CIO ? 1 : 0);
                        C000700l.A09(1623278504, A03);
                        return true;
                    case 5:
                        parcel4.enforceInterface($const$string);
                        int Byg = Byg(parcel4.readString(), parcel4.createByteArray(), parcel4.readInt(), MqttPublishListener.Stub.A00(parcel4.readStrongBinder()));
                        parcel3.writeNoException();
                        parcel3.writeInt(Byg);
                        C000700l.A09(-1928959810, A03);
                        return true;
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        parcel4.enforceInterface($const$string);
                        boolean Byj = Byj(parcel4.readString(), parcel4.createByteArray(), parcel4.readLong(), MqttPublishListener.Stub.A00(parcel4.readStrongBinder()), parcel4.readLong());
                        parcel3.writeNoException();
                        parcel3.writeInt(Byj ? 1 : 0);
                        C000700l.A09(-780511620, A03);
                        return true;
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                        parcel4.enforceInterface($const$string);
                        boolean Byl = Byl(parcel4.readString(), parcel4.createByteArray(), parcel4.readLong(), MqttPublishListener.Stub.A00(parcel4.readStrongBinder()), parcel4.readLong(), parcel4.readString());
                        parcel3.writeNoException();
                        parcel3.writeInt(Byl ? 1 : 0);
                        C000700l.A09(-1801143079, A03);
                        return true;
                    case 8:
                        parcel4.enforceInterface($const$string);
                        int Byo = Byo(parcel4.readString(), parcel4.createByteArray(), parcel4.readInt(), A00(parcel4.readStrongBinder()));
                        parcel3.writeNoException();
                        parcel3.writeInt(Byo);
                        C000700l.A09(-1070613498, A03);
                        return true;
                    case Process.SIGKILL:
                        parcel4.enforceInterface($const$string);
                        int Bym = Bym(parcel4.readString(), parcel4.createByteArray(), parcel4.readLong(), A00(parcel4.readStrongBinder()));
                        parcel3.writeNoException();
                        parcel3.writeInt(Bym);
                        C000700l.A09(1147640188, A03);
                        return true;
                    case AnonymousClass1Y3.A01:
                        parcel4.enforceInterface($const$string);
                        String AiF = AiF();
                        parcel3.writeNoException();
                        parcel3.writeString(AiF);
                        C000700l.A09(-1192608431, A03);
                        return true;
                    case AnonymousClass1Y3.A02:
                        parcel4.enforceInterface($const$string);
                        String Av6 = Av6();
                        parcel3.writeNoException();
                        parcel3.writeString(Av6);
                        C000700l.A09(172162845, A03);
                        return true;
                    case AnonymousClass1Y3.A03:
                        parcel4.enforceInterface($const$string);
                        String Ae1 = Ae1();
                        parcel3.writeNoException();
                        parcel3.writeString(Ae1);
                        C000700l.A09(721232477, A03);
                        return true;
                    default:
                        boolean onTransact = super.onTransact(i3, parcel4, parcel3, i2);
                        C000700l.A09(-1622354504, A03);
                        return onTransact;
                }
            } else {
                parcel3.writeString($const$string);
                C000700l.A09(29913409, A03);
                return true;
            }
        }
    }

    boolean AU4(long j);

    String Ae1();

    String AiF();

    String Av6();

    boolean BEB();

    int Byg(String str, byte[] bArr, int i, MqttPublishListener mqttPublishListener);

    boolean Byj(String str, byte[] bArr, long j, MqttPublishListener mqttPublishListener, long j2);

    boolean Byl(String str, byte[] bArr, long j, MqttPublishListener mqttPublishListener, long j2, String str2);

    int Bym(String str, byte[] bArr, long j, MqttPubAckCallback mqttPubAckCallback);

    int Byo(String str, byte[] bArr, int i, MqttPubAckCallback mqttPubAckCallback);

    boolean CIO(List list, int i);

    boolean isConnected();
}
