package com.facebook.push.fbns.ipc;

import X.C03070Hw;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public final class FbnsAIDLRequest extends FbnsAIDLResult {
    public static final Parcelable.Creator CREATOR = new C03070Hw();
    public int A00;

    public void A00(Parcel parcel, int i) {
        super.A00(parcel, i);
        parcel.writeInt(this.A00);
    }

    public FbnsAIDLRequest(Bundle bundle, int i) {
        super(bundle);
        this.A00 = i;
    }

    public FbnsAIDLRequest(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
    }
}
