package com.facebook.push.fbns.ipc;

import X.C000700l;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public interface IFbnsAIDLService extends IInterface {

    public abstract class Stub extends Binder implements IFbnsAIDLService {

        public final class Proxy implements IFbnsAIDLService {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(502217712);
                this.A00 = iBinder;
                C000700l.A09(1131072382, A03);
            }

            public FbnsAIDLResult BzG(FbnsAIDLRequest fbnsAIDLRequest) {
                FbnsAIDLResult fbnsAIDLResult;
                int A03 = C000700l.A03(1819998277);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.fbns.ipc.IFbnsAIDLService");
                    if (fbnsAIDLRequest != null) {
                        obtain.writeInt(1);
                        fbnsAIDLRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        fbnsAIDLResult = (FbnsAIDLResult) FbnsAIDLResult.CREATOR.createFromParcel(obtain2);
                    } else {
                        fbnsAIDLResult = null;
                    }
                    return fbnsAIDLResult;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1651473098, A03);
                }
            }

            public void CK8(FbnsAIDLRequest fbnsAIDLRequest) {
                int A03 = C000700l.A03(1204005627);
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.push.fbns.ipc.IFbnsAIDLService");
                    if (fbnsAIDLRequest != null) {
                        obtain.writeInt(1);
                        fbnsAIDLRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(2, obtain, null, 1);
                } finally {
                    obtain.recycle();
                    C000700l.A09(1041951890, A03);
                }
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(-335947478);
                IBinder iBinder = this.A00;
                C000700l.A09(-1666669739, A03);
                return iBinder;
            }
        }

        public Stub() {
            int A03 = C000700l.A03(-986241021);
            attachInterface(this, "com.facebook.push.fbns.ipc.IFbnsAIDLService");
            C000700l.A09(-1802141584, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(920453875, C000700l.A03(1307666724));
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            int A03 = C000700l.A03(-761289823);
            FbnsAIDLRequest fbnsAIDLRequest = null;
            if (i == 1) {
                parcel.enforceInterface("com.facebook.push.fbns.ipc.IFbnsAIDLService");
                if (parcel.readInt() != 0) {
                    fbnsAIDLRequest = (FbnsAIDLRequest) FbnsAIDLRequest.CREATOR.createFromParcel(parcel);
                }
                FbnsAIDLResult BzG = BzG(fbnsAIDLRequest);
                parcel2.writeNoException();
                if (BzG != null) {
                    parcel2.writeInt(1);
                    BzG.writeToParcel(parcel2, 1);
                } else {
                    parcel2.writeInt(0);
                }
                C000700l.A09(-925902130, A03);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("com.facebook.push.fbns.ipc.IFbnsAIDLService");
                if (parcel.readInt() != 0) {
                    fbnsAIDLRequest = (FbnsAIDLRequest) FbnsAIDLRequest.CREATOR.createFromParcel(parcel);
                }
                CK8(fbnsAIDLRequest);
                C000700l.A09(987443338, A03);
                return true;
            } else if (i != 1598968902) {
                boolean onTransact = super.onTransact(i, parcel, parcel2, i2);
                C000700l.A09(-1143376027, A03);
                return onTransact;
            } else {
                parcel2.writeString("com.facebook.push.fbns.ipc.IFbnsAIDLService");
                C000700l.A09(-1081379517, A03);
                return true;
            }
        }
    }

    FbnsAIDLResult BzG(FbnsAIDLRequest fbnsAIDLRequest);

    void CK8(FbnsAIDLRequest fbnsAIDLRequest);
}
