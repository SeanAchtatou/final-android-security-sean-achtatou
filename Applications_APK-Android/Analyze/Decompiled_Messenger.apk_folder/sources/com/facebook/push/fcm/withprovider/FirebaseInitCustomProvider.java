package com.facebook.push.fcm.withprovider;

import X.AnonymousClass09D;
import X.C010708t;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class FirebaseInitCustomProvider extends ContentProvider {
    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public boolean onCreate() {
        Context context = getContext();
        if (context != null) {
            return AnonymousClass09D.A02(context);
        }
        C010708t.A0I("FirebaseCustomInitProvider", "FirebaseApp custom init failure: context is null.");
        return false;
    }
}
