package com.facebook.reactivesocket.livequery.common;

import X.AnonymousClass0UN;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;

public class LiveQueryServiceFactory {
    private AnonymousClass0UN $ul_mInjectionContext;

    public static final LiveQueryServiceFactory $ul_$xXXcom_facebook_reactivesocket_livequery_common_LiveQueryServiceFactory$xXXFACTORY_METHOD(AnonymousClass1XY r1) {
        return new LiveQueryServiceFactory(r1);
    }

    public LiveQueryService build() {
        return (LiveQueryService) AnonymousClass1XX.A03(AnonymousClass1Y3.AGq, this.$ul_mInjectionContext);
    }

    public LiveQueryServiceFactory(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(0, r3);
    }
}
