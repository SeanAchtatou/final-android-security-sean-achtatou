package com.facebook.reactivesocket.flipper.common;

import X.AnonymousClass0UN;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;

public class FlipperLiveDataProviderFactory {
    private AnonymousClass0UN $ul_mInjectionContext;

    public static final FlipperLiveDataProviderFactory $ul_$xXXcom_facebook_reactivesocket_flipper_common_FlipperLiveDataProviderFactory$xXXFACTORY_METHOD(AnonymousClass1XY r1) {
        return new FlipperLiveDataProviderFactory(r1);
    }

    public FlipperLiveDataProvider build() {
        return (FlipperLiveDataProvider) AnonymousClass1XX.A03(AnonymousClass1Y3.A7z, this.$ul_mInjectionContext);
    }

    public FlipperLiveDataProviderFactory(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(0, r3);
    }
}
