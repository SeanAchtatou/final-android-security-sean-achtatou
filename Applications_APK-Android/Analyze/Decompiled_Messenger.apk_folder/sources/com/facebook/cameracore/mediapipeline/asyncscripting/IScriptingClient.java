package com.facebook.cameracore.mediapipeline.asyncscripting;

import X.C000700l;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public interface IScriptingClient extends IInterface {

    public abstract class Stub extends Binder implements IScriptingClient {

        public final class Proxy implements IScriptingClient {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(453762452);
                this.A00 = iBinder;
                C000700l.A09(-722914831, A03);
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(1221083897);
                IBinder iBinder = this.A00;
                C000700l.A09(-1481972022, A03);
                return iBinder;
            }

            public String call(String str) {
                int A03 = C000700l.A03(-1132442133);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                    obtain.writeString(str);
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(529076104, A03);
                }
            }

            public void onObjectsReleased(String str) {
                int A03 = C000700l.A03(-1635529680);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                    obtain.writeString(str);
                    this.A00.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1022884156, A03);
                }
            }

            public void onScriptingError(String str) {
                int A03 = C000700l.A03(-1576007521);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                    obtain.writeString(str);
                    this.A00.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1189400427, A03);
                }
            }

            public String postMsg(String str) {
                int A03 = C000700l.A03(-839080438);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                    obtain.writeString(str);
                    this.A00.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1361123404, A03);
                }
            }
        }

        public Stub() {
            int A03 = C000700l.A03(-1987584607);
            attachInterface(this, "com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
            C000700l.A09(1938215930, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(-1465280929, C000700l.A03(1175370875));
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            int A03 = C000700l.A03(495627488);
            if (i == 1) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                String call = call(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeString(call);
                C000700l.A09(-887385785, A03);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                String postMsg = postMsg(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeString(postMsg);
                C000700l.A09(-981056117, A03);
                return true;
            } else if (i == 3) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                onScriptingError(parcel.readString());
                parcel2.writeNoException();
                C000700l.A09(1032047546, A03);
                return true;
            } else if (i == 4) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                onObjectsReleased(parcel.readString());
                parcel2.writeNoException();
                C000700l.A09(1777351722, A03);
                return true;
            } else if (i != 1598968902) {
                boolean onTransact = super.onTransact(i, parcel, parcel2, i2);
                C000700l.A09(-107051023, A03);
                return onTransact;
            } else {
                parcel2.writeString("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                C000700l.A09(-1229750545, A03);
                return true;
            }
        }
    }

    String call(String str);

    void onObjectsReleased(String str);

    void onScriptingError(String str);

    String postMsg(String str);
}
