package com.facebook.cameracore.mediapipeline.asyncscripting;

import X.AnonymousClass01q;
import X.C000700l;
import X.C010708t;
import android.os.RemoteException;
import com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm;
import com.facebook.jni.HybridData;

public class JsVm extends IJsVm.Stub {
    private final IScriptingClient mClient;
    private final HybridData mHybridData = initHybrid();

    private native HybridData initHybrid();

    public native void enqueueMessages(String str);

    public native void enqueueScript(String str);

    public native void execute();

    public native void gc();

    public native void init();

    static {
        AnonymousClass01q.A08("graphicsengine-asyncscripting-service-native");
    }

    public JsVm(IScriptingClient iScriptingClient) {
        int A03 = C000700l.A03(1428991706);
        this.mClient = iScriptingClient;
        C000700l.A09(-2055602778, A03);
    }

    public String call(String str) {
        int A03 = C000700l.A03(-1053987904);
        try {
            String call = this.mClient.call(str);
            C000700l.A09(857861290, A03);
            return call;
        } catch (RemoteException e) {
            C010708t.A08(JsVm.class, "call failed", e);
            C000700l.A09(1674538201, A03);
            return "0Remote execution failed.";
        }
    }

    public void destroy() {
        int A03 = C000700l.A03(-1635354059);
        this.mHybridData.resetNative();
        C000700l.A09(-930874251, A03);
    }

    public void onObjectsReleased(String str) {
        int A03 = C000700l.A03(1892974737);
        try {
            this.mClient.onObjectsReleased(str);
        } catch (RemoteException e) {
            C010708t.A08(JsVm.class, "onObjectsReleased failed", e);
        }
        C000700l.A09(-1014655151, A03);
    }

    public void onScriptingError(String str) {
        int A03 = C000700l.A03(965678205);
        try {
            this.mClient.onScriptingError(str);
        } catch (RemoteException e) {
            C010708t.A08(JsVm.class, "onScriptingError failed", e);
        }
        C000700l.A09(-853584479, A03);
    }

    public String post(String str) {
        int A03 = C000700l.A03(1520946115);
        try {
            String postMsg = this.mClient.postMsg(str);
            C000700l.A09(93752082, A03);
            return postMsg;
        } catch (RemoteException e) {
            C010708t.A08(JsVm.class, "post failed", e);
            C000700l.A09(1656787009, A03);
            return "0Remote execution failed.";
        }
    }
}
