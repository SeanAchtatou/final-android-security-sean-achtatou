package com.facebook.cameracore.mediapipeline.asyncscripting;

import X.AnonymousClass00S;
import X.AnonymousClass02C;
import X.C000700l;
import X.C010708t;
import X.C03270Kt;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService;
import java.util.concurrent.FutureTask;

public class AsyncScriptingService extends Service {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    private final IAsyncScriptingService.Stub A01 = new IAsyncScriptingService.Stub() {
        {
            C000700l.A09(496344847, C000700l.A03(1050089820));
        }

        public IJsVm AWI(IScriptingClient iScriptingClient) {
            int A03 = C000700l.A03(-217309775);
            try {
                AsyncScriptingService asyncScriptingService = AsyncScriptingService.this;
                FutureTask futureTask = new FutureTask(new C03270Kt(iScriptingClient));
                AnonymousClass00S.A04(asyncScriptingService.A00, futureTask, -2067022467);
                JsVm jsVm = (JsVm) futureTask.get();
                jsVm.init();
                C000700l.A09(1194790814, A03);
                return jsVm;
            } catch (Exception e) {
                C010708t.A08(AsyncScriptingService.class, "createVm failed", e);
                C000700l.A09(-313062250, A03);
                return null;
            }
        }
    };

    public int onStartCommand(Intent intent, int i, int i2) {
        int A012 = AnonymousClass02C.A01(this, -734984540);
        int onStartCommand = super.onStartCommand(intent, i, i2);
        AnonymousClass02C.A02(-144220526, A012);
        return onStartCommand;
    }

    public IBinder onBind(Intent intent) {
        return this.A01;
    }
}
