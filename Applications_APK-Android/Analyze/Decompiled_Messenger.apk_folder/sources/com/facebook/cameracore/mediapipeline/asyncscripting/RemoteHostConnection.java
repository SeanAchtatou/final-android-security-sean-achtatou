package com.facebook.cameracore.mediapipeline.asyncscripting;

import X.AnonymousClass00S;
import X.AnonymousClass01q;
import X.AnonymousClass0NO;
import X.C000700l;
import X.C010708t;
import X.C03260Kr;
import android.os.RemoteException;
import com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient;
import com.facebook.jni.HybridData;
import io.card.payment.BuildConfig;

public class RemoteHostConnection extends IScriptingClient.Stub {
    private final AnonymousClass0NO mCallback;
    public final HybridData mHybridData;
    public final Object mLock = new Object();
    public IAsyncScriptingService mService;
    public IJsVm mVm;

    private native HybridData initHybrid();

    public native String call(String str);

    public native void onConnected();

    public native void onDisconnected();

    public native void onObjectsReleased(String str);

    public native void onScriptingError(String str);

    public native String postMsg(String str);

    static {
        AnonymousClass01q.A08("graphicsengine-asyncscripting-native");
    }

    public RemoteHostConnection(AnonymousClass0NO r3) {
        int A03 = C000700l.A03(-311500060);
        this.mCallback = r3;
        this.mHybridData = initHybrid();
        C000700l.A09(-1063310889, A03);
    }

    public void destroy() {
        int A03 = C000700l.A03(928534539);
        synchronized (this.mLock) {
            try {
                IJsVm iJsVm = this.mVm;
                if (iJsVm != null) {
                    iJsVm.destroy();
                }
            } catch (RemoteException e) {
                C010708t.A08(RemoteHostConnection.class, "destroy failed", e);
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(100995229, A03);
                    throw th;
                }
            }
            this.mHybridData.resetNative();
        }
        AnonymousClass0NO r1 = this.mCallback;
        synchronized (r1.A00.mConnections) {
            r1.A00.mConnections.remove(this);
            if (r1.A00.mConnections.isEmpty()) {
                AsyncScriptingClient asyncScriptingClient = r1.A00;
                AnonymousClass00S.A04(asyncScriptingClient.mMainThreadHandler, new C03260Kr(asyncScriptingClient), 54250609);
            }
        }
        C000700l.A09(610643559, A03);
    }

    public boolean execute(String str, String str2) {
        int A03 = C000700l.A03(-1869552225);
        synchronized (this.mLock) {
            try {
                IJsVm iJsVm = this.mVm;
                if (iJsVm != null) {
                    while (!str.isEmpty()) {
                        if (str.length() > 56320) {
                            iJsVm.enqueueMessages(str.substring(0, 51200));
                            str = str.substring(51200);
                        } else {
                            iJsVm.enqueueMessages(str);
                            str = BuildConfig.FLAVOR;
                        }
                    }
                    IJsVm iJsVm2 = this.mVm;
                    while (!str2.isEmpty()) {
                        if (str2.length() > 56320) {
                            iJsVm2.enqueueScript(str2.substring(0, 51200));
                            str2 = str2.substring(51200);
                        } else {
                            iJsVm2.enqueueScript(str2);
                            str2 = BuildConfig.FLAVOR;
                        }
                    }
                    this.mVm.execute();
                    C000700l.A09(-2108436817, A03);
                    return true;
                }
                C000700l.A09(1257086561, A03);
                return false;
            } catch (RemoteException e) {
                C010708t.A08(RemoteHostConnection.class, "execute failed", e);
                C000700l.A09(2081448664, A03);
                return false;
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(803535809, A03);
                    throw th;
                }
            }
        }
    }

    public void gc() {
        int A03 = C000700l.A03(-1269348964);
        synchronized (this.mLock) {
            try {
                IJsVm iJsVm = this.mVm;
                if (iJsVm != null) {
                    iJsVm.gc();
                }
            } catch (RemoteException e) {
                C010708t.A08(RemoteHostConnection.class, "gc failed", e);
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(994182981, A03);
                    throw th;
                }
            }
        }
        C000700l.A09(-1627498408, A03);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        X.C000700l.A09(2123924163, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean init() {
        /*
            r6 = this;
            r0 = -747855384(0xffffffffd36ca1e8, float:-1.01632862E12)
            int r5 = X.C000700l.A03(r0)
            java.lang.Object r4 = r6.mLock
            monitor-enter(r4)
            com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService r0 = r6.mService     // Catch:{ all -> 0x0033 }
            r3 = 0
            if (r0 != 0) goto L_0x0017
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            r0 = 1437700761(0x55b19299, float:2.44053988E13)
            X.C000700l.A09(r0, r5)
            return r3
        L_0x0017:
            com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm r0 = r0.AWI(r6)     // Catch:{ RemoteException -> 0x001e }
            r6.mVm = r0     // Catch:{ RemoteException -> 0x001e }
            goto L_0x0026
        L_0x001e:
            r2 = move-exception
            java.lang.Class<com.facebook.cameracore.mediapipeline.asyncscripting.RemoteHostConnection> r1 = com.facebook.cameracore.mediapipeline.asyncscripting.RemoteHostConnection.class
            java.lang.String r0 = "createVm failed"
            X.C010708t.A08(r1, r0, r2)     // Catch:{ all -> 0x0033 }
        L_0x0026:
            com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm r0 = r6.mVm     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x002b
            r3 = 1
        L_0x002b:
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            r0 = 2123924163(0x7e9882c3, float:1.013608E38)
            X.C000700l.A09(r0, r5)
            return r3
        L_0x0033:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            r0 = -1636423067(0xffffffff9e762a65, float:-1.30318985E-20)
            X.C000700l.A09(r0, r5)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.cameracore.mediapipeline.asyncscripting.RemoteHostConnection.init():boolean");
    }

    public void onServiceConnected(IAsyncScriptingService iAsyncScriptingService) {
        int A03 = C000700l.A03(-132381534);
        synchronized (this.mLock) {
            try {
                if (!this.mHybridData.isValid()) {
                    C000700l.A09(1476000586, A03);
                    return;
                }
                this.mService = iAsyncScriptingService;
                onConnected();
                C000700l.A09(-1966738905, A03);
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(251751505, A03);
                    throw th;
                }
            }
        }
    }
}
