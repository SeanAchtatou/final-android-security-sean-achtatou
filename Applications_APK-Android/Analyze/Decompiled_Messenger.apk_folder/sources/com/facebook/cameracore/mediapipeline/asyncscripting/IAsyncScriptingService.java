package com.facebook.cameracore.mediapipeline.asyncscripting;

import X.C000700l;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm;
import com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient;

public interface IAsyncScriptingService extends IInterface {

    public abstract class Stub extends Binder implements IAsyncScriptingService {

        public final class Proxy implements IAsyncScriptingService {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(924517914);
                this.A00 = iBinder;
                C000700l.A09(1266805516, A03);
            }

            public IJsVm AWI(IScriptingClient iScriptingClient) {
                IBinder iBinder;
                IJsVm iJsVm;
                int A03 = C000700l.A03(-1538577711);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService");
                    if (iScriptingClient != null) {
                        iBinder = iScriptingClient.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    IBinder readStrongBinder = obtain2.readStrongBinder();
                    if (readStrongBinder == null) {
                        iJsVm = null;
                    } else {
                        IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                        if (queryLocalInterface == null || !(queryLocalInterface instanceof IJsVm)) {
                            iJsVm = new IJsVm.Stub.Proxy(readStrongBinder);
                        } else {
                            iJsVm = (IJsVm) queryLocalInterface;
                        }
                    }
                    return iJsVm;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(320755389, A03);
                }
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(1423698248);
                IBinder iBinder = this.A00;
                C000700l.A09(-775474166, A03);
                return iBinder;
            }
        }

        public Stub() {
            int A03 = C000700l.A03(1164939362);
            attachInterface(this, "com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService");
            C000700l.A09(1262119678, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(-1714008954, C000700l.A03(-276172850));
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            IScriptingClient proxy;
            IBinder iBinder;
            int A03 = C000700l.A03(-79947513);
            if (i == 1) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService");
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    proxy = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof IScriptingClient)) {
                        proxy = new IScriptingClient.Stub.Proxy(readStrongBinder);
                    } else {
                        proxy = (IScriptingClient) queryLocalInterface;
                    }
                }
                IJsVm AWI = AWI(proxy);
                parcel2.writeNoException();
                if (AWI != null) {
                    iBinder = AWI.asBinder();
                } else {
                    iBinder = null;
                }
                parcel2.writeStrongBinder(iBinder);
                C000700l.A09(-1525674664, A03);
                return true;
            } else if (i != 1598968902) {
                boolean onTransact = super.onTransact(i, parcel, parcel2, i2);
                C000700l.A09(2028596487, A03);
                return onTransact;
            } else {
                parcel2.writeString("com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService");
                C000700l.A09(1684625220, A03);
                return true;
            }
        }
    }

    IJsVm AWI(IScriptingClient iScriptingClient);
}
