package com.facebook.cameracore.mediapipeline.asyncscripting;

import X.AnonymousClass00S;
import X.AnonymousClass0NO;
import X.C03250Kq;
import X.C03260Kr;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import java.util.HashSet;
import java.util.Set;

public class AsyncScriptingClient {
    public final Set mConnections = new HashSet();
    public final Context mContext;
    private final AnonymousClass0NO mDestroyCallback = new AnonymousClass0NO(this);
    public final Handler mMainThreadHandler;
    public IAsyncScriptingService mRemoteService;
    public boolean mRunning = false;
    public final ServiceConnection mServiceConnection = new C03250Kq(this);

    public RemoteHostConnection createConnection() {
        RemoteHostConnection remoteHostConnection;
        synchronized (this.mConnections) {
            remoteHostConnection = new RemoteHostConnection(this.mDestroyCallback);
            this.mConnections.add(remoteHostConnection);
            IAsyncScriptingService iAsyncScriptingService = this.mRemoteService;
            if (iAsyncScriptingService != null) {
                remoteHostConnection.onServiceConnected(iAsyncScriptingService);
            }
            if (!this.mRunning) {
                AnonymousClass00S.A04(this.mMainThreadHandler, new C03260Kr(this), 54250609);
            }
        }
        return remoteHostConnection;
    }

    public AsyncScriptingClient(Context context) {
        this.mContext = context;
        this.mMainThreadHandler = new Handler(context.getMainLooper());
    }
}
