package com.facebook.cameracore.mediapipeline.asyncscripting;

import X.C000700l;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public interface IJsVm extends IInterface {

    public abstract class Stub extends Binder implements IJsVm {

        public final class Proxy implements IJsVm {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(620808280);
                this.A00 = iBinder;
                C000700l.A09(-1856199118, A03);
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(1701007223);
                IBinder iBinder = this.A00;
                C000700l.A09(-1385908481, A03);
                return iBinder;
            }

            public void destroy() {
                int A03 = C000700l.A03(1036175516);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                    this.A00.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1545677806, A03);
                }
            }

            public void enqueueMessages(String str) {
                int A03 = C000700l.A03(-157903298);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                    obtain.writeString(str);
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(430575973, A03);
                }
            }

            public void enqueueScript(String str) {
                int A03 = C000700l.A03(1821347943);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                    obtain.writeString(str);
                    this.A00.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(402730969, A03);
                }
            }

            public void execute() {
                int A03 = C000700l.A03(-1474589944);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                    this.A00.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(840586074, A03);
                }
            }

            public void gc() {
                int A03 = C000700l.A03(1047885744);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                    this.A00.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(621469096, A03);
                }
            }
        }

        public Stub() {
            int A03 = C000700l.A03(-2032457863);
            attachInterface(this, "com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
            C000700l.A09(224776790, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(1833317482, C000700l.A03(1200048791));
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            int A03 = C000700l.A03(1497468752);
            if (i == 1) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                enqueueMessages(parcel.readString());
                parcel2.writeNoException();
                C000700l.A09(1808259660, A03);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                enqueueScript(parcel.readString());
                parcel2.writeNoException();
                C000700l.A09(345714691, A03);
                return true;
            } else if (i == 3) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                execute();
                parcel2.writeNoException();
                C000700l.A09(-849921121, A03);
                return true;
            } else if (i == 4) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                destroy();
                parcel2.writeNoException();
                C000700l.A09(643273019, A03);
                return true;
            } else if (i == 5) {
                parcel.enforceInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                gc();
                parcel2.writeNoException();
                C000700l.A09(-461292295, A03);
                return true;
            } else if (i != 1598968902) {
                boolean onTransact = super.onTransact(i, parcel, parcel2, i2);
                C000700l.A09(1358129505, A03);
                return onTransact;
            } else {
                parcel2.writeString("com.facebook.cameracore.mediapipeline.asyncscripting.IJsVm");
                C000700l.A09(-389537917, A03);
                return true;
            }
        }
    }

    void destroy();

    void enqueueMessages(String str);

    void enqueueScript(String str);

    void execute();

    void gc();
}
