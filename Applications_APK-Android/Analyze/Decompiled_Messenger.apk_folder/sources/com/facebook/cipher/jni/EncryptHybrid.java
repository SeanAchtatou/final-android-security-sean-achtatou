package com.facebook.cipher.jni;

import com.facebook.jni.HybridData;

public class EncryptHybrid {
    private final HybridData mHybridData;

    private static native HybridData initHybrid(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public native byte[] end();

    public native byte[] start();

    public native void write(byte[] bArr, int i, byte[] bArr2, int i2, int i3);

    private EncryptHybrid(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public EncryptHybrid(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        this.mHybridData = initHybrid(bArr, bArr2, bArr3);
    }
}
