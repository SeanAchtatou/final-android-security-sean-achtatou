package com.facebook.cipher.jni;

import com.facebook.crypto.keychain.KeyChain;
import com.facebook.jni.HybridData;

public class CipherHybrid {
    private final HybridData mHybridData;

    private static native HybridData initHybrid(byte b, KeyChain keyChain);

    public native DecryptHybrid createDecrypt(byte[] bArr, int i, int i2);

    public native EncryptHybrid createEncrypt(byte[] bArr, int i, int i2);

    public CipherHybrid(byte b, KeyChain keyChain) {
        this.mHybridData = initHybrid(b, keyChain);
    }

    private CipherHybrid(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
