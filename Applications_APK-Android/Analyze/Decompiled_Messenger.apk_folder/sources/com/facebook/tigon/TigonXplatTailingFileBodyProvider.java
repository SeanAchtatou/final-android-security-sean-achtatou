package com.facebook.tigon;

import X.C31881kc;
import com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory;
import com.facebook.jni.HybridData;

public class TigonXplatTailingFileBodyProvider extends TigonBodyProvider {
    private static native HybridData initHybrid(String str, AndroidAsyncExecutorFactory androidAsyncExecutorFactory, int i, int i2, int i3, int i4);

    public native void close();

    public native void flush();

    public long getContentLength() {
        return Long.MAX_VALUE;
    }

    public String getName() {
        return "TigonTailingFileBody";
    }

    public void beginStream(TigonBodyStream tigonBodyStream) {
        throw new IllegalStateException("should not be used");
    }

    public TigonXplatTailingFileBodyProvider(String str, AndroidAsyncExecutorFactory androidAsyncExecutorFactory, int i, int i2, int i3, int i4) {
        this.mHybridData = initHybrid(str, androidAsyncExecutorFactory, i, i2, i3, i4);
    }

    static {
        C31881kc.A00();
    }
}
