package com.facebook.tigon;

import com.facebook.jni.HybridData;

public class TigonXplatRequestToken implements TigonRequestToken {
    private final HybridData mHybridData;

    public native void cancel();

    public native void changePriority(int i);

    private TigonXplatRequestToken(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
