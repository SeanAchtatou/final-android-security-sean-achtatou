package com.facebook.tigon;

import X.C005505z;
import X.C31881kc;
import com.facebook.jni.HybridData;

public abstract class TigonXplatBodyProvider extends TigonBodyProvider {
    private native HybridData initHybrid();

    public TigonXplatBodyProvider() {
        C005505z.A03("TigonXplatBodyProvider", 2093585195);
        try {
            this.mHybridData = initHybrid();
        } finally {
            C005505z.A00(950562492);
        }
    }

    static {
        C31881kc.A00();
    }
}
