package com.facebook.tigon.tigonobserver;

import X.AnonymousClass01q;
import X.AnonymousClass06c;
import X.AnonymousClass07A;
import X.AnonymousClass07J;
import X.AnonymousClass2YI;
import X.AnonymousClass2YJ;
import X.AnonymousClass2YK;
import X.AnonymousClass2YL;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import com.facebook.jni.HybridData;
import com.facebook.tigon.TigonXplatService;
import java.util.concurrent.Executor;

public class TigonObservable {
    public final AnonymousClass2YJ[] mDebugObservers;
    public final Executor mExecutor;
    private final HybridData mHybridData;
    public final AnonymousClass07J mObjectPool;
    public final AnonymousClass2YI[] mObservers;

    private native HybridData initHybrid(TigonXplatService tigonXplatService, boolean z, boolean z2);

    private void onAdded(TigonObserverData tigonObserverData) {
        runExecutor(0, tigonObserverData);
    }

    private void onDownloadBody(TigonBodyObservation tigonBodyObservation) {
        AnonymousClass2YK r2 = (AnonymousClass2YK) this.mObjectPool.A01();
        r2.A00 = 7;
        r2.A01 = tigonBodyObservation;
        AnonymousClass07A.A04(this.mExecutor, r2, 1156976575);
    }

    private void onEOM(TigonObserverData tigonObserverData) {
        runExecutor(3, tigonObserverData);
    }

    private void onError(TigonObserverData tigonObserverData) {
        runExecutor(4, tigonObserverData);
    }

    private void onResponse(TigonObserverData tigonObserverData) {
        runExecutor(2, tigonObserverData);
    }

    private void onStarted(TigonObserverData tigonObserverData) {
        runExecutor(1, tigonObserverData);
    }

    private void onUploadBody(TigonBodyObservation tigonBodyObservation) {
        AnonymousClass2YK r2 = (AnonymousClass2YK) this.mObjectPool.A01();
        r2.A00 = 6;
        r2.A01 = tigonBodyObservation;
        AnonymousClass07A.A04(this.mExecutor, r2, 1156976575);
    }

    private void onWillRetry(TigonObserverData tigonObserverData) {
        runExecutor(5, tigonObserverData);
    }

    static {
        AnonymousClass01q.A08("tigonliger");
    }

    private void runExecutor(int i, TigonObserverData tigonObserverData) {
        AnonymousClass2YK r2 = (AnonymousClass2YK) this.mObjectPool.A01();
        r2.A00 = i;
        r2.A02 = tigonObserverData;
        AnonymousClass07A.A04(this.mExecutor, r2, 1349476424);
    }

    public TigonObservable(TigonXplatService tigonXplatService, boolean z, boolean z2, Executor executor, AnonymousClass2YI[] r9, AnonymousClass2YJ[] r10) {
        Class<AnonymousClass2YK> cls = AnonymousClass2YK.class;
        AnonymousClass06c r3 = new AnonymousClass06c(null, cls, AwakeTimeSinceBootClock.INSTANCE);
        r3.A03 = new AnonymousClass2YL(this, cls);
        this.mObjectPool = r3.A00();
        if (executor == null) {
            throw new NullPointerException(String.valueOf("Executor is required"));
        } else if (tigonXplatService.hasSecretaryService()) {
            this.mObservers = r9;
            this.mDebugObservers = r10;
            this.mExecutor = executor;
            this.mHybridData = initHybrid(tigonXplatService, z, z2);
        } else {
            throw new IllegalArgumentException(String.valueOf("Tigon stack doesn't support TigonSecretary"));
        }
    }
}
