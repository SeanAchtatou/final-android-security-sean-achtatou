package com.facebook.tigon;

import java.nio.ByteBuffer;

public interface TigonBodyStream {
    void reportBodyLength(int i);

    void reportError(TigonError tigonError);

    int transferBytes(ByteBuffer byteBuffer, int i);

    void writeEOM();
}
