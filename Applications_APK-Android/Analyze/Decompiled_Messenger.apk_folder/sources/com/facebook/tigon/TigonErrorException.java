package com.facebook.tigon;

import X.AnonymousClass08S;
import java.io.IOException;

public class TigonErrorException extends IOException {
    public final TigonError tigonError;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TigonErrorException(com.facebook.tigon.TigonError r4) {
        /*
            r3 = this;
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "TigonError("
            r2.<init>(r0)
            java.lang.String r0 = "error="
            r2.append(r0)
            int r1 = r4.mCategory
            if (r1 == 0) goto L_0x0075
            r0 = 1
            if (r1 == r0) goto L_0x0072
            r0 = 2
            if (r1 == r0) goto L_0x006f
            r0 = 3
            if (r1 == r0) goto L_0x006c
            r0 = 4
            if (r1 == r0) goto L_0x0069
            r0 = 5
            if (r1 == r0) goto L_0x0066
            java.lang.String r0 = "<Unknown>"
        L_0x0021:
            r2.append(r0)
            java.lang.String r1 = ", "
            r2.append(r1)
            java.lang.String r0 = "errorDomain="
            r2.append(r0)
            java.lang.String r0 = r4.mErrorDomain
            r2.append(r0)
            r2.append(r1)
            java.lang.String r0 = "domainErrorCode="
            r2.append(r0)
            int r0 = r4.mDomainErrorCode
            r2.append(r0)
            java.lang.String r1 = r4.mAnalyticsDetail
            if (r1 == 0) goto L_0x0057
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0057
            java.lang.String r0 = ", analyticsDetail=\""
            r2.append(r0)
            r2.append(r1)
            java.lang.String r0 = "\""
            r2.append(r0)
        L_0x0057:
            java.lang.String r0 = ")"
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            r3.tigonError = r4
            return
        L_0x0066:
            java.lang.String r0 = "RequestNotSupported"
            goto L_0x0021
        L_0x0069:
            java.lang.String r0 = "InvalidRequest"
            goto L_0x0021
        L_0x006c:
            java.lang.String r0 = "FatalError"
            goto L_0x0021
        L_0x006f:
            java.lang.String r0 = "TransientError"
            goto L_0x0021
        L_0x0072:
            java.lang.String r0 = "Cancel"
            goto L_0x0021
        L_0x0075:
            java.lang.String r0 = "None"
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.tigon.TigonErrorException.<init>(com.facebook.tigon.TigonError):void");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:0:0x0000 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.lang.Throwable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String convertExceptionToRequestStatus(java.io.IOException r2) {
        /*
            if (r2 != 0) goto L_0x0005
            java.lang.String r0 = "done"
            return r0
        L_0x0005:
            boolean r0 = r2 instanceof com.facebook.tigon.TigonErrorException
            r1 = 0
            if (r0 == 0) goto L_0x001a
            com.facebook.tigon.TigonErrorException r2 = (com.facebook.tigon.TigonErrorException) r2
        L_0x000c:
            if (r2 == 0) goto L_0x0010
            com.facebook.tigon.TigonError r1 = r2.tigonError
        L_0x0010:
            if (r1 == 0) goto L_0x0024
            int r1 = r1.mCategory
            r0 = 1
            if (r1 != r0) goto L_0x0024
            java.lang.String r0 = "cancelled"
            return r0
        L_0x001a:
            java.lang.Throwable r2 = r2.getCause()
            boolean r0 = r2 instanceof java.io.IOException
            if (r0 != 0) goto L_0x0005
            r2 = r1
            goto L_0x000c
        L_0x0024:
            java.lang.String r0 = "error"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.tigon.TigonErrorException.convertExceptionToRequestStatus(java.io.IOException):java.lang.String");
    }

    public static String formatTigonException(IOException iOException) {
        TigonError tigonError2;
        TigonErrorException tigonErrorException;
        if (iOException == null) {
            return null;
        }
        Throwable th = iOException;
        while (true) {
            tigonError2 = null;
            if (!(th instanceof TigonErrorException)) {
                th = th.getCause();
                if (!(th instanceof IOException)) {
                    tigonErrorException = null;
                    break;
                }
            } else {
                tigonErrorException = (TigonErrorException) th;
                break;
            }
        }
        if (tigonErrorException != null) {
            tigonError2 = tigonErrorException.tigonError;
        }
        if (tigonError2 != null) {
            String str = tigonError2.mErrorDomain;
            if (str.startsWith("Tigon") && str.endsWith("Domain")) {
                str = str.substring(5, str.length() - 6);
            }
            return tigonError2.mCategory + ":" + str + ":" + tigonError2.mDomainErrorCode;
        }
        String simpleName = iOException.getClass().getSimpleName();
        Throwable cause = iOException.getCause();
        if (cause != null) {
            return AnonymousClass08S.A0P(simpleName, "|", cause.getClass().getSimpleName());
        }
        return simpleName;
    }
}
