package com.facebook.tigon.nativeservice.authed;

import X.AnonymousClass01q;
import X.AnonymousClass1XY;
import X.C05540Zi;
import X.C10580kT;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.jni.HybridData;
import com.facebook.tigon.iface.TigonServiceHolder;
import com.facebook.tigon.nativeservice.common.NativePlatformContextHolder;
import com.facebook.tigon.tigonliger.TigonLigerService;

@UserScoped
public class NativeAuthedTigonServiceHolder extends TigonServiceHolder {
    private static C05540Zi $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXINSTANCE;

    private static native HybridData initHybrid(TigonServiceHolder tigonServiceHolder, NativePlatformContextHolder nativePlatformContextHolder, String str);

    public static final NativeAuthedTigonServiceHolder $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXFACTORY_METHOD(AnonymousClass1XY r6) {
        NativeAuthedTigonServiceHolder nativeAuthedTigonServiceHolder;
        synchronized (NativeAuthedTigonServiceHolder.class) {
            C05540Zi A00 = C05540Zi.A00($ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXINSTANCE);
            $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXINSTANCE = A00;
            try {
                if (A00.A03(r6)) {
                    AnonymousClass1XY r0 = (AnonymousClass1XY) $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXINSTANCE.A01();
                    $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXINSTANCE.A00 = new NativeAuthedTigonServiceHolder(TigonLigerService.$ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXFACTORY_METHOD(r0), NativePlatformContextHolder.$ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXFACTORY_METHOD(r0), C10580kT.A00(r0));
                }
                C05540Zi r1 = $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXINSTANCE;
                nativeAuthedTigonServiceHolder = (NativeAuthedTigonServiceHolder) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXINSTANCE.A02();
                throw th;
            }
        }
        return nativeAuthedTigonServiceHolder;
    }

    static {
        AnonymousClass01q.A08("tigonnativeservice");
    }

    public NativeAuthedTigonServiceHolder(TigonServiceHolder tigonServiceHolder, NativePlatformContextHolder nativePlatformContextHolder, ViewerContext viewerContext) {
        super(initHybrid(tigonServiceHolder, nativePlatformContextHolder, viewerContext.mAuthToken));
    }
}
