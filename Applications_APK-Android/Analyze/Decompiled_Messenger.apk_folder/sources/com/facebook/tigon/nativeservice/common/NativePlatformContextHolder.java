package com.facebook.tigon.nativeservice.common;

import X.AnonymousClass01q;
import X.AnonymousClass0VB;
import X.AnonymousClass0VG;
import X.AnonymousClass0WD;
import X.AnonymousClass0WT;
import X.AnonymousClass0ZM;
import X.AnonymousClass125;
import X.AnonymousClass13O;
import X.AnonymousClass13Q;
import X.AnonymousClass13b;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y7;
import X.C010708t;
import X.C04310Tq;
import X.C04430Uq;
import X.C04460Ut;
import X.C06600bl;
import X.C09340h3;
import X.C10630ka;
import X.C25051Yd;
import X.C25951af;
import X.C28081eE;
import X.C32061l3;
import android.net.NetworkInfo;
import com.facebook.jni.HybridData;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.proxygen.NetworkStatusMonitor;
import java.util.HashSet;
import javax.inject.Singleton;

@Singleton
public class NativePlatformContextHolder implements AnonymousClass0ZM {
    private static volatile NativePlatformContextHolder $ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXINSTANCE;
    public final AnonymousClass13b mAnalyticsConnectionUtils;
    public AnonymousClass125 mCarrierMonitor;
    public C04460Ut mFbBroadcastManager;
    public AnonymousClass13Q mHttpConfig;
    public final HybridData mHybridData;
    public NetworkInfo mLastNetworkInfo = this.mNetworkManager.A0D();
    private final C10630ka mLigerHttpClientProvider;
    private final C25051Yd mMobileConfig;
    public C09340h3 mNetworkManager;
    public NetworkStatusMonitor mNetworkStatusMonitor;
    public AnonymousClass13O mServerConfig;

    private static native HybridData initHybrid(NetworkStatusMonitor networkStatusMonitor, boolean z, String str, String str2, String str3, int i, int i2, int i3, boolean z2);

    private native void updateAppState(boolean z, String str);

    private native void updateCarrierParameters(String str, String str2, String str3);

    private native void updateConnectionType(String str);

    private native void updateDomain(String str);

    private native void updateLigerPrintTraceEvents(boolean z);

    public void onBackgroundAppJob() {
        String AdC;
        String A02 = this.mServerConfig.A05.A02();
        if (0 == 0 && (AdC = this.mHttpConfig.AdC()) != null) {
            A02 = AdC;
        }
        updateAppState(false, A02);
    }

    public void onForegroundAppJob() {
        String AdC;
        String A02 = this.mServerConfig.A05.A02();
        if (1 == 0 && (AdC = this.mHttpConfig.AdC()) != null) {
            A02 = AdC;
        }
        updateAppState(true, A02);
    }

    public static final NativePlatformContextHolder $ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXFACTORY_METHOD(AnonymousClass1XY r12) {
        if ($ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXINSTANCE == null) {
            synchronized (NativePlatformContextHolder.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXINSTANCE, r12);
                if (A00 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r12.getApplicationInjector();
                        $ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXINSTANCE = new NativePlatformContextHolder(applicationInjector, FbSharedPreferencesModule.A00(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.B8e, applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.B9n, applicationInjector), AnonymousClass13O.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.At6, applicationInjector), C04430Uq.A02(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AeN, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.A7w, applicationInjector));
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXINSTANCE;
    }

    public NativePlatformContextHolder(AnonymousClass1XY r12, FbSharedPreferences fbSharedPreferences, C04310Tq r14, C04310Tq r15, AnonymousClass13O r16, C04310Tq r17, C04460Ut r18, C04310Tq r19, C04310Tq r20) {
        this.mLigerHttpClientProvider = C10630ka.A00(r12);
        this.mMobileConfig = AnonymousClass0WT.A00(r12);
        this.mHttpConfig = (AnonymousClass13Q) r15.get();
        AnonymousClass13O r3 = r16;
        this.mServerConfig = r3;
        this.mCarrierMonitor = (AnonymousClass125) r17.get();
        this.mNetworkManager = (C09340h3) r19.get();
        this.mFbBroadcastManager = r18;
        this.mAnalyticsConnectionUtils = (AnonymousClass13b) r20.get();
        try {
            AnonymousClass01q.A08("liger");
            this.mNetworkStatusMonitor = this.mLigerHttpClientProvider.A00;
        } catch (UnsatisfiedLinkError e) {
            C010708t.A0L("NativePlatformContext", "Failed to load Liger:", e);
        }
        String domain = this.mHttpConfig.getDomain();
        this.mHybridData = initHybrid(this.mNetworkStatusMonitor, !((Boolean) r14.get()).booleanValue(), r3.A05.A02(), domain, AnonymousClass13b.A01(this.mLastNetworkInfo), this.mMobileConfig.AqL(563353684738438L, 10000), (int) this.mMobileConfig.At0(563521184203237L), (int) this.mMobileConfig.At0(563353683100024L), fbSharedPreferences.Aep(C25951af.A0G, false));
        HashSet hashSet = new HashSet();
        hashSet.add(C25951af.A0J);
        hashSet.add(C25951af.A0G);
        fbSharedPreferences.C0h(hashSet, this);
        onCellLocationChanged();
        AnonymousClass125 r0 = this.mCarrierMonitor;
        synchronized (r0.A04) {
            r0.A04.add(this);
        }
        C06600bl BMm = this.mFbBroadcastManager.BMm();
        BMm.A02("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED", new C32061l3(this));
        BMm.A00().A00();
        C28081eE.A00(NativePlatformContextHolder.class);
    }

    public static void handleConnectivityUpdate(NativePlatformContextHolder nativePlatformContextHolder) {
        NetworkInfo A0D = nativePlatformContextHolder.mNetworkManager.A0D();
        NetworkInfo networkInfo = nativePlatformContextHolder.mLastNetworkInfo;
        boolean z = true;
        if (A0D != null ? !(networkInfo != null && A0D.getType() == networkInfo.getType() && A0D.getSubtype() == networkInfo.getSubtype()) : networkInfo != null) {
            z = false;
        }
        if (!z) {
            nativePlatformContextHolder.mLastNetworkInfo = A0D;
            nativePlatformContextHolder.updateConnectionType(AnonymousClass13b.A01(A0D));
        }
    }

    public void onCellLocationChanged() {
        AnonymousClass125 r0 = this.mCarrierMonitor;
        AnonymousClass125.A02(r0);
        String str = r0.A05;
        AnonymousClass125 r02 = this.mCarrierMonitor;
        AnonymousClass125.A02(r02);
        String str2 = r02.A07;
        AnonymousClass125 r03 = this.mCarrierMonitor;
        AnonymousClass125.A02(r03);
        updateCarrierParameters(str, str2, r03.A06);
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r3) {
        if (C25951af.A0J.equals(r3)) {
            updateDomain(this.mHttpConfig.getDomain());
        } else if (C25951af.A0G.equals(r3)) {
            updateLigerPrintTraceEvents(fbSharedPreferences.Aep(r3, false));
        }
    }
}
