package com.facebook.tigon.nativeservice;

import X.AnonymousClass01q;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import com.facebook.jni.HybridData;
import com.facebook.tigon.iface.TigonServiceHolder;
import com.facebook.tigon.nativeservice.common.NativePlatformContextHolder;
import com.facebook.tigon.tigonliger.TigonLigerService;
import javax.inject.Singleton;

@Singleton
public class NativeTigonServiceHolder extends TigonServiceHolder {
    private static volatile NativeTigonServiceHolder $ul_$xXXcom_facebook_tigon_nativeservice_NativeTigonServiceHolder$xXXINSTANCE;

    private static native HybridData initHybrid(TigonServiceHolder tigonServiceHolder, NativePlatformContextHolder nativePlatformContextHolder);

    public static final NativeTigonServiceHolder $ul_$xXXcom_facebook_tigon_nativeservice_NativeTigonServiceHolder$xXXFACTORY_METHOD(AnonymousClass1XY r5) {
        if ($ul_$xXXcom_facebook_tigon_nativeservice_NativeTigonServiceHolder$xXXINSTANCE == null) {
            synchronized (NativeTigonServiceHolder.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_tigon_nativeservice_NativeTigonServiceHolder$xXXINSTANCE, r5);
                if (A00 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        $ul_$xXXcom_facebook_tigon_nativeservice_NativeTigonServiceHolder$xXXINSTANCE = new NativeTigonServiceHolder(TigonLigerService.$ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXFACTORY_METHOD(applicationInjector), NativePlatformContextHolder.$ul_$xXXcom_facebook_tigon_nativeservice_common_NativePlatformContextHolder$xXXFACTORY_METHOD(applicationInjector));
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_tigon_nativeservice_NativeTigonServiceHolder$xXXINSTANCE;
    }

    static {
        AnonymousClass01q.A08("tigonnativeservice");
    }

    public NativeTigonServiceHolder(TigonServiceHolder tigonServiceHolder, NativePlatformContextHolder nativePlatformContextHolder) {
        super(initHybrid(tigonServiceHolder, nativePlatformContextHolder));
    }
}
