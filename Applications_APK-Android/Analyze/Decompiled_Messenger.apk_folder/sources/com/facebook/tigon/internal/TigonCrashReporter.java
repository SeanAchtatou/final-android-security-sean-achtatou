package com.facebook.tigon.internal;

import X.AnonymousClass08S;
import X.AnonymousClass09P;
import X.AnonymousClass1XY;
import X.C04750Wa;
import X.C13650ro;
import com.facebook.tigon.TigonErrorReporter;

public class TigonCrashReporter {
    public final TigonErrorReporter mErrorReporter;

    public static final TigonCrashReporter $ul_$xXXcom_facebook_tigon_internal_TigonCrashReporter$xXXFACTORY_METHOD(AnonymousClass1XY r2) {
        return new TigonCrashReporter(C04750Wa.A01(r2));
    }

    public void crashReport(String str, Throwable th) {
        String str2;
        if (th != null) {
            str2 = th.getClass().getSimpleName();
        } else {
            str2 = str;
        }
        this.mErrorReporter.softReport(AnonymousClass08S.A0J("Tigon: ", str2), str, th);
    }

    public TigonCrashReporter(AnonymousClass09P r2) {
        this.mErrorReporter = new C13650ro(r2);
    }
}
