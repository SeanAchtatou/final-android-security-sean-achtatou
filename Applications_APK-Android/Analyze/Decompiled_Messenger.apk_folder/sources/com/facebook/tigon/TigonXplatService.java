package com.facebook.tigon;

import X.AnonymousClass00C;
import X.AnonymousClass1ZJ;
import X.C005505z;
import X.C31521jp;
import X.C31881kc;
import X.C48062Zj;
import X.C48072Zk;
import X.C48182Zw;
import X.C75223jU;
import com.facebook.jni.HybridData;
import com.facebook.tigon.iface.TigonRequest;
import com.facebook.tigon.iface.TigonServiceHolder;
import java.nio.ByteBuffer;
import java.util.concurrent.Executor;

public class TigonXplatService extends TigonServiceHolder implements C31521jp {
    private final AnonymousClass1ZJ mTigonRequestCounter;

    private native byte[] getNetworkStatusInfoNative();

    private native TigonXplatRequestToken sendRequestBodyBufferIntegerBuffer(TigonRequest tigonRequest, byte[] bArr, int i, TigonBodyProvider tigonBodyProvider, TigonCallbacks tigonCallbacks, Executor executor);

    private native TigonXplatRequestToken sendRequestIntegerBuffer(TigonRequest tigonRequest, byte[] bArr, int i, ByteBuffer[] byteBufferArr, int i2, TigonCallbacks tigonCallbacks, Executor executor);

    public native boolean hasSecretaryService();

    public void onPreRequest() {
    }

    public native void releaseBodyBuffer(ByteBuffer byteBuffer);

    public boolean isAvailable() {
        HybridData hybridData = this.mHybridData;
        if (hybridData == null) {
            return false;
        }
        return hybridData.isValid();
    }

    public TigonXplatService(HybridData hybridData, TigonErrorReporter tigonErrorReporter, AnonymousClass1ZJ r5) {
        super(hybridData);
        this.mTigonRequestCounter = r5;
        C005505z.A03("TigonXplatService", 2140942195);
        try {
            C31881kc.A00();
            C005505z.A00(1241925685);
        } catch (Throwable th) {
            C005505z.A00(984373407);
            throw th;
        }
    }

    public C75223jU getNetworkStatusInfo() {
        int length;
        byte[] networkStatusInfoNative = getNetworkStatusInfoNative();
        if (networkStatusInfoNative == null || (length = networkStatusInfoNative.length) == 0) {
            return new C75223jU(0, 0, 0, 0, -1, -1, -1, -1, -1, -1);
        }
        return C48182Zw.A02(networkStatusInfoNative, length);
    }

    public TigonRequestToken sendRequest(TigonRequest tigonRequest, TigonBodyProvider tigonBodyProvider, TigonCallbacks tigonCallbacks, Executor executor) {
        onPreRequest();
        C48062Zj r1 = new C48062Zj(1024);
        TigonRequest tigonRequest2 = tigonRequest;
        C48072Zk.A00(r1, tigonRequest);
        AnonymousClass1ZJ r0 = this.mTigonRequestCounter;
        if (r0 != null) {
            r0.A07.getAndIncrement();
        }
        return sendRequestBodyBufferIntegerBuffer(tigonRequest2, r1.A01, r1.A00, tigonBodyProvider, tigonCallbacks, executor);
    }

    /* JADX INFO: finally extract failed */
    public TigonRequestToken sendRequest(TigonRequest tigonRequest, ByteBuffer[] byteBufferArr, int i, TigonCallbacks tigonCallbacks, Executor executor) {
        C005505z.A03("TigonXplatService - sendRequest", 2143782891);
        try {
            onPreRequest();
            C48062Zj r4 = new C48062Zj(1024);
            AnonymousClass00C.A01(32, "TigonJavaSerializer - serializeTigonRequest", -1127935006);
            TigonRequest tigonRequest2 = tigonRequest;
            C48072Zk.A00(r4, tigonRequest);
            AnonymousClass00C.A00(32, 597820622);
            AnonymousClass1ZJ r0 = this.mTigonRequestCounter;
            if (r0 != null) {
                r0.A07.getAndIncrement();
            }
            TigonXplatRequestToken sendRequestIntegerBuffer = sendRequestIntegerBuffer(tigonRequest2, r4.A01, r4.A00, byteBufferArr, i, tigonCallbacks, executor);
            C005505z.A00(1564158251);
            return sendRequestIntegerBuffer;
        } catch (Throwable th) {
            C005505z.A00(1322138648);
            throw th;
        }
    }
}
