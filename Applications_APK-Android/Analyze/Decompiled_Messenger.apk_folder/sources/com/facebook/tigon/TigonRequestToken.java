package com.facebook.tigon;

public interface TigonRequestToken {
    void cancel();

    void changePriority(int i);
}
