package com.facebook.tigon;

import X.C31881kc;
import X.C48062Zj;
import X.C48082Zl;
import com.facebook.jni.HybridData;
import java.nio.ByteBuffer;

public class TigonXplatBodyStream implements TigonBodyStream {
    private final HybridData mHybridData;

    private native void reportBodyLengthNative(int i);

    private native void reportErrorNativeByteBuffer(byte[] bArr, int i);

    private native int transferBytesNative(ByteBuffer byteBuffer, int i);

    private native void writeEOMNative();

    public void reportError(TigonError tigonError) {
        C48062Zj r2 = new C48062Zj();
        C48082Zl.A01(r2, tigonError.mCategory);
        C48082Zl.A03(r2, tigonError.mErrorDomain);
        C48082Zl.A01(r2, tigonError.mDomainErrorCode);
        C48082Zl.A03(r2, tigonError.mAnalyticsDetail);
        reportErrorNativeByteBuffer(r2.A01, r2.A00);
    }

    public TigonXplatBodyStream(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public int transferBytes(ByteBuffer byteBuffer, int i) {
        return transferBytesNative(byteBuffer, i);
    }

    public void reportBodyLength(int i) {
        reportBodyLengthNative(i);
    }

    static {
        C31881kc.A00();
    }

    public void writeEOM() {
        writeEOMNative();
    }
}
