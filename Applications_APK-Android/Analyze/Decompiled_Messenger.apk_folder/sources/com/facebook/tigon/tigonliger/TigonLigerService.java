package com.facebook.tigon.tigonliger;

import X.AnonymousClass0UQ;
import X.AnonymousClass0VB;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1ZI;
import X.C04750Wa;
import X.C10630ka;
import X.C12230or;
import X.C13640rn;
import X.C26251b9;
import X.C31521jp;
import X.C31531jq;
import X.C64113Ab;
import com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory;
import com.facebook.jni.HybridData;
import com.facebook.proxygen.EventBase;
import com.facebook.proxygen.HTTPClient;
import com.facebook.proxygen.NetworkStatusMonitor;
import com.facebook.tigon.TigonXplatService;
import com.facebook.tigon.internal.TigonCrashReporter;
import com.facebook.xanalytics.XAnalyticsHolder;
import javax.inject.Singleton;

@Singleton
public class TigonLigerService extends TigonXplatService implements C31521jp {
    private static volatile TigonLigerService $ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXINSTANCE;
    public static final Class TAG = TigonLigerService.class;
    public AndroidAsyncExecutorFactory mDefaultExecutorFactory;
    private C10630ka mLigerClientProvider;

    private static native HybridData initHybrid(EventBase eventBase, HTTPClient hTTPClient, NetworkStatusMonitor networkStatusMonitor, TigonLigerConfig tigonLigerConfig, TigonXplatInterceptorsHolder tigonXplatInterceptorsHolder, AndroidAsyncExecutorFactory androidAsyncExecutorFactory, TigonCrashReporter tigonCrashReporter, XAnalyticsHolder xAnalyticsHolder, boolean z);

    private native void setRatelimitNative(long j, long j2);

    public native void cancelAllRequests();

    public native void onAppStateChange(boolean z);

    public static final TigonLigerService $ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXFACTORY_METHOD(AnonymousClass1XY r12) {
        if ($ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXINSTANCE == null) {
            synchronized (TigonLigerService.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXINSTANCE, r12);
                if (A00 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r12.getApplicationInjector();
                        $ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXINSTANCE = new TigonLigerService(new C31531jq(applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AOm, applicationInjector), new TigonLigerConfig(C26251b9.A05(applicationInjector)), AnonymousClass0VB.A00(AnonymousClass1Y3.B7d, applicationInjector), C13640rn.A01(applicationInjector), new TigonCrashReporter(C04750Wa.A01(applicationInjector)), C12230or.A00(applicationInjector), AnonymousClass1ZI.A00(applicationInjector));
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerService$xXXINSTANCE;
    }

    public void onPreRequest() {
        this.mLigerClientProvider.A02();
    }

    public void setRatelimit(C64113Ab r5) {
        setRatelimitNative(r5.A00, r5.A01);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TigonLigerService(X.C31531jq r11, X.AnonymousClass0US r12, com.facebook.tigon.tigonliger.TigonLigerConfig r13, X.AnonymousClass0US r14, com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory r15, com.facebook.tigon.internal.TigonCrashReporter r16, X.C12230or r17, X.AnonymousClass1ZJ r18) {
        /*
            r10 = this;
            com.facebook.xanalytics.XAnalyticsHolder r8 = r17.BA9()
            r7 = r16
            java.lang.String r1 = "TigonLigerModule.loadLibrary"
            r0 = -1213926442(0xffffffffb7a4f3d6, float:-1.9663868E-5)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x009c }
            java.lang.String r0 = "tigonliger"
            X.AnonymousClass01q.A08(r0)     // Catch:{ all -> 0x0094 }
            r0 = -1257619331(0xffffffffb50a407d, float:-5.150285E-7)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x009c }
            boolean r0 = r11.A01()
            r6 = r15
            if (r0 != 0) goto L_0x003e
            java.lang.Class r1 = com.facebook.tigon.tigonliger.TigonLigerService.TAG
            java.lang.String r0 = "Can't load liger"
        L_0x0024:
            X.C010708t.A06(r1, r0)
            com.facebook.jni.HybridData r1 = new com.facebook.jni.HybridData
            r1.<init>()
        L_0x002c:
            com.facebook.tigon.TigonErrorReporter r0 = r7.mErrorReporter
            r2 = r18
            r10.<init>(r1, r0, r2)
            java.lang.String r1 = "TigonLigerService"
            r0 = -99624478(0xfffffffffa0fd9e2, float:-1.867294E35)
            X.C005505z.A03(r1, r0)
            r10.mDefaultExecutorFactory = r15
            goto L_0x0071
        L_0x003e:
            java.lang.Object r0 = r12.get()
            X.0ka r0 = (X.C10630ka) r0
            com.facebook.proxygen.HTTPClient r2 = r0.A04
            java.lang.Object r0 = r12.get()
            X.0ka r0 = (X.C10630ka) r0
            X.1kG r3 = r0.A00
            X.02L r0 = X.AnonymousClass02L.A05
            java.lang.Boolean r0 = r0.A01
            if (r0 != 0) goto L_0x0059
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
        L_0x0059:
            com.facebook.proxygen.EventBase r1 = r2.mEventBase
            java.lang.Object r5 = r14.get()
            com.facebook.tigon.tigonliger.TigonXplatInterceptorsHolder r5 = (com.facebook.tigon.tigonliger.TigonXplatInterceptorsHolder) r5
            boolean r9 = r0.booleanValue()
            r4 = r13
            com.facebook.jni.HybridData r1 = initHybrid(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            if (r1 != 0) goto L_0x002c
            java.lang.Class r1 = com.facebook.tigon.tigonliger.TigonLigerService.TAG
            java.lang.String r0 = "Can't load liger pointers"
            goto L_0x0024
        L_0x0071:
            java.lang.Object r0 = r12.get()     // Catch:{ Exception -> 0x007d }
            X.0ka r0 = (X.C10630ka) r0     // Catch:{ Exception -> 0x007d }
            r10.mLigerClientProvider = r0     // Catch:{ Exception -> 0x007d }
            r0 = -666122408(0xffffffffd84bc758, float:-8.9622814E14)
            goto L_0x0088
        L_0x007d:
            r2 = move-exception
            java.lang.Class r1 = com.facebook.tigon.tigonliger.TigonLigerService.TAG     // Catch:{ all -> 0x008c }
            java.lang.String r0 = "Can't initialize tigon"
            X.C010708t.A09(r1, r0, r2)     // Catch:{ all -> 0x008c }
            r0 = 2120760582(0x7e683d06, float:7.7174437E37)
        L_0x0088:
            X.C005505z.A00(r0)
            return
        L_0x008c:
            r1 = move-exception
            r0 = -893653436(0xffffffffcabbee44, float:-6158114.0)
            X.C005505z.A00(r0)
            throw r1
        L_0x0094:
            r1 = move-exception
            r0 = 996873775(0x3b6b162f, float:0.0035871377)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x009c }
            throw r1     // Catch:{ all -> 0x009c }
        L_0x009c:
            r1 = move-exception
            java.lang.String r0 = "failed to load native tigonliger lib"
            r7.crashReport(r0, r1)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.tigon.tigonliger.TigonLigerService.<init>(X.1jq, X.0US, com.facebook.tigon.tigonliger.TigonLigerConfig, X.0US, com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory, com.facebook.tigon.internal.TigonCrashReporter, X.0or, X.1ZJ):void");
    }
}
