package com.facebook.tigon.tigonliger;

import X.AnonymousClass1LZ;
import X.AnonymousClass1XY;
import X.C13630rm;
import X.C13720rv;
import X.C26251b9;

public class TigonLigerConfig {
    public final boolean bbrHeaderEnabled = false;
    public final double bdpCoef = 0.0d;
    public final long bdpLowerBound = 0;
    public final String[] cancelableRequests = null;
    public final boolean e2eEnabled = false;
    public final long exclusivityTimeoutMs = 0;
    public final String[] forwardableHeaders = C13630rm.A00;
    public final long initialBandwidthBps = -1;
    public final long initialTTFBMs = -1;
    public final int largeRequestStrategy = 0;
    public final boolean makeUrgentRequestsExclusiveInflight = false;
    public final long maxStreamingCachedBufferSize = 32768;
    public final int notsentLowatValue = 0;
    public final boolean proxygenShutdownErrorIsTransient = false;
    public final boolean qplEnabled = false;
    public final boolean qplInlineExecutor = false;
    public final int[] redirectErrorCodes = C13720rv.A00;
    public final boolean removeAuthTokenIfNotWhitelisted = false;
    public final int[] requestTypeAndLimit;
    public final boolean retryOnTimeout = false;
    public final boolean retryTransientErrors = false;
    public final long urgentRequestDeadlineThresholdMs = 0;
    public final boolean useBackgroundRetry = true;
    public final boolean useExponentialRetry = false;
    public final String[] whitelistedDomains = new String[0];

    public static final TigonLigerConfig $ul_$xXXcom_facebook_tigon_tigonliger_TigonLigerConfig$xXXFACTORY_METHOD(AnonymousClass1XY r2) {
        return new TigonLigerConfig(C26251b9.A05(r2));
    }

    public TigonLigerConfig(AnonymousClass1LZ r4) {
        int[] iArr = new int[3];
        this.requestTypeAndLimit = iArr;
        iArr[0] = 5;
        iArr[1] = 5;
        iArr[2] = 3;
    }
}
