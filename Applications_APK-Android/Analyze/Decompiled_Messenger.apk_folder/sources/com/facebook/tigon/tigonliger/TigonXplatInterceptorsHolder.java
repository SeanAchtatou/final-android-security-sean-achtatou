package com.facebook.tigon.tigonliger;

import X.AnonymousClass0WD;
import X.AnonymousClass0X5;
import X.AnonymousClass0X6;
import X.AnonymousClass1XY;
import X.C005505z;
import X.C183612n;
import com.facebook.jni.HybridData;
import com.facebook.tigon.RequestInterceptor;
import com.facebook.tigon.ResponseInterceptor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
public class TigonXplatInterceptorsHolder {
    private static volatile TigonXplatInterceptorsHolder $ul_$xXXcom_facebook_tigon_tigonliger_TigonXplatInterceptorsHolder$xXXINSTANCE;
    private final HybridData mHybridData;
    private final List mRequestInterceptors;
    private final List mResponseInterceptors;

    private static native HybridData initHybrid();

    private native void registerRequestInterceptor(RequestInterceptor requestInterceptor);

    private native void registerResponseInterceptor(ResponseInterceptor responseInterceptor);

    public static final TigonXplatInterceptorsHolder $ul_$xXXcom_facebook_tigon_tigonliger_TigonXplatInterceptorsHolder$xXXFACTORY_METHOD(AnonymousClass1XY r7) {
        if ($ul_$xXXcom_facebook_tigon_tigonliger_TigonXplatInterceptorsHolder$xXXINSTANCE == null) {
            synchronized (TigonXplatInterceptorsHolder.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_tigon_tigonliger_TigonXplatInterceptorsHolder$xXXINSTANCE, r7);
                if (A00 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        $ul_$xXXcom_facebook_tigon_tigonliger_TigonXplatInterceptorsHolder$xXXINSTANCE = new TigonXplatInterceptorsHolder(new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A2l), new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A2m));
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_tigon_tigonliger_TigonXplatInterceptorsHolder$xXXINSTANCE;
    }

    public TigonXplatInterceptorsHolder(Set set, Set set2) {
        C005505z.A03("TigonXplatInterceptorsHolder", 804014213);
        try {
            this.mHybridData = initHybrid();
            this.mRequestInterceptors = new ArrayList(set);
            this.mResponseInterceptors = new ArrayList(set2);
            C183612n r1 = new C183612n();
            Collections.sort(this.mRequestInterceptors, r1);
            Collections.sort(this.mResponseInterceptors, r1);
            for (RequestInterceptor registerRequestInterceptor : this.mRequestInterceptors) {
                registerRequestInterceptor(registerRequestInterceptor);
            }
            for (ResponseInterceptor registerResponseInterceptor : this.mResponseInterceptors) {
                registerResponseInterceptor(registerResponseInterceptor);
            }
        } finally {
            C005505z.A00(1592610642);
        }
    }
}
