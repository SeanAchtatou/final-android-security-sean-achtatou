package com.facebook.location.foreground;

import X.AnonymousClass00S;
import X.AnonymousClass069;
import X.AnonymousClass07B;
import X.AnonymousClass0UN;
import X.AnonymousClass0VK;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1G0;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass38N;
import X.AnonymousClass4VN;
import X.C04310Tq;
import X.C05460Za;
import X.C06540bf;
import X.C06790c5;
import X.C11890o7;
import X.C22361La;
import X.C33561nm;
import X.C50882ew;
import X.C50892ex;
import X.C50932f1;
import X.C54412mN;
import android.os.Build;
import android.os.Handler;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
public final class ForegroundLocationFrameworkController implements C05460Za {
    private static volatile ForegroundLocationFrameworkController A0D;
    public long A00;
    public long A01;
    public long A02;
    public C06790c5 A03;
    public C06790c5 A04;
    public AnonymousClass0UN A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    private ListenableFuture A0A;
    public final C04310Tq A0B;
    private final Runnable A0C = new C11890o7(this);

    public static synchronized long A00(ForegroundLocationFrameworkController foregroundLocationFrameworkController) {
        synchronized (foregroundLocationFrameworkController) {
            if (A07(foregroundLocationFrameworkController)) {
                long At1 = ((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A04.At1(563443875316148L, 0);
                return At1;
            }
            long At12 = ((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A04.At1(563443875381685L, 90000);
            return At12;
        }
    }

    private synchronized void A02() {
        C06790c5 r0 = this.A04;
        if (r0 != null) {
            if (r0.A02()) {
                this.A04.A01();
            }
            this.A04 = null;
        }
        A03(this);
        ((C54412mN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BD4, this.A05)).A03();
        C50932f1 r2 = (C50932f1) AnonymousClass1XX.A02(10, AnonymousClass1Y3.ACX, this.A05);
        AnonymousClass1G0 r1 = r2.A00;
        if (r1 != null) {
            r1.A01(false);
            r2.A00 = null;
        }
        C50892ex r12 = (C50892ex) AnonymousClass1XX.A02(19, AnonymousClass1Y3.AHw, this.A05);
        synchronized (r12) {
            r12.A0C.clear();
        }
    }

    public static synchronized void A03(ForegroundLocationFrameworkController foregroundLocationFrameworkController) {
        synchronized (foregroundLocationFrameworkController) {
            ListenableFuture listenableFuture = foregroundLocationFrameworkController.A0A;
            if (listenableFuture != null) {
                listenableFuture.cancel(false);
                foregroundLocationFrameworkController.A0A = null;
            }
        }
    }

    public static synchronized void A04(ForegroundLocationFrameworkController foregroundLocationFrameworkController) {
        synchronized (foregroundLocationFrameworkController) {
            if (foregroundLocationFrameworkController.A09) {
                foregroundLocationFrameworkController.A09 = false;
                foregroundLocationFrameworkController.A02();
                C50882ew r2 = (C50882ew) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BM3, foregroundLocationFrameworkController.A05);
                try {
                    C50882ew.A05(r2, true);
                    C50882ew.A03(r2, false);
                    C50882ew.A04(r2, false);
                    C50882ew.A06(r2, false);
                    C22361La A002 = C50882ew.A00(r2, "fgl_app_background");
                    if (A002 != null) {
                        A002.A03("session_duration_ms", r2.A0C.now() - r2.A09);
                        A002.A02("session_request_count", r2.A00);
                        A002.A02("session_scan_count", r2.A01);
                        A002.A02("session_scan_fail_count", r2.A02);
                        A002.A02("session_scan_success_count", r2.A03);
                        A002.A02("session_write_count", r2.A04);
                        A002.A02("session_write_fail_count", r2.A05);
                        A002.A02("session_write_success_count", r2.A06);
                        A002.A0A();
                    }
                    r2.A09 = Long.MIN_VALUE;
                    r2.A00 = Integer.MIN_VALUE;
                    r2.A01 = Integer.MIN_VALUE;
                    r2.A02 = Integer.MIN_VALUE;
                    r2.A03 = Integer.MIN_VALUE;
                    r2.A04 = Integer.MIN_VALUE;
                    r2.A05 = Integer.MIN_VALUE;
                    r2.A06 = Integer.MIN_VALUE;
                } catch (IllegalStateException unused) {
                }
            }
        }
    }

    public static synchronized void A05(ForegroundLocationFrameworkController foregroundLocationFrameworkController) {
        synchronized (foregroundLocationFrameworkController) {
            A03(foregroundLocationFrameworkController);
            if (A08(foregroundLocationFrameworkController)) {
                A06(foregroundLocationFrameworkController, 0);
            }
        }
    }

    public static synchronized void A06(ForegroundLocationFrameworkController foregroundLocationFrameworkController, long j) {
        synchronized (foregroundLocationFrameworkController) {
            foregroundLocationFrameworkController.A0A = ((AnonymousClass0VK) AnonymousClass1XX.A02(14, AnonymousClass1Y3.Ax6, foregroundLocationFrameworkController.A05)).C4Y(foregroundLocationFrameworkController.A0C, j, TimeUnit.MILLISECONDS);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (A09(r3) == false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A08(com.facebook.location.foreground.ForegroundLocationFrameworkController r3) {
        /*
            monitor-enter(r3)
            r2 = 2
            int r1 = X.AnonymousClass1Y3.B5j     // Catch:{ all -> 0x001c }
            X.0UN r0 = r3.A05     // Catch:{ all -> 0x001c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x001c }
            X.0Ud r0 = (X.AnonymousClass0Ud) r0     // Catch:{ all -> 0x001c }
            boolean r0 = r0.A0H()     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x0019
            boolean r1 = A09(r3)     // Catch:{ all -> 0x001c }
            r0 = 1
            if (r1 != 0) goto L_0x001a
        L_0x0019:
            r0 = 0
        L_0x001a:
            monitor-exit(r3)
            return r0
        L_0x001c:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.location.foreground.ForegroundLocationFrameworkController.A08(com.facebook.location.foreground.ForegroundLocationFrameworkController):boolean");
    }

    public static synchronized boolean A09(ForegroundLocationFrameworkController foregroundLocationFrameworkController) {
        boolean z;
        synchronized (foregroundLocationFrameworkController) {
            z = false;
            if (((C33561nm) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AAJ, foregroundLocationFrameworkController.A05)).A05() == AnonymousClass07B.A0N) {
                z = true;
            }
        }
        return z;
    }

    public static final ForegroundLocationFrameworkController A01(AnonymousClass1XY r4) {
        if (A0D == null) {
            synchronized (ForegroundLocationFrameworkController.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0D, r4);
                if (A002 != null) {
                    try {
                        A0D = new ForegroundLocationFrameworkController(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0D;
    }

    public static boolean A07(ForegroundLocationFrameworkController foregroundLocationFrameworkController) {
        if (((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A04.At1(563443875316148L, 0) <= 0 || ((((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A04.Aeo(281968898278288L, false) && !((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A03()) || (((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A04.Aeo(281968898212751L, false) && ((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A01.A00.Aeq(C06540bf.A06).asBoolean(false)))) {
            return false;
        }
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Asv, foregroundLocationFrameworkController.A05)).now() - foregroundLocationFrameworkController.A00;
        long At1 = ((AnonymousClass4VN) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGZ, foregroundLocationFrameworkController.A05)).A04.At1(563443875250611L, 0);
        if (At1 <= 0 || now >= At1) {
            return false;
        }
        return true;
    }

    public void A0A() {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 29) {
            z = true;
        }
        if (z) {
            AnonymousClass00S.A05((Handler) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Agg, this.A05), new AnonymousClass38N(this), 500, 1228763219);
        }
    }

    private ForegroundLocationFrameworkController(AnonymousClass1XY r3) {
        this.A05 = new AnonymousClass0UN(20, r3);
        this.A0B = AnonymousClass0XJ.A0K(r3);
    }

    public void clearUserData() {
        A02();
    }
}
