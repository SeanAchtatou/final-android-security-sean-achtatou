package com.facebook.graphservice.nativeutil;

import X.AnonymousClass01q;
import X.AnonymousClass107;
import com.facebook.jni.HybridData;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

public class NativeMap extends AbstractMap<String, Object> {
    private final HybridData mHybridData = initHybridData();

    private static native HybridData initHybridData();

    private native void putBoolean(String str, boolean z);

    private native void putDouble(String str, double d);

    private native void putInt(String str, int i);

    private native void putNativeList(String str, NativeList nativeList);

    private native void putNativeMap(String str, NativeMap nativeMap);

    private native void putNull(String str);

    private native void putString(String str, String str2);

    public native Map consumeMap();

    static {
        AnonymousClass01q.A08("graphservice-jni-nativeutil");
    }

    public Set entrySet() {
        throw new UnsupportedOperationException("not implemented");
    }

    public /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
        String str = (String) obj;
        Object A00 = AnonymousClass107.A00(obj2);
        if (A00 == null) {
            putNull(str);
            return obj2;
        } else if (A00 instanceof Boolean) {
            putBoolean(str, ((Boolean) A00).booleanValue());
            return obj2;
        } else if (A00 instanceof Integer) {
            putInt(str, ((Integer) A00).intValue());
            return obj2;
        } else if (A00 instanceof Number) {
            putDouble(str, ((Number) A00).doubleValue());
            return obj2;
        } else if (A00 instanceof String) {
            putString(str, (String) A00);
            return obj2;
        } else if (A00 instanceof NativeMap) {
            putNativeMap(str, (NativeMap) A00);
            return obj2;
        } else if (A00 instanceof NativeList) {
            putNativeList(str, (NativeList) A00);
            return obj2;
        } else {
            throw new IllegalArgumentException("Could not convert " + A00.getClass());
        }
    }

    public NativeMap() {
    }

    public NativeMap(Map map) {
        putAll(map);
    }
}
