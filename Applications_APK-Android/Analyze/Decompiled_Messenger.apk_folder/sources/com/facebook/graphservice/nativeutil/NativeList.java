package com.facebook.graphservice.nativeutil;

import X.AnonymousClass01q;
import X.AnonymousClass107;
import com.facebook.jni.HybridData;
import java.util.List;

public class NativeList {
    private final HybridData mHybridData;

    private native void addBoolean(boolean z);

    private native void addDouble(double d);

    private native void addInt(int i);

    private native void addNativeList(NativeList nativeList);

    private native void addNativeMap(NativeMap nativeMap);

    private native void addNull();

    private native void addString(String str);

    private static native HybridData initHybridData();

    public native List consumeList();

    static {
        AnonymousClass01q.A08("graphservice-jni-nativeutil");
    }

    private NativeList(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public NativeList(List list) {
        this.mHybridData = initHybridData();
        if (list != null) {
            for (Object A00 : list) {
                Object A002 = AnonymousClass107.A00(A00);
                if (A002 == null) {
                    addNull();
                } else if (A002 instanceof Boolean) {
                    addBoolean(((Boolean) A002).booleanValue());
                } else if (A002 instanceof Integer) {
                    addInt(((Integer) A002).intValue());
                } else if (A002 instanceof Number) {
                    addDouble(((Number) A002).doubleValue());
                } else if (A002 instanceof String) {
                    addString((String) A002);
                } else if (A002 instanceof NativeMap) {
                    addNativeMap((NativeMap) A002);
                } else if (A002 instanceof NativeList) {
                    addNativeList((NativeList) A002);
                } else {
                    throw new IllegalArgumentException("Could not convert " + A002.getClass());
                }
            }
        }
    }
}
