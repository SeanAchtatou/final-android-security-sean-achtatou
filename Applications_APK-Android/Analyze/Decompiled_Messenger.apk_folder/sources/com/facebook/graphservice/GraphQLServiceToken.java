package com.facebook.graphservice;

import X.AnonymousClass01q;
import com.facebook.graphservice.interfaces.GraphQLService;
import com.facebook.jni.HybridData;

public class GraphQLServiceToken implements GraphQLService.Token {
    private final HybridData mHybridData;

    public native void cancel();

    static {
        AnonymousClass01q.A08("graphservice-jni");
    }

    private GraphQLServiceToken(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
