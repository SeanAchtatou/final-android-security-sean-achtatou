package com.facebook.graphservice.factory;

import X.AnonymousClass01q;
import X.AnonymousClass11R;
import com.facebook.graphservice.asset.GraphServiceAsset;
import com.facebook.graphservice.serialization.TreeJsonSerializerJNI;
import com.facebook.graphservice.serialization.TreeSerializerJNI;
import com.facebook.graphservice.tree.TreeBuilderJNI;
import com.facebook.graphservice.tree.TreeJNI;
import com.facebook.jni.HybridData;

public class GraphQLServiceFactory {
    private final HybridData mHybridData;

    private static native HybridData initHybridData(GraphServiceAsset graphServiceAsset);

    private native long legacyPersistIdForQueryNameHash(long j);

    private native TreeBuilderJNI newTreeBuilderFromTree(TreeJNI treeJNI, Class cls, int i);

    private native TreeBuilderJNI newTreeBuilderFromType(String str, Class cls, int i);

    private native TreeBuilderJNI newUpdateBuilderFromTree(TreeJNI treeJNI, Class cls, int i);

    private native long ossPersistIdForQueryNameHash(long j);

    private native String queryTextForQueryNameHash(long j);

    public native TreeJsonSerializerJNI newTreeJsonSerializer();

    public native TreeSerializerJNI newTreeSerializer();

    static {
        AnonymousClass01q.A08("graphservice-jni-factory");
    }

    public long getLegacyPersistIdForQueryNameHash(long j) {
        return legacyPersistIdForQueryNameHash(j);
    }

    public long getOssPersistIdForQueryNameHash(long j) {
        return ossPersistIdForQueryNameHash(j);
    }

    public String getQueryTextForQueryNameHash(long j) {
        return queryTextForQueryNameHash(j);
    }

    public GraphQLServiceFactory() {
        this.mHybridData = null;
    }

    public GraphQLServiceFactory(GraphServiceAsset graphServiceAsset) {
        this.mHybridData = initHybridData(graphServiceAsset);
    }

    public /* bridge */ /* synthetic */ AnonymousClass11R newTreeBuilder(String str) {
        return newTreeBuilderFromType(str, TreeBuilderJNI.class, 0);
    }

    public AnonymousClass11R newTreeBuilder(String str, Class cls, int i) {
        return newTreeBuilderFromType(str, cls, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0007, code lost:
        if (r6.isValid() == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass11R newTreeBuilder(java.lang.String r3, java.lang.Class r4, int r5, com.facebook.graphservice.interfaces.Tree r6) {
        /*
            r2 = this;
            if (r6 == 0) goto L_0x0009
            boolean r1 = r6.isValid()
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            com.google.common.base.Preconditions.checkArgument(r0)
            com.facebook.graphservice.tree.TreeJNI r6 = (com.facebook.graphservice.tree.TreeJNI) r6
            com.facebook.graphservice.tree.TreeBuilderJNI r0 = r2.newTreeBuilderFromTree(r6, r4, r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.graphservice.factory.GraphQLServiceFactory.newTreeBuilder(java.lang.String, java.lang.Class, int, com.facebook.graphservice.interfaces.Tree):X.11R");
    }
}
