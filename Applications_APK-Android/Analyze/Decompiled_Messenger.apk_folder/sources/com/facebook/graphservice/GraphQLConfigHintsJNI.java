package com.facebook.graphservice;

import X.AnonymousClass01q;
import X.C33431nZ;
import com.facebook.jni.HybridData;
import java.util.Map;

public class GraphQLConfigHintsJNI {
    private HybridData mHybridData;

    private static native HybridData initHybridData(int i, int i2, boolean z, String[] strArr, Map map, int i3, boolean z2, int i4, String str, String str2, boolean z3, String[] strArr2, int i5, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, String str3, boolean z10, Map map2, String str4, String str5);

    static {
        AnonymousClass01q.A08("graphservice-jni");
    }

    public GraphQLConfigHintsJNI(C33431nZ r40) {
        C33431nZ r7 = r40;
        int i = r7.cacheTtlSeconds;
        int i2 = r7.freshCacheTtlSeconds;
        boolean z = r7.doNotResumeLiveQuery;
        String[] strArr = r7.excludedCacheKeyParameters;
        Map map = r7.additionalHttpHeaders;
        int i3 = r7.networkTimeoutSeconds;
        boolean z2 = r7.terminateAfterFreshResponse;
        int i4 = r7.hackQueryType;
        String str = r7.hackQueryContext;
        String str2 = r7.locale;
        boolean z3 = r7.parseOnClientExecutor;
        String[] strArr2 = r7.analyticTags;
        int i5 = r7.requestPurpose;
        boolean z4 = r7.ensureCacheWrite;
        boolean z5 = r7.onlyCacheInitialNetworkResponse;
        boolean z6 = r7.enableExperimentalGraphStoreCache;
        boolean z7 = r7.enableOfflineCaching;
        boolean z8 = r7.markHttpRequestReplaySafe;
        boolean z9 = r7.primed;
        String str3 = r7.primedClientQueryId;
        boolean z10 = r7.sendCacheAgeForAdaptiveFetch;
        Map map2 = r7.adaptiveFetchClientParams;
        String str4 = r7.clientTraceId;
        String str5 = r7.clientQueryId;
        String str6 = str2;
        boolean z11 = z3;
        String[] strArr3 = strArr2;
        int i6 = i5;
        boolean z12 = z4;
        boolean z13 = z5;
        boolean z14 = z6;
        this.mHybridData = initHybridData(i, i2, z, strArr, map, i3, z2, i4, str, str6, z11, strArr3, i6, z12, z13, z14, z7, z8, z9, str3, z10, map2, str4, str5);
    }
}
