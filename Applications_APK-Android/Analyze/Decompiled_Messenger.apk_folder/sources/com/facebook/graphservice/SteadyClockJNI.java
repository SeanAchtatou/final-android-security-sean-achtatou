package com.facebook.graphservice;

import X.AnonymousClass01q;
import X.AnonymousClass069;

public class SteadyClockJNI implements AnonymousClass069 {
    private static native long nowJNI();

    static {
        AnonymousClass01q.A08("graphservice-jni");
    }

    public long now() {
        return nowJNI();
    }
}
