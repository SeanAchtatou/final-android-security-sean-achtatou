package com.facebook.graphservice.modelutil;

import X.A3W;
import X.C100354qi;
import X.C101324sM;
import X.C182911o;
import X.C58012t1;
import X.C73513gF;
import X.C77163mm;

public class GSMBuilderShape0S0000000 extends C182911o {
    public GSMBuilderShape0S0000000(int i) {
        super(i);
    }

    public C101324sM A02() {
        return (C101324sM) getResult(C101324sM.class, 537206042);
    }

    public GSTModelShape1S0000000 A03() {
        return (GSTModelShape1S0000000) getResult(GSTModelShape1S0000000.class, 1291861580);
    }

    public GSTModelShape1S0000000 A04() {
        return (GSTModelShape1S0000000) getResult(GSTModelShape1S0000000.class, 684260477);
    }

    public GSTModelShape1S0000000 A05() {
        return (GSTModelShape1S0000000) getResult(GSTModelShape1S0000000.class, -1863968103);
    }

    public GSTModelShape1S0000000 A06() {
        return (GSTModelShape1S0000000) getResult(GSTModelShape1S0000000.class, -156769861);
    }

    public C73513gF A07() {
        return (C73513gF) getResult(C73513gF.class, 431007235);
    }

    public C77163mm A08() {
        return (C77163mm) getResult(C77163mm.class, 2050259240);
    }

    public C100354qi A09() {
        return (C100354qi) getResult(C100354qi.class, 1548097390);
    }

    public C58012t1 A0A() {
        return (C58012t1) getResult(C58012t1.class, 1042585914);
    }

    public A3W A0B() {
        return (A3W) getResult(A3W.class, 57213880);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0I(String str) {
        setString("id", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Double]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0C(double d) {
        setDouble("height", Double.valueOf(d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Double]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0D(double d) {
        setDouble("rotation", Double.valueOf(d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Double]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0E(double d) {
        setDouble("width", Double.valueOf(d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Double]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0F(double d) {
        setDouble("x", Double.valueOf(d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Double]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(int, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setDouble(int, java.lang.Double):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setDouble(java.lang.String, java.lang.Double):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0G(double d) {
        setDouble("y", Double.valueOf(d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0H(int i) {
        setInt("offset", Integer.valueOf(i));
    }
}
