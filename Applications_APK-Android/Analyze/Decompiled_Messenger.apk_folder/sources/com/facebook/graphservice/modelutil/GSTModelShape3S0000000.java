package com.facebook.graphservice.modelutil;

import X.AnonymousClass1Y3;
import X.C12190on;
import X.C35961s6;
import com.facebook.graphql.enums.GraphQLMessengerCallToActionRenderStyle;
import com.facebook.graphql.enums.GraphQLMessengerCallToActionType;
import com.google.common.collect.ImmutableList;

public class GSTModelShape3S0000000 extends C12190on implements C35961s6 {
    public GSTModelShape3S0000000(int i, int[] iArr) {
        super(i, iArr);
    }

    public ImmutableList Abl() {
        return A0M(719606795, GSTModelShape1S0000000.class, -2012121995);
    }

    public GraphQLMessengerCallToActionType Abm() {
        return (GraphQLMessengerCallToActionType) A0O(-1206637242, GraphQLMessengerCallToActionType.A03);
    }

    public String Abp() {
        return A0P(1851392783);
    }

    public String Abq() {
        return A0P(1852205030);
    }

    public GSTModelShape1S0000000 Ag2() {
        return (GSTModelShape1S0000000) A0J(-1274961143, GSTModelShape1S0000000.class, 733396122);
    }

    public GSTModelShape1S0000000 Ait() {
        return (GSTModelShape1S0000000) A0J(-816385927, GSTModelShape1S0000000.class, -98833704);
    }

    public GraphQLMessengerCallToActionRenderStyle Aiu() {
        return (GraphQLMessengerCallToActionRenderStyle) A0O(1260551063, GraphQLMessengerCallToActionRenderStyle.A02);
    }

    public boolean Aqt() {
        return getBooleanValue(-464089615);
    }

    public boolean ArA() {
        return getBooleanValue(1294588541);
    }

    public boolean ArG() {
        return getBooleanValue(-140054959);
    }

    public String Asu() {
        return A0P(-1962564327);
    }

    public String AvP() {
        return A0P(-1190436537);
    }

    public String Awz() {
        return A0P(-803548981);
    }

    public GSTModelShape1S0000000 AxZ() {
        return (GSTModelShape1S0000000) A0J(-1429774008, GSTModelShape1S0000000.class, -210625380);
    }

    public GSTModelShape1S0000000 B84() {
        return (GSTModelShape1S0000000) A0J(976997417, GSTModelShape1S0000000.class, -580595041);
    }

    public GSTModelShape1S0000000 B9r() {
        return (GSTModelShape1S0000000) A0J(343464117, GSTModelShape1S0000000.class, 456592221);
    }

    public String getId() {
        return A0P(AnonymousClass1Y3.ARf);
    }
}
