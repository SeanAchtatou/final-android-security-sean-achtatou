package com.facebook.graphservice.modelutil;

import X.A7A;
import X.AnonymousClass1Y3;
import X.AnonymousClass24W;
import X.AnonymousClass4BN;
import X.AnonymousClass4BS;
import X.AnonymousClass4BZ;
import X.AnonymousClass4DL;
import X.AnonymousClass4DN;
import X.AnonymousClass4GY;
import X.AnonymousClass68B;
import X.AnonymousClass6NH;
import X.AnonymousClass7QB;
import X.AnonymousClass7QE;
import X.AnonymousClass7S5;
import X.AnonymousClass80H;
import X.AnonymousClass8E0;
import X.AnonymousClass8G5;
import X.C101244sE;
import X.C101324sM;
import X.C12190on;
import X.C125065uT;
import X.C133816Nv;
import X.C143896mu;
import X.C146346rI;
import X.C1531076i;
import X.C16820xp;
import X.C26133Csh;
import X.C26136Csk;
import X.C35961s6;
import X.C58012t1;
import X.C61192yU;
import X.C86574Bd;
import X.C86704Bq;
import X.C87434Et;
import X.C97644lf;
import X.CFI;
import X.CNA;
import X.EVY;
import com.facebook.graphql.enums.GraphQLFLTEntAREffectAssetCompressionType;
import com.facebook.graphql.enums.GraphQLJobOpeningSource;
import com.facebook.graphql.enums.GraphQLLightweightEventType;
import com.facebook.graphql.enums.GraphQLPaymentsFormFieldType;
import com.facebook.graphservice.factory.GraphQLServiceFactory;
import com.facebook.graphservice.interfaces.Tree;
import com.facebook.graphservice.tree.TreeJNI;
import com.google.common.collect.ImmutableList;

public class GSTModelShape1S0000000 extends C12190on {
    public GSTModelShape1S0000000(int i, int[] iArr) {
        super(i, iArr);
    }

    public static GSMBuilderShape0S0000000 A07(GSTModelShape1S0000000 gSTModelShape1S0000000, GraphQLServiceFactory graphQLServiceFactory) {
        if (gSTModelShape1S0000000 == null || !(gSTModelShape1S0000000 instanceof Tree) || !gSTModelShape1S0000000.isValid()) {
            return null;
        }
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("StoryHistoryContainer", GSMBuilderShape0S0000000.class, 969972740, gSTModelShape1S0000000);
    }

    public static GSMBuilderShape0S0000000 A08(GSTModelShape1S0000000 gSTModelShape1S0000000, GraphQLServiceFactory graphQLServiceFactory) {
        if (gSTModelShape1S0000000 == null || !(gSTModelShape1S0000000 instanceof Tree) || !gSTModelShape1S0000000.isValid()) {
            return null;
        }
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("PaymentPhase", GSMBuilderShape0S0000000.class, -1637705107, gSTModelShape1S0000000);
    }

    public static GSMBuilderShape0S0000000 A09(GSTModelShape1S0000000 gSTModelShape1S0000000, GraphQLServiceFactory graphQLServiceFactory) {
        if (gSTModelShape1S0000000 == null || !(gSTModelShape1S0000000 instanceof Tree) || !gSTModelShape1S0000000.isValid()) {
            return null;
        }
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("Question", GSMBuilderShape0S0000000.class, -1863968103, gSTModelShape1S0000000);
    }

    public static GSMBuilderShape0S0000000 A0A(GSTModelShape1S0000000 gSTModelShape1S0000000, GraphQLServiceFactory graphQLServiceFactory) {
        if (gSTModelShape1S0000000 == null || !(gSTModelShape1S0000000 instanceof Tree) || !gSTModelShape1S0000000.isValid()) {
            return null;
        }
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder(AnonymousClass80H.$const$string(39), GSMBuilderShape0S0000000.class, -389748053, gSTModelShape1S0000000);
    }

    public static boolean A0F(Object obj, int i) {
        return (obj instanceof GSTModelShape1S0000000) && ((GSTModelShape1S0000000) obj).mTypeTag == i;
    }

    public static GSMBuilderShape0S0000000 A00(GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("StoryOverlayRectangle", GSMBuilderShape0S0000000.class, 1291861580);
    }

    public static GSMBuilderShape0S0000000 A01(GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("PeerToPeerTransferContext", GSMBuilderShape0S0000000.class, 299761361);
    }

    public static GSMBuilderShape0S0000000 A02(GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("Question", GSMBuilderShape0S0000000.class, -1863968103);
    }

    public static GSMBuilderShape0S0000000 A03(GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder(AnonymousClass80H.$const$string(AnonymousClass1Y3.A2a), GSMBuilderShape0S0000000.class, -156769861);
    }

    public static GSMBuilderShape0S0000000 A04(GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("User", GSMBuilderShape0S0000000.class, -226051106);
    }

    public static GSMBuilderShape0S0000000 A05(GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("StoryCardSliderPoll", GSMBuilderShape0S0000000.class, 864418276);
    }

    public static GSMBuilderShape0S0000000 A06(GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("TextWithEntities", GSMBuilderShape0S0000000.class, 1079170184);
    }

    public static GSMBuilderShape0S0000000 A0B(String str, GraphQLServiceFactory graphQLServiceFactory) {
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder(str, GSMBuilderShape0S0000000.class, 684260477);
    }

    public static GSTModelShape1S0000000 A0C(GSTModelShape1S0000000 gSTModelShape1S0000000, GraphQLServiceFactory graphQLServiceFactory) {
        String typeName;
        if ((gSTModelShape1S0000000 instanceof TreeJNI) && gSTModelShape1S0000000.isValid()) {
            return (GSTModelShape1S0000000) gSTModelShape1S0000000.reinterpret(GSTModelShape1S0000000.class, 684260477);
        }
        GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = null;
        if (gSTModelShape1S0000000 != null && (typeName = gSTModelShape1S0000000.getTypeName()) != null && (gSTModelShape1S0000000 instanceof Tree) && gSTModelShape1S0000000.isValid()) {
            gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder(typeName, GSMBuilderShape0S0000000.class, 684260477, gSTModelShape1S0000000);
        }
        if (gSMBuilderShape0S0000000 == null) {
            return null;
        }
        return gSMBuilderShape0S0000000.A04();
    }

    public static GSTModelShape1S0000000 A0D(GSTModelShape1S0000000 gSTModelShape1S0000000, GraphQLServiceFactory graphQLServiceFactory) {
        if ((gSTModelShape1S0000000 instanceof TreeJNI) && gSTModelShape1S0000000.isValid()) {
            return (GSTModelShape1S0000000) gSTModelShape1S0000000.reinterpret(GSTModelShape1S0000000.class, -1637705107);
        }
        GSMBuilderShape0S0000000 A08 = A08(gSTModelShape1S0000000, graphQLServiceFactory);
        if (A08 == null) {
            return null;
        }
        return (GSTModelShape1S0000000) A08.getResult(GSTModelShape1S0000000.class, -1637705107);
    }

    public static GSTModelShape1S0000000 A0E(GSTModelShape1S0000000 gSTModelShape1S0000000, GraphQLServiceFactory graphQLServiceFactory) {
        if ((gSTModelShape1S0000000 instanceof TreeJNI) && gSTModelShape1S0000000.isValid()) {
            return (GSTModelShape1S0000000) gSTModelShape1S0000000.reinterpret(GSTModelShape1S0000000.class, 329547056);
        }
        GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = null;
        if (gSTModelShape1S0000000 != null && (gSTModelShape1S0000000 instanceof Tree) && gSTModelShape1S0000000.isValid()) {
            gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("PaymentReceiptView", GSMBuilderShape0S0000000.class, 329547056, gSTModelShape1S0000000);
        }
        if (gSMBuilderShape0S0000000 == null) {
            return null;
        }
        return (GSTModelShape1S0000000) gSMBuilderShape0S0000000.getResult(GSTModelShape1S0000000.class, 329547056);
    }

    public double A0V() {
        return getDoubleValue((int) AnonymousClass1Y3.A0t);
    }

    public double A0W() {
        return getDoubleValue((int) AnonymousClass1Y3.A0u);
    }

    public AnonymousClass8G5 A0g() {
        return (AnonymousClass8E0) A0J(1385608141, AnonymousClass8E0.class, 1463903662);
    }

    public GraphQLFLTEntAREffectAssetCompressionType A0h() {
        return (GraphQLFLTEntAREffectAssetCompressionType) A0O(-2051744141, GraphQLFLTEntAREffectAssetCompressionType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE);
    }

    public GraphQLJobOpeningSource A0i() {
        return (GraphQLJobOpeningSource) A0O(-1605858595, GraphQLJobOpeningSource.A01);
    }

    public GraphQLLightweightEventType A0j() {
        return (GraphQLLightweightEventType) A0O(-761751664, GraphQLLightweightEventType.A08);
    }

    public GraphQLPaymentsFormFieldType A0k() {
        return (GraphQLPaymentsFormFieldType) A0O(576861023, GraphQLPaymentsFormFieldType.A06);
    }

    public AnonymousClass68B A0l() {
        return (C101244sE) A0J(110371416, C101244sE.class, 1342072150);
    }

    public A7A A0m() {
        return (A7A) A0J(-1298275357, A7A.class, 2012351341);
    }

    public C143896mu A0n() {
        return (C146346rI) A0J(1254546617, C146346rI.class, -1003936475);
    }

    public C125065uT A0o() {
        return (AnonymousClass4BS) A0J(883555422, AnonymousClass4BS.class, -1121199273);
    }

    public AnonymousClass4BZ A0p() {
        return (C101324sM) A0J(100313435, C101324sM.class, 537206042);
    }

    public AnonymousClass4BZ A0q() {
        return (C101324sM) A0J(1782764648, C101324sM.class, 537206042);
    }

    public AnonymousClass4BZ A0r() {
        return (C101324sM) A0J(-1998221310, C101324sM.class, 537206042);
    }

    public AnonymousClass4BZ A0s() {
        return (C101324sM) A0J(-1815128087, C101324sM.class, 537206042);
    }

    public AnonymousClass4BZ A0t() {
        return (C101324sM) A0J(-424480887, C101324sM.class, 537206042);
    }

    public GSTModelShape1S0000000 A0u() {
        return (GSTModelShape1S0000000) A0J(92645877, GSTModelShape1S0000000.class, 2087983071);
    }

    public GSTModelShape1S0000000 A0v() {
        return (GSTModelShape1S0000000) A0J(92645877, GSTModelShape1S0000000.class, -1430266968);
    }

    public GSTModelShape1S0000000 A0w() {
        return (GSTModelShape1S0000000) A0J(-988594317, GSTModelShape1S0000000.class, 228567119);
    }

    public GSTModelShape1S0000000 A0x() {
        return (GSTModelShape1S0000000) A0J(-978570939, GSTModelShape1S0000000.class, -514510646);
    }

    public GSTModelShape1S0000000 A0y() {
        return (GSTModelShape1S0000000) A0J(-1224321666, GSTModelShape1S0000000.class, -2053575892);
    }

    public GSTModelShape1S0000000 A0z() {
        return (GSTModelShape1S0000000) A0J(-1224321666, GSTModelShape1S0000000.class, -418734808);
    }

    public GSTModelShape1S0000000 A10() {
        return (GSTModelShape1S0000000) A0J(-1828105316, GSTModelShape1S0000000.class, -104171155);
    }

    public GSTModelShape1S0000000 A11() {
        return (GSTModelShape1S0000000) A0J(1554253136, GSTModelShape1S0000000.class, 1881188725);
    }

    public GSTModelShape1S0000000 A12() {
        return (GSTModelShape1S0000000) A0J(-6652390, GSTModelShape1S0000000.class, 2054072718);
    }

    public GSTModelShape1S0000000 A13() {
        return (GSTModelShape1S0000000) A0J(-1526931561, GSTModelShape1S0000000.class, 311253028);
    }

    public GSTModelShape1S0000000 A14() {
        return (GSTModelShape1S0000000) A0J(3168, GSTModelShape1S0000000.class, 2087361092);
    }

    public GSTModelShape1S0000000 A15() {
        return (GSTModelShape1S0000000) A0J(-88565816, GSTModelShape1S0000000.class, -859482661);
    }

    public GSTModelShape1S0000000 A16() {
        return (GSTModelShape1S0000000) A0J(-1361224287, GSTModelShape1S0000000.class, 741373622);
    }

    public GSTModelShape1S0000000 A17() {
        return (GSTModelShape1S0000000) A0J(176286925, GSTModelShape1S0000000.class, -1586336358);
    }

    public GSTModelShape1S0000000 A18() {
        return (GSTModelShape1S0000000) A0J(-1354792126, GSTModelShape1S0000000.class, 1675310908);
    }

    public GSTModelShape1S0000000 A19() {
        return (GSTModelShape1S0000000) A0J(1333734487, GSTModelShape1S0000000.class, 1921750813);
    }

    public GSTModelShape1S0000000 A1A() {
        return (GSTModelShape1S0000000) A0J(265360233, GSTModelShape1S0000000.class, -35878956);
    }

    public GSTModelShape1S0000000 A1B() {
        return (GSTModelShape1S0000000) A0J(-1829477990, GSTModelShape1S0000000.class, 673781220);
    }

    public GSTModelShape1S0000000 A1C() {
        return (GSTModelShape1S0000000) A0J(1318671859, GSTModelShape1S0000000.class, -1373545378);
    }

    public GSTModelShape1S0000000 A1D() {
        return (GSTModelShape1S0000000) A0J(-1099638656, GSTModelShape1S0000000.class, -1012068429);
    }

    public GSTModelShape1S0000000 A1E() {
        return (GSTModelShape1S0000000) A0J(-645208747, GSTModelShape1S0000000.class, 2054072718);
    }

    public GSTModelShape1S0000000 A1F() {
        return (GSTModelShape1S0000000) A0J(269348155, GSTModelShape1S0000000.class, -61678789);
    }

    public GSTModelShape1S0000000 A1G() {
        return (GSTModelShape1S0000000) A0J(446812962, GSTModelShape1S0000000.class, 1745482795);
    }

    public GSTModelShape1S0000000 A1H() {
        return (GSTModelShape1S0000000) A0J(564950663, GSTModelShape1S0000000.class, 199624089);
    }

    public GSTModelShape1S0000000 A1I() {
        return (GSTModelShape1S0000000) A0J(1185668893, GSTModelShape1S0000000.class, -1680778145);
    }

    public GSTModelShape1S0000000 A1J() {
        return (GSTModelShape1S0000000) A0J(1400838279, GSTModelShape1S0000000.class, -495572513);
    }

    public GSTModelShape1S0000000 A1K() {
        return (GSTModelShape1S0000000) A0J(241987690, GSTModelShape1S0000000.class, 1710897872);
    }

    public GSTModelShape1S0000000 A1L() {
        return (GSTModelShape1S0000000) A0J(-163755499, GSTModelShape1S0000000.class, -845589036);
    }

    public GSTModelShape1S0000000 A1M() {
        return (GSTModelShape1S0000000) A0J(100313435, GSTModelShape1S0000000.class, 1017057558);
    }

    public GSTModelShape1S0000000 A1N() {
        return (GSTModelShape1S0000000) A0J(100313435, GSTModelShape1S0000000.class, -102393834);
    }

    public GSTModelShape1S0000000 A1O() {
        return (GSTModelShape1S0000000) A0J(100313435, GSTModelShape1S0000000.class, 605617470);
    }

    public GSTModelShape1S0000000 A1P() {
        return (GSTModelShape1S0000000) A0J(-978828488, GSTModelShape1S0000000.class, -104171155);
    }

    public GSTModelShape1S0000000 A1Q() {
        return (GSTModelShape1S0000000) A0J(-525984962, GSTModelShape1S0000000.class, 1291861580);
    }

    public GSTModelShape1S0000000 A1R() {
        return (GSTModelShape1S0000000) A0J(-2001303675, GSTModelShape1S0000000.class, -1918292560);
    }

    public GSTModelShape1S0000000 A1S() {
        return (GSTModelShape1S0000000) A0J(-1058180099, GSTModelShape1S0000000.class, 1450059307);
    }

    public GSTModelShape1S0000000 A1T() {
        return (GSTModelShape1S0000000) A0J(2108813409, GSTModelShape1S0000000.class, -120510922);
    }

    public GSTModelShape1S0000000 A1U() {
        return (GSTModelShape1S0000000) A0J(1960030857, GSTModelShape1S0000000.class, -348740057);
    }

    public GSTModelShape1S0000000 A1V() {
        return (GSTModelShape1S0000000) A0J(-457859463, GSTModelShape1S0000000.class, -1605233356);
    }

    public GSTModelShape1S0000000 A1W() {
        return (GSTModelShape1S0000000) A0J(-1831767292, GSTModelShape1S0000000.class, 513250268);
    }

    public GSTModelShape1S0000000 A1X() {
        return (GSTModelShape1S0000000) A0J(1901043637, GSTModelShape1S0000000.class, 252366612);
    }

    public GSTModelShape1S0000000 A1Y() {
        return (GSTModelShape1S0000000) A0J(-954429273, GSTModelShape1S0000000.class, 227722351);
    }

    public GSTModelShape1S0000000 A1Z() {
        return (GSTModelShape1S0000000) A0J(869797367, GSTModelShape1S0000000.class, -538753547);
    }

    public GSTModelShape1S0000000 A1a() {
        return (GSTModelShape1S0000000) A0J(1934729419, GSTModelShape1S0000000.class, -1425716356);
    }

    public GSTModelShape1S0000000 A1b() {
        return (GSTModelShape1S0000000) A0J(1345868457, GSTModelShape1S0000000.class, -493539821);
    }

    public GSTModelShape1S0000000 A1c() {
        return (GSTModelShape1S0000000) A0J(994922861, GSTModelShape1S0000000.class, 422852160);
    }

    public GSTModelShape1S0000000 A1d() {
        return (GSTModelShape1S0000000) A0J(-616808, GSTModelShape1S0000000.class, -494128793);
    }

    public GSTModelShape1S0000000 A1e() {
        return (GSTModelShape1S0000000) A0J(1754948465, GSTModelShape1S0000000.class, 341355684);
    }

    public GSTModelShape1S0000000 A1f() {
        return (GSTModelShape1S0000000) A0J(1754948465, GSTModelShape1S0000000.class, 2054498147);
    }

    public GSTModelShape1S0000000 A1g() {
        return (GSTModelShape1S0000000) A0J(-462094004, GSTModelShape1S0000000.class, 1138673927);
    }

    public GSTModelShape1S0000000 A1h() {
        return (GSTModelShape1S0000000) A0J(-462094004, GSTModelShape1S0000000.class, 1105096044);
    }

    public GSTModelShape1S0000000 A1i() {
        return (GSTModelShape1S0000000) A0J(266399994, GSTModelShape1S0000000.class, -1165605710);
    }

    public GSTModelShape1S0000000 A1j() {
        return (GSTModelShape1S0000000) A0J(266399994, GSTModelShape1S0000000.class, -940010585);
    }

    public GSTModelShape1S0000000 A1k() {
        return (GSTModelShape1S0000000) A0J(266399994, GSTModelShape1S0000000.class, -494128793);
    }

    public GSTModelShape1S0000000 A1l() {
        return (GSTModelShape1S0000000) A0J(266399994, GSTModelShape1S0000000.class, -1892209117);
    }

    public GSTModelShape1S0000000 A1m() {
        return (GSTModelShape1S0000000) A0J(-2026355421, GSTModelShape1S0000000.class, 1927247353);
    }

    public GSTModelShape1S0000000 A1n() {
        return (GSTModelShape1S0000000) A0J(-794200758, GSTModelShape1S0000000.class, 921501124);
    }

    public GSTModelShape1S0000000 A1o() {
        return (GSTModelShape1S0000000) A0J(1239242757, GSTModelShape1S0000000.class, 830136729);
    }

    public GSTModelShape1S0000000 A1p() {
        return (GSTModelShape1S0000000) A0J(-1443070655, GSTModelShape1S0000000.class, 1986364547);
    }

    public GSTModelShape1S0000000 A1q() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, 196745266);
    }

    public GSTModelShape1S0000000 A1r() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, 1845746626);
    }

    public GSTModelShape1S0000000 A1s() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, -1452138703);
    }

    public GSTModelShape1S0000000 A1t() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, 1624207503);
    }

    public GSTModelShape1S0000000 A1u() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, -864554112);
    }

    public GSTModelShape1S0000000 A1v() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, 964743220);
    }

    public GSTModelShape1S0000000 A1w() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, -6253768);
    }

    public GSTModelShape1S0000000 A1x() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, -1082076157);
    }

    public GSTModelShape1S0000000 A1y() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, -370149678);
    }

    public GSTModelShape1S0000000 A1z() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, -1985266588);
    }

    public GSTModelShape1S0000000 A20() {
        return (GSTModelShape1S0000000) A0J(3386882, GSTModelShape1S0000000.class, 91960430);
    }

    public GSTModelShape1S0000000 A21() {
        return (GSTModelShape1S0000000) A0J(-1263194516, GSTModelShape1S0000000.class, 572556773);
    }

    public GSTModelShape1S0000000 A22() {
        return (GSTModelShape1S0000000) A0J(1839623931, GSTModelShape1S0000000.class, -1510350880);
    }

    public GSTModelShape1S0000000 A23() {
        return (GSTModelShape1S0000000) A0J(3433103, GSTModelShape1S0000000.class, -934296781);
    }

    public GSTModelShape1S0000000 A24() {
        return (GSTModelShape1S0000000) A0J(3433103, GSTModelShape1S0000000.class, 596697613);
    }

    public GSTModelShape1S0000000 A25() {
        return (GSTModelShape1S0000000) A0J(3433103, GSTModelShape1S0000000.class, 680808654);
    }

    public GSTModelShape1S0000000 A26() {
        return (GSTModelShape1S0000000) A0J(3433103, GSTModelShape1S0000000.class, -1724729451);
    }

    public GSTModelShape1S0000000 A27() {
        return (GSTModelShape1S0000000) A0J(1324364035, GSTModelShape1S0000000.class, 1729585154);
    }

    public GSTModelShape1S0000000 A28() {
        return (GSTModelShape1S0000000) A0J(-860066186, GSTModelShape1S0000000.class, -1973844842);
    }

    public GSTModelShape1S0000000 A29() {
        return (GSTModelShape1S0000000) A0J(-860066186, GSTModelShape1S0000000.class, -1163512412);
    }

    public GSTModelShape1S0000000 A2A() {
        return (GSTModelShape1S0000000) A0J(-860066186, GSTModelShape1S0000000.class, -211943411);
    }

    public GSTModelShape1S0000000 A2B() {
        return (GSTModelShape1S0000000) A0J(-860066186, GSTModelShape1S0000000.class, 1155687200);
    }

    public GSTModelShape1S0000000 A2C() {
        return (GSTModelShape1S0000000) A0J(-286033967, GSTModelShape1S0000000.class, 2126231013);
    }

    public GSTModelShape1S0000000 A2D() {
        return (GSTModelShape1S0000000) A0J(-1062319133, GSTModelShape1S0000000.class, -1861396282);
    }

    public GSTModelShape1S0000000 A2E() {
        return (GSTModelShape1S0000000) A0J(106642798, GSTModelShape1S0000000.class, 1777756217);
    }

    public GSTModelShape1S0000000 A2F() {
        return (GSTModelShape1S0000000) A0J(696777252, GSTModelShape1S0000000.class, 1655859142);
    }

    public GSTModelShape1S0000000 A2G() {
        return (GSTModelShape1S0000000) A0J(-1682449686, GSTModelShape1S0000000.class, 245794106);
    }

    public GSTModelShape1S0000000 A2H() {
        return (GSTModelShape1S0000000) A0J(-193853315, GSTModelShape1S0000000.class, 571140476);
    }

    public GSTModelShape1S0000000 A2I() {
        return (GSTModelShape1S0000000) A0J(-1065138896, GSTModelShape1S0000000.class, 1679342960);
    }

    public GSTModelShape1S0000000 A2J() {
        return (GSTModelShape1S0000000) A0J(1372341280, GSTModelShape1S0000000.class, 1679342960);
    }

    public GSTModelShape1S0000000 A2K() {
        return (GSTModelShape1S0000000) A0J(-1058332932, GSTModelShape1S0000000.class, 1679342960);
    }

    public GSTModelShape1S0000000 A2L() {
        return (GSTModelShape1S0000000) A0J(1782764648, GSTModelShape1S0000000.class, -1587208257);
    }

    public GSTModelShape1S0000000 A2M() {
        return (GSTModelShape1S0000000) A0J(693933935, GSTModelShape1S0000000.class, 684260477);
    }

    public GSTModelShape1S0000000 A2N() {
        return (GSTModelShape1S0000000) A0J(1097546742, GSTModelShape1S0000000.class, -877265774);
    }

    public GSTModelShape1S0000000 A2O() {
        return (GSTModelShape1S0000000) A0J(1097546742, GSTModelShape1S0000000.class, -1961434515);
    }

    public GSTModelShape1S0000000 A2P() {
        return (GSTModelShape1S0000000) A0J(1252597855, GSTModelShape1S0000000.class, -1725173366);
    }

    public GSTModelShape1S0000000 A2Q() {
        return (GSTModelShape1S0000000) A0J(-1307788907, GSTModelShape1S0000000.class, 2027240176);
    }

    public GSTModelShape1S0000000 A2R() {
        return (GSTModelShape1S0000000) A0J(-1307788907, GSTModelShape1S0000000.class, 974452808);
    }

    public GSTModelShape1S0000000 A2S() {
        return (GSTModelShape1S0000000) A0J(-1725248398, GSTModelShape1S0000000.class, 548556612);
    }

    public GSTModelShape1S0000000 A2T() {
        return (GSTModelShape1S0000000) A0J(763039218, GSTModelShape1S0000000.class, 1114775080);
    }

    public GSTModelShape1S0000000 A2U() {
        return (GSTModelShape1S0000000) A0J(-332767512, GSTModelShape1S0000000.class, 2040968);
    }

    public GSTModelShape1S0000000 A2V() {
        return (GSTModelShape1S0000000) A0J(-1442296241, GSTModelShape1S0000000.class, -95213696);
    }

    public GSTModelShape1S0000000 A2W() {
        return (GSTModelShape1S0000000) A0J(-1442296241, GSTModelShape1S0000000.class, -1597613766);
    }

    public GSTModelShape1S0000000 A2X() {
        return (GSTModelShape1S0000000) A0J(-982254365, GSTModelShape1S0000000.class, 1739646070);
    }

    public GSTModelShape1S0000000 A2Y() {
        return (GSTModelShape1S0000000) A0J(-1890252483, GSTModelShape1S0000000.class, 1660608901);
    }

    public GSTModelShape1S0000000 A2Z() {
        return (GSTModelShape1S0000000) A0J(1634011452, GSTModelShape1S0000000.class, 157289253);
    }

    public GSTModelShape1S0000000 A2a() {
        return (GSTModelShape1S0000000) A0J(-2034744124, GSTModelShape1S0000000.class, -167706386);
    }

    public GSTModelShape1S0000000 A2b() {
        return (GSTModelShape1S0000000) A0J(20623059, GSTModelShape1S0000000.class, 1040143753);
    }

    public GSTModelShape1S0000000 A2c() {
        return (GSTModelShape1S0000000) A0J(-891050150, GSTModelShape1S0000000.class, 16480762);
    }

    public GSTModelShape1S0000000 A2d() {
        return (GSTModelShape1S0000000) A0J(592157427, GSTModelShape1S0000000.class, 1012143089);
    }

    public GSTModelShape1S0000000 A2e() {
        return (GSTModelShape1S0000000) A0J(-148662098, GSTModelShape1S0000000.class, -1614994632);
    }

    public GSTModelShape1S0000000 A2f() {
        return (GSTModelShape1S0000000) A0J(-880905839, GSTModelShape1S0000000.class, -2027790600);
    }

    public GSTModelShape1S0000000 A2g() {
        return (GSTModelShape1S0000000) A0J(1854819208, GSTModelShape1S0000000.class, 802898961);
    }

    public GSTModelShape1S0000000 A2h() {
        return (GSTModelShape1S0000000) A0J(-1184643414, GSTModelShape1S0000000.class, -919186426);
    }

    public GSTModelShape1S0000000 A2i() {
        return (GSTModelShape1S0000000) A0J(-775936925, GSTModelShape1S0000000.class, 338091187);
    }

    public GSTModelShape1S0000000 A2j() {
        return (GSTModelShape1S0000000) A0J(-775936925, GSTModelShape1S0000000.class, 996533544);
    }

    public GSTModelShape1S0000000 A2k() {
        return (GSTModelShape1S0000000) A0J(-1267058000, GSTModelShape1S0000000.class, -506845057);
    }

    public GSTModelShape1S0000000 A2l() {
        return (GSTModelShape1S0000000) A0J(860819769, GSTModelShape1S0000000.class, -1590027005);
    }

    public GSTModelShape1S0000000 A2m() {
        return (GSTModelShape1S0000000) A0J(1152263352, GSTModelShape1S0000000.class, -1705211597);
    }

    public GSTModelShape1S0000000 A2n() {
        return (GSTModelShape1S0000000) A0J(3599307, GSTModelShape1S0000000.class, 628331255);
    }

    public GSTModelShape1S0000000 A2o() {
        return (GSTModelShape1S0000000) A0J(3599307, GSTModelShape1S0000000.class, -632336103);
    }

    public GSTModelShape1S0000000 A2p() {
        return (GSTModelShape1S0000000) A0J(-664008627, GSTModelShape1S0000000.class, 707078882);
    }

    public GSTModelShape1S0000000 A2q() {
        return (GSTModelShape1S0000000) A0J(111972721, GSTModelShape1S0000000.class, -1565716103);
    }

    public GSTModelShape1S0000000 A2r() {
        return (GSTModelShape1S0000000) A0J(-1075724407, GSTModelShape1S0000000.class, 1889249680);
    }

    public GSTModelShape1S0000000 A2s() {
        return (GSTModelShape1S0000000) A0J(-816631278, GSTModelShape1S0000000.class, -137131909);
    }

    public GSTModelShape1S0000000 A2t() {
        return (GSTModelShape1S0000000) A0J(-816631278, GSTModelShape1S0000000.class, 451510084);
    }

    public GSTModelShape1S0000000 A2u() {
        return (GSTModelShape1S0000000) A0J(-816631278, GSTModelShape1S0000000.class, -2101452517);
    }

    public GSTModelShape1S0000000 A2v() {
        return (GSTModelShape1S0000000) A0J(309451119, GSTModelShape1S0000000.class, 2068656848);
    }

    public GSTModelShape1S0000000 A2w() {
        return (GSTModelShape1S0000000) A0J(-810660181, GSTModelShape1S0000000.class, -323015438);
    }

    public /* bridge */ /* synthetic */ GSTModelShape1S0000000 A2x() {
        return (GSTModelShape1S0000000) A0J(103772132, GSTModelShape1S0000000.class, 1616276231);
    }

    public /* bridge */ /* synthetic */ GSTModelShape1S0000000 A2y() {
        return (GSTModelShape1S0000000) A0J(-1249474914, GSTModelShape1S0000000.class, -156769861);
    }

    public C35961s6 A2z() {
        return (GSTModelShape3S0000000) A0J(322316244, GSTModelShape3S0000000.class, 423528630);
    }

    public EVY A30() {
        return (EVY) A0J(-234430262, EVY.class, 278824512);
    }

    public CNA A31() {
        return (AnonymousClass4BN) A0J(3327403, AnonymousClass4BN.class, 2132723742);
    }

    public C61192yU A32() {
        return (C58012t1) A0J(244603111, C58012t1.class, 1042585914);
    }

    public AnonymousClass7S5 A33() {
        return (AnonymousClass7S5) A0J(266399994, AnonymousClass7S5.class, 567424580);
    }

    public AnonymousClass7QB A34() {
        return (AnonymousClass7QB) A0J(696006808, AnonymousClass7QB.class, -1175876925);
    }

    public AnonymousClass7QE A35() {
        return (AnonymousClass7QE) A0J(696006808, AnonymousClass7QE.class, -1180597048);
    }

    public C16820xp A36() {
        return (C16820xp) A0J(1867669994, C16820xp.class, 1916299833);
    }

    public C26136Csk A37() {
        return (C26133Csh) A0J(-1890252483, C26133Csh.class, -1095075201);
    }

    public AnonymousClass24W A38() {
        return (AnonymousClass24W) A0J(3386882, AnonymousClass24W.class, -817923101);
    }

    public C1531076i A39() {
        return (C1531076i) A0J(3386882, C1531076i.class, -216695744);
    }

    public AnonymousClass6NH A3A() {
        return (AnonymousClass4DN) A0J(591189366, AnonymousClass4DN.class, -895192639);
    }

    public C133816Nv A3B() {
        return (AnonymousClass4DL) A0J(-801074910, AnonymousClass4DL.class, 369377121);
    }

    public C133816Nv A3C() {
        return (AnonymousClass4DL) A0J(3619493, AnonymousClass4DL.class, 369377121);
    }

    public CFI A3D() {
        return (C86574Bd) A0J(106934601, C86574Bd.class, 579244465);
    }

    public C86704Bq A3E() {
        return (C86704Bq) A0J(-612557761, C86704Bq.class, 1675946283);
    }

    public ImmutableList A3F() {
        return A0M(-64262029, GSTModelShape1S0000000.class, 1252421754);
    }

    public ImmutableList A3G() {
        return A0M(-738997328, GSTModelShape1S0000000.class, 906223158);
    }

    public ImmutableList A3H() {
        return A0M(241352577, GSTModelShape1S0000000.class, 646499151);
    }

    public ImmutableList A3I() {
        return A0M(-1164843116, GSTModelShape1S0000000.class, -1900815635);
    }

    public ImmutableList A3J() {
        return A0M(-746436398, GSTModelShape1S0000000.class, 1055088658);
    }

    public ImmutableList A3K() {
        return A0M(-447446250, GSTModelShape1S0000000.class, 96187451);
    }

    public ImmutableList A3L() {
        return A0M(96356950, GSTModelShape1S0000000.class, 888407518);
    }

    public ImmutableList A3M() {
        return A0M(627814927, GSTModelShape3S0000000.class, 423528630);
    }

    public ImmutableList A3N() {
        return A0M(104993457, GSTModelShape1S0000000.class, -653193155);
    }

    public ImmutableList A3O() {
        return A0M(104993457, C97644lf.class, 1203327745);
    }

    public ImmutableList A3P() {
        return A0M(104993457, GSTModelShape1S0000000.class, -936788936);
    }

    public ImmutableList A3Q() {
        return A0M(104993457, GSTModelShape1S0000000.class, -1158327733);
    }

    public ImmutableList A3R() {
        return A0M(104993457, AnonymousClass4GY.class, -664565278);
    }

    public ImmutableList A3S() {
        return A0M(104993457, GSTModelShape1S0000000.class, 378229695);
    }

    public ImmutableList A3T() {
        return A0M(104993457, GSTModelShape1S0000000.class, 1899957318);
    }

    public ImmutableList A3U() {
        return A0M(104993457, GSTModelShape1S0000000.class, -389748053);
    }

    public ImmutableList A3V() {
        return A0M(104993457, GSTModelShape1S0000000.class, -226051106);
    }

    public ImmutableList A3W() {
        return A0M(104993457, GSTModelShape1S0000000.class, 1207439779);
    }

    public ImmutableList A3X() {
        return A0M(104993457, GSTModelShape1S0000000.class, 1043817606);
    }

    public ImmutableList A3Y() {
        return A0M(104993457, GSTModelShape1S0000000.class, -1030923629);
    }

    public ImmutableList A3Z() {
        return A0M(104993457, GSTModelShape1S0000000.class, 1517090099);
    }

    public ImmutableList A3a() {
        return A0M(104993457, GSTModelShape1S0000000.class, 313241816);
    }

    public ImmutableList A3b() {
        return A0M(-892168674, GSTModelShape1S0000000.class, 333599975);
    }

    public ImmutableList A3c() {
        return A0M(109761319, C87434Et.class, 1525500545);
    }

    public ImmutableList A3d() {
        return A0M(2117924273, GSTModelShape1S0000000.class, 1366449572);
    }

    public String A3m() {
        return A0P(AnonymousClass1Y3.ARf);
    }

    public double A0Q() {
        return getDoubleValue(-1221029593);
    }

    public double A0R() {
        return getDoubleValue(-1439978388);
    }

    public double A0S() {
        return getDoubleValue(137365935);
    }

    public double A0T() {
        return getDoubleValue(-40300674);
    }

    public double A0U() {
        return getDoubleValue(113126854);
    }

    public int A0X() {
        return getIntValue(94851343);
    }

    public int A0Y() {
        return getIntValue(1681295657);
    }

    public int A0Z() {
        return getIntValue(-1221029593);
    }

    public int A0a() {
        return getIntValue(-1106363674);
    }

    public int A0b() {
        return getIntValue(-1019779949);
    }

    public int A0c() {
        return getIntValue(115581542);
    }

    public int A0d() {
        return getIntValue(789880110);
    }

    public int A0e() {
        return getIntValue(113126854);
    }

    public long A0f() {
        return getTimeValue(109757538);
    }

    public String A3e() {
        return A0P(-1413853096);
    }

    public String A3f() {
        return A0P(-553259998);
    }

    public String A3g() {
        return A0P(575402001);
    }

    public String A3h() {
        return A0P(-1724546052);
    }

    public String A3i() {
        return A0P(-481040315);
    }

    public String A3j() {
        return A0P(-1938755376);
    }

    public String A3k() {
        return A0P(-817778335);
    }

    public String A3l() {
        return A0P(1814466875);
    }

    public String A3n() {
        return A0P(-877823861);
    }

    public String A3o() {
        return A0P(102727412);
    }

    public String A3p() {
        return A0P(1152095023);
    }

    public String A3q() {
        return A0P(-1690722221);
    }

    public String A3r() {
        return A0P(3373707);
    }

    public String A3s() {
        return A0P(1333068768);
    }

    public String A3t() {
        return A0P(752641086);
    }

    public String A3u() {
        return A0P(964289556);
    }

    public String A3v() {
        return A0P(1565793390);
    }

    public String A3w() {
        return A0P(-2061635299);
    }

    public String A3x() {
        return A0P(-2060497896);
    }

    public String A3y() {
        return A0P(3556653);
    }

    public String A3z() {
        return A0P(1930607596);
    }

    public String A40() {
        return A0P(110371416);
    }

    public String A41() {
        return A0P(116076);
    }

    public String A42() {
        return A0P(116079);
    }

    public String A43() {
        return A0P(111972721);
    }

    public boolean A44() {
        return getBooleanValue(-1575811850);
    }

    public boolean A45() {
        return getBooleanValue(-1867169789);
    }
}
