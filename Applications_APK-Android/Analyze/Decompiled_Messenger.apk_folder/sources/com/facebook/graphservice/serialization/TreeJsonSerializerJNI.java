package com.facebook.graphservice.serialization;

import X.AnonymousClass01q;
import com.facebook.graphservice.interfaces.Tree;
import com.facebook.graphservice.interfaces.TreeJsonSerializer;
import com.facebook.graphservice.tree.TreeJNI;
import com.facebook.jni.HybridData;

public class TreeJsonSerializerJNI implements TreeJsonSerializer {
    private final HybridData mHybridData;

    private native TreeJNI deserializeFromJsonNative(String str, long j, Class cls, int i, String str2);

    static {
        AnonymousClass01q.A08("graphservice-jni-serialization");
    }

    private TreeJsonSerializerJNI(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public Tree deserializeFromJson(String str, long j, Class cls, int i, String str2) {
        return deserializeFromJsonNative(str, j, cls, i, str2);
    }
}
