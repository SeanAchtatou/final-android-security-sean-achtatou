package com.facebook.graphservice.serialization;

import X.AnonymousClass01q;
import com.facebook.graphservice.interfaces.Tree;
import com.facebook.graphservice.interfaces.TreeSerializer;
import com.facebook.graphservice.tree.TreeJNI;
import com.facebook.jni.HybridData;
import com.google.common.base.Preconditions;
import java.nio.ByteBuffer;

public class TreeSerializerJNI implements TreeSerializer {
    private final HybridData mHybridData;

    private native TreeJNI deserializeTreeFromByteBufferNative(ByteBuffer byteBuffer, Class cls, int i);

    private native TreeJNI deserializeTreeNative(String str, Class cls, int i, int i2, int i3);

    private native int serializeTreeNative(TreeJNI treeJNI, String str, boolean z);

    private native ByteBuffer serializeTreeToByteBufferNative(TreeJNI treeJNI);

    public Tree deserializeTree(String str, Class cls, int i) {
        return deserializeTreeNative(str, cls, i, 0, 0);
    }

    static {
        AnonymousClass01q.A08("graphservice-jni-serialization");
    }

    private TreeSerializerJNI(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public Tree deserializeTreeFromByteBuffer(ByteBuffer byteBuffer, Class cls, int i) {
        if (!byteBuffer.isDirect()) {
            ByteBuffer allocateDirect = ByteBuffer.allocateDirect(byteBuffer.capacity());
            if (allocateDirect.isDirect()) {
                allocateDirect.put(byteBuffer.duplicate());
                allocateDirect.position(0);
                byteBuffer = allocateDirect;
            } else {
                throw new UnsupportedOperationException("Direct ByteBuffer is not supported on this platform");
            }
        }
        return deserializeTreeFromByteBufferNative(byteBuffer, cls, i);
    }

    public int serializeTree(Tree tree, String str) {
        Preconditions.checkNotNull(tree);
        Preconditions.checkArgument(tree.isValid());
        return serializeTreeNative((TreeJNI) tree, str, false);
    }

    public ByteBuffer serializeTreeToByteBuffer(Tree tree) {
        Preconditions.checkNotNull(tree);
        Preconditions.checkArgument(tree.isValid());
        return serializeTreeToByteBufferNative((TreeJNI) tree);
    }
}
