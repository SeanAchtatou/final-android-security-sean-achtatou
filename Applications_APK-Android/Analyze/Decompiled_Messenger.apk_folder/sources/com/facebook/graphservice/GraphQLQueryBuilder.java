package com.facebook.graphservice;

import X.AnonymousClass01q;
import X.C005505z;
import X.C33431nZ;
import com.facebook.graphservice.asset.GraphServiceAsset;
import com.facebook.graphservice.interfaces.GraphQLQuery;
import com.facebook.graphservice.nativeutil.NativeMap;
import com.facebook.jni.HybridData;

public class GraphQLQueryBuilder {
    private final HybridData mHybridData;

    private static native HybridData initHybridData(GraphQLConfigHintsJNI graphQLConfigHintsJNI, String str, String str2, long j, NativeMap nativeMap, Class cls, int i, GraphServiceAsset graphServiceAsset, boolean z);

    public native GraphQLQuery getResult();

    static {
        AnonymousClass01q.A08("graphservice-jni");
    }

    public GraphQLQueryBuilder(C33431nZ r11, String str, String str2, long j, NativeMap nativeMap, Class cls, int i, GraphServiceAsset graphServiceAsset, boolean z) {
        C005505z.A05("GraphQLQueryBuilder(%s)", str2, 2008495003);
        String str3 = str;
        long j2 = j;
        NativeMap nativeMap2 = nativeMap;
        this.mHybridData = initHybridData(new GraphQLConfigHintsJNI(r11), str3, str2, j2, nativeMap2, cls, i, graphServiceAsset, z);
        C005505z.A00(-1440774956);
    }
}
