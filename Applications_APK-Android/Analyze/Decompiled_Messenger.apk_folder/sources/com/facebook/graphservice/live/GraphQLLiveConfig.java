package com.facebook.graphservice.live;

import X.AnonymousClass0UN;
import X.AnonymousClass1OV;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass80H;
import X.AnonymousClass8S4;
import X.AnonymousClass8S5;
import X.AnonymousClass8S6;
import X.C23191Oo;
import X.C23211Oq;
import X.C25051Yd;
import io.card.payment.BuildConfig;
import java.util.Random;

public class GraphQLLiveConfig {
    public AnonymousClass0UN $ul_mInjectionContext;

    public double getGlobalDouble(String str, double d) {
        return d;
    }

    public int getGlobalInt(String str, int i) {
        return i;
    }

    public String getGlobalString(String str, String str2) {
        return str2;
    }

    public static final GraphQLLiveConfig $ul_$xXXcom_facebook_graphservice_live_GraphQLLiveConfig$xXXFACTORY_METHOD(AnonymousClass1XY r1) {
        return new GraphQLLiveConfig(r1);
    }

    public C23211Oq getConfigForId(String str) {
        return ((AnonymousClass1OV) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5m, this.$ul_mInjectionContext)).A02(844957506142295L, new AnonymousClass8S6(str));
    }

    public boolean getGlobalBool(String str, boolean z) {
        C25051Yd r2;
        long j;
        if (str.equals("isIndefinitelyRetryableErrorsEnabled")) {
            r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
            j = 288166535765659L;
        } else if (str.equals("isPaused")) {
            return ((Boolean) AnonymousClass1XX.A03(AnonymousClass1Y3.B8e, this.$ul_mInjectionContext)).booleanValue();
        } else {
            if (str.equals("liveQueryGenerateRequestID")) {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 284739152122542L;
            } else if (!str.equals("liveQueryOverRequestStreamExperiment")) {
                return z;
            } else {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282007552852987L;
            }
        }
        return r2.Aem(j);
    }

    public C23211Oq getInitialsRolloutConfigForId(String str) {
        return ((AnonymousClass1OV) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5m, this.$ul_mInjectionContext)).A02(845086355619952L, new AnonymousClass8S5(str));
    }

    public boolean getLiveConfigBool(String str, String str2, boolean z) {
        C25051Yd r2;
        long j;
        if (str2.equals("isLiveQueryEnabled")) {
            return getConfigForId(str).A04("live_query_enabled", true);
        }
        if (!str2.equals("isInitialThroughWWW")) {
            return z;
        }
        if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(282136401675403L)) {
            C23211Oq initialsRolloutConfigForId = getInitialsRolloutConfigForId(str);
            String str3 = BuildConfig.FLAVOR;
            C23191Oo A00 = C23211Oq.A00(initialsRolloutConfigForId, "group");
            if (A00 != null) {
                str3 = A00.toString();
            }
            if (str3.equalsIgnoreCase("complete")) {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282136401740940L;
            } else if (str3.equalsIgnoreCase("test_group_1")) {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282136401806477L;
            } else if (str3.equalsIgnoreCase("test_group_2")) {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282136401872014L;
            } else if (str3.equalsIgnoreCase("test_group_3")) {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282136401937551L;
            } else if (str3.equalsIgnoreCase("test_group_4")) {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282136402003088L;
            } else if (str3.equalsIgnoreCase("test_group_5")) {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282136402068625L;
            } else if (!str3.equalsIgnoreCase("test_group_6")) {
                return false;
            } else {
                r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
                j = 282136402134162L;
            }
        } else {
            r2 = (C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext);
            j = 282007552787450L;
        }
        return r2.Aem(j);
    }

    public double getLiveConfigDouble(String str, String str2, double d) {
        if (!((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284739151860396L)) {
            return d;
        }
        if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284739151925933L)) {
            return 1.0d;
        }
        new Random();
        return getLoggingConfigForId(str).A01("sampling_rate", d);
    }

    public int getLiveConfigInt(String str, String str2, int i) {
        if (!str2.equals("pollingInterval")) {
            return i;
        }
        boolean z = false;
        if (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(282007552656377L)) {
            z = true;
        }
        if (z) {
            return -1;
        }
        return (int) getConfigForId(str).A02("polling_interval_sec", 5);
    }

    public String getLiveConfigString(String str, String str2, String str3) {
        if (str2.equals("liveQueryForceLogContext") && ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284739151860396L)) {
            C23211Oq loggingConfigForId = getLoggingConfigForId(str);
            boolean Aem = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.$ul_mInjectionContext)).Aem(284739151925933L);
            String $const$string = AnonymousClass80H.$const$string(482);
            if (Aem || new Random().nextDouble() < loggingConfigForId.A01("sampling_rate", 0.0d)) {
                C23191Oo A00 = C23211Oq.A00(loggingConfigForId, $const$string);
                if (A00 != null) {
                    return A00.toString();
                }
                return BuildConfig.FLAVOR;
            }
        }
        return str3;
    }

    public C23211Oq getLoggingConfigForId(String str) {
        return ((AnonymousClass1OV) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5m, this.$ul_mInjectionContext)).A02(847689105474040L, new AnonymousClass8S4(str));
    }

    public GraphQLLiveConfig(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(2, r3);
    }
}
