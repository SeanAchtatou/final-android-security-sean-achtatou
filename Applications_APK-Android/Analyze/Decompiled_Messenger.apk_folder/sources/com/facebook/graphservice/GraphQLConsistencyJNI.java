package com.facebook.graphservice;

import X.AnonymousClass01q;
import X.AnonymousClass11R;
import X.AnonymousClass1OP;
import X.C05460Za;
import X.C34141on;
import com.facebook.graphservice.asset.GraphServiceAsset;
import com.facebook.graphservice.interfaces.GraphQLConsistency;
import com.facebook.graphservice.interfaces.GraphQLService;
import com.facebook.graphservice.interfaces.Tree;
import com.facebook.graphservice.tree.TreeBuilderJNI;
import com.facebook.graphservice.tree.TreeJNI;
import com.facebook.jni.HybridData;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

public class GraphQLConsistencyJNI implements GraphQLConsistency, C05460Za, AnonymousClass1OP {
    private final String mDirectoryPathForManualCleanup;
    private final HybridData mHybridData;

    public class MutationHandle {
        private final HybridData mHybridData;

        public native void commit();

        public native void rollback();

        static {
            AnonymousClass01q.A08("graphservice-jni");
        }

        public MutationHandle(HybridData hybridData) {
            this.mHybridData = hybridData;
        }
    }

    private native ListenableFuture applyOptimistic(TreeJNI treeJNI, GraphQLConsistency.MutationPublisherRequest mutationPublisherRequest);

    private native ListenableFuture applyOptimisticBuilder(TreeBuilderJNI treeBuilderJNI, GraphQLConsistency.MutationPublisherRequest mutationPublisherRequest);

    private static native HybridData initHybridData(GraphServiceAsset graphServiceAsset, String str, Executor executor, ScheduledExecutorService scheduledExecutorService, int i, int i2);

    private native ListenableFuture lookup(TreeJNI treeJNI, Class cls, int i);

    private native ListenableFuture publish(TreeJNI treeJNI, boolean z);

    private native ListenableFuture publishBuilder(TreeBuilderJNI treeBuilderJNI, boolean z);

    private native GraphQLService.Token subscribe(TreeJNI treeJNI, GraphQLService.DataCallbacks dataCallbacks, Executor executor, Class cls, int i, boolean z);

    static {
        AnonymousClass01q.A08("graphservice-jni");
    }

    private TreeJNI assertTree(Object obj) {
        if (!(obj instanceof TreeJNI)) {
            throw new IllegalArgumentException("Can only subscribe on an instance served from GraphQLService");
        } else if (((Tree) obj).isValid()) {
            return (TreeJNI) obj;
        } else {
            throw new IllegalArgumentException("Tree is not backed by native data");
        }
    }

    public void clearUserData() {
        String str = this.mDirectoryPathForManualCleanup;
        if (str != null) {
            C34141on.A02(str);
        }
    }

    public GraphQLConsistencyJNI(GraphServiceAsset graphServiceAsset, String str, Executor executor, ScheduledExecutorService scheduledExecutorService, int i, int i2) {
        this.mDirectoryPathForManualCleanup = str;
        this.mHybridData = initHybridData(graphServiceAsset, str, executor, scheduledExecutorService, i, i2);
    }

    public ListenableFuture publishBuilderWithFullConsistency(AnonymousClass11R r2) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return publishBuilder((TreeBuilderJNI) r2, true);
    }

    public ListenableFuture publishWithFullConsistency(Tree tree) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return publish((TreeJNI) tree, true);
    }

    public GraphQLService.Token subscribeWithFullConsistency(Object obj, GraphQLService.DataCallbacks dataCallbacks, Executor executor) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return subscribe(assertTree(obj), dataCallbacks, executor, obj.getClass(), ((Tree) obj).getTypeTag(), true);
    }

    public void trimToMinimum() {
        clearUserData();
    }

    public void trimToNothing() {
        clearUserData();
    }

    public ListenableFuture applyOptimistic(Tree tree, GraphQLConsistency.MutationPublisherRequest mutationPublisherRequest) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return applyOptimistic((TreeJNI) tree, mutationPublisherRequest);
    }

    public ListenableFuture applyOptimisticBuilder(AnonymousClass11R r2, GraphQLConsistency.MutationPublisherRequest mutationPublisherRequest) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return applyOptimisticBuilder((TreeBuilderJNI) r2, mutationPublisherRequest);
    }

    public ListenableFuture lookup(Object obj) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return lookup(assertTree(obj), obj.getClass(), ((Tree) obj).getTypeTag());
    }

    public ListenableFuture publish(Tree tree) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return publish((TreeJNI) tree, false);
    }

    public ListenableFuture publishBuilder(AnonymousClass11R r2) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return publishBuilder((TreeBuilderJNI) r2, false);
    }

    public GraphQLService.Token subscribe(Object obj, GraphQLService.DataCallbacks dataCallbacks, Executor executor) {
        GraphQLServiceJNI.warnIfTestEnvironment();
        return subscribe(assertTree(obj), dataCallbacks, executor, obj.getClass(), ((Tree) obj).getTypeTag(), false);
    }
}
