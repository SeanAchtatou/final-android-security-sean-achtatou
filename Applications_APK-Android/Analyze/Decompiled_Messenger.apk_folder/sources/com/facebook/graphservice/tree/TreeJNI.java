package com.facebook.graphservice.tree;

import X.AnonymousClass01q;
import X.AnonymousClass08S;
import X.AnonymousClass7NQ;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.graphservice.interfaces.Tree;
import com.facebook.jni.HybridClassBase;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.nio.charset.Charset;
import java.util.Arrays;

public class TreeJNI extends HybridClassBase implements Tree {
    private static final Charset UTF_8 = Charset.forName("UTF8");
    public final int[] mSetFields;
    public final int mTypeTag;

    private native ImmutableList getBooleanListNative(int i);

    private native ImmutableList getBooleanListNative(String str);

    private native Boolean getBooleanNative(String str);

    private native boolean getBooleanValueNative(int i);

    private native boolean getBooleanValueNative(String str);

    private native ImmutableList getDoubleListNative(int i);

    private native ImmutableList getDoubleListNative(String str);

    private native Double getDoubleNative(String str);

    private native double getDoubleValueNative(int i);

    private native double getDoubleValueNative(String str);

    private native ImmutableList getIntListNative(int i);

    private native ImmutableList getIntListNative(String str);

    private native Integer getIntNative(String str);

    private native int getIntValueNative(int i);

    private native int getIntValueNative(String str);

    private native ImmutableList getStringListNative(int i);

    private native ImmutableList getStringListNative(String str);

    private native String getStringNative(int i);

    private native String getStringNative(String str);

    private native ImmutableList getTimeListNative(int i);

    private native ImmutableList getTimeListNative(String str);

    private native Long getTimeNative(String str);

    private native long getTimeValueNative(int i);

    private native long getTimeValueNative(String str);

    private native TreeJNI[] getTreeArrayNative(int i, Class cls, int i2);

    private native TreeJNI[] getTreeArrayNative(String str, Class cls, int i);

    private native TreeJNI getTreeNative(int i, Class cls, int i2);

    private native TreeJNI getTreeNative(String str, Class cls, int i);

    private native TreeJNI rerootNative(String str, Class cls, int i);

    public final native ImmutableList getAliases();

    public native Boolean getBooleanVariable(String str);

    public final native ImmutableList getCanonicals();

    public final native Tree.FieldType getFieldType(String str);

    public native String getTypeName();

    public final native boolean hasFieldValue(int i);

    public final native boolean hasFieldValue(String str);

    public final native boolean hasPrimaryKey();

    public native boolean isDeepEqual(TreeJNI treeJNI);

    public final native TreeJNI reinterpret(Class cls, int i);

    public native String toExpensiveHumanReadableDebugString();

    public native String toString();

    static {
        AnonymousClass01q.A08(TurboLoader.Locator.$const$string(79));
    }

    public static final TreeJNI[] filterNullArrayEntries(TreeJNI[] treeJNIArr) {
        if (treeJNIArr == null) {
            return null;
        }
        int i = 0;
        for (TreeJNI treeJNI : treeJNIArr) {
            if (treeJNI == null) {
                i++;
            }
        }
        if (i == 0) {
            return treeJNIArr;
        }
        TreeJNI[] treeJNIArr2 = new TreeJNI[(r5 - i)];
        int i2 = 0;
        for (TreeJNI treeJNI2 : treeJNIArr) {
            if (treeJNI2 != null) {
                treeJNIArr2[i2] = treeJNI2;
                i2++;
            }
        }
        return treeJNIArr2;
    }

    private boolean isFieldUnset(int i) {
        int[] iArr = this.mSetFields;
        if (iArr == null || Arrays.binarySearch(iArr, i) >= 0) {
            return false;
        }
        return true;
    }

    public int getFieldCacheIndex(int i) {
        int[] iArr = this.mSetFields;
        if (iArr == null) {
            return -1;
        }
        return Arrays.binarySearch(iArr, i);
    }

    public final AnonymousClass7NQ getPaginableTreeList(String str, Class cls, int i) {
        AnonymousClass7NQ r8;
        ImmutableList copyOf;
        ImmutableList copyOf2;
        String str2 = str;
        String A0J = AnonymousClass08S.A0J(str2, "_key");
        String A0J2 = AnonymousClass08S.A0J(str2, "_has_previous_page");
        String A0J3 = AnonymousClass08S.A0J(str2, "_has_next_page");
        String A0J4 = AnonymousClass08S.A0J(str2, "_is_loading_previous");
        String A0J5 = AnonymousClass08S.A0J(str2, "_is_loading_next");
        String A0J6 = AnonymousClass08S.A0J(str2, "_next_page_uuid");
        String A0J7 = AnonymousClass08S.A0J(str2, "_previous_page_uuid");
        TreeJNI[] treeArray = getTreeArray(str2, cls, i);
        String string = getString(A0J);
        if (string == null) {
            if (treeArray == null) {
                copyOf2 = RegularImmutableList.A02;
            } else {
                copyOf2 = ImmutableList.copyOf(treeArray);
            }
            return r8;
        }
        if (treeArray == null) {
            copyOf = RegularImmutableList.A02;
        } else {
            copyOf = ImmutableList.copyOf(treeArray);
        }
        r8 = new AnonymousClass7NQ(string, copyOf, getBooleanValue(A0J2), getBooleanValue(A0J3), getBooleanValue(A0J4), getBooleanValue(A0J5), getString(A0J6), getString(A0J7));
        return r8;
    }

    public final int getTypeTag() {
        return this.mTypeTag;
    }

    public TreeJNI(int i, int[] iArr) {
        this.mTypeTag = i;
        this.mSetFields = iArr;
        if (iArr != null) {
            Arrays.sort(iArr);
        }
    }

    public final Boolean getBoolean(String str) {
        if (isFieldUnset(str.hashCode())) {
            return null;
        }
        return getBooleanNative(str);
    }

    public final Double getDouble(String str) {
        if (isFieldUnset(str.hashCode())) {
            return null;
        }
        return getDoubleNative(str);
    }

    public final Integer getInt(String str) {
        if (isFieldUnset(str.hashCode())) {
            return null;
        }
        return getIntNative(str);
    }

    public final Long getTime(String str) {
        if (isFieldUnset(str.hashCode())) {
            return null;
        }
        return getTimeNative(str);
    }

    public final ImmutableList getBooleanList(int i) {
        if (isFieldUnset(i)) {
            return RegularImmutableList.A02;
        }
        return getBooleanListNative(i);
    }

    public final ImmutableList getBooleanList(String str) {
        if (isFieldUnset(str.hashCode())) {
            return RegularImmutableList.A02;
        }
        return getBooleanListNative(str);
    }

    public final boolean getBooleanValue(int i) {
        if (isFieldUnset(i)) {
            return false;
        }
        return getBooleanValueNative(i);
    }

    public final boolean getBooleanValue(String str) {
        if (isFieldUnset(str.hashCode())) {
            return false;
        }
        return getBooleanValueNative(str);
    }

    public final ImmutableList getDoubleList(int i) {
        if (isFieldUnset(i)) {
            return RegularImmutableList.A02;
        }
        return getDoubleListNative(i);
    }

    public final ImmutableList getDoubleList(String str) {
        if (isFieldUnset(str.hashCode())) {
            return RegularImmutableList.A02;
        }
        return getDoubleListNative(str);
    }

    public final double getDoubleValue(int i) {
        if (isFieldUnset(i)) {
            return 0.0d;
        }
        return getDoubleValueNative(i);
    }

    public final double getDoubleValue(String str) {
        if (isFieldUnset(str.hashCode())) {
            return 0.0d;
        }
        return getDoubleValueNative(str);
    }

    public final ImmutableList getIntList(int i) {
        if (isFieldUnset(i)) {
            return RegularImmutableList.A02;
        }
        return getIntListNative(i);
    }

    public final ImmutableList getIntList(String str) {
        if (isFieldUnset(str.hashCode())) {
            return RegularImmutableList.A02;
        }
        return getIntListNative(str);
    }

    public final int getIntValue(int i) {
        if (isFieldUnset(i)) {
            return 0;
        }
        return getIntValueNative(i);
    }

    public final int getIntValue(String str) {
        if (isFieldUnset(str.hashCode())) {
            return 0;
        }
        return getIntValueNative(str);
    }

    public final String getString(int i) {
        if (isFieldUnset(i)) {
            return null;
        }
        return getStringNative(i);
    }

    public final String getString(String str) {
        if (isFieldUnset(str.hashCode())) {
            return null;
        }
        return getStringNative(str);
    }

    public final ImmutableList getStringList(int i) {
        if (isFieldUnset(i)) {
            return RegularImmutableList.A02;
        }
        return getStringListNative(i);
    }

    public final ImmutableList getStringList(String str) {
        if (isFieldUnset(str.hashCode())) {
            return RegularImmutableList.A02;
        }
        return getStringListNative(str);
    }

    public final ImmutableList getTimeList(int i) {
        if (isFieldUnset(i)) {
            return RegularImmutableList.A02;
        }
        return getTimeListNative(i);
    }

    public final ImmutableList getTimeList(String str) {
        if (isFieldUnset(str.hashCode())) {
            return RegularImmutableList.A02;
        }
        return getTimeListNative(str);
    }

    public final long getTimeValue(int i) {
        if (isFieldUnset(i)) {
            return 0;
        }
        return getTimeValueNative(i);
    }

    public final long getTimeValue(String str) {
        if (isFieldUnset(str.hashCode())) {
            return 0;
        }
        return getTimeValueNative(str);
    }

    public /* bridge */ /* synthetic */ Tree getTree(String str) {
        return getTree(str, TreeJNI.class, 0);
    }

    public final TreeJNI getTree(int i, Class cls, int i2) {
        if (isFieldUnset(i)) {
            return null;
        }
        return getTreeNative(i, cls, i2);
    }

    public final TreeJNI getTree(String str, Class cls, int i) {
        if (isFieldUnset(str.hashCode())) {
            return null;
        }
        return getTreeNative(str, cls, i);
    }

    private TreeJNI[] getTreeArray(int i, Class cls, int i2) {
        if (isFieldUnset(i)) {
            return null;
        }
        return filterNullArrayEntries(getTreeArrayNative(i, cls, i2));
    }

    private TreeJNI[] getTreeArray(String str, Class cls, int i) {
        if (isFieldUnset(str.hashCode())) {
            return null;
        }
        return filterNullArrayEntries(getTreeArrayNative(str, cls, i));
    }

    public final ImmutableList getTreeList(int i, Class cls, int i2) {
        TreeJNI[] treeArray = getTreeArray(i, cls, i2);
        if (treeArray != null) {
            return ImmutableList.copyOf(treeArray);
        }
        return RegularImmutableList.A02;
    }

    public final ImmutableList getTreeList(String str, Class cls, int i) {
        TreeJNI[] treeArray = getTreeArray(str, cls, i);
        if (treeArray != null) {
            return ImmutableList.copyOf(treeArray);
        }
        return RegularImmutableList.A02;
    }

    public final TreeJNI reroot(String str) {
        return rerootNative(str, TreeJNI.class, 0);
    }

    public final TreeJNI reroot(String str, Class cls, int i) {
        return rerootNative(str, cls, i);
    }
}
