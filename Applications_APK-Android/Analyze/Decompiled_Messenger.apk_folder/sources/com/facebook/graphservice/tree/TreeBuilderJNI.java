package com.facebook.graphservice.tree;

import X.AnonymousClass01q;
import X.AnonymousClass08S;
import X.AnonymousClass11R;
import X.AnonymousClass7NQ;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.graphservice.interfaces.Tree;
import com.facebook.jni.HybridClassBase;
import com.google.common.base.Preconditions;

public class TreeBuilderJNI extends HybridClassBase implements AnonymousClass11R {
    public final int mTypeTag;

    private native TreeJNI getResultJNI(Class cls, int i);

    private native TreeJNI[] getTreeJNIListByAddingTreeToList(TreeJNI treeJNI, Class cls, int i, Iterable iterable);

    private native TreeBuilderJNI setTreeBuilderJNI(String str, String str2, String str3, TreeBuilderJNI treeBuilderJNI);

    private native TreeBuilderJNI setTreeBuilderJNIList(String str, String str2, String str3, Iterable iterable);

    /* access modifiers changed from: private */
    /* renamed from: setTreeJNI */
    public native TreeBuilderJNI setTree(int i, TreeJNI treeJNI);

    private native TreeBuilderJNI setTreeJNI(String str, TreeJNI treeJNI);

    /* access modifiers changed from: private */
    /* renamed from: setTreeJNIList */
    public native TreeBuilderJNI setTreeList(int i, Iterable iterable);

    private native TreeBuilderJNI setTreeJNIList(String str, Iterable iterable);

    public final native boolean hasPrimaryKey();

    public final native TreeBuilderJNI setBoolean(int i, Boolean bool);

    public final native TreeBuilderJNI setBoolean(String str, Boolean bool);

    public final native TreeBuilderJNI setBooleanList(int i, Iterable iterable);

    public final native TreeBuilderJNI setBooleanList(String str, Iterable iterable);

    public final native TreeBuilderJNI setDouble(int i, Double d);

    public final native TreeBuilderJNI setDouble(String str, Double d);

    public final native TreeBuilderJNI setDoubleList(int i, Iterable iterable);

    public final native TreeBuilderJNI setDoubleList(String str, Iterable iterable);

    public final native TreeBuilderJNI setInt(int i, Integer num);

    public final native TreeBuilderJNI setInt(String str, Integer num);

    public final native TreeBuilderJNI setIntList(int i, Iterable iterable);

    public final native TreeBuilderJNI setIntList(String str, Iterable iterable);

    public final native TreeBuilderJNI setNull(int i);

    public final native TreeBuilderJNI setNull(String str);

    public final native TreeBuilderJNI setString(int i, String str);

    public final native TreeBuilderJNI setString(String str, String str2);

    public final native TreeBuilderJNI setStringList(int i, Iterable iterable);

    public final native TreeBuilderJNI setStringList(String str, Iterable iterable);

    public final native TreeBuilderJNI setTime(int i, Long l);

    public final native TreeBuilderJNI setTime(String str, Long l);

    public final native TreeBuilderJNI setTimeList(int i, Iterable iterable);

    public final native TreeBuilderJNI setTimeList(String str, Iterable iterable);

    static {
        AnonymousClass01q.A08(TurboLoader.Locator.$const$string(79));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(int, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setBoolean(int, java.lang.Boolean):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setBoolean(java.lang.String, java.lang.Boolean):com.facebook.graphservice.tree.TreeBuilderJNI */
    public final TreeBuilderJNI setPaginableTreeList(String str, AnonymousClass7NQ r5) {
        String A0J = AnonymousClass08S.A0J(str, "_has_previous_page");
        return setBoolean(A0J, Boolean.valueOf(r5.A05)).setBoolean(AnonymousClass08S.A0J(str, "_has_next_page"), Boolean.valueOf(r5.A04)).setTreeJNIList(str, r5.A00);
    }

    public TreeBuilderJNI(int i) {
        this.mTypeTag = i;
    }

    public Tree getResult(Class cls, int i) {
        TreeJNI resultJNI = getResultJNI(cls, i);
        boolean z = false;
        if (resultJNI.getTypeTag() == i) {
            z = true;
        }
        Preconditions.checkState(z);
        return resultJNI;
    }

    public final TreeBuilderJNI setTree(String str, Tree tree) {
        return setTreeJNI(str, (TreeJNI) tree);
    }

    public final TreeBuilderJNI setTreeList(String str, Iterable iterable) {
        return setTreeJNIList(str, iterable);
    }
}
