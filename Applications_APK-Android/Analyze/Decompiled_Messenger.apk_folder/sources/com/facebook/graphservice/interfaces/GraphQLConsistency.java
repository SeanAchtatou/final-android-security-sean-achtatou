package com.facebook.graphservice.interfaces;

import X.AnonymousClass11R;
import com.google.common.util.concurrent.ListenableFuture;

public interface GraphQLConsistency extends GraphQLBaseConsistency {

    public class MutationPublisherRequest {
        public String mutationName;
    }

    ListenableFuture applyOptimistic(Tree tree, MutationPublisherRequest mutationPublisherRequest);

    ListenableFuture applyOptimisticBuilder(AnonymousClass11R r1, MutationPublisherRequest mutationPublisherRequest);

    ListenableFuture publish(Tree tree);

    ListenableFuture publishBuilder(AnonymousClass11R r1);

    ListenableFuture publishBuilderWithFullConsistency(AnonymousClass11R r1);

    ListenableFuture publishWithFullConsistency(Tree tree);
}
