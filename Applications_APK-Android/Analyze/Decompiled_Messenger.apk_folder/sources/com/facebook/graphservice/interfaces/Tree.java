package com.facebook.graphservice.interfaces;

import com.google.common.collect.ImmutableList;

public interface Tree {

    public enum FieldType {
        NONE,
        BOOL,
        DOUBLE,
        INT,
        TIME,
        STRING,
        DYNAMIC,
        BOOL_LIST,
        DOUBLE_LIST,
        INT_LIST,
        TIME_LIST,
        STRING_LIST,
        TREE,
        TREE_LIST
    }

    String getString(int i);

    ImmutableList getStringList(int i);

    Tree getTree(String str);

    String getTypeName();

    int getTypeTag();

    boolean isValid();

    String toExpensiveHumanReadableDebugString();
}
