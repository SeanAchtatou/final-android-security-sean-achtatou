package com.facebook.graphservice.interfaces;

import com.facebook.tigon.TigonErrorException;
import java.util.concurrent.Executor;

public interface GraphQLService {

    public interface Token {
        void cancel();
    }

    public interface DataCallbacks {
        void onError(TigonErrorException tigonErrorException, Summary summary);

        void onUpdate(Tree tree, Summary summary);
    }

    public interface OperationCallbacks {
        void onError(TigonErrorException tigonErrorException);

        void onSuccess();
    }

    Token handleQuery(GraphQLQuery graphQLQuery, DataCallbacks dataCallbacks, Executor executor);

    Token loadNextPageForKey(String str, int i, int i2, boolean z, OperationCallbacks operationCallbacks, Executor executor, String str2);
}
