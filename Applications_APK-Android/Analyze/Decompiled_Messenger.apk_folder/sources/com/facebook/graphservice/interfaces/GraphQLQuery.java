package com.facebook.graphservice.interfaces;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class GraphQLQuery {
    private final HybridData mHybridData;

    public native int cacheTtlSeconds();

    public native int freshCacheTtlSeconds();

    public native boolean hasAnalyticsHints();

    public native String queryName();

    public native boolean terminateAfterFreshResponse();

    static {
        AnonymousClass01q.A08("graphservice-jni");
    }

    private GraphQLQuery(HybridData hybridData) {
        this.mHybridData = hybridData;
    }
}
