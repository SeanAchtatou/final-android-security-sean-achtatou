package com.facebook.graphservice.interfaces;

public interface TreeJsonSerializer {
    Tree deserializeFromJson(String str, long j, Class cls, int i, String str2);
}
