package com.facebook.graphservice.nativeconfigloader;

import X.AnonymousClass01q;

public class GraphServiceNativeConfigLoader {
    public static native void loadNativeConfigs();

    static {
        AnonymousClass01q.A08("graphservice-jni-nativeconfigloader");
    }
}
