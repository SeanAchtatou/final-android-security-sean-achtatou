package com.facebook.graphservice;

import X.AnonymousClass01q;
import X.AnonymousClass0VW;
import X.C005505z;
import X.C010708t;
import X.C60272ws;
import com.facebook.compactdisk.current.CompactDiskManager;
import com.facebook.compactdisk.current.DiskCacheConfig;
import com.facebook.graphservice.asset.GraphServiceAsset;
import com.facebook.graphservice.interfaces.GraphQLQuery;
import com.facebook.graphservice.interfaces.GraphQLService;
import com.facebook.graphservice.live.GraphQLLiveConfig;
import com.facebook.jni.HybridData;
import com.facebook.livefeed.LiveFeedClientQEStore;
import com.facebook.livefeed.service.common.LiveFeedServiceFactory;
import com.facebook.reactivesocket.flipper.common.FlipperLiveDataProviderFactory;
import com.facebook.reactivesocket.livequery.common.LiveQueryServiceFactory;
import com.facebook.stash.core.Stash;
import com.facebook.tigon.iface.TigonServiceHolder;
import com.facebook.xanalytics.XAnalyticsHolder;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

public class GraphQLServiceJNI implements GraphQLService {
    private final HybridData mHybridData;

    private native GraphQLService.Token handleQueryJNI(GraphQLQuery graphQLQuery, GraphQLService.DataCallbacks dataCallbacks, Executor executor);

    private static native HybridData initHybridData(GraphServiceAsset graphServiceAsset, TigonServiceHolder tigonServiceHolder, ScheduledExecutorService scheduledExecutorService, ScheduledExecutorService scheduledExecutorService2, LiveQueryServiceFactory liveQueryServiceFactory, FlipperLiveDataProviderFactory flipperLiveDataProviderFactory, CompactDiskManager compactDiskManager, Stash stash, DiskCacheConfig diskCacheConfig, ExecutorService executorService, GraphQLConsistencyJNI graphQLConsistencyJNI, GraphQLLiveConfig graphQLLiveConfig, LiveFeedClientQEStore liveFeedClientQEStore, LiveFeedServiceFactory liveFeedServiceFactory, XAnalyticsHolder xAnalyticsHolder, boolean z, String str, String str2, String str3, int i, int i2, boolean z2, String str4, boolean z3, boolean z4);

    public native GraphQLService.Token experimentalResetForKey(String str, boolean z, GraphQLService.OperationCallbacks operationCallbacks, Executor executor);

    public native GraphQLService.Token loadNextPageForKey(String str, int i, int i2, boolean z, GraphQLService.OperationCallbacks operationCallbacks, Executor executor, String str2);

    public native GraphQLService.Token loadPreviousPageForKey(String str, int i, GraphQLService.OperationCallbacks operationCallbacks, Executor executor, String str2);

    static {
        AnonymousClass01q.A08("graphservice-jni");
    }

    public static void warnIfTestEnvironment() {
        if (AnonymousClass0VW.A00()) {
            C010708t.A0R("GraphServices_JNI", new UnsupportedOperationException("GraphServices_JNI"), "Calling GraphServices in a test.");
        }
    }

    public GraphQLService.Token handleQuery(GraphQLQuery graphQLQuery, GraphQLService.DataCallbacks dataCallbacks, Executor executor) {
        warnIfTestEnvironment();
        C005505z.A05("GS.handleQuery(%s)", graphQLQuery.queryName(), -741714079);
        try {
            return handleQueryJNI(graphQLQuery, new C60272ws(graphQLQuery, dataCallbacks), executor);
        } finally {
            C005505z.A00(-1854037400);
        }
    }

    public GraphQLServiceJNI(GraphServiceAsset graphServiceAsset, TigonServiceHolder tigonServiceHolder, ScheduledExecutorService scheduledExecutorService, ScheduledExecutorService scheduledExecutorService2, LiveQueryServiceFactory liveQueryServiceFactory, FlipperLiveDataProviderFactory flipperLiveDataProviderFactory, CompactDiskManager compactDiskManager, Stash stash, DiskCacheConfig diskCacheConfig, ExecutorService executorService, GraphQLConsistencyJNI graphQLConsistencyJNI, GraphQLLiveConfig graphQLLiveConfig, LiveFeedClientQEStore liveFeedClientQEStore, LiveFeedServiceFactory liveFeedServiceFactory, XAnalyticsHolder xAnalyticsHolder, boolean z, String str, String str2, String str3, int i, int i2, boolean z2, String str4, boolean z3, boolean z4) {
        LiveFeedClientQEStore liveFeedClientQEStore2 = liveFeedClientQEStore;
        GraphQLLiveConfig graphQLLiveConfig2 = graphQLLiveConfig;
        GraphQLConsistencyJNI graphQLConsistencyJNI2 = graphQLConsistencyJNI;
        ExecutorService executorService2 = executorService;
        DiskCacheConfig diskCacheConfig2 = diskCacheConfig;
        Stash stash2 = stash;
        CompactDiskManager compactDiskManager2 = compactDiskManager;
        FlipperLiveDataProviderFactory flipperLiveDataProviderFactory2 = flipperLiveDataProviderFactory;
        ScheduledExecutorService scheduledExecutorService3 = scheduledExecutorService;
        ScheduledExecutorService scheduledExecutorService4 = scheduledExecutorService2;
        LiveQueryServiceFactory liveQueryServiceFactory2 = liveQueryServiceFactory;
        String str5 = str3;
        TigonServiceHolder tigonServiceHolder2 = tigonServiceHolder;
        String str6 = str2;
        GraphServiceAsset graphServiceAsset2 = graphServiceAsset;
        String str7 = str;
        boolean z5 = z;
        this.mHybridData = initHybridData(graphServiceAsset2, tigonServiceHolder2, scheduledExecutorService3, scheduledExecutorService4, liveQueryServiceFactory2, flipperLiveDataProviderFactory2, compactDiskManager2, stash2, diskCacheConfig2, executorService2, graphQLConsistencyJNI2, graphQLLiveConfig2, liveFeedClientQEStore2, liveFeedServiceFactory, xAnalyticsHolder, z5, str7, str6, str5, i, i2, z2, str4, z3, z4);
    }
}
