package com.facebook.graphservice;

import X.AnonymousClass08S;
import X.AnonymousClass8t6;
import X.AnonymousClass8t7;
import X.C25231Yv;
import com.facebook.graphservice.interfaces.GraphQLQuery;
import com.facebook.graphservice.interfaces.GraphQLService;
import com.google.common.base.Preconditions;
import java.util.concurrent.Executor;

public class GraphQLServiceDecorator implements GraphQLService {
    private final GraphQLService A00;

    public GraphQLService.Token loadNextPageForKey(String str, int i, int i2, boolean z, GraphQLService.OperationCallbacks operationCallbacks, Executor executor, String str2) {
        AnonymousClass8t7 r7 = operationCallbacks;
        String str3 = str;
        if (C25231Yv.A01()) {
            r7 = new AnonymousClass8t7(operationCallbacks, AnonymousClass08S.A0J("GraphQL_loadNextPageForKeyWithUUID_", str));
        }
        return this.A00.loadNextPageForKey(str3, i, i2, z, r7, executor, str2);
    }

    public GraphQLServiceDecorator(GraphQLService graphQLService) {
        Preconditions.checkNotNull(graphQLService, "Got null GraphQLService in GraphQLServiceDecorator");
        this.A00 = graphQLService;
    }

    public GraphQLService.Token handleQuery(GraphQLQuery graphQLQuery, GraphQLService.DataCallbacks dataCallbacks, Executor executor) {
        String queryName = graphQLQuery.queryName();
        if (C25231Yv.A01()) {
            dataCallbacks = new AnonymousClass8t6(dataCallbacks, AnonymousClass08S.A0J("GraphQL_handleQuery_", queryName));
        }
        return this.A00.handleQuery(graphQLQuery, dataCallbacks, executor);
    }
}
