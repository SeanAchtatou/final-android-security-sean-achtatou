package com.facebook.graphservice.asset;

import X.AnonymousClass01q;
import com.facebook.graphservice.interfaces.FragmentSpace;
import com.facebook.graphservice.interfaces.GraphSchema;
import com.facebook.graphservice.nativeconfigloader.GraphServiceNativeConfigLoader;
import com.facebook.graphservice.tree.TreeShapeJNI;
import com.facebook.jni.HybridData;

public class GraphServiceAsset implements FragmentSpace, GraphSchema {
    private final HybridData mHybridData;

    private native TreeShapeJNI.ResolverJNI getFlatbufferTreeShapeResolver(String str, long j);

    private static native HybridData initHybridData(String str);

    static {
        AnonymousClass01q.A08("graphservice-jni-asset");
    }

    public GraphServiceAsset(String str) {
        this.mHybridData = initHybridData(str);
        GraphServiceNativeConfigLoader.loadNativeConfigs();
    }
}
