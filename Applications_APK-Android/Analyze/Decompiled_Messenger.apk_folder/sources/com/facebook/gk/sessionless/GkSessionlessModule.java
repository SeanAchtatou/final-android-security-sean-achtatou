package com.facebook.gk.sessionless;

import X.AnonymousClass062;
import X.AnonymousClass064;
import X.AnonymousClass0UN;
import X.AnonymousClass0UV;
import X.AnonymousClass0WB;
import X.AnonymousClass0WC;
import X.AnonymousClass0WD;
import X.AnonymousClass0WG;
import X.AnonymousClass0WH;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YA;
import X.AnonymousClass1YI;
import X.C04850Wk;
import android.content.Context;
import com.facebook.gk.store.GatekeeperWriter;
import com.facebook.inject.InjectorModule;

@InjectorModule
public class GkSessionlessModule extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static final Object A02 = new Object();
    private static volatile AnonymousClass0WC A03;
    private static volatile AnonymousClass0WB A04;
    private static volatile AnonymousClass0WG A05;

    public class GkSessionlessModuleSelendroidInjector implements AnonymousClass062 {
        public AnonymousClass0UN A00;

        public GatekeeperWriter getGatekeeperWriter() {
            return (GatekeeperWriter) AnonymousClass1XX.A03(AnonymousClass1Y3.AoE, this.A00);
        }

        public GkSessionlessModuleSelendroidInjector(Context context) {
            this.A00 = new AnonymousClass0UN(0, AnonymousClass1XX.get(context));
        }
    }

    public static final AnonymousClass0WC A02(AnonymousClass1XY r3) {
        if (A03 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A03 = new C04850Wk();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final AnonymousClass0WB A03(AnonymousClass1XY r10) {
        if (A04 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r10);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r10.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnonymousClass0WC A022 = A02(applicationInjector);
                        AnonymousClass0WG A052 = A05(applicationInjector);
                        Context applicationContext = A003.getApplicationContext();
                        boolean z = false;
                        if (A022 != null) {
                            z = true;
                        }
                        AnonymousClass064.A04(z);
                        A04 = new AnonymousClass0WB(A022, new AnonymousClass0WH(A022, applicationContext.getDir("sessionless_gatekeepers", 0)), null, A052, null);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final AnonymousClass0WG A05(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (A02) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        r4.getApplicationInjector();
                        A05 = new AnonymousClass0WG("SessionlessGatekeeperStore");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final AnonymousClass1YI A00(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public static final AnonymousClass1YI A01(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public static final GatekeeperWriter A04(AnonymousClass1XY r0) {
        return A03(r0);
    }
}
