package com.facebook.platform.common.provider;

import X.AnonymousClass08S;
import X.AnonymousClass0TE;
import X.AnonymousClass1XX;
import X.AnonymousClass4M0;
import X.C04490Ux;
import android.content.UriMatcher;

public class PlatformProviderBase extends AnonymousClass0TE {
    public static String A00;
    public static final UriMatcher A01 = new UriMatcher(-1);

    public void A0G() {
        super.A0G();
        AnonymousClass1XX r0 = AnonymousClass1XX.get(getContext());
        AnonymousClass4M0.A00(r0);
        String A0t = C04490Ux.A0t(r0);
        A00 = A0t;
        A01.addURI(AnonymousClass08S.A0J(A0t, ".provider.PlatformProvider"), "versions", 1);
    }
}
