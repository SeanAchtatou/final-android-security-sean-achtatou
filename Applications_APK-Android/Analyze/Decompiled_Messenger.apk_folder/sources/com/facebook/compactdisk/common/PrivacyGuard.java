package com.facebook.compactdisk.common;

import X.C30471i6;
import X.C30511iA;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import java.util.UUID;

public class PrivacyGuard implements C30471i6 {
    private SharedPreferences A00;
    private String A01;

    public synchronized String getUUID() {
        if (this.A01 == null) {
            String string = this.A00.getString("UUID", null);
            this.A01 = string;
            if (string == null) {
                StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                try {
                    UUID randomUUID = UUID.randomUUID();
                    StrictMode.setThreadPolicy(allowThreadDiskReads);
                    this.A01 = randomUUID.toString();
                    this.A00.edit().putString("UUID", this.A01).apply();
                } catch (Throwable th) {
                    StrictMode.setThreadPolicy(allowThreadDiskReads);
                    throw th;
                }
            }
        }
        return this.A01;
    }

    public synchronized void invalidate() {
        this.A01 = null;
        this.A00.edit().remove("UUID").apply();
    }

    public PrivacyGuard(Context context, C30511iA r4) {
        synchronized (r4) {
            r4.A01.add(this);
        }
        this.A00 = context.getSharedPreferences("CompactDisk", 0);
    }
}
