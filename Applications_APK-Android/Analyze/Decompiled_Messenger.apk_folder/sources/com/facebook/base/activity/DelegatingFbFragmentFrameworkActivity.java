package com.facebook.base.activity;

import X.C000700l;
import X.C12980qK;
import X.C13060qW;
import X.C13080qc;
import X.C13090qd;
import X.C27231cr;
import X.C32281lV;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import androidx.fragment.app.Fragment;

public class DelegatingFbFragmentFrameworkActivity extends FbFragmentActivity {
    public C13080qc A00;

    public void A10() {
        this.A00.BmS();
    }

    public void A11(Fragment fragment) {
        this.A00.BOX(fragment);
    }

    public View A14(int i) {
        return this.A00.B8z(i);
    }

    public Object A16(Class cls) {
        return this.A00.AqZ(cls);
    }

    public void A17() {
        this.A00.onActivityDestroy();
    }

    public void A19(Intent intent) {
        this.A00.BNF(intent);
    }

    public void A1B(Bundle bundle) {
        this.A00.BNB(bundle);
    }

    public void A1C(Bundle bundle) {
        this.A00.BPZ(bundle);
    }

    public Object Aze(Object obj) {
        return this.A00.Aze(obj);
    }

    public C13060qW B4m() {
        return this.A00.B4m();
    }

    public boolean BB8(Throwable th) {
        return this.A00.BB8(th);
    }

    public void C0V(C27231cr r2) {
        this.A00.C0V(r2);
    }

    public void CB0(Object obj, Object obj2) {
        this.A00.CB0(obj, obj2);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.A00.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return this.A00.dispatchTouchEvent(motionEvent);
    }

    public void finish() {
        this.A00.AZu();
    }

    public void finishFromChild(Activity activity) {
        this.A00.AZx(activity);
    }

    public Intent getIntent() {
        return this.A00.getIntent();
    }

    public MenuInflater getMenuInflater() {
        return this.A00.Atv();
    }

    public Resources getResources() {
        return this.A00.B1A();
    }

    public Window getWindow() {
        return this.A00.B9w();
    }

    public boolean hasWindowFocus() {
        return this.A00.BBy();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        this.A00.BNH(i, i2, intent);
    }

    public void onAttachedToWindow() {
        this.A00.onAttachedToWindow();
    }

    public void onBackPressed() {
        this.A00.onBackPressed();
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.A00.onConfigurationChanged(configuration);
    }

    public void onContentChanged() {
        this.A00.onContentChanged();
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        return this.A00.BUO(menuItem);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.A00.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public Dialog onCreateDialog(int i) {
        return this.A00.BUk(i);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return this.A00.BUt(menu);
    }

    public View onCreatePanelView(int i) {
        return this.A00.onCreatePanelView(i);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return this.A00.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return this.A00.onKeyUp(i, keyEvent);
    }

    public void onLowMemory() {
        this.A00.onLowMemory();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return this.A00.BhL(menuItem);
    }

    public void onPostCreate(Bundle bundle) {
        this.A00.BjD(bundle);
    }

    public void onPostResume() {
        this.A00.BjI();
    }

    public void onPrepareDialog(int i, Dialog dialog) {
        this.A00.BjR(i, dialog);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return this.A00.BjY(menu);
    }

    public void onSaveInstanceState(Bundle bundle) {
        this.A00.Bmx(bundle);
    }

    public boolean onSearchRequested() {
        return this.A00.onSearchRequested();
    }

    public void onTrimMemory(int i) {
        this.A00.onTrimMemory(i);
    }

    public void onUserInteraction() {
        this.A00.Btu();
    }

    public void onWindowFocusChanged(boolean z) {
        this.A00.onWindowFocusChanged(z);
    }

    public void setContentView(int i) {
        this.A00.C7B(i);
    }

    public void setIntent(Intent intent) {
        this.A00.C8U(intent);
    }

    public void setRequestedOrientation(int i) {
        this.A00.CBQ(i);
    }

    public void startActivity(Intent intent) {
        this.A00.CGn(intent);
    }

    public void startActivityForResult(Intent intent, int i) {
        this.A00.startActivityForResult(intent, i);
    }

    public DelegatingFbFragmentFrameworkActivity(C12980qK r2) {
        C32281lV r0 = new C32281lV(this);
        r2.A00 = this;
        r2.A01 = r0;
        this.A00 = new C13090qd(r2);
    }

    public void onPause() {
        int A002 = C000700l.A00(-40861928);
        this.A00.onPause();
        C000700l.A07(-779747833, A002);
    }

    public void onResume() {
        int A002 = C000700l.A00(279891343);
        this.A00.onResume();
        C000700l.A07(538970676, A002);
    }

    public void onStart() {
        int A002 = C000700l.A00(-531876491);
        this.A00.onStart();
        C000700l.A07(-1137686555, A002);
    }

    public void onStop() {
        int A002 = C000700l.A00(-565756428);
        this.A00.onStop();
        C000700l.A07(-680968360, A002);
    }
}
