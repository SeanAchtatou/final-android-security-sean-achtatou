package com.facebook.base.activity;

import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass09P;
import X.AnonymousClass0UN;
import X.AnonymousClass0UQ;
import X.AnonymousClass0US;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y7;
import X.AnonymousClass1YI;
import X.AnonymousClass1YL;
import X.AnonymousClass20J;
import X.AnonymousClass26J;
import X.AnonymousClass80H;
import X.C000700l;
import X.C001500z;
import X.C005505z;
import X.C006006f;
import X.C007707a;
import X.C008707s;
import X.C010708t;
import X.C012809p;
import X.C04210Sx;
import X.C04220Sy;
import X.C04230Sz;
import X.C04500Uy;
import X.C05260Yg;
import X.C05760aH;
import X.C06850cB;
import X.C07790eA;
import X.C11450nB;
import X.C11460nC;
import X.C11470nD;
import X.C11480nE;
import X.C11490nF;
import X.C11510nI;
import X.C11560nN;
import X.C11590nQ;
import X.C11600nR;
import X.C11610nS;
import X.C11620nT;
import X.C11650nY;
import X.C11660nZ;
import X.C11670nb;
import X.C11680nc;
import X.C12330pA;
import X.C12460pP;
import X.C12960qH;
import X.C12970qJ;
import X.C13060qW;
import X.C13180qr;
import X.C13200qt;
import X.C16000wK;
import X.C187614z;
import X.C25051Yd;
import X.C25141Ym;
import X.C27231cr;
import X.C27241cs;
import X.C28591f3;
import X.C30281hn;
import X.C50022dF;
import X.C73203fg;
import X.C73213fh;
import X.C73223fi;
import X.C99084oO;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ParseException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.runtimepermissions.RequestPermissionsConfig;
import com.facebook.runtimepermissions.RuntimePermissionsNeverAskAgainDialogFragment;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FbFragmentActivity extends FragmentActivity implements C11450nB, C11460nC, C11470nD, C04500Uy, C11480nE, C27241cs, C11490nF {
    public static final Class A0C = FbFragmentActivity.class;
    public C11610nS A00;
    public AnonymousClass0UN A01;
    public AnonymousClass0US A02;
    public C04220Sy A03;
    private String A04 = BuildConfig.FLAVOR;
    private boolean A05;
    private boolean A06;
    private boolean A07 = false;
    private boolean A08;
    private final C11560nN A09 = new C11560nN();
    private final C007707a A0A = new C007707a();
    private final HashSet A0B = new HashSet();

    public void A17() {
    }

    public void A18(Context context) {
    }

    public void A19(Intent intent) {
        this.A08 = true;
    }

    public void A1A(Bundle bundle) {
    }

    public void A1B(Bundle bundle) {
    }

    public void A1C(Bundle bundle) {
    }

    public void A12(Fragment fragment, Intent intent, int i, Bundle bundle) {
        ((AnonymousClass26J) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Awr, this.A01)).A01(this, intent);
        super.A12(fragment, intent, i, bundle);
    }

    public Uri A13(Intent intent) {
        if (Build.VERSION.SDK_INT >= 22) {
            return getReferrer();
        }
        Uri uri = (Uri) intent.getParcelableExtra("android.intent.extra.REFERRER");
        if (uri != null) {
            return uri;
        }
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        if (stringExtra == null) {
            return null;
        }
        try {
            return Uri.parse(stringExtra);
        } catch (ParseException unused) {
            return null;
        }
    }

    public C11610nS A15(boolean z) {
        if (this.A00 == null) {
            if (z) {
                this.A00 = new C11600nR(this, getWindow(), null);
            } else {
                throw new IllegalStateException("getAppCompatDelete() was called without first calling createAppCompatDelegate()");
            }
        }
        return this.A00;
    }

    public void AMa(C13180qr r4) {
        C11590nQ r1 = (C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01);
        synchronized (r1) {
            C11590nQ.A05(r1, r4);
        }
    }

    public Object Aze(Object obj) {
        return this.A09.A00(obj);
    }

    public boolean BB8(Throwable th) {
        return ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0Z(th);
    }

    public void C0V(C27231cr r4) {
        ((C28591f3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AIO, this.A01)).A02(r4);
    }

    public void C1L(C13180qr r4) {
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0Q(r4);
    }

    public void CB0(Object obj, Object obj2) {
        this.A09.A01(obj, obj2);
    }

    public Resources getResources() {
        return (C05760aH) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AZY, this.A01);
    }

    public void invalidateOptionsMenu() {
        C11610nS r0 = this.A00;
        if (r0 != null) {
            r0.A0B();
        } else {
            super.invalidateOptionsMenu();
        }
    }

    public Dialog onCreateDialog(int i) {
        Dialog A062 = ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A06(i);
        if (A062 == null) {
            return super.onCreateDialog(i);
        }
        return A062;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0O(menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        Optional A082 = ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A08(i, keyEvent);
        if (A082.isPresent()) {
            return ((Boolean) A082.get()).booleanValue();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        Optional A092 = ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A09(i, keyEvent);
        if (A092.isPresent()) {
            return ((Boolean) A092.get()).booleanValue();
        }
        return super.onKeyUp(i, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0Y(menuItem) || super.onOptionsItemSelected(menuItem)) {
            return true;
        }
        return false;
    }

    public void onPrepareDialog(int i, Dialog dialog) {
        if (!((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0V(i, dialog)) {
            super.onPrepareDialog(i, dialog);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0P(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onSearchRequested() {
        Optional A072 = ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A07();
        if (A072.isPresent()) {
            return ((Boolean) A072.get()).booleanValue();
        }
        return super.onSearchRequested();
    }

    private void A0x() {
        Resources resources = super.getResources();
        ((C05760aH) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AZY, this.A01)).A09(resources.getConfiguration(), resources.getDisplayMetrics());
    }

    public void A10() {
        super.A10();
        C11590nQ r3 = (C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01);
        C11590nQ.A03(r3);
        C005505z.A03("FbActivityListeners.onResumeFragments", 27138216);
        try {
            for (C13200qt BmT : r3.A06) {
                BmT.BmT(r3.A01);
            }
            C11590nQ.A02(r3);
        } finally {
            C005505z.A00(-1738320561);
            C11590nQ.A04(r3);
        }
    }

    public void A11(Fragment fragment) {
        super.A11(fragment);
        this.A0B.add(fragment.getClass().getName());
        C11590nQ r3 = (C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01);
        C11590nQ.A03(r3);
        C005505z.A03("FbActivityListeners.onAttachFragment", 934559197);
        try {
            for (C13200qt BOW : r3.A06) {
                BOW.BOW(r3.A01, fragment);
            }
            C11590nQ.A02(r3);
        } finally {
            C005505z.A00(1741363174);
            C11590nQ.A04(r3);
        }
    }

    public View A14(int i) {
        return C012809p.A00(this, i);
    }

    public Object A16(Class cls) {
        if (cls.isInstance(this)) {
            return this;
        }
        return null;
    }

    public Object BzI(Class cls) {
        return A16(cls);
    }

    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        A18(context);
        boolean A002 = C008707s.A00();
        if (A002) {
            C005505z.A05("%s.attachBaseContext()", getClass().getSimpleName(), -607161390);
        }
        try {
            AnonymousClass1XX r2 = AnonymousClass1XX.get(this);
            this.A01 = new AnonymousClass0UN(16, r2);
            AnonymousClass0UQ.A00(AnonymousClass1Y3.AtK, r2);
            this.A02 = AnonymousClass0UQ.A00(AnonymousClass1Y3.B3a, r2);
            A0x();
        } finally {
            if (A002) {
                C005505z.A00(853050562);
            }
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        boolean A002 = C008707s.A00();
        if (A002) {
            C005505z.A03("FbFragmentActivity.dispatchTouchEvent", -952809832);
        }
        if (A002) {
            try {
                C005505z.A03("FbActivityListeners.onTouchEvent", -1524505008);
            } catch (Throwable th) {
                if (A002) {
                    C005505z.A00(1433356246);
                }
                throw th;
            }
        }
        for (C50022dF BsP : (Set) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A5q, this.A01)) {
            BsP.BsP(this, motionEvent);
        }
        if (A002) {
            C005505z.A00(-1902058590);
        }
        boolean dispatchTouchEvent = super.dispatchTouchEvent(motionEvent);
        if (A002) {
            C005505z.A00(-602776854);
        }
        return dispatchTouchEvent;
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (!C006006f.A01() || strArr == null || strArr.length == 0 || !"e2e".equals(strArr[0])) {
            super.dump(str, fileDescriptor, printWriter, strArr);
            return;
        }
        C007707a r1 = this.A0A;
        printWriter.print(str);
        printWriter.println("Top Level Window View Hierarchy:");
        C007707a.A01(AnonymousClass08S.A0J(str, "  "), printWriter, C007707a.A00(r1), 0, 0);
    }

    public void finish() {
        super.finish();
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0A();
        C11650nY r2 = (C11650nY) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AS7, this.A01);
        if (r2.A00 == C001500z.A03) {
            C07790eA r7 = r2.A01;
            synchronized (r7) {
                if (r7.A0A.containsKey(Integer.valueOf(hashCode()))) {
                    r7.A08.remove((C12330pA) r7.A0A.get(Integer.valueOf(hashCode())));
                    r7.A0A.remove(Integer.valueOf(hashCode()));
                }
                List A072 = r7.A07();
                if (!r7.A03.equals(A072)) {
                    AnonymousClass1YL r0 = r7.A07;
                    ImmutableList immutableList = C07790eA.A0B;
                    if (r0.A05(immutableList)) {
                        r7.A07.A04(immutableList, new C11620nT(r7.A07(), AnonymousClass07B.A01, C07790eA.A01(r7)), C25141Ym.INSTANCE);
                        r7.A03 = A072;
                    }
                }
            }
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0K(i, i2, intent);
    }

    public void onApplyThemeResource(Resources.Theme theme, int i, boolean z) {
        super.onApplyThemeResource(theme, i, z);
        boolean z2 = false;
        Resources[] resourcesArr = {getApplicationContext().getResources(), super.getResources(), getResources()};
        ((C11660nZ) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AcV, this.A01)).A01(theme);
        int i2 = AnonymousClass1Y3.AcV;
        this.A06 = ((C11660nZ) AnonymousClass1XX.A02(5, i2, this.A01)).A03();
        C11660nZ r1 = (C11660nZ) AnonymousClass1XX.A02(5, i2, this.A01);
        boolean z3 = r1.A00;
        this.A05 = z3;
        if (z3 || (r1.A02() && ((AnonymousClass1YI) AnonymousClass1XX.A02(12, AnonymousClass1Y3.AcD, this.A01)).AbO(802, false))) {
            z2 = true;
        }
        if (z2) {
            int i3 = 2132476124;
            if (this.A06) {
                i3 = 2132476126;
            }
            theme.applyStyle(i3, true);
        }
        C11590nQ r3 = (C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01);
        C11590nQ.A03(r3);
        C005505z.A03("FbActivityListeners.onApplyThemeResource", -1225551862);
        try {
            for (C13200qt BON : r3.A06) {
                BON.BON(this, theme, i, z);
            }
            C11590nQ.A02(r3);
        } finally {
            C005505z.A00(565523551);
            C11590nQ.A04(r3);
        }
    }

    public void onBackPressed() {
        C13060qW B4m = B4m();
        if (C16000wK.A01(B4m)) {
            if (!((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0U()) {
                super.onBackPressed();
            }
            String $const$string = C99084oO.$const$string(45);
            String $const$string2 = AnonymousClass80H.$const$string(128);
            C11670nb r3 = new C11670nb("click");
            r3.A0D("pigeon_reserved_keyword_obj_id", $const$string);
            r3.A0D("pigeon_reserved_keyword_obj_type", $const$string2);
            ((DeprecatedAnalyticsLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AR0, ((C11680nc) AnonymousClass1XX.A03(AnonymousClass1Y3.AMC, this.A01)).A00)).A09(r3);
            B4m.A0U();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        A0x();
        super.onConfigurationChanged(configuration);
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0M(configuration);
        C11610nS r0 = this.A00;
        if (r0 != null) {
            r0.A0I(configuration);
        }
    }

    public void onContentChanged() {
        super.onContentChanged();
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0C();
    }

    /* JADX WARN: Type inference failed for: r9v0, types: [X.07L, android.os.Bundle, X.3fd] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final void onCreate(android.os.Bundle r24) {
        /*
            r23 = this;
            r0 = -185146432(0xfffffffff4f6e3c0, float:-1.5648491E32)
            int r8 = X.C000700l.A00(r0)
            int r2 = X.AnonymousClass1Y3.ALy
            r5 = r23
            X.0UN r1 = r5.A01
            r0 = 8
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0UW r0 = (X.AnonymousClass0UW) r0
            r0.A03()
            boolean r19 = X.C008707s.A00()
            if (r19 == 0) goto L_0x002e
            java.lang.Class r0 = r5.getClass()
            java.lang.String r2 = r0.getSimpleName()
            r1 = 656412251(0x27200e5b, float:2.2212243E-15)
            java.lang.String r0 = "%s.onCreate"
            X.C005505z.A05(r0, r2, r1)
        L_0x002e:
            r4 = r24
            if (r24 == 0) goto L_0x003b
            java.lang.Class<com.facebook.base.activity.FbFragmentActivity> r0 = com.facebook.base.activity.FbFragmentActivity.class
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ all -> 0x0591 }
            r4.setClassLoader(r0)     // Catch:{ all -> 0x0591 }
        L_0x003b:
            r9 = 0
            if (r9 == 0) goto L_0x0041
            r9.onActivityCreated(r5, r9)     // Catch:{ all -> 0x0591 }
        L_0x0041:
            int r22 = X.AnonymousClass1Y3.AHa     // Catch:{ all -> 0x0591 }
            X.0UN r1 = r5.A01     // Catch:{ all -> 0x0591 }
            r10 = 7
            r0 = r22
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r10, r0, r1)     // Catch:{ all -> 0x0591 }
            X.0nQ r0 = (X.C11590nQ) r0     // Catch:{ all -> 0x0591 }
            r0.A01 = r5     // Catch:{ all -> 0x0591 }
            boolean r3 = r0.A0X(r4)     // Catch:{ all -> 0x0591 }
            if (r24 == 0) goto L_0x008b
            java.lang.String r0 = "attached_fragment_names"
            java.util.ArrayList r1 = r4.getStringArrayList(r0)     // Catch:{ all -> 0x0591 }
            if (r1 == 0) goto L_0x0063
            java.util.HashSet r0 = r5.A0B     // Catch:{ all -> 0x0591 }
            r0.addAll(r1)     // Catch:{ all -> 0x0591 }
        L_0x0063:
            java.util.HashSet r0 = r5.A0B     // Catch:{ all -> 0x0591 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0591 }
            if (r0 != 0) goto L_0x008b
            int r1 = X.AnonymousClass1Y3.A7B     // Catch:{ all -> 0x0591 }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x0591 }
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0591 }
            X.0Ek r2 = (X.C02370Ek) r2     // Catch:{ all -> 0x0591 }
            java.util.HashSet r0 = r5.A0B     // Catch:{ all -> 0x0591 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0591 }
        L_0x007b:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0591 }
            if (r0 == 0) goto L_0x008b
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0591 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0591 }
            r2.A00(r0)     // Catch:{ all -> 0x0591 }
            goto L_0x007b
        L_0x008b:
            r5.A1A(r4)     // Catch:{ all -> 0x0591 }
            super.onCreate(r4)     // Catch:{ all -> 0x0591 }
            r7 = 1
            if (r3 == 0) goto L_0x00a5
            r5.A07 = r7     // Catch:{ all -> 0x0591 }
            if (r19 == 0) goto L_0x009e
            r0 = 543874978(0x206adfa2, float:1.9894565E-19)
            X.C005505z.A00(r0)
        L_0x009e:
            r0 = 165356469(0x9db23b5, float:5.275591E-33)
            X.C000700l.A07(r0, r8)
            return
        L_0x00a5:
            int r1 = X.AnonymousClass1Y3.B4k     // Catch:{ all -> 0x0591 }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x0591 }
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0591 }
            X.1dP r2 = (X.C27571dP) r2     // Catch:{ all -> 0x0591 }
            boolean r0 = r2.A01     // Catch:{ all -> 0x0591 }
            if (r0 != 0) goto L_0x0180
            r2.A01 = r7     // Catch:{ all -> 0x0591 }
            int r1 = X.AnonymousClass1Y3.ANx     // Catch:{ all -> 0x0591 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0591 }
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0591 }
            X.1cZ r1 = (X.C27051cZ) r1     // Catch:{ all -> 0x0591 }
            X.0gK r0 = new X.0gK     // Catch:{ all -> 0x0591 }
            r0.<init>()     // Catch:{ all -> 0x0591 }
            X.C27041cY.A00 = r0     // Catch:{ all -> 0x0591 }
            X.C09070gU.A00 = r1     // Catch:{ all -> 0x0591 }
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0591 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0591 }
            java.lang.Object r6 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0591 }
            X.1YI r6 = (X.AnonymousClass1YI) r6     // Catch:{ all -> 0x0591 }
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x0591 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0591 }
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0591 }
            X.1Yd r3 = (X.C25051Yd) r3     // Catch:{ all -> 0x0591 }
            r0 = 285495066105550(0x103a8000016ce, double:1.41053304219928E-309)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.onlyProcessAutogeneratedTransitionIdsWhenNecessary = r0     // Catch:{ all -> 0x0591 }
            r0 = 665(0x299, float:9.32E-43)
            r2 = 0
            boolean r0 = r6.AbO(r0, r2)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.incrementalMountWhenNotVisible = r0     // Catch:{ all -> 0x0591 }
            r0 = 284730561925795(0x102f6000012a3, double:1.40675588968609E-309)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.isReconciliationEnabled = r0     // Catch:{ all -> 0x0591 }
            r0 = 284730562319017(0x102f6000612a9, double:1.406755891628864E-309)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.shouldUseDeepCloneDuringReconciliation = r0     // Catch:{ all -> 0x0591 }
            r0 = 284730562384554(0x102f6000712aa, double:1.40675589195266E-309)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.useInternalNodesForLayoutDiffing = r0     // Catch:{ all -> 0x0591 }
            r0 = 123(0x7b, float:1.72E-43)
            boolean r0 = r6.AbO(r0, r2)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.enableThreadTracingStacktrace = r0     // Catch:{ all -> 0x0591 }
            r0 = 282119222920323(0x1009600110483, double:1.3938541607636E-309)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.useCancelableLayoutFutures = r0     // Catch:{ all -> 0x0591 }
            r0 = 664(0x298, float:9.3E-43)
            boolean r0 = r6.AbO(r0, r2)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.enableOnErrorHandling = r0     // Catch:{ all -> 0x0591 }
            r0 = 2306129062625548358(0x2001042a00001846, double:1.5864138603113508E-154)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.disableComponentHostPool = r0     // Catch:{ all -> 0x0591 }
            r0 = 124(0x7c, float:1.74E-43)
            boolean r0 = r6.AbO(r0, r2)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.skipVisChecksForFullyVisible = r0     // Catch:{ all -> 0x0591 }
            r0 = 563130342965463(0x2002a000e00d7, double:2.78223356590034E-309)
            r11 = 5
            int r0 = r3.AqL(r0, r11)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.DEFAULT_BACKGROUND_THREAD_PRIORITY = r0     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.unmountAllWhenComponentTreeSetToNull = r2     // Catch:{ all -> 0x0591 }
            r0 = 283837210234391(0x1022600170e17, double:1.40234214588232E-309)
            boolean r0 = r3.Aeo(r0, r2)     // Catch:{ all -> 0x0591 }
            X.C27571dP.A02 = r0     // Catch:{ all -> 0x0591 }
            r0 = 283837210299928(0x1022600180e18, double:1.402342146206116E-309)
            r3.Aeo(r0, r2)     // Catch:{ all -> 0x0591 }
            r0 = 298(0x12a, float:4.18E-43)
            boolean r0 = r6.AbO(r0, r2)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.canInterruptAndMoveLayoutsBetweenThreads = r0     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.inheritPriorityFromUiThread = r2     // Catch:{ all -> 0x0591 }
            r0 = 288067751648842(0x105ff00021e4a, double:1.42324379764421E-309)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.prioritizeRenderingOnParallel = r0     // Catch:{ all -> 0x0591 }
            r0 = 288067752107601(0x105ff00091e51, double:1.42324379991078E-309)
            boolean r0 = r3.Aem(r0)     // Catch:{ all -> 0x0591 }
            X.AnonymousClass07c.useSharedFutureOnParallel = r0     // Catch:{ all -> 0x0591 }
        L_0x0180:
            if (r24 == 0) goto L_0x018a
            java.util.HashSet r0 = r5.A0B     // Catch:{ all -> 0x0591 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0591 }
            if (r0 == 0) goto L_0x0517
        L_0x018a:
            int r2 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x0591 }
            X.0UN r1 = r5.A01     // Catch:{ all -> 0x0591 }
            r0 = 12
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x0591 }
            X.1YI r2 = (X.AnonymousClass1YI) r2     // Catch:{ all -> 0x0591 }
            r1 = 370(0x172, float:5.18E-43)
            r0 = 0
            boolean r0 = r2.AbO(r1, r0)     // Catch:{ all -> 0x0591 }
            if (r0 == 0) goto L_0x0517
            boolean r21 = X.C008707s.A00()     // Catch:{ all -> 0x0591 }
            if (r21 == 0) goto L_0x01b5
            java.lang.Class r0 = r5.getClass()     // Catch:{ all -> 0x0591 }
            java.lang.String r2 = r0.getSimpleName()     // Catch:{ all -> 0x0591 }
            r1 = -1303722038(0xffffffffb24ac7ca, float:-1.1803374E-8)
            java.lang.String r0 = "%s.onSetupSurfaceManager()"
            X.C005505z.A05(r0, r2, r1)     // Catch:{ all -> 0x0591 }
        L_0x01b5:
            android.content.Intent r2 = r5.getIntent()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            if (r2 == 0) goto L_0x04dc
            android.os.Bundle r0 = r2.getExtras()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            if (r0 == 0) goto L_0x04dc
            android.os.Bundle r1 = r2.getExtras()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.Class r0 = r5.getClass()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r1.setClassLoader(r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.os.Bundle r0 = X.C12280p0.A00(r2)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            if (r0 != 0) goto L_0x01d7
            goto L_0x01dc
        L_0x01d7:
            X.1dQ r6 = X.C12280p0.A01(r5, r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            goto L_0x01dd
        L_0x01dc:
            r6 = 0
        L_0x01dd:
            if (r6 == 0) goto L_0x04d6
            X.0oz r20 = new X.0oz     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r20.<init>()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r2 = r6.A00     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0 = r20
            java.util.concurrent.atomic.AtomicReference r0 = r0.A04     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.Object r0 = r0.get()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            if (r0 != 0) goto L_0x04c7
            r0 = r20
            X.2dy r1 = r0.A03     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r12 = 0
            java.lang.String r0 = "SurfaceDelegate"
            r1.Bpg(r6, r12, r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.content.Intent r0 = r5.getIntent()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.os.Bundle r11 = X.C12280p0.A00(r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.String r1 = r6.A02     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.String r0 = "_getSurfaceManager"
            X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            X.C73153fa.A00()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            if (r11 == 0) goto L_0x0219
            X.1dQ r1 = X.C12280p0.A01(r5, r11)     // Catch:{ all -> 0x04bf }
            boolean r0 = r6.equals(r1)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x0219
            r6 = r1
        L_0x0219:
            java.lang.Object r1 = X.C12280p0.A02     // Catch:{ all -> 0x04bf }
            monitor-enter(r1)     // Catch:{ all -> 0x04bf }
            X.1Gd r0 = X.C12280p0.A00     // Catch:{ all -> 0x04b7 }
            r0.remove(r6)     // Catch:{ all -> 0x04b7 }
            monitor-exit(r1)     // Catch:{ all -> 0x04b7 }
            X.0p1 r18 = X.C12280p0.A01     // Catch:{ all -> 0x04bf }
            r0 = r18
            X.0p7 r11 = r0.A06(r6)     // Catch:{ all -> 0x04bf }
            r3 = 0
            if (r11 != 0) goto L_0x0338
            r0 = 0
            long r14 = X.C73163fb.A00(r6, r0)     // Catch:{ all -> 0x04bf }
            r16 = 0
            int r11 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1))
            r15 = 0
            if (r11 == 0) goto L_0x023b
            r15 = 1
        L_0x023b:
            if (r15 == 0) goto L_0x0244
            r0 = r18
            X.1dR r11 = X.C12290p1.A01(r0, r6)     // Catch:{ all -> 0x04bf }
            goto L_0x024a
        L_0x0244:
            r0 = r18
            X.1dR r11 = r0.A05(r6)     // Catch:{ all -> 0x04bf }
        L_0x024a:
            if (r11 != 0) goto L_0x0263
            X.0p2 r11 = new X.0p2     // Catch:{ all -> 0x04bf }
            X.2dy r0 = r0.A07     // Catch:{ all -> 0x04bf }
            r0.BtH(r6)     // Catch:{ all -> 0x04bf }
            X.3fc r0 = r6.A02(r5)     // Catch:{ all -> 0x04bf }
            X.3fd r1 = r0.A00()     // Catch:{ all -> 0x04bf }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x04bf }
            r11.<init>(r1, r0)     // Catch:{ all -> 0x04bf }
            goto L_0x0282
        L_0x0263:
            X.3fd r1 = r11.A02     // Catch:{ all -> 0x04bf }
            X.2dy r14 = r0.A07     // Catch:{ all -> 0x04bf }
            int r13 = r11.A01     // Catch:{ all -> 0x04bf }
            int r0 = r11.A00     // Catch:{ all -> 0x04bf }
            r14.BVa(r13, r6, r0)     // Catch:{ all -> 0x04bf }
            if (r15 != 0) goto L_0x0279
            r11.A02 = r9     // Catch:{ all -> 0x04bf }
            r11.A01 = r12     // Catch:{ all -> 0x04bf }
            X.0v3 r0 = X.C27591dR.A03     // Catch:{ all -> 0x04bf }
            r0.C0t(r11)     // Catch:{ all -> 0x04bf }
        L_0x0279:
            X.0p2 r11 = new X.0p2     // Catch:{ all -> 0x04bf }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r16)     // Catch:{ all -> 0x04bf }
            r11.<init>(r1, r0)     // Catch:{ all -> 0x04bf }
        L_0x0282:
            java.lang.Object r1 = r11.A00     // Catch:{ all -> 0x04bf }
            X.3fd r1 = (X.C73183fd) r1     // Catch:{ all -> 0x04bf }
            java.lang.Object r0 = r11.A01     // Catch:{ all -> 0x04bf }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x04bf }
            boolean r11 = r0.booleanValue()     // Catch:{ all -> 0x04bf }
            r0 = r18
            X.2dy r0 = r0.A07     // Catch:{ all -> 0x04bf }
            r0.Br4(r6, r12)     // Catch:{ all -> 0x04bf }
            if (r11 == 0) goto L_0x029a
            r1.AZY(r12)     // Catch:{ all -> 0x04bf }
        L_0x029a:
            r3 = 0
            r16 = 0
            r11 = 0
            X.0p3 r0 = new X.0p3     // Catch:{ all -> 0x04bf }
            if (r9 != 0) goto L_0x02a3
            r3 = r5
        L_0x02a3:
            r0.<init>(r3, r12)     // Catch:{ all -> 0x04bf }
            X.1dS r3 = new X.1dS     // Catch:{ all -> 0x04bf }
            r3.<init>(r0, r6, r1)     // Catch:{ all -> 0x04bf }
            r0 = r18
            X.2dy r0 = r0.A07     // Catch:{ all -> 0x04bf }
            r3.A02 = r0     // Catch:{ all -> 0x04bf }
            r1 = 1
            if (r12 == r1) goto L_0x02b5
            r1 = 0
        L_0x02b5:
            android.app.Activity r9 = X.C73193fe.A01(r5)     // Catch:{ all -> 0x04bf }
            if (r9 == 0) goto L_0x02fd
            int r15 = r6.A00     // Catch:{ all -> 0x04bf }
            r0 = -1
            if (r15 != r0) goto L_0x02c4
            int r15 = X.C73193fe.A00(r9)     // Catch:{ all -> 0x04bf }
        L_0x02c4:
            android.util.DisplayMetrics r13 = new android.util.DisplayMetrics     // Catch:{ all -> 0x04bf }
            r13.<init>()     // Catch:{ all -> 0x04bf }
            android.view.WindowManager r0 = r9.getWindowManager()     // Catch:{ all -> 0x04bf }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ all -> 0x04bf }
            r0.getMetrics(r13)     // Catch:{ all -> 0x04bf }
            int r14 = r13.widthPixels     // Catch:{ all -> 0x04bf }
            android.util.DisplayMetrics r13 = new android.util.DisplayMetrics     // Catch:{ all -> 0x04bf }
            r13.<init>()     // Catch:{ all -> 0x04bf }
            android.view.WindowManager r0 = r9.getWindowManager()     // Catch:{ all -> 0x04bf }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ all -> 0x04bf }
            r0.getMetrics(r13)     // Catch:{ all -> 0x04bf }
            int r13 = r13.heightPixels     // Catch:{ all -> 0x04bf }
            int r13 = r13 - r15
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r14, r0)     // Catch:{ all -> 0x04bf }
            r3.A01 = r0     // Catch:{ all -> 0x04bf }
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r13, r0)     // Catch:{ all -> 0x04bf }
            r3.A00 = r0     // Catch:{ all -> 0x04bf }
            if (r1 == 0) goto L_0x02fd
            r3.A04 = r7     // Catch:{ all -> 0x04bf }
        L_0x02fd:
            r3.A03 = r11     // Catch:{ all -> 0x04bf }
            X.0p7 r11 = new X.0p7     // Catch:{ all -> 0x04bf }
            r11.<init>(r3)     // Catch:{ all -> 0x04bf }
            if (r12 == 0) goto L_0x0316
            r0 = r18
            java.lang.Object r3 = r0.A06     // Catch:{ all -> 0x04bf }
            monitor-enter(r3)     // Catch:{ all -> 0x04bf }
            X.04a r0 = r0.A05     // Catch:{ all -> 0x0312 }
            r0.put(r6, r11)     // Catch:{ all -> 0x0312 }
            monitor-exit(r3)     // Catch:{ all -> 0x0312 }
            goto L_0x0316
        L_0x0312:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0312 }
            goto L_0x04be
        L_0x0316:
            if (r1 == 0) goto L_0x0325
            int r0 = (r16 > r16 ? 1 : (r16 == r16 ? 0 : -1))
            if (r0 <= 0) goto L_0x0325
            r12 = r18
            r14 = r16
            java.lang.Runnable r3 = X.C12290p1.A03(r12, r6, r14)     // Catch:{ all -> 0x04bf }
            goto L_0x0326
        L_0x0325:
            r3 = 0
        L_0x0326:
            if (r1 == 0) goto L_0x03e2
            boolean r0 = r9 instanceof androidx.fragment.app.FragmentActivity     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x03e2
            X.1dT r1 = new X.1dT     // Catch:{ all -> 0x04bf }
            r0 = r18
            r1.<init>(r0, r9, r6, r3)     // Catch:{ all -> 0x04bf }
            r9.runOnUiThread(r1)     // Catch:{ all -> 0x04bf }
            goto L_0x03e2
        L_0x0338:
            r0 = 0
            long r13 = X.C73163fb.A00(r6, r0)     // Catch:{ all -> 0x04bf }
            int r12 = (r13 > r0 ? 1 : (r13 == r0 ? 0 : -1))
            r9 = 0
            if (r12 == 0) goto L_0x0344
            r9 = 1
        L_0x0344:
            if (r9 == 0) goto L_0x034d
            r0 = r18
            X.1dR r1 = X.C12290p1.A01(r0, r6)     // Catch:{ all -> 0x04bf }
            goto L_0x0353
        L_0x034d:
            r0 = r18
            X.1dR r1 = r0.A05(r6)     // Catch:{ all -> 0x04bf }
        L_0x0353:
            if (r1 == 0) goto L_0x0361
            if (r9 != 0) goto L_0x0361
            r0 = 0
            r1.A02 = r0     // Catch:{ all -> 0x04bf }
            r1.A01 = r3     // Catch:{ all -> 0x04bf }
            X.0v3 r0 = X.C27591dR.A03     // Catch:{ all -> 0x04bf }
            r0.C0t(r1)     // Catch:{ all -> 0x04bf }
        L_0x0361:
            java.lang.Object r1 = r11.A0B     // Catch:{ all -> 0x04bf }
            monitor-enter(r1)     // Catch:{ all -> 0x04bf }
            com.facebook.litho.ComponentTree r0 = r11.A02     // Catch:{ all -> 0x04b4 }
            monitor-exit(r1)     // Catch:{ all -> 0x04b4 }
            r9 = 0
            if (r0 == 0) goto L_0x0379
            X.0p4 r0 = r0.A0S     // Catch:{ all -> 0x04bf }
            android.content.Context r0 = r0.A09     // Catch:{ all -> 0x04bf }
            android.content.Context r1 = X.C12320p7.A00(r5)     // Catch:{ all -> 0x04bf }
            android.content.Context r0 = X.C12320p7.A00(r0)     // Catch:{ all -> 0x04bf }
            if (r1 != r0) goto L_0x0379
            r9 = 1
        L_0x0379:
            if (r9 == 0) goto L_0x0387
            r0 = r18
            X.2dy r1 = r0.A07     // Catch:{ all -> 0x04bf }
            X.0p3 r0 = r11.A08     // Catch:{ all -> 0x04bf }
            int r0 = r0.A01     // Catch:{ all -> 0x04bf }
            r1.Br4(r6, r0)     // Catch:{ all -> 0x04bf }
            goto L_0x03e2
        L_0x0387:
            r0 = r18
            X.2dy r9 = r0.A07     // Catch:{ all -> 0x04bf }
            int r1 = r6.hashCode()     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "SurfaceManager_wrong_context"
            r9.AYD(r0, r1)     // Catch:{ all -> 0x04bf }
            r0 = r18
            X.2dy r0 = r0.A07     // Catch:{ all -> 0x04bf }
            r0.Br4(r6, r3)     // Catch:{ all -> 0x04bf }
            java.lang.String r1 = r6.A02     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "_recreateSurfaceManagerWithNewContext"
            X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x04bf }
            X.C73153fa.A00()     // Catch:{ all -> 0x04bf }
            X.0p3 r1 = new X.0p3     // Catch:{ all -> 0x04ba }
            X.0p3 r0 = r11.A08     // Catch:{ all -> 0x04ba }
            int r0 = r0.A01     // Catch:{ all -> 0x04ba }
            r1.<init>(r5, r0)     // Catch:{ all -> 0x04ba }
            X.3fd r0 = r11.A05     // Catch:{ all -> 0x04ba }
            r0.C1p(r11)     // Catch:{ all -> 0x04ba }
            X.3fd r0 = r11.A05     // Catch:{ all -> 0x04ba }
            X.1dS r3 = new X.1dS     // Catch:{ all -> 0x04ba }
            r3.<init>(r1, r6, r0)     // Catch:{ all -> 0x04ba }
            java.lang.Object r9 = r11.A0B     // Catch:{ all -> 0x04ba }
            monitor-enter(r9)     // Catch:{ all -> 0x04ba }
            int r6 = r11.A01     // Catch:{ all -> 0x04b1 }
            int r1 = r11.A00     // Catch:{ all -> 0x04b1 }
            monitor-exit(r9)     // Catch:{ all -> 0x04b1 }
            X.2dy r0 = r11.A06     // Catch:{ all -> 0x04ba }
            r3.A02 = r0     // Catch:{ all -> 0x04ba }
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r0)     // Catch:{ all -> 0x04ba }
            r3.A01 = r0     // Catch:{ all -> 0x04ba }
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r0)     // Catch:{ all -> 0x04ba }
            r3.A00 = r0     // Catch:{ all -> 0x04ba }
            X.2FG r0 = r11.A09     // Catch:{ all -> 0x04ba }
            r3.A03 = r0     // Catch:{ all -> 0x04ba }
            X.0p7 r11 = new X.0p7     // Catch:{ all -> 0x04ba }
            r11.<init>(r3)     // Catch:{ all -> 0x04ba }
            X.C73153fa.A01()     // Catch:{ all -> 0x04bf }
        L_0x03e2:
            X.C73153fa.A01()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0 = r20
            java.util.concurrent.atomic.AtomicReference r0 = r0.A04     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0.set(r11)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.util.DisplayMetrics r1 = new android.util.DisplayMetrics     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r1.<init>()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.view.WindowManager r0 = r5.getWindowManager()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0.getMetrics(r1)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r1 = r1.widthPixels     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0 = 1073741824(0x40000000, float:2.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0 = r20
            r0.A01 = r1     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.util.DisplayMetrics r1 = new android.util.DisplayMetrics     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r1.<init>()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.view.WindowManager r0 = r5.getWindowManager()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0.getMetrics(r1)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r1 = r1.heightPixels     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r1 = r1 - r2
            r0 = 1073741824(0x40000000, float:2.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0 = r20
            r0.A00 = r1     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.util.concurrent.atomic.AtomicReference r0 = r0.A04     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.Object r3 = r0.get()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            X.0p7 r3 = (X.C12320p7) r3     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            if (r3 == 0) goto L_0x04a9
            r0 = r20
            com.facebook.litho.LithoView r0 = r0.A02     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            if (r0 != 0) goto L_0x04c7
            com.facebook.litho.LithoView r6 = new com.facebook.litho.LithoView     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r6.<init>(r5)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0 = r20
            r0.A02 = r6     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r11 = r0.A01     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r12 = r0.A00     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.Object r1 = r3.A0B     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            monitor-enter(r1)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r14 = r3.A01     // Catch:{ all -> 0x04a6 }
            int r2 = r3.A00     // Catch:{ all -> 0x04a6 }
            com.facebook.litho.ComponentTree r9 = r3.A02     // Catch:{ all -> 0x04a6 }
            monitor-exit(r1)     // Catch:{ all -> 0x04a6 }
            if (r9 != 0) goto L_0x0473
            X.2dy r2 = r3.A06     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r1 = r3.A04     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.String r0 = "Create_ComponentTree_on_UI_Thread"
            r2.AYD(r0, r1)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            com.facebook.litho.ComponentTree r0 = X.C12320p7.A02(r3, r11, r12)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.Object r1 = r3.A0B     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            monitor-enter(r1)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            com.facebook.litho.ComponentTree r9 = r3.A02     // Catch:{ all -> 0x0470 }
            if (r9 != 0) goto L_0x046a
            r3.A01 = r11     // Catch:{ all -> 0x0470 }
            r3.A00 = r12     // Catch:{ all -> 0x0470 }
            r3.A02 = r0     // Catch:{ all -> 0x0470 }
            monitor-exit(r1)     // Catch:{ all -> 0x0470 }
            goto L_0x04a1
        L_0x046a:
            int r14 = r3.A01     // Catch:{ all -> 0x0470 }
            int r2 = r3.A00     // Catch:{ all -> 0x0470 }
            monitor-exit(r1)     // Catch:{ all -> 0x0470 }
            goto L_0x0473
        L_0x0470:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0470 }
            goto L_0x04c3
        L_0x0473:
            r13 = 1
            r0 = -1
            if (r14 == r0) goto L_0x0489
            if (r2 == r0) goto L_0x0489
            if (r14 != r11) goto L_0x047e
            if (r2 != r12) goto L_0x047e
            r13 = 0
        L_0x047e:
            if (r13 == 0) goto L_0x0489
            X.2dy r2 = r3.A06     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            int r1 = r3.A04     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.String r0 = "wrong_size_spec"
            r2.AYD(r0, r1)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
        L_0x0489:
            if (r13 == 0) goto L_0x0493
            r0 = -1
            if (r11 == r0) goto L_0x0493
            if (r12 == r0) goto L_0x0493
            r9.A0N(r11, r12)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
        L_0x0493:
            java.lang.Object r1 = r3.A0B     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            monitor-enter(r1)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            com.facebook.litho.ComponentTree r0 = r3.A02     // Catch:{ all -> 0x04a3 }
            if (r0 != r9) goto L_0x04a0
            r3.A01 = r11     // Catch:{ all -> 0x04a3 }
            r3.A00 = r12     // Catch:{ all -> 0x04a3 }
            monitor-exit(r1)     // Catch:{ all -> 0x04a3 }
            goto L_0x04c4
        L_0x04a0:
            monitor-exit(r1)     // Catch:{ all -> 0x04a3 }
        L_0x04a1:
            r9 = r0
            goto L_0x04c4
        L_0x04a3:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x04a3 }
            goto L_0x04c3
        L_0x04a6:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x04a6 }
            goto L_0x04c3
        L_0x04a9:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.String r0 = "SurfaceManager is null in createLithoView()."
            r1.<init>(r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            throw r1     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
        L_0x04b1:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x04b1 }
            throw r0     // Catch:{ all -> 0x04ba }
        L_0x04b4:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x04b4 }
            goto L_0x04be
        L_0x04b7:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x04b7 }
            goto L_0x04be
        L_0x04ba:
            r0 = move-exception
            X.C73153fa.A01()     // Catch:{ all -> 0x04bf }
        L_0x04be:
            throw r0     // Catch:{ all -> 0x04bf }
        L_0x04bf:
            r0 = move-exception
            X.C73153fa.A01()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
        L_0x04c3:
            throw r0     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
        L_0x04c4:
            r6.A0b(r9)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
        L_0x04c7:
            r2 = 15
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            X.0UN r0 = r5.A01     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
            r0.now()     // Catch:{ BadParcelableException -> 0x04e5, 1wD -> 0x04df }
        L_0x04d6:
            if (r21 == 0) goto L_0x0517
            r0 = 1440201367(0x55d7ba97, float:2.96495497E13)
            goto L_0x0503
        L_0x04dc:
            if (r21 == 0) goto L_0x0517
            goto L_0x0511
        L_0x04df:
            if (r21 == 0) goto L_0x0517
            r0 = -971808967(0xffffffffc6135f39, float:-9431.806)
            goto L_0x0503
        L_0x04e5:
            r3 = move-exception
            r2 = 4
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0507 }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x0507 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0507 }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x0507 }
            java.lang.Class r0 = com.facebook.base.activity.FbFragmentActivity.A0C     // Catch:{ all -> 0x0507 }
            java.lang.String r1 = r0.getSimpleName()     // Catch:{ all -> 0x0507 }
            java.lang.String r0 = r3.getMessage()     // Catch:{ all -> 0x0507 }
            r2.CGS(r1, r0)     // Catch:{ all -> 0x0507 }
            if (r21 == 0) goto L_0x0517
            r0 = -626863746(0xffffffffdaa2d17e, float:-2.29146427E16)
        L_0x0503:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0591 }
            goto L_0x0517
        L_0x0507:
            r1 = move-exception
            if (r21 == 0) goto L_0x0510
            r0 = -1582141224(0xffffffffa1b270d8, float:-1.2091619E-18)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0591 }
        L_0x0510:
            throw r1     // Catch:{ all -> 0x0591 }
        L_0x0511:
            r0 = -1440595635(0xffffffffaa22414d, float:-1.4411146E-13)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0591 }
        L_0x0517:
            r5.A1C(r4)     // Catch:{ all -> 0x0591 }
            X.0UN r1 = r5.A01     // Catch:{ all -> 0x0591 }
            r0 = r22
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r10, r0, r1)     // Catch:{ all -> 0x0591 }
            X.0nQ r0 = (X.C11590nQ) r0     // Catch:{ all -> 0x0591 }
            boolean r0 = r0.A0W(r4)     // Catch:{ all -> 0x0591 }
            if (r0 == 0) goto L_0x053b
            r5.A07 = r7     // Catch:{ all -> 0x0591 }
            if (r19 == 0) goto L_0x0534
            r0 = -321432960(0xffffffffecd75280, float:-2.0824692E27)
            X.C005505z.A00(r0)
        L_0x0534:
            r0 = -872985590(0xffffffffcbf74c0a, float:-3.2413716E7)
            X.C000700l.A07(r0, r8)
            return
        L_0x053b:
            r3 = 4
            r5.A1B(r4)     // Catch:{ all -> 0x0591 }
            X.0UN r1 = r5.A01     // Catch:{ all -> 0x0591 }
            r0 = r22
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r10, r0, r1)     // Catch:{ all -> 0x0591 }
            X.0nQ r0 = (X.C11590nQ) r0     // Catch:{ all -> 0x0591 }
            r0.A0B()     // Catch:{ all -> 0x0591 }
            java.lang.Class r0 = r5.getClass()     // Catch:{ all -> 0x0591 }
            java.lang.String r2 = r0.getSimpleName()     // Catch:{ all -> 0x0591 }
            java.lang.String r1 = "_FLAG_"
            int r0 = r5.hashCode()     // Catch:{ all -> 0x0591 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x0591 }
            java.lang.String r2 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x0591 }
            r5.A04 = r2     // Catch:{ all -> 0x0591 }
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0591 }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x0591 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x0591 }
            X.09P r1 = (X.AnonymousClass09P) r1     // Catch:{ all -> 0x0591 }
            java.lang.String r0 = "1"
            r1.Bz2(r2, r0)     // Catch:{ all -> 0x0591 }
            int r1 = X.AnonymousClass1Y3.BSU     // Catch:{ all -> 0x0591 }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x0591 }
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0591 }
            X.1er r1 = (X.C28471er) r1     // Catch:{ all -> 0x0591 }
            X.0US r0 = r5.A02     // Catch:{ all -> 0x0591 }
            r1.A01(r5, r0)     // Catch:{ all -> 0x0591 }
            if (r19 == 0) goto L_0x058a
            r0 = -761969664(0xffffffffd2954400, float:-3.20545489E11)
            X.C005505z.A00(r0)
        L_0x058a:
            r0 = -685152717(0xffffffffd7296633, float:-1.86256407E14)
            X.C000700l.A07(r0, r8)
            return
        L_0x0591:
            r1 = move-exception
            if (r19 == 0) goto L_0x059a
            r0 = 200525911(0xbf3c857, float:9.3901633E-32)
            X.C005505z.A00(r0)
        L_0x059a:
            r0 = 544053587(0x206d9953, float:2.0125412E-19)
            X.C000700l.A07(r0, r8)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.base.activity.FbFragmentActivity.onCreate(android.os.Bundle):void");
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        LayoutInflater.Factory factory;
        boolean A002 = C008707s.A00();
        if (A002) {
            C005505z.A05("onCreateView(%s)", str, -584173869);
        }
        try {
            View onCreateView = super.onCreateView(str, context, attributeSet);
            if (onCreateView == null && (factory = (LayoutInflater.Factory) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AKW, this.A01)) != null) {
                onCreateView = factory.onCreateView(str, context, attributeSet);
            }
            return onCreateView;
        } finally {
            if (A002) {
                C005505z.A00(455650095);
            }
        }
    }

    public final void onDestroy() {
        int A002 = C000700l.A00(1836282813);
        if (!this.A07) {
            A17();
        }
        try {
            ((C28591f3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AIO, this.A01)).A01();
            ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0D();
            C11610nS r0 = this.A00;
            if (r0 != null) {
                r0.A0C();
                this.A00 = null;
            }
        } finally {
            super.onDestroy();
            ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A01)).removeCustomData(this.A04);
            C000700l.A07(1874763330, A002);
        }
    }

    public final void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (!isFinishing() || ((AnonymousClass20J) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BRn, this.A01)).A00(getClass(), DeliverOnNewIntentWhenFinishing.class)) {
            ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0L(intent);
            this.A08 = false;
            A19(intent);
            Preconditions.checkState(this.A08, "onActivityNewIntent didn't call super.onActivityNewIntent()");
        }
    }

    public void onPause() {
        int A002 = C000700l.A00(980943167);
        boolean A003 = C008707s.A00();
        if (A003) {
            C005505z.A05("%s.onPause", C05260Yg.A00(getClass()), 964953698);
        }
        try {
            super.onPause();
            ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A01)).C1g(null);
            ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0E();
        } finally {
            if (A003) {
                C005505z.A00(134759632);
            }
            C000700l.A07(-1867768974, A002);
        }
    }

    public void onPictureInPictureModeChanged(boolean z, Configuration configuration) {
        super.onPictureInPictureModeChanged(z, configuration);
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0T(z, configuration);
    }

    public void onPostCreate(Bundle bundle) {
        String str;
        C12330pA r7;
        Integer num;
        Integer num2;
        super.onPostCreate(bundle);
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0N(bundle);
        C11650nY r2 = (C11650nY) AnonymousClass1XX.A02(11, AnonymousClass1Y3.AS7, this.A01);
        if (r2.A00 == C001500z.A03) {
            C07790eA r5 = r2.A01;
            synchronized (r5) {
                str = null;
                if (this instanceof C11510nI) {
                    str = ((C11510nI) this).Act();
                }
                r7 = new C12330pA(this, str);
                synchronized (r5) {
                    if (r5.A0A.containsKey(Integer.valueOf(hashCode()))) {
                        int indexOf = r5.A08.indexOf((C12330pA) r5.A0A.get(Integer.valueOf(hashCode())));
                        for (int i = 0; i < indexOf; i++) {
                            r5.A0A.remove(Integer.valueOf(((C12330pA) r5.A08.removeFirst()).A0E));
                        }
                    }
                }
            }
            if (r5.A08.isEmpty() || !r7.A08().equals(((C12330pA) r5.A08.peek()).A08())) {
                num = AnonymousClass07B.A00;
            } else if (r7.A0E != ((C12330pA) r5.A08.peek()).A0E && !C06850cB.A0C(r7.A0E(), ((C12330pA) r5.A08.peek()).A0E())) {
                num = AnonymousClass07B.A01;
            } else if (r7.A0E != ((C12330pA) r5.A08.peek()).A0E && C06850cB.A0C(r7.A0E(), ((C12330pA) r5.A08.peek()).A0E())) {
                num = AnonymousClass07B.A0N;
            } else if (r7.A0E != ((C12330pA) r5.A08.peek()).A0E || C06850cB.A0C(r7.A0E(), ((C12330pA) r5.A08.peek()).A0E())) {
                num = AnonymousClass07B.A0Y;
            } else {
                num = AnonymousClass07B.A0C;
            }
            switch (num.intValue()) {
                case 0:
                case 1:
                    C07790eA.A03(r5, r7);
                    num2 = AnonymousClass07B.A00;
                    break;
                case 2:
                case 3:
                    synchronized (r5) {
                        r5.A0A.remove(Integer.valueOf(((C12330pA) r5.A08.removeFirst()).A0E));
                        C07790eA.A03(r5, r7);
                        num2 = AnonymousClass07B.A00;
                        break;
                    }
                case 4:
                    r5.A08.peek();
                    num2 = AnonymousClass07B.A01;
                    break;
                default:
                    throw new IllegalStateException(AnonymousClass08S.A0J("Unrecognized navigation status ", C73203fg.A00(num)));
            }
            AnonymousClass1YL r0 = r5.A05;
            ImmutableList immutableList = C07790eA.A0B;
            if (r0.A05(immutableList)) {
                r5.A05.A04(immutableList, new C04210Sx(r7.A06), C25141Ym.INSTANCE);
            }
            if (!(this instanceof FragmentActivity) && str != null && !str.equals("unknown")) {
                List A072 = r5.A07();
                if (!r5.A03.equals(A072) && r5.A07.A05(immutableList)) {
                    r5.A07.A04(immutableList, new C11620nT(A072, num2, C07790eA.A01(r5)), C25141Ym.INSTANCE);
                    r5.A03 = A072;
                }
            }
        }
        C11610nS r02 = this.A00;
        if (r02 != null) {
            r02.A0K(bundle);
        }
    }

    public void onPostResume() {
        super.onPostResume();
        C11610nS r0 = this.A00;
        if (r0 != null) {
            r0.A0D();
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        String str;
        Integer num;
        boolean z;
        super.onRequestPermissionsResult(i, strArr, iArr);
        C04220Sy r0 = this.A03;
        if (r0 != null) {
            C04230Sz r3 = r0.A00;
            Activity activity = r3.A00;
            if (activity instanceof FbFragmentActivity) {
                ((FbFragmentActivity) activity).A03 = null;
            }
            if (i == 0) {
                int length = iArr.length;
                if (r7 != length) {
                    C010708t.A0Q("ActivityRuntimePermissionsManager", "Mismatch of array lengths between permissions (%d) and grantResults (%d)", Integer.valueOf(r7), Integer.valueOf(length));
                } else {
                    for (int i2 = 0; i2 < r7; i2++) {
                        C73213fh r10 = (C73213fh) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AkV, r3.A01);
                        if (iArr[i2] == 0) {
                            str = "PRIMARY";
                        } else {
                            str = "SECONDARY";
                        }
                        r10.A01(str, "SYSTEM", strArr[i2], r3.A00);
                        C73223fi r4 = (C73223fi) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AAN, r3.A01);
                        if (iArr[i2] == 0) {
                            num = AnonymousClass07B.A01;
                        } else {
                            num = AnonymousClass07B.A0C;
                        }
                        r4.A02(num, strArr[i2], "system");
                    }
                }
                for (String A092 : strArr) {
                    C12460pP r8 = (C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, r3.A01);
                    C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, r8.A00)).edit();
                    edit.putBoolean((AnonymousClass1Y7) r8.A02.A09(A092), true);
                    edit.commit();
                }
                if (length >= 1) {
                    int i3 = 0;
                    while (true) {
                        if (i3 < length) {
                            if (iArr[i3] != 0) {
                                break;
                            }
                            i3++;
                        } else {
                            z = true;
                            break;
                        }
                    }
                }
                z = false;
                if (z) {
                    r3.A03.BiO();
                } else {
                    ArrayList arrayList = new ArrayList();
                    for (String str2 : strArr) {
                        if (((C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, r3.A01)).A08(str2)) {
                            arrayList.add(str2);
                        }
                    }
                    String[] strArr2 = (String[]) arrayList.toArray(new String[arrayList.size()]);
                    boolean z2 = false;
                    String[] A0A2 = ((C12460pP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AWJ, r3.A01)).A0A(r3.A00, strArr);
                    String[] A042 = C04230Sz.A04(r3, strArr);
                    RequestPermissionsConfig requestPermissionsConfig = r3.A02;
                    int i4 = requestPermissionsConfig.A00;
                    boolean z3 = true;
                    if (!(i4 == 2 || i4 == 3 || i4 == 1)) {
                        z3 = false;
                    }
                    if (!z3 || (r9 = A042.length) <= 0 || !r3.A05) {
                        if (r3.A05 || A042.length > 0) {
                            z2 = true;
                        }
                        r3.A05 = z2;
                        r3.A03.BiP(A0A2, A042);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putStringArray("permissions_never_ask_again", A042);
                        bundle.putParcelable("config", requestPermissionsConfig);
                        RuntimePermissionsNeverAskAgainDialogFragment runtimePermissionsNeverAskAgainDialogFragment = new RuntimePermissionsNeverAskAgainDialogFragment();
                        runtimePermissionsNeverAskAgainDialogFragment.A1P(bundle);
                        runtimePermissionsNeverAskAgainDialogFragment.A03 = new C12960qH(r3, A042, strArr2, A0A2);
                        r3.A04 = runtimePermissionsNeverAskAgainDialogFragment;
                        C73213fh r6 = (C73213fh) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AkV, r3.A01);
                        Activity activity2 = r3.A00;
                        for (String A012 : A042) {
                            r6.A01("IMPRESSION", "NEVER_ASK_AGAIN", A012, activity2);
                        }
                        runtimePermissionsNeverAskAgainDialogFragment.A25(((FragmentActivity) r3.A00).B4m(), null);
                    }
                }
            }
            C04230Sz.A00(r3, strArr);
        }
    }

    public void onResume() {
        int A002 = C000700l.A00(-579160485);
        boolean A003 = C008707s.A00();
        if (A003) {
            C005505z.A05("%s.onResume", C05260Yg.A00(getClass()), -1266791901);
        }
        if (A003) {
            try {
                C005505z.A03("StartOp FbFragmentActivity.dispatchWithManualOps", -1605047666);
            } catch (Throwable th) {
                if (A003) {
                    C005505z.A00(1213327204);
                    C005505z.A00(-387574970);
                }
                C000700l.A07(-465304790, A002);
                throw th;
            }
        }
        if (A003) {
            C005505z.A00(1689428151);
        }
        if (A003) {
            C005505z.A03("StartOp FbFragmentActivity.updateResources", 1116098877);
        }
        A0x();
        if (A003) {
            C005505z.A00(1723817876);
        }
        if (A003) {
            C005505z.A03("StartOp FbFragmentActivity.superOnResume", -935619179);
        }
        super.onResume();
        if (A003) {
            C005505z.A00(-1496800449);
        }
        if (A003) {
            C005505z.A03("StartOp FbFragmentActivity.setUpDumpsys", 2102002139);
        }
        if (0 != 0) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A01)).Bz9(null, null);
        }
        if (A003) {
            C005505z.A00(-848019519);
        }
        if (A003) {
            C005505z.A03("StartOp FbFragmentActivity.dispatchOnResume", -1145402413);
        }
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0F();
        if (A003) {
            C005505z.A00(-1461460432);
        }
        if (A003) {
            C005505z.A03("StartOp FbFragmentActivity.recreateIfDarkModeChanged", 626679175);
        }
        int i = AnonymousClass1Y3.AcV;
        if (((C11660nZ) AnonymousClass1XX.A02(5, i, this.A01)).A02()) {
            if (((C11660nZ) AnonymousClass1XX.A02(5, i, this.A01)).A03() != this.A06) {
                this.A06 = ((C11660nZ) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AcV, this.A01)).A03();
                recreate();
            }
            if (0 != 0) {
                recreate();
            }
        }
        boolean z = this.A05;
        boolean z2 = ((C11660nZ) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AcV, this.A01)).A00;
        if (z != z2) {
            this.A05 = z2;
            recreate();
        }
        if (A003) {
            C005505z.A00(113751420);
        }
        if (A003) {
            C005505z.A03("StartOp FbFragmentActivity.errorReporterSetActivityName", 132024301);
        }
        AnonymousClass1XX.A03(AnonymousClass1Y3.ABs, this.A01);
        ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A01)).C2l(C187614z.A01(this));
        if (A003) {
            C005505z.A00(-1434264854);
        }
        if (A003) {
            C005505z.A03("StartOp FbFragmentActivity.completeSequence", 769134112);
        }
        if (A003) {
            C005505z.A00(-223968466);
            C005505z.A00(1637882198);
        }
        C000700l.A07(-346235319, A002);
    }

    /* JADX INFO: finally extract failed */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putStringArrayList("attached_fragment_names", new ArrayList(this.A0B));
        C11590nQ r2 = (C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01);
        C11590nQ.A03(r2);
        C005505z.A03("FbActivityListeners.onSaveInstanceState", -537580678);
        try {
            for (C13200qt Bmx : r2.A06) {
                Bmx.Bmx(bundle);
            }
            C11590nQ.A02(r2);
            C005505z.A00(1212120082);
            C11590nQ.A04(r2);
            C11610nS r0 = this.A00;
            if (r0 != null) {
                r0.A0L(bundle);
            }
        } catch (Throwable th) {
            C005505z.A00(-616643847);
            C11590nQ.A04(r2);
            throw th;
        }
    }

    public void onStart() {
        int A002 = C000700l.A00(993614065);
        boolean A003 = C008707s.A00();
        if (A003) {
            C005505z.A05("%s.onStart", C05260Yg.A00(getClass()), -1279260055);
        }
        try {
            super.onStart();
            ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0G();
            C11610nS r0 = this.A00;
            if (r0 != null) {
                r0.A0E();
            }
        } finally {
            if (A003) {
                C005505z.A00(751986958);
            }
            C000700l.A07(-1000182798, A002);
        }
    }

    public void onStop() {
        int A002 = C000700l.A00(-1768347970);
        boolean A003 = C008707s.A00();
        if (A003) {
            C005505z.A05("%s.onStop", C05260Yg.A00(getClass()), 2098917170);
        }
        try {
            super.onStop();
            ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0H();
            C11610nS r0 = this.A00;
            if (r0 != null) {
                r0.A0F();
            }
        } finally {
            if (A003) {
                C005505z.A00(2079594195);
            }
            C000700l.A07(-240090656, A002);
        }
    }

    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0R(charSequence, i);
    }

    public void onTrimMemory(int i) {
        super.onTrimMemory(i);
        C11590nQ r3 = (C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01);
        C11590nQ.A03(r3);
        C005505z.A03("FbActivityListeners.onTrimMemory", 1933100203);
        try {
            for (C13200qt Bt3 : r3.A06) {
                Bt3.Bt3(r3.A01, i);
            }
            C11590nQ.A02(r3);
        } finally {
            C005505z.A00(1255634127);
            C11590nQ.A04(r3);
        }
    }

    public void onUserInteraction() {
        super.onUserInteraction();
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0I();
    }

    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0J();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        ((C11590nQ) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AHa, this.A01)).A0S(z);
    }

    public void setContentView(int i) {
        boolean A002 = C008707s.A00();
        if (A002) {
            C005505z.A05("setContentView(%s)", getResources().getResourceName(i), -889737807);
        }
        try {
            super.setContentView(i);
            if (A002) {
                C005505z.A00(-1362975143);
            }
        } catch (RuntimeException e) {
            Class<C12970qJ> cls = C12970qJ.class;
            Preconditions.checkNotNull(e);
            if (!cls.isInstance(e)) {
                throw new C12970qJ(this, i, e);
            }
            throw cls.cast(e);
        } catch (Throwable th) {
            if (A002) {
                C005505z.A00(1309554827);
            }
            throw th;
        }
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        C11610nS r0 = this.A00;
        if (r0 != null) {
            r0.A0P(charSequence);
        }
    }

    public void startActivity(Intent intent) {
        ((AnonymousClass26J) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Awr, this.A01)).A01(this, intent);
        if (((C25051Yd) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AOJ, this.A01)).Aem(284082021863169L)) {
            intent.addFlags(65536);
        }
        super.startActivity(intent);
    }

    public void startActivity(Intent intent, Bundle bundle) {
        ((AnonymousClass26J) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Awr, this.A01)).A01(this, intent);
        super.startActivity(intent, bundle);
    }

    public void startActivityForResult(Intent intent, int i) {
        ((AnonymousClass26J) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Awr, this.A01)).A02(this, intent);
        super.startActivityForResult(intent, i);
    }

    public void startActivityForResult(Intent intent, int i, Bundle bundle) {
        ((AnonymousClass26J) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Awr, this.A01)).A02(this, intent);
        super.startActivityForResult(intent, i, bundle);
    }
}
