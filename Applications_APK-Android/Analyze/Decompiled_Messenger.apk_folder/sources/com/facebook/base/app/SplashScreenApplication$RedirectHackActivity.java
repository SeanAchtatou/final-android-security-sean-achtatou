package com.facebook.base.app;

import X.AnonymousClass001;
import X.C000700l;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;

public final class SplashScreenApplication$RedirectHackActivity extends Activity {
    public Bundle A00;
    public long A01;
    public ArrayList A02;
    public boolean A03;
    public boolean A04;
    public final /* synthetic */ AnonymousClass001 A05;

    public SplashScreenApplication$RedirectHackActivity(AnonymousClass001 r1) {
        this.A05 = r1;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 773972459 && i2 == 1062849428) {
            onBackPressed();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002b, code lost:
        if ("android.intent.action.MAIN".equals(r1.getAction()) == false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            r0 = 226787603(0xd848113, float:8.166202E-31)
            int r7 = X.C000700l.A00(r0)
            r0 = 0
            super.onCreate(r0)
            r8 = 1
            boolean r0 = r9.isTaskRoot()
            if (r0 != 0) goto L_0x002d
            android.content.Intent r1 = r9.getIntent()
            if (r1 == 0) goto L_0x002d
            java.lang.String r0 = "android.intent.category.LAUNCHER"
            boolean r0 = r1.hasCategory(r0)
            if (r0 == 0) goto L_0x002d
            java.lang.String r1 = r1.getAction()
            java.lang.String r0 = "android.intent.action.MAIN"
            boolean r1 = r0.equals(r1)
            r0 = 1
            if (r1 != 0) goto L_0x002e
        L_0x002d:
            r0 = 0
        L_0x002e:
            if (r0 == 0) goto L_0x0033
            r9.finish()
        L_0x0033:
            X.001 r0 = r9.A05
            java.util.Random r3 = r0.A0V
            if (r3 != 0) goto L_0x0040
            java.util.Random r3 = new java.util.Random
            r3.<init>()
            r0.A0V = r3
        L_0x0040:
            long r1 = r3.nextLong()
            r4 = 0
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0040
            r9.A01 = r1
            r1 = 0
            X.001 r0 = r9.A05
            java.util.ArrayList r0 = r0.A0d
            r0.add(r9)
            r9.setVisible(r1)
            X.001 r0 = r9.A05
            java.util.Random r1 = r0.A0V
            if (r1 != 0) goto L_0x0064
            java.util.Random r1 = new java.util.Random
            r1.<init>()
            r0.A0V = r1
        L_0x0064:
            long r2 = r1.nextLong()
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0064
            android.content.Intent r4 = r9.getIntent()
            X.001 r1 = r9.A05
            android.content.ComponentName r0 = r4.getComponent()
            java.lang.String r0 = r0.getClassName()
            r1.A0L = r0
            X.001 r0 = r9.A05
            android.content.Intent r6 = new android.content.Intent
            java.lang.Class r0 = r0.A0G(r4)
            r6.<init>(r9, r0)
            java.lang.String r0 = "com.facebook.showSplashScreen"
            r6.setAction(r0)
            r0 = 262144(0x40000, float:3.67342E-40)
            r6.setFlags(r0)
            android.content.Intent r1 = r9.getIntent()
            java.lang.String r0 = "com.facebook.base.app.originalIntent"
            r6.putExtra(r0, r1)
            long r4 = r9.A01
            java.lang.String r0 = "com.facebook.base.app.rhaId"
            r6.putExtra(r0, r4)
            java.lang.String r0 = "com.facebook.base.app.splashId"
            r6.putExtra(r0, r2)
            X.001 r0 = r9.A05
            java.util.ArrayList r1 = r0.A0b
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            r1.add(r0)
            r0 = 773972459(0x2e21e1eb, float:3.6807817E-11)
            r9.startActivityForResult(r6, r0)
            r9.A04 = r8
            r0 = -295276920(0xffffffffee666e88, float:-1.7828794E28)
            X.C000700l.A07(r0, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.base.app.SplashScreenApplication$RedirectHackActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        int A002 = C000700l.A00(1408584323);
        this.A03 = true;
        this.A05.A0d.remove(this);
        super.onDestroy();
        C000700l.A07(-1883634750, A002);
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (this.A02 == null) {
            this.A02 = new ArrayList();
        }
        this.A02.add(intent);
    }

    public void onPause() {
        int A002 = C000700l.A00(-1838715564);
        super.onPause();
        C000700l.A07(-1551286505, A002);
    }

    public void onResume() {
        int A002 = C000700l.A00(1016517864);
        super.onResume();
        C000700l.A07(1421229067, A002);
    }

    public void onStart() {
        int A002 = C000700l.A00(1268789936);
        super.onStart();
        this.A04 = false;
        C000700l.A07(-639168436, A002);
    }

    public void onStop() {
        int A002 = C000700l.A00(-1440632020);
        this.A04 = true;
        super.onStop();
        C000700l.A07(-1553777951, A002);
    }
}
