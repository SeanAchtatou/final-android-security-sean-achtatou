package com.facebook.base.app;

import X.AnonymousClass001;
import X.AnonymousClass08c;
import X.AnonymousClass0KT;
import X.C000700l;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewTreeObserver;
import java.util.Iterator;

public class SplashScreenActivity extends Activity {
    public int A00;
    public long A01;
    public long A02;
    public boolean A03;
    public boolean A04;

    public static void A00(ViewTreeObserver viewTreeObserver, SplashScreenActivity splashScreenActivity) {
        viewTreeObserver.addOnDrawListener(new AnonymousClass08c(splashScreenActivity, viewTreeObserver));
    }

    public Context getApplicationContext() {
        Context applicationContext = super.getApplicationContext();
        if (!(applicationContext instanceof AnonymousClass001)) {
            return applicationContext.getApplicationContext();
        }
        return applicationContext;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewTreeObserver viewTreeObserver = getWindow().getDecorView().getViewTreeObserver();
        if (Build.VERSION.SDK_INT >= 16) {
            A00(viewTreeObserver, this);
        } else {
            viewTreeObserver.addOnPreDrawListener(new AnonymousClass0KT(this, viewTreeObserver));
        }
    }

    public final void onBackPressed() {
        setResult(1062849428);
        super.onBackPressed();
        ((AnonymousClass001) getApplicationContext()).A0H(7);
    }

    public void onCreate(Bundle bundle) {
        SplashScreenApplication$RedirectHackActivity splashScreenApplication$RedirectHackActivity;
        int A002 = C000700l.A00(-1048628720);
        super.onCreate(null);
        Intent intent = getIntent();
        this.A02 = intent.getLongExtra("com.facebook.base.app.splashId", 0);
        this.A01 = intent.getLongExtra("com.facebook.base.app.rhaId", 0);
        AnonymousClass001 r10 = (AnonymousClass001) getApplicationContext();
        long j = this.A02;
        long j2 = this.A01;
        r10.A0b.remove(Long.valueOf(j));
        Iterator it = r10.A0d.iterator();
        while (true) {
            if (!it.hasNext()) {
                splashScreenApplication$RedirectHackActivity = null;
                break;
            }
            splashScreenApplication$RedirectHackActivity = (SplashScreenApplication$RedirectHackActivity) it.next();
            if (splashScreenApplication$RedirectHackActivity.A01 == j2) {
                break;
            }
        }
        if (splashScreenApplication$RedirectHackActivity == null) {
            finish();
            overridePendingTransition(0, 17432577);
        } else {
            r10.A0c.add(this);
        }
        if (!isFinishing()) {
            this.A00 = 1;
        }
        C000700l.A07(2009630515, A002);
    }

    public void onDestroy() {
        int A002 = C000700l.A00(-1340489811);
        this.A00 = 0;
        ((AnonymousClass001) getApplicationContext()).A0c.remove(this);
        super.onDestroy();
        C000700l.A07(-147085439, A002);
    }

    public void onPause() {
        int A002 = C000700l.A00(1688926624);
        this.A00 = 2;
        ((AnonymousClass001) getApplicationContext()).A0H(4);
        super.onPause();
        C000700l.A07(675034844, A002);
    }

    public void onRestart() {
        int A002 = C000700l.A00(58502865);
        super.onRestart();
        ((AnonymousClass001) getApplicationContext()).A0H(6);
        C000700l.A07(2142645999, A002);
    }

    public void onResume() {
        int A002 = C000700l.A00(-1587993587);
        super.onResume();
        this.A00 = 3;
        ((AnonymousClass001) getApplicationContext()).A0H(3);
        C000700l.A07(-1562040553, A002);
    }

    public void onStart() {
        int A002 = C000700l.A00(472043040);
        super.onStart();
        this.A00 = 2;
        ((AnonymousClass001) getApplicationContext()).A0H(2);
        C000700l.A07(-1758357729, A002);
    }

    public void onStop() {
        int A002 = C000700l.A00(-1540660584);
        this.A00 = 1;
        this.A04 = true;
        super.onStop();
        ((AnonymousClass001) getApplicationContext()).A0H(5);
        C000700l.A07(-342889990, A002);
    }
}
