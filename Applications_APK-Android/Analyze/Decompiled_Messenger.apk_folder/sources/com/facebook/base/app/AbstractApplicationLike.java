package com.facebook.base.app;

import X.AnonymousClass00J;
import X.AnonymousClass00M;
import X.AnonymousClass014;
import X.AnonymousClass05x;
import X.AnonymousClass07A;
import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass09C;
import X.AnonymousClass09Z;
import X.AnonymousClass0Tl;
import X.AnonymousClass0UN;
import X.AnonymousClass0UQ;
import X.AnonymousClass0US;
import X.AnonymousClass0UW;
import X.AnonymousClass0Ud;
import X.AnonymousClass0VM;
import X.AnonymousClass0WE;
import X.AnonymousClass0WP;
import X.AnonymousClass0WR;
import X.AnonymousClass0XV;
import X.AnonymousClass0XZ;
import X.AnonymousClass0Y3;
import X.AnonymousClass0Y7;
import X.AnonymousClass0YZ;
import X.AnonymousClass0jG;
import X.AnonymousClass1XX;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YI;
import X.AnonymousClass1YQ;
import X.AnonymousClass1YR;
import X.AnonymousClass2HG;
import X.C000100f;
import X.C001000r;
import X.C001300x;
import X.C005505z;
import X.C006406k;
import X.C006906p;
import X.C010708t;
import X.C04410Uo;
import X.C04420Up;
import X.C04430Uq;
import X.C04650Vo;
import X.C04800Wf;
import X.C05110Xr;
import X.C05260Yg;
import X.C05350Yp;
import X.C05760aH;
import X.C09110gY;
import X.C09120gZ;
import X.C21877Ajg;
import X.C24781Xb;
import X.C26751bx;
import X.C30414Evp;
import X.C70993be;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.text.TextUtils;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.common.memory.manager.MemoryManager;
import com.facebook.common.util.TriState;
import com.facebook.messaging.filelogger.MessagingFileLogger;
import com.facebook.messenger.app.MessengerApplicationImpl;
import com.facebook.messenger.app.MessengerInstacrashLoopBugReportService;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.base.FinalizableReferenceQueue;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import io.card.payment.BuildConfig;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractApplicationLike extends ApplicationLike implements AnonymousClass09C {
    public AnonymousClass1XX A00;
    public AnonymousClass0UN A01;
    public final Application A02;
    public final AtomicInteger A03 = new AtomicInteger();
    public volatile AnonymousClass0US A04;
    private volatile boolean A05 = false;

    public void A04() {
        int i;
        if (this instanceof MessengerApplicationImpl) {
            MessengerApplicationImpl messengerApplicationImpl = (MessengerApplicationImpl) this;
            if (((AnonymousClass1YI) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AcD, messengerApplicationImpl.A04)).AbO(198, false)) {
                MessagingFileLogger messagingFileLogger = (MessagingFileLogger) AnonymousClass1XX.A02(10, AnonymousClass1Y3.AxJ, messengerApplicationImpl.A04);
                if (C70993be.class.isAssignableFrom(C010708t.A01.getClass())) {
                    try {
                        i = Integer.parseInt(((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, messagingFileLogger.A00)).B4F(AnonymousClass0jG.A02, ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL));
                    } catch (NumberFormatException unused) {
                        i = -1;
                    }
                    if (i == -1) {
                        C010708t.A01(3);
                    } else {
                        C010708t.A01(i);
                    }
                }
            }
            if (AnonymousClass014.A00() > 0 && ((TriState) AnonymousClass1XX.A02(12, AnonymousClass1Y3.Amd, messengerApplicationImpl.A04)) == TriState.YES) {
                C21877Ajg ajg = new C21877Ajg(messengerApplicationImpl.A02);
                C006406k.A02(ajg.A00, new Intent(ajg.A00, MessengerInstacrashLoopBugReportService.class), ajg.A01, 1, -864702977);
            }
            AnonymousClass07A.A02((ExecutorService) AnonymousClass1XX.A02(15, AnonymousClass1Y3.BIG, messengerApplicationImpl.A04), new C26751bx(messengerApplicationImpl), -33262389);
        }
    }

    /* JADX INFO: finally extract failed */
    public void A05(AnonymousClass00M r6) {
        if (this instanceof MessengerApplicationImpl) {
            MessengerApplicationImpl messengerApplicationImpl = (MessengerApplicationImpl) this;
            C005505z.A03("MessengerApplicationImpl.injectMe", 839621905);
            try {
                AnonymousClass1XX r2 = AnonymousClass1XX.get(messengerApplicationImpl.A02);
                messengerApplicationImpl.A04 = new AnonymousClass0UN(18, r2);
                messengerApplicationImpl.A03 = AnonymousClass0WE.A01(r2);
                messengerApplicationImpl.A02 = AnonymousClass0Ud.A00(r2);
                messengerApplicationImpl.A01 = C04430Uq.A02(r2);
                C005505z.A00(1987721962);
                AnonymousClass0Ud r4 = messengerApplicationImpl.A02;
                long j = messengerApplicationImpl.A00;
                C005505z.A03("AppStateManager.notifyApplicationOnCreateComplete", -1344909247);
                try {
                    r4.A0E = j;
                    ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, r4.A02)).C0e(new C04410Uo(r4));
                    ((AnonymousClass0UW) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ALy, r4.A02)).A04(new C04420Up(r4));
                } finally {
                    C005505z.A00(-1459104595);
                }
            } catch (Throwable th) {
                C005505z.A00(182242946);
                throw th;
            }
        }
    }

    public void A01() {
        if (this.A05) {
            MemoryManager.A03((MemoryManager) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A7u, this.A01), false, 15);
        }
    }

    /* JADX INFO: finally extract failed */
    public void A02() {
        String str;
        Throwable th;
        int i;
        ListenableFuture listenableFuture;
        ListenableFuture CIB;
        Logger.getLogger(FinalizableReferenceQueue.class.getName()).setLevel(Level.SEVERE);
        long nanoTime = System.nanoTime();
        AnonymousClass05x.A00();
        try {
            Class.forName("android.os.AsyncTask");
        } catch (ClassNotFoundException e) {
            C010708t.A0M("com.facebook.base.app.AbstractApplicationLike", "Exception trying to initialize AsyncTask", e);
        }
        try {
            this.A02.getSystemService("audio");
        } catch (Resources.NotFoundException | NullPointerException unused) {
        }
        ErrorReporter.putCustomData("app_on_create_count", Integer.toString(this.A03.incrementAndGet()));
        AnonymousClass00M A002 = AnonymousClass00M.A00();
        if (TextUtils.isEmpty(A002.A01)) {
            str = "empty";
        } else {
            str = A002.A01;
        }
        ErrorReporter.putCustomData("process_name_on_start", str);
        synchronized (this) {
            C000100f r4 = A002.A00;
            if (r4 != null) {
                String str2 = r4.A00;
                if (BuildConfig.FLAVOR.equals(str2)) {
                    str2 = "___DEFAULT___";
                }
                try {
                    ImmutableList.of((AnonymousClass0Tl) Class.forName(AnonymousClass08S.A0P("generated_rootmodule.", str2, "ProcessRootModule")).newInstance());
                    Application application = this.A02;
                    if (AnonymousClass1XX.A00 == null) {
                        AnonymousClass1XX.A00 = null;
                        this.A00 = new C24781Xb(application);
                        C005505z.A03("ApplicationLike.onCreate#notifyAll", -336883825);
                        notifyAll();
                        C005505z.A00(1166473119);
                    } else {
                        th = new RuntimeException("you cannot call createForApp twice");
                    }
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e2) {
                    throw new IllegalArgumentException("Invalid process name for getting root module: " + r4, e2);
                } catch (Throwable th2) {
                    th = th2;
                    C005505z.A00(-1007743399);
                }
            } else {
                th = new IllegalStateException("processName has null PrivateProcessName - cannot infer root module");
            }
            throw th;
        }
        AnonymousClass1XX r2 = AnonymousClass1XX.get(this.A02);
        this.A01 = new AnonymousClass0UN(6, r2);
        this.A04 = AnonymousClass0UQ.A00(AnonymousClass1Y3.AZY, r2);
        this.A05 = true;
        int i2 = 3;
        if (((C001300x) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B3a, this.A01)).A01.ordinal() != 2) {
            i2 = 5;
        }
        C010708t.A01(i2);
        A05(A002);
        C005505z.A03("FbAppInitializer.run", 2029337348);
        try {
            AnonymousClass0Y7 r42 = ((AnonymousClass0Y3) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BNi, this.A01)).A00;
            C005505z.A03("FbAppInitializer-ModuleInit", 674242506);
            try {
                int i3 = 0;
                if (((AnonymousClass0WR) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Arq, r42.A01)) != null) {
                    while (true) {
                        AnonymousClass0WR r1 = (AnonymousClass0WR) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Arq, r42.A01);
                        if (i3 >= r1.BKI()) {
                            break;
                        }
                        AnonymousClass1YQ BLp = r1.BLp();
                        if (BLp != null) {
                            C005505z.A03(C05260Yg.A00(BLp.getClass()), 1886706358);
                            BLp.init();
                            C005505z.A00(830443275);
                        }
                        i3++;
                    }
                }
                C005505z.A00(-1750679499);
                ((AnonymousClass1YR) AnonymousClass1XX.A03(AnonymousClass1Y3.AdX, r42.A01)).A01("application_init");
                Preconditions.checkState(!r42.A04, "FbAppInitializer should only be run once.");
                r42.A04 = true;
                if (AnonymousClass0Y7.A07(r42)) {
                    i = AnonymousClass0Y7.A0B + 1;
                } else {
                    i = 2;
                }
                C04650Vo A022 = ((AnonymousClass0VM) AnonymousClass1XX.A02(12, AnonymousClass1Y3.Arj, r42.A01)).A02(i, AnonymousClass0Y7.A0C, "HPINeedInit");
                boolean z = AnonymousClass00J.A01((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r42.A01)).A2W;
                r42.A05 = z;
                if (!z) {
                    C05110Xr r3 = new C05110Xr(r42, A022);
                    if (AnonymousClass00J.A01((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r42.A01)).A1X) {
                        listenableFuture = A022.CIC(r3);
                    } else {
                        listenableFuture = ((AnonymousClass0WP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8a, r42.A01)).CIB("FbAppInitializer-HiPri", r3, AnonymousClass0XV.STARTUP_INITIALIZATION, A022);
                    }
                    int i4 = AnonymousClass1Y3.A3Z;
                    AnonymousClass0UN r32 = r42.A01;
                    C05350Yp.A08(listenableFuture, (C04800Wf) AnonymousClass1XX.A02(5, i4, r32), (Executor) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BS7, r32));
                    AnonymousClass0WR r12 = (AnonymousClass0WR) r42.A03.get();
                    if (r12 != null) {
                        AnonymousClass0Y7.A03(r42, r12, true);
                    }
                    r42.A03 = null;
                    ((AnonymousClass0YZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AZQ, r42.A01)).start();
                } else {
                    AnonymousClass2HG r33 = new AnonymousClass2HG(r42, A022, false);
                    if (AnonymousClass00J.A01((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r42.A01)).A1X) {
                        CIB = A022.CIC(r33);
                    } else {
                        CIB = ((AnonymousClass0WP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8a, r42.A01)).CIB("FbAppInitializer-HiPriWDep", r33, AnonymousClass0XV.STARTUP_INITIALIZATION, A022);
                    }
                    int i5 = AnonymousClass1Y3.A3Z;
                    AnonymousClass0UN r34 = r42.A01;
                    C05350Yp.A08(listenableFuture, (C04800Wf) AnonymousClass1XX.A02(5, i5, r34), (Executor) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BS7, r34));
                    ((AnonymousClass0YZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AZQ, r42.A01)).start();
                }
                C005505z.A00(-626075659);
                this.A04.get();
                long nanoTime2 = (System.nanoTime() - nanoTime) / 1000000;
                AnonymousClass0XZ r6 = (AnonymousClass0XZ) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Amn, this.A01);
                if (r6.A00 == 0) {
                    r6.A00 = r6.A01.now() - nanoTime2;
                }
                C05350Yp.A08(listenableFuture, new C09110gY(this), (Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A6Z, this.A01));
                C001000r.A00(new C006906p((AnonymousClass09Z) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BQ2, this.A01)));
                C001000r.A00(new C09120gZ());
            } catch (Throwable th3) {
                C005505z.A00(570736059);
                throw th3;
            }
        } catch (Throwable th4) {
            C005505z.A00(1721291425);
            throw th4;
        }
    }

    public void A03(int i) {
        Integer num;
        if (this.A05) {
            MemoryManager memoryManager = (MemoryManager) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A7u, this.A01);
            if (i == 5) {
                num = AnonymousClass07B.A01;
            } else if (i == 10) {
                num = AnonymousClass07B.A0C;
            } else if (i == 15) {
                num = AnonymousClass07B.A00;
            } else if (i == 20) {
                num = AnonymousClass07B.A0n;
            } else if (i == 40) {
                num = AnonymousClass07B.A0N;
            } else if (i == 60) {
                num = AnonymousClass07B.A0Y;
            } else if (i != 80) {
                num = null;
            } else {
                num = AnonymousClass07B.A0i;
            }
            if (num != null) {
                for (C30414Evp onTrim$REDEX$0MTKyj5a3W4 : MemoryManager.A01(memoryManager)) {
                    onTrim$REDEX$0MTKyj5a3W4.onTrim$REDEX$0MTKyj5a3W4(num);
                }
            }
            if (MemoryManager.A04(memoryManager, i)) {
                MemoryManager.A03(memoryManager, true, i);
            }
        }
    }

    public Resources Awo() {
        if (this.A04 == null) {
            return null;
        }
        return (C05760aH) this.A04.get();
    }

    public AbstractApplicationLike(Application application, C001300x r3) {
        this.A02 = application;
        C001300x.A07 = r3;
    }
}
