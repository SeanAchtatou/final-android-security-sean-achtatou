package com.facebook.crypto.keychain;

import X.AnonymousClass07B;
import X.AnonymousClass08S;
import X.AnonymousClass16O;
import X.AnonymousClass189;
import X.AnonymousClass18B;
import X.C194418e;
import X.C196018x;
import X.C37911wY;
import X.C97434lG;
import com.facebook.crypto.module.LightSharedPreferencesPersistence;
import java.security.SecureRandom;
import java.util.Arrays;

public class UserStorageKeyChain implements KeyChain {
    public static final C194418e A05 = C194418e.A00("device_key");
    public static final Integer A06 = AnonymousClass07B.A01;
    public static final byte[] A07 = {0, 0, 0, 0};
    public String A00;
    public byte[] A01;
    public final AnonymousClass18B A02;
    public final LightSharedPreferencesPersistence A03;
    public final SecureRandom A04;

    public static void A00(byte[] bArr) {
        if (bArr != null) {
            int length = bArr.length;
            Integer num = A06;
            if (length != AnonymousClass189.A03(num)) {
                throw new C97434lG(AnonymousClass08S.A0B("Incorrect key length: ", length, ". It should be: ", AnonymousClass189.A03(num)), null);
            }
            return;
        }
        throw new C97434lG("Key cannot be null", null);
    }

    public synchronized byte[] getCipherKey() {
        byte[] bArr = this.A01;
        if (bArr != null) {
            if (Arrays.equals(bArr, A07)) {
                byte[] bArr2 = new byte[AnonymousClass189.A03(A06)];
                this.A04.nextBytes(bArr2);
                AnonymousClass16O A062 = this.A03.A00.A06();
                LightSharedPreferencesPersistence.A00(A062, "user_storage_device_key", bArr2);
                A062.A06();
                this.A01 = bArr2;
            }
        } else {
            C37911wY r1 = new C37911wY("Key is not configured");
            A01(r1.getMessage(), r1);
            throw r1;
        }
        return this.A01;
    }

    public void A01(String str, Throwable th) {
        if (this instanceof C196018x) {
            ((C196018x) this).A00.softReport(UserStorageKeyChain.class.getName(), str, th);
        }
    }

    public byte[] getMacKey() {
        throw new UnsupportedOperationException();
    }

    public byte[] getNewIV() {
        byte[] bArr = new byte[AnonymousClass189.A02(AnonymousClass07B.A01)];
        this.A04.nextBytes(bArr);
        return bArr;
    }

    public UserStorageKeyChain(LightSharedPreferencesPersistence lightSharedPreferencesPersistence, AnonymousClass18B r7, String str) {
        int length;
        this.A03 = lightSharedPreferencesPersistence;
        this.A02 = r7;
        this.A00 = str;
        this.A04 = r7.A01;
        if (str != null) {
            byte[] bArr = this.A01;
            if (bArr != null) {
                Arrays.fill(bArr, (byte) 0);
                this.A01 = null;
            }
            byte[] A012 = LightSharedPreferencesPersistence.A01(this.A03, "user_storage_device_key");
            if (!(A012 == null || (length = A012.length) == AnonymousClass189.A03(A06) || Arrays.equals(A012, A07))) {
                AnonymousClass16O A062 = this.A03.A00.A06();
                LightSharedPreferencesPersistence.A00(A062, "user_storage_device_key", null);
                A062.A06();
                A01(AnonymousClass08S.A09("Error loading device key. Length: ", length), null);
                A012 = null;
            }
            if (A012 == null && this.A00 != null) {
                A012 = A07;
                AnonymousClass16O A063 = this.A03.A00.A06();
                LightSharedPreferencesPersistence.A00(A063, "user_storage_device_key", A012);
                A063.A06();
            }
            this.A01 = A012;
        }
    }
}
