package com.facebook.crypto.module;

import X.AnonymousClass01q;
import com.facebook.cipher.jni.CipherHybrid;
import com.facebook.crypto.keychain.KeyChain;
import com.facebook.jni.HybridData;

public class LoggedInUserCryptoHybrid {
    public static LoggedInUserCryptoHybrid sInstance;
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public native CipherHybrid createCipher();

    public native void setKeyChain(KeyChain keyChain);

    public native void unsetKeyChain();

    static {
        AnonymousClass01q.A08("cryptojni");
    }
}
