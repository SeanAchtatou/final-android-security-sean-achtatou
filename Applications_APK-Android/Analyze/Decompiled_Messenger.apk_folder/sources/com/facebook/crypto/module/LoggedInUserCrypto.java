package com.facebook.crypto.module;

import X.AnonymousClass09P;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass18G;
import X.AnonymousClass192;
import X.AnonymousClass1XY;
import X.C04310Tq;
import X.C04750Wa;
import X.C194418e;
import X.C198639Vx;
import X.C200629cN;
import X.C30521iB;
import X.C30821ih;
import X.C37821wJ;
import X.C97434lG;
import android.text.TextUtils;
import com.facebook.crypto.keychain.UserStorageKeyChain;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

public final class LoggedInUserCrypto {
    private static final AnonymousClass18G A05 = AnonymousClass18G.A00.A04();
    public final AnonymousClass09P A00;
    public final UserStorageKeyChain A01;
    private final AnonymousClass192 A02;
    private final List A03 = new ArrayList();
    private final C04310Tq A04;

    @Singleton
    public final class AuthListener {
        private static volatile AuthListener A05;
        public String A00;
        public final C198639Vx A01;
        public final AnonymousClass09P A02;
        public final LoggedInUserCrypto A03;
        public final C04310Tq A04;

        public static final AuthListener A00(AnonymousClass1XY r7) {
            if (A05 == null) {
                synchronized (AuthListener.class) {
                    AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r7);
                    if (A002 != null) {
                        try {
                            AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                            A05 = new AuthListener(C30521iB.A00(applicationInjector), C04750Wa.A01(applicationInjector), AnonymousClass0XJ.A0K(applicationInjector), C198639Vx.A00(applicationInjector));
                            A002.A01();
                        } catch (Throwable th) {
                            A002.A01();
                            throw th;
                        }
                    }
                }
            }
            return A05;
        }

        private AuthListener(LoggedInUserCrypto loggedInUserCrypto, AnonymousClass09P r2, C04310Tq r3, C198639Vx r4) {
            this.A03 = loggedInUserCrypto;
            this.A02 = r2;
            this.A04 = r3;
            this.A01 = r4;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0049, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r7.A00.softReport("com.facebook.crypto.module.LoggedInUserCrypto", "Unexpected error disabling encryption", r3);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A00(com.facebook.crypto.module.LoggedInUserCrypto r7, boolean r8) {
        /*
            monitor-enter(r7)
            com.facebook.crypto.keychain.UserStorageKeyChain r5 = r7.A01     // Catch:{ Exception -> 0x0049 }
            monitor-enter(r5)     // Catch:{ Exception -> 0x0049 }
            byte[] r0 = r5.A01     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0044
            com.facebook.crypto.module.LightSharedPreferencesPersistence r1 = r5.A03     // Catch:{ all -> 0x0046 }
            java.lang.String r0 = r5.A00     // Catch:{ all -> 0x0046 }
            X.9WN r0 = r1.A02(r0)     // Catch:{ all -> 0x0046 }
            byte[] r6 = r0.A01     // Catch:{ all -> 0x0046 }
            r0 = 0
            if (r6 != 0) goto L_0x0016
            r0 = 1
        L_0x0016:
            r4 = 0
            if (r0 != 0) goto L_0x002c
            if (r8 != 0) goto L_0x002c
            com.facebook.crypto.module.LightSharedPreferencesPersistence r0 = r5.A03     // Catch:{ all -> 0x0046 }
            X.0aI r0 = r0.A00     // Catch:{ all -> 0x0046 }
            X.16O r1 = r0.A06()     // Catch:{ all -> 0x0046 }
            java.lang.String r0 = "user_storage_device_key"
            com.facebook.crypto.module.LightSharedPreferencesPersistence.A00(r1, r0, r4)     // Catch:{ all -> 0x0046 }
            r1.A06()     // Catch:{ all -> 0x0046 }
            goto L_0x003a
        L_0x002c:
            com.facebook.crypto.module.LightSharedPreferencesPersistence r3 = r5.A03     // Catch:{ all -> 0x0046 }
            java.lang.String r2 = r5.A00     // Catch:{ all -> 0x0046 }
            X.9WN r1 = new X.9WN     // Catch:{ all -> 0x0046 }
            byte[] r0 = r5.A01     // Catch:{ all -> 0x0046 }
            r1.<init>(r4, r6, r0)     // Catch:{ all -> 0x0046 }
            r3.A03(r2, r1)     // Catch:{ all -> 0x0046 }
        L_0x003a:
            r5.A00 = r4     // Catch:{ all -> 0x0046 }
            byte[] r1 = r5.A01     // Catch:{ all -> 0x0046 }
            r0 = 0
            java.util.Arrays.fill(r1, r0)     // Catch:{ all -> 0x0046 }
            r5.A01 = r4     // Catch:{ all -> 0x0046 }
        L_0x0044:
            monitor-exit(r5)     // Catch:{ Exception -> 0x0049 }
            goto L_0x0053
        L_0x0046:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ Exception -> 0x0049 }
            throw r0     // Catch:{ Exception -> 0x0049 }
        L_0x0049:
            r3 = move-exception
            X.09P r2 = r7.A00     // Catch:{ all -> 0x0070 }
            java.lang.String r1 = "com.facebook.crypto.module.LoggedInUserCrypto"
            java.lang.String r0 = "Unexpected error disabling encryption"
            r2.softReport(r1, r0, r3)     // Catch:{ all -> 0x0070 }
        L_0x0053:
            java.util.List r0 = r7.A03     // Catch:{ all -> 0x0070 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0070 }
        L_0x0059:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0070 }
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0070 }
            X.9cN r0 = (X.C200629cN) r0     // Catch:{ all -> 0x0070 }
            r0.CJz()     // Catch:{ all -> 0x0070 }
            goto L_0x0059
        L_0x0069:
            java.util.List r0 = r7.A03     // Catch:{ all -> 0x0070 }
            r0.clear()     // Catch:{ all -> 0x0070 }
            monitor-exit(r7)
            return
        L_0x0070:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.crypto.module.LoggedInUserCrypto.A00(com.facebook.crypto.module.LoggedInUserCrypto, boolean):void");
    }

    public synchronized C30821ih A02(byte[] bArr, C194418e r4) {
        return new C30821ih(A05(bArr, r4));
    }

    public synchronized void A03(C200629cN r4) {
        boolean z;
        byte[] bArr;
        boolean z2 = false;
        if (this.A04.get() == null) {
            z2 = true;
        }
        if (!z2) {
            UserStorageKeyChain userStorageKeyChain = this.A01;
            synchronized (userStorageKeyChain) {
                z = false;
                if (userStorageKeyChain.A01 != null) {
                    z = true;
                }
            }
            if (z) {
                bArr = this.A01.getCipherKey();
            } else {
                bArr = null;
            }
            r4.C8k(bArr);
            this.A03.add(r4);
        } else {
            throw new RuntimeException("User is not logged in, configure is not available");
        }
    }

    public synchronized boolean A04() {
        boolean z;
        UserStorageKeyChain userStorageKeyChain = this.A01;
        synchronized (userStorageKeyChain) {
            z = false;
            if (userStorageKeyChain.A01 != null) {
                z = true;
            }
        }
        return z;
    }

    public synchronized byte[] A05(byte[] bArr, C194418e r5) {
        if (A04()) {
        } else {
            this.A00.CGS("com.facebook.crypto.module.LoggedInUserCrypto", "UserCrypto not available");
            throw new C37821wJ();
        }
        return this.A02.A00(bArr, r5);
    }

    public synchronized byte[] A06(byte[] bArr, C194418e r5) {
        if (A04()) {
        } else {
            this.A00.CGS("com.facebook.crypto.module.LoggedInUserCrypto", "UserCrypto not available");
            throw new C37821wJ();
        }
        return this.A02.A01(bArr, r5);
    }

    public LoggedInUserCrypto(AnonymousClass192 r2, UserStorageKeyChain userStorageKeyChain, AnonymousClass09P r4, C04310Tq r5) {
        this.A02 = r2;
        this.A01 = userStorageKeyChain;
        this.A00 = r4;
        this.A04 = r5;
    }

    public static byte[] A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return A05.A08(str);
        } catch (IllegalArgumentException e) {
            throw new C97434lG("Incorrect key, invalid hex", e);
        }
    }
}
