package com.facebook.crypto.module;

import X.AnonymousClass08S;
import X.AnonymousClass09P;
import X.AnonymousClass0XQ;
import X.AnonymousClass16O;
import X.AnonymousClass18G;
import X.AnonymousClass9WN;
import X.C05770aI;
import io.card.payment.BuildConfig;

public final class LightSharedPreferencesPersistence {
    private static final AnonymousClass18G A02 = AnonymousClass18G.A00.A04();
    public final C05770aI A00;
    private final AnonymousClass09P A01;

    public static void A00(AnonymousClass16O r1, String str, byte[] bArr) {
        if (bArr == null) {
            r1.A08(str);
        } else {
            r1.A0B(str, A02.A06(bArr));
        }
    }

    public static byte[] A01(LightSharedPreferencesPersistence lightSharedPreferencesPersistence, String str) {
        String A07 = lightSharedPreferencesPersistence.A00.A07(str, BuildConfig.FLAVOR);
        if (A07.isEmpty()) {
            return null;
        }
        try {
            return A02.A08(A07);
        } catch (IllegalArgumentException unused) {
            lightSharedPreferencesPersistence.A01.CGS("com.facebook.crypto.module.LightSharedPreferencesPersistence", AnonymousClass08S.A0S("Error loading hex key, ", str, " = ", A07));
            AnonymousClass16O A06 = lightSharedPreferencesPersistence.A00.A06();
            A06.A08(str);
            A06.A06();
            return null;
        }
    }

    public AnonymousClass9WN A02(String str) {
        String A0J = AnonymousClass08S.A0J("user_storage_encrypted_key.", str);
        return new AnonymousClass9WN(null, A01(this, A0J), A01(this, AnonymousClass08S.A0J("user_storage_not_encrypted_key.", str)));
    }

    public void A03(String str, AnonymousClass9WN r7) {
        String A0J = AnonymousClass08S.A0J("user_storage_encrypted_key.", str);
        String A0J2 = AnonymousClass08S.A0J("user_storage_not_encrypted_key.", str);
        AnonymousClass16O A06 = this.A00.A06();
        A00(A06, "user_storage_device_key", r7.A00);
        A00(A06, A0J, r7.A01);
        A00(A06, A0J2, r7.A02);
        A06.A06();
    }

    public LightSharedPreferencesPersistence(AnonymousClass0XQ r2, AnonymousClass09P r3) {
        this.A00 = r2.A00("user_storage_device_key");
        this.A01 = r3;
    }
}
