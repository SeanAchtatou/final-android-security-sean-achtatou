package com.facebook.logcatinterceptor;

import X.AnonymousClass01q;

public final class LogcatInterceptor {
    public static volatile boolean sInstalled;

    private static native String nativeGetLogcatContents();

    public static native void nativeInstall(String str, int i, int i2);

    public static native void nativeIntegrateWithBreakpad(boolean z);

    static {
        AnonymousClass01q.A08("logcat-interceptor");
    }

    public static String getLogcatContents() {
        if (sInstalled) {
            return nativeGetLogcatContents();
        }
        throw new RuntimeException("Logcat interceptor not installed");
    }

    public static synchronized boolean isInstalled() {
        boolean z;
        synchronized (LogcatInterceptor.class) {
            z = sInstalled;
        }
        return z;
    }
}
