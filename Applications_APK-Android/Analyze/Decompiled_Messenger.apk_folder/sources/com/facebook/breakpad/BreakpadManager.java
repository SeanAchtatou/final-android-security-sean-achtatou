package com.facebook.breakpad;

import java.io.File;
import java.util.Formatter;
import java.util.TreeMap;

public class BreakpadManager {
    public static volatile File mCrashDirectory;
    public static String mNativeLibraryName;

    public static native boolean containsKey(String str);

    public static native void crashProcessByAssert(String str);

    public static native void crashThisProcess();

    public static native void crashThisProcessAsan();

    public static native boolean disableCoreDumpingImpl();

    private static native boolean enableCoreDumpingImpl(String str);

    public static native String getCustomData(String str);

    public static native long getMinidumpFlags();

    public static native void install(String str, String str2, String str3, int i);

    private static native boolean isCoreResourceHardUnlimited();

    public static native boolean isPrivacyModeEnabled();

    public static native void nativeGetCustomDataSnapshot(TreeMap treeMap);

    private static native void nativeSetCustomData(String str, String str2);

    public static native boolean nativeSetJvmStreamEnabled(boolean z, boolean z2);

    public static native void removeCustomData(String str);

    public static void setCustomData(String str, String str2, Object... objArr) {
        if (objArr.length > 0 && str2 != null) {
            StringBuilder sb = new StringBuilder();
            Formatter formatter = new Formatter(sb);
            formatter.format(str2, objArr);
            formatter.close();
            str2 = sb.toString();
        }
        nativeSetCustomData(str, str2);
    }

    public static native void setMinidumpFlags(long j);

    public static native void setVersionInfo(int i, String str);

    public static native boolean simulateSignalDelivery(int i, String str);

    public static native void uninstall();

    public static File getCrashDirectory() {
        if (mCrashDirectory != null) {
            return mCrashDirectory;
        }
        throw new RuntimeException("Breakpad not installed");
    }

    public static boolean isActive() {
        if (mCrashDirectory != null) {
            return true;
        }
        return false;
    }
}
