package com.facebook.acra.anr.sigquit;

import X.AnonymousClass00S;
import android.os.Build;
import android.os.Handler;
import com.facebook.acra.anr.ANRDetectorConfig;

public class SigquitDetector {
    private static SigquitDetector sInstance;
    private static boolean sIsArt;
    public SigquitDetectorListener mListener;
    private Handler mMainThreadHandler;

    public static native void nativeAddSignalHandler();

    public static native void nativeCleanupAppStateFile();

    public static native boolean nativeHookMethods();

    private static native void nativeInit(Object obj, boolean z, int i, String str, String str2, String str3, String str4, boolean z2, boolean z3, boolean z4, boolean z5, String str5, boolean z6, boolean z7);

    public static native void nativeStartDetector();

    public static native void nativeStopDetector();

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r1.startsWith("0.") != false) goto L_0x0019;
     */
    static {
        /*
            java.lang.String r0 = "java.vm.version"
            java.lang.String r1 = java.lang.System.getProperty(r0)
            if (r1 == 0) goto L_0x0019
            java.lang.String r0 = "1."
            boolean r0 = r1.startsWith(r0)
            if (r0 != 0) goto L_0x0019
            java.lang.String r0 = "0."
            boolean r1 = r1.startsWith(r0)
            r0 = 1
            if (r1 == 0) goto L_0x001a
        L_0x0019:
            r0 = 0
        L_0x001a:
            com.facebook.acra.anr.sigquit.SigquitDetector.sIsArt = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.sigquit.SigquitDetector.<clinit>():void");
    }

    public static SigquitDetector getInstance(SigquitDetectorListener sigquitDetectorListener) {
        if (sInstance == null) {
            sInstance = new SigquitDetector(sigquitDetectorListener);
        }
        return sInstance;
    }

    private static void onSigquitDetected(String str, String str2, boolean z, boolean z2) {
        sInstance.sigquitDetected(str, str2, z, z2);
    }

    private void sigquitDetected(String str, String str2, boolean z, boolean z2) {
        this.mListener.sigquitDetected(str, str2, z, z2);
    }

    public void init(ANRDetectorConfig aNRDetectorConfig) {
        ANRDetectorConfig aNRDetectorConfig2 = aNRDetectorConfig;
        this.mMainThreadHandler = aNRDetectorConfig2.mMainThreadHandler;
        nativeInit(this, sIsArt, Build.VERSION.SDK_INT, aNRDetectorConfig2.mAppVersionCode, aNRDetectorConfig2.mAppVersionName, aNRDetectorConfig2.mTracesPath, aNRDetectorConfig2.mTracesExtension, aNRDetectorConfig2.mShouldReportSoftErrors, aNRDetectorConfig2.mShouldLogOnSignalHandler, aNRDetectorConfig2.mShouldAvoidMutexOnSignalHandler, aNRDetectorConfig2.mUseStaticMethodCallback, aNRDetectorConfig2.getSigquitTimeFilePath(), aNRDetectorConfig2.mShouldRecordSignalTime, aNRDetectorConfig2.processShouldSendAnrReports());
    }

    public void installSignalHandler(final Handler handler, final boolean z) {
        final AnonymousClass1 r0 = new Runnable() {
            public void run() {
                boolean nativeHookMethods = SigquitDetector.nativeHookMethods();
                SigquitDetector.this.mListener.onHookedMethods(nativeHookMethods);
                if (nativeHookMethods && z) {
                    SigquitDetector.nativeStartDetector();
                }
            }
        };
        AnonymousClass00S.A04(this.mMainThreadHandler, new Runnable() {
            public void run() {
                SigquitDetector.nativeAddSignalHandler();
                AnonymousClass00S.A04(handler, r0, -376972743);
            }
        }, -2014004933);
    }

    private SigquitDetector(SigquitDetectorListener sigquitDetectorListener) {
        this.mListener = sigquitDetectorListener;
    }

    public void cleanupAppStateFile() {
        nativeCleanupAppStateFile();
    }

    public void startDetector() {
        nativeStartDetector();
    }

    public void stopDetector() {
        nativeStopDetector();
    }
}
