package com.facebook.acra.anr.base;

import X.AnonymousClass04f;
import X.AnonymousClass0OR;
import X.AnonymousClass0OS;
import android.os.Debug;
import android.os.Looper;
import android.os.SystemClock;
import com.facebook.acra.anr.ANRDataProvider;
import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.ANRDetectorListener;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anr.IANRReport;
import java.io.File;

public abstract class AbstractANRDetector implements IANRDetector {
    private static final String LOG_TAG = "AbstractANRDetector";
    public final ANRDetectorConfig mANRConfig;
    private ANRDataProvider mANRDataProvider;
    private long mANRReportTime;
    public ANRDetectorListener mAnrDetectorListener;
    public long mDetectorStartTime;
    private boolean mInAnr;
    public volatile boolean mInForegroundV1;
    public volatile boolean mInForegroundV2;
    private final Object mStateLock = new Object();

    public void collectStackTrace() {
    }

    public long getReadyTime() {
        return 0;
    }

    public long getSwitchTime() {
        return 0;
    }

    public void nativeLibraryLoaded(boolean z) {
    }

    public abstract void notifyStateListeners(AnonymousClass04f r1);

    public void setCheckIntervalMs(long j) {
    }

    public synchronized void setListener(ANRDetectorListener aNRDetectorListener) {
        this.mAnrDetectorListener = aNRDetectorListener;
    }

    public abstract void start(long j);

    public abstract void stop(IANRDetector.ANRDetectorStopListener aNRDetectorStopListener);

    public void anrHasEnded(boolean z) {
        if (z) {
            this.mANRConfig.mANRReport.finalizeAndTryToSendReport(SystemClock.uptimeMillis() - this.mANRReportTime);
        }
    }

    public boolean inAnrState() {
        boolean z;
        synchronized (this.mStateLock) {
            z = this.mInAnr;
        }
        return z;
    }

    public boolean isDebuggerConnected() {
        if (!this.mANRConfig.mIsInternalBuild || !Debug.isDebuggerConnected()) {
            return false;
        }
        return true;
    }

    public void logMainThreadUnblocked(long j) {
        this.mANRConfig.mANRReport.logMainThreadUnblocked(j);
    }

    public void reportSoftError(String str, Throwable th) {
        ANRDataProvider aNRDataProvider = this.mANRDataProvider;
        if (aNRDataProvider != null) {
            aNRDataProvider.reportSoftError(str, th);
        }
    }

    public void setInAnrState(boolean z) {
        synchronized (this.mStateLock) {
            this.mInAnr = z;
        }
    }

    public boolean shouldCollectAndUploadANRReports() {
        ANRDataProvider aNRDataProvider = this.mANRDataProvider;
        if (aNRDataProvider != null) {
            return aNRDataProvider.shouldCollectAndUploadANRReports();
        }
        return this.mANRConfig.mCachedShouldUploadANRReports;
    }

    public AbstractANRDetector(ANRDetectorConfig aNRDetectorConfig) {
        this.mANRConfig = aNRDetectorConfig;
    }

    public static boolean isTest() {
        return false;
    }

    public ANRDetectorConfig getANRConfig() {
        return this.mANRConfig;
    }

    public ANRDataProvider getANRDataProvider() {
        return this.mANRDataProvider;
    }

    public long getDetectorStartTime() {
        return this.mDetectorStartTime;
    }

    public boolean isInForegroundV1() {
        return this.mInForegroundV1;
    }

    public void processMonitorStarted() {
        if (shouldCollectAndUploadANRReportsNow()) {
            this.mANRConfig.mANRReport.logProcessMonitorStart(SystemClock.uptimeMillis());
        }
    }

    public void processMonitorStopped(int i) {
        if (shouldCollectAndUploadANRReportsNow()) {
            this.mANRConfig.mANRReport.logProcessMonitorFailure(SystemClock.uptimeMillis(), i);
        }
    }

    public void setInAnrStateOnAppStateUpdater() {
    }

    public boolean shouldCollectAndUploadANRReportsNow() {
        if (!shouldCollectAndUploadANRReports()) {
            return false;
        }
        if (this.mInForegroundV2 || this.mInForegroundV1) {
            return true;
        }
        return false;
    }

    public void setANRDataProvider(ANRDataProvider aNRDataProvider) {
        this.mANRDataProvider = aNRDataProvider;
    }

    public void start() {
        start(-1);
    }

    public void startReport() {
        startReport(null, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void startReport(String str, String str2, Long l) {
        ANRDetectorListener aNRDetectorListener;
        String str3;
        String str4;
        synchronized (this) {
            aNRDetectorListener = this.mAnrDetectorListener;
        }
        File file = null;
        if (aNRDetectorListener != null) {
            str3 = aNRDetectorListener.getBlackBoxTraceId();
            str4 = aNRDetectorListener.getLongStallTraceId();
            aNRDetectorListener.onStartANRDataCapture();
        } else {
            str3 = null;
            str4 = null;
        }
        collectStackTrace();
        this.mANRReportTime = SystemClock.uptimeMillis();
        ANRDetectorConfig aNRDetectorConfig = this.mANRConfig;
        IANRReport iANRReport = aNRDetectorConfig.mANRReport;
        boolean z = false;
        if (this.mANRDataProvider == null) {
            z = true;
        }
        int i = aNRDetectorConfig.mDetectorId;
        boolean z2 = this.mInForegroundV1;
        boolean z3 = this.mInForegroundV2;
        long j = this.mANRReportTime;
        long j2 = this.mDetectorStartTime;
        long readyTime = getReadyTime();
        long switchTime = getSwitchTime();
        ANRDetectorConfig aNRDetectorConfig2 = this.mANRConfig;
        if (aNRDetectorConfig2.mShouldRecordSignalTime) {
            file = aNRDetectorConfig2.getSigquitTimeDir();
        }
        iANRReport.startReport(z, str3, str4, i, z2, z3, j, j2, readyTime, switchTime, str, str2, file, this.mANRConfig.mProcessName.replace('.', '_').replace(':', '_'), l);
        Looper mainLooper = Looper.getMainLooper();
        synchronized (AnonymousClass0OS.class) {
            AnonymousClass0OR r0 = (AnonymousClass0OR) AnonymousClass0OS.A00.get(mainLooper);
            if (r0 != null) {
                r0.dumpToLogCat();
            }
        }
        if (aNRDetectorListener != null) {
            aNRDetectorListener.onEndANRDataCapture();
        }
    }
}
