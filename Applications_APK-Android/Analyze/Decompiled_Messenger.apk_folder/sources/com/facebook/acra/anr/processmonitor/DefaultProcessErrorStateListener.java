package com.facebook.acra.anr.processmonitor;

public class DefaultProcessErrorStateListener implements ProcessErrorStateListener {
    public void onCheckFailed() {
    }

    public void onCheckPerformed() {
    }

    public void onErrorCleared() {
    }

    public boolean onErrorDetectOnOtherProcess(String str, String str2, String str3) {
        return true;
    }

    public void onErrorDetected(String str, String str2) {
    }

    public void onMaxChecksReachedAfterError() {
    }

    public void onMaxChecksReachedBeforeError() {
    }

    public void onStart() {
    }
}
