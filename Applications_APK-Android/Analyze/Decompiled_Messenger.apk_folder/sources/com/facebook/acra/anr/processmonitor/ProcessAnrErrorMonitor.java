package com.facebook.acra.anr.processmonitor;

import X.C010708t;
import android.app.ActivityManager;
import android.content.Context;
import android.os.DeadObjectException;
import android.os.Process;
import com.facebook.acra.ACRA;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ProcessAnrErrorMonitor {
    public static final int DEFAULT_POLLING_TIME_MS = 500;
    public static final String LOG_TAG = "ProcessAnrErrorMonitor";
    public static final int UNLIMITED_NUMBER_OF_CHECKS = 0;
    private final Context mContext;
    public final boolean mContinuousMonitoring;
    private long mCurrentMonitorThreadId;
    private MonitorThread mErrorCheckThread;
    public final int mMaxNumberOfChecksAfterError;
    public final int mMaxNumberOfChecksBeforeError;
    private final int mMyUid;
    public final int mPollingTime;
    public final String mProcessName;
    private State mState;

    public class AnrCheckState {
        public boolean mAnrConfirmed;
        public int mCount;
    }

    public class AnrErrorState {
        public String mErrorMsg;
        public String mProcessName;
        public String mTag;
    }

    public class MonitorThread extends Thread {
        private final ActivityManager mAm;
        private final long mDelay;
        private boolean mFirstCheck;
        public final long mId;
        private volatile ProcessErrorStateListener mListener;
        private final Object mMonitorLock;
        private boolean mPauseRequested;
        private final Set mProcessesInAnr;
        private boolean mStopRequested;

        public boolean checkIteration(AnrCheckState anrCheckState) {
            boolean z = true;
            try {
                LinkedList checkProcessError = ProcessAnrErrorMonitor.this.checkProcessError(this.mAm);
                if (this.mFirstCheck) {
                    C010708t.A0P(ProcessAnrErrorMonitor.LOG_TAG, "Starting process monitor checks for process '%s'", ProcessAnrErrorMonitor.this.mProcessName);
                    this.mFirstCheck = false;
                    ProcessAnrErrorMonitor.this.updateStateAndMaybeCallListener(StateChangeReason.MONITOR_STARTED, this.mListener);
                }
                AnrErrorState anrErrorState = null;
                if (!checkProcessError.isEmpty()) {
                    AnrErrorState anrErrorState2 = (AnrErrorState) checkProcessError.getFirst();
                    if (anrErrorState2.mProcessName.equals(ProcessAnrErrorMonitor.this.mProcessName)) {
                        anrErrorState = anrErrorState2;
                    }
                }
                if (anrErrorState != null && !anrCheckState.mAnrConfirmed) {
                    anrCheckState.mAnrConfirmed = true;
                    anrCheckState.mCount = 0;
                    C010708t.A0P(ProcessAnrErrorMonitor.LOG_TAG, "ANR detected Short msg: %s Tag: %s", anrErrorState.mErrorMsg, anrErrorState.mTag);
                    ProcessAnrErrorMonitor.this.updateStateAndMaybeCallListener(StateChangeReason.ERROR_DETECTED, this.mListener, anrErrorState.mErrorMsg, anrErrorState.mTag);
                } else if (anrErrorState != null || !anrCheckState.mAnrConfirmed) {
                    if (anrErrorState != null || anrCheckState.mAnrConfirmed) {
                        int i = anrCheckState.mCount + 1;
                        anrCheckState.mCount = i;
                        ProcessAnrErrorMonitor processAnrErrorMonitor = ProcessAnrErrorMonitor.this;
                        int i2 = processAnrErrorMonitor.mMaxNumberOfChecksAfterError;
                        if (i2 > 0 && i >= i2) {
                            processAnrErrorMonitor.updateStateAndMaybeCallListener(StateChangeReason.MAX_NUMBER_AFTER_ERROR, this.mListener);
                            C010708t.A0P(ProcessAnrErrorMonitor.LOG_TAG, "Stopping checks for '%s' because of MAX_NUMBER_AFTER_ERROR", ProcessAnrErrorMonitor.this.mProcessName);
                        }
                    } else {
                        int i3 = anrCheckState.mCount + 1;
                        anrCheckState.mCount = i3;
                        ProcessAnrErrorMonitor processAnrErrorMonitor2 = ProcessAnrErrorMonitor.this;
                        int i4 = processAnrErrorMonitor2.mMaxNumberOfChecksBeforeError;
                        if (i4 > 0 && i3 >= i4) {
                            processAnrErrorMonitor2.updateStateAndMaybeCallListener(StateChangeReason.MAX_NUMBER_BEFORE_ERROR, this.mListener);
                            C010708t.A0P(ProcessAnrErrorMonitor.LOG_TAG, "Stopping checks for '%s' because of MAX_NUMBER_BEFORE_ERROR", ProcessAnrErrorMonitor.this.mProcessName);
                        }
                    }
                    z = false;
                } else {
                    C010708t.A0J(ProcessAnrErrorMonitor.LOG_TAG, "On error cleared");
                    ProcessAnrErrorMonitor.this.updateStateAndMaybeCallListener(StateChangeReason.ERROR_CLEARED, this.mListener);
                    z = ProcessAnrErrorMonitor.this.mContinuousMonitoring;
                    if (z) {
                        anrCheckState.mAnrConfirmed = false;
                        anrCheckState.mCount = 0;
                    }
                }
                if (!checkProcessError.isEmpty()) {
                    maybeLogAnrStateFromOtherProcesses(checkProcessError, ProcessAnrErrorMonitor.this.mProcessName);
                }
                maybeCallIterationListener();
                return z;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof DeadObjectException) {
                    ProcessAnrErrorMonitor.this.updateStateAndMaybeCallListener(StateChangeReason.ERROR_QUERYING_ACTIVITY_MANAGER, this.mListener);
                    C010708t.A0O(ProcessAnrErrorMonitor.LOG_TAG, "Stopping checks for '%s' because of ERROR_QUERYING_ACTIVITY_MANAGER", ProcessAnrErrorMonitor.this.mProcessName, e);
                    return false;
                }
                throw e;
            }
        }

        private void maybeCallIterationListener() {
            if (this.mListener != null) {
                this.mListener.onCheckPerformed();
            }
        }

        private void maybeLogAnrStateFromOtherProcesses(LinkedList linkedList, String str) {
            if (this.mListener != null) {
                Iterator it = linkedList.iterator();
                while (it.hasNext()) {
                    AnrErrorState anrErrorState = (AnrErrorState) it.next();
                    String str2 = anrErrorState.mProcessName;
                    if (!str2.equals(str)) {
                        C010708t.A0P(ProcessAnrErrorMonitor.LOG_TAG, "Error found in process '%s' different from process being searched '%s'", str2, str);
                        if (!this.mProcessesInAnr.contains(anrErrorState.mProcessName) && this.mListener.onErrorDetectOnOtherProcess(anrErrorState.mProcessName, anrErrorState.mErrorMsg, anrErrorState.mTag)) {
                            this.mProcessesInAnr.add(anrErrorState.mProcessName);
                        }
                    }
                }
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:52|(1:54)|55|56|57|58|(1:60)(1:81)) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
            if (r13.mListener == null) goto L_0x001f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x003a, code lost:
            if (r13.mListener == null) goto L_0x003c;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x007f */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x002a A[SYNTHETIC, Splitter:B:18:0x002a] */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0078 A[Catch:{ all -> 0x0099 }] */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0055 A[EDGE_INSN: B:74:0x0055->B:37:0x0055 ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x0028 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void monitorLoop() {
            /*
                r13 = this;
                long r1 = r13.mDelay
                r11 = 0
                r6 = 0
                int r0 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
                if (r0 > 0) goto L_0x000d
                com.facebook.acra.anr.processmonitor.ProcessErrorStateListener r0 = r13.mListener
                if (r0 != 0) goto L_0x005a
            L_0x000d:
                long r3 = r13.mDelay
                java.lang.Object r5 = r13.mMonitorLock
                monitor-enter(r5)
                int r0 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
                if (r0 <= 0) goto L_0x001b
                boolean r0 = r13.mStopRequested     // Catch:{ all -> 0x0057 }
                if (r0 != 0) goto L_0x0021
                goto L_0x001f
            L_0x001b:
                com.facebook.acra.anr.processmonitor.ProcessErrorStateListener r0 = r13.mListener     // Catch:{ all -> 0x0057 }
                if (r0 != 0) goto L_0x0021
            L_0x001f:
                r10 = 1
                goto L_0x0022
            L_0x0021:
                r10 = 0
            L_0x0022:
                boolean r9 = r13.mStopRequested     // Catch:{ all -> 0x0057 }
                long r7 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0057 }
            L_0x0028:
                if (r10 == 0) goto L_0x0055
                java.lang.Object r0 = r13.mMonitorLock     // Catch:{ InterruptedException -> 0x002f }
                r0.wait(r3)     // Catch:{ InterruptedException -> 0x002f }
            L_0x002f:
                int r0 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
                if (r0 <= 0) goto L_0x0038
                boolean r0 = r13.mStopRequested     // Catch:{ all -> 0x0057 }
                if (r0 != 0) goto L_0x003e
                goto L_0x003c
            L_0x0038:
                com.facebook.acra.anr.processmonitor.ProcessErrorStateListener r0 = r13.mListener     // Catch:{ all -> 0x0057 }
                if (r0 != 0) goto L_0x003e
            L_0x003c:
                r10 = 1
                goto L_0x003f
            L_0x003e:
                r10 = 0
            L_0x003f:
                boolean r9 = r13.mStopRequested     // Catch:{ all -> 0x0057 }
                if (r10 == 0) goto L_0x0028
                int r0 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
                if (r0 <= 0) goto L_0x0028
                long r3 = r13.mDelay     // Catch:{ all -> 0x0057 }
                long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0057 }
                long r0 = r0 - r7
                long r3 = r3 - r0
                r1 = 1
                int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
                if (r0 >= 0) goto L_0x0028
            L_0x0055:
                monitor-exit(r5)     // Catch:{ all -> 0x0057 }
                goto L_0x0096
            L_0x0057:
                r0 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0057 }
                goto L_0x009b
            L_0x005a:
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$AnrCheckState r5 = new com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$AnrCheckState
                r5.<init>()
                r5.mAnrConfirmed = r6
                r5.mCount = r6
            L_0x0063:
                boolean r0 = r13.checkIteration(r5)
                if (r0 == 0) goto L_0x0098
                java.lang.Object r4 = r13.mMonitorLock
                monitor-enter(r4)
                boolean r0 = r13.mStopRequested     // Catch:{ all -> 0x0099 }
                if (r0 != 0) goto L_0x0089
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.this     // Catch:{ all -> 0x0099 }
                int r3 = r0.mPollingTime     // Catch:{ all -> 0x0099 }
            L_0x0074:
                boolean r0 = r13.mPauseRequested     // Catch:{ all -> 0x0099 }
                if (r0 == 0) goto L_0x0079
                r3 = 0
            L_0x0079:
                java.lang.Object r2 = r13.mMonitorLock     // Catch:{ InterruptedException -> 0x007f }
                long r0 = (long) r3     // Catch:{ InterruptedException -> 0x007f }
                r2.wait(r0)     // Catch:{ InterruptedException -> 0x007f }
            L_0x007f:
                boolean r0 = r13.mPauseRequested     // Catch:{ all -> 0x0099 }
                if (r0 == 0) goto L_0x0087
                boolean r0 = r13.mStopRequested     // Catch:{ all -> 0x0099 }
                if (r0 == 0) goto L_0x0074
            L_0x0087:
                boolean r0 = r13.mStopRequested     // Catch:{ all -> 0x0099 }
            L_0x0089:
                monitor-exit(r4)     // Catch:{ all -> 0x0099 }
                if (r0 == 0) goto L_0x0063
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor r2 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.this
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r1 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.STOP_REQUESTED
                com.facebook.acra.anr.processmonitor.ProcessErrorStateListener r0 = r13.mListener
                r2.updateStateAndMaybeCallListener(r1, r0)
                return
            L_0x0096:
                if (r9 == 0) goto L_0x005a
            L_0x0098:
                return
            L_0x0099:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0099 }
            L_0x009b:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.MonitorThread.monitorLoop():void");
        }

        public static void pauseRequested(MonitorThread monitorThread) {
            synchronized (monitorThread.mMonitorLock) {
                monitorThread.mPauseRequested = true;
                monitorThread.mMonitorLock.notifyAll();
            }
        }

        public static void resumeRequested(MonitorThread monitorThread) {
            synchronized (monitorThread.mMonitorLock) {
                monitorThread.mPauseRequested = false;
                monitorThread.mMonitorLock.notifyAll();
            }
        }

        public static void stopRequested(MonitorThread monitorThread) {
            synchronized (monitorThread.mMonitorLock) {
                monitorThread.mStopRequested = true;
                monitorThread.mMonitorLock.notifyAll();
            }
        }

        public boolean hasListener() {
            if (this.mListener != null) {
                return true;
            }
            return false;
        }

        public void setListener(ProcessErrorStateListener processErrorStateListener) {
            synchronized (this.mMonitorLock) {
                this.mListener = processErrorStateListener;
                this.mMonitorLock.notifyAll();
            }
        }

        public long getMonitorId() {
            return this.mId;
        }

        public void run() {
            monitorLoop();
        }

        public MonitorThread(ActivityManager activityManager, ProcessErrorStateListener processErrorStateListener, long j, long j2) {
            super("ProcessAnrErrorMonitorThread");
            this.mMonitorLock = new Object();
            this.mProcessesInAnr = new HashSet();
            this.mAm = activityManager;
            this.mListener = processErrorStateListener;
            this.mId = j;
            this.mDelay = j2;
            this.mFirstCheck = true;
        }

        public /* synthetic */ MonitorThread(ProcessAnrErrorMonitor processAnrErrorMonitor, ActivityManager activityManager, ProcessErrorStateListener processErrorStateListener, long j, long j2, AnonymousClass1 r8) {
            this(activityManager, processErrorStateListener, j, j2);
        }
    }

    public enum State {
        NOT_MONITORING,
        MONITORING_NO_ERROR_DETECTED,
        MONITORING_ERROR_DETECTED
    }

    public enum StateChangeReason {
        MONITOR_STARTED,
        ERROR_CLEARED,
        ERROR_DETECTED,
        MAX_NUMBER_BEFORE_ERROR,
        MAX_NUMBER_AFTER_ERROR,
        STOP_REQUESTED,
        ERROR_QUERYING_ACTIVITY_MANAGER
    }

    public synchronized MonitorThread getErrorCheckThread() {
        return this.mErrorCheckThread;
    }

    public synchronized State getState() {
        return this.mState;
    }

    public void pause() {
        synchronized (this) {
            if (this.mState != State.NOT_MONITORING) {
                MonitorThread.pauseRequested(this.mErrorCheckThread);
            }
        }
    }

    public void resume() {
        synchronized (this) {
            if (this.mState != State.NOT_MONITORING) {
                MonitorThread.resumeRequested(this.mErrorCheckThread);
            }
        }
    }

    public synchronized void setErrorCheckThread(MonitorThread monitorThread) {
        this.mErrorCheckThread = monitorThread;
    }

    public void stopMonitoring() {
        synchronized (this) {
            if (this.mState != State.NOT_MONITORING) {
                MonitorThread.stopRequested(this.mErrorCheckThread);
            }
        }
    }

    /* renamed from: com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$1  reason: invalid class name */
    public /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$com$facebook$acra$anr$processmonitor$ProcessAnrErrorMonitor$StateChangeReason;

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0036 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0024 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x002d */
        static {
            /*
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason[] r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.values()
                int r0 = r0.length
                int[] r2 = new int[r0]
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.AnonymousClass1.$SwitchMap$com$facebook$acra$anr$processmonitor$ProcessAnrErrorMonitor$StateChangeReason = r2
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.MONITOR_STARTED     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0 = 1
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.ERROR_DETECTED     // Catch:{ NoSuchFieldError -> 0x001b }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x001b }
                r0 = 2
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x001b }
            L_0x001b:
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.ERROR_CLEARED     // Catch:{ NoSuchFieldError -> 0x0024 }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0024 }
                r0 = 3
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0024 }
            L_0x0024:
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.MAX_NUMBER_AFTER_ERROR     // Catch:{ NoSuchFieldError -> 0x002d }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x002d }
                r0 = 4
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x002d }
            L_0x002d:
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.MAX_NUMBER_BEFORE_ERROR     // Catch:{ NoSuchFieldError -> 0x0036 }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0036 }
                r0 = 5
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0036 }
            L_0x0036:
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.STOP_REQUESTED     // Catch:{ NoSuchFieldError -> 0x003f }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x003f }
                r0 = 6
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x003f }
            L_0x003f:
                com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor$StateChangeReason r0 = com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.StateChangeReason.ERROR_QUERYING_ACTIVITY_MANAGER     // Catch:{ NoSuchFieldError -> 0x0048 }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
                r0 = 7
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
            L_0x0048:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor.AnonymousClass1.<clinit>():void");
        }
    }

    public void startMonitoring(ProcessErrorStateListener processErrorStateListener) {
        startMonitoringAfterDelay(processErrorStateListener, 0);
    }

    public void startMonitoringAfterDelay(ProcessErrorStateListener processErrorStateListener, long j) {
        IllegalArgumentException illegalArgumentException;
        long j2 = j;
        C010708t.A0P(LOG_TAG, "startMonitoring with delay: %d", Long.valueOf(j));
        ActivityManager activityManager = (ActivityManager) this.mContext.getSystemService("activity");
        synchronized (this) {
            ProcessErrorStateListener processErrorStateListener2 = processErrorStateListener;
            if (processErrorStateListener != null || j == 0) {
                MonitorThread monitorThread = this.mErrorCheckThread;
                if (monitorThread == null || monitorThread.hasListener()) {
                    if (this.mState != State.NOT_MONITORING) {
                        MonitorThread.stopRequested(this.mErrorCheckThread);
                    }
                    long j3 = this.mCurrentMonitorThreadId + 1;
                    this.mCurrentMonitorThreadId = j3;
                    MonitorThread monitorThread2 = new MonitorThread(activityManager, processErrorStateListener2, j3, j2);
                    this.mErrorCheckThread = monitorThread2;
                    if (processErrorStateListener == null) {
                        this.mState = State.NOT_MONITORING;
                    } else {
                        this.mState = State.MONITORING_NO_ERROR_DETECTED;
                    }
                    monitorThread2.start();
                } else if (processErrorStateListener != null) {
                    this.mErrorCheckThread.setListener(processErrorStateListener);
                } else {
                    illegalArgumentException = new IllegalArgumentException("Listener cannot be null");
                }
            } else {
                illegalArgumentException = new IllegalArgumentException("Cannot delay and wait for listener at the same time");
            }
            throw illegalArgumentException;
        }
    }

    public MonitorThread startMonitoringForTest(ProcessErrorStateListener processErrorStateListener) {
        ActivityManager activityManager = (ActivityManager) this.mContext.getSystemService("activity");
        synchronized (this) {
            if (this.mState != State.NOT_MONITORING) {
                MonitorThread.stopRequested(this.mErrorCheckThread);
            }
            long j = this.mCurrentMonitorThreadId + 1;
            this.mCurrentMonitorThreadId = j;
            this.mErrorCheckThread = new MonitorThread(activityManager, processErrorStateListener, j, 0);
        }
        return this.mErrorCheckThread;
    }

    public static /* synthetic */ String access$000() {
        return LOG_TAG;
    }

    public LinkedList checkProcessError(ActivityManager activityManager) {
        List<ActivityManager.ProcessErrorStateInfo> processesInErrorState = activityManager.getProcessesInErrorState();
        LinkedList linkedList = new LinkedList();
        if (processesInErrorState != null) {
            for (ActivityManager.ProcessErrorStateInfo next : processesInErrorState) {
                if (next.condition == 2 && next.uid == this.mMyUid) {
                    AnrErrorState anrErrorState = new AnrErrorState();
                    anrErrorState.mErrorMsg = next.shortMsg;
                    anrErrorState.mTag = next.tag;
                    String str = next.processName;
                    anrErrorState.mProcessName = str;
                    if (str.equals(this.mProcessName)) {
                        linkedList.addFirst(anrErrorState);
                    } else {
                        linkedList.addLast(anrErrorState);
                    }
                }
            }
        }
        return linkedList;
    }

    public ProcessAnrErrorMonitor(Context context, String str, int i, int i2) {
        this(context, str, false, 500, false, i, i2);
    }

    public ProcessAnrErrorMonitor(Context context, String str, boolean z, int i, int i2) {
        this(context, str, z, 500, false, i, i2);
    }

    public ProcessAnrErrorMonitor(Context context, String str, boolean z, int i, boolean z2, int i2, int i3) {
        this.mContext = context;
        this.mProcessName = str;
        this.mState = State.NOT_MONITORING;
        this.mPollingTime = i;
        this.mContinuousMonitoring = z2;
        this.mMaxNumberOfChecksBeforeError = i2;
        this.mMaxNumberOfChecksAfterError = i3;
        this.mMyUid = Process.myUid();
        if (z) {
            startMonitoringAfterDelay(null, 0);
        }
    }

    public void updateStateAndMaybeCallListener(StateChangeReason stateChangeReason, ProcessErrorStateListener processErrorStateListener) {
        updateStateAndMaybeCallListener(stateChangeReason, processErrorStateListener, null, null);
    }

    public synchronized void updateStateAndMaybeCallListener(StateChangeReason stateChangeReason, ProcessErrorStateListener processErrorStateListener, String str, String str2) {
        if (this.mErrorCheckThread.mId == this.mCurrentMonitorThreadId) {
            switch (stateChangeReason.ordinal()) {
                case 0:
                    processErrorStateListener.onStart();
                    break;
                case 1:
                    if (this.mContinuousMonitoring) {
                        this.mState = State.MONITORING_NO_ERROR_DETECTED;
                    } else {
                        this.mState = State.NOT_MONITORING;
                    }
                    processErrorStateListener.onErrorCleared();
                    break;
                case 2:
                    this.mState = State.MONITORING_ERROR_DETECTED;
                    processErrorStateListener.onErrorDetected(str, str2);
                    break;
                case 3:
                    this.mState = State.NOT_MONITORING;
                    processErrorStateListener.onMaxChecksReachedBeforeError();
                    break;
                case 4:
                    this.mState = State.NOT_MONITORING;
                    processErrorStateListener.onMaxChecksReachedAfterError();
                    break;
                case 5:
                    this.mState = State.NOT_MONITORING;
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    this.mState = State.NOT_MONITORING;
                    processErrorStateListener.onCheckFailed();
                    break;
                default:
                    throw new IllegalArgumentException("Unexpected state change reason: " + stateChangeReason);
            }
        }
    }
}
