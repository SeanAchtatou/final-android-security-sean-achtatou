package com.facebook.acra.anr.multisignal;

import X.AnonymousClass00S;
import X.AnonymousClass04f;
import X.C010608s;
import X.C010708t;
import android.os.Debug;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import com.facebook.acra.ACRA;
import com.facebook.acra.anr.ANRDataProvider;
import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.ANRDetectorListener;
import com.facebook.acra.anr.AppStateUpdater;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anr.IANRReport;
import com.facebook.acra.anr.processmonitor.DefaultProcessErrorStateListener;
import com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor;
import com.facebook.acra.anr.processmonitor.ProcessErrorStateListener;
import com.facebook.acra.anr.sigquit.SigquitDetector;
import com.facebook.acra.anr.sigquit.SigquitDetectorListener;
import com.facebook.forker.Process;
import java.io.File;
import java.io.IOException;
import org.webrtc.audio.WebRtcAudioRecord;

public class MultiSignalANRDetector implements IANRDetector, SigquitDetectorListener {
    public static final String LOG_TAG = "MultiSignalANRDetector";
    private static final String MULTI_SIGNAL_DETECTOR_THREAD_NAME = "MultiSignalANRDetectorThread";
    private static MultiSignalANRDetector sInstance;
    private ANRDataProvider mANRDataProvider;
    private long mANRReportTime;
    public final ANRDetectorConfig mAnrDetectorConfig;
    private ANRDetectorListener mAnrDetectorListener;
    public final Runnable mConfirmationExpiredRunnable = new Runnable() {
        public void run() {
            C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "On confirmation expired");
            MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
            if (multiSignalANRDetector.mWaitingForConfirmation) {
                multiSignalANRDetector.setCurrentAnrState(Event.AM_EXPIRED);
                MultiSignalANRDetector multiSignalANRDetector2 = MultiSignalANRDetector.this;
                multiSignalANRDetector2.mWaitingForConfirmation = false;
                if (multiSignalANRDetector2.mHasPendingReport) {
                    multiSignalANRDetector2.mAnrDetectorConfig.mANRReport.logAmExpiration(SystemClock.uptimeMillis());
                }
                MultiSignalANRDetector multiSignalANRDetector3 = MultiSignalANRDetector.this;
                if (MultiSignalANRDetector.isCurrentStateNoAnrDetected(multiSignalANRDetector3)) {
                    MultiSignalANRDetector.errorCleared(multiSignalANRDetector3);
                }
            }
        }
    };
    private AnonymousClass04f mCurrentState;
    private long mDetectorStartTime;
    public int mErrorCheckCounter;
    private final ProcessErrorStateListener mErrorMonitorListener = new DefaultProcessErrorStateListener() {
        public void onCheckFailed() {
            C010708t.A0I(MultiSignalANRDetector.LOG_TAG, "onCheckFailed");
            MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
            multiSignalANRDetector.mLostErrorDetectionTime = SystemClock.uptimeMillis();
            AnonymousClass00S.A04(multiSignalANRDetector.mProcessingThreadHandler, new Runnable() {
                public void run() {
                    MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
                    if (multiSignalANRDetector.mHasPendingReport) {
                        multiSignalANRDetector.mAnrDetectorConfig.mANRReport.logProcessMonitorFailure(multiSignalANRDetector.mLostErrorDetectionTime, 3);
                    }
                }
            }, -57466807);
        }

        public void onCheckPerformed() {
            MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
            if (multiSignalANRDetector.mAnrDetectorConfig.mForegroundCheckPeriod > 0) {
                AnonymousClass00S.A04(multiSignalANRDetector.mProcessingThreadHandler, new Runnable() {
                    public void run() {
                        MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
                        if (multiSignalANRDetector.mMovedToBackground) {
                            int i = multiSignalANRDetector.mErrorCheckCounter + 1;
                            multiSignalANRDetector.mErrorCheckCounter = i;
                            if (i % multiSignalANRDetector.mAnrDetectorConfig.mForegroundCheckPeriod == 0) {
                                C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "Pausing error state checks");
                                MultiSignalANRDetector.this.mProcessAnrErrorMonitor.pause();
                                MultiSignalANRDetector.this.mProcessAnrErrorMonitorPaused = true;
                            }
                        }
                    }
                }, 1289251035);
            }
        }

        public void onErrorCleared() {
            C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "On onErrorCleared");
            AnonymousClass00S.A04(MultiSignalANRDetector.this.mProcessingThreadHandler, new Runnable() {
                public void run() {
                    MultiSignalANRDetector.this.setCurrentAnrState(Event.DIALOG_DISMISSED);
                    MultiSignalANRDetector.errorCleared(MultiSignalANRDetector.this);
                }
            }, -1982691600);
        }

        public void onErrorDetected(final String str, final String str2) {
            boolean z;
            C010708t.A0P(MultiSignalANRDetector.LOG_TAG, "On error detected %s %s", str, str2);
            if (!MultiSignalANRDetector.isDebuggerConnected(MultiSignalANRDetector.this)) {
                synchronized (MultiSignalANRDetector.this.mStartStopLock) {
                    z = MultiSignalANRDetector.this.mRunning;
                }
                if (z) {
                    AnonymousClass00S.A04(MultiSignalANRDetector.this.mProcessingThreadHandler, new Runnable() {
                        public void run() {
                            C010708t.A0P(MultiSignalANRDetector.LOG_TAG, "On error detected waiting for confirmation %b", Boolean.valueOf(MultiSignalANRDetector.this.mWaitingForConfirmation));
                            MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
                            multiSignalANRDetector.mSystemErrorMessage = str;
                            multiSignalANRDetector.mSystemErrorTag = str2;
                            multiSignalANRDetector.mSystemErrorUptime = SystemClock.uptimeMillis();
                            multiSignalANRDetector.setCurrentAnrState(Event.AM_CONFIRMED);
                            MultiSignalANRDetector multiSignalANRDetector2 = MultiSignalANRDetector.this;
                            if (multiSignalANRDetector2.mWaitingForConfirmation) {
                                AnonymousClass00S.A02(multiSignalANRDetector2.mProcessingThreadHandler, multiSignalANRDetector2.mConfirmationExpiredRunnable);
                                MultiSignalANRDetector.this.mWaitingForConfirmation = false;
                            }
                            MultiSignalANRDetector.maybeStartReport(MultiSignalANRDetector.this, Event.AM_CONFIRMED);
                        }
                    }, 1455871980);
                }
            }
        }

        public void onStart() {
            C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "Started monitoring");
        }

        public boolean onErrorDetectOnOtherProcess(String str, String str2, String str3) {
            final long uptimeMillis = SystemClock.uptimeMillis();
            final String str4 = str2;
            final String str5 = str3;
            final String str6 = str;
            AnonymousClass00S.A04(MultiSignalANRDetector.this.mProcessingThreadHandler, new Runnable() {
                public void run() {
                    MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
                    if (multiSignalANRDetector.mHasPendingReport) {
                        multiSignalANRDetector.mAnrDetectorConfig.mANRReport.logOtherProcessAnr(str6, str4, str5, uptimeMillis);
                    }
                }
            }, -742755300);
            return true;
        }
    };
    public final C010608s mForegroundTransitionListener = new C010608s() {
        public void onBackground() {
            AnonymousClass00S.A04(MultiSignalANRDetector.this.mProcessingThreadHandler, new Runnable() {
                public void run() {
                    C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "Moving to background");
                    MultiSignalANRDetector.this.mMovedToBackground = true;
                }
            }, 806332448);
        }

        public void onForeground() {
            AnonymousClass00S.A04(MultiSignalANRDetector.this.mProcessingThreadHandler, new Runnable() {
                public void run() {
                    MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
                    multiSignalANRDetector.mMovedToBackground = false;
                    if (multiSignalANRDetector.mProcessAnrErrorMonitorPaused) {
                        C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "Resuming error state checks");
                        MultiSignalANRDetector.this.mProcessAnrErrorMonitor.resume();
                        MultiSignalANRDetector.this.mProcessAnrErrorMonitorPaused = false;
                    }
                }
            }, -1280365826);
        }
    };
    public boolean mHasPendingReport;
    public long mLostErrorDetectionTime;
    public boolean mMovedToBackground;
    private boolean mNativeHookInPlace;
    public ProcessAnrErrorMonitor mProcessAnrErrorMonitor;
    public boolean mProcessAnrErrorMonitorPaused;
    private final HandlerThread mProcessingThread;
    public final Handler mProcessingThreadHandler;
    public boolean mRunning;
    public long mSigquitCallbackUptime;
    public String mSigquitData;
    public final SigquitDetector mSigquitDetector;
    public String mSigquitFileName;
    public final Object mStartStopLock = new Object();
    private boolean mStartedInForegroundV1;
    private boolean mStartedInForegroundV2;
    public String mSystemErrorMessage;
    public String mSystemErrorTag;
    public long mSystemErrorUptime;
    public boolean mWaitingForConfirmation;
    public boolean mWaitingForMainThreadBlockedCheck;

    public enum ActionOnSigquit {
        IGNORE,
        CLEAR_CURRENT_ERROR_STATE,
        START_REPORT
    }

    public enum Event {
        SIGQUIT_RECEIVED,
        AM_CONFIRMED,
        AM_EXPIRED,
        MT_UNBLOCKED,
        DIALOG_DISMISSED
    }

    public static MultiSignalANRDetector getTestInstance(ANRDetectorConfig aNRDetectorConfig, SigquitDetector sigquitDetector, HandlerThread handlerThread) {
        if (0 != 0) {
            MultiSignalANRDetector multiSignalANRDetector = new MultiSignalANRDetector(aNRDetectorConfig, sigquitDetector, handlerThread);
            sInstance = multiSignalANRDetector;
            return multiSignalANRDetector;
        }
        throw new AssertionError();
    }

    public AnonymousClass04f getCurrentState() {
        if (0 != 0) {
            return this.mCurrentState;
        }
        throw new AssertionError();
    }

    public ProcessErrorStateListener getErrorMonitorListener() {
        if (0 != 0) {
            return this.mErrorMonitorListener;
        }
        throw new AssertionError();
    }

    public void setAnrErrorMonitor(ProcessAnrErrorMonitor processAnrErrorMonitor) {
        if (0 != 0) {
            this.mProcessAnrErrorMonitor = processAnrErrorMonitor;
            return;
        }
        throw new AssertionError();
    }

    public void setCheckIntervalMs(long j) {
    }

    public void setInternalState(AnonymousClass04f r2) {
        if (0 != 0) {
            this.mCurrentState = r2;
            return;
        }
        throw new AssertionError();
    }

    public void startForTesting() {
        if (0 != 0) {
            synchronized (this.mStartStopLock) {
                this.mRunning = true;
            }
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: com.facebook.acra.anr.multisignal.MultiSignalANRDetector$9  reason: invalid class name */
    public /* synthetic */ class AnonymousClass9 {
        public static final /* synthetic */ int[] $SwitchMap$com$facebook$acra$anr$multisignal$MultiSignalANRDetector$Event;
        public static final /* synthetic */ int[] $SwitchMap$com$facebook$reliability$anr$AnrState;

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|(3:35|36|38)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(31:0|1|2|3|(2:5|6)|7|9|10|11|(2:13|14)|15|17|18|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|38) */
        /* JADX WARNING: Can't wrap try/catch for region: R(33:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|38) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0047 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x004f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0057 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x005f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0067 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0070 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x0079 */
        static {
            /*
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event[] r0 = com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event.values()
                int r0 = r0.length
                int[] r2 = new int[r0]
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector.AnonymousClass9.$SwitchMap$com$facebook$acra$anr$multisignal$MultiSignalANRDetector$Event = r2
                r6 = 1
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event r0 = com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event.SIGQUIT_RECEIVED     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2[r0] = r6     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r5 = 2
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event r0 = com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event.AM_CONFIRMED     // Catch:{ NoSuchFieldError -> 0x001b }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x001b }
                r2[r0] = r5     // Catch:{ NoSuchFieldError -> 0x001b }
            L_0x001b:
                r4 = 3
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event r0 = com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event.MT_UNBLOCKED     // Catch:{ NoSuchFieldError -> 0x0024 }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0024 }
                r2[r0] = r4     // Catch:{ NoSuchFieldError -> 0x0024 }
            L_0x0024:
                r3 = 4
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event r0 = com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event.AM_EXPIRED     // Catch:{ NoSuchFieldError -> 0x002d }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x002d }
                r2[r0] = r3     // Catch:{ NoSuchFieldError -> 0x002d }
            L_0x002d:
                r1 = 5
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event r0 = com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event.DIALOG_DISMISSED     // Catch:{ NoSuchFieldError -> 0x0036 }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0036 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0036 }
            L_0x0036:
                X.04f[] r0 = X.AnonymousClass04f.values()
                int r0 = r0.length
                int[] r2 = new int[r0]
                com.facebook.acra.anr.multisignal.MultiSignalANRDetector.AnonymousClass9.$SwitchMap$com$facebook$reliability$anr$AnrState = r2
                X.04f r0 = X.AnonymousClass04f.NO_ANR_DETECTED     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r2[r0] = r6     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                X.04f r0 = X.AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED     // Catch:{ NoSuchFieldError -> 0x004f }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x004f }
                r2[r0] = r5     // Catch:{ NoSuchFieldError -> 0x004f }
            L_0x004f:
                X.04f r0 = X.AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_BLOCKED     // Catch:{ NoSuchFieldError -> 0x0057 }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0057 }
                r2[r0] = r4     // Catch:{ NoSuchFieldError -> 0x0057 }
            L_0x0057:
                X.04f r0 = X.AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_UNBLOCKED     // Catch:{ NoSuchFieldError -> 0x005f }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x005f }
                r2[r0] = r3     // Catch:{ NoSuchFieldError -> 0x005f }
            L_0x005f:
                X.04f r0 = X.AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_BLOCKED     // Catch:{ NoSuchFieldError -> 0x0067 }
                int r0 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0067 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0067 }
            L_0x0067:
                X.04f r0 = X.AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_UNBLOCKED     // Catch:{ NoSuchFieldError -> 0x0070 }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0070 }
                r0 = 6
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0070 }
            L_0x0070:
                X.04f r0 = X.AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_UNBLOCKED     // Catch:{ NoSuchFieldError -> 0x0079 }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0079 }
                r0 = 7
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0079 }
            L_0x0079:
                X.04f r0 = X.AnonymousClass04f.SIGQUIT_RECEIVED_AM_EXPIRED_MT_BLOCKED     // Catch:{ NoSuchFieldError -> 0x0083 }
                int r1 = r0.ordinal()     // Catch:{ NoSuchFieldError -> 0x0083 }
                r0 = 8
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0083 }
            L_0x0083:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.multisignal.MultiSignalANRDetector.AnonymousClass9.<clinit>():void");
        }
    }

    private void addActivityManagerConfirmationDataToReport() {
        this.mAnrDetectorConfig.mANRReport.logSystemInfo(this.mSystemErrorMessage, this.mSystemErrorTag, this.mSystemErrorUptime);
    }

    private void addInfoToReport(Event event) {
        if (event == Event.SIGQUIT_RECEIVED) {
            addSigquitDataToReport();
        } else if (event == Event.AM_CONFIRMED) {
            addActivityManagerConfirmationDataToReport();
        } else {
            throw new IllegalArgumentException("Event should be SIGQUIT_RECEIVED or AM_CONFIRMED");
        }
    }

    private void addSigquitDataToReport() {
        this.mAnrDetectorConfig.mANRReport.logSigquitData(this.mSigquitData, this.mSigquitFileName, this.mSigquitCallbackUptime);
    }

    public static void errorCleared(MultiSignalANRDetector multiSignalANRDetector) {
        C010708t.A0P(LOG_TAG, "Clearing error state has pending report %b", Boolean.valueOf(multiSignalANRDetector.mHasPendingReport));
        if (multiSignalANRDetector.mHasPendingReport) {
            multiSignalANRDetector.mAnrDetectorConfig.mANRReport.finalizeAndTryToSendReport(SystemClock.uptimeMillis() - multiSignalANRDetector.mANRReportTime);
            multiSignalANRDetector.mHasPendingReport = false;
        }
    }

    public static ActionOnSigquit getActionOnSigquit(MultiSignalANRDetector multiSignalANRDetector) {
        AnonymousClass04f r1 = multiSignalANRDetector.mCurrentState;
        if (r1 == AnonymousClass04f.NO_ANR_DETECTED || r1 == AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_BLOCKED || r1 == AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_UNBLOCKED) {
            return ActionOnSigquit.START_REPORT;
        }
        if (r1 == AnonymousClass04f.SIGQUIT_RECEIVED_AM_EXPIRED_MT_BLOCKED || r1 == AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_UNBLOCKED || r1 == AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED) {
            return ActionOnSigquit.CLEAR_CURRENT_ERROR_STATE;
        }
        return ActionOnSigquit.IGNORE;
    }

    public static MultiSignalANRDetector getInstance(ANRDetectorConfig aNRDetectorConfig) {
        if (sInstance == null) {
            sInstance = new MultiSignalANRDetector(aNRDetectorConfig);
        }
        return sInstance;
    }

    public static boolean isCurrentStateNoAnrDetected(MultiSignalANRDetector multiSignalANRDetector) {
        if (multiSignalANRDetector.mCurrentState == AnonymousClass04f.NO_ANR_DETECTED) {
            return true;
        }
        return false;
    }

    private boolean isCurrentStateUnconfirmed() {
        AnonymousClass04f r2 = this.mCurrentState;
        if (r2 == AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED || r2 == AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_UNBLOCKED) {
            return true;
        }
        return false;
    }

    public static boolean isDebuggerConnected(MultiSignalANRDetector multiSignalANRDetector) {
        if (!multiSignalANRDetector.mAnrDetectorConfig.mIsInternalBuild || !Debug.isDebuggerConnected()) {
            return false;
        }
        return true;
    }

    private void logUnexpectedStateTransition(Event event) {
        ANRDataProvider aNRDataProvider;
        C010708t.A0P(LOG_TAG, "Unexpected event %s received in state %s", event, this.mCurrentState);
        if (event != Event.SIGQUIT_RECEIVED && (aNRDataProvider = this.mANRDataProvider) != null) {
            aNRDataProvider.reportSoftError("Unexpected event", new IllegalStateException("Unexpected event " + event + " received in state " + this.mCurrentState));
        }
    }

    public static void mainThreadUnblocked(MultiSignalANRDetector multiSignalANRDetector) {
        C010708t.A0J(LOG_TAG, "Running on the main thread");
        final long uptimeMillis = SystemClock.uptimeMillis();
        AnonymousClass00S.A04(multiSignalANRDetector.mProcessingThreadHandler, new Runnable() {
            public void run() {
                MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
                multiSignalANRDetector.mWaitingForMainThreadBlockedCheck = false;
                multiSignalANRDetector.setCurrentAnrState(Event.MT_UNBLOCKED);
                MultiSignalANRDetector multiSignalANRDetector2 = MultiSignalANRDetector.this;
                if (multiSignalANRDetector2.mHasPendingReport) {
                    multiSignalANRDetector2.mAnrDetectorConfig.mANRReport.logMainThreadUnblocked(uptimeMillis);
                }
                MultiSignalANRDetector multiSignalANRDetector3 = MultiSignalANRDetector.this;
                if (MultiSignalANRDetector.isCurrentStateNoAnrDetected(multiSignalANRDetector3)) {
                    MultiSignalANRDetector.errorCleared(multiSignalANRDetector3);
                } else {
                    MultiSignalANRDetector.maybeAdvanceExpirationTask(multiSignalANRDetector3);
                }
            }
        }, 1133714194);
    }

    private void maybeStartMainThreadBlockedCheck() {
        if (!this.mWaitingForMainThreadBlockedCheck) {
            AnonymousClass04f r1 = this.mCurrentState;
            if (r1 == AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED || r1 == AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_BLOCKED) {
                C010708t.A0J(LOG_TAG, "Posting main thread check");
                this.mWaitingForMainThreadBlockedCheck = true;
                AnonymousClass00S.A04(this.mAnrDetectorConfig.mMainThreadHandler, new Runnable() {
                    public void run() {
                        MultiSignalANRDetector.mainThreadUnblocked(MultiSignalANRDetector.this);
                    }
                }, 1705381466);
            }
        }
    }

    public static void maybeStartReport(MultiSignalANRDetector multiSignalANRDetector, Event event) {
        boolean shouldUploadAnrReports;
        String str;
        C010708t.A0P(LOG_TAG, "On maybeStartReport event: %s has pending report %b", event, Boolean.valueOf(multiSignalANRDetector.mHasPendingReport));
        if (multiSignalANRDetector.mHasPendingReport) {
            multiSignalANRDetector.addInfoToReport(event);
            return;
        }
        if (multiSignalANRDetector.mStartedInForegroundV2 || multiSignalANRDetector.mStartedInForegroundV1) {
            shouldUploadAnrReports = multiSignalANRDetector.shouldUploadAnrReports();
        } else {
            shouldUploadAnrReports = false;
        }
        multiSignalANRDetector.maybeStartTimerForActivityManagerConfirmation();
        if (shouldUploadAnrReports) {
            C010708t.A0J(LOG_TAG, "Reporting ANR start");
            try {
                multiSignalANRDetector.startReport(event);
            } catch (IOException e) {
                C010708t.A0R(LOG_TAG, e, "Error starting ANR report");
                multiSignalANRDetector.mHasPendingReport = false;
            }
        } else if (event == Event.SIGQUIT_RECEIVED && (str = multiSignalANRDetector.mSigquitFileName) != null) {
            new File(str).delete();
        }
    }

    private void maybeStartTimerForActivityManagerConfirmation() {
        if (!this.mWaitingForConfirmation && this.mAnrDetectorConfig.mRecoveryTimeout > 0 && isCurrentStateUnconfirmed()) {
            C010708t.A0J(LOG_TAG, "Starting timer for AM confirmation");
            this.mWaitingForConfirmation = true;
            AnonymousClass00S.A05(this.mProcessingThreadHandler, this.mConfirmationExpiredRunnable, (long) this.mAnrDetectorConfig.mRecoveryTimeout, 2104556552);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (r2 == X.AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_BLOCKED) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setASLState(com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event r8, boolean r9, boolean r10) {
        /*
            r7 = this;
            com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event r0 = com.facebook.acra.anr.multisignal.MultiSignalANRDetector.Event.SIGQUIT_RECEIVED
            r5 = 1
            r6 = 0
            if (r8 != r0) goto L_0x0007
            r6 = 1
        L_0x0007:
            com.facebook.acra.anr.ANRDetectorConfig r0 = r7.mAnrDetectorConfig
            com.facebook.acra.anr.AppStateUpdater r4 = r0.mAppStateUpdater
            r3 = 0
            if (r6 == 0) goto L_0x001a
            com.facebook.acra.anr.multisignal.MultiSignalANRDetector$6 r1 = new com.facebook.acra.anr.multisignal.MultiSignalANRDetector$6
            r1.<init>()
            boolean r0 = r0.mCleanupOnASLThread
            if (r0 != 0) goto L_0x0054
            r1.run()
        L_0x001a:
            X.04f r2 = r7.mCurrentState
            X.04f r0 = X.AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED
            if (r2 == r0) goto L_0x0025
            X.04f r0 = X.AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_BLOCKED
            r1 = 0
            if (r2 != r0) goto L_0x0026
        L_0x0025:
            r1 = 1
        L_0x0026:
            if (r4 == 0) goto L_0x003a
            if (r6 == 0) goto L_0x0041
            if (r1 == 0) goto L_0x0041
            com.facebook.acra.anr.ANRDetectorConfig r0 = r7.mAnrDetectorConfig
            boolean r0 = r0.mShouldUseFgStateInDetectorOnAslUpdate
            if (r0 == 0) goto L_0x0041
            if (r10 != 0) goto L_0x0037
            if (r9 != 0) goto L_0x0037
            r5 = 0
        L_0x0037:
            r4.updateAnrState(r2, r3, r5)
        L_0x003a:
            if (r1 == 0) goto L_0x0040
            r7.mStartedInForegroundV1 = r9
            r7.mStartedInForegroundV2 = r10
        L_0x0040:
            return
        L_0x0041:
            boolean r10 = r4.isAppInForegroundV2()
            boolean r9 = r4.isAppInForegroundV1()
            X.04f r0 = r7.mCurrentState
            if (r10 != 0) goto L_0x0050
            if (r9 != 0) goto L_0x0050
            r5 = 0
        L_0x0050:
            r4.updateAnrState(r0, r3, r5)
            goto L_0x003a
        L_0x0054:
            r3 = r1
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.multisignal.MultiSignalANRDetector.setASLState(com.facebook.acra.anr.multisignal.MultiSignalANRDetector$Event, boolean, boolean):void");
    }

    private boolean shouldUploadAnrReports() {
        ANRDataProvider aNRDataProvider = this.mANRDataProvider;
        if (aNRDataProvider != null) {
            return aNRDataProvider.shouldCollectAndUploadANRReports();
        }
        return this.mAnrDetectorConfig.mCachedShouldUploadANRReports;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private void startReport(Event event) {
        String str;
        String str2;
        long j;
        String str3;
        String str4;
        File file;
        boolean z = true;
        this.mHasPendingReport = true;
        this.mANRReportTime = SystemClock.uptimeMillis();
        ANRDetectorListener aNRDetectorListener = this.mAnrDetectorListener;
        if (aNRDetectorListener != null) {
            str = aNRDetectorListener.getBlackBoxTraceId();
            str2 = aNRDetectorListener.getLongStallTraceId();
            aNRDetectorListener.onStartANRDataCapture();
        } else {
            str = null;
            str2 = null;
        }
        Event event2 = event;
        if (event2 == Event.SIGQUIT_RECEIVED) {
            str3 = this.mSigquitData;
            str4 = this.mSigquitFileName;
            j = this.mSigquitCallbackUptime;
        } else {
            j = 0;
            str3 = null;
            str4 = null;
        }
        ANRDetectorConfig aNRDetectorConfig = this.mAnrDetectorConfig;
        IANRReport iANRReport = aNRDetectorConfig.mANRReport;
        if (this.mANRDataProvider != null) {
            z = false;
        }
        int i = aNRDetectorConfig.mDetectorId;
        boolean z2 = this.mStartedInForegroundV1;
        boolean z3 = this.mStartedInForegroundV2;
        long uptimeMillis = SystemClock.uptimeMillis();
        long j2 = this.mDetectorStartTime;
        if (aNRDetectorConfig.mShouldRecordSignalTime) {
            file = aNRDetectorConfig.getSigquitTimeDir();
        } else {
            file = null;
        }
        int i2 = i;
        boolean z4 = z2;
        boolean z5 = z3;
        IANRReport iANRReport2 = iANRReport;
        boolean z6 = z;
        iANRReport2.startReport(z6, str, str2, i2, z4, z5, uptimeMillis, j2, 0, 0, str3, str4, file, this.mAnrDetectorConfig.mProcessName.replace('.', '_').replace(':', '_'), Long.valueOf(j));
        if (event2 == Event.AM_CONFIRMED) {
            addActivityManagerConfirmationDataToReport();
        }
        long j3 = this.mLostErrorDetectionTime;
        if (j3 != 0) {
            this.mAnrDetectorConfig.mANRReport.logProcessMonitorFailure(j3, 3);
        }
        if (aNRDetectorListener != null) {
            aNRDetectorListener.onEndANRDataCapture();
        }
    }

    public void nativeLibraryLoaded(boolean z) {
        this.mSigquitDetector.init(this.mAnrDetectorConfig);
        this.mSigquitDetector.installSignalHandler(this.mProcessingThreadHandler, z);
    }

    public void onHookedMethods(boolean z) {
        synchronized (this.mStartStopLock) {
            this.mNativeHookInPlace = z;
        }
    }

    public void sigquitDetected(String str, String str2, boolean z, boolean z2) {
        boolean z3;
        C010708t.A0J(LOG_TAG, "On sigquitDetected call");
        if (!isDebuggerConnected(this)) {
            synchronized (this.mStartStopLock) {
                z3 = this.mRunning;
            }
            if (z3) {
                final long uptimeMillis = SystemClock.uptimeMillis();
                final String str3 = str2;
                final String str4 = str;
                final boolean z4 = z2;
                final boolean z5 = z;
                AnonymousClass00S.A04(this.mProcessingThreadHandler, new Runnable() {
                    public void run() {
                        boolean z;
                        ActionOnSigquit actionOnSigquit = MultiSignalANRDetector.getActionOnSigquit(MultiSignalANRDetector.this);
                        if (actionOnSigquit == ActionOnSigquit.IGNORE) {
                            C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "Ignoring new sigquit");
                            String str = str3;
                            if (str != null) {
                                new File(str).delete();
                            }
                            MultiSignalANRDetector.this.mAnrDetectorConfig.mANRReport.logExtraSigquit(uptimeMillis);
                            return;
                        }
                        if (actionOnSigquit == ActionOnSigquit.CLEAR_CURRENT_ERROR_STATE) {
                            C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "Will clear error state");
                            z = true;
                        } else {
                            C010708t.A0J(MultiSignalANRDetector.LOG_TAG, "Will start new report");
                            z = false;
                        }
                        if (z) {
                            MultiSignalANRDetector.errorCleared(MultiSignalANRDetector.this);
                        }
                        MultiSignalANRDetector multiSignalANRDetector = MultiSignalANRDetector.this;
                        multiSignalANRDetector.mSigquitCallbackUptime = uptimeMillis;
                        multiSignalANRDetector.mSigquitData = str4;
                        multiSignalANRDetector.mSigquitFileName = str3;
                        Event event = Event.SIGQUIT_RECEIVED;
                        multiSignalANRDetector.setCurrentAnrState(event, z5, z4);
                        MultiSignalANRDetector.maybeStartReport(MultiSignalANRDetector.this, event);
                    }
                }, -1213203030);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0026, code lost:
        r0 = r1.mAppStateUpdater;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void start() {
        /*
            r5 = this;
            java.lang.Object r4 = r5.mStartStopLock
            monitor-enter(r4)
            boolean r0 = r5.mRunning     // Catch:{ all -> 0x0039 }
            if (r0 != 0) goto L_0x0037
            java.lang.String r1 = "MultiSignalANRDetector"
            java.lang.String r0 = "Starting"
            X.C010708t.A0J(r1, r0)     // Catch:{ all -> 0x0039 }
            r0 = 1
            r5.mRunning = r0     // Catch:{ all -> 0x0039 }
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0039 }
            r5.mDetectorStartTime = r0     // Catch:{ all -> 0x0039 }
            com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor r3 = r5.mProcessAnrErrorMonitor     // Catch:{ all -> 0x0039 }
            com.facebook.acra.anr.processmonitor.ProcessErrorStateListener r2 = r5.mErrorMonitorListener     // Catch:{ all -> 0x0039 }
            r0 = 4000(0xfa0, double:1.9763E-320)
            r3.startMonitoringAfterDelay(r2, r0)     // Catch:{ all -> 0x0039 }
            com.facebook.acra.anr.ANRDetectorConfig r1 = r5.mAnrDetectorConfig     // Catch:{ all -> 0x0039 }
            int r0 = r1.mForegroundCheckPeriod     // Catch:{ all -> 0x0039 }
            if (r0 < 0) goto L_0x0037
            com.facebook.acra.anr.AppStateUpdater r0 = r1.mAppStateUpdater     // Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x0037
            android.os.Handler r2 = r5.mProcessingThreadHandler     // Catch:{ all -> 0x0039 }
            com.facebook.acra.anr.multisignal.MultiSignalANRDetector$4 r1 = new com.facebook.acra.anr.multisignal.MultiSignalANRDetector$4     // Catch:{ all -> 0x0039 }
            r1.<init>(r0)     // Catch:{ all -> 0x0039 }
            r0 = 1016029526(0x3c8f6156, float:0.017502468)
            X.AnonymousClass00S.A04(r2, r1, r0)     // Catch:{ all -> 0x0039 }
        L_0x0037:
            monitor-exit(r4)     // Catch:{ all -> 0x0039 }
            return
        L_0x0039:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0039 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.multisignal.MultiSignalANRDetector.start():void");
    }

    public void stop(IANRDetector.ANRDetectorStopListener aNRDetectorStopListener) {
        AppStateUpdater appStateUpdater;
        synchronized (this.mStartStopLock) {
            if (this.mRunning) {
                C010708t.A0J(LOG_TAG, "Stopping");
                this.mRunning = false;
                this.mProcessAnrErrorMonitor.stopMonitoring();
                AnonymousClass00S.A02(this.mProcessingThreadHandler, this.mConfirmationExpiredRunnable);
                if (this.mNativeHookInPlace) {
                    SigquitDetector.nativeStopDetector();
                }
                ANRDetectorConfig aNRDetectorConfig = this.mAnrDetectorConfig;
                if (aNRDetectorConfig.mForegroundCheckPeriod >= 0 && (appStateUpdater = aNRDetectorConfig.mAppStateUpdater) != null) {
                    appStateUpdater.removeForegroundTransitionListener(this.mForegroundTransitionListener);
                }
                if (aNRDetectorStopListener != null) {
                    aNRDetectorStopListener.onStop();
                }
            }
        }
    }

    public static /* synthetic */ String access$000() {
        return LOG_TAG;
    }

    private static boolean isTest() {
        return false;
    }

    public static void maybeAdvanceExpirationTask(MultiSignalANRDetector multiSignalANRDetector) {
        int i;
        if (multiSignalANRDetector.isCurrentStateUnconfirmed() && (i = multiSignalANRDetector.mAnrDetectorConfig.mExpirationTimeAfterMainThreadUnblocked) > 0 && multiSignalANRDetector.mWaitingForConfirmation) {
            long uptimeMillis = (multiSignalANRDetector.mSigquitCallbackUptime + ((long) i)) - SystemClock.uptimeMillis();
            AnonymousClass00S.A02(multiSignalANRDetector.mProcessingThreadHandler, multiSignalANRDetector.mConfirmationExpiredRunnable);
            AnonymousClass00S.A05(multiSignalANRDetector.mProcessingThreadHandler, multiSignalANRDetector.mConfirmationExpiredRunnable, uptimeMillis, -447211071);
        }
    }

    private void setCurrentAnrStateFromNoAnrDetected(Event event) {
        switch (event.ordinal()) {
            case 0:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED;
                return;
            case 1:
                this.mCurrentState = AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_BLOCKED;
                return;
            case 2:
            default:
                logUnexpectedStateTransition(event);
                return;
            case 3:
                return;
        }
    }

    private void setCurrentAnrStateFromNoSigquitAmConfirmedMtBlocked(Event event) {
        switch (event.ordinal()) {
            case 0:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_BLOCKED;
                return;
            case 1:
            case 2:
            default:
                logUnexpectedStateTransition(event);
                return;
            case 3:
                this.mCurrentState = AnonymousClass04f.NO_SIGQUIT_AM_CONFIRMED_MT_UNBLOCKED;
                return;
            case 4:
                this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
                return;
        }
    }

    private void setCurrentAnrStateFromNoSigquitAmConfirmedMtUnblocked(Event event) {
        switch (event.ordinal()) {
            case 0:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_UNBLOCKED;
                return;
            case 1:
            case 2:
            case 3:
            default:
                logUnexpectedStateTransition(event);
                return;
            case 4:
                this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
                return;
        }
    }

    private void setCurrentAnrStateFromSigquitReceivedAmConfirmedMtBlocked(Event event) {
        switch (event.ordinal()) {
            case 3:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_UNBLOCKED;
                return;
            case 4:
                this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
                return;
            default:
                logUnexpectedStateTransition(event);
                return;
        }
    }

    private void setCurrentAnrStateFromSigquitReceivedAmConfirmedMtUnblocked(Event event) {
        if (event.ordinal() != 4) {
            logUnexpectedStateTransition(event);
        } else {
            this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
        }
    }

    private void setCurrentAnrStateFromSigquitReceivedAmExpiredMtBlocked(Event event) {
        switch (event.ordinal()) {
            case 0:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED;
                return;
            case 1:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_BLOCKED;
                return;
            case 2:
            default:
                logUnexpectedStateTransition(event);
                return;
            case 3:
                this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
                return;
        }
    }

    private void setCurrentAnrStateFromSigquitReceivedAmUnconfirmedMtBlocked(Event event) {
        switch (event.ordinal()) {
            case 0:
                return;
            case 1:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_BLOCKED;
                return;
            case 2:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_EXPIRED_MT_BLOCKED;
                return;
            case 3:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_UNBLOCKED;
                return;
            default:
                logUnexpectedStateTransition(event);
                return;
        }
    }

    private void setCurrentAnrStateFromSigquitReceivedAmUnconfirmedMtUnblocked(Event event) {
        switch (event.ordinal()) {
            case 0:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_UNCONFIRMED_MT_BLOCKED;
                return;
            case 1:
                this.mCurrentState = AnonymousClass04f.SIGQUIT_RECEIVED_AM_CONFIRMED_MT_UNBLOCKED;
                return;
            case 2:
                this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
                return;
            default:
                logUnexpectedStateTransition(event);
                return;
        }
    }

    public void setANRDataProvider(ANRDataProvider aNRDataProvider) {
        this.mANRDataProvider = aNRDataProvider;
    }

    public void setListener(ANRDetectorListener aNRDetectorListener) {
        this.mAnrDetectorListener = aNRDetectorListener;
    }

    private MultiSignalANRDetector(ANRDetectorConfig aNRDetectorConfig) {
        this.mAnrDetectorConfig = aNRDetectorConfig;
        this.mSigquitDetector = SigquitDetector.getInstance(this);
        this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
        this.mProcessAnrErrorMonitor = new ProcessAnrErrorMonitor(aNRDetectorConfig.mContext, aNRDetectorConfig.mProcessName, false, 500, true, 0, 0);
        HandlerThread handlerThread = new HandlerThread(MULTI_SIGNAL_DETECTOR_THREAD_NAME);
        this.mProcessingThread = handlerThread;
        handlerThread.start();
        this.mProcessingThreadHandler = new Handler(this.mProcessingThread.getLooper());
    }

    private MultiSignalANRDetector(ANRDetectorConfig aNRDetectorConfig, SigquitDetector sigquitDetector, HandlerThread handlerThread) {
        this.mAnrDetectorConfig = aNRDetectorConfig;
        this.mSigquitDetector = sigquitDetector;
        this.mCurrentState = AnonymousClass04f.NO_ANR_DETECTED;
        this.mProcessAnrErrorMonitor = new ProcessAnrErrorMonitor(aNRDetectorConfig.mContext, aNRDetectorConfig.mProcessName, false, 500, true, 0, 0);
        this.mProcessingThread = handlerThread;
        handlerThread.start();
        this.mProcessingThreadHandler = new Handler(this.mProcessingThread.getLooper());
    }

    public void setCurrentAnrState(Event event) {
        setCurrentAnrState(event, false, false);
    }

    public void setCurrentAnrState(Event event, boolean z, boolean z2) {
        C010708t.A0P(LOG_TAG, "Transitioning from %s event %s inFgV1: %b inFgV2: %b", this.mCurrentState, event, Boolean.valueOf(z), Boolean.valueOf(z2));
        AnonymousClass04f r3 = this.mCurrentState;
        switch (r3.ordinal()) {
            case 0:
                setCurrentAnrStateFromNoAnrDetected(event);
                break;
            case 1:
            case 2:
            default:
                throw new IllegalStateException("Unknown state: " + r3);
            case 3:
                setCurrentAnrStateFromSigquitReceivedAmUnconfirmedMtBlocked(event);
                break;
            case 4:
                setCurrentAnrStateFromSigquitReceivedAmConfirmedMtBlocked(event);
                break;
            case 5:
                setCurrentAnrStateFromSigquitReceivedAmConfirmedMtUnblocked(event);
                break;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                setCurrentAnrStateFromSigquitReceivedAmUnconfirmedMtUnblocked(event);
                break;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                setCurrentAnrStateFromSigquitReceivedAmExpiredMtBlocked(event);
                break;
            case 8:
                setCurrentAnrStateFromNoSigquitAmConfirmedMtBlocked(event);
                break;
            case Process.SIGKILL /*9*/:
                setCurrentAnrStateFromNoSigquitAmConfirmedMtUnblocked(event);
                break;
        }
        setASLState(event, z, z2);
        maybeStartMainThreadBlockedCheck();
    }
}
