package com.facebook.acra.anr;

import X.AnonymousClass04f;
import X.C010608s;
import java.util.ArrayList;
import java.util.List;

public abstract class AppStateUpdater {
    private final List mListeners = new ArrayList();

    public boolean isAppInForegroundV1() {
        return false;
    }

    public boolean isAppInForegroundV2() {
        return false;
    }

    public void addForegroundTransitionListener(C010608s r3) {
        synchronized (this.mListeners) {
            this.mListeners.add(r3);
        }
    }

    public void callListenerBackground() {
        synchronized (this.mListeners) {
            for (C010608s onBackground : this.mListeners) {
                onBackground.onBackground();
            }
        }
    }

    public void callListenersForeground() {
        synchronized (this.mListeners) {
            for (C010608s onForeground : this.mListeners) {
                onForeground.onForeground();
            }
        }
    }

    public int getForegroundTransitionListenerCount() {
        int size;
        synchronized (this.mListeners) {
            size = this.mListeners.size();
        }
        return size;
    }

    public void removeForegroundTransitionListener(C010608s r3) {
        synchronized (this.mListeners) {
            this.mListeners.remove(r3);
        }
    }

    public void updateAnrState(AnonymousClass04f r3) {
        throw new AbstractMethodError("Method needs to be overridden");
    }

    public void updateAnrState(AnonymousClass04f r1, Runnable runnable) {
        updateAnrState(r1);
    }

    public void updateAnrState(AnonymousClass04f r1, Runnable runnable, boolean z) {
        updateAnrState(r1, runnable);
    }
}
