package com.facebook.acra.anr.sigquit.detector;

import X.AnonymousClass00S;
import X.AnonymousClass04f;
import X.C010708t;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.AppStateUpdater;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anr.base.AbstractANRDetector;
import com.facebook.acra.anr.processmonitor.DefaultProcessErrorStateListener;
import com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor;
import com.facebook.acra.anr.sigquit.SigquitDetector;
import com.facebook.acra.anr.sigquit.SigquitDetectorListener;
import java.io.File;
import java.io.IOException;

public class SigquitBasedANRDetector extends AbstractANRDetector implements SigquitDetectorListener {
    public static final String LOG_TAG = "SigquitBasedANRDetector";
    private static SigquitBasedANRDetector sInstance;
    private static boolean sNativeInited;
    private final Runnable mClearAnrStateRunnable;
    private long mDetectorReadyTime;
    private boolean mHookInPlace;
    private NativeInitListener mNativeInitListener;
    private ProcessAnrErrorMonitor mProcessAnrErrorMonitor;
    private ProcessAnrErrorMonitorListener mProcessAnrErrorMonitorListener;
    private HandlerThread mProcessingThread;
    private Handler mProcessingThreadHandler;
    private final Object mProcessingThreadLock;
    private volatile boolean mRunning;
    public final SigquitDetector mSigquitDetector;
    private final boolean mStartProcessErrorMonitorAfterANRDetection;
    private final Object mStateLock;
    private long mSwitchTime;
    public boolean mWaitingForANRClearTimeout;
    public final Object mWaitingToClearANRLock;

    public interface NativeInitListener {
        void onNativeInit();
    }

    public class ProcessAnrErrorMonitorListener extends DefaultProcessErrorStateListener {
        public ProcessAnrErrorMonitorListener() {
        }

        public void onCheckFailed() {
            C010708t.A0I(SigquitBasedANRDetector.LOG_TAG, "Failed when checking process error state");
            SigquitBasedANRDetector.this.processMonitorStopped(3);
        }

        public void onErrorCleared() {
            if (SigquitBasedANRDetector.this.inAnrState()) {
                SigquitBasedANRDetector.this.anrErrorClearedOnProcessMonitor();
            }
        }

        public boolean onErrorDetectOnOtherProcess(String str, String str2, String str3) {
            if (!SigquitBasedANRDetector.this.inAnrState() || !SigquitBasedANRDetector.this.shouldCollectAndUploadANRReportsNow()) {
                return false;
            }
            String str4 = str;
            SigquitBasedANRDetector.this.mANRConfig.mANRReport.logOtherProcessAnr(str4, str2, str3, SystemClock.uptimeMillis());
            return true;
        }

        public void onErrorDetected(String str, String str2) {
            if (SigquitBasedANRDetector.this.inAnrState() && SigquitBasedANRDetector.this.shouldCollectAndUploadANRReportsNow()) {
                SigquitBasedANRDetector.this.mANRConfig.mANRReport.logSystemInfo(str, str2, SystemClock.uptimeMillis());
            }
        }

        public void onMaxChecksReachedAfterError() {
            C010708t.A0J(SigquitBasedANRDetector.LOG_TAG, "Reached max number of checks after error was detected");
            SigquitBasedANRDetector.this.processMonitorStopped(2);
        }

        public void onMaxChecksReachedBeforeError() {
            C010708t.A0J(SigquitBasedANRDetector.LOG_TAG, "Reached max number of checks before error was detected");
            SigquitBasedANRDetector.this.processMonitorStopped(1);
        }

        public void onStart() {
            SigquitBasedANRDetector.this.processMonitorStarted();
        }
    }

    public void anrErrorClearedOnProcessMonitor() {
        setInAnrState(false);
        notifyStateListeners(AnonymousClass04f.NO_ANR_DETECTED);
        if (shouldCollectAndUploadANRReportsNow()) {
            anrHasEnded(true);
        }
    }

    public ProcessAnrErrorMonitorListener getProcessAnrErrorMonitorListener() {
        if (0 != 0) {
            return this.mProcessAnrErrorMonitorListener;
        }
        throw new AssertionError();
    }

    public boolean isRunning() {
        if (0 != 0) {
            return this.mRunning;
        }
        throw new AssertionError();
    }

    public void startForTesting(HandlerThread handlerThread, long j) {
        if (0 != 0) {
            sNativeInited = true;
            this.mRunning = true;
            this.mProcessingThread = handlerThread;
            handlerThread.start();
            this.mProcessingThreadHandler = new Handler(this.mProcessingThread.getLooper());
            if (j == -1) {
                j = SystemClock.uptimeMillis();
            }
            this.mDetectorStartTime = j;
            return;
        }
        throw new AssertionError();
    }

    public static synchronized SigquitBasedANRDetector getTestInstance(ANRDetectorConfig aNRDetectorConfig, ProcessAnrErrorMonitor processAnrErrorMonitor) {
        SigquitBasedANRDetector sigquitBasedANRDetector;
        synchronized (SigquitBasedANRDetector.class) {
            if (0 != 0) {
                sigquitBasedANRDetector = new SigquitBasedANRDetector(aNRDetectorConfig, processAnrErrorMonitor);
                sInstance = sigquitBasedANRDetector;
            } else {
                throw new AssertionError();
            }
        }
        return sigquitBasedANRDetector;
    }

    public static void installSignalHandlerAndMaybeStart(SigquitBasedANRDetector sigquitBasedANRDetector, boolean z) {
        synchronized (sigquitBasedANRDetector.mStateLock) {
            if (!sNativeInited) {
                ANRDetectorConfig aNRDetectorConfig = sigquitBasedANRDetector.mANRConfig;
                final AppStateUpdater appStateUpdater = aNRDetectorConfig.mAppStateUpdater;
                aNRDetectorConfig.mAppStateUpdater = new AppStateUpdater() {
                    public void updateAnrState(AnonymousClass04f r3, Runnable runnable) {
                        AnonymousClass1 r1;
                        if (r3 == AnonymousClass04f.DURING_ANR || !SigquitBasedANRDetector.this.mANRConfig.mCleanupOnASLThread) {
                            if (r3 == AnonymousClass04f.NO_ANR_DETECTED) {
                                SigquitDetector.nativeCleanupAppStateFile();
                            }
                            r1 = null;
                        } else {
                            r1 = new Runnable() {
                                public void run() {
                                    SigquitDetector.nativeCleanupAppStateFile();
                                }
                            };
                        }
                        AppStateUpdater appStateUpdater = appStateUpdater;
                        if (appStateUpdater != null) {
                            appStateUpdater.updateAnrState(r3, r1);
                        }
                    }
                };
                sigquitBasedANRDetector.mSigquitDetector.init(aNRDetectorConfig);
                sNativeInited = true;
            }
            sigquitBasedANRDetector.mSigquitDetector.installSignalHandler(sigquitBasedANRDetector.mProcessingThreadHandler, z);
        }
    }

    private boolean isProcessErrorMonitorMonitoring() {
        ProcessAnrErrorMonitor processAnrErrorMonitor = this.mProcessAnrErrorMonitor;
        if (processAnrErrorMonitor == null || processAnrErrorMonitor.getState() == ProcessAnrErrorMonitor.State.NOT_MONITORING) {
            return false;
        }
        return true;
    }

    public static void maybeStartACRAReport(SigquitBasedANRDetector sigquitBasedANRDetector, String str, String str2, long j) {
        boolean shouldCollectAndUploadANRReports;
        if (sigquitBasedANRDetector.mInForegroundV1 || sigquitBasedANRDetector.mInForegroundV2) {
            shouldCollectAndUploadANRReports = sigquitBasedANRDetector.shouldCollectAndUploadANRReports();
        } else {
            shouldCollectAndUploadANRReports = false;
        }
        if (shouldCollectAndUploadANRReports) {
            try {
                sigquitBasedANRDetector.startReport(str, str2, Long.valueOf(j));
            } catch (IOException e) {
                C010708t.A0R(LOG_TAG, e, "Error saving ANR report");
            }
        } else if (str2 != null) {
            new File(str2).delete();
        }
    }

    private void maybeStartProcessErrorMonitor() {
        synchronized (this.mStateLock) {
            if (this.mStartProcessErrorMonitorAfterANRDetection) {
                if (this.mProcessAnrErrorMonitor == null) {
                    ANRDetectorConfig aNRDetectorConfig = this.mANRConfig;
                    this.mProcessAnrErrorMonitor = new ProcessAnrErrorMonitor(aNRDetectorConfig.mContext, aNRDetectorConfig.mProcessName, aNRDetectorConfig.mMaxNumberOfProcessMonitorChecksBeforeError, aNRDetectorConfig.mMaxNumberOfProcessMonitorChecksAfterError);
                }
                if (this.mProcessAnrErrorMonitor.getState() != ProcessAnrErrorMonitor.State.NOT_MONITORING) {
                    this.mProcessAnrErrorMonitor.stopMonitoring();
                }
                ProcessAnrErrorMonitorListener processAnrErrorMonitorListener = new ProcessAnrErrorMonitorListener();
                this.mProcessAnrErrorMonitor.startMonitoringAfterDelay(processAnrErrorMonitorListener, 0);
                this.mProcessAnrErrorMonitorListener = processAnrErrorMonitorListener;
            }
        }
    }

    private void stopHandlerThread() {
        synchronized (this.mProcessingThreadLock) {
            this.mProcessingThreadHandler = null;
            HandlerThread handlerThread = this.mProcessingThread;
            if (handlerThread != null) {
                handlerThread.quit();
                this.mProcessingThread = null;
            }
        }
    }

    public static void updateForegroundState(SigquitBasedANRDetector sigquitBasedANRDetector, boolean z, boolean z2) {
        sigquitBasedANRDetector.mInForegroundV1 = z;
        sigquitBasedANRDetector.mInForegroundV2 = z2;
    }

    public long getReadyTime() {
        long j;
        synchronized (this.mStateLock) {
            j = this.mDetectorReadyTime;
        }
        return j;
    }

    public long getSwitchTime() {
        long j;
        synchronized (this.mStateLock) {
            j = this.mSwitchTime;
        }
        return j;
    }

    public void onHookedMethods(boolean z) {
        if (z) {
            synchronized (this.mStateLock) {
                this.mHookInPlace = true;
                NativeInitListener nativeInitListener = this.mNativeInitListener;
                if (nativeInitListener != null) {
                    nativeInitListener.onNativeInit();
                }
                this.mNativeInitListener = null;
            }
            return;
        }
        stopHandlerThread();
    }

    public void setReadyTime(long j) {
        synchronized (this.mStateLock) {
            this.mDetectorReadyTime = j;
        }
    }

    public void setSwitchTime(long j) {
        synchronized (this.mStateLock) {
            this.mSwitchTime = j;
        }
    }

    public void sigquitDetected(String str, String str2, boolean z, boolean z2) {
        final boolean z3;
        C010708t.A0J(LOG_TAG, "On sigquitDetected call");
        final long uptimeMillis = SystemClock.uptimeMillis();
        if (!isDebuggerConnected() && this.mRunning) {
            if (this.mANRConfig.mRecoveryTimeout > 0) {
                synchronized (this.mWaitingToClearANRLock) {
                    try {
                        synchronized (this.mProcessingThreadLock) {
                            Handler handler = this.mProcessingThreadHandler;
                            if (handler != null) {
                                this.mWaitingForANRClearTimeout = false;
                                AnonymousClass00S.A02(handler, this.mClearAnrStateRunnable);
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
            }
            if (inAnrState()) {
                C010708t.A0J(LOG_TAG, "Detected a new ANR before the end of the previous one");
                z3 = true;
            } else {
                z3 = false;
            }
            setInAnrState(true);
            synchronized (this.mProcessingThreadLock) {
                try {
                    Handler handler2 = this.mProcessingThreadHandler;
                    if (handler2 != null) {
                        final String str3 = str2;
                        final String str4 = str;
                        final boolean z4 = z2;
                        final boolean z5 = z;
                        AnonymousClass00S.A04(handler2, new Runnable() {
                            public void run() {
                                if (z3) {
                                    C010708t.A0J(SigquitBasedANRDetector.LOG_TAG, "Clearing current ANR");
                                    SigquitBasedANRDetector.this.anrErrorClearedOnProcessMonitor();
                                }
                                C010708t.A0J(SigquitBasedANRDetector.LOG_TAG, "On processing thread handling ANR start");
                                SigquitBasedANRDetector.updateForegroundState(SigquitBasedANRDetector.this, z5, z4);
                                SigquitBasedANRDetector.this.notifyStateListeners(AnonymousClass04f.DURING_ANR);
                                AnonymousClass00S.A04(SigquitBasedANRDetector.this.mANRConfig.mMainThreadHandler, new Runnable() {
                                    public void run() {
                                        SigquitBasedANRDetector.mainThreadUnblocked(SigquitBasedANRDetector.this);
                                    }
                                }, 286887716);
                                SigquitBasedANRDetector.maybeStartACRAReport(SigquitBasedANRDetector.this, str4, str3, uptimeMillis);
                            }
                        }, -589628868);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public void start(long j) {
        synchronized (this.mStateLock) {
            if (this.mDetectorStartTime <= 0) {
                this.mDetectorStartTime = j;
            }
            if (this.mHookInPlace && !this.mRunning) {
                if (this.mDetectorStartTime == -1) {
                    this.mDetectorStartTime = SystemClock.uptimeMillis();
                }
                this.mRunning = true;
            }
        }
    }

    public void stop(IANRDetector.ANRDetectorStopListener aNRDetectorStopListener) {
        synchronized (this.mStateLock) {
            if (this.mHookInPlace) {
                this.mRunning = false;
                SigquitDetector.nativeStopDetector();
                stopHandlerThread();
            }
        }
        if (aNRDetectorStopListener != null) {
            aNRDetectorStopListener.onStop();
        }
    }

    public static /* synthetic */ String access$200() {
        return LOG_TAG;
    }

    public static void mainThreadUnblocked(SigquitBasedANRDetector sigquitBasedANRDetector) {
        if (sigquitBasedANRDetector.inAnrState()) {
            final long uptimeMillis = SystemClock.uptimeMillis();
            synchronized (sigquitBasedANRDetector.mProcessingThreadLock) {
                Handler handler = sigquitBasedANRDetector.mProcessingThreadHandler;
                if (handler != null) {
                    AnonymousClass00S.A04(handler, new Runnable() {
                        public void run() {
                            if (SigquitBasedANRDetector.this.inAnrState()) {
                                SigquitBasedANRDetector.this.logMainThreadUnblocked(uptimeMillis);
                                SigquitBasedANRDetector.this.notifyStateListeners(AnonymousClass04f.ANR_RECOVERED);
                            }
                        }
                    }, -183509534);
                }
            }
        }
    }

    public void processMonitorStopped(int i) {
        super.processMonitorStopped(i);
        if (this.mANRConfig.mRecoveryTimeout > 0) {
            synchronized (this.mWaitingToClearANRLock) {
                synchronized (this.mProcessingThreadLock) {
                    Handler handler = this.mProcessingThreadHandler;
                    if (handler != null) {
                        this.mWaitingForANRClearTimeout = true;
                        AnonymousClass00S.A05(handler, this.mClearAnrStateRunnable, (long) this.mANRConfig.mRecoveryTimeout, 2021088198);
                    }
                }
            }
        }
    }

    private SigquitBasedANRDetector(ANRDetectorConfig aNRDetectorConfig) {
        super(aNRDetectorConfig);
        this.mStateLock = new Object();
        this.mProcessingThreadLock = new Object();
        this.mWaitingToClearANRLock = new Object();
        this.mClearAnrStateRunnable = new Runnable() {
            public void run() {
                synchronized (SigquitBasedANRDetector.this.mWaitingToClearANRLock) {
                    SigquitBasedANRDetector sigquitBasedANRDetector = SigquitBasedANRDetector.this;
                    if (sigquitBasedANRDetector.mWaitingForANRClearTimeout) {
                        sigquitBasedANRDetector.notifyStateListeners(AnonymousClass04f.NO_ANR_DETECTED);
                        if (SigquitBasedANRDetector.this.shouldCollectAndUploadANRReportsNow()) {
                            SigquitBasedANRDetector.this.anrHasEnded(true);
                        }
                        SigquitBasedANRDetector.this.mWaitingForANRClearTimeout = false;
                    }
                }
            }
        };
        this.mSigquitDetector = SigquitDetector.getInstance(this);
        this.mProcessAnrErrorMonitor = null;
        this.mStartProcessErrorMonitorAfterANRDetection = true;
    }

    private SigquitBasedANRDetector(ANRDetectorConfig aNRDetectorConfig, ProcessAnrErrorMonitor processAnrErrorMonitor) {
        super(aNRDetectorConfig);
        this.mStateLock = new Object();
        this.mProcessingThreadLock = new Object();
        this.mWaitingToClearANRLock = new Object();
        this.mClearAnrStateRunnable = new Runnable() {
            public void run() {
                synchronized (SigquitBasedANRDetector.this.mWaitingToClearANRLock) {
                    SigquitBasedANRDetector sigquitBasedANRDetector = SigquitBasedANRDetector.this;
                    if (sigquitBasedANRDetector.mWaitingForANRClearTimeout) {
                        sigquitBasedANRDetector.notifyStateListeners(AnonymousClass04f.NO_ANR_DETECTED);
                        if (SigquitBasedANRDetector.this.shouldCollectAndUploadANRReportsNow()) {
                            SigquitBasedANRDetector.this.anrHasEnded(true);
                        }
                        SigquitBasedANRDetector.this.mWaitingForANRClearTimeout = false;
                    }
                }
            }
        };
        this.mSigquitDetector = SigquitDetector.getInstance(this);
        this.mProcessAnrErrorMonitor = processAnrErrorMonitor;
        this.mStartProcessErrorMonitorAfterANRDetection = true;
    }

    private static synchronized SigquitBasedANRDetector getInstance() {
        SigquitBasedANRDetector sigquitBasedANRDetector;
        synchronized (SigquitBasedANRDetector.class) {
            sigquitBasedANRDetector = sInstance;
            if (sigquitBasedANRDetector == null) {
                throw new IllegalStateException("Instance has not been initialized yet");
            }
        }
        return sigquitBasedANRDetector;
    }

    public static synchronized SigquitBasedANRDetector getInstance(ANRDetectorConfig aNRDetectorConfig) {
        SigquitBasedANRDetector sigquitBasedANRDetector;
        synchronized (SigquitBasedANRDetector.class) {
            if (sInstance == null) {
                sInstance = new SigquitBasedANRDetector(aNRDetectorConfig);
            }
            sigquitBasedANRDetector = sInstance;
        }
        return sigquitBasedANRDetector;
    }

    public void nativeLibraryLoaded(NativeInitListener nativeInitListener, boolean z) {
        synchronized (this.mStateLock) {
            this.mNativeInitListener = nativeInitListener;
            nativeLibraryLoaded(z);
        }
    }

    public void nativeLibraryLoaded(final boolean z) {
        synchronized (this.mStateLock) {
            if (!this.mHookInPlace) {
                synchronized (this.mProcessingThreadLock) {
                    if (this.mProcessingThread == null) {
                        HandlerThread handlerThread = new HandlerThread("SigquitBasedANRDetectorThread");
                        this.mProcessingThread = handlerThread;
                        handlerThread.start();
                        this.mProcessingThreadHandler = new Handler(this.mProcessingThread.getLooper());
                    }
                    AnonymousClass00S.A04(this.mProcessingThreadHandler, new Runnable() {
                        public void run() {
                            SigquitBasedANRDetector.installSignalHandlerAndMaybeStart(SigquitBasedANRDetector.this, z);
                        }
                    }, 131752079);
                }
            }
        }
    }

    public void notifyStateListeners(AnonymousClass04f r7) {
        ANRDetectorConfig aNRDetectorConfig = this.mANRConfig;
        boolean z = false;
        if (aNRDetectorConfig.mShouldUseFgStateInDetectorOnAslUpdate) {
            if (this.mInForegroundV2 || this.mInForegroundV1) {
                z = true;
            }
            notifyStateListeners(r7, z);
            return;
        }
        AppStateUpdater appStateUpdater = aNRDetectorConfig.mAppStateUpdater;
        AnonymousClass04f r0 = AnonymousClass04f.DURING_ANR;
        if (r7 == r0) {
            if (appStateUpdater != null) {
                boolean isAppInForegroundV1 = appStateUpdater.isAppInForegroundV1();
                if (appStateUpdater.isAppInForegroundV2() || isAppInForegroundV1) {
                    z = true;
                }
                appStateUpdater.updateAnrState(r0, null, z);
            }
            maybeStartProcessErrorMonitor();
        } else if (appStateUpdater == null) {
        } else {
            if (isProcessErrorMonitorMonitoring() || r7 == AnonymousClass04f.ANR_RECOVERED) {
                appStateUpdater.updateAnrState(AnonymousClass04f.ANR_RECOVERED, null);
            } else {
                appStateUpdater.updateAnrState(AnonymousClass04f.NO_ANR_DETECTED, null);
            }
        }
    }

    private void notifyStateListeners(AnonymousClass04f r4, boolean z) {
        AppStateUpdater appStateUpdater = this.mANRConfig.mAppStateUpdater;
        AnonymousClass04f r0 = AnonymousClass04f.DURING_ANR;
        if (r4 == r0) {
            if (appStateUpdater != null) {
                appStateUpdater.updateAnrState(r0, null, z);
            }
            maybeStartProcessErrorMonitor();
        } else if (appStateUpdater == null) {
        } else {
            if (isProcessErrorMonitorMonitoring() || r4 == AnonymousClass04f.ANR_RECOVERED) {
                appStateUpdater.updateAnrState(AnonymousClass04f.ANR_RECOVERED, null);
            } else {
                appStateUpdater.updateAnrState(AnonymousClass04f.NO_ANR_DETECTED, null);
            }
        }
    }
}
