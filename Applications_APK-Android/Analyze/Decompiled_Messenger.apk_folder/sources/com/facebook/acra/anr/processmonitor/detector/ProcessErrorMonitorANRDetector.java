package com.facebook.acra.anr.processmonitor.detector;

import X.AnonymousClass04f;
import X.C010708t;
import android.os.Looper;
import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.ANRException;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anr.base.AbstractANRDetector;
import com.facebook.acra.anr.processmonitor.ProcessAnrErrorMonitor;
import com.facebook.acra.anr.processmonitor.ProcessErrorStateListener;

public class ProcessErrorMonitorANRDetector extends AbstractANRDetector implements ProcessErrorStateListener {
    private static final String LOG_TAG = "ProcessErrorMonitorANRDetector";
    private static final int START_DELAY_MS = 4000;
    private static ProcessErrorMonitorANRDetector sInstance;
    private final ProcessAnrErrorMonitor mAnrErrorMonitor;
    private long mDetectorStartTime;
    public boolean mErrorCleared;
    private boolean mFirstStart = true;
    private boolean mInAnr;
    private final Object mReportLock = new Object();
    private boolean mShouldReport;

    public class ThreadProvider {
        public static void runOnNewThread(Runnable runnable) {
            new Thread(runnable, "ProcessErrorMonitorANRDetectorThread").start();
        }
    }

    public static synchronized void endAndProcessANRDataCapture(ProcessErrorMonitorANRDetector processErrorMonitorANRDetector) {
        synchronized (processErrorMonitorANRDetector) {
            if (processErrorMonitorANRDetector.mInAnr) {
                processErrorMonitorANRDetector.mInAnr = false;
                ThreadProvider.runOnNewThread(new Runnable() {
                    public void run() {
                        synchronized (ProcessErrorMonitorANRDetector.this) {
                            ProcessErrorMonitorANRDetector processErrorMonitorANRDetector = ProcessErrorMonitorANRDetector.this;
                            if (!processErrorMonitorANRDetector.mErrorCleared) {
                                processErrorMonitorANRDetector.notifyStateListeners(AnonymousClass04f.ANR_RECOVERED);
                            }
                        }
                    }
                });
            }
        }
    }

    public void onCheckFailed() {
    }

    public void onCheckPerformed() {
    }

    public void onErrorCleared() {
        boolean z;
        synchronized (this) {
            try {
                z = this.mShouldReport;
                if (!this.mErrorCleared) {
                    this.mErrorCleared = true;
                    notifyStateListeners(AnonymousClass04f.NO_ANR_DETECTED);
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        synchronized (this.mReportLock) {
            try {
                anrHasEnded(z);
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public boolean onErrorDetectOnOtherProcess(String str, String str2, String str3) {
        return true;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0039 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onErrorDetected(java.lang.String r4, java.lang.String r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 1
            r3.mInAnr = r0     // Catch:{ all -> 0x0040 }
            r0 = 0
            r3.mErrorCleared = r0     // Catch:{ all -> 0x0040 }
            X.04f r0 = X.AnonymousClass04f.DURING_ANR     // Catch:{ all -> 0x0040 }
            r3.notifyStateListeners(r0)     // Catch:{ all -> 0x0040 }
            boolean r0 = r3.shouldCollectAndUploadANRReportsNow()     // Catch:{ all -> 0x0040 }
            r3.mShouldReport = r0     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x001f
            com.facebook.acra.anr.ANRDetectorConfig r0 = r3.mANRConfig     // Catch:{ all -> 0x0040 }
            com.facebook.acra.anr.IANRReport r2 = r0.mANRReport     // Catch:{ all -> 0x0040 }
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0040 }
            r2.logSystemInfo(r4, r5, r0)     // Catch:{ all -> 0x0040 }
        L_0x001f:
            monitor-exit(r3)     // Catch:{ all -> 0x0040 }
            com.facebook.acra.anr.ANRDetectorConfig r0 = r3.mANRConfig
            android.os.Handler r2 = r0.mMainThreadHandler
            com.facebook.acra.anr.processmonitor.detector.ProcessErrorMonitorANRDetector$1 r1 = new com.facebook.acra.anr.processmonitor.detector.ProcessErrorMonitorANRDetector$1
            r1.<init>()
            r0 = -376456408(0xffffffffe98fbb28, float:-2.1720027E25)
            X.AnonymousClass00S.A04(r2, r1, r0)
            boolean r0 = r3.mShouldReport
            if (r0 == 0) goto L_0x003f
            java.lang.Object r1 = r3.mReportLock
            monitor-enter(r1)
            r3.startReport()     // Catch:{ IOException -> 0x0039 }
        L_0x0039:
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            goto L_0x003e
        L_0x003b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            goto L_0x0042
        L_0x003e:
            return
        L_0x003f:
            return
        L_0x0040:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0040 }
        L_0x0042:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.processmonitor.detector.ProcessErrorMonitorANRDetector.onErrorDetected(java.lang.String, java.lang.String):void");
    }

    public void onMaxChecksReachedAfterError() {
    }

    public void onMaxChecksReachedBeforeError() {
    }

    public void onStart() {
    }

    public synchronized void start(long j) {
        if (this.mDetectorStartTime <= 0) {
            this.mDetectorStartTime = j;
        }
        if (this.mFirstStart) {
            this.mFirstStart = false;
            this.mAnrErrorMonitor.startMonitoringAfterDelay(this, 4000);
        }
    }

    public void stop(IANRDetector.ANRDetectorStopListener aNRDetectorStopListener) {
        synchronized (this) {
            this.mAnrErrorMonitor.stopMonitoring();
        }
        if (aNRDetectorStopListener != null) {
            aNRDetectorStopListener.onStop();
        }
    }

    public static synchronized ProcessErrorMonitorANRDetector getInstance(ANRDetectorConfig aNRDetectorConfig, int i) {
        ProcessErrorMonitorANRDetector processErrorMonitorANRDetector;
        synchronized (ProcessErrorMonitorANRDetector.class) {
            if (sInstance == null) {
                sInstance = new ProcessErrorMonitorANRDetector(aNRDetectorConfig, i);
            }
            processErrorMonitorANRDetector = sInstance;
        }
        return processErrorMonitorANRDetector;
    }

    public static synchronized void resetInstance() {
        synchronized (ProcessErrorMonitorANRDetector.class) {
            sInstance = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (r2 != false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void notifyStateListeners(X.AnonymousClass04f r7) {
        /*
            r6 = this;
            com.facebook.acra.anr.ANRDetectorConfig r0 = r6.mANRConfig
            com.facebook.acra.anr.AppStateUpdater r5 = r0.mAppStateUpdater
            X.04f r0 = X.AnonymousClass04f.DURING_ANR
            r4 = 0
            if (r7 != r0) goto L_0x0023
            if (r5 == 0) goto L_0x0022
            boolean r3 = r5.isAppInForegroundV2()
            boolean r2 = r5.isAppInForegroundV1()
            X.04f r1 = X.AnonymousClass04f.DURING_ANR
            if (r3 != 0) goto L_0x001a
            r0 = 0
            if (r2 == 0) goto L_0x001b
        L_0x001a:
            r0 = 1
        L_0x001b:
            r5.updateAnrState(r1, r4, r0)
            r6.mInForegroundV1 = r2
            r6.mInForegroundV2 = r3
        L_0x0022:
            return
        L_0x0023:
            if (r5 == 0) goto L_0x0022
            X.04f r0 = X.AnonymousClass04f.ANR_RECOVERED
            if (r7 == r0) goto L_0x002f
            X.04f r0 = X.AnonymousClass04f.NO_ANR_DETECTED
            r5.updateAnrState(r0, r4)
            return
        L_0x002f:
            r5.updateAnrState(r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.processmonitor.detector.ProcessErrorMonitorANRDetector.notifyStateListeners(X.04f):void");
    }

    private ProcessErrorMonitorANRDetector(ANRDetectorConfig aNRDetectorConfig, int i) {
        super(aNRDetectorConfig);
        this.mAnrErrorMonitor = new ProcessAnrErrorMonitor(aNRDetectorConfig.mContext, aNRDetectorConfig.mProcessName, false, i, true, 0, 0);
    }

    public void collectStackTrace() {
        StackTraceElement[] stackTrace = Looper.getMainLooper().getThread().getStackTrace();
        ANRException aNRException = new ANRException("ANR detected by ProcessErrorMonitorAnrDetector");
        aNRException.setStackTrace(stackTrace);
        C010708t.A0R(LOG_TAG, aNRException, "Generating ANR Report");
    }
}
