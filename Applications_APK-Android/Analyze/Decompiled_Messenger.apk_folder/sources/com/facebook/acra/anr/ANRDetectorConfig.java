package com.facebook.acra.anr;

import android.content.Context;
import android.os.Handler;
import java.io.File;
import java.io.IOException;

public class ANRDetectorConfig {
    public final IANRReport mANRReport;
    public AppStateUpdater mAppStateUpdater;
    public final String mAppVersionCode;
    public final String mAppVersionName;
    public final boolean mCachedShouldUploadANRReports;
    public final boolean mCleanupOnASLThread;
    public final Context mContext;
    public final int mDetectorId;
    public final int mExpirationTimeAfterMainThreadUnblocked;
    public final int mForegroundCheckPeriod;
    public final boolean mIsInternalBuild;
    public final Handler mMainThreadHandler;
    public final int mMaxNumberOfProcessMonitorChecksAfterError;
    public final int mMaxNumberOfProcessMonitorChecksBeforeError;
    public final String mProcessName;
    public final int mRecoveryTimeout;
    public final boolean mShouldAvoidMutexOnSignalHandler;
    public final boolean mShouldLogOnSignalHandler;
    public final boolean mShouldRecordSignalTime;
    public final boolean mShouldReportSoftErrors;
    public final boolean mShouldUseFgStateInDetectorOnAslUpdate;
    public final String mTracesExtension;
    public final String mTracesPath;
    public final boolean mUseStaticMethodCallback;

    public File getSigquitTimeDir() {
        return this.mContext.getDir("sigquit", 0);
    }

    public boolean processShouldSendAnrReports() {
        if (!this.mProcessName.contains(":") || this.mProcessName.endsWith(":browser")) {
            return true;
        }
        return false;
    }

    public IANRReport getANRReport() {
        return this.mANRReport;
    }

    public AppStateUpdater getAppStateUpdater() {
        return this.mAppStateUpdater;
    }

    public String getAppVersionCode() {
        return this.mAppVersionCode;
    }

    public String getAppVersionName() {
        return this.mAppVersionName;
    }

    public boolean getCachedShouldUploadANRReports() {
        return this.mCachedShouldUploadANRReports;
    }

    public Context getContext() {
        return this.mContext;
    }

    public int getDetectorId() {
        return this.mDetectorId;
    }

    public int getExpirationTimeAfterMainThreadUnblocked() {
        return this.mExpirationTimeAfterMainThreadUnblocked;
    }

    public int getForegroundCheckPeriod() {
        return this.mForegroundCheckPeriod;
    }

    public Handler getMainThreadHandler() {
        return this.mMainThreadHandler;
    }

    public int getMaxNumberOfProcessMonitorChecksAfterError() {
        return this.mMaxNumberOfProcessMonitorChecksAfterError;
    }

    public int getMaxNumberOfProcessMonitorChecksBeforeError() {
        return this.mMaxNumberOfProcessMonitorChecksBeforeError;
    }

    public String getProcessName() {
        return this.mProcessName;
    }

    public int getRecoveryTimeout() {
        return this.mRecoveryTimeout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String getSigquitTimeFilePath() {
        try {
            return new File(getSigquitTimeDir(), this.mProcessName.replace('.', '_').replace(':', '_')).getCanonicalPath();
        } catch (IOException unused) {
            return null;
        }
    }

    public String getTracesExtension() {
        return this.mTracesExtension;
    }

    public String getTracesPath() {
        return this.mTracesPath;
    }

    public boolean isInternalBuild() {
        return this.mIsInternalBuild;
    }

    public boolean shouldAvoidMutexOnSignalHandler() {
        return this.mShouldAvoidMutexOnSignalHandler;
    }

    public boolean shouldCleanupANRStateOnASLThread() {
        return this.mCleanupOnASLThread;
    }

    public boolean shouldLogOnSignalHandler() {
        return this.mShouldLogOnSignalHandler;
    }

    public boolean shouldRecordSignalTime() {
        return this.mShouldRecordSignalTime;
    }

    public boolean shouldReportSoftErrors() {
        return this.mShouldReportSoftErrors;
    }

    public boolean shouldUseFgStateInDetectorOnASLUpdate() {
        return this.mShouldUseFgStateInDetectorOnAslUpdate;
    }

    public boolean shouldUseStaticMethodCallback() {
        return this.mUseStaticMethodCallback;
    }

    public void setAppStateUpdater(AppStateUpdater appStateUpdater) {
        this.mAppStateUpdater = appStateUpdater;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ANRDetectorConfig(android.content.Context r26, java.lang.String r27, com.facebook.acra.anr.IANRReport r28, com.facebook.acra.anr.AppStateUpdater r29, android.os.Handler r30, int r31, boolean r32, boolean r33) {
        /*
            r25 = this;
            r0 = r25
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = -1
            r13 = 0
            r14 = 20
            r15 = 100
            r16 = 0
            r17 = 0
            java.lang.String r18 = "1"
            java.lang.String r19 = "1"
            java.lang.String r20 = "."
            java.lang.String r21 = "anr"
            r22 = 0
            r23 = 0
            r24 = 0
            r6 = r31
            r5 = r30
            r7 = r32
            r8 = r33
            r1 = r26
            r2 = r27
            r3 = r28
            r4 = r29
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anr.ANRDetectorConfig.<init>(android.content.Context, java.lang.String, com.facebook.acra.anr.IANRReport, com.facebook.acra.anr.AppStateUpdater, android.os.Handler, int, boolean, boolean):void");
    }

    public ANRDetectorConfig(Context context, String str, IANRReport iANRReport, AppStateUpdater appStateUpdater, Handler handler, int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, int i2, boolean z6, int i3, int i4, boolean z7, boolean z8, String str2, String str3, String str4, String str5, boolean z9, int i5, int i6) {
        this.mContext = context;
        this.mProcessName = str;
        this.mANRReport = iANRReport;
        this.mAppStateUpdater = appStateUpdater;
        this.mMainThreadHandler = handler;
        this.mDetectorId = i;
        this.mIsInternalBuild = z;
        this.mCleanupOnASLThread = z2;
        this.mShouldReportSoftErrors = z3;
        this.mShouldLogOnSignalHandler = z4;
        this.mShouldAvoidMutexOnSignalHandler = z5;
        this.mRecoveryTimeout = i2;
        this.mUseStaticMethodCallback = z6;
        this.mMaxNumberOfProcessMonitorChecksBeforeError = i3;
        this.mMaxNumberOfProcessMonitorChecksAfterError = i4;
        this.mShouldRecordSignalTime = z7;
        this.mCachedShouldUploadANRReports = z8;
        this.mAppVersionCode = str2;
        this.mAppVersionName = str3;
        this.mTracesPath = str4;
        this.mTracesExtension = str5;
        this.mShouldUseFgStateInDetectorOnAslUpdate = z9;
        this.mExpirationTimeAfterMainThreadUnblocked = i5;
        this.mForegroundCheckPeriod = i6;
    }
}
