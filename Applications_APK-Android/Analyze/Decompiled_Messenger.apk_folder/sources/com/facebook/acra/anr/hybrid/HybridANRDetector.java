package com.facebook.acra.anr.hybrid;

import android.os.SystemClock;
import com.facebook.acra.anr.ANRDataProvider;
import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.ANRDetectorListener;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anr.base.AbstractANRDetector;
import com.facebook.acra.anr.processmonitor.detector.ProcessErrorMonitorANRDetector;
import com.facebook.acra.anr.sigquit.detector.SigquitBasedANRDetector;

public class HybridANRDetector implements IANRDetector, IANRDetector.ANRDetectorStopListener, SigquitBasedANRDetector.NativeInitListener {
    private static final String LOG_TAG = "HybridANRDetector";
    private static HybridANRDetector sInstance;
    private long mDetectorStartTime;
    private AbstractANRDetector mInitialDetector;
    private boolean mNativeLibLoadedCalled;
    private boolean mRunning;
    private SigquitBasedANRDetector mSigquitBasedDetector;

    public synchronized void nativeLibraryLoaded(boolean z) {
        if (!this.mNativeLibLoadedCalled) {
            boolean z2 = true;
            this.mNativeLibLoadedCalled = true;
            if (this.mInitialDetector != null) {
                SigquitBasedANRDetector sigquitBasedANRDetector = this.mSigquitBasedDetector;
                if (!z && !this.mRunning) {
                    z2 = false;
                }
                sigquitBasedANRDetector.nativeLibraryLoaded(this, z2);
            }
        }
    }

    public synchronized void onNativeInit() {
        AbstractANRDetector abstractANRDetector = this.mInitialDetector;
        if (abstractANRDetector != null) {
            abstractANRDetector.stop(this);
        }
    }

    public synchronized void setANRDataProvider(ANRDataProvider aNRDataProvider) {
        AbstractANRDetector abstractANRDetector = this.mInitialDetector;
        if (abstractANRDetector != null) {
            abstractANRDetector.setANRDataProvider(aNRDataProvider);
        }
        this.mSigquitBasedDetector.setANRDataProvider(aNRDataProvider);
    }

    public synchronized void setCheckIntervalMs(long j) {
    }

    public synchronized void setListener(ANRDetectorListener aNRDetectorListener) {
        AbstractANRDetector abstractANRDetector = this.mInitialDetector;
        if (abstractANRDetector != null) {
            abstractANRDetector.setListener(aNRDetectorListener);
        }
        this.mSigquitBasedDetector.setListener(aNRDetectorListener);
    }

    public synchronized void start() {
        long uptimeMillis = SystemClock.uptimeMillis();
        this.mDetectorStartTime = uptimeMillis;
        AbstractANRDetector abstractANRDetector = this.mInitialDetector;
        if (abstractANRDetector != null) {
            abstractANRDetector.start(uptimeMillis);
        } else {
            this.mSigquitBasedDetector.start(uptimeMillis);
        }
        this.mRunning = true;
    }

    public synchronized void stop(IANRDetector.ANRDetectorStopListener aNRDetectorStopListener) {
        AbstractANRDetector abstractANRDetector = this.mInitialDetector;
        if (abstractANRDetector != null) {
            abstractANRDetector.stop(aNRDetectorStopListener);
        } else {
            this.mSigquitBasedDetector.stop(aNRDetectorStopListener);
        }
        this.mRunning = false;
    }

    public static synchronized HybridANRDetector getInstance(ANRDetectorConfig aNRDetectorConfig, int i) {
        HybridANRDetector hybridANRDetector;
        synchronized (HybridANRDetector.class) {
            if (sInstance == null) {
                sInstance = new HybridANRDetector(aNRDetectorConfig, i);
            }
            hybridANRDetector = sInstance;
        }
        return hybridANRDetector;
    }

    public static synchronized HybridANRDetector getTestInstance(ProcessErrorMonitorANRDetector processErrorMonitorANRDetector, SigquitBasedANRDetector sigquitBasedANRDetector) {
        HybridANRDetector hybridANRDetector;
        synchronized (HybridANRDetector.class) {
            hybridANRDetector = new HybridANRDetector(processErrorMonitorANRDetector, sigquitBasedANRDetector);
            sInstance = hybridANRDetector;
        }
        return hybridANRDetector;
    }

    public void onStop() {
        this.mSigquitBasedDetector.setSwitchTime(SystemClock.uptimeMillis());
        synchronized (this) {
            this.mInitialDetector = null;
            this.mSigquitBasedDetector.setReadyTime(SystemClock.uptimeMillis());
            if (this.mRunning) {
                this.mSigquitBasedDetector.start(this.mDetectorStartTime);
            }
        }
    }

    private HybridANRDetector(ANRDetectorConfig aNRDetectorConfig, int i) {
        this.mInitialDetector = ProcessErrorMonitorANRDetector.getInstance(aNRDetectorConfig, i);
        this.mSigquitBasedDetector = SigquitBasedANRDetector.getInstance(aNRDetectorConfig);
    }

    private HybridANRDetector(ProcessErrorMonitorANRDetector processErrorMonitorANRDetector, SigquitBasedANRDetector sigquitBasedANRDetector) {
        this.mInitialDetector = processErrorMonitorANRDetector;
        this.mSigquitBasedDetector = sigquitBasedANRDetector;
    }
}
