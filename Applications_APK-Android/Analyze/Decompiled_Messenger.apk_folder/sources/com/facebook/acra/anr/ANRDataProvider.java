package com.facebook.acra.anr;

public abstract class ANRDataProvider {
    public static final int DEFAULT_MAX_NUMBER_OF_PROCESS_MONITOR_CHECKS_AFTER_ERROR = 100;
    public static final int DEFAULT_MAX_NUMBER_OF_PROCESS_MONITOR_CHECKS_BEFORE_ERROR = 20;

    public int detectionIntervalTimeMs() {
        return 0;
    }

    public int detectorToUse() {
        return 0;
    }

    public int getExpirationTimeoutOnMainThreadUnblocked() {
        return 0;
    }

    public int getForegroundCheckPeriod() {
        return 0;
    }

    public int getMaxNumberOfProcessMonitorChecksAfterError() {
        return 100;
    }

    public int getMaxNumberOfProcessMonitorChecksBeforeError() {
        return 20;
    }

    public int getRecoveryTimeout() {
        return -1;
    }

    public void provideDexStatus() {
    }

    public void provideLooperMonitorInfo() {
    }

    public void provideLooperProfileInfo() {
    }

    public void provideStats() {
    }

    public void reportSoftError(String str, Throwable th) {
    }

    public boolean shouldANRDetectorRun() {
        return false;
    }

    public boolean shouldAvoidMutexOnSignalHandler() {
        return false;
    }

    public boolean shouldCleanupStateOnASLThread() {
        return false;
    }

    public boolean shouldCollectAndUploadANRReports() {
        return false;
    }

    public boolean shouldDedupDiskPersistence() {
        return false;
    }

    public boolean shouldLogOnSignalHandler() {
        return false;
    }

    public boolean shouldLogProcessPositionInAnrTraceFile() {
        return false;
    }

    public boolean shouldRecordSignalTime() {
        return false;
    }

    public boolean shouldReportSoftErrors() {
        return false;
    }

    public boolean shouldRunANRDetectorOnBrowserProcess() {
        return false;
    }

    public boolean shouldUploadSystemANRTraces() {
        return false;
    }

    public boolean shouldUseFgStateFromDetectorInASL() {
        return false;
    }

    public boolean shouldUseStaticMethodCallback() {
        return false;
    }
}
