package com.facebook.acra;

import X.AnonymousClass08S;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Debug;
import android.os.Process;
import android.os.UserManager;
import io.card.payment.BuildConfig;
import java.util.Locale;

public class DumpSysCollector {

    public class Api17Utils {

        public class Api11Utils {
            public static String collectLargerMemoryInfo(Context context) {
                return AnonymousClass08S.A09("Large heap size =", ((ActivityManager) context.getSystemService("activity")).getLargeMemoryClass());
            }

            private Api11Utils() {
            }
        }

        public static String collectUserInfo(Context context) {
            return Long.toString(((UserManager) context.getSystemService("user")).getSerialNumberForUser(Process.myUserHandle()));
        }

        private Api17Utils() {
        }
    }

    public static String collectLargerMemoryInfo(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            return Api17Utils.Api11Utils.collectLargerMemoryInfo(context);
        }
        return BuildConfig.FLAVOR;
    }

    public static String collectMemInfo(Context context) {
        StringBuilder sb = new StringBuilder();
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        Debug.MemoryInfo memoryInfo2 = new Debug.MemoryInfo();
        Debug.getMemoryInfo(memoryInfo2);
        int memoryClass = activityManager.getMemoryClass();
        int i = memoryInfo2.nativePrivateDirty;
        int i2 = i + memoryInfo2.dalvikPrivateDirty;
        float f = ((float) memoryClass) * 1024.0f;
        int i3 = (int) (((float) (i * 100)) / f);
        int i4 = memoryInfo2.otherPrivateDirty;
        sb.append(String.format(Locale.US, "percent dalvik+native / native / d+n+other / other %d / %d / %d / %d", Integer.valueOf((int) ((((float) i2) / f) * 100.0f)), Integer.valueOf(i3), Integer.valueOf((int) (((float) ((i2 + i4) * 100)) / f)), Integer.valueOf((int) (((float) (i4 * 100)) / f))));
        StringBuilder sb2 = new StringBuilder("avail/thresh/low? ");
        long j = memoryInfo.availMem;
        sb2.append(j);
        sb2.append("/");
        long j2 = memoryInfo.threshold;
        sb2.append(j2);
        sb2.append("/");
        sb2.append(memoryInfo.lowMemory);
        sb2.append("/");
        sb2.append("(");
        sb2.append((int) (((float) (j2 * 100)) / ((float) j)));
        sb2.append("%) memclass=");
        sb2.append(memoryClass);
        sb.append(sb2.toString());
        sb.append("DebugMemInfo(kB): Private / Proportional / Shared");
        Locale locale = Locale.US;
        sb.append(String.format(locale, "          dalvik: %7d / %7d / %7d", Integer.valueOf(memoryInfo2.dalvikPrivateDirty), Integer.valueOf(memoryInfo2.dalvikPss), Integer.valueOf(memoryInfo2.dalvikSharedDirty)));
        sb.append(String.format(locale, "          native: %7d / %7d / %7d", Integer.valueOf(memoryInfo2.nativePrivateDirty), Integer.valueOf(memoryInfo2.nativePss), Integer.valueOf(memoryInfo2.nativeSharedDirty)));
        sb.append(String.format(locale, "           other: %7d / %7d / %7d", Integer.valueOf(memoryInfo2.otherPrivateDirty), Integer.valueOf(memoryInfo2.otherPss), Integer.valueOf(memoryInfo2.otherSharedDirty)));
        sb.append(String.format(locale, "GC: %d GCs, %d freed, %d free count", Integer.valueOf(Debug.getGlobalGcInvocationCount()), Integer.valueOf(Debug.getGlobalFreedSize()), Integer.valueOf(Debug.getGlobalFreedCount())));
        Locale locale2 = Locale.US;
        sb.append(String.format(locale2, "Native Heap: size/allocated/free %7d / %7d / %7d", Long.valueOf(Debug.getNativeHeapSize()), Long.valueOf(Debug.getNativeHeapAllocatedSize()), Long.valueOf(Debug.getNativeHeapFreeSize())));
        sb.append(String.format(locale2, "Threads: alloc count/alloc size/ext ac/ext as %7d / %7d / %7d / %7d", Integer.valueOf(Debug.getThreadAllocCount()), Integer.valueOf(Debug.getThreadAllocSize()), Integer.valueOf(Debug.getThreadExternalAllocCount()), Integer.valueOf(Debug.getThreadExternalAllocSize())));
        Runtime runtime = Runtime.getRuntime();
        sb.append(String.format(Locale.US, "Java Heap: size/allocated/free %7d / %7d / %7d", Long.valueOf(runtime.maxMemory()), Long.valueOf(runtime.totalMemory() - runtime.freeMemory()), Long.valueOf(runtime.freeMemory())));
        return sb.toString();
    }

    public static String collectUserInfo(Context context) {
        if (Build.VERSION.SDK_INT >= 17) {
            return Api17Utils.collectUserInfo(context);
        }
        return BuildConfig.FLAVOR;
    }
}
