package com.facebook.acra;

import X.AnonymousClass00V;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import com.facebook.acra.AppComponentStats;
import com.facebook.acra.Spool;
import com.facebook.acra.config.AcraReportingConfig;
import com.facebook.acra.constants.ReportField;
import com.facebook.acra.util.PackageManagerWrapper;
import com.facebook.acra.util.StatFsUtil;
import io.card.payment.BuildConfig;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CrashTimeDataCollector {
    private static final String ANDROID_RUNTIME_ART = "ART";
    private static final String ANDROID_RUNTIME_DALVIK = "DALVIK";
    private static final String ANDROID_RUNTIME_UNKNOWN = "UNKNOWN";
    private static final int DEFAULT_TRACE_COUNT_LIMIT = 5;
    private static final String JAVA_BOOT_CLASS_PATH = "java.boot.class.path";
    private static final String KNOWN_ART_JAR = "/system/framework/core-libart.jar";
    private static final String KNOWN_DALVIK_JAR = "/system/framework/core.jar";
    private static final String PROCESS_NAME_UNSET = "!";
    private static final String WEBVIEW_PACKAGE_NAME = "com.google.android.webview";
    private static volatile String processNameByAms = "!";
    private static final Map sDeviceSpecificFields = Collections.synchronizedMap(new TreeMap());
    private static volatile PackageManagerWrapper sPackageManagerWrapper;

    public class Api19Utils {
        public static boolean isLowRamDevice(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager == null || !activityManager.isLowRamDevice()) {
                return false;
            }
            return true;
        }

        private Api19Utils() {
        }
    }

    public class Api21Utils {
        private Api21Utils() {
        }

        public static String[] getCpuAbis() {
            return Build.SUPPORTED_ABIS;
        }
    }

    private static void attachComponentStats(Context context, AcraReportingConfig acraReportingConfig, CrashReportData crashReportData, Writer writer) {
        if (shouldAddField(ReportField.COMPONENTS_TOTAL, crashReportData, acraReportingConfig) || shouldAddField(ReportField.COMPONENTS_ENABLED, crashReportData, acraReportingConfig) || shouldAddField(ReportField.COMPONENTS_DISABLED, crashReportData, acraReportingConfig) || shouldAddField(ReportField.COMPONENTS_DEFAULT, crashReportData, acraReportingConfig) || shouldAddField(ReportField.COMPONENTS_DISABLED_NAMES, crashReportData, acraReportingConfig) || shouldAddField(ReportField.COMPONENTS_DEFAULT_NAMES, crashReportData, acraReportingConfig) || shouldAddField(ReportField.COMPONENTS_FLAG_STATE, crashReportData, acraReportingConfig)) {
            try {
                AppComponentStats.Stats stats = new AppComponentStats(context).getStats();
                if (shouldAddField(ReportField.COMPONENTS_TOTAL, crashReportData, acraReportingConfig)) {
                    ErrorReporter.put(ReportField.COMPONENTS_TOTAL, Integer.toString(stats.totalCount), crashReportData, writer);
                }
                if (shouldAddField(ReportField.COMPONENTS_ENABLED, crashReportData, acraReportingConfig)) {
                    ErrorReporter.put(ReportField.COMPONENTS_ENABLED, Integer.toString(stats.enabledCount), crashReportData, writer);
                }
                if (shouldAddField(ReportField.COMPONENTS_DISABLED, crashReportData, acraReportingConfig)) {
                    ErrorReporter.put(ReportField.COMPONENTS_DISABLED, Integer.toString(stats.disabledCount), crashReportData, writer);
                }
                if (shouldAddField(ReportField.COMPONENTS_DEFAULT, crashReportData, acraReportingConfig)) {
                    ErrorReporter.put(ReportField.COMPONENTS_DEFAULT, Integer.toString(stats.defaultCount), crashReportData, writer);
                }
                if (shouldAddField(ReportField.COMPONENTS_DEFAULT_NAMES, crashReportData, acraReportingConfig)) {
                    ErrorReporter.put(ReportField.COMPONENTS_DEFAULT_NAMES, stats.defaultComponents.toString(), crashReportData, writer);
                }
                if (shouldAddField(ReportField.COMPONENTS_DISABLED_NAMES, crashReportData, acraReportingConfig)) {
                    ErrorReporter.put(ReportField.COMPONENTS_DISABLED_NAMES, stats.disabledComponents.toString(), crashReportData, writer);
                }
                if (shouldAddField(ReportField.COMPONENTS_FLAG_STATE, crashReportData, acraReportingConfig)) {
                    ErrorReporter.put(ReportField.COMPONENTS_FLAG_STATE, Integer.toString(stats.flagState), crashReportData, writer);
                }
            } catch (Throwable th) {
                ErrorReporter.put(ReportField.COMPONENTS_TOTAL, th.toString(), crashReportData, writer);
                noteReportFieldFailure(crashReportData, ReportField.COMPONENTS_TOTAL, th);
            }
        }
    }

    private static String getAndroidRuntime() {
        if (Build.VERSION.SDK_INT < 19) {
            return ANDROID_RUNTIME_DALVIK;
        }
        String property = System.getProperty(JAVA_BOOT_CLASS_PATH);
        if (property == null) {
            return ANDROID_RUNTIME_UNKNOWN;
        }
        if (property.contains(KNOWN_ART_JAR)) {
            return ANDROID_RUNTIME_ART;
        }
        if (property.contains(KNOWN_DALVIK_JAR)) {
            return ANDROID_RUNTIME_DALVIK;
        }
        return ANDROID_RUNTIME_UNKNOWN;
    }

    private static String getCpuAbis() {
        String[] strArr;
        if (Build.VERSION.SDK_INT >= 21) {
            strArr = Api21Utils.getCpuAbis();
        } else {
            strArr = new String[]{Build.CPU_ABI, Build.CPU_ABI2};
        }
        String arrays = Arrays.toString(strArr);
        int length = arrays.length();
        if (length < 2 || arrays.charAt(0) != '[') {
            return arrays;
        }
        int i = length - 1;
        if (arrays.charAt(i) == ']') {
            return arrays.substring(1, i);
        }
        return arrays;
    }

    private static PackageManagerWrapper getPackageManagerWrapper(Context context) {
        if (sPackageManagerWrapper == null) {
            sPackageManagerWrapper = new PackageManagerWrapper(context, ACRA.LOG_TAG);
        }
        return sPackageManagerWrapper;
    }

    private static String getProcessNameFromAmsOrNull(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        String str = processNameByAms;
        if (!PROCESS_NAME_UNSET.equals(str)) {
            return str;
        }
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        String str2 = null;
        if (activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null) {
            return null;
        }
        Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ActivityManager.RunningAppProcessInfo next = it.next();
            if (next.pid == myPid) {
                str2 = next.processName;
                break;
            }
        }
        processNameByAms = str2;
        return str2;
    }

    public static void noteReportFieldFailure(CrashReportData crashReportData, String str, Throwable th) {
        try {
            if (crashReportData.fieldFailures == null) {
                crashReportData.fieldFailures = new ArrayList();
            }
            crashReportData.fieldFailures.add(String.format("%s: [%s]", str, th));
        } catch (Throwable unused) {
        }
    }

    private static void reportInternalStorageUsage(AcraReportingConfig acraReportingConfig, CrashReportData crashReportData, Writer writer) {
        if (shouldAddField(ReportField.DISK_SIZE_TOTAL, crashReportData, acraReportingConfig)) {
            try {
                ErrorReporter.put(ReportField.DISK_SIZE_TOTAL, Long.toString(StatFsUtil.getTotalInternalStorageSpace(StatFsUtil.IN_KILO_BYTE)), crashReportData, writer);
            } catch (Exception e) {
                noteReportFieldFailure(crashReportData, ReportField.DISK_SIZE_TOTAL, e);
            }
        }
        if (shouldAddField(ReportField.DISK_SIZE_AVAILABLE, crashReportData, acraReportingConfig)) {
            try {
                ErrorReporter.put(ReportField.DISK_SIZE_AVAILABLE, Long.toString(StatFsUtil.getAvailableInternalStorageSpace(StatFsUtil.IN_KILO_BYTE)), crashReportData, writer);
            } catch (Exception e2) {
                noteReportFieldFailure(crashReportData, ReportField.DISK_SIZE_AVAILABLE, e2);
            }
        }
        if (shouldAddField(ReportField.DISK_SIZE_USED, crashReportData, acraReportingConfig)) {
            try {
                ErrorReporter.put(ReportField.DISK_SIZE_USED, Long.toString(StatFsUtil.getUsedInternalStorageSpace(StatFsUtil.IN_KILO_BYTE)), crashReportData, writer);
            } catch (Exception e3) {
                noteReportFieldFailure(crashReportData, ReportField.DISK_SIZE_USED, e3);
            }
        }
    }

    private static void resetProcessNameByAmsCache() {
        processNameByAms = PROCESS_NAME_UNSET;
    }

    private static String toString(Display display) {
        if (display == null) {
            return BuildConfig.FLAVOR;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        return "width=" + display.getWidth() + 10 + "height=" + display.getHeight() + 10 + "pixelFormat=" + display.getPixelFormat() + 10 + "refreshRate=" + display.getRefreshRate() + "fps" + 10 + "metrics.density=x" + displayMetrics.density + 10 + "metrics.scaledDensity=x" + displayMetrics.scaledDensity + 10 + "metrics.widthPixels=" + displayMetrics.widthPixels + 10 + "metrics.heightPixels=" + displayMetrics.heightPixels + 10 + "metrics.xdpi=" + displayMetrics.xdpi + 10 + "metrics.ydpi=" + displayMetrics.ydpi;
    }

    private static long getDeviceUptime() {
        return SystemClock.elapsedRealtime();
    }

    public static String getPackageManagerVersionCode(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return "no package manager";
        }
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getApplicationInfo().packageName, 0);
            if (packageInfo == null) {
                return "no package info";
            }
            return Long.toString((long) packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            return e.toString();
        }
    }

    private static String getProcessName(Context context) {
        String processNameFromAmsOrNull = getProcessNameFromAmsOrNull(context);
        if (processNameFromAmsOrNull == null && (processNameFromAmsOrNull = AnonymousClass00V.A00("/proc/self/cmdline")) != null) {
            processNameFromAmsOrNull = processNameFromAmsOrNull.trim();
        }
        if (processNameFromAmsOrNull == null) {
            return BuildConfig.FLAVOR;
        }
        return processNameFromAmsOrNull;
    }

    public static String getProcessNameFromAms(Context context) {
        String processNameFromAmsOrNull = getProcessNameFromAmsOrNull(context);
        if (processNameFromAmsOrNull == null) {
            return "n/a";
        }
        return processNameFromAmsOrNull;
    }

    private static long getProcessUptime(ErrorReporter errorReporter) {
        return SystemClock.uptimeMillis() - errorReporter.getAppStartTickTimeMs();
    }

    public static String getPublicSourceDir(Context context) {
        try {
            return context.createPackageContext(context.getPackageName(), 0).getApplicationInfo().publicSourceDir;
        } catch (PackageManager.NameNotFoundException unused) {
            return "package name not found";
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d3, code lost:
        if (r10.equals(com.facebook.acra.constants.ReportField.DEVICE_CPU_ABIS) == false) goto L_0x00d5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x018e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00da A[SYNTHETIC, Splitter:B:55:0x00da] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00e6 A[SYNTHETIC, Splitter:B:58:0x00e6] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00ff A[SYNTHETIC, Splitter:B:65:0x00ff] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0113 A[Catch:{ all -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0118 A[Catch:{ all -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x011d A[Catch:{ all -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0128 A[Catch:{ all -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x012f A[Catch:{ all -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0134 A[Catch:{ all -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0139 A[Catch:{ all -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x015d A[Catch:{ all -> 0x0187 }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0162 A[Catch:{ all -> 0x0187 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void populateConstantDeviceData(com.facebook.acra.ErrorReporter r28, com.facebook.acra.config.AcraReportingConfig r29, com.facebook.acra.CrashReportData r30, java.io.Writer r31) {
        /*
            android.content.Context r8 = r29.getApplicationContext()
            java.lang.String r7 = "BUILD"
            r15 = 0
            java.lang.String r6 = "JAIL_BROKEN"
            java.lang.String r5 = "INSTALLATION_ID"
            java.lang.String r4 = "TOTAL_MEM_SIZE"
            java.lang.String r3 = "ENVIRONMENT"
            java.lang.String r2 = "ANDROID_RUNTIME"
            java.lang.String r1 = "device_cpu_abis"
            java.lang.String r23 = "is_64_bit_build"
            java.lang.String r24 = "DISPLAY"
            java.lang.String r25 = "DEVICE_FEATURES"
            java.lang.String r26 = "IS_LOW_RAM_DEVICE"
            java.lang.String r27 = "WEBVIEW_VERSION"
            java.lang.String r28 = "PLAY_SERVICES_VERSION"
            r21 = r2
            r22 = r1
            r19 = r4
            r20 = r3
            r17 = r6
            r18 = r5
            r16 = r7
            java.lang.String[] r16 = new java.lang.String[]{r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28}
            r12 = 13
            r11 = 0
        L_0x0034:
            if (r11 >= r12) goto L_0x0193
            r10 = r16[r11]
            r13 = r30
            r0 = r29
            boolean r0 = shouldAddField(r10, r13, r0)
            if (r0 == 0) goto L_0x018e
            java.util.Map r0 = com.facebook.acra.CrashTimeDataCollector.sDeviceSpecificFields     // Catch:{ all -> 0x0184 }
            boolean r0 = r0.containsKey(r10)     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x0055
            java.util.Map r0 = com.facebook.acra.CrashTimeDataCollector.sDeviceSpecificFields     // Catch:{ all -> 0x0182 }
            java.lang.Object r14 = r0.get(r10)     // Catch:{ all -> 0x0182 }
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ all -> 0x0182 }
            r9 = 1
            goto L_0x017a
        L_0x0055:
            int r0 = r10.hashCode()     // Catch:{ all -> 0x0184 }
            switch(r0) {
                case -2055404088: goto L_0x0065;
                case -1905220446: goto L_0x006d;
                case -1605187834: goto L_0x0078;
                case -522074816: goto L_0x0080;
                case -450347134: goto L_0x0088;
                case -94228910: goto L_0x0093;
                case 52048902: goto L_0x009e;
                case 63557198: goto L_0x00a9;
                case 1052445527: goto L_0x00b1;
                case 1171703578: goto L_0x00bc;
                case 1460053360: goto L_0x00c4;
                case 1730504971: goto L_0x00ce;
                case 2067056115: goto L_0x005d;
                default: goto L_0x005c;
            }     // Catch:{ all -> 0x0184 }
        L_0x005c:
            goto L_0x00d5
        L_0x005d:
            boolean r0 = r10.equals(r3)     // Catch:{ all -> 0x0182 }
            r9 = 4
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x0065:
            boolean r0 = r10.equals(r2)     // Catch:{ all -> 0x0182 }
            r9 = 5
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x006d:
            java.lang.String r0 = "DISPLAY"
            boolean r0 = r10.equals(r0)     // Catch:{ all -> 0x0182 }
            r9 = 8
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x0078:
            boolean r0 = r10.equals(r4)     // Catch:{ all -> 0x0182 }
            r9 = 3
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x0080:
            boolean r0 = r10.equals(r5)     // Catch:{ all -> 0x0182 }
            r9 = 2
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x0088:
            java.lang.String r0 = "PLAY_SERVICES_VERSION"
            boolean r0 = r10.equals(r0)     // Catch:{ all -> 0x0182 }
            r9 = 12
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x0093:
            java.lang.String r0 = "WEBVIEW_VERSION"
            boolean r0 = r10.equals(r0)     // Catch:{ all -> 0x0182 }
            r9 = 11
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x009e:
            java.lang.String r0 = "DEVICE_FEATURES"
            boolean r0 = r10.equals(r0)     // Catch:{ all -> 0x0182 }
            r9 = 9
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x00a9:
            boolean r0 = r10.equals(r7)     // Catch:{ all -> 0x0182 }
            r9 = 0
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x00b1:
            java.lang.String r0 = "IS_LOW_RAM_DEVICE"
            boolean r0 = r10.equals(r0)     // Catch:{ all -> 0x0182 }
            r9 = 10
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x00bc:
            boolean r0 = r10.equals(r6)     // Catch:{ all -> 0x0182 }
            r9 = 1
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x00c4:
            java.lang.String r0 = "is_64_bit_build"
            boolean r0 = r10.equals(r0)     // Catch:{ all -> 0x0182 }
            r9 = 7
            if (r0 != 0) goto L_0x00d6
            goto L_0x00d5
        L_0x00ce:
            boolean r0 = r10.equals(r1)     // Catch:{ all -> 0x0182 }
            r9 = 6
            if (r0 != 0) goto L_0x00d6
        L_0x00d5:
            r9 = -1
        L_0x00d6:
            r14 = 0
            switch(r9) {
                case 0: goto L_0x010c;
                case 1: goto L_0x0113;
                case 2: goto L_0x0118;
                case 3: goto L_0x011d;
                case 4: goto L_0x0128;
                case 5: goto L_0x012f;
                case 6: goto L_0x0134;
                case 7: goto L_0x0139;
                case 8: goto L_0x014c;
                case 9: goto L_0x015d;
                case 10: goto L_0x0162;
                case 11: goto L_0x00e6;
                case 12: goto L_0x00ff;
                default: goto L_0x00da;
            }     // Catch:{ all -> 0x0182 }
        L_0x00da:
            java.lang.RuntimeException r9 = new java.lang.RuntimeException     // Catch:{ all -> 0x0184 }
            java.lang.String r0 = "Missing case for "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r10)     // Catch:{ all -> 0x0184 }
            r9.<init>(r0)     // Catch:{ all -> 0x0184 }
            throw r9     // Catch:{ all -> 0x0184 }
        L_0x00e6:
            int r9 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0187 }
            r0 = 21
            if (r9 < r0) goto L_0x0173
            com.facebook.acra.util.PackageManagerWrapper r15 = getPackageManagerWrapper(r8)     // Catch:{ all -> 0x0187 }
            java.lang.String r9 = "com.google.android.webview"
            r0 = 0
            android.content.pm.PackageInfo r0 = r15.getPackageInfo(r9, r0)     // Catch:{ all -> 0x0187 }
            if (r0 == 0) goto L_0x00fd
            java.lang.String r14 = r0.versionName     // Catch:{ all -> 0x0187 }
            r9 = 1
            goto L_0x0107
        L_0x00fd:
            r9 = 0
            goto L_0x0107
        L_0x00ff:
            java.lang.String r14 = com.facebook.acra.ReflectionCollector.getPlayServicesVersion(r8)     // Catch:{ all -> 0x0109 }
            r9 = 0
            if (r14 == 0) goto L_0x0107
            r9 = 1
        L_0x0107:
            r15 = 1
            goto L_0x0175
        L_0x0109:
            r0 = move-exception
            r15 = 1
            goto L_0x0189
        L_0x010c:
            java.lang.Class<android.os.Build> r0 = android.os.Build.class
            java.lang.String r14 = com.facebook.acra.ReflectionCollector.collectConstants(r0)     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x0113:
            java.lang.String r14 = com.facebook.acra.util.CrashTimeDataCollectorHelper.getJailStatus()     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x0118:
            java.lang.String r14 = com.facebook.acra.util.Installation.id(r8)     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x011d:
            r14 = 1
            long r14 = com.facebook.acra.util.StatFsUtil.getTotalInternalStorageSpace(r14)     // Catch:{ all -> 0x0184 }
            java.lang.String r14 = java.lang.Long.toString(r14)     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x0128:
            java.lang.Class<android.os.Environment> r0 = android.os.Environment.class
            java.lang.String r14 = com.facebook.acra.ReflectionCollector.collectStaticGettersResults(r0)     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x012f:
            java.lang.String r14 = getAndroidRuntime()     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x0134:
            java.lang.String r14 = getCpuAbis()     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x0139:
            java.lang.String r9 = "arm64"
            if (r9 == 0) goto L_0x0148
            java.lang.String r0 = "64"
            boolean r0 = r9.contains(r0)     // Catch:{ all -> 0x0184 }
        L_0x0143:
            java.lang.String r14 = java.lang.Boolean.toString(r0)     // Catch:{ all -> 0x0184 }
            goto L_0x014a
        L_0x0148:
            r0 = 0
            goto L_0x0143
        L_0x014a:
            r15 = 0
            goto L_0x0171
        L_0x014c:
            java.lang.String r0 = "window"
            java.lang.Object r0 = r8.getSystemService(r0)     // Catch:{ all -> 0x0187 }
            android.view.WindowManager r0 = (android.view.WindowManager) r0     // Catch:{ all -> 0x0187 }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ all -> 0x0187 }
            java.lang.String r14 = toString(r0)     // Catch:{ all -> 0x0187 }
            goto L_0x0170
        L_0x015d:
            java.lang.String r14 = com.facebook.acra.DeviceFeaturesCollector.getFeatures(r8)     // Catch:{ all -> 0x0187 }
            goto L_0x0170
        L_0x0162:
            int r9 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0187 }
            r0 = 19
            if (r9 < r0) goto L_0x0173
            boolean r0 = com.facebook.acra.CrashTimeDataCollector.Api19Utils.isLowRamDevice(r8)     // Catch:{ all -> 0x0187 }
            java.lang.String r14 = java.lang.Boolean.toString(r0)     // Catch:{ all -> 0x0187 }
        L_0x0170:
            r15 = 1
        L_0x0171:
            r9 = 1
            goto L_0x0175
        L_0x0173:
            r15 = 1
            r9 = 0
        L_0x0175:
            java.util.Map r0 = com.facebook.acra.CrashTimeDataCollector.sDeviceSpecificFields     // Catch:{ all -> 0x0182 }
            r0.put(r10, r14)     // Catch:{ all -> 0x0182 }
        L_0x017a:
            if (r9 == 0) goto L_0x018e
            r0 = r31
            com.facebook.acra.ErrorReporter.put(r10, r14, r13, r0)     // Catch:{ all -> 0x0182 }
            goto L_0x018e
        L_0x0182:
            r0 = move-exception
            goto L_0x0189
        L_0x0184:
            r0 = move-exception
            r15 = 0
            goto L_0x0189
        L_0x0187:
            r0 = move-exception
            r15 = 1
        L_0x0189:
            if (r15 != 0) goto L_0x018e
            noteReportFieldFailure(r13, r10, r0)
        L_0x018e:
            int r11 = r11 + 1
            r15 = 0
            goto L_0x0034
        L_0x0193:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.CrashTimeDataCollector.populateConstantDeviceData(com.facebook.acra.ErrorReporter, com.facebook.acra.config.AcraReportingConfig, com.facebook.acra.CrashReportData, java.io.Writer):void");
    }

    private static void populateCustomData(ErrorReporter errorReporter, AcraReportingConfig acraReportingConfig, Throwable th, CrashReportData crashReportData, Writer writer) {
        for (Map.Entry entry : errorReporter.getCustomFieldsSnapshot().entrySet()) {
            if (shouldAddField((String) entry.getKey(), crashReportData, acraReportingConfig)) {
                try {
                    ErrorReporter.put((String) entry.getKey(), (String) entry.getValue(), crashReportData, writer);
                } catch (Throwable th2) {
                    noteReportFieldFailure(crashReportData, (String) entry.getKey(), th2);
                }
            }
        }
        for (Map.Entry entry2 : errorReporter.getLazyCustomFieldsSnapshot().entrySet()) {
            if (shouldAddLazyField((String) entry2.getKey(), crashReportData, acraReportingConfig)) {
                try {
                    ErrorReporter.put((String) entry2.getKey(), ((CustomReportDataSupplier) entry2.getValue()).getCustomData(th), crashReportData, writer);
                } catch (Throwable th3) {
                    noteReportFieldFailure(crashReportData, (String) entry2.getKey(), th3);
                }
            }
        }
    }

    public static boolean shouldAddField(String str, CrashReportData crashReportData, AcraReportingConfig acraReportingConfig) {
        if (crashReportData.containsKey(str) || !acraReportingConfig.shouldReportField(str)) {
            return false;
        }
        return true;
    }

    public static boolean shouldAddLazyField(String str, CrashReportData crashReportData, AcraReportingConfig acraReportingConfig) {
        if (acraReportingConfig.shouldLazyFieldsOverwriteExistingValues()) {
            return acraReportingConfig.shouldReportField(str);
        }
        return shouldAddField(str, crashReportData, acraReportingConfig);
    }

    public static void gatherCrashData(ErrorReporter errorReporter, AcraReportingConfig acraReportingConfig, String str, Throwable th, CrashReportData crashReportData, Writer writer, Spool.FileBeingConsumed fileBeingConsumed, boolean z) {
        AcraReportingConfig acraReportingConfig2 = acraReportingConfig;
        CrashReportData crashReportData2 = crashReportData;
        ErrorReporter errorReporter2 = errorReporter;
        Writer writer2 = writer;
        if (shouldAddField(ReportField.UID, crashReportData, acraReportingConfig)) {
            try {
                ErrorReporter.put(ReportField.UID, errorReporter2.getUserId(), crashReportData, writer);
            } catch (Throwable th2) {
                noteReportFieldFailure(crashReportData, ReportField.UID, th2);
            }
        }
        if (shouldAddField(ReportField.CLIENT_UID, crashReportData, acraReportingConfig) && errorReporter2.getClientUserId() != null && errorReporter2.getClientUserId().length() > 0) {
            try {
                ErrorReporter.put(ReportField.CLIENT_UID, errorReporter2.getClientUserId(), crashReportData, writer);
            } catch (Throwable th3) {
                noteReportFieldFailure(crashReportData, ReportField.CLIENT_UID, th3);
            }
        }
        if (shouldAddField(ReportField.STACK_TRACE, crashReportData, acraReportingConfig)) {
            try {
                ErrorReporter.put(ReportField.STACK_TRACE, str, crashReportData, writer);
            } catch (Throwable th4) {
                noteReportFieldFailure(crashReportData, ReportField.STACK_TRACE, th4);
            }
        }
        if (errorReporter2.getConstantFields() != null) {
            for (Map.Entry entry : errorReporter2.getConstantFields().entrySet()) {
                if (shouldAddField((String) entry.getKey(), crashReportData, acraReportingConfig)) {
                    try {
                        ErrorReporter.put((String) entry.getKey(), (String) entry.getValue(), crashReportData, writer);
                    } catch (Throwable th5) {
                        noteReportFieldFailure(crashReportData, (String) entry.getKey(), th5);
                    }
                }
            }
        }
        populateCrashTimeData(fileBeingConsumed, errorReporter2, acraReportingConfig2, th, crashReportData2, writer2, z);
        populateConstantDeviceData(errorReporter2, acraReportingConfig2, crashReportData, writer);
        populateCustomData(errorReporter2, acraReportingConfig2, th, crashReportData, writer);
        if (crashReportData.fieldFailures != null) {
            if (shouldAddField(ReportField.FIELD_FAILURES, crashReportData, acraReportingConfig2)) {
                try {
                    ErrorReporter.put(ReportField.FIELD_FAILURES, TextUtils.join("\n", crashReportData.fieldFailures), crashReportData, writer);
                } catch (Throwable unused) {
                }
            }
            crashReportData.fieldFailures = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:115:0x018f A[Catch:{ all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x019d A[Catch:{ all -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x023a A[SYNTHETIC, Splitter:B:156:0x023a] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x029a  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x02da A[SYNTHETIC, Splitter:B:199:0x02da] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x02f7  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x02fa  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x033e  */
    /* JADX WARNING: Removed duplicated region for block: B:226:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046 A[Catch:{ all -> 0x004e }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x008a A[SYNTHETIC, Splitter:B:43:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a2 A[SYNTHETIC, Splitter:B:49:0x00a2] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00b6 A[SYNTHETIC, Splitter:B:55:0x00b6] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ca A[SYNTHETIC, Splitter:B:61:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00f3 A[SYNTHETIC, Splitter:B:70:0x00f3] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0143 A[SYNTHETIC, Splitter:B:93:0x0143] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x015a A[SYNTHETIC, Splitter:B:99:0x015a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void populateCrashTimeData(com.facebook.acra.Spool.FileBeingConsumed r13, com.facebook.acra.ErrorReporter r14, com.facebook.acra.config.AcraReportingConfig r15, java.lang.Throwable r16, com.facebook.acra.CrashReportData r17, java.io.Writer r18, boolean r19) {
        /*
            android.content.Context r9 = r14.getContext()
            r4 = 0
            r8 = 0
            if (r13 == 0) goto L_0x0009
            r8 = 1
        L_0x0009:
            r12 = 0
            if (r8 == 0) goto L_0x001c
            com.facebook.acra.util.minidump.MinidumpReader r6 = new com.facebook.acra.util.minidump.MinidumpReader     // Catch:{ Exception -> 0x0014 }
            java.io.RandomAccessFile r0 = r13.file     // Catch:{ Exception -> 0x0014 }
            r6.<init>(r0)     // Catch:{ Exception -> 0x0014 }
            goto L_0x001d
        L_0x0014:
            r2 = move-exception
            java.lang.String r1 = "ACRA"
            java.lang.String r0 = "There was a problem reading the minidump"
            X.C010708t.A0R(r1, r2, r0)
        L_0x001c:
            r6 = r12
        L_0x001d:
            r3 = r17
            r2 = r18
            r10 = r15
            if (r19 == 0) goto L_0x0052
            java.lang.String r7 = "PROCESS_NAME"
            boolean r0 = shouldAddField(r7, r3, r15)
            if (r0 == 0) goto L_0x0052
            if (r8 != 0) goto L_0x002f
            goto L_0x0046
        L_0x002f:
            r0 = 1197932550(0x47670006, float:59136.023)
            java.lang.String r5 = r6.getString(r0)     // Catch:{ all -> 0x004e }
            if (r5 != 0) goto L_0x003a
            java.lang.String r5 = ""
        L_0x003a:
            int r1 = r5.indexOf(r4)     // Catch:{ all -> 0x004e }
            r0 = -1
            if (r1 == r0) goto L_0x004a
            java.lang.String r5 = r5.substring(r4, r1)     // Catch:{ all -> 0x004e }
            goto L_0x004a
        L_0x0046:
            java.lang.String r5 = getProcessName(r9)     // Catch:{ all -> 0x004e }
        L_0x004a:
            com.facebook.acra.ErrorReporter.put(r7, r5, r3, r2)     // Catch:{ all -> 0x004e }
            goto L_0x0052
        L_0x004e:
            r0 = move-exception
            noteReportFieldFailure(r3, r7, r0)
        L_0x0052:
            java.lang.String r1 = "USER_APP_START_DATE"
            boolean r0 = shouldAddField(r1, r3, r15)
            if (r0 == 0) goto L_0x0068
            if (r8 != 0) goto L_0x0068
            java.lang.String r0 = r14.getAppStartDateFormat3339()     // Catch:{ all -> 0x0064 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x0064 }
            goto L_0x0068
        L_0x0064:
            r0 = move-exception
            noteReportFieldFailure(r3, r1, r0)
        L_0x0068:
            java.lang.String r5 = "PROCESS_UPTIME"
            boolean r0 = shouldAddField(r5, r3, r15)
            if (r0 == 0) goto L_0x0082
            if (r8 != 0) goto L_0x0082
            long r0 = getProcessUptime(r14)     // Catch:{ all -> 0x007e }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x007e }
            com.facebook.acra.ErrorReporter.put(r5, r0, r3, r2)     // Catch:{ all -> 0x007e }
            goto L_0x0082
        L_0x007e:
            r0 = move-exception
            noteReportFieldFailure(r3, r5, r0)
        L_0x0082:
            java.lang.String r5 = "DEVICE_UPTIME"
            boolean r0 = shouldAddField(r5, r3, r15)
            if (r0 == 0) goto L_0x009a
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0096 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x0096 }
            com.facebook.acra.ErrorReporter.put(r5, r0, r3, r2)     // Catch:{ all -> 0x0096 }
            goto L_0x009a
        L_0x0096:
            r0 = move-exception
            noteReportFieldFailure(r3, r5, r0)
        L_0x009a:
            java.lang.String r1 = "APP_VERSION_CODE_IN_PACKAGE_MANAGER"
            boolean r0 = shouldAddField(r1, r3, r15)
            if (r0 == 0) goto L_0x00ae
            java.lang.String r0 = getPackageManagerVersionCode(r9)     // Catch:{ all -> 0x00aa }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x00aa }
            goto L_0x00ae
        L_0x00aa:
            r0 = move-exception
            noteReportFieldFailure(r3, r1, r0)
        L_0x00ae:
            java.lang.String r1 = "PUBLIC_SOURCE_DIR_IN_PACKAGE_MANAGER"
            boolean r0 = shouldAddField(r1, r3, r15)
            if (r0 == 0) goto L_0x00c2
            java.lang.String r0 = getPublicSourceDir(r9)     // Catch:{ all -> 0x00be }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x00be }
            goto L_0x00c2
        L_0x00be:
            r0 = move-exception
            noteReportFieldFailure(r3, r1, r0)
        L_0x00c2:
            java.lang.String r7 = "PUBLIC_SOURCE_DIR_LAST_MODIFIED"
            boolean r0 = shouldAddField(r7, r3, r15)
            if (r0 == 0) goto L_0x00eb
            java.io.File r5 = new java.io.File     // Catch:{ all -> 0x00e7 }
            java.lang.String r0 = getPublicSourceDir(r9)     // Catch:{ all -> 0x00e7 }
            r5.<init>(r0)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = "File doesn't exist"
            boolean r0 = r5.exists()     // Catch:{ all -> 0x00e7 }
            if (r0 == 0) goto L_0x00e3
            long r0 = r5.lastModified()     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x00e7 }
        L_0x00e3:
            com.facebook.acra.ErrorReporter.put(r7, r1, r3, r2)     // Catch:{ all -> 0x00e7 }
            goto L_0x00eb
        L_0x00e7:
            r0 = move-exception
            noteReportFieldFailure(r3, r7, r0)
        L_0x00eb:
            java.lang.String r1 = "CRASH_CONFIGURATION"
            boolean r0 = shouldAddField(r1, r3, r15)
            if (r0 == 0) goto L_0x0109
            android.content.res.Resources r0 = r9.getResources()     // Catch:{ all -> 0x0105 }
            if (r0 == 0) goto L_0x0109
            android.content.res.Configuration r0 = r0.getConfiguration()     // Catch:{ all -> 0x0105 }
            java.lang.String r0 = com.facebook.acra.ConfigurationInspector.toString(r0)     // Catch:{ all -> 0x0105 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x0105 }
            goto L_0x0109
        L_0x0105:
            r0 = move-exception
            noteReportFieldFailure(r3, r1, r0)
        L_0x0109:
            java.lang.String r5 = "AVAILABLE_MEM_SIZE"
            boolean r0 = shouldAddField(r5, r3, r15)
            if (r0 == 0) goto L_0x0125
            if (r8 != 0) goto L_0x0125
            r0 = 1
            long r0 = com.facebook.acra.util.StatFsUtil.getAvailableInternalStorageSpace(r0)     // Catch:{ all -> 0x0121 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x0121 }
            com.facebook.acra.ErrorReporter.put(r5, r0, r3, r2)     // Catch:{ all -> 0x0121 }
            goto L_0x0125
        L_0x0121:
            r0 = move-exception
            noteReportFieldFailure(r3, r5, r0)
        L_0x0125:
            java.lang.String r1 = "DUMPSYS_MEMINFO"
            boolean r0 = shouldAddField(r1, r3, r15)
            if (r0 == 0) goto L_0x013b
            if (r8 != 0) goto L_0x013b
            java.lang.String r0 = com.facebook.acra.DumpSysCollector.collectMemInfo(r9)     // Catch:{ all -> 0x0137 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x0137 }
            goto L_0x013b
        L_0x0137:
            r0 = move-exception
            noteReportFieldFailure(r3, r1, r0)
        L_0x013b:
            java.lang.String r1 = "DUMPSYS_USERINFO"
            boolean r0 = shouldAddField(r1, r3, r15)
            if (r0 == 0) goto L_0x014f
            java.lang.String r0 = com.facebook.acra.DumpSysCollector.collectUserInfo(r9)     // Catch:{ all -> 0x014b }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x014b }
            goto L_0x014f
        L_0x014b:
            r0 = move-exception
            noteReportFieldFailure(r3, r1, r0)
        L_0x014f:
            reportInternalStorageUsage(r15, r3, r2)
            java.lang.String r0 = "USER_CRASH_DATE"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x017f
            android.text.format.Time r5 = new android.text.format.Time     // Catch:{ all -> 0x0179 }
            r5.<init>()     // Catch:{ all -> 0x0179 }
            if (r8 != 0) goto L_0x0162
            goto L_0x016c
        L_0x0162:
            java.io.File r0 = r13.fileName     // Catch:{ all -> 0x0179 }
            long r0 = r0.lastModified()     // Catch:{ all -> 0x0179 }
            r5.set(r0)     // Catch:{ all -> 0x0179 }
            goto L_0x016f
        L_0x016c:
            r5.setToNow()     // Catch:{ all -> 0x0179 }
        L_0x016f:
            java.lang.String r1 = "USER_CRASH_DATE"
            java.lang.String r0 = r5.format3339(r4)     // Catch:{ all -> 0x0179 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x0179 }
            goto L_0x017f
        L_0x0179:
            r1 = move-exception
            java.lang.String r0 = "USER_CRASH_DATE"
            noteReportFieldFailure(r3, r0, r1)
        L_0x017f:
            java.lang.String r0 = "ACTIVITY_LOG"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x01ad
            if (r8 != 0) goto L_0x01ad
            r0 = r16
            boolean r0 = r0 instanceof java.lang.OutOfMemoryError     // Catch:{ all -> 0x01a7 }
            if (r0 == 0) goto L_0x019d
            com.facebook.acra.util.SimpleTraceLogger r0 = r14.getActivityLogger()     // Catch:{ all -> 0x01a7 }
            java.lang.String r1 = r0.toString()     // Catch:{ all -> 0x01a7 }
        L_0x0197:
            java.lang.String r0 = "ACTIVITY_LOG"
            com.facebook.acra.ErrorReporter.put(r0, r1, r3, r2)     // Catch:{ all -> 0x01a7 }
            goto L_0x01ad
        L_0x019d:
            com.facebook.acra.util.SimpleTraceLogger r1 = r14.getActivityLogger()     // Catch:{ all -> 0x01a7 }
            r0 = 5
            java.lang.String r1 = r1.toString(r0)     // Catch:{ all -> 0x01a7 }
            goto L_0x0197
        L_0x01a7:
            r1 = move-exception
            java.lang.String r0 = "ACTIVITY_LOG"
            noteReportFieldFailure(r3, r0, r1)
        L_0x01ad:
            java.lang.String r0 = "PROCESS_NAME_BY_AMS"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x01ca
            if (r8 != 0) goto L_0x01ca
            java.lang.String r1 = "PROCESS_NAME_BY_AMS"
            java.lang.String r0 = getProcessNameFromAms(r9)     // Catch:{ all -> 0x01c4 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x01c4 }
            resetProcessNameByAmsCache()     // Catch:{ all -> 0x01c4 }
            goto L_0x01ca
        L_0x01c4:
            r1 = move-exception
            java.lang.String r0 = "PROCESS_NAME_BY_AMS"
            noteReportFieldFailure(r3, r0, r1)
        L_0x01ca:
            java.lang.String r0 = "OPEN_FD_COUNT"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x01ea
            java.lang.String r1 = "OPEN_FD_COUNT"
            com.facebook.acra.util.ProcFileReader r0 = com.facebook.acra.util.ProcFileReader.getProcFileReader()     // Catch:{ all -> 0x01e4 }
            int r0 = r0.getOpenFDCount()     // Catch:{ all -> 0x01e4 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x01e4 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x01e4 }
            goto L_0x01ea
        L_0x01e4:
            r1 = move-exception
            java.lang.String r0 = "OPEN_FD_COUNT"
            noteReportFieldFailure(r3, r0, r1)
        L_0x01ea:
            com.facebook.acra.util.ProcFileReader r0 = com.facebook.acra.util.ProcFileReader.getProcFileReader()     // Catch:{ all -> 0x01f3 }
            com.facebook.acra.util.ProcFileReader$OpenFDLimits r5 = r0.getOpenFDLimits()     // Catch:{ all -> 0x01f3 }
            goto L_0x01fc
        L_0x01f3:
            r5 = move-exception
            java.lang.String r1 = "ACRA"
            java.lang.String r0 = "unable to retrieve open FD info: not logging FD fields"
            X.C010708t.A0S(r1, r5, r0)
            r5 = r12
        L_0x01fc:
            if (r5 == 0) goto L_0x0232
            java.lang.String r0 = "OPEN_FD_SOFT_LIMIT"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x0218
            java.lang.String r1 = "OPEN_FD_SOFT_LIMIT"
            int r0 = r5.softLimit     // Catch:{ all -> 0x0212 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x0212 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x0212 }
            goto L_0x0218
        L_0x0212:
            r1 = move-exception
            java.lang.String r0 = "OPEN_FD_SOFT_LIMIT"
            noteReportFieldFailure(r3, r0, r1)
        L_0x0218:
            java.lang.String r0 = "OPEN_FD_HARD_LIMIT"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x0232
            java.lang.String r1 = "OPEN_FD_HARD_LIMIT"
            int r0 = r5.hardLimit     // Catch:{ all -> 0x022c }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x022c }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x022c }
            goto L_0x0232
        L_0x022c:
            r1 = move-exception
            java.lang.String r0 = "OPEN_FD_HARD_LIMIT"
            noteReportFieldFailure(r3, r0, r1)
        L_0x0232:
            java.lang.String r0 = "RUNTIME_PERMISSIONS"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x0250
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x024a }
            r0 = 23
            if (r1 < r0) goto L_0x0250
            java.lang.String r1 = com.facebook.acra.PermissionsReporter.getAppGrantedPermissions(r9)     // Catch:{ all -> 0x024a }
            java.lang.String r0 = "RUNTIME_PERMISSIONS"
            com.facebook.acra.ErrorReporter.put(r0, r1, r3, r2)     // Catch:{ all -> 0x024a }
            goto L_0x0250
        L_0x024a:
            r1 = move-exception
            java.lang.String r0 = "RUNTIME_PERMISSIONS"
            noteReportFieldFailure(r3, r0, r1)
        L_0x0250:
            java.lang.String r5 = "LOGCAT"
            if (r8 != 0) goto L_0x029a
            boolean r0 = shouldAddField(r5, r3, r15)
            if (r0 == 0) goto L_0x0266
            java.lang.String r0 = com.facebook.acra.LogCatCollector.collectLogCat(r9, r15, r12, r4)     // Catch:{ all -> 0x0262 }
            com.facebook.acra.ErrorReporter.put(r5, r0, r3, r2)     // Catch:{ all -> 0x0262 }
            goto L_0x0266
        L_0x0262:
            r0 = move-exception
            noteReportFieldFailure(r3, r5, r0)
        L_0x0266:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 19
            if (r1 >= r0) goto L_0x02d2
            java.lang.String r0 = "EVENTSLOG"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x0286
            java.lang.String r1 = "EVENTSLOG"
            java.lang.String r0 = "events"
            java.lang.String r0 = com.facebook.acra.LogCatCollector.collectLogCat(r9, r15, r0, r4)     // Catch:{ all -> 0x0280 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x0280 }
            goto L_0x0286
        L_0x0280:
            r1 = move-exception
            java.lang.String r0 = "EVENTSLOG"
            noteReportFieldFailure(r3, r0, r1)
        L_0x0286:
            java.lang.String r0 = "RADIOLOG"
            boolean r0 = shouldAddField(r0, r3, r15)
            if (r0 == 0) goto L_0x02d2
            java.lang.String r1 = "RADIOLOG"
            java.lang.String r0 = "radio"
            java.lang.String r0 = com.facebook.acra.LogCatCollector.collectLogCat(r9, r15, r0, r4)     // Catch:{ all -> 0x02cc }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x02cc }
            goto L_0x02d2
        L_0x029a:
            java.lang.String r0 = "LOGCAT_NATIVE"
            boolean r0 = r15.shouldReportField(r0)
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r3.containsKey(r5)
            if (r0 != 0) goto L_0x02d2
            if (r8 == 0) goto L_0x02bb
            java.lang.String r0 = "logcatFileName"
            java.lang.String r12 = r6.getCustomData(r0)     // Catch:{ all -> 0x02c7 }
            java.lang.String r4 = "ACRA"
            java.lang.String r1 = "logcat Logcat file name from minidump : %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r12}     // Catch:{ all -> 0x02c7 }
            X.C010708t.A0P(r4, r1, r0)     // Catch:{ all -> 0x02c7 }
        L_0x02bb:
            r11 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            java.lang.String r0 = com.facebook.acra.LogCatCollector.collectLogCat(r9, r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x02c7 }
            com.facebook.acra.ErrorReporter.put(r5, r0, r3, r2)     // Catch:{ all -> 0x02c7 }
            goto L_0x02d2
        L_0x02c7:
            r0 = move-exception
            noteReportFieldFailure(r3, r5, r0)
            goto L_0x02d2
        L_0x02cc:
            r1 = move-exception
            java.lang.String r0 = "RADIOLOG"
            noteReportFieldFailure(r3, r0, r1)
        L_0x02d2:
            java.lang.String r0 = "LARGE_MEM_HEAP"
            boolean r0 = shouldAddField(r0, r3, r10)
            if (r0 == 0) goto L_0x02f0
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x02ea }
            r0 = 11
            if (r1 < r0) goto L_0x02f0
            java.lang.String r1 = "LARGE_MEM_HEAP"
            java.lang.String r0 = com.facebook.acra.DumpSysCollector.collectLargerMemoryInfo(r9)     // Catch:{ all -> 0x02ea }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x02ea }
            goto L_0x02f0
        L_0x02ea:
            r1 = move-exception
            java.lang.String r0 = "LARGE_MEM_HEAP"
            noteReportFieldFailure(r3, r0, r1)
        L_0x02f0:
            int r4 = android.os.Build.VERSION.SDK_INT
            r1 = 16
            r0 = 0
            if (r4 < r1) goto L_0x02f8
            r0 = 1
        L_0x02f8:
            if (r0 == 0) goto L_0x033c
            java.lang.String r0 = "OPEN_FILE_DESCRIPTORS"
            boolean r0 = shouldAddField(r0, r3, r10)
            if (r0 == 0) goto L_0x0316
            java.lang.String r1 = "OPEN_FILE_DESCRIPTORS"
            com.facebook.acra.util.ProcFileReader r0 = com.facebook.acra.util.ProcFileReader.getProcFileReader()     // Catch:{ all -> 0x0310 }
            java.lang.String r0 = r0.getOpenFileDescriptors()     // Catch:{ all -> 0x0310 }
            com.facebook.acra.ErrorReporter.put(r1, r0, r3, r2)     // Catch:{ all -> 0x0310 }
            goto L_0x0316
        L_0x0310:
            r1 = move-exception
            java.lang.String r0 = "OPEN_FILE_DESCRIPTORS"
            noteReportFieldFailure(r3, r0, r1)
        L_0x0316:
            java.lang.String r0 = "DATA_FILE_LS_LR"
            boolean r0 = shouldAddField(r0, r3, r10)
            if (r0 == 0) goto L_0x033c
            java.lang.String r5 = "DATA_FILE_LS_LR"
            java.lang.String r4 = "/system/bin/ls"
            java.lang.String r1 = "-lLR"
            android.content.pm.ApplicationInfo r0 = r9.getApplicationInfo()     // Catch:{ all -> 0x0336 }
            java.lang.String r0 = r0.dataDir     // Catch:{ all -> 0x0336 }
            java.lang.String[] r0 = new java.lang.String[]{r4, r1, r0}     // Catch:{ all -> 0x0336 }
            java.lang.String r0 = com.facebook.acra.util.CommandOutputCollector.collect(r0)     // Catch:{ all -> 0x0336 }
            com.facebook.acra.ErrorReporter.put(r5, r0, r3, r2)     // Catch:{ all -> 0x0336 }
            goto L_0x033c
        L_0x0336:
            r1 = move-exception
            java.lang.String r0 = "DATA_FILE_LS_LR"
            noteReportFieldFailure(r3, r0, r1)
        L_0x033c:
            if (r8 != 0) goto L_0x0341
            attachComponentStats(r9, r10, r3, r2)
        L_0x0341:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.CrashTimeDataCollector.populateCrashTimeData(com.facebook.acra.Spool$FileBeingConsumed, com.facebook.acra.ErrorReporter, com.facebook.acra.config.AcraReportingConfig, java.lang.Throwable, com.facebook.acra.CrashReportData, java.io.Writer, boolean):void");
    }
}
