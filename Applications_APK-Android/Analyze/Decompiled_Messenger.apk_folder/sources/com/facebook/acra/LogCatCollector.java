package com.facebook.acra;

import X.AnonymousClass08X;
import X.C010708t;
import android.content.Context;
import android.os.Build;
import android.util.Base64;
import com.facebook.acra.config.AcraReportingConfig;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.zip.GZIPOutputStream;

public class LogCatCollector {
    private static final String COMPRESS_NEWLINE = "\\n";
    private static final String NEWLINE = "\n";
    private static final String TAG = "LogCatCollector";
    public static final String UTF_8_ENCODING = "UTF-8";
    private static final Method sGetLogcatInterceptorContentsMethod;
    private static final Method sIsLogcatInterceptorInstalledMethod;

    static {
        Method method;
        Method method2 = null;
        try {
            Class<?> cls = Class.forName("com.facebook.logcatinterceptor.LogcatInterceptor");
            method = cls.getMethod("getLogcatContents", new Class[0]);
            try {
                method2 = cls.getMethod("isInstalled", new Class[0]);
            } catch (ClassNotFoundException unused) {
            } catch (NoSuchMethodException e) {
                e = e;
                try {
                    C010708t.A0R(TAG, e, "Could not find method on LogcatInterceptor");
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (ClassNotFoundException unused2) {
            method = null;
        } catch (NoSuchMethodException e2) {
            e = e2;
            method = null;
            C010708t.A0R(TAG, e, "Could not find method on LogcatInterceptor");
        } catch (Throwable th2) {
            th = th2;
            method = null;
            sGetLogcatInterceptorContentsMethod = method;
            throw th;
        }
        sGetLogcatInterceptorContentsMethod = method;
        sIsLogcatInterceptorInstalledMethod = method2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String collectLogCatBySpawningOtherProcess(java.lang.String[] r6, java.lang.String r7, java.lang.String r8) {
        /*
            r5 = 0
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ IOException -> 0x0077 }
            r2.<init>()     // Catch:{ IOException -> 0x0077 }
            java.lang.String r0 = "logcat"
            r2.add(r0)     // Catch:{ IOException -> 0x0077 }
            if (r7 == 0) goto L_0x0015
            java.lang.String r0 = "-b"
            r2.add(r0)     // Catch:{ IOException -> 0x0077 }
            r2.add(r7)     // Catch:{ IOException -> 0x0077 }
        L_0x0015:
            java.util.List r0 = java.util.Arrays.asList(r6)     // Catch:{ IOException -> 0x0077 }
            r2.addAll(r0)     // Catch:{ IOException -> 0x0077 }
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0077 }
            int r0 = r2.size()     // Catch:{ IOException -> 0x0077 }
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x0077 }
            java.lang.Object[] r0 = r2.toArray(r0)     // Catch:{ IOException -> 0x0077 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ IOException -> 0x0077 }
            java.lang.Process r0 = r1.exec(r0)     // Catch:{ IOException -> 0x0077 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0077 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0077 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0077 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0077 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0077 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ IOException -> 0x0077 }
            r0 = 200(0xc8, float:2.8E-43)
            r4.<init>(r0)     // Catch:{ IOException -> 0x0077 }
            r2 = 0
            r1 = 0
        L_0x0047:
            java.lang.String r0 = r3.readLine()     // Catch:{ IOException -> 0x0077 }
            if (r0 == 0) goto L_0x005b
            r4.add(r0)     // Catch:{ IOException -> 0x0077 }
            int r0 = r0.length()     // Catch:{ IOException -> 0x0077 }
            int r1 = r1 + r0
            int r0 = r8.length()     // Catch:{ IOException -> 0x0077 }
            int r1 = r1 + r0
            goto L_0x0047
        L_0x005b:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0077 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0077 }
        L_0x0060:
            int r0 = r4.size()     // Catch:{ IOException -> 0x0075 }
            if (r2 >= r0) goto L_0x0080
            java.lang.Object r0 = r4.get(r2)     // Catch:{ IOException -> 0x0075 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0075 }
            r3.append(r0)     // Catch:{ IOException -> 0x0075 }
            r3.append(r8)     // Catch:{ IOException -> 0x0075 }
            int r2 = r2 + 1
            goto L_0x0060
        L_0x0075:
            r2 = move-exception
            goto L_0x0079
        L_0x0077:
            r2 = move-exception
            r3 = r5
        L_0x0079:
            java.lang.String r1 = "LogCatCollector"
            java.lang.String r0 = "LogCatCollector.collectLogcat could not retrieve data."
            X.C010708t.A0R(r1, r2, r0)
        L_0x0080:
            if (r3 == 0) goto L_0x0086
            java.lang.String r5 = r3.toString()
        L_0x0086:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.LogCatCollector.collectLogCatBySpawningOtherProcess(java.lang.String[], java.lang.String, java.lang.String):java.lang.String");
    }

    public static String compressBase64(String str) {
        if (!(str == null || str.length() == 0)) {
            try {
                byte[] bytes = str.getBytes(UTF_8_ENCODING);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                gZIPOutputStream.write(bytes);
                gZIPOutputStream.close();
                return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
            } catch (IOException e) {
                C010708t.A0R(TAG, e, "Failed to compress string");
            }
        }
        return null;
    }

    public static String getLogcatFileContent(String str) {
        File file = new File(str);
        StringBuilder sb = new StringBuilder();
        try {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append(10);
                }
                bufferedReader.close();
            } catch (IOException e) {
                C010708t.A0R(TAG, e, "Could not close LogcatInterceptor buffer reader");
            }
        } catch (FileNotFoundException e2) {
            C010708t.A0R(TAG, e2, "Could not find LogcatInterceptor file");
        }
        return sb.toString();
    }

    private static boolean isLogcatInterceptorInstalled() {
        Method method = sIsLogcatInterceptorInstalledMethod;
        if (method != null) {
            try {
                return ((Boolean) method.invoke(null, new Object[0])).booleanValue();
            } catch (IllegalAccessException | InvocationTargetException e) {
                C010708t.A0R(TAG, e, "Could not call isInstalled method on LogcatInterceptor");
            }
        }
        return false;
    }

    public static String collectLogCat(Context context, AcraReportingConfig acraReportingConfig, String str, String str2, boolean z, boolean z2, boolean z3) {
        String str3;
        String str4;
        if (z2 || !AnonymousClass08X.A07(context, "acraconfig_logcat_interceptor_after_crash_enabled") || ((str != null && !str.equals("main")) || str2 == null)) {
            str3 = null;
        } else {
            str3 = getLogcatFileContent(str2);
        }
        if (str3 == null && Build.VERSION.SDK_INT >= 16) {
            boolean z4 = true;
            if (AnonymousClass08X.A00(context, "acraconfig_avoid_spawn_process_to_collect_logcat", 0) != 1) {
                z4 = false;
            }
            if (!z4) {
                String[] logcatArguments = acraReportingConfig.logcatArguments(z3);
                if (z) {
                    str4 = COMPRESS_NEWLINE;
                } else {
                    str4 = NEWLINE;
                }
                str3 = collectLogCatBySpawningOtherProcess(logcatArguments, str, str4);
            }
        }
        if (str3 == null) {
            return null;
        }
        return z ? compressBase64(str3) : str3;
    }

    public static String collectLogCat(Context context, AcraReportingConfig acraReportingConfig, String str, boolean z) {
        return collectLogCat(context, acraReportingConfig, str, null, z, false, false);
    }

    public static String collectLogCat(Context context, AcraReportingConfig acraReportingConfig, String str, boolean z, boolean z2) {
        return collectLogCat(context, acraReportingConfig, str, null, z, z2, false);
    }
}
