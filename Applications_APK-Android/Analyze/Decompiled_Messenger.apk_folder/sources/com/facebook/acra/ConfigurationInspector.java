package com.facebook.acra;

import X.AnonymousClass08S;
import X.C010708t;
import android.content.res.Configuration;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.TreeMap;

public class ConfigurationInspector {
    private static final String FIELD_MCC = "mcc";
    private static final String FIELD_MNC = "mnc";
    private static final String FIELD_SCREENLAYOUT = "screenLayout";
    private static final String FIELD_UIMODE = "uiMode";
    private static final String PREFIX_HARDKEYBOARDHIDDEN = "HARDKEYBOARDHIDDEN_";
    private static final String PREFIX_KEYBOARD = "KEYBOARD_";
    private static final String PREFIX_KEYBOARDHIDDEN = "KEYBOARDHIDDEN_";
    private static final String PREFIX_NAVIGATION = "NAVIGATION_";
    private static final String PREFIX_NAVIGATIONHIDDEN = "NAVIGATIONHIDDEN_";
    private static final String PREFIX_ORIENTATION = "ORIENTATION_";
    private static final String PREFIX_SCREENLAYOUT = "SCREENLAYOUT_";
    private static final String PREFIX_TOUCHSCREEN = "TOUCHSCREEN_";
    private static final String PREFIX_UI_MODE = "UI_MODE_";
    private static final String SUFFIX_MASK = "_MASK";
    private static SparseArray mHardKeyboardHiddenValues = new SparseArray();
    private static SparseArray mKeyboardHiddenValues = new SparseArray();
    private static SparseArray mKeyboardValues = new SparseArray();
    private static SparseArray mNavigationHiddenValues = new SparseArray();
    private static SparseArray mNavigationValues = new SparseArray();
    private static SparseArray mOrientationValues = new SparseArray();
    private static SparseArray mScreenLayoutValues = new SparseArray();
    private static SparseArray mTouchScreenValues = new SparseArray();
    private static SparseArray mUiModeValues = new SparseArray();
    private static final TreeMap mValueArrays;

    static {
        TreeMap treeMap = new TreeMap();
        mValueArrays = treeMap;
        treeMap.put(PREFIX_HARDKEYBOARDHIDDEN, mHardKeyboardHiddenValues);
        TreeMap treeMap2 = mValueArrays;
        treeMap2.put(PREFIX_KEYBOARD, mKeyboardValues);
        treeMap2.put(PREFIX_KEYBOARDHIDDEN, mKeyboardHiddenValues);
        treeMap2.put(PREFIX_NAVIGATION, mNavigationValues);
        treeMap2.put(PREFIX_NAVIGATIONHIDDEN, mNavigationHiddenValues);
        treeMap2.put(PREFIX_ORIENTATION, mOrientationValues);
        treeMap2.put(PREFIX_SCREENLAYOUT, mScreenLayoutValues);
        treeMap2.put(PREFIX_TOUCHSCREEN, mTouchScreenValues);
        treeMap2.put(PREFIX_UI_MODE, mUiModeValues);
        for (Field field : Configuration.class.getFields()) {
            if (Modifier.isStatic(field.getModifiers()) && Modifier.isFinal(field.getModifiers())) {
                String name = field.getName();
                try {
                    if (name.startsWith(PREFIX_HARDKEYBOARDHIDDEN)) {
                        mHardKeyboardHiddenValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_KEYBOARD)) {
                        mKeyboardValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_KEYBOARDHIDDEN)) {
                        mKeyboardHiddenValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_NAVIGATION)) {
                        mNavigationValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_NAVIGATIONHIDDEN)) {
                        mNavigationHiddenValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_ORIENTATION)) {
                        mOrientationValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_SCREENLAYOUT)) {
                        mScreenLayoutValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_TOUCHSCREEN)) {
                        mTouchScreenValues.put(field.getInt(null), name);
                    } else if (name.startsWith(PREFIX_UI_MODE)) {
                        mUiModeValues.put(field.getInt(null), name);
                    }
                } catch (IllegalAccessException | IllegalArgumentException e) {
                    C010708t.A0S(ACRA.LOG_TAG, e, "Error while inspecting device configuration: ");
                }
            }
        }
    }

    private static String activeFlags(SparseArray sparseArray, int i) {
        int i2;
        StringBuilder sb = new StringBuilder();
        for (int i3 = 0; i3 < sparseArray.size(); i3++) {
            int keyAt = sparseArray.keyAt(i3);
            if (((String) sparseArray.get(keyAt)).endsWith(SUFFIX_MASK) && (i2 = keyAt & i) > 0) {
                if (sb.length() > 0) {
                    sb.append('+');
                }
                sb.append((String) sparseArray.get(i2));
            }
        }
        return sb.toString();
    }

    public static String toString(Configuration configuration) {
        StringBuilder sb = new StringBuilder();
        for (Field field : configuration.getClass().getFields()) {
            try {
                if (!Modifier.isStatic(field.getModifiers())) {
                    sb.append(field.getName());
                    sb.append('=');
                    if (field.getType().equals(Integer.TYPE)) {
                        sb.append(getFieldValueName(configuration, field));
                    } else {
                        Object obj = field.get(configuration);
                        if (obj == null) {
                            sb.append("null");
                        } else if (obj instanceof Object[]) {
                            sb.append(Arrays.deepToString((Object[]) obj));
                        } else {
                            sb.append(field.get(configuration).toString());
                        }
                    }
                    sb.append(10);
                }
            } catch (IllegalAccessException | IllegalArgumentException e) {
                C010708t.A0R(ACRA.LOG_TAG, e, "Error while inspecting device configuration: ");
            }
        }
        return sb.toString();
    }

    private static String getFieldValueName(Configuration configuration, Field field) {
        String str;
        TreeMap treeMap;
        String str2;
        String name = field.getName();
        if (!name.equals(FIELD_MCC) && !name.equals(FIELD_MNC)) {
            if (name.equals(FIELD_UIMODE)) {
                treeMap = mValueArrays;
                str2 = PREFIX_UI_MODE;
            } else if (name.equals(FIELD_SCREENLAYOUT)) {
                treeMap = mValueArrays;
                str2 = PREFIX_SCREENLAYOUT;
            } else {
                SparseArray sparseArray = (SparseArray) mValueArrays.get(AnonymousClass08S.A06(name.toUpperCase(), '_'));
                if (!(sparseArray == null || (str = (String) sparseArray.get(field.getInt(configuration))) == null)) {
                    return str;
                }
            }
            return activeFlags((SparseArray) treeMap.get(str2), field.getInt(configuration));
        }
        return Integer.toString(field.getInt(configuration));
    }
}
