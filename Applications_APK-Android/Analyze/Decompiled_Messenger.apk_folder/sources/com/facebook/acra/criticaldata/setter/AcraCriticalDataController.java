package com.facebook.acra.criticaldata.setter;

import X.AnonymousClass0US;
import X.AnonymousClass0VB;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YA;
import X.AnonymousClass1YQ;
import X.AnonymousClass99A;
import X.C000700l;
import X.C04310Tq;
import X.C09420hL;
import X.C25901aa;
import X.C25921ac;
import X.C56192pV;
import android.content.Context;
import com.facebook.acra.criticaldata.CriticalAppData;
import com.facebook.auth.component.listener.interfaces.AuthenticationResult;
import com.facebook.common.util.TriState;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
public class AcraCriticalDataController implements AnonymousClass1YQ, C56192pV {
    private static volatile AcraCriticalDataController $ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXINSTANCE;
    private final Context mContext;
    private final C25921ac mDeviceId;
    private final TriState mIsEmployee;
    private final C04310Tq mLoggedInUserProvider;

    public String getSimpleName() {
        return "AcraCriticalDataController";
    }

    public static final AcraCriticalDataController $ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXFACTORY_METHOD(AnonymousClass1XY r4) {
        if ($ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXINSTANCE == null) {
            synchronized (AcraCriticalDataController.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXINSTANCE, r4);
                if (A00 != null) {
                    try {
                        $ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXINSTANCE = new AcraCriticalDataController(r4.getApplicationInjector());
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXINSTANCE;
    }

    public static final AnonymousClass0US $ul_$xXXcom_facebook_inject_Lazy$x3Ccom_facebook_acra_criticaldata_setter_AcraCriticalDataController$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BEE, r1);
    }

    public static final C04310Tq $ul_$xXXjavax_inject_Provider$x3Ccom_facebook_acra_criticaldata_setter_AcraCriticalDataController$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BEE, r1);
    }

    public void authComplete(AuthenticationResult authenticationResult) {
        if (authenticationResult != null) {
            CriticalAppData.setUserId(this.mContext, authenticationResult.B7U());
        }
    }

    public void logoutComplete() {
        CriticalAppData.setUserId(this.mContext, BuildConfig.FLAVOR);
    }

    public void onChanged(C09420hL r3, C09420hL r4, AnonymousClass99A r5, String str) {
        CriticalAppData.setDeviceId(this.mContext, r4.A01);
    }

    public static final AcraCriticalDataController $ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXACCESS_METHOD(AnonymousClass1XY r0) {
        return $ul_$xXXcom_facebook_acra_criticaldata_setter_AcraCriticalDataController$xXXFACTORY_METHOD(r0);
    }

    public AcraCriticalDataController(AnonymousClass1XY r2) {
        this.mContext = AnonymousClass1YA.A00(r2);
        this.mLoggedInUserProvider = AnonymousClass0XJ.A0K(r2);
        this.mIsEmployee = AnonymousClass0XJ.A04(r2);
        this.mDeviceId = C25901aa.A00(r2);
    }

    public void init() {
        int A03 = C000700l.A03(-1197446369);
        CriticalAppData.setUserAndDeviceId(this.mContext, (String) this.mLoggedInUserProvider.get(), this.mDeviceId.B7Z(), TriState.YES.equals(this.mIsEmployee));
        C000700l.A09(1300093768, A03);
    }
}
