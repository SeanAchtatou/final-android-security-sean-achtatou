package com.facebook.acra.uploader;

import X.AnonymousClass08S;
import X.AnonymousClass0UN;
import X.AnonymousClass0UQ;
import X.AnonymousClass0US;
import X.AnonymousClass0VG;
import X.AnonymousClass0XL;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YA;
import X.AnonymousClass1YQ;
import X.AnonymousClass94Q;
import X.BAD;
import X.BDS;
import X.BDW;
import X.C000700l;
import X.C010708t;
import X.C04310Tq;
import X.C05540Zi;
import X.C05920aY;
import X.C22797BDa;
import X.C22799BDc;
import X.C22801BDe;
import X.C22807BDk;
import X.C22816BDt;
import X.C22817BDu;
import X.C22818BDv;
import android.content.Context;
import com.facebook.acra.BatchUploader;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.util.AttachmentUtil;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.auth.viewercontext.ViewerContext;
import java.io.File;
import java.util.HashMap;

@UserScoped
public class ReportUploader implements AnonymousClass1YQ, BatchUploader {
    private static C05540Zi $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXINSTANCE = null;
    public static final String AUTHORIZATION_KEY = "Authorization";
    public static final String AUTHORIZATION_VALUE_PREFIX = "OAuth ";
    public static final String LOG_TAG = "ReportUploader";
    public static final int MAX_TRACES_TO_UPLOAD = 5;
    private AnonymousClass0UN $ul_mInjectionContext;
    private final Context mContext;
    private final BDS mUploader;
    private final C05920aY mViewerContextManager;

    public String getSimpleName() {
        return LOG_TAG;
    }

    public static final ReportUploader $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXFACTORY_METHOD(AnonymousClass1XY r4) {
        ReportUploader reportUploader;
        synchronized (ReportUploader.class) {
            C05540Zi A00 = C05540Zi.A00($ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXINSTANCE);
            $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXINSTANCE = A00;
            try {
                if (A00.A03(r4)) {
                    $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXINSTANCE.A00 = new ReportUploader((AnonymousClass1XY) $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXINSTANCE.A01());
                }
                C05540Zi r1 = $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXINSTANCE;
                reportUploader = (ReportUploader) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXINSTANCE.A02();
                throw th;
            }
        }
        return reportUploader;
    }

    public static final AnonymousClass0US $ul_$xXXcom_facebook_inject_Lazy$x3Ccom_facebook_acra_uploader_ReportUploader$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.BLS, r1);
    }

    public static final C04310Tq $ul_$xXXjavax_inject_Provider$x3Ccom_facebook_acra_uploader_ReportUploader$x3E$xXXACCESS_METHOD(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.BLS, r1);
    }

    public void uploadReports(File[] fileArr) {
        if (fileArr != null && (r7 = fileArr.length) != 0) {
            BDS bds = (BDS) AnonymousClass1XX.A03(AnonymousClass1Y3.B3E, this.$ul_mInjectionContext);
            ViewerContext B9R = this.mViewerContextManager.B9R();
            if (B9R == null || B9R.mAuthToken == null) {
                C010708t.A0I(LOG_TAG, "Could not get auth token, aborting");
                return;
            }
            C22801BDe bDe = bds.A00;
            if (bDe == null) {
                C010708t.A0I(LOG_TAG, "Could not get uploader, aborting");
                return;
            }
            AttachmentUtil.sortPruneOldFiles(fileArr, 5);
            HashMap hashMap = new HashMap();
            hashMap.put(AUTHORIZATION_KEY, AnonymousClass08S.A0J(AUTHORIZATION_VALUE_PREFIX, B9R.mAuthToken));
            BDW bdw = new BDW(C22816BDt.A03);
            bdw.A05 = hashMap;
            BAD A00 = BAD.A00();
            bdw.A00 = A00;
            bdw.A03 = new C22797BDa(A00);
            C22799BDc bDc = new C22799BDc(bdw);
            for (final File file : fileArr) {
                if (file != null) {
                    if (!AttachmentUtil.validateGzip(file)) {
                        C010708t.A0P(LOG_TAG, "Bad gzip file %s", file.getName());
                        file.delete();
                    } else {
                        C22807BDk bDk = new C22807BDk(file, "application/gzip");
                        try {
                            file.getName();
                            bDe.A01(bDk, bDc, new C22817BDu() {
                                public void onCancellation() {
                                }

                                public void onCompletion(AnonymousClass94Q r2) {
                                    file.getName();
                                    file.delete();
                                }

                                public void onFailure(C22818BDv bDv) {
                                    C010708t.A0V(ReportUploader.LOG_TAG, bDv, "onFailure %s", file.getName());
                                }

                                public void onProgress(float f) {
                                    file.getName();
                                }

                                public void onStart() {
                                    file.getName();
                                }
                            });
                        } catch (C22818BDv e) {
                            C010708t.A0U(LOG_TAG, e, "Failed to upload %s", file.getName());
                        }
                    }
                }
            }
        }
    }

    public static final ReportUploader $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXACCESS_METHOD(AnonymousClass1XY r0) {
        return $ul_$xXXcom_facebook_acra_uploader_ReportUploader$xXXFACTORY_METHOD(r0);
    }

    public ReportUploader(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(0, r3);
        this.mUploader = BDS.A00(r3);
        this.mContext = AnonymousClass1YA.A02(r3);
        this.mViewerContextManager = AnonymousClass0XL.A01(r3);
    }

    public static /* synthetic */ String access$000() {
        return LOG_TAG;
    }

    public void init() {
        int A03 = C000700l.A03(-932960492);
        ErrorReporter.getInstance().setBatchUploader(this);
        C000700l.A09(-1433186858, A03);
    }
}
