package com.facebook.acra.util;

import X.C010708t;
import android.util.JsonWriter;
import com.facebook.acra.LogCatCollector;
import com.facebook.acra.constants.ReportField;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

public class JsonReportWriter {
    public static final String LOG_TAG = "JsonReportWriter";

    public static void writeJsonReport(Map map, Map map2, OutputStream outputStream) {
        JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(outputStream, LogCatCollector.UTF_8_ENCODING));
        jsonWriter.setIndent("  ");
        jsonWriter.beginObject();
        for (Map.Entry entry : map.entrySet()) {
            if (entry.getValue() == null) {
                C010708t.A0O(LOG_TAG, "Ignoring NULL Field %s", entry.getKey());
            } else {
                jsonWriter.name((String) entry.getKey()).value((String) entry.getValue());
            }
        }
        for (Map.Entry entry2 : map2.entrySet()) {
            C010708t.A0O(LOG_TAG, "Field %s -> %s", entry2.getKey(), entry2.getValue());
            InputStreamField inputStreamField = (InputStreamField) entry2.getValue();
            if (!((String) entry2.getKey()).equals(ReportField.MINIDUMP)) {
                entry2.getKey();
            } else {
                InputStream inputStream = inputStreamField.mInputStream;
                if (inputStream instanceof FileInputStream) {
                    ((FileInputStream) inputStream).getChannel().position(0L);
                }
                jsonWriter.name((String) entry2.getKey()).value(AttachmentUtil.loadAttachment(inputStream, (int) inputStreamField.mLength));
            }
        }
        jsonWriter.endObject();
        jsonWriter.close();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0012, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeGzipJsonReport(java.util.Map r2, java.util.Map r3, java.io.OutputStream r4) {
        /*
            java.util.zip.GZIPOutputStream r1 = new java.util.zip.GZIPOutputStream
            r1.<init>(r4)
            writeJsonReport(r2, r3, r1)     // Catch:{ all -> 0x000c }
            r1.close()
            return
        L_0x000c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x000e }
        L_0x000e:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0012 }
        L_0x0012:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.JsonReportWriter.writeGzipJsonReport(java.util.Map, java.util.Map, java.io.OutputStream):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean writeGzipJsonReport(java.util.Map r3, java.util.Map r4, java.io.File r5) {
        /*
            java.lang.System.currentTimeMillis()
            r0 = 1
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x001d }
            r1.<init>(r5)     // Catch:{ IOException -> 0x001d }
            writeGzipJsonReport(r3, r4, r1)     // Catch:{ all -> 0x0016 }
            r1.close()     // Catch:{ IOException -> 0x001d }
            java.lang.System.currentTimeMillis()
            r5.length()
            return r0
        L_0x0016:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0018 }
        L_0x0018:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x001c }
        L_0x001c:
            throw r0     // Catch:{ IOException -> 0x001d }
        L_0x001d:
            r4 = move-exception
            java.lang.String r3 = "JsonReportWriter"
            java.lang.String r0 = r5.getPath()
            r2 = 0
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Could not write report %s"
            X.C010708t.A0U(r3, r4, r0, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.JsonReportWriter.writeGzipJsonReport(java.util.Map, java.util.Map, java.io.File):boolean");
    }
}
