package com.facebook.acra.util.minidump;

import X.C010708t;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.JsonToken;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashSet;

public class MinidumpReader {
    private static final String ALT_STACK = "WriteThreadUnwindStream failed";
    private static final String CUSTOM_STREAM_GLOBAL = "global";
    public static final String LOG_TAG = "MinidumpReader";
    public static final int MD_FB_APP_CUSTOM_DATA = -87110918;
    public static final int MD_FB_APP_STATE_LOG = -87110452;
    public static final int MD_FB_APP_VERSION_CODE = -87110917;
    public static final int MD_FB_APP_VERSION_NAME = -87110916;
    public static final int MD_FB_DUMP_ERROR_LOG = -87110912;
    public static final int MD_FB_JAVA_STACK = -87110915;
    public static final int MD_FB_STREAM_MARKERS = -87162880;
    private static final long MD_FB_UNWIND_SYMBOLS_OFFSET = 17592186044416L;
    private static final int MD_HEADER_SIGNATURE = 1347241037;
    public static final int MD_LINUX_CMD_LINE = 1197932550;
    public static final int MD_MODULE_LIST_STREAM = 4;
    private static final int MD_MODULE_LIST_STREAM_OFFSET = 16;
    private static final int MD_THREAD_LIST_STREAM_OFFSET = 8;
    private static final int MODULE_FULL_SIZE = 108;
    private static final String MODULE_LIST = "WriteMappings failed";
    private static final int MODULE_LIST_OFFSET = 24;
    private static final String THREAD_LIST = "WriteThreadListStream failed";
    private RandomAccessFile mHandle;
    private int mStreamCount;
    private int mStreamsPos;

    public static boolean checkMinidumpMarkerStream(long j) {
        boolean z = false;
        if ((16 & j) != 0) {
            z = true;
        }
        boolean z2 = false;
        if ((8 & j) != 0) {
            z2 = true;
        }
        boolean z3 = false;
        if ((j & MD_FB_UNWIND_SYMBOLS_OFFSET) != 0) {
            z3 = true;
        }
        return z || (z2 && z3);
    }

    private static JsonReader retrieveJsonNode(JsonReader jsonReader, String str) {
        if (jsonReader == null) {
            return null;
        }
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (jsonReader.peek() != JsonToken.NULL) {
                if (nextName.equals(str)) {
                    return jsonReader;
                }
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        return null;
    }

    public HashSet getModuleList() {
        try {
            MDLocationDescription findStream = findStream(4);
            if (findStream == null) {
                C010708t.A0I(LOG_TAG, "Stream is Null");
                return null;
            }
            this.mHandle.seek((long) findStream.pos);
            int readIntLE = readIntLE();
            HashSet hashSet = new HashSet();
            int i = findStream.pos;
            for (int i2 = 0; i2 < readIntLE; i2++) {
                this.mHandle.seek((long) (i + 24));
                this.mHandle.seek((long) readIntLE());
                String moduleString = getModuleString(new MDLocationDescription((int) this.mHandle.getFilePointer(), readIntLE()));
                if (moduleString != null && moduleString.startsWith("/system") && moduleString.endsWith(".so")) {
                    hashSet.add(moduleString);
                }
                i += MODULE_FULL_SIZE;
            }
            return hashSet;
        } catch (IOException e) {
            C010708t.A0R(LOG_TAG, e, "getModuleList failed to read");
            return null;
        }
    }

    public class MDLocationDescription {
        public int pos;
        public int size;

        public MDLocationDescription(int i, int i2) {
            this.pos = i;
            this.size = i2;
        }
    }

    public class MinidumpMarkers {
        public long endMarker;
        public long startMarker;

        public MinidumpMarkers(long j, long j2) {
            this.startMarker = j;
            this.endMarker = j2;
        }
    }

    public static boolean checkMinidumpErrLogStream(String str) {
        if (str == null) {
            return false;
        }
        if (str.contains(MODULE_LIST)) {
            return true;
        }
        if (!str.contains(THREAD_LIST) || !str.contains(ALT_STACK)) {
            return false;
        }
        return true;
    }

    private MDLocationDescription findStream(int i) {
        this.mHandle.seek((long) this.mStreamsPos);
        for (int i2 = 0; i2 < this.mStreamCount; i2++) {
            int readIntLE = readIntLE();
            int readIntLE2 = readIntLE();
            int readIntLE3 = readIntLE();
            if (readIntLE == i) {
                return new MDLocationDescription(readIntLE3, readIntLE2);
            }
        }
        return null;
    }

    private String getModuleString(MDLocationDescription mDLocationDescription) {
        this.mHandle.seek((long) mDLocationDescription.pos);
        int i = mDLocationDescription.size;
        byte[] bArr = new byte[i];
        byte[] bArr2 = new byte[(i >> 1)];
        this.mHandle.read(bArr);
        for (int i2 = 0; i2 < (mDLocationDescription.size >> 1); i2++) {
            bArr2[i2] = bArr[i2 << 1];
        }
        return new String(bArr2);
    }

    private int readIntLE() {
        int readInt = this.mHandle.readInt();
        return ((readInt >> 24) & 255) | ((readInt & 255) << 24) | ((65280 & readInt) << 8) | ((16711680 & readInt) >> 8);
    }

    private long readLongIntLE() {
        long readLong = this.mHandle.readLong();
        return (((readLong >> 56) & 255) << 0) | (((readLong >> 0) & 255) << 56) | (((readLong >> 8) & 255) << 48) | (((readLong >> 16) & 255) << 40) | (((readLong >> 24) & 255) << 32) | (((readLong >> 32) & 255) << 24) | (((readLong >> 40) & 255) << 16) | (((readLong >> 48) & 255) << 8);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getCustomDataFromJson(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            android.util.JsonReader r1 = new android.util.JsonReader
            java.io.StringReader r0 = new java.io.StringReader
            r0.<init>(r3)
            r1.<init>(r0)
            java.lang.String r0 = "global"
            android.util.JsonReader r0 = retrieveJsonNode(r1, r0)     // Catch:{ all -> 0x0020 }
            android.util.JsonReader r0 = retrieveJsonNode(r0, r4)     // Catch:{ all -> 0x0020 }
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = r0.nextString()     // Catch:{ all -> 0x0020 }
            goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            r1.close()
            return r0
        L_0x0020:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0022 }
        L_0x0022:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0026 }
        L_0x0026:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.minidump.MinidumpReader.getCustomDataFromJson(java.lang.String, java.lang.String):java.lang.String");
    }

    public MinidumpMarkers getMinidumpMarkers() {
        try {
            MDLocationDescription findStream = findStream(MD_FB_STREAM_MARKERS);
            if (findStream == null) {
                return null;
            }
            this.mHandle.seek((long) findStream.pos);
            return new MinidumpMarkers(readLongIntLE(), readLongIntLE());
        } catch (IOException e) {
            C010708t.A0R(LOG_TAG, e, "getMinidumpMarkers failed to read");
            return null;
        }
    }

    public MinidumpReader(RandomAccessFile randomAccessFile) {
        this.mHandle = randomAccessFile;
        randomAccessFile.seek(0);
        if (readIntLE() == MD_HEADER_SIGNATURE) {
            this.mHandle.skipBytes(4);
            this.mStreamCount = readIntLE();
            this.mStreamsPos = readIntLE();
            return;
        }
        throw new RuntimeException("Invalid minidump signature");
    }

    public boolean checkIfMinidumpCorrupted() {
        boolean z;
        MinidumpMarkers minidumpMarkers = getMinidumpMarkers();
        if (minidumpMarkers != null) {
            z = checkMinidumpMarkerStream(minidumpMarkers.startMarker ^ minidumpMarkers.endMarker);
        } else {
            z = false;
        }
        boolean checkMinidumpErrLogStream = checkMinidumpErrLogStream(getErrorLogData());
        if (z || checkMinidumpErrLogStream) {
            return true;
        }
        return false;
    }

    public String getCustomData(String str) {
        String str2;
        try {
            str2 = getString((int) MD_FB_APP_CUSTOM_DATA);
            if (str2 == null) {
                return null;
            }
            try {
                return getCustomDataFromJson(str2, str);
            } catch (Exception e) {
                e = e;
                C010708t.A0U(LOG_TAG, e, "getCustomData error: %s", str2);
                return null;
            }
        } catch (Exception e2) {
            e = e2;
            str2 = BuildConfig.FLAVOR;
            C010708t.A0U(LOG_TAG, e, "getCustomData error: %s", str2);
            return null;
        }
    }

    public String getErrorLogData() {
        try {
            return getString((int) MD_FB_DUMP_ERROR_LOG);
        } catch (IOException e) {
            C010708t.A0U(LOG_TAG, e, "getErrorLogData error: %s", BuildConfig.FLAVOR);
            return null;
        }
    }

    public String getJavaStack() {
        try {
            return getString((int) MD_FB_JAVA_STACK);
        } catch (IOException e) {
            C010708t.A0R(LOG_TAG, e, "getJavaStack error");
            return null;
        }
    }

    public Integer getInt(int i) {
        return getInt(findStream(i));
    }

    private Integer getInt(MDLocationDescription mDLocationDescription) {
        if (mDLocationDescription == null || mDLocationDescription.size != 4) {
            return null;
        }
        this.mHandle.seek((long) mDLocationDescription.pos);
        return Integer.valueOf(readIntLE());
    }

    public String getString(int i) {
        String string = getString(findStream(i));
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return string;
    }

    private String getString(MDLocationDescription mDLocationDescription) {
        if (mDLocationDescription == null) {
            return null;
        }
        this.mHandle.seek((long) mDLocationDescription.pos);
        byte[] bArr = new byte[mDLocationDescription.size];
        this.mHandle.read(bArr);
        return new String(bArr);
    }
}
