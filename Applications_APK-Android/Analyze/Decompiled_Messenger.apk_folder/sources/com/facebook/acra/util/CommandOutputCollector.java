package com.facebook.acra.util;

public class CommandOutputCollector {
    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|(2:6|(1:8)(1:9))|10|11|12|13|14|15|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0051 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0051=Splitter:B:23:0x0051, B:13:0x003c=Splitter:B:13:0x003c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String collect(java.lang.String... r8) {
        /*
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.ProcessBuilder r1 = new java.lang.ProcessBuilder
            r1.<init>(r8)
            r0 = 1
            java.lang.ProcessBuilder r0 = r1.redirectErrorStream(r0)
            java.lang.Process r6 = r0.start()
            java.io.OutputStream r0 = r6.getOutputStream()     // Catch:{ all -> 0x0052 }
            r0.close()     // Catch:{ all -> 0x0052 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ all -> 0x0052 }
            java.io.InputStream r0 = r6.getInputStream()     // Catch:{ all -> 0x0052 }
            r5.<init>(r0)     // Catch:{ all -> 0x0052 }
            r0 = 4096(0x1000, float:5.74E-42)
            r4 = 4096(0x1000, float:5.74E-42)
            char[] r3 = new char[r0]     // Catch:{ all -> 0x004b }
        L_0x0029:
            r2 = 0
            int r1 = r5.read(r3, r2, r4)     // Catch:{ all -> 0x004b }
            r0 = -1
            if (r1 == r0) goto L_0x0035
            r7.append(r3, r2, r1)     // Catch:{ all -> 0x004b }
            goto L_0x0029
        L_0x0035:
            r5.close()     // Catch:{ all -> 0x0052 }
            r6.waitFor()     // Catch:{ InterruptedException -> 0x003c }
            goto L_0x0043
        L_0x003c:
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0052 }
            r0.interrupt()     // Catch:{ all -> 0x0052 }
        L_0x0043:
            r6.destroy()
            java.lang.String r0 = r7.toString()
            return r0
        L_0x004b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004d }
        L_0x004d:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x0051 }
        L_0x0051:
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
            r6.destroy()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.CommandOutputCollector.collect(java.lang.String[]):java.lang.String");
    }
}
