package com.facebook.acra.util;

import X.AnonymousClass1Y3;
import X.C010708t;
import android.os.Process;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class JavaProcFileReader extends ProcFileReader {
    private static final String FD_DIR = String.format("/proc/%s/fd", Integer.valueOf(Process.myPid()));
    private static final String FD_DIR_STRING = "/fd/";
    private static final String LS_SYMLINK_ARROW = " -> ";
    private static final String PIPE_STRING = "pipe";
    private static final int[] PROC_OPEN_FD_LIMITS_FORMAT = {32, 32, AnonymousClass1Y3.A2G, AnonymousClass1Y3.AaT, AnonymousClass1Y3.AaT, AnonymousClass1Y3.A2G};
    private static final String SOCKET_STRING = "socket";
    private static final String TAG = "JavaProcFileReader";
    private static JavaProcFileReader sInstance;

    private static int findNewLineOrEnd(byte[] bArr, int i) {
        int length = bArr.length;
        if (i >= length) {
            return -1;
        }
        while (i < length - 1 && (r1 = bArr[i]) != 10 && r1 != 0) {
            i++;
        }
        return i;
    }

    private static boolean startsWithOffset(byte[] bArr, int i, byte[] bArr2) {
        int length = bArr.length - i;
        int length2 = bArr2.length;
        if (length >= length2) {
            int i2 = 0;
            while (i2 < length2) {
                if (bArr[i2 + i] == bArr2[i2]) {
                    i2++;
                }
            }
            return true;
        }
        return false;
    }

    public static synchronized JavaProcFileReader getInstance() {
        JavaProcFileReader javaProcFileReader;
        synchronized (JavaProcFileReader.class) {
            if (sInstance == null) {
                sInstance = new JavaProcFileReader();
            }
            javaProcFileReader = sInstance;
        }
        return javaProcFileReader;
    }

    public int getOpenFDCount() {
        try {
            String[] list = new File(FD_DIR).list();
            if (list != null) {
                return list.length;
            }
            return -1;
        } catch (SecurityException e) {
            C010708t.A0I(TAG, e.getMessage());
            return -2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0059, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x005d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.acra.util.ProcFileReader.OpenFDLimits getOpenFDLimits() {
        /*
            r13 = this;
            r0 = 8192(0x2000, float:1.14794E-41)
            byte[] r6 = new byte[r0]
            java.lang.String r0 = "Max open files"
            byte[] r5 = r0.getBytes()
            r0 = 2
            java.lang.String[] r10 = new java.lang.String[r0]
            r11 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x005e }
            java.lang.String r0 = "/proc/self/limits"
            r1.<init>(r0)     // Catch:{ IOException -> 0x005e }
            r4 = 1
            r0 = 8191(0x1fff, float:1.1478E-41)
            r3 = 0
            int r2 = r1.read(r6, r3, r0)     // Catch:{ all -> 0x0057 }
            r6[r2] = r3     // Catch:{ all -> 0x0057 }
            r1.close()     // Catch:{ IOException -> 0x005e }
            r7 = 0
        L_0x0023:
            int r1 = r2 + -1
            int r1 = r1 - r7
            int r0 = r5.length
            if (r1 <= r0) goto L_0x004d
            int r8 = findNewLineOrEnd(r6, r7)
            boolean r0 = startsWithOffset(r6, r7, r5)
            if (r0 == 0) goto L_0x0054
            int[] r9 = com.facebook.acra.util.JavaProcFileReader.PROC_OPEN_FD_LIMITS_FORMAT
            X.00X r5 = X.AnonymousClass00V.A00
            if (r5 != 0) goto L_0x004e
            r0 = 0
        L_0x003a:
            if (r0 == 0) goto L_0x004d
            com.facebook.acra.util.ProcFileReader$OpenFDLimits r11 = new com.facebook.acra.util.ProcFileReader$OpenFDLimits
            r0 = r10[r3]
            int r1 = java.lang.Integer.parseInt(r0)
            r0 = r10[r4]
            int r0 = java.lang.Integer.parseInt(r0)
            r11.<init>(r1, r0)
        L_0x004d:
            return r11
        L_0x004e:
            r12 = r11
            boolean r0 = r5.BwU(r6, r7, r8, r9, r10, r11, r12)
            goto L_0x003a
        L_0x0054:
            int r7 = r8 + 1
            goto L_0x0023
        L_0x0057:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0059 }
        L_0x0059:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x005d }
        L_0x005d:
            throw r0     // Catch:{ IOException -> 0x005e }
        L_0x005e:
            r2 = move-exception
            java.lang.String r1 = "JavaProcFileReader"
            java.lang.String r0 = "Failed to read /proc/self/limits"
            X.C010708t.A0S(r1, r2, r0)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.JavaProcFileReader.getOpenFDLimits():com.facebook.acra.util.ProcFileReader$OpenFDLimits");
    }

    public String getOpenFileDescriptors() {
        String str;
        StringBuilder sb = new StringBuilder();
        try {
            String[] split = CommandOutputCollector.collect("/system/bin/ls", "-l", String.format(FD_DIR, new Object[0])).split("\n");
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 1; i < split.length; i++) {
                String str2 = split[i];
                int lastIndexOf = str2.lastIndexOf(LS_SYMLINK_ARROW);
                if (lastIndexOf != -1) {
                    String substring = str2.substring(lastIndexOf + 4);
                    int lastIndexOf2 = substring.lastIndexOf(FD_DIR_STRING);
                    if (lastIndexOf2 == -1) {
                        str = substring;
                    } else {
                        str = substring.substring(lastIndexOf2 + 4);
                    }
                    if (str.startsWith(PIPE_STRING)) {
                        substring = PIPE_STRING;
                    } else if (str.startsWith(SOCKET_STRING)) {
                        substring = SOCKET_STRING;
                    }
                    Counter counter = (Counter) linkedHashMap.get(substring);
                    if (counter == null) {
                        counter = new Counter();
                        linkedHashMap.put(substring, counter);
                    }
                    counter.count++;
                }
            }
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                sb.append((String) entry.getKey());
                sb.append(" ");
                sb.append(((Counter) entry.getValue()).count);
                sb.append("\n");
            }
            return sb.toString();
        } catch (IOException | IndexOutOfBoundsException | SecurityException e) {
            C010708t.A0R(TAG, e, "Exception caught while reading open file descriptors");
            return e.getMessage();
        }
    }

    private JavaProcFileReader() {
    }

    public class Counter {
        public int count;

        public Counter() {
        }
    }
}
