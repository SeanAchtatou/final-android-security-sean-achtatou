package com.facebook.acra.util;

import com.facebook.acra.util.ProcFileReader;
import java.util.concurrent.atomic.AtomicBoolean;

public class NativeProcFileReader extends ProcFileReader {
    private static final String TAG = "NativeProcFileReader";
    private static NativeProcFileReader sInstance;
    private static Thread sLoadSoThread;
    public static final AtomicBoolean sReadyToUse = new AtomicBoolean(false);

    private native int[] getOpenFDLimitsNative();

    public native int getOpenFDCount();

    public native String getOpenFileDescriptors();

    public static synchronized NativeProcFileReader getInstance() {
        NativeProcFileReader nativeProcFileReader;
        synchronized (NativeProcFileReader.class) {
            if (sInstance == null) {
                sInstance = new NativeProcFileReader();
            }
            nativeProcFileReader = sInstance;
        }
        return nativeProcFileReader;
    }

    public static boolean isReady() {
        return sReadyToUse.get();
    }

    public static void nativeLibraryLoaded() {
        Class<NativeProcFileReader> cls = NativeProcFileReader.class;
        synchronized (cls) {
            sReadyToUse.set(true);
            cls.notifyAll();
        }
    }

    private NativeProcFileReader() {
        if (!sReadyToUse.get()) {
            throw new IllegalStateException("Class is not ready");
        }
    }

    public ProcFileReader.OpenFDLimits getOpenFDLimits() {
        int[] openFDLimitsNative = getOpenFDLimitsNative();
        return new ProcFileReader.OpenFDLimits(openFDLimitsNative[0], openFDLimitsNative[1]);
    }
}
