package com.facebook.acra.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

public class HttpRequestMultipart {
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String CONTENT_DISPOSITION_FILE = "form-data; filename=\"file\"; name=";
    private static final String CONTENT_DISPOSITION_FORM_DATA = "form-data; name=";
    private static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding";
    private static final String CONTENT_TRANSFER_ENCODING_BINARY = "binary";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_APP_BINARY = "application/binary";
    private static final String CONTENT_TYPE_FORM_MULTIPART_FORMAT = "multipart/form-data;boundary=%s";
    private static final String LINE_FEED = "\r\n";
    private static final int STREAM_BLOCK_SIZE = 8192;
    private static final String USER_AGENT = "User-Agent";
    private HttpConnectionProvider mConnectionProvider;
    public Map mHeaders;

    private static void copyStream(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[8192];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    private static void writeAcraData(String str, OutputStream outputStream, String str2, Map map) {
        outputStream.write(generateMultipartHeader(str2, CONTENT_DISPOSITION_FORM_DATA, str));
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
        HttpRequest.encodeParameters(map, gZIPOutputStream);
        gZIPOutputStream.finish();
        outputStream.write(LINE_FEED.getBytes());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ea, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00eb, code lost:
        if (r8 != null) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00f0 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendPost(java.net.URL r10, java.util.Map r11, java.util.Map r12, com.facebook.acra.util.ACRAResponse r13, java.lang.String r14, boolean r15) {
        /*
            r9 = this;
            com.facebook.acra.util.HttpConnectionProvider r0 = r9.mConnectionProvider
            java.net.HttpURLConnection r1 = r0.getConnection(r10)
            java.lang.String r4 = generateBoundary()
            java.lang.String r0 = "POST"
            r1.setRequestMethod(r0)
            java.lang.String r0 = "User-Agent"
            r1.setRequestProperty(r0, r14)
            r7 = 1
            r6 = 0
            java.lang.Object[] r2 = new java.lang.Object[]{r4}
            java.lang.String r0 = "multipart/form-data;boundary=%s"
            java.lang.String r2 = java.lang.String.format(r0, r2)
            java.lang.String r0 = "Content-Type"
            r1.setRequestProperty(r0, r2)
            java.util.Map r0 = r9.mHeaders
            if (r0 == 0) goto L_0x0059
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0059
            java.util.Map r0 = r9.mHeaders
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r5 = r0.iterator()
        L_0x0039:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0059
            java.lang.Object r3 = r5.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r0 = r3.getKey()
            java.lang.String r2 = r0.toString()
            java.lang.Object r0 = r3.getValue()
            java.lang.String r0 = r0.toString()
            r1.setRequestProperty(r2, r0)
            goto L_0x0039
        L_0x0059:
            r1.setDoOutput(r7)
            r1.setChunkedStreamingMode(r6)
            r0 = -1925968007(0xffffffff8d340f79, float:-5.5485407E-31)
            java.io.OutputStream r8 = X.AnonymousClass0EN.A01(r1, r0)     // Catch:{ all -> 0x00f1 }
            java.io.OutputStream r3 = com.facebook.acra.util.AcraRadioMonitorBridge.createOutputDecorator(r8)     // Catch:{ all -> 0x00e8 }
            java.lang.String r0 = "acra_data"
            writeAcraData(r0, r3, r4, r11)     // Catch:{ all -> 0x00e8 }
            java.util.Set r0 = r12.entrySet()     // Catch:{ all -> 0x00e8 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x00e8 }
        L_0x0077:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x00e8 }
            if (r0 == 0) goto L_0x00c5
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x00e8 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x00e8 }
            java.lang.Object r6 = r0.getValue()     // Catch:{ all -> 0x00e8 }
            com.facebook.acra.util.InputStreamField r6 = (com.facebook.acra.util.InputStreamField) r6     // Catch:{ all -> 0x00e8 }
            boolean r5 = r6.mSendCompressed     // Catch:{ all -> 0x00e8 }
            boolean r2 = r6.mSendAsAFile     // Catch:{ all -> 0x00e8 }
            java.lang.Object r0 = r0.getKey()     // Catch:{ all -> 0x00e8 }
            if (r2 == 0) goto L_0x00c2
            java.lang.String r2 = "form-data; filename=\"file\"; name="
        L_0x0095:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00e8 }
            byte[] r0 = generateMultipartHeader(r4, r2, r0)     // Catch:{ all -> 0x00e8 }
            r3.write(r0)     // Catch:{ all -> 0x00e8 }
            if (r5 == 0) goto L_0x00a3
            goto L_0x00a5
        L_0x00a3:
            r2 = r3
            goto L_0x00ac
        L_0x00a5:
            com.facebook.acra.util.CompressionOutputStream r2 = new com.facebook.acra.util.CompressionOutputStream     // Catch:{ all -> 0x00e8 }
            r0 = 8192(0x2000, float:1.14794E-41)
            r2.<init>(r3, r0, r15)     // Catch:{ all -> 0x00e8 }
        L_0x00ac:
            java.io.InputStream r0 = r6.mInputStream     // Catch:{ all -> 0x00e8 }
            copyStream(r0, r2)     // Catch:{ all -> 0x00e8 }
            if (r5 == 0) goto L_0x00b8
            com.facebook.acra.util.CompressionOutputStream r2 = (com.facebook.acra.util.CompressionOutputStream) r2     // Catch:{ all -> 0x00e8 }
            r2.finish()     // Catch:{ all -> 0x00e8 }
        L_0x00b8:
            java.lang.String r0 = "\r\n"
            byte[] r0 = r0.getBytes()     // Catch:{ all -> 0x00e8 }
            r3.write(r0)     // Catch:{ all -> 0x00e8 }
            goto L_0x0077
        L_0x00c2:
            java.lang.String r2 = "form-data; name="
            goto L_0x0095
        L_0x00c5:
            byte[] r0 = generateMultipartEndFooter(r4)     // Catch:{ all -> 0x00e8 }
            r3.write(r0)     // Catch:{ all -> 0x00e8 }
            r3.flush()     // Catch:{ all -> 0x00e8 }
            int r0 = r1.getResponseCode()     // Catch:{ all -> 0x00e8 }
            r13.mStatus = r0     // Catch:{ all -> 0x00e8 }
            r0 = -1357962091(0xffffffffaf0f2495, float:-1.3018771E-10)
            java.io.InputStream r0 = X.AnonymousClass0EN.A00(r1, r0)     // Catch:{ all -> 0x00e8 }
            r0.close()     // Catch:{ all -> 0x00e8 }
            if (r8 == 0) goto L_0x00e4
            r8.close()     // Catch:{ all -> 0x00f1 }
        L_0x00e4:
            r1.disconnect()
            return
        L_0x00e8:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ea }
        L_0x00ea:
            r0 = move-exception
            if (r8 == 0) goto L_0x00f0
            r8.close()     // Catch:{ all -> 0x00f0 }
        L_0x00f0:
            throw r0     // Catch:{ all -> 0x00f1 }
        L_0x00f1:
            r0 = move-exception
            r1.disconnect()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.HttpRequestMultipart.sendPost(java.net.URL, java.util.Map, java.util.Map, com.facebook.acra.util.ACRAResponse, java.lang.String, boolean):void");
    }

    public HttpRequestMultipart(HttpConnectionProvider httpConnectionProvider) {
        this.mConnectionProvider = httpConnectionProvider;
    }

    private static String generateBoundary() {
        return UUID.randomUUID().toString();
    }

    private static byte[] generateMultipartEndFooter(String str) {
        return String.format("--%s--\r\n", str).getBytes();
    }

    private static byte[] generateMultipartHeader(String str, String str2, String str3) {
        return String.format("--%s\r\nContent-Disposition: %s\"%s\"\r\nContent-Type: application/binary\r\nContent-Transfer-Encoding: binary\r\n\r\n", str, str2, str3).getBytes();
    }

    public void setHeaders(Map map) {
        this.mHeaders = map;
    }
}
