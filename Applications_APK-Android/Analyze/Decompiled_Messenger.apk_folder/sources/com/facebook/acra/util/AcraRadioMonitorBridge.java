package com.facebook.acra.util;

import android.os.SystemClock;
import java.io.OutputStream;

public class AcraRadioMonitorBridge {
    private static volatile AcraRadioListener mListener;

    public class OutputStreamDecorator extends OutputStream {
        private final OutputStream mDecoratedStream;
        private final AcraRadioListener mListener;

        public OutputStreamDecorator(OutputStream outputStream, AcraRadioListener acraRadioListener) {
            this.mDecoratedStream = outputStream;
            this.mListener = acraRadioListener;
        }

        public void close() {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                this.mDecoratedStream.close();
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), 0);
            } catch (Throwable th) {
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), 0);
                throw th;
            }
        }

        public void flush() {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                this.mDecoratedStream.flush();
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), 0);
            } catch (Throwable th) {
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), 0);
                throw th;
            }
        }

        public void write(int i) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                this.mDecoratedStream.write(i);
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), 1);
            } catch (Throwable th) {
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), 1);
                throw th;
            }
        }

        public void write(byte[] bArr) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                this.mDecoratedStream.write(bArr);
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), bArr.length);
            } catch (Throwable th) {
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), bArr.length);
                throw th;
            }
        }

        public void write(byte[] bArr, int i, int i2) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            int i3 = i2;
            try {
                this.mDecoratedStream.write(bArr, i, i2);
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), i3);
            } catch (Throwable th) {
                this.mListener.onRadioActive(elapsedRealtime, SystemClock.elapsedRealtime(), i2);
                throw th;
            }
        }
    }

    public static OutputStream createOutputDecorator(OutputStream outputStream) {
        if (mListener != null) {
            return new OutputStreamDecorator(outputStream, mListener);
        }
        return outputStream;
    }

    public static void setRadioListener(AcraRadioListener acraRadioListener) {
        mListener = acraRadioListener;
    }
}
