package com.facebook.acra.util;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.util.UUID;

public class Installation {
    private static final String INSTALLATION = "ACRA-INSTALLATION";
    private static String sID;

    public static synchronized String id(Context context) {
        String str;
        synchronized (Installation.class) {
            if (sID == null) {
                File filesDir = context.getFilesDir();
                if (filesDir == null) {
                    str = "n/a";
                } else {
                    File file = new File(filesDir.getParent(), INSTALLATION);
                    try {
                        if (!file.exists()) {
                            writeInstallationFile(file);
                        }
                        sID = readInstallationFile(file);
                    } catch (Exception unused) {
                        str = "n/a";
                    }
                }
            }
            str = sID;
        }
        return str;
    }

    private static String readInstallationFile(File file) {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        try {
            byte[] bArr = new byte[((int) randomAccessFile.length())];
            randomAccessFile.readFully(bArr);
            return new String(bArr);
        } finally {
            randomAccessFile.close();
        }
    }

    private static void writeInstallationFile(File file) {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        try {
            fileOutputStream.write(UUID.randomUUID().toString().getBytes());
        } finally {
            fileOutputStream.close();
        }
    }
}
