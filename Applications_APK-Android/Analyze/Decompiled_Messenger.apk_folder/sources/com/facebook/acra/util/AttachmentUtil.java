package com.facebook.acra.util;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public class AttachmentUtil {
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0024, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0020, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String compressToBase64String(byte[] r4) {
        /*
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream
            r2.<init>(r3)
            int r0 = r4.length     // Catch:{ all -> 0x001e }
            r1 = 0
            r2.write(r4, r1, r0)     // Catch:{ all -> 0x001e }
            r2.finish()     // Catch:{ all -> 0x001e }
            byte[] r0 = r3.toByteArray()     // Catch:{ all -> 0x001e }
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r1)     // Catch:{ all -> 0x001e }
            r2.close()
            return r0
        L_0x001e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0020 }
        L_0x0020:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0024 }
        L_0x0024:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.AttachmentUtil.compressToBase64String(byte[]):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0013 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String loadAttachment(java.io.InputStream r4, int r5) {
        /*
            byte[] r3 = new byte[r5]
            r2 = 0
            r1 = 0
        L_0x0004:
            int r0 = r5 - r2
            if (r0 <= 0) goto L_0x0011
            int r1 = r4.read(r3, r2, r0)
            r0 = -1
            if (r1 == r0) goto L_0x0011
            int r2 = r2 + r1
            goto L_0x0004
        L_0x0011:
            if (r1 != 0) goto L_0x0016
            java.lang.String r0 = ""
            return r0
        L_0x0016:
            java.lang.String r0 = compressToBase64String(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.AttachmentUtil.loadAttachment(java.io.InputStream, int):java.lang.String");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|22|23|24) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:28|29|30|31|32) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x002f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0036 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x003d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean validateGzip(java.io.File r4) {
        /*
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x003e }
            r3.<init>(r4)     // Catch:{ IOException -> 0x003e }
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0037 }
            r2.<init>(r3)     // Catch:{ all -> 0x0037 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x0030 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ all -> 0x0030 }
            r0.<init>(r2)     // Catch:{ all -> 0x0030 }
            r1.<init>(r0)     // Catch:{ all -> 0x0030 }
        L_0x0014:
            boolean r0 = r1.ready()     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x001e
            r1.readLine()     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x001e:
            r1.close()     // Catch:{ all -> 0x0030 }
            r2.close()     // Catch:{ all -> 0x0037 }
            r3.close()     // Catch:{ IOException -> 0x003e }
            r0 = 1
            return r0
        L_0x0029:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x002f }
        L_0x002f:
            throw r0     // Catch:{ all -> 0x0030 }
        L_0x0030:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0032 }
        L_0x0032:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0036 }
        L_0x0036:
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x003d }
        L_0x003d:
            throw r0     // Catch:{ IOException -> 0x003e }
        L_0x003e:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.util.AttachmentUtil.validateGzip(java.io.File):boolean");
    }

    public static File[] sortPruneOldFiles(File file, int i) {
        File[] listFiles = file.listFiles();
        sortPruneOldFiles(listFiles, i);
        return listFiles;
    }

    public static File[] sortPruneOldFiles(File[] fileArr, int i) {
        int length = fileArr.length;
        if (length != 0) {
            Arrays.sort(fileArr, new Comparator() {
                public int compare(File file, File file2) {
                    return Long.valueOf(file2.lastModified()).compareTo(Long.valueOf(file.lastModified()));
                }
            });
            if (length > i) {
                while (i < length) {
                    fileArr[i].delete();
                    fileArr[i] = null;
                    i++;
                }
            }
        }
        return fileArr;
    }
}
