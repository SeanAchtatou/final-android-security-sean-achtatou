package com.facebook.acra.util;

import android.os.Build;

public class NoSync {
    private static boolean sSyncDisabled;

    private static native boolean disableFSSync(int i, boolean z);

    private NoSync() {
    }

    public static void disableFSSync(boolean z) {
        if (!sSyncDisabled) {
            sSyncDisabled = disableFSSync(Build.VERSION.SDK_INT, z);
        }
    }
}
