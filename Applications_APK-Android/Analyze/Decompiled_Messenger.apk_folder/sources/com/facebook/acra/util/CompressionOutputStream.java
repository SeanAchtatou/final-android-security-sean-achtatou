package com.facebook.acra.util;

import X.C03860Pz;
import java.io.FilterOutputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.zip.GZIPOutputStream;

public class CompressionOutputStream extends FilterOutputStream {
    private static boolean triedLoadingZstd;
    private static Class zstdImpl;
    private boolean mUseZstd;

    private static OutputStream getUnderlyingStream(OutputStream outputStream, int i, boolean z) {
        if (z && getZstdImpl() != null) {
            try {
                Class zstdImpl2 = getZstdImpl();
                Class cls = Integer.TYPE;
                return (OutputStream) zstdImpl2.getConstructor(OutputStream.class, cls, cls).newInstance(outputStream, Integer.valueOf(i), 13);
            } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException unused) {
                zstdImpl = null;
            }
        }
        return new GZIPOutputStream(outputStream);
    }

    private static Class getZstdImpl() {
        if (triedLoadingZstd) {
            return zstdImpl;
        }
        try {
            zstdImpl = Class.forName("com.facebook.zstd.ZstdOutputStream");
        } catch (ClassNotFoundException | ExceptionInInitializerError unused) {
            zstdImpl = null;
        }
        triedLoadingZstd = true;
        return zstdImpl;
    }

    public void finish() {
        if (!this.mUseZstd || getZstdImpl() == null) {
            ((GZIPOutputStream) this.out).finish();
        } else {
            ((C03860Pz) this.out).A00();
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        this.out.write(bArr, i, i2);
    }

    public CompressionOutputStream(OutputStream outputStream, int i, boolean z) {
        super(getUnderlyingStream(outputStream, i, z));
        this.mUseZstd = z;
    }
}
