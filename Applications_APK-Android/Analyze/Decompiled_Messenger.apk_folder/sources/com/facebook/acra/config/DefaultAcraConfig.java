package com.facebook.acra.config;

import android.content.Context;
import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anr.hybrid.HybridANRDetector;
import com.facebook.acra.anr.processmonitor.detector.ProcessErrorMonitorANRDetector;
import com.facebook.acra.anr.sigquit.detector.SigquitBasedANRDetector;
import com.facebook.acra.sender.FlexibleReportSender;
import com.facebook.acra.sender.HttpPostSender;
import java.lang.Thread;

public class DefaultAcraConfig implements AcraReportingConfig {
    private final Context mApplicationContext;
    private final String mCrashReportUrl;
    private final String mDefaultCrashReportUrl;
    private final Thread.UncaughtExceptionHandler mExceptionHandler;
    private final boolean mIsInternalBuild;
    private final boolean mIsZeroCrashlogBlocked;
    private final String mSessionId;
    private final boolean mShouldStartANRDetector;

    public boolean allowCollectionOfMaxNumberOfLinesInLogcat() {
        return false;
    }

    public boolean allowProxy() {
        return true;
    }

    public boolean allowUnsafeConnectionsForDebugging() {
        return true;
    }

    public IANRDetector createANRDetector(int i, ANRDetectorConfig aNRDetectorConfig, int i2) {
        if (i == 5) {
            return ProcessErrorMonitorANRDetector.getInstance(aNRDetectorConfig, i2);
        }
        if (i == 3) {
            return SigquitBasedANRDetector.getInstance(aNRDetectorConfig);
        }
        if (i == 4) {
            return HybridANRDetector.getInstance(aNRDetectorConfig, i2);
        }
        return null;
    }

    public boolean enableLeanCrashReporting() {
        return false;
    }

    public String getLogcatNumberOfLinesToCapture() {
        return "200";
    }

    public int getMaxPendingJavaCrashReports() {
        return Integer.MAX_VALUE;
    }

    public int getMaxPendingMiniDumpReports() {
        return Integer.MAX_VALUE;
    }

    public int getOomReservationOverride() {
        return 0;
    }

    public long getPreallocatedFileSizeOverride() {
        return 0;
    }

    public StartupBlockingConfig getStartupBlockingConfig() {
        return null;
    }

    public String getUserAgent() {
        return "Android";
    }

    public int reportSoftErrorsImmediately() {
        return 0;
    }

    public boolean shouldImmediatelyUpload() {
        return true;
    }

    public boolean shouldInstallPeriodicReporter() {
        return true;
    }

    public boolean shouldLazyFieldsOverwriteExistingValues() {
        return false;
    }

    public boolean shouldOnlyWriteReport() {
        return false;
    }

    public boolean shouldReportField(String str) {
        return true;
    }

    public boolean shouldSkipReportOnSocketTimeout() {
        return false;
    }

    public boolean shouldStopAnrDetectorOnErrorReporting() {
        return false;
    }

    public boolean shouldUseUploadService() {
        return false;
    }

    public int socketTimeout() {
        return 3000;
    }

    public FlexibleReportSender createReportSender() {
        return new HttpPostSender(this);
    }

    public String[] logcatArguments(boolean z) {
        String logcatNumberOfLinesToCapture;
        if (z) {
            logcatNumberOfLinesToCapture = "10000";
        } else {
            logcatNumberOfLinesToCapture = getLogcatNumberOfLinesToCapture();
        }
        return new String[]{"-t", logcatNumberOfLinesToCapture, "-v", "threadtime"};
    }

    public String crashReportUrl() {
        return this.mCrashReportUrl;
    }

    public Context getApplicationContext() {
        return this.mApplicationContext;
    }

    public String getDefaultCrashLogUrl() {
        return this.mDefaultCrashReportUrl;
    }

    public Thread.UncaughtExceptionHandler getDefaultUncaughtExceptionHandler() {
        return this.mExceptionHandler;
    }

    public String getSessionId() {
        return this.mSessionId;
    }

    public boolean isInternalBuild() {
        return this.mIsInternalBuild;
    }

    public boolean isZeroCrashlogBlocked() {
        return this.mIsZeroCrashlogBlocked;
    }

    public boolean shouldStartANRDetector() {
        return this.mShouldStartANRDetector;
    }

    public DefaultAcraConfig(Context context, String str, boolean z) {
        this(context, str, z, false, false);
    }

    public DefaultAcraConfig(Context context, String str, boolean z, boolean z2, boolean z3) {
        this(context, str, z, z2, z3, null);
    }

    public DefaultAcraConfig(Context context, String str, boolean z, boolean z2, boolean z3, String str2) {
        if (context == null) {
            throw new IllegalArgumentException("Application context cannot be null.");
        } else if (str != null) {
            this.mApplicationContext = context;
            this.mCrashReportUrl = str;
            this.mDefaultCrashReportUrl = str;
            this.mIsInternalBuild = z;
            this.mExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            this.mShouldStartANRDetector = z2;
            this.mIsZeroCrashlogBlocked = z3;
            this.mSessionId = str2;
        } else {
            throw new IllegalArgumentException("Crash report url cannot be null.");
        }
    }
}
