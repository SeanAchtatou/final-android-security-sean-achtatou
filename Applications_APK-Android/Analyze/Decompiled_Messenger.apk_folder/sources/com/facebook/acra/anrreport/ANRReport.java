package com.facebook.acra.anrreport;

import X.AnonymousClass08S;
import X.C010708t;
import android.content.Context;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.FileGenerator;
import com.facebook.acra.PerformanceMarker;
import com.facebook.acra.anr.ANRDataProvider;
import com.facebook.acra.anr.IANRReport;
import com.facebook.acra.constants.ErrorReportingConstants;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ANRReport implements IANRReport {
    private static final String LOG_TAG = "ANRReport";
    private static final int MAXIMUM_NUMBER_OF_OTHER_PROCESSES_TO_REPORT = 5;
    public ANRDataProvider mANRDataProvider;
    private final Map mANRProcessErrorProperties = new HashMap();
    private Context mContext;
    private int mCurrentAnrProcessStateIndex;
    private final ErrorReporter mErrorReporter;
    public final UUIDFileGenerator mFileGenerator;
    private int mMaxUsedAnrProcessStateIndex;
    public PerformanceMarker mPerformanceMarker;
    private File mTracesFile;

    public class UUIDFileGenerator implements FileGenerator {
        private final Context mContext;
        private final String mDirectory;
        private final String mExtension;

        public File generate() {
            return new File(this.mContext.getDir(this.mDirectory, 0), AnonymousClass08S.A0J(UUID.randomUUID().toString(), this.mExtension));
        }

        public UUIDFileGenerator(Context context, String str, String str2) {
            this.mContext = context;
            this.mExtension = str;
            this.mDirectory = str2;
        }
    }

    private long convertRawBytesToLong(byte[] bArr) {
        long j = 0;
        int i = 0;
        for (int i2 = 0; i2 < 8; i2++) {
            j += (((long) bArr[i2]) & 255) << i;
            i += 8;
        }
        return j;
    }

    private static boolean deleteFile(File file) {
        if (file == null) {
            return true;
        }
        boolean delete = file.delete();
        if (!delete && !file.exists()) {
            delete = true;
        }
        if (!delete) {
            C010708t.A0P(LOG_TAG, "Could not delete error report: %s", file.getName());
        }
        return delete;
    }

    private void addProcessErrorPropertiesToErrorReport() {
        boolean z;
        synchronized (this.mANRProcessErrorProperties) {
            try {
                z = this.mErrorReporter.addToAnrInProgressUpdateFile(this.mANRProcessErrorProperties);
            } catch (IOException unused) {
                z = false;
            }
            if (z) {
                this.mANRProcessErrorProperties.clear();
            }
        }
    }

    private void initializeProcessErrorPropertiesOnErrorReport() {
        synchronized (this.mANRProcessErrorProperties) {
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTED, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTED));
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_FAILURE_TIME, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_FAILURE_TIME));
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_FAILURE_CAUSE, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_FAILURE_CAUSE));
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_SYSTEM_ERROR_MSG, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_SYSTEM_ERROR_MSG));
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_SYSTEM_TAG, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_SYSTEM_TAG));
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_START_TIME, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_START_TIME));
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_MAIN_THREAD_UNBLOCKED_UPTIME, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_MAIN_THREAD_UNBLOCKED_UPTIME));
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_AM_CONFIRMATION_EXPIRED_UPTIME, (String) this.mANRProcessErrorProperties.get(ErrorReportingConstants.ANR_AM_CONFIRMATION_EXPIRED_UPTIME));
            for (int i = 1; i <= this.mMaxUsedAnrProcessStateIndex; i++) {
                ErrorReporter.putCustomData(AnonymousClass08S.A09(ErrorReportingConstants.ANR_OTHER_PROCESS_ERROR_PREFIX, i), (String) this.mANRProcessErrorProperties.get(AnonymousClass08S.A09(ErrorReportingConstants.ANR_OTHER_PROCESS_ERROR_PREFIX, i)));
            }
            this.mANRProcessErrorProperties.clear();
        }
    }

    private static void purgeDirectory(File file) {
        if (file != null && file.listFiles() != null) {
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory()) {
                    purgeDirectory(file2);
                }
                deleteFile(file2);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:15|16|17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0043, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0048, code lost:
        X.C010708t.A0P(com.facebook.acra.anrreport.ANRReport.LOG_TAG, "Could not read from file %s", r7.getName());
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0047 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.SortedMap recordSigquitTimes(java.io.File r10) {
        /*
            r9 = this;
            java.util.TreeMap r6 = new java.util.TreeMap
            r6.<init>()
            java.io.File[] r5 = r10.listFiles()
            int r4 = r5.length
            r3 = 0
        L_0x000b:
            if (r3 >= r4) goto L_0x005a
            r7 = r5[r3]
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0048 }
            r2.<init>(r7)     // Catch:{ IOException -> 0x0048 }
            r8 = 8
            byte[] r1 = new byte[r8]     // Catch:{ all -> 0x0041 }
            int r0 = r2.read(r1)     // Catch:{ all -> 0x0041 }
            if (r0 == r8) goto L_0x002e
            java.lang.String r8 = "ANRReport"
            java.lang.String r1 = "Corrupted file %s"
            java.lang.String r0 = r7.getName()     // Catch:{ all -> 0x0041 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x0041 }
            X.C010708t.A0P(r8, r1, r0)     // Catch:{ all -> 0x0041 }
            goto L_0x003d
        L_0x002e:
            long r0 = r9.convertRawBytesToLong(r1)     // Catch:{ all -> 0x0041 }
            java.lang.Long r1 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = r7.getName()     // Catch:{ all -> 0x0041 }
            r6.put(r1, r0)     // Catch:{ all -> 0x0041 }
        L_0x003d:
            r2.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0057
        L_0x0041:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0047 }
        L_0x0047:
            throw r0     // Catch:{ IOException -> 0x0048 }
        L_0x0048:
            java.lang.String r2 = "ANRReport"
            java.lang.String r0 = r7.getName()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Could not read from file %s"
            X.C010708t.A0P(r2, r0, r1)
        L_0x0057:
            int r3 = r3 + 1
            goto L_0x000b
        L_0x005a:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anrreport.ANRReport.recordSigquitTimes(java.io.File):java.util.SortedMap");
    }

    public void finalizeAndTryToSendReport(long j) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.clear();
            this.mCurrentAnrProcessStateIndex = 1;
        }
        ErrorReporter.putCustomData(ErrorReportingConstants.ANR_RECOVERY_DELAY_TAG, String.valueOf(j));
        ANRDataProvider aNRDataProvider = this.mANRDataProvider;
        if (aNRDataProvider == null || aNRDataProvider.shouldANRDetectorRun()) {
            this.mErrorReporter.prepareReports(Integer.MAX_VALUE, null, true, ErrorReporter.CrashReportType.CACHED_ANR_REPORT);
        } else {
            purgeDirectory(this.mContext.getDir(ErrorReporter.SIGQUIT_DIR, 0));
        }
    }

    public void logAmExpiration(long j) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_AM_CONFIRMATION_EXPIRED_UPTIME, Long.toString(j));
            addProcessErrorPropertiesToErrorReport();
        }
    }

    public void logExtraSigquit(long j) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_EXTRA_SIGQUIT_UPTIME, Long.toString(j));
            addProcessErrorPropertiesToErrorReport();
        }
    }

    public void logMainThreadUnblocked(long j) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_MAIN_THREAD_UNBLOCKED_UPTIME, Long.toString(j));
            addProcessErrorPropertiesToErrorReport();
        }
    }

    public void logOtherProcessAnr(String str, String str2, String str3, long j) {
        synchronized (this.mANRProcessErrorProperties) {
            int i = this.mCurrentAnrProcessStateIndex;
            if (i < 5) {
                this.mANRProcessErrorProperties.put(AnonymousClass08S.A09(ErrorReportingConstants.ANR_OTHER_PROCESS_ERROR_PREFIX, i), str + ',' + j + ',' + str2 + ',' + str3);
                addProcessErrorPropertiesToErrorReport();
                int i2 = this.mCurrentAnrProcessStateIndex;
                if (i2 > this.mMaxUsedAnrProcessStateIndex) {
                    this.mMaxUsedAnrProcessStateIndex = i2;
                }
                this.mCurrentAnrProcessStateIndex = i2 + 1;
            }
        }
    }

    public void logProcessMonitorFailure(long j, int i) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_FAILURE_TIME, Long.toString(j));
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_FAILURE_CAUSE, Integer.toString(i));
            addProcessErrorPropertiesToErrorReport();
        }
    }

    public void logProcessMonitorStart(long j) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTION_START_TIME, Long.toString(j));
            this.mCurrentAnrProcessStateIndex = 1;
            addProcessErrorPropertiesToErrorReport();
        }
    }

    public void logSigquitData(String str, String str2, long j) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_JAVA_CALLBACK_UPTIME, Long.toString(j));
            if (!(str == null && str2 == null)) {
                try {
                    this.mErrorReporter.amendANRReportWithSigquitData(str, str2);
                    this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_WITH_SIGQUIT_TRACES, "1");
                } catch (IOException e) {
                    C010708t.A0L(LOG_TAG, "Failed to save SIGQUIT", e);
                }
            }
            addProcessErrorPropertiesToErrorReport();
        }
    }

    public void logSystemInfo(String str, String str2, long j) {
        synchronized (this.mANRProcessErrorProperties) {
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_PROCESS_ERROR_DETECTED, Long.toString(j));
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_SYSTEM_ERROR_MSG, str);
            this.mANRProcessErrorProperties.put(ErrorReportingConstants.ANR_SYSTEM_TAG, str2);
            addProcessErrorPropertiesToErrorReport();
        }
    }

    public ANRReport(Context context, ErrorReporter errorReporter) {
        this.mContext = context;
        this.mErrorReporter = errorReporter;
        this.mFileGenerator = new UUIDFileGenerator(context, ".cachedreport", ErrorReporter.SIGQUIT_DIR);
        this.mCurrentAnrProcessStateIndex = 1;
        this.mMaxUsedAnrProcessStateIndex = 0;
    }

    public FileGenerator getFileGenerator() {
        return this.mFileGenerator;
    }

    public void setANRDataProvider(ANRDataProvider aNRDataProvider) {
        this.mANRDataProvider = aNRDataProvider;
    }

    public void setPerformanceMarker(PerformanceMarker performanceMarker) {
        this.mPerformanceMarker = performanceMarker;
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0, types: [java.io.OutputStream, java.lang.String] */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void startReport(boolean r7, java.lang.String r8, java.lang.String r9, int r10, boolean r11, boolean r12, long r13, long r15, long r17, long r19, java.lang.String r21, java.lang.String r22, java.io.File r23, java.lang.String r24, java.lang.Long r25) {
        /*
            r6 = this;
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter
            long r0 = r0.getAppStartTickTimeMs()
            long r2 = r13 - r0
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter
            long r0 = r0.getAppStartTickTimeMs()
            long r15 = r15 - r0
            com.facebook.acra.PerformanceMarker r0 = r6.mPerformanceMarker
            if (r0 == 0) goto L_0x0016
            r0.markerStart()
        L_0x0016:
            com.facebook.acra.anr.ANRDataProvider r0 = r6.mANRDataProvider
            if (r0 == 0) goto L_0x002c
            r0.provideStats()
            com.facebook.acra.anr.ANRDataProvider r0 = r6.mANRDataProvider
            r0.provideLooperProfileInfo()
            com.facebook.acra.anr.ANRDataProvider r0 = r6.mANRDataProvider
            r0.provideDexStatus()
            com.facebook.acra.anr.ANRDataProvider r0 = r6.mANRDataProvider
            r0.provideLooperMonitorInfo()
        L_0x002c:
            r6.initializeProcessErrorPropertiesOnErrorReport()
            java.lang.String r1 = java.lang.String.valueOf(r13)
            java.lang.String r0 = "anr_detected_uptime"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            java.lang.String r1 = java.lang.String.valueOf(r2)
            java.lang.String r0 = "anr_detect_time_tag"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            java.lang.String r1 = "anr_recovery_delay"
            java.lang.String r0 = "-1"
            com.facebook.acra.ErrorReporter.putCustomData(r1, r0)
            java.lang.String r1 = java.lang.String.valueOf(r7)
            java.lang.String r0 = "anr_detected_pre_gkstore"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            java.lang.String r1 = java.lang.String.valueOf(r10)
            java.lang.String r0 = "anr_detector_id"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            java.lang.String r1 = java.lang.String.valueOf(r15)
            java.lang.String r0 = "anr_detector_start_time"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            java.lang.String r1 = java.lang.String.valueOf(r11)
            java.lang.String r0 = "anr_started_in_foreground"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            java.lang.String r1 = java.lang.String.valueOf(r12)
            java.lang.String r0 = "anr_started_in_foreground_v2"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            java.lang.String r1 = java.lang.String.valueOf(r25)
            java.lang.String r0 = "anr_java_callback_uptime"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            r2 = 0
            int r0 = (r17 > r2 ? 1 : (r17 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0095
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter
            long r0 = r0.getAppStartTickTimeMs()
            long r17 = r17 - r0
            java.lang.String r1 = java.lang.String.valueOf(r17)
            java.lang.String r0 = "anr_detector_actual_start_time"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
        L_0x0095:
            int r0 = (r19 > r2 ? 1 : (r19 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00aa
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter
            long r0 = r0.getAppStartTickTimeMs()
            long r19 = r19 - r0
            java.lang.String r1 = java.lang.String.valueOf(r19)
            java.lang.String r0 = "anr_detector_switch_time"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
        L_0x00aa:
            java.lang.String r0 = "black_box_trace"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r8)
            java.lang.String r0 = "long_stall_trace"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r9)
            java.lang.String r1 = com.facebook.acra.asyncbroadcastreceiver.AsyncBroadcastReceiverObserver.blameActiveReceivers()
            java.lang.String r0 = "anr_async_broadcast_receivers"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            r3 = 0
            java.lang.String r0 = "first_sigquit"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r3)
            java.lang.String r0 = "sigquit_times"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r3)
            r0 = r23
            if (r23 == 0) goto L_0x0127
            java.util.SortedMap r0 = r6.recordSigquitTimes(r0)
            r1 = 1
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r4 = r0.iterator()
        L_0x00de:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x011e
            java.lang.Object r2 = r4.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            if (r1 == 0) goto L_0x0118
            java.lang.Object r0 = r2.getValue()
            java.lang.String r0 = (java.lang.String) r0
            r1 = r24
            boolean r0 = r0.equals(r1)
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "first_sigquit"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            r1 = 0
        L_0x0102:
            java.lang.Object r0 = r2.getValue()
            java.lang.String r0 = (java.lang.String) r0
            r5.append(r0)
            java.lang.String r0 = ","
            r5.append(r0)
            java.lang.Object r0 = r2.getKey()
            r5.append(r0)
            goto L_0x00de
        L_0x0118:
            java.lang.String r0 = ","
            r5.append(r0)
            goto L_0x0102
        L_0x011e:
            java.lang.String r1 = r5.toString()
            java.lang.String r0 = "sigquit_times"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
        L_0x0127:
            java.lang.String r0 = "should_dedup_disk_persistence_gk_cached"
            boolean r5 = com.facebook.acra.ACRA.getFlagValue(r0)
            r4 = r22
            if (r22 != 0) goto L_0x01b4
            if (r5 == 0) goto L_0x0139
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x01eb }
            r1.<init>()     // Catch:{ IOException -> 0x01eb }
            goto L_0x0155
        L_0x0139:
            java.io.File r0 = r6.mTracesFile     // Catch:{ IOException -> 0x01eb }
            if (r0 != 0) goto L_0x014e
            com.facebook.acra.anrreport.ANRReport$UUIDFileGenerator r4 = new com.facebook.acra.anrreport.ANRReport$UUIDFileGenerator     // Catch:{ IOException -> 0x01eb }
            android.content.Context r2 = r6.mContext     // Catch:{ IOException -> 0x01eb }
            java.lang.String r1 = ".stacktrace"
            java.lang.String r0 = "traces"
            r4.<init>(r2, r1, r0)     // Catch:{ IOException -> 0x01eb }
            java.io.File r0 = r4.generate()     // Catch:{ IOException -> 0x01eb }
            r6.mTracesFile = r0     // Catch:{ IOException -> 0x01eb }
        L_0x014e:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x01eb }
            java.io.File r0 = r6.mTracesFile     // Catch:{ IOException -> 0x01eb }
            r1.<init>(r0)     // Catch:{ IOException -> 0x01eb }
        L_0x0155:
            r3 = r1
            r2 = r21
            if (r21 == 0) goto L_0x0181
            java.lang.String r1 = "anr_with_sigquit_traces"
            java.lang.String r0 = "1"
            com.facebook.acra.ErrorReporter.putCustomData(r1, r0)     // Catch:{ IOException -> 0x01eb }
            java.io.PrintWriter r1 = new java.io.PrintWriter     // Catch:{ IOException -> 0x01eb }
            r1.<init>(r3)     // Catch:{ IOException -> 0x01eb }
            if (r5 != 0) goto L_0x017a
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter     // Catch:{ IOException -> 0x01eb }
            java.lang.String r0 = r0.getAppVersionCode()     // Catch:{ IOException -> 0x01eb }
            r1.println(r0)     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter     // Catch:{ IOException -> 0x01eb }
            java.lang.String r0 = r0.getAppVersionName()     // Catch:{ IOException -> 0x01eb }
            r1.println(r0)     // Catch:{ IOException -> 0x01eb }
        L_0x017a:
            r1.write(r2)     // Catch:{ IOException -> 0x01eb }
            r1.flush()     // Catch:{ IOException -> 0x01eb }
            goto L_0x019d
        L_0x0181:
            java.lang.String r1 = "anr_with_sigquit_traces"
            java.lang.String r0 = "0"
            com.facebook.acra.ErrorReporter.putCustomData(r1, r0)     // Catch:{ IOException -> 0x01eb }
            if (r5 != 0) goto L_0x019a
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter     // Catch:{ IOException -> 0x01eb }
            java.lang.String r1 = r0.getAppVersionCode()     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.ErrorReporter r0 = r6.mErrorReporter     // Catch:{ IOException -> 0x01eb }
            java.lang.String r0 = r0.getAppVersionName()     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.StackTraceDumper.dumpStackTraces(r3, r1, r0)     // Catch:{ IOException -> 0x01eb }
            goto L_0x019d
        L_0x019a:
            com.facebook.acra.StackTraceDumper.dumpStackTraces(r3)     // Catch:{ IOException -> 0x01eb }
        L_0x019d:
            if (r5 == 0) goto L_0x01a0
            goto L_0x01c8
        L_0x01a0:
            java.io.File r0 = r6.mTracesFile     // Catch:{ IOException -> 0x01eb }
            r0.getCanonicalPath()     // Catch:{ IOException -> 0x01eb }
            java.io.File r0 = r6.mTracesFile     // Catch:{ IOException -> 0x01eb }
            r0.length()     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.ErrorReporter r2 = r6.mErrorReporter     // Catch:{ IOException -> 0x01eb }
            java.io.File r1 = r6.mTracesFile     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.anrreport.ANRReport$UUIDFileGenerator r0 = r6.mFileGenerator     // Catch:{ IOException -> 0x01eb }
            r2.prepareANRReport(r1, r0)     // Catch:{ IOException -> 0x01eb }
            goto L_0x01d3
        L_0x01b4:
            java.lang.String r1 = "anr_with_sigquit_traces"
            java.lang.String r0 = "1"
            com.facebook.acra.ErrorReporter.putCustomData(r1, r0)     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.ErrorReporter r2 = r6.mErrorReporter     // Catch:{ IOException -> 0x01eb }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x01eb }
            r1.<init>(r4)     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.anrreport.ANRReport$UUIDFileGenerator r0 = r6.mFileGenerator     // Catch:{ IOException -> 0x01eb }
            r2.prepareANRReport(r1, r0)     // Catch:{ IOException -> 0x01eb }
            goto L_0x01d3
        L_0x01c8:
            com.facebook.acra.ErrorReporter r2 = r6.mErrorReporter     // Catch:{ IOException -> 0x01eb }
            java.lang.String r1 = r3.toString()     // Catch:{ IOException -> 0x01eb }
            com.facebook.acra.anrreport.ANRReport$UUIDFileGenerator r0 = r6.mFileGenerator     // Catch:{ IOException -> 0x01eb }
            r2.prepareANRReport(r1, r0)     // Catch:{ IOException -> 0x01eb }
        L_0x01d3:
            com.facebook.acra.PerformanceMarker r1 = r6.mPerformanceMarker     // Catch:{ IOException -> 0x01eb }
            if (r1 == 0) goto L_0x01db
            r0 = 2
            r1.markerEnd(r0)     // Catch:{ IOException -> 0x01eb }
        L_0x01db:
            if (r3 == 0) goto L_0x01e0
            r3.close()
        L_0x01e0:
            java.util.Map r1 = r6.mANRProcessErrorProperties
            monitor-enter(r1)
            r6.addProcessErrorPropertiesToErrorReport()     // Catch:{ all -> 0x01e8 }
            monitor-exit(r1)     // Catch:{ all -> 0x01e8 }
            return
        L_0x01e8:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01e8 }
            goto L_0x01fb
        L_0x01eb:
            r2 = move-exception
            com.facebook.acra.PerformanceMarker r1 = r6.mPerformanceMarker     // Catch:{ all -> 0x01f5 }
            if (r1 == 0) goto L_0x01f4
            r0 = 3
            r1.markerEnd(r0)     // Catch:{ all -> 0x01f5 }
        L_0x01f4:
            throw r2     // Catch:{ all -> 0x01f5 }
        L_0x01f5:
            r0 = move-exception
            if (r3 == 0) goto L_0x01fb
            r3.close()
        L_0x01fb:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.anrreport.ANRReport.startReport(boolean, java.lang.String, java.lang.String, int, boolean, boolean, long, long, long, long, java.lang.String, java.lang.String, java.io.File, java.lang.String, java.lang.Long):void");
    }
}
