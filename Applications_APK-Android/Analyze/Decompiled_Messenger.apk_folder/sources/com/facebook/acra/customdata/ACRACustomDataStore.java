package com.facebook.acra.customdata;

import java.util.Formatter;
import java.util.Map;
import java.util.TreeMap;

public class ACRACustomDataStore implements CustomDataStore {
    private final Map mInstanceCustomParameters = new TreeMap();

    public boolean containsKey(String str) {
        return this.mInstanceCustomParameters.containsKey(str);
    }

    public String getCustomData(String str) {
        return (String) this.mInstanceCustomParameters.get(str);
    }

    public Map getSnapshot() {
        return new TreeMap(this.mInstanceCustomParameters);
    }

    public void removeCustomData(String str) {
        if (str != null) {
            this.mInstanceCustomParameters.remove(str);
        }
    }

    public void setCustomData(String str, String str2, Object... objArr) {
        if (str == null) {
            return;
        }
        if (str2 != null) {
            if (objArr.length > 0) {
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb);
                formatter.format(str2, objArr);
                formatter.close();
                str2 = sb.toString();
            }
            this.mInstanceCustomParameters.put(str, str2);
            return;
        }
        removeCustomData(str);
    }
}
