package com.facebook.acra.customdata;

import java.util.Map;

public class ProxyCustomDataStore implements CustomDataStore {
    private static CustomDataStore dataStore = new ACRACustomDataStore();

    public synchronized boolean containsKey(String str) {
        return dataStore.containsKey(str);
    }

    public synchronized String getCustomData(String str) {
        return dataStore.getCustomData(str);
    }

    public synchronized Map getSnapshot() {
        return dataStore.getSnapshot();
    }

    public synchronized void removeCustomData(String str) {
        dataStore.removeCustomData(str);
    }

    public synchronized void setCustomData(String str, String str2, Object... objArr) {
        dataStore.setCustomData(str, str2, objArr);
    }

    public synchronized void setDataStore(CustomDataStore customDataStore) {
        for (Map.Entry entry : dataStore.getSnapshot().entrySet()) {
            customDataStore.setCustomData((String) entry.getKey(), (String) entry.getValue(), new Object[0]);
        }
        dataStore = customDataStore;
    }

    public class Holder {
        public static final ProxyCustomDataStore CUSTOM_DATA = new ProxyCustomDataStore();

        private Holder() {
        }
    }

    public static ProxyCustomDataStore getInstance() {
        return Holder.CUSTOM_DATA;
    }
}
