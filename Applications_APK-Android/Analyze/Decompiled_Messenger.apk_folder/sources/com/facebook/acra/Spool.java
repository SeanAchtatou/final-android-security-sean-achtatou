package com.facebook.acra;

import java.io.Closeable;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.OverlappingFileLockException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class Spool {
    private static final String TAG = "Spool";
    private final File mDirectoryName;
    public final HashSet mHazardList = new HashSet();

    public final class FileBeingConsumed implements Closeable {
        public final RandomAccessFile file;
        public final File fileName;

        public FileBeingConsumed(File file2, RandomAccessFile randomAccessFile) {
            this.fileName = file2;
            this.file = randomAccessFile;
        }

        public void close() {
            synchronized (Spool.this) {
                Spool.this.mHazardList.remove(this.fileName);
            }
            Spool.this.closeSilently(this.file);
        }
    }

    public final class FileBeingProduced implements Closeable {
        public final RandomAccessFile file;
        public final File fileName;

        public FileBeingProduced(File file2, RandomAccessFile randomAccessFile) {
            this.fileName = file2;
            this.file = randomAccessFile;
        }

        public void close() {
            synchronized (Spool.this) {
                Spool.this.mHazardList.remove(this.fileName);
            }
            this.file.close();
        }
    }

    public final class Snapshot implements Iterator, Closeable {
        private FileBeingConsumed mCurrent = null;
        public final Descriptor[] mDescriptors;
        private int mPosition = 0;

        public Snapshot(Descriptor[] descriptorArr) {
            this.mDescriptors = descriptorArr;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r7 = new java.io.RandomAccessFile(r3, "rw");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
            if (r10.this$0.tryLock(r7) != false) goto L_0x0044;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x002f, code lost:
            r10.this$0.closeSilently(r7);
            r1 = r10.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0036, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
            r10.this$0.mHazardList.remove(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x003e, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0040, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0048, code lost:
            if (r3.exists() != false) goto L_0x005f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x004a, code lost:
            r10.this$0.closeSilently(r7);
            r1 = r10.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0051, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
            r10.this$0.mHazardList.remove(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0059, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x005a, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x005b, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0067, code lost:
            if (r7.length() != 0) goto L_0x0080;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0069, code lost:
            r3.delete();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x006c, code lost:
            r10.this$0.closeSilently(r7);
            r1 = r10.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0073, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
            r10.this$0.mHazardList.remove(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x007b, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x007c, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x007d, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
            r0 = r10.this$0;
            r1 = new com.facebook.acra.Spool.FileBeingConsumed(r0, r3, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x0087, code lost:
            r0.closeSilently(null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x008a, code lost:
            return r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x008b, code lost:
            r5 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x0094, code lost:
            r2 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x0095, code lost:
            r7 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x0097, code lost:
            r5 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x0098, code lost:
            r7 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x00b8, code lost:
            monitor-enter(r10.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
            r10.this$0.mHazardList.remove(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x00c1, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x00c2, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x00c5, code lost:
            return null;
         */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x00b6  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x00c5 A[Catch:{ all -> 0x00c2 }, RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x00d2  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private com.facebook.acra.Spool.FileBeingConsumed tryOpenFileForConsumption(com.facebook.acra.Spool.Descriptor r11) {
            /*
                r10 = this;
                java.io.File r3 = r11.fileName
                r4 = 1
                r6 = 0
                com.facebook.acra.Spool r1 = com.facebook.acra.Spool.this     // Catch:{ IOException -> 0x009a, all -> 0x00c8 }
                monitor-enter(r1)     // Catch:{ IOException -> 0x009a, all -> 0x00c8 }
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ all -> 0x008d }
                java.util.HashSet r0 = r0.mHazardList     // Catch:{ all -> 0x008d }
                boolean r0 = r0.contains(r3)     // Catch:{ all -> 0x008d }
                if (r0 == 0) goto L_0x0018
                monitor-exit(r1)     // Catch:{ all -> 0x008d }
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this
                r0.closeSilently(r6)
                return r6
            L_0x0018:
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ all -> 0x008d }
                java.util.HashSet r0 = r0.mHazardList     // Catch:{ all -> 0x008d }
                r0.add(r3)     // Catch:{ all -> 0x008d }
                monitor-exit(r1)     // Catch:{ all -> 0x0091 }
                java.io.RandomAccessFile r7 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0097, all -> 0x0094 }
                java.lang.String r0 = "rw"
                r7.<init>(r3, r0)     // Catch:{ IOException -> 0x0097, all -> 0x0094 }
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ IOException -> 0x008b }
                boolean r0 = r0.tryLock(r7)     // Catch:{ IOException -> 0x008b }
                if (r0 != 0) goto L_0x0044
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this
                r0.closeSilently(r7)
                com.facebook.acra.Spool r1 = com.facebook.acra.Spool.this
                monitor-enter(r1)
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ all -> 0x0040 }
                java.util.HashSet r0 = r0.mHazardList     // Catch:{ all -> 0x0040 }
                r0.remove(r3)     // Catch:{ all -> 0x0040 }
                monitor-exit(r1)     // Catch:{ all -> 0x0040 }
                return r6
            L_0x0040:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0040 }
                goto L_0x00e0
            L_0x0044:
                boolean r0 = r3.exists()     // Catch:{ IOException -> 0x008b }
                if (r0 != 0) goto L_0x005f
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this
                r0.closeSilently(r7)
                com.facebook.acra.Spool r1 = com.facebook.acra.Spool.this
                monitor-enter(r1)
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ all -> 0x005b }
                java.util.HashSet r0 = r0.mHazardList     // Catch:{ all -> 0x005b }
                r0.remove(r3)     // Catch:{ all -> 0x005b }
                monitor-exit(r1)     // Catch:{ all -> 0x005b }
                return r6
            L_0x005b:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x005b }
                goto L_0x00e0
            L_0x005f:
                long r8 = r7.length()     // Catch:{ IOException -> 0x008b }
                r1 = 0
                int r0 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
                if (r0 != 0) goto L_0x0080
                r3.delete()     // Catch:{ IOException -> 0x008b }
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this
                r0.closeSilently(r7)
                com.facebook.acra.Spool r1 = com.facebook.acra.Spool.this
                monitor-enter(r1)
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ all -> 0x007d }
                java.util.HashSet r0 = r0.mHazardList     // Catch:{ all -> 0x007d }
                r0.remove(r3)     // Catch:{ all -> 0x007d }
                monitor-exit(r1)     // Catch:{ all -> 0x007d }
                return r6
            L_0x007d:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x007d }
                goto L_0x00e0
            L_0x0080:
                com.facebook.acra.Spool$FileBeingConsumed r1 = new com.facebook.acra.Spool$FileBeingConsumed     // Catch:{ IOException -> 0x008b }
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ IOException -> 0x008b }
                r1.<init>(r3, r7)     // Catch:{ IOException -> 0x008b }
                r0.closeSilently(r6)
                return r1
            L_0x008b:
                r5 = move-exception
                goto L_0x009d
            L_0x008d:
                r0 = move-exception
                r4 = 0
            L_0x008f:
                monitor-exit(r1)     // Catch:{ all -> 0x0091 }
                goto L_0x0093
            L_0x0091:
                r0 = move-exception
                goto L_0x008f
            L_0x0093:
                throw r0     // Catch:{ IOException -> 0x0097, all -> 0x0094 }
            L_0x0094:
                r2 = move-exception
                r7 = r6
                goto L_0x00cb
            L_0x0097:
                r5 = move-exception
                r7 = r6
                goto L_0x009d
            L_0x009a:
                r5 = move-exception
                r7 = r6
                r4 = 0
            L_0x009d:
                java.lang.String r2 = "Spool"
                java.lang.String r1 = "unexpected failure opening %s: deleting and continuing scan"
                java.lang.String r0 = r3.getName()     // Catch:{ all -> 0x00c6 }
                java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00c6 }
                X.C010708t.A0V(r2, r5, r1, r0)     // Catch:{ all -> 0x00c6 }
                r3.delete()     // Catch:{ all -> 0x00c6 }
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this
                r0.closeSilently(r7)
                if (r4 == 0) goto L_0x00c5
                com.facebook.acra.Spool r1 = com.facebook.acra.Spool.this
                monitor-enter(r1)
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ all -> 0x00c2 }
                java.util.HashSet r0 = r0.mHazardList     // Catch:{ all -> 0x00c2 }
                r0.remove(r3)     // Catch:{ all -> 0x00c2 }
                monitor-exit(r1)     // Catch:{ all -> 0x00c2 }
                return r6
            L_0x00c2:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00c2 }
                goto L_0x00e0
            L_0x00c5:
                return r6
            L_0x00c6:
                r2 = move-exception
                goto L_0x00cb
            L_0x00c8:
                r2 = move-exception
                r7 = r6
                r4 = 0
            L_0x00cb:
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this
                r0.closeSilently(r7)
                if (r4 == 0) goto L_0x00e1
                com.facebook.acra.Spool r1 = com.facebook.acra.Spool.this
                monitor-enter(r1)
                com.facebook.acra.Spool r0 = com.facebook.acra.Spool.this     // Catch:{ all -> 0x00de }
                java.util.HashSet r0 = r0.mHazardList     // Catch:{ all -> 0x00de }
                r0.remove(r3)     // Catch:{ all -> 0x00de }
                monitor-exit(r1)     // Catch:{ all -> 0x00de }
                goto L_0x00e1
            L_0x00de:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00de }
            L_0x00e0:
                throw r0
            L_0x00e1:
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.Spool.Snapshot.tryOpenFileForConsumption(com.facebook.acra.Spool$Descriptor):com.facebook.acra.Spool$FileBeingConsumed");
        }

        private void updateCurrent() {
            while (this.mCurrent == null) {
                int i = this.mPosition;
                Descriptor[] descriptorArr = this.mDescriptors;
                if (i < descriptorArr.length) {
                    this.mPosition = i + 1;
                    this.mCurrent = tryOpenFileForConsumption(descriptorArr[i]);
                } else {
                    return;
                }
            }
        }

        public void close() {
            Spool.this.closeSilently(this.mCurrent);
        }

        public int getEstimate() {
            return this.mDescriptors.length;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public boolean hasNext() {
            updateCurrent();
            if (this.mCurrent != null) {
                return true;
            }
            return false;
        }

        public FileBeingConsumed next() {
            updateCurrent();
            FileBeingConsumed fileBeingConsumed = this.mCurrent;
            if (fileBeingConsumed != null) {
                this.mCurrent = null;
                return fileBeingConsumed;
            }
            throw new NoSuchElementException();
        }
    }

    public FileBeingProduced produce(String str) {
        return produceWithDonorFile(str, null);
    }

    public boolean tryLock(RandomAccessFile randomAccessFile) {
        try {
            return randomAccessFile.getChannel().tryLock(0, Long.MAX_VALUE, false) != null;
        } catch (OverlappingFileLockException unused) {
            return false;
        } catch (IOException e) {
            String message = e.getMessage();
            if (message != null && (message.contains(": EAGAIN (") || message.contains(": errno 11 ("))) {
                return false;
            }
            throw e;
        }
    }

    public final class Descriptor {
        public final String fileBaseName;
        public final File fileName;
        public final long lastModifiedTime;

        public Descriptor(String str, long j, File file) {
            this.fileBaseName = str;
            this.lastModifiedTime = j;
            this.fileName = file;
        }
    }

    public void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x00d0, code lost:
        r4 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x00d2, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x00d3, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x00dc, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x00dd, code lost:
        if (r4 != null) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x00df, code lost:
        r4.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x00e2, code lost:
        closeSilently(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x00e5, code lost:
        if (r3 != null) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x00e7, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:?, code lost:
        r7.mHazardList.remove(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x00ef, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x00f1, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0038, code lost:
        if (r3.exists() == false) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003a, code lost:
        closeSilently(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003d, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r7.mHazardList.remove(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0043, code lost:
        monitor-exit(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0044, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0045, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0049, code lost:
        if (r9 == null) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004f, code lost:
        if (r9.exists() == false) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0051, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r6 = new java.io.RandomAccessFile(r9, "rw");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x005d, code lost:
        if (tryLock(r6) == false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0063, code lost:
        if (r9.renameTo(r3) == false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0065, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0066, code lost:
        if (r1 != false) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0069, code lost:
        r5 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x006b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x006c, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x006e, code lost:
        r5 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x006f, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        X.C010708t.A0V(com.facebook.acra.Spool.TAG, r5, "error using donor file %s; falling back to regular path", r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        closeSilently(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x007f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        closeSilently(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0084, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0085, code lost:
        if (r6 != null) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r2 = new java.io.RandomAccessFile(r3, "rw");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0092, code lost:
        if (tryLock(r2) == false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0094, code lost:
        r3.delete();
        closeSilently(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x009a, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        r7.mHazardList.remove(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00a1, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00a2, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00a5, code lost:
        r6 = r2;
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00a8, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00a9, code lost:
        r6 = r2;
        r4 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x00ac, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x00b1, code lost:
        if (r3.exists() != false) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00b3, code lost:
        if (r2 != null) goto L_0x00b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x00b5, code lost:
        r2.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x00b8, code lost:
        closeSilently(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x00bb, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r7.mHazardList.remove(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x00c2, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x00c3, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        r0 = new com.facebook.acra.Spool.FileBeingProduced(r7, r3, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x00cb, code lost:
        closeSilently(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x00ce, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x00cf, code lost:
        r1 = th;
     */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0087 A[SYNTHETIC, Splitter:B:63:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x00b3 A[Catch:{ all -> 0x00cf }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x00c6 A[SYNTHETIC, Splitter:B:95:0x00c6] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:57:0x0080=Splitter:B:57:0x0080, B:54:0x007b=Splitter:B:54:0x007b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.acra.Spool.FileBeingProduced produceWithDonorFile(java.lang.String r8, java.io.File r9) {
        /*
            r7 = this;
            java.lang.String r0 = "/"
            boolean r0 = r8.contains(r0)
            if (r0 != 0) goto L_0x00f2
            java.lang.String r0 = "."
            boolean r0 = r0.equals(r8)
            if (r0 != 0) goto L_0x00f2
            java.lang.String r0 = ".."
            boolean r0 = r0.equals(r8)
            if (r0 != 0) goto L_0x00f2
            r4 = 0
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x00d8 }
            java.io.File r0 = r7.mDirectoryName     // Catch:{ all -> 0x00d8 }
            r3.<init>(r0, r8)     // Catch:{ all -> 0x00d8 }
            monitor-enter(r7)     // Catch:{ all -> 0x00d8 }
            java.util.HashSet r0 = r7.mHazardList     // Catch:{ all -> 0x00d5 }
            boolean r0 = r0.contains(r3)     // Catch:{ all -> 0x00d5 }
            if (r0 == 0) goto L_0x002e
            monitor-exit(r7)     // Catch:{ all -> 0x00d5 }
            r7.closeSilently(r4)
            return r4
        L_0x002e:
            java.util.HashSet r0 = r7.mHazardList     // Catch:{ all -> 0x00d5 }
            r0.add(r3)     // Catch:{ all -> 0x00d5 }
            monitor-exit(r7)     // Catch:{ all -> 0x00d5 }
            boolean r0 = r3.exists()     // Catch:{ all -> 0x00d2 }
            if (r0 == 0) goto L_0x0049
            r7.closeSilently(r4)
            monitor-enter(r7)
            java.util.HashSet r0 = r7.mHazardList     // Catch:{ all -> 0x0045 }
            r0.remove(r3)     // Catch:{ all -> 0x0045 }
            monitor-exit(r7)     // Catch:{ all -> 0x0045 }
            return r4
        L_0x0045:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0045 }
            goto L_0x00f1
        L_0x0049:
            if (r9 == 0) goto L_0x0084
            boolean r0 = r9.exists()     // Catch:{ all -> 0x00d2 }
            if (r0 == 0) goto L_0x0084
            r1 = 0
            java.io.RandomAccessFile r6 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            java.lang.String r0 = "rw"
            r6.<init>(r9, r0)     // Catch:{ Exception -> 0x006e, all -> 0x006b }
            boolean r0 = r7.tryLock(r6)     // Catch:{ Exception -> 0x0069 }
            if (r0 == 0) goto L_0x0066
            boolean r0 = r9.renameTo(r3)     // Catch:{ Exception -> 0x0069 }
            if (r0 == 0) goto L_0x0066
            r1 = 1
        L_0x0066:
            if (r1 != 0) goto L_0x0085
            goto L_0x007b
        L_0x0069:
            r5 = move-exception
            goto L_0x0070
        L_0x006b:
            r0 = move-exception
            r6 = r4
            goto L_0x0080
        L_0x006e:
            r5 = move-exception
            r6 = r4
        L_0x0070:
            java.lang.String r2 = "Spool"
            java.lang.String r1 = "error using donor file %s; falling back to regular path"
            java.lang.Object[] r0 = new java.lang.Object[]{r9}     // Catch:{ all -> 0x007f }
            X.C010708t.A0V(r2, r5, r1, r0)     // Catch:{ all -> 0x007f }
        L_0x007b:
            r7.closeSilently(r6)     // Catch:{ all -> 0x00dc }
            goto L_0x0084
        L_0x007f:
            r0 = move-exception
        L_0x0080:
            r7.closeSilently(r6)     // Catch:{ all -> 0x00dc }
            throw r0     // Catch:{ all -> 0x00d2 }
        L_0x0084:
            r6 = r4
        L_0x0085:
            if (r6 != 0) goto L_0x00ac
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ all -> 0x00dc }
            java.lang.String r0 = "rw"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x00dc }
            boolean r0 = r7.tryLock(r2)     // Catch:{ all -> 0x00a8 }
            if (r0 != 0) goto L_0x00a5
            r3.delete()
            r7.closeSilently(r2)
            monitor-enter(r7)
            java.util.HashSet r0 = r7.mHazardList     // Catch:{ all -> 0x00a2 }
            r0.remove(r3)     // Catch:{ all -> 0x00a2 }
            monitor-exit(r7)     // Catch:{ all -> 0x00a2 }
            return r4
        L_0x00a2:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00a2 }
            goto L_0x00f1
        L_0x00a5:
            r6 = r2
            r2 = r3
            goto L_0x00ad
        L_0x00a8:
            r1 = move-exception
            r6 = r2
            r4 = r3
            goto L_0x00dd
        L_0x00ac:
            r2 = r4
        L_0x00ad:
            boolean r0 = r3.exists()     // Catch:{ all -> 0x00cf }
            if (r0 != 0) goto L_0x00c6
            if (r2 == 0) goto L_0x00b8
            r2.delete()
        L_0x00b8:
            r7.closeSilently(r6)
            monitor-enter(r7)
            java.util.HashSet r0 = r7.mHazardList     // Catch:{ all -> 0x00c3 }
            r0.remove(r3)     // Catch:{ all -> 0x00c3 }
            monitor-exit(r7)     // Catch:{ all -> 0x00c3 }
            return r4
        L_0x00c3:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00c3 }
            goto L_0x00f1
        L_0x00c6:
            com.facebook.acra.Spool$FileBeingProduced r0 = new com.facebook.acra.Spool$FileBeingProduced     // Catch:{ all -> 0x00cf }
            r0.<init>(r3, r6)     // Catch:{ all -> 0x00cf }
            r7.closeSilently(r4)
            return r0
        L_0x00cf:
            r1 = move-exception
            r4 = r2
            goto L_0x00dd
        L_0x00d2:
            r1 = move-exception
            r6 = r4
            goto L_0x00dd
        L_0x00d5:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00d5 }
            throw r0     // Catch:{ all -> 0x00d8 }
        L_0x00d8:
            r1 = move-exception
            r3 = r4
            r6 = r4
            goto L_0x00dd
        L_0x00dc:
            r1 = move-exception
        L_0x00dd:
            if (r4 == 0) goto L_0x00e2
            r4.delete()
        L_0x00e2:
            r7.closeSilently(r6)
            if (r3 == 0) goto L_0x00f1
            monitor-enter(r7)
            java.util.HashSet r0 = r7.mHazardList     // Catch:{ all -> 0x00ef }
            r0.remove(r3)     // Catch:{ all -> 0x00ef }
            monitor-exit(r7)     // Catch:{ all -> 0x00ef }
            goto L_0x00f1
        L_0x00ef:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00ef }
        L_0x00f1:
            throw r1
        L_0x00f2:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "illegal spool file name: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r8)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.Spool.produceWithDonorFile(java.lang.String, java.io.File):com.facebook.acra.Spool$FileBeingProduced");
    }

    public Snapshot snapshot(Comparator comparator, FilenameFilter filenameFilter) {
        String[] list = this.mDirectoryName.list(filenameFilter);
        if (list == null) {
            list = new String[0];
        }
        int length = list.length;
        Descriptor[] descriptorArr = new Descriptor[length];
        for (int i = 0; i < length; i++) {
            String str = list[i];
            File file = new File(this.mDirectoryName, str);
            descriptorArr[i] = new Descriptor(str, file.lastModified(), file);
        }
        if (comparator != null) {
            Arrays.sort(descriptorArr, comparator);
        }
        return new Snapshot(descriptorArr);
    }

    public Spool(File file) {
        this.mDirectoryName = file;
    }
}
