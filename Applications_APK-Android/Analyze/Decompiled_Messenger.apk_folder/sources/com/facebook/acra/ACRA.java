package com.facebook.acra;

import X.AnonymousClass00M;
import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass01q;
import X.AnonymousClass08S;
import X.AnonymousClass08X;
import X.C010708t;
import X.C02120Da;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.ANRDetectorListener;
import com.facebook.acra.anr.AppStateUpdater;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anrreport.ANRReport;
import com.facebook.acra.config.AcraReportingConfig;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.acra.sender.FlexibleReportSender;
import com.facebook.acra.util.NativeProcFileReader;
import com.facebook.acra.util.NoSync;
import com.facebook.errorreporting.nightwatch.Nightwatch;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ACRA {
    private static final String ACRA_FLAGS_STORE = "acra_flags_store";
    private static final String ANDROID_ANR_DETECTOR_TO_USE = "android_anr_detector_to_use";
    private static final String ANR_EXPIRATION_TIMEOUT_ON_MAIN_THREAD_UNBLOCKED = "expiration_timeout_main_thread_unblocked";
    private static final String ANR_FOREGROUND_CHECK_PERIOD = "foreground_check_period";
    private static final String ANR_PROCESS_MONITOR_MAX_CHECKS_AFTER = "anr_process_monitor_max_checks_after";
    private static final String ANR_PROCESS_MONITOR_MAX_CHECKS_BEFORE = "anr_process_monitor_max_checks_before";
    private static final String ANR_RECOVERY_TIMEOUT = "anr_recovery_timeout";
    public static final String BREAKPAD_LIB_NAME = "breakpad_lib_name";
    private static final String ERROR_MONITOR_CHECK_INTERVAL = "error_monitor_check_interval";
    private static final String FORCE_NIGHTWATCH_PROPERTY_NAME = "com.facebook.force_nightwatch";
    public static final int HYBRID_ANR_DETECTOR = 4;
    public static final String IS_FIRST_RUN_AFTER_UPGRADE = "is_first_run_after_upgrade";
    public static final String LOGCAT_FILE_KEY = "logcatFileName";
    public static final String LOG_TAG = "ACRA";
    public static final int MULTI_SIGNAL_ANR_DETECTOR = 6;
    public static final int POST_TASK_BASED_ANR_DETECTOR = 2;
    public static final int PROCESS_ERROR_MONITOR_ANR_DETECTOR = 5;
    private static final String REPORT_HOST_FILE_NAME = "report_host.txt";
    private static final String RUN_ANR_DETECTOR_ON_BROWSER_PROCESS = "run_anr_detector_on_browser_process";
    public static final String SESSION_ID_KEY = "session_id";
    private static final String SHOULD_AVOID_MUTEX_ON_SIGNAL_HANDLER = "avoid_mutex_on_signal_handler";
    private static final String SHOULD_CLEANUP_ANR_STATE_ON_ASL_THREAD = "anr_cleanup_on_asl_thread";
    private static final String SHOULD_DEDUP_DISK_PERSISTENCE_GK_CACHED = "should_dedup_disk_persistence_gk_cached";
    private static final String SHOULD_LOG_ON_SIGNAL_HANDLER = "log_on_signal_handler";
    private static final String SHOULD_LOG_PROCESS_POSITION_IN_ANR_TRACE_FILE = "log_position_anr_trace_file";
    private static final String SHOULD_RECORD_SIGNAL_TIME = "record_signal_time";
    private static final String SHOULD_REPORT_SOFT_ERRORS = "should_report_soft_errors";
    private static final String SHOULD_UPLOAD_ANR_REPORTS = "anr_gk_cached";
    private static final String SHOULD_UPLOAD_SYSTEM_ANR_TRACES_GK_CACHED = "should_upload_system_anr_traces_gk_cached";
    private static final String SHOULD_USE_FG_STATE_FROM_DETECTOR_IN_ASL = "should_use_fg_state_from_anr_detector_in_asl";
    private static final String SHOULD_USE_STATIC_METHOD_CALLBACK = "use_static_method_callback";
    public static final int SIGQUIT_BASED_ANR_DETECTOR = 3;
    private static final String SKIP_SSL_CERT_CHECKS_FILE_NAME = "skip_cert_checks.txt";
    private static IANRDetector mANRDetector;
    public static ANRReport mANRReport;
    public static AcraReportingConfig mConfig;
    private static String mReportHost;
    private static FlexibleReportSender mReportSender;
    private static final Object sANRDetectorLock = new Object();
    private static AppStateUpdater sAppStateUpdater;
    public static boolean sInitialized;
    public static boolean sNativeLibraryLoaded;
    public static final Object sNativeLibraryLoadingLock = new Object();

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.lang.CharSequence, java.lang.String] */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void initSenderHost(android.content.Context r4) {
        /*
            r3 = 0
            java.lang.String r0 = "report_host.txt"
            java.io.File r2 = r4.getFileStreamPath(r0)     // Catch:{ IOException -> 0x003f, all -> 0x0038 }
            boolean r0 = r2.canRead()     // Catch:{ IOException -> 0x003f, all -> 0x0038 }
            if (r0 != 0) goto L_0x0011
            closeStreamNoException(r3)
            return
        L_0x0011:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x003f, all -> 0x0038 }
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ IOException -> 0x003f, all -> 0x0038 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x003f, all -> 0x0038 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x003f, all -> 0x0038 }
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x003e, all -> 0x0036 }
            if (r0 == 0) goto L_0x0025
            java.lang.String r3 = r0.trim()     // Catch:{ IOException -> 0x003e, all -> 0x0036 }
        L_0x0025:
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ IOException -> 0x003e, all -> 0x0036 }
            if (r0 != 0) goto L_0x0032
            com.facebook.acra.sender.FlexibleReportSender r0 = com.facebook.acra.ACRA.mReportSender     // Catch:{ IOException -> 0x003e, all -> 0x0036 }
            r0.setHost(r3)     // Catch:{ IOException -> 0x003e, all -> 0x0036 }
            com.facebook.acra.ACRA.mReportHost = r3     // Catch:{ IOException -> 0x003e, all -> 0x0036 }
        L_0x0032:
            closeStreamNoException(r1)
            return
        L_0x0036:
            r0 = move-exception
            goto L_0x003a
        L_0x0038:
            r0 = move-exception
            r1 = r3
        L_0x003a:
            closeStreamNoException(r1)
            throw r0
        L_0x003e:
            r3 = r1
        L_0x003f:
            closeStreamNoException(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ACRA.initSenderHost(android.content.Context):void");
    }

    private static void writeSenderHost(String str) {
        OutputStreamWriter outputStreamWriter = null;
        try {
            OutputStreamWriter outputStreamWriter2 = new OutputStreamWriter(mConfig.getApplicationContext().openFileOutput(REPORT_HOST_FILE_NAME, 0));
            try {
                outputStreamWriter2.write(str);
                outputStreamWriter2.flush();
                closeStreamNoException(outputStreamWriter2);
            } catch (IOException e) {
                e = e;
                outputStreamWriter = outputStreamWriter2;
                try {
                    C010708t.A0R(LOG_TAG, e, "could not write to host file: ");
                    closeStreamNoException(outputStreamWriter);
                } catch (Throwable th) {
                    th = th;
                    closeStreamNoException(outputStreamWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                outputStreamWriter = outputStreamWriter2;
                closeStreamNoException(outputStreamWriter);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            C010708t.A0R(LOG_TAG, e, "could not write to host file: ");
            closeStreamNoException(outputStreamWriter);
        }
    }

    private static void closeStreamNoException(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                C010708t.A0R(LOG_TAG, e, "Error while closing stream: ");
            }
        }
    }

    public static void customDelayedMessagesSent() {
        ErrorReporter.putCustomData(ErrorReportingConstants.CUSTOM_DELAYED_MESSAGES_SENT, "true");
    }

    private static void deleteHostsFileIfEmpty(Context context) {
        try {
            File fileStreamPath = context.getFileStreamPath(REPORT_HOST_FILE_NAME);
            if (!fileStreamPath.exists()) {
                return;
            }
            if (!fileStreamPath.canRead() || !fileStreamPath.canWrite()) {
                C010708t.A0I(LOG_TAG, "cannot read or write host file");
            } else if (fileStreamPath.length() == 0 && !context.deleteFile(REPORT_HOST_FILE_NAME)) {
                C010708t.A0I(LOG_TAG, "could not delete empty host file");
            }
        } catch (SecurityException e) {
            C010708t.A0R(LOG_TAG, e, "could not delete empty host file: ");
        }
    }

    public static int getAnrDetectorId() {
        return getIntValue(ANDROID_ANR_DETECTOR_TO_USE);
    }

    public static boolean getCachedShouldDedupDiskPersistence() {
        return getFlagValue(SHOULD_DEDUP_DISK_PERSISTENCE_GK_CACHED);
    }

    public static boolean getCachedShouldLogProcessPositionInAnrTraceFile() {
        return getFlagValue(SHOULD_LOG_PROCESS_POSITION_IN_ANR_TRACE_FILE);
    }

    public static boolean getCachedShouldUploadANRReports() {
        return getFlagValueDefaultTrue(SHOULD_UPLOAD_ANR_REPORTS);
    }

    public static boolean getCachedShouldUploadSystemANRTraces() {
        return getFlagValue(SHOULD_UPLOAD_SYSTEM_ANR_TRACES_GK_CACHED);
    }

    private static boolean getFlagValueDefaultTrue(String str) {
        return mConfig.getApplicationContext().getSharedPreferences(ACRA_FLAGS_STORE, 0).getBoolean(str, true);
    }

    private static boolean getSystemProperty(String str, boolean z) {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return ((Boolean) cls.getMethod("getBoolean", String.class, Boolean.TYPE).invoke(cls, str, Boolean.valueOf(z))).booleanValue();
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException unused) {
            return z;
        }
    }

    private static void initSenderSkipCertChecks(Context context) {
        mReportSender.setSkipSslCertsChecks(context.getFileStreamPath(SKIP_SSL_CERT_CHECKS_FILE_NAME).exists());
    }

    private static void initializeAnrDetector(Context context, ErrorReporter errorReporter, String str) {
        int intValue = getIntValue(ANDROID_ANR_DETECTOR_TO_USE);
        int intValue2 = getIntValue(ERROR_MONITOR_CHECK_INTERVAL);
        synchronized (sANRDetectorLock) {
            ErrorReporter errorReporter2 = errorReporter;
            Context context2 = context;
            ANRReport aNRReport = new ANRReport(context2, errorReporter2);
            mANRReport = aNRReport;
            mANRDetector = mConfig.createANRDetector(intValue, new ANRDetectorConfig(context2, str, aNRReport, sAppStateUpdater, new Handler(Looper.getMainLooper()), intValue, mConfig.isInternalBuild(), getFlagValue(SHOULD_CLEANUP_ANR_STATE_ON_ASL_THREAD), getFlagValue(SHOULD_REPORT_SOFT_ERRORS), getFlagValue(SHOULD_LOG_ON_SIGNAL_HANDLER), getFlagValue(SHOULD_AVOID_MUTEX_ON_SIGNAL_HANDLER), getIntValue(ANR_RECOVERY_TIMEOUT), getFlagValue(SHOULD_USE_STATIC_METHOD_CALLBACK), getIntValue(ANR_PROCESS_MONITOR_MAX_CHECKS_BEFORE), getIntValue(ANR_PROCESS_MONITOR_MAX_CHECKS_AFTER), getFlagValue(SHOULD_RECORD_SIGNAL_TIME), getFlagValueDefaultTrue(SHOULD_UPLOAD_ANR_REPORTS), errorReporter2.getAppVersionCode(), errorReporter2.getAppVersionName(), errorReporter2.getSigquitTracesPath(), errorReporter2.getSigquitTracesExtension(), getFlagValue(SHOULD_USE_FG_STATE_FROM_DETECTOR_IN_ASL), getIntValue(ANR_EXPIRATION_TIMEOUT_ON_MAIN_THREAD_UNBLOCKED), getIntValue(ANR_FOREGROUND_CHECK_PERIOD)), intValue2);
        }
    }

    private static void installPeriodicReporter(Context context, final ErrorReporter errorReporter) {
        int A00 = AnonymousClass08X.A00(context, "acraconfig_logcat_native_crash_periodic_interval_mins", 0);
        if (A00 > 0) {
            long j = (long) A00;
            Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(new Runnable() {
                public void run() {
                    ErrorReporter.this.checkNativeReports();
                }
            }, j, j, TimeUnit.MINUTES);
        }
    }

    private static void loadAcraNativeLibrary(final Context context) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    synchronized (ACRA.sNativeLibraryLoadingLock) {
                        AnonymousClass01q.A08("acra");
                        ACRA.sNativeLibraryLoaded = true;
                    }
                    ACRA.nativeLibrarySuccessfullyLoaded(context);
                } catch (UnsatisfiedLinkError unused) {
                }
            }
        }).start();
    }

    private static void maybeInitializeAndStartANRDetector(ErrorReporter errorReporter) {
        Context applicationContext = mConfig.getApplicationContext();
        String processName = getProcessName();
        if (!shouldRunANRDetector(processName)) {
            C010708t.A0O(LOG_TAG, "Skipping ANR Detector for process: %s", processName);
            return;
        }
        C010708t.A0O(LOG_TAG, "Initializing ANR detector for process: %s", processName);
        if (mConfig.shouldStartANRDetector()) {
            initializeAnrDetector(applicationContext, errorReporter, processName);
            IANRDetector iANRDetector = mANRDetector;
            if (iANRDetector != null) {
                iANRDetector.start();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void nativeLibrarySuccessfullyLoaded(Context context) {
        if (AnonymousClass08X.A07(context, "acraconfig_enable_nightwatch")) {
            String processName = getProcessName();
            if (!processName.contains(":")) {
                boolean A07 = AnonymousClass08X.A07(context, "acraconfig_nightwatch_use_setsid");
                boolean A072 = AnonymousClass08X.A07(context, "acraconfig_nightwatch_use_lss_on_exec");
                Random random = new Random();
                try {
                    String canonicalPath = new File(context.getDir("watcher", 0), AnonymousClass08S.A0S(processName.replace(':', '_'), "_", new UUID(random.nextLong(), random.nextLong()).toString(), ".txt")).getCanonicalPath();
                    String A04 = AnonymousClass01q.A04("libwatcher_binary.so");
                    if (A04 == null) {
                        C010708t.A0I("Nightwatch", "Could not find watcher binary");
                    } else {
                        Nightwatch.start(A04, canonicalPath, A07, A072);
                    }
                } catch (IOException e) {
                    C010708t.A0L("Nightwatch", "Error starting watcher", e);
                }
            }
        }
        synchronized (sANRDetectorLock) {
            IANRDetector iANRDetector = mANRDetector;
            if (iANRDetector != null) {
                iANRDetector.nativeLibraryLoaded(getFlagValue(SHOULD_UPLOAD_ANR_REPORTS));
            }
        }
        NativeProcFileReader.nativeLibraryLoaded();
        if (AnonymousClass08X.A07(context, "acraconfig_disable_fs_sync_syscalls")) {
            NoSync.disableFSSync(AnonymousClass08X.A07(context, "acraconfig_use_fast_fs_sync_hooks"));
        }
    }

    public static void onSplashScreenDismissed() {
        ErrorReporter.putCustomData(ErrorReportingConstants.SPLASH_SCREEN_DISMISSED, "true");
    }

    public static void safeToLoadNativeLibraries(Context context) {
        synchronized (sNativeLibraryLoadingLock) {
            if (!sNativeLibraryLoaded) {
                loadAcraNativeLibrary(context);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00d8, code lost:
        r5.setANRDataProvider(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00db, code lost:
        if (r2 == false) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00dd, code lost:
        r1 = com.facebook.acra.ACRA.sNativeLibraryLoadingLock;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00df, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r3 = com.facebook.acra.ACRA.sNativeLibraryLoaded;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e2, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00e4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00e7, code lost:
        if (r3 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00e9, code lost:
        com.facebook.acra.ACRA.mANRDetector.nativeLibraryLoaded(getFlagValue(com.facebook.acra.ACRA.SHOULD_UPLOAD_ANR_REPORTS));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00f2, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f5, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void setANRDataProvider(com.facebook.acra.anr.ANRDataProvider r7) {
        /*
            com.facebook.acra.config.AcraReportingConfig r0 = com.facebook.acra.ACRA.mConfig
            boolean r0 = r0.shouldStartANRDetector()
            if (r0 == 0) goto L_0x00f6
            boolean r0 = r7.shouldANRDetectorRun()
            java.lang.String r6 = "anr_gk_cached"
            updateCachedFlagIfNeeded(r6, r0)
            boolean r1 = r7.shouldUploadSystemANRTraces()
            java.lang.String r0 = "should_upload_system_anr_traces_gk_cached"
            updateCachedFlagIfNeeded(r0, r1)
            r1 = 0
            java.lang.String r0 = "should_dedup_disk_persistence_gk_cached"
            updateCachedFlagIfNeeded(r0, r1)
            int r1 = r7.detectorToUse()
            java.lang.String r0 = "android_anr_detector_to_use"
            updateCachedIntIfNeeded(r0, r1)
            boolean r1 = r7.shouldRunANRDetectorOnBrowserProcess()
            java.lang.String r0 = "run_anr_detector_on_browser_process"
            updateCachedFlagIfNeeded(r0, r1)
            int r1 = r7.detectionIntervalTimeMs()
            java.lang.String r0 = "error_monitor_check_interval"
            updateCachedIntIfNeeded(r0, r1)
            boolean r1 = r7.shouldCleanupStateOnASLThread()
            java.lang.String r0 = "anr_cleanup_on_asl_thread"
            updateCachedFlagIfNeeded(r0, r1)
            boolean r1 = r7.shouldReportSoftErrors()
            java.lang.String r0 = "should_report_soft_errors"
            updateCachedFlagIfNeeded(r0, r1)
            boolean r1 = r7.shouldLogOnSignalHandler()
            java.lang.String r0 = "log_on_signal_handler"
            updateCachedFlagIfNeeded(r0, r1)
            boolean r1 = r7.shouldAvoidMutexOnSignalHandler()
            java.lang.String r0 = "avoid_mutex_on_signal_handler"
            updateCachedFlagIfNeeded(r0, r1)
            int r1 = r7.getRecoveryTimeout()
            java.lang.String r0 = "anr_recovery_timeout"
            updateCachedIntIfNeeded(r0, r1)
            r1 = 0
            java.lang.String r0 = "use_static_method_callback"
            updateCachedFlagIfNeeded(r0, r1)
            int r1 = r7.getMaxNumberOfProcessMonitorChecksBeforeError()
            java.lang.String r0 = "anr_process_monitor_max_checks_before"
            updateCachedIntIfNeeded(r0, r1)
            int r1 = r7.getMaxNumberOfProcessMonitorChecksAfterError()
            java.lang.String r0 = "anr_process_monitor_max_checks_after"
            updateCachedIntIfNeeded(r0, r1)
            boolean r1 = r7.shouldRecordSignalTime()
            java.lang.String r0 = "record_signal_time"
            updateCachedFlagIfNeeded(r0, r1)
            boolean r1 = r7.shouldLogProcessPositionInAnrTraceFile()
            java.lang.String r0 = "log_position_anr_trace_file"
            updateCachedFlagIfNeeded(r0, r1)
            boolean r1 = r7.shouldUseFgStateFromDetectorInASL()
            java.lang.String r0 = "should_use_fg_state_from_anr_detector_in_asl"
            updateCachedFlagIfNeeded(r0, r1)
            int r1 = r7.getExpirationTimeoutOnMainThreadUnblocked()
            java.lang.String r0 = "expiration_timeout_main_thread_unblocked"
            updateCachedIntIfNeeded(r0, r1)
            int r1 = r7.getForegroundCheckPeriod()
            java.lang.String r0 = "foreground_check_period"
            updateCachedIntIfNeeded(r0, r1)
            com.facebook.acra.ErrorReporter r5 = com.facebook.acra.ErrorReporter.getInstance()
            java.lang.Object r4 = com.facebook.acra.ACRA.sANRDetectorLock
            monitor-enter(r4)
            com.facebook.acra.anr.IANRDetector r0 = com.facebook.acra.ACRA.mANRDetector     // Catch:{ all -> 0x00f3 }
            r3 = 0
            r2 = 0
            if (r0 != 0) goto L_0x00ce
            r2 = 1
            com.facebook.acra.config.AcraReportingConfig r0 = com.facebook.acra.ACRA.mConfig     // Catch:{ all -> 0x00f3 }
            android.content.Context r1 = r0.getApplicationContext()     // Catch:{ all -> 0x00f3 }
            java.lang.String r0 = getProcessName()     // Catch:{ all -> 0x00f3 }
            initializeAnrDetector(r1, r5, r0)     // Catch:{ all -> 0x00f3 }
            com.facebook.acra.anr.IANRDetector r0 = com.facebook.acra.ACRA.mANRDetector     // Catch:{ all -> 0x00f3 }
            if (r0 != 0) goto L_0x00ce
            monitor-exit(r4)     // Catch:{ all -> 0x00f3 }
            return
        L_0x00ce:
            com.facebook.acra.anrreport.ANRReport r0 = com.facebook.acra.ACRA.mANRReport     // Catch:{ all -> 0x00f3 }
            r0.mANRDataProvider = r7     // Catch:{ all -> 0x00f3 }
            com.facebook.acra.anr.IANRDetector r0 = com.facebook.acra.ACRA.mANRDetector     // Catch:{ all -> 0x00f3 }
            r0.setANRDataProvider(r7)     // Catch:{ all -> 0x00f3 }
            monitor-exit(r4)     // Catch:{ all -> 0x00f3 }
            r5.setANRDataProvider(r7)
            if (r2 == 0) goto L_0x00e7
            java.lang.Object r1 = com.facebook.acra.ACRA.sNativeLibraryLoadingLock
            monitor-enter(r1)
            boolean r3 = com.facebook.acra.ACRA.sNativeLibraryLoaded     // Catch:{ all -> 0x00e4 }
            monitor-exit(r1)     // Catch:{ all -> 0x00e4 }
            goto L_0x00e7
        L_0x00e4:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00e4 }
            goto L_0x00f5
        L_0x00e7:
            if (r3 == 0) goto L_0x00f6
            com.facebook.acra.anr.IANRDetector r1 = com.facebook.acra.ACRA.mANRDetector
            boolean r0 = getFlagValue(r6)
            r1.nativeLibraryLoaded(r0)
            return
        L_0x00f3:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00f3 }
        L_0x00f5:
            throw r0
        L_0x00f6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ACRA.setANRDataProvider(com.facebook.acra.anr.ANRDataProvider):void");
    }

    public static void setANRDetectorCheckIntervalMs(long j) {
        IANRDetector iANRDetector = mANRDetector;
        if (iANRDetector != null) {
            iANRDetector.setCheckIntervalMs(j);
        }
    }

    public static void setANRDetectorListener(ANRDetectorListener aNRDetectorListener) {
        IANRDetector iANRDetector = mANRDetector;
        if (iANRDetector != null) {
            iANRDetector.setListener(aNRDetectorListener);
        }
    }

    public static void setPerformanceMarker(PerformanceMarker performanceMarker) {
        ANRReport aNRReport = mANRReport;
        if (aNRReport != null) {
            aNRReport.mPerformanceMarker = performanceMarker;
        }
    }

    public static void setReportHost(String str) {
        mReportSender.setHost(str);
        writeSenderHost(str);
    }

    public static void setSkipSslCertChecks(boolean z) {
        mReportSender.setSkipSslCertsChecks(z);
        writeSkipCertChecksFile(z);
    }

    private static boolean shouldRunANRDetector(String str) {
        if (getFlagValue(SHOULD_RECORD_SIGNAL_TIME) || (!str.contains(":")) || (str.contains(":browser") && getFlagValue(RUN_ANR_DETECTOR_ON_BROWSER_PROCESS))) {
            return true;
        }
        return false;
    }

    private static boolean shouldRunNightwatch(Context context) {
        return AnonymousClass08X.A07(context, "acraconfig_enable_nightwatch");
    }

    public static void startANRDetector() {
        IANRDetector iANRDetector = mANRDetector;
        if (iANRDetector != null) {
            iANRDetector.start();
        }
    }

    public static void stopANRDetector() {
        IANRDetector iANRDetector = mANRDetector;
        if (iANRDetector != null) {
            iANRDetector.stop(null);
        }
    }

    private static void updateCachedFlagIfNeeded(String str, boolean z) {
        SharedPreferences sharedPreferences = mConfig.getApplicationContext().getSharedPreferences(ACRA_FLAGS_STORE, 0);
        if (sharedPreferences.getBoolean(str, false) != z) {
            sharedPreferences.edit().putBoolean(str, z).apply();
        }
    }

    private static void updateCachedIntIfNeeded(String str, int i) {
        SharedPreferences sharedPreferences = mConfig.getApplicationContext().getSharedPreferences(ACRA_FLAGS_STORE, 0);
        if (sharedPreferences.getInt(str, 0) != i) {
            sharedPreferences.edit().putInt(str, i).apply();
        }
    }

    private static void writeSkipCertChecksFile(boolean z) {
        String str;
        String str2;
        Object[] objArr;
        try {
            File fileStreamPath = mConfig.getApplicationContext().getFileStreamPath(SKIP_SSL_CERT_CHECKS_FILE_NAME);
            if (z) {
                fileStreamPath.createNewFile();
                if (!fileStreamPath.exists()) {
                    str = LOG_TAG;
                    str2 = "Failed to create skip cert checks file: %s";
                    objArr = new Object[]{fileStreamPath.toString()};
                } else {
                    return;
                }
            } else {
                fileStreamPath.delete();
                if (fileStreamPath.exists()) {
                    str = LOG_TAG;
                    str2 = "Failed to delete skip cert checks file: %s";
                    objArr = new Object[]{fileStreamPath.toString()};
                } else {
                    return;
                }
            }
            C010708t.A0O(str, str2, objArr);
        } catch (IOException e) {
            C010708t.A0R(LOG_TAG, e, "could not create ssl cert checks file.");
        }
    }

    public static ANRReport getANRReport() {
        return mANRReport;
    }

    public static AcraReportingConfig getConfig() {
        return mConfig;
    }

    public static String getProcessName() {
        String str = AnonymousClass00M.A00().A01;
        if (str == null) {
            return "unknown";
        }
        return str;
    }

    public static String getReportHost() {
        return mReportHost;
    }

    public static boolean isInitialized() {
        return sInitialized;
    }

    private static boolean getFlagValue(SharedPreferences sharedPreferences, String str, boolean z) {
        return sharedPreferences.getBoolean(str, z);
    }

    public static boolean getFlagValue(String str) {
        return mConfig.getApplicationContext().getSharedPreferences(ACRA_FLAGS_STORE, 0).getBoolean(str, false);
    }

    private static int getIntValue(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getInt(str, 0);
    }

    private static int getIntValue(String str) {
        return mConfig.getApplicationContext().getSharedPreferences(ACRA_FLAGS_STORE, 0).getInt(str, 0);
    }

    public static ErrorReporter init(AcraReportingConfig acraReportingConfig) {
        return init(acraReportingConfig, 0, null, null);
    }

    public static ErrorReporter init(AcraReportingConfig acraReportingConfig, long j) {
        return init(acraReportingConfig, j, null, null);
    }

    public static ErrorReporter init(AcraReportingConfig acraReportingConfig, long j, AppStateUpdater appStateUpdater) {
        return init(acraReportingConfig, j, appStateUpdater, null);
    }

    public static ErrorReporter init(AcraReportingConfig acraReportingConfig, long j, AppStateUpdater appStateUpdater, ErrorReporter.ExcludedReportObserver excludedReportObserver) {
        sInitialized = true;
        acraReportingConfig.crashReportUrl();
        ErrorReporter instance = ErrorReporter.getInstance();
        if (j > 0) {
            instance.setAppStartTickTimeMs(j);
        }
        if (mConfig == null) {
            mConfig = acraReportingConfig;
            Context applicationContext = acraReportingConfig.getApplicationContext();
            getProcessName();
            deleteHostsFileIfEmpty(applicationContext);
            if (excludedReportObserver != null) {
                instance.setExcludedReportObserver(excludedReportObserver);
            }
            instance.init(mConfig);
            Throwable th = null;
            try {
                instance.initFallible();
            } catch (Throwable th2) {
                th = th2;
            }
            FlexibleReportSender createReportSender = mConfig.createReportSender();
            mReportSender = createReportSender;
            instance.setReportSender(createReportSender);
            initSenderHost(applicationContext);
            initSenderSkipCertChecks(applicationContext);
            instance.checkReportsOnApplicationStart();
            if (mConfig.shouldStopAnrDetectorOnErrorReporting()) {
                AnonymousClass016.A02(new AnonymousClass015() {
                    public void handleUncaughtException(Thread thread, Throwable th, C02120Da r3) {
                        ACRA.stopANRDetector();
                    }
                }, 100);
            }
            AnonymousClass016.A02(instance, 0);
            if (th != null) {
                instance.reportErrorAndTerminate(Thread.currentThread(), th);
            }
            AcraReportingConfig acraReportingConfig2 = mConfig;
            if (acraReportingConfig2.shouldInstallPeriodicReporter()) {
                installPeriodicReporter(acraReportingConfig2.getApplicationContext(), instance);
            }
        }
        sAppStateUpdater = appStateUpdater;
        maybeInitializeAndStartANRDetector(instance);
        return instance;
    }
}
