package com.facebook.acra;

import X.AnonymousClass015;
import X.AnonymousClass016;
import X.AnonymousClass08S;
import X.C009007v;
import X.C009107w;
import X.C010708t;
import X.C02120Da;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.PowerManager;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import com.facebook.acra.Spool;
import com.facebook.acra.anr.ANRDataProvider;
import com.facebook.acra.config.AcraReportingConfig;
import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.acra.constants.ReportField;
import com.facebook.acra.criticaldata.CriticalAppData;
import com.facebook.acra.customdata.ProxyCustomDataStore;
import com.facebook.acra.sender.FlexibleReportSender;
import com.facebook.acra.sender.HttpPostSender;
import com.facebook.acra.sender.ReportSender;
import com.facebook.acra.sender.ReportSenderException;
import com.facebook.acra.util.AttachmentUtil;
import com.facebook.acra.util.CrashTimeDataCollectorHelper;
import com.facebook.acra.util.InputStreamField;
import com.facebook.acra.util.JsonReportWriter;
import com.facebook.acra.util.PackageManagerWrapper;
import com.facebook.acra.util.SimpleTraceLogger;
import com.facebook.acra.util.minidump.MinidumpReader;
import com.facebook.common.build.BuildConstants;
import io.card.payment.BuildConfig;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.net.Proxy;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ErrorReporter implements AnonymousClass015 {
    public static final String ACRA_DIRNAME = "acra-reports";
    private static final String ANR_EXTRA_PROPERTIES_EXTENSION = ".upd";
    private static final Object ANR_REPORTING_LOCK = new Object();
    public static final String ANR_TRACES_FILE_PATH = "/data/anr/traces.txt";
    public static final String CACHED_REPORTFILE_EXTENSION = ".cachedreport";
    public static final String CPUSPIN_DIR = "traces_cpuspin";
    public static final long CPUSPIN_MAX_REPORT_SIZE = 524288;
    public static final String CRASH_ATTACHMENT_DUMMY_STACKTRACE = "crash attachment";
    public static final String CRASH_DUMP_SYS_LIBS_FILE = "crash_dump_sys_libs";
    public static final long DEFAULT_MAX_REPORT_SIZE = 1572864;
    public static final int DEFAULT_OOM_RESERVATION = 65536;
    public static final String DUMPFILE_EXTENSION = ".dmp";
    public static final String DUMP_DIR = "minidumps";
    private static final String FILE_IAB_OPEN_TIMES = "iab_open_times";
    private static final String FILE_LAST_ACTIVITY = "last_activity_opened";
    private static final int HANDLE_EXCEPTION_NEVER_SEND_IMMEDIATELY = 4;
    private static final int HANDLE_EXCEPTION_SEND_IMMEDIATELY = 1;
    private static final int HANDLE_EXCEPTION_SEND_SYNCHRONOUSLY = 2;
    public static final int MAX_ANR_TRACES_TIME_DELTA_MS = 15000;
    public static final long MAX_REPORT_AGE = 604800000;
    public static final int MAX_SEND_REPORTS = 5;
    public static final int MAX_STACK_LENGTH_FOR_OVERFLOW = 800000;
    private static final int MAX_TRACE_COUNT_LIMIT = 20;
    private static final int MAX_TRANSLATION_HOOK_RUNS = 4;
    private static final int MAX_VERSION_LINE_LENGTH = 20;
    private static final long MS_PER_DAY = 86400000;
    public static final long NATIVE_MAX_REPORT_SIZE = 8388608;
    private static final String NO_FILE = "NO_FILE";
    public static final long NUM_NATIVE_CRASHES_SAVED = 5;
    public static final String PREALLOCATED_REPORTFILE = "reportfile.prealloc";
    public static final String REPORTED_CRASH_DIR = "reported_crashes";
    public static final String REPORTFILE_EXTENSION = ".stacktrace";
    private static final CrashReportType[] REPORTS_TO_CHECK_ON_STARTUP = {CrashReportType.ACRA_CRASH_REPORT, CrashReportType.NATIVE_CRASH_REPORT};
    public static final String SIGQUIT_DIR = "traces";
    public static final long SIGQUIT_MAX_REPORT_SIZE = 524288;
    private static final int SIGQUIT_PROCESS_NAME_BUFFER_SIZE = 1024;
    public static final String STACK_TRIMMED_MESSAGE = "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxSTACK_FRAMES_TRIMMED_FOR_OVERFLOWxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
    public static final String TAG = "ErrorReporter";
    private static final Object UNCAUGHT_EXCEPTION_LOCK = new Object();
    public static final String UNKNOWN_FIELD_VALUE = "unknown";
    private static final String mInternalException = "ACRA_INTERNAL=java.lang.Exception: An exception occurred while trying to collect data about an ACRA internal error\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:810)\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:866)\n\tat com.facebook.acra.ErrorReporter.uncaughtException(ErrorReporter.java:666)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:693)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:690)\n";
    /* access modifiers changed from: private */
    public static final AtomicInteger mSendAttempts = new AtomicInteger();
    private static final Pattern mSigquitCmdLinePattern = Pattern.compile("^Cmd line: (.*)", 8);
    public static SecureSettingsResolver sSecureSettingsResolver = new DefaultSecureSettingsResolver();
    public static final ReentrantReadWriteLock sSystemLibFileLock = new ReentrantReadWriteLock();
    private volatile ANRDataProvider mANRDataProvider;
    private final SimpleTraceLogger mActivityLogger;
    private final Set mAnrFilesInProgress;
    private final Time mAppStartDate;
    private volatile long mAppStartTickTimeMs;
    private volatile String mAppVersionCode;
    private volatile String mAppVersionName;
    /* access modifiers changed from: private */
    public BatchUploader mBatchUploader;
    private BlackBoxRecorderControl mBlackBoxRecorderControl;
    private volatile Thread.UncaughtExceptionHandler mChainedHandler;
    private volatile String mClientUserId;
    private volatile AcraReportingConfig mConfig;
    private volatile Map mConstantFields;
    /* access modifiers changed from: private */
    public volatile Context mContext;
    private final AtomicReference mCrashReportedObserver;
    private final AtomicReference mExceptionTranslationHook;
    private volatile ExcludedReportObserver mExcludedReportObserver;
    private volatile boolean mFinishedCheckingReports;
    private volatile boolean mInitializationComplete;
    private volatile long mInstallTime;
    private final Map mInstanceLazyCustomParameters;
    private volatile LogBridge mLogBridge;
    private volatile long mMaxReportSize;
    private volatile byte[] mOomReservation;
    private int mPendingReportWriters;
    private volatile File mPreallocFileName;
    private final ArrayList mReportSenders;
    private volatile boolean mStartedBatchUploader;
    private volatile String mUserId;

    public class AcraReportHandler implements ReportHandler {
        public boolean handleReport(ErrorReporter errorReporter, Spool.FileBeingConsumed fileBeingConsumed, String str, boolean z) {
            File file = fileBeingConsumed.fileName;
            String name = file.getName();
            try {
                CrashReportData access$000 = errorReporter.loadAcraCrashReport(fileBeingConsumed);
                if (access$000 != null) {
                    access$000.put(ReportField.ACRA_REPORT_TYPE, CrashReportType.ACRA_CRASH_REPORT.name());
                    access$000.put(ReportField.ACRA_REPORT_FILENAME, name);
                    access$000.put(ReportField.UPLOADED_BY_PROCESS, str);
                    errorReporter.sendCrashReport(access$000);
                    boolean unused = ErrorReporter.deleteFile(file);
                }
                return true;
            } catch (RuntimeException e) {
                C010708t.A0R(ACRA.LOG_TAG, e, "Failed to send crash reports");
                boolean unused2 = ErrorReporter.deleteFile(file);
                return false;
            } catch (IOException e2) {
                C010708t.A0U(ACRA.LOG_TAG, e2, "Failed to load crash report for %s", name);
                boolean unused3 = ErrorReporter.deleteFile(file);
                return false;
            } catch (ReportSenderException e3) {
                C010708t.A0U(ACRA.LOG_TAG, e3, "Failed to send crash report for %s", name);
                return false;
            }
        }

        public AcraReportHandler() {
        }
    }

    public final class Api16Utils {
        public static void applyBigTextStyle(Notification.Builder builder, String str) {
            builder.setStyle(new Notification.BigTextStyle().bigText(str));
        }

        private Api16Utils() {
        }
    }

    public final class Api9Utils {
        public static void disableStrictMode() {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }

        public static void restoreStrictMode(Object obj) {
            StrictMode.setThreadPolicy((StrictMode.ThreadPolicy) obj);
        }

        private Api9Utils() {
        }

        public static Object saveStrictMode() {
            return StrictMode.getThreadPolicy();
        }
    }

    public class CachedANRReportHandler implements ReportHandler {
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d2, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
            r9.close();
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:50:0x00d6 */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:50:0x00d6=Splitter:B:50:0x00d6, B:40:0x00c8=Splitter:B:40:0x00c8} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int findANRTraces(java.lang.String r19, long r20, java.lang.String r22, java.lang.StringBuilder r23) {
            /*
                r18 = this;
                r0 = -1
                r11 = r19
                if (r19 != 0) goto L_0x0006
                return r0
            L_0x0006:
                java.io.BufferedReader r9 = new java.io.BufferedReader     // Catch:{ ParseException -> 0x00e0, IOException -> 0x00d7, NumberFormatException -> 0x00e9 }
                java.io.FileReader r0 = new java.io.FileReader     // Catch:{ ParseException -> 0x00e0, IOException -> 0x00d7, NumberFormatException -> 0x00e9 }
                r1 = r22
                r0.<init>(r1)     // Catch:{ ParseException -> 0x00e0, IOException -> 0x00d7, NumberFormatException -> 0x00e9 }
                r9.<init>(r0)     // Catch:{ ParseException -> 0x00e0, IOException -> 0x00d7, NumberFormatException -> 0x00e9 }
                java.lang.String r0 = "----- pid (\\d+) at (\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}).* -----"
                java.util.regex.Pattern r10 = java.util.regex.Pattern.compile(r0)     // Catch:{ all -> 0x00d0 }
                java.lang.String r0 = "----- end (\\d+) -----"
                java.util.regex.Pattern r8 = java.util.regex.Pattern.compile(r0)     // Catch:{ all -> 0x00d0 }
                java.lang.String r0 = "Cmd line: (.*)"
                java.util.regex.Pattern r7 = java.util.regex.Pattern.compile(r0)     // Catch:{ all -> 0x00d0 }
                r2 = 0
                r13 = 0
                r6 = -1
                r17 = 0
            L_0x0029:
                java.lang.String r5 = r9.readLine()     // Catch:{ all -> 0x00d0 }
                if (r5 == 0) goto L_0x00cc
                java.lang.String r4 = "\n"
                r3 = 1
                r12 = r23
                if (r13 == 0) goto L_0x0054
                r12.append(r5)     // Catch:{ all -> 0x00d0 }
                r12.append(r4)     // Catch:{ all -> 0x00d0 }
                java.util.regex.Matcher r1 = r8.matcher(r5)     // Catch:{ all -> 0x00d0 }
                boolean r0 = r1.find()     // Catch:{ all -> 0x00d0 }
                if (r0 == 0) goto L_0x00c5
                java.lang.String r0 = r1.group(r3)     // Catch:{ all -> 0x00d0 }
                int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x00d0 }
                if (r6 == r0) goto L_0x00c8
                r12.setLength(r2)     // Catch:{ all -> 0x00d0 }
                goto L_0x00c8
            L_0x0054:
                java.util.regex.Matcher r1 = r10.matcher(r5)     // Catch:{ all -> 0x00d0 }
                boolean r0 = r1.find()     // Catch:{ all -> 0x00d0 }
                if (r0 == 0) goto L_0x00c5
                if (r23 == 0) goto L_0x0063
                r12.setLength(r2)     // Catch:{ all -> 0x00d0 }
            L_0x0063:
                java.lang.String r0 = r1.group(r3)     // Catch:{ all -> 0x00d0 }
                int r6 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x00d0 }
                r0 = 2
                java.lang.String r2 = r1.group(r0)     // Catch:{ all -> 0x00d0 }
                java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat     // Catch:{ all -> 0x00d0 }
                java.lang.String r0 = "yyyy-MM-dd HH:mm:ss"
                r1.<init>(r0)     // Catch:{ all -> 0x00d0 }
                java.util.Date r0 = r1.parse(r2)     // Catch:{ all -> 0x00d0 }
                long r15 = r0.getTime()     // Catch:{ all -> 0x00d0 }
                java.lang.String r2 = r9.readLine()     // Catch:{ all -> 0x00d0 }
                java.util.regex.Matcher r1 = r7.matcher(r2)     // Catch:{ all -> 0x00d0 }
                boolean r0 = r1.find()     // Catch:{ all -> 0x00d0 }
                if (r0 == 0) goto L_0x00c5
                java.lang.String r1 = r1.group(r3)     // Catch:{ all -> 0x00d0 }
                boolean r0 = r1.equals(r11)     // Catch:{ all -> 0x00d0 }
                if (r0 == 0) goto L_0x009a
                if (r23 != 0) goto L_0x009a
                goto L_0x00c8
            L_0x009a:
                if (r0 == 0) goto L_0x00b7
                long r13 = r20 - r15
                long r15 = java.lang.Math.abs(r13)     // Catch:{ all -> 0x00d0 }
                r13 = 15000(0x3a98, double:7.411E-320)
                int r0 = (r15 > r13 ? 1 : (r15 == r13 ? 0 : -1))
                if (r0 >= 0) goto L_0x00b7
                if (r23 == 0) goto L_0x00b7
                r12.append(r5)     // Catch:{ all -> 0x00d0 }
                r12.append(r4)     // Catch:{ all -> 0x00d0 }
                r12.append(r2)     // Catch:{ all -> 0x00d0 }
                r12.append(r4)     // Catch:{ all -> 0x00d0 }
                goto L_0x00b8
            L_0x00b7:
                r3 = 0
            L_0x00b8:
                if (r3 != 0) goto L_0x00c4
                java.lang.String r0 = "/"
                boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x00d0 }
                if (r0 != 0) goto L_0x00c4
                int r17 = r17 + 1
            L_0x00c4:
                r13 = r3
            L_0x00c5:
                r2 = 0
                goto L_0x0029
            L_0x00c8:
                r9.close()     // Catch:{ ParseException -> 0x00e0, IOException -> 0x00d7, NumberFormatException -> 0x00e9 }
                return r17
            L_0x00cc:
                r9.close()     // Catch:{ ParseException -> 0x00e0, IOException -> 0x00d7, NumberFormatException -> 0x00e9 }
                goto L_0x00f1
            L_0x00d0:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x00d2 }
            L_0x00d2:
                r0 = move-exception
                r9.close()     // Catch:{ all -> 0x00d6 }
            L_0x00d6:
                throw r0     // Catch:{ ParseException -> 0x00e0, IOException -> 0x00d7, NumberFormatException -> 0x00e9 }
            L_0x00d7:
                r2 = move-exception
                java.lang.String r1 = "ACRA"
                java.lang.String r0 = "Failed to read ANR traces."
                X.C010708t.A0R(r1, r2, r0)
                goto L_0x00f1
            L_0x00e0:
                r2 = move-exception
                java.lang.String r1 = "ACRA"
                java.lang.String r0 = "Failed to parse ANR timestamp."
                X.C010708t.A0R(r1, r2, r0)
                goto L_0x00f1
            L_0x00e9:
                r2 = move-exception
                java.lang.String r1 = "ACRA"
                java.lang.String r0 = "Failed to extract pid from ANR traces."
                X.C010708t.A0R(r1, r2, r0)
            L_0x00f1:
                r0 = -1
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.CachedANRReportHandler.findANRTraces(java.lang.String, long, java.lang.String, java.lang.StringBuilder):int");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        private void addUpdatedProperties(CrashReportData crashReportData, CrashReportData crashReportData2, String str, File file) {
            boolean z;
            String customData;
            String customData2;
            String str2;
            HashMap hashMap = new HashMap();
            int i = -1;
            CrashReportData crashReportData3 = crashReportData2;
            CrashReportData crashReportData4 = crashReportData;
            if (!ACRA.getFlagValue("should_upload_system_anr_traces_gk_cached") || (((str2 = (String) crashReportData4.get(ErrorReportingConstants.ANR_WITH_SIGQUIT_TRACES)) != null && !str2.equals("0")) || ((String) crashReportData3.get(ReportField.SIGQUIT)) != null)) {
                z = false;
            } else {
                StringBuilder sb = new StringBuilder();
                int findANRTraces = findANRTraces(crashReportData4.getProperty(ReportField.PROCESS_NAME), Long.parseLong(crashReportData4.getProperty(ReportField.TIME_OF_CRASH)), ErrorReporter.ANR_TRACES_FILE_PATH, sb);
                String str3 = null;
                if (findANRTraces > -1) {
                    str3 = sb.toString();
                }
                if (str3 != null) {
                    hashMap.put(ReportField.SIGQUIT, AttachmentUtil.compressToBase64String(str3.getBytes()));
                    hashMap.put(ErrorReportingConstants.ANR_SYSTEM_TRACES_PRESENT, "true");
                } else {
                    hashMap.put(ErrorReportingConstants.ANR_SYSTEM_TRACES_PRESENT, "false");
                }
                i = findANRTraces;
                z = true;
            }
            if (ACRA.getFlagValue("log_position_anr_trace_file")) {
                if (!z) {
                    i = findANRTraces(crashReportData4.getProperty(ReportField.PROCESS_NAME), Long.parseLong(crashReportData4.getProperty(ReportField.TIME_OF_CRASH)), ErrorReporter.ANR_TRACES_FILE_PATH, null);
                }
                hashMap.put(ErrorReportingConstants.ANR_TRACE_POSITION, String.valueOf(i));
            }
            hashMap.put(ReportField.UPLOADED_BY_PROCESS, str);
            if (((String) crashReportData3.get(ErrorReportingConstants.ANR_RECOVERY_DELAY_TAG)) == null) {
                hashMap.put(ErrorReportingConstants.ANR_RECOVERY_DELAY_TAG, ErrorReporter.getCustomData(ErrorReportingConstants.ANR_RECOVERY_DELAY_TAG));
            }
            if (((String) crashReportData3.get(ErrorReportingConstants.ANR_SYSTEM_ERROR_MSG)) == null && (customData2 = ErrorReporter.getCustomData(ErrorReportingConstants.ANR_SYSTEM_ERROR_MSG)) != null) {
                hashMap.put(ErrorReportingConstants.ANR_SYSTEM_ERROR_MSG, customData2);
            }
            if (((String) crashReportData3.get(ErrorReportingConstants.ANR_SYSTEM_TAG)) == null && (customData = ErrorReporter.getCustomData(ErrorReportingConstants.ANR_SYSTEM_TAG)) != null) {
                hashMap.put(ErrorReportingConstants.ANR_SYSTEM_TAG, customData);
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            crashReportData3.putAll(hashMap, CrashReportData.getWriter(fileOutputStream));
            fileOutputStream.close();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void
         arg types: [com.facebook.acra.CrashReportData, int, ?[OBJECT, ARRAY]]
         candidates:
          ClspMth{java.util.HashMap.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
          ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
          ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
          ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
          com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x006a, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            r3.close();
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006e */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean handleReport(com.facebook.acra.ErrorReporter r11, com.facebook.acra.Spool.FileBeingConsumed r12, java.lang.String r13, boolean r14) {
            /*
                r10 = this;
                boolean r0 = r11.shouldReportANRs()
                r8 = 0
                r7 = 1
                r4 = 0
                if (r0 != 0) goto L_0x0017
                android.content.Context r1 = r11.mContext
                java.lang.String r0 = "traces"
                java.io.File r0 = r1.getDir(r0, r4)
                com.facebook.acra.ErrorReporter.purgeDirectory(r0, r8)
                return r7
            L_0x0017:
                java.io.File r6 = r12.fileName
                java.lang.String r9 = r6.getName()
                java.lang.String r1 = r6.getCanonicalPath()     // Catch:{ IOException -> 0x0095 }
                java.io.File r5 = new java.io.File
                java.lang.String r0 = ".upd"
                java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
                r5.<init>(r0)
                java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ RuntimeException -> 0x0086, IOException -> 0x0079, ReportSenderException -> 0x006f }
                r3.<init>(r6)     // Catch:{ RuntimeException -> 0x0086, IOException -> 0x0079, ReportSenderException -> 0x006f }
                com.facebook.acra.CrashReportData r2 = new com.facebook.acra.CrashReportData     // Catch:{ all -> 0x0068 }
                r2.<init>()     // Catch:{ all -> 0x0068 }
                r2.load(r3)     // Catch:{ all -> 0x0068 }
                com.facebook.acra.CrashReportData r1 = new com.facebook.acra.CrashReportData     // Catch:{ all -> 0x0068 }
                r1.<init>()     // Catch:{ all -> 0x0068 }
                boolean r0 = r5.exists()     // Catch:{ all -> 0x0068 }
                if (r0 == 0) goto L_0x004c
                java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0068 }
                r0.<init>(r5)     // Catch:{ all -> 0x0068 }
                r1.load(r0)     // Catch:{ all -> 0x0068 }
            L_0x004c:
                r10.addUpdatedProperties(r2, r1, r13, r5)     // Catch:{ all -> 0x0068 }
                r2.merge(r1, r7, r8)     // Catch:{ all -> 0x0068 }
                if (r14 == 0) goto L_0x005b
                java.lang.String r1 = "SENT_IMMEDIATELY"
                java.lang.String r0 = "true"
                r2.put(r1, r0)     // Catch:{ all -> 0x0068 }
            L_0x005b:
                r11.sendCrashReport(r2)     // Catch:{ all -> 0x0068 }
                boolean unused = com.facebook.acra.ErrorReporter.deleteFile(r6)     // Catch:{ all -> 0x0068 }
                boolean unused = com.facebook.acra.ErrorReporter.deleteFile(r5)     // Catch:{ all -> 0x0068 }
                r3.close()     // Catch:{ RuntimeException -> 0x0086, IOException -> 0x0079, ReportSenderException -> 0x006f }
                return r7
            L_0x0068:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x006a }
            L_0x006a:
                r0 = move-exception
                r3.close()     // Catch:{ all -> 0x006e }
            L_0x006e:
                throw r0     // Catch:{ RuntimeException -> 0x0086, IOException -> 0x0079, ReportSenderException -> 0x006f }
            L_0x006f:
                r3 = move-exception
                java.lang.String r2 = "ACRA"
                java.lang.Object[] r1 = new java.lang.Object[]{r9}
                java.lang.String r0 = "Failed to send crash report for %s"
                goto L_0x009e
            L_0x0079:
                r3 = move-exception
                java.lang.String r2 = "ACRA"
                java.lang.Object[] r1 = new java.lang.Object[]{r9}
                java.lang.String r0 = "Failed to load crash report for %s"
                X.C010708t.A0U(r2, r3, r0, r1)
                goto L_0x008e
            L_0x0086:
                r2 = move-exception
                java.lang.String r1 = "ACRA"
                java.lang.String r0 = "Failed to send crash reports"
                X.C010708t.A0R(r1, r2, r0)
            L_0x008e:
                boolean unused = com.facebook.acra.ErrorReporter.deleteFile(r6)
                boolean unused = com.facebook.acra.ErrorReporter.deleteFile(r5)
                return r4
            L_0x0095:
                r3 = move-exception
                java.lang.String r2 = "ACRA"
                java.lang.Object[] r1 = new java.lang.Object[]{r9}
                java.lang.String r0 = "Failed to get full path of crash report for %s"
            L_0x009e:
                X.C010708t.A0U(r2, r3, r0, r1)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.CachedANRReportHandler.handleReport(com.facebook.acra.ErrorReporter, com.facebook.acra.Spool$FileBeingConsumed, java.lang.String, boolean):boolean");
        }
    }

    public enum CrashReportType {
        ACRA_CRASH_REPORT(ErrorReporter.ACRA_DIRNAME, ErrorReporter.DEFAULT_MAX_REPORT_SIZE, null, new AcraReportHandler(), ErrorReporter.REPORTFILE_EXTENSION),
        NATIVE_CRASH_REPORT("minidumps", ErrorReporter.NATIVE_MAX_REPORT_SIZE, ReportField.MINIDUMP, null, ErrorReporter.DUMPFILE_EXTENSION),
        ANR_REPORT(ErrorReporter.SIGQUIT_DIR, 524288, ReportField.SIGQUIT, null, ErrorReporter.REPORTFILE_EXTENSION),
        CACHED_ANR_REPORT(ErrorReporter.SIGQUIT_DIR, 524288, ReportField.SIGQUIT, new CachedANRReportHandler(), ".cachedreport");
        
        public final String attachmentField;
        public final long defaultMaxSize;
        public final String directory;
        public final String[] fileExtensions;
        private final ReportHandler handler;
        private final Object mLock = new Object();
        private Spool mSpool;

        public static Spool.Snapshot getPendingCrashReports(CrashReportType crashReportType, Context context) {
            final String[] strArr = crashReportType.fileExtensions;
            return crashReportType.getSpool(context).snapshot(new FifoSpoolComparator(), new FilenameFilter() {
                public boolean accept(File file, String str) {
                    for (String endsWith : strArr) {
                        if (str.endsWith(endsWith)) {
                            return true;
                        }
                    }
                    return false;
                }
            });
        }

        public Spool getSpool(Context context) {
            Spool spool;
            synchronized (this.mLock) {
                if (this.mSpool == null) {
                    this.mSpool = new Spool(context.getDir(this.directory, 0));
                }
                spool = this.mSpool;
            }
            return spool;
        }

        private CrashReportType(String str, long j, String str2, ReportHandler reportHandler, String... strArr) {
            this.directory = str;
            this.defaultMaxSize = j;
            this.attachmentField = str2;
            this.handler = reportHandler;
            this.fileExtensions = strArr;
        }

        public ReportHandler getHandler() {
            return this.handler;
        }
    }

    public interface CrashReportedObserver {
        void onCrashReported(CrashReportData crashReportData);
    }

    public interface ExcludedReportObserver {
        void onExclude(CrashReportData crashReportData);
    }

    public interface ReportHandler {
        boolean handleReport(ErrorReporter errorReporter, Spool.FileBeingConsumed fileBeingConsumed, String str, boolean z);
    }

    public final class ReportsSenderWorker extends Thread {
        private Throwable exception;
        private FileGenerator mGenerator;
        public final CrashReportData mInMemoryReportToSend;
        private final Spool.FileBeingProduced mReportFileUnderConstruction;
        private final CrashReportType[] mReportTypesToSend;

        private PowerManager.WakeLock acquireWakeLock() {
            if (!new PackageManagerWrapper(ErrorReporter.this.mContext, ACRA.LOG_TAG).hasPermission("android.permission.WAKE_LOCK")) {
                return null;
            }
            PowerManager.WakeLock A00 = C009007v.A00((PowerManager) ErrorReporter.this.mContext.getSystemService("power"), 1, "ACRA wakelock");
            C009007v.A03(A00, false);
            A00.acquire();
            C009107w.A01(A00, -1);
            return A00;
        }

        public void run() {
            synchronized (ErrorReporter.this) {
                try {
                    ErrorReporter.access$908(ErrorReporter.this);
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            try {
                doSend();
            } catch (Throwable th2) {
                ErrorReporter.this.safeClose(this.mReportFileUnderConstruction);
                throw th2;
            }
            ErrorReporter.this.safeClose(this.mReportFileUnderConstruction);
            synchronized (ErrorReporter.this) {
                try {
                    ErrorReporter.access$910(ErrorReporter.this);
                } catch (Throwable th3) {
                    while (true) {
                        th = th3;
                        break;
                    }
                }
            }
            ErrorReporter.this.startUploadIfReady();
            return;
            throw th;
        }

        public void doSend() {
            PowerManager.WakeLock wakeLock;
            ErrorReporter.mSendAttempts.getAndIncrement();
            try {
                wakeLock = acquireWakeLock();
                try {
                    CrashReportData crashReportData = this.mInMemoryReportToSend;
                    if (crashReportData == null) {
                        ErrorReporter.this.prepareReports(Integer.MAX_VALUE, this.mGenerator, true, this.mReportTypesToSend);
                    } else {
                        crashReportData.put(ReportField.UPLOADED_BY_PROCESS, CrashTimeDataCollector.getProcessNameFromAms(ErrorReporter.this.mContext));
                        ErrorReporter.this.sendCrashReport(crashReportData);
                        Spool.FileBeingProduced fileBeingProduced = this.mReportFileUnderConstruction;
                        if (fileBeingProduced != null) {
                            fileBeingProduced.fileName.delete();
                        }
                    }
                    if (wakeLock != null && wakeLock.isHeld()) {
                        C009007v.A01(wakeLock);
                    }
                } catch (Throwable th) {
                    th = th;
                    if (wakeLock != null && wakeLock.isHeld()) {
                        C009007v.A01(wakeLock);
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                wakeLock = null;
                C009007v.A01(wakeLock);
                throw th;
            }
        }

        public Throwable getException() {
            return this.exception;
        }

        public void routeReportToFile(FileGenerator fileGenerator) {
            this.mGenerator = fileGenerator;
        }

        public ReportsSenderWorker(ErrorReporter errorReporter, CrashReportData crashReportData, Spool.FileBeingProduced fileBeingProduced) {
            this(crashReportData, fileBeingProduced, null);
        }

        private ReportsSenderWorker(CrashReportData crashReportData, Spool.FileBeingProduced fileBeingProduced, CrashReportType[] crashReportTypeArr) {
            super("ReportsSenderWorker");
            this.exception = null;
            this.mGenerator = null;
            this.mInMemoryReportToSend = crashReportData;
            this.mReportFileUnderConstruction = fileBeingProduced;
            this.mReportTypesToSend = crashReportTypeArr;
        }

        public ReportsSenderWorker(ErrorReporter errorReporter, CrashReportType... crashReportTypeArr) {
            this(null, null, crashReportTypeArr);
        }
    }

    public interface SecureSettingsResolver {
        String getString(ContentResolver contentResolver, String str);
    }

    private void buildAttachmentWrapperCrashReport(CrashReportData crashReportData, CrashReportType crashReportType, Spool.FileBeingConsumed fileBeingConsumed, Writer writer, boolean z) {
        Writer writer2 = writer;
        CrashReportData crashReportData2 = crashReportData;
        try {
            crashReportData.put(ReportField.ACRA_REPORT_TYPE, crashReportType.name(), writer);
            AcraReportingConfig acraReportingConfig = this.mConfig;
            Spool.FileBeingConsumed fileBeingConsumed2 = null;
            CrashAttachmentException crashAttachmentException = new CrashAttachmentException();
            if (crashReportType == CrashReportType.NATIVE_CRASH_REPORT) {
                fileBeingConsumed2 = fileBeingConsumed;
            }
            CrashTimeDataCollector.gatherCrashData(this, acraReportingConfig, CRASH_ATTACHMENT_DUMMY_STACKTRACE, crashAttachmentException, crashReportData2, writer2, fileBeingConsumed2, z);
        } catch (Throwable th) {
            put(ReportField.REPORT_LOAD_THROW, AnonymousClass08S.A0J("retrieve exception: ", th.getMessage()), crashReportData, writer);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0075, code lost:
        if (0 == 0) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int buildCachedCrashReport(com.facebook.acra.ErrorReporter.CrashReportType r12, java.lang.String r13, java.io.File r14, com.facebook.acra.FileGenerator r15) {
        /*
            r11 = this;
            r6 = r12
            boolean r0 = r11.shouldSkipReport(r12)
            r4 = 0
            if (r0 != 0) goto L_0x008c
            if (r13 != 0) goto L_0x0014
            if (r14 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "stackTrace and traceFile can't be null at the same time"
            r1.<init>(r0)
            throw r1
        L_0x0014:
            r7 = 0
            if (r13 == 0) goto L_0x0018
            goto L_0x0033
        L_0x0018:
            if (r14 == 0) goto L_0x0037
            boolean r0 = r11.mustEmbedAttachments(r12)     // Catch:{ all -> 0x0043 }
            com.facebook.acra.CrashReportData r7 = r11.loadCrashAttachment(r14, r12, r0)     // Catch:{ all -> 0x0043 }
            if (r7 != 0) goto L_0x0037
            java.lang.String r2 = "ACRA"
            java.lang.String r1 = "Failed to load crash attachment report %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r14}     // Catch:{ all -> 0x0043 }
            X.C010708t.A0O(r2, r1, r0)     // Catch:{ all -> 0x0043 }
            deleteFile(r14)
            return r4
        L_0x0033:
            com.facebook.acra.CrashReportData r7 = r11.createCrashReportFromStackTrace(r13, r12)     // Catch:{ all -> 0x0043 }
        L_0x0037:
            r8 = 0
            r10 = 1
            r5 = r11
            r9 = r15
            int r0 = r5.maybeSendCrashReport(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0043 }
            int r4 = r4 + r0
            if (r7 == 0) goto L_0x007a
            goto L_0x0077
        L_0x0043:
            r3 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = "Failed to build cached crash report"
            r2.<init>(r0)     // Catch:{ all -> 0x0080 }
            if (r14 == 0) goto L_0x0055
            r0 = 32
            r2.append(r0)     // Catch:{ all -> 0x0080 }
            r2.append(r14)     // Catch:{ all -> 0x0080 }
        L_0x0055:
            java.lang.String r1 = "ACRA"
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0080 }
            X.C010708t.A0R(r1, r3, r0)     // Catch:{ all -> 0x0080 }
            java.lang.String r2 = "ANRValidationError<"
            java.lang.Class r0 = r3.getClass()     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = r0.getSimpleName()     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = "::Non-cached>"
            java.lang.String r1 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0080 }
            reportSoftError(r3, r1, r0, r11)     // Catch:{ all -> 0x0080 }
            if (r7 == 0) goto L_0x007a
        L_0x0077:
            r11.closeInputStreamFields(r7)
        L_0x007a:
            if (r14 == 0) goto L_0x008c
            deleteFile(r14)
            return r4
        L_0x0080:
            r0 = move-exception
            if (r7 == 0) goto L_0x0086
            r11.closeInputStreamFields(r7)
        L_0x0086:
            if (r14 == 0) goto L_0x008b
            deleteFile(r14)
        L_0x008b:
            throw r0
        L_0x008c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.buildCachedCrashReport(com.facebook.acra.ErrorReporter$CrashReportType, java.lang.String, java.io.File, com.facebook.acra.FileGenerator):int");
    }

    /* access modifiers changed from: private */
    public static boolean deleteFile(File file) {
        if (file == null) {
            return true;
        }
        boolean delete = file.delete();
        if (!delete && !file.exists()) {
            delete = true;
        }
        file.getName();
        if (!delete) {
            C010708t.A0P(ACRA.LOG_TAG, "Could not delete error report: %s", file.getName());
        }
        return delete;
    }

    private void discardOverlappingReports(CrashReportType... crashReportTypeArr) {
        for (CrashReportType crashReportType : crashReportTypeArr) {
            if ((crashReportType == CrashReportType.NATIVE_CRASH_REPORT || crashReportType == CrashReportType.ACRA_CRASH_REPORT) && roughlyCountPendingReportsOfType(crashReportType) > 0) {
                purgeDirectory(this.mContext.getDir(SIGQUIT_DIR, 0), null);
                return;
            }
        }
    }

    private int keepNLatestDumpFiles(File file) {
        File[] listFiles;
        if (file == null || !file.exists() || (listFiles = file.listFiles()) == null) {
            return 0;
        }
        Arrays.sort(listFiles, new Comparator() {
            public int compare(File file, File file2) {
                return Long.valueOf(file2.lastModified()).compareTo(Long.valueOf(file.lastModified()));
            }
        });
        int i = 0;
        int i2 = 0;
        for (File file2 : listFiles) {
            i2++;
            if (((long) i2) > 5) {
                file2.delete();
                i++;
            }
        }
        return i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void
     arg types: [com.facebook.acra.CrashReportData, int, java.io.Writer]
     candidates:
      ClspMth{java.util.HashMap.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void */
    private int maybeSendCrashReport(CrashReportType crashReportType, CrashReportData crashReportData, Spool.FileBeingConsumed fileBeingConsumed, FileGenerator fileGenerator, boolean z) {
        String str;
        boolean z2;
        String str2;
        Writer writer = null;
        CrashReportType crashReportType2 = crashReportType;
        Spool.FileBeingConsumed fileBeingConsumed2 = fileBeingConsumed;
        if (fileGenerator != null) {
            try {
                File generate = fileGenerator.generate();
                str = generate.getCanonicalPath();
                if (crashReportType == CrashReportType.ANR_REPORT && z) {
                    synchronized (this.mAnrFilesInProgress) {
                        this.mAnrFilesInProgress.add(str);
                    }
                }
                writer = CrashReportData.getWriter(new FileOutputStream(generate));
            } catch (Throwable th) {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        C010708t.A0R(ACRA.LOG_TAG, e, "IO Exception");
                        reportSoftError(e, "ANRValidationError<IOException::Non-cached>", e.toString(), this);
                    }
                }
                throw th;
            }
        } else {
            str = fileBeingConsumed.fileName.getName();
        }
        CrashReportData crashReportData2 = new CrashReportData();
        String str3 = crashReportType.attachmentField;
        if (str3 != null) {
            crashReportData2.put(crashReportType.attachmentField, (String) crashReportData.get(str3), writer);
        }
        if (crashReportType != CrashReportType.ANR_REPORT || (str2 = (String) crashReportData.get(ReportField.PROCESS_NAME)) == null) {
            z2 = true;
        } else {
            crashReportData2.put(ReportField.PROCESS_NAME, str2, writer);
            z2 = false;
        }
        buildAttachmentWrapperCrashReport(crashReportData2, crashReportType2, fileBeingConsumed2, writer, z2);
        if (crashReportType == CrashReportType.ANR_REPORT && !z) {
            crashReportData2.put(ErrorReportingConstants.ANR_PARTIAL_REPORT, "true", writer);
            crashReportData2.put(ErrorReportingConstants.ANR_RECOVERY_DELAY_TAG, ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL, writer);
            crashReportData2.put(ErrorReportingConstants.ANR_WITH_SIGQUIT_TRACES, "1", writer);
        }
        crashReportData2.put(ReportField.REPORT_ID, str.substring(0, str.lastIndexOf(46)), writer);
        crashReportData2.merge((Map) crashReportData, false, writer);
        crashReportData2.mergeFieldOverwrite(crashReportData, ReportField.APP_VERSION_CODE, writer);
        crashReportData2.mergeFieldOverwrite(crashReportData, ReportField.APP_VERSION_NAME, writer);
        crashReportData2.mergeFieldOverwrite(crashReportData, ReportField.SESSION_ID, writer);
        crashReportData2.put(ReportField.EXCEPTION_CAUSE, CRASH_ATTACHMENT_DUMMY_STACKTRACE, writer);
        if (writer == null) {
            sendCrashReport(crashReportData2);
        }
        if (writer != null) {
            try {
                writer.close();
                return 1;
            } catch (IOException e2) {
                C010708t.A0R(ACRA.LOG_TAG, e2, "IO Exception");
                reportSoftError(e2, "ANRValidationError<IOException::Non-cached>", e2.toString(), this);
            }
        }
        return 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00ea, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00eb, code lost:
        if (r11 != null) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:71:0x00f0 */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00da A[SYNTHETIC, Splitter:B:55:0x00da] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int processCrashAttachmentsLocked(int r15, com.facebook.acra.ErrorReporter.CrashReportType r16, com.facebook.acra.FileGenerator r17, boolean r18) {
        /*
            r14 = this;
            r8 = r14
            r9 = r16
            boolean r1 = r14.shouldSkipReport(r9)
            r0 = 0
            if (r1 == 0) goto L_0x000b
            return r0
        L_0x000b:
            com.facebook.acra.ErrorReporter$CrashReportType r0 = com.facebook.acra.ErrorReporter.CrashReportType.NATIVE_CRASH_REPORT
            if (r9 != r0) goto L_0x0027
            android.content.Context r1 = r14.mContext
            java.lang.String r0 = "android_acra_save_native_reports"
            boolean r0 = X.AnonymousClass08X.A07(r1, r0)
            if (r0 == 0) goto L_0x0027
            java.io.File r4 = r14.createBackUpDirectory(r9)
        L_0x001d:
            android.content.Context r0 = r14.mContext
            com.facebook.acra.Spool$Snapshot r7 = com.facebook.acra.ErrorReporter.CrashReportType.getPendingCrashReports(r9, r0)
            r1 = 0
            r5 = 0
            r6 = 0
            goto L_0x0029
        L_0x0027:
            r4 = 0
            goto L_0x001d
        L_0x0029:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x00f1
            if (r5 >= r15) goto L_0x00f1
            com.facebook.acra.Spool$FileBeingConsumed r11 = r7.next()     // Catch:{ all -> 0x00f8 }
            int r2 = r1 + 1
            r0 = 5
            if (r1 < r0) goto L_0x0040
            java.io.File r0 = r11.fileName     // Catch:{ all -> 0x00e8 }
            deleteFile(r0)     // Catch:{ all -> 0x00e8 }
            goto L_0x0068
        L_0x0040:
            boolean r0 = r14.mustEmbedAttachments(r9)     // Catch:{ ReportSenderException -> 0x00a3, all -> 0x0077 }
            com.facebook.acra.CrashReportData r10 = r14.loadCrashAttachment(r11, r9, r0)     // Catch:{ ReportSenderException -> 0x00a3, all -> 0x0077 }
            if (r10 == 0) goto L_0x0066
            r14.processCrashAttachmentBeforeSend(r11, r9, r10, r0)     // Catch:{ ReportSenderException -> 0x0074, all -> 0x0071 }
            java.lang.String r1 = getLogcatFileName(r11, r9)     // Catch:{ ReportSenderException -> 0x0074, all -> 0x0071 }
            r14.updateGLCwithSystemLibs(r11)     // Catch:{ ReportSenderException -> 0x0074, all -> 0x0071 }
            r3 = r10
            r12 = r17
            r13 = r18
            int r0 = r8.maybeSendCrashReport(r9, r10, r11, r12, r13)     // Catch:{ ReportSenderException -> 0x006f, all -> 0x006d }
            int r6 = r6 + r0
            r14.processCrashAttachmentAfterSend(r11, r9, r4, r1)     // Catch:{ ReportSenderException -> 0x006f, all -> 0x006d }
            int r5 = r5 + 1
            r14.closeInputStreamFields(r10)     // Catch:{ all -> 0x00e8 }
        L_0x0066:
            if (r11 == 0) goto L_0x006b
        L_0x0068:
            r11.close()     // Catch:{ all -> 0x00f8 }
        L_0x006b:
            r1 = r2
            goto L_0x0029
        L_0x006d:
            r5 = move-exception
            goto L_0x0079
        L_0x006f:
            r5 = move-exception
            goto L_0x00a5
        L_0x0071:
            r5 = move-exception
            r3 = r10
            goto L_0x0079
        L_0x0074:
            r5 = move-exception
            r3 = r10
            goto L_0x00a5
        L_0x0077:
            r5 = move-exception
            r3 = 0
        L_0x0079:
            java.lang.String r2 = "ACRA"
            java.lang.String r1 = "Failed on crash attachment file %s"
            java.io.File r0 = r11.fileName     // Catch:{ all -> 0x00e1 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00e1 }
            X.C010708t.A0U(r2, r5, r1, r0)     // Catch:{ all -> 0x00e1 }
            java.lang.String r2 = "ANRValidationError<"
            java.lang.Class r0 = r5.getClass()     // Catch:{ all -> 0x00e1 }
            java.lang.String r1 = r0.getSimpleName()     // Catch:{ all -> 0x00e1 }
            java.lang.String r0 = "::Non-cached>"
            java.lang.String r1 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x00e1 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x00e1 }
            reportSoftError(r5, r1, r0, r14)     // Catch:{ all -> 0x00e1 }
            java.io.File r0 = r11.fileName     // Catch:{ all -> 0x00e1 }
            deleteFile(r0)     // Catch:{ all -> 0x00e1 }
            goto L_0x00d8
        L_0x00a3:
            r5 = move-exception
            r3 = 0
        L_0x00a5:
            java.lang.String r2 = "ACRA"
            java.lang.String r1 = "Failed to send crash attachment report %s"
            java.io.File r0 = r11.fileName     // Catch:{ all -> 0x00e1 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00e1 }
            X.C010708t.A0U(r2, r5, r1, r0)     // Catch:{ all -> 0x00e1 }
            com.facebook.acra.config.AcraReportingConfig r0 = r14.mConfig     // Catch:{ all -> 0x00e1 }
            boolean r0 = r0.shouldSkipReportOnSocketTimeout()     // Catch:{ all -> 0x00e1 }
            if (r0 == 0) goto L_0x00d8
            java.lang.Throwable r0 = r5.getCause()     // Catch:{ all -> 0x00e1 }
            if (r0 == 0) goto L_0x00d8
            java.lang.Throwable r0 = r5.getCause()     // Catch:{ all -> 0x00e1 }
            boolean r0 = r0 instanceof java.net.SocketTimeoutException     // Catch:{ all -> 0x00e1 }
            if (r0 == 0) goto L_0x00d8
            java.lang.String r1 = "Timeout while sending, deleting report %s"
            java.io.File r0 = r11.fileName     // Catch:{ all -> 0x00e1 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00e1 }
            X.C010708t.A0U(r2, r5, r1, r0)     // Catch:{ all -> 0x00e1 }
            java.io.File r0 = r11.fileName     // Catch:{ all -> 0x00e1 }
            deleteFile(r0)     // Catch:{ all -> 0x00e1 }
        L_0x00d8:
            if (r3 == 0) goto L_0x00dd
            r14.closeInputStreamFields(r3)     // Catch:{ all -> 0x00e8 }
        L_0x00dd:
            r11.close()     // Catch:{ all -> 0x00f8 }
            goto L_0x00f1
        L_0x00e1:
            r0 = move-exception
            if (r3 == 0) goto L_0x00e7
            r14.closeInputStreamFields(r3)     // Catch:{ all -> 0x00e8 }
        L_0x00e7:
            throw r0     // Catch:{ all -> 0x00e8 }
        L_0x00e8:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ea }
        L_0x00ea:
            r0 = move-exception
            if (r11 == 0) goto L_0x00f0
            r11.close()     // Catch:{ all -> 0x00f0 }
        L_0x00f0:
            throw r0     // Catch:{ all -> 0x00f8 }
        L_0x00f1:
            r7.close()
            r14.keepNLatestDumpFiles(r4)
            return r6
        L_0x00f8:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00fa }
        L_0x00fa:
            r0 = move-exception
            if (r7 == 0) goto L_0x0100
            r7.close()     // Catch:{ all -> 0x0100 }
        L_0x0100:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.processCrashAttachmentsLocked(int, com.facebook.acra.ErrorReporter$CrashReportType, com.facebook.acra.FileGenerator, boolean):int");
    }

    /* access modifiers changed from: private */
    public synchronized void startUploadIfReady() {
        if (this.mConfig.shouldUseUploadService() && this.mInitializationComplete && this.mBatchUploader != null && this.mFinishedCheckingReports && this.mPendingReportWriters <= 0 && !this.mConfig.shouldOnlyWriteReport() && !this.mStartedBatchUploader) {
            final File[] listFiles = this.mContext.getDir(ErrorReportingConstants.TRACE_UPLOAD_DIR, 0).listFiles();
            this.mStartedBatchUploader = true;
            new Thread(new Runnable() {
                public void run() {
                    ErrorReporter.this.mBatchUploader.uploadReports(listFiles);
                }
            }).start();
        }
    }

    private void uncaughtExceptionImpl(Thread thread, Throwable th, boolean z) {
        Object obj;
        this.mOomReservation = null;
        discardOverlappingReports(CrashReportType.ACRA_CRASH_REPORT);
        try {
            if (Build.VERSION.SDK_INT >= 9) {
                Api9Utils.disableStrictMode();
            }
        } catch (Throwable th2) {
            tryLogInternalError(th2);
        }
        int i = 3;
        try {
            C010708t.A0O(ACRA.LOG_TAG, "ACRA caught a %s exception for %s version %s. Building report.", th.getClass().getSimpleName(), this.mContext.getPackageName(), this.mAppVersionCode);
        } catch (Throwable th3) {
            tryLogInternalError(th3);
        }
        TreeMap treeMap = new TreeMap();
        Throwable translateException = translateException(th, treeMap);
        if (translateException != null) {
            if (z) {
                i = 4;
            }
            if (!this.mConfig.shouldImmediatelyUpload()) {
                i = 4;
            }
            if (getMostSignificantCause(translateException) instanceof OutOfMemoryError) {
                i &= -2;
            }
            try {
                BlackBoxRecorderControl blackBoxRecorderControl = this.mBlackBoxRecorderControl;
                if (blackBoxRecorderControl != null) {
                    obj = blackBoxRecorderControl.captureBlackBoxTrace(treeMap, 1);
                } else {
                    obj = null;
                }
                handleExceptionInternal(translateException, new CrashReportData(treeMap), null, i);
                if (blackBoxRecorderControl != null) {
                    blackBoxRecorderControl.awaitForBlackBoxTraceCompletion(obj);
                }
            } catch (Throwable th4) {
                if (!z) {
                    C010708t.A0R(ACRA.LOG_TAG, th4, "error during error reporting: will attempt to report error");
                    uncaughtExceptionImpl(thread, th4, true);
                    return;
                }
                throw th4;
            }
        }
    }

    public HashSet getNewLibs(File file, HashSet hashSet) {
        BufferedReader bufferedReader = null;
        if (hashSet == null || hashSet.isEmpty()) {
            return null;
        }
        try {
            sSystemLibFileLock.readLock().lock();
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file));
            while (true) {
                try {
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null) {
                        break;
                    } else if (hashSet.contains(readLine)) {
                        hashSet.remove(readLine);
                    }
                } catch (IOException e) {
                    e = e;
                    bufferedReader = bufferedReader2;
                    try {
                        C010708t.A0R(ACRA.LOG_TAG, e, "GLC getNewLibs IO Exception");
                        safeClose(bufferedReader);
                        sSystemLibFileLock.readLock().unlock();
                        return hashSet;
                    } catch (Throwable th) {
                        th = th;
                        safeClose(bufferedReader);
                        sSystemLibFileLock.readLock().unlock();
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = bufferedReader2;
                    safeClose(bufferedReader);
                    sSystemLibFileLock.readLock().unlock();
                    throw th;
                }
            }
            safeClose(bufferedReader2);
        } catch (IOException e2) {
            e = e2;
            C010708t.A0R(ACRA.LOG_TAG, e, "GLC getNewLibs IO Exception");
            safeClose(bufferedReader);
            sSystemLibFileLock.readLock().unlock();
            return hashSet;
        }
        sSystemLibFileLock.readLock().unlock();
        return hashSet;
    }

    public String getSigquitTracesExtension() {
        return REPORTFILE_EXTENSION;
    }

    public void handleExceptionDelayed(Throwable th, CrashReportData crashReportData) {
        handleExceptionInternal(th, crashReportData, null, 0);
    }

    public synchronized void setBatchUploader(BatchUploader batchUploader) {
        if (this.mConfig != null && this.mConfig.shouldUseUploadService()) {
            this.mBatchUploader = batchUploader;
            startUploadIfReady();
        }
    }

    public class DefaultSecureSettingsResolver implements SecureSettingsResolver {
        public String getString(ContentResolver contentResolver, String str) {
            return Settings.Secure.getString(contentResolver, str);
        }
    }

    public class Holder {
        public static final ErrorReporter ERROR_REPORTER = new ErrorReporter();

        private Holder() {
        }
    }

    public static /* synthetic */ int access$908(ErrorReporter errorReporter) {
        int i = errorReporter.mPendingReportWriters;
        errorReporter.mPendingReportWriters = i + 1;
        return i;
    }

    public static /* synthetic */ int access$910(ErrorReporter errorReporter) {
        int i = errorReporter.mPendingReportWriters;
        errorReporter.mPendingReportWriters = i - 1;
        return i;
    }

    private void addCriticalData() {
        String userId = CriticalAppData.getUserId(this.mContext);
        String clientUserId = CriticalAppData.getClientUserId(this.mContext);
        String deviceId = CriticalAppData.getDeviceId(this.mContext);
        if (userId.length() > 0) {
            setUserId(userId);
        }
        if (clientUserId.length() > 0) {
            setClientUserId(clientUserId);
        }
        if (deviceId.length() > 0) {
            putCustomData(ErrorReportingConstants.DEVICE_ID_KEY, deviceId);
        }
        for (Map.Entry entry : CriticalAppData.getAdditionalParamValues(this.mContext).entrySet()) {
            String str = (String) entry.getValue();
            if (!TextUtils.isEmpty(str)) {
                putCustomData((String) entry.getKey(), str);
            }
        }
    }

    private void attachIabInfo(CrashReportData crashReportData) {
        File file = new File(this.mContext.getFilesDir(), FILE_IAB_OPEN_TIMES);
        String readFile = readFile(file);
        if (NO_FILE.equals(readFile)) {
            crashReportData.put(ReportField.IAB_OPEN_TIMES, "0");
        } else if (readFile != null) {
            crashReportData.put(ReportField.IAB_OPEN_TIMES, readFile);
        }
        if (file.exists()) {
            file.delete();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:19|20|21|22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x004d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void attachLastActivityInfo(com.facebook.acra.CrashReportData r7) {
        /*
            r6 = this;
            java.io.File r5 = new java.io.File
            android.content.Context r0 = r6.mContext
            java.io.File r1 = r0.getFilesDir()
            java.lang.String r0 = "last_activity_opened"
            r5.<init>(r1, r0)
            boolean r0 = r5.exists()
            java.lang.String r1 = "LAST_ACTIVITY_LOGGED"
            if (r0 != 0) goto L_0x001b
            java.lang.String r0 = "NO_FILE"
            r7.put(r1, r0)
            return
        L_0x001b:
            java.io.FileReader r4 = new java.io.FileReader
            r4.<init>(r5)
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x004e }
            r0 = 1024(0x400, float:1.435E-42)
            r3.<init>(r4, r0)     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r3.readLine()     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x003d
            r7.put(r1, r0)     // Catch:{ all -> 0x0047 }
            java.lang.String r2 = "LAST_ACTIVITY_LOGGED_TIME"
            long r0 = r5.lastModified()     // Catch:{ all -> 0x0047 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x0047 }
            r7.put(r2, r0)     // Catch:{ all -> 0x0047 }
        L_0x003d:
            r5.delete()     // Catch:{ all -> 0x0047 }
            r3.close()     // Catch:{ all -> 0x004e }
            r4.close()
            return
        L_0x0047:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x004d }
        L_0x004d:
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0050 }
        L_0x0050:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x0054 }
        L_0x0054:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.attachLastActivityInfo(com.facebook.acra.CrashReportData):void");
    }

    private void cancelBlockingNotification() {
        ((NotificationManager) this.mContext.getSystemService("notification")).cancel(2);
    }

    private void checkNativeReportsOnApplicationStart() {
        int roughlyCountPendingReportsOfType = roughlyCountPendingReportsOfType(CrashReportType.NATIVE_CRASH_REPORT);
        int maxPendingMiniDumpReports = this.mConfig.getMaxPendingMiniDumpReports();
        if (maxPendingMiniDumpReports < Integer.MAX_VALUE && roughlyCountPendingReportsOfType > maxPendingMiniDumpReports) {
            C010708t.A0P(TAG, "Minidump count above threshold %d", Integer.valueOf(roughlyCountPendingReportsOfType));
            removeCrashFiles(CrashReportType.NATIVE_CRASH_REPORT, roughlyCountPendingReportsOfType - maxPendingMiniDumpReports);
        }
        if (roughlyCountPendingReportsOfType > 5) {
            ReportsSenderWorker reportsSenderWorker = new ReportsSenderWorker(this, CrashReportType.NATIVE_CRASH_REPORT);
            Object obj = null;
            if (Build.VERSION.SDK_INT >= 9) {
                obj = Api9Utils.saveStrictMode();
                Api9Utils.disableStrictMode();
            }
            try {
                reportsSenderWorker.doSend();
            } catch (Throwable th) {
                if (obj != null) {
                    Api9Utils.restoreStrictMode(obj);
                }
                throw th;
            }
            if (obj != null) {
                Api9Utils.restoreStrictMode(obj);
            }
        }
    }

    private void closeInputStreamFields(CrashReportData crashReportData) {
        InputStream inputStream;
        for (Map.Entry value : crashReportData.mInputStreamFields.entrySet()) {
            InputStreamField inputStreamField = (InputStreamField) value.getValue();
            if (!(inputStreamField == null || (inputStream = inputStreamField.mInputStream) == null)) {
                try {
                    inputStream.close();
                } catch (IOException unused) {
                }
            }
        }
    }

    public static boolean containsKeyInCustomData(String str) {
        return ProxyCustomDataStore.Holder.CUSTOM_DATA.containsKey(str);
    }

    private File createBackUpDirectory(CrashReportType crashReportType) {
        File file;
        try {
            file = new File(this.mContext.getDir(crashReportType.directory, 0).getParent(), REPORTED_CRASH_DIR);
            try {
                if (!file.exists()) {
                    file.mkdir();
                    return file;
                }
            } catch (NullPointerException e) {
                e = e;
                C010708t.A0U(ACRA.LOG_TAG, e, "Failed to create backup directory %s", REPORTED_CRASH_DIR);
                return file;
            }
        } catch (NullPointerException e2) {
            e = e2;
            file = null;
            C010708t.A0U(ACRA.LOG_TAG, e, "Failed to create backup directory %s", REPORTED_CRASH_DIR);
            return file;
        }
        return file;
    }

    private CrashReportData createCrashReportFromStackTrace(String str, CrashReportType crashReportType) {
        CrashReportData crashReportData = new CrashReportData();
        crashReportData.put(ReportField.TIME_OF_CRASH, Long.toString(System.currentTimeMillis()));
        try {
            crashReportData.put(crashReportType.attachmentField, AttachmentUtil.compressToBase64String(str.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
            crashReportData.put(ReportField.REPORT_LOAD_THROW, AnonymousClass08S.A0J("throwable: ", e.getMessage()));
            C010708t.A0R(ACRA.LOG_TAG, e, "Could not load crash report. File will be deleted.");
        }
        backfillCrashReportData(crashReportData);
        return crashReportData;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0017, code lost:
        if (r1.equals("false") == false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void foregroundNativeCrashDetect(com.facebook.acra.Spool.FileBeingConsumed r6) {
        /*
            r5 = this;
            com.facebook.acra.util.minidump.MinidumpReader r2 = new com.facebook.acra.util.minidump.MinidumpReader     // Catch:{ Exception -> 0x0054 }
            java.io.RandomAccessFile r0 = r6.file     // Catch:{ Exception -> 0x0054 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r0 = "APP_STARTED_IN_BACKGROUND"
            java.lang.String r1 = r2.getCustomData(r0)     // Catch:{ Exception -> 0x0054 }
            r4 = 0
            if (r1 == 0) goto L_0x0019
            java.lang.String r0 = "false"
            boolean r0 = r1.equals(r0)     // Catch:{ Exception -> 0x0054 }
            r3 = 1
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r3 = 0
        L_0x001a:
            r0 = -87110452(0xfffffffffacecccc, float:-5.3688346E35)
            java.lang.String r2 = r2.getString(r0)     // Catch:{ Exception -> 0x0054 }
            r1 = -1
            if (r2 == 0) goto L_0x002c
            java.lang.String r0 = "Resumed"
            int r0 = r2.indexOf(r0)     // Catch:{ Exception -> 0x0054 }
            if (r0 != r1) goto L_0x0038
        L_0x002c:
            if (r3 == 0) goto L_0x005d
            if (r2 == 0) goto L_0x005d
            java.lang.String r0 = "\"activities\":[]"
            int r0 = r2.indexOf(r0)     // Catch:{ Exception -> 0x0054 }
            if (r0 == r1) goto L_0x005d
        L_0x0038:
            android.content.Context r1 = r5.mContext     // Catch:{ Exception -> 0x0054 }
            java.lang.String r0 = "FacebookApplication"
            android.content.SharedPreferences r0 = r1.getSharedPreferences(r0, r4)     // Catch:{ Exception -> 0x0054 }
            android.content.SharedPreferences$Editor r3 = r0.edit()     // Catch:{ Exception -> 0x0054 }
            java.lang.String r2 = "crash_foreground_timestamp"
            java.io.File r0 = r6.fileName     // Catch:{ Exception -> 0x0054 }
            long r0 = r0.lastModified()     // Catch:{ Exception -> 0x0054 }
            android.content.SharedPreferences$Editor r0 = r3.putLong(r2, r0)     // Catch:{ Exception -> 0x0054 }
            r0.commit()     // Catch:{ Exception -> 0x0054 }
            return
        L_0x0054:
            r3 = move-exception
            java.lang.String r2 = "ACRA"
            r1 = 0
            java.lang.String r0 = "Error writing into the SharedPreferences"
            r5.writeToLogBridge(r2, r0, r3, r1)
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.foregroundNativeCrashDetect(com.facebook.acra.Spool$FileBeingConsumed):void");
    }

    public static File getCrashDumpSysLibPath(Context context) {
        return context.getFileStreamPath(CRASH_DUMP_SYS_LIBS_FILE);
    }

    public static String getCustomData(String str) {
        return ProxyCustomDataStore.Holder.CUSTOM_DATA.getCustomData(str);
    }

    private static String getLogcatFileName(Spool.FileBeingConsumed fileBeingConsumed, CrashReportType crashReportType) {
        if (crashReportType != CrashReportType.NATIVE_CRASH_REPORT) {
            return null;
        }
        try {
            return new MinidumpReader(fileBeingConsumed.file).getCustomData(ACRA.LOGCAT_FILE_KEY);
        } catch (IOException e) {
            C010708t.A0U(ACRA.LOG_TAG, e, "Failed to find logcat file %s", fileBeingConsumed.fileName);
            return null;
        }
    }

    public static Throwable getMostSignificantCause(Throwable th) {
        if (!(th instanceof NonCrashException)) {
            while (th.getCause() != null) {
                th = th.getCause();
            }
        }
        return th;
    }

    public static int getSendAttempts() {
        return mSendAttempts.get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0057, code lost:
        if (r0 != 1) goto L_0x0059;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0187 A[Catch:{ all -> 0x00ab, all -> 0x01e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x01dc A[Catch:{ all -> 0x00ab, all -> 0x01e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006e A[Catch:{ all -> 0x0065, all -> 0x01e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008c A[Catch:{ all -> 0x0084, all -> 0x01ed }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0091 A[SYNTHETIC, Splitter:B:40:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00a6 A[SYNTHETIC, Splitter:B:47:0x00a6] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00b3 A[SYNTHETIC, Splitter:B:53:0x00b3] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00db A[Catch:{ all -> 0x014a }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00ee A[Catch:{ all -> 0x014a }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f7 A[SYNTHETIC, Splitter:B:66:0x00f7] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00fd A[SYNTHETIC, Splitter:B:70:0x00fd] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0167 A[SYNTHETIC, Splitter:B:97:0x0167] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.acra.ErrorReporter.ReportsSenderWorker handleExceptionInternal(java.lang.Throwable r18, com.facebook.acra.CrashReportData r19, java.lang.String r20, int r21) {
        /*
            r17 = this;
            r11 = r20
            r9 = r17
            boolean r0 = r9.mInitializationComplete
            r2 = 0
            if (r0 != 0) goto L_0x000a
            return r2
        L_0x000a:
            java.lang.Object r1 = com.facebook.acra.ErrorReporter.UNCAUGHT_EXCEPTION_LOCK
            monitor-enter(r1)
            r1.notify()     // Catch:{ all -> 0x01f9 }
            monitor-exit(r1)     // Catch:{ all -> 0x01f9 }
            r12 = r18
            java.lang.Throwable r0 = getMostSignificantCause(r12)
            java.lang.Class r5 = r0.getClass()
            com.facebook.acra.CrashReportData r13 = new com.facebook.acra.CrashReportData
            r13.<init>()
            boolean r4 = r12 instanceof com.facebook.acra.NonCrashException
            r3 = 2
            r1 = 1
            if (r4 == 0) goto L_0x0059
            com.facebook.acra.config.AcraReportingConfig r0 = r9.mConfig
            int r0 = r0.reportSoftErrorsImmediately()
            if (r0 != 0) goto L_0x0050
            r8 = r21 | 4
        L_0x0030:
            r0 = r8 & 4
            if (r0 == 0) goto L_0x0038
            r8 = r8 & -4
            r13.throwAwayWrites = r1
        L_0x0038:
            if (r4 == 0) goto L_0x004d
            r0 = r12
            com.facebook.acra.NonCrashException r0 = (com.facebook.acra.NonCrashException) r0
            java.lang.String r3 = r0.getExceptionFriendlyName()
        L_0x0041:
            java.lang.String r1 = "ACRA"
            java.lang.String r0 = "Handling exception for "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r9.writeToLogBridge(r1, r0, r12, r11)
            goto L_0x005c
        L_0x004d:
            java.lang.String r3 = "crash"
            goto L_0x0041
        L_0x0050:
            if (r0 != r3) goto L_0x0055
            r8 = r21 | 1
            goto L_0x0030
        L_0x0055:
            r8 = r21 | 3
            if (r0 == r1) goto L_0x0030
        L_0x0059:
            r8 = r21
            goto L_0x0030
        L_0x005c:
            com.facebook.acra.ErrorReporter$CrashReportType r1 = com.facebook.acra.ErrorReporter.CrashReportType.ACRA_CRASH_REPORT     // Catch:{ all -> 0x0065 }
            android.content.Context r0 = r9.mContext     // Catch:{ all -> 0x0065 }
            com.facebook.acra.Spool r1 = r1.getSpool(r0)     // Catch:{ all -> 0x0065 }
            goto L_0x006c
        L_0x0065:
            r0 = move-exception
            r13.generatingIoError = r0     // Catch:{ all -> 0x01e9 }
            r9.tryLogInternalError(r0)     // Catch:{ all -> 0x01e9 }
            r1 = r2
        L_0x006c:
            if (r1 == 0) goto L_0x008c
            r4 = r2
            r6 = r2
            r7 = r2
        L_0x0071:
            if (r4 != 0) goto L_0x008f
            java.util.UUID r6 = com.facebook.acra.util.CrashTimeDataCollectorHelper.generateReportUuid()     // Catch:{ all -> 0x0084 }
            java.lang.String r0 = ".stacktrace"
            java.lang.String r7 = r9.genCrashReportFileName(r5, r6, r0)     // Catch:{ all -> 0x0084 }
            java.io.File r0 = r9.mPreallocFileName     // Catch:{ all -> 0x0084 }
            com.facebook.acra.Spool$FileBeingProduced r4 = r1.produceWithDonorFile(r7, r0)     // Catch:{ all -> 0x0084 }
            goto L_0x0071
        L_0x0084:
            r0 = move-exception
            r13.generatingIoError = r0     // Catch:{ all -> 0x01ed }
            r9.tryLogInternalError(r0)     // Catch:{ all -> 0x01ed }
            r7 = r2
            goto L_0x008f
        L_0x008c:
            r6 = r2
            r7 = r2
            r4 = r2
        L_0x008f:
            if (r4 == 0) goto L_0x00a3
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ all -> 0x009d }
            java.io.RandomAccessFile r0 = r4.file     // Catch:{ all -> 0x009d }
            java.io.FileDescriptor r0 = r0.getFD()     // Catch:{ all -> 0x009d }
            r3.<init>(r0)     // Catch:{ all -> 0x009d }
            goto L_0x00a4
        L_0x009d:
            r0 = move-exception
            r13.generatingIoError = r0     // Catch:{ all -> 0x01ed }
            r9.tryLogInternalError(r0)     // Catch:{ all -> 0x01ed }
        L_0x00a3:
            r3 = r2
        L_0x00a4:
            if (r3 == 0) goto L_0x00b1
            java.io.Writer r2 = com.facebook.acra.CrashReportData.getWriter(r3)     // Catch:{ all -> 0x00ab }
            goto L_0x00b1
        L_0x00ab:
            r0 = move-exception
            r13.generatingIoError = r0     // Catch:{ all -> 0x01e7 }
            r9.tryLogInternalError(r0)     // Catch:{ all -> 0x01e7 }
        L_0x00b1:
            if (r6 != 0) goto L_0x00b7
            java.util.UUID r6 = com.facebook.acra.util.CrashTimeDataCollectorHelper.generateReportUuid()     // Catch:{ all -> 0x014a }
        L_0x00b7:
            java.lang.String r1 = "ACRA_REPORT_TYPE"
            com.facebook.acra.ErrorReporter$CrashReportType r0 = com.facebook.acra.ErrorReporter.CrashReportType.ACRA_CRASH_REPORT     // Catch:{ all -> 0x014a }
            java.lang.String r0 = r0.name()     // Catch:{ all -> 0x014a }
            put(r1, r0, r13, r2)     // Catch:{ all -> 0x014a }
            com.facebook.acra.config.AcraReportingConfig r0 = r9.mConfig     // Catch:{ all -> 0x014a }
            java.lang.String r1 = "ACRA_REPORT_FILENAME"
            boolean r0 = r0.shouldReportField(r1)     // Catch:{ all -> 0x014a }
            if (r0 == 0) goto L_0x00d1
            if (r7 == 0) goto L_0x00d1
            put(r1, r7, r13, r2)     // Catch:{ all -> 0x014a }
        L_0x00d1:
            com.facebook.acra.config.AcraReportingConfig r1 = r9.mConfig     // Catch:{ all -> 0x014a }
            java.lang.String r0 = "REPORT_ID"
            boolean r0 = r1.shouldReportField(r0)     // Catch:{ all -> 0x014a }
            if (r0 == 0) goto L_0x00e4
            java.lang.String r1 = "REPORT_ID"
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x014a }
            put(r1, r0, r13, r2)     // Catch:{ all -> 0x014a }
        L_0x00e4:
            com.facebook.acra.config.AcraReportingConfig r0 = r9.mConfig     // Catch:{ all -> 0x014a }
            java.lang.String r1 = "EXCEPTION_CAUSE"
            boolean r0 = r0.shouldReportField(r1)     // Catch:{ all -> 0x014a }
            if (r0 == 0) goto L_0x00f5
            java.lang.String r0 = r5.getName()     // Catch:{ all -> 0x014a }
            put(r1, r0, r13, r2)     // Catch:{ all -> 0x014a }
        L_0x00f5:
            if (r20 != 0) goto L_0x00fb
            java.lang.String r11 = throwableToString(r12)     // Catch:{ all -> 0x00fb }
        L_0x00fb:
            if (r19 == 0) goto L_0x0140
            java.util.Set r0 = r19.entrySet()     // Catch:{ all -> 0x013a }
            java.util.Iterator r6 = r0.iterator()     // Catch:{ all -> 0x013a }
        L_0x0105:
            boolean r0 = r6.hasNext()     // Catch:{ all -> 0x013a }
            if (r0 == 0) goto L_0x0140
            java.lang.Object r5 = r6.next()     // Catch:{ all -> 0x013a }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x013a }
            java.lang.Object r1 = r5.getKey()     // Catch:{ all -> 0x013a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x013a }
            com.facebook.acra.config.AcraReportingConfig r0 = r9.mConfig     // Catch:{ all -> 0x013a }
            boolean r0 = com.facebook.acra.CrashTimeDataCollector.shouldAddField(r1, r13, r0)     // Catch:{ all -> 0x013a }
            if (r0 == 0) goto L_0x0105
            java.lang.Object r1 = r5.getKey()     // Catch:{ all -> 0x012f }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x012f }
            java.lang.Object r0 = r5.getValue()     // Catch:{ all -> 0x012f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x012f }
            put(r1, r0, r13, r2)     // Catch:{ all -> 0x012f }
            goto L_0x0105
        L_0x012f:
            r1 = move-exception
            java.lang.Object r0 = r5.getKey()     // Catch:{ all -> 0x013a }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x013a }
            com.facebook.acra.CrashTimeDataCollector.noteReportFieldFailure(r13, r0, r1)     // Catch:{ all -> 0x013a }
            goto L_0x0105
        L_0x013a:
            r1 = move-exception
            java.lang.String r0 = "pre-populating fields from draft"
            r9.tryLogInternalError(r0, r1)     // Catch:{ all -> 0x014a }
        L_0x0140:
            com.facebook.acra.config.AcraReportingConfig r10 = r9.mConfig     // Catch:{ all -> 0x014a }
            r15 = 0
            r14 = r2
            r16 = 1
            com.facebook.acra.CrashTimeDataCollector.gatherCrashData(r9, r10, r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x014a }
            goto L_0x0165
        L_0x014a:
            r5 = move-exception
            java.lang.String r0 = "gathering crash data"
            r9.tryLogInternalError(r0, r5)     // Catch:{ all -> 0x01e7 }
            java.lang.String r1 = "ACRA_INTERNAL"
            java.lang.String r0 = throwableToString(r5)     // Catch:{ all -> 0x015a }
            put(r1, r0, r13, r2)     // Catch:{ all -> 0x015a }
            goto L_0x0165
        L_0x015a:
            r0 = move-exception
            r9.tryLogInternalError(r0)     // Catch:{ all -> 0x01e7 }
            java.lang.String r1 = "ACRA_INTERNAL"
            java.lang.String r0 = "ACRA_INTERNAL=java.lang.Exception: An exception occurred while trying to collect data about an ACRA internal error\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:810)\n\tat com.facebook.acra.ErrorReporter.handleException(ErrorReporter.java:866)\n\tat com.facebook.acra.ErrorReporter.uncaughtException(ErrorReporter.java:666)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:693)\n\tat java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:690)\n"
            put(r1, r0, r13, r2)     // Catch:{ all -> 0x01e7 }
        L_0x0165:
            if (r2 == 0) goto L_0x0183
            r2.flush()     // Catch:{ all -> 0x017d }
            if (r3 == 0) goto L_0x016f
            r3.flush()     // Catch:{ all -> 0x017d }
        L_0x016f:
            java.io.RandomAccessFile r0 = r4.file     // Catch:{ all -> 0x017d }
            java.nio.channels.FileChannel r5 = r0.getChannel()     // Catch:{ all -> 0x017d }
            long r0 = r5.position()     // Catch:{ all -> 0x017d }
            r5.truncate(r0)     // Catch:{ all -> 0x017d }
            goto L_0x0183
        L_0x017d:
            r0 = move-exception
            r13.generatingIoError = r0     // Catch:{ all -> 0x01e7 }
            r9.tryLogInternalError(r0)     // Catch:{ all -> 0x01e7 }
        L_0x0183:
            java.lang.Throwable r0 = r13.generatingIoError     // Catch:{ all -> 0x01e7 }
            if (r0 == 0) goto L_0x0190
            java.lang.String r1 = "GENERATING_IO_ERROR"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01e7 }
            put(r1, r0, r13, r2)     // Catch:{ all -> 0x01e7 }
        L_0x0190:
            java.lang.Throwable r0 = r13.generatingIoError     // Catch:{ all -> 0x01e7 }
            if (r0 == 0) goto L_0x01a2
            r0 = r8 & 4
            if (r0 != 0) goto L_0x01a2
            com.facebook.acra.config.AcraReportingConfig r0 = r9.mConfig     // Catch:{ all -> 0x01e7 }
            boolean r0 = r0.shouldImmediatelyUpload()     // Catch:{ all -> 0x01e7 }
            if (r0 == 0) goto L_0x01a2
            r8 = r8 | 1
        L_0x01a2:
            r0 = r8 & 1
            if (r0 == 0) goto L_0x01dc
            java.lang.String r1 = "SENT_IMMEDIATELY"
            java.lang.String r0 = "true"
            r13.put(r1, r0)     // Catch:{ all -> 0x01d4 }
            com.facebook.acra.ErrorReporter$ReportsSenderWorker r5 = new com.facebook.acra.ErrorReporter$ReportsSenderWorker     // Catch:{ all -> 0x01d4 }
            r5.<init>(r9, r13, r4)     // Catch:{ all -> 0x01d4 }
            r0 = r8 & 2
            if (r0 == 0) goto L_0x01cb
            r5.doSend()     // Catch:{ all -> 0x01d2 }
            java.util.concurrent.atomic.AtomicReference r0 = r9.mCrashReportedObserver     // Catch:{ all -> 0x01d2 }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x01d2 }
            com.facebook.acra.ErrorReporter$CrashReportedObserver r1 = (com.facebook.acra.ErrorReporter.CrashReportedObserver) r1     // Catch:{ all -> 0x01d2 }
            if (r1 == 0) goto L_0x01dd
            com.facebook.acra.CrashReportData r0 = r5.mInMemoryReportToSend     // Catch:{ all -> 0x01d2 }
            if (r0 == 0) goto L_0x01dd
            r1.onCrashReported(r0)     // Catch:{ all -> 0x01d2 }
            goto L_0x01dd
        L_0x01cb:
            r5.start()     // Catch:{ all -> 0x01d0 }
            r4 = 0
            goto L_0x01dd
        L_0x01d0:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x01d2 }
        L_0x01d2:
            r1 = move-exception
            goto L_0x01d6
        L_0x01d4:
            r1 = move-exception
            r5 = 0
        L_0x01d6:
            java.lang.String r0 = "sending in-memory report"
            r9.tryLogInternalError(r0, r1)     // Catch:{ all -> 0x01e7 }
            goto L_0x01dd
        L_0x01dc:
            r5 = 0
        L_0x01dd:
            r9.safeClose(r2)
            r9.safeClose(r3)
            r9.safeClose(r4)
            return r5
        L_0x01e7:
            r0 = move-exception
            goto L_0x01ef
        L_0x01e9:
            r0 = move-exception
            r3 = 0
            r4 = 0
            goto L_0x01ef
        L_0x01ed:
            r0 = move-exception
            r3 = r2
        L_0x01ef:
            r9.safeClose(r2)
            r9.safeClose(r3)
            r9.safeClose(r4)
            throw r0
        L_0x01f9:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01f9 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.handleExceptionInternal(java.lang.Throwable, com.facebook.acra.CrashReportData, java.lang.String, int):com.facebook.acra.ErrorReporter$ReportsSenderWorker");
    }

    private boolean isConnectedToWifi() {
        try {
            return ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getNetworkInfo(1).isConnected();
        } catch (Throwable th) {
            writeToLogBridge(ACRA.LOG_TAG, "Error retrieving wifi state", th, null);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public CrashReportData loadAcraCrashReport(Spool.FileBeingConsumed fileBeingConsumed) {
        return loadCrashReport(fileBeingConsumed, CrashReportType.ACRA_CRASH_REPORT, this.mMaxReportSize, true);
    }

    private boolean mustEmbedAttachments(CrashReportType crashReportType) {
        if (crashReportType == CrashReportType.NATIVE_CRASH_REPORT) {
            Iterator it = this.mReportSenders.iterator();
            while (it.hasNext()) {
                ReportSender reportSender = (ReportSender) it.next();
                if (reportSender instanceof HttpPostSender) {
                    if (!((HttpPostSender) reportSender).mUseMultipartPost) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    private static boolean objectsEqual(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || !obj.equals(obj2)) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005d, code lost:
        if (r2 != null) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0062, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0052 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void preallocateReportFile(java.io.File r9, long r10) {
        /*
            r8 = this;
            java.lang.Class<com.facebook.acra.ErrorReporter> r2 = com.facebook.acra.ErrorReporter.class
            java.util.UUID r1 = java.util.UUID.randomUUID()
            java.lang.String r0 = ".stacktrace"
            java.lang.String r2 = r8.genCrashReportFileName(r2, r1, r0)
            com.facebook.acra.ErrorReporter$CrashReportType r1 = com.facebook.acra.ErrorReporter.CrashReportType.ACRA_CRASH_REPORT
            android.content.Context r0 = r8.mContext
            com.facebook.acra.Spool r1 = r1.getSpool(r0)
            r0 = 0
            com.facebook.acra.Spool$FileBeingProduced r2 = r1.produceWithDonorFile(r2, r0)
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ all -> 0x0053 }
            java.io.File r0 = r2.fileName     // Catch:{ all -> 0x0053 }
            r7.<init>(r0)     // Catch:{ all -> 0x0053 }
            r0 = 32768(0x8000, float:4.5918E-41)
            r6 = 32768(0x8000, float:4.5918E-41)
            byte[] r5 = new byte[r0]     // Catch:{ all -> 0x004c }
            r3 = 0
        L_0x002a:
            int r0 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x0034
            r7.write(r5)     // Catch:{ all -> 0x004c }
            long r0 = (long) r6     // Catch:{ all -> 0x004c }
            long r3 = r3 + r0
            goto L_0x002a
        L_0x0034:
            java.io.FileDescriptor r0 = r7.getFD()     // Catch:{ all -> 0x004c }
            r0.sync()     // Catch:{ all -> 0x004c }
            r7.close()     // Catch:{ all -> 0x0053 }
            java.io.File r0 = r2.fileName     // Catch:{ all -> 0x0053 }
            renameOrThrow(r0, r9)     // Catch:{ all -> 0x0053 }
            java.io.File r0 = r2.fileName     // Catch:{ all -> 0x005a }
            r0.delete()     // Catch:{ all -> 0x005a }
            r2.close()
            return
        L_0x004c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
            r7.close()     // Catch:{ all -> 0x0052 }
        L_0x0052:
            throw r0     // Catch:{ all -> 0x0053 }
        L_0x0053:
            r1 = move-exception
            java.io.File r0 = r2.fileName     // Catch:{ all -> 0x005a }
            r0.delete()     // Catch:{ all -> 0x005a }
            throw r1     // Catch:{ all -> 0x005a }
        L_0x005a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005c }
        L_0x005c:
            r0 = move-exception
            if (r2 == 0) goto L_0x0062
            r2.close()     // Catch:{ all -> 0x0062 }
        L_0x0062:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.preallocateReportFile(java.io.File, long):void");
    }

    private void processCoreDump(CrashReportData crashReportData, Spool.FileBeingConsumed fileBeingConsumed) {
        boolean z;
        String str;
        if (this.mConfig.shouldReportField(ReportField.CORE_DUMP)) {
            File file = new File(this.mContext.getApplicationInfo().dataDir, "core");
            if (file.exists()) {
                if ("arm64" != 0) {
                    z = "arm64".contains("64");
                } else {
                    z = false;
                }
                if (z) {
                    str = "/system/bin/app_process64";
                } else {
                    str = "/system/bin/app_process32";
                }
                File file2 = new File(str);
                if (!file2.exists()) {
                    file2 = new File("/system/bin/app_process");
                }
                if (file2.exists() && Math.abs(file.lastModified() - fileBeingConsumed.fileName.lastModified()) <= 60000 && isConnectedToWifi()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    long j = currentTimeMillis - MS_PER_DAY;
                    File file3 = new File(this.mContext.getApplicationInfo().dataDir, "core_dump_proccessed");
                    if (!file3.exists() || file3.lastModified() <= j) {
                        try {
                            CrashReportData crashReportData2 = crashReportData;
                            crashReportData2.mInputStreamFields.put(ReportField.CORE_DUMP, new InputStreamField(new FileInputStream(file), true, true, file.length()));
                            crashReportData2.mInputStreamFields.put(ReportField.APP_PROCESS_FILE, new InputStreamField(new FileInputStream(file2), true, true, file2.length()));
                            if (!file3.exists()) {
                                new FileOutputStream(file3).close();
                            } else {
                                file3.setLastModified(currentTimeMillis);
                            }
                        } catch (Exception e) {
                            writeToLogBridge(ACRA.LOG_TAG, AnonymousClass08S.A0J("Error openning core dump file: ", file.getAbsolutePath()), e, null);
                        }
                    }
                }
            }
        }
    }

    private void processCrashAttachmentAfterSend(Spool.FileBeingConsumed fileBeingConsumed, CrashReportType crashReportType, File file, String str) {
        if (crashReportType != CrashReportType.NATIVE_CRASH_REPORT || file == null || !file.exists()) {
            fileBeingConsumed.fileName.getCanonicalPath();
            deleteFile(fileBeingConsumed.fileName);
        } else {
            File file2 = new File(file, fileBeingConsumed.fileName.getName());
            file2.delete();
            fileBeingConsumed.fileName.renameTo(file2);
            fileBeingConsumed.fileName.getCanonicalPath();
            file2.getCanonicalPath();
        }
        if (str != null) {
            deleteFile(new File(str));
        }
    }

    private void processCrashAttachmentBeforeSend(Spool.FileBeingConsumed fileBeingConsumed, CrashReportType crashReportType, CrashReportData crashReportData, boolean z) {
        if (crashReportType == CrashReportType.NATIVE_CRASH_REPORT) {
            if (!z) {
                processCoreDump(crashReportData, fileBeingConsumed);
            }
            foregroundNativeCrashDetect(fileBeingConsumed);
            MinidumpReader minidumpReader = new MinidumpReader(fileBeingConsumed.file);
            String customData = minidumpReader.getCustomData(ACRA.SESSION_ID_KEY);
            if (!TextUtils.isEmpty(customData)) {
                crashReportData.put(ReportField.SESSION_ID, customData);
            }
            String customData2 = minidumpReader.getCustomData("is_first_run_after_upgrade");
            if (!TextUtils.isEmpty(customData2)) {
                crashReportData.put("is_first_run_after_upgrade", customData2);
            }
            String customData3 = minidumpReader.getCustomData(ReportField.SHOULD_REPORT_DISK_INFO_NATIVE);
            if (!TextUtils.isEmpty(customData3)) {
                crashReportData.put(ReportField.SHOULD_REPORT_DISK_INFO_NATIVE, customData3);
            }
            String customData4 = minidumpReader.getCustomData(ReportField.NO_DEVICE_MEMORY);
            if (!TextUtils.isEmpty(customData4)) {
                crashReportData.put(ReportField.NO_DEVICE_MEMORY, customData4);
            }
            Integer num = minidumpReader.getInt((int) MinidumpReader.MD_FB_APP_VERSION_CODE);
            if (num != null) {
                crashReportData.put(ReportField.APP_VERSION_CODE, num.toString());
            }
            String string = minidumpReader.getString((int) MinidumpReader.MD_FB_APP_VERSION_NAME);
            if (!TextUtils.isEmpty(string)) {
                crashReportData.put(ReportField.APP_VERSION_NAME, string);
            }
            String customData5 = minidumpReader.getCustomData(ACRA.BREAKPAD_LIB_NAME);
            if (!TextUtils.isEmpty(customData5)) {
                crashReportData.put(ReportField.BREAKPAD_LIB_NAME, customData5);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void purgeDirectory(File file, final String str) {
        AnonymousClass1 r0;
        File[] listFiles;
        if (str != null) {
            r0 = new FileFilter() {
                public boolean accept(File file) {
                    return file.getName().endsWith(str);
                }
            };
        } else {
            r0 = null;
        }
        if (file != null && (listFiles = file.listFiles(r0)) != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    purgeDirectory(file2, str);
                }
                deleteFile(file2);
            }
        }
    }

    public static void put(String str, String str2, CrashReportData crashReportData, Writer writer) {
        if (crashReportData.generatingIoError != null) {
            writer = null;
        }
        try {
            crashReportData.put(str, str2, writer);
        } catch (IOException e) {
            crashReportData.generatingIoError = e;
        }
    }

    public static void putCustomData(String str, String str2) {
        ProxyCustomDataStore.Holder.CUSTOM_DATA.setCustomData(str, str2, new Object[0]);
    }

    private static String readVersionLine(BufferedInputStream bufferedInputStream) {
        bufferedInputStream.mark(21);
        int read = bufferedInputStream.read();
        char[] cArr = new char[20];
        int i = 0;
        while (true) {
            if (read != -1 && i < 20 && read != 10) {
                if (!Character.isDigit(read) && read != 46) {
                    bufferedInputStream.reset();
                    i = 0;
                    break;
                }
                cArr[i] = (char) read;
                read = bufferedInputStream.read();
                i++;
            } else {
                break;
            }
        }
        return new String(cArr, 0, i);
    }

    private void removeCoreDump() {
        new File(this.mContext.getApplicationInfo().dataDir, "core").delete();
    }

    private void removeCrashFiles(final CrashReportType crashReportType, int i) {
        if (i > 0) {
            try {
                Spool.Snapshot snapshot = crashReportType.getSpool(this.mContext).snapshot(new FifoSpoolComparator(), new FilenameFilter() {
                    public boolean accept(File file, String str) {
                        for (String endsWith : crashReportType.fileExtensions) {
                            if (str.endsWith(endsWith)) {
                                return true;
                            }
                        }
                        return false;
                    }
                });
                if (snapshot == null) {
                    C010708t.A0J(TAG, "removeCrashFiles no snapshot");
                    return;
                }
                while (snapshot.hasNext() && i > 0) {
                    Spool.FileBeingConsumed next = snapshot.next();
                    File file = next.fileName;
                    if (file != null) {
                        if (!file.delete()) {
                            C010708t.A0P(TAG, "removeCrashFiles Crash file not deleted %s", next.fileName.getAbsolutePath());
                        } else {
                            i--;
                        }
                    }
                }
            } catch (Throwable th) {
                C010708t.A0R(TAG, th, "removeCrashFiles");
            }
        }
    }

    public static void removeCustomData(String str) {
        ProxyCustomDataStore.Holder.CUSTOM_DATA.removeCustomData(str);
    }

    /* access modifiers changed from: private */
    public void safeClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                tryLogInternalError("safeClose", th);
            }
        }
    }

    /* access modifiers changed from: private */
    public void sendCrashReport(CrashReportData crashReportData) {
        ArrayList arrayList;
        if (this.mConfig.isZeroCrashlogBlocked()) {
            return;
        }
        if (this.mConfig.shouldOnlyWriteReport()) {
            writeJsonReport(crashReportData);
            return;
        }
        synchronized (this.mReportSenders) {
            arrayList = new ArrayList(this.mReportSenders);
        }
        if (!arrayList.isEmpty()) {
            Iterator it = arrayList.iterator();
            boolean z = false;
            while (it.hasNext()) {
                ReportSender reportSender = (ReportSender) it.next();
                try {
                    reportSender.send(crashReportData);
                    z = true;
                } catch (ReportSenderException e) {
                    if (z) {
                        C010708t.A0V(ACRA.LOG_TAG, e, "ReportSender of class %s failed but other senders completed their task. ACRA will not send this report again.", reportSender.getClass().getName());
                    } else if (!(reportSender instanceof HttpPostSender) || !this.mConfig.shouldUseUploadService() || !writeJsonReport(crashReportData)) {
                        throw e;
                    }
                }
            }
            return;
        }
        throw new ReportSenderException("no configured report senders", null);
    }

    /* access modifiers changed from: private */
    public boolean shouldReportANRs() {
        ANRDataProvider aNRDataProvider = this.mANRDataProvider;
        if (aNRDataProvider == null || !aNRDataProvider.shouldANRDetectorRun()) {
            return false;
        }
        return true;
    }

    private boolean shouldSkipReport(CrashReportType crashReportType) {
        return new File(this.mContext.getDir(crashReportType.directory, 0), ".noupload").exists();
    }

    private void showBlockingNotification(StartupBlockingConfig startupBlockingConfig) {
        try {
            Notification.Builder smallIcon = new Notification.Builder(this.mContext).setContentTitle(this.mContext.getString(startupBlockingConfig.notificationTitle)).setContentText(this.mContext.getString(startupBlockingConfig.notificationText)).setSmallIcon(17301543);
            if (Build.VERSION.SDK_INT >= 16) {
                Api16Utils.applyBigTextStyle(smallIcon, this.mContext.getString(startupBlockingConfig.notificationText));
            }
            ((NotificationManager) this.mContext.getSystemService("notification")).notify(2, smallIcon.getNotification());
        } catch (Throwable unused) {
        }
    }

    private void slurpAttachment(CrashReportData crashReportData, InputStream inputStream, CrashReportType crashReportType, long j) {
        if (crashReportType == CrashReportType.NATIVE_CRASH_REPORT) {
            try {
                attachLastActivityInfo(crashReportData);
            } catch (IOException e) {
                C010708t.A0S(TAG, e, "error attaching activity information");
            }
            try {
                attachIabInfo(crashReportData);
            } catch (IOException e2) {
                C010708t.A0S(TAG, e2, "error attaching IAB information");
            }
        }
        crashReportData.put(crashReportType.attachmentField, AttachmentUtil.loadAttachment(inputStream, (int) j));
        crashReportData.put(ReportField.ATTACHMENT_ORIGINAL_SIZE, String.valueOf(j));
    }

    public static String throwableToString(Throwable th) {
        if (th == null) {
            th = new Exception("Report requested by developer");
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        printWriter.close();
        if (th instanceof StackOverflowError) {
            trimStackBuffer(stringWriter.getBuffer(), MAX_STACK_LENGTH_FOR_OVERFLOW);
        }
        return stringWriter.toString();
    }

    private Throwable translateException(Throwable th, Map map) {
        Throwable th2;
        ExceptionTranslationHook exceptionTranslationHook = (ExceptionTranslationHook) this.mExceptionTranslationHook.get();
        int i = 0;
        while (true) {
            th2 = th;
            for (ExceptionTranslationHook exceptionTranslationHook2 = exceptionTranslationHook; exceptionTranslationHook2 != null && th2 != null; exceptionTranslationHook2 = exceptionTranslationHook2.next) {
                try {
                    th2 = exceptionTranslationHook2.translate(th2, map);
                } catch (Throwable th3) {
                    C010708t.A0V(ACRA.LOG_TAG, th3, "ignoring error in exception translation hook %s", exceptionTranslationHook2);
                }
            }
            if (th2 == th || (i = i + 1) >= 4) {
                return th2;
            }
            th = th2;
        }
        return th2;
    }

    private boolean writeJsonReport(CrashReportData crashReportData) {
        String str;
        long j;
        SecureRandom secureRandom = new SecureRandom();
        try {
            long nextLong = secureRandom.nextLong() ^ this.mAppStartTickTimeMs;
            long nextLong2 = secureRandom.nextLong();
            if (this.mUserId == null) {
                j = 0;
            } else {
                j = Long.parseLong(this.mUserId);
            }
            str = new UUID(nextLong, nextLong2 ^ j).toString();
        } catch (NumberFormatException unused) {
            str = UUID.randomUUID().toString();
        }
        File file = new File(this.mContext.getDir(ErrorReportingConstants.TRACE_UPLOAD_DIR, 0), AnonymousClass08S.A0J(str, ".gz"));
        if (!JsonReportWriter.writeGzipJsonReport(crashReportData, crashReportData.mInputStreamFields, file)) {
            return false;
        }
        file.getName();
        return true;
    }

    private void writeToLogBridge(String str, String str2, Throwable th, String str3) {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[0];
        StackTraceElement[] stackTrace = th.getStackTrace();
        int length = stackTrace.length;
        int i = 0;
        while (i < length) {
            StackTraceElement stackTraceElement2 = stackTrace[i];
            if (!stackTraceElement2.getClassName().equals(stackTraceElement.getClassName()) || !stackTraceElement2.getMethodName().equals(stackTraceElement.getMethodName())) {
                i++;
            } else {
                C010708t.A0R(TAG, th, "Unable to log over log bridge due to exception.");
                return;
            }
        }
        LogBridge logBridge = getLogBridge();
        if (logBridge != null) {
            if (str3 != null) {
                logBridge.log(str, AnonymousClass08S.A0P(str2, "\n", str3), null);
            } else {
                logBridge.log(str, str2, th);
            }
        } else if (str3 != null) {
            C010708t.A0O(str, "%s\n%s", str2, str3);
        } else {
            C010708t.A0U(str, th, "%s", str2);
        }
    }

    public void addExceptionTranslationHook(ExceptionTranslationHook exceptionTranslationHook) {
        exceptionTranslationHook.next = (ExceptionTranslationHook) this.mExceptionTranslationHook.getAndSet(exceptionTranslationHook);
    }

    public void addReportSender(ReportSender reportSender) {
        synchronized (this.mReportSenders) {
            this.mReportSenders.add(reportSender);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public boolean addToAnrInProgressUpdateFile(Map map) {
        synchronized (this.mAnrFilesInProgress) {
            if (this.mAnrFilesInProgress.isEmpty()) {
                return false;
            }
            String str = (String) this.mAnrFilesInProgress.iterator().next();
            FileOutputStream fileOutputStream = new FileOutputStream(new File(AnonymousClass08S.A0J(str, ANR_EXTRA_PROPERTIES_EXTENSION)), true);
            new CrashReportData(map).store(fileOutputStream, (String) null);
            fileOutputStream.close();
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        if (r11 == null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        r5.put(com.facebook.acra.ErrorReporter.CrashReportType.ANR_REPORT.attachmentField, com.facebook.acra.util.AttachmentUtil.compressToBase64String(r11.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        r5.store(r2, (java.lang.String) null);
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0048, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0049, code lost:
        r3 = new java.io.File(r12);
        r8 = r3.length();
        r6 = new java.io.BufferedInputStream(new java.io.FileInputStream(r12));
        r4 = readSigquitFileHeader(r6);
        r1 = r4.commandLine;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0062, code lost:
        if (r1 == null) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0064, code lost:
        r5.put(com.facebook.acra.constants.ReportField.PROCESS_NAME, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006f, code lost:
        if (android.text.TextUtils.isEmpty(r4.versionCode) != false) goto L_0x007f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0071, code lost:
        r5.put(com.facebook.acra.constants.ReportField.APP_VERSION_CODE, r4.versionCode);
        r5.put(com.facebook.acra.constants.ReportField.APP_VERSION_NAME, r4.versionName);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007f, code lost:
        slurpAttachment(r5, r6, com.facebook.acra.ErrorReporter.CrashReportType.ANR_REPORT, r8);
        deleteFile(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        r2 = new java.io.FileOutputStream(new java.io.File(X.AnonymousClass08S.A0J(r2, com.facebook.acra.ErrorReporter.ANR_EXTRA_PROPERTIES_EXTENSION)), true);
        r5 = new com.facebook.acra.CrashReportData();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void amendANRReportWithSigquitData(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            java.util.Set r1 = r10.mAnrFilesInProgress
            monitor-enter(r1)
            java.util.Set r0 = r10.mAnrFilesInProgress     // Catch:{ all -> 0x0089 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x000d
            monitor-exit(r1)     // Catch:{ all -> 0x0089 }
            return
        L_0x000d:
            java.util.Set r0 = r10.mAnrFilesInProgress     // Catch:{ all -> 0x0089 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0089 }
            java.lang.Object r2 = r0.next()     // Catch:{ all -> 0x0089 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0089 }
            monitor-exit(r1)     // Catch:{ all -> 0x0089 }
            java.io.File r1 = new java.io.File
            java.lang.String r0 = ".upd"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r2, r0)
            r1.<init>(r0)
            java.io.FileOutputStream r2 = new java.io.FileOutputStream
            r0 = 1
            r2.<init>(r1, r0)
            com.facebook.acra.CrashReportData r5 = new com.facebook.acra.CrashReportData
            r5.<init>()
            if (r11 == 0) goto L_0x0049
            byte[] r0 = r11.getBytes()
            java.lang.String r1 = com.facebook.acra.util.AttachmentUtil.compressToBase64String(r0)
            com.facebook.acra.ErrorReporter$CrashReportType r0 = com.facebook.acra.ErrorReporter.CrashReportType.ANR_REPORT
            java.lang.String r0 = r0.attachmentField
            r5.put(r0, r1)
        L_0x0041:
            r0 = 0
            r5.store(r2, r0)
            r2.close()
            return
        L_0x0049:
            java.io.File r3 = new java.io.File
            r3.<init>(r12)
            long r8 = r3.length()
            java.io.FileInputStream r0 = new java.io.FileInputStream
            r0.<init>(r12)
            java.io.BufferedInputStream r6 = new java.io.BufferedInputStream
            r6.<init>(r0)
            com.facebook.acra.ErrorReporter$SigquitFileHeader r4 = readSigquitFileHeader(r6)
            java.lang.String r1 = r4.commandLine
            if (r1 == 0) goto L_0x0069
            java.lang.String r0 = "PROCESS_NAME"
            r5.put(r0, r1)
        L_0x0069:
            java.lang.String r0 = r4.versionCode
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x007f
            java.lang.String r1 = r4.versionCode
            java.lang.String r0 = "APP_VERSION_CODE"
            r5.put(r0, r1)
            java.lang.String r1 = r4.versionName
            java.lang.String r0 = "APP_VERSION_NAME"
            r5.put(r0, r1)
        L_0x007f:
            com.facebook.acra.ErrorReporter$CrashReportType r7 = com.facebook.acra.ErrorReporter.CrashReportType.ANR_REPORT
            r4 = r10
            r4.slurpAttachment(r5, r6, r7, r8)
            deleteFile(r3)
            goto L_0x0041
        L_0x0089:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0089 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.amendANRReportWithSigquitData(java.lang.String, java.lang.String):void");
    }

    public void backfillCrashReportData(CrashReportData crashReportData) {
        String str = (String) crashReportData.get(ReportField.REPORT_ID);
        if (str == null || str.length() == 0) {
            for (Map.Entry entry : this.mConstantFields.entrySet()) {
                if (crashReportData.get(entry.getKey()) == null) {
                    crashReportData.put(entry.getKey(), entry.getValue());
                }
            }
        }
        String userId = getUserId();
        String str2 = (String) crashReportData.get(ReportField.UID);
        if (!TextUtils.isEmpty(userId) && TextUtils.isEmpty(str2)) {
            crashReportData.put(ReportField.UID, userId);
        }
    }

    public void checkNativeReports() {
        CrashReportType crashReportType = CrashReportType.NATIVE_CRASH_REPORT;
        if (roughlyCountPendingReportsOfType(crashReportType) > 0) {
            checkReportsOfType(crashReportType);
        }
    }

    public ReportsSenderWorker checkReportsOfType(CrashReportType... crashReportTypeArr) {
        ReportsSenderWorker reportsSenderWorker = new ReportsSenderWorker(this, crashReportTypeArr);
        reportsSenderWorker.start();
        int roughlyCountPendingReportsOfType = roughlyCountPendingReportsOfType(crashReportTypeArr);
        StartupBlockingConfig startupBlockingConfig = this.mConfig.getStartupBlockingConfig();
        if (startupBlockingConfig != null && roughlyCountPendingReportsOfType > startupBlockingConfig.minNumQueuedReportsToBlockStartup) {
            long uptimeMillis = SystemClock.uptimeMillis();
            try {
                if (startupBlockingConfig.notifyWhileBlockingStartup) {
                    showBlockingNotification(startupBlockingConfig);
                }
                reportsSenderWorker.join(startupBlockingConfig.maxTimeSpentBlockedOnUploadMs);
                if (startupBlockingConfig.notifyWhileBlockingStartup) {
                    cancelBlockingNotification();
                }
            } catch (InterruptedException unused) {
                Log.e(ACRA.LOG_TAG, "interrupted while waiting for error reports to upload");
            } catch (Throwable th) {
                StartTimeBlockedRecorder.sDurationStartupBlocked = SystemClock.uptimeMillis() - uptimeMillis;
                StartTimeBlockedRecorder.sTotalCrashesUploaded = roughlyCountPendingReportsOfType;
                throw th;
            }
            StartTimeBlockedRecorder.sDurationStartupBlocked = SystemClock.uptimeMillis() - uptimeMillis;
            StartTimeBlockedRecorder.sTotalCrashesUploaded = roughlyCountPendingReportsOfType;
        }
        return reportsSenderWorker;
    }

    public void deletePartialANRReports() {
        synchronized (ANR_REPORTING_LOCK) {
            purgeDirectory(this.mContext.getDir(SIGQUIT_DIR, 0), REPORTFILE_EXTENSION);
        }
    }

    public String getAppStartDateFormat3339() {
        String format3339;
        synchronized (this.mAppStartDate) {
            format3339 = this.mAppStartDate.format3339(false);
        }
        return format3339;
    }

    public Map getCustomFieldsSnapshot() {
        return ProxyCustomDataStore.Holder.CUSTOM_DATA.getSnapshot();
    }

    public String getEventsLog() {
        if (Build.VERSION.SDK_INT >= 19) {
            return null;
        }
        return LogCatCollector.collectLogCat(this.mContext, this.mConfig, "events", null, false, true, this.mConfig.allowCollectionOfMaxNumberOfLinesInLogcat());
    }

    public Map getLazyCustomFieldsSnapshot() {
        TreeMap treeMap;
        synchronized (this.mInstanceLazyCustomParameters) {
            treeMap = new TreeMap(this.mInstanceLazyCustomParameters);
        }
        return treeMap;
    }

    public String getLogcatOutputIfPidFound(boolean z, Integer num) {
        String collectLogCat = LogCatCollector.collectLogCat(this.mContext, this.mConfig, null, null, false, z, this.mConfig.allowCollectionOfMaxNumberOfLinesInLogcat());
        if (collectLogCat == null || (num != null && !Pattern.compile(AnonymousClass08S.A0P("^\\d+-\\d+\\s+\\d+:\\d+:\\d+\\.\\d+\\s+", num.toString(), "\\s+\\d+\\s+[A-Z]\\s+(.*?)$"), 8).matcher(collectLogCat).find())) {
            return null;
        }
        return collectLogCat;
    }

    public String getSigquitTracesPath() {
        return this.mContext.getDir(SIGQUIT_DIR, 0).getPath();
    }

    public void handleUncaughtException(Thread thread, Throwable th, C02120Da r6) {
        int roughlyCountPendingReportsOfType;
        int maxPendingJavaCrashReports = this.mConfig.getMaxPendingJavaCrashReports();
        if (maxPendingJavaCrashReports >= Integer.MAX_VALUE || (roughlyCountPendingReportsOfType = roughlyCountPendingReportsOfType(CrashReportType.ACRA_CRASH_REPORT)) < maxPendingJavaCrashReports) {
            synchronized (UNCAUGHT_EXCEPTION_LOCK) {
                try {
                    uncaughtExceptionImpl(thread, th, false);
                } catch (Throwable th2) {
                    tryLogInternalError(th2);
                }
            }
            return;
        }
        C010708t.A0U(TAG, th, "Maximum pending Java crash threshold reached, Not storing count=%d", Integer.valueOf(roughlyCountPendingReportsOfType));
    }

    public void init(AcraReportingConfig acraReportingConfig) {
        if (!this.mInitializationComplete) {
            this.mContext = acraReportingConfig.getApplicationContext();
            if (this.mContext != null) {
                this.mInstallTime = new File(this.mContext.getApplicationInfo().sourceDir).lastModified();
                if (this.mInstallTime == 0) {
                    C010708t.A0J(ACRA.LOG_TAG, "could not retrieve APK mod time");
                }
                this.mConfig = acraReportingConfig;
                this.mChainedHandler = acraReportingConfig.getDefaultUncaughtExceptionHandler();
                if (this.mConfig.getSessionId() != null) {
                    putCustomData(ReportField.SESSION_ID, this.mConfig.getSessionId());
                }
                addCriticalData();
                this.mInitializationComplete = true;
                return;
            }
            throw new AssertionError("context must be non-null");
        }
        throw new IllegalStateException("ErrorReporter already initialized");
    }

    public void initFallible() {
        int oomReservationOverride = this.mConfig.getOomReservationOverride();
        if (oomReservationOverride <= 0) {
            oomReservationOverride = 65536;
        }
        this.mOomReservation = new byte[oomReservationOverride];
        synchronized (this.mAppStartDate) {
            this.mAppStartDate.setToNow();
            this.mOomReservation[0] = 1;
        }
        populateConstantFields();
        File file = new File(this.mContext.getDir(ACRA_DIRNAME, 0), PREALLOCATED_REPORTFILE);
        long preallocatedFileSizeOverride = this.mConfig.getPreallocatedFileSizeOverride();
        if (preallocatedFileSizeOverride <= 0) {
            preallocatedFileSizeOverride = DEFAULT_MAX_REPORT_SIZE;
        }
        if (file.length() < preallocatedFileSizeOverride) {
            try {
                preallocateReportFile(file, preallocatedFileSizeOverride);
            } catch (Throwable th) {
                tryLogInternalError(th);
                file = null;
            }
        }
        this.mPreallocFileName = file;
    }

    public int prepareCachedANRReports(int i) {
        Object obj = UNCAUGHT_EXCEPTION_LOCK;
        synchronized (obj) {
            obj.notify();
        }
        return checkAndHandleReportsLocked(i, CrashReportType.CACHED_ANR_REPORT, true);
    }

    public int prepareReports(int i, FileGenerator fileGenerator, boolean z, CrashReportType... crashReportTypeArr) {
        int processCrashAttachmentsLocked;
        Object obj = UNCAUGHT_EXCEPTION_LOCK;
        synchronized (obj) {
            obj.notify();
        }
        discardOverlappingReports(crashReportTypeArr);
        int i2 = 0;
        for (CrashReportType crashReportType : crashReportTypeArr) {
            int max = Math.max(0, i - i2);
            if (crashReportType.getHandler() != null) {
                processCrashAttachmentsLocked = checkAndHandleReportsLocked(max, crashReportType, false);
            } else {
                processCrashAttachmentsLocked = processCrashAttachmentsLocked(max, crashReportType, fileGenerator, z);
            }
            i2 += processCrashAttachmentsLocked;
        }
        removeCoreDump();
        return i2;
    }

    public void putLazyCustomData(String str, CustomReportDataSupplier customReportDataSupplier) {
        synchronized (this.mInstanceLazyCustomParameters) {
            this.mInstanceLazyCustomParameters.put(str, customReportDataSupplier);
        }
    }

    public void registerActivity(String str) {
        if (str != null) {
            this.mActivityLogger.append(str);
        }
    }

    public void removeAllReportSenders() {
        synchronized (this.mReportSenders) {
            this.mReportSenders.clear();
        }
    }

    public CustomReportDataSupplier removeLazyCustomData(String str) {
        CustomReportDataSupplier customReportDataSupplier;
        if (str == null) {
            return null;
        }
        synchronized (this.mInstanceLazyCustomParameters) {
            customReportDataSupplier = (CustomReportDataSupplier) this.mInstanceLazyCustomParameters.remove(str);
        }
        return customReportDataSupplier;
    }

    public void reportErrorAndTerminate(Thread thread, Throwable th) {
        synchronized (AnonymousClass016.class) {
            AnonymousClass016.A00().uncaughtException(thread, th);
        }
    }

    public int roughlyCountPendingReportsOfType(CrashReportType... crashReportTypeArr) {
        if (this.mContext == null) {
            C010708t.A0I(ACRA.LOG_TAG, "Trying to get ACRA reports but ACRA is not initialized.");
            return 0;
        }
        int i = 0;
        for (CrashReportType pendingCrashReports : crashReportTypeArr) {
            i += CrashReportType.getPendingCrashReports(pendingCrashReports, this.mContext).mDescriptors.length;
        }
        return i;
    }

    public void setCrashReportedObserver(CrashReportedObserver crashReportedObserver) {
        this.mCrashReportedObserver.set(crashReportedObserver);
    }

    public void setReportProxy(Proxy proxy) {
        synchronized (this.mReportSenders) {
            Iterator it = this.mReportSenders.iterator();
            while (it.hasNext()) {
                ReportSender reportSender = (ReportSender) it.next();
                if (reportSender instanceof FlexibleReportSender) {
                    ((FlexibleReportSender) reportSender).setProxy(proxy);
                }
            }
        }
    }

    public void setReportSender(ReportSender reportSender) {
        synchronized (this.mReportSenders) {
            removeAllReportSenders();
            addReportSender(reportSender);
        }
    }

    public void updateGLCwithSystemLibs(Spool.FileBeingConsumed fileBeingConsumed) {
        File crashDumpSysLibPath = getCrashDumpSysLibPath(this.mContext);
        if (!crashDumpSysLibPath.exists()) {
            try {
                crashDumpSysLibPath.createNewFile();
            } catch (IOException e) {
                C010708t.A0R(ACRA.LOG_TAG, e, "Failed to create GLC Lib file");
                return;
            }
        }
        try {
            writeLibsToFile(crashDumpSysLibPath, getNewLibs(crashDumpSysLibPath, new MinidumpReader(fileBeingConsumed.file).getModuleList()));
        } catch (IOException e2) {
            C010708t.A0R(ACRA.LOG_TAG, e2, "Failed to create GLC Lib file");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public void writeLibsToFile(File file, HashSet hashSet) {
        if (hashSet != null && !hashSet.isEmpty()) {
            BufferedWriter bufferedWriter = null;
            try {
                sSystemLibFileLock.writeLock().lock();
                BufferedWriter bufferedWriter2 = new BufferedWriter(new FileWriter(file, true), hashSet.size());
                try {
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        bufferedWriter2.write(AnonymousClass08S.A0J((String) it.next(), "\n"));
                    }
                    safeClose(bufferedWriter2);
                } catch (IOException e) {
                    e = e;
                    bufferedWriter = bufferedWriter2;
                    try {
                        C010708t.A0R(ACRA.LOG_TAG, e, "GLC file to write Exception");
                        safeClose(bufferedWriter);
                        sSystemLibFileLock.writeLock().unlock();
                    } catch (Throwable th) {
                        th = th;
                        safeClose(bufferedWriter);
                        sSystemLibFileLock.writeLock().unlock();
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedWriter = bufferedWriter2;
                    safeClose(bufferedWriter);
                    sSystemLibFileLock.writeLock().unlock();
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                C010708t.A0R(ACRA.LOG_TAG, e, "GLC file to write Exception");
                safeClose(bufferedWriter);
                sSystemLibFileLock.writeLock().unlock();
            }
            sSystemLibFileLock.writeLock().unlock();
        }
    }

    public void writeReportToStream(Throwable th, OutputStream outputStream) {
        CrashReportData crashReportData = new CrashReportData();
        Writer writer = CrashReportData.getWriter(outputStream);
        String throwableToString = throwableToString(th);
        crashReportData.put(ReportField.REPORT_ID, CrashTimeDataCollectorHelper.generateReportUuid().toString(), writer);
        CrashTimeDataCollector.gatherCrashData(this, this.mConfig, throwableToString, th, crashReportData, writer, null, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0069, code lost:
        if (r3 == null) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0071, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0072, code lost:
        if (r3 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0078, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x007b, code lost:
        return r4;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0032 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x0077 */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x005c A[EDGE_INSN: B:72:0x005c->B:34:0x005c ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int checkAndHandleReportsLocked(int r9, com.facebook.acra.ErrorReporter.CrashReportType r10, boolean r11) {
        /*
            r8 = this;
            com.facebook.acra.ErrorReporter$ReportHandler r0 = r10.getHandler()
            if (r0 == 0) goto L_0x0085
            android.content.Context r0 = r8.mContext
            java.lang.String r5 = com.facebook.acra.CrashTimeDataCollector.getProcessNameFromAms(r0)
            android.content.Context r0 = r8.mContext
            com.facebook.acra.Spool$Snapshot r7 = com.facebook.acra.ErrorReporter.CrashReportType.getPendingCrashReports(r10, r0)
            r4 = 0
            r1 = 0
        L_0x0014:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x007c }
            if (r0 == 0) goto L_0x0078
            if (r4 >= r9) goto L_0x0078
            com.facebook.acra.Spool$FileBeingConsumed r3 = r7.next()     // Catch:{ all -> 0x007c }
            int r6 = r1 + 1
            r0 = 5
            if (r1 < r0) goto L_0x002b
            java.io.File r0 = r3.fileName     // Catch:{ all -> 0x006f }
            deleteFile(r0)     // Catch:{ all -> 0x006f }
            goto L_0x0061
        L_0x002b:
            r1 = 0
            java.io.File r0 = r3.fileName     // Catch:{ IOException -> 0x0032 }
            java.lang.String r1 = r0.getCanonicalPath()     // Catch:{ IOException -> 0x0032 }
        L_0x0032:
            com.facebook.acra.ErrorReporter$CrashReportType r0 = com.facebook.acra.ErrorReporter.CrashReportType.CACHED_ANR_REPORT     // Catch:{ all -> 0x006f }
            if (r10 != r0) goto L_0x0051
            java.util.Set r2 = r8.mAnrFilesInProgress     // Catch:{ all -> 0x006f }
            monitor-enter(r2)     // Catch:{ all -> 0x006f }
            java.util.Set r0 = r8.mAnrFilesInProgress     // Catch:{ all -> 0x0066 }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x004e
            if (r11 == 0) goto L_0x0047
            monitor-exit(r2)     // Catch:{ all -> 0x0066 }
            if (r3 == 0) goto L_0x0064
            goto L_0x0061
        L_0x0047:
            java.util.Set r0 = r8.mAnrFilesInProgress     // Catch:{ all -> 0x0066 }
            r0.remove(r1)     // Catch:{ all -> 0x0066 }
            r1 = 1
            goto L_0x004f
        L_0x004e:
            r1 = 0
        L_0x004f:
            monitor-exit(r2)     // Catch:{ all -> 0x0066 }
            goto L_0x0052
        L_0x0051:
            r1 = 0
        L_0x0052:
            com.facebook.acra.ErrorReporter$ReportHandler r0 = r10.getHandler()     // Catch:{ all -> 0x006f }
            boolean r0 = r0.handleReport(r8, r3, r5, r1)     // Catch:{ all -> 0x006f }
            if (r0 != 0) goto L_0x005d
            goto L_0x0069
        L_0x005d:
            int r4 = r4 + 1
            if (r3 == 0) goto L_0x0064
        L_0x0061:
            r3.close()     // Catch:{ all -> 0x007c }
        L_0x0064:
            r1 = r6
            goto L_0x0014
        L_0x0066:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x006f }
        L_0x0069:
            if (r3 == 0) goto L_0x0078
            r3.close()     // Catch:{ all -> 0x007c }
            goto L_0x0078
        L_0x006f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0071 }
        L_0x0071:
            r0 = move-exception
            if (r3 == 0) goto L_0x0077
            r3.close()     // Catch:{ all -> 0x0077 }
        L_0x0077:
            throw r0     // Catch:{ all -> 0x007c }
        L_0x0078:
            r7.close()
            return r4
        L_0x007c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007e }
        L_0x007e:
            r0 = move-exception
            if (r7 == 0) goto L_0x0084
            r7.close()     // Catch:{ all -> 0x0084 }
        L_0x0084:
            throw r0
        L_0x0085:
            java.lang.NullPointerException r1 = new java.lang.NullPointerException
            java.lang.String r0 = "ErrorReporter::checkAndHandleReportsLocked report type requires a handler"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.checkAndHandleReportsLocked(int, com.facebook.acra.ErrorReporter$CrashReportType, boolean):int");
    }

    private String genCrashReportFileName(Class cls, UUID uuid, String str) {
        String str2;
        String uuid2 = uuid.toString();
        String simpleName = cls.getSimpleName();
        if (this.mAppVersionCode != null) {
            str2 = AnonymousClass08S.A0J("-", this.mAppVersionCode);
        } else {
            str2 = BuildConfig.FLAVOR;
        }
        return AnonymousClass08S.A0T(uuid2, "-", simpleName, str2, str);
    }

    public static ErrorReporter getInstance() {
        return Holder.ERROR_REPORTER;
    }

    private void populateConstantFields() {
        String str;
        String str2;
        String str3;
        this.mAppVersionCode = Integer.toString(BuildConstants.A00());
        PackageManagerWrapper packageManagerWrapper = new PackageManagerWrapper(this.mContext, ACRA.LOG_TAG);
        PackageInfo packageInfo = packageManagerWrapper.getPackageInfo();
        if (packageInfo == null || packageInfo.versionCode != BuildConstants.A00() || (str3 = packageInfo.versionName) == null) {
            this.mAppVersionName = "not set";
        } else {
            this.mAppVersionName = str3;
        }
        TreeMap treeMap = new TreeMap();
        if (this.mConfig.shouldReportField(ReportField.ANDROID_ID)) {
            try {
                str2 = sSecureSettingsResolver.getString(this.mContext.getContentResolver(), "android_id");
            } catch (Exception e) {
                C010708t.A0L(TAG, "Failed to fetch the constant field ANDROID_ID", e);
                str2 = "unknown";
            }
            treeMap.put(ReportField.ANDROID_ID, str2);
        }
        if (this.mConfig.shouldReportField(ReportField.APP_VERSION_CODE)) {
            treeMap.put(ReportField.APP_VERSION_CODE, this.mAppVersionCode);
        }
        if (this.mConfig.shouldReportField(ReportField.APP_VERSION_NAME)) {
            treeMap.put(ReportField.APP_VERSION_NAME, this.mAppVersionName);
        }
        if (this.mConfig.shouldReportField(ReportField.PACKAGE_NAME)) {
            treeMap.put(ReportField.PACKAGE_NAME, this.mContext.getPackageName());
        }
        if (this.mConfig.shouldReportField(ReportField.INSTALLER_NAME)) {
            treeMap.put(ReportField.INSTALLER_NAME, packageManagerWrapper.getInstallerPackageName());
        }
        if (this.mConfig.shouldReportField(ReportField.PHONE_MODEL)) {
            treeMap.put(ReportField.PHONE_MODEL, Build.MODEL);
        }
        if (this.mConfig.shouldReportField(ReportField.DEVICE)) {
            treeMap.put(ReportField.DEVICE, Build.DEVICE);
        }
        if (this.mConfig.shouldReportField(ReportField.ANDROID_VERSION)) {
            treeMap.put(ReportField.ANDROID_VERSION, Build.VERSION.RELEASE);
        }
        if (this.mConfig.shouldReportField(ReportField.OS_VERSION)) {
            treeMap.put(ReportField.OS_VERSION, System.getProperty("os.version"));
        }
        if (this.mConfig.shouldReportField(ReportField.BUILD_HOST)) {
            treeMap.put(ReportField.BUILD_HOST, Build.HOST);
        }
        if (this.mConfig.shouldReportField(ReportField.BRAND)) {
            treeMap.put(ReportField.BRAND, Build.BRAND);
        }
        if (this.mConfig.shouldReportField(ReportField.PRODUCT)) {
            treeMap.put(ReportField.PRODUCT, Build.PRODUCT);
        }
        if (this.mConfig.shouldReportField(ReportField.FILE_PATH)) {
            File filesDir = this.mContext.getFilesDir();
            if (filesDir != null) {
                str = filesDir.getAbsolutePath();
            } else {
                str = "n/a";
            }
            treeMap.put(ReportField.FILE_PATH, str);
        }
        if (this.mConfig.shouldReportField(ReportField.APP_INSTALL_TIME) && packageInfo != null) {
            treeMap.put(ReportField.APP_INSTALL_TIME, CrashTimeDataCollectorHelper.formatTimestamp(packageInfo.firstInstallTime));
        }
        if (this.mConfig.shouldReportField(ReportField.APP_INSTALL_EPOCH_TIME) && packageInfo != null) {
            treeMap.put(ReportField.APP_INSTALL_EPOCH_TIME, String.valueOf(packageInfo.firstInstallTime));
        }
        if (this.mConfig.shouldReportField(ReportField.APP_UPGRADE_TIME) && packageInfo != null) {
            treeMap.put(ReportField.APP_UPGRADE_TIME, CrashTimeDataCollectorHelper.formatTimestamp(packageInfo.lastUpdateTime));
        }
        if (this.mConfig.shouldReportField(ReportField.APP_UPGRADE_EPOCH_TIME) && packageInfo != null) {
            treeMap.put(ReportField.APP_UPGRADE_EPOCH_TIME, String.valueOf(packageInfo.lastUpdateTime));
        }
        if (this.mConfig.shouldReportField(ReportField.APP_SINCE_UPGRADE_TIME) && packageInfo != null) {
            treeMap.put(ReportField.APP_SINCE_UPGRADE_TIME, Long.toString(System.currentTimeMillis() - packageInfo.lastUpdateTime));
        }
        if (this.mConfig.shouldReportField(ReportField.PUBLIC_SOURCE_DIR)) {
            if (this.mContext.getApplicationInfo() != null) {
                treeMap.put(ReportField.PUBLIC_SOURCE_DIR, this.mContext.getApplicationInfo().publicSourceDir);
            } else {
                treeMap.put(ReportField.PUBLIC_SOURCE_DIR, "null application info");
            }
        }
        this.mConstantFields = Collections.unmodifiableMap(treeMap);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:22|23|24|25|26) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x002f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0036 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String readFile(java.io.File r3) {
        /*
            boolean r0 = r3.exists()
            if (r0 != 0) goto L_0x0009
            java.lang.String r0 = "NO_FILE"
            return r0
        L_0x0009:
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ Exception -> 0x0037 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0037 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x0030 }
            r0 = 1024(0x400, float:1.435E-42)
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0030 }
            java.lang.String r0 = r1.readLine()     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0022
            r1.close()     // Catch:{ all -> 0x0030 }
            r2.close()     // Catch:{ Exception -> 0x0037 }
            return r0
        L_0x0022:
            r1.close()     // Catch:{ all -> 0x0030 }
            r2.close()     // Catch:{ Exception -> 0x0037 }
            goto L_0x0037
        L_0x0029:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x002f }
        L_0x002f:
            throw r0     // Catch:{ all -> 0x0030 }
        L_0x0030:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0032 }
        L_0x0032:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0036 }
        L_0x0036:
            throw r0     // Catch:{ Exception -> 0x0037 }
        L_0x0037:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.readFile(java.io.File):java.lang.String");
    }

    private static SigquitFileHeader readSigquitFileHeader(BufferedInputStream bufferedInputStream) {
        String str;
        String readVersionLine = readVersionLine(bufferedInputStream);
        String str2 = null;
        if (!TextUtils.isEmpty(readVersionLine)) {
            str = readVersionLine(bufferedInputStream);
        } else {
            str = null;
        }
        if (bufferedInputStream.markSupported()) {
            bufferedInputStream.mark(1024);
            byte[] bArr = new byte[1024];
            int read = bufferedInputStream.read(bArr, 0, 1024);
            if (read > 0) {
                Matcher matcher = mSigquitCmdLinePattern.matcher(new String(bArr, 0, read));
                if (matcher.find()) {
                    str2 = matcher.group(1);
                }
            }
            bufferedInputStream.reset();
        }
        SigquitFileHeader sigquitFileHeader = new SigquitFileHeader();
        sigquitFileHeader.versionCode = readVersionLine;
        sigquitFileHeader.versionName = str;
        sigquitFileHeader.commandLine = str2;
        return sigquitFileHeader;
    }

    private static void renameOrThrow(File file, File file2) {
        if (!file.renameTo(file2)) {
            throw new IOException(String.format("rename of %s to %s failed", file, file2));
        }
    }

    public static void trimStackBuffer(StringBuffer stringBuffer, int i) {
        int i2;
        int lastIndexOf;
        int indexOf;
        if (stringBuffer.length() > i && (lastIndexOf = stringBuffer.lastIndexOf("\n", (i2 = i >> 1))) >= 0 && (indexOf = stringBuffer.indexOf("\n", stringBuffer.length() - i2)) >= 0) {
            stringBuffer.replace(lastIndexOf, indexOf, STACK_TRIMMED_MESSAGE);
        }
    }

    public void checkReportsOnApplicationStart() {
        checkNativeReportsOnApplicationStart();
        if (roughlyCountPendingReportsOfType(REPORTS_TO_CHECK_ON_STARTUP) > 0) {
            checkReportsOfType(REPORTS_TO_CHECK_ON_STARTUP);
        }
        synchronized (this) {
            this.mFinishedCheckingReports = true;
        }
        startUploadIfReady();
    }

    public SimpleTraceLogger getActivityLogger() {
        return this.mActivityLogger;
    }

    public long getAppStartTickTimeMs() {
        return this.mAppStartTickTimeMs;
    }

    public String getAppVersionCode() {
        return this.mAppVersionCode;
    }

    public String getAppVersionName() {
        return this.mAppVersionName;
    }

    public String getClientUserId() {
        return this.mClientUserId;
    }

    public Map getConstantFields() {
        return this.mConstantFields;
    }

    public Context getContext() {
        return this.mContext;
    }

    public LogBridge getLogBridge() {
        return this.mLogBridge;
    }

    public String getUserId() {
        return this.mUserId;
    }

    public void setANRDataProvider(ANRDataProvider aNRDataProvider) {
        this.mANRDataProvider = aNRDataProvider;
    }

    public void setAppStartTickTimeMs(long j) {
        this.mAppStartTickTimeMs = j;
    }

    public void setBlackBoxRecorderControl(BlackBoxRecorderControl blackBoxRecorderControl) {
        this.mBlackBoxRecorderControl = blackBoxRecorderControl;
    }

    public void setClientUserId(String str) {
        this.mClientUserId = str;
    }

    public void setExcludedReportObserver(ExcludedReportObserver excludedReportObserver) {
        this.mExcludedReportObserver = excludedReportObserver;
    }

    public void setLogBridge(LogBridge logBridge) {
        this.mLogBridge = logBridge;
    }

    public void setMaxReportSize(long j) {
        this.mMaxReportSize = j;
    }

    public void setUserId(String str) {
        this.mUserId = str;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        reportErrorAndTerminate(thread, th);
    }

    private ErrorReporter() {
        this.mReportSenders = new ArrayList();
        this.mOomReservation = null;
        this.mMaxReportSize = DEFAULT_MAX_REPORT_SIZE;
        this.mAnrFilesInProgress = new HashSet();
        this.mInstanceLazyCustomParameters = new HashMap();
        this.mPreallocFileName = null;
        this.mActivityLogger = new SimpleTraceLogger(20);
        this.mAppStartDate = new Time();
        this.mExceptionTranslationHook = new AtomicReference();
        this.mExcludedReportObserver = null;
        this.mCrashReportedObserver = new AtomicReference();
    }

    public ReportsSenderWorker handleException(Throwable th) {
        return handleException(th, (CrashReportData) null);
    }

    public ReportsSenderWorker handleException(Throwable th, CrashReportData crashReportData) {
        return handleExceptionInternal(th, crashReportData, null, 1);
    }

    public ReportsSenderWorker handleException(Throwable th, String str, CrashReportData crashReportData) {
        return handleExceptionInternal(th, crashReportData, str, 1);
    }

    public ReportsSenderWorker handleException(Throwable th, Map map) {
        return handleException(th, map != null ? new CrashReportData(map) : null);
    }

    private CrashReportData loadCrashAttachment(Spool.FileBeingConsumed fileBeingConsumed, CrashReportType crashReportType, boolean z) {
        return loadCrashReport(fileBeingConsumed, crashReportType, crashReportType.defaultMaxSize, z);
    }

    private CrashReportData loadCrashAttachment(File file, CrashReportType crashReportType, boolean z) {
        return loadCrashReport(file, null, crashReportType, crashReportType.defaultMaxSize, z);
    }

    private CrashReportData loadCrashReport(Spool.FileBeingConsumed fileBeingConsumed, CrashReportType crashReportType, long j, boolean z) {
        return loadCrashReport(fileBeingConsumed.fileName, fileBeingConsumed.file, crashReportType, j, z);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:73|74|75|76|77) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0079, code lost:
        if ((r6 - r3) <= com.facebook.acra.ErrorReporter.MAX_REPORT_AGE) goto L_0x0032;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:76:0x0134 */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00cb  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:76:0x0134=Splitter:B:76:0x0134, B:68:0x012a=Splitter:B:68:0x012a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.acra.CrashReportData loadCrashReport(java.io.File r18, java.io.RandomAccessFile r19, com.facebook.acra.ErrorReporter.CrashReportType r20, long r21, boolean r23) {
        /*
            r17 = this;
            r8 = r17
            r5 = r18
            java.lang.String r0 = r5.getName()
            long r6 = java.lang.System.currentTimeMillis()
            long r3 = r5.lastModified()
            long r12 = r5.length()
            com.facebook.acra.CrashReportData r9 = new com.facebook.acra.CrashReportData
            r9.<init>()
            java.lang.String r2 = java.lang.Long.toString(r3)
            java.lang.String r1 = "TIME_OF_CRASH"
            r9.put(r1, r2)
            com.facebook.acra.ErrorReporter$CrashReportType r1 = com.facebook.acra.ErrorReporter.CrashReportType.ANR_REPORT
            r11 = r20
            if (r11 != r1) goto L_0x0072
            android.content.Context r2 = r8.mContext
            java.lang.String r1 = "acraconfig_report_old_anrs"
            boolean r1 = X.AnonymousClass08X.A07(r2, r1)
            if (r1 == 0) goto L_0x0072
        L_0x0032:
            r4 = 0
        L_0x0033:
            java.lang.String r1 = r11.attachmentField
            java.lang.String r2 = "MINIDUMP"
            boolean r1 = r2.equals(r1)
            r3 = 0
            if (r1 == 0) goto L_0x005b
            com.facebook.acra.config.AcraReportingConfig r1 = r8.mConfig
            boolean r1 = r1.shouldReportField(r2)
            if (r1 != 0) goto L_0x005b
            java.lang.String r6 = "CONFIG_DONT_REPORT_DUMP"
        L_0x0048:
            android.content.Context r2 = r8.mContext
            java.lang.String r1 = "android_acra_delete_corrupted_minidumps"
            boolean r1 = X.AnonymousClass08X.A07(r2, r1)
            if (r1 == 0) goto L_0x0096
            r4 = r19
            if (r19 == 0) goto L_0x0096
            com.facebook.acra.ErrorReporter$CrashReportType r1 = com.facebook.acra.ErrorReporter.CrashReportType.NATIVE_CRASH_REPORT
            if (r11 != r1) goto L_0x0096
            goto L_0x007c
        L_0x005b:
            if (r4 == 0) goto L_0x0060
            java.lang.String r6 = "DUMP_TOO_OLD"
            goto L_0x0048
        L_0x0060:
            int r1 = (r12 > r21 ? 1 : (r12 == r21 ? 0 : -1))
            if (r1 <= 0) goto L_0x0070
            java.lang.String r2 = java.lang.String.valueOf(r12)
            java.lang.String r1 = "ATTACHMENT_ORIGINAL_SIZE"
            r9.put(r1, r2)
            java.lang.String r6 = "DUMP_TOO_BIG"
            goto L_0x0048
        L_0x0070:
            r6 = r3
            goto L_0x0048
        L_0x0072:
            long r6 = r6 - r3
            r2 = 604800000(0x240c8400, double:2.988109026E-315)
            int r1 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            r4 = 1
            if (r1 > 0) goto L_0x0033
            goto L_0x0032
        L_0x007c:
            com.facebook.acra.util.minidump.MinidumpReader r2 = new com.facebook.acra.util.minidump.MinidumpReader     // Catch:{ IOException -> 0x008e }
            r2.<init>(r4)     // Catch:{ IOException -> 0x008e }
            boolean r1 = r2.checkIfMinidumpCorrupted()     // Catch:{ IOException -> 0x008e }
            if (r1 == 0) goto L_0x0096
            java.lang.String r6 = "CORRUPTED_MINIDUMP"
            java.lang.String r4 = r2.getJavaStack()     // Catch:{ IOException -> 0x008e }
            goto L_0x0097
        L_0x008e:
            r4 = move-exception
            java.lang.String r2 = "ACRA"
            java.lang.String r1 = "Failed to read minidump file"
            X.C010708t.A0R(r2, r4, r1)
        L_0x0096:
            r4 = r3
        L_0x0097:
            if (r6 == 0) goto L_0x00cb
            java.lang.String r2 = "ACRA"
            java.lang.Object[] r1 = new java.lang.Object[]{r0, r6}
            java.lang.String r0 = "deleting crash report %s: %s"
            X.C010708t.A0P(r2, r0, r1)
            deleteFile(r5)
            java.lang.String r0 = r11.attachmentField
            if (r0 == 0) goto L_0x00ae
            r9.put(r0, r6)
        L_0x00ae:
            java.lang.String r0 = "MINIDUMP_EXCLUDE_REASON"
            r9.put(r0, r6)
            if (r4 == 0) goto L_0x00ba
            java.lang.String r0 = "JAVA_STACK_FROM_DUMP"
            r9.put(r0, r4)
        L_0x00ba:
            com.facebook.acra.ErrorReporter$ExcludedReportObserver r0 = r8.mExcludedReportObserver
            if (r0 == 0) goto L_0x00ca
            r0.onExclude(r9)     // Catch:{ all -> 0x00c2 }
            return r9
        L_0x00c2:
            r2 = move-exception
            java.lang.String r1 = "ErrorReporter"
            java.lang.String r0 = "Exception in observer"
            X.C010708t.A0L(r1, r0, r2)
        L_0x00ca:
            return r9
        L_0x00cb:
            if (r23 == 0) goto L_0x013c
            java.io.FileInputStream r4 = new java.io.FileInputStream
            r4.<init>(r5)
            java.io.BufferedInputStream r10 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0135 }
            r10.<init>(r4)     // Catch:{ all -> 0x0135 }
            com.facebook.acra.ErrorReporter$CrashReportType r1 = com.facebook.acra.ErrorReporter.CrashReportType.ACRA_CRASH_REPORT     // Catch:{ all -> 0x010a }
            if (r11 != r1) goto L_0x00df
            r9.load(r10)     // Catch:{ all -> 0x010a }
            goto L_0x012a
        L_0x00df:
            com.facebook.acra.ErrorReporter$CrashReportType r1 = com.facebook.acra.ErrorReporter.CrashReportType.ANR_REPORT     // Catch:{ all -> 0x010a }
            if (r11 != r1) goto L_0x0108
            com.facebook.acra.ErrorReporter$SigquitFileHeader r5 = readSigquitFileHeader(r10)     // Catch:{ all -> 0x010a }
            java.lang.String r2 = r5.commandLine     // Catch:{ all -> 0x010a }
            if (r2 == 0) goto L_0x00f0
            java.lang.String r1 = "PROCESS_NAME"
            r9.put(r1, r2)     // Catch:{ all -> 0x010a }
        L_0x00f0:
            java.lang.String r3 = r5.versionCode     // Catch:{ all -> 0x010a }
            java.lang.String r2 = r5.versionName     // Catch:{ all -> 0x010a }
        L_0x00f4:
            r8.slurpAttachment(r9, r10, r11, r12)     // Catch:{ all -> 0x010a }
            boolean r1 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x010a }
            if (r1 != 0) goto L_0x012a
            java.lang.String r1 = "APP_VERSION_CODE"
            r9.put(r1, r3)     // Catch:{ all -> 0x010a }
            java.lang.String r1 = "APP_VERSION_NAME"
            r9.put(r1, r2)     // Catch:{ all -> 0x010a }
            goto L_0x012a
        L_0x0108:
            r2 = r3
            goto L_0x00f4
        L_0x010a:
            r5 = move-exception
            java.lang.String r3 = "REPORT_LOAD_THROW"
            java.lang.String r2 = "throwable: "
            java.lang.String r1 = r5.getMessage()     // Catch:{ all -> 0x012e }
            java.lang.String r1 = X.AnonymousClass08S.A0J(r2, r1)     // Catch:{ all -> 0x012e }
            r9.put(r3, r1)     // Catch:{ all -> 0x012e }
            java.lang.String r3 = "ACRA"
            java.lang.String r2 = "Could not load crash report: %s. File will be deleted."
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x012e }
            X.C010708t.A0U(r3, r5, r2, r1)     // Catch:{ all -> 0x012e }
            android.content.Context r1 = r8.mContext     // Catch:{ all -> 0x012e }
            r1.deleteFile(r0)     // Catch:{ all -> 0x012e }
        L_0x012a:
            r10.close()     // Catch:{ all -> 0x0135 }
            goto L_0x0154
        L_0x012e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0130 }
        L_0x0130:
            r0 = move-exception
            r10.close()     // Catch:{ all -> 0x0134 }
        L_0x0134:
            throw r0     // Catch:{ all -> 0x0135 }
        L_0x0135:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0137 }
        L_0x0137:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x013b }
        L_0x013b:
            throw r0
        L_0x013c:
            java.io.FileInputStream r12 = new java.io.FileInputStream
            r12.<init>(r5)
            java.util.Map r2 = r9.mInputStreamFields
            java.lang.String r1 = r11.attachmentField
            com.facebook.acra.util.InputStreamField r11 = new com.facebook.acra.util.InputStreamField
            long r15 = r5.length()
            r13 = 1
            r14 = 0
            r11.<init>(r12, r13, r14, r15)
            r2.put(r1, r11)
            goto L_0x0157
        L_0x0154:
            r4.close()
        L_0x0157:
            java.lang.String r1 = "ACRA_REPORT_FILENAME"
            r9.put(r1, r0)
            r8.backfillCrashReportData(r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.ErrorReporter.loadCrashReport(java.io.File, java.io.RandomAccessFile, com.facebook.acra.ErrorReporter$CrashReportType, long, boolean):com.facebook.acra.CrashReportData");
    }

    public int prepareANRReport(String str, FileGenerator fileGenerator) {
        Object obj = UNCAUGHT_EXCEPTION_LOCK;
        synchronized (obj) {
            obj.notify();
        }
        return buildCachedCrashReport(CrashReportType.ANR_REPORT, str, null, fileGenerator);
    }

    public void prepareANRReport(FileGenerator fileGenerator) {
        Object obj = UNCAUGHT_EXCEPTION_LOCK;
        synchronized (obj) {
            try {
                obj.notify();
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        synchronized (ANR_REPORTING_LOCK) {
            try {
                prepareReports(Integer.MAX_VALUE, fileGenerator, false, CrashReportType.ANR_REPORT);
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public void prepareANRReport(File file, FileGenerator fileGenerator) {
        Object obj = UNCAUGHT_EXCEPTION_LOCK;
        synchronized (obj) {
            try {
                obj.notify();
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        synchronized (ANR_REPORTING_LOCK) {
            try {
                buildCachedCrashReport(CrashReportType.ANR_REPORT, null, file, fileGenerator);
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    private static void reportSoftError(Throwable th, String str, String str2, ErrorReporter errorReporter) {
        reportSoftError(th, str, str2, null, errorReporter);
    }

    private static void reportSoftError(Throwable th, String str, String str2, String str3, ErrorReporter errorReporter) {
        CrashReportData crashReportData = new CrashReportData();
        crashReportData.put(ErrorReportingConstants.SOFT_ERROR_CATEGORY, str);
        crashReportData.put(ErrorReportingConstants.SOFT_ERROR_MESSAGE, str2);
        if (str3 != null) {
            crashReportData.put(ReportField.SESSION_ID, str3);
        }
        errorReporter.handleException(th, crashReportData);
    }

    private void tryLogInternalError(String str, Throwable th) {
        if (str == null) {
            str = "???";
        }
        try {
            C010708t.A0U(ACRA.LOG_TAG, th, "internal ACRA error: %s: ", str);
        } catch (Throwable unused) {
        }
    }

    private void tryLogInternalError(Throwable th) {
        tryLogInternalError(null, th);
    }

    public final class FifoSpoolComparator implements Comparator {
        public FifoSpoolComparator() {
        }

        public int compare(Spool.Descriptor descriptor, Spool.Descriptor descriptor2) {
            long j = descriptor.lastModifiedTime;
            long j2 = descriptor2.lastModifiedTime;
            if (j == j2) {
                return 0;
            }
            return j < j2 ? -1 : 1;
        }
    }

    public class CrashAttachmentException extends Throwable {
        public CrashAttachmentException() {
        }
    }

    public class SigquitFileHeader {
        public String commandLine;
        public String versionCode;
        public String versionName;

        public SigquitFileHeader() {
        }
    }
}
