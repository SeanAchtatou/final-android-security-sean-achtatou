package com.facebook.acra;

import java.util.Map;

public interface BlackBoxRecorderControl {
    public static final int JAVA_CRASH = 1;

    void awaitForBlackBoxTraceCompletion(Object obj);

    Object captureBlackBoxTrace(Map map, int i);
}
