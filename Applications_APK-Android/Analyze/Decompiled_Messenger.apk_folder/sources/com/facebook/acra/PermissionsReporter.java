package com.facebook.acra;

import X.C010708t;
import android.content.Context;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class PermissionsReporter {
    private static final String[] ALL_PERMISSIONS_SAMPLES = {"android.permission.READ_CALENDAR", "android.permission.CAMERA", "android.permission.READ_CONTACTS", "android.permission.ACCESS_FINE_LOCATION", "android.permission.RECORD_AUDIO", "android.permission.READ_PHONE_STATE", "android.permission.BODY_SENSORS", "android.permission.SEND_SMS", "android.permission.READ_EXTERNAL_STORAGE"};
    public static final String TAG = "PermissionsReporter";

    private static boolean isPermissionGranted(Context context, String str) {
        try {
            return context.checkCallingOrSelfPermission(str) == 0;
        } catch (RuntimeException unused) {
            return false;
        }
    }

    public static String getAppGrantedPermissions(Context context) {
        JSONObject jSONObject = new JSONObject();
        int i = 0;
        while (true) {
            String[] strArr = ALL_PERMISSIONS_SAMPLES;
            if (i >= strArr.length) {
                return jSONObject.toString();
            }
            String groupPermission = getGroupPermission(strArr[i]);
            int lastIndexOf = groupPermission.lastIndexOf(46);
            if (lastIndexOf >= 0) {
                groupPermission = groupPermission.substring(lastIndexOf + 1);
            }
            try {
                jSONObject.put(groupPermission, isPermissionGranted(context, ALL_PERMISSIONS_SAMPLES[i]));
            } catch (JSONException e) {
                C010708t.A0R(TAG, e, "Caught JSONException");
            }
            i++;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0060, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0074, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007e, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0089, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0095, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a1, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ac, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b8, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c4, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d0, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00dc, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00e7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fe, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x010a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getGroupPermission(java.lang.String r1) {
        /*
            int r0 = r1.hashCode()
            switch(r0) {
                case -2062386608: goto L_0x0102;
                case -1928411001: goto L_0x00f7;
                case -1921431796: goto L_0x00eb;
                case -1888586689: goto L_0x00e0;
                case -1479758289: goto L_0x00d4;
                case -1238066820: goto L_0x00c8;
                case -895673731: goto L_0x00bc;
                case -406040016: goto L_0x00b0;
                case -63024214: goto L_0x00a5;
                case -5573545: goto L_0x0099;
                case 52602690: goto L_0x008d;
                case 112197485: goto L_0x0081;
                case 214526995: goto L_0x0077;
                case 463403621: goto L_0x006d;
                case 603653886: goto L_0x0063;
                case 610633091: goto L_0x0058;
                case 784519842: goto L_0x004d;
                case 952819282: goto L_0x0042;
                case 1271781903: goto L_0x0038;
                case 1365911975: goto L_0x002d;
                case 1831139720: goto L_0x0022;
                case 1977429404: goto L_0x0018;
                case 2133799037: goto L_0x000d;
                default: goto L_0x0007;
            }
        L_0x0007:
            r1 = -1
        L_0x0008:
            switch(r1) {
                case 0: goto L_0x0126;
                case 1: goto L_0x0126;
                case 2: goto L_0x0123;
                case 3: goto L_0x0120;
                case 4: goto L_0x0120;
                case 5: goto L_0x0120;
                case 6: goto L_0x011d;
                case 7: goto L_0x011d;
                case 8: goto L_0x011a;
                case 9: goto L_0x0117;
                case 10: goto L_0x0117;
                case 11: goto L_0x0117;
                case 12: goto L_0x0117;
                case 13: goto L_0x0117;
                case 14: goto L_0x0117;
                case 15: goto L_0x0117;
                case 16: goto L_0x0114;
                case 17: goto L_0x0111;
                case 18: goto L_0x0111;
                case 19: goto L_0x0111;
                case 20: goto L_0x0111;
                case 21: goto L_0x010e;
                case 22: goto L_0x010e;
                default: goto L_0x000b;
            }
        L_0x000b:
            r0 = 0
            return r0
        L_0x000d:
            java.lang.String r0 = "com.android.voicemail.permission.ADD_VOICEMAIL"
            boolean r0 = r1.equals(r0)
            r1 = 13
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0018:
            java.lang.String r0 = "android.permission.READ_CONTACTS"
            boolean r0 = r1.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0022:
            java.lang.String r0 = "android.permission.RECORD_AUDIO"
            boolean r0 = r1.equals(r0)
            r1 = 8
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x002d:
            java.lang.String r0 = "android.permission.WRITE_EXTERNAL_STORAGE"
            boolean r0 = r1.equals(r0)
            r1 = 22
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0038:
            java.lang.String r0 = "android.permission.GET_ACCOUNTS"
            boolean r0 = r1.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0042:
            java.lang.String r0 = "android.permission.PROCESS_OUTGOING_CALLS"
            boolean r0 = r1.equals(r0)
            r1 = 15
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x004d:
            java.lang.String r0 = "android.permission.USE_SIP"
            boolean r0 = r1.equals(r0)
            r1 = 14
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0058:
            java.lang.String r0 = "android.permission.WRITE_CALL_LOG"
            boolean r0 = r1.equals(r0)
            r1 = 12
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0063:
            java.lang.String r0 = "android.permission.WRITE_CALENDAR"
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x006d:
            java.lang.String r0 = "android.permission.CAMERA"
            boolean r0 = r1.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0077:
            java.lang.String r0 = "android.permission.WRITE_CONTACTS"
            boolean r0 = r1.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0081:
            java.lang.String r0 = "android.permission.CALL_PHONE"
            boolean r0 = r1.equals(r0)
            r1 = 10
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x008d:
            java.lang.String r0 = "android.permission.SEND_SMS"
            boolean r0 = r1.equals(r0)
            r1 = 17
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0099:
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            boolean r0 = r1.equals(r0)
            r1 = 9
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00a5:
            java.lang.String r0 = "android.permission.ACCESS_COARSE_LOCATION"
            boolean r0 = r1.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00b0:
            java.lang.String r0 = "android.permission.READ_EXTERNAL_STORAGE"
            boolean r0 = r1.equals(r0)
            r1 = 21
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00bc:
            java.lang.String r0 = "android.permission.RECEIVE_SMS"
            boolean r0 = r1.equals(r0)
            r1 = 18
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00c8:
            java.lang.String r0 = "android.permission.BODY_SENSORS"
            boolean r0 = r1.equals(r0)
            r1 = 16
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00d4:
            java.lang.String r0 = "android.permission.RECEIVE_WAP_PUSH"
            boolean r0 = r1.equals(r0)
            r1 = 20
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00e0:
            java.lang.String r0 = "android.permission.ACCESS_FINE_LOCATION"
            boolean r0 = r1.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00eb:
            java.lang.String r0 = "android.permission.READ_CALL_LOG"
            boolean r0 = r1.equals(r0)
            r1 = 11
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00f7:
            java.lang.String r0 = "android.permission.READ_CALENDAR"
            boolean r0 = r1.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0102:
            java.lang.String r0 = "android.permission.READ_SMS"
            boolean r0 = r1.equals(r0)
            r1 = 19
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x010e:
            java.lang.String r0 = "android.permission-group.STORAGE"
            return r0
        L_0x0111:
            java.lang.String r0 = "android.permission-group.SMS"
            return r0
        L_0x0114:
            java.lang.String r0 = "android.permission-group.SENSORS"
            return r0
        L_0x0117:
            java.lang.String r0 = "android.permission-group.PHONE"
            return r0
        L_0x011a:
            java.lang.String r0 = "android.permission-group.MICROPHONE"
            return r0
        L_0x011d:
            java.lang.String r0 = "android.permission-group.LOCATION"
            return r0
        L_0x0120:
            java.lang.String r0 = "android.permission-group.CONTACTS"
            return r0
        L_0x0123:
            java.lang.String r0 = "android.permission-group.CAMERA"
            return r0
        L_0x0126:
            java.lang.String r0 = "android.permission-group.CALENDAR"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.PermissionsReporter.getGroupPermission(java.lang.String):java.lang.String");
    }
}
