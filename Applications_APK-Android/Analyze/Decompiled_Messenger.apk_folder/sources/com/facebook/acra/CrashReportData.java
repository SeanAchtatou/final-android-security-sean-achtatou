package com.facebook.acra;

import X.AnonymousClass1Y3;
import com.facebook.forker.Process;
import io.card.payment.BuildConfig;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class CrashReportData extends LinkedHashMap<String, String> {
    private static final int CONTINUE = 3;
    private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final int IGNORE = 5;
    private static final int KEY_DONE = 4;
    private static final int NONE = 0;
    private static final String PROP_DTD_NAME = "http://java.sun.com/dtd/properties.dtd";
    private static final int SLASH = 1;
    private static final int UNICODE = 2;
    private static String lineSeparator = "\n";
    public ArrayList fieldFailures;
    public Throwable generatingIoError;
    public Map mInputStreamFields = new LinkedHashMap();
    public boolean throwAwayWrites;

    public static Writer getWriter(OutputStream outputStream) {
        try {
            return new OutputStreamWriter(outputStream, "ISO8859_1");
        } catch (UnsupportedEncodingException unused) {
            return null;
        }
    }

    private void mergeInputStreamFields(CrashReportData crashReportData, boolean z) {
        for (Map.Entry entry : crashReportData.mInputStreamFields.entrySet()) {
            if (z || !this.mInputStreamFields.containsKey(entry.getKey())) {
                this.mInputStreamFields.put(entry.getKey(), entry.getValue());
            }
        }
    }

    private static void storeComment(Writer writer, String str) {
        writer.write("#");
        writer.write(str);
        writer.write(lineSeparator);
    }

    private static void storeKeyValuePair(Writer writer, String str, String str2) {
        if (str2 == null) {
            str2 = BuildConfig.FLAVOR;
        }
        int length = str.length() + str2.length() + 1;
        StringBuilder sb = new StringBuilder(length + (length / 5));
        dumpString(sb, str, true);
        sb.append('=');
        dumpString(sb, str2, false);
        sb.append(lineSeparator);
        writer.write(sb.toString());
        writer.flush();
    }

    private String substitutePredefinedEntries(String str) {
        return str.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("'", "&apos;").replaceAll("\"", "&quot;");
    }

    public String put(String str, String str2, Writer writer) {
        String str3;
        if (!this.throwAwayWrites) {
            str3 = (String) put(str, str2);
        } else {
            str3 = null;
        }
        if (writer != null) {
            storeKeyValuePair(writer, str, str2);
        }
        return str3;
    }

    private static void dumpString(Appendable appendable, String str, boolean z) {
        int length = str.length();
        int i = 0;
        if (!z && length > 0 && str.charAt(0) == ' ') {
            appendable.append("\\ ");
            i = 1;
        }
        while (i < length) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case Process.SIGKILL /*9*/:
                    appendable.append("\\t");
                    break;
                case AnonymousClass1Y3.A01 /*10*/:
                    appendable.append("\\n");
                    break;
                case AnonymousClass1Y3.A02 /*11*/:
                default:
                    if ((z && charAt == ' ') || charAt == '\\' || charAt == '#' || charAt == '!' || charAt == ':') {
                        appendable.append('\\');
                    }
                    if (charAt >= ' ' && charAt <= '~') {
                        appendable.append(charAt);
                        break;
                    } else {
                        appendable.append("\\u");
                        char[] cArr = HEX_DIGITS;
                        appendable.append(cArr[(charAt >>> 12) & 15]);
                        appendable.append(cArr[(charAt >>> 8) & 15]);
                        appendable.append(cArr[(charAt >>> 4) & 15]);
                        appendable.append(cArr[(charAt >>> 0) & 15]);
                        break;
                    }
                case AnonymousClass1Y3.A03 /*12*/:
                    appendable.append("\\f");
                    break;
                case 13:
                    appendable.append("\\r");
                    break;
            }
            i++;
        }
    }

    private boolean isEbcdic(BufferedInputStream bufferedInputStream) {
        byte read;
        do {
            read = (byte) bufferedInputStream.read();
            if (read == -1 || read == 35 || read == 10 || read == 61) {
                return false;
            }
        } while (read != 21);
        return true;
    }

    public Map getInputStreamFields() {
        return this.mInputStreamFields;
    }

    public void merge(Map map, boolean z, Writer writer) {
        for (String str : map.keySet()) {
            String str2 = (String) map.get(str);
            if (str2 != null && (z || get(str) == null)) {
                put(str, str2, writer);
            }
        }
        if (map instanceof CrashReportData) {
            mergeInputStreamFields((CrashReportData) map, false);
        }
    }

    public void mergeFieldOverwrite(Map map, String str, Writer writer) {
        String str2 = (String) map.get(str);
        if (str2 != null) {
            put(str, str2, writer);
        }
    }

    public void putAll(Map map, Writer writer) {
        putAll(map);
        if (writer != null) {
            for (Map.Entry entry : map.entrySet()) {
                storeKeyValuePair(writer, (String) entry.getKey(), (String) entry.getValue());
            }
            writer.flush();
        }
    }

    public Object setProperty(String str, String str2) {
        return put(str, str2);
    }

    public void save(OutputStream outputStream, String str) {
        try {
            store(outputStream, str);
        } catch (IOException unused) {
        }
    }

    public CrashReportData() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void
     arg types: [java.util.Map, int, ?[OBJECT, ARRAY]]
     candidates:
      ClspMth{java.util.HashMap.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      ClspMth{java.util.Map.merge(java.lang.Object, java.lang.Object, java.util.function.BiFunction):V}
      com.facebook.acra.CrashReportData.merge(java.util.Map, boolean, java.io.Writer):void */
    public CrashReportData(Map map) {
        try {
            merge(map, true, (Writer) null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getProperty(String str) {
        return (String) super.get(str);
    }

    public String getProperty(String str, String str2) {
        String str3 = (String) super.get(str);
        return str3 == null ? str2 : str3;
    }

    public void list(PrintStream printStream) {
        if (printStream != null) {
            StringBuilder sb = new StringBuilder(80);
            for (Map.Entry entry : entrySet()) {
                if (entry.getValue() != null) {
                    sb.append((String) entry.getKey());
                    sb.append('=');
                    int length = ((String) entry.getValue()).length();
                    String str = (String) entry.getValue();
                    if (length > 40) {
                        sb.append(str.substring(0, 37));
                        sb.append("...");
                    } else {
                        sb.append(str);
                    }
                    printStream.println(sb.toString());
                    sb.setLength(0);
                }
            }
            return;
        }
        throw new NullPointerException();
    }

    public void list(PrintWriter printWriter) {
        if (printWriter != null) {
            StringBuilder sb = new StringBuilder(80);
            for (Map.Entry entry : entrySet()) {
                if (entry.getValue() != null) {
                    sb.append((String) entry.getKey());
                    sb.append('=');
                    int length = ((String) entry.getValue()).length();
                    String str = (String) entry.getValue();
                    if (length > 40) {
                        sb.append(str.substring(0, 37));
                        sb.append("...");
                    } else {
                        sb.append(str);
                    }
                    printWriter.println(sb.toString());
                    sb.setLength(0);
                }
            }
            return;
        }
        throw new NullPointerException();
    }

    public synchronized void load(InputStream inputStream) {
        InputStreamReader inputStreamReader;
        if (inputStream != null) {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            bufferedInputStream.mark(Integer.MAX_VALUE);
            boolean isEbcdic = isEbcdic(bufferedInputStream);
            bufferedInputStream.reset();
            if (!isEbcdic) {
                inputStreamReader = new InputStreamReader(bufferedInputStream, "ISO8859-1");
            } else {
                inputStreamReader = new InputStreamReader(bufferedInputStream);
            }
            load(inputStreamReader);
        } else {
            throw new NullPointerException();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:98:0x00f6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void load(java.io.Reader r17) {
        /*
            r16 = this;
            r7 = r16
            monitor-enter(r16)
            r0 = 40
            char[] r10 = new char[r0]     // Catch:{ all -> 0x0162 }
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ all -> 0x0162 }
            r0 = r17
            r11.<init>(r0)     // Catch:{ all -> 0x0162 }
            r2 = 2
            r9 = 4
            r12 = 1
            r8 = -1
            r13 = 0
            r1 = 0
            r3 = 0
            r6 = 0
            r5 = -1
            r4 = 0
        L_0x0018:
            r15 = 1
        L_0x0019:
            int r0 = r11.read()     // Catch:{ all -> 0x0162 }
            if (r0 == r8) goto L_0x012d
            if (r0 == 0) goto L_0x012d
            char r0 = (char) r0     // Catch:{ all -> 0x0162 }
            int r14 = r10.length     // Catch:{ all -> 0x0162 }
            if (r1 != r14) goto L_0x002d
            int r14 = r14 << 1
            char[] r14 = new char[r14]     // Catch:{ all -> 0x0162 }
            java.lang.System.arraycopy(r10, r13, r14, r13, r1)     // Catch:{ all -> 0x0162 }
            r10 = r14
        L_0x002d:
            r14 = 133(0x85, float:1.86E-43)
            r13 = 10
            if (r3 != r2) goto L_0x0056
            r2 = 16
            int r2 = java.lang.Character.digit(r0, r2)     // Catch:{ all -> 0x0162 }
            if (r2 < 0) goto L_0x0044
            int r4 = r4 << 4
            int r4 = r4 + r2
            int r6 = r6 + 1
            r2 = 2
            if (r6 < r9) goto L_0x00e1
            goto L_0x0048
        L_0x0044:
            if (r6 > r9) goto L_0x0048
            goto L_0x0139
        L_0x0048:
            int r3 = r1 + 1
            char r2 = (char) r4     // Catch:{ all -> 0x0162 }
            r10[r1] = r2     // Catch:{ all -> 0x0162 }
            if (r0 == r13) goto L_0x0054
            if (r0 == r14) goto L_0x0054
            r1 = r3
            r2 = 2
            goto L_0x00b7
        L_0x0054:
            r1 = r3
            r3 = 0
        L_0x0056:
            r2 = 13
            if (r3 != r12) goto L_0x008e
            if (r0 == r13) goto L_0x0106
            if (r0 == r2) goto L_0x008a
            r2 = 98
            if (r0 == r2) goto L_0x0087
            r2 = 102(0x66, float:1.43E-43)
            if (r0 == r2) goto L_0x0084
            r2 = 110(0x6e, float:1.54E-43)
            if (r0 == r2) goto L_0x0081
            r2 = 114(0x72, float:1.6E-43)
            if (r0 == r2) goto L_0x007e
            if (r0 == r14) goto L_0x0106
            r2 = 116(0x74, float:1.63E-43)
            if (r0 == r2) goto L_0x00f1
            r2 = 117(0x75, float:1.64E-43)
            if (r0 != r2) goto L_0x00f3
            r2 = 2
            r13 = 0
            r3 = 2
            r6 = 0
            r4 = 0
            goto L_0x0019
        L_0x007e:
            r0 = 13
            goto L_0x00f3
        L_0x0081:
            r0 = 10
            goto L_0x00f3
        L_0x0084:
            r0 = 12
            goto L_0x00f3
        L_0x0087:
            r0 = 8
            goto L_0x00f3
        L_0x008a:
            r2 = 2
            r13 = 0
            r3 = 3
            goto L_0x0019
        L_0x008e:
            if (r0 == r13) goto L_0x0103
            if (r0 == r2) goto L_0x010b
            r12 = 33
            if (r0 == r12) goto L_0x00bb
            r12 = 35
            if (r0 == r12) goto L_0x00bb
            r2 = 58
            if (r0 == r2) goto L_0x00b2
            r2 = 61
            if (r0 == r2) goto L_0x00b2
            r2 = 92
            if (r0 == r2) goto L_0x00a9
            if (r0 == r14) goto L_0x010b
            goto L_0x00be
        L_0x00a9:
            if (r3 != r9) goto L_0x00ac
            r5 = r1
        L_0x00ac:
            r2 = 2
            r12 = 1
            r13 = 0
            r3 = 1
            goto L_0x0019
        L_0x00b2:
            if (r5 != r8) goto L_0x00be
            r5 = r1
            r2 = 2
            r12 = 1
        L_0x00b7:
            r13 = 0
            r3 = 0
            goto L_0x0019
        L_0x00bb:
            if (r15 == 0) goto L_0x00be
            goto L_0x00e4
        L_0x00be:
            boolean r12 = java.lang.Character.isWhitespace(r0)     // Catch:{ all -> 0x0162 }
            r2 = 5
            if (r12 == 0) goto L_0x00eb
            r2 = 3
            if (r3 != r2) goto L_0x00c9
            r3 = 5
        L_0x00c9:
            if (r1 == 0) goto L_0x00df
            if (r1 == r5) goto L_0x00df
            r2 = 5
            if (r3 == r2) goto L_0x00df
            if (r5 != r8) goto L_0x00eb
            r2 = 2
            r12 = 1
            r13 = 0
            r3 = 4
            goto L_0x0019
        L_0x00d8:
            char r0 = (char) r0     // Catch:{ all -> 0x0162 }
            if (r0 == r2) goto L_0x00df
            if (r0 == r13) goto L_0x00df
            if (r0 != r14) goto L_0x00e4
        L_0x00df:
            r2 = 2
            r12 = 1
        L_0x00e1:
            r13 = 0
            goto L_0x0019
        L_0x00e4:
            int r0 = r11.read()     // Catch:{ all -> 0x0162 }
            if (r0 == r8) goto L_0x00df
            goto L_0x00d8
        L_0x00eb:
            if (r3 == r2) goto L_0x00f3
            r2 = 3
            if (r3 != r2) goto L_0x00f4
            goto L_0x00f3
        L_0x00f1:
            r0 = 9
        L_0x00f3:
            r3 = 0
        L_0x00f4:
            if (r3 != r9) goto L_0x00f8
            r5 = r1
            r3 = 0
        L_0x00f8:
            int r2 = r1 + 1
            r10[r1] = r0     // Catch:{ all -> 0x0162 }
            r1 = r2
            r2 = 2
            r12 = 1
            r13 = 0
            r15 = 0
            goto L_0x0019
        L_0x0103:
            r0 = 3
            if (r3 != r0) goto L_0x010b
        L_0x0106:
            r2 = 2
            r13 = 0
            r3 = 5
            goto L_0x0019
        L_0x010b:
            if (r1 > 0) goto L_0x0111
            if (r1 != 0) goto L_0x0125
            if (r5 != 0) goto L_0x0125
        L_0x0111:
            if (r5 != r8) goto L_0x0114
            r5 = r1
        L_0x0114:
            java.lang.String r2 = new java.lang.String     // Catch:{ all -> 0x0162 }
            r0 = 0
            r2.<init>(r10, r0, r1)     // Catch:{ all -> 0x0162 }
            java.lang.String r1 = r2.substring(r0, r5)     // Catch:{ all -> 0x0162 }
            java.lang.String r0 = r2.substring(r5)     // Catch:{ all -> 0x0162 }
            r7.put(r1, r0)     // Catch:{ all -> 0x0162 }
        L_0x0125:
            r1 = 0
            r2 = 2
            r12 = 1
            r13 = 0
            r3 = 0
            r5 = -1
            goto L_0x0018
        L_0x012d:
            if (r3 != r2) goto L_0x0141
            if (r6 > r9) goto L_0x0141
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0162 }
            java.lang.String r0 = "luni.08"
            r1.<init>(r0)     // Catch:{ all -> 0x0162 }
            goto L_0x0140
        L_0x0139:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0162 }
            java.lang.String r0 = "luni.09"
            r1.<init>(r0)     // Catch:{ all -> 0x0162 }
        L_0x0140:
            throw r1     // Catch:{ all -> 0x0162 }
        L_0x0141:
            if (r5 != r8) goto L_0x0146
            if (r1 <= 0) goto L_0x0146
            r5 = r1
        L_0x0146:
            if (r5 < 0) goto L_0x0160
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0162 }
            r0.<init>(r10, r13, r1)     // Catch:{ all -> 0x0162 }
            java.lang.String r2 = r0.substring(r13, r5)     // Catch:{ all -> 0x0162 }
            java.lang.String r1 = r0.substring(r5)     // Catch:{ all -> 0x0162 }
            if (r3 != r12) goto L_0x015d
            java.lang.String r0 = "\u0000"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0162 }
        L_0x015d:
            r7.put(r2, r1)     // Catch:{ all -> 0x0162 }
        L_0x0160:
            monitor-exit(r16)
            return
        L_0x0162:
            r0 = move-exception
            monitor-exit(r16)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.CrashReportData.load(java.io.Reader):void");
    }

    public synchronized void store(OutputStream outputStream, String str) {
        store(getWriter(outputStream), str);
    }

    public synchronized void store(Writer writer, String str) {
        if (str != null) {
            storeComment(writer, str);
        }
        for (Map.Entry entry : entrySet()) {
            storeKeyValuePair(writer, (String) entry.getKey(), (String) entry.getValue());
        }
        writer.flush();
    }

    public void storeToXML(OutputStream outputStream, String str) {
        storeToXML(outputStream, str, LogCatCollector.UTF_8_ENCODING);
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x000e */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x00ad=Splitter:B:19:0x00ad, B:5:0x000e=Splitter:B:5:0x000e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void storeToXML(java.io.OutputStream r7, java.lang.String r8, java.lang.String r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            if (r7 == 0) goto L_0x00ad
            if (r9 == 0) goto L_0x00ad
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r9)     // Catch:{ IllegalCharsetNameException -> 0x001e, UnsupportedCharsetException -> 0x000e }
            java.lang.String r1 = r0.name()     // Catch:{ IllegalCharsetNameException -> 0x001e, UnsupportedCharsetException -> 0x000e }
            goto L_0x002d
        L_0x000e:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ all -> 0x00b3 }
            java.lang.String r1 = "Warning: encoding "
            java.lang.String r0 = " is not supported, using UTF-8 as default encoding"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r9, r0)     // Catch:{ all -> 0x00b3 }
            r2.println(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r1 = "UTF-8"
            goto L_0x002d
        L_0x001e:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ all -> 0x00b3 }
            java.lang.String r1 = "Warning: encoding name "
            java.lang.String r0 = " is illegal, using UTF-8 as default encoding"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r9, r0)     // Catch:{ all -> 0x00b3 }
            r2.println(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r1 = "UTF-8"
        L_0x002d:
            java.io.PrintStream r4 = new java.io.PrintStream     // Catch:{ all -> 0x00b3 }
            r0 = 0
            r4.<init>(r7, r0, r1)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "<?xml version=\"1.0\" encoding=\""
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            r4.print(r1)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "\"?>"
            r4.println(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "<!DOCTYPE properties SYSTEM \""
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "http://java.sun.com/dtd/properties.dtd"
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r5 = "\">"
            r4.println(r5)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "<properties>"
            r4.println(r0)     // Catch:{ all -> 0x00b3 }
            if (r8 == 0) goto L_0x0067
            java.lang.String r0 = "<comment>"
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = r6.substitutePredefinedEntries(r8)     // Catch:{ all -> 0x00b3 }
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "</comment>"
            r4.println(r0)     // Catch:{ all -> 0x00b3 }
        L_0x0067:
            java.util.Set r0 = r6.entrySet()     // Catch:{ all -> 0x00b3 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x00b3 }
        L_0x006f:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x00b3 }
            if (r0 == 0) goto L_0x00a3
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x00b3 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x00b3 }
            java.lang.Object r2 = r0.getKey()     // Catch:{ all -> 0x00b3 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x00b3 }
            java.lang.Object r1 = r0.getValue()     // Catch:{ all -> 0x00b3 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "<entry key=\""
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = r6.substitutePredefinedEntries(r2)     // Catch:{ all -> 0x00b3 }
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            r4.print(r5)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = r6.substitutePredefinedEntries(r1)     // Catch:{ all -> 0x00b3 }
            r4.print(r0)     // Catch:{ all -> 0x00b3 }
            java.lang.String r0 = "</entry>"
            r4.println(r0)     // Catch:{ all -> 0x00b3 }
            goto L_0x006f
        L_0x00a3:
            java.lang.String r0 = "</properties>"
            r4.println(r0)     // Catch:{ all -> 0x00b3 }
            r4.flush()     // Catch:{ all -> 0x00b3 }
            monitor-exit(r6)
            return
        L_0x00ad:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x00b3 }
            r0.<init>()     // Catch:{ all -> 0x00b3 }
            throw r0     // Catch:{ all -> 0x00b3 }
        L_0x00b3:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.acra.CrashReportData.storeToXML(java.io.OutputStream, java.lang.String, java.lang.String):void");
    }
}
