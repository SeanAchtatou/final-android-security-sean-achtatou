package com.facebook.acra.sender;

import X.AnonymousClass08S;
import android.net.Uri;
import com.facebook.acra.ACRA;
import com.facebook.acra.CrashReportData;
import com.facebook.acra.config.AcraReportingConfig;
import com.facebook.acra.constants.ReportField;
import com.facebook.acra.util.ACRAResponse;
import com.facebook.acra.util.HttpConnectionProvider;
import com.facebook.acra.util.HttpRequest;
import com.facebook.acra.util.HttpRequestMultipart;
import com.facebook.acra.util.SSLConnectionProvider;
import com.facebook.acra.util.UnsafeConnectionProvider;
import io.card.payment.BuildConfig;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;

public class HttpPostSender implements FlexibleReportSender {
    private final AcraReportingConfig mConfig;
    private Uri mCrashReportEndpoint;
    private Proxy mProxy;
    private boolean mSkipSslCertChecks;
    public boolean mUseMultipartPost;
    public boolean mUseZstd;

    private void sendInternal(CrashReportData crashReportData) {
        Proxy proxy;
        HttpConnectionProvider sSLConnectionProvider;
        URL url = new URL(this.mCrashReportEndpoint.toString());
        url.toString();
        AcraReportingConfig acraReportingConfig = this.mConfig;
        String str = null;
        if (acraReportingConfig.allowProxy()) {
            proxy = this.mProxy;
        } else {
            proxy = null;
        }
        if (!this.mSkipSslCertChecks || !acraReportingConfig.allowUnsafeConnectionsForDebugging()) {
            sSLConnectionProvider = new SSLConnectionProvider(acraReportingConfig.socketTimeout(), proxy);
        } else {
            sSLConnectionProvider = new UnsafeConnectionProvider(acraReportingConfig.socketTimeout(), proxy);
        }
        String userAgent = ACRA.mConfig.getUserAgent();
        HashMap hashMap = new HashMap();
        CrashReportData crashReportData2 = crashReportData;
        if (crashReportData.containsKey(ReportField.UID)) {
            str = (String) crashReportData.get(ReportField.UID);
        }
        if (str != null && !str.equals(BuildConfig.FLAVOR) && !str.equals("0")) {
            hashMap.put("Cookie", AnonymousClass08S.A0J("c_user=", str));
        }
        if (this.mUseMultipartPost) {
            HttpRequestMultipart httpRequestMultipart = new HttpRequestMultipart(sSLConnectionProvider);
            httpRequestMultipart.mHeaders = hashMap;
            httpRequestMultipart.sendPost(url, crashReportData2, crashReportData.mInputStreamFields, new ACRAResponse(), userAgent, this.mUseZstd);
            return;
        }
        HttpRequest httpRequest = new HttpRequest(sSLConnectionProvider);
        httpRequest.mHeaders = hashMap;
        httpRequest.sendPost(url, crashReportData, new ACRAResponse(), userAgent);
    }

    public boolean setHost(String str) {
        if (str == null || str.equals(BuildConfig.FLAVOR)) {
            return false;
        }
        if (str.equals(this.mCrashReportEndpoint.getHost())) {
            return true;
        }
        this.mCrashReportEndpoint = this.mCrashReportEndpoint.buildUpon().authority(str).build();
        return true;
    }

    public HttpPostSender(AcraReportingConfig acraReportingConfig) {
        this.mConfig = acraReportingConfig;
        this.mCrashReportEndpoint = Uri.parse(acraReportingConfig.crashReportUrl());
    }

    public boolean getUseMultipartPost() {
        return this.mUseMultipartPost;
    }

    public void send(CrashReportData crashReportData) {
        try {
            sendInternal(crashReportData);
        } catch (Throwable th) {
            throw new ReportSenderException("Error while sending report to Http Post Form.", th);
        }
    }

    public void setProxy(Proxy proxy) {
        this.mProxy = proxy;
    }

    public void setSkipSslCertsChecks(boolean z) {
        this.mSkipSslCertChecks = z;
    }

    public void setUseMultipartPost(boolean z) {
        this.mUseMultipartPost = z;
    }

    public void setUseZstd(boolean z) {
        this.mUseZstd = z;
    }
}
