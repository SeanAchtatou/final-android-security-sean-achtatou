package com.facebook.acra;

import android.os.Looper;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;

public class StackTraceDumper {
    private StackTraceDumper() {
    }

    private static void printThread(PrintWriter printWriter, Thread thread, StackTraceElement[] stackTraceElementArr) {
        printWriter.print(thread);
        printWriter.print(" ");
        printWriter.print(thread.getState());
        printWriter.println(":");
        for (StackTraceElement println : stackTraceElementArr) {
            printWriter.println(println);
        }
        printWriter.println();
    }

    public static void dumpStackTraces(OutputStream outputStream) {
        dumpStackTraces(outputStream, null, null);
    }

    public static void dumpStackTraces(OutputStream outputStream, String str, String str2) {
        PrintWriter printWriter = new PrintWriter(outputStream);
        if (str != null) {
            printWriter.println(str);
            printWriter.println(str2);
        }
        Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        Thread thread = Looper.getMainLooper().getThread();
        for (Map.Entry next : allStackTraces.entrySet()) {
            printThread(printWriter, (Thread) next.getKey(), (StackTraceElement[]) next.getValue());
        }
        if (!allStackTraces.containsKey(thread)) {
            printThread(printWriter, thread, thread.getStackTrace());
        }
        printWriter.flush();
    }
}
