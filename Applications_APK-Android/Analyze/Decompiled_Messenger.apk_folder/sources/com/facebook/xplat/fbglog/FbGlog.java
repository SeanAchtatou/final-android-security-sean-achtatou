package com.facebook.xplat.fbglog;

import X.AnonymousClass01q;
import X.AnonymousClass090;
import X.C010708t;
import X.C011308z;

public class FbGlog {
    private static AnonymousClass090 sCallback;

    public static native void setLogLevel(int i);

    static {
        AnonymousClass01q.A08("fb");
    }

    public static synchronized void ensureSubscribedToBLogLevelChanges() {
        synchronized (FbGlog.class) {
            if (sCallback == null) {
                C011308z r0 = new C011308z();
                sCallback = r0;
                C010708t.A04(r0);
                setLogLevel(C010708t.A01.Aum());
            }
        }
    }
}
