package com.facebook.aborthooks;

import X.AnonymousClass01q;

public abstract class AbortHooks {
    public static native void hookAbort();

    public static native void hookAndroidLogAssert();

    public static native void hookAndroidSetAbortMessage();

    public static native void install();

    public static native void setGlogFatalHandler();

    static {
        AnonymousClass01q.A08("aborthooks");
    }
}
