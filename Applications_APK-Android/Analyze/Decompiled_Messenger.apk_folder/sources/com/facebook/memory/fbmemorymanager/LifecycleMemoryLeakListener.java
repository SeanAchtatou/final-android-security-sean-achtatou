package com.facebook.memory.fbmemorymanager;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass165;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C138056cU;
import X.C28611f5;
import X.C28671fB;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import javax.inject.Singleton;

@Singleton
public final class LifecycleMemoryLeakListener implements AnonymousClass165 {
    private static volatile LifecycleMemoryLeakListener A01;
    public AnonymousClass0UN A00;

    public void BNK(Fragment fragment, int i, int i2, Intent intent) {
    }

    public void BNh(Fragment fragment, Bundle bundle) {
    }

    public void BNp(Fragment fragment, boolean z) {
    }

    public void BNx(Fragment fragment, View view, Bundle bundle) {
    }

    public void BNy(Fragment fragment) {
    }

    public void BOY(Fragment fragment, Fragment fragment2) {
    }

    public void BP9(C138056cU r1) {
    }

    public void BPa(Fragment fragment, Bundle bundle) {
    }

    public void BPf(Bundle bundle) {
    }

    public void BTL(Fragment fragment, Configuration configuration) {
    }

    public void Bi0(Fragment fragment) {
    }

    public void BmO(Fragment fragment) {
    }

    public void Bmy(Fragment fragment, Bundle bundle) {
    }

    public void BoL(Fragment fragment, boolean z) {
    }

    public void BpQ(Fragment fragment) {
    }

    public void Bq5(Fragment fragment) {
    }

    public static final LifecycleMemoryLeakListener A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (LifecycleMemoryLeakListener.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new LifecycleMemoryLeakListener(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void BVy(Fragment fragment) {
        C28671fB A012 = ((C28611f5) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AIJ, this.A00)).A01();
        if (C28671fB.A00().A06().A01() && A012.A0B != null) {
            C28671fB.A01(C28671fB.A00()).A00.A05(fragment);
        }
    }

    private LifecycleMemoryLeakListener(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
