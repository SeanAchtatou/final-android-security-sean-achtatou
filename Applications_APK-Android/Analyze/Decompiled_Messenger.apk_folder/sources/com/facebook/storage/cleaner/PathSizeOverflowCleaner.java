package com.facebook.storage.cleaner;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C25051Yd;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import javax.inject.Singleton;

@Singleton
public final class PathSizeOverflowCleaner {
    private static volatile PathSizeOverflowCleaner A04;
    public AnonymousClass0UN A00;
    public final HashMap A01 = new HashMap();
    public final HashMap A02 = new HashMap();
    public final boolean A03;

    public static final PathSizeOverflowCleaner A01(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (PathSizeOverflowCleaner.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new PathSizeOverflowCleaner(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private PathSizeOverflowCleaner(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(3, r4);
        this.A00 = r2;
        this.A03 = ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, r2)).Aem(282875136051131L);
    }

    public static long A00(File file) {
        File[] listFiles;
        long j = 0;
        if (file.exists()) {
            if (file.isFile()) {
                return file.length();
            }
            LinkedList linkedList = new LinkedList();
            linkedList.add(file);
            while (!linkedList.isEmpty()) {
                File file2 = (File) linkedList.remove();
                if (file2.exists() && (listFiles = file2.listFiles()) != null) {
                    for (File file3 : listFiles) {
                        if (file3.isFile()) {
                            j += file3.length();
                        } else if (file3.isDirectory()) {
                            linkedList.add(file3);
                        }
                    }
                }
            }
        }
        return j;
    }
}
