package com.facebook.storage.monitor.fbapps;

import X.AnonymousClass09P;
import X.AnonymousClass0UX;
import X.AnonymousClass0WD;
import X.AnonymousClass0ZD;
import X.AnonymousClass1XY;
import X.C012109i;
import X.C04750Wa;
import X.C30751ia;
import com.facebook.common.file.FileModule;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.concurrent.ScheduledExecutorService;
import javax.inject.Singleton;

@Singleton
public final class FBAppsStorageResourceMonitor extends C30751ia {
    private static volatile FBAppsStorageResourceMonitor A00;

    public static final FBAppsStorageResourceMonitor A00(AnonymousClass1XY r7) {
        if (A00 == null) {
            synchronized (FBAppsStorageResourceMonitor.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A00 = new FBAppsStorageResourceMonitor(AnonymousClass0UX.A0j(applicationInjector), FileModule.A01(applicationInjector), C04750Wa.A01(applicationInjector), AnonymousClass0ZD.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private FBAppsStorageResourceMonitor(ScheduledExecutorService scheduledExecutorService, C012109i r2, AnonymousClass09P r3, QuickPerformanceLogger quickPerformanceLogger) {
        super(scheduledExecutorService, r2, r3, quickPerformanceLogger);
    }
}
