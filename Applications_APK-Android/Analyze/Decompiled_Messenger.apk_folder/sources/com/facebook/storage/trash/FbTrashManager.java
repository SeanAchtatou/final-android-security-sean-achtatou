package com.facebook.storage.trash;

import X.AnonymousClass07A;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass1OP;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YA;
import X.C200909dA;
import X.C30721iX;
import X.C31051j4;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
public final class FbTrashManager extends C31051j4 implements AnonymousClass1OP {
    private static volatile FbTrashManager A01;
    public AnonymousClass0UN A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private FbTrashManager(X.AnonymousClass1XY r4, android.content.Context r5, X.C30721iX r6) {
        /*
            r3 = this;
            X.0oR r0 = X.C12040oR.A02
            if (r0 != 0) goto L_0x000f
            X.0oR r1 = new X.0oR
            android.content.Context r0 = r5.getApplicationContext()
            r1.<init>(r0)
            X.C12040oR.A02 = r1
        L_0x000f:
            X.0oR r2 = X.C12040oR.A02
            X.1j6 r1 = new X.1j6
            java.lang.String r0 = "fb_trash_manager"
            r1.<init>(r0)
            r0 = 3
            r1.A00 = r0
            java.io.File r0 = r2.A02(r1)
            r3.<init>(r0)
            X.0UN r1 = new X.0UN
            r0 = 2
            r1.<init>(r0, r4)
            r3.A00 = r1
            r6.C0U(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.storage.trash.FbTrashManager.<init>(X.1XY, android.content.Context, X.1iX):void");
    }

    public static final FbTrashManager A00(AnonymousClass1XY r6) {
        if (A01 == null) {
            synchronized (FbTrashManager.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A01 = new FbTrashManager(applicationInjector, AnonymousClass1YA.A00(applicationInjector), C30721iX.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A03() {
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aoc, this.A00), new C200909dA(this), -89022099);
    }

    public void trimToMinimum() {
        A03();
    }

    public void trimToNothing() {
        A03();
    }
}
