package com.facebook.graphql.calls;

import X.C06390bQ;
import X.C12240os;
import X.C16910xz;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@JsonSerialize(using = GraphQlCallInputSerializer.class)
public abstract class GraphQlCallInput {
    private static final C06390bQ A02 = C06390bQ.A00();
    public C12240os A00 = null;
    public C06390bQ A01 = A02;

    private Object A00(Object obj) {
        ArrayList arrayList;
        if (obj == null) {
            return null;
        }
        if (obj instanceof C16910xz) {
            C16910xz r5 = (C16910xz) obj;
            if (r5.A00.size() > 0) {
                if (r5.A00.get(0) instanceof C12240os) {
                    arrayList = new ArrayList(r5.A00.size());
                    for (int i = 0; i < r5.A00.size(); i++) {
                        if (r5.A00.get(i) != null) {
                            arrayList.add((Map) A00(r5.A00.get(i)));
                        }
                    }
                    return arrayList;
                }
            }
            if (r5.A00.size() > 0) {
                if (r5.A00.get(0) instanceof C16910xz) {
                    arrayList = new ArrayList(r5.A00.size());
                    for (int i2 = 0; i2 < r5.A00.size(); i2++) {
                        if (r5.A00.get(i2) != null) {
                            arrayList.add((List) A00(r5.A00.get(i2)));
                        }
                    }
                    return arrayList;
                }
            }
            ArrayList arrayList2 = new ArrayList(r5.A00.size());
            for (int i3 = 0; i3 < r5.A00.size(); i3++) {
                Object obj2 = r5.A00.get(i3);
                if (obj2 == null) {
                    arrayList2.add(null);
                } else if (obj2 instanceof Number) {
                    arrayList2.add(obj2);
                } else {
                    arrayList2.add(obj2.toString());
                }
            }
            return arrayList2;
        } else if (!(obj instanceof C12240os)) {
            return obj;
        } else {
            TreeMap treeMap = new TreeMap();
            A02(this, (C12240os) obj, treeMap);
            return treeMap;
        }
    }

    private void A01(C16910xz r5, List list) {
        Object obj;
        if (list != null && !list.isEmpty()) {
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (obj != null) {
                    break;
                }
            }
            if (obj == null) {
                return;
            }
            if (obj instanceof List) {
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    A01(r5.A0E(), (List) it2.next());
                }
            } else if (obj instanceof String) {
                Iterator it3 = list.iterator();
                while (it3.hasNext()) {
                    C16910xz.A00(r5, (String) it3.next());
                }
            } else if (obj instanceof Number) {
                Iterator it4 = list.iterator();
                while (it4.hasNext()) {
                    C16910xz.A00(r5, (Number) it4.next());
                }
            } else if (obj instanceof Enum) {
                Iterator it5 = list.iterator();
                while (it5.hasNext()) {
                    C16910xz.A00(r5, ((Enum) it5.next()).toString());
                }
            } else if (obj instanceof GraphQlCallInput) {
                Iterator it6 = list.iterator();
                while (it6.hasNext()) {
                    r5.A0G(((GraphQlCallInput) it6.next()).A03());
                }
            } else if (obj instanceof Map) {
                Iterator it7 = list.iterator();
                while (it7.hasNext()) {
                    A04(r5.A0F(), (Map) it7.next());
                }
            } else {
                throw new IllegalArgumentException("List value type is not supported: " + obj.getClass());
            }
        }
    }

    public static void A02(GraphQlCallInput graphQlCallInput, C12240os r4, Map map) {
        if (r4 != null) {
            for (int i = 0; i < r4.A00; i++) {
                map.put(r4.A0H(i), graphQlCallInput.A00(r4.A0G(i)));
            }
        }
    }

    public C12240os A03() {
        if (this.A00 == null) {
            this.A00 = this.A01.A02();
        }
        return this.A00;
    }

    public void A04(C12240os r5, Map map) {
        String str;
        for (Map.Entry entry : map.entrySet()) {
            String str2 = (String) entry.getKey();
            Object value = entry.getValue();
            if (value != null) {
                if (value instanceof Boolean) {
                    if (((Boolean) value).booleanValue()) {
                        str = "true";
                    } else {
                        str = "false";
                    }
                    r5.A0K(str2, str);
                } else if (value instanceof Number) {
                    r5.A0J(str2, (Number) value);
                } else if (value instanceof String) {
                    r5.A0K(str2, (String) value);
                } else if (value instanceof Enum) {
                    r5.A0K(str2, value.toString());
                } else if (value instanceof GraphQlCallInput) {
                    r5.A0I(str2, ((GraphQlCallInput) value).A03());
                } else if (value instanceof List) {
                    A01(r5.A0E(str2), (List) value);
                } else if (value instanceof Map) {
                    A04(r5.A0F(str2), (Map) value);
                } else {
                    throw new IllegalArgumentException("Unexpected object value type " + value.getClass());
                }
            }
        }
    }

    public void A05(String str, GraphQlCallInput graphQlCallInput) {
        A03().A0I(str, graphQlCallInput.A03());
    }

    public void A06(String str, Boolean bool) {
        C12240os.A01(A03(), str, bool);
    }

    public void A07(String str, Double d) {
        A03().A0J(str, d);
    }

    public void A08(String str, Integer num) {
        A03().A0J(str, num);
    }

    public void A09(String str, String str2) {
        A03().A0K(str, str2);
    }

    public void A0A(String str, List list) {
        A01(A03().A0E(str), list);
    }
}
