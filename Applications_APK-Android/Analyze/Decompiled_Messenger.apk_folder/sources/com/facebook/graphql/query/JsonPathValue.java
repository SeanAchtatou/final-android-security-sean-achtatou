package com.facebook.graphql.query;

import X.AnonymousClass44Y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.json.AutoGenJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@AutoGenJsonDeserializer
@JsonDeserialize(using = JsonPathValueDeserializer.class)
public class JsonPathValue implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass44Y();
    @JsonProperty("value")
    public String mValue;

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return this.mValue;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mValue);
    }
}
