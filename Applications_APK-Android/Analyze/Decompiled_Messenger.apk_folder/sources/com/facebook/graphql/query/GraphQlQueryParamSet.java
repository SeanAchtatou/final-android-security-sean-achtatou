package com.facebook.graphql.query;

import com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000;
import com.facebook.graphql.calls.GraphQlCallInput;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Map;
import java.util.TreeMap;

@JsonDeserialize(using = GraphQlQueryParamSetDeserializer.class)
@JsonSerialize(using = GraphQlQueryParamSetSerializer.class)
public final class GraphQlQueryParamSet {
    public GQLCallInputCInputShape0S0000000 A00 = new GQLCallInputCInputShape0S0000000(132);
    public Map A01 = new TreeMap();

    public Map A00() {
        GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = this.A00;
        TreeMap treeMap = new TreeMap();
        GraphQlCallInput.A02(gQLCallInputCInputShape0S0000000, gQLCallInputCInputShape0S0000000.A00, treeMap);
        return treeMap;
    }

    public void A01(String str, GraphQlCallInput graphQlCallInput) {
        if (graphQlCallInput != null) {
            this.A00.A05(str, graphQlCallInput);
        }
    }
}
