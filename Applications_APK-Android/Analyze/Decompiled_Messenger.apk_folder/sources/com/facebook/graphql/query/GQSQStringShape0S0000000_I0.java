package com.facebook.graphql.query;

import X.C10880l0;
import com.facebook.acra.ACRA;
import org.webrtc.audio.WebRtcAudioRecord;

public class GQSQStringShape0S0000000_I0 extends C10880l0 {
    public final int A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GQSQStringShape0S0000000_I0(int r13) {
        /*
            r12 = this;
            r12.A00 = r13
            switch(r13) {
                case 0: goto L_0x0020;
                case 1: goto L_0x0032;
                case 2: goto L_0x0048;
                case 3: goto L_0x005a;
                case 4: goto L_0x0070;
                case 5: goto L_0x0082;
                case 6: goto L_0x0098;
                default: goto L_0x0005;
            }
        L_0x0005:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r1 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            java.lang.String r8 = "FetchZeroNTUpgradeViewQuery"
            java.lang.String r9 = "viewer"
            r2 = -788425066(0xffffffffd1019696, float:-3.4786075E10)
            r3 = 3506542230(0xd1019696, double:1.7324620515E-314)
            r6 = 1
            r7 = 0
            r10 = 3506542230(0xd1019696, double:1.7324620515E-314)
        L_0x001a:
            r0 = r12
            r5 = 0
            r0.<init>(r1, r2, r3, r5, r6, r7, r8, r9, r10)
            return
        L_0x0020:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r1 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            java.lang.String r8 = "FetchInterstitials"
            java.lang.String r9 = "viewer"
            r2 = 959606570(0x39326f2a, float:1.7016815E-4)
            r3 = 959606570(0x39326f2a, double:4.7410864E-315)
            r6 = 0
            r7 = 4
            r10 = 959606570(0x39326f2a, double:4.7410864E-315)
            goto L_0x001a
        L_0x0032:
            java.lang.Class<X.2t1> r1 = X.C58012t1.class
            java.lang.String r8 = "XMAQuery"
            java.lang.String r9 = "node"
            r2 = 1042585914(0x3e24993a, float:0.16074076)
            r3 = 2226725815(0x84b923b7, double:1.100148728E-314)
            r6 = 0
            r7 = 0
            r10 = 2226725815(0x84b923b7, double:1.100148728E-314)
            goto L_0x001a
        L_0x0048:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r1 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            java.lang.String r8 = "InboxV2Query"
            java.lang.String r9 = "viewer"
            r2 = 1649765866(0x62556dea, float:9.8426916E20)
            r3 = 1649765866(0x62556dea, double:8.15092638E-315)
            r6 = 0
            r7 = 0
            r10 = 1649765866(0x62556dea, double:8.15092638E-315)
            goto L_0x001a
        L_0x005a:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r1 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            java.lang.String r8 = "MessageRequestsSnippet"
            java.lang.String r9 = "viewer"
            r2 = -805817769(0xffffffffcff83257, float:-8.3280973E9)
            r3 = 3489149527(0xcff83257, double:1.7238689145E-314)
            r6 = 0
            r7 = 4
            r10 = 3489149527(0xcff83257, double:1.7238689145E-314)
            goto L_0x001a
        L_0x0070:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r1 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            java.lang.String r8 = "FetchMyMontageThreadFbidQuery"
            java.lang.String r9 = "viewer"
            r2 = 795818810(0x2f6f3b3a, float:2.1757965E-10)
            r3 = 795818810(0x2f6f3b3a, double:3.931867343E-315)
            r6 = 1
            r7 = 0
            r10 = 795818810(0x2f6f3b3a, double:3.931867343E-315)
            goto L_0x001a
        L_0x0082:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r1 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            java.lang.String r8 = "PaymentInvoiceBannerQuery"
            java.lang.String r9 = "payment_invoice_banner_list"
            r2 = -2105168077(0xffffffff8285af33, float:-1.9643153E-37)
            r3 = 2189799219(0x8285af33, double:1.0819045654E-314)
            r6 = 0
            r7 = 0
            r10 = 2189799219(0x8285af33, double:1.0819045654E-314)
            goto L_0x001a
        L_0x0098:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r1 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            java.lang.String r8 = "FetchZeroNTMessengerInterstitialQuery"
            java.lang.String r9 = "viewer"
            r2 = 2049975882(0x7a30264a, float:2.2865521E35)
            r3 = 2049975882(0x7a30264a, double:1.012822658E-314)
            r6 = 1
            r7 = 0
            r10 = 2049975882(0x7a30264a, double:1.012822658E-314)
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.graphql.query.GQSQStringShape0S0000000_I0.<init>(int):void");
    }

    public boolean A0C(String str) {
        switch (this.A00) {
            case 0:
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                int hashCode = str.hashCode();
                return hashCode == -338181066 || hashCode == 109250890 || hashCode == 1735518709;
            case 1:
                return str.hashCode() == -338181066;
            case 2:
            case 3:
            case 4:
            default:
                return super.A0C(str);
            case 5:
                return str.hashCode() == 109250890;
        }
    }
}
