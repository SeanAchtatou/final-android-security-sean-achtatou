package com.facebook.graphql.executor;

import X.AnonymousClass0mH;
import X.AnonymousClass0mI;
import X.AnonymousClass102;
import X.AnonymousClass324;
import X.C04300To;
import X.C12200oo;
import X.C417826y;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.graphservice.interfaces.Summary;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphQLResult extends AnonymousClass0mH implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0mI();
    private Set A00;
    public final List A01;
    private final Class A02;
    private final String A03;
    private final String A04;
    private final Map A05;
    private final Map A06;
    private final boolean A07;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ArrayList A032;
        Object obj = this.A03;
        boolean z = true;
        if (obj instanceof List) {
            parcel.writeInt(1);
            AnonymousClass324.A0C(parcel, (List) obj);
        } else {
            if (obj != null && !(obj instanceof C12200oo)) {
                z = false;
            }
            Preconditions.checkState(z);
            parcel.writeInt(0);
            AnonymousClass324.A0B(parcel, (C12200oo) obj);
        }
        parcel.writeSerializable(this.A01);
        parcel.writeLong(this.A00);
        parcel.writeSerializable(this.A02);
        Set set = this.A00;
        if (set == null) {
            A032 = null;
        } else {
            A032 = C04300To.A03(set);
        }
        parcel.writeList(A032);
        parcel.writeMap(this.A06);
        parcel.writeMap(this.A05);
        C417826y.A0V(parcel, this.A07);
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeList(this.A01);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GraphQLResult(android.os.Parcel r7) {
        /*
            r6 = this;
            int r2 = r7.readInt()
            r0 = 1
            if (r2 != r0) goto L_0x0064
            java.util.List r1 = X.AnonymousClass324.A06(r7)
        L_0x000b:
            java.io.Serializable r2 = r7.readSerializable()
            X.102 r2 = (X.AnonymousClass102) r2
            long r3 = r7.readLong()
            r5 = 0
            r0 = r6
            r0.<init>(r1, r2, r3, r5)
            java.io.Serializable r0 = r7.readSerializable()
            java.lang.Class r0 = (java.lang.Class) r0
            r6.A02 = r0
            r1 = 0
            if (r0 == 0) goto L_0x0029
            java.lang.ClassLoader r5 = r0.getClassLoader()
        L_0x0029:
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            java.util.ArrayList r0 = r7.readArrayList(r0)
            if (r0 == 0) goto L_0x0039
            com.google.common.collect.ImmutableSet r1 = com.google.common.collect.ImmutableSet.A0A(r0)
        L_0x0039:
            r6.A00 = r1
            java.util.HashMap r0 = r7.readHashMap(r5)
            r6.A06 = r0
            java.util.HashMap r0 = r7.readHashMap(r5)
            r6.A05 = r0
            boolean r0 = X.C417826y.A0W(r7)
            r6.A07 = r0
            java.lang.String r0 = r7.readString()
            r6.A04 = r0
            java.lang.String r0 = r7.readString()
            r6.A03 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r6.A01 = r0
            r7.readList(r0, r5)
            return
        L_0x0064:
            if (r2 != 0) goto L_0x006b
            java.lang.Object r1 = X.AnonymousClass324.A03(r7)
            goto L_0x000b
        L_0x006b:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Unknown value "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r2)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.graphql.executor.GraphQLResult.<init>(android.os.Parcel):void");
    }

    public GraphQLResult(Object obj, AnonymousClass102 r15, long j) {
        this(obj, r15, j, null, null, null, true, null, null, null, null);
    }

    public GraphQLResult(Object obj, AnonymousClass102 r8, long j, Set set, Map map, Map map2, boolean z, String str, String str2, List list, Summary summary) {
        super(obj, r8, j, summary);
        Class<?> cls;
        if (obj == null) {
            cls = null;
        } else {
            cls = obj.getClass();
        }
        this.A02 = cls;
        this.A00 = set;
        this.A06 = map;
        this.A05 = map2;
        this.A07 = z;
        this.A04 = str;
        this.A03 = str2;
        this.A01 = list;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GraphQLResult(java.lang.Object r14, com.facebook.graphservice.interfaces.Summary r15, X.AnonymousClass102 r16, long r17) {
        /*
            r13 = this;
            r0 = r13
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 1
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = r15
            r1 = r14
            r3 = r17
            r2 = r16
            r0.<init>(r1, r2, r3, r5, r6, r7, r8, r9, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.graphql.executor.GraphQLResult.<init>(java.lang.Object, com.facebook.graphservice.interfaces.Summary, X.102, long):void");
    }
}
