package com.facebook.graphql.enums;

import X.AnonymousClass1Y3;
import X.AnonymousClass80H;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLThreadConnectivityStatusSubtitleType extends Enum {
    private static final /* synthetic */ GraphQLThreadConnectivityStatusSubtitleType[] A00;
    public static final GraphQLThreadConnectivityStatusSubtitleType A01;
    public static final GraphQLThreadConnectivityStatusSubtitleType A02;
    public static final GraphQLThreadConnectivityStatusSubtitleType A03;
    public static final GraphQLThreadConnectivityStatusSubtitleType A04;
    public static final GraphQLThreadConnectivityStatusSubtitleType A05;
    public static final GraphQLThreadConnectivityStatusSubtitleType A06;
    public static final GraphQLThreadConnectivityStatusSubtitleType A07;

    static {
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType = new GraphQLThreadConnectivityStatusSubtitleType("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A05 = graphQLThreadConnectivityStatusSubtitleType;
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType2 = new GraphQLThreadConnectivityStatusSubtitleType("ADDRESSBOOK_CONTACT", 1);
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType3 = new GraphQLThreadConnectivityStatusSubtitleType("BASED_ON_SETTINGS", 2);
        A01 = graphQLThreadConnectivityStatusSubtitleType3;
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType4 = new GraphQLThreadConnectivityStatusSubtitleType("FOLLOWS_VIEWER_ON_IG", 3);
        A02 = graphQLThreadConnectivityStatusSubtitleType4;
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType5 = new GraphQLThreadConnectivityStatusSubtitleType("HAS_VIEWER_CONTACT_INFO", 4);
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType6 = new GraphQLThreadConnectivityStatusSubtitleType("INSTAGRAM", 5);
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType7 = new GraphQLThreadConnectivityStatusSubtitleType(AnonymousClass80H.$const$string(AnonymousClass1Y3.A0i), 6);
        A03 = graphQLThreadConnectivityStatusSubtitleType7;
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType8 = new GraphQLThreadConnectivityStatusSubtitleType("NONE", 7);
        A04 = graphQLThreadConnectivityStatusSubtitleType8;
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType9 = new GraphQLThreadConnectivityStatusSubtitleType("VIEWER_FOLLOWS_ON_IG", 8);
        A06 = graphQLThreadConnectivityStatusSubtitleType9;
        GraphQLThreadConnectivityStatusSubtitleType graphQLThreadConnectivityStatusSubtitleType10 = new GraphQLThreadConnectivityStatusSubtitleType("VIEWER_HAS_ADDRESSBOOK_CONTACT", 9);
        A07 = graphQLThreadConnectivityStatusSubtitleType10;
        A00 = new GraphQLThreadConnectivityStatusSubtitleType[]{graphQLThreadConnectivityStatusSubtitleType, graphQLThreadConnectivityStatusSubtitleType2, graphQLThreadConnectivityStatusSubtitleType3, graphQLThreadConnectivityStatusSubtitleType4, graphQLThreadConnectivityStatusSubtitleType5, graphQLThreadConnectivityStatusSubtitleType6, graphQLThreadConnectivityStatusSubtitleType7, graphQLThreadConnectivityStatusSubtitleType8, graphQLThreadConnectivityStatusSubtitleType9, graphQLThreadConnectivityStatusSubtitleType10};
    }

    public static GraphQLThreadConnectivityStatusSubtitleType valueOf(String str) {
        return (GraphQLThreadConnectivityStatusSubtitleType) Enum.valueOf(GraphQLThreadConnectivityStatusSubtitleType.class, str);
    }

    public static GraphQLThreadConnectivityStatusSubtitleType[] values() {
        return (GraphQLThreadConnectivityStatusSubtitleType[]) A00.clone();
    }

    private GraphQLThreadConnectivityStatusSubtitleType(String str, int i) {
    }
}
