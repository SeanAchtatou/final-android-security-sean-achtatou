package com.facebook.graphql.enums;

import X.AnonymousClass1Y3;
import X.AnonymousClass80H;
import X.ECX;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLLivingRoomEntrySource extends Enum {
    private static final /* synthetic */ GraphQLLivingRoomEntrySource[] A00;
    public static final GraphQLLivingRoomEntrySource A01;
    public static final GraphQLLivingRoomEntrySource A02;
    public static final GraphQLLivingRoomEntrySource A03;
    public static final GraphQLLivingRoomEntrySource A04;
    public static final GraphQLLivingRoomEntrySource A05;
    public static final GraphQLLivingRoomEntrySource A06;
    public static final GraphQLLivingRoomEntrySource A07;
    public static final GraphQLLivingRoomEntrySource A08;
    public static final GraphQLLivingRoomEntrySource A09;
    public static final GraphQLLivingRoomEntrySource A0A;
    public static final GraphQLLivingRoomEntrySource A0B;

    static {
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource = new GraphQLLivingRoomEntrySource("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A06 = graphQLLivingRoomEntrySource;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource2 = new GraphQLLivingRoomEntrySource("WATCH_TAB", 1);
        A0B = graphQLLivingRoomEntrySource2;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource3 = new GraphQLLivingRoomEntrySource("TAHOE", 2);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource4 = new GraphQLLivingRoomEntrySource("TAHOE_TO_FEED", 3);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource5 = new GraphQLLivingRoomEntrySource(AnonymousClass80H.$const$string(339), 4);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource6 = new GraphQLLivingRoomEntrySource(AnonymousClass80H.$const$string(226), 5);
        A01 = graphQLLivingRoomEntrySource6;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource7 = new GraphQLLivingRoomEntrySource(ECX.$const$string(20), 6);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource8 = new GraphQLLivingRoomEntrySource("EVERGREEN_CONSUMPTION_QP", 7);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource9 = new GraphQLLivingRoomEntrySource("EVERGREEN_PRODUCTION_QP", 8);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource10 = new GraphQLLivingRoomEntrySource("FEED", 9);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource11 = new GraphQLLivingRoomEntrySource("FEED_QP", 10);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource12 = new GraphQLLivingRoomEntrySource(AnonymousClass80H.$const$string(11), 11);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource13 = new GraphQLLivingRoomEntrySource(AnonymousClass80H.$const$string(358), 12);
        A07 = graphQLLivingRoomEntrySource13;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource14 = new GraphQLLivingRoomEntrySource("GROUP_FEED", 13);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource15 = new GraphQLLivingRoomEntrySource("GROUPS_TAB", 14);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource16 = new GraphQLLivingRoomEntrySource("PAGE_FEED_OPTION", 15);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource17 = new GraphQLLivingRoomEntrySource(AnonymousClass80H.$const$string(AnonymousClass1Y3.A0l), 16);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource18 = new GraphQLLivingRoomEntrySource("PERMALINK", 17);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource19 = new GraphQLLivingRoomEntrySource("DEEPLINK", 18);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource20 = new GraphQLLivingRoomEntrySource("COMPOSER", 19);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource21 = new GraphQLLivingRoomEntrySource("STORY_ATTACHMENT", 20);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource22 = new GraphQLLivingRoomEntrySource("PUSH_NOTIFICATIONS", 21);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource23 = new GraphQLLivingRoomEntrySource("CTA_CREATION_DIALOG", 22);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource24 = new GraphQLLivingRoomEntrySource("MESSENGER_AUTO_JOIN", 23);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource25 = new GraphQLLivingRoomEntrySource("MESSENGER_RTC_CALL", 24);
        A03 = graphQLLivingRoomEntrySource25;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource26 = new GraphQLLivingRoomEntrySource("MESSENGER_RTC_CALL_PIP", 25);
        A04 = graphQLLivingRoomEntrySource26;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource27 = new GraphQLLivingRoomEntrySource("MESSENGER_COMPOSER", 26);
        A02 = graphQLLivingRoomEntrySource27;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource28 = new GraphQLLivingRoomEntrySource("MESSENGER_INBOX", 27);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource29 = new GraphQLLivingRoomEntrySource("MESSENGER_THREAD_BANNER", 28);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource30 = new GraphQLLivingRoomEntrySource("MESSENGER_VIDEO_ATTACHMENT", 29);
        A05 = graphQLLivingRoomEntrySource30;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource31 = new GraphQLLivingRoomEntrySource("MESSENGER_XMA", 30);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource32 = new GraphQLLivingRoomEntrySource("MESSENGER_SOLO_PLAYER", 31);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource33 = new GraphQLLivingRoomEntrySource("MESSENGER_SOLO_PLAYER_FULL_SCREEN", 32);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource34 = new GraphQLLivingRoomEntrySource("LIVE_VIDEO_SHARE_SHEET", 33);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource35 = new GraphQLLivingRoomEntrySource("END_SCREEN_CHAINING", 34);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource36 = new GraphQLLivingRoomEntrySource("SCHEDULED_STORY_ATTACHMENT", 35);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource37 = new GraphQLLivingRoomEntrySource(AnonymousClass80H.$const$string(116), 36);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource38 = new GraphQLLivingRoomEntrySource("SEARCH_VIDEO_TAB", 37);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource39 = new GraphQLLivingRoomEntrySource("SEARCH_VIDEO_HOME", 38);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource40 = new GraphQLLivingRoomEntrySource("FEED_JOIN_CTA", 39);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource41 = new GraphQLLivingRoomEntrySource("REPLAY_CHAINING_BANNER", 40);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource42 = new GraphQLLivingRoomEntrySource("REPLAY_CHIANING_LIST", 41);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource43 = new GraphQLLivingRoomEntrySource("GOODWILL_THROWBACK_CTA", 42);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource44 = new GraphQLLivingRoomEntrySource("GROUP_FEED_QP", 43);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource45 = new GraphQLLivingRoomEntrySource("WATCH_FEED_INJECTION", 44);
        A0A = graphQLLivingRoomEntrySource45;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource46 = new GraphQLLivingRoomEntrySource("WATCHING_TOGETHER_UNIT", 45);
        A08 = graphQLLivingRoomEntrySource46;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource47 = new GraphQLLivingRoomEntrySource("WATCHING_TOGETHER_UNIT_GROUPS_TAB", 46);
        A09 = graphQLLivingRoomEntrySource47;
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource48 = new GraphQLLivingRoomEntrySource("WATCH_PARTY_RST_CUE", 47);
        GraphQLLivingRoomEntrySource graphQLLivingRoomEntrySource49 = new GraphQLLivingRoomEntrySource("UFI_VOICE_ACTOR_CHANGE", 48);
        GraphQLLivingRoomEntrySource[] graphQLLivingRoomEntrySourceArr = new GraphQLLivingRoomEntrySource[49];
        System.arraycopy(new GraphQLLivingRoomEntrySource[]{graphQLLivingRoomEntrySource, graphQLLivingRoomEntrySource2, graphQLLivingRoomEntrySource3, graphQLLivingRoomEntrySource4, graphQLLivingRoomEntrySource5, graphQLLivingRoomEntrySource6, graphQLLivingRoomEntrySource7, graphQLLivingRoomEntrySource8, graphQLLivingRoomEntrySource9, graphQLLivingRoomEntrySource10, graphQLLivingRoomEntrySource11, graphQLLivingRoomEntrySource12, graphQLLivingRoomEntrySource13, graphQLLivingRoomEntrySource14, graphQLLivingRoomEntrySource15, graphQLLivingRoomEntrySource16, graphQLLivingRoomEntrySource17, graphQLLivingRoomEntrySource18, graphQLLivingRoomEntrySource19, graphQLLivingRoomEntrySource20, graphQLLivingRoomEntrySource21, graphQLLivingRoomEntrySource22, graphQLLivingRoomEntrySource23, graphQLLivingRoomEntrySource24, graphQLLivingRoomEntrySource25, graphQLLivingRoomEntrySource26, graphQLLivingRoomEntrySource27}, 0, graphQLLivingRoomEntrySourceArr, 0, 27);
        System.arraycopy(new GraphQLLivingRoomEntrySource[]{graphQLLivingRoomEntrySource28, graphQLLivingRoomEntrySource29, graphQLLivingRoomEntrySource30, graphQLLivingRoomEntrySource31, graphQLLivingRoomEntrySource32, graphQLLivingRoomEntrySource33, graphQLLivingRoomEntrySource34, graphQLLivingRoomEntrySource35, graphQLLivingRoomEntrySource36, graphQLLivingRoomEntrySource37, graphQLLivingRoomEntrySource38, graphQLLivingRoomEntrySource39, graphQLLivingRoomEntrySource40, graphQLLivingRoomEntrySource41, graphQLLivingRoomEntrySource42, graphQLLivingRoomEntrySource43, graphQLLivingRoomEntrySource44, graphQLLivingRoomEntrySource45, graphQLLivingRoomEntrySource46, graphQLLivingRoomEntrySource47, graphQLLivingRoomEntrySource48, graphQLLivingRoomEntrySource49}, 0, graphQLLivingRoomEntrySourceArr, 27, 22);
        A00 = graphQLLivingRoomEntrySourceArr;
    }

    public static GraphQLLivingRoomEntrySource valueOf(String str) {
        return (GraphQLLivingRoomEntrySource) Enum.valueOf(GraphQLLivingRoomEntrySource.class, str);
    }

    public static GraphQLLivingRoomEntrySource[] values() {
        return (GraphQLLivingRoomEntrySource[]) A00.clone();
    }

    private GraphQLLivingRoomEntrySource(String str, int i) {
    }
}
