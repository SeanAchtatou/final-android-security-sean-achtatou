package com.facebook.graphql.enums;

public enum GraphQLMessengerThreadViewMode {
    UNSET_OR_UNRECOGNIZED_ENUM_VALUE,
    DEFAULT,
    DARK_MODE,
    LIGHT_MODE
}
