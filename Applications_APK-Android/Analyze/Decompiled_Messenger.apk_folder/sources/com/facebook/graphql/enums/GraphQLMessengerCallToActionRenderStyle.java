package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLMessengerCallToActionRenderStyle extends Enum {
    private static final /* synthetic */ GraphQLMessengerCallToActionRenderStyle[] A00;
    public static final GraphQLMessengerCallToActionRenderStyle A01;
    public static final GraphQLMessengerCallToActionRenderStyle A02;

    static {
        GraphQLMessengerCallToActionRenderStyle graphQLMessengerCallToActionRenderStyle = new GraphQLMessengerCallToActionRenderStyle("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A02 = graphQLMessengerCallToActionRenderStyle;
        GraphQLMessengerCallToActionRenderStyle graphQLMessengerCallToActionRenderStyle2 = new GraphQLMessengerCallToActionRenderStyle("NORMAL", 1);
        A01 = graphQLMessengerCallToActionRenderStyle2;
        A00 = new GraphQLMessengerCallToActionRenderStyle[]{graphQLMessengerCallToActionRenderStyle, graphQLMessengerCallToActionRenderStyle2, new GraphQLMessengerCallToActionRenderStyle("LARGE_BUTTON", 2)};
    }

    public static GraphQLMessengerCallToActionRenderStyle valueOf(String str) {
        return (GraphQLMessengerCallToActionRenderStyle) Enum.valueOf(GraphQLMessengerCallToActionRenderStyle.class, str);
    }

    public static GraphQLMessengerCallToActionRenderStyle[] values() {
        return (GraphQLMessengerCallToActionRenderStyle[]) A00.clone();
    }

    private GraphQLMessengerCallToActionRenderStyle(String str, int i) {
    }
}
