package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLMessengerPlatformWebviewStyleType extends Enum {
    private static final /* synthetic */ GraphQLMessengerPlatformWebviewStyleType[] A00;
    public static final GraphQLMessengerPlatformWebviewStyleType A01;

    static {
        GraphQLMessengerPlatformWebviewStyleType graphQLMessengerPlatformWebviewStyleType = new GraphQLMessengerPlatformWebviewStyleType("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A01 = graphQLMessengerPlatformWebviewStyleType;
        A00 = new GraphQLMessengerPlatformWebviewStyleType[]{graphQLMessengerPlatformWebviewStyleType, new GraphQLMessengerPlatformWebviewStyleType("PLATFORM", 1), new GraphQLMessengerPlatformWebviewStyleType("DEFAULT", 2)};
    }

    public static GraphQLMessengerPlatformWebviewStyleType valueOf(String str) {
        return (GraphQLMessengerPlatformWebviewStyleType) Enum.valueOf(GraphQLMessengerPlatformWebviewStyleType.class, str);
    }

    public static GraphQLMessengerPlatformWebviewStyleType[] values() {
        return (GraphQLMessengerPlatformWebviewStyleType[]) A00.clone();
    }

    private GraphQLMessengerPlatformWebviewStyleType(String str, int i) {
    }
}
