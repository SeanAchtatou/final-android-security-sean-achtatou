package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLMessengerAdProductType extends Enum {
    private static final /* synthetic */ GraphQLMessengerAdProductType[] A00;
    public static final GraphQLMessengerAdProductType A01;
    public static final GraphQLMessengerAdProductType A02;
    public static final GraphQLMessengerAdProductType A03;

    static {
        GraphQLMessengerAdProductType graphQLMessengerAdProductType = new GraphQLMessengerAdProductType("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A03 = graphQLMessengerAdProductType;
        GraphQLMessengerAdProductType graphQLMessengerAdProductType2 = new GraphQLMessengerAdProductType("MESSENGER_DESTINATION", 1);
        A01 = graphQLMessengerAdProductType2;
        GraphQLMessengerAdProductType graphQLMessengerAdProductType3 = new GraphQLMessengerAdProductType("CAROUSEL", 2);
        GraphQLMessengerAdProductType graphQLMessengerAdProductType4 = new GraphQLMessengerAdProductType("SINGLE_MEDIA", 3);
        GraphQLMessengerAdProductType graphQLMessengerAdProductType5 = new GraphQLMessengerAdProductType("NEKO", 4);
        GraphQLMessengerAdProductType graphQLMessengerAdProductType6 = new GraphQLMessengerAdProductType("NO_CTA", 5);
        A02 = graphQLMessengerAdProductType6;
        A00 = new GraphQLMessengerAdProductType[]{graphQLMessengerAdProductType, graphQLMessengerAdProductType2, graphQLMessengerAdProductType3, graphQLMessengerAdProductType4, graphQLMessengerAdProductType5, graphQLMessengerAdProductType6, new GraphQLMessengerAdProductType("PHOTO", 6), new GraphQLMessengerAdProductType("MULTI_PHOTO", 7)};
    }

    public static GraphQLMessengerAdProductType valueOf(String str) {
        return (GraphQLMessengerAdProductType) Enum.valueOf(GraphQLMessengerAdProductType.class, str);
    }

    public static GraphQLMessengerAdProductType[] values() {
        return (GraphQLMessengerAdProductType[]) A00.clone();
    }

    private GraphQLMessengerAdProductType(String str, int i) {
    }
}
