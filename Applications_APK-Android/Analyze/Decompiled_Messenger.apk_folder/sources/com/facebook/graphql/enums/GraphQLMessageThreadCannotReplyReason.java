package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLMessageThreadCannotReplyReason extends Enum {
    private static final /* synthetic */ GraphQLMessageThreadCannotReplyReason[] A00;
    public static final GraphQLMessageThreadCannotReplyReason A01;
    public static final GraphQLMessageThreadCannotReplyReason A02;
    public static final GraphQLMessageThreadCannotReplyReason A03;
    public static final GraphQLMessageThreadCannotReplyReason A04;
    public static final GraphQLMessageThreadCannotReplyReason A05;
    public static final GraphQLMessageThreadCannotReplyReason A06;
    public static final GraphQLMessageThreadCannotReplyReason A07;

    static {
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason = new GraphQLMessageThreadCannotReplyReason("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A07 = graphQLMessageThreadCannotReplyReason;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason2 = new GraphQLMessageThreadCannotReplyReason("BLOCKED", 1);
        A01 = graphQLMessageThreadCannotReplyReason2;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason3 = new GraphQLMessageThreadCannotReplyReason("MESSENGER_BLOCKEE", 2);
        A02 = graphQLMessageThreadCannotReplyReason3;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason4 = new GraphQLMessageThreadCannotReplyReason("COMPOSER_DISABLED_BOT", 3);
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason5 = new GraphQLMessageThreadCannotReplyReason("HAS_EMAIL_PARTICIPANT", 4);
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason6 = new GraphQLMessageThreadCannotReplyReason("OBJECT_ORIGINATED", 5);
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason7 = new GraphQLMessageThreadCannotReplyReason("READ_ONLY", 6);
        A04 = graphQLMessageThreadCannotReplyReason7;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason8 = new GraphQLMessageThreadCannotReplyReason("VIEWER_NOT_SUBSCRIBED", 7);
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason9 = new GraphQLMessageThreadCannotReplyReason("RECIPIENTS_NOT_LOADABLE", 8);
        A05 = graphQLMessageThreadCannotReplyReason9;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason10 = new GraphQLMessageThreadCannotReplyReason("RECIPIENTS_UNAVAILABLE", 9);
        A06 = graphQLMessageThreadCannotReplyReason10;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason11 = new GraphQLMessageThreadCannotReplyReason("RECIPIENTS_INVALID", 10);
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason12 = new GraphQLMessageThreadCannotReplyReason("RECIPIENTS_INACTIVE_WORK_ACC", 11);
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason13 = new GraphQLMessageThreadCannotReplyReason("MONTAGE_NOT_AUTHOR", 12);
        A03 = graphQLMessageThreadCannotReplyReason13;
        A00 = new GraphQLMessageThreadCannotReplyReason[]{graphQLMessageThreadCannotReplyReason, graphQLMessageThreadCannotReplyReason2, graphQLMessageThreadCannotReplyReason3, graphQLMessageThreadCannotReplyReason4, graphQLMessageThreadCannotReplyReason5, graphQLMessageThreadCannotReplyReason6, graphQLMessageThreadCannotReplyReason7, graphQLMessageThreadCannotReplyReason8, graphQLMessageThreadCannotReplyReason9, graphQLMessageThreadCannotReplyReason10, graphQLMessageThreadCannotReplyReason11, graphQLMessageThreadCannotReplyReason12, graphQLMessageThreadCannotReplyReason13, new GraphQLMessageThreadCannotReplyReason("VIEWER_MUTED_IN_FBGROUP", 13)};
    }

    public static GraphQLMessageThreadCannotReplyReason valueOf(String str) {
        return (GraphQLMessageThreadCannotReplyReason) Enum.valueOf(GraphQLMessageThreadCannotReplyReason.class, str);
    }

    public static GraphQLMessageThreadCannotReplyReason[] values() {
        return (GraphQLMessageThreadCannotReplyReason[]) A00.clone();
    }

    private GraphQLMessageThreadCannotReplyReason(String str, int i) {
    }
}
