package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLMessengerGroupThreadSubType extends Enum {
    private static final /* synthetic */ GraphQLMessengerGroupThreadSubType[] A00;
    public static final GraphQLMessengerGroupThreadSubType A01;
    public static final GraphQLMessengerGroupThreadSubType A02;
    public static final GraphQLMessengerGroupThreadSubType A03;
    public static final GraphQLMessengerGroupThreadSubType A04;
    public static final GraphQLMessengerGroupThreadSubType A05;
    public static final GraphQLMessengerGroupThreadSubType A06;

    static {
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType = new GraphQLMessengerGroupThreadSubType("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A05 = graphQLMessengerGroupThreadSubType;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType2 = new GraphQLMessengerGroupThreadSubType("ADMIN_MODEL_V2_THREAD", 1);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType3 = new GraphQLMessengerGroupThreadSubType("FB_GROUP_CHAT", 2);
        A02 = graphQLMessengerGroupThreadSubType3;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType4 = new GraphQLMessengerGroupThreadSubType("MARKETPLACE_THREAD", 3);
        A04 = graphQLMessengerGroupThreadSubType4;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType5 = new GraphQLMessengerGroupThreadSubType("SCHOOL_CHAT", 4);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType6 = new GraphQLMessengerGroupThreadSubType("DEPRECATED__WORK_SYNCED_CHAT", 5);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType7 = new GraphQLMessengerGroupThreadSubType("ADMIN_NOT_SUPPORTED_THREAD", 6);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType8 = new GraphQLMessengerGroupThreadSubType("BELL_SYNCED_CHAT", 7);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType9 = new GraphQLMessengerGroupThreadSubType("GAMES_APP_THREAD", 8);
        A03 = graphQLMessengerGroupThreadSubType9;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType10 = new GraphQLMessengerGroupThreadSubType("VAULT_CHAT", 9);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType11 = new GraphQLMessengerGroupThreadSubType("VERSE_CHAT", 10);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType12 = new GraphQLMessengerGroupThreadSubType("GENERIC_COMMERCE_THREAD", 11);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType13 = new GraphQLMessengerGroupThreadSubType("USER_JOB_THREAD", 12);
        A06 = graphQLMessengerGroupThreadSubType13;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType14 = new GraphQLMessengerGroupThreadSubType("COWORKER_GROUP_THREAD", 13);
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType15 = new GraphQLMessengerGroupThreadSubType("APPROVAL_ENFORCED_CHATROOM_THREAD", 14);
        A01 = graphQLMessengerGroupThreadSubType15;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType16 = graphQLMessengerGroupThreadSubType13;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType17 = graphQLMessengerGroupThreadSubType12;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType18 = graphQLMessengerGroupThreadSubType11;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType19 = graphQLMessengerGroupThreadSubType10;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType20 = graphQLMessengerGroupThreadSubType9;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType21 = graphQLMessengerGroupThreadSubType8;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType22 = graphQLMessengerGroupThreadSubType7;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType23 = graphQLMessengerGroupThreadSubType6;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType24 = graphQLMessengerGroupThreadSubType5;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType25 = graphQLMessengerGroupThreadSubType4;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType26 = graphQLMessengerGroupThreadSubType3;
        GraphQLMessengerGroupThreadSubType graphQLMessengerGroupThreadSubType27 = graphQLMessengerGroupThreadSubType;
        A00 = new GraphQLMessengerGroupThreadSubType[]{graphQLMessengerGroupThreadSubType27, graphQLMessengerGroupThreadSubType2, graphQLMessengerGroupThreadSubType26, graphQLMessengerGroupThreadSubType25, graphQLMessengerGroupThreadSubType24, graphQLMessengerGroupThreadSubType23, graphQLMessengerGroupThreadSubType22, graphQLMessengerGroupThreadSubType21, graphQLMessengerGroupThreadSubType20, graphQLMessengerGroupThreadSubType19, graphQLMessengerGroupThreadSubType18, graphQLMessengerGroupThreadSubType17, graphQLMessengerGroupThreadSubType16, graphQLMessengerGroupThreadSubType14, graphQLMessengerGroupThreadSubType15};
    }

    public static GraphQLMessengerGroupThreadSubType valueOf(String str) {
        return (GraphQLMessengerGroupThreadSubType) Enum.valueOf(GraphQLMessengerGroupThreadSubType.class, str);
    }

    public static GraphQLMessengerGroupThreadSubType[] values() {
        return (GraphQLMessengerGroupThreadSubType[]) A00.clone();
    }

    private GraphQLMessengerGroupThreadSubType(String str, int i) {
    }
}
