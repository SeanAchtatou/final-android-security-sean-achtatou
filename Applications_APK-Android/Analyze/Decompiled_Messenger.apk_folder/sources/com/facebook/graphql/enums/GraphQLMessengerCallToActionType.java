package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLMessengerCallToActionType extends Enum {
    private static final /* synthetic */ GraphQLMessengerCallToActionType[] A00;
    public static final GraphQLMessengerCallToActionType A01;
    public static final GraphQLMessengerCallToActionType A02;
    public static final GraphQLMessengerCallToActionType A03;

    static {
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType = new GraphQLMessengerCallToActionType("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A03 = graphQLMessengerCallToActionType;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType2 = new GraphQLMessengerCallToActionType("OPEN_NATIVE", 1);
        A02 = graphQLMessengerCallToActionType2;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType3 = new GraphQLMessengerCallToActionType("OPEN_URL", 2);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType4 = new GraphQLMessengerCallToActionType("POSTBACK", 3);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType5 = new GraphQLMessengerCallToActionType("ACCOUNT_LINK", 4);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType6 = new GraphQLMessengerCallToActionType("SHARE", 5);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType7 = new GraphQLMessengerCallToActionType("PAYMENT", 6);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType8 = new GraphQLMessengerCallToActionType("FACEBOOK_REPORT_A_PROBLEM", 7);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType9 = new GraphQLMessengerCallToActionType("NAVIGATION", 8);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType10 = new GraphQLMessengerCallToActionType("EXTENSIBLE_SHARE", 9);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType11 = new GraphQLMessengerCallToActionType("OPEN_PAGE_ABOUT", 10);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType12 = new GraphQLMessengerCallToActionType("OPEN_BRANDED_CAMERA", 11);
        A01 = graphQLMessengerCallToActionType12;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType13 = new GraphQLMessengerCallToActionType("OPEN_THREAD", 12);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType14 = new GraphQLMessengerCallToActionType("OPEN_MARKETPLACE_PROFILE_REPORT", 13);
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType15 = graphQLMessengerCallToActionType7;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType16 = graphQLMessengerCallToActionType8;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType17 = graphQLMessengerCallToActionType9;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType18 = graphQLMessengerCallToActionType10;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType19 = graphQLMessengerCallToActionType11;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType20 = graphQLMessengerCallToActionType12;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType21 = graphQLMessengerCallToActionType13;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType22 = graphQLMessengerCallToActionType14;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType23 = graphQLMessengerCallToActionType;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType24 = graphQLMessengerCallToActionType2;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType25 = graphQLMessengerCallToActionType3;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType26 = graphQLMessengerCallToActionType4;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType27 = graphQLMessengerCallToActionType5;
        GraphQLMessengerCallToActionType graphQLMessengerCallToActionType28 = graphQLMessengerCallToActionType6;
        A00 = new GraphQLMessengerCallToActionType[]{graphQLMessengerCallToActionType23, graphQLMessengerCallToActionType24, graphQLMessengerCallToActionType25, graphQLMessengerCallToActionType26, graphQLMessengerCallToActionType27, graphQLMessengerCallToActionType28, graphQLMessengerCallToActionType15, graphQLMessengerCallToActionType16, graphQLMessengerCallToActionType17, graphQLMessengerCallToActionType18, graphQLMessengerCallToActionType19, graphQLMessengerCallToActionType20, graphQLMessengerCallToActionType21, graphQLMessengerCallToActionType22, new GraphQLMessengerCallToActionType("OPEN_DIRECT_SEND_VIEW", 14), new GraphQLMessengerCallToActionType("BOOKING", 15), new GraphQLMessengerCallToActionType("BOOKING_ADD_TO_CALENDAR", 16), new GraphQLMessengerCallToActionType("ACCOUNT_UNLINK", 17), new GraphQLMessengerCallToActionType("SUBSCRIPTION_PRESELECT", 18), new GraphQLMessengerCallToActionType("OPEN_REACT_NATIVE_MINI_APP", 19), new GraphQLMessengerCallToActionType("OPEN_CANCEL_RIDE_MUTATION", 20), new GraphQLMessengerCallToActionType("MANAGE_MESSAGES", 21)};
    }

    public static GraphQLMessengerCallToActionType valueOf(String str) {
        return (GraphQLMessengerCallToActionType) Enum.valueOf(GraphQLMessengerCallToActionType.class, str);
    }

    public static GraphQLMessengerCallToActionType[] values() {
        return (GraphQLMessengerCallToActionType[]) A00.clone();
    }

    private GraphQLMessengerCallToActionType(String str, int i) {
    }
}
