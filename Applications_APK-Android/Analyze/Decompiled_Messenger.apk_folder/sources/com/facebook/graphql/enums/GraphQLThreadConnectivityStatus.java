package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLThreadConnectivityStatus extends Enum {
    private static final /* synthetic */ GraphQLThreadConnectivityStatus[] A00;
    public static final GraphQLThreadConnectivityStatus A01;
    public static final GraphQLThreadConnectivityStatus A02;

    static {
        GraphQLThreadConnectivityStatus graphQLThreadConnectivityStatus = new GraphQLThreadConnectivityStatus("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A02 = graphQLThreadConnectivityStatus;
        GraphQLThreadConnectivityStatus graphQLThreadConnectivityStatus2 = new GraphQLThreadConnectivityStatus("UNCONNECTED", 1);
        A01 = graphQLThreadConnectivityStatus2;
        A00 = new GraphQLThreadConnectivityStatus[]{graphQLThreadConnectivityStatus, graphQLThreadConnectivityStatus2, new GraphQLThreadConnectivityStatus("IMPLICIT_OR_TWO_WAY_MESSAGING", 2)};
    }

    public static GraphQLThreadConnectivityStatus valueOf(String str) {
        return (GraphQLThreadConnectivityStatus) Enum.valueOf(GraphQLThreadConnectivityStatus.class, str);
    }

    public static GraphQLThreadConnectivityStatus[] values() {
        return (GraphQLThreadConnectivityStatus[]) A00.clone();
    }

    private GraphQLThreadConnectivityStatus(String str, int i) {
    }
}
