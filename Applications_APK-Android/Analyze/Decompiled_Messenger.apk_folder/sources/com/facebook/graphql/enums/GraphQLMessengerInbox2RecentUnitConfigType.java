package com.facebook.graphql.enums;

public enum GraphQLMessengerInbox2RecentUnitConfigType {
    UNSET_OR_UNRECOGNIZED_ENUM_VALUE,
    FIXED_NUMBER,
    STALENESS_BASED,
    UNREAD_BASED,
    READ_UNREAD_STALENESS_BASED
}
