package com.facebook.graphql.enums;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class GraphQLMessengerInboxUnitType extends Enum {
    private static final /* synthetic */ GraphQLMessengerInboxUnitType[] A00;
    public static final GraphQLMessengerInboxUnitType A01;
    public static final GraphQLMessengerInboxUnitType A02;
    public static final GraphQLMessengerInboxUnitType A03;
    public static final GraphQLMessengerInboxUnitType A04;
    public static final GraphQLMessengerInboxUnitType A05;
    public static final GraphQLMessengerInboxUnitType A06;
    public static final GraphQLMessengerInboxUnitType A07;
    public static final GraphQLMessengerInboxUnitType A08;
    public static final GraphQLMessengerInboxUnitType A09;
    public static final GraphQLMessengerInboxUnitType A0A;
    public static final GraphQLMessengerInboxUnitType A0B;
    public static final GraphQLMessengerInboxUnitType A0C;
    public static final GraphQLMessengerInboxUnitType A0D;
    public static final GraphQLMessengerInboxUnitType A0E;
    public static final GraphQLMessengerInboxUnitType A0F;
    public static final GraphQLMessengerInboxUnitType A0G;
    public static final GraphQLMessengerInboxUnitType A0H;
    public static final GraphQLMessengerInboxUnitType A0I;
    public static final GraphQLMessengerInboxUnitType A0J;
    public static final GraphQLMessengerInboxUnitType A0K;
    public static final GraphQLMessengerInboxUnitType A0L;
    public static final GraphQLMessengerInboxUnitType A0M;
    public static final GraphQLMessengerInboxUnitType A0N;
    public static final GraphQLMessengerInboxUnitType A0O;
    public static final GraphQLMessengerInboxUnitType A0P;
    public static final GraphQLMessengerInboxUnitType A0Q;
    public static final GraphQLMessengerInboxUnitType A0R;
    public static final GraphQLMessengerInboxUnitType A0S;
    public static final GraphQLMessengerInboxUnitType A0T;
    public static final GraphQLMessengerInboxUnitType A0U;
    public static final GraphQLMessengerInboxUnitType A0V;

    static {
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType = new GraphQLMessengerInboxUnitType("UNSET_OR_UNRECOGNIZED_ENUM_VALUE", 0);
        A0S = graphQLMessengerInboxUnitType;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType2 = new GraphQLMessengerInboxUnitType("ACTIVE_NOW", 1);
        A01 = graphQLMessengerInboxUnitType2;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType3 = new GraphQLMessengerInboxUnitType("ALL_CONTACTS", 2);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType4 = new GraphQLMessengerInboxUnitType("ALL_REMAINING_THREADS", 3);
        A02 = graphQLMessengerInboxUnitType4;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType5 = new GraphQLMessengerInboxUnitType("ALOHA", 4);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType6 = new GraphQLMessengerInboxUnitType("ANNOUNCEMENT", 5);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType7 = new GraphQLMessengerInboxUnitType("BLENDED_HSCROLL", 6);
        A03 = graphQLMessengerInboxUnitType7;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType8 = new GraphQLMessengerInboxUnitType("BLENDED_FAVORITE", 7);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType9 = new GraphQLMessengerInboxUnitType("BMR", 8);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType10 = new GraphQLMessengerInboxUnitType("BYMM", 9);
        A04 = graphQLMessengerInboxUnitType10;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType11 = new GraphQLMessengerInboxUnitType("CAMERA_ROLL", 10);
        A05 = graphQLMessengerInboxUnitType11;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType12 = new GraphQLMessengerInboxUnitType("CHAT_EXTENSION_SUGGESTION", 11);
        A06 = graphQLMessengerInboxUnitType12;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType13 = new GraphQLMessengerInboxUnitType("CONVERSATION_REQUESTS", 12);
        A07 = graphQLMessengerInboxUnitType13;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType14 = new GraphQLMessengerInboxUnitType("CONVERSATION_STARTERS", 13);
        A08 = graphQLMessengerInboxUnitType14;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType15 = new GraphQLMessengerInboxUnitType("CYMK", 14);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType16 = new GraphQLMessengerInboxUnitType("DIRECT_M", 15);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType17 = new GraphQLMessengerInboxUnitType("DISCOVERY_DIRECTORY_IMAGE_BANNER", 16);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType18 = new GraphQLMessengerInboxUnitType("DISCOVERY_DIRECTORY_CATEGORY", 17);
        A09 = graphQLMessengerInboxUnitType18;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType19 = new GraphQLMessengerInboxUnitType("DISCOVERY_LOCATION_UPSELL", 18);
        A0A = graphQLMessengerInboxUnitType19;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType20 = new GraphQLMessengerInboxUnitType("DISCOVER_TAB_UNIT", 19);
        A0B = graphQLMessengerInboxUnitType20;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType21 = new GraphQLMessengerInboxUnitType("EXTERNAL_URL", 20);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType22 = new GraphQLMessengerInboxUnitType("FEATURED_STICKER_PACKS", 21);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType23 = new GraphQLMessengerInboxUnitType("GAMES", 22);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType24 = new GraphQLMessengerInboxUnitType("GIFS", 23);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType25 = new GraphQLMessengerInboxUnitType("MESSAGE_REQUEST_THREADS", 24);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType26 = new GraphQLMessengerInboxUnitType("MESSAGE_THREADS", 25);
        A0E = graphQLMessengerInboxUnitType26;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType27 = new GraphQLMessengerInboxUnitType("MESSENGER_ADS", 26);
        A0F = graphQLMessengerInboxUnitType27;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType28 = new GraphQLMessengerInboxUnitType("MONTAGE_AND_ACTIVE_NOW", 27);
        A0G = graphQLMessengerInboxUnitType28;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType29 = new GraphQLMessengerInboxUnitType("MONTAGE_COMPOSER", 28);
        A0H = graphQLMessengerInboxUnitType29;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType30 = new GraphQLMessengerInboxUnitType("MOST_RECENT_THREADS", 29);
        A0I = graphQLMessengerInboxUnitType30;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType31 = new GraphQLMessengerInboxUnitType("MULTIACCOUNT", 30);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType32 = new GraphQLMessengerInboxUnitType("INVITE", 31);
        A0D = graphQLMessengerInboxUnitType32;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType33 = new GraphQLMessengerInboxUnitType("PEOPLE_TAB_HONEYCOMB_ACTIVE_NOW", 32);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType34 = new GraphQLMessengerInboxUnitType("PHOTO_REMINDERS", 33);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType35 = new GraphQLMessengerInboxUnitType("PYMM", 34);
        A0K = graphQLMessengerInboxUnitType35;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType36 = new GraphQLMessengerInboxUnitType("QP", 35);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType37 = new GraphQLMessengerInboxUnitType("RECENT_GROUP_THREADS", 36);
        A0L = graphQLMessengerInboxUnitType37;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType38 = new GraphQLMessengerInboxUnitType("RECENT_SMS_THREADS", 37);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType39 = new GraphQLMessengerInboxUnitType("ROOM_SUGGESTIONS", 38);
        A0M = graphQLMessengerInboxUnitType39;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType40 = new GraphQLMessengerInboxUnitType("RTC_RECOMMENDATION", 39);
        A0N = graphQLMessengerInboxUnitType40;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType41 = new GraphQLMessengerInboxUnitType("SUBSCRIPTION_CONTENTS", 40);
        A0O = graphQLMessengerInboxUnitType41;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType42 = new GraphQLMessengerInboxUnitType("SUBSCRIPTION_NUX", 41);
        A0P = graphQLMessengerInboxUnitType42;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType43 = new GraphQLMessengerInboxUnitType("SUGGESTED_CHATS", 42);
        A0Q = graphQLMessengerInboxUnitType43;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType44 = new GraphQLMessengerInboxUnitType("SUGGESTED_FB_GROUPS_FOR_CHATS", 43);
        A0R = graphQLMessengerInboxUnitType44;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType45 = new GraphQLMessengerInboxUnitType("VIDEOS", 44);
        A0T = graphQLMessengerInboxUnitType45;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType46 = new GraphQLMessengerInboxUnitType("WORKCHAT_GROUP_THREADS", 45);
        A0U = graphQLMessengerInboxUnitType46;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType47 = new GraphQLMessengerInboxUnitType("WORKCHAT_INVITE", 46);
        A0V = graphQLMessengerInboxUnitType47;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType48 = new GraphQLMessengerInboxUnitType("NEEDY_USERS", 47);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType49 = new GraphQLMessengerInboxUnitType("COMBINED_DIRECT_M", 48);
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType50 = new GraphQLMessengerInboxUnitType("INSTANT_GAMES_FOOTER", 49);
        A0C = graphQLMessengerInboxUnitType50;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType51 = new GraphQLMessengerInboxUnitType("PLAY_OFFLINE_UNIT", 50);
        A0J = graphQLMessengerInboxUnitType51;
        GraphQLMessengerInboxUnitType graphQLMessengerInboxUnitType52 = new GraphQLMessengerInboxUnitType("INSTANT_GAMES_BADGING", 51);
        GraphQLMessengerInboxUnitType[] graphQLMessengerInboxUnitTypeArr = new GraphQLMessengerInboxUnitType[52];
        System.arraycopy(new GraphQLMessengerInboxUnitType[]{graphQLMessengerInboxUnitType, graphQLMessengerInboxUnitType2, graphQLMessengerInboxUnitType3, graphQLMessengerInboxUnitType4, graphQLMessengerInboxUnitType5, graphQLMessengerInboxUnitType6, graphQLMessengerInboxUnitType7, graphQLMessengerInboxUnitType8, graphQLMessengerInboxUnitType9, graphQLMessengerInboxUnitType10, graphQLMessengerInboxUnitType11, graphQLMessengerInboxUnitType12, graphQLMessengerInboxUnitType13, graphQLMessengerInboxUnitType14, graphQLMessengerInboxUnitType15, graphQLMessengerInboxUnitType16, graphQLMessengerInboxUnitType17, graphQLMessengerInboxUnitType18, graphQLMessengerInboxUnitType19, graphQLMessengerInboxUnitType20, graphQLMessengerInboxUnitType21, graphQLMessengerInboxUnitType22, graphQLMessengerInboxUnitType23, graphQLMessengerInboxUnitType24, graphQLMessengerInboxUnitType25, graphQLMessengerInboxUnitType26, graphQLMessengerInboxUnitType27}, 0, graphQLMessengerInboxUnitTypeArr, 0, 27);
        System.arraycopy(new GraphQLMessengerInboxUnitType[]{graphQLMessengerInboxUnitType28, graphQLMessengerInboxUnitType29, graphQLMessengerInboxUnitType30, graphQLMessengerInboxUnitType31, graphQLMessengerInboxUnitType32, graphQLMessengerInboxUnitType33, graphQLMessengerInboxUnitType34, graphQLMessengerInboxUnitType35, graphQLMessengerInboxUnitType36, graphQLMessengerInboxUnitType37, graphQLMessengerInboxUnitType38, graphQLMessengerInboxUnitType39, graphQLMessengerInboxUnitType40, graphQLMessengerInboxUnitType41, graphQLMessengerInboxUnitType42, graphQLMessengerInboxUnitType43, graphQLMessengerInboxUnitType44, graphQLMessengerInboxUnitType45, graphQLMessengerInboxUnitType46, graphQLMessengerInboxUnitType47, graphQLMessengerInboxUnitType48, graphQLMessengerInboxUnitType49, graphQLMessengerInboxUnitType50, graphQLMessengerInboxUnitType51, graphQLMessengerInboxUnitType52}, 0, graphQLMessengerInboxUnitTypeArr, 27, 25);
        A00 = graphQLMessengerInboxUnitTypeArr;
    }

    public static GraphQLMessengerInboxUnitType valueOf(String str) {
        return (GraphQLMessengerInboxUnitType) Enum.valueOf(GraphQLMessengerInboxUnitType.class, str);
    }

    public static GraphQLMessengerInboxUnitType[] values() {
        return (GraphQLMessengerInboxUnitType[]) A00.clone();
    }

    private GraphQLMessengerInboxUnitType(String str, int i) {
    }
}
