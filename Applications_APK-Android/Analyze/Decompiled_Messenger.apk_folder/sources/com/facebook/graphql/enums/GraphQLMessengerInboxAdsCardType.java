package com.facebook.graphql.enums;

public enum GraphQLMessengerInboxAdsCardType {
    UNSET_OR_UNRECOGNIZED_ENUM_VALUE,
    END_CARD,
    IMAGE,
    VIDEO
}
