package com.facebook.graphql.querybuilder.common;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = ScaleInputPixelRatioSerializer.class)
public enum ScaleInputPixelRatio {
    NUMBER_1("1"),
    NUMBER_1_5("1.5"),
    NUMBER_2("2"),
    NUMBER_3("3"),
    NUMBER_4("4");
    
    public final String serverValue;

    public String toString() {
        return this.serverValue;
    }

    private ScaleInputPixelRatio(String str) {
        this.serverValue = str;
    }
}
