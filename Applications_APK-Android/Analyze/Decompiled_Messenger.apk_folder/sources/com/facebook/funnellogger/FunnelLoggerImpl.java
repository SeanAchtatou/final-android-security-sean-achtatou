package com.facebook.funnellogger;

import X.AnonymousClass06B;
import X.AnonymousClass08S;
import X.AnonymousClass0UN;
import X.AnonymousClass0V4;
import X.AnonymousClass0V7;
import X.AnonymousClass0WD;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YI;
import X.AnonymousClass25W;
import X.AnonymousClass25X;
import X.AnonymousClass260;
import X.AnonymousClass261;
import X.AnonymousClass26K;
import X.C010708t;
import X.C05180Xy;
import X.C05920aY;
import X.C08900gA;
import X.C109245Jm;
import X.C11670nb;
import X.C182058cg;
import X.C185414b;
import X.C185514c;
import X.C195929Ja;
import X.C22932BMs;
import X.C28081eE;
import X.C29377EYb;
import X.C29381EYf;
import X.C30396EvX;
import X.C413525c;
import X.C415025u;
import X.C415125v;
import android.os.HandlerThread;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
public final class FunnelLoggerImpl implements C185414b {
    private static volatile FunnelLoggerImpl A07;
    public AnonymousClass0UN A00;
    public Map A01 = new HashMap();
    public Map A02 = new HashMap();
    public boolean A03;
    public final C185514c A04;
    private volatile boolean A05 = false;
    private volatile boolean A06 = false;

    public static C109245Jm A02(FunnelLoggerImpl funnelLoggerImpl, String str, C08900gA r13, long j, long j2, long j3, boolean z) {
        int A012;
        C08900gA r10 = r13;
        if (r13.A06) {
            A012 = 1;
        } else {
            A012 = ((AnonymousClass260) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B64, funnelLoggerImpl.A00)).A01(r13);
        }
        String str2 = str;
        long j4 = j3;
        long j5 = j2;
        if (A012 != Integer.MAX_VALUE) {
            ((C195929Ja) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AcI, funnelLoggerImpl.A00)).A03(str2, Long.valueOf(j), j5, j4, null);
            if (z) {
                return new C109245Jm(r10, j5, j5, null, true);
            }
            int i = r13.A02;
            boolean z2 = false;
            if (i != -1) {
                z2 = true;
            }
            if (z2) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBd, funnelLoggerImpl.A00)).markerStart(i, r13.A0E, "FunnelInstanceId", Long.toString(j));
            }
            return new C109245Jm(r10, j5, j5, null, false);
        } else if (!r13.A0A) {
            return null;
        } else {
            ((C195929Ja) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AcI, funnelLoggerImpl.A00)).A03(str2, Long.valueOf(j), j5, j4, null);
            return new C109245Jm(r10, j5, j5, null, true);
        }
    }

    public synchronized void AMt(C08900gA r5, AnonymousClass26K r6) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A02 = r6;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(8, r2));
    }

    public synchronized void AMu(C08900gA r5, long j, String str) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A04 = Long.valueOf(j);
        r3.A09 = str;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(4, r2));
    }

    public synchronized void AMv(C08900gA r5, String str) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A09 = str;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(4, r2));
    }

    public synchronized void AOH(C08900gA r8, long j, String str) {
        AON(r8, j, str, null, null);
    }

    public synchronized void AOI(C08900gA r2, String str) {
        AOO(r2, str, null, null);
    }

    public synchronized void AOJ(C08900gA r8, long j, String str) {
        AOQ(r8, j, str, null, null);
    }

    public synchronized void AOK(C08900gA r2, String str) {
        AOR(r2, str, null, null);
    }

    public synchronized void AOL(C08900gA r8, long j, String str, String str2) {
        AON(r8, j, str, str2, null);
    }

    public synchronized void AOM(C08900gA r2, String str, String str2) {
        AOO(r2, str, str2, null);
    }

    public synchronized void AON(C08900gA r5, long j, String str, String str2, AnonymousClass26K r10) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A04 = Long.valueOf(j);
        r3.A07 = str;
        r3.A03 = false;
        r3.A08 = str2;
        r3.A01 = r10;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(3, r2));
    }

    public synchronized void AOO(C08900gA r8, String str, String str2, AnonymousClass26K r11) {
        C08900gA r1 = r8;
        String str3 = str2;
        AOP(r1, str, str3, r11, ((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
    }

    public synchronized void AOQ(C08900gA r5, long j, String str, String str2, AnonymousClass26K r10) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A04 = Long.valueOf(j);
        r3.A07 = str;
        r3.A03 = true;
        r3.A08 = str2;
        r3.A01 = r10;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(3, r2));
    }

    public synchronized void AOR(C08900gA r5, String str, String str2, AnonymousClass26K r8) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A07 = str;
        r3.A03 = true;
        r3.A08 = str2;
        r3.A01 = r8;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(3, r2));
    }

    public synchronized void ARl(C08900gA r5) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(5, r2));
    }

    public synchronized void ARm(C08900gA r5, long j) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A04 = Long.valueOf(j);
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(5, r2));
    }

    public synchronized void AYS(C08900gA r5) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r0 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(2, r0));
    }

    public synchronized void AYT(C08900gA r5, long j) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A04 = Long.valueOf(j);
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        AnonymousClass25X r0 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(2, r0));
    }

    public synchronized ImmutableList Ans(C08900gA r3) {
        ImmutableList immutableList;
        C415025u r1 = (C415025u) this.A01.get(r3.A0D);
        if (r1 != null && !r1.A0B) {
            C415025u.A00(r1);
            List list = r1.A03;
            if (list != null) {
                C415025u.A00(r1);
                immutableList = ImmutableList.copyOf((Collection) list);
            }
        }
        immutableList = RegularImmutableList.A02;
        return immutableList;
    }

    public synchronized long Ant(C08900gA r3) {
        long j;
        C415025u r1 = (C415025u) this.A01.get(r3.A0D);
        if (r1 == null || r1.A0B) {
            j = 0;
        } else {
            C415025u.A00(r1);
            j = r1.A07;
        }
        return j;
    }

    public synchronized void CH1(C08900gA r5) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        r3.A0A = A05(r5);
        r3.A06 = Long.valueOf(A03().longValue());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(1, r2));
    }

    public synchronized void CH2(C08900gA r5, long j) {
        A06(r5);
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A04 = Long.valueOf(j);
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        r3.A0A = A05(r5);
        r3.A06 = Long.valueOf(A03().longValue());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(1, r2));
    }

    public synchronized void CH3(C08900gA r5) {
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        r3.A0A = A05(r5);
        r3.A06 = Long.valueOf(A03().longValue());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(7, r2));
    }

    public synchronized void CH4(C08900gA r5, long j) {
        AnonymousClass25W r3 = new AnonymousClass25W();
        r3.A00 = r5;
        r3.A04 = Long.valueOf(j);
        r3.A05 = Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now());
        r3.A0A = A05(r5);
        r3.A06 = Long.valueOf(A03().longValue());
        AnonymousClass25X r2 = new AnonymousClass25X(r3);
        C185514c r1 = this.A04;
        r1.sendMessage(r1.obtainMessage(7, r2));
    }

    public static C415025u A00(FunnelLoggerImpl funnelLoggerImpl, C08900gA r22, long j, long j2, List list, long j3, boolean z) {
        int A012;
        String arrayNode;
        C08900gA r6 = r22;
        FunnelLoggerImpl funnelLoggerImpl2 = funnelLoggerImpl;
        if (r6.A06) {
            A012 = 1;
        } else {
            A012 = ((AnonymousClass260) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B64, funnelLoggerImpl2.A00)).A01(r6);
        }
        long j4 = j2;
        if (A012 == Integer.MAX_VALUE) {
            if (!r6.A0A) {
                return null;
            }
        } else if (!z) {
            List<String> list2 = list;
            if (list == null) {
                arrayNode = null;
            } else {
                ArrayNode arrayNode2 = new ArrayNode(JsonNodeFactory.instance);
                for (String add : list2) {
                    arrayNode2.add(add);
                }
                arrayNode = arrayNode2.toString();
            }
            C08900gA r13 = r6;
            C415025u r12 = new C415025u(r13, j, A012, j4, arrayNode, j3, false);
            C08900gA r62 = r12.A09;
            int i = r62.A02;
            boolean z2 = false;
            if (i != -1) {
                z2 = true;
            }
            if (z2) {
                C415025u.A00(r12);
                short s = r62.A0E;
                C415025u.A00(r12);
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBd, funnelLoggerImpl2.A00)).markerStart(i, s, "FunnelInstanceId", Long.toString(r12.A07));
            }
            return r12;
        }
        return new C415025u(r6, 0, 0, j4, null, 0, true);
    }

    public static final FunnelLoggerImpl A01(AnonymousClass1XY r5) {
        if (A07 == null) {
            synchronized (FunnelLoggerImpl.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A07 = new FunnelLoggerImpl(applicationInjector, AnonymousClass0V4.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    private Long A03() {
        long j;
        String str;
        ViewerContext Asn = ((C05920aY) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BJQ, this.A00)).Asn();
        if (Asn == null || (str = Asn.mUserId) == null) {
            j = 0;
        } else {
            j = Long.parseLong(str);
        }
        return Long.valueOf(j);
    }

    public static String A04(AnonymousClass25X r3) {
        Long l = r3.A04;
        if (l == null) {
            return r3.A00.A0D;
        }
        C08900gA r0 = r3.A00;
        return AnonymousClass08S.A0O(r0.A0D, ":", l.longValue());
    }

    private List A05(C08900gA r4) {
        if (r4.A05) {
            return ((C22932BMs) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ABj, this.A00)).A00.A06();
        }
        return null;
    }

    private static void A06(C08900gA r1) {
        if (r1 == null) {
            throw new IllegalArgumentException("FunnelDefinition is null, expecting non-null value");
        }
    }

    public static void A07(FunnelLoggerImpl funnelLoggerImpl) {
        LinkedList<String> linkedList = new LinkedList<>();
        for (Map.Entry entry : funnelLoggerImpl.A02.entrySet()) {
            C109245Jm r4 = (C109245Jm) entry.getValue();
            String str = (String) entry.getKey();
            try {
                if (funnelLoggerImpl.A0D(str, r4, false, null) || funnelLoggerImpl.A0E(str, r4, false, null) || funnelLoggerImpl.A0F(str, r4, false, null)) {
                    linkedList.add(str);
                } else {
                    funnelLoggerImpl.A0G(str, r4, false, null);
                }
            } catch (NullPointerException e) {
                C010708t.A0W("FunnelLoggerImpl", e, "NPE for key: %s", entry.getKey());
            }
        }
        for (String remove : linkedList) {
            funnelLoggerImpl.A02.remove(remove);
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        X.C010708t.A0T("FunnelLoggerImpl", r2, "Failed to load funnels");
        r1 = (X.C413525c) X.AnonymousClass1XX.A02(6, X.AnonymousClass1Y3.Aaz, r7.A00);
        X.C413625d.A01(r1.A02).delete();
        r1.A01.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01ae, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01af, code lost:
        r7.A06 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01b1, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A08(com.facebook.funnellogger.FunnelLoggerImpl r21) {
        /*
            r7 = r21
            boolean r0 = r7.A06
            if (r0 == 0) goto L_0x0007
            return
        L_0x0007:
            java.lang.String r21 = "FunnelLoggerImpl"
            r6 = 6
            r5 = 1
            int r1 = X.AnonymousClass1Y3.Aaz     // Catch:{ IOException -> 0x018b }
            X.0UN r0 = r7.A00     // Catch:{ IOException -> 0x018b }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ IOException -> 0x018b }
            X.25c r8 = (X.C413525c) r8     // Catch:{ IOException -> 0x018b }
            X.25d r0 = r8.A02     // Catch:{ IOException -> 0x018b }
            r20 = r0
            monitor-enter(r20)     // Catch:{ IOException -> 0x018b }
            java.io.File r3 = X.C413625d.A01(r20)     // Catch:{ all -> 0x0188 }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ all -> 0x0188 }
            r4.<init>()     // Catch:{ all -> 0x0188 }
            boolean r0 = r3.exists()     // Catch:{ all -> 0x0188 }
            if (r0 == 0) goto L_0x0177
            java.io.DataInputStream r19 = new java.io.DataInputStream     // Catch:{ all -> 0x0188 }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0188 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x0188 }
            r1.<init>(r3)     // Catch:{ all -> 0x0188 }
            r0 = 1024(0x400, float:1.435E-42)
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0188 }
            r0 = r19
            r0.<init>(r2)     // Catch:{ all -> 0x0188 }
            byte r0 = r19.readByte()     // Catch:{ all -> 0x0172 }
            if (r0 == r5) goto L_0x005a
            java.lang.String r3 = "FunnelBackupStorageFileImpl"
            java.lang.String r2 = "Expected version %d, found version %d"
            java.lang.Byte r1 = java.lang.Byte.valueOf(r5)     // Catch:{ all -> 0x0172 }
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)     // Catch:{ all -> 0x0172 }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0}     // Catch:{ all -> 0x0172 }
            X.C010708t.A0O(r3, r2, r0)     // Catch:{ all -> 0x0172 }
            r19.close()     // Catch:{ all -> 0x0188 }
            goto L_0x0177
        L_0x005a:
            int r9 = r19.readInt()     // Catch:{ all -> 0x0172 }
            r3 = 0
        L_0x005f:
            if (r3 >= r9) goto L_0x016b
            java.lang.String r2 = r19.readUTF()     // Catch:{ all -> 0x0172 }
            X.25t r18 = new X.25t     // Catch:{ all -> 0x0172 }
            r18.<init>()     // Catch:{ all -> 0x0172 }
            r17 = 0
        L_0x006c:
            if (r17 != 0) goto L_0x014d
            short r10 = r19.readShort()     // Catch:{ all -> 0x0172 }
            switch(r10) {
                case 1: goto L_0x013f;
                case 2: goto L_0x0135;
                case 3: goto L_0x012b;
                case 4: goto L_0x0121;
                case 5: goto L_0x0117;
                case 6: goto L_0x00fb;
                case 7: goto L_0x00aa;
                case 8: goto L_0x00a1;
                case 9: goto L_0x009e;
                case 10: goto L_0x0095;
                case 11: goto L_0x008c;
                case 12: goto L_0x0083;
                default: goto L_0x0075;
            }     // Catch:{ all -> 0x0172 }
        L_0x0075:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0172 }
            java.lang.String r1 = "Unsupported type "
            java.lang.String r0 = " while loading funnels"
            java.lang.String r0 = X.AnonymousClass08S.A0A(r1, r10, r0)     // Catch:{ all -> 0x0172 }
            r2.<init>(r0)     // Catch:{ all -> 0x0172 }
            throw r2     // Catch:{ all -> 0x0172 }
        L_0x0083:
            long r0 = r19.readLong()     // Catch:{ all -> 0x0172 }
            r10 = r18
            r10.A04 = r0     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x008c:
            java.lang.String r1 = r19.readUTF()     // Catch:{ all -> 0x0172 }
            r0 = r18
            r0.A07 = r1     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x0095:
            boolean r1 = r19.readBoolean()     // Catch:{ all -> 0x0172 }
            r0 = r18
            r0.A09 = r1     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x009e:
            r17 = 1
            goto L_0x006c
        L_0x00a1:
            boolean r1 = r19.readBoolean()     // Catch:{ all -> 0x0172 }
            r0 = r18
            r0.A0A = r1     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x00aa:
            int r15 = r19.readInt()     // Catch:{ all -> 0x0172 }
            java.util.ArrayList r14 = new java.util.ArrayList     // Catch:{ all -> 0x0172 }
            r14.<init>(r15)     // Catch:{ all -> 0x0172 }
            r13 = 0
        L_0x00b4:
            if (r13 >= r15) goto L_0x00f5
            r16 = 0
            r12 = 0
            r11 = r12
            r10 = r12
            r1 = 0
        L_0x00bc:
            if (r16 != 0) goto L_0x00ea
            short r0 = r19.readShort()     // Catch:{ all -> 0x0172 }
            switch(r0) {
                case 701: goto L_0x00e5;
                case 702: goto L_0x00e0;
                case 703: goto L_0x00db;
                case 704: goto L_0x00d6;
                case 705: goto L_0x00d3;
                default: goto L_0x00c5;
            }     // Catch:{ all -> 0x0172 }
        L_0x00c5:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0172 }
            java.lang.String r2 = "Unsupported type "
            java.lang.String r1 = " while loading funnels"
            java.lang.String r0 = X.AnonymousClass08S.A0A(r2, r0, r1)     // Catch:{ all -> 0x0172 }
            r3.<init>(r0)     // Catch:{ all -> 0x0172 }
            throw r3     // Catch:{ all -> 0x0172 }
        L_0x00d3:
            r16 = 1
            goto L_0x00bc
        L_0x00d6:
            java.lang.String r10 = r19.readUTF()     // Catch:{ all -> 0x0172 }
            goto L_0x00bc
        L_0x00db:
            java.lang.String r11 = r19.readUTF()     // Catch:{ all -> 0x0172 }
            goto L_0x00bc
        L_0x00e0:
            int r1 = r19.readInt()     // Catch:{ all -> 0x0172 }
            goto L_0x00bc
        L_0x00e5:
            java.lang.String r12 = r19.readUTF()     // Catch:{ all -> 0x0172 }
            goto L_0x00bc
        L_0x00ea:
            X.25v r0 = new X.25v     // Catch:{ all -> 0x0172 }
            r0.<init>(r12, r1, r11, r10)     // Catch:{ all -> 0x0172 }
            r14.add(r0)     // Catch:{ all -> 0x0172 }
            int r13 = r13 + 1
            goto L_0x00b4
        L_0x00f5:
            r0 = r18
            r0.A08 = r14     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x00fb:
            int r11 = r19.readInt()     // Catch:{ all -> 0x0172 }
            X.0Xy r10 = new X.0Xy     // Catch:{ all -> 0x0172 }
            r10.<init>(r11)     // Catch:{ all -> 0x0172 }
            r1 = 0
        L_0x0105:
            if (r1 >= r11) goto L_0x0111
            java.lang.String r0 = r19.readUTF()     // Catch:{ all -> 0x0172 }
            r10.add(r0)     // Catch:{ all -> 0x0172 }
            int r1 = r1 + 1
            goto L_0x0105
        L_0x0111:
            r0 = r18
            r0.A05 = r10     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x0117:
            long r0 = r19.readLong()     // Catch:{ all -> 0x0172 }
            r10 = r18
            r10.A03 = r0     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x0121:
            long r0 = r19.readLong()     // Catch:{ all -> 0x0172 }
            r10 = r18
            r10.A01 = r0     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x012b:
            int r1 = r19.readInt()     // Catch:{ all -> 0x0172 }
            r0 = r18
            r0.A00 = r1     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x0135:
            long r0 = r19.readLong()     // Catch:{ all -> 0x0172 }
            r10 = r18
            r10.A02 = r0     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x013f:
            java.lang.String r0 = r19.readUTF()     // Catch:{ all -> 0x0172 }
            X.0gA r1 = X.C08900gA.A01(r0)     // Catch:{ all -> 0x0172 }
            r0 = r18
            r0.A06 = r1     // Catch:{ all -> 0x0172 }
            goto L_0x006c
        L_0x014d:
            X.25u r1 = new X.25u     // Catch:{ all -> 0x0172 }
            r0 = r18
            r1.<init>(r0)     // Catch:{ all -> 0x0172 }
            X.0gA r0 = r1.A09     // Catch:{ all -> 0x0172 }
            if (r0 == 0) goto L_0x015c
            r4.put(r2, r1)     // Catch:{ all -> 0x0172 }
            goto L_0x0167
        L_0x015c:
            java.lang.String r10 = "FunnelBackupStorageFileImpl"
            java.lang.String r1 = "Parsed funnel instance with null FunnelDefinition for key: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x0172 }
            X.C010708t.A0O(r10, r1, r0)     // Catch:{ all -> 0x0172 }
        L_0x0167:
            int r3 = r3 + 1
            goto L_0x005f
        L_0x016b:
            r19.close()     // Catch:{ all -> 0x0188 }
            r4.size()     // Catch:{ all -> 0x0188 }
            goto L_0x0177
        L_0x0172:
            r0 = move-exception
            r19.close()     // Catch:{ all -> 0x0188 }
            throw r0     // Catch:{ all -> 0x0188 }
        L_0x0177:
            monitor-exit(r20)     // Catch:{ IOException -> 0x018b }
            X.25f r0 = r8.A01()     // Catch:{ IOException -> 0x018b }
            r0.C2P(r4)     // Catch:{ IOException -> 0x018b }
            boolean r0 = r4.isEmpty()     // Catch:{ IOException -> 0x018b }
            if (r0 != 0) goto L_0x01ab
            r7.A01 = r4     // Catch:{ IOException -> 0x018b }
            goto L_0x01ab
        L_0x0188:
            r0 = move-exception
            monitor-exit(r20)     // Catch:{ IOException -> 0x018b }
            throw r0     // Catch:{ IOException -> 0x018b }
        L_0x018b:
            r2 = move-exception
            java.lang.String r1 = "Failed to load funnels"
            r0 = r21
            X.C010708t.A0T(r0, r2, r1)     // Catch:{ all -> 0x01ae }
            int r1 = X.AnonymousClass1Y3.Aaz     // Catch:{ all -> 0x01ae }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x01ae }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x01ae }
            X.25c r1 = (X.C413525c) r1     // Catch:{ all -> 0x01ae }
            X.25d r0 = r1.A02     // Catch:{ all -> 0x01ae }
            java.io.File r0 = X.C413625d.A01(r0)     // Catch:{ all -> 0x01ae }
            r0.delete()     // Catch:{ all -> 0x01ae }
            X.25f r0 = r1.A01     // Catch:{ all -> 0x01ae }
            r0.clear()     // Catch:{ all -> 0x01ae }
        L_0x01ab:
            r7.A06 = r5
            return
        L_0x01ae:
            r0 = move-exception
            r7.A06 = r5
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.funnellogger.FunnelLoggerImpl.A08(com.facebook.funnellogger.FunnelLoggerImpl):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, short, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public static void A09(FunnelLoggerImpl funnelLoggerImpl, String str, C415025u r9, C182058cg r10, long j) {
        if (!r9.A0B) {
            C415125v r4 = new C415125v("funnel_end", (int) (j - r9.A06), r10.mType);
            r9.A01(r4, j);
            A0B(funnelLoggerImpl, str, r9.A09, r4);
            C415025u.A00(r9);
            C11670nb r3 = new C11670nb("funnel_analytics");
            r3.A0D("name", r9.A09.A0D);
            C415025u.A00(r9);
            r3.A09("funnel_id", r9.A09.A0E);
            r3.A0A("instance_id", r9.A07);
            r3.A0A(TraceFieldType.StartTime, r9.A06);
            r3.A09("sampling_rate", r9.A05);
            r3.A0A("funnel_uid", r9.A08);
            r3.A0E("pseudo_end", r9.A09.A08);
            r3.A09("version", r9.A09.A00);
            String str2 = r9.A0A;
            if (str2 != null) {
                r3.A0D("source_path", str2);
            }
            r3.A0E("high_priority_channel", r9.A09.A0C);
            if (r9.A09.A0C) {
                C11670nb.A00(r3, true).put("upload_this_event_now", "true");
            }
            if (r9.A04) {
                if (r9.A01 == null) {
                    r9.A01 = new C05180Xy();
                }
                r9.A01.add("tracked");
            }
            C05180Xy r1 = r9.A01;
            if (r1 != null) {
                ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
                Iterator it = r1.iterator();
                while (it.hasNext()) {
                    arrayNode.add((String) it.next());
                }
                r3.A0B("tags", arrayNode);
            }
            AnonymousClass26K r12 = r9.A02;
            if (r12 != null) {
                r3.A0C("payload", r12);
            }
            List<C415125v> list = r9.A03;
            if (list != null) {
                ArrayNode arrayNode2 = new ArrayNode(JsonNodeFactory.instance);
                for (C415125v A002 : list) {
                    arrayNode2.add(A002.A00());
                }
                r3.A0B("actions", arrayNode2);
            }
            if (r10 != C182058cg.A04) {
                ((AnonymousClass261) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BR6, funnelLoggerImpl.A00)).A04(r9.A09);
            }
            ((AnonymousClass261) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BR6, funnelLoggerImpl.A00)).A02(r3, r9.A09);
            C29381EYf eYf = (C29381EYf) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B5y, funnelLoggerImpl.A00);
            if (C29377EYb.A00 == null) {
                C29377EYb.A00 = new C29377EYb(eYf);
            }
            C29377EYb.A00.A01(r3);
            C08900gA r6 = r9.A09;
            int i = r6.A02;
            boolean z = false;
            if (i != -1) {
                z = true;
            }
            if (z) {
                C415025u.A00(r9);
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBd, funnelLoggerImpl.A00)).markerEnd(i, (int) r6.A0E, (short) 2);
            }
            try {
                ((C413525c) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Aaz, funnelLoggerImpl.A00)).A01().Bzy(str, j);
            } catch (IOException e) {
                C010708t.A0N("FunnelLoggerImpl", "Failed to write end funnel operation to changelog.", e);
            }
            int i2 = AnonymousClass1Y3.Awn;
            if (!((Set) AnonymousClass1XX.A02(10, i2, funnelLoggerImpl.A00)).isEmpty()) {
                String A052 = r3.A05();
                for (C30396EvX onEndFunnel : (Set) AnonymousClass1XX.A02(10, i2, funnelLoggerImpl.A00)) {
                    onEndFunnel.onEndFunnel(r9.A09.A0D, A052);
                }
            }
        }
    }

    public static void A0B(FunnelLoggerImpl funnelLoggerImpl, String str, C08900gA r7, C415125v r8) {
        C415025u r4;
        if (r7.A09 && (r4 = (C415025u) funnelLoggerImpl.A01.get(str)) != null && !r4.A0B && r8 != null) {
            C415025u.A00(r4);
            C11670nb r3 = new C11670nb("server_aggregated_funnel_analytics");
            r3.A0D("name", r4.A09.A0D);
            r3.A0A("instance_id", r4.A07);
            r3.A0A(TraceFieldType.StartTime, r4.A06);
            r3.A0A("funnel_uid", r4.A08);
            if (r8 != null) {
                r3.A0B("action", r8.A00());
            }
            C29381EYf eYf = (C29381EYf) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B5y, funnelLoggerImpl.A00);
            if (C29377EYb.A00 == null) {
                C29377EYb.A00 = new C29377EYb(eYf);
            }
            C29377EYb.A00.A01(r3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002e, code lost:
        if (r7.isOpen() == false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x021f, code lost:
        if (r3.isOpen() == false) goto L_0x0221;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0C(com.facebook.funnellogger.FunnelLoggerImpl r26) {
        /*
            r4 = r26
            boolean r0 = r4.A03
            r18 = 0
            if (r0 == 0) goto L_0x0264
            boolean r0 = r4.A05
            if (r0 != 0) goto L_0x0264
            java.lang.String r5 = "FunnelLoggerImpl"
            int r1 = X.AnonymousClass1Y3.AcI
            X.0UN r0 = r4.A00
            r3 = 13
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.9Ja r2 = (X.C195929Ja) r2
            X.9Hq r0 = r2.A00
            X.8PQ r0 = r0.A00
            android.database.sqlite.SQLiteDatabase r7 = r0.A06()
            java.util.LinkedList r1 = new java.util.LinkedList
            r1.<init>()
            if (r7 == 0) goto L_0x0030
            boolean r6 = r7.isOpen()
            r0 = 1
            if (r6 != 0) goto L_0x0031
        L_0x0030:
            r0 = 0
        L_0x0031:
            if (r0 != 0) goto L_0x00f3
            java.lang.String r6 = X.C195649Hq.A03
            java.lang.String r0 = "Db is not currently connected. Fail to read all records"
            X.C010708t.A0K(r6, r0)
        L_0x003a:
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            java.util.HashSet r9 = new java.util.HashSet
            r9.<init>()
            java.util.Iterator r17 = r1.iterator()
        L_0x0048:
            boolean r0 = r17.hasNext()
            r10 = 0
            if (r0 == 0) goto L_0x0115
            java.lang.Object r12 = r17.next()
            X.8SS r12 = (X.AnonymousClass8SS) r12
            java.lang.String r11 = r12.A04
            java.lang.String r0 = ":"
            java.lang.String[] r0 = r11.split(r0)
            r6 = r0[r18]
            long r0 = r12.A01
            int r15 = r12.A00
            java.lang.String r14 = r12.A05
            boolean r7 = r8.containsKey(r11)
            r13 = 2
            if (r7 != 0) goto L_0x00b9
            r7 = 1
            if (r15 == r7) goto L_0x0088
            java.lang.String r6 = "FunnelLoggerDbUtils"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r15)
            java.lang.Object[] r1 = new java.lang.Object[]{r11, r0, r14}
            java.lang.String r0 = "No funnel start records returned from db for funnel %s. Operation code is %d. Operation value is %s"
            X.C010708t.A0Q(r6, r0, r1)
            boolean r0 = r9.contains(r11)
            if (r0 != 0) goto L_0x0048
            r9.add(r11)
            goto L_0x0048
        L_0x0088:
            X.0gA r20 = X.C08900gA.A01(r6)
            if (r20 != 0) goto L_0x0099
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r7 = "Unable to find FunnelDefinition for: "
            java.lang.String r6 = X.AnonymousClass08S.A0J(r7, r6)
            r10.<init>(r6)
        L_0x0099:
            java.util.LinkedList r10 = new java.util.LinkedList
            r10.<init>()
            r10.add(r12)
            android.util.Pair r7 = new android.util.Pair
            X.5Jm r6 = new X.5Jm
            r25 = 0
            r26 = 0
            r19 = r6
            r21 = r0
            r23 = r0
            r19.<init>(r20, r21, r23, r25, r26)
            r7.<init>(r6, r10)
            r8.put(r11, r7)
            goto L_0x0048
        L_0x00b9:
            java.lang.Object r6 = r8.get(r11)
            if (r6 == 0) goto L_0x00c9
            java.lang.Object r6 = r8.get(r11)
            android.util.Pair r6 = (android.util.Pair) r6
            java.lang.Object r10 = r6.first
            X.5Jm r10 = (X.C109245Jm) r10
        L_0x00c9:
            if (r10 == 0) goto L_0x0048
            long r6 = r10.A00
            int r16 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r16 <= 0) goto L_0x00d3
            r10.A00 = r0
        L_0x00d3:
            if (r15 != r13) goto L_0x00de
            r10.A02 = r14
            short r0 = r10.A03
            int r0 = r0 + 1
            short r0 = (short) r0
            r10.A03 = r0
        L_0x00de:
            java.lang.Object r0 = r8.get(r11)
            if (r0 == 0) goto L_0x0048
            java.lang.Object r0 = r8.get(r11)
            android.util.Pair r0 = (android.util.Pair) r0
            java.lang.Object r0 = r0.second
            java.util.List r0 = (java.util.List) r0
            r0.add(r12)
            goto L_0x0048
        L_0x00f3:
            r6 = 0
            java.lang.String r8 = "funnel_logger_table"
            java.lang.String[] r9 = X.C195649Hq.A04     // Catch:{ SQLiteException -> 0x0106 }
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            java.lang.String r14 = X.C195649Hq.A02     // Catch:{ SQLiteException -> 0x0106 }
            android.database.Cursor r6 = r7.query(r8, r9, r10, r11, r12, r13, r14)     // Catch:{ SQLiteException -> 0x0106 }
            X.C195649Hq.A01(r6, r1)     // Catch:{ SQLiteException -> 0x0106 }
            goto L_0x010e
        L_0x0106:
            r8 = move-exception
            java.lang.String r7 = X.C195649Hq.A03     // Catch:{ all -> 0x025d }
            java.lang.String r0 = "Exception when query record db or iterate cursor. Exception: %s"
            X.C010708t.A0N(r7, r0, r8)     // Catch:{ all -> 0x025d }
        L_0x010e:
            if (r6 == 0) goto L_0x003a
            r6.close()
            goto L_0x003a
        L_0x0115:
            java.util.Set r0 = r8.entrySet()
            java.util.Iterator r11 = r0.iterator()
        L_0x011d:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x0184
            java.lang.Object r7 = r11.next()
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7
            java.lang.Object r0 = r7.getValue()
            if (r0 == 0) goto L_0x011d
            java.lang.Object r0 = r7.getValue()
            android.util.Pair r0 = (android.util.Pair) r0
            java.lang.Object r0 = r0.first
            X.5Jm r0 = (X.C109245Jm) r0
            java.lang.String r0 = r0.A02
            if (r0 == 0) goto L_0x011d
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0168 }
            java.lang.Object r0 = r7.getValue()     // Catch:{ JSONException -> 0x0168 }
            android.util.Pair r0 = (android.util.Pair) r0     // Catch:{ JSONException -> 0x0168 }
            java.lang.Object r0 = r0.first     // Catch:{ JSONException -> 0x0168 }
            X.5Jm r0 = (X.C109245Jm) r0     // Catch:{ JSONException -> 0x0168 }
            java.lang.String r0 = r0.A02     // Catch:{ JSONException -> 0x0168 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0168 }
            java.lang.String r0 = "name"
            java.lang.String r6 = r1.getString(r0)     // Catch:{ JSONException -> 0x0168 }
            java.lang.Object r0 = r7.getValue()     // Catch:{ JSONException -> 0x0168 }
            android.util.Pair r0 = (android.util.Pair) r0     // Catch:{ JSONException -> 0x0168 }
            java.lang.Object r1 = r0.first     // Catch:{ JSONException -> 0x0168 }
            X.5Jm r1 = (X.C109245Jm) r1     // Catch:{ JSONException -> 0x0168 }
            r1.A02 = r6     // Catch:{ JSONException -> 0x0168 }
            short r0 = r1.A03     // Catch:{ JSONException -> 0x0168 }
            int r0 = r0 + 1
            short r0 = (short) r0     // Catch:{ JSONException -> 0x0168 }
            r1.A03 = r0     // Catch:{ JSONException -> 0x0168 }
            goto L_0x011d
        L_0x0168:
            r6 = move-exception
            java.lang.Object r0 = r7.getValue()
            android.util.Pair r0 = (android.util.Pair) r0
            java.lang.Object r1 = r0.first
            X.5Jm r1 = (X.C109245Jm) r1
            r1.A02 = r10
            short r0 = r1.A03
            int r0 = r0 + 1
            short r0 = (short) r0
            r1.A03 = r0
            java.lang.String r1 = "FunnelLoggerDbUtils"
            java.lang.String r0 = "Failed to parse funnel action raw json string when init funnel. Exception: "
            X.C010708t.A0N(r1, r0, r6)
            goto L_0x011d
        L_0x0184:
            java.util.Iterator r1 = r9.iterator()
        L_0x0188:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0198
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            r2.A02(r0)
            goto L_0x0188
        L_0x0198:
            java.util.LinkedList r7 = new java.util.LinkedList
            r7.<init>()
            java.util.Set r0 = r8.entrySet()
            java.util.Iterator r10 = r0.iterator()
        L_0x01a5:
            boolean r0 = r10.hasNext()
            r6 = 1
            if (r0 == 0) goto L_0x01fc
            java.lang.Object r8 = r10.next()
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8
            java.lang.Object r9 = r8.getKey()
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r0 = r8.getValue()
            android.util.Pair r0 = (android.util.Pair) r0
            java.lang.Object r2 = r0.first
            X.5Jm r2 = (X.C109245Jm) r2
            java.lang.Object r0 = r8.getValue()
            android.util.Pair r0 = (android.util.Pair) r0
            java.lang.Object r1 = r0.second
            java.util.List r1 = (java.util.List) r1
            boolean r0 = r4.A0D(r9, r2, r6, r1)     // Catch:{ NullPointerException -> 0x01ed }
            if (r0 != 0) goto L_0x01e9
            boolean r0 = r4.A0E(r9, r2, r6, r1)     // Catch:{ NullPointerException -> 0x01ed }
            if (r0 != 0) goto L_0x01e9
            boolean r0 = r4.A0F(r9, r2, r6, r1)     // Catch:{ NullPointerException -> 0x01ed }
            if (r0 != 0) goto L_0x01e9
            java.util.Map r0 = r4.A02     // Catch:{ NullPointerException -> 0x01ed }
            r0.put(r9, r2)     // Catch:{ NullPointerException -> 0x01ed }
            boolean r0 = r4.A0G(r9, r2, r6, r1)     // Catch:{ NullPointerException -> 0x01ed }
            if (r0 == 0) goto L_0x01a5
        L_0x01e9:
            r7.add(r9)     // Catch:{ NullPointerException -> 0x01ed }
            goto L_0x01a5
        L_0x01ed:
            r2 = move-exception
            java.lang.Object r0 = r8.getKey()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "NPE for key: %s"
            X.C010708t.A0W(r5, r2, r0, r1)
            goto L_0x01a5
        L_0x01fc:
            int r1 = X.AnonymousClass1Y3.AcI
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.9Ja r0 = (X.C195929Ja) r0
            X.9Hq r1 = r0.A00
            X.0W6 r0 = X.C180698Zg.A00
            java.lang.String r0 = r0.toString()
            X.0av r5 = X.C06160ax.A04(r0, r7)
            X.8PQ r0 = r1.A00     // Catch:{ SQLiteException -> 0x0246 }
            android.database.sqlite.SQLiteDatabase r3 = r0.A06()     // Catch:{ SQLiteException -> 0x0246 }
            if (r3 == 0) goto L_0x0221
            boolean r1 = r3.isOpen()     // Catch:{ SQLiteException -> 0x0246 }
            r0 = 1
            if (r1 != 0) goto L_0x0222
        L_0x0221:
            r0 = 0
        L_0x0222:
            if (r0 != 0) goto L_0x0238
            java.lang.String r2 = X.C195649Hq.A03     // Catch:{ SQLiteException -> 0x0246 }
            java.lang.String r1 = "Db is not currently connected. Fail to delete records for %d funnels"
            int r0 = r7.size()     // Catch:{ SQLiteException -> 0x0246 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ SQLiteException -> 0x0246 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ SQLiteException -> 0x0246 }
            X.C010708t.A0Q(r2, r1, r0)     // Catch:{ SQLiteException -> 0x0246 }
            goto L_0x025a
        L_0x0238:
            java.lang.String r2 = "funnel_logger_table"
            java.lang.String r1 = r5.A02()     // Catch:{ SQLiteException -> 0x0246 }
            java.lang.String[] r0 = r5.A04()     // Catch:{ SQLiteException -> 0x0246 }
            r3.delete(r2, r1, r0)     // Catch:{ SQLiteException -> 0x0246 }
            goto L_0x025a
        L_0x0246:
            r1 = move-exception
            java.lang.String r2 = X.C195649Hq.A03
            int r0 = r7.size()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Funnel delete record from DB operation failed with exception %s. Total count of funnel key %d"
            X.C010708t.A0Q(r2, r0, r1)
        L_0x025a:
            r4.A05 = r6
            return r6
        L_0x025d:
            r0 = move-exception
            if (r6 == 0) goto L_0x0263
            r6.close()
        L_0x0263:
            throw r0
        L_0x0264:
            return r18
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.funnellogger.FunnelLoggerImpl.A0C(com.facebook.funnellogger.FunnelLoggerImpl):boolean");
    }

    private boolean A0D(String str, C109245Jm r11, boolean z, List list) {
        C08900gA r3 = r11.A04;
        if (!r3.A0B) {
            return false;
        }
        A0A(this, str, r3, C182058cg.A06, ((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now(), z, list);
        return true;
    }

    private boolean A0E(String str, C109245Jm r15, boolean z, List list) {
        int min;
        C08900gA r7 = r15.A04;
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now() - r15.A00;
        int i = r7.A04;
        if (i == -1) {
            min = 600;
        } else {
            min = Math.min(i, 86400);
        }
        if (now <= ((long) min) * 1000) {
            return false;
        }
        A0A(this, str, r7, C182058cg.A07, ((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now(), z, list);
        return true;
    }

    private boolean A0F(String str, C109245Jm r15, boolean z, List list) {
        C08900gA r7 = r15.A04;
        if (((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now() - r15.A01 <= ((long) r7.A02()) * 1000) {
            return false;
        }
        A0A(this, str, r7, C182058cg.A08, ((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now(), z, list);
        return true;
    }

    private boolean A0G(String str, C109245Jm r11, boolean z, List list) {
        C08900gA r3 = r11.A04;
        if (r11.A05 || !r3.A08) {
            return false;
        }
        A0A(this, str, r3, C182058cg.A04, ((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, this.A00)).now(), z, list);
        return true;
    }

    private FunnelLoggerImpl(AnonymousClass1XY r5, AnonymousClass0V4 r6) {
        this.A00 = new AnonymousClass0UN(14, r5);
        HandlerThread A022 = r6.A02("funnel-logger-worker", AnonymousClass0V7.NORMAL);
        A022.start();
        this.A04 = new C185514c(this, A022.getLooper());
        C28081eE.A00(FunnelLoggerImpl.class);
        this.A03 = ((AnonymousClass1YI) AnonymousClass1XX.A02(12, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A48, false);
    }

    public void AOP(C08900gA r4, String str, String str2, AnonymousClass26K r7, long j) {
        A06(r4);
        AnonymousClass25W r1 = new AnonymousClass25W();
        r1.A00 = r4;
        r1.A07 = str;
        r1.A03 = false;
        r1.A08 = str2;
        r1.A01 = r7;
        r1.A05 = Long.valueOf(j);
        AnonymousClass25X r2 = new AnonymousClass25X(r1);
        C185514c r12 = this.A04;
        r12.sendMessage(r12.obtainMessage(3, r2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, short, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0046, code lost:
        if (r16.isOpen() == false) goto L_0x0048;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0A(com.facebook.funnellogger.FunnelLoggerImpl r22, java.lang.String r23, X.C08900gA r24, X.C182058cg r25, long r26, boolean r28, java.util.List r29) {
        /*
            r2 = r29
            r6 = r22
            java.util.Map r3 = r6.A02
            r7 = r23
            java.lang.Object r3 = r3.get(r7)
            X.5Jm r3 = (X.C109245Jm) r3
            r15 = 13
            if (r3 == 0) goto L_0x0024
            boolean r3 = r3.A05
            if (r3 == 0) goto L_0x0024
            int r1 = X.AnonymousClass1Y3.AcI
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r15, r1, r0)
            X.9Ja r0 = (X.C195929Ja) r0
            r0.A02(r7)
            return
        L_0x0024:
            if (r28 != 0) goto L_0x0056
            int r3 = X.AnonymousClass1Y3.AcI
            X.0UN r2 = r6.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r15, r3, r2)
            X.9Ja r2 = (X.C195929Ja) r2
            X.9Hq r2 = r2.A00
            if (r23 == 0) goto L_0x025b
            X.8PQ r2 = r2.A00
            android.database.sqlite.SQLiteDatabase r16 = r2.A06()
            java.util.LinkedList r2 = new java.util.LinkedList
            r2.<init>()
            if (r16 == 0) goto L_0x0048
            boolean r4 = r16.isOpen()
            r3 = 1
            if (r4 != 0) goto L_0x0049
        L_0x0048:
            r3 = 0
        L_0x0049:
            if (r3 != 0) goto L_0x0107
            java.lang.String r5 = X.C195649Hq.A03
            java.lang.Object[] r4 = new java.lang.Object[]{r7}
            java.lang.String r3 = "Db is not currently connected. Fail to read records for funnel %s"
            X.C010708t.A0Q(r5, r3, r4)
        L_0x0056:
            java.util.Map r3 = r6.A02
            java.lang.Object r3 = r3.get(r7)
            if (r3 == 0) goto L_0x0103
            java.util.Map r3 = r6.A02
            java.lang.Object r3 = r3.get(r7)
            X.5Jm r3 = (X.C109245Jm) r3
            long r3 = r3.A01
        L_0x0068:
            r9 = r24
            boolean r5 = r9.A06
            r8 = 1
            r11 = 5
            if (r5 == 0) goto L_0x00d9
            r16 = 1
        L_0x0072:
            long r0 = r26 - r3
            int r11 = (int) r0
            java.util.ArrayList r21 = new java.util.ArrayList
            r21.<init>()
            java.util.ArrayList r20 = new java.util.ArrayList
            r20.<init>()
            r4 = 0
            r13 = 0
            if (r2 == 0) goto L_0x012f
            java.util.Iterator r12 = r2.iterator()
            r2 = 0
            r17 = 0
        L_0x008c:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x0134
            java.lang.Object r10 = r12.next()
            X.8SS r10 = (X.AnonymousClass8SS) r10
            int r1 = r10.A00
            if (r1 == r8) goto L_0x00ca
            r0 = 2
            if (r1 == r0) goto L_0x00c2
            r0 = 3
            if (r1 == r0) goto L_0x00b8
            r0 = 4
            if (r1 == r0) goto L_0x00b5
            java.lang.String r10 = "FunnelLoggerDbUtils"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Invalid operation code %d read from db."
            X.C010708t.A0Q(r10, r0, r1)
            goto L_0x008c
        L_0x00b5:
            java.lang.String r13 = r10.A05
            goto L_0x008c
        L_0x00b8:
            java.lang.String r0 = r10.A05
            r22 = r20
            r23 = r0
            r22.add(r23)
            goto L_0x008c
        L_0x00c2:
            java.lang.String r0 = r10.A05
            r22 = r0
            r21.add(r22)
            goto L_0x008c
        L_0x00ca:
            java.lang.Long r0 = r10.A03
            long r4 = r0.longValue()
            java.lang.Long r0 = r10.A02
            long r17 = r0.longValue()
            long r2 = r10.A01
            goto L_0x008c
        L_0x00d9:
            int r10 = X.AnonymousClass1Y3.B64
            X.0UN r5 = r6.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r11, r10, r5)
            X.260 r5 = (X.AnonymousClass260) r5
            X.0Xj r5 = r5.A00
            java.lang.String r13 = r9.A0D
            java.lang.String r12 = "funnel_analytics"
            r11 = 0
            X.C05030Xj.A01(r5)
            X.0Xm r10 = r5.A01
            int r16 = r10.A01(r12, r13, r11, r8)
            r10 = -1
            r5 = r16
            if (r5 != r10) goto L_0x00fc
            int r16 = X.AnonymousClass7YU.A00(r9)
        L_0x00fc:
            if (r16 > 0) goto L_0x0072
            r16 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x0072
        L_0x0103:
            r3 = 0
            goto L_0x0068
        L_0x0107:
            r3 = 0
            java.lang.String r17 = "funnel_logger_table"
            java.lang.String[] r18 = X.C195649Hq.A04     // Catch:{ RuntimeException -> 0x0120 }
            java.lang.String r19 = X.C195649Hq.A01     // Catch:{ RuntimeException -> 0x0120 }
            java.lang.String[] r20 = new java.lang.String[]{r7}     // Catch:{ RuntimeException -> 0x0120 }
            r21 = 0
            r22 = 0
            r23 = 0
            android.database.Cursor r3 = r16.query(r17, r18, r19, r20, r21, r22, r23)     // Catch:{ RuntimeException -> 0x0120 }
            X.C195649Hq.A01(r3, r2)     // Catch:{ RuntimeException -> 0x0120 }
            goto L_0x0128
        L_0x0120:
            r8 = move-exception
            java.lang.String r5 = X.C195649Hq.A03     // Catch:{ all -> 0x0254 }
            java.lang.String r4 = "Exception when query record db or iterate cursor. Funnel key: $s. Exception: %s"
            X.C010708t.A0N(r5, r4, r8)     // Catch:{ all -> 0x0254 }
        L_0x0128:
            if (r3 == 0) goto L_0x0056
            r3.close()
            goto L_0x0056
        L_0x012f:
            r2 = 0
            r0 = 0
            goto L_0x0137
        L_0x0134:
            r0 = r4
            r4 = r17
        L_0x0137:
            com.fasterxml.jackson.databind.node.ObjectNode r14 = new com.fasterxml.jackson.databind.node.ObjectNode
            com.fasterxml.jackson.databind.node.JsonNodeFactory r10 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance
            r14.<init>(r10)
            java.lang.String r12 = "name"
            java.lang.String r10 = "funnel_end"
            r14.put(r12, r10)
            java.lang.String r10 = "relative_time"
            r14.put(r10, r11)
            r17 = r25
            r10 = r17
            java.lang.String r11 = r10.mType
            java.lang.String r10 = "tag"
            r14.put(r10, r11)
            X.0nb r11 = new X.0nb
            java.lang.String r10 = "funnel_analytics"
            r11.<init>(r10)
            java.lang.String r10 = r9.A0D
            r11.A0D(r12, r10)
            java.lang.String r10 = "instance_id"
            r11.A0A(r10, r4)
            java.lang.String r4 = "start_time"
            r11.A0A(r4, r2)
            java.lang.String r2 = "sampling_rate"
            r5 = r16
            r11.A09(r2, r5)
            java.lang.String r2 = "funnel_uid"
            r11.A0A(r2, r0)
            boolean r1 = r9.A08
            java.lang.String r0 = "pseudo_end"
            r11.A0E(r0, r1)
            int r1 = r9.A00
            java.lang.String r0 = "version"
            r11.A09(r0, r1)
            boolean r1 = r9.A0C
            java.lang.String r0 = "high_priority_channel"
            r11.A0E(r0, r1)
            boolean r0 = r9.A0C
            if (r0 == 0) goto L_0x019b
            java.lang.String r2 = "upload_this_event_now"
            java.lang.String r1 = "true"
            java.util.Map r0 = X.C11670nb.A00(r11, r8)
            r0.put(r2, r1)
        L_0x019b:
            com.fasterxml.jackson.databind.node.ArrayNode r2 = new com.fasterxml.jackson.databind.node.ArrayNode
            com.fasterxml.jackson.databind.node.JsonNodeFactory r0 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance
            r2.<init>(r0)
            java.util.Iterator r1 = r20.iterator()
        L_0x01a6:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x01b6
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            r2.add(r0)
            goto L_0x01a6
        L_0x01b6:
            java.lang.String r0 = "tags"
            r11.A0B(r0, r2)
            if (r13 == 0) goto L_0x01c2
            java.lang.String r0 = "payload"
            r11.A0D(r0, r13)
        L_0x01c2:
            com.fasterxml.jackson.databind.node.ArrayNode r2 = new com.fasterxml.jackson.databind.node.ArrayNode
            com.fasterxml.jackson.databind.node.JsonNodeFactory r0 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance
            r2.<init>(r0)
            java.util.Iterator r1 = r21.iterator()
        L_0x01cd:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x01dd
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            r2.add(r0)
            goto L_0x01cd
        L_0x01dd:
            java.lang.String r0 = r14.toString()
            r2.add(r0)
            java.lang.String r0 = "actions"
            r11.A0B(r0, r2)
            java.lang.String r1 = "db_persist"
            r11.A0E(r1, r8)
            if (r28 != 0) goto L_0x01fd
            int r1 = X.AnonymousClass1Y3.AcI
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r15, r1, r0)
            X.9Ja r0 = (X.C195929Ja) r0
            r0.A02(r7)
        L_0x01fd:
            X.8cg r1 = X.C182058cg.A04
            r2 = 4
            r0 = r17
            if (r0 == r1) goto L_0x0211
            int r1 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.261 r0 = (X.AnonymousClass261) r0
            r0.A04(r9)
        L_0x0211:
            int r1 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.261 r0 = (X.AnonymousClass261) r0
            r0.A02(r11, r9)
            int r1 = X.AnonymousClass1Y3.B5y
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.EYf r1 = (X.C29381EYf) r1
            X.EYb r0 = X.C29377EYb.A00
            if (r0 != 0) goto L_0x0233
            X.EYb r0 = new X.EYb
            r0.<init>(r1)
            X.C29377EYb.A00 = r0
        L_0x0233:
            X.EYb r0 = X.C29377EYb.A00
            r0.A01(r11)
            int r3 = r9.A02
            r2 = -1
            r0 = 0
            if (r3 == r2) goto L_0x023f
            r0 = 1
        L_0x023f:
            if (r0 == 0) goto L_0x0253
            r2 = 8
            int r1 = X.AnonymousClass1Y3.BBd
            X.0UN r0 = r6.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2
            short r1 = r9.A0E
            r0 = 2
            r2.markerEnd(r3, r1, r0)
        L_0x0253:
            return
        L_0x0254:
            r0 = move-exception
            if (r3 == 0) goto L_0x025a
            r3.close()
        L_0x025a:
            throw r0
        L_0x025b:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Funnel key is null, expecting non-null value"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.funnellogger.FunnelLoggerImpl.A0A(com.facebook.funnellogger.FunnelLoggerImpl, java.lang.String, X.0gA, X.8cg, long, boolean, java.util.List):void");
    }
}
