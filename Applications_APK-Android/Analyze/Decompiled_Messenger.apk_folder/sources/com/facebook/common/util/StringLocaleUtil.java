package com.facebook.common.util;

import java.util.Locale;

public class StringLocaleUtil {
    public static final String toLowerCaseLocaleSafe(String str) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase(Locale.US);
    }

    public static final String toUpperCaseLocaleSafe(String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase(Locale.US);
    }

    public static final String A00(String str, Object... objArr) {
        return String.format(Locale.getDefault(), str, objArr);
    }
}
