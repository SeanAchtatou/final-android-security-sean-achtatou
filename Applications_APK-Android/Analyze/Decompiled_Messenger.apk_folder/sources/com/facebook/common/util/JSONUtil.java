package com.facebook.common.util;

import X.AnonymousClass080;
import X.AnonymousClass0jJ;
import X.AnonymousClass14O;
import X.C010708t;
import X.C04300To;
import X.C185013u;
import X.C28941fc;
import X.C28951fd;
import X.C28961fe;
import X.C28971ff;
import X.C28981fg;
import X.C37571vt;
import android.net.Uri;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;

public final class JSONUtil {
    public static float A02(JsonNode jsonNode) {
        return A03(jsonNode, 0.0f);
    }

    public static int A04(JsonNode jsonNode) {
        return A05(jsonNode, 0);
    }

    public static JsonNode A0C(Object obj) {
        return A0D(obj, false);
    }

    public static String A0N(JsonNode jsonNode) {
        return A0Q(jsonNode, null);
    }

    public static boolean A0S(JsonNode jsonNode) {
        return A0U(jsonNode, false);
    }

    public static double A00(JsonNode jsonNode) {
        return A01(jsonNode, 0.0d);
    }

    private static double A01(JsonNode jsonNode, double d) {
        if (jsonNode != null && !jsonNode.isNull()) {
            if (jsonNode.isTextual()) {
                try {
                    return Double.parseDouble(jsonNode.textValue());
                } catch (NumberFormatException unused) {
                    return d;
                }
            } else if (jsonNode.isNumber()) {
                return jsonNode.doubleValue();
            }
        }
        return d;
    }

    public static float A03(JsonNode jsonNode, float f) {
        if (jsonNode != null && !jsonNode.isNull()) {
            if (jsonNode.isTextual()) {
                try {
                    return Float.parseFloat(jsonNode.textValue());
                } catch (NumberFormatException unused) {
                    return f;
                }
            } else if (jsonNode.isNumber()) {
                return jsonNode.numberValue().floatValue();
            }
        }
        return f;
    }

    public static int A05(JsonNode jsonNode, int i) {
        if (jsonNode != null && !jsonNode.isNull()) {
            if (jsonNode.isTextual()) {
                try {
                    return Integer.parseInt(jsonNode.textValue());
                } catch (NumberFormatException unused) {
                    return i;
                }
            } else if (jsonNode.isNumber()) {
                return jsonNode.intValue();
            }
        }
        return i;
    }

    public static long A06(JsonNode jsonNode) {
        return A07(jsonNode, 0);
    }

    public static long A07(JsonNode jsonNode, long j) {
        if (jsonNode != null && !jsonNode.isNull()) {
            if (jsonNode.isTextual()) {
                try {
                    return Long.parseLong(jsonNode.textValue());
                } catch (NumberFormatException unused) {
                    return j;
                }
            } else if (jsonNode.isNumber()) {
                return jsonNode.longValue();
            }
        }
        return j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0038, code lost:
        if ("true".equals(r1) == false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004a, code lost:
        if (r3.intValue() != 0) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.common.util.TriState A09(com.fasterxml.jackson.databind.JsonNode r3) {
        /*
            if (r3 == 0) goto L_0x004d
            boolean r0 = r3.isNull()
            if (r0 != 0) goto L_0x004d
            boolean r0 = r3.isBoolean()
            if (r0 == 0) goto L_0x0017
            boolean r0 = r3.booleanValue()
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)
            return r0
        L_0x0017:
            boolean r0 = r3.isTextual()
            r2 = 0
            if (r0 == 0) goto L_0x0040
            java.lang.String r1 = r3.textValue()
            java.lang.String r0 = "on"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x003a
            java.lang.String r0 = "1"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x003a
            java.lang.String r0 = "true"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x003b
        L_0x003a:
            r2 = 1
        L_0x003b:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r2)
            return r0
        L_0x0040:
            boolean r0 = r3.isNumber()
            if (r0 == 0) goto L_0x004d
            int r0 = r3.intValue()
            if (r0 == 0) goto L_0x003b
            goto L_0x003a
        L_0x004d:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.util.JSONUtil.A09(com.fasterxml.jackson.databind.JsonNode):com.facebook.common.util.TriState");
    }

    private static JsonNode A0D(Object obj, boolean z) {
        if (obj == null) {
            return NullNode.instance;
        }
        if (obj instanceof CharSequence) {
            return new TextNode(obj.toString());
        }
        if (obj instanceof Boolean) {
            if (((Boolean) obj).booleanValue()) {
                return BooleanNode.TRUE;
            }
            return BooleanNode.FALSE;
        } else if (obj instanceof Float) {
            return new C28941fc(((Float) obj).floatValue());
        } else {
            if (obj instanceof Double) {
                return new DoubleNode(((Double) obj).doubleValue());
            }
            if (obj instanceof Short) {
                return new C28951fd(((Short) obj).shortValue());
            }
            if (obj instanceof Integer) {
                int intValue = ((Integer) obj).intValue();
                if (intValue > 10 || intValue < -1) {
                    return new AnonymousClass14O(intValue);
                }
                return AnonymousClass14O.CANONICALS[intValue - -1];
            } else if (obj instanceof Long) {
                return new LongNode(((Long) obj).longValue());
            } else {
                if (obj instanceof BigDecimal) {
                    return new C28961fe((BigDecimal) obj);
                }
                if (obj instanceof BigInteger) {
                    return new C28971ff((BigInteger) obj);
                }
                if (obj instanceof Map) {
                    ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        objectNode.put(entry.getKey().toString(), A0D(entry.getValue(), z));
                    }
                    return objectNode;
                } else if (obj instanceof Iterable) {
                    ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
                    for (Object A0D : (Iterable) obj) {
                        arrayNode.add(A0D(A0D, z));
                    }
                    return arrayNode;
                } else if (obj instanceof Object[]) {
                    ArrayNode arrayNode2 = new ArrayNode(JsonNodeFactory.instance);
                    for (Object A0D2 : (Object[]) obj) {
                        arrayNode2.add(A0D(A0D2, z));
                    }
                    return arrayNode2;
                } else {
                    Class<?> cls = obj.getClass();
                    boolean z2 = false;
                    if (cls.getAnnotation(JsonSerialize.class) != null) {
                        z2 = true;
                    }
                    if (z2) {
                        return new C28981fg(obj);
                    }
                    if (z) {
                        return A0D(new AnonymousClass080(obj), z);
                    }
                    throw new IllegalArgumentException("Can't convert to json: " + obj + ", of type: " + cls);
                }
            }
        }
    }

    public static ArrayNode A0E(JsonNode jsonNode, String str) {
        return (ArrayNode) A0F(jsonNode, str, ArrayNode.class);
    }

    private static C185013u A0G(Class cls) {
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        if (ArrayNode.class.equals(cls)) {
            return new ArrayNode(jsonNodeFactory);
        }
        if (ObjectNode.class.equals(cls)) {
            return new ObjectNode(jsonNodeFactory);
        }
        throw new IllegalArgumentException("Unsupported node type: " + cls);
    }

    public static ObjectNode A0H(JsonNode jsonNode, String str) {
        return (ObjectNode) A0F(jsonNode, str, ObjectNode.class);
    }

    public static ObjectNode A0I(Map map) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                objectNode.put((String) entry.getKey(), (String) entry.getValue());
            }
        }
        return objectNode;
    }

    public static ImmutableMap A0L(ObjectNode objectNode, AnonymousClass0jJ r7) {
        HashMap hashMap = new HashMap(objectNode.size());
        Iterator fieldNames = objectNode.fieldNames();
        while (fieldNames.hasNext()) {
            String str = (String) fieldNames.next();
            JsonNode jsonNode = objectNode.get(str);
            if (!jsonNode.isArray()) {
                hashMap.put(str, jsonNode.asText());
            } else {
                try {
                    hashMap.put(str, r7.writeValueAsString(jsonNode));
                } catch (C37571vt e) {
                    hashMap.put(str, BuildConfig.FLAVOR);
                    C010708t.A0L("com.facebook.common.util.JSONUtil", "Failed to parse json list", e);
                }
            }
        }
        return ImmutableMap.copyOf(hashMap);
    }

    public static Iterable A0M(JsonNode jsonNode, String str) {
        return (Iterable) MoreObjects.firstNonNull(A0B(jsonNode, str, ArrayNode.class), RegularImmutableList.A02);
    }

    public static String A0Q(JsonNode jsonNode, String str) {
        if (jsonNode != null && !jsonNode.isNull()) {
            if (jsonNode.isTextual()) {
                return jsonNode.textValue();
            }
            if (jsonNode.isNumber()) {
                return jsonNode.numberValue().toString();
            }
        }
        return str;
    }

    public static boolean A0T(JsonNode jsonNode) {
        if (jsonNode == null || jsonNode.isNull()) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0U(com.fasterxml.jackson.databind.JsonNode r3, boolean r4) {
        /*
            if (r3 == 0) goto L_0x0045
            boolean r0 = r3.isNull()
            if (r0 != 0) goto L_0x0045
            boolean r0 = r3.isBoolean()
            if (r0 == 0) goto L_0x0013
            boolean r0 = r3.booleanValue()
            return r0
        L_0x0013:
            boolean r0 = r3.isTextual()
            r2 = 0
            if (r0 == 0) goto L_0x0038
            java.lang.String r1 = r3.textValue()
            java.lang.String r0 = "on"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0036
            java.lang.String r0 = "1"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0036
            java.lang.String r0 = "true"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0037
        L_0x0036:
            r2 = 1
        L_0x0037:
            return r2
        L_0x0038:
            boolean r0 = r3.isNumber()
            if (r0 == 0) goto L_0x0045
            int r0 = r3.intValue()
            if (r0 == 0) goto L_0x0037
            goto L_0x0036
        L_0x0045:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.util.JSONUtil.A0U(com.fasterxml.jackson.databind.JsonNode, boolean):boolean");
    }

    public static Uri A08(JsonNode jsonNode, String str) {
        return Uri.parse(A0O(jsonNode, str));
    }

    private static JsonNode A0A(JsonNode jsonNode, String str) {
        JsonNode jsonNode2 = jsonNode.get(str);
        if (jsonNode2 != null) {
            return jsonNode2;
        }
        throw new NullPointerException(Preconditions.format("No key %s in %s", str, jsonNode));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (r6.isInstance(r3) != false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.fasterxml.jackson.databind.JsonNode A0B(com.fasterxml.jackson.databind.JsonNode r4, java.lang.String r5, java.lang.Class r6) {
        /*
            com.fasterxml.jackson.databind.JsonNode r3 = r4.get(r5)
            if (r3 == 0) goto L_0x000d
            boolean r0 = r6.isInstance(r3)
            r1 = 0
            if (r0 == 0) goto L_0x000e
        L_0x000d:
            r1 = 1
        L_0x000e:
            java.lang.String r0 = r6.getSimpleName()
            java.lang.String r2 = "Node %s in not an %s in %s"
            if (r1 == 0) goto L_0x001d
            java.lang.Object r0 = r6.cast(r3)
            com.fasterxml.jackson.databind.JsonNode r0 = (com.fasterxml.jackson.databind.JsonNode) r0
            return r0
        L_0x001d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.Object[] r0 = new java.lang.Object[]{r5, r0, r4}
            java.lang.String r0 = com.google.common.base.Preconditions.format(r2, r0)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.util.JSONUtil.A0B(com.fasterxml.jackson.databind.JsonNode, java.lang.String, java.lang.Class):com.fasterxml.jackson.databind.JsonNode");
    }

    private static C185013u A0F(JsonNode jsonNode, String str, Class cls) {
        C185013u r0 = (C185013u) A0B(jsonNode, str, cls);
        if (r0 == null) {
            return A0G(cls);
        }
        return r0;
    }

    public static ImmutableList A0J(JsonNode jsonNode, String str) {
        ArrayNode A0E = A0E(jsonNode, str);
        ImmutableList.Builder builder = new ImmutableList.Builder();
        Iterator it = A0E.iterator();
        while (it.hasNext()) {
            builder.add((Object) A0N((JsonNode) it.next()));
        }
        return builder.build();
    }

    public static ImmutableList A0K(JSONArray jSONArray) {
        return ImmutableList.copyOf((Collection) A0R(jSONArray));
    }

    public static String A0O(JsonNode jsonNode, String str) {
        return A0Q(jsonNode.get(str), BuildConfig.FLAVOR);
    }

    public static String A0P(JsonNode jsonNode, String str) {
        return A0A(jsonNode, str).asText();
    }

    public static ArrayList A0R(JSONArray jSONArray) {
        ArrayList A01 = C04300To.A01(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            A01.add(jSONArray.getString(i));
        }
        return A01;
    }
}
