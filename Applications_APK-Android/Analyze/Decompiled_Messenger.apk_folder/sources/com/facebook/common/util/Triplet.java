package com.facebook.common.util;

import X.C02170Df;
import android.os.Parcel;
import android.os.Parcelable;

public final class Triplet extends ParcelablePair implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C02170Df();
    public final Object A00;

    public Object[] A00() {
        return new Object[]{this.first, this.second, this.A00};
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeValue(this.A00);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Triplet(android.os.Parcel r5) {
        /*
            r4 = this;
            java.lang.Class<X.Euf> r3 = X.C30355Euf.class
            java.lang.ClassLoader r0 = r3.getClassLoader()
            java.lang.Object r2 = r5.readValue(r0)
            java.lang.ClassLoader r0 = r3.getClassLoader()
            java.lang.Object r1 = r5.readValue(r0)
            java.lang.ClassLoader r0 = r3.getClassLoader()
            java.lang.Object r0 = r5.readValue(r0)
            r4.<init>(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.util.Triplet.<init>(android.os.Parcel):void");
    }

    public Triplet(Object obj, Object obj2, Object obj3) {
        super(obj, obj2);
        this.A00 = obj3;
    }
}
