package com.facebook.common.util;

import X.AnonymousClass08S;
import X.C02180Dg;
import X.C24931Xr;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import java.util.Arrays;
import java.util.Iterator;

public class ParcelablePair extends Pair implements Iterable, Parcelable {
    public static final Parcelable.Creator CREATOR = new C02180Dg();

    public int describeContents() {
        return 0;
    }

    public Object[] A00() {
        return new Object[]{this.first, this.second};
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ParcelablePair)) {
            return false;
        }
        return Arrays.equals(A00(), ((ParcelablePair) obj).A00());
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.first);
        parcel.writeValue(this.second);
    }

    public int hashCode() {
        return Arrays.hashCode(A00());
    }

    public Iterator iterator() {
        Object[] A00 = A00();
        return C24931Xr.A06(A00, 0, A00.length, 0);
    }

    public String toString() {
        return AnonymousClass08S.A0J(getClass().getSimpleName(), Arrays.toString(A00()));
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ParcelablePair(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.Class<X.Euf> r2 = X.C30355Euf.class
            java.lang.ClassLoader r0 = r2.getClassLoader()
            java.lang.Object r1 = r4.readValue(r0)
            java.lang.ClassLoader r0 = r2.getClassLoader()
            java.lang.Object r0 = r4.readValue(r0)
            r3.<init>(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.util.ParcelablePair.<init>(android.os.Parcel):void");
    }

    public ParcelablePair(Object obj, Object obj2) {
        super(obj, obj2);
    }
}
