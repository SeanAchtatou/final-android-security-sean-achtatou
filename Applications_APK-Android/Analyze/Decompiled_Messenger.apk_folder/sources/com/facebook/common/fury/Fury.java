package com.facebook.common.fury;

import java.util.concurrent.atomic.AtomicBoolean;

public final class Fury {
    public static final AtomicBoolean A00 = new AtomicBoolean();
    public static volatile long A01;
    public static volatile boolean A02;
}
