package com.facebook.common.dextricks;

import X.AnonymousClass01q;
import android.content.Context;
import com.facebook.common.dextricks.MultiDexClassLoader;
import com.facebook.common.dextricks.stats.ClassLoadingStats;
import com.facebook.common.dextricks.stats.ClassLoadingStatsNative;
import dalvik.system.DexFile;
import java.util.ArrayList;

public final class MultiDexClassLoaderArtNative extends MultiDexClassLoader {
    private static final int CRASH_CORRELATION_BAIL_THRESHOLD = 3;
    public static final String FAILEDSTART_FILE = "multidexclassloader_artnative_failedstart";
    private final ArrayList mAuxDexes;
    private final ArrayList mPrimaryDexes;

    private native void cleanupFailedInit();

    private native Class init(ClassLoader classLoader, ClassLoader classLoader2, DexFile[] dexFileArr, ClassLoadingStatsNative classLoadingStatsNative, String str, boolean z);

    public static native boolean isFastHooked();

    private static native void nativeConfigure(DexFile[] dexFileArr, int i);

    public native DexFile[] doGetConfiguredDexFiles();

    public native Class findClass(String str);

    public native String[] getRecentFailedClasses();

    public native Class loadClass(String str, boolean z);

    public void onColdstartDone() {
    }

    public String toString() {
        return "MultiDexClassLoaderArtNative";
    }

    static {
        AnonymousClass01q.A08("dextricks");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void maybeBail(android.content.Context r2) {
        /*
            java.lang.String r0 = "multidexclassloader_artnative_failedstart"
            java.io.File r1 = r2.getFileStreamPath(r0)
            boolean r0 = r1.exists()
            if (r0 != 0) goto L_0x0041
            r1.createNewFile()
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x002e
            java.lang.String[] r1 = android.os.Build.SUPPORTED_32_BIT_ABIS
            int r0 = r1.length
            if (r0 == 0) goto L_0x002e
            r0 = 0
            r1 = r1[r0]
        L_0x001d:
            java.lang.String r0 = "x86"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0039
            java.lang.String r0 = "multidexclassloader_artnative_modelspecific_blacklisted"
            boolean r0 = X.AnonymousClass08X.A07(r2, r0)
            if (r0 != 0) goto L_0x0031
            return
        L_0x002e:
            java.lang.String r1 = android.os.Build.CPU_ABI
            goto L_0x001d
        L_0x0031:
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r0 = "Bailing: Blacklisted device model"
            r1.<init>(r0)
            throw r1
        L_0x0039:
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r0 = "Bailing: x86 unsupported"
            r1.<init>(r0)
            throw r1
        L_0x0041:
            r1.delete()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r0 = "MDCLAN bailing: Failed to start last time"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.MultiDexClassLoaderArtNative.maybeBail(android.content.Context):void");
    }

    public MultiDexClassLoaderArtNative(Context context, ClassLoader classLoader, ArrayList arrayList, ArrayList arrayList2, boolean z, boolean z2) {
        if (!z2) {
            maybeBail(context);
        }
        this.mPrimaryDexes = arrayList;
        this.mAuxDexes = arrayList2;
        if (arrayList == null || arrayList.isEmpty()) {
            throw new Exception("Bailing: No primary dexes");
        }
        ClassLoadingStatsNative classLoadingStatsNative = new ClassLoadingStatsNative();
        ClassLoadingStats.A00.getAndSet(classLoadingStatsNative);
        try {
            ClassLoader classLoader2 = this.mPutativeLoader;
            ArrayList arrayList3 = this.mPrimaryDexes;
            init(classLoader, classLoader2, (DexFile[]) arrayList3.toArray(new DexFile[arrayList3.size()]), classLoadingStatsNative, context.getDir("mdclan", 0).getAbsolutePath(), z);
        } catch (Exception e) {
            cleanupFailedInit();
            throw e;
        }
    }

    public void configure(MultiDexClassLoader.Configuration configuration) {
        super.configure(configuration);
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.mPrimaryDexes);
        arrayList.addAll(configuration.mDexFiles);
        arrayList.addAll(this.mAuxDexes);
        nativeConfigure((DexFile[]) arrayList.toArray(new DexFile[arrayList.size()]), configuration.configFlags);
        configureArtHacks(configuration);
    }
}
