package com.facebook.common.dextricks.classtracing.logger;

import X.AnonymousClass09A;
import X.C001000r;
import com.facebook.common.dextricks.classid.ClassId;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ClassTracingLoggerLite {
    public static final ConcurrentLinkedQueue A00 = new ConcurrentLinkedQueue();
    public static volatile boolean A01;
    public static volatile boolean A02;

    static {
        C001000r.A00(new AnonymousClass09A());
    }

    public static void classLoaded(Class cls) {
        if (A01 && ClassId.sInitialized) {
            A00.add(Long.valueOf(ClassId.getClassId(cls)));
        }
    }

    public static void classNotFound() {
        if (A01 && ClassId.sInitialized) {
            A00.add(-1L);
        }
    }

    public static void beginClassLoad(String str) {
    }
}
