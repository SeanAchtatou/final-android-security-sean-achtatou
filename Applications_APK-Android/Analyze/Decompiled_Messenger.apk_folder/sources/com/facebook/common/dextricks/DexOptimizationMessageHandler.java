package com.facebook.common.dextricks;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.Process;
import android.os.RemoteException;
import java.util.List;

public final class DexOptimizationMessageHandler extends Handler {
    private static final int DEFAULT_RESTART_IMPORTANCE_THRESHOLD = 300;
    public static final int MSG_OPT_DONE = 2;
    public static final int MSG_OPT_RESTART_CHECK = 3;
    public static final int MSG_OPT_START = 1;
    public static final int OPT_RESULT_ERROR = 1;
    public static final int OPT_RESULT_SHUTDOWN = 2;
    public static final int OPT_RESULT_SUCCESS = 0;
    private static final int RESTART_CHECK_INTERVAL_MS = 1000;
    private static final int UNKNOWN_IMPORTANCE = -1;
    public final Context mContext;
    public final boolean mDisableProcessRestart;
    private final PowerManager mPowerManager;
    private final int mRestartImportanceThreshold;
    private final boolean mRestartOnlyIfScreenOff;

    public class Api16PlusUtil {
        public static int getMyImportance() {
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            return runningAppProcessInfo.importance;
        }

        private Api16PlusUtil() {
        }
    }

    public static void send(Messenger messenger, int i, int i2) {
        if (messenger != null) {
            try {
                messenger.send(Message.obtain(null, i, i2, 0));
            } catch (RemoteException e) {
                Mlog.w(e, "cannot send status, receiver is dead", new Object[0]);
            }
        } else {
            Mlog.w("cannot send status, receiver is null", new Object[0]);
        }
    }

    private boolean checkIfShouldRestartProcess(int i) {
        String str;
        if (!this.mDisableProcessRestart && i >= this.mRestartImportanceThreshold) {
            if (this.mRestartOnlyIfScreenOff) {
                boolean isScreenOn = isScreenOn();
                if (isScreenOn) {
                    str = "on";
                } else {
                    str = "off";
                }
                Mlog.safeFmt("[c] checking if screen is off (screen is %s) with importance %d [threshold: %d]", str, Integer.valueOf(i), Integer.valueOf(this.mRestartImportanceThreshold));
                if (isScreenOn) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private int getMyImportance() {
        if (Build.VERSION.SDK_INT >= 16) {
            try {
                return Api16PlusUtil.getMyImportance();
            } catch (NullPointerException e) {
                Mlog.w(e, "getMyMemoryState failed due to NPE: falling back to legacy process list API", new Object[0]);
            } catch (RuntimeException e2) {
                Mlog.w(e2, "getMyMemoryState failed due to internal error: exit gracefully", new Object[0]);
                return -1;
            }
        }
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.mContext.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            int myPid = Process.myPid();
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == myPid) {
                    return next.importance;
                }
            }
        }
        return -1;
    }

    private boolean isScreenOn() {
        if (Build.VERSION.SDK_INT >= 20) {
            return this.mPowerManager.isInteractive();
        }
        return this.mPowerManager.isScreenOn();
    }

    private static Looper makeLooper() {
        HandlerThread handlerThread = new HandlerThread("DexOptimizationMessageHandlerThread");
        handlerThread.start();
        return handlerThread.getLooper();
    }

    private boolean scheduleRestartChecks() {
        return !this.mDisableProcessRestart;
    }

    public void handleMessage(Message message) {
        String str;
        int i = message.what;
        if (i != 1) {
            if (i == 2) {
                int i2 = message.arg1;
                if (i2 == 0) {
                    str = OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS;
                } else if (i2 == 1) {
                    str = "failed";
                } else if (i2 != 2) {
                    str = "unknown";
                } else {
                    str = "interrupted due to service shutdown";
                }
                boolean z = !this.mDisableProcessRestart;
                Mlog.safeFmt("[c] received optimization-done message (result: %s schedule checks: %s)", str, Boolean.valueOf(z));
                if (message.arg1 != 0 || !z) {
                    return;
                }
            } else if (i != 3) {
                super.handleMessage(message);
                return;
            } else {
                int myImportance = getMyImportance();
                Integer valueOf = Integer.valueOf(myImportance);
                Mlog.safeFmt("[c] current importance: %s threshold: %s", valueOf, Integer.valueOf(this.mRestartImportanceThreshold));
                if (checkIfShouldRestartProcess(myImportance)) {
                    Mlog.w("[c] low importance: %s: restarting ourselves", valueOf);
                    Process.sendSignal(Process.myPid(), 9);
                    throw new AssertionError("somehow survived SIGKILL");
                } else if (myImportance == -1) {
                    Mlog.w("[c] importance unknown: not scheduling further checks", new Object[0]);
                    return;
                } else if (!(!this.mDisableProcessRestart)) {
                    Mlog.safeFmt("[c] Do not schedule any further restart checks", new Object[0]);
                    return;
                } else {
                    Mlog.safeFmt("[c] scheduling another importance check in %sms", 1000);
                }
            }
            sendMessageDelayed(obtainMessage(3), 1000);
            return;
        }
        Mlog.safeFmt("[c] received opt start message, canceling any restart checks", new Object[0]);
        removeMessages(3);
    }

    public DexOptimizationMessageHandler(Context context, boolean z, int i, boolean z2) {
        super(makeLooper());
        this.mContext = context.getApplicationContext();
        this.mPowerManager = (PowerManager) context.getSystemService("power");
        this.mDisableProcessRestart = z;
        this.mRestartImportanceThreshold = i <= 0 ? DEFAULT_RESTART_IMPORTANCE_THRESHOLD : i;
        this.mRestartOnlyIfScreenOff = z2;
    }
}
