package com.facebook.common.dextricks;

import X.AnonymousClass08S;
import X.AnonymousClass0Me;
import com.facebook.forker.Process;
import com.facebook.forker.ProcessBuilder;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class DexOptRunner {
    private final ProcessBuilder mTemplate;
    private final File mTmpDir;
    private File mTmpFbDexOpt = null;

    public class DexOptException extends RuntimeException {
        public final String errout;
        public final int status;

        public DexOptException(int i, String str) {
            super(AnonymousClass08S.A0T("dexopt failed with status ", Process.describeStatus(i), ": [", str, "]"));
            this.status = i;
            this.errout = str;
        }
    }

    public void addDexOptOptions(ProcessBuilder processBuilder) {
    }

    public boolean attemptAllocate(int i, long j) {
        return DalvikInternals.attemptAllocate(i, j, -1);
    }

    public void cleanup() {
        if (this.mTmpFbDexOpt != null) {
            try {
                Mlog.safeFmt("Cleaning up temporary fbdexopt", new Object[0]);
                Fs.deleteRecursive(this.mTmpFbDexOpt);
            } catch (IOException unused) {
                Mlog.w("Unable to delete temporary fbdexopt", new Object[0]);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00f4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00f5, code lost:
        if (r8 != null) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fa, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run(java.io.InputStream r20, int r21, java.lang.String r22, java.io.RandomAccessFile r23, java.lang.String r24, java.lang.String[] r25) {
        /*
            r19 = this;
            r5 = r23
            long r1 = r5.getFilePointer()
            r3 = 0
            r9 = 1
            r7 = 0
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            r2 = 0
            if (r0 != 0) goto L_0x0010
            r2 = 1
        L_0x0010:
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.String r0 = "odex fpos must be 0"
            com.facebook.common.dextricks.Mlog.assertThat(r2, r0, r1)
            long r1 = r5.length()
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            r2 = 0
            if (r0 != 0) goto L_0x0021
            r2 = 1
        L_0x0021:
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.String r0 = "odex must be empty"
            com.facebook.common.dextricks.Mlog.assertThat(r2, r0, r1)
            java.io.FileDescriptor r0 = r5.getFD()
            int r2 = com.facebook.forker.Fd.fileno(r0)
            com.facebook.common.dextricks.DalvikInternals.dexOptCreateEmptyHeader(r2)
            long r0 = r5.getFilePointer()
            int r6 = (int) r0
            r8 = r21
            if (r8 <= r9) goto L_0x0056
            long r0 = r5.getFilePointer()
            long r3 = (long) r8
            long r0 = r0 + r3
            r3 = -1
            boolean r0 = com.facebook.common.dextricks.DalvikInternals.attemptAllocate(r2, r0, r3)
            if (r0 == 0) goto L_0x007a
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "allocated more %s bytes for dex content"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
        L_0x0056:
            r0 = r20
            r3 = r19
            int r5 = r3.copyDexToOdex(r0, r8, r5)
            r0 = 2147483647(0x7fffffff, float:NaN)
            r11 = r22
            if (r5 == r0) goto L_0x00fb
            r10 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            java.lang.Object[] r1 = new java.lang.Object[]{r0, r11}
            java.lang.String r0 = "wrote %s bytes to dex %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            java.io.File r0 = r3.mTmpDir
            java.io.RandomAccessFile r8 = com.facebook.common.dextricks.Fs.openUnlinkedTemporaryFile(r0)
            goto L_0x0082
        L_0x007a:
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.String r0 = "unable to preallocate on this system"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            goto L_0x0056
        L_0x0082:
            com.facebook.forker.ProcessBuilder r0 = r3.mTemplate     // Catch:{ all -> 0x00f2 }
            com.facebook.forker.ProcessBuilder r4 = r0.clone()     // Catch:{ all -> 0x00f2 }
            java.io.FileDescriptor r0 = r8.getFD()     // Catch:{ all -> 0x00f2 }
            int r0 = com.facebook.forker.Fd.fileno(r0)     // Catch:{ all -> 0x00f2 }
            int[] r1 = r4.mStreamDispositions     // Catch:{ all -> 0x00f2 }
            r1[r9] = r0     // Catch:{ all -> 0x00f2 }
            r0 = -5
            r1[r10] = r0     // Catch:{ all -> 0x00f2 }
            r4.setFdCloseOnExec(r2, r7)     // Catch:{ all -> 0x00f2 }
            r3.addDexOptOptions(r4)     // Catch:{ all -> 0x00f2 }
            java.lang.String r9 = "--"
            java.lang.String r10 = java.lang.Integer.toString(r2)     // Catch:{ all -> 0x00f2 }
            java.lang.String r12 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x00f2 }
            r13 = r12
            java.lang.String r0 = "BOOTCLASSPATH"
            java.lang.String r14 = java.lang.System.getenv(r0)     // Catch:{ all -> 0x00f2 }
            r1 = r25
            if (r25 != 0) goto L_0x00cc
            java.lang.String r15 = ""
        L_0x00b4:
            long r0 = (long) r6     // Catch:{ all -> 0x00f2 }
            java.lang.String r16 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x00f2 }
            long r0 = (long) r5     // Catch:{ all -> 0x00f2 }
            java.lang.String r17 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x00f2 }
            r18 = r24
            java.lang.String[] r0 = new java.lang.String[]{r9, r10, r11, r12, r13, r14, r15, r16, r17, r18}     // Catch:{ all -> 0x00f2 }
            r4.addArguments(r0)     // Catch:{ all -> 0x00f2 }
            com.facebook.forker.Process r1 = r3.startSubprocess(r4)     // Catch:{ all -> 0x00f2 }
            goto L_0x00d3
        L_0x00cc:
            java.lang.String r0 = ":"
            java.lang.String r15 = android.text.TextUtils.join(r0, r1)     // Catch:{ all -> 0x00f2 }
            goto L_0x00b4
        L_0x00d3:
            r3.waitForDexOpt(r1, r2)     // Catch:{ all -> 0x00ed }
            int r2 = r1.exitValueEx()     // Catch:{ all -> 0x00ed }
            r1.destroy()     // Catch:{ all -> 0x00f2 }
            if (r2 != 0) goto L_0x00e3
            r8.close()
            return
        L_0x00e3:
            com.facebook.common.dextricks.DexOptRunner$DexOptException r1 = new com.facebook.common.dextricks.DexOptRunner$DexOptException     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = com.facebook.common.dextricks.Fs.readProgramOutputFile(r8)     // Catch:{ all -> 0x00f2 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x00f2 }
            throw r1     // Catch:{ all -> 0x00f2 }
        L_0x00ed:
            r0 = move-exception
            r1.destroy()     // Catch:{ all -> 0x00f2 }
            throw r0     // Catch:{ all -> 0x00f2 }
        L_0x00f2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00f4 }
        L_0x00f4:
            r0 = move-exception
            if (r8 == 0) goto L_0x00fa
            r8.close()     // Catch:{ all -> 0x00fa }
        L_0x00fa:
            throw r0
        L_0x00fb:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "refusing to deal with impossibly huge dex file "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r11)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexOptRunner.run(java.io.InputStream, int, java.lang.String, java.io.RandomAccessFile, java.lang.String, java.lang.String[]):void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:13|14|15|16|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x003e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DexOptRunner(java.io.File r6) {
        /*
            r5 = this;
            r5.<init>()
            r2 = 0
            r5.mTmpFbDexOpt = r2
            r5.mTmpDir = r6
            java.lang.String r1 = "fbdexopt"
            java.io.File r4 = X.AnonymousClass01q.A01(r1)
            boolean r0 = r4.canExecute()
            if (r0 != 0) goto L_0x0046
            java.io.File r3 = java.io.File.createTempFile(r1, r2, r6)
            r5.mTmpFbDexOpt = r3
            java.io.FileOutputStream r2 = new java.io.FileOutputStream
            r2.<init>(r3)
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x003f }
            r1.<init>(r4)     // Catch:{ all -> 0x003f }
            r0 = 2147483647(0x7fffffff, float:NaN)
            X.AnonymousClass0Me.A09(r2, r1, r0)     // Catch:{ all -> 0x0038 }
            r2.flush()     // Catch:{ all -> 0x0038 }
            r1.close()     // Catch:{ all -> 0x003f }
            r2.close()
            r0 = 1
            r3.setExecutable(r0, r0)
            goto L_0x0047
        L_0x0038:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003a }
        L_0x003a:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x003e }
        L_0x003e:
            throw r0     // Catch:{ all -> 0x003f }
        L_0x003f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0041 }
        L_0x0041:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0045 }
        L_0x0045:
            throw r0
        L_0x0046:
            r3 = r4
        L_0x0047:
            com.facebook.forker.ProcessBuilder r2 = new com.facebook.forker.ProcessBuilder
            java.lang.String r1 = r3.getAbsolutePath()
            r0 = 0
            java.lang.String[] r0 = new java.lang.String[r0]
            r2.<init>(r1, r0)
            java.lang.String r1 = X.AnonymousClass01q.A03()
            java.lang.String r0 = "LD_LIBRARY_PATH"
            r2.setenv(r0, r1)
            r2.mTmpDir = r6
            r5.mTemplate = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexOptRunner.<init>(java.io.File):void");
    }

    public int copyDexToOdex(InputStream inputStream, int i, RandomAccessFile randomAccessFile) {
        byte[] bArr = new byte[DexStore.LOAD_RESULT_PGO];
        int i2 = 0;
        while (i2 < Integer.MAX_VALUE) {
            int A0A = AnonymousClass0Me.A0A(inputStream, bArr, Integer.MAX_VALUE - i2);
            if (A0A == -1) {
                break;
            }
            randomAccessFile.write(bArr, 0, A0A);
            i2 += A0A;
        }
        return i2;
    }

    public Process startSubprocess(ProcessBuilder processBuilder) {
        return processBuilder.create();
    }

    public void waitForDexOpt(Process process, int i) {
        process.waitForUninterruptibly();
    }
}
