package com.facebook.common.dextricks;

import X.AnonymousClass01q;
import X.AnonymousClass08S;
import X.AnonymousClass08X;
import X.AnonymousClass095;
import X.AnonymousClass1Y3;
import android.content.Context;
import android.os.Build;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.text.TextUtils;
import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.Dex2oatLogcatParser;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.common.dextricks.DexStore;
import com.facebook.common.dextricks.DexUnpacker;
import com.facebook.common.dextricks.MultiDexClassLoader;
import com.facebook.common.dextricks.OdexScheme;
import com.facebook.forker.Process;
import com.facebook.forker.ProcessBuilder;
import io.card.payment.BuildConfig;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.webrtc.audio.WebRtcAudioRecord;

public final class OdexSchemeArtXdex extends OdexSchemeArtTurbo {
    private static final boolean APPLY_MIRANDA_HACK = true;
    private static final String CREATED_BY_OATMEAL = "86827de6f1ef3407f8dc98b76382d3a6e0759ab3";
    public static final String DEX_MANIFEST_RESOURCE_PATH = "dex_manifest.txt";
    private static final String ENV_LD_PRELOAD = "LD_PRELOAD";
    private static final int MAX_OAT_OPTIMIZATION_ATTEMPTS = 3;
    public static final long MIN_DISK_FREE_FOR_MIXED_MODE = 419430400;
    private static final String MISSING_PGO_SOFT_ERROR_CATEGORY = "OdexSchemeArtXdex_MissingPGO";
    private static final String MIXED_MODE_DATA_RESOURCE_PATH = "mixed_mode.txt";
    private static final int OREO_SDK_INT = 26;
    private static final String QUICK_DATA_RESOURCE_PATH = "oatmeal.bin";
    private static final String REGENERATE_SOFT_ERROR_CATEGORY = "OdexSchemeArtXdex_REGEN";
    private static final long STATE_DEX2OAT_CLASSPATH_SET = 2048;
    private static final long STATE_DEX2OAT_QUICKENING_NEEDED = 64;
    private static final long STATE_DEX2OAT_QUICK_ATTEMPTED = 512;
    private static final long STATE_DO_PERIODIC_PGO_COMP_ATTEMPTED = 32768;
    private static final long STATE_DO_PERIODIC_PGO_COMP_FINISHED = 65536;
    private static final long STATE_DO_PERIODIC_PGO_COMP_NEEDED = 16384;
    private static final long STATE_MASK = 20720;
    private static final long STATE_MIXED_ATTEMPTED = 1024;
    private static final long STATE_MIXED_NEEDED = 128;
    private static final long STATE_OATMEAL_QUICKENING_NEEDED = 32;
    private static final long STATE_OATMEAL_QUICK_ATTEMPTED = 256;
    private static final long STATE_OPT_COMPLETED = 16;
    private static final long STATE_PGO_ATTEMPTED = 8192;
    private static final long STATE_PGO_NEEDED = 4096;
    private static final String TMP_DEX_MANIFEST_FILE = "art_dex_manifest";
    private static final String TMP_MIXED_MODE_DATA_FILE = "art_mixed_mode_data_input";
    private static final String TMP_QUICK_DATA_FILE = "art_quick_data_input";
    private final DexUnpacker mDexUnpacker;
    private final DexManifest.Dex[] mDexes;
    private boolean mEnableMemoryDex2OatHook;
    private boolean mEnableMemoryDex2OatHookInited = false;
    private final boolean mIsLoadable;
    private String mOatmealPath;
    private final AnonymousClass095 mPGOProfileUtil;
    private final ResProvider mResProvider;
    private final ArrayList oatFiles;

    public class CriticalCannotTruncateDexesSection implements Closeable {
        private final boolean mHasMarked;
        private final File mRoot;

        public void close() {
            if (this.mHasMarked) {
                OdexSchemeArtXdex.clearCannotTruncateDexesFlag(this.mRoot);
            }
        }

        public CriticalCannotTruncateDexesSection(OdexSchemeArtXdex odexSchemeArtXdex, File file) {
            this(file, true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x000e, code lost:
            if (com.facebook.common.dextricks.OdexSchemeArtXdex.markCannotTruncateDexesFlag(r4) == false) goto L_0x0010;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public CriticalCannotTruncateDexesSection(java.io.File r4, boolean r5) {
            /*
                r2 = this;
                com.facebook.common.dextricks.OdexSchemeArtXdex.this = r3
                r2.<init>()
                r2.mRoot = r4
                if (r5 == 0) goto L_0x0010
                boolean r1 = com.facebook.common.dextricks.OdexSchemeArtXdex.markCannotTruncateDexesFlag(r4)
                r0 = 1
                if (r1 != 0) goto L_0x0011
            L_0x0010:
                r0 = 0
            L_0x0011:
                r2.mHasMarked = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.CriticalCannotTruncateDexesSection.<init>(com.facebook.common.dextricks.OdexSchemeArtXdex, java.io.File, boolean):void");
        }
    }

    public class Dex2OatProgressListener implements DexStore.ExternalProcessProgressListener {
        private static final long CHECKPOINT_PERIOD = TimeUnit.MINUTES.toMillis(5);
        private static final String EVENT_NAME = "OptsvcEvent";
        private final Context mAppContext;
        private final int mAttemptNumber;
        private Integer mExitStatus;
        private final String mJobName;
        private long mLastCheckpointMs;
        private final long mStartTimeMs;

        private void convertDex2OatStatInfoToEventMap(Map map, Dex2oatLogcatParser.Dex2OatStatInfo dex2OatStatInfo) {
            String normalizeDex2OatStatType = normalizeDex2OatStatType(dex2OatStatInfo.statType);
            addDex2OatStatToEventMap(map, normalizeDex2OatStatType, OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_TOTAL_CASES, Integer.valueOf(dex2OatStatInfo.totalCases));
            addDex2OatStatToEventMap(map, normalizeDex2OatStatType, OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, Integer.valueOf(dex2OatStatInfo.success));
            addDex2OatStatToEventMap(map, normalizeDex2OatStatType, OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_FAILURE, Integer.valueOf(dex2OatStatInfo.failure));
            addDex2OatStatToEventMap(map, normalizeDex2OatStatType, OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_PERCENT, Double.valueOf(dex2OatStatInfo.percent));
        }

        private String makeKeyNameFromDex2OatStatInfo(String str, String str2) {
            return AnonymousClass08S.A0S("dex2oat_stat_", str, "_", str2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
         arg types: [int, int]
         candidates:
          ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
          ClspMth{java.lang.String.replace(char, char):java.lang.String} */
        private String normalizeDex2OatStatType(String str) {
            return str.replace(' ', '_');
        }

        public void logEvent(String str, Dex2oatLogcatParser dex2oatLogcatParser) {
            Set<Dex2oatLogcatParser.Dex2OatStatInfo> emptySet;
            if (this.mExitStatus == null) {
                Mlog.w("Bad call to logEvent, exit status not set", new Object[0]);
                return;
            }
            HashMap hashMap = new HashMap();
            hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_JOB_NAME, this.mJobName);
            hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_STEP, "finish");
            hashMap.put("duration", Long.valueOf(SystemClock.uptimeMillis() - this.mStartTimeMs));
            hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_EXIT_CODE, this.mExitStatus);
            if (!TextUtils.isEmpty(str)) {
                hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_DETAIL_MSG, str);
            }
            hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_ATTEMPT_NUMBER, Integer.valueOf(this.mAttemptNumber));
            if (dex2oatLogcatParser != null) {
                emptySet = dex2oatLogcatParser.mDex2OatStatInfos;
            } else {
                emptySet = Collections.emptySet();
            }
            for (Dex2oatLogcatParser.Dex2OatStatInfo convertDex2OatStatInfoToEventMap : emptySet) {
                convertDex2OatStatInfoToEventMap(hashMap, convertDex2OatStatInfoToEventMap);
            }
            OptSvcAnalyticsStore.logEvent(this.mAppContext, EVENT_NAME, hashMap);
        }

        public Dex2OatProgressListener(Context context, String str, int i) {
            this.mAppContext = OdexSchemeArtXdex.getAppContext(context);
            this.mJobName = str;
            this.mAttemptNumber = i;
            long uptimeMillis = SystemClock.uptimeMillis();
            this.mStartTimeMs = uptimeMillis;
            this.mLastCheckpointMs = uptimeMillis;
        }

        private void addDex2OatStatToEventMap(Map map, String str, String str2, Object obj) {
            String makeKeyNameFromDex2OatStatInfo = makeKeyNameFromDex2OatStatInfo(str, str2);
            Mlog.safeFmt("Adding dex2oat stat key %s with %s", makeKeyNameFromDex2OatStatInfo, obj);
            map.put(makeKeyNameFromDex2OatStatInfo, obj);
        }

        public void onCheckpoint() {
            long uptimeMillis = SystemClock.uptimeMillis();
            if (uptimeMillis - this.mLastCheckpointMs >= CHECKPOINT_PERIOD) {
                this.mLastCheckpointMs = uptimeMillis;
                HashMap hashMap = new HashMap();
                hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_JOB_NAME, this.mJobName);
                hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_STEP, "checkpoint");
                hashMap.put("duration", Long.valueOf(uptimeMillis - this.mStartTimeMs));
                hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_ATTEMPT_NUMBER, Integer.valueOf(this.mAttemptNumber));
                OptSvcAnalyticsStore.logEvent(this.mAppContext, EVENT_NAME, hashMap);
            }
        }

        public void onComplete(int i) {
            this.mExitStatus = Integer.valueOf(i);
        }
    }

    public interface DexSelector {
        boolean select(DexManifest.Dex dex);
    }

    public class ManifestEntry {
        public final boolean background;
        public final String canary;
        public final boolean coldstart;
        public final boolean extended;
        public final int ordinal;
        public final boolean primary;
        public final boolean scroll;

        public static ManifestEntry fromCSV(String str) {
            String[] split = str.split(",");
            int length = split.length;
            if (length >= 1) {
                String convertClassToDotForm = OdexSchemeArtXdex.convertClassToDotForm(split[0]);
                int i = 1;
                int i2 = -1;
                boolean z = false;
                boolean z2 = false;
                boolean z3 = false;
                boolean z4 = false;
                boolean z5 = false;
                while (i < length) {
                    String[] split2 = split[i].split("=");
                    if (split2.length == 2) {
                        String str2 = split2[0];
                        if (str2.equals("primary")) {
                            z = false;
                            if (Integer.valueOf(split2[1]).intValue() == 1) {
                                z = true;
                            }
                        } else if (str2.equals("extended")) {
                            z3 = false;
                            if (Integer.valueOf(split2[1]).intValue() == 1) {
                                z3 = true;
                            }
                        } else if (str2.equals("scroll")) {
                            z4 = false;
                            if (Integer.valueOf(split2[1]).intValue() == 1) {
                                z4 = true;
                            }
                        } else if (str2.equals("coldstart")) {
                            z2 = false;
                            if (Integer.valueOf(split2[1]).intValue() == 1) {
                                z2 = true;
                            }
                        } else if (str2.equals("background")) {
                            z5 = false;
                            if (Integer.valueOf(split2[1]).intValue() == 1) {
                                z5 = true;
                            }
                        } else if (str2.equals("ordinal")) {
                            i2 = Integer.valueOf(split2[1]).intValue();
                        }
                        i++;
                    }
                }
                return new ManifestEntry(convertClassToDotForm, i2, z, z2, z3, z4, z5);
            }
            return null;
        }

        public ManifestEntry(String str, int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            this.canary = str;
            this.ordinal = i;
            this.primary = z;
            this.coldstart = z2;
            this.extended = z3;
            this.scroll = z4;
            this.background = z5;
        }
    }

    public static boolean anyOptimizationDone(long j) {
        return (j & STATE_MASK) != 0;
    }

    private List getMixedModeDexInfos(List list, DexStore.TmpDir tmpDir, boolean z, byte b, DexManifest.Dex[] dexArr) {
        DexStore.TmpDir tmpDir2 = tmpDir;
        File extractResourceFile = extractResourceFile(DEX_MANIFEST_RESOURCE_PATH, tmpDir.directory, TMP_DEX_MANIFEST_FILE);
        List list2 = list;
        DexManifest.Dex[] dexArr2 = dexArr;
        boolean z2 = z;
        if (extractResourceFile == null) {
            Mlog.safeFmt("[opt][mixed_mode] Getting mixed mode dex infos in legacy mode", new Object[0]);
            return getMixedModeDexInfosLegacy(list, tmpDir, z, dexArr);
        }
        Mlog.safeFmt("[opt][mixed_mode] Getting mixed mode dex infos in manifest mode", new Object[0]);
        return getMixedModeDexInfosManifest(extractResourceFile, list2, tmpDir2, z2, dexSelectorForMultidexCompilationStrategy(extractResourceFile, b), dexArr2);
    }

    public static boolean optimizationCompleted(long j) {
        return (j & 16) != 0;
    }

    public static boolean quickeningNeeded(long j) {
        long j2 = j & STATE_MASK;
        return j2 == STATE_OATMEAL_QUICKENING_NEEDED || j2 == STATE_DEX2OAT_QUICKENING_NEEDED;
    }

    public String getSchemeName() {
        return "OdexSchemeArtXdex";
    }

    public final class Dex2OatHookInfo {
        public final String envFlag;
        public final String libName;

        public Dex2OatHookInfo(String str, String str2) {
            this.libName = str;
            this.envFlag = str2;
        }
    }

    public class OptimizationStateHolder {
        public long status;
        public long statusIntent;
        public boolean success;

        public OptimizationStateHolder(boolean z, long j, long j2) {
            this.success = z;
            this.status = j;
            this.statusIntent = j2;
        }
    }

    public class Renamer {
        private ArrayList mDestFiles = new ArrayList();
        private ArrayList mSourceFiles = new ArrayList();
        private final DexStore.TmpDir mTmpDir;

        public void renameOrThrow() {
            for (int i = 0; i < this.mSourceFiles.size(); i++) {
                File file = (File) this.mSourceFiles.get(i);
                File file2 = (File) this.mDestFiles.get(i);
                Mlog.safeFmt("Renaming %s to %s", file, file2);
                Fs.renameOrThrow(file, file2);
            }
        }

        public void reset() {
            Mlog.safeFmt("Resetting Renamer", new Object[0]);
            this.mSourceFiles.clear();
            this.mDestFiles.clear();
        }

        public Renamer(DexStore.TmpDir tmpDir) {
            this.mTmpDir = tmpDir;
        }

        public void addFile(File file, File file2) {
            Mlog.safeFmt("Queueing Rename of src %s to dest %s", file.getAbsolutePath(), file2.getAbsolutePath());
            this.mSourceFiles.add(file);
            this.mDestFiles.add(file2);
        }
    }

    private static void addOldPgoDex2OatParams(ProcessBuilder processBuilder) {
        if (!AnonymousClass095.A07 && AnonymousClass095.A06) {
            processBuilder.mArgv.add("--compiler-backend=Quick");
            processBuilder.mArgv.add("--top-k-profile-threshold=100.0");
        }
    }

    private static boolean anyCompilationNeeded(DexStore.Config config) {
        if (config.enableMixedMode || config.enableMixedModePgo || config.tryPeriodicPgoCompilation) {
            return true;
        }
        return false;
    }

    private static List convertCopiedDexInfoToExpectedDexInfo(List list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new ExpectedFileInfo((DexUnpacker.CopiedDexInfo) it.next()));
        }
        return arrayList;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:10|(2:12|13)|14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        if (r1 != null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0023 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0033 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:27:0x0033=Splitter:B:27:0x0033, B:18:0x0029=Splitter:B:18:0x0029} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.File extractResourceFile(java.lang.String r5, java.io.File r6, java.lang.String r7) {
        /*
            r4 = this;
            java.lang.String r0 = "txt"
            java.io.File r3 = java.io.File.createTempFile(r7, r0, r6)     // Catch:{ IOException -> 0x0034 }
            r3.deleteOnExit()     // Catch:{ IOException -> 0x0034 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0034 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0034 }
            com.facebook.common.dextricks.ResProvider r0 = r4.mResProvider     // Catch:{ all -> 0x002d }
            java.io.InputStream r1 = r0.open(r5)     // Catch:{ all -> 0x002d }
            r0 = 2147483647(0x7fffffff, float:NaN)
            X.AnonymousClass0Me.A0B(r2, r1, r0)     // Catch:{ all -> 0x001b }
            goto L_0x0024
        L_0x001b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001d }
        L_0x001d:
            r0 = move-exception
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ all -> 0x0023 }
        L_0x0023:
            throw r0     // Catch:{ all -> 0x002d }
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ all -> 0x002d }
        L_0x0029:
            r2.close()     // Catch:{ IOException -> 0x0034 }
            return r3
        L_0x002d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002f }
        L_0x002f:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0033 }
        L_0x0033:
            throw r0     // Catch:{ IOException -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            java.lang.String r1 = "[opt][res] io exception "
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]
            com.facebook.common.dextricks.Mlog.e(r1, r0)
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.extractResourceFile(java.lang.String, java.io.File, java.lang.String):java.io.File");
    }

    private static String getBootClassPathValue(DexStore dexStore, boolean z) {
        String str = System.getenv("BOOTCLASSPATH");
        if (!z) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        File[] dependencyOdexFiles = dexStore.getDependencyOdexFiles();
        for (int i = 0; i < dependencyOdexFiles.length; i += 2) {
            if (sb.length() > 0) {
                sb.append(":");
            }
            sb.append(dependencyOdexFiles[i].getAbsoluteFile());
        }
        return sb.toString();
    }

    private static File getCannotTruncateDexFlagFile(File file) {
        return DexStoreUtils.makeIgnoreDirtyCheckFile(file, "cannot_trunc_dex.flg");
    }

    private static boolean getCannotTruncateDexesFlag(File file) {
        return DexStoreUtils.makeIgnoreDirtyCheckFile(file, "cannot_trunc_dex.flg").exists();
    }

    private static String getClassPathValue(DexStore dexStore, File file, List list) {
        StringBuilder sb = new StringBuilder();
        File[] dependencyOdexFiles = dexStore.getDependencyOdexFiles();
        for (int i = 0; i < dependencyOdexFiles.length; i += 2) {
            if (sb.length() > 0) {
                sb.append(":");
            }
            sb.append(dependencyOdexFiles[i].getAbsoluteFile());
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            File file2 = ((ExpectedFileInfo) it.next()).getFile(file);
            if (sb.length() > 0) {
                sb.append(":");
            }
            sb.append(file2.getAbsolutePath());
        }
        return sb.toString();
    }

    private static long getCompilationStatusFlags(DexStore.Config config) {
        long j;
        if (config.enableMixedMode) {
            j = STATE_MIXED_NEEDED;
        } else {
            j = 0;
        }
        if (config.tryPeriodicPgoCompilation) {
            j |= STATE_DO_PERIODIC_PGO_COMP_NEEDED;
        }
        if (config.enableMixedModePgo) {
            return j | STATE_PGO_NEEDED;
        }
        return j;
    }

    private String[] getDefaultDexNames() {
        return makeExpectedFileList(this.mDexes, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.common.dextricks.OdexSchemeArtXdex.Dex2OatHookInfo getDex2OatLibHooks(android.content.Context r5, com.facebook.common.dextricks.DexStore.OptimizationSession r6) {
        /*
            r4 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 0
            r0 = 21
            if (r1 < r0) goto L_0x002a
            java.lang.String r1 = "arm64"
            if (r1 == 0) goto L_0x002a
            if (r1 == 0) goto L_0x001b
            java.lang.String r0 = "64"
            boolean r0 = r1.contains(r0)
        L_0x0013:
            if (r0 == 0) goto L_0x002a
            java.lang.String[] r0 = android.os.Build.SUPPORTED_64_BIT_ABIS
            int r0 = r0.length
            if (r0 == 0) goto L_0x002a
            goto L_0x001d
        L_0x001b:
            r0 = 0
            goto L_0x0013
        L_0x001d:
            java.lang.String r0 = "/proc/self/exe"
            java.lang.String r1 = android.system.Os.readlink(r0)     // Catch:{ ErrnoException -> 0x002c }
            java.lang.String r0 = "64"
            boolean r0 = r1.contains(r0)     // Catch:{ ErrnoException -> 0x002c }
            goto L_0x002d
        L_0x002a:
            r0 = r2
            goto L_0x002d
        L_0x002c:
            r0 = r2
        L_0x002d:
            r3 = 0
            if (r0 == 0) goto L_0x0038
            java.lang.Object[] r1 = new java.lang.Object[r2]
            java.lang.String r0 = "Not installing Dex2OatLibHooks on 64 bit device"
        L_0x0034:
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return r3
        L_0x0038:
            com.facebook.common.dextricks.OptimizationConfiguration r0 = r6.config
            boolean r0 = r0.installDex2OatHooks
            if (r0 != 0) goto L_0x0049
            boolean r0 = r4.shouldEnableMemoryDex2OatHook(r5)
            if (r0 != 0) goto L_0x0049
            java.lang.Object[] r1 = new java.lang.Object[r2]
            java.lang.String r0 = "Not installing Dex2OatLibHooks"
            goto L_0x0034
        L_0x0049:
            java.lang.Object[] r1 = new java.lang.Object[r2]
            java.lang.String r0 = "Going to try installing Dex2OatLibHooks"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatHookInfo r2 = new com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatHookInfo
            java.lang.String r1 = "dex2oathooks"
            java.lang.String r0 = "FB_ENABLE_DEX2OAT_HOOKS"
            r2.<init>(r1, r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.getDex2OatLibHooks(android.content.Context, com.facebook.common.dextricks.DexStore$OptimizationSession):com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatHookInfo");
    }

    private static Dex2OatHookInfo getMirandaFixLibHook() {
        if (Build.VERSION.SDK_INT == 21) {
            return new Dex2OatHookInfo("arthook", "FB_ENABLE_MIRANDA_HACK");
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0049, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004e, code lost:
        r2 = e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:18:0x0042, B:22:0x0048, B:27:0x004d] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x004d */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0064  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0042=Splitter:B:18:0x0042, B:27:0x004d=Splitter:B:27:0x004d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List getMixedModeDexInfosLegacy(java.util.List r7, com.facebook.common.dextricks.DexStore.TmpDir r8, boolean r9, com.facebook.common.dextricks.DexManifest.Dex[] r10) {
        /*
            r6 = this;
            java.io.File r2 = r8.directory
            java.lang.String r1 = "mixed_mode.txt"
            java.lang.String r0 = "art_mixed_mode_data_input"
            java.io.File r3 = r6.extractResourceFile(r1, r2, r0)
            r5 = 0
            r4 = 0
            if (r3 != 0) goto L_0x0016
            java.lang.Object[] r1 = new java.lang.Object[r4]
            java.lang.String r0 = "[opt][mixed_mode] no mixed mode data file found"
            com.facebook.common.dextricks.Mlog.w(r0, r1)
            return r5
        L_0x0016:
            java.lang.Object[] r1 = new java.lang.Object[r4]
            java.lang.String r0 = "[opt][mixed_mode] found mixed mode data file"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0050 }
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ IOException -> 0x0050 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0050 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0050 }
            java.lang.String r0 = r2.readLine()     // Catch:{ all -> 0x0046 }
            java.lang.String r3 = r0.trim()     // Catch:{ all -> 0x0046 }
            if (r3 == 0) goto L_0x003a
            boolean r0 = r3.isEmpty()     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0042
            goto L_0x003a
        L_0x0038:
            r0 = move-exception
            goto L_0x0048
        L_0x003a:
            java.lang.String r1 = "[opt][mixed_mode] mixed mode data missing in mixed mode data file"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x0046 }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x0046 }
            r3 = r5
        L_0x0042:
            r2.close()     // Catch:{ IOException -> 0x004e }
            goto L_0x0059
        L_0x0046:
            r0 = move-exception
            r3 = r5
        L_0x0048:
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x004d }
        L_0x004d:
            throw r0     // Catch:{ IOException -> 0x004e }
        L_0x004e:
            r2 = move-exception
            goto L_0x0052
        L_0x0050:
            r2 = move-exception
            r3 = r5
        L_0x0052:
            java.lang.Object[] r1 = new java.lang.Object[r4]
            java.lang.String r0 = "[opt][mixed_mode] problem reading mixed mode data file"
            com.facebook.common.dextricks.Mlog.w(r2, r0, r1)
        L_0x0059:
            java.lang.Object[] r1 = new java.lang.Object[]{r3}
            java.lang.String r0 = "[opt][mixed_mode] mixed mode canary is %s"
            com.facebook.common.dextricks.Mlog.w(r0, r1)
            if (r3 == 0) goto L_0x0077
            if (r9 == 0) goto L_0x0081
            java.lang.String r0 = r7.toString()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "[opt][mixed_mode] mixed mode must also take into account pgo compilation: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
        L_0x0073:
            java.util.List r5 = makeMixedModeFileList(r10, r3, r7)
        L_0x0077:
            java.lang.Object[] r1 = new java.lang.Object[]{r5}
            java.lang.String r0 = "[opt][mixed_mode] mixed mode dex names: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return r5
        L_0x0081:
            r7 = r5
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.getMixedModeDexInfosLegacy(java.util.List, com.facebook.common.dextricks.DexStore$TmpDir, boolean, com.facebook.common.dextricks.DexManifest$Dex[]):java.util.List");
    }

    private List getMixedModeDexInfosManifest(File file, List list, DexStore.TmpDir tmpDir, boolean z, DexSelector dexSelector, DexManifest.Dex[] dexArr) {
        if (z) {
            Mlog.safeFmt("[opt][mixed_mode] mixed mode must also take into account pgo compilation: %s", list.toString());
        } else {
            list = null;
        }
        return makeMixedModeFileList(dexArr, dexSelector, list);
    }

    private List getNewInitialOptimizedSecondaryDexes(Context context, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.Config config, DexStore.TmpDir tmpDir, Renamer renamer, AtomicReference atomicReference) {
        DexStore.TmpDir tmpDir2 = tmpDir;
        File makeTmpSubDirectory = makeTmpSubDirectory(tmpDir, "tmp-unpack-dexes");
        DexStore dexStore2 = dexStore;
        List newSecondaryDexes = getNewSecondaryDexes(makeTmpSubDirectory, dexStore.root);
        atomicReference.set(makeTmpSubDirectory);
        if (newSecondaryDexes == null) {
            return null;
        }
        Mlog.safeFmt("Starting to do initial optimizations for temp secondary dexes", new Object[0]);
        DexStore.DexStoreTestHooks dexStoreTestHooks = DexStore.sDexStoreTestHooks;
        if (dexStoreTestHooks != null) {
            dexStoreTestHooks.onSecondaryDexesUnpackedForRecompilation();
        }
        Mlog.safeFmt("Optimizing temp secondary dexes got result status: %d", Long.valueOf(initialDexOptimizations(context, dexStore2, optimizationSession, config, tmpDir2, renamer, makeTmpSubDirectory, newSecondaryDexes)));
        renamer.renameOrThrow();
        renamer.reset();
        return newSecondaryDexes;
    }

    private List getNewSecondaryDexes(File file, File file2) {
        return convertCopiedDexInfoToExpectedDexInfo(this.mDexUnpacker.copySecondaryDexes(file, file2));
    }

    private static int getOdexFlags() {
        if (Build.VERSION.SDK_INT < 26) {
            return 5;
        }
        return 1;
    }

    private static void initAllDex2OatHooks(ProcessBuilder processBuilder, Dex2OatHookInfo... dex2OatHookInfoArr) {
        boolean z;
        if (dex2OatHookInfoArr != null && (r6 = dex2OatHookInfoArr.length) != 0) {
            StringBuilder sb = new StringBuilder();
            String str = System.getenv(ENV_LD_PRELOAD);
            if (str != null) {
                sb.append(str);
                z = true;
            } else {
                z = false;
            }
            boolean z2 = false;
            for (Dex2OatHookInfo dex2OatHookInfo : dex2OatHookInfoArr) {
                if (dex2OatHookInfo != null) {
                    Mlog.safeFmt("Installing hook for %s", dex2OatHookInfo.libName);
                    File A01 = AnonymousClass01q.A01(dex2OatHookInfo.libName);
                    if (A01 != null) {
                        if (z) {
                            sb.append(File.pathSeparatorChar);
                        }
                        sb.append(A01.getAbsolutePath());
                        String str2 = dex2OatHookInfo.envFlag;
                        if (str2 != null) {
                            processBuilder.setenv(str2, "1");
                        }
                        z2 = true;
                        z = true;
                    }
                }
            }
            if (z2) {
                Mlog.safeFmt("Setting %s to %s", ENV_LD_PRELOAD, sb.toString());
                processBuilder.setenv(ENV_LD_PRELOAD, sb.toString());
                processBuilder.setenv("LD_LIBRARY_PATH", AnonymousClass01q.A03());
            }
        }
    }

    private static boolean isOatFileStillValid(File file, long j, long j2) {
        if (Build.VERSION.SDK_INT < 26) {
            long length = file.length();
            long lastModified = file.lastModified();
            if (!(j == length && j2 == lastModified && j != 0)) {
                return false;
            }
        }
        return true;
    }

    private boolean needsTruncation(File file, int i) {
        if (i < 0 || dexAppearsTruncated(file, i)) {
            return false;
        }
        return true;
    }

    public static String oatNameFromDexName(String str) {
        String str2;
        if (str.contains(".")) {
            str = str.substring(0, str.lastIndexOf(46));
        }
        if (Build.VERSION.SDK_INT >= 26) {
            str2 = DexManifest.ODEX_EXT;
        } else {
            str2 = ".oat";
        }
        return AnonymousClass08S.A0J(str, str2);
    }

    private void optimizeInitial(Context context, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.Config config, DexStore.TmpDir tmpDir, Renamer renamer, DexStore.OptimizationSession.Job job, OptimizationStateHolder optimizationStateHolder) {
        DexStore.OptimizationSession optimizationSession2 = optimizationSession;
        if (optimizationSession2.optimizationAttemptNumber <= optimizationSession2.config.maximumOptimizationAttempts) {
            OptimizationStateHolder optimizationStateHolder2 = optimizationStateHolder;
            Context context2 = context;
            optimizationStateHolder2.status = initialDexOptimizations(context2, dexStore, optimizationSession2, config, tmpDir, renamer, null, null) | optimizationStateHolder2.status;
            return;
        }
        throw new IllegalStateException("Unable to optimize in a reasonable amount of attempts");
    }

    public static boolean pgoProfileRecompilationNeeded(DexStore.OptimizationHistoryLog optimizationHistoryLog, long j) {
        if (!optimizationCompleted(optimizationHistoryLog.schemeStatus) || (j & STATE_DO_PERIODIC_PGO_COMP_NEEDED) == 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:30|31|32|33|34) */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (X.AnonymousClass0MU.A00 == false) goto L_0x0011;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0080 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x0087 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void saveOatFiles() {
        /*
            r9 = this;
            java.lang.String r3 = "Exception: %s"
            int r1 = com.facebook.common.build.BuildConstants.A00()
            r0 = 1
            if (r1 <= r0) goto L_0x000a
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x0011
            boolean r1 = X.AnonymousClass0MU.A00
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x00a6
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 >= r0) goto L_0x00a6
            java.lang.String r0 = android.os.Environment.DIRECTORY_DOWNLOADS
            java.io.File r0 = android.os.Environment.getExternalStoragePublicDirectory(r0)
            r5 = 0
            java.lang.String r1 = r0.getCanonicalPath()     // Catch:{ IOException -> 0x0099 }
            java.lang.String r0 = "/FB/"
            java.lang.String r8 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ IOException -> 0x0099 }
            java.lang.Object[] r1 = new java.lang.Object[]{r8}
            java.lang.String r0 = "Saving oat files to sdCardPath: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            java.util.ArrayList r0 = r9.oatFiles
            java.util.Iterator r7 = r0.iterator()
        L_0x003a:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0091
            java.lang.Object r6 = r7.next()
            java.io.File r6 = (java.io.File) r6
            java.lang.String r0 = r6.getName()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Copying oatFile: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            java.io.File r4 = new java.io.File
            java.lang.String r0 = r6.getName()
            r4.<init>(r8, r0)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0088 }
            java.lang.String r0 = r6.getCanonicalPath()     // Catch:{ IOException -> 0x0088 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0088 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x0081 }
            r1.<init>(r4)     // Catch:{ all -> 0x0081 }
            r0 = 2147483647(0x7fffffff, float:NaN)
            X.AnonymousClass0Me.A09(r1, r2, r0)     // Catch:{ all -> 0x007a }
            r1.flush()     // Catch:{ all -> 0x007a }
            r1.close()     // Catch:{ all -> 0x0081 }
            r2.close()     // Catch:{ IOException -> 0x0088 }
            goto L_0x003a
        L_0x007a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007c }
        L_0x007c:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0080 }
        L_0x0080:
            throw r0     // Catch:{ all -> 0x0081 }
        L_0x0081:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0083 }
        L_0x0083:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0087 }
        L_0x0087:
            throw r0     // Catch:{ IOException -> 0x0088 }
        L_0x0088:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r0 = "OatFile couldn't be saved to sdcard "
            com.facebook.common.dextricks.Mlog.w(r0, r1)
            goto L_0x009a
        L_0x0091:
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r0 = "Copying oat files complete."
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return
        L_0x0099:
            r2 = move-exception
        L_0x009a:
            java.lang.String r0 = r2.toString()
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            com.facebook.common.dextricks.Mlog.w(r3, r0)
            return
        L_0x00a6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.saveOatFiles():void");
    }

    private void setCompilerFilter(DexStore.Config config, ProcessBuilder processBuilder) {
        String str;
        String str2;
        byte b = config.artFilter;
        if (b != 0) {
            String str3 = null;
            switch (b) {
                case 1:
                    str3 = "verify-none";
                    break;
                case 2:
                    str3 = "interpret-only";
                    break;
                case 3:
                    str3 = "space";
                    break;
                case 4:
                    str3 = "balanced";
                    break;
                case 5:
                    str3 = "speed";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    str3 = "everything";
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    str3 = "time";
                    break;
                default:
                    Mlog.w("ignoring unknown configured ART filter %s", Byte.valueOf(b));
                    break;
            }
            if (str3 != null) {
                processBuilder.mArgv.add(AnonymousClass08S.A0J("--compiler-filter=", str3));
                return;
            }
        }
        String str4 = SystemProperties.get("dalvik.vm.dex2oat-filter");
        boolean isEmpty = str4.isEmpty();
        if (!isEmpty) {
            if (!isEmpty) {
                str2 = str4;
            } else {
                str2 = "<none>";
            }
            Mlog.safeFmt("Setting dex2oatFilter to system prop: %s", str2);
            str = AnonymousClass08S.A0J("--compiler-filter=", str4);
        } else {
            Mlog.safeFmt("[opt] no compile-filter set or pgo data, compiling with verify-none", new Object[0]);
            str = "--compiler-filter=verify-none";
        }
        processBuilder.mArgv.add(str);
    }

    private boolean shouldEnableMemoryDex2OatHook(Context context) {
        if (!this.mEnableMemoryDex2OatHookInited) {
            this.mEnableMemoryDex2OatHook = AnonymousClass08X.A08(context, Experiments.ANDROID_ENABLE_MEMORY_DEX2OAT_HOOK, false);
            this.mEnableMemoryDex2OatHookInited = true;
        }
        return this.mEnableMemoryDex2OatHook;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a3, code lost:
        if (r1 != null) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x00a8 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void startOptimizerProcess(com.facebook.forker.ProcessBuilder r11, android.content.Context r12, java.lang.String r13, com.facebook.common.dextricks.DexStore.OptimizationSession r14, com.facebook.common.dextricks.Dex2oatLogcatParser r15, com.facebook.common.dextricks.DexStore.TmpDir r16) {
        /*
            r10 = this;
            com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatProgressListener r5 = new com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatProgressListener
            int r0 = r14.optimizationAttemptNumber
            r5.<init>(r12, r13, r0)
            r4 = 0
            r0 = r16
            java.io.File r0 = r0.directory     // Catch:{ all -> 0x00a9 }
            java.io.RandomAccessFile r9 = com.facebook.common.dextricks.Fs.openUnlinkedTemporaryFile(r0)     // Catch:{ all -> 0x00a9 }
            java.io.FileDescriptor r0 = r9.getFD()     // Catch:{ all -> 0x00ac }
            int r1 = com.facebook.forker.Fd.fileno(r0)     // Catch:{ all -> 0x00ac }
            r0 = 1
            int[] r2 = r11.mStreamDispositions     // Catch:{ all -> 0x00ac }
            r2[r0] = r1     // Catch:{ all -> 0x00ac }
            r1 = 2
            r0 = -5
            r2[r1] = r0     // Catch:{ all -> 0x00ac }
            java.lang.String r1 = "starting job %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r13}     // Catch:{ all -> 0x00ac }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00ac }
            com.facebook.common.dextricks.OptimizationConfiguration r0 = r14.config     // Catch:{ all -> 0x00ac }
            com.facebook.common.dextricks.Prio r0 = r0.prio     // Catch:{ all -> 0x00ac }
            com.facebook.common.dextricks.Prio$With r1 = new com.facebook.common.dextricks.Prio$With     // Catch:{ all -> 0x00ac }
            r1.<init>()     // Catch:{ all -> 0x00ac }
            com.facebook.forker.Process r4 = r11.create()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ all -> 0x00ac }
        L_0x003c:
            int r1 = r4.mPid     // Catch:{ all -> 0x00ac }
            int r8 = r14.waitForAndManageProcess(r4, r5)     // Catch:{ all -> 0x00ac }
            if (r15 == 0) goto L_0x0064
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00ac }
            r0 = 10000(0x2710, float:1.4013E-41)
            boolean r0 = r15.readAndParseProcess(r1, r0)     // Catch:{ all -> 0x00ac }
            java.lang.String r3 = "Success getting logcat dex2oat data: %s in %d ms"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x00ac }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00ac }
            long r0 = r0 - r6
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x00ac }
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r0}     // Catch:{ all -> 0x00ac }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r0)     // Catch:{ all -> 0x00ac }
        L_0x0064:
            java.lang.String r3 = com.facebook.common.dextricks.Fs.readProgramOutputFile(r9)     // Catch:{ IOException -> 0x0069 }
            goto L_0x006a
        L_0x0069:
            r3 = 0
        L_0x006a:
            java.lang.String r2 = "Got result from dex2oat [exit status: %d]: %s"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x00ac }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r3}     // Catch:{ all -> 0x00ac }
            com.facebook.common.dextricks.Mlog.safeFmt(r2, r0)     // Catch:{ all -> 0x00ac }
            com.facebook.common.dextricks.Fs.safeClose(r9)
            r4.destroy()
            r5.logEvent(r3, r15)
            java.lang.Object[] r1 = new java.lang.Object[]{r13, r1}
            java.lang.String r0 = "%s exited with status %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            if (r8 != 0) goto L_0x008c
            return
        L_0x008c:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r0 = com.facebook.forker.Process.describeStatus(r8)
            java.lang.Object[] r1 = new java.lang.Object[]{r13, r0, r3}
            java.lang.String r0 = "%s failed: %s: %s"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r2.<init>(r0)
            throw r2
        L_0x00a0:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a2 }
        L_0x00a2:
            r0 = move-exception
            if (r1 == 0) goto L_0x00a8
            r1.close()     // Catch:{ all -> 0x00a8 }
        L_0x00a8:
            throw r0     // Catch:{ all -> 0x00ac }
        L_0x00a9:
            r0 = move-exception
            r9 = r4
            goto L_0x00ad
        L_0x00ac:
            r0 = move-exception
        L_0x00ad:
            com.facebook.common.dextricks.Fs.safeClose(r9)
            if (r4 == 0) goto L_0x00b5
            r4.destroy()
        L_0x00b5:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.startOptimizerProcess(com.facebook.forker.ProcessBuilder, android.content.Context, java.lang.String, com.facebook.common.dextricks.DexStore$OptimizationSession, com.facebook.common.dextricks.Dex2oatLogcatParser, com.facebook.common.dextricks.DexStore$TmpDir):void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:11|12|13|14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x001c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void truncateWithBackup(java.io.File r4, java.io.File r5, int r6) {
        /*
            r3 = this;
            if (r6 < 0) goto L_0x0027
            com.facebook.common.dextricks.Fs.renameOrThrow(r4, r5)
            java.io.FileOutputStream r2 = new java.io.FileOutputStream
            r2.<init>(r4)
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x001d }
            r1.<init>(r5)     // Catch:{ all -> 0x001d }
            X.AnonymousClass0Me.A09(r2, r1, r6)     // Catch:{ all -> 0x0016 }
            r1.close()     // Catch:{ all -> 0x001d }
            goto L_0x0024
        L_0x0016:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0018 }
        L_0x0018:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x001c }
        L_0x001c:
            throw r0     // Catch:{ all -> 0x001d }
        L_0x001d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001f }
        L_0x001f:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0023 }
        L_0x0023:
            throw r0
        L_0x0024:
            r2.close()
        L_0x0027:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.truncateWithBackup(java.io.File, java.io.File, int):void");
    }

    public static String vdexNameFromOdexName(String str) {
        if (str.contains(".")) {
            str = str.substring(0, str.lastIndexOf(46));
        }
        return AnonymousClass08S.A0J(str, ".vdex");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, boolean):void
     arg types: [java.io.File, int]
     candidates:
      com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, java.io.File):void
      com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, boolean):void */
    public void configureClassLoader(File file, MultiDexClassLoader.Configuration configuration) {
        String A0J;
        int i = 0;
        File file2 = file;
        MultiDexClassLoader.Configuration configuration2 = configuration;
        if (!this.mIsLoadable) {
            Mlog.safeFmt("App is not loadable yet, avoid loading secondary dexes", new Object[0]);
            super.configureClassLoader(file2, configuration2);
            return;
        }
        Mlog.safeFmt("We pass through this code when loading secondary dexes", new Object[0]);
        if (Build.VERSION.SDK_INT >= 26) {
            Mlog.assertThat(Arrays.asList(this.expectedFiles).contains(OdexSchemeArtTurbo.OREO_ODEX_DIR), "expect oat dir", new Object[0]);
        }
        Mlog.safeFmt("loading pre-built omni-oat", new Object[0]);
        DexStore findOpened = DexStore.findOpened(file2);
        long reportStatus = findOpened.reportStatus();
        int i2 = findOpened.readConfig().artTruncatedDexSize;
        if (Build.VERSION.SDK_INT >= 26) {
            A0J = OdexSchemeArtTurbo.getOreoOdexOutputDirectory(file2, false);
        } else {
            A0J = AnonymousClass08S.A0J(file2.getCanonicalPath(), "/");
        }
        Mlog.safeFmt("Looking at expected files: %s", Arrays.toString(this.expectedFiles));
        int i3 = 0;
        boolean z = true;
        while (true) {
            String[] strArr = this.expectedFiles;
            if (i3 >= strArr.length) {
                break;
            }
            Mlog.safeFmt("Looking at expected file: %s", strArr[i3]);
            String str = this.expectedFiles[i3];
            if (!str.endsWith(DexManifest.DEX_EXT)) {
                Mlog.safeFmt("Skipping since the expected file is not a dex file", new Object[i]);
            } else {
                File file3 = new File(AnonymousClass08S.A0J(A0J, oatNameFromDexName(str)));
                long lastModified = file3.lastModified();
                long length = file3.length();
                File file4 = new File(file2, this.expectedFiles[i3]);
                File file5 = new File(file2, AnonymousClass08S.A0J(this.expectedFiles[i3], ".backup"));
                String A0J2 = AnonymousClass08S.A0J("odexSchemeArtXDex.configureClassLoader() status=", Long.toHexString(findOpened.reportStatus()));
                if (!file4.exists()) {
                    A0J2 = A0J2 + " expected dex file " + file4 + " not found";
                } else if (file4.length() == 0 && file3.exists()) {
                    A0J2 = A0J2 + " attempting to load 0 length dex file " + file4 + " when we seemed to have already compiled to " + file3;
                }
                Mlog.safeFmt(A0J2, new Object[0]);
                if (z) {
                    try {
                        if (shouldTruncateDexesNow(file2, file4, reportStatus, i2)) {
                            truncateWithBackup(file4, file5, i2);
                            try {
                                Mlog.safeFmt("attempting to truncate %s to %d", file4, Integer.valueOf(i2));
                                configuration2.addDex(file4, file3);
                                Mlog.safeFmt("added truncated dex ok " + file4, new Object[0]);
                                Fs.deleteRecursive(file5);
                            } catch (IOException e) {
                                Fs.renameOrThrow(file5, file4);
                                DexTricksErrorReporter.reportSampledSoftError(REGENERATE_SOFT_ERROR_CATEGORY, "failed to load truncated dex", e);
                                findOpened.forceRegenerateOnNextLoad();
                                configuration2.addDex(file4, file3);
                                Mlog.safeFmt("added full dex ok " + file4, new Object[0]);
                            }
                            z &= isOatFileStillValid(file3, length, lastModified);
                        }
                    } catch (IOException e2) {
                        DexTricksErrorReporter.reportSampledSoftError(REGENERATE_SOFT_ERROR_CATEGORY, "IOException adding dex " + file4 + " will rethrow and attempt recovery", e2);
                        findOpened.forceRegenerateOnNextLoad();
                        throw new DexStore.RecoverableDexException(new IOException(A0J2, e2));
                    }
                }
                if (z) {
                    configuration2.addDex(file4, file3);
                } else {
                    configuration2.addDex(file4, false);
                }
                Mlog.safeFmt("added dex ok " + file4, new Object[0]);
                z &= isOatFileStillValid(file3, length, lastModified);
            }
            i3++;
            i = 0;
        }
        if (!z) {
            DexTricksErrorReporter.reportSampledSoftError(REGENERATE_SOFT_ERROR_CATEGORY, "failed filesize/time integrity checks", null);
            findOpened.forceRegenerateOnNextLoad();
        }
        if (optimizationCompleted(reportStatus)) {
            findOpened.writeStatusLocked(reportStatus);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0060, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0064 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.common.dextricks.OdexSchemeArtXdex.DexSelector dexSelectorForMultidexCompilationStrategy(java.io.File r10, final byte r11) {
        /*
            r9 = this;
            java.util.concurrent.atomic.AtomicInteger r4 = new java.util.concurrent.atomic.AtomicInteger
            r0 = 2147483647(0x7fffffff, float:NaN)
            r4.<init>(r0)
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r5 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0065 }
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ IOException -> 0x0065 }
            r0.<init>(r10)     // Catch:{ IOException -> 0x0065 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0065 }
        L_0x0018:
            java.lang.String r0 = r2.readLine()     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x005a
            java.lang.String r8 = r0.trim()     // Catch:{ all -> 0x005e }
            com.facebook.common.dextricks.OdexSchemeArtXdex$ManifestEntry r7 = com.facebook.common.dextricks.OdexSchemeArtXdex.ManifestEntry.fromCSV(r8)     // Catch:{ all -> 0x005e }
            if (r7 != 0) goto L_0x0034
            java.lang.String r0 = "[opt][mixed_mode] could not parse manifest entry for : "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r8)     // Catch:{ all -> 0x005e }
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x005e }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x005e }
            goto L_0x0018
        L_0x0034:
            java.lang.String r6 = "[opt][mixed_mode] read manifest entry for "
            java.lang.String r1 = r7.canary     // Catch:{ all -> 0x005e }
            java.lang.String r0 = " : "
            java.lang.String r1 = X.AnonymousClass08S.A0S(r6, r1, r0, r8)     // Catch:{ all -> 0x005e }
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x005e }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x005e }
            java.lang.String r0 = r7.canary     // Catch:{ all -> 0x005e }
            r3.put(r0, r7)     // Catch:{ all -> 0x005e }
            boolean r0 = r7.coldstart     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x0018
            int r1 = r4.get()     // Catch:{ all -> 0x005e }
            int r0 = r7.ordinal     // Catch:{ all -> 0x005e }
            int r0 = java.lang.Math.min(r1, r0)     // Catch:{ all -> 0x005e }
            r4.set(r0)     // Catch:{ all -> 0x005e }
            goto L_0x0018
        L_0x005a:
            r2.close()     // Catch:{ IOException -> 0x0065 }
            goto L_0x006d
        L_0x005e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0064 }
        L_0x0064:
            throw r0     // Catch:{ IOException -> 0x0065 }
        L_0x0065:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r0 = "[opt][mixed_mode] problem reading manifest file"
            com.facebook.common.dextricks.Mlog.w(r2, r0, r1)
        L_0x006d:
            com.facebook.common.dextricks.OdexSchemeArtXdex$2 r0 = new com.facebook.common.dextricks.OdexSchemeArtXdex$2
            r0.<init>(r3, r11, r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.dexSelectorForMultidexCompilationStrategy(java.io.File, byte):com.facebook.common.dextricks.OdexSchemeArtXdex$DexSelector");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:15|16|17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0038 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getOatmealPath(com.facebook.common.dextricks.DexStore.TmpDir r6) {
        /*
            r5 = this;
            java.lang.String r0 = r5.mOatmealPath
            if (r0 == 0) goto L_0x0005
            return r0
        L_0x0005:
            java.lang.String r2 = "oatmeal"
            java.io.File r4 = X.AnonymousClass01q.A01(r2)
            r4.getParentFile()
            boolean r0 = r4.canExecute()
            if (r0 != 0) goto L_0x0040
            r1 = 0
            java.io.File r0 = r6.directory
            java.io.File r3 = java.io.File.createTempFile(r2, r1, r0)
            java.io.FileOutputStream r2 = new java.io.FileOutputStream
            r2.<init>(r3)
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x0039 }
            r1.<init>(r4)     // Catch:{ all -> 0x0039 }
            r0 = 2147483647(0x7fffffff, float:NaN)
            X.AnonymousClass0Me.A09(r2, r1, r0)     // Catch:{ all -> 0x0032 }
            r2.flush()     // Catch:{ all -> 0x0032 }
            r1.close()     // Catch:{ all -> 0x0039 }
            goto L_0x0042
        L_0x0032:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0038 }
        L_0x0038:
            throw r0     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003b }
        L_0x003b:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x003f }
        L_0x003f:
            throw r0
        L_0x0040:
            r3 = r4
            goto L_0x0049
        L_0x0042:
            r2.close()
            r0 = 1
            r3.setExecutable(r0, r0)
        L_0x0049:
            java.lang.String r0 = r3.getAbsolutePath()
            r5.mOatmealPath = r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.getOatmealPath(com.facebook.common.dextricks.DexStore$TmpDir):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0149, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:83:0x014d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x0154 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int loadInformationalStatus(java.io.File r19, long r20) {
        /*
            r18 = this;
            r0 = r18
            com.facebook.common.dextricks.DexManifest$Dex[] r0 = r0.mDexes
            r4 = 0
            java.lang.String[] r13 = makeExpectedFileList(r0, r4)
            int r1 = android.os.Build.VERSION.SDK_INT
            r12 = 0
            r0 = 26
            r2 = r19
            if (r1 < r0) goto L_0x0168
            java.lang.String r11 = com.facebook.common.dextricks.OdexSchemeArtTurbo.getOreoOdexOutputDirectory(r2, r12)
        L_0x0017:
            r10 = 0
            r9 = 0
        L_0x0019:
            int r0 = r13.length
            r2 = 4096(0x1000, double:2.0237E-320)
            if (r10 >= r0) goto L_0x0042
            java.io.File r8 = new java.io.File
            r0 = r13[r10]
            java.lang.String r0 = oatNameFromDexName(r0)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r11, r0)
            r8.<init>(r0)
            boolean r0 = r8.exists()
            if (r0 != 0) goto L_0x00e0
            java.lang.String r1 = "loadInformationalStatus didn't find: "
            java.lang.String r0 = r8.getAbsolutePath()
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.Object[] r0 = new java.lang.Object[r12]
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)
        L_0x0042:
            r0 = 32
            long r6 = r20 & r0
            r16 = 512(0x200, double:2.53E-321)
            r14 = 256(0x100, double:1.265E-321)
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x00ce
            long r6 = r20 & r14
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x00ce
            r9 = r9 | 256(0x100, float:3.59E-43)
        L_0x0056:
            r0 = 128(0x80, double:6.32E-322)
            long r6 = r20 & r0
            r12 = 1024(0x400, double:5.06E-321)
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x0068
            long r6 = r20 & r12
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0068
            r9 = r9 | 1024(0x400, float:1.435E-42)
        L_0x0068:
            long r10 = r20 & r2
            r6 = 8192(0x2000, double:4.0474E-320)
            int r0 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            long r1 = r20 & r6
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x007a
            r0 = 32768(0x8000, float:4.5918E-41)
            r9 = r9 | r0
        L_0x007a:
            long r1 = r20 & r14
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x00c5
            r9 = r9 | 2048(0x800, float:2.87E-42)
        L_0x0082:
            long r1 = r20 & r12
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x008a
            r9 = r9 | 8192(0x2000, float:1.14794E-41)
        L_0x008a:
            long r1 = r20 & r6
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0093
            r0 = 65536(0x10000, float:9.18355E-41)
            r9 = r9 | r0
        L_0x0093:
            r6 = 2048(0x800, double:1.0118E-320)
            long r1 = r20 & r6
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x009d
            r9 = r9 | 16384(0x4000, float:2.2959E-41)
        L_0x009d:
            r6 = 16384(0x4000, double:8.0948E-320)
            long r1 = r20 & r6
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x00c4
            r6 = 65536(0x10000, double:3.2379E-319)
            long r1 = r20 & r6
            r6 = 32768(0x8000, double:1.61895E-319)
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x00bb
            long r1 = r20 & r6
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x00bb
            r0 = 131072(0x20000, float:1.83671E-40)
            r0 = r0 | r9
            r9 = r0
        L_0x00bb:
            long r20 = r20 & r6
            int r0 = (r20 > r4 ? 1 : (r20 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x00c4
            r0 = 262144(0x40000, float:3.67342E-40)
            r9 = r9 | r0
        L_0x00c4:
            return r9
        L_0x00c5:
            long r1 = r20 & r16
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0082
            r9 = r9 | 4096(0x1000, float:5.74E-42)
            goto L_0x0082
        L_0x00ce:
            r0 = 64
            long r6 = r20 & r0
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x0056
            long r6 = r20 & r16
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0056
            r9 = r9 | 512(0x200, float:7.175E-43)
            goto L_0x0056
        L_0x00e0:
            java.lang.String r1 = "loadInformationalStatus DID find: "
            java.lang.String r0 = r8.getAbsolutePath()
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.Object[] r0 = new java.lang.Object[r12]
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0155 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x0155 }
            java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x014e }
            r0 = 1024(0x400, float:1.435E-42)
            r6.<init>(r0)     // Catch:{ all -> 0x014e }
            long r14 = com.facebook.common.dextricks.Fs.discardFromInputStream(r7, r2)     // Catch:{ all -> 0x0147 }
            int r0 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0113
            java.lang.String r1 = "loadInformationalStatus couldn't read more than 4k of the beginning of "
            java.lang.String r0 = r8.getAbsolutePath()     // Catch:{ all -> 0x0147 }
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0147 }
            java.lang.Object[] r0 = new java.lang.Object[r12]     // Catch:{ all -> 0x0147 }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x0147 }
            goto L_0x0140
        L_0x0113:
            r0 = 4096(0x1000, float:5.74E-42)
            X.AnonymousClass0Me.A0B(r6, r7, r0)     // Catch:{ all -> 0x0147 }
            r6.flush()     // Catch:{ all -> 0x0147 }
            java.lang.String r1 = r6.toString()     // Catch:{ all -> 0x0147 }
            java.lang.String r0 = "86827de6f1ef3407f8dc98b76382d3a6e0759ab3"
            int r0 = r1.indexOf(r0)     // Catch:{ all -> 0x0147 }
            r1 = 0
            if (r0 < 0) goto L_0x0129
            r1 = 1
        L_0x0129:
            java.lang.String r0 = "loadInformationalStatus? "
            java.lang.String r14 = X.AnonymousClass08S.A0X(r0, r1)     // Catch:{ all -> 0x0147 }
            java.lang.Object[] r0 = new java.lang.Object[r12]     // Catch:{ all -> 0x0147 }
            com.facebook.common.dextricks.Mlog.safeFmt(r14, r0)     // Catch:{ all -> 0x0147 }
            if (r1 == 0) goto L_0x0140
            r9 = r9 | 128(0x80, float:1.794E-43)
            r6.close()     // Catch:{ all -> 0x014e }
            r7.close()     // Catch:{ IOException -> 0x0155 }
            goto L_0x0042
        L_0x0140:
            r6.close()     // Catch:{ all -> 0x014e }
            r7.close()     // Catch:{ IOException -> 0x0155 }
            goto L_0x0164
        L_0x0147:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0149 }
        L_0x0149:
            r0 = move-exception
            r6.close()     // Catch:{ all -> 0x014d }
        L_0x014d:
            throw r0     // Catch:{ all -> 0x014e }
        L_0x014e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0150 }
        L_0x0150:
            r0 = move-exception
            r7.close()     // Catch:{ all -> 0x0154 }
        L_0x0154:
            throw r0     // Catch:{ IOException -> 0x0155 }
        L_0x0155:
            java.lang.String r1 = "loadInformationalStatus couldn't open "
            java.lang.String r0 = r8.getAbsolutePath()
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.Object[] r0 = new java.lang.Object[r12]
            com.facebook.common.dextricks.Mlog.w(r1, r0)
        L_0x0164:
            int r10 = r10 + 1
            goto L_0x0019
        L_0x0168:
            java.lang.String r1 = r2.getCanonicalPath()
            java.lang.String r0 = "/"
            java.lang.String r11 = X.AnonymousClass08S.A0J(r1, r0)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.loadInformationalStatus(java.io.File, long):int");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:40|41|42|43|44) */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ff, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x0103 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:43:0x0103=Splitter:B:43:0x0103, B:31:0x00f5=Splitter:B:31:0x00f5} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void optimize(android.content.Context r26, com.facebook.common.dextricks.DexStore r27, com.facebook.common.dextricks.DexStore.OptimizationSession r28) {
        /*
            r25 = this;
            r9 = r27
            com.facebook.common.dextricks.DexStore$Config r6 = r9.readConfig()
            java.lang.String r0 = "dexopt"
            com.facebook.common.dextricks.DexStore$TmpDir r5 = r9.makeTemporaryDirectory(r0)
            com.facebook.common.dextricks.OdexSchemeArtXdex$Renamer r4 = new com.facebook.common.dextricks.OdexSchemeArtXdex$Renamer     // Catch:{ all -> 0x0104 }
            r4.<init>(r5)     // Catch:{ all -> 0x0104 }
            java.lang.String r1 = "[opt] opened tmpDir %s; starting job"
            java.io.File r0 = r5.directory     // Catch:{ all -> 0x0104 }
            r3 = 0
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x0104 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x0104 }
            com.facebook.common.dextricks.DexStore$OptimizationSession$Job r2 = new com.facebook.common.dextricks.DexStore$OptimizationSession$Job     // Catch:{ all -> 0x0104 }
            r7 = r28
            r2.<init>()     // Catch:{ all -> 0x0104 }
            java.lang.String r1 = "[opt] opened job"
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00fd }
            long r0 = r2.initialStatus     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.DexStore$OptimizationHistoryLog r10 = r7.getOptimizationHistoryLog()     // Catch:{ all -> 0x00fd }
            r8 = r25
            com.facebook.common.dextricks.OdexScheme$NeedOptimizationState r0 = r8.needOptimization(r0, r6, r10)     // Catch:{ all -> 0x00fd }
            boolean r0 = r0.needsOptimization()     // Catch:{ all -> 0x00fd }
            if (r0 != 0) goto L_0x0046
            java.lang.String r1 = "[opt] nothing to do: ART xdex already complete and no need to further optimize"
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00fd }
            goto L_0x00f5
        L_0x0046:
            com.facebook.common.dextricks.OdexSchemeArtXdex$OptimizationStateHolder r15 = new com.facebook.common.dextricks.OdexSchemeArtXdex$OptimizationStateHolder     // Catch:{ all -> 0x00fd }
            r16 = 1
            long r0 = r2.initialStatus     // Catch:{ all -> 0x00fd }
            r19 = 0
            r17 = r0
            r15.<init>(r16, r17, r19)     // Catch:{ all -> 0x00fd }
            r2.startOptimizing()     // Catch:{ all -> 0x00fd }
            int r10 = r7.optimizationAttemptNumber     // Catch:{ all -> 0x00fd }
            java.lang.String r1 = "[opt] Optimization attempt %d"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x00fd }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00fd }
            long r0 = r15.status     // Catch:{ all -> 0x00fd }
            boolean r0 = anyOptimizationDone(r0)     // Catch:{ all -> 0x00fd }
            r17 = r26
            if (r0 != 0) goto L_0x00bd
            r18 = r9
            r19 = r7
            r20 = r6
            r21 = r5
            r22 = r4
            r23 = r2
            r24 = r15
            r16 = r8
            r16.optimizeInitial(r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ all -> 0x00fd }
        L_0x0082:
            long r0 = r15.statusIntent     // Catch:{ all -> 0x00fd }
            r2.startCommitting(r0)     // Catch:{ all -> 0x00fd }
            boolean r0 = r15.success     // Catch:{ all -> 0x00fd }
            if (r0 == 0) goto L_0x00b5
            r4.renameOrThrow()     // Catch:{ all -> 0x00fd }
        L_0x008e:
            java.lang.String r3 = "[opt] new status 0x%x"
            long r0 = r15.status     // Catch:{ all -> 0x00fd }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x00fd }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r0)     // Catch:{ all -> 0x00fd }
            long r3 = r15.status     // Catch:{ all -> 0x00fd }
            long r0 = r15.statusIntent     // Catch:{ all -> 0x00fd }
            long r3 = r3 | r0
            r2.finishCommit(r3)     // Catch:{ all -> 0x00fd }
            java.lang.String r1 = "ART xdex optimization phase complete"
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00fd }
            boolean r0 = r15.success     // Catch:{ all -> 0x00fd }
            if (r0 == 0) goto L_0x00f5
            r25.saveOatFiles()     // Catch:{ all -> 0x00fd }
            goto L_0x00f5
        L_0x00b5:
            java.lang.String r1 = "[opt] failure to set up the optimization command"
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x00fd }
            goto L_0x008e
        L_0x00bd:
            r13 = 16
            r11 = -20721(0xffffffffffffaf0f, double:NaN)
            r0 = 3
            if (r10 > r0) goto L_0x00dd
            r1 = 3
            r18 = r9
            r19 = r7
            r20 = r6
            r21 = r5
            r22 = r4
            r23 = r2
            r24 = r15
            r16 = r8
            r16.optimizeFurther(r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x00d9 }
            goto L_0x0082
        L_0x00d9:
            r0 = move-exception
            if (r10 != r1) goto L_0x00fc
            goto L_0x00ea
        L_0x00dd:
            java.lang.String r1 = "[opt] Detected error but seem to be in a complete state"
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch:{ all -> 0x00fd }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x00fd }
            long r0 = r15.status     // Catch:{ all -> 0x00fd }
            long r0 = r0 & r11
            r15.status = r0     // Catch:{ all -> 0x00fd }
            goto L_0x00ef
        L_0x00ea:
            long r0 = r15.status     // Catch:{ all -> 0x00fd }
            long r0 = r0 & r11
            r15.status = r0     // Catch:{ all -> 0x00fd }
        L_0x00ef:
            long r0 = r0 | r13
            r15.status = r0     // Catch:{ all -> 0x00fd }
            r15.success = r3     // Catch:{ all -> 0x00fd }
            goto L_0x0082
        L_0x00f5:
            r2.close()     // Catch:{ all -> 0x0104 }
            r5.close()
            return
        L_0x00fc:
            throw r0     // Catch:{ all -> 0x00fd }
        L_0x00fd:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ff }
        L_0x00ff:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0103 }
        L_0x0103:
            throw r0     // Catch:{ all -> 0x0104 }
        L_0x0104:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0106 }
        L_0x0106:
            r0 = move-exception
            if (r5 == 0) goto L_0x010c
            r5.close()     // Catch:{ all -> 0x010c }
        L_0x010c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.optimize(android.content.Context, com.facebook.common.dextricks.DexStore, com.facebook.common.dextricks.DexStore$OptimizationSession):void");
    }

    public OdexSchemeArtXdex(Context context, DexManifest.Dex[] dexArr, ResProvider resProvider, long j) {
        super(getOdexFlags(), makeExpectedFileList(dexArr, j));
        boolean z = false;
        this.mDexes = dexArr;
        this.mResProvider = resProvider;
        this.mIsLoadable = (j & STATE_MASK) != 0 ? true : z;
        this.mPGOProfileUtil = AnonymousClass095.A00(context);
        this.mDexUnpacker = new DexUnpacker(context, resProvider);
        this.mOatmealPath = null;
        this.oatFiles = new ArrayList();
    }

    private boolean areDexesTruncated(DexStore dexStore, File file, DexManifest.Dex[] dexArr) {
        int i = dexStore.readConfig().artTruncatedDexSize;
        if (i <= 0) {
            return false;
        }
        for (DexManifest.Dex dex : dexArr) {
            if (!dexAppearsTruncated(dex, file, i)) {
                Mlog.w("Dex %s is not truncated", dex.assetName);
                return false;
            }
        }
        Mlog.safeFmt("All dexes appear truncated", new Object[0]);
        return true;
    }

    private void checkTmpOatFileLength(File file) {
        if (file.exists() && file.length() == 0) {
            throw new RuntimeException(file + " is an impossibly short oat file");
        }
    }

    public static void clearCannotTruncateDexesFlag(File file) {
        Mlog.safeFmt("Clearing flag to not truncate dex files at root: %s", file.getAbsolutePath());
        File makeIgnoreDirtyCheckFile = DexStoreUtils.makeIgnoreDirtyCheckFile(file, "cannot_trunc_dex.flg");
        if (!makeIgnoreDirtyCheckFile.exists()) {
            Mlog.w("Dex truncation file does not exist. This is likely an error", new Object[0]);
        }
        if (!makeIgnoreDirtyCheckFile.delete() && makeIgnoreDirtyCheckFile.exists()) {
            Mlog.w("Was not able to delete cannot truncate dexes file", new Object[0]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String convertClassToDotForm(String str) {
        return str.substring(1, str.length() - 1).replace('/', '.');
    }

    public static Context getAppContext(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (applicationContext == null) {
            return context;
        }
        return applicationContext;
    }

    private String getArtImageLocation() {
        String artImageLocation = DalvikInternals.getArtImageLocation();
        if (artImageLocation == null || BuildConfig.FLAVOR.equals(artImageLocation)) {
            return AnonymousClass08S.A0P("/data/dalvik-cache/", OdexSchemeArtTurbo.getArch(), "/system@framework@boot.art");
        }
        return artImageLocation;
    }

    private String getOatVersion() {
        String str;
        String oatFormatVersion = DalvikInternals.getOatFormatVersion();
        if (oatFormatVersion != null && !oatFormatVersion.isEmpty()) {
            return oatFormatVersion;
        }
        int i = Build.VERSION.SDK_INT;
        if (i != 10000) {
            switch (i) {
                case 2:
                case 3:
                case 4:
                case 5:
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case 8:
                case Process.SIGKILL /*9*/:
                case AnonymousClass1Y3.A01 /*10*/:
                case AnonymousClass1Y3.A02 /*11*/:
                case AnonymousClass1Y3.A03 /*12*/:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case Process.SIGCONT /*18*/:
                case Process.SIGSTOP /*19*/:
                case 20:
                    break;
                case AnonymousClass1Y3.A05 /*21*/:
                    return "039";
                case AnonymousClass1Y3.A06 /*22*/:
                    return "045";
                case 23:
                    return "064";
                case AnonymousClass1Y3.A07 /*24*/:
                    return "079";
                case 25:
                    return "088";
                case 26:
                    return "124";
                case AnonymousClass1Y3.A09 /*27*/:
                    return "131";
                default:
                    str = "unknown api version: VERSION.SDK_INT = ";
                    break;
            }
            Mlog.e(AnonymousClass08S.A09(str, i), new Object[0]);
            return "039";
        }
        str = "oatmeal should be used on Dalvik. VERSION.SDK_INT = ";
        Mlog.e(AnonymousClass08S.A09(str, i), new Object[0]);
        return "039";
    }

    private int getTruncatedSize(DexStore dexStore) {
        return dexStore.readConfig().artTruncatedDexSize;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        if (r5 >= 28) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List makeExpectedFileInfoList(com.facebook.common.dextricks.DexManifest.Dex[] r7, long r8) {
        /*
            boolean r6 = anyOptimizationDone(r8)
            int r5 = android.os.Build.VERSION.SDK_INT
            java.lang.String r4 = "oat"
            r2 = 0
            r3 = 26
            if (r5 < r3) goto L_0x0012
            r1 = 28
            r0 = r4
            if (r5 < r1) goto L_0x0013
        L_0x0012:
            r0 = r2
        L_0x0013:
            if (r6 != 0) goto L_0x0016
            r0 = r2
        L_0x0016:
            java.util.List r2 = com.facebook.common.dextricks.OdexSchemeArtTurbo.makeExpectedFileInfoList(r7, r0)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            java.lang.String r0 = r2.toString()
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "makeExpectedFile: are oat around: %s expected files: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            if (r6 == 0) goto L_0x0037
            if (r5 < r3) goto L_0x0045
            com.facebook.common.dextricks.ExpectedFileInfo r0 = new com.facebook.common.dextricks.ExpectedFileInfo
            r0.<init>(r4)
            r2.add(r0)
        L_0x0037:
            java.lang.String r0 = r2.toString()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "makeExpectedFile: RETURN expected files: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return r2
        L_0x0045:
            java.util.ArrayList r3 = new java.util.ArrayList
            int r0 = r2.size()
            int r0 = r0 << 1
            r3.<init>(r0)
            r3.addAll(r2)
            java.util.Iterator r2 = r2.iterator()
        L_0x0057:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0074
            java.lang.Object r0 = r2.next()
            com.facebook.common.dextricks.ExpectedFileInfo r0 = (com.facebook.common.dextricks.ExpectedFileInfo) r0
            com.facebook.common.dextricks.ExpectedFileInfo r1 = new com.facebook.common.dextricks.ExpectedFileInfo
            java.lang.String r0 = r0.toExpectedFileString()
            java.lang.String r0 = oatNameFromDexName(r0)
            r1.<init>(r0)
            r3.add(r1)
            goto L_0x0057
        L_0x0074:
            r2 = r3
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.makeExpectedFileInfoList(com.facebook.common.dextricks.DexManifest$Dex[], long):java.util.List");
    }

    private static String[] makeExpectedFileList(DexManifest.Dex[] dexArr, long j) {
        return OdexSchemeArtTurbo.makeExpectedFileListFrom(makeExpectedFileInfoList(dexArr, j));
    }

    private static File makeTmpSubDirectory(DexStore.TmpDir tmpDir, String str) {
        File file = new File(tmpDir.directory, AnonymousClass08S.A0P(str, "-", UUID.randomUUID().toString().replace("-", BuildConfig.FLAVOR)));
        Fs.mkdirOrThrow(file);
        return file;
    }

    public static boolean markCannotTruncateDexesFlag(File file) {
        Mlog.safeFmt("Marking cannot truncate dex files at root: %s", file.getAbsolutePath());
        File makeIgnoreDirtyCheckFile = DexStoreUtils.makeIgnoreDirtyCheckFile(file, "cannot_trunc_dex.flg");
        if (makeIgnoreDirtyCheckFile.exists()) {
            Mlog.w("Dex truncation file already exists. This is likely an error", new Object[0]);
        }
        try {
            boolean createNewFile = makeIgnoreDirtyCheckFile.createNewFile();
            if (createNewFile) {
                return createNewFile;
            }
            Mlog.w("Was not successful creating cannot truncate dexes file", new Object[0]);
            return createNewFile;
        } catch (IOException e) {
            Mlog.w(e, "Was not able to create cannot truncate dexes file", new Object[0]);
            return false;
        }
    }

    public static boolean mixedNeeded(long j) {
        if (optimizationCompleted(j) || (j & STATE_MIXED_NEEDED) == 0) {
            return false;
        }
        return true;
    }

    private static String oatNameFromExpectedFileInfo(ExpectedFileInfo expectedFileInfo) {
        String str;
        if (expectedFileInfo.hasDex()) {
            str = expectedFileInfo.dex.makeDexName();
        } else {
            str = expectedFileInfo.rawFile;
        }
        return oatNameFromDexName(str);
    }

    private static String readProgramOutputFileSafely(RandomAccessFile randomAccessFile) {
        try {
            return Fs.readProgramOutputFile(randomAccessFile);
        } catch (IOException unused) {
            return null;
        }
    }

    private boolean shouldTruncateDexesNow(File file, File file2, long j, int i) {
        if (!needsTruncation(file2, i) || !optimizationCompleted(j)) {
            return false;
        }
        return !getCannotTruncateDexesFlag(file);
    }

    public boolean loadNotOptimized(long j) {
        return !anyOptimizationDone(j);
    }

    public OdexScheme.NeedOptimizationState needOptimization(long j, DexStore.Config config, DexStore.OptimizationHistoryLog optimizationHistoryLog) {
        Mlog.safeFmt("NeedOptimization: opt complete: %s last scheme: %s pgo: %s opt history: %s", Boolean.valueOf(optimizationCompleted(j)), Boolean.valueOf(optimizationCompleted(optimizationHistoryLog.schemeStatus)), Boolean.valueOf(config.tryPeriodicPgoCompilation), optimizationHistoryLog);
        if (!config.tryPeriodicPgoCompilation || !optimizationHistoryLog.lastCompilationSessionWasASuccess()) {
            return OdexScheme.NeedOptimizationState.shouldOptimize(!optimizationCompleted(j));
        }
        if (!optimizationCompleted(optimizationHistoryLog.schemeStatus)) {
            return OdexScheme.NeedOptimizationState.NEED_OPTIMIZATION;
        }
        long timeDeltaFromLastCompilationSessionMs = optimizationHistoryLog.timeDeltaFromLastCompilationSessionMs();
        Mlog.safeFmt("NeedOptimization: timeDelta %d ms min interval: %d ms", Long.valueOf(timeDeltaFromLastCompilationSessionMs), Long.valueOf(config.minTimeBetweenPgoCompilationMs));
        if (timeDeltaFromLastCompilationSessionMs == -1 || timeDeltaFromLastCompilationSessionMs < config.minTimeBetweenPgoCompilationMs) {
            return OdexScheme.NeedOptimizationState.NO_OPTIMIZATION_NEEDED;
        }
        Mlog.safeFmt("NeedOptimization: Time to run additional pgo optimizations", new Object[0]);
        return OdexScheme.NeedOptimizationState.NEED_REOPTIMIZATION;
    }

    private static boolean dexAppearsTruncated(DexManifest.Dex dex, File file, int i) {
        String makeDexName = dex.makeDexName();
        File file2 = new File(file, makeDexName);
        if (!file2.exists()) {
            Mlog.w("Dex [asset: %s] %s seems not to exist", dex.assetName, makeDexName);
        }
        return dexAppearsTruncated(file2, i);
    }

    private static boolean dexAppearsTruncated(File file, int i) {
        return file.length() <= ((long) i);
    }

    private List getDexInfos(Context context, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.Config config, DexStore.TmpDir tmpDir, Renamer renamer, List list, boolean z, boolean z2, byte b, File file, DexManifest.Dex[] dexArr, AtomicReference atomicReference) {
        boolean z3 = z2;
        Mlog.safeFmt("[opt] dex2oat - get dex infos for mm: %s pgo comp: %s", Boolean.valueOf(z), Boolean.valueOf(z3));
        DexStore dexStore2 = dexStore;
        DexManifest.Dex[] dexArr2 = dexArr;
        File file2 = file;
        try {
            DexStore.TmpDir tmpDir2 = tmpDir;
            if (areDexesTruncated(dexStore, file2, dexArr2)) {
                Mlog.safeFmt("Dexes appear truncated. Re-unpacking secondary dexes", new Object[0]);
                if (file2 == dexStore.root) {
                    return getNewInitialOptimizedSecondaryDexes(context, dexStore2, optimizationSession, config, tmpDir2, renamer, atomicReference);
                }
                throw new IllegalStateException("Can only copy temp secondary dexes from the root");
            }
            List list2 = list;
            if (!z) {
                return list;
            }
            return getMixedModeDexInfos(list2, tmpDir2, z3, b, dexArr2);
        } catch (IOException e) {
            Mlog.w(e, "Could not get dex infos due to IO error. Bailing...", new Object[0]);
            return null;
        }
    }

    private long initialDexOptimizations(Context context, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.Config config, DexStore.TmpDir tmpDir, Renamer renamer, File file, List list) {
        DexStore.Config config2 = config;
        Renamer renamer2 = renamer;
        DexStore.TmpDir tmpDir2 = tmpDir;
        File file2 = file;
        List list2 = list;
        Context context2 = context;
        DexStore dexStore2 = dexStore;
        DexStore.OptimizationSession optimizationSession2 = optimizationSession;
        if (config2.enableOatmeal) {
            optimizeOatmeal(context2, renamer2, dexStore2, optimizationSession2, tmpDir2, false, file2, list2);
            Mlog.safeFmt("[opt] first oatmeal run", new Object[0]);
            if (config2.enableQuickening) {
                if (config2.enableOatmealQuickening) {
                    return STATE_OATMEAL_QUICKENING_NEEDED;
                }
                return STATE_DEX2OAT_QUICKENING_NEEDED;
            } else if (anyCompilationNeeded(config2)) {
                return getCompilationStatusFlags(config2);
            } else {
                return 16;
            }
        } else {
            Mlog.safeFmt("[opt] first dex2oat run", new Object[0]);
            optimizeDex2Oat(context2, dexStore2, renamer2, optimizationSession2, config2, tmpDir2, config2.enableArtVerifyNone, false, false, false, false, (byte) 0, (byte) 0, false, file2, list2);
            return 16;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        if (r21 == false) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001f, code lost:
        if (r63 != false) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean innerOptimizeDex2Oat(android.content.Context r50, com.facebook.common.dextricks.DexStore r51, com.facebook.common.dextricks.OdexSchemeArtXdex.Renamer r52, com.facebook.common.dextricks.DexStore.OptimizationSession r53, com.facebook.common.dextricks.DexStore.Config r54, com.facebook.common.dextricks.DexStore.TmpDir r55, boolean r56, boolean r57, boolean r58, boolean r59, boolean r60, byte r61, byte r62, boolean r63, java.io.File r64, java.util.List r65) {
        /*
            r49 = this;
            r1 = r65
            r3 = r49
            r37 = r64
            r2 = r37
            r14 = r53
            com.facebook.common.dextricks.OptimizationConfiguration r0 = r14.config
            int r4 = r0.flags
            r0 = 1
            r4 = r4 & r0
            r22 = 0
            if (r4 == 0) goto L_0x0016
            r22 = 1
        L_0x0016:
            X.095 r0 = r3.mPGOProfileUtil
            boolean r21 = X.AnonymousClass095.A03(r0)
            if (r60 != 0) goto L_0x0021
            r0 = 0
            if (r63 == 0) goto L_0x0022
        L_0x0021:
            r0 = 1
        L_0x0022:
            if (r63 == 0) goto L_0x0028
            r32 = 1
            if (r21 != 0) goto L_0x002a
        L_0x0028:
            r32 = 0
        L_0x002a:
            if (r0 == 0) goto L_0x003f
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r60)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r63)
            java.lang.Object[] r4 = new java.lang.Object[]{r4, r0}
            if (r21 == 0) goto L_0x0356
            java.lang.String r0 = "[opt] Trying PGO compilation [mm: %s periodic recomp: %s] and found PGO profile file"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r4)
        L_0x003f:
            r6 = 0
            r46 = r55
            r47 = r52
            r48 = r50
            r31 = r57
            r4 = r51
            if (r65 == 0) goto L_0x031f
            r3 = r1
            r13 = r2
        L_0x004e:
            r0 = r59 ^ 1
            java.lang.String r20 = getBootClassPathValue(r4, r0)
            if (r59 == 0) goto L_0x031c
            java.lang.String r11 = getClassPathValue(r4, r13, r1)
        L_0x005a:
            if (r3 == 0) goto L_0x036f
            int r0 = r3.size()
            if (r0 == 0) goto L_0x036f
            com.facebook.common.dextricks.Dex2oatLogcatParser r19 = new com.facebook.common.dextricks.Dex2oatLogcatParser
            r0 = r46
            java.io.File r1 = r0.directory
            java.lang.String r15 = "dex2oat"
            r0 = r19
            r0.<init>(r15, r1)
            java.util.Iterator r18 = r3.iterator()
        L_0x0073:
            boolean r0 = r18.hasNext()
            if (r0 == 0) goto L_0x036d
            java.lang.Object r1 = r18.next()
            com.facebook.common.dextricks.ExpectedFileInfo r1 = (com.facebook.common.dextricks.ExpectedFileInfo) r1
            java.lang.Object[] r3 = new java.lang.Object[]{r1}
            java.lang.String r0 = "[opt] dex2oat - go for expected file %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r3)
            java.io.File r39 = r1.getFile(r13)
            java.lang.Object[] r3 = new java.lang.Object[]{r39}
            java.lang.String r0 = "[opt] dex2oat - go for dex %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r3)
            java.lang.String r3 = oatNameFromExpectedFileInfo(r1)
            java.io.File r4 = new java.io.File
            r4.<init>(r13, r3)
            java.io.File r0 = new java.io.File
            r5 = r46
            java.io.File r5 = r5.directory
            r0.<init>(r5, r3)
            java.io.File r5 = new java.io.File
            r5.<init>(r2, r3)
            r3 = r49
            java.util.ArrayList r6 = r3.oatFiles
            r6.add(r4)
            boolean r6 = r39.exists()
            java.lang.Boolean r38 = java.lang.Boolean.valueOf(r6)
            boolean r6 = r4.exists()
            java.lang.Boolean r40 = java.lang.Boolean.valueOf(r6)
            boolean r6 = r0.exists()
            java.lang.Boolean r42 = java.lang.Boolean.valueOf(r6)
            boolean r6 = r5.exists()
            java.lang.Boolean r44 = java.lang.Boolean.valueOf(r6)
            r35 = r1
            r36 = r2
            r41 = r4
            r43 = r0
            r45 = r5
            java.lang.Object[] r6 = new java.lang.Object[]{r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45}
            java.lang.String r1 = "[opt] Current State: \n Expected File Info %s \n Root: %s \n Potential Root: %s \n dex-file [exists: %s]: %s \n oat-location [exists: %s]: %s \n oat-file [exists: %s]: %s \n oat-file-overwrite [exists: %s]: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r6)
            com.facebook.forker.ProcessBuilder r1 = new com.facebook.forker.ProcessBuilder
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r6 = "--oat-file="
            r7.append(r6)
            r7.append(r0)
            java.lang.String r23 = r7.toString()
            java.lang.String r6 = "--oat-location="
            java.lang.String r5 = r5.getPath()
            java.lang.String r24 = X.AnonymousClass08S.A0J(r6, r5)
            java.lang.String r5 = "--dex-file="
            java.lang.String r6 = r39.getPath()
            java.lang.String r25 = X.AnonymousClass08S.A0J(r5, r6)
            java.lang.String r26 = "--no-watch-dog"
            java.lang.String r27 = "--dump-timing"
            java.lang.String r28 = "--dump-stats"
            java.lang.String[] r6 = new java.lang.String[]{r23, r24, r25, r26, r27, r28}
            java.lang.String r5 = "/system/bin/dex2oat"
            r1.<init>(r5, r6)
            java.lang.String r6 = "BOOTCLASSPATH"
            r5 = r20
            r1.setenv(r6, r5)
            android.content.Context r5 = getAppContext(r48)
            java.io.File r5 = com.facebook.forker.ProcessBuilder.genDefaultTmpDir(r5)
            r1.mTmpDir = r5
            if (r11 == 0) goto L_0x013d
            java.lang.String r5 = "CLASSPATH"
            r1.setenv(r5, r11)
            java.lang.Object[] r6 = new java.lang.Object[]{r11}
            java.lang.String r5 = "dex2oat CLASSPATH: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r6)
        L_0x013d:
            java.lang.Object[] r6 = new java.lang.Object[]{r20}
            java.lang.String r5 = "dex2oat BOOTCLASSPATH: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r6)
            java.lang.String r5 = "dalvik.vm.dex2oat-Xms"
            java.lang.String r6 = android.os.SystemProperties.get(r5)
            boolean r5 = r6.isEmpty()
            java.lang.String r7 = "--runtime-arg"
            if (r5 != 0) goto L_0x0164
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r7)
            java.lang.String r5 = "-Xms"
            java.lang.String r6 = X.AnonymousClass08S.A0J(r5, r6)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x0164:
            java.lang.String r5 = "dalvik.vm.dex2oat-Xmx"
            java.lang.String r6 = android.os.SystemProperties.get(r5)
            boolean r5 = r6.isEmpty()
            if (r5 != 0) goto L_0x0180
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r7)
            java.lang.String r5 = "-Xmx"
            java.lang.String r6 = X.AnonymousClass08S.A0J(r5, r6)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x0180:
            com.facebook.common.dextricks.DexStore$Config r9 = r14.dexStoreConfig
            java.lang.String r17 = "everything-profile"
            java.lang.String r12 = "everything"
            java.lang.String r6 = "--profile-file="
            java.lang.String r8 = "--compiler-filter="
            if (r57 == 0) goto L_0x0275
            r5 = 0
            java.lang.Object[] r7 = new java.lang.Object[r5]
            java.lang.String r5 = "[opt] Enabling compilation for mixed mode"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r7)
            java.lang.String r7 = "speed"
            if (r60 == 0) goto L_0x0271
            if (r21 == 0) goto L_0x035d
            boolean r16 = X.AnonymousClass095.A07
            r5 = 1
            r10 = r61
            if (r10 == r5) goto L_0x0267
            r5 = 2
            if (r10 == r5) goto L_0x0260
            r5 = 3
            if (r10 == r5) goto L_0x0256
            if (r16 == 0) goto L_0x01ab
            java.lang.String r7 = "speed-profile"
        L_0x01ab:
            addOldPgoDex2OatParams(r1)
            java.io.File r10 = com.facebook.common.dextricks.DexStoreUtils.getCurrentMainDexStorePgoProfile(r48)
            java.lang.String r5 = r10.getAbsolutePath()
            java.lang.String r6 = X.AnonymousClass08S.A0J(r6, r5)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
            java.lang.Object[] r6 = new java.lang.Object[]{r7, r10}
            java.lang.String r5 = "[opt] Using PGO profile for mixed mode compilation [compiler filter: %s] at %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r6)
            java.lang.String r26 = "dex2oat-mixedmode-pgo"
        L_0x01ca:
            java.lang.String r6 = X.AnonymousClass08S.A0J(r8, r7)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x01d3:
            if (r22 == 0) goto L_0x01dc
            java.lang.String r6 = "-j1"
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x01dc:
            int r6 = r9.artHugeMethodMax
            if (r6 < 0) goto L_0x01eb
            java.lang.String r5 = "--huge-method-max="
            java.lang.String r6 = X.AnonymousClass08S.A09(r5, r6)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x01eb:
            int r6 = r9.artLargeMethodMax
            if (r6 < 0) goto L_0x01fa
            java.lang.String r5 = "--large-method-max="
            java.lang.String r6 = X.AnonymousClass08S.A09(r5, r6)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x01fa:
            int r6 = r9.artSmallMethodMax
            if (r6 < 0) goto L_0x0209
            java.lang.String r5 = "--small-method-max="
            java.lang.String r6 = X.AnonymousClass08S.A09(r5, r6)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x0209:
            int r6 = r9.artTinyMethodMax
            if (r6 < 0) goto L_0x0218
            java.lang.String r5 = "--tiny-method-max="
            java.lang.String r6 = X.AnonymousClass08S.A09(r5, r6)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x0218:
            com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatHookInfo r7 = getMirandaFixLibHook()
            r6 = 0
            r5 = r48
            com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatHookInfo r5 = r3.getDex2OatLibHooks(r5, r14)
            com.facebook.common.dextricks.OdexSchemeArtXdex$Dex2OatHookInfo[] r5 = new com.facebook.common.dextricks.OdexSchemeArtXdex.Dex2OatHookInfo[]{r7, r5}
            initAllDex2OatHooks(r1, r5)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r5 = "[opt] Running compiler: "
            r7.<init>(r5)
            r7.append(r1)
            java.lang.String r5 = r7.toString()
            java.lang.Object[] r6 = new java.lang.Object[r6]
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r6)
            r5 = r47
            r5.addFile(r0, r4)
            r25 = r48
            r27 = r14
            r23 = r3
            r24 = r1
            r28 = r19
            r29 = r46
            r23.startOptimizerProcess(r24, r25, r26, r27, r28, r29)
            r3.checkTmpOatFileLength(r0)
            goto L_0x0073
        L_0x0256:
            if (r16 == 0) goto L_0x025c
            java.lang.String r7 = "verify-profile"
            goto L_0x01ab
        L_0x025c:
            java.lang.String r7 = "verify-none"
            goto L_0x01ab
        L_0x0260:
            r7 = r12
            if (r16 == 0) goto L_0x01ab
            r7 = r17
            goto L_0x01ab
        L_0x0267:
            if (r16 == 0) goto L_0x026d
            java.lang.String r7 = "space-profile"
            goto L_0x01ab
        L_0x026d:
            java.lang.String r7 = "space"
            goto L_0x01ab
        L_0x0271:
            java.lang.String r26 = "dex2oat-mixedmode"
            goto L_0x01ca
        L_0x0275:
            if (r32 == 0) goto L_0x02af
            java.io.File r10 = com.facebook.common.dextricks.DexStoreUtils.getCurrentMainDexStorePgoProfile(r48)
            if (r10 == 0) goto L_0x02ad
            java.io.File r5 = r10.getAbsoluteFile()
        L_0x0281:
            java.lang.Object[] r7 = new java.lang.Object[]{r5}
            java.lang.String r5 = "[opt] Enabling compilation for pgo %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r7)
            java.lang.String r5 = r10.getAbsolutePath()
            java.lang.String r6 = X.AnonymousClass08S.A0J(r6, r5)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
            boolean r5 = X.AnonymousClass095.A07
            if (r5 == 0) goto L_0x029d
            r12 = r17
        L_0x029d:
            java.lang.String r6 = X.AnonymousClass08S.A0J(r8, r12)
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
            addOldPgoDex2OatParams(r1)
            java.lang.String r26 = "dex2oat-pgo"
            goto L_0x01d3
        L_0x02ad:
            r5 = 0
            goto L_0x0281
        L_0x02af:
            if (r58 == 0) goto L_0x02c4
            r5 = 0
            java.lang.Object[] r6 = new java.lang.Object[r5]
            java.lang.String r5 = "[opt] Enabling quickening"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r6)
            java.lang.String r6 = "--compiler-filter=interpret-only"
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
            java.lang.String r26 = "dex2oat-quicken"
            goto L_0x01d3
        L_0x02c4:
            r5 = 0
            java.lang.Object[] r6 = new java.lang.Object[r5]
            if (r56 == 0) goto L_0x02d9
            java.lang.String r5 = "[opt] Enabling verify-none option for art"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r6)
            java.lang.String r6 = "--compiler-filter=verify-none"
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
        L_0x02d5:
            r26 = r15
            goto L_0x01d3
        L_0x02d9:
            java.lang.String r5 = "[opt] Loading compiler system flags"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r6)
            r3.setCompilerFilter(r9, r1)
            java.lang.String r5 = "dalvik.vm.dex2oat-flags"
            java.lang.String r6 = android.os.SystemProperties.get(r5)
            if (r6 == 0) goto L_0x0319
            boolean r5 = r6.isEmpty()
            if (r5 != 0) goto L_0x0319
            r5 = r6
        L_0x02f0:
            java.lang.Object[] r7 = new java.lang.Object[]{r5}
            java.lang.String r5 = "[opt] compiler defaults system flags are %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r7)
            boolean r5 = r6.isEmpty()
            if (r5 != 0) goto L_0x02d5
            android.text.TextUtils$SimpleStringSplitter r7 = new android.text.TextUtils$SimpleStringSplitter
            r5 = 32
            r7.<init>(r5)
            r7.setString(r6)
        L_0x0309:
            boolean r5 = r7.hasNext()
            if (r5 == 0) goto L_0x02d5
            java.lang.String r6 = r7.next()
            java.util.ArrayList r5 = r1.mArgv
            r5.add(r6)
            goto L_0x0309
        L_0x0319:
            java.lang.String r5 = "<none>"
            goto L_0x02f0
        L_0x031c:
            r11 = 0
            goto L_0x005a
        L_0x031f:
            com.facebook.common.dextricks.DexManifest$Dex[] r5 = r3.mDexes
            r0 = 0
            java.util.List r1 = makeExpectedFileInfoList(r5, r0)
            java.util.concurrent.atomic.AtomicReference r5 = new java.util.concurrent.atomic.AtomicReference
            r5.<init>(r6)
            com.facebook.common.dextricks.DexManifest$Dex[] r0 = r3.mDexes
            r26 = r14
            r28 = r46
            r13 = r2
            r27 = r54
            r33 = r62
            r25 = r4
            r29 = r47
            r30 = r1
            r34 = r2
            r35 = r0
            r36 = r5
            r23 = r3
            r24 = r48
            java.util.List r3 = r23.getDexInfos(r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36)
            java.lang.Object r2 = r5.get()
            java.io.File r2 = (java.io.File) r2
            if (r2 != 0) goto L_0x004e
            r2 = r13
            goto L_0x004e
        L_0x0356:
            java.lang.String r0 = "[opt] did NOT find PGO profile file [mm: %s periodic: %s]"
            com.facebook.common.dextricks.Mlog.w(r0, r4)
            goto L_0x003f
        L_0x035d:
            java.lang.String r2 = "OdexSchemeArtXdex_MissingPGO"
            java.lang.String r1 = "PGO file could not be located"
            r0 = 0
            com.facebook.common.dextricks.DexTricksErrorReporter.reportSampledSoftError(r2, r1, r0)
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r0 = "Couldn't find PGO profile for mixed mode compilation"
            r1.<init>(r0)
            throw r1
        L_0x036d:
            r0 = 1
            return r0
        L_0x036f:
            r2 = 0
            java.lang.Object[] r1 = new java.lang.Object[r2]
            java.lang.String r0 = "[opt] no dex file to compile"
            com.facebook.common.dextricks.Mlog.w(r0, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.innerOptimizeDex2Oat(android.content.Context, com.facebook.common.dextricks.DexStore, com.facebook.common.dextricks.OdexSchemeArtXdex$Renamer, com.facebook.common.dextricks.DexStore$OptimizationSession, com.facebook.common.dextricks.DexStore$Config, com.facebook.common.dextricks.DexStore$TmpDir, boolean, boolean, boolean, boolean, boolean, byte, byte, boolean, java.io.File, java.util.List):boolean");
    }

    public static List makeMixedModeFileList(DexManifest.Dex[] dexArr, DexSelector dexSelector, List list) {
        int length;
        if (list == null || (length = dexArr.length) == list.size()) {
            int length2 = dexArr.length;
            ArrayList arrayList = new ArrayList(length2);
            for (int i = 0; i < length2; i++) {
                DexManifest.Dex dex = dexArr[i];
                if (dex != null && dexSelector.select(dex)) {
                    Mlog.safeFmt("[mixed_mode] selected dex = { %s, %s, %s }", dex.canaryClass, dex.hash, dex.assetName);
                    if (list == null) {
                        arrayList.add(new ExpectedFileInfo(dex));
                    } else if (i < list.size()) {
                        list.set(i, new ExpectedFileInfo(dex));
                    }
                }
            }
            return list != null ? list : arrayList;
        }
        throw new IllegalStateException(String.format("Dexes array [size: %d] needs to be the same size as replaceDexNames array [size: %d]", Integer.valueOf(length), Integer.valueOf(list.size())));
    }

    public static List makeMixedModeFileList(DexManifest.Dex[] dexArr, String str, List list) {
        final String convertClassToDotForm = convertClassToDotForm(str);
        return makeMixedModeFileList(dexArr, new DexSelector() {
            public boolean select(DexManifest.Dex dex) {
                Mlog.w("[mixed_mode] comparing %s and %s", dex.canaryClass, convertClassToDotForm);
                return dex.canaryClass.equals(convertClassToDotForm);
            }
        }, list);
    }

    private boolean optimizeDex2Oat(Context context, DexStore dexStore, Renamer renamer, DexStore.OptimizationSession optimizationSession, DexStore.Config config, DexStore.TmpDir tmpDir, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, byte b, byte b2, boolean z6) {
        return optimizeDex2Oat(context, dexStore, renamer, optimizationSession, config, tmpDir, z, z2, z3, z4, z5, b, b2, z6, null, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean optimizeDex2Oat(android.content.Context r21, com.facebook.common.dextricks.DexStore r22, com.facebook.common.dextricks.OdexSchemeArtXdex.Renamer r23, com.facebook.common.dextricks.DexStore.OptimizationSession r24, com.facebook.common.dextricks.DexStore.Config r25, com.facebook.common.dextricks.DexStore.TmpDir r26, boolean r27, boolean r28, boolean r29, boolean r30, boolean r31, byte r32, byte r33, boolean r34, java.io.File r35, java.util.List r36) {
        /*
            r20 = this;
            r5 = r22
            r0 = r35
            if (r35 != 0) goto L_0x0008
            java.io.File r0 = r5.root
        L_0x0008:
            com.facebook.common.dextricks.OdexSchemeArtXdex$CriticalCannotTruncateDexesSection r1 = new com.facebook.common.dextricks.OdexSchemeArtXdex$CriticalCannotTruncateDexesSection
            r3 = r20
            r2 = r34
            r1.<init>(r0, r2)
            r9 = r26
            r8 = r25
            r7 = r24
            r6 = r23
            r4 = r21
            r10 = r27
            r11 = r28
            r12 = r29
            r13 = r30
            r19 = r36
            r16 = r33
            r15 = r32
            r14 = r31
            r17 = r2
            r18 = r0
            boolean r0 = r3.innerOptimizeDex2Oat(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ all -> 0x0037 }
            r1.close()
            return r0
        L_0x0037:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x003d }
        L_0x003d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtXdex.optimizeDex2Oat(android.content.Context, com.facebook.common.dextricks.DexStore, com.facebook.common.dextricks.OdexSchemeArtXdex$Renamer, com.facebook.common.dextricks.DexStore$OptimizationSession, com.facebook.common.dextricks.DexStore$Config, com.facebook.common.dextricks.DexStore$TmpDir, boolean, boolean, boolean, boolean, boolean, byte, byte, boolean, java.io.File, java.util.List):boolean");
    }

    private void optimizeFurther(Context context, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.Config config, DexStore.TmpDir tmpDir, Renamer renamer, DexStore.OptimizationSession.Job job, OptimizationStateHolder optimizationStateHolder) {
        String str;
        String str2;
        long j;
        long j2;
        long j3;
        DexStore.OptimizationSession optimizationSession2 = optimizationSession;
        DexStore.OptimizationHistoryLog optimizationHistoryLog = optimizationSession2.getOptimizationHistoryLog();
        OptimizationStateHolder optimizationStateHolder2 = optimizationStateHolder;
        long j4 = optimizationStateHolder2.status;
        boolean pgoProfileRecompilationNeeded = pgoProfileRecompilationNeeded(optimizationHistoryLog, j4);
        Mlog.safeFmt("[opt] Optimizing further quickeningNeeded: %s opt completed: %s mm: %s pgo: %s", Boolean.valueOf(quickeningNeeded(j4)), Boolean.valueOf(optimizationCompleted(j4)), Boolean.valueOf(mixedNeeded(j4)), Boolean.valueOf(pgoProfileRecompilationNeeded));
        long j5 = optimizationStateHolder2.status;
        long j6 = 16;
        Renamer renamer2 = renamer;
        DexStore dexStore2 = dexStore;
        Context context2 = context;
        DexStore.Config config2 = config;
        DexStore.TmpDir tmpDir2 = tmpDir;
        if (quickeningNeeded(j5)) {
            Mlog.safeFmt("[opt] quickening", new Object[0]);
            if (config2.enableOatmealQuickening) {
                optimizationStateHolder2.success = optimizeOatmeal(context2, renamer2, dexStore2, optimizationSession2, tmpDir2, true);
                optimizationStateHolder2.statusIntent = STATE_OATMEAL_QUICK_ATTEMPTED;
            } else if (config2.enableDex2OatQuickening) {
                optimizationStateHolder2.success = optimizeDex2Oat(context2, dexStore2, renamer2, optimizationSession2, config2, tmpDir2, config2.enableArtVerifyNone, false, true, true, false, (byte) 0, (byte) 0, false);
                optimizationStateHolder2.statusIntent = STATE_DEX2OAT_QUICK_ATTEMPTED;
            }
            long j7 = optimizationStateHolder2.status;
            if (anyCompilationNeeded(config2)) {
                j6 = getCompilationStatusFlags(config2);
            }
            long j8 = j7 | j6;
            optimizationStateHolder2.status = j8;
            if (optimizationStateHolder2.success) {
                if (config2.enableOatmealQuickening) {
                    j3 = STATE_OATMEAL_QUICKENING_NEEDED;
                } else {
                    j3 = STATE_DEX2OAT_QUICKENING_NEEDED;
                }
                optimizationStateHolder2.status = j8 & (j3 ^ -1);
            }
            Mlog.safeFmt("[opt] new status 0x%x", Long.valueOf(optimizationStateHolder2.status));
        } else if (mixedNeeded(j5) || pgoProfileRecompilationNeeded) {
            boolean mixedNeeded = mixedNeeded(j5);
            boolean optimizationCompleted = optimizationCompleted(j5);
            String str3 = BuildConfig.FLAVOR;
            String str4 = "NOT ";
            if (mixedNeeded) {
                str4 = str3;
            }
            if (!pgoProfileRecompilationNeeded) {
                str3 = "NOT ";
            }
            if (optimizationCompleted) {
                str = "Yes";
            } else {
                str = "No";
            }
            Mlog.safeFmt("[opt] mixed mode %sneeded and pgo profile recompilation %sneeded. Already Complete: %s", str4, str3, str);
            if (!optimizationCompleted) {
                Mlog.safeFmt("[opt] inspecting free disk space", new Object[0]);
                long availableBytes = new StatFs(dexStore2.root.getAbsolutePath()).getAvailableBytes();
                Mlog.safeFmt("[opt] " + availableBytes + " bytes available on " + dexStore2.root + " filesystem", new Object[0]);
                long j9 = optimizationSession2.config.requiredDiskSpaceForCompilation;
                if (availableBytes >= j9) {
                    Mlog.safeFmt("[opt] sufficient disk space for mixed mode or pgo compilation", new Object[0]);
                } else {
                    Mlog.safeFmt("[opt] insufficient disk space %d for mixed mode or pgo compilation", Long.valueOf(j9));
                    throw new IOException("Insufficient disk space for mixed mode or pgo compilation");
                }
            } else {
                Mlog.safeFmt("[opt] skipping disk space check for mixed mode or pgo compilation", new Object[0]);
            }
            if (pgoProfileRecompilationNeeded) {
                if (!DexStorePgoUtils.isMainDexStoreProfileChangeSignificant(context2, true)) {
                    Mlog.safeFmt("[opt] No need to recompile since PGO file is not different enough", new Object[0]);
                    return;
                }
                Mlog.safeFmt("[opt] PGO file is different enough to recompile. Attempting recompile", new Object[0]);
            } else if (config2.enableMixedModePgo) {
                Mlog.safeFmt("[opt] Saving reference PGO file", new Object[0]);
                DexStorePgoUtils.createNewMainDexStoreReferencePgoProfile(context2);
            }
            boolean optimizeDex2Oat = optimizeDex2Oat(context2, dexStore2, renamer2, optimizationSession2, config2, tmpDir2, config2.enableArtVerifyNone, mixedNeeded, false, config2.enableMixedModeClassPath, config2.enableMixedModePgo, config2.pgoCompilerFilter, config2.multidexCompilationStrategy, pgoProfileRecompilationNeeded);
            optimizationStateHolder2.success = optimizeDex2Oat;
            optimizationStateHolder2.status |= 16;
            if (optimizeDex2Oat) {
                str2 = "succeeded";
            } else {
                str2 = "failed";
            }
            Mlog.safeFmt("[opt] optimizeDex2Oat: %s", str2);
            if (optimizationStateHolder2.success) {
                if (mixedNeeded) {
                    optimizationStateHolder2.status &= -129;
                }
                if (config2.enableMixedModePgo) {
                    optimizationStateHolder2.status &= -4097;
                }
                if (pgoProfileRecompilationNeeded) {
                    Mlog.safeFmt("[opt] Adding pgo recomp finished", Long.valueOf(optimizationStateHolder2.status));
                    optimizationStateHolder2.status |= STATE_DO_PERIODIC_PGO_COMP_FINISHED;
                }
                if (config2.enableMixedModeClassPath) {
                    optimizationStateHolder2.status |= STATE_DEX2OAT_CLASSPATH_SET;
                }
                Mlog.safeFmt("[opt] new status 0x%x", Long.valueOf(optimizationStateHolder2.status));
            }
            long j10 = 0;
            optimizationStateHolder2.statusIntent = 0;
            if (mixedNeeded) {
                j = 1024;
            } else {
                j = 0;
            }
            long j11 = 0 | j;
            optimizationStateHolder2.statusIntent = j11;
            if (config2.enableMixedModePgo) {
                j2 = STATE_PGO_ATTEMPTED;
            } else {
                j2 = 0;
            }
            long j12 = j2 | j11;
            optimizationStateHolder2.statusIntent = j12;
            if (pgoProfileRecompilationNeeded) {
                j10 = STATE_DO_PERIODIC_PGO_COMP_ATTEMPTED;
            }
            optimizationStateHolder2.statusIntent = j12 | j10;
        } else {
            Mlog.safeFmt("[opt] Have no further optimizations to do", new Object[0]);
        }
    }

    private boolean optimizeOatmeal(Context context, Renamer renamer, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.TmpDir tmpDir, boolean z) {
        return optimizeOatmeal(context, renamer, dexStore, optimizationSession, tmpDir, z, null, null);
    }

    private boolean optimizeOatmeal(Context context, Renamer renamer, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.TmpDir tmpDir, boolean z, File file, List list) {
        String str;
        File file2 = file;
        List<ExpectedFileInfo> list2 = list;
        DexStore dexStore2 = dexStore;
        if (file == null) {
            file2 = dexStore2.root;
        }
        if (list == null) {
            list2 = makeExpectedFileInfoList(this.mDexes, 0);
        }
        DexStore.TmpDir tmpDir2 = tmpDir;
        DexStore.OptimizationSession optimizationSession2 = optimizationSession;
        Context context2 = context;
        Renamer renamer2 = renamer;
        if (Build.VERSION.SDK_INT >= 26) {
            return optimizeOatmealForOreo(context2, renamer2, dexStore2, optimizationSession2, tmpDir2, file2, list2);
        }
        ProcessBuilder processBuilder = new ProcessBuilder(getOatmealPath(tmpDir2), "--write-elf", "--build", AnonymousClass08S.A0J("--arch=", OdexSchemeArtTurbo.getArch()), "--one-oat-per-dex", AnonymousClass08S.A0J("--art-image-location=", getArtImageLocation()), AnonymousClass08S.A0J("--oat-version=", getOatVersion()));
        processBuilder.setenv("LD_LIBRARY_PATH", AnonymousClass01q.A03());
        processBuilder.mTmpDir = ProcessBuilder.genDefaultTmpDir(getAppContext(context2));
        if (DalvikInternals.detectSamsungOatFormat()) {
            processBuilder.mArgv.add("--samsung-oatformat");
        }
        if (z) {
            File extractResourceFile = extractResourceFile(QUICK_DATA_RESOURCE_PATH, tmpDir2.directory, TMP_QUICK_DATA_FILE);
            if (extractResourceFile != null) {
                processBuilder.mArgv.add("--quickening-data=" + tmpDir2.directory + "/" + extractResourceFile.getName());
                Mlog.safeFmt("[opt][quickening] found quick-data file", new Object[0]);
            } else {
                Mlog.safeFmt("[opt][quickening] no quickening data found", new Object[0]);
                return false;
            }
        } else {
            Mlog.safeFmt("[opt][quickening] no quickening requested", new Object[0]);
        }
        for (ExpectedFileInfo expectedFileInfo : list2) {
            processBuilder.mArgv.add(AnonymousClass08S.A0J("--dex=", expectedFileInfo.getFile(file2).getPath()));
            String oatNameFromExpectedFileInfo = oatNameFromExpectedFileInfo(expectedFileInfo);
            File file3 = new File(file2, oatNameFromExpectedFileInfo);
            File file4 = new File(tmpDir2.directory, oatNameFromExpectedFileInfo);
            renamer2.addFile(file4, file3);
            processBuilder.mArgv.add(AnonymousClass08S.A0J("--oat=", file4.getPath()));
            this.oatFiles.add(file3);
        }
        Mlog.safeFmt("oatmeal: %s", processBuilder);
        if (z) {
            str = "oatmeal-quicken";
        } else {
            str = "oatmeal";
        }
        startOptimizerProcess(processBuilder, context2, str, optimizationSession2, null, tmpDir2);
        return true;
    }

    private boolean optimizeOatmealForOreo(Context context, Renamer renamer, DexStore dexStore, DexStore.OptimizationSession optimizationSession, DexStore.TmpDir tmpDir, File file, List list) {
        File file2 = file;
        String oreoOdexOutputDirectory = OdexSchemeArtTurbo.getOreoOdexOutputDirectory(file2, true);
        DexStore.TmpDir tmpDir2 = tmpDir;
        ProcessBuilder processBuilder = new ProcessBuilder(getOatmealPath(tmpDir2), "--write-elf", "--build", AnonymousClass08S.A0J("--arch=", OdexSchemeArtTurbo.getArch()), AnonymousClass08S.A0J("--art-image-location=", getArtImageLocation()), AnonymousClass08S.A0P("--oat=", tmpDir2.directory.getPath(), "/"), AnonymousClass08S.A0J("--oat-version=", getOatVersion()));
        processBuilder.setenv("LD_LIBRARY_PATH", AnonymousClass01q.A03());
        Context context2 = context;
        processBuilder.mTmpDir = ProcessBuilder.genDefaultTmpDir(getAppContext(context2));
        if (DalvikInternals.detectSamsungOatFormat()) {
            processBuilder.mArgv.add("--samsung-oatformat");
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ExpectedFileInfo expectedFileInfo = (ExpectedFileInfo) it.next();
            processBuilder.mArgv.add(AnonymousClass08S.A0J("--dex=", expectedFileInfo.getFile(file2).getPath()));
            String oatNameFromExpectedFileInfo = oatNameFromExpectedFileInfo(expectedFileInfo);
            String vdexNameFromOdexName = vdexNameFromOdexName(oatNameFromExpectedFileInfo);
            File file3 = new File(oreoOdexOutputDirectory, oatNameFromExpectedFileInfo);
            File file4 = new File(tmpDir2.directory, oatNameFromExpectedFileInfo);
            Renamer renamer2 = renamer;
            renamer2.addFile(file4, file3);
            renamer2.addFile(new File(tmpDir2.directory, vdexNameFromOdexName), new File(oreoOdexOutputDirectory, vdexNameFromOdexName));
        }
        Mlog.safeFmt("oatmeal: %s", processBuilder);
        startOptimizerProcess(processBuilder, context2, "oatmeal", optimizationSession, null, tmpDir2);
        return true;
    }
}
