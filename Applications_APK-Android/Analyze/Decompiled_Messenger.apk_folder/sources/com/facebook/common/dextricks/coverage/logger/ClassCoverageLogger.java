package com.facebook.common.dextricks.coverage.logger;

import X.AnonymousClass00I;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ClassCoverageLogger {
    public static final Queue A00 = new ConcurrentLinkedQueue();
    public static volatile String A01 = AnonymousClass00I.A02("fb.throw_on_class_load");
    public static volatile boolean A02 = "true".equals(AnonymousClass00I.A02("fb.enable_class_coverage"));
}
