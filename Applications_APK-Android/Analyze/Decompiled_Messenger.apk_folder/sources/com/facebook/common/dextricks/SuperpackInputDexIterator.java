package com.facebook.common.dextricks;

import X.AnonymousClass02K;
import X.AnonymousClass08S;
import X.AnonymousClass094;
import X.C000000c;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.superpack.SuperpackArchive;
import com.facebook.superpack.SuperpackFile;
import com.facebook.superpack.SuperpackFileInputStream;
import com.facebook.superpack.ditto.DittoPatch;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.SynchronousQueue;

public final class SuperpackInputDexIterator extends InputDexIterator {
    private final AnonymousClass094 mArchiveExtension;
    private final int[] mDexToArchiveMap;
    private final AnonymousClass02K mEvent;
    private final SynchronousQueue[] mFileQueues;
    private int mNextDexIndex;
    private final DittoPatch mPatch;
    public boolean mShuttingDownFlag;
    private final SuperpackArchive mSuperpackArchive;
    private SuperpackFile mSuperpackFileToClose;
    private final Thread[] mThreads;
    private final C000000c mTracer;

    public class Builder {
        public AnonymousClass094 archiveExtension = AnonymousClass094.NONE;
        public int[] dexToArchive;
        public DexManifest manifest;
        public DittoPatch patch;
        public ArrayList rawArchives;
        public C000000c tracer;

        public Builder addRawArchive(InputStream inputStream) {
            if (inputStream != null) {
                this.rawArchives.add(inputStream);
                return this;
            }
            throw new NullPointerException();
        }

        public Builder assignDexToArchive(int i, int i2) {
            if (i2 < 0 || i2 >= this.rawArchives.size()) {
                throw new IndexOutOfBoundsException();
            }
            this.dexToArchive[i] = i2;
            return this;
        }

        public SuperpackInputDexIterator build() {
            if (this.tracer == null) {
                throw new NullPointerException();
            } else if (this.rawArchives.size() >= 1) {
                return new SuperpackInputDexIterator(this);
            } else {
                throw new IllegalStateException();
            }
        }

        public Builder setTracer(C000000c r2) {
            if (r2 != null) {
                this.tracer = r2;
                return this;
            }
            throw new NullPointerException();
        }

        public Builder(DexManifest dexManifest) {
            if (dexManifest != null) {
                this.manifest = dexManifest;
                this.dexToArchive = new int[dexManifest.dexes.length];
                this.tracer = null;
                this.rawArchives = new ArrayList();
                this.archiveExtension = dexManifest.superpackExtension;
                return;
            }
            throw new NullPointerException();
        }

        public Builder setPatch(DittoPatch dittoPatch) {
            this.patch = dittoPatch;
            return this;
        }
    }

    public class UnpackingRunnable implements Runnable {
        private InputStream mInput;
        private SynchronousQueue mOutput;

        public UnpackingRunnable(InputStream inputStream, SynchronousQueue synchronousQueue) {
            this.mInput = inputStream;
            this.mOutput = synchronousQueue;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x002a, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x002b, code lost:
            if (r2 != null) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0030 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
                com.facebook.common.dextricks.SuperpackInputDexIterator r1 = com.facebook.common.dextricks.SuperpackInputDexIterator.this     // Catch:{ IOException -> 0x003f, InterruptedException -> 0x0031 }
                java.io.InputStream r0 = r3.mInput     // Catch:{ IOException -> 0x003f, InterruptedException -> 0x0031 }
                com.facebook.superpack.SuperpackArchive r2 = com.facebook.common.dextricks.SuperpackInputDexIterator.getSuperpackArchive(r1, r0)     // Catch:{ IOException -> 0x003f, InterruptedException -> 0x0031 }
                java.io.InputStream r0 = r3.mInput     // Catch:{ all -> 0x0028 }
                r0.close()     // Catch:{ all -> 0x0028 }
            L_0x000d:
                boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0028 }
                if (r0 == 0) goto L_0x0024
                com.facebook.common.dextricks.SuperpackInputDexIterator r0 = com.facebook.common.dextricks.SuperpackInputDexIterator.this     // Catch:{ all -> 0x0028 }
                com.facebook.superpack.SuperpackFile r1 = com.facebook.common.dextricks.SuperpackInputDexIterator.getNextFileFromSpk(r0, r2)     // Catch:{ all -> 0x0028 }
                java.util.concurrent.SynchronousQueue r0 = r3.mOutput     // Catch:{ all -> 0x001f }
                r0.put(r1)     // Catch:{ all -> 0x001f }
                goto L_0x000d
            L_0x001f:
                r0 = move-exception
                r1.close()     // Catch:{ all -> 0x0028 }
                throw r0     // Catch:{ all -> 0x0028 }
            L_0x0024:
                r2.close()     // Catch:{ IOException -> 0x003f, InterruptedException -> 0x0031 }
                return
            L_0x0028:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x002a }
            L_0x002a:
                r0 = move-exception
                if (r2 == 0) goto L_0x0030
                r2.close()     // Catch:{ all -> 0x0030 }
            L_0x0030:
                throw r0     // Catch:{ IOException -> 0x003f, InterruptedException -> 0x0031 }
            L_0x0031:
                r1 = move-exception
                com.facebook.common.dextricks.SuperpackInputDexIterator r0 = com.facebook.common.dextricks.SuperpackInputDexIterator.this
                boolean r0 = r0.mShuttingDownFlag
                if (r0 != 0) goto L_0x003e
                java.lang.RuntimeException r0 = new java.lang.RuntimeException
                r0.<init>(r1)
                throw r0
            L_0x003e:
                return
            L_0x003f:
                r1 = move-exception
                java.lang.RuntimeException r0 = new java.lang.RuntimeException
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.SuperpackInputDexIterator.UnpackingRunnable.run():void");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.superpack.SuperpackFile applyPatch(com.facebook.superpack.SuperpackFile r4) {
        /*
            r3 = this;
            com.facebook.superpack.ditto.DittoPatch r2 = r3.mPatch
            if (r2 == 0) goto L_0x0026
            X.00c r1 = r3.mTracer
            r0 = 47448066(0x2d40002, float:3.1150605E-37)
            X.02K r1 = r1.AOr(r0)
            com.facebook.superpack.SuperpackFile r4 = r2.A00(r4)     // Catch:{ all -> 0x001d }
            r3.maybeCloseLastSuperpackFile()     // Catch:{ all -> 0x001d }
            r3.setLastSuperpackFileToClose(r4)     // Catch:{ all -> 0x001d }
            if (r1 == 0) goto L_0x0026
            r1.close()
            return r4
        L_0x001d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001f }
        L_0x001f:
            r0 = move-exception
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ all -> 0x0025 }
        L_0x0025:
            throw r0
        L_0x0026:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.SuperpackInputDexIterator.applyPatch(com.facebook.superpack.SuperpackFile):com.facebook.superpack.SuperpackFile");
    }

    public static Builder builder(DexManifest dexManifest) {
        return new Builder(dexManifest);
    }

    public static String getArchiveExtension(Builder builder) {
        AnonymousClass094 r3 = builder.manifest.superpackExtension;
        if (r3 == AnonymousClass094.NONE || r3 == AnonymousClass094.XZ) {
            return ".dex.spk.xz";
        }
        if (r3 == AnonymousClass094.ZST) {
            return ".dex.spk.zst";
        }
        throw new RuntimeException("Unknown Superpack Archive Extension " + r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0025, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.superpack.SuperpackFile getNextFileFromSpk(com.facebook.common.dextricks.SuperpackInputDexIterator r1, com.facebook.superpack.SuperpackArchive r2) {
        /*
            X.00c r1 = r1.mTracer
            r0 = 34603016(0x2100008, float:1.0579458E-37)
            X.02K r1 = r1.AOr(r0)
            com.facebook.superpack.SuperpackFile r0 = r2.next()     // Catch:{ all -> 0x001d }
            if (r1 == 0) goto L_0x0012
            r1.close()
        L_0x0012:
            if (r0 == 0) goto L_0x0015
            return r0
        L_0x0015:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "superpack archive has no files left"
            r1.<init>(r0)
            throw r1
        L_0x001d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001f }
        L_0x001f:
            r0 = move-exception
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ all -> 0x0025 }
        L_0x0025:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.SuperpackInputDexIterator.getNextFileFromSpk(com.facebook.common.dextricks.SuperpackInputDexIterator, com.facebook.superpack.SuperpackArchive):com.facebook.superpack.SuperpackFile");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:13|14|15|16|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003f, code lost:
        if (r4 != null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005f, code lost:
        if (r4 != null) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0064, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0024 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.superpack.SuperpackArchive getSuperpackArchive(com.facebook.common.dextricks.SuperpackInputDexIterator r5, java.io.InputStream r6) {
        /*
            X.00c r1 = r5.mTracer
            r0 = 34603015(0x2100007, float:1.0579457E-37)
            X.02K r4 = r1.AOr(r0)
            X.094 r3 = r5.mArchiveExtension     // Catch:{ all -> 0x005c }
            X.094 r0 = X.AnonymousClass094.NONE     // Catch:{ all -> 0x005c }
            if (r3 != r0) goto L_0x0025
            com.facebook.xzdecoder.XzInputStream r1 = new com.facebook.xzdecoder.XzInputStream     // Catch:{ all -> 0x005c }
            r1.<init>(r6)     // Catch:{ all -> 0x005c }
            java.lang.String r0 = "spk"
            com.facebook.superpack.SuperpackArchive r0 = com.facebook.superpack.SuperpackArchive.A00(r1, r0)     // Catch:{ all -> 0x001e }
            r1.close()     // Catch:{ all -> 0x005c }
            goto L_0x003f
        L_0x001e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0020 }
        L_0x0020:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0024 }
        L_0x0024:
            throw r0     // Catch:{ all -> 0x005c }
        L_0x0025:
            X.094 r0 = X.AnonymousClass094.XZ     // Catch:{ all -> 0x005c }
            if (r3 != r0) goto L_0x0032
            java.lang.String r0 = "xz"
            com.facebook.superpack.SuperpackArchive r0 = com.facebook.superpack.SuperpackArchive.A00(r6, r0)     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x0044
            goto L_0x0041
        L_0x0032:
            X.094 r0 = X.AnonymousClass094.ZST     // Catch:{ all -> 0x005c }
            if (r3 != r0) goto L_0x0045
            java.lang.String r0 = "zst"
            com.facebook.superpack.SuperpackArchive r0 = com.facebook.superpack.SuperpackArchive.A00(r6, r0)     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x0044
            goto L_0x0041
        L_0x003f:
            if (r4 == 0) goto L_0x0044
        L_0x0041:
            r4.close()
        L_0x0044:
            return r0
        L_0x0045:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x005c }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x005c }
            r1.<init>()     // Catch:{ all -> 0x005c }
            java.lang.String r0 = "Unknown Superpack Archive Extension "
            r1.append(r0)     // Catch:{ all -> 0x005c }
            r1.append(r3)     // Catch:{ all -> 0x005c }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x005c }
            r2.<init>(r0)     // Catch:{ all -> 0x005c }
            throw r2     // Catch:{ all -> 0x005c }
        L_0x005c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            if (r4 == 0) goto L_0x0064
            r4.close()     // Catch:{ all -> 0x0064 }
        L_0x0064:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.SuperpackInputDexIterator.getSuperpackArchive(com.facebook.common.dextricks.SuperpackInputDexIterator, java.io.InputStream):com.facebook.superpack.SuperpackArchive");
    }

    private void maybeCloseLastSuperpackFile() {
        SuperpackFile superpackFile = this.mSuperpackFileToClose;
        if (superpackFile != null) {
            superpackFile.close();
            this.mSuperpackFileToClose = null;
        }
    }

    private SuperpackFile nextSuperpackFile() {
        int[] iArr = this.mDexToArchiveMap;
        int i = this.mNextDexIndex;
        this.mNextDexIndex = i + 1;
        int i2 = iArr[i];
        if (i2 == 0) {
            return getNextFileFromSpk(this, this.mSuperpackArchive);
        }
        try {
            return (SuperpackFile) this.mFileQueues[i2 - 1].take();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void setLastSuperpackFileToClose(SuperpackFile superpackFile) {
        if (this.mSuperpackFileToClose == null) {
            this.mSuperpackFileToClose = superpackFile;
            return;
        }
        throw new IllegalStateException();
    }

    public void close() {
        if (!this.mShuttingDownFlag) {
            this.mShuttingDownFlag = true;
            maybeCloseLastSuperpackFile();
            try {
                this.mSuperpackArchive.close();
                DittoPatch dittoPatch = this.mPatch;
                if (dittoPatch != null) {
                    dittoPatch.close();
                }
                for (Thread thread : this.mThreads) {
                    thread.interrupt();
                    thread.join();
                }
                this.mEvent.close();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (Throwable th) {
                this.mEvent.close();
                throw th;
            }
        } else {
            throw new IllegalStateException("Iterator already closed");
        }
    }

    public InputDex nextImpl(DexManifest.Dex dex) {
        SuperpackFileInputStream superpackFileInputStream;
        maybeCloseLastSuperpackFile();
        SuperpackFile nextSuperpackFile = nextSuperpackFile();
        if (dex.assetName.equals(nextSuperpackFile.A01())) {
            setLastSuperpackFileToClose(nextSuperpackFile);
            SuperpackFile applyPatch = applyPatch(nextSuperpackFile);
            synchronized (applyPatch) {
                if (applyPatch.A01 != 0) {
                    superpackFileInputStream = new SuperpackFileInputStream(applyPatch);
                } else {
                    throw new IllegalStateException();
                }
            }
            return new InputDex(dex, superpackFileInputStream);
        }
        throw new RuntimeException(AnonymousClass08S.A0S("Wrong dex, expected ", dex.assetName, ", got ", nextSuperpackFile.A01()));
    }

    public SuperpackInputDexIterator(Builder builder) {
        super(builder.manifest, null);
        C000000c r1 = builder.tracer;
        this.mTracer = r1;
        this.mEvent = r1.AOr(34603017);
        this.mPatch = builder.patch;
        this.mArchiveExtension = builder.archiveExtension;
        try {
            this.mShuttingDownFlag = false;
            this.mNextDexIndex = 0;
            this.mDexToArchiveMap = builder.dexToArchive;
            int size = builder.rawArchives.size() - 1;
            this.mThreads = new Thread[size];
            this.mFileQueues = new SynchronousQueue[size];
            int i = 0;
            while (i < size) {
                this.mFileQueues[i] = new SynchronousQueue();
                int i2 = i + 1;
                this.mThreads[i] = new Thread(new UnpackingRunnable((InputStream) builder.rawArchives.get(i2), this.mFileQueues[i]));
                this.mThreads[i].start();
                i = i2;
            }
            this.mSuperpackArchive = getSuperpackArchive(this, (InputStream) builder.rawArchives.get(0));
        } catch (Throwable th) {
            DittoPatch dittoPatch = this.mPatch;
            if (dittoPatch != null) {
                dittoPatch.close();
            }
            this.mEvent.close();
            throw th;
        }
    }
}
