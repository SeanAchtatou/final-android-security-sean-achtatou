package com.facebook.common.dextricks;

import X.AnonymousClass00C;
import X.AnonymousClass08S;
import X.AnonymousClass1Y3;
import android.os.Build;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.common.dextricks.MultiDexClassLoader;
import com.facebook.common.dextricks.OdexScheme;
import com.facebook.common.dextricks.storer.Storer;
import dalvik.system.BaseDexClassLoader;
import dalvik.system.VMRuntime;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class OdexSchemeOreo extends OdexScheme {
    public static boolean sForceUnpack;
    private HashMap mDexNameMap;
    private long mStorer;
    public IOException[] mSuppressedExceptions;
    private File mZipFile;

    public class Arrow {
        public final String[] codePaths;
        public final String profileFile;

        public Arrow(String str, String[] strArr) {
            this.profileFile = str;
            this.codePaths = strArr;
        }
    }

    public final class OreoCompiler extends OdexScheme.Compiler {
        public HashMap mDexNameMap;
        public long mStorer;

        public void compile(InputDex inputDex) {
            int read;
            AnonymousClass00C.A01(32, "OdexSchemeOreo.compile", 180903662);
            try {
                String str = (String) this.mDexNameMap.get(inputDex.dex.hash);
                if (str != null) {
                    Storer.start(this.mStorer, str, 4);
                    InputStream dexContents = inputDex.getDexContents();
                    byte[] bArr = new byte[DexStore.LOAD_RESULT_PGO];
                    do {
                        read = dexContents.read(bArr);
                        if (read >= 0) {
                            Storer.write(this.mStorer, bArr, read);
                            continue;
                        }
                    } while (read >= 32768);
                    Storer.finish(this.mStorer);
                    return;
                }
                throw new RuntimeException("Unexpected input dex!");
            } finally {
                AnonymousClass00C.A00(32, -2078960940);
            }
        }

        public OreoCompiler(long j, HashMap hashMap) {
            this.mStorer = j;
            this.mDexNameMap = hashMap;
        }
    }

    public OdexSchemeOreo(DexManifest.Dex[] dexArr, File file) {
        super(1, new String[]{file.getName()});
        this.mZipFile = file;
        this.mDexNameMap = makeNameMap(dexArr);
    }

    public void configureClassLoader(File file, MultiDexClassLoader.Configuration configuration) {
    }

    public String getSchemeName() {
        return "OdexSchemeOreo";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (com.facebook.common.dextricks.classid.ClassId.sInitialized == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void enableTracingIfNeeded() {
        /*
            boolean r0 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sEnabled
            if (r0 == 0) goto L_0x0009
            boolean r1 = com.facebook.common.dextricks.classid.ClassId.sInitialized
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x001b
            X.0Nu r1 = X.C03450Nu.A00()
            if (r1 == 0) goto L_0x001b
            X.0Mc r0 = new X.0Mc
            r0.<init>()
            r1.A01(r0)
            return
        L_0x001b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeOreo.enableTracingIfNeeded():void");
    }

    private static HashMap makeNameMap(DexManifest.Dex[] dexArr) {
        int length = dexArr.length;
        HashMap hashMap = new HashMap(length);
        for (int i = 0; i < length; i++) {
            String str = dexArr[i].hash;
            if (i != 0) {
                hashMap.put(str, AnonymousClass08S.A0A("classes", i + 1, DexManifest.DEX_EXT));
            } else {
                hashMap.put(str, "classes.dex");
            }
        }
        return hashMap;
    }

    private static IOException[] threadSafeAddDexPath(BaseDexClassLoader baseDexClassLoader, List list) {
        int length;
        int length2;
        Object[] objArr;
        int length3;
        Class<?> cls = Class.forName("dalvik.system.DexPathList");
        Class<?> cls2 = Class.forName("dalvik.system.DexPathList$Element");
        Class<List> cls3 = List.class;
        Method declaredMethod = cls.getDeclaredMethod("makeDexElements", cls3, File.class, cls3, ClassLoader.class);
        declaredMethod.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        Object[] objArr2 = (Object[]) declaredMethod.invoke(null, list, null, arrayList, baseDexClassLoader);
        Field declaredField = BaseDexClassLoader.class.getDeclaredField("pathList");
        declaredField.setAccessible(true);
        Object obj = declaredField.get(baseDexClassLoader);
        if (objArr2 != null && (length2 = objArr2.length) > 0) {
            Field declaredField2 = cls.getDeclaredField("dexElements");
            declaredField2.setAccessible(true);
            Object[] objArr3 = (Object[]) declaredField2.get(obj);
            if (objArr3 == null || (length3 = objArr3.length) <= 0) {
                objArr = objArr2;
            } else {
                objArr = (Object[]) Array.newInstance(cls2, length2 + length3);
                System.arraycopy(objArr3, 0, objArr, 0, length3);
                System.arraycopy(objArr2, 0, objArr, length3, length2);
            }
            declaredField2.set(obj, objArr);
        }
        Field declaredField3 = cls.getDeclaredField("dexElementsSuppressedExceptions");
        declaredField3.setAccessible(true);
        int size = arrayList.size();
        if (size <= 0) {
            return (IOException[]) declaredField3.get(obj);
        }
        IOException[] iOExceptionArr = (IOException[]) arrayList.toArray(new IOException[size]);
        IOException[] iOExceptionArr2 = (IOException[]) declaredField3.get(obj);
        if (iOExceptionArr2 != null && (length = iOExceptionArr2.length) > 0) {
            IOException[] iOExceptionArr3 = new IOException[(length + size)];
            System.arraycopy(iOExceptionArr2, 0, iOExceptionArr3, 0, length);
            System.arraycopy(iOExceptionArr, 0, iOExceptionArr3, length, size);
            iOExceptionArr = iOExceptionArr3;
        }
        declaredField3.set(obj, iOExceptionArr);
        return iOExceptionArr;
    }

    public void finalizeZip() {
        Storer.cleanup(this.mStorer);
        this.mStorer = 0;
    }

    public void initializeClassLoader() {
        Throwable e;
        ClassLoader classLoader = OdexSchemeOreo.class.getClassLoader();
        if (classLoader instanceof BaseDexClassLoader) {
            if (!DalvikInternals.toggleBlockDex2Oat(true)) {
                DexTricksErrorReporter.reportSampledSoftError("FBDex101", "Failed to block dex2oat", null);
            }
            try {
                ArrayList arrayList = new ArrayList();
                arrayList.add(this.mZipFile);
                this.mSuppressedExceptions = threadSafeAddDexPath((BaseDexClassLoader) classLoader, arrayList);
                if (!DalvikInternals.toggleBlockDex2Oat(false)) {
                    DexTricksErrorReporter.reportSampledSoftError("FBDex101", "Failed to unblock dex2oat", null);
                }
                e = null;
            } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e2) {
                e = e2;
                DexTricksErrorReporter.reportSampledSoftError("FBDex101", "Failed to merge dex elements", e);
                if (!DalvikInternals.toggleBlockDex2Oat(false)) {
                    DexTricksErrorReporter.reportSampledSoftError("FBDex101", "Failed to unblock dex2oat", null);
                }
            } catch (Throwable th) {
                if (!DalvikInternals.toggleBlockDex2Oat(false)) {
                    DexTricksErrorReporter.reportSampledSoftError("FBDex101", "Failed to unblock dex2oat", null);
                }
                throw th;
            }
        } else {
            String A0J = AnonymousClass08S.A0J("Unknown Application ClassLoader: ", classLoader.toString());
            DexTricksErrorReporter.reportSampledSoftError("FBDex101", A0J, null);
            e = new RuntimeException(A0J);
        }
        if (e == null) {
            enableTracingIfNeeded();
            setupErrorReportingFields();
            return;
        }
        throw new RuntimeException("[FBDex101] Unknown Application ClassLoader or failed to merge dex, app bound to crash with NoClassDef", e);
    }

    public OdexScheme.Compiler makeCompiler(DexStore dexStore, int i) {
        long open = Storer.open(this.mZipFile.getPath(), AnonymousClass1Y3.A3R);
        this.mStorer = open;
        return new OreoCompiler(open, this.mDexNameMap);
    }

    public boolean needsUnpack() {
        if (sForceUnpack || !this.mZipFile.exists()) {
            return true;
        }
        if (!OreoFileUtils.isTruncated(this.mZipFile)) {
            return false;
        }
        return !OreoFileUtils.hasVdexOdex(this.mZipFile);
    }

    public void registerCodeAndProfile() {
        File file;
        File parentFile = this.mZipFile.getParentFile();
        if (Build.VERSION.SDK_INT < 27) {
            file = new File(parentFile, AnonymousClass08S.A0J(this.mZipFile.getName(), ".prof"));
        } else {
            File file2 = new File(parentFile, OdexSchemeArtTurbo.OREO_ODEX_DIR);
            file = new File(file2, AnonymousClass08S.A0J(this.mZipFile.getName(), ".cur.prof"));
            if (!file2.exists() && !file2.mkdir()) {
                DexTricksErrorReporter.reportSampledSoftError("RegisterProf", "Failed to mkdir for prof dir: " + file, null);
                return;
            }
        }
        try {
            if (file.createNewFile()) {
                Mlog.w("Created new profile file: %s", file);
            }
            String path = file.getPath();
            String[] strArr = {this.mZipFile.getPath()};
            try {
                Method declaredMethod = VMRuntime.class.getDeclaredMethod("registerAppInfo", String.class, String[].class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(null, path, strArr);
            } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException unused) {
                Achilles.attack(new Arrow(path, strArr), Achilles.derp());
            }
        } catch (IOException e) {
            DexTricksErrorReporter.reportSampledSoftError("RegisterProf", "Failed to touch new profile file", e);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r0 == false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void setupErrorReportingFields() {
        /*
            com.facebook.acra.ErrorReporter r2 = com.facebook.acra.ErrorReporter.getInstance()
            com.facebook.common.dextricks.OdexSchemeOreo$1 r1 = new com.facebook.common.dextricks.OdexSchemeOreo$1
            r1.<init>()
            java.lang.String r0 = "multiDexClassLoader"
            r2.putLazyCustomData(r0, r1)
            X.0Nu r2 = X.C03450Nu.A06
            if (r2 == 0) goto L_0x001d
            r1 = r2
            monitor-enter(r1)
            boolean r0 = r2.A04     // Catch:{ all -> 0x0017 }
            goto L_0x001a
        L_0x0017:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x001a:
            monitor-exit(r1)
            if (r0 != 0) goto L_0x0023
        L_0x001d:
            java.lang.Class<com.facebook.common.dextricks.OdexSchemeOreo> r0 = com.facebook.common.dextricks.OdexSchemeOreo.class
            java.lang.ClassLoader r2 = r0.getClassLoader()
        L_0x0023:
            java.lang.String r1 = r2.toString()
            java.lang.String r0 = "oreoClassLoader"
            com.facebook.acra.ErrorReporter.putCustomData(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeOreo.setupErrorReportingFields():void");
    }

    public IOException[] getSuppressedExceptions() {
        return this.mSuppressedExceptions;
    }
}
