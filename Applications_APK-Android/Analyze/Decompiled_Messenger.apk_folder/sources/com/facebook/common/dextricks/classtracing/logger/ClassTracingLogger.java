package com.facebook.common.dextricks.classtracing.logger;

import X.AnonymousClass03T;
import X.C001000r;
import com.facebook.common.dextricks.classid.ClassId;

public final class ClassTracingLogger {
    public static final Class TAG = ClassTracingLogger.class;
    public static volatile boolean sEnabled;
    public static volatile boolean sEverEnabled;
    public static volatile boolean sLoggerEnabled;
    public static volatile boolean sSystraceEnabled;

    static {
        C001000r.A00(new AnonymousClass03T());
    }

    public static void beginClassLoad(String str) {
        if (sEnabled && ClassId.sInitialized) {
            ClassTracingLoggerNativeHolder.classLoadStarted(str);
        }
    }

    public static void classLoaded(Class cls) {
        if (sEnabled && ClassId.sInitialized) {
            ClassTracingLoggerNativeHolder.classLoaded(ClassId.getClassId(cls));
        }
    }

    public static void classNotFound() {
        if (sEnabled && ClassId.sInitialized) {
            ClassTracingLoggerNativeHolder.classLoadCancelled();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        if (com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sSystraceEnabled != false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void initialize() {
        /*
            boolean r4 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sEnabled
            boolean r0 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sLoggerEnabled
            r1 = 1
            if (r0 != 0) goto L_0x000c
            boolean r0 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sSystraceEnabled
            r3 = 0
            if (r0 == 0) goto L_0x000d
        L_0x000c:
            r3 = 1
        L_0x000d:
            if (r3 != 0) goto L_0x0011
            if (r4 == 0) goto L_0x0022
        L_0x0011:
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sEverEnabled = r1
            r4 = r4 ^ r1
            boolean r2 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sLoggerEnabled
            boolean r1 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sSystraceEnabled
            if (r4 == 0) goto L_0x001f
            java.lang.String r0 = "classtracing"
            X.AnonymousClass01q.A08(r0)
        L_0x001f:
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerNativeHolder.configureTracing(r2, r1)
        L_0x0022:
            com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sEnabled = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.initialize():void");
    }
}
