package com.facebook.common.dextricks;

import X.AnonymousClass08S;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.xzdecoder.XzInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public final class InputDex implements Closeable {
    private static final byte STATE_RAW = 0;
    private static final byte STATE_USED = 2;
    private static final byte STATE_ZIPPED = 1;
    private static final String XZS_ASSET_SUFFIX = ".xzs.tmp~";
    public final DexManifest.Dex dex;
    private InputStream mContents;
    private byte mState;
    private int sizeHint;

    /* JADX WARN: Failed to insert an additional move for type inference into block B:7:0x002c */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x0040 */
    /* JADX WARN: Type inference failed for: r5v0, types: [java.io.FileInputStream, java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r5v2 */
    /* JADX WARN: Type inference failed for: r5v4, types: [java.util.zip.CheckedInputStream, java.io.Closeable] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void synthesizeDexJarFile(java.io.OutputStream r13) {
        /*
            r12 = this;
            java.io.InputStream r6 = r12.mContents
            java.util.zip.ZipOutputStream r4 = new java.util.zip.ZipOutputStream
            r4.<init>(r13)
            java.util.zip.ZipEntry r8 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x008e }
            java.lang.String r0 = "classes.dex"
            r8.<init>(r0)     // Catch:{ all -> 0x008e }
            boolean r0 = r6 instanceof java.io.FileInputStream     // Catch:{ all -> 0x008e }
            if (r0 == 0) goto L_0x007e
            r1 = r6
            java.io.FileInputStream r1 = (java.io.FileInputStream) r1     // Catch:{ all -> 0x008e }
            java.nio.channels.FileChannel r7 = r1.getChannel()     // Catch:{ all -> 0x008e }
            long r9 = r7.position()     // Catch:{ all -> 0x008e }
            r2 = 0
            int r0 = (r9 > r2 ? 1 : (r9 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x007e
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ all -> 0x008e }
            java.io.FileDescriptor r0 = r1.getFD()     // Catch:{ all -> 0x008e }
            r5.<init>(r0)     // Catch:{ all -> 0x008e }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0073 }
            r1.<init>(r5)     // Catch:{ all -> 0x0073 }
            java.util.zip.CheckedInputStream r5 = new java.util.zip.CheckedInputStream     // Catch:{ all -> 0x0070 }
            java.util.zip.CRC32 r0 = new java.util.zip.CRC32     // Catch:{ all -> 0x0070 }
            r0.<init>()     // Catch:{ all -> 0x0070 }
            r5.<init>(r1, r0)     // Catch:{ all -> 0x0070 }
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            long r0 = r5.skip(r0)     // Catch:{ all -> 0x0073 }
            r8.setSize(r0)     // Catch:{ all -> 0x0073 }
            java.util.zip.Checksum r0 = r5.getChecksum()     // Catch:{ all -> 0x0073 }
            long r0 = r0.getValue()     // Catch:{ all -> 0x0073 }
            r8.setCrc(r0)     // Catch:{ all -> 0x0073 }
            r0 = 0
            r8.setMethod(r0)     // Catch:{ all -> 0x0073 }
            java.lang.String r10 = "computed zip data for %s from file size:%s crc:%s"
            long r0 = r8.getSize()     // Catch:{ all -> 0x0073 }
            java.lang.Long r9 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0073 }
            long r0 = r8.getCrc()     // Catch:{ all -> 0x0073 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0073 }
            java.lang.Object[] r0 = new java.lang.Object[]{r12, r9, r0}     // Catch:{ all -> 0x0073 }
            com.facebook.common.dextricks.Mlog.safeFmt(r10, r0)     // Catch:{ all -> 0x0073 }
            goto L_0x0078
        L_0x0070:
            r0 = move-exception
            r5 = r1
            goto L_0x0074
        L_0x0073:
            r0 = move-exception
        L_0x0074:
            com.facebook.common.dextricks.Fs.safeClose(r5)     // Catch:{ all -> 0x008e }
            throw r0     // Catch:{ all -> 0x008e }
        L_0x0078:
            com.facebook.common.dextricks.Fs.safeClose(r5)     // Catch:{ all -> 0x008e }
            r7.position(r2)     // Catch:{ all -> 0x008e }
        L_0x007e:
            r4.putNextEntry(r8)     // Catch:{ all -> 0x008e }
            r0 = 2147483647(0x7fffffff, float:NaN)
            X.AnonymousClass0Me.A0B(r4, r6, r0)     // Catch:{ all -> 0x008e }
            r4.finish()     // Catch:{ all -> 0x008e }
            r4.close()
            return
        L_0x008e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0090 }
        L_0x0090:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x0094 }
        L_0x0094:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.InputDex.synthesizeDexJarFile(java.io.OutputStream):void");
    }

    public void close() {
        Fs.safeClose(this.mContents);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0070, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0074, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void extract(java.io.File r7) {
        /*
            r6 = this;
            byte r5 = r6.mState
            r0 = 2
            r6.mState = r0
            r1 = 1
            if (r5 == 0) goto L_0x0012
            if (r5 == r1) goto L_0x0012
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "InputDex already used"
            r1.<init>(r0)
            throw r1
        L_0x0012:
            java.lang.String r4 = r7.getName()
            java.io.FileOutputStream r2 = new java.io.FileOutputStream
            r2.<init>(r7)
            java.lang.String r0 = ".dex.jar"
            boolean r0 = r4.endsWith(r0)     // Catch:{ all -> 0x006e }
            r3 = 2147483647(0x7fffffff, float:NaN)
            if (r0 == 0) goto L_0x0044
            if (r5 != r1) goto L_0x0037
            java.lang.String r1 = "copying existing zip file %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r6}     // Catch:{ all -> 0x006e }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x006e }
            java.io.InputStream r0 = r6.mContents     // Catch:{ all -> 0x006e }
            X.AnonymousClass0Me.A0B(r2, r0, r3)     // Catch:{ all -> 0x006e }
            goto L_0x005e
        L_0x0037:
            java.lang.String r1 = "synthesizing new zip file %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r6}     // Catch:{ all -> 0x006e }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x006e }
            r6.synthesizeDexJarFile(r2)     // Catch:{ all -> 0x006e }
            goto L_0x005e
        L_0x0044:
            java.lang.String r0 = ".dex"
            boolean r0 = r4.endsWith(r0)     // Catch:{ all -> 0x006e }
            if (r0 == 0) goto L_0x0062
            java.lang.String r1 = "writing raw dex file %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r6}     // Catch:{ all -> 0x006e }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x006e }
            r6.mState = r5     // Catch:{ all -> 0x006e }
            java.io.InputStream r0 = r6.getDexContents()     // Catch:{ all -> 0x006e }
            X.AnonymousClass0Me.A0B(r2, r0, r3)     // Catch:{ all -> 0x006e }
        L_0x005e:
            r2.close()
            return
        L_0x0062:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x006e }
            java.lang.String r0 = "don't know how to make this kind of file: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r4)     // Catch:{ all -> 0x006e }
            r1.<init>(r0)     // Catch:{ all -> 0x006e }
            throw r1     // Catch:{ all -> 0x006e }
        L_0x006e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0074 }
        L_0x0074:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.InputDex.extract(java.io.File):void");
    }

    public InputStream getDexContents() {
        ZipEntry nextEntry;
        int i;
        byte b = this.mState;
        this.mState = 2;
        if (b == 1) {
            ZipInputStream zipInputStream = new ZipInputStream(this.mContents);
            this.mContents = zipInputStream;
            do {
                nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    break;
                }
            } while (!nextEntry.getName().equals("classes.dex"));
            if (nextEntry != null) {
                long size = nextEntry.getSize();
                if (size > 2147483647L) {
                    i = Integer.MAX_VALUE;
                } else {
                    i = (int) size;
                }
                this.sizeHint = i;
                return this.mContents;
            }
            throw new IOException(String.format("zip file %s did not contain a classes.dex", this.dex));
        } else if (b == 0) {
            return this.mContents;
        } else {
            throw new RuntimeException("InputDex already used");
        }
    }

    public int getSizeHint(InputStream inputStream) {
        int i = this.sizeHint;
        if (i > 0) {
            return i;
        }
        int available = inputStream.available();
        if (available > 1) {
            return available;
        }
        return -1;
    }

    public String toString() {
        return String.format("InputDex:[%s]", this.dex.assetName);
    }

    public InputDex(DexManifest.Dex dex2, InputStream inputStream) {
        this.dex = dex2;
        String str = dex2.assetName;
        try {
            str = str.endsWith(XZS_ASSET_SUFFIX) ? str.substring(0, str.length() - 9) : str;
            if (str.endsWith(".xz")) {
                str = str.substring(0, str.length() - 3);
                inputStream = new XzInputStream(inputStream);
            }
            if (str.endsWith(".jar")) {
                str = str.substring(0, str.length() - 4);
                this.mState = 1;
            }
            if (str.endsWith(DexManifest.DEX_EXT)) {
                this.mContents = inputStream;
                Fs.safeClose((Closeable) null);
                return;
            }
            throw new RuntimeException(AnonymousClass08S.A0J("malformed dex asset name: ", dex2.assetName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Throwable th) {
            Fs.safeClose(inputStream);
            throw th;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:15|16|17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0044, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0045, code lost:
        if (r3 != null) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x003b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void patch(com.facebook.superpack.ditto.DittoPatch r6) {
        /*
            r5 = this;
            java.io.InputStream r3 = r5.getDexContents()
            com.facebook.superpack.SuperpackFile r4 = new com.facebook.superpack.SuperpackFile     // Catch:{ all -> 0x0042 }
            java.lang.String r0 = "dex"
            if (r0 == 0) goto L_0x003c
            if (r3 == 0) goto L_0x003c
            long r0 = com.facebook.superpack.SuperpackFile.createSuperpackFileNative(r0, r3)     // Catch:{ all -> 0x0042 }
            r4.<init>(r0)     // Catch:{ all -> 0x0042 }
            com.facebook.superpack.SuperpackFile r2 = r6.A00(r4)     // Catch:{ all -> 0x0035 }
            com.facebook.superpack.SuperpackFileInputStream r1 = new com.facebook.superpack.SuperpackFileInputStream     // Catch:{ all -> 0x0035 }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0035 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0035 }
            r5.mContents = r1     // Catch:{ all -> 0x0035 }
            int r0 = r2.A00()     // Catch:{ all -> 0x0035 }
            r5.sizeHint = r0     // Catch:{ all -> 0x0035 }
            r0 = 0
            r5.mState = r0     // Catch:{ all -> 0x0035 }
            r4.close()     // Catch:{ all -> 0x0042 }
            if (r3 == 0) goto L_0x0034
            r3.close()
        L_0x0034:
            return
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x003b }
        L_0x003b:
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x003c:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x0042 }
            r0.<init>()     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x0042:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0044 }
        L_0x0044:
            r0 = move-exception
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ all -> 0x004a }
        L_0x004a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.InputDex.patch(com.facebook.superpack.ditto.DittoPatch):void");
    }
}
