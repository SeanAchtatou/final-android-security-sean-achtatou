package com.facebook.common.dextricks.turboloader;

import X.AnonymousClass01q;
import X.AnonymousClass0MP;
import X.AnonymousClass1Y3;
import X.C52652jT;
import android.content.Context;
import com.facebook.acra.ACRA;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.acra.constants.ReportField;
import com.facebook.acra.uploader.ReportUploader;
import com.facebook.acra.util.HttpRequest;
import com.facebook.common.dextricks.Experiments;
import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.forker.Process;
import dalvik.system.DexFile;
import java.io.File;
import java.util.List;
import org.webrtc.audio.WebRtcAudioRecord;

public class TurboLoader {
    private final DexFile[] mAuxDexes;
    private final DexFile[] mPrimaryDexes;
    private DexFile[] mSecondaryDexes;
    public final String turboLoaderMapFile;
    private final File turboLoaderTempDir;

    public class Locator {
        public static String $const$string(int i) {
            switch (i) {
                case 0:
                    return "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP";
                case 1:
                    return "android.permission.READ_EXTERNAL_STORAGE";
                case 2:
                    return "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED";
                case 3:
                    return "android.net.conn.CONNECTIVITY_CHANGE";
                case 4:
                    return "android.permission.READ_CONTACTS";
                case 5:
                    return "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED";
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    return "android.permission.GET_ACCOUNTS";
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    return "android.permission.READ_SMS";
                case 8:
                    return "com.whatsapp";
                case Process.SIGKILL /*9*/:
                    return "android.intent.action.CLOSE_SYSTEM_DIALOGS";
                case AnonymousClass1Y3.A01 /*10*/:
                    return "android.permission.ACCESS_COARSE_LOCATION";
                case AnonymousClass1Y3.A02 /*11*/:
                    return "android.permission.ACCESS_FINE_LOCATION";
                case AnonymousClass1Y3.A03 /*12*/:
                    return "com.facebook.rti.mqtt.intent.ACTION_WAKEUP";
                case 13:
                    return "stack_trace";
                case 14:
                    return "android.intent.category.LAUNCHER";
                case 15:
                    return "android.permission.WRITE_CONTACTS";
                case 16:
                    return "com.facebook.auth.login";
                case 17:
                    return "com.facebook.orca";
                case Process.SIGCONT /*18*/:
                    return "com.instagram.android";
                case Process.SIGSTOP /*19*/:
                    return "AppStateLogger is not ready yet";
                case 20:
                    return "airplane_mode_on";
                case AnonymousClass1Y3.A05 /*21*/:
                    return "android.permission.CALL_PHONE";
                case AnonymousClass1Y3.A06 /*22*/:
                    return "android:support:fragments";
                case 23:
                    return OptSvcAnalyticsStore.LOGGING_KEY_ATTEMPT_NUMBER;
                case AnonymousClass1Y3.A07 /*24*/:
                    return "disconnected";
                case 25:
                    return "extra_notification_sender";
                case AnonymousClass1Y3.A08 /*26*/:
                    return "unregistered";
                case AnonymousClass1Y3.A09 /*27*/:
                    return "/graphql";
                case 28:
                    return "APP_FOREGROUND";
                case 29:
                    return ReportUploader.AUTHORIZATION_KEY;
                case AnonymousClass1Y3.A0A /*30*/:
                    return "THREAD_LIST";
                case AnonymousClass1Y3.A0B /*31*/:
                    return "android.intent.action.ACTION_POWER_CONNECTED";
                case 32:
                    return "android.intent.action.ACTION_POWER_DISCONNECTED";
                case 33:
                    return "android.intent.action.BOOT_COMPLETED";
                case AnonymousClass1Y3.A0C /*34*/:
                    return "android.intent.action.MY_PACKAGE_REPLACED";
                case 35:
                    return "android.intent.action.USER_PRESENT";
                case 36:
                    return HttpRequest.POST_CONTENT_TYPE_FORM_URLENCODED;
                case AnonymousClass1Y3.A0D /*37*/:
                    return "com.facebook.wakizashi";
                case AnonymousClass1Y3.A0E /*38*/:
                    return "low_power_state";
                case AnonymousClass1Y3.A0F /*39*/:
                    return "timeout";
                case AnonymousClass1Y3.A0G /*40*/:
                    return ".facebook.com";
                case AnonymousClass1Y3.A0H /*41*/:
                    return "/proc/meminfo";
                case 42:
                    return "/t_rtc";
                case 43:
                    return "/t_rtc_multi";
                case AnonymousClass1Y3.A0I /*44*/:
                    return "<none>";
                case AnonymousClass1Y3.A0J /*45*/:
                    return ReportField.ACTIVITY_LOG;
                case AnonymousClass1Y3.A0K /*46*/:
                    return "AppModules::InitialInstallRequestTs";
                case AnonymousClass1Y3.A0L /*47*/:
                    return "AppStateLoggerCore";
                case 48:
                    return "DISCONNECTED";
                case 49:
                    return "EXPIRED_SESSION";
                case 50:
                    return "PERMISSION_DENIED";
                case AnonymousClass1Y3.A0M /*51*/:
                    return "PUBLISH_";
                case AnonymousClass1Y3.A0N /*52*/:
                    return "Package manager has died";
                case AnonymousClass1Y3.A0O /*53*/:
                    return "RtiGracefulSystemMethodHelper";
                case 54:
                    return "UNDEFINED";
                case 55:
                    return "activity";
                case AnonymousClass1Y3.A0P /*56*/:
                    return "analytics";
                case 57:
                    return "android.intent.action.BATTERY_CHANGED";
                case AnonymousClass1Y3.A0Q /*58*/:
                    return "android.intent.action.SCREEN_OFF";
                case 59:
                    return "android.intent.action.SCREEN_ON";
                case AnonymousClass1Y3.A0R /*60*/:
                    return "android.os.SystemProperties";
                case 61:
                    return "android.os.action.POWER_SAVE_MODE_CHANGED";
                case 62:
                    return "android.permission.CAMERA";
                case AnonymousClass1Y3.A0S /*63*/:
                    return "android.permission.READ_PHONE_STATE";
                case 64:
                    return "android.permission.RECORD_AUDIO";
                case AnonymousClass1Y3.A0T /*65*/:
                    return "android.permission.WRITE_EXTERNAL_STORAGE";
                case 66:
                    return "app_ver";
                case 67:
                    return "arservicesoptional";
                case AnonymousClass1Y3.A0U /*68*/:
                    return "caffe2deeptext";
                case 69:
                    return "client_event";
                case 70:
                    return "com.facebook.katana";
                case AnonymousClass1Y3.A0V /*71*/:
                    return "com.facebook.lite";
                case 72:
                    return "com.facebook.mlite";
                case AnonymousClass1Y3.A0W /*73*/:
                    return "com.instagram.direct";
                case 74:
                    return "connectivity";
                case AnonymousClass1Y3.A0X /*75*/:
                    return "eglGetDisplay failed";
                case AnonymousClass1Y3.A0Y /*76*/:
                    return "eglInitialize failed";
                case AnonymousClass1Y3.A0Z /*77*/:
                    return "eglMakeCurrent failed";
                case 78:
                    return "fbinternal";
                case 79:
                    return "graphservice-jni-tree";
                case AnonymousClass1Y3.A0a /*80*/:
                    return "instantgamesads";
                case AnonymousClass1Y3.A0b /*81*/:
                    return "jniexecutors";
                case 82:
                    return "main_activity";
                case 83:
                    return "manual";
                case AnonymousClass1Y3.A0c /*84*/:
                    return "navigation_module";
                case 85:
                    return "networkInfo";
                case 86:
                    return "service_name";
                case 87:
                    return ErrorReportingConstants.SOFT_ERROR_MESSAGE;
                case 88:
                    return "upload";
                case AnonymousClass1Y3.A0d /*89*/:
                    return "year_class";
                case AnonymousClass1Y3.A0e /*90*/:
                    return "/get_media_resp";
                case 91:
                    return "/mercury";
                case 92:
                    return "/orca_message_notifications";
                case 93:
                    return "/proc/self/status";
                case AnonymousClass1Y3.A0f /*94*/:
                    return "/quick_promotion_refresh";
                case 95:
                    return "/sys/devices/system/cpu/";
                case AnonymousClass1Y3.A0g /*96*/:
                    return "/t_assist_rp";
                case 97:
                    return "/t_push";
                case 98:
                    return "/t_region_hint";
                case 99:
                    return "/t_thread_typing";
                case 100:
                    return "/webrtc";
                case AnonymousClass1Y3.A0i /*101*/:
                    return "/webrtc_response";
                case AnonymousClass1Y3.A0j /*102*/:
                    return "Android";
                case 103:
                    return "AppModules::InstallLatency";
                case AnonymousClass1Y3.A0k /*104*/:
                    return "AppModules::Uninstall";
                case AnonymousClass1Y3.A0l /*105*/:
                    return "COMPLETE";
                case AnonymousClass1Y3.A0m /*106*/:
                    return "CONNECTED";
                case AnonymousClass1Y3.A0n /*107*/:
                    return "ColdStart";
                case 108:
                    return "Content-Disposition";
                case AnonymousClass1Y3.A0o /*109*/:
                    return "Content-Encoding";
                case AnonymousClass1Y3.A0p /*110*/:
                    return "Content-Transfer-Encoding";
                case 111:
                    return "EndToEnd-Test";
                case 112:
                    return "FAILURE";
                case AnonymousClass1Y3.A0q /*113*/:
                    return "ISO-8859-1";
                case 114:
                    return "MAIN_THREAD";
                case 115:
                    return "MESSENGER";
                case 116:
                    return "MM-dd HH:mm:ss.SSS";
                case AnonymousClass1Y3.A0r /*117*/:
                    return "NOT_READY";
                case 118:
                    return "NO_FILE";
                case AnonymousClass1Y3.A0s /*119*/:
                    return "No application has been registered with AppStateLogger";
                case AnonymousClass1Y3.A0t /*120*/:
                    return "OFFLINE";
                case AnonymousClass1Y3.A0u /*121*/:
                    return "Optional.absent()";
                case AnonymousClass1Y3.A0v /*122*/:
                    return "Optional.of(";
                case 123:
                    return "Orca.PERSISTENT_KICK";
                case 124:
                    return "PUBLISH";
                case AnonymousClass1Y3.A0w /*125*/:
                    return "REGISTER";
                case AnonymousClass1Y3.A0x /*126*/:
                    return "SEND_MESSAGE";
                case 127:
                    return "TIMEOUT";
                case 128:
                    return "UNICODE";
                case 129:
                    return "USER_ID_KEY";
                case AnonymousClass1Y3.A0y /*130*/:
                    return "UTF-16LE";
                case AnonymousClass1Y3.A0z /*131*/:
                    return "Xo8WBi6jzSxKDVR4drqm84yr9iU";
                case 132:
                    return "addSuppressed";
                case AnonymousClass1Y3.A10 /*133*/:
                    return "android";
                case 134:
                    return "android.intent.action.VIEW";
                case AnonymousClass1Y3.A11 /*135*/:
                    return Experiments.ANDROID_ABORTHOOKS_ENABLED;
                case AnonymousClass1Y3.A12 /*136*/:
                    return Experiments.CHECK_TRACER_TRACING;
                case 137:
                    return Experiments.ANDROID_SMART_GC_USE_NEW_DISTRACT_COORDINATOR;
                case AnonymousClass1Y3.A13 /*138*/:
                    return Experiments.ANDROID_ENABLE_MEMORY_DEX2OAT_HOOK;
                case 139:
                    return Experiments.DO_NOT_RESTART_PROCESS_AFTER_DEX_OPT;
                case AnonymousClass1Y3.A14 /*140*/:
                    return "android_enable_terminate_handler";
                case AnonymousClass1Y3.A15 /*141*/:
                    return Experiments.ANDROID_TRY_TO_RECOVER_FROM_ADDDEX_IO_EXCEPTION;
                case AnonymousClass1Y3.A16 /*142*/:
                    return "app_version_name";
                case 143:
                    return "batch_size";
                case AnonymousClass1Y3.A17 /*144*/:
                    return "browser";
                case 145:
                    return "checkpoint";
                case 146:
                    return "clipboard";
                case AnonymousClass1Y3.A18 /*147*/:
                    return "com.facebook.";
                case AnonymousClass1Y3.A19 /*148*/:
                    return "com.facebook.messenger";
                case AnonymousClass1Y3.A1A /*149*/:
                    return "compressed";
                case AnonymousClass1Y3.A1B /*150*/:
                    return "contents";
                case 151:
                    return "deleted";
                case 152:
                    return OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_FAILURE;
                case AnonymousClass1Y3.A1C /*153*/:
                    return "fb-messenger-sametask";
                case 154:
                    return "fb-messenger-secure";
                case 155:
                    return Experiments.ENABLE_IO_LOGGING_ACROSS_ADD_DEXES;
                case AnonymousClass1Y3.A1D /*156*/:
                    return "fb_jpegturbo";
                case AnonymousClass1Y3.A1E /*157*/:
                    return "h:mm:ss a";
                case 158:
                    return "handler";
                case 159:
                    return "healthstats";
                case 160:
                    return "install";
                case AnonymousClass1Y3.A1F /*161*/:
                    return "internalprefs";
                case AnonymousClass1Y3.A1G /*162*/:
                    return "invalid";
                case AnonymousClass1Y3.A1H /*163*/:
                    return "java.vm.version";
                case 164:
                    return "logging_level";
                case 165:
                    return ErrorReportingConstants.DEVICE_ID_KEY;
                case 166:
                    return "mqtt_health_stats";
                case AnonymousClass1Y3.A1I /*167*/:
                    return "not set";
                case 168:
                    return "os.arch";
                case 169:
                    return "package";
                case AnonymousClass1Y3.A1J /*170*/:
                    return "postCallback";
                case AnonymousClass1Y3.A1K /*171*/:
                    return "read_bytes";
                case AnonymousClass1Y3.A1L /*172*/:
                    return "received";
                case 173:
                    return "registered";
                case AnonymousClass1Y3.A1M /*174*/:
                    return "transient_network_data";
                case AnonymousClass1Y3.A1N /*175*/:
                    return "undefined";
                case AnonymousClass1Y3.A1O /*176*/:
                    return "video_logging_level";
                case AnonymousClass1Y3.A1P /*177*/:
                    return "webview";
                case 178:
                    return "xW-31ZG6ZwTfBH_Zj1NTcv6gAhE";
                case 179:
                    return "/proc/self/stat";
                case AnonymousClass1Y3.A1Q /*180*/:
                    return "ExecutionException";
                case AnonymousClass1Y3.A1R /*181*/:
                    return "android.intent.action.MAIN";
                case 182:
                    return "android.permission.READ_CALENDAR";
                case 183:
                    return "app_backgrounded";
                case 184:
                    return "app_version_code";
                case AnonymousClass1Y3.A1S /*185*/:
                    return "compactdisk-current-jni";
                case 186:
                    return "connection_class";
                case AnonymousClass1Y3.A1T /*187*/:
                    return C52652jT.A00;
                case AnonymousClass1Y3.A1U /*188*/:
                    return "manufacturer";
                case AnonymousClass1Y3.A1V /*189*/:
                    return "message_type";
                case AnonymousClass1Y3.A1W /*190*/:
                    return "notification";
                case AnonymousClass1Y3.A1X /*191*/:
                    return "null reference";
                case 192:
                    return "process_name";
                default:
                    return "removeCallbacks";
            }
        }
    }

    private native void init();

    private native void install(DexFile[] dexFileArr, DexFile[] dexFileArr2, String str, boolean z, String str2);

    private native Locator locateClassNative(String str, String str2);

    static {
        AnonymousClass01q.A08("turboloader");
    }

    public TurboLoader(Context context, List list, List list2, File file) {
        DexFile[] dexFileArr;
        DexFile[] dexFileArr2 = null;
        if (list != null) {
            dexFileArr = (DexFile[]) list.toArray(new DexFile[list.size()]);
        } else {
            dexFileArr = null;
        }
        this.mPrimaryDexes = dexFileArr;
        this.mAuxDexes = list2 != null ? (DexFile[]) list2.toArray(new DexFile[list2.size()]) : dexFileArr2;
        this.turboLoaderTempDir = new File(context.getCacheDir(), "turbo_loader_tmp");
        this.turboLoaderMapFile = new File(file, "classmap.bin").getAbsolutePath();
        if (AnonymousClass0MP.A00) {
            init();
        }
    }

    public void install(List list, boolean z) {
        DexFile[] dexFileArr;
        if (list != null) {
            dexFileArr = (DexFile[]) list.toArray(new DexFile[list.size()]);
        } else {
            dexFileArr = null;
        }
        this.mSecondaryDexes = dexFileArr;
        if (AnonymousClass0MP.A00) {
            install(this.mPrimaryDexes, dexFileArr, this.turboLoaderMapFile, z, this.turboLoaderTempDir.getAbsolutePath());
        }
    }
}
