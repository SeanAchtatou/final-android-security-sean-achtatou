package com.facebook.common.dextricks.verifier;

import X.AnonymousClass01q;
import X.AnonymousClass1Y3;
import android.content.Context;
import android.os.Build;
import android.util.Log;

public class Verifier {
    public static boolean sDisabledRuntimeVerification;
    private static final boolean sHasNativeCode;
    public static boolean sTriedDisableRuntimeVerification;

    private static native boolean disableRuntimeVerification_6_0_1();

    private static native boolean disableRuntimeVerification_7_0_0();

    private static native boolean disableRuntimeVerification_7_1_2();

    private static native boolean disableRuntimeVerification_8_0_0();

    private static native boolean disableRuntimeVerification_8_1_0();

    private static native boolean disableRuntimeVerification_9_0_0();

    static {
        boolean z;
        try {
            AnonymousClass01q.A08("verifier");
            z = true;
        } catch (Throwable unused) {
            z = false;
        }
        sHasNativeCode = z;
    }

    public static synchronized void disableRuntimeVerification(Context context) {
        synchronized (Verifier.class) {
            if (!sTriedDisableRuntimeVerification && sHasNativeCode) {
                switch (Build.VERSION.SDK_INT) {
                    case 23:
                        sDisabledRuntimeVerification = disableRuntimeVerification_6_0_1();
                        sTriedDisableRuntimeVerification = true;
                        break;
                    case AnonymousClass1Y3.A07 /*24*/:
                        sDisabledRuntimeVerification = disableRuntimeVerification_7_0_0();
                        sTriedDisableRuntimeVerification = true;
                        break;
                    case 25:
                        sDisabledRuntimeVerification = disableRuntimeVerification_7_1_2();
                        sTriedDisableRuntimeVerification = true;
                        break;
                    case AnonymousClass1Y3.A08 /*26*/:
                        sDisabledRuntimeVerification = disableRuntimeVerification_8_0_0();
                        sTriedDisableRuntimeVerification = true;
                        break;
                    case AnonymousClass1Y3.A09 /*27*/:
                        sDisabledRuntimeVerification = disableRuntimeVerification_8_1_0();
                        sTriedDisableRuntimeVerification = true;
                        break;
                    case 28:
                        sDisabledRuntimeVerification = disableRuntimeVerification_9_0_0();
                        sTriedDisableRuntimeVerification = true;
                        break;
                    default:
                        sDisabledRuntimeVerification = false;
                        sTriedDisableRuntimeVerification = false;
                        break;
                }
                if (sTriedDisableRuntimeVerification && !sDisabledRuntimeVerification) {
                    Log.w("Verifier", "Could not disable runtime verification");
                }
            }
        }
    }
}
