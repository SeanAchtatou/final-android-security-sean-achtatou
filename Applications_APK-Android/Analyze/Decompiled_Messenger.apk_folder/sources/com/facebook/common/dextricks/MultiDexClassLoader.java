package com.facebook.common.dextricks;

import X.AnonymousClass00K;
import X.AnonymousClass00L;
import X.AnonymousClass00M;
import X.AnonymousClass08S;
import X.AnonymousClass0HK;
import X.C011108x;
import android.content.Context;
import android.os.Build;
import android.os.SystemProperties;
import android.util.Log;
import com.facebook.acra.CustomReportDataSupplier;
import com.facebook.acra.ErrorReporter;
import com.facebook.common.dextricks.stats.ClassLoadingStats;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import dalvik.system.DexFile;
import io.card.payment.BuildConfig;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class MultiDexClassLoader extends ClassLoader {
    public static final ClassLoader APP_CLASSLOADER;
    private static final int BASE_DEX_RETRY_WAIT_MS = 500;
    private static final Field CLASSLOADER_PARENT_FIELD;
    private static final Object INSTALL_LOCK = new Object();
    private static final int MAX_LOAD_DEX_RETRY = 3;
    private static final ClassLoader SYSTEM_CLASSLOADER;
    public static final String TAG = "MultiDexClassLoader";
    private static final boolean USE_DALVIK_NATIVE_LOADER = true;
    private static final boolean USE_FANCY_LOADER = true;
    private static volatile AnonymousClass0HK sFallbackDexLoader;
    public static Throwable sFancyLoaderFailure;
    public static volatile ClassLoader sInstalledClassLoader;
    public Configuration mConfig;
    public final ClassLoader mPutativeLoader = APP_CLASSLOADER;
    public TurboLoader mTurboLoader;

    public final class Configuration {
        public static final int LOAD_SECONDARY = 4;
        public static final int SUPPORTS_LOCATORS = 2;
        public final boolean allowRetryAddDexOnIOException;
        public final ArrayList coldstartDexBaseNames = new ArrayList();
        public final int coldstartDexCount;
        public int configFlags;
        public final boolean disableVerifier;
        public final ArrayList mDexFiles = new ArrayList();
        public final AnonymousClass00L mFbColdStartExperiment;

        private void appendColdstartDexBaseName(File file) {
            if (this.coldstartDexBaseNames.size() < this.coldstartDexCount) {
                String name = file.getName();
                String substring = name.substring(0, name.indexOf(46));
                this.coldstartDexBaseNames.add(substring);
                DalvikInternals.addDexBaseNames(substring);
            }
        }

        public int getNumberConfiguredDexFiles() {
            return this.mDexFiles.size();
        }

        public Configuration(int i, int i2, boolean z, boolean z2, AnonymousClass00L r6) {
            this.configFlags = i;
            this.coldstartDexCount = i2;
            this.disableVerifier = z;
            this.allowRetryAddDexOnIOException = z2;
            this.mFbColdStartExperiment = r6;
        }

        public int getConfigFlags() {
            return this.configFlags;
        }

        public AnonymousClass00L getExperiment() {
            return this.mFbColdStartExperiment;
        }

        public void setConfigFlags(int i) {
            this.configFlags = i;
        }

        public void addDex(DexFile dexFile) {
            this.mDexFiles.add(dexFile);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, boolean):void
         arg types: [java.io.File, int]
         candidates:
          com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, java.io.File):void
          com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, boolean):void */
        public void addDex(File file) {
            addDex(file, false);
        }

        public void addDex(File file, File file2) {
            addDex(file, file2, false);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0011, code lost:
            if (r10.allowRetryAddDexOnIOException == false) goto L_0x0013;
         */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x004d  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0058  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void addDex(java.io.File r11, java.io.File r12, boolean r13) {
            /*
                r10 = this;
                java.lang.String r4 = r11.getAbsolutePath()
                r5 = 0
                if (r12 == 0) goto L_0x0056
                java.lang.String r7 = r12.getAbsolutePath()
            L_0x000b:
                r6 = 0
                if (r13 == 0) goto L_0x0013
                boolean r0 = r10.allowRetryAddDexOnIOException
                r9 = 1
                if (r0 != 0) goto L_0x0014
            L_0x0013:
                r9 = 0
            L_0x0014:
                r8 = 0
            L_0x0015:
                if (r8 <= 0) goto L_0x0025
                int r0 = r8 * 500
                long r0 = (long) r0
                java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x001e }
                goto L_0x0025
            L_0x001e:
                java.lang.Thread r0 = java.lang.Thread.currentThread()
                r0.interrupt()
            L_0x0025:
                int r8 = r8 + 1
                r3 = 3
                java.lang.String r0 = r11.getAbsolutePath()     // Catch:{ IOException -> 0x0031 }
                dalvik.system.DexFile r5 = dalvik.system.DexFile.loadDex(r0, r7, r6)     // Catch:{ IOException -> 0x0031 }
                goto L_0x0045
            L_0x0031:
                r2 = move-exception
                java.lang.Object[] r1 = new java.lang.Object[]{r4}
                java.lang.String r0 = "Failed loading dex ( %s )"
                java.lang.String r1 = java.lang.String.format(r0, r1)
                java.lang.String r0 = "MultiDexClassLoader"
                android.util.Log.w(r0, r1, r2)
                if (r9 == 0) goto L_0x0064
                if (r3 < r8) goto L_0x0064
            L_0x0045:
                if (r9 == 0) goto L_0x004b
                if (r5 != 0) goto L_0x004b
                if (r8 <= r3) goto L_0x0015
            L_0x004b:
                if (r5 == 0) goto L_0x0058
                java.util.ArrayList r0 = r10.mDexFiles
                r0.add(r5)
                r10.appendColdstartDexBaseName(r11)
                return
            L_0x0056:
                r7 = r5
                goto L_0x000b
            L_0x0058:
                java.io.IOException r1 = new java.io.IOException
                java.lang.String r0 = "Could not load dex file "
                java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r4)
                r1.<init>(r0)
                throw r1
            L_0x0064:
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, java.io.File, boolean):void");
        }

        public void addDex(File file, boolean z) {
            addDex(file, null, z);
        }
    }

    public static void clearFancyLoaderFailure() {
        sFancyLoaderFailure = null;
    }

    public static void forceLoadProfiloIfPresent() {
        String[] strArr = {"com.facebook.profilo.logger.api.ProfiloLogger", "com.facebook.profilo.core.TraceEvents", "com.facebook.profilo.entries.EntryType", "com.facebook.profilo.logger.ClassLoadLogger", "com.facebook.profilo.logger.Logger", "com.facebook.profilo.core.ProvidersRegistry"};
        for (int i = 0; i < 6; i++) {
            try {
                Class.forName(strArr[i]);
            } catch (ClassNotFoundException unused) {
            }
        }
    }

    public abstract DexFile[] doGetConfiguredDexFiles();

    public String[] getRecentFailedClasses() {
        return new String[0];
    }

    public abstract void onColdstartDone();

    static {
        try {
            APP_CLASSLOADER = MultiDexClassLoader.class.getClassLoader();
            Field declaredField = ClassLoader.class.getDeclaredField("parent");
            CLASSLOADER_PARENT_FIELD = declaredField;
            declaredField.setAccessible(true);
            SYSTEM_CLASSLOADER = (ClassLoader) CLASSLOADER_PARENT_FIELD.get(APP_CLASSLOADER);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public MultiDexClassLoader() {
        super(SYSTEM_CLASSLOADER);
    }

    private static ClassLoader createMultiDexClassLoader(Context context, ArrayList arrayList, ArrayList arrayList2, AnonymousClass00L r11) {
        ArrayList arrayList3 = arrayList;
        ArrayList arrayList4 = arrayList2;
        Context context2 = context;
        if (!"true".equals(SystemProperties.get("com.facebook.force_mdclj")) && !"Amazon".equals(Build.BRAND)) {
            try {
                if (!C011108x.A00) {
                    return new MultiDexClassLoaderDalvikNative(context, arrayList, arrayList2);
                }
                if (AnonymousClass00M.A00().A05()) {
                    boolean equals = "true".equals(SystemProperties.get("com.facebook.force_mdclan"));
                    int i = ((AnonymousClass00K) r11).A0T;
                    if (i == 1 || i == 2 || equals) {
                        ClassLoader classLoader = SYSTEM_CLASSLOADER;
                        boolean z = false;
                        if (i == 2) {
                            z = true;
                        }
                        return new MultiDexClassLoaderArtNative(context2, classLoader, arrayList3, arrayList4, z, equals);
                    }
                }
            } catch (Exception | NoSuchFieldError e) {
                Log.w(TAG, "unable to use native MDCL: falling back to Java impl", e);
                sFancyLoaderFailure = e;
            }
        }
        return new MultiDexClassLoaderJava(context2, arrayList3, arrayList2);
    }

    public static Configuration getConfiguration() {
        ClassLoader classLoader = sInstalledClassLoader;
        if (classLoader == null || !(classLoader instanceof MultiDexClassLoader)) {
            return null;
        }
        return ((MultiDexClassLoader) classLoader).mConfig;
    }

    public static DexFile[] getConfiguredDexFiles() {
        ClassLoader classLoader = sInstalledClassLoader;
        if (classLoader == null || !(classLoader instanceof MultiDexClassLoader)) {
            return new DexFile[0];
        }
        return ((MultiDexClassLoader) classLoader).doGetConfiguredDexFiles();
    }

    public static ClassLoader install(Context context, ArrayList arrayList, ArrayList arrayList2, AnonymousClass00L r7) {
        ClassLoader classLoader;
        RuntimeException runtimeException;
        ClassLoader classLoader2 = sInstalledClassLoader;
        if (classLoader2 != null) {
            return classLoader2;
        }
        synchronized (INSTALL_LOCK) {
            classLoader = sInstalledClassLoader;
            if (classLoader == null) {
                try {
                    Class.forName("com.facebook.common.dextricks.FatalDexError");
                    Class.forName("com.facebook.common.dextricks.DexFileLoadOld");
                    Class.forName("com.facebook.common.dextricks.DexFileLoadNew");
                    Class.forName("com.facebook.common.dextricks.stats.ClassLoadingStats");
                    Class.forName("com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats");
                    Class.forName("com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger");
                    Class.forName("com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerNativeHolder");
                    Class.forName("com.facebook.common.dextricks.classtracing.logger.ClassTracingLoggerLite");
                    Class.forName("com.facebook.common.dextricks.coverage.logger.ClassCoverageLogger");
                    Class.forName("com.facebook.common.dextricks.benchmarkhelper.ClassloadNameCollector");
                    Class.forName("com.facebook.common.dextricks.classid.ClassId");
                    forceLoadProfiloIfPresent();
                    ErrorReporter.getInstance().putLazyCustomData("recentClassLoadFailures", new CustomReportDataSupplier() {
                        public String getCustomData(Throwable th) {
                            ClassLoader classLoader = MultiDexClassLoader.sInstalledClassLoader;
                            if (classLoader instanceof MultiDexClassLoader) {
                                return Arrays.toString(((MultiDexClassLoader) classLoader).getRecentFailedClasses());
                            }
                            return BuildConfig.FLAVOR;
                        }
                    });
                    ErrorReporter.getInstance().putLazyCustomData("multiDexClassLoader", new CustomReportDataSupplier() {
                        public String getCustomData(Throwable th) {
                            return MultiDexClassLoader.sInstalledClassLoader.toString();
                        }
                    });
                    classLoader = createMultiDexClassLoader(context, arrayList, arrayList2, r7);
                    try {
                        if (classLoader instanceof MultiDexClassLoader) {
                            CLASSLOADER_PARENT_FIELD.set(((MultiDexClassLoader) classLoader).mPutativeLoader, classLoader);
                        }
                        sInstalledClassLoader = classLoader;
                    } catch (IllegalAccessException e) {
                        runtimeException = new RuntimeException(e);
                        throw runtimeException;
                    }
                } catch (ClassNotFoundException e2) {
                    runtimeException = new RuntimeException(e2);
                    throw runtimeException;
                }
            }
        }
        return classLoader;
    }

    private static boolean isArt() {
        if (Build.VERSION.SDK_INT >= 21) {
            return true;
        }
        return false;
    }

    public static final boolean maybeFallbackLoadDexes(String str, Throwable th) {
        AnonymousClass0HK r0 = sFallbackDexLoader;
        if (r0 == null) {
            return false;
        }
        return r0.onClassNotFound(str, th);
    }

    public static void notifyCurrentClassLoaderThatColdstartDone() {
        ClassLoader classLoader = sInstalledClassLoader;
        if (classLoader != null && (classLoader instanceof MultiDexClassLoader)) {
            ((MultiDexClassLoader) classLoader).onColdstartDone();
        }
    }

    public MultiDexClassLoader configureTurboLoader(Context context, List list, List list2, Configuration configuration, File file) {
        boolean exists;
        TurboLoader turboLoader = new TurboLoader(context, list, list2, file);
        this.mTurboLoader = turboLoader;
        boolean z = false;
        if ((configuration.configFlags & 2) != 0) {
            z = true;
        }
        turboLoader.install(configuration.mDexFiles, z);
        String str = this.mTurboLoader.turboLoaderMapFile;
        if (str == null) {
            exists = false;
        } else {
            exists = new File(str).exists();
        }
        if (exists) {
            ClassLoadingStats.A00().incrementTurboLoaderMapGenerationSuccesses();
            return this;
        }
        ClassLoadingStats.A00().incrementTurboLoaderMapGenerationFailures();
        return this;
    }

    public static ClassLoader get() {
        return sInstalledClassLoader;
    }

    public static Throwable getFancyLoaderFailure() {
        return sFancyLoaderFailure;
    }

    public void configureArtHacks(Configuration configuration) {
        if (isArt()) {
            int i = 0;
            if (configuration.disableVerifier) {
                i = 4;
            }
            if (i != 0) {
                DalvikInternals.installArtHacks(i, Build.VERSION.SDK_INT);
            }
        }
    }

    public Configuration getConfig() {
        return this.mConfig;
    }

    public final Class maybeFallbackLoadClass(String str, Throwable th) {
        ClassNotFoundException classNotFoundException;
        ClassNotFoundException classNotFoundException2;
        try {
            if (maybeFallbackLoadDexes(str, th)) {
                Class<?> findClass = findClass(str);
                if (findClass != null) {
                    return findClass;
                }
                Log.w(TAG, AnonymousClass08S.A0J("findClass failed without throwing for ", str));
            }
            if (th instanceof ClassNotFoundException) {
                classNotFoundException = (ClassNotFoundException) th;
                throw classNotFoundException;
            }
            classNotFoundException2 = new ClassNotFoundException(str, th);
            classNotFoundException = classNotFoundException2;
            throw classNotFoundException;
        } catch (RuntimeException e) {
            classNotFoundException2 = new ClassNotFoundException(AnonymousClass08S.A0J("Fallback class load failed for ", str), e);
        }
    }

    public void notifyColdstartDone() {
        onColdstartDone();
    }

    public static void setFallbackDexLoader(AnonymousClass0HK r0) {
        sFallbackDexLoader = r0;
    }

    public void configure(Configuration configuration) {
        this.mConfig = configuration;
    }
}
