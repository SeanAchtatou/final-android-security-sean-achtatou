package com.facebook.common.dextricks;

import android.text.TextUtils;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.common.dextricks.DexOptRunner;
import com.facebook.common.dextricks.DexStore;
import com.facebook.common.dextricks.OdexScheme;
import com.facebook.common.dextricks.ReentrantLockFile;
import com.facebook.forker.Process;
import com.facebook.forker.ProcessBuilder;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.Iterator;

public final class OdexSchemeXdex extends OdexSchemeTurbo {
    private final DexManifest.Dex[] mDexes;

    public final class DexToOptimize implements Closeable {
        public final int dexNr;
        public final ReentrantLockFile.Lock signalLock;

        public void close() {
            this.signalLock.close();
        }

        public String toString() {
            return String.format("DexToOptimize(dexNr=%d)", Integer.valueOf(this.dexNr));
        }

        public DexToOptimize(int i, ReentrantLockFile.Lock lock) {
            this.dexNr = i;
            this.signalLock = lock;
        }
    }

    public final class PoliteDexOptRunner extends DexOptRunner {
        private final byte[] mBuffer = new byte[DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED];
        private final DexStore.OptimizationSession mOptimizationSession;

        public void addDexOptOptions(ProcessBuilder processBuilder) {
            processBuilder.mArgv.add("-n");
            OdexSchemeTurbo.addConfiguredDexOptOptions(this.mOptimizationSession.dexStoreConfig, processBuilder);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
            if (r1 != null) goto L_0x001a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
            r0 = move-exception;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.facebook.forker.Process startSubprocess(com.facebook.forker.ProcessBuilder r3) {
            /*
                r2 = this;
                com.facebook.common.dextricks.DexStore$OptimizationSession r0 = r2.mOptimizationSession
                com.facebook.common.dextricks.OptimizationConfiguration r0 = r0.config
                com.facebook.common.dextricks.Prio r0 = r0.prio
                com.facebook.common.dextricks.Prio$With r1 = new com.facebook.common.dextricks.Prio$With
                r1.<init>()
                com.facebook.forker.Process r0 = r3.create()     // Catch:{ all -> 0x0015 }
                if (r1 == 0) goto L_0x0014
                r1.close()
            L_0x0014:
                return r0
            L_0x0015:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0017 }
            L_0x0017:
                r0 = move-exception
                if (r1 == 0) goto L_0x001d
                r1.close()     // Catch:{ all -> 0x001d }
            L_0x001d:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeXdex.PoliteDexOptRunner.startSubprocess(com.facebook.forker.ProcessBuilder):com.facebook.forker.Process");
        }

        public void waitForDexOpt(Process process, int i) {
            this.mOptimizationSession.waitForAndManageProcess(process, null);
        }

        public PoliteDexOptRunner(DexStore.OptimizationSession optimizationSession, File file) {
            super(file);
            this.mOptimizationSession = optimizationSession;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x003d, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
            if (r5 != null) goto L_0x0040;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            r5.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0043, code lost:
            throw r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int copyDexToOdex(java.io.InputStream r8, int r9, java.io.RandomAccessFile r10) {
            /*
                r7 = this;
                java.io.FileDescriptor r0 = r10.getFD()
                int r6 = com.facebook.forker.Fd.fileno(r0)
                com.facebook.common.dextricks.DexStore$OptimizationSession r0 = r7.mOptimizationSession
                com.facebook.common.dextricks.OptimizationConfiguration r0 = r0.config
                com.facebook.common.dextricks.Prio r0 = r0.prio
                com.facebook.common.dextricks.Prio r0 = r0.ioOnly()
                com.facebook.common.dextricks.Prio$With r5 = new com.facebook.common.dextricks.Prio$With
                r5.<init>()
                r4 = 0
                r3 = 0
            L_0x0019:
                byte[] r1 = r7.mBuffer     // Catch:{ all -> 0x003b }
                int r0 = r1.length     // Catch:{ all -> 0x003b }
                int r2 = X.AnonymousClass0Me.A0A(r8, r1, r0)     // Catch:{ all -> 0x003b }
                r1 = -1
                if (r2 == r1) goto L_0x0035
                byte[] r0 = r7.mBuffer     // Catch:{ all -> 0x003b }
                r10.write(r0, r4, r2)     // Catch:{ all -> 0x003b }
                int r3 = r3 + r2
                com.facebook.common.dextricks.DexStore$OptimizationSession r0 = r7.mOptimizationSession     // Catch:{ all -> 0x003b }
                r0.checkShouldStop()     // Catch:{ all -> 0x003b }
                com.facebook.common.dextricks.DalvikInternals.fdatasync(r6, r1)     // Catch:{ all -> 0x003b }
                com.facebook.common.dextricks.Fs.tryDiscardPageCache(r6)     // Catch:{ all -> 0x003b }
                goto L_0x0019
            L_0x0035:
                if (r5 == 0) goto L_0x003a
                r5.close()
            L_0x003a:
                return r3
            L_0x003b:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x003d }
            L_0x003d:
                r0 = move-exception
                if (r5 == 0) goto L_0x0043
                r5.close()     // Catch:{ all -> 0x0043 }
            L_0x0043:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeXdex.PoliteDexOptRunner.copyDexToOdex(java.io.InputStream, int, java.io.RandomAccessFile):int");
        }
    }

    public OdexSchemeXdex(DexManifest.Dex[] dexArr) {
        super(1, dexArr);
        this.mDexes = dexArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003f, code lost:
        if (r2 != null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.common.dextricks.OdexSchemeXdex.DexToOptimize findDexToOptimize(com.facebook.common.dextricks.DexStore r8, long r9) {
        /*
            r7 = this;
            r4 = 0
            r3 = 0
        L_0x0002:
            com.facebook.common.dextricks.DexManifest$Dex[] r0 = r7.mDexes
            int r1 = r0.length
            r0 = 0
            if (r3 >= r1) goto L_0x0045
            r5 = 16
            long r5 = r5 << r3
            long r5 = r5 & r9
            r1 = 0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x002e
            java.lang.String[] r1 = r7.expectedFiles
            int r0 = r3 << 1
            int r0 = r0 + r4
            r2 = r1[r0]
            java.io.File r1 = new java.io.File
            java.io.File r0 = r8.root
            r1.<init>(r0, r2)
            com.facebook.common.dextricks.ReentrantLockFile r2 = com.facebook.common.dextricks.ReentrantLockFile.open(r1)
            com.facebook.common.dextricks.ReentrantLockFile$Lock r1 = r2.tryAcquire(r4)     // Catch:{ all -> 0x003c }
            r2.close()
            if (r1 == 0) goto L_0x002e
            goto L_0x0031
        L_0x002e:
            int r3 = r3 + 1
            goto L_0x0002
        L_0x0031:
            com.facebook.common.dextricks.OdexSchemeXdex$DexToOptimize r0 = new com.facebook.common.dextricks.OdexSchemeXdex$DexToOptimize     // Catch:{ all -> 0x0037 }
            r0.<init>(r3, r1)     // Catch:{ all -> 0x0037 }
            return r0
        L_0x0037:
            r0 = move-exception
            r1.close()
            throw r0
        L_0x003c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003e }
        L_0x003e:
            r0 = move-exception
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ all -> 0x0044 }
        L_0x0044:
            throw r0
        L_0x0045:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeXdex.findDexToOptimize(com.facebook.common.dextricks.DexStore, long):com.facebook.common.dextricks.OdexSchemeXdex$DexToOptimize");
    }

    public String getSchemeName() {
        return "OdexSchemeXdex";
    }

    private boolean isFileCorruptionException(DexOptRunner.DexOptException dexOptException) {
        String str;
        if (dexOptException.status != 1 || (str = dexOptException.errout) == null) {
            return false;
        }
        int indexOf = str.indexOf("E/dalvikvm: ERROR: bad checksum");
        if (indexOf == 0 || (indexOf > 0 && str.charAt(indexOf - 1) == 10)) {
            return true;
        }
        return false;
    }

    private PartialInputStream openDexInsideOdex(FileInputStream fileInputStream) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        FileChannel channel = fileInputStream.getChannel();
        channel.position(8L);
        allocate.order(ByteOrder.nativeOrder());
        if (channel.read(allocate) == 8) {
            boolean z = false;
            allocate.position(0);
            int i = allocate.getInt();
            int i2 = allocate.getInt();
            Mlog.safeFmt("dexOffset:%s dexLength:%s", Integer.valueOf(i), Integer.valueOf(i2));
            boolean z2 = false;
            if (i <= 0) {
                z2 = true;
            }
            if (i2 <= 0) {
                z = true;
            }
            if (!z && !z2) {
                channel.position((long) i);
                return new PartialInputStream(fileInputStream, i2);
            }
        }
        throw new IllegalArgumentException("invalid odex file");
    }

    public OdexScheme.NeedOptimizationState needOptimization(long j, DexStore.Config config, DexStore.OptimizationHistoryLog optimizationHistoryLog) {
        boolean z = true;
        long length = (long) ((1 << this.mDexes.length) - 1);
        long j2 = j >> 4;
        Mlog.safeFmt("expectedDexBits:0x%08x actualDexBits:0x%08x", Long.valueOf(length), Long.valueOf(j2));
        if (length == j2) {
            z = false;
        }
        return OdexScheme.NeedOptimizationState.shouldOptimize(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:48:0x00ac */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void optimize(android.content.Context r17, com.facebook.common.dextricks.DexStore r18, com.facebook.common.dextricks.DexStore.OptimizationSession r19) {
        /*
            r16 = this;
            r7 = r16
            r3 = r18
            java.io.File[] r14 = r3.getDependencyOdexFiles()
            java.lang.String r0 = r3.getOdexCachePath()
            byte[] r15 = com.facebook.common.dextricks.DalvikInternals.readOdexDepBlock(r0)
            java.lang.String r0 = "dexopt"
            com.facebook.common.dextricks.DexStore$TmpDir r2 = r3.makeTemporaryDirectory(r0)
            java.lang.String r1 = "[opt] opened tmpDir %s"
            r5 = 1
            java.io.File r0 = r2.directory     // Catch:{ all -> 0x00ad }
            r4 = 0
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00ad }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00ad }
            r10 = r19
            com.facebook.common.dextricks.OptimizationConfiguration r0 = r10.config     // Catch:{ all -> 0x00ad }
            int r1 = r0.flags     // Catch:{ all -> 0x00ad }
            r1 = r1 & r5
            r0 = 0
            if (r1 == 0) goto L_0x002e
            r0 = 1
        L_0x002e:
            if (r0 == 0) goto L_0x0038
            com.facebook.common.dextricks.OdexSchemeXdex$PoliteDexOptRunner r12 = new com.facebook.common.dextricks.OdexSchemeXdex$PoliteDexOptRunner     // Catch:{ all -> 0x00ad }
            java.io.File r0 = r2.directory     // Catch:{ all -> 0x00ad }
            r12.<init>(r10, r0)     // Catch:{ all -> 0x00ad }
            goto L_0x0041
        L_0x0038:
            com.facebook.common.dextricks.OdexSchemeTurbo$TurboDexOptRunner r12 = new com.facebook.common.dextricks.OdexSchemeTurbo$TurboDexOptRunner     // Catch:{ all -> 0x00ad }
            com.facebook.common.dextricks.DexStore$Config r1 = r10.dexStoreConfig     // Catch:{ all -> 0x00ad }
            java.io.File r0 = r2.directory     // Catch:{ all -> 0x00ad }
            r12.<init>(r1, r0)     // Catch:{ all -> 0x00ad }
        L_0x0041:
            r6 = 0
        L_0x0042:
            java.lang.String r1 = "[opt] starting optimization pass; creating job"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x00ad }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00ad }
            com.facebook.common.dextricks.DexStore$OptimizationSession$Job r11 = new com.facebook.common.dextricks.DexStore$OptimizationSession$Job     // Catch:{ all -> 0x00ad }
            r11.<init>()     // Catch:{ all -> 0x00ad }
            java.lang.String r1 = "[opt] opened job"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x00a6 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00a6 }
            if (r6 != 0) goto L_0x005d
            java.io.File r0 = r2.directory     // Catch:{ all -> 0x00a6 }
            r7.prepareTmpDirForXdex(r14, r3, r0)     // Catch:{ all -> 0x00a6 }
            r6 = 1
        L_0x005d:
            long r0 = r11.initialStatus     // Catch:{ all -> 0x00a6 }
            com.facebook.common.dextricks.OdexSchemeXdex$DexToOptimize r13 = r7.findDexToOptimize(r3, r0)     // Catch:{ all -> 0x00a6 }
            java.lang.String r1 = "[opt] dto %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r13}     // Catch:{ all -> 0x00a6 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00a6 }
            if (r13 == 0) goto L_0x008c
            java.io.File r9 = r2.directory     // Catch:{ DexOptException -> 0x0075 }
            r8 = r3
            r7.optimize1(r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ DexOptException -> 0x0075 }
            goto L_0x0089
        L_0x0075:
            r1 = move-exception
            boolean r0 = com.facebook.common.dextricks.Fs.isKernelPageCacheFlushIsBroken     // Catch:{ all -> 0x0093 }
            if (r0 != 0) goto L_0x0092
            boolean r0 = r7.isFileCorruptionException(r1)     // Catch:{ all -> 0x0093 }
            if (r0 == 0) goto L_0x0092
            java.lang.String r1 = "detected odex file corruption: trying again with kernel workaround"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x0093 }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x0093 }
            com.facebook.common.dextricks.Fs.isKernelPageCacheFlushIsBroken = r5     // Catch:{ all -> 0x0093 }
        L_0x0089:
            r13.close()     // Catch:{ all -> 0x00a6 }
        L_0x008c:
            r11.close()     // Catch:{ all -> 0x00ad }
            if (r13 != 0) goto L_0x0042
            goto L_0x0098
        L_0x0092:
            throw r1     // Catch:{ all -> 0x0093 }
        L_0x0093:
            r0 = move-exception
            r13.close()     // Catch:{ all -> 0x00a6 }
            throw r0     // Catch:{ all -> 0x00a6 }
        L_0x0098:
            r12.cleanup()     // Catch:{ all -> 0x00ad }
            java.lang.String r1 = "[opt] optimization complete"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x00ad }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00ad }
            r2.close()
            return
        L_0x00a6:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a8 }
        L_0x00a8:
            r0 = move-exception
            r11.close()     // Catch:{ all -> 0x00ac }
        L_0x00ac:
            throw r0     // Catch:{ all -> 0x00ad }
        L_0x00ad:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00af }
        L_0x00af:
            r0 = move-exception
            if (r2 == 0) goto L_0x00b5
            r2.close()     // Catch:{ all -> 0x00b5 }
        L_0x00b5:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeXdex.optimize(android.content.Context, com.facebook.common.dextricks.DexStore, com.facebook.common.dextricks.DexStore$OptimizationSession):void");
    }

    private void makeFakeCacheSymlink(File file, File file2, File file3) {
        String str;
        if (!file2.exists()) {
            throw new IllegalStateException("expected file to exist: " + file2);
        } else if (file3.exists()) {
            if (file2.getPath().endsWith(".jar")) {
                str = "classes.dex";
            } else {
                str = null;
            }
            File dexOptGenerateCacheFileName = Fs.dexOptGenerateCacheFileName(file, file2, str);
            Mlog.safeFmt("[opt] symlink %s -> %s", dexOptGenerateCacheFileName, file3);
            Fs.symlink(file3, dexOptGenerateCacheFileName);
        } else {
            throw new IllegalStateException("expected file to exist: " + file3);
        }
    }

    private void prepareTmpDirForXdex(File[] fileArr, DexStore dexStore, File file) {
        int i;
        String str;
        File findSystemDalvikCache = Fs.findSystemDalvikCache();
        File file2 = new File(file, "dalvik-cache");
        Fs.mkdirOrThrow(file2);
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
        simpleStringSplitter.setString(System.getenv("BOOTCLASSPATH"));
        Iterator<String> it = simpleStringSplitter.iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            String next = it.next();
            if (next.length() != 0) {
                if (next.endsWith(".jar")) {
                    str = "classes.dex";
                } else {
                    str = null;
                }
                File file3 = new File(next);
                File dexOptGenerateCacheFileName = Fs.dexOptGenerateCacheFileName(file2, file3, str);
                File dexOptGenerateCacheFileName2 = Fs.dexOptGenerateCacheFileName(findSystemDalvikCache, file3, str);
                Mlog.safeFmt("[opt] symlink %s -> %s", dexOptGenerateCacheFileName, dexOptGenerateCacheFileName2);
                Fs.symlink(dexOptGenerateCacheFileName2, dexOptGenerateCacheFileName);
            }
        }
        for (int i2 = 0; i2 < fileArr.length; i2 += 2) {
            makeFakeCacheSymlink(file2, fileArr[i2], fileArr[i2 + 1]);
        }
        while (true) {
            String[] strArr = this.expectedFiles;
            if (i < strArr.length) {
                makeFakeCacheSymlink(file2, new File(dexStore.root, strArr[i]), new File(dexStore.root, this.expectedFiles[i + 1]));
                i += 2;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:47|(2:49|50)|51|52) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:39|40|41|42|43) */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0100, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x0104 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x010d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void optimize1(com.facebook.common.dextricks.DexStore r20, java.io.File r21, com.facebook.common.dextricks.DexStore.OptimizationSession r22, com.facebook.common.dextricks.DexStore.OptimizationSession.Job r23, com.facebook.common.dextricks.DexOptRunner r24, com.facebook.common.dextricks.OdexSchemeXdex.DexToOptimize r25, java.io.File[] r26, byte[] r27) {
        /*
            r19 = this;
            r10 = r19
            r8 = r23
            r8.startOptimizing()
            java.lang.String[] r2 = r10.expectedFiles
            r9 = r25
            int r0 = r9.dexNr
            r3 = 2
            int r1 = r0 << 1
            r7 = 0
            int r0 = r1 + r7
            r15 = r2[r0]
            r4 = 1
            int r1 = r1 + r4
            r2 = r2[r1]
            java.io.File r6 = new java.io.File
            r0 = r21
            r6.<init>(r0, r2)
            java.io.File r1 = new java.io.File
            r13 = r20
            java.io.File r0 = r13.root
            r1.<init>(r0, r15)
            java.io.File r5 = new java.io.File
            java.io.File r0 = r13.root
            r5.<init>(r0, r2)
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r5}
            java.lang.String r0 = "[opt] started optimizing %s -> %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            r12 = r26
            int r2 = r12.length
            int r1 = r2 / r3
            java.lang.String[] r0 = r10.expectedFiles
            int r0 = r0.length
            int r0 = r0 / r3
            int r1 = r1 + r0
            int r1 = r1 - r4
            java.lang.String[] r3 = new java.lang.String[r1]
            r1 = 0
            r11 = 0
        L_0x0048:
            if (r1 >= r2) goto L_0x0058
            int r14 = r11 + 1
            r0 = r26[r1]
            java.lang.String r0 = r0.getPath()
            r3[r11] = r0
            int r1 = r1 + 2
            r11 = r14
            goto L_0x0048
        L_0x0058:
            r12 = 0
        L_0x0059:
            java.lang.String[] r0 = r10.expectedFiles
            int r1 = r0.length
            if (r12 >= r1) goto L_0x0079
            int r2 = r12 >> 1
            int r1 = r9.dexNr
            if (r2 == r1) goto L_0x0076
            int r14 = r11 + 1
            java.io.File r2 = new java.io.File
            java.io.File r1 = r13.root
            r0 = r0[r12]
            r2.<init>(r1, r0)
            java.lang.String r0 = r2.getPath()
            r3[r11] = r0
            r11 = r14
        L_0x0076:
            int r12 = r12 + 2
            goto L_0x0059
        L_0x0079:
            int r0 = r3.length
            r2 = 0
            if (r11 != r0) goto L_0x007e
            r2 = 1
        L_0x007e:
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.String r0 = "accounted for all dex files"
            com.facebook.common.dextricks.Mlog.assertThat(r2, r0, r1)
            java.io.FileInputStream r1 = new java.io.FileInputStream
            r1.<init>(r5)
            com.facebook.common.dextricks.PartialInputStream r13 = r10.openDexInsideOdex(r1)     // Catch:{ all -> 0x010e }
            int r14 = r13.available()     // Catch:{ all -> 0x0105 }
            if (r14 > r4) goto L_0x0095
            r14 = -1
        L_0x0095:
            java.lang.String r2 = "[opt] size hint for %s: %s"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)     // Catch:{ all -> 0x0105 }
            java.lang.Object[] r0 = new java.lang.Object[]{r5, r0}     // Catch:{ all -> 0x0105 }
            com.facebook.common.dextricks.Mlog.safeFmt(r2, r0)     // Catch:{ all -> 0x0105 }
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0105 }
            java.lang.String r0 = "rw"
            r2.<init>(r6, r0)     // Catch:{ all -> 0x0105 }
            java.lang.String r17 = "xdex"
            r12 = r24
            r16 = r2
            r18 = r3
            r12.run(r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x00f9 }
            java.io.FileDescriptor r0 = r2.getFD()     // Catch:{ all -> 0x00fe }
            int r3 = com.facebook.forker.Fd.fileno(r0)     // Catch:{ all -> 0x00fe }
            r0 = r27
            com.facebook.common.dextricks.DalvikInternals.replaceOdexDepBlock(r3, r0)     // Catch:{ all -> 0x00fe }
            r0 = r22
            com.facebook.common.dextricks.OptimizationConfiguration r0 = r0.config     // Catch:{ all -> 0x00fe }
            com.facebook.common.dextricks.Prio r0 = r0.prio     // Catch:{ all -> 0x00fe }
            int r0 = r0.ioPriority     // Catch:{ all -> 0x00fe }
            com.facebook.common.dextricks.DalvikInternals.fsync(r3, r0)     // Catch:{ all -> 0x00fe }
            com.facebook.common.dextricks.Fs.tryDiscardPageCache(r3)     // Catch:{ all -> 0x00fe }
            r2.close()     // Catch:{ all -> 0x0105 }
            r13.close()     // Catch:{ all -> 0x010e }
            r1.close()
            r0 = 0
            long r3 = r8.startCommitting(r0)
            r1 = 16
            int r0 = r9.dexNr
            long r1 = r1 << r0
            long r3 = r3 | r1
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.String r0 = "[opt] started commit"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            com.facebook.common.dextricks.Fs.renameOrThrow(r6, r5)
            r8.finishCommit(r3)
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.String r0 = "[opt] finished commit"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return
        L_0x00f9:
            r0 = move-exception
            com.facebook.common.dextricks.Fs.deleteRecursive(r6)     // Catch:{ all -> 0x00fe }
            throw r0     // Catch:{ all -> 0x00fe }
        L_0x00fe:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0100 }
        L_0x0100:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0104 }
        L_0x0104:
            throw r0     // Catch:{ all -> 0x0105 }
        L_0x0105:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0107 }
        L_0x0107:
            r0 = move-exception
            if (r13 == 0) goto L_0x010d
            r13.close()     // Catch:{ all -> 0x010d }
        L_0x010d:
            throw r0     // Catch:{ all -> 0x010e }
        L_0x010e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0110 }
        L_0x0110:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0114 }
        L_0x0114:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeXdex.optimize1(com.facebook.common.dextricks.DexStore, java.io.File, com.facebook.common.dextricks.DexStore$OptimizationSession, com.facebook.common.dextricks.DexStore$OptimizationSession$Job, com.facebook.common.dextricks.DexOptRunner, com.facebook.common.dextricks.OdexSchemeXdex$DexToOptimize, java.io.File[], byte[]):void");
    }
}
