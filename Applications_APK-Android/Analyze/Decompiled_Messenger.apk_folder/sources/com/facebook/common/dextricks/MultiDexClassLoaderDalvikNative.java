package com.facebook.common.dextricks;

import X.AnonymousClass00K;
import X.AnonymousClass01q;
import X.AnonymousClass08S;
import X.AnonymousClass1Y3;
import android.content.Context;
import android.util.Log;
import com.facebook.common.dextricks.MultiDexClassLoader;
import com.facebook.common.dextricks.stats.ClassLoadingStats;
import com.facebook.common.dextricks.stats.ClassLoadingStatsNative;
import dalvik.system.DexFile;
import java.util.ArrayList;

public final class MultiDexClassLoaderDalvikNative extends MultiDexClassLoader {
    public static final int AVG_DEX_SIZE = 7340032;
    public static final int DEFAULT_LOCK_DEX_NUM = -1;
    public static final int MAX_COLDSTART_OAT_SIZE = 31457280;
    public static final int MB = 1048576;
    public static final int OAT_HEADER_SIZE = 1048576;
    private static final boolean USE_LOW_LEVEL_DALVIK_HOOKS = true;
    private static final boolean USE_O1_DALVIK_LOCATOR_HACK = true;
    private static final int WANT_PROC_EXEC = 1;
    public static boolean sIsIntialized;
    private final DexFile[] mAuxDexes;
    private boolean mDirectLookupsEnabled = false;
    private boolean mHacksAttempted = false;
    private boolean mO1HackEnabled = false;
    private final DexFile[] mPrimaryDexes;
    private DexFile[] mSecondaryDexes;

    private static native void nativeConfigure(Object[] objArr, Object[] objArr2, int i, int i2, int i3, int i4, int i5);

    private native void nativeEnableDirectLookupHooks();

    private static native void nativeEnableO1Hack();

    private native void nativeInitialize(Object obj, ClassLoadingStatsNative classLoadingStatsNative, Object[] objArr);

    private static native void setReplacementDvmDescriptorToName();

    private static native void unlockAllMemory();

    public synchronized void configure(MultiDexClassLoader.Configuration configuration) {
        int i;
        ArrayList arrayList = configuration.mDexFiles;
        DexFile[] dexFileArr = (DexFile[]) arrayList.toArray(new DexFile[arrayList.size()]);
        AnonymousClass00K r1 = (AnonymousClass00K) configuration.mFbColdStartExperiment;
        if (r1 == null || !r1.A14) {
            i = -1;
        } else {
            i = r1.A0P;
        }
        nativeConfigure(this.mPrimaryDexes, dexFileArr, configuration.configFlags, AnonymousClass1Y3.A87, AnonymousClass1Y3.A87, i, 1);
        this.mSecondaryDexes = dexFileArr;
        if (!this.mHacksAttempted) {
            this.mHacksAttempted = true;
            try {
                enableDirectLookupHooks();
                this.mDirectLookupsEnabled = true;
            } catch (Exception e) {
                Log.w(MultiDexClassLoader.TAG, "unable to install direct Dalvik class-lookup hooks; continuing with classloader API", e);
            }
            try {
                enableO1Hack();
                this.mO1HackEnabled = true;
            } catch (Exception e2) {
                Log.w(MultiDexClassLoader.TAG, "unable to enable O1 Dalvik hack", e2);
            }
        }
        return;
    }

    public native Class findClass(String str);

    public native Class loadClass(String str, boolean z);

    static {
        AnonymousClass01q.A08("dextricks");
    }

    public void enableDirectLookupHooks() {
        DexFile[] dexFileArr = this.mPrimaryDexes;
        if (dexFileArr == null || this.mAuxDexes == null) {
            throw new UnsupportedOperationException("cannot enable direct hooks: we could not locate primary and aux dexes");
        } else if (dexFileArr.length == 1) {
            nativeEnableDirectLookupHooks();
        } else {
            throw new UnsupportedOperationException("cannot enable direct hooks: must have found exactly one primary dex");
        }
    }

    public void enableO1Hack() {
        int length;
        DexFile[] dexFileArr = this.mPrimaryDexes;
        if (dexFileArr == null) {
            length = 0;
        } else {
            length = dexFileArr.length;
        }
        if (length == 1) {
            nativeEnableO1Hack();
            return;
        }
        throw new UnsupportedOperationException(AnonymousClass08S.A09("To use the O(1) class lookup hack, must have exactly one primary dex: have ", length));
    }

    public String toString() {
        return String.format("MultiDexClassLoaderDalvikNative(lookups=%s,o1=%s)", Boolean.valueOf(this.mDirectLookupsEnabled), Boolean.valueOf(this.mO1HackEnabled));
    }

    public MultiDexClassLoaderDalvikNative(Context context, ArrayList arrayList, ArrayList arrayList2) {
        boolean z = false;
        if ((!arrayList.isEmpty() || !arrayList2.isEmpty()) ? true : z) {
            this.mPrimaryDexes = (DexFile[]) arrayList.toArray(new DexFile[arrayList.size()]);
            this.mAuxDexes = (DexFile[]) arrayList2.toArray(new DexFile[arrayList2.size()]);
        } else {
            Log.w(MultiDexClassLoader.TAG, "cannot enable dex hooks: failure to locate primary/aux dexes");
            this.mPrimaryDexes = null;
            this.mAuxDexes = null;
        }
        ClassLoadingStatsNative classLoadingStatsNative = new ClassLoadingStatsNative();
        ClassLoadingStats.A00.getAndSet(classLoadingStatsNative);
        nativeInitialize(this.mPutativeLoader, classLoadingStatsNative, this.mAuxDexes);
        setReplacementDvmDescriptorToName();
        sIsIntialized = true;
    }

    public DexFile[] doGetConfiguredDexFiles() {
        return this.mSecondaryDexes;
    }

    public void onColdstartDone() {
        unlockAllMemory();
    }
}
