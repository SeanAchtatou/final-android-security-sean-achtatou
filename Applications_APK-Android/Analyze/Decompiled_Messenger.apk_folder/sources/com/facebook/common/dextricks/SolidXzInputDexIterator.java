package com.facebook.common.dextricks;

import X.AnonymousClass02K;
import X.AnonymousClass08S;
import X.C000000c;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.superpack.ditto.DittoPatch;
import com.facebook.xzdecoder.XzInputStream;
import java.io.InputStream;

public final class SolidXzInputDexIterator extends InputDexIterator {
    public boolean mConsumingStream;
    private final AnonymousClass02K mEvent;
    private SliceInputStream mLastPartIs;
    private final ResProvider mResProvider;
    private final C000000c mTracer;
    public final XzInputStream mXzs;

    public final class SliceInputStream extends InputStream {
        private int mBytesRead = 0;
        private int mBytesToRead;

        public SliceInputStream(int i) {
            this.mBytesToRead = i;
            SolidXzInputDexIterator.this.mConsumingStream = true;
        }

        public int available() {
            return this.mBytesToRead - this.mBytesRead;
        }

        public void close() {
            SolidXzInputDexIterator.this.mConsumingStream = false;
        }

        public int read() {
            if (this.mBytesRead == this.mBytesToRead) {
                return -1;
            }
            int read = SolidXzInputDexIterator.this.mXzs.read();
            if (read != -1) {
                this.mBytesRead++;
                return read;
            }
            throw new RuntimeException("truncated xzs stream");
        }

        public int read(byte[] bArr, int i, int i2) {
            if (i2 > 0 && this.mBytesRead == this.mBytesToRead) {
                return -1;
            }
            int read = SolidXzInputDexIterator.this.mXzs.read(bArr, i, Math.min(i2, this.mBytesToRead - this.mBytesRead));
            if (read > 0) {
                this.mBytesRead += read;
            }
            return read;
        }
    }

    public void close() {
        try {
            Fs.safeClose(this.mXzs);
        } finally {
            this.mEvent.close();
        }
    }

    public InputDex nextImpl(DexManifest.Dex dex) {
        if (!this.mConsumingStream) {
            SliceInputStream sliceInputStream = this.mLastPartIs;
            if (sliceInputStream != null) {
                int available = sliceInputStream.available();
                if (available > 0) {
                    Fs.discardFromInputStream(sliceInputStream, (long) available);
                }
                this.mLastPartIs = null;
            }
            SliceInputStream sliceInputStream2 = new SliceInputStream(getJarFileSizeFromMeta(this.mResProvider, AnonymousClass08S.A0J(dex.assetName, ".meta")));
            this.mLastPartIs = sliceInputStream2;
            return new InputDex(dex, sliceInputStream2);
        }
        throw new RuntimeException("previous InputDex not closed");
    }

    public SolidXzInputDexIterator(DexManifest dexManifest, DittoPatch dittoPatch, ResProvider resProvider, InputStream inputStream, C000000c r6) {
        super(dexManifest, dittoPatch);
        this.mResProvider = resProvider;
        this.mXzs = new XzInputStream(inputStream);
        this.mTracer = r6;
        this.mEvent = r6.AOr(34603012);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:17|18|19|20|21) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:25|26|27|28|29) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x003f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x0046 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int getJarFileSizeFromMeta(com.facebook.common.dextricks.ResProvider r7, java.lang.String r8) {
        /*
            r6 = this;
            java.io.InputStream r2 = r7.open(r8)
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x0047 }
            r4.<init>(r2)     // Catch:{ all -> 0x0047 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ all -> 0x0040 }
            r5.<init>(r4)     // Catch:{ all -> 0x0040 }
            java.lang.String r3 = r5.readLine()     // Catch:{ all -> 0x0039 }
            r1 = 0
            r0 = 32
            int r0 = r3.indexOf(r0)     // Catch:{ all -> 0x0039 }
            java.lang.String r1 = r3.substring(r1, r0)     // Catch:{ all -> 0x0039 }
            r0 = 58
            int r0 = r1.indexOf(r0)     // Catch:{ all -> 0x0039 }
            int r0 = r0 + 1
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x0039 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x0039 }
            r5.close()     // Catch:{ all -> 0x0040 }
            r4.close()     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0038
            r2.close()
        L_0x0038:
            return r0
        L_0x0039:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003b }
        L_0x003b:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x003f }
        L_0x003f:
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x0042:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x0046 }
        L_0x0046:
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x0047:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ all -> 0x004f }
        L_0x004f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.SolidXzInputDexIterator.getJarFileSizeFromMeta(com.facebook.common.dextricks.ResProvider, java.lang.String):int");
    }
}
