package com.facebook.common.dextricks;

import java.io.PrintWriter;

public final class DexErrorRecoveryInfo {
    public boolean dexoptDuringColdStart;
    public Throwable fallbackCause;
    public int loadResult;
    public String odexSchemeName;
    public long odexSize;
    public Throwable regenRetryCause;
    public Throwable xdexFailureCause;

    /* JADX WARNING: Can't wrap try/catch for region: R(5:15|16|17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0071, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0075 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x007c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r5 = this;
            java.io.StringWriter r2 = new java.io.StringWriter     // Catch:{ IOException -> 0x007d }
            r2.<init>()     // Catch:{ IOException -> 0x007d }
            java.io.PrintWriter r3 = new java.io.PrintWriter     // Catch:{ all -> 0x0076 }
            r3.<init>(r2)     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = "<DexErrorRecoveryInfo"
            r3.append(r0)     // Catch:{ all -> 0x006f }
            java.lang.String r1 = " loadResult=%x"
            int r0 = r5.loadResult     // Catch:{ all -> 0x006f }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x006f }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x006f }
            r3.format(r1, r0)     // Catch:{ all -> 0x006f }
            java.lang.String r1 = " odexSchemeName=%s"
            java.lang.String r0 = r5.odexSchemeName     // Catch:{ all -> 0x006f }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x006f }
            r3.format(r1, r0)     // Catch:{ all -> 0x006f }
            java.lang.String r1 = " dexoptDuringColdStart=%b"
            boolean r0 = r5.dexoptDuringColdStart     // Catch:{ all -> 0x006f }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x006f }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x006f }
            r3.format(r1, r0)     // Catch:{ all -> 0x006f }
            java.lang.String r4 = " odexSize=%d"
            long r0 = r5.odexSize     // Catch:{ all -> 0x006f }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x006f }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x006f }
            r3.format(r4, r0)     // Catch:{ all -> 0x006f }
            java.lang.String r1 = "regenRetryCause"
            java.lang.Throwable r0 = r5.regenRetryCause     // Catch:{ all -> 0x006f }
            printExOrNull(r3, r1, r0)     // Catch:{ all -> 0x006f }
            java.lang.String r1 = "fallbackCause"
            java.lang.Throwable r0 = r5.fallbackCause     // Catch:{ all -> 0x006f }
            printExOrNull(r3, r1, r0)     // Catch:{ all -> 0x006f }
            java.lang.String r1 = "xdexFailureCause"
            java.lang.Throwable r0 = r5.xdexFailureCause     // Catch:{ all -> 0x006f }
            printExOrNull(r3, r1, r0)     // Catch:{ all -> 0x006f }
            java.lang.String r0 = ">"
            r3.append(r0)     // Catch:{ all -> 0x006f }
            r3.flush()     // Catch:{ all -> 0x006f }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x006f }
            r3.close()     // Catch:{ all -> 0x0076 }
            r2.close()     // Catch:{ IOException -> 0x007d }
            return r0
        L_0x006f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0071 }
        L_0x0071:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0075 }
        L_0x0075:
            throw r0     // Catch:{ all -> 0x0076 }
        L_0x0076:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0078 }
        L_0x0078:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x007c }
        L_0x007c:
            throw r0     // Catch:{ IOException -> 0x007d }
        L_0x007d:
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexErrorRecoveryInfo.toString():java.lang.String");
    }

    private static void printExOrNull(PrintWriter printWriter, String str, Throwable th) {
        printWriter.format(" %s=", str);
        if (th == null) {
            printWriter.append((CharSequence) "null");
            return;
        }
        printWriter.append((CharSequence) "[");
        th.printStackTrace(printWriter);
        printWriter.append((CharSequence) "]");
    }
}
