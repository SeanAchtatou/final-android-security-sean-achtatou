package com.facebook.common.dextricks;

import X.AnonymousClass08S;
import android.os.Build;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.common.dextricks.DexStore;
import com.facebook.common.dextricks.MultiDexClassLoader;
import com.facebook.common.dextricks.OdexScheme;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OdexSchemeArtTurbo extends OdexScheme {
    public static final String OREO_ODEX_DIR = "oat";
    public static boolean sAttemptedArtHackInstallation;

    public final class TurboArtCompiler extends OdexScheme.Compiler {
        private final DexStore mDexStore;
        private final int mFlags;
        private final DexStore.TmpDir mTmpDir;

        public void close() {
            this.mTmpDir.close();
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(5:23|24|25|26|27) */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
            r9.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            throw r0;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0076 */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:26:0x0076=Splitter:B:26:0x0076, B:14:0x005b=Splitter:B:14:0x005b} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void compile(com.facebook.common.dextricks.InputDex r11) {
            /*
                r10 = this;
                com.facebook.common.dextricks.DexManifest$Dex r0 = r11.dex
                java.lang.String r8 = r0.makeDexName()
                java.io.File r2 = new java.io.File
                com.facebook.common.dextricks.DexStore r0 = r10.mDexStore
                java.io.File r0 = r0.root
                r2.<init>(r0, r8)
                int r1 = r10.mFlags
                r0 = 1
                r1 = r1 & r0
                if (r1 == 0) goto L_0x001c
                boolean r0 = r2.exists()
                if (r0 == 0) goto L_0x001c
                return
            L_0x001c:
                java.io.File r7 = new java.io.File
                com.facebook.common.dextricks.DexStore$TmpDir r0 = r10.mTmpDir
                java.io.File r0 = r0.directory
                r7.<init>(r0, r8)
                java.io.InputStream r6 = r11.getDexContents()
                int r0 = r11.getSizeHint(r6)     // Catch:{ all -> 0x0077 }
                java.lang.String r1 = "size hint for %s: %s"
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0077 }
                java.lang.Object[] r0 = new java.lang.Object[]{r11, r0}     // Catch:{ all -> 0x0077 }
                com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x0077 }
                java.io.RandomAccessFile r9 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0077 }
                java.lang.String r0 = "rw"
                r9.<init>(r7, r0)     // Catch:{ all -> 0x0077 }
                r0 = 32768(0x8000, float:4.5918E-41)
                byte[] r5 = new byte[r0]     // Catch:{ all -> 0x0070 }
                r4 = 2147483647(0x7fffffff, float:NaN)
                r3 = 0
                r2 = 0
            L_0x004b:
                if (r2 >= r4) goto L_0x005b
                int r0 = r4 - r2
                int r1 = X.AnonymousClass0Me.A0A(r6, r5, r0)     // Catch:{ all -> 0x0070 }
                r0 = -1
                if (r1 == r0) goto L_0x005b
                r9.write(r5, r3, r1)     // Catch:{ all -> 0x0070 }
                int r2 = r2 + r1
                goto L_0x004b
            L_0x005b:
                r9.close()     // Catch:{ all -> 0x0077 }
                if (r6 == 0) goto L_0x0063
                r6.close()
            L_0x0063:
                java.io.File r1 = new java.io.File
                com.facebook.common.dextricks.DexStore r0 = r10.mDexStore
                java.io.File r0 = r0.root
                r1.<init>(r0, r8)
                com.facebook.common.dextricks.Fs.renameOrThrow(r7, r1)
                return
            L_0x0070:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0072 }
            L_0x0072:
                r0 = move-exception
                r9.close()     // Catch:{ all -> 0x0076 }
            L_0x0076:
                throw r0     // Catch:{ all -> 0x0077 }
            L_0x0077:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0079 }
            L_0x0079:
                r0 = move-exception
                if (r6 == 0) goto L_0x007f
                r6.close()     // Catch:{ all -> 0x007f }
            L_0x007f:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtTurbo.TurboArtCompiler.compile(com.facebook.common.dextricks.InputDex):void");
        }

        public TurboArtCompiler(DexStore dexStore, int i) {
            this.mDexStore = dexStore;
            this.mFlags = i;
            this.mTmpDir = dexStore.makeTemporaryDirectory("turbo-art-compiler");
        }
    }

    public static List makeExpectedFileInfoList(DexManifest.Dex[] dexArr, String str) {
        int length = dexArr.length;
        int i = length;
        if (str != null) {
            length++;
        }
        ArrayList arrayList = new ArrayList(length);
        for (int i2 = 0; i2 < i; i2++) {
            arrayList.add(new ExpectedFileInfo(dexArr[i2]));
        }
        if (str != null) {
            arrayList.add(new ExpectedFileInfo(str));
        }
        return arrayList;
    }

    public String getSchemeName() {
        return "OdexSchemeArtTurbo";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        if (r4.equals("x86") == false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r4.equals("armeabi-v7a") != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getArch() {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r5 = 0
            r0 = 21
            if (r1 < r0) goto L_0x0032
            java.lang.String[] r0 = android.os.Build.SUPPORTED_32_BIT_ABIS
            r4 = r0[r5]
        L_0x000b:
            int r3 = r4.hashCode()
            r0 = 117110(0x1c976, float:1.64106E-40)
            java.lang.String r2 = "x86"
            r1 = 1
            if (r3 == r0) goto L_0x0029
            r0 = 145444210(0x8ab4d72, float:1.0309895E-33)
            if (r3 != r0) goto L_0x0030
            java.lang.String r0 = "armeabi-v7a"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0030
        L_0x0024:
            if (r5 == 0) goto L_0x0041
            if (r5 != r1) goto L_0x0035
            return r2
        L_0x0029:
            boolean r0 = r4.equals(r2)
            r5 = 1
            if (r0 != 0) goto L_0x0024
        L_0x0030:
            r5 = -1
            goto L_0x0024
        L_0x0032:
            java.lang.String r4 = android.os.Build.CPU_ABI
            goto L_0x000b
        L_0x0035:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Unknown ABI "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r4)
            r1.<init>(r0)
            throw r1
        L_0x0041:
            java.lang.String r0 = "arm"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtTurbo.getArch():java.lang.String");
    }

    public static String getOreoOdexOutputDirectory(File file, boolean z) {
        File file2 = new File(file, AnonymousClass08S.A0J("oat/", getArch()));
        if (z) {
            Fs.mkdirOrThrow(file2);
        }
        return AnonymousClass08S.A0J(file2.getPath(), "/");
    }

    public static String[] makeExpectedFileListFrom(List list) {
        if (list == null) {
            return null;
        }
        int size = list.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            strArr[i] = ((ExpectedFileInfo) list.get(i)).toExpectedFileString();
        }
        return strArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, boolean):void
     arg types: [java.io.File, int]
     candidates:
      com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, java.io.File):void
      com.facebook.common.dextricks.MultiDexClassLoader.Configuration.addDex(java.io.File, boolean):void */
    public void configureClassLoader(File file, MultiDexClassLoader.Configuration configuration) {
        if (!sAttemptedArtHackInstallation) {
            sAttemptedArtHackInstallation = true;
            try {
                DalvikInternals.installArtHacks(6, Build.VERSION.SDK_INT);
            } catch (Exception e) {
                Mlog.w(e, "failed to install verifier-disabling ART hacks; continuing slowly", new Object[0]);
            }
        }
        int enabledThreadArtHacks = DalvikInternals.getEnabledThreadArtHacks();
        DalvikInternals.setEnabledThreadArtHacks(enabledThreadArtHacks | 6);
        try {
            Mlog.safeFmt("enabled ART verifier hack (warning-level logs following are expected)", new Object[0]);
            int i = 0;
            while (true) {
                String[] strArr = this.expectedFiles;
                if (i < strArr.length) {
                    String str = strArr[i];
                    if (!str.equals(OREO_ODEX_DIR)) {
                        configuration.addDex(new File(file, str), true);
                    }
                    i++;
                } else {
                    return;
                }
            }
        } finally {
            DalvikInternals.setEnabledThreadArtHacks(enabledThreadArtHacks);
            Mlog.safeFmt("restored old ART hack mask", new Object[0]);
        }
    }

    public final OdexScheme.Compiler makeCompiler(DexStore dexStore, int i) {
        return new TurboArtCompiler(dexStore, i);
    }

    public static String[] makeExpectedFileList(DexManifest.Dex[] dexArr, String str) {
        return makeExpectedFileListFrom(makeExpectedFileInfoList(dexArr, str));
    }

    public OdexSchemeArtTurbo(int i, String[] strArr) {
        super(i, strArr);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OdexSchemeArtTurbo(com.facebook.common.dextricks.DexManifest.Dex[] r3) {
        /*
            r2 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 < r0) goto L_0x0016
            r0 = 28
            if (r1 >= r0) goto L_0x0016
            java.lang.String r0 = "oat"
        L_0x000c:
            java.lang.String[] r1 = makeExpectedFileList(r3, r0)
            r0 = 8
            r2.<init>(r0, r1)
            return
        L_0x0016:
            r0 = 0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.OdexSchemeArtTurbo.<init>(com.facebook.common.dextricks.DexManifest$Dex[]):void");
    }
}
