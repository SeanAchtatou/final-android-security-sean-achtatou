package com.facebook.common.dextricks;

import X.AnonymousClass06I;
import X.AnonymousClass08S;
import android.os.Process;
import com.facebook.acra.CrashReportData;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.common.build.BuildConstants;
import java.util.Random;

public class DexTricksErrorReporter {
    private static final int DEFAULT_SOFT_ERROR_REPORTING_FREQUENCY = 1000;
    private static final Random RANDOM = new Random();

    public static String formatCategorySampling(String str) {
        return AnonymousClass08S.A0M(str, " [freq=", 1000, "]");
    }

    public static boolean isInternalBuild() {
        if (BuildConstants.A00() > 1) {
            return false;
        }
        return true;
    }

    private static boolean randomSamplingCoinflip() {
        if (isInternalBuild() || RANDOM.nextInt(1000) == 0) {
            return true;
        }
        return false;
    }

    public static void reportSampledSoftError(final String str, final String str2, Throwable th) {
        Mlog.e(th, "SOFT ERROR %s: %s", str, str2);
        if (randomSamplingCoinflip()) {
            final AnonymousClass06I r3 = new AnonymousClass06I(AnonymousClass08S.A0P(str, " | ", str2), th);
            new Thread(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.facebook.acra.ErrorReporter.handleException(java.lang.Throwable, com.facebook.acra.CrashReportData):com.facebook.acra.ErrorReporter$ReportsSenderWorker
                 arg types: [X.06I, com.facebook.acra.CrashReportData]
                 candidates:
                  com.facebook.acra.ErrorReporter.handleException(java.lang.Throwable, java.util.Map):com.facebook.acra.ErrorReporter$ReportsSenderWorker
                  com.facebook.acra.ErrorReporter.handleException(java.lang.Throwable, com.facebook.acra.CrashReportData):com.facebook.acra.ErrorReporter$ReportsSenderWorker */
                public void run() {
                    String formatCategorySampling;
                    try {
                        Process.setThreadPriority(10);
                        if (DexTricksErrorReporter.isInternalBuild()) {
                            formatCategorySampling = str;
                        } else {
                            formatCategorySampling = DexTricksErrorReporter.formatCategorySampling(str);
                        }
                        int i = 1000;
                        if (DexTricksErrorReporter.isInternalBuild()) {
                            i = 1;
                        }
                        CrashReportData crashReportData = new CrashReportData();
                        crashReportData.put(ErrorReportingConstants.SOFT_ERROR_CATEGORY, formatCategorySampling);
                        crashReportData.put(ErrorReportingConstants.SOFT_ERROR_MESSAGE, str2);
                        crashReportData.put(ErrorReportingConstants.SOFT_ERROR_OCCURRENCE_COUNT, String.valueOf(i));
                        ErrorReporter.getInstance().handleException((Throwable) r3, crashReportData);
                    } catch (Throwable th) {
                        Mlog.w(th, "Unable to report soft error", new Object[0]);
                    }
                }
            }, "dexTrickError").start();
        }
    }
}
