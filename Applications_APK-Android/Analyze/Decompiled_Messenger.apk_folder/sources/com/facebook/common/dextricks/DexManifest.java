package com.facebook.common.dextricks;

import X.AnonymousClass08S;
import X.AnonymousClass094;

public final class DexManifest {
    public static final String DEX_EXT = ".dex";
    public static final String ODEX_EXT = ".odex";
    public final Dex[] dexes;
    public final String id;
    public final boolean locators;
    public final String[] requires;
    public final boolean rootRelative;
    public final AnonymousClass094 superpackExtension;
    public final int superpackFiles;

    public final class Dex {
        public final String assetName;
        public final String canaryClass;
        public final String hash;

        public String makeDexName() {
            return DexManifest.makeCompileUnitNameFromHashAndExtension(this.hash, DexManifest.DEX_EXT);
        }

        public String makeOdexName() {
            return DexManifest.makeCompileUnitNameFromHashAndExtension(this.hash, DexManifest.ODEX_EXT);
        }

        public String toString() {
            return String.format("<Dex assetName:[%s]>", this.assetName);
        }

        public Dex(String str, String str2, String str3) {
            this.assetName = str;
            this.hash = str2;
            this.canaryClass = str3;
        }
    }

    public static String makeCompileUnitNameFromHashAndExtension(String str, String str2) {
        if (!str2.startsWith(".")) {
            str2 = AnonymousClass08S.A0J(".", str2);
        }
        return AnonymousClass08S.A0P("prog-", str, str2);
    }

    public static String makeDexNameFromHash(String str) {
        return makeCompileUnitNameFromHashAndExtension(str, DEX_EXT);
    }

    public static String makeOdexNameFromHash(String str) {
        return makeCompileUnitNameFromHashAndExtension(str, ODEX_EXT);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0111, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0115, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DexManifest(java.io.InputStream r16) {
        /*
            r15 = this;
            r15.<init>()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            X.094 r12 = X.AnonymousClass094.NONE
            java.lang.String r11 = "dex"
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.io.BufferedReader r9 = new java.io.BufferedReader
            java.io.InputStreamReader r1 = new java.io.InputStreamReader
            java.lang.String r0 = "UTF-8"
            r2 = r16
            r1.<init>(r2, r0)
            r9.<init>(r1)
            r14 = 0
            r13 = 1
            r8 = 0
            r6 = 0
            r5 = 0
        L_0x0024:
            java.lang.String r2 = r9.readLine()     // Catch:{ all -> 0x010f }
            if (r2 == 0) goto L_0x00e5
            int r0 = r2.length()     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x0024
            java.lang.String r1 = "Secondary program dex metadata: [%s]"
            java.lang.Object[] r0 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x010f }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x010f }
            java.lang.String r0 = ".root_relative"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x0043
            r8 = 1
            goto L_0x0024
        L_0x0043:
            java.lang.String r0 = ".locators"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x004d
            r6 = 1
            goto L_0x0024
        L_0x004d:
            java.lang.String r0 = ".superpack_files"
            boolean r0 = r2.startsWith(r0)     // Catch:{ all -> 0x010f }
            java.lang.String r1 = " "
            if (r0 == 0) goto L_0x0062
            java.lang.String[] r0 = r2.split(r1)     // Catch:{ all -> 0x010f }
            r0 = r0[r13]     // Catch:{ all -> 0x010f }
            int r5 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x0062:
            java.lang.String r0 = ".superpack_extension"
            boolean r0 = r2.startsWith(r0)     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x009a
            java.lang.String[] r0 = r2.split(r1)     // Catch:{ all -> 0x010f }
            r1 = r0[r13]     // Catch:{ all -> 0x010f }
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x0079
            X.094 r12 = X.AnonymousClass094.NONE     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x0079:
            java.lang.String r0 = "xz"
            boolean r0 = r1.equalsIgnoreCase(r0)     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x0084
            X.094 r12 = X.AnonymousClass094.XZ     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x0084:
            java.lang.String r0 = "zst"
            boolean r0 = r1.equalsIgnoreCase(r0)     // Catch:{ all -> 0x010f }
            if (r0 != 0) goto L_0x0097
            java.lang.String r0 = "zstd"
            boolean r0 = r1.equalsIgnoreCase(r0)     // Catch:{ all -> 0x010f }
            if (r0 != 0) goto L_0x0097
            X.094 r12 = X.AnonymousClass094.NONE     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x0097:
            X.094 r12 = X.AnonymousClass094.ZST     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x009a:
            java.lang.String r0 = ".id"
            boolean r0 = r2.startsWith(r0)     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x00aa
            java.lang.String[] r0 = r2.split(r1)     // Catch:{ all -> 0x010f }
            r11 = r0[r13]     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x00aa:
            java.lang.String r0 = ".requires"
            boolean r0 = r2.startsWith(r0)     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x00bd
            java.lang.String[] r0 = r2.split(r1)     // Catch:{ all -> 0x010f }
            r0 = r0[r13]     // Catch:{ all -> 0x010f }
            r10.add(r0)     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x00bd:
            java.lang.String r0 = "."
            boolean r0 = r2.startsWith(r0)     // Catch:{ all -> 0x010f }
            if (r0 == 0) goto L_0x00d0
            java.lang.String r1 = "ignoring dex metadata pragma [%s]"
            java.lang.Object[] r0 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x010f }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x00d0:
            java.lang.String[] r4 = r2.split(r1)     // Catch:{ all -> 0x010f }
            com.facebook.common.dextricks.DexManifest$Dex r3 = new com.facebook.common.dextricks.DexManifest$Dex     // Catch:{ all -> 0x010f }
            r2 = r4[r14]     // Catch:{ all -> 0x010f }
            r1 = r4[r13]     // Catch:{ all -> 0x010f }
            r0 = 2
            r0 = r4[r0]     // Catch:{ all -> 0x010f }
            r3.<init>(r2, r1, r0)     // Catch:{ all -> 0x010f }
            r7.add(r3)     // Catch:{ all -> 0x010f }
            goto L_0x0024
        L_0x00e5:
            r9.close()
            r15.rootRelative = r8
            r15.locators = r6
            r15.superpackFiles = r5
            r15.superpackExtension = r12
            r15.id = r11
            int r0 = r10.size()
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.Object[] r0 = r10.toArray(r0)
            java.lang.String[] r0 = (java.lang.String[]) r0
            r15.requires = r0
            int r0 = r7.size()
            com.facebook.common.dextricks.DexManifest$Dex[] r0 = new com.facebook.common.dextricks.DexManifest.Dex[r0]
            java.lang.Object[] r0 = r7.toArray(r0)
            com.facebook.common.dextricks.DexManifest$Dex[] r0 = (com.facebook.common.dextricks.DexManifest.Dex[]) r0
            r15.dexes = r0
            return
        L_0x010f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0111 }
        L_0x0111:
            r0 = move-exception
            r9.close()     // Catch:{ all -> 0x0115 }
        L_0x0115:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexManifest.<init>(java.io.InputStream):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001b, code lost:
        if (r2 != null) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0020, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.common.dextricks.DexManifest loadManifestFrom(com.facebook.common.dextricks.ResProvider r3, java.lang.String r4, boolean r5) {
        /*
            java.io.InputStream r2 = r3.open(r4)
            com.facebook.common.dextricks.DexManifest r1 = new com.facebook.common.dextricks.DexManifest     // Catch:{ all -> 0x0018 }
            r1.<init>(r2)     // Catch:{ all -> 0x0018 }
            if (r2 == 0) goto L_0x000e
            r2.close()
        L_0x000e:
            if (r5 == 0) goto L_0x0017
            boolean r0 = r1.rootRelative
            if (r0 == 0) goto L_0x0017
            r3.markRootRelative()
        L_0x0017:
            return r1
        L_0x0018:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001a }
        L_0x001a:
            r0 = move-exception
            if (r2 == 0) goto L_0x0020
            r2.close()     // Catch:{ all -> 0x0020 }
        L_0x0020:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexManifest.loadManifestFrom(com.facebook.common.dextricks.ResProvider, java.lang.String, boolean):com.facebook.common.dextricks.DexManifest");
    }
}
