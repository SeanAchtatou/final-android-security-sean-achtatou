package com.facebook.common.dextricks;

import com.facebook.common.dextricks.DexManifest;
import com.facebook.common.dextricks.DexUnpacker;
import java.io.File;

public class ExpectedFileInfo {
    public final boolean coldstart;
    public final DexManifest.Dex dex;
    public final File dexFile;
    public final String dexName;
    public final boolean extended;
    public boolean mIsOptional;
    public final int ordinal;
    public final boolean primary;
    public final String rawFile;
    public final boolean scroll;

    public static ExpectedFileInfo makeOdexFromName(String str) {
        return new ExpectedFileInfo(str);
    }

    public boolean hasDex() {
        if (this.dex != null) {
            return true;
        }
        return false;
    }

    public boolean hasManifestData() {
        if (this.ordinal != -1) {
            return true;
        }
        return false;
    }

    public boolean isNonRootDex() {
        if (this.dex == null || this.dexFile == null) {
            return false;
        }
        return true;
    }

    public boolean isRawFile() {
        if (this.rawFile != null) {
            return true;
        }
        return false;
    }

    public boolean isRootDex() {
        if (this.dex == null || this.dexName == null) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[Expected File Info: ");
        if (isRawFile()) {
            sb.append("Raw File: ");
            sb.append(this.rawFile);
        } else if (isRootDex()) {
            sb.append("Dex Name: ");
            sb.append(this.dexName);
        } else if (isNonRootDex()) {
            sb.append("Dex File: ");
            sb.append(this.dexFile.getPath());
        } else {
            sb.append("ERROR!");
        }
        sb.append(" ]");
        return sb.toString();
    }

    public static ExpectedFileInfo makeOdexFrom(DexManifest.Dex dex2) {
        return new ExpectedFileInfo(dex2.makeOdexName());
    }

    public DexManifest.Dex getDex() {
        return this.dex;
    }

    public File getFileFromAnotherRoot(File file) {
        if (isRawFile() || isRootDex()) {
            return new File(file, toExpectedFileString());
        }
        if (isNonRootDex()) {
            return new File(file, this.dexFile.getName());
        }
        throw new RuntimeException("Cannot generate file");
    }

    public boolean isOptional() {
        return this.mIsOptional;
    }

    public String toExpectedFileString() {
        if (isRawFile()) {
            return this.rawFile;
        }
        if (isRootDex()) {
            return this.dexName;
        }
        throw new IllegalStateException("Cannot generated an expected string");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ExpectedFileInfo(DexManifest.Dex dex2) {
        this(dex2, dex2 != null ? dex2.makeDexName() : null);
    }

    public ExpectedFileInfo(DexManifest.Dex dex2, String str) {
        this(dex2, str, null, null);
    }

    private ExpectedFileInfo(DexManifest.Dex dex2, String str, File file, String str2) {
        this(dex2, str, file, str2, -1, false, false, false, false, false);
    }

    private ExpectedFileInfo(DexManifest.Dex dex2, String str, File file, String str2, int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.dex = dex2;
        this.dexName = str;
        this.dexFile = file;
        this.rawFile = str2;
        this.ordinal = i;
        this.primary = z;
        this.coldstart = z2;
        this.extended = z3;
        this.scroll = z4;
        this.mIsOptional = z5;
    }

    public ExpectedFileInfo(DexUnpacker.CopiedDexInfo copiedDexInfo) {
        this(copiedDexInfo.dex, null, copiedDexInfo.dexFile, null);
    }

    public ExpectedFileInfo(String str) {
        this(null, null, null, str);
    }

    public static ExpectedFileInfo[] convertTo(String[] strArr) {
        if (strArr == null) {
            return null;
        }
        int length = strArr.length;
        ExpectedFileInfo[] expectedFileInfoArr = new ExpectedFileInfo[length];
        for (int i = 0; i < length; i++) {
            String str = strArr[i];
            expectedFileInfoArr[i] = str != null ? new ExpectedFileInfo(str) : null;
        }
        return expectedFileInfoArr;
    }

    public static String[] convertTo(ExpectedFileInfo[] expectedFileInfoArr) {
        if (expectedFileInfoArr == null) {
            return null;
        }
        int length = expectedFileInfoArr.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            ExpectedFileInfo expectedFileInfo = expectedFileInfoArr[i];
            strArr[i] = expectedFileInfo != null ? expectedFileInfo.toExpectedFileString() : null;
        }
        return strArr;
    }

    public File getFile(DexStore dexStore) {
        return getFile(dexStore.root);
    }

    public File getFile(File file) {
        if (isRawFile() || isRootDex()) {
            return new File(file, toExpectedFileString());
        }
        if (isNonRootDex()) {
            return this.dexFile;
        }
        throw new RuntimeException("Cannot generate file");
    }

    public ExpectedFileInfo setIsOptional() {
        this.mIsOptional = true;
        return this;
    }

    public ExpectedFileInfo setIsOptional(boolean z) {
        this.mIsOptional = z;
        return this;
    }
}
