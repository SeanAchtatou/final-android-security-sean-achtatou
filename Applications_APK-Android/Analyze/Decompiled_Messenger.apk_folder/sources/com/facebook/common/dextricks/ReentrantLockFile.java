package com.facebook.common.dextricks;

import java.io.Closeable;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public final class ReentrantLockFile implements Closeable {
    public static final int ACQUIRE_SHARED = 1;
    private static final boolean LOCK_DEBUG = false;
    private static final ReentrantLockFile sListHead = new ReentrantLockFile();
    public final File lockFileName;
    private FileChannel mChannel;
    private int mLockFlags;
    private final Lock mLockHandle;
    private boolean mLockInProgress;
    public Thread mLockOwner;
    private int mLockShareCount;
    private ReentrantLockFile mNext;
    private ReentrantLockFile mPrev;
    private int mReferenceCount;
    private FileLock mTheLock;

    public final class Lock implements Closeable {
        public Lock() {
        }

        public void close() {
            ReentrantLockFile.this.release();
        }

        public ReentrantLockFile getReentrantLockFile() {
            return ReentrantLockFile.this;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x005f, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0067, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.facebook.common.dextricks.ReentrantLockFile.Lock tryAcquire(int r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            java.nio.channels.FileChannel r5 = r11.mChannel     // Catch:{ all -> 0x0070 }
            if (r5 == 0) goto L_0x0068
            r0 = r12 & 1
            r1 = 0
            r4 = 1
            r10 = 0
            if (r0 == 0) goto L_0x000d
            r10 = 1
        L_0x000d:
            boolean r0 = r11.mLockInProgress     // Catch:{ all -> 0x0070 }
            r3 = 0
            if (r0 != 0) goto L_0x0066
            int r2 = r11.mLockShareCount     // Catch:{ all -> 0x0070 }
            if (r2 <= 0) goto L_0x0030
            int r0 = r11.mLockFlags     // Catch:{ all -> 0x0070 }
            r0 = r0 & r4
            if (r0 == 0) goto L_0x001c
            r1 = 1
        L_0x001c:
            if (r10 == 0) goto L_0x0020
            if (r1 != 0) goto L_0x002a
        L_0x0020:
            if (r1 != 0) goto L_0x0066
            java.lang.Thread r1 = r11.mLockOwner     // Catch:{ all -> 0x0070 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0070 }
            if (r1 != r0) goto L_0x0066
        L_0x002a:
            int r2 = r2 + r4
            r11.mLockShareCount = r2     // Catch:{ all -> 0x0070 }
            com.facebook.common.dextricks.ReentrantLockFile$Lock r0 = r11.mLockHandle     // Catch:{ all -> 0x0070 }
            goto L_0x005e
        L_0x0030:
            r6 = 0
            r8 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            java.nio.channels.FileLock r0 = r5.tryLock(r6, r8, r10)     // Catch:{ IOException -> 0x003c }
            goto L_0x0054
        L_0x003c:
            r2 = move-exception
            java.lang.String r1 = r2.getMessage()     // Catch:{ all -> 0x0070 }
            if (r1 == 0) goto L_0x0060
            java.lang.String r0 = ": EAGAIN ("
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x0070 }
            if (r0 != 0) goto L_0x0053
            java.lang.String r0 = ": errno 11 ("
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x0070 }
            if (r0 == 0) goto L_0x0060
        L_0x0053:
            r0 = r3
        L_0x0054:
            if (r0 == 0) goto L_0x0066
            r11.addrefLocked()     // Catch:{ all -> 0x0070 }
            r11.claimLock(r12, r0)     // Catch:{ all -> 0x0070 }
            com.facebook.common.dextricks.ReentrantLockFile$Lock r0 = r11.mLockHandle     // Catch:{ all -> 0x0070 }
        L_0x005e:
            monitor-exit(r11)
            return r0
        L_0x0060:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0070 }
            r1.<init>(r2)     // Catch:{ all -> 0x0070 }
            goto L_0x006f
        L_0x0066:
            monitor-exit(r11)
            return r3
        L_0x0068:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0070 }
            java.lang.String r0 = "cannot acquire closed lock"
            r1.<init>(r0)     // Catch:{ all -> 0x0070 }
        L_0x006f:
            throw r1     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.ReentrantLockFile.tryAcquire(int):com.facebook.common.dextricks.ReentrantLockFile$Lock");
    }

    private void addrefLocked() {
        if (this.mChannel != null) {
            this.mReferenceCount++;
            return;
        }
        throw new IllegalStateException("cannot add reference to dead lock");
    }

    private void claimLock(int i, FileLock fileLock) {
        if ((i & 1) == 0) {
            this.mLockOwner = Thread.currentThread();
        }
        this.mTheLock = fileLock;
        this.mLockFlags = i;
        this.mLockShareCount = 1;
    }

    public static synchronized ReentrantLockFile open(File file) {
        FileChannel fileChannel;
        RandomAccessFile randomAccessFile;
        synchronized (ReentrantLockFile.class) {
            File absoluteFile = file.getAbsoluteFile();
            ReentrantLockFile reentrantLockFile = sListHead;
            do {
                reentrantLockFile = reentrantLockFile.mNext;
                if (reentrantLockFile == sListHead) {
                    RandomAccessFile randomAccessFile2 = null;
                    try {
                        randomAccessFile = new RandomAccessFile(absoluteFile, "rw");
                    } catch (Throwable th) {
                        th = th;
                        fileChannel = null;
                        Fs.safeClose(randomAccessFile2);
                        Fs.safeClose(fileChannel);
                        throw th;
                    }
                    try {
                        fileChannel = randomAccessFile.getChannel();
                        try {
                            ReentrantLockFile reentrantLockFile2 = new ReentrantLockFile(absoluteFile, fileChannel);
                            ReentrantLockFile reentrantLockFile3 = sListHead;
                            reentrantLockFile2.mPrev = reentrantLockFile3;
                            reentrantLockFile2.mNext = reentrantLockFile3.mNext;
                            reentrantLockFile3.mNext = reentrantLockFile2;
                            reentrantLockFile2.mNext.mPrev = reentrantLockFile2;
                            Fs.safeClose((Closeable) null);
                            Fs.safeClose((Closeable) null);
                            return reentrantLockFile2;
                        } catch (Throwable th2) {
                            th = th2;
                            Fs.safeClose(randomAccessFile2);
                            Fs.safeClose(fileChannel);
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        fileChannel = null;
                        randomAccessFile2 = randomAccessFile;
                        Fs.safeClose(randomAccessFile2);
                        Fs.safeClose(fileChannel);
                        throw th;
                    }
                }
            } while (!absoluteFile.equals(reentrantLockFile.lockFileName));
            synchronized (reentrantLockFile) {
                try {
                    reentrantLockFile.addrefLocked();
                } catch (Throwable th4) {
                    while (true) {
                        th = th4;
                        break;
                    }
                }
            }
            return reentrantLockFile;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002f, code lost:
        if (r3 == false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0031, code lost:
        close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0034, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r0 = r10.mChannel.lock(0, Long.MAX_VALUE, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004b, code lost:
        if (r0 != null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        monitor-enter(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r10.mLockInProgress = false;
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0053, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0055, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x005b, code lost:
        monitor-enter(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        claimLock(r11, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r10.mLockInProgress = false;
        notifyAll();
        r0 = r10.mLockHandle;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0066, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0067, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0068, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0069, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x006c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x006e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0074, code lost:
        throw new java.lang.RuntimeException(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0075, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        monitor-enter(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        r10.mLockInProgress = false;
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x007e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0084, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x008c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x008d, code lost:
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x008f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0092, code lost:
        close();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:35:0x004d, B:67:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0092  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.common.dextricks.ReentrantLockFile.Lock acquireInterruptubly(int r11) {
        /*
            r10 = this;
            r0 = r11 & 1
            r1 = 1
            r2 = 0
            r9 = 0
            if (r0 == 0) goto L_0x0008
            r9 = 1
        L_0x0008:
            r10.assertMonitorLockNotHeld()
            monitor-enter(r10)     // Catch:{ all -> 0x000d }
            goto L_0x0011
        L_0x000d:
            r0 = move-exception
            r3 = 0
            goto L_0x0090
        L_0x0011:
            r3 = 0
        L_0x0012:
            com.facebook.common.dextricks.ReentrantLockFile$Lock r0 = r10.tryAcquire(r11)     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x001c
            com.facebook.common.dextricks.ReentrantLockFile$Lock r0 = r10.mLockHandle     // Catch:{ all -> 0x0085 }
            monitor-exit(r10)     // Catch:{ all -> 0x0085 }
            goto L_0x002f
        L_0x001c:
            boolean r0 = r10.mLockInProgress     // Catch:{ all -> 0x0085 }
            if (r0 != 0) goto L_0x0025
            int r0 = r10.mLockShareCount     // Catch:{ all -> 0x0085 }
            if (r0 != 0) goto L_0x0025
            goto L_0x0035
        L_0x0025:
            if (r3 != 0) goto L_0x002b
            r10.addrefLocked()     // Catch:{ all -> 0x0085 }
            r3 = 1
        L_0x002b:
            r10.wait()     // Catch:{ all -> 0x0085 }
            goto L_0x0012
        L_0x002f:
            if (r3 == 0) goto L_0x0034
            r10.close()
        L_0x0034:
            return r0
        L_0x0035:
            if (r3 != 0) goto L_0x003b
            r10.addrefLocked()     // Catch:{ all -> 0x0085 }
            r3 = 1
        L_0x003b:
            r10.mLockInProgress = r1     // Catch:{ all -> 0x0085 }
            monitor-exit(r10)     // Catch:{ all -> 0x0085 }
            java.nio.channels.FileChannel r4 = r10.mChannel     // Catch:{ IOException -> 0x006e }
            r5 = 0
            r7 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            java.nio.channels.FileLock r0 = r4.lock(r5, r7, r9)     // Catch:{ IOException -> 0x006e }
            if (r0 != 0) goto L_0x005b
            monitor-enter(r10)     // Catch:{ all -> 0x008f }
            r10.mLockInProgress = r2     // Catch:{ all -> 0x0055 }
            r10.notifyAll()     // Catch:{ all -> 0x0055 }
            monitor-exit(r10)     // Catch:{ all -> 0x0055 }
            goto L_0x0058
        L_0x0055:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x0055 }
            goto L_0x0084
        L_0x0058:
            r10.close()     // Catch:{ all -> 0x008f }
        L_0x005b:
            monitor-enter(r10)     // Catch:{ all -> 0x008f }
            r10.claimLock(r11, r0)     // Catch:{ all -> 0x0068 }
            r10.mLockInProgress = r2     // Catch:{ all -> 0x006c }
            r10.notifyAll()     // Catch:{ all -> 0x006c }
            com.facebook.common.dextricks.ReentrantLockFile$Lock r0 = r10.mLockHandle     // Catch:{ all -> 0x006c }
            monitor-exit(r10)     // Catch:{ all -> 0x006c }
            return r0
        L_0x0068:
            r0 = move-exception
            r2 = r3
        L_0x006a:
            monitor-exit(r10)     // Catch:{ all -> 0x006c }
            goto L_0x008b
        L_0x006c:
            r0 = move-exception
            goto L_0x006a
        L_0x006e:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0075 }
            r0.<init>(r1)     // Catch:{ all -> 0x0075 }
            throw r0     // Catch:{ all -> 0x0075 }
        L_0x0075:
            r0 = move-exception
            monitor-enter(r10)     // Catch:{ all -> 0x008f }
            r10.mLockInProgress = r2     // Catch:{ all -> 0x007e }
            r10.notifyAll()     // Catch:{ all -> 0x007e }
            monitor-exit(r10)     // Catch:{ all -> 0x007e }
            goto L_0x0081
        L_0x007e:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x007e }
            goto L_0x0084
        L_0x0081:
            r10.close()     // Catch:{ all -> 0x008f }
        L_0x0084:
            throw r0     // Catch:{ all -> 0x008f }
        L_0x0085:
            r0 = move-exception
            r2 = r3
        L_0x0087:
            monitor-exit(r10)     // Catch:{ all -> 0x0089 }
            goto L_0x008b
        L_0x0089:
            r0 = move-exception
            goto L_0x0087
        L_0x008b:
            throw r0     // Catch:{ all -> 0x008c }
        L_0x008c:
            r0 = move-exception
            r3 = r2
            goto L_0x0090
        L_0x008f:
            r0 = move-exception
        L_0x0090:
            if (r3 == 0) goto L_0x0095
            r10.close()
        L_0x0095:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.ReentrantLockFile.acquireInterruptubly(int):com.facebook.common.dextricks.ReentrantLockFile$Lock");
    }

    public void donateLock(Thread thread) {
        boolean z = false;
        if (this.mLockOwner == Thread.currentThread()) {
            z = true;
        }
        Mlog.assertThat(z, "caller must own lock exclusively", new Object[0]);
        this.mLockOwner = thread;
    }

    public void stealLock() {
        boolean z = false;
        if (this.mLockOwner != null) {
            z = true;
        }
        Mlog.assertThat(z, "cannot steal unowned lock", new Object[0]);
        this.mLockOwner = Thread.currentThread();
    }

    private void assertMonitorLockNotHeld() {
        Mlog.assertThat(!Thread.holdsLock(this), "lock order violation", new Object[0]);
    }

    public Lock acquire(int i) {
        Lock lock;
        boolean z;
        try {
            lock = acquireInterruptubly(i);
            z = false;
        } catch (InterruptedException unused) {
            z = true;
            lock = null;
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return lock;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        r2 = com.facebook.common.dextricks.ReentrantLockFile.class;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0015, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r0 = r3.mReferenceCount - 1;
        r3.mReferenceCount = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001c, code lost:
        if (r0 != 0) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001e, code lost:
        r1 = r3.mPrev;
        r1.mNext = r3.mNext;
        r3.mNext.mPrev = r1;
        r3.mPrev = null;
        r3.mNext = null;
        com.facebook.common.dextricks.Fs.safeClose(r3.mChannel);
        r3.mChannel = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0036, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x003f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close() {
        /*
            r3 = this;
            r3.assertMonitorLockNotHeld()
            monitor-enter(r3)
            java.nio.channels.FileChannel r0 = r3.mChannel     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0010
            int r0 = r3.mReferenceCount     // Catch:{ all -> 0x003d }
            r1 = 1
            if (r0 <= r1) goto L_0x0012
            int r0 = r0 - r1
            r3.mReferenceCount = r0     // Catch:{ all -> 0x003d }
        L_0x0010:
            monitor-exit(r3)     // Catch:{ all -> 0x003d }
            return
        L_0x0012:
            monitor-exit(r3)     // Catch:{ all -> 0x003d }
            java.lang.Class<com.facebook.common.dextricks.ReentrantLockFile> r2 = com.facebook.common.dextricks.ReentrantLockFile.class
            monitor-enter(r2)
            monitor-enter(r3)     // Catch:{ all -> 0x003a }
            int r0 = r3.mReferenceCount     // Catch:{ all -> 0x0037 }
            int r0 = r0 - r1
            r3.mReferenceCount = r0     // Catch:{ all -> 0x0037 }
            if (r0 != 0) goto L_0x0034
            com.facebook.common.dextricks.ReentrantLockFile r1 = r3.mPrev     // Catch:{ all -> 0x0037 }
            com.facebook.common.dextricks.ReentrantLockFile r0 = r3.mNext     // Catch:{ all -> 0x0037 }
            r1.mNext = r0     // Catch:{ all -> 0x0037 }
            com.facebook.common.dextricks.ReentrantLockFile r0 = r3.mNext     // Catch:{ all -> 0x0037 }
            r0.mPrev = r1     // Catch:{ all -> 0x0037 }
            r1 = 0
            r3.mPrev = r1     // Catch:{ all -> 0x0037 }
            r3.mNext = r1     // Catch:{ all -> 0x0037 }
            java.nio.channels.FileChannel r0 = r3.mChannel     // Catch:{ all -> 0x0037 }
            com.facebook.common.dextricks.Fs.safeClose(r0)     // Catch:{ all -> 0x0037 }
            r3.mChannel = r1     // Catch:{ all -> 0x0037 }
        L_0x0034:
            monitor-exit(r3)     // Catch:{ all -> 0x0037 }
            monitor-exit(r2)     // Catch:{ all -> 0x003a }
            return
        L_0x0037:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0037 }
            throw r0     // Catch:{ all -> 0x003a }
        L_0x003a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003a }
            goto L_0x003f
        L_0x003d:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x003d }
        L_0x003f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.ReentrantLockFile.close():void");
    }

    public Thread getExclusiveOwner() {
        return this.mLockOwner;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (r5.mLockOwner == java.lang.Thread.currentThread()) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void release() {
        /*
            r5 = this;
            r5.assertMonitorLockNotHeld()
            monitor-enter(r5)
            int r0 = r5.mLockShareCount     // Catch:{ all -> 0x0054 }
            r4 = 1
            r3 = 0
            r2 = 0
            if (r0 <= 0) goto L_0x000c
            r2 = 1
        L_0x000c:
            java.lang.String r1 = "lock release balance"
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch:{ all -> 0x0054 }
            com.facebook.common.dextricks.Mlog.assertThat(r2, r1, r0)     // Catch:{ all -> 0x0054 }
            int r1 = r5.mLockFlags     // Catch:{ all -> 0x0054 }
            r1 = r1 & r4
            r0 = 0
            if (r1 == 0) goto L_0x001a
            r0 = 1
        L_0x001a:
            if (r0 != 0) goto L_0x0025
            java.lang.Thread r1 = r5.mLockOwner     // Catch:{ all -> 0x0054 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0054 }
            r2 = 0
            if (r1 != r0) goto L_0x0026
        L_0x0025:
            r2 = 1
        L_0x0026:
            java.lang.String r1 = "lock thread affinity"
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch:{ all -> 0x0054 }
            com.facebook.common.dextricks.Mlog.assertThat(r2, r1, r0)     // Catch:{ all -> 0x0054 }
            int r0 = r5.mLockShareCount     // Catch:{ all -> 0x0054 }
            int r0 = r0 - r4
            r5.mLockShareCount = r0     // Catch:{ all -> 0x0054 }
            if (r0 != 0) goto L_0x0042
            java.nio.channels.FileLock r0 = r5.mTheLock     // Catch:{ IOException -> 0x003b }
            r0.release()     // Catch:{ IOException -> 0x003b }
            r0 = 0
            goto L_0x0044
        L_0x003b:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0054 }
            r0.<init>(r1)     // Catch:{ all -> 0x0054 }
            throw r0     // Catch:{ all -> 0x0054 }
        L_0x0042:
            r4 = 0
            goto L_0x004d
        L_0x0044:
            r5.mLockOwner = r0     // Catch:{ all -> 0x0054 }
            r5.mTheLock = r0     // Catch:{ all -> 0x0054 }
            r5.mLockFlags = r3     // Catch:{ all -> 0x0054 }
            r5.notifyAll()     // Catch:{ all -> 0x0054 }
        L_0x004d:
            monitor-exit(r5)     // Catch:{ all -> 0x0054 }
            if (r4 == 0) goto L_0x0053
            r5.close()
        L_0x0053:
            return
        L_0x0054:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0054 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.ReentrantLockFile.release():void");
    }

    private ReentrantLockFile() {
        this.lockFileName = null;
        this.mLockHandle = null;
        this.mNext = this;
        this.mPrev = this;
    }

    private ReentrantLockFile(File file, FileChannel fileChannel) {
        this.lockFileName = file;
        this.mChannel = fileChannel;
        this.mReferenceCount = 1;
        this.mLockHandle = new Lock();
    }
}
