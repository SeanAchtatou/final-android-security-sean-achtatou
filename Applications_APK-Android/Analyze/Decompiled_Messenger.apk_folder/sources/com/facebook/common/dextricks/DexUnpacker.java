package com.facebook.common.dextricks;

import X.AnonymousClass08Q;
import X.C000000c;
import android.content.Context;
import com.facebook.common.dextricks.DexManifest;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DexUnpacker {
    private final Context mAppContext;
    private final DexIteratorFactory mDexIteratorFactory;
    private final ResProvider mResProvider;

    public final class CopiedDexInfo {
        public final DexManifest.Dex dex;
        public final File dexFile;

        public CopiedDexInfo renamedTo(File file) {
            File file2 = this.dexFile;
            File file3 = new File(file, this.dex.makeDexName());
            if (file3.equals(file2)) {
                return this;
            }
            Mlog.safeFmt("Copying unpacked dex file %s to final dest %s", file2.getAbsolutePath(), file3.getAbsolutePath());
            Fs.renameOrThrow(file2, file3);
            return new CopiedDexInfo(this.dex, file3);
        }

        public CopiedDexInfo(DexManifest.Dex dex2, File file) {
            this.dex = dex2;
            this.dexFile = file;
        }
    }

    public class CopyDexIterator implements Iterator, Closeable {
        private final File mDestDir;
        private final InputDexIterator mInputDexIterator;

        public void close() {
            this.mInputDexIterator.close();
        }

        public boolean hasNext() {
            return this.mInputDexIterator.hasNext();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public CopyDexIterator(InputDexIterator inputDexIterator, File file) {
            this.mInputDexIterator = inputDexIterator;
            this.mDestDir = file;
        }

        public CopiedDexInfo next() {
            InputDex next = this.mInputDexIterator.next();
            try {
                CopiedDexInfo copyDex = DexUnpacker.copyDex(next, this.mDestDir);
                if (next != null) {
                    next.close();
                }
                return copyDex;
            } catch (IOException e) {
                Mlog.w(e, "Error unpacking dex%s", next);
                if (next != null) {
                    next.close();
                }
                return null;
            } catch (Throwable th) {
                if (next != null) {
                    next.close();
                }
                throw th;
            }
        }
    }

    public CopyDexIterator getCopyDexIterator(DexManifest dexManifest, File file, C000000c r8) {
        if (dexManifest == null) {
            Mlog.w("Cannot unpack dexes with a null manifest", new Object[0]);
            return null;
        }
        String str = dexManifest.id;
        try {
            DexIteratorFactory dexIteratorFactory = this.mDexIteratorFactory;
            if (r8 == null) {
                r8 = AnonymousClass08Q.A00;
            }
            return new CopyDexIterator(dexIteratorFactory.openDexIterator(str, dexManifest, r8, this.mAppContext), file);
        } catch (IOException e) {
            Mlog.w(e, "Could not create copy dex iterator", new Object[0]);
            return null;
        }
    }

    private static List copyAllDexes(CopyDexIterator copyDexIterator) {
        if (copyDexIterator != null) {
            ArrayList arrayList = new ArrayList();
            while (copyDexIterator.hasNext()) {
                CopiedDexInfo next = copyDexIterator.next();
                if (next != null) {
                    arrayList.add(next);
                } else {
                    throw new IOException("Could not unpack dex");
                }
            }
            return arrayList;
        }
        throw new IOException("Could not unpack dex since it could not find dexes");
    }

    private static List copyAllDexesAndMove(File file, CopyDexIterator copyDexIterator) {
        if (copyDexIterator != null) {
            ArrayList arrayList = new ArrayList();
            while (copyDexIterator.hasNext()) {
                CopiedDexInfo next = copyDexIterator.next();
                if (next != null) {
                    arrayList.add(next.renamedTo(file));
                } else {
                    throw new IOException("Could not unpack dex");
                }
            }
            return arrayList;
        }
        throw new IOException("Could not unpack dex since it could not find dexes");
    }

    private static C000000c getNonNullTracer(C000000c r0) {
        if (r0 == null) {
            return AnonymousClass08Q.A00;
        }
        return r0;
    }

    public DexUnpacker(Context context, ResProvider resProvider) {
        this.mAppContext = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.mResProvider = resProvider;
        this.mDexIteratorFactory = new DexIteratorFactory(resProvider);
    }

    public static CopiedDexInfo copyDex(InputDex inputDex, File file) {
        Mlog.safeFmt("Unpacking %s", inputDex);
        File file2 = new File(file, inputDex.dex.makeDexName());
        inputDex.extract(file2);
        Mlog.safeFmt("Unpacked dex file to %s", file2.getAbsolutePath());
        return new CopiedDexInfo(inputDex.dex, file2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        if (r1 != null) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0016, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List copyDexes(com.facebook.common.dextricks.DexManifest r3, java.io.File r4, X.C000000c r5) {
        /*
            r2 = this;
            com.facebook.common.dextricks.DexUnpacker$CopyDexIterator r1 = r2.getCopyDexIterator(r3, r4, r5)
            java.util.List r0 = copyAllDexes(r1)     // Catch:{ all -> 0x000e }
            if (r1 == 0) goto L_0x000d
            r1.close()
        L_0x000d:
            return r0
        L_0x000e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0010 }
        L_0x0010:
            r0 = move-exception
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ all -> 0x0016 }
        L_0x0016:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexUnpacker.copyDexes(com.facebook.common.dextricks.DexManifest, java.io.File, X.00c):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0012, code lost:
        if (r1 != null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List copySecondaryDexes(java.io.File r3) {
        /*
            r2 = this;
            r0 = 0
            com.facebook.common.dextricks.DexUnpacker$CopyDexIterator r1 = r2.getCopySecondaryDexIterator(r3, r0)
            java.util.List r0 = copyAllDexes(r1)     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x000e
            r1.close()
        L_0x000e:
            return r0
        L_0x000f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0011 }
        L_0x0011:
            r0 = move-exception
            if (r1 == 0) goto L_0x0017
            r1.close()     // Catch:{ all -> 0x0017 }
        L_0x0017:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexUnpacker.copySecondaryDexes(java.io.File):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        if (r1 != null) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0016, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List copySecondaryDexes(java.io.File r3, X.C000000c r4) {
        /*
            r2 = this;
            com.facebook.common.dextricks.DexUnpacker$CopyDexIterator r1 = r2.getCopySecondaryDexIterator(r3, r4)
            java.util.List r0 = copyAllDexes(r1)     // Catch:{ all -> 0x000e }
            if (r1 == 0) goto L_0x000d
            r1.close()
        L_0x000d:
            return r0
        L_0x000e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0010 }
        L_0x0010:
            r0 = move-exception
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ all -> 0x0016 }
        L_0x0016:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexUnpacker.copySecondaryDexes(java.io.File, X.00c):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0012, code lost:
        if (r1 != null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List copySecondaryDexes(java.io.File r3, java.io.File r4) {
        /*
            r2 = this;
            r0 = 0
            com.facebook.common.dextricks.DexUnpacker$CopyDexIterator r1 = r2.getCopySecondaryDexIterator(r3, r0)
            java.util.List r0 = copyAllDexesAndMove(r4, r1)     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x000e
            r1.close()
        L_0x000e:
            return r0
        L_0x000f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0011 }
        L_0x0011:
            r0 = move-exception
            if (r1 == 0) goto L_0x0017
            r1.close()     // Catch:{ all -> 0x0017 }
        L_0x0017:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexUnpacker.copySecondaryDexes(java.io.File, java.io.File):java.util.List");
    }

    public CopyDexIterator getCopySecondaryDexIterator(File file) {
        return getCopySecondaryDexIterator(file, null);
    }

    public CopyDexIterator getCopySecondaryDexIterator(File file, C000000c r6) {
        try {
            return getCopyDexIterator(DexManifest.loadManifestFrom(this.mResProvider, DexStoreUtils.SECONDARY_DEX_MANIFEST, false), file, r6);
        } catch (IOException e) {
            Mlog.w(e, "Cannot unpack secondary dexes", new Object[0]);
            return null;
        }
    }
}
