package com.facebook.common.dextricks;

import X.AnonymousClass018;
import X.AnonymousClass08S;
import X.AnonymousClass08X;
import X.AnonymousClass095;
import X.AnonymousClass0D4;
import X.AnonymousClass1Y3;
import X.C000000c;
import X.C011108x;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Parcel;
import android.os.SystemClock;
import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.common.dextricks.MultiDexClassLoader;
import com.facebook.common.dextricks.OptimizationConfiguration;
import com.facebook.common.dextricks.ReentrantLockFile;
import com.facebook.forker.Fd;
import com.facebook.forker.Process;
import dalvik.system.DexFile;
import io.card.payment.BuildConfig;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.webrtc.audio.WebRtcAudioRecord;

public final class DexStore {
    private static final String CONFIG_FILENAME = "config";
    private static final String CONFIG_TMP_FILENAME = "config.tmp";
    public static final long CS_DEX0OPT = 16;
    public static final long CS_STATE_MASK = 15;
    public static final byte CS_STATE_SHIFT = 4;
    private static final int DAYS_TO_MS_FACTOR = 86400000;
    public static final byte DEFAULT_MULTIDEX_COMPILATION_STRATEGY = 0;
    public static final byte DEFAULT_PGO_COMPILER_FILTER = 0;
    private static final String DEPS_FILENAME = "deps";
    public static final int DS_ASYNC = 4;
    public static final int DS_DO_NOT_OPTIMIZE = 1;
    public static final int DS_FORCE_SYNC = 8;
    public static final int DS_LOAD_SECONDARY = 16;
    public static final int DS_NO_RETRY = 2;
    private static final int LA_LOAD_EXISTING = 0;
    private static final int LA_REGEN_ALL = 2;
    private static final int LA_REGEN_MISSING = 1;
    public static final int LOAD_RESULT_CREATED_BY_OATMEAL = 128;
    public static final int LOAD_RESULT_DEX2OAT_CLASSPATH_SET = 16384;
    public static final int LOAD_RESULT_DEX2OAT_QUICKENED = 512;
    public static final int LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED = 4096;
    public static final int LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP = 131072;
    public static final int LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED = 262144;
    public static final int LOAD_RESULT_MIXED_MODE = 1024;
    public static final int LOAD_RESULT_MIXED_MODE_ATTEMPTED = 8192;
    public static final int LOAD_RESULT_NEED_OPTIMIZATION = 2;
    public static final int LOAD_RESULT_NEED_REOPTIMIZATION = 1048576;
    public static final int LOAD_RESULT_NOT_OPTIMIZED = 8;
    public static final int LOAD_RESULT_OATMEAL_QUICKENED = 256;
    public static final int LOAD_RESULT_OATMEAL_QUICKEN_ATTEMPTED = 2048;
    public static final int LOAD_RESULT_OPTIMIZATION_IS_CRAZY_EXPENSIVE = 4;
    public static final int LOAD_RESULT_PGO = 32768;
    public static final int LOAD_RESULT_PGO_ATTEMPTED = 65536;
    public static final int LOAD_RESULT_RECOVERED_FROM_BAD_GEN = 64;
    public static final int LOAD_RESULT_RECOVERED_FROM_CORRUPTION = 16;
    public static final int LOAD_RESULT_REGENERATED = 1;
    public static final int LOAD_RESULT_REGEN_FORCED = 32;
    public static final int LOAD_RESULT_WITH_VDEX_ODEX = 524288;
    private static final String MDEX_DIRECTORY = "mdex";
    private static final String MDEX_LOCK_FILENAME = "mdex_lock";
    private static final String MDEX_STATUS_FILENAME = "mdex_status2";
    private static final long MDEX_STATUS_XOR = -374168170706063353L;
    private static final int MS_IN_NS = 1000000;
    public static final byte MULTIDEX_COMPILATION_STRATEGY_EVERY_COLDSTART_DEX = 2;
    public static final byte MULTIDEX_COMPILATION_STRATEGY_EVERY_DEX = 1;
    public static final byte MULTIDEX_COMPILATION_STRATEGY_FIRST_COLDSTART_DEX = 0;
    private static final long NO_TIMESTAMP = 0;
    private static final String ODEX_LOCK_FILENAME = "odex_lock";
    private static final String OPTIMIZATION_HISTORY_LOG_FILENAME = "optimization_history_log";
    private static final String OPTIMIZATION_LOG_FILENAME = "optimization_log";
    public static final byte PGO_COMPILER_FILTER_EVERYTHING_PROFILE = 2;
    public static final byte PGO_COMPILER_FILTER_SPACE_PROFILE = 1;
    public static final byte PGO_COMPILER_FILTER_SPEED_PROFILE = 0;
    public static final byte PGO_COMPILER_FILTER_VERIFY_PROFILE = 3;
    private static final String REGEN_STAMP_FILENAME = "regen_stamp";
    private static final byte STATE_ART_TURBO = 7;
    private static final byte STATE_ART_XDEX = 8;
    private static final byte STATE_BAD_GEN = 5;
    private static final byte STATE_FALLBACK = 2;
    private static final byte STATE_INVALID = 0;
    private static final byte STATE_NOOP = 9;
    private static final byte STATE_REGEN_FORCED = 6;
    private static final byte STATE_RESERVED1 = 10;
    private static final byte STATE_TURBO = 4;
    private static final byte STATE_TX_FAILED = 1;
    private static final byte STATE_XDEX = 3;
    private static final String TMPDIR_LOCK_SUFFIX = ".tmpdir_lock";
    private static final String TMPDIR_SUFFIX = ".tmpdir";
    public static boolean logDexAddPageFaults;
    public static long majPageFaultsDelta;
    public static long pageInBytesDelta;
    private static boolean sAttemptedCrossDexHookInstallation;
    private static long sCachedLastAppUpdateTime;
    private static Throwable sCrossDexHookInstallationError;
    public static DexStoreClock sDexStoreClock = new NormalDexStoreClock();
    public static DexStoreTestHooks sDexStoreTestHooks;
    private static DexStore sListHead;
    private static boolean sLoadedCompressedOreo;
    private static MultiDexClassLoader.Configuration sMergedDexConfig;
    private final ArrayList auxiliaryDexes;
    private String id;
    private final File mApk;
    private OptimizationHistoryLog mCachedOptimizationHistoryLog;
    private final List mChildStores = new ArrayList();
    private DexIteratorFactory mDexIteratorFactory;
    private DexErrorRecoveryInfo mLastDeri;
    public DexManifest mLoadedManifest;
    public final ReentrantLockFile mLockFile;
    private DexManifest mManifest;
    private String mMegaZipPath = null;
    private final List mParentStores = new ArrayList();
    private ResProvider mResProvider;
    public final DexStore next = sListHead;
    private final ArrayList primaryDexes;
    public final File root;

    public final class Config {
        public static final byte ART_FILTER_BALANCED = 4;
        public static final byte ART_FILTER_DEFAULT = 0;
        public static final byte ART_FILTER_EVERYTHING = 6;
        public static final byte ART_FILTER_INTERPRET_ONLY = 2;
        public static final byte ART_FILTER_SPACE = 3;
        public static final byte ART_FILTER_SPEED = 5;
        public static final byte ART_FILTER_TIME = 7;
        public static final byte ART_FILTER_VERIFY_NONE = 1;
        public static final byte DALVIK_OPT_ALL = 3;
        public static final byte DALVIK_OPT_DEFAULT = 0;
        public static final byte DALVIK_OPT_FULL = 4;
        public static final byte DALVIK_OPT_NONE = 1;
        public static final byte DALVIK_OPT_VERIFIED = 2;
        public static final byte DALVIK_REGISTER_MAPS_DEFAULT = 0;
        public static final byte DALVIK_REGISTER_MAPS_NO = 1;
        public static final byte DALVIK_REGISTER_MAPS_YES = 2;
        public static final byte DALVIK_VERIFY_ALL = 3;
        public static final byte DALVIK_VERIFY_DEFAULT = 0;
        public static final byte DALVIK_VERIFY_NONE = 1;
        public static final byte DALVIK_VERIFY_REMOTE = 2;
        public static final byte MODE_DEFAULT = 0;
        public static final byte MODE_FORCE_FALLBACK = 1;
        public static final byte MODE_FORCE_TURBO = 2;
        public static final byte MODE_FORCE_XDEX = 3;
        public static final byte SYNC_CONTROL_ASYNC = 1;
        public static final byte SYNC_CONTROL_DEFAULT = 0;
        public static final byte SYNC_CONTROL_SYNC = 2;
        public static final byte VERSION = 7;
        public final byte artFilter;
        public final int artHugeMethodMax;
        public final int artLargeMethodMax;
        public final int artSmallMethodMax;
        public final int artTinyMethodMax;
        public final int artTruncatedDexSize;
        public final byte dalvikOptimize;
        public final byte dalvikRegisterMaps;
        public final byte dalvikVerify;
        public final boolean enableArtVerifyNone;
        public final boolean enableDex2OatQuickening;
        public final boolean enableMixedMode;
        public final boolean enableMixedModeClassPath;
        public final boolean enableMixedModePgo;
        public final boolean enableOatmeal;
        public final boolean enableOatmealQuickening;
        public final boolean enableQuickening;
        public final long minTimeBetweenPgoCompilationMs;
        public final byte mode;
        public final byte multidexCompilationStrategy;
        public final byte pgoCompilerFilter;
        public final byte sync;
        public final boolean tryPeriodicPgoCompilation;

        public final class Builder {
            public byte mArtFilter = 0;
            public int mArtHugeMethodMax = -1;
            public int mArtLargeMethodMax = -1;
            public int mArtSmallMethodMax = -1;
            public int mArtTinyMethodMax = -1;
            public int mArtTruncatedDexSize = -1;
            public byte mDalvikOptimize = 0;
            public byte mDalvikRegisterMaps = 0;
            public byte mDalvikVerify = 0;
            public boolean mDoPeriodicPgoCompilation = false;
            public boolean mEnableArtVerifyNone = false;
            public boolean mEnableDex2OatQuickening = false;
            public boolean mEnableMixedMode = false;
            public boolean mEnableMixedModeClassPath = false;
            public boolean mEnableMixedModePgo = false;
            public boolean mEnableOatmeal = Config.enableOatmealByDefault();
            public boolean mEnableOatmealQuickening = false;
            public boolean mEnableQuickening = false;
            public long mMinTimeBetweenPgoCompilationMs = -1;
            public byte mMode = 0;
            public byte mMultidexCompilationStrategy = 0;
            public byte mPgoCompilerFilter = 0;
            public byte mSync = 0;

            public Config build() {
                byte b = this.mMode;
                byte b2 = this.mSync;
                byte b3 = this.mDalvikVerify;
                byte b4 = this.mDalvikOptimize;
                byte b5 = this.mDalvikRegisterMaps;
                byte b6 = this.mArtFilter;
                int i = this.mArtHugeMethodMax;
                int i2 = this.mArtLargeMethodMax;
                int i3 = this.mArtSmallMethodMax;
                int i4 = this.mArtTinyMethodMax;
                int i5 = this.mArtTruncatedDexSize;
                boolean z = this.mEnableArtVerifyNone;
                boolean z2 = this.mEnableOatmeal;
                byte b7 = b5;
                byte b8 = b6;
                int i6 = i;
                int i7 = i2;
                return new Config(b, b2, b3, b4, b7, b8, i6, i7, i3, i4, i5, z, z2, this.mEnableDex2OatQuickening, this.mEnableOatmealQuickening, this.mEnableQuickening, this.mEnableMixedMode, this.mEnableMixedModeClassPath, this.mEnableMixedModePgo, this.mPgoCompilerFilter, this.mDoPeriodicPgoCompilation, this.mMinTimeBetweenPgoCompilationMs, this.mMultidexCompilationStrategy);
            }

            public Builder setMinTimeBetweenPgoCompilationDays(double d) {
                this.mMinTimeBetweenPgoCompilationMs = convertDaysToMs(d);
                return this;
            }

            private long convertDaysToMs(double d) {
                return Math.round(d * 8.64E7d);
            }

            public Builder setArtFilter(byte b) {
                this.mArtFilter = b;
                return this;
            }

            public Builder setArtHugeMethodMax(int i) {
                this.mArtHugeMethodMax = i;
                return this;
            }

            public Builder setArtLargeMethodMax(int i) {
                this.mArtLargeMethodMax = i;
                return this;
            }

            public Builder setArtSmallMethodMax(int i) {
                this.mArtSmallMethodMax = i;
                return this;
            }

            public Builder setArtTinyMethodMax(int i) {
                this.mArtTinyMethodMax = i;
                return this;
            }

            public Builder setArtTruncatedDexSize(int i) {
                this.mArtTruncatedDexSize = i;
                return this;
            }

            public Builder setDalvikOptimize(byte b) {
                this.mDalvikOptimize = b;
                return this;
            }

            public Builder setDalvikRegisterMaps(byte b) {
                this.mDalvikRegisterMaps = b;
                return this;
            }

            public Builder setDalvikVerify(byte b) {
                this.mDalvikVerify = b;
                return this;
            }

            public Builder setDoPeriodicPgoCompilation(boolean z) {
                this.mDoPeriodicPgoCompilation = z;
                return this;
            }

            public Builder setEnableArtVerifyNone(boolean z) {
                this.mEnableArtVerifyNone = z;
                return this;
            }

            public Builder setEnableDex2OatQuickening(boolean z) {
                this.mEnableDex2OatQuickening = z;
                return this;
            }

            public Builder setEnableMixedMode(boolean z) {
                this.mEnableMixedMode = z;
                return this;
            }

            public Builder setEnableMixedModeClassPath(boolean z) {
                this.mEnableMixedModeClassPath = z;
                return this;
            }

            public Builder setEnableMixedModePgo(boolean z) {
                this.mEnableMixedModePgo = z;
                return this;
            }

            public Builder setEnableOatmeal(boolean z) {
                this.mEnableOatmeal = z;
                return this;
            }

            public Builder setEnableOatmealQuickening(boolean z) {
                this.mEnableOatmealQuickening = z;
                return this;
            }

            public Builder setEnableQuickening(boolean z) {
                this.mEnableQuickening = z;
                return this;
            }

            public Builder setMinTimeBetweenPgoCompilationMs(long j) {
                this.mMinTimeBetweenPgoCompilationMs = j;
                return this;
            }

            public Builder setMode(byte b) {
                this.mMode = b;
                return this;
            }

            public Builder setMultidexCompilationStrategy(byte b) {
                this.mMultidexCompilationStrategy = b;
                return this;
            }

            public Builder setPgoCompilerFilter(byte b) {
                this.mPgoCompilerFilter = b;
                return this;
            }

            public Builder setSync(byte b) {
                this.mSync = b;
                return this;
            }

            public Builder(Config config) {
                this.mMode = config.mode;
                this.mSync = config.sync;
                this.mDalvikVerify = config.dalvikVerify;
                this.mDalvikOptimize = config.dalvikOptimize;
                this.mDalvikRegisterMaps = config.dalvikRegisterMaps;
                this.mArtFilter = config.artFilter;
                this.mArtHugeMethodMax = config.artHugeMethodMax;
                this.mArtLargeMethodMax = config.artLargeMethodMax;
                this.mArtSmallMethodMax = config.artSmallMethodMax;
                this.mArtTinyMethodMax = config.artTinyMethodMax;
                this.mArtTruncatedDexSize = config.artTruncatedDexSize;
                this.mEnableArtVerifyNone = config.enableArtVerifyNone;
                this.mEnableOatmeal = config.enableOatmeal;
                this.mEnableDex2OatQuickening = config.enableDex2OatQuickening;
                this.mEnableOatmealQuickening = config.enableOatmealQuickening;
                this.mEnableQuickening = config.enableQuickening;
                this.mEnableMixedMode = config.enableMixedMode;
                this.mEnableMixedModeClassPath = config.enableMixedModeClassPath;
                this.mEnableMixedModePgo = config.enableMixedModePgo;
                this.mPgoCompilerFilter = config.pgoCompilerFilter;
                this.mDoPeriodicPgoCompilation = config.tryPeriodicPgoCompilation;
                this.mMinTimeBetweenPgoCompilationMs = config.minTimeBetweenPgoCompilationMs;
                this.mMultidexCompilationStrategy = config.multidexCompilationStrategy;
            }
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj != null && getClass() == obj.getClass()) {
                    Config config = (Config) obj;
                    if (!(this.mode == config.mode && this.sync == config.sync && this.dalvikVerify == config.dalvikVerify && this.dalvikOptimize == config.dalvikOptimize && this.dalvikRegisterMaps == config.dalvikRegisterMaps && this.artFilter == config.artFilter && this.artHugeMethodMax == config.artHugeMethodMax && this.artLargeMethodMax == config.artLargeMethodMax && this.artSmallMethodMax == config.artSmallMethodMax && this.artTinyMethodMax == config.artTinyMethodMax && this.artTruncatedDexSize == config.artTruncatedDexSize && this.enableArtVerifyNone == config.enableArtVerifyNone && this.enableOatmeal == config.enableOatmeal && this.enableDex2OatQuickening == config.enableDex2OatQuickening && this.enableOatmealQuickening == config.enableOatmealQuickening && this.enableQuickening == config.enableQuickening && this.enableMixedMode == config.enableMixedMode && this.enableMixedModeClassPath == config.enableMixedModeClassPath && this.enableMixedModePgo == config.enableMixedModePgo && this.pgoCompilerFilter == config.pgoCompilerFilter && this.multidexCompilationStrategy == config.multidexCompilationStrategy && this.tryPeriodicPgoCompilation == config.tryPeriodicPgoCompilation && this.minTimeBetweenPgoCompilationMs == config.minTimeBetweenPgoCompilationMs)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        public static File getConfigFileName(File file) {
            return new File(file, DexStore.CONFIG_FILENAME);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x007f, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r0.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0083, code lost:
            throw r1;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static com.facebook.common.dextricks.DexStore.Config read(java.io.File r25) {
            /*
                java.io.RandomAccessFile r0 = new java.io.RandomAccessFile
                java.lang.String r1 = "r"
                r2 = r25
                r0.<init>(r2, r1)
                byte r2 = r0.readByte()     // Catch:{ all -> 0x007d }
                r1 = 7
                if (r2 != r1) goto L_0x0075
                byte r2 = r0.readByte()     // Catch:{ all -> 0x007d }
                byte r3 = r0.readByte()     // Catch:{ all -> 0x007d }
                byte r4 = r0.readByte()     // Catch:{ all -> 0x007d }
                byte r5 = r0.readByte()     // Catch:{ all -> 0x007d }
                byte r6 = r0.readByte()     // Catch:{ all -> 0x007d }
                byte r7 = r0.readByte()     // Catch:{ all -> 0x007d }
                int r8 = r0.readInt()     // Catch:{ all -> 0x007d }
                int r9 = r0.readInt()     // Catch:{ all -> 0x007d }
                int r10 = r0.readInt()     // Catch:{ all -> 0x007d }
                int r11 = r0.readInt()     // Catch:{ all -> 0x007d }
                int r12 = r0.readInt()     // Catch:{ all -> 0x007d }
                boolean r13 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                boolean r14 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                boolean r15 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                boolean r16 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                boolean r17 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                boolean r18 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                boolean r19 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                boolean r20 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                byte r21 = r0.readByte()     // Catch:{ all -> 0x007d }
                boolean r22 = r0.readBoolean()     // Catch:{ all -> 0x007d }
                long r23 = r0.readLong()     // Catch:{ all -> 0x007d }
                byte r25 = r0.readByte()     // Catch:{ all -> 0x007d }
                com.facebook.common.dextricks.DexStore$Config r1 = new com.facebook.common.dextricks.DexStore$Config     // Catch:{ all -> 0x007d }
                r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r25)     // Catch:{ all -> 0x007d }
                r0.close()
                return r1
            L_0x0075:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x007d }
                java.lang.String r1 = "unexpected version"
                r2.<init>(r1)     // Catch:{ all -> 0x007d }
                throw r2     // Catch:{ all -> 0x007d }
            L_0x007d:
                r1 = move-exception
                throw r1     // Catch:{ all -> 0x007f }
            L_0x007f:
                r1 = move-exception
                r0.close()     // Catch:{ all -> 0x0083 }
            L_0x0083:
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.Config.read(java.io.File):com.facebook.common.dextricks.DexStore$Config");
        }

        public boolean equalsForBootstrapPurposes(Config config) {
            if (config != null && config.mode == this.mode && config.sync == this.sync && config.artFilter == this.artFilter && config.enableArtVerifyNone == this.enableArtVerifyNone && config.enableOatmeal == this.enableOatmeal && config.enableDex2OatQuickening == this.enableDex2OatQuickening && config.enableOatmealQuickening == this.enableOatmealQuickening && config.enableMixedMode == this.enableMixedMode && config.enableMixedModeClassPath == this.enableMixedModeClassPath && config.enableMixedModePgo == this.enableMixedModePgo && config.pgoCompilerFilter == this.pgoCompilerFilter && config.multidexCompilationStrategy == this.multidexCompilationStrategy && config.tryPeriodicPgoCompilation == this.tryPeriodicPgoCompilation) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return ((((((((((((((((((((((((((((((((((((((((((((AnonymousClass1Y3.BS5 + this.mode) * 31) + this.sync) * 31) + this.dalvikVerify) * 31) + this.dalvikOptimize) * 31) + this.dalvikRegisterMaps) * 31) + this.artFilter) * 31) + this.artHugeMethodMax) * 31) + this.artLargeMethodMax) * 31) + this.artSmallMethodMax) * 31) + this.artTinyMethodMax) * 31) + this.artTruncatedDexSize) * 31) + (this.enableArtVerifyNone ? 1 : 0)) * 31) + (this.enableOatmeal ? 1 : 0)) * 31) + (this.enableDex2OatQuickening ? 1 : 0)) * 31) + (this.enableOatmealQuickening ? 1 : 0)) * 31) + (this.enableQuickening ? 1 : 0)) * 31) + (this.enableMixedMode ? 1 : 0)) * 31) + (this.enableMixedModeClassPath ? 1 : 0)) * 31) + (this.enableMixedModePgo ? 1 : 0)) * 31) + this.pgoCompilerFilter) * 31) + this.multidexCompilationStrategy) * 31) + (this.tryPeriodicPgoCompilation ? 1 : 0)) * 31) + ((int) this.minTimeBetweenPgoCompilationMs);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0096, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0092, code lost:
            r0 = move-exception;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void writeAndSync(java.io.File r4) {
            /*
                r3 = this;
                java.io.RandomAccessFile r2 = new java.io.RandomAccessFile
                java.lang.String r0 = "rw"
                r2.<init>(r4, r0)
                r0 = 7
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.mode     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.sync     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.dalvikVerify     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.dalvikOptimize     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.dalvikRegisterMaps     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.artFilter     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                int r0 = r3.artHugeMethodMax     // Catch:{ all -> 0x0090 }
                r2.writeInt(r0)     // Catch:{ all -> 0x0090 }
                int r0 = r3.artLargeMethodMax     // Catch:{ all -> 0x0090 }
                r2.writeInt(r0)     // Catch:{ all -> 0x0090 }
                int r0 = r3.artSmallMethodMax     // Catch:{ all -> 0x0090 }
                r2.writeInt(r0)     // Catch:{ all -> 0x0090 }
                int r0 = r3.artTinyMethodMax     // Catch:{ all -> 0x0090 }
                r2.writeInt(r0)     // Catch:{ all -> 0x0090 }
                int r0 = r3.artTruncatedDexSize     // Catch:{ all -> 0x0090 }
                r2.writeInt(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableArtVerifyNone     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableOatmeal     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableDex2OatQuickening     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableOatmealQuickening     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableQuickening     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableMixedMode     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableMixedModeClassPath     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.enableMixedModePgo     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.pgoCompilerFilter     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                boolean r0 = r3.tryPeriodicPgoCompilation     // Catch:{ all -> 0x0090 }
                r2.writeBoolean(r0)     // Catch:{ all -> 0x0090 }
                long r0 = r3.minTimeBetweenPgoCompilationMs     // Catch:{ all -> 0x0090 }
                r2.writeLong(r0)     // Catch:{ all -> 0x0090 }
                byte r0 = r3.multidexCompilationStrategy     // Catch:{ all -> 0x0090 }
                r2.writeByte(r0)     // Catch:{ all -> 0x0090 }
                long r0 = r2.getFilePointer()     // Catch:{ all -> 0x0090 }
                r2.setLength(r0)     // Catch:{ all -> 0x0090 }
                java.io.FileDescriptor r0 = r2.getFD()     // Catch:{ all -> 0x0090 }
                r0.sync()     // Catch:{ all -> 0x0090 }
                r2.close()
                return
            L_0x0090:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0092 }
            L_0x0092:
                r0 = move-exception
                r2.close()     // Catch:{ all -> 0x0096 }
            L_0x0096:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.Config.writeAndSync(java.io.File):void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
            if (X.AnonymousClass0MU.A04 == false) goto L_0x000f;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static boolean enableOatmealByDefault() {
            /*
                int r1 = com.facebook.common.build.BuildConstants.A00()
                r0 = 1
                if (r1 <= r0) goto L_0x0008
                r0 = 0
            L_0x0008:
                if (r0 == 0) goto L_0x000f
                boolean r1 = X.AnonymousClass0MU.A04
                r0 = 1
                if (r1 != 0) goto L_0x0010
            L_0x000f:
                r0 = 0
            L_0x0010:
                if (r0 != 0) goto L_0x0017
                boolean r1 = X.AnonymousClass018.A01
                r0 = 0
                if (r1 == 0) goto L_0x0018
            L_0x0017:
                r0 = 1
            L_0x0018:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.Config.enableOatmealByDefault():boolean");
        }

        public static Config readFromRoot(File file) {
            return read(getConfigFileName(file));
        }

        public boolean isDefault() {
            return equals(new Config((byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, -1, -1, -1, -1, -1, false, enableOatmealByDefault(), false, false, false, false, false, false, (byte) 0, false, -1, (byte) 0));
        }

        public byte[] readDepBlock() {
            Parcel obtain = Parcel.obtain();
            try {
                obtain.writeByte(this.mode);
                obtain.writeByte(this.sync);
                obtain.writeByte(this.dalvikVerify);
                obtain.writeByte(this.dalvikOptimize);
                obtain.writeByte(this.dalvikRegisterMaps);
                obtain.writeByte(this.artFilter);
                obtain.writeInt(this.artHugeMethodMax);
                obtain.writeInt(this.artLargeMethodMax);
                obtain.writeInt(this.artSmallMethodMax);
                obtain.writeInt(this.artTinyMethodMax);
                int i = 0;
                obtain.writeBooleanArray(new boolean[]{this.enableArtVerifyNone, this.enableOatmeal, this.enableDex2OatQuickening, this.enableOatmealQuickening, this.enableQuickening, this.enableMixedMode, this.enableMixedModeClassPath, this.enableMixedModePgo});
                obtain.writeByte(this.pgoCompilerFilter);
                if (this.tryPeriodicPgoCompilation) {
                    i = 1;
                }
                obtain.writeByte((byte) i);
                obtain.writeLong(this.minTimeBetweenPgoCompilationMs);
                obtain.writeByte(this.multidexCompilationStrategy);
                return obtain.marshall();
            } finally {
                obtain.recycle();
            }
        }

        public Config(byte b, byte b2, byte b3, byte b4, byte b5, byte b6, int i, int i2, int i3, int i4, int i5, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, byte b7, boolean z9, long j, byte b8) {
            this.mode = b;
            this.sync = b2;
            this.dalvikVerify = b3;
            this.dalvikOptimize = b4;
            this.dalvikRegisterMaps = b5;
            this.artFilter = b6;
            this.artHugeMethodMax = i;
            this.artLargeMethodMax = i2;
            this.artSmallMethodMax = i3;
            this.artTinyMethodMax = i4;
            this.artTruncatedDexSize = i5;
            this.enableArtVerifyNone = z;
            this.enableOatmeal = z2;
            this.enableDex2OatQuickening = z3;
            this.enableOatmealQuickening = z4;
            this.enableQuickening = z5;
            this.enableMixedMode = z6;
            this.enableMixedModeClassPath = z7;
            this.enableMixedModePgo = z8;
            this.pgoCompilerFilter = b7;
            this.tryPeriodicPgoCompilation = z9;
            this.minTimeBetweenPgoCompilationMs = j;
            this.multidexCompilationStrategy = b8;
        }
    }

    public interface DexStoreClock {
        long now();
    }

    public interface DexStoreTestHooks {
        void onSecondaryDexesUnpackedForRecompilation();
    }

    public interface ExternalProcessProgressListener {
        void onCheckpoint();

        void onComplete(int i);
    }

    public final class FinishRegenerationThread extends Thread {
        private final ReentrantLockFile.Lock mHeldLock;
        private final long mNewStatus;
        private final OdexScheme mOdexScheme;

        public FinishRegenerationThread(OdexScheme odexScheme, ReentrantLockFile.Lock lock, long j) {
            super(AnonymousClass08S.A0J("TxFlush-", DexStore.this.root.getName()));
            this.mHeldLock = lock;
            this.mNewStatus = j;
            this.mOdexScheme = odexScheme;
        }

        public void run() {
            try {
                Mlog.safeFmt("running syncer thread", new Object[0]);
                for (String file : this.mOdexScheme.expectedFiles) {
                    File file2 = new File(DexStore.this.root, file);
                    if (file2.exists()) {
                        DalvikInternals.fsyncNamed(file2.getCanonicalPath(), -1);
                    }
                }
                DexStore.this.writeStatusLocked(this.mNewStatus);
                this.mHeldLock.close();
                Mlog.safeFmt("finished syncer thread: initial regeneration of dex store %s complete", DexStore.this.root);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (Throwable th) {
                this.mHeldLock.close();
                throw th;
            }
        }
    }

    public final class OptimizationHistoryLog {
        public static final long NO_TIME_DELTA = -1;
        public static final int SUCCESS = 2;
        private final File file;
        private final long lastFileModifiedTime;
        public final long lastOptedAppUpgradeTimestamp;
        public final long lastSuccessfulOptimizationTimestampMs;
        private final long optStatus;
        public final long schemeStatus;

        public static File getDefaultFile(File file2) {
            return new File(file2, DexStore.OPTIMIZATION_HISTORY_LOG_FILENAME);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0037, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0033, code lost:
            r0 = move-exception;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static com.facebook.common.dextricks.DexStore.OptimizationHistoryLog read(java.io.File r14) {
            /*
                java.io.RandomAccessFile r1 = new java.io.RandomAccessFile
                java.lang.String r0 = "r"
                r1.<init>(r14, r0)
                long r2 = r1.readLong()     // Catch:{ all -> 0x0031 }
                long r4 = com.facebook.common.dextricks.DexStore.sanityCheckTimestamp(r2)     // Catch:{ all -> 0x0031 }
                long r6 = r1.readLong()     // Catch:{ all -> 0x0031 }
                long r8 = r1.readLong()     // Catch:{ all -> 0x0031 }
                long r10 = r1.readLong()     // Catch:{ all -> 0x0031 }
                long r12 = com.facebook.common.dextricks.DexStore.lastModifiedTime(r14)     // Catch:{ all -> 0x0031 }
                com.facebook.common.dextricks.DexStore$OptimizationHistoryLog r3 = new com.facebook.common.dextricks.DexStore$OptimizationHistoryLog     // Catch:{ all -> 0x0031 }
                r3.<init>(r4, r6, r8, r10, r12, r14)     // Catch:{ all -> 0x0031 }
                java.lang.String r2 = "Read opt history log %s"
                java.lang.Object[] r0 = new java.lang.Object[]{r3}     // Catch:{ all -> 0x0031 }
                com.facebook.common.dextricks.Mlog.safeFmt(r2, r0)     // Catch:{ all -> 0x0031 }
                r1.close()
                return r3
            L_0x0031:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0033 }
            L_0x0033:
                r0 = move-exception
                r1.close()     // Catch:{ all -> 0x0037 }
            L_0x0037:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.OptimizationHistoryLog.read(java.io.File):com.facebook.common.dextricks.DexStore$OptimizationHistoryLog");
        }

        public boolean isLogFileAsFromRoot(File file2) {
            if (file2 != null) {
                return getDefaultFile(file2).equals(this.file);
            }
            if (this.file == null) {
                return true;
            }
            return false;
        }

        public boolean isNotDefault() {
            if (this.file != null) {
                return true;
            }
            return false;
        }

        public boolean isOutOfDate() {
            if (DexStore.lastModifiedTime(this.file) != this.lastFileModifiedTime) {
                return true;
            }
            return false;
        }

        public boolean lastCompilationSessionWasASuccess() {
            if ((this.optStatus & 2) != 0) {
                return true;
            }
            return false;
        }

        public long timeDeltaFromLastCompilationSessionMs() {
            long j = this.lastSuccessfulOptimizationTimestampMs;
            if (j == 0) {
                return -1;
            }
            long now = DexStore.sDexStoreClock.now();
            if (j <= now) {
                return now - j;
            }
            return -1;
        }

        public String toString() {
            String str;
            String str2;
            StringBuilder sb = new StringBuilder("[ Opt History Log: ");
            sb.append("Default: ");
            sb.append(!isNotDefault());
            sb.append(", ");
            sb.append("Last Compile time: ");
            sb.append(this.lastSuccessfulOptimizationTimestampMs);
            sb.append(" ms, ");
            sb.append("Delta: ");
            sb.append(timeDeltaFromLastCompilationSessionMs());
            sb.append(" ms, ");
            sb.append("Opt Status: ");
            sb.append(this.optStatus);
            sb.append(" (");
            if (lastCompilationSessionWasASuccess()) {
                str = OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS;
            } else {
                str = OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_FAILURE;
            }
            sb.append(str);
            sb.append("), ");
            sb.append("Scheme Status: ");
            sb.append(this.schemeStatus);
            sb.append(", ");
            sb.append("Last app update time: ");
            sb.append(this.lastOptedAppUpgradeTimestamp);
            sb.append(" ms, ");
            sb.append("File: ");
            File file2 = this.file;
            if (file2 != null) {
                str2 = file2.getAbsolutePath();
            } else {
                str2 = "None";
            }
            sb.append(str2);
            sb.append(']');
            return sb.toString();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0028, code lost:
            r0 = move-exception;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void writeToDisk(java.io.File r4) {
            /*
                r3 = this;
                java.io.RandomAccessFile r2 = new java.io.RandomAccessFile
                java.lang.String r0 = "rw"
                r2.<init>(r4, r0)
                long r0 = r3.lastSuccessfulOptimizationTimestampMs     // Catch:{ all -> 0x0026 }
                r2.writeLong(r0)     // Catch:{ all -> 0x0026 }
                long r0 = r3.optStatus     // Catch:{ all -> 0x0026 }
                r2.writeLong(r0)     // Catch:{ all -> 0x0026 }
                long r0 = r3.schemeStatus     // Catch:{ all -> 0x0026 }
                r2.writeLong(r0)     // Catch:{ all -> 0x0026 }
                long r0 = r3.lastOptedAppUpgradeTimestamp     // Catch:{ all -> 0x0026 }
                r2.writeLong(r0)     // Catch:{ all -> 0x0026 }
                java.io.FileDescriptor r0 = r2.getFD()     // Catch:{ all -> 0x0026 }
                r0.sync()     // Catch:{ all -> 0x0026 }
                r2.close()
                return
            L_0x0026:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0028 }
            L_0x0028:
                r0 = move-exception
                r2.close()     // Catch:{ all -> 0x002c }
            L_0x002c:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.OptimizationHistoryLog.writeToDisk(java.io.File):void");
        }

        private static boolean canUseHistoryLogForThisApp(Context context, OptimizationHistoryLog optimizationHistoryLog) {
            long appUpgradeTimestamp = DexStore.getAppUpgradeTimestamp(context);
            if (appUpgradeTimestamp == 0) {
                Mlog.w("Found not app version", new Object[0]);
                return false;
            }
            Mlog.w("Comparing new %d to old %s", Long.valueOf(appUpgradeTimestamp), Long.valueOf(optimizationHistoryLog.lastOptedAppUpgradeTimestamp));
            if (appUpgradeTimestamp == optimizationHistoryLog.lastOptedAppUpgradeTimestamp) {
                return true;
            }
            return false;
        }

        public static void clearHistoryLog(File file2) {
            getDefaultFile(file2).delete();
        }

        public static OptimizationHistoryLog readOrMakeDefault(Context context, File file2) {
            try {
                OptimizationHistoryLog read = read(file2);
                if (canUseHistoryLogForThisApp(context, read)) {
                    return read;
                }
                Mlog.safeFmt("Could not use previous history log since it was for a different version or corrupted. optHistoryLog: %s", read);
                return new OptimizationHistoryLog();
            } catch (IOException unused) {
                return new OptimizationHistoryLog();
            }
        }

        public static OptimizationHistoryLog readOrMakeDefaultFromRoot(Context context, File file2) {
            return readOrMakeDefault(context, getDefaultFile(file2));
        }

        public static void writeNewStatus(Context context, File file2, boolean z, long j) {
            long j2;
            String str;
            File defaultFile = getDefaultFile(file2);
            if (z) {
                j2 = 2;
            } else {
                j2 = 0;
            }
            long now = DexStore.sDexStoreClock.now();
            long appUpgradeTimestamp = DexStore.getAppUpgradeTimestamp(context);
            if (z) {
                str = OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS;
            } else {
                str = "error";
            }
            long j3 = j;
            Mlog.safeFmt("Writing optimization history log %s [opt status: %d scheme status: %d] (app last update time %d) at %d ms for %s", str, Long.valueOf(j2), Long.valueOf(j3), Long.valueOf(appUpgradeTimestamp), Long.valueOf(now), defaultFile.getAbsolutePath());
            OptimizationHistoryLog optimizationHistoryLog = new OptimizationHistoryLog(now, j2, j3, appUpgradeTimestamp, 0, defaultFile);
            optimizationHistoryLog.writeToDisk(defaultFile);
            Mlog.safeFmt("Wrote optimization history log %s", optimizationHistoryLog);
        }

        private OptimizationHistoryLog() {
            this(0, 0, 0, 0, 0, null);
        }

        private OptimizationHistoryLog(long j, long j2, long j3, long j4, long j5, File file2) {
            this.lastSuccessfulOptimizationTimestampMs = j;
            this.optStatus = j2;
            this.schemeStatus = j3;
            this.lastOptedAppUpgradeTimestamp = j4;
            this.lastFileModifiedTime = j5;
            this.file = file2;
        }
    }

    public final class OptimizationLog {
        public static final int COMPLETE = 1;
        public static final int COUNTER_AWAKE_MS = 0;
        public static final int COUNTER_AWAKE_RUN_MS = 3;
        public static final int COUNTER_AWAKE_YIELD_MS = 2;
        public static final int COUNTER_REAL_TIME_MS = 1;
        public static final int NR_COUNTERS = 4;
        public static final int SUCCESS = 2;
        public long[] counters = new long[4];
        public int flags = 0;
        public boolean isNotDefault = false;
        public long[] lastAttemptCounters = new long[4];
        public String lastFailureExceptionJson = BuildConfig.FLAVOR;
        public int nrOptimizationsAttempted = 0;
        public int nrOptimizationsFailed = 0;

        public static String getCounterName(int i) {
            if (i == 0) {
                return "COUNTER_AWAKE_MS";
            }
            if (i == 1) {
                return "COUNTER_AWAKE_REAL_TIME_MS";
            }
            if (i == 2) {
                return "COUNTER_AWAKE_YIELD_MS";
            }
            if (i == 3) {
                return "COUNTER_AWAKE_RUN_MS";
            }
            throw new AssertionError(AnonymousClass08S.A09("unknown counter ", i));
        }

        public static File getDefaultFile(File file) {
            return new File(file, DexStore.OPTIMIZATION_LOG_FILENAME);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0044, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            r4.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0048, code lost:
            throw r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static com.facebook.common.dextricks.DexStore.OptimizationLog read(java.io.File r5) {
            /*
                java.io.RandomAccessFile r4 = new java.io.RandomAccessFile
                java.lang.String r0 = "r"
                r4.<init>(r5, r0)
                com.facebook.common.dextricks.DexStore$OptimizationLog r5 = new com.facebook.common.dextricks.DexStore$OptimizationLog     // Catch:{ all -> 0x0042 }
                r5.<init>()     // Catch:{ all -> 0x0042 }
                int r0 = r4.readInt()     // Catch:{ all -> 0x0042 }
                r5.flags = r0     // Catch:{ all -> 0x0042 }
                int r0 = r4.readInt()     // Catch:{ all -> 0x0042 }
                r5.nrOptimizationsAttempted = r0     // Catch:{ all -> 0x0042 }
                int r0 = r4.readInt()     // Catch:{ all -> 0x0042 }
                r5.nrOptimizationsFailed = r0     // Catch:{ all -> 0x0042 }
                r3 = 0
            L_0x001f:
                r0 = 4
                if (r3 >= r0) goto L_0x0035
                long[] r2 = r5.counters     // Catch:{ all -> 0x0042 }
                long r0 = r4.readLong()     // Catch:{ all -> 0x0042 }
                r2[r3] = r0     // Catch:{ all -> 0x0042 }
                long[] r2 = r5.lastAttemptCounters     // Catch:{ all -> 0x0042 }
                long r0 = r4.readLong()     // Catch:{ all -> 0x0042 }
                r2[r3] = r0     // Catch:{ all -> 0x0042 }
                int r3 = r3 + 1
                goto L_0x001f
            L_0x0035:
                java.lang.String r0 = r4.readUTF()     // Catch:{ all -> 0x0042 }
                r5.lastFailureExceptionJson = r0     // Catch:{ all -> 0x0042 }
                r0 = 1
                r5.isNotDefault = r0     // Catch:{ all -> 0x0042 }
                r4.close()
                return r5
            L_0x0042:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0044 }
            L_0x0044:
                r0 = move-exception
                r4.close()     // Catch:{ all -> 0x0048 }
            L_0x0048:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.OptimizationLog.read(java.io.File):com.facebook.common.dextricks.DexStore$OptimizationLog");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x003d, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            r3.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
            throw r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void write(java.io.File r5) {
            /*
                r4 = this;
                java.io.RandomAccessFile r3 = new java.io.RandomAccessFile
                java.lang.String r0 = "rw"
                r3.<init>(r5, r0)
                int r0 = r4.flags     // Catch:{ all -> 0x003b }
                r3.writeInt(r0)     // Catch:{ all -> 0x003b }
                int r0 = r4.nrOptimizationsAttempted     // Catch:{ all -> 0x003b }
                r3.writeInt(r0)     // Catch:{ all -> 0x003b }
                int r0 = r4.nrOptimizationsFailed     // Catch:{ all -> 0x003b }
                r3.writeInt(r0)     // Catch:{ all -> 0x003b }
                r2 = 0
            L_0x0017:
                r0 = 4
                if (r2 >= r0) goto L_0x002b
                long[] r0 = r4.counters     // Catch:{ all -> 0x003b }
                r0 = r0[r2]     // Catch:{ all -> 0x003b }
                r3.writeLong(r0)     // Catch:{ all -> 0x003b }
                long[] r0 = r4.lastAttemptCounters     // Catch:{ all -> 0x003b }
                r0 = r0[r2]     // Catch:{ all -> 0x003b }
                r3.writeLong(r0)     // Catch:{ all -> 0x003b }
                int r2 = r2 + 1
                goto L_0x0017
            L_0x002b:
                java.lang.String r0 = r4.lastFailureExceptionJson     // Catch:{ all -> 0x003b }
                r3.writeUTF(r0)     // Catch:{ all -> 0x003b }
                java.io.FileDescriptor r0 = r3.getFD()     // Catch:{ all -> 0x003b }
                r0.sync()     // Catch:{ all -> 0x003b }
                r3.close()
                return
            L_0x003b:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x003d }
            L_0x003d:
                r0 = move-exception
                r3.close()     // Catch:{ all -> 0x0041 }
            L_0x0041:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.OptimizationLog.write(java.io.File):void");
        }

        public static OptimizationLog readFromRoot(File file) {
            return read(getDefaultFile(file));
        }

        public static OptimizationLog readOrMakeDefault(File file) {
            try {
                return read(file);
            } catch (FileNotFoundException unused) {
                return new OptimizationLog();
            }
        }

        public static OptimizationLog readOrMakeDefaultFromRoot(File file) {
            return readOrMakeDefault(getDefaultFile(file));
        }

        public boolean isNotDefault() {
            return this.isNotDefault;
        }

        public void writeFromRoot(File file) {
            write(getDefaultFile(file));
        }
    }

    public final class OptimizationSession implements Closeable {
        private static final int PHASE_RUNNING = 0;
        private static final int PHASE_YIELDING = 1;
        public long accumulatedRunNs;
        public long accumulatedYieldNs;
        public final OptimizationConfiguration config;
        public final OptimizationConfiguration.Provider configProvider;
        public final Config dexStoreConfig;
        public final boolean inForeground;
        private final Context mContext;
        public final ReentrantLockFile mOptLockFile;
        private final FileInputStream mRegenStampFile;
        public final int optimizationAttemptNumber;
        public final long startRealtimeMs;
        public final long startUptimeMs;

        public final class Job implements Closeable {
            private static final int PHASE_COMMITTING = 2;
            private static final int PHASE_DONE = 3;
            private static final int PHASE_OPTIMIZING = 1;
            private static final int PHASE_PREPARING = 0;
            public final long initialStatus;
            private ReentrantLockFile.Lock mCommitLock;
            private ReentrantLockFile.Lock mOptLock;
            private int mPhase;

            public Job() {
                try {
                    this.mCommitLock = DexStore.this.mLockFile.acquireInterruptubly(0);
                    long readStatusLocked = DexStore.readStatusLocked(DexStore.this);
                    this.initialStatus = readStatusLocked;
                    checkBadStatus(readStatusLocked);
                } catch (Throwable th) {
                    close();
                    throw th;
                }
            }

            private void checkBadStatus(long j) {
                byte b = (byte) ((int) (15 & j));
                if (b == 0 || b == 1 || b == 5 || b >= 10) {
                    throw new OptimizationCanceledException(String.format("bad status %x for dex store %s starting tx", Long.valueOf(j), DexStore.this.root));
                }
                OptimizationSession.this.checkShouldStop();
            }

            public void close() {
                ReentrantLockFile.Lock lock = this.mCommitLock;
                if (lock != null) {
                    lock.close();
                    this.mCommitLock = null;
                }
                ReentrantLockFile.Lock lock2 = this.mOptLock;
                if (lock2 != null) {
                    lock2.close();
                    this.mOptLock = null;
                }
            }

            public void finishCommit(long j) {
                boolean z = false;
                if (this.mPhase == 2) {
                    z = true;
                }
                Mlog.assertThat(z, "wrong phase", new Object[0]);
                DexStore.this.writeStatusLocked(j);
                this.mCommitLock.close();
                this.mCommitLock = null;
                this.mPhase = 3;
            }

            public void startOptimizing() {
                boolean z = false;
                if (this.mPhase == 0) {
                    z = true;
                }
                Mlog.assertThat(z, "wrong phase", new Object[0]);
                this.mOptLock = OptimizationSession.this.mOptLockFile.acquireInterruptubly(1);
                this.mCommitLock.close();
                this.mCommitLock = null;
                this.mPhase = 1;
            }

            public long startCommitting() {
                return startCommitting(0);
            }

            public long startCommitting(long j) {
                int i = this.mPhase;
                boolean z = false;
                if (i == 1) {
                    z = true;
                }
                Mlog.assertThat(z, "wrong phase: %s", Integer.valueOf(i));
                this.mOptLock.close();
                this.mOptLock = null;
                this.mCommitLock = DexStore.this.mLockFile.acquireInterruptubly(0);
                long readStatusLocked = DexStore.readStatusLocked(DexStore.this);
                checkBadStatus(readStatusLocked);
                long j2 = j | readStatusLocked;
                DexStore.this.writeStatusLocked(1 | (j2 << 4));
                this.mPhase = 2;
                return j2;
            }
        }

        private byte determineOptimizationFailureState(byte b) {
            if (b == 8) {
                return 7;
            }
            return b == 3 ? (byte) 4 : 5;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0018, code lost:
            if (r1 != null) goto L_0x001a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x001d */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void copeWithOptimizationFailure(java.lang.Throwable r5) {
            /*
                r4 = this;
                r3 = 0
                com.facebook.common.dextricks.DexStore r0 = com.facebook.common.dextricks.DexStore.this     // Catch:{ all -> 0x001e }
                com.facebook.common.dextricks.ReentrantLockFile r0 = r0.mLockFile     // Catch:{ all -> 0x001e }
                com.facebook.common.dextricks.ReentrantLockFile$Lock r1 = r0.acquire(r3)     // Catch:{ all -> 0x001e }
                r4.checkShouldStop()     // Catch:{ all -> 0x0015 }
                r4.copeWithOptimizationFailureImpl(r5)     // Catch:{ all -> 0x0015 }
                if (r1 == 0) goto L_0x0026
                r1.close()     // Catch:{ all -> 0x001e }
                return
            L_0x0015:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0017 }
            L_0x0017:
                r0 = move-exception
                if (r1 == 0) goto L_0x001d
                r1.close()     // Catch:{ all -> 0x001d }
            L_0x001d:
                throw r0     // Catch:{ all -> 0x001e }
            L_0x001e:
                r2 = move-exception
                java.lang.Object[] r1 = new java.lang.Object[r3]
                java.lang.String r0 = "recording optimization failure itself failed"
                com.facebook.common.dextricks.Mlog.w(r2, r0, r1)
            L_0x0026:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.OptimizationSession.copeWithOptimizationFailure(java.lang.Throwable):void");
        }

        private int incrementOptimizationAttempts(OptimizationLog optimizationLog) {
            int i = optimizationLog.nrOptimizationsAttempted;
            if (i >= 1) {
                return 1 + i;
            }
            return 1;
        }

        private int resumeProcess(Process process) {
            process.kill(18);
            return process.waitFor(-1, 5);
        }

        private boolean shouldWriteOptimizationHistoryLog(OptimizationLog optimizationLog) {
            if ((optimizationLog.flags & 1) == 0) {
                return false;
            }
            return true;
        }

        private int stopProcess(Process process) {
            process.kill(20);
            return process.waitFor(-1, 6);
        }

        private void updateOptimizationLogCounters(OptimizationLog optimizationLog) {
            long[] jArr = optimizationLog.lastAttemptCounters;
            jArr[0] = SystemClock.uptimeMillis() - this.startUptimeMs;
            jArr[1] = SystemClock.elapsedRealtime() - this.startRealtimeMs;
            jArr[3] = this.accumulatedRunNs / 1000000;
            jArr[2] = this.accumulatedYieldNs / 1000000;
            for (int i = 0; i < 4; i++) {
                long[] jArr2 = optimizationLog.counters;
                jArr2[i] = jArr2[i] + jArr[i];
            }
        }

        public void checkShouldStop() {
            if (DalvikInternals.getOpenFileLinkCount(Fd.fileno(this.mRegenStampFile.getFD())) == 0) {
                throw new OptimizationCanceledException("obsolete optimization: regeneration pending");
            }
        }

        public void close() {
            Fs.safeClose(this.mOptLockFile);
            Fs.safeClose(this.mRegenStampFile);
        }

        public void copeWithOptimizationFailureImpl(Throwable th) {
            OptimizationLog readOrMakeDefaultFromRoot = OptimizationLog.readOrMakeDefaultFromRoot(DexStore.this.root);
            Mlog.w(th, "optimization failed (%s failures already)", Integer.valueOf(readOrMakeDefaultFromRoot.nrOptimizationsFailed));
            long readStatusLocked = DexStore.readStatusLocked(DexStore.this);
            byte b = (byte) ((int) (15 & readStatusLocked));
            updateOptimizationLogCounters(readOrMakeDefaultFromRoot);
            readOrMakeDefaultFromRoot.nrOptimizationsFailed++;
            readOrMakeDefaultFromRoot.lastFailureExceptionJson = AnonymousClass0D4.A00(th);
            int i = readOrMakeDefaultFromRoot.nrOptimizationsFailed;
            int i2 = this.config.maximumOptimizationAttempts;
            if (i >= i2) {
                Mlog.w("too many optimization failures (threshold is %s): will not keep trying", Integer.valueOf(i2));
                readStatusLocked = (long) determineOptimizationFailureState(b);
                readOrMakeDefaultFromRoot.flags |= 1;
            }
            writeCurrentStateFromRoot(readOrMakeDefaultFromRoot, readStatusLocked);
        }

        public OptimizationHistoryLog getOptimizationHistoryLog() {
            DexStore dexStore = DexStore.this;
            return DexStore.getCurrentOptHistoryLogFromRoot(dexStore, this.mContext, dexStore.root);
        }

        public int maxOptimizationAttempts() {
            return this.config.maximumOptimizationAttempts;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
            if (r2 != null) goto L_0x002f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
            throw r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void noteOptimizationSuccess() {
            /*
                r3 = this;
                com.facebook.common.dextricks.DexStore r0 = com.facebook.common.dextricks.DexStore.this
                com.facebook.common.dextricks.ReentrantLockFile r1 = r0.mLockFile
                r0 = 0
                com.facebook.common.dextricks.ReentrantLockFile$Lock r2 = r1.acquire(r0)
                boolean r0 = r3.inForeground     // Catch:{ all -> 0x002a }
                if (r0 != 0) goto L_0x0010
                r3.checkShouldStop()     // Catch:{ all -> 0x002a }
            L_0x0010:
                com.facebook.common.dextricks.DexStore r0 = com.facebook.common.dextricks.DexStore.this     // Catch:{ all -> 0x002a }
                java.io.File r0 = r0.root     // Catch:{ all -> 0x002a }
                com.facebook.common.dextricks.DexStore$OptimizationLog r1 = com.facebook.common.dextricks.DexStore.OptimizationLog.readOrMakeDefaultFromRoot(r0)     // Catch:{ all -> 0x002a }
                int r0 = r1.flags     // Catch:{ all -> 0x002a }
                r0 = r0 | 3
                r1.flags = r0     // Catch:{ all -> 0x002a }
                r3.updateOptimizationLogCounters(r1)     // Catch:{ all -> 0x002a }
                r3.writeCurrentStateWithCurrentStatusFromRoot(r1)     // Catch:{ all -> 0x002a }
                if (r2 == 0) goto L_0x0029
                r2.close()
            L_0x0029:
                return
            L_0x002a:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x002c }
            L_0x002c:
                r0 = move-exception
                if (r2 == 0) goto L_0x0032
                r2.close()     // Catch:{ all -> 0x0032 }
            L_0x0032:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.OptimizationSession.noteOptimizationSuccess():void");
        }

        /* JADX WARNING: Removed duplicated region for block: B:13:0x004a  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x006b  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0083 A[LOOP:0: B:1:0x0015->B:27:0x0083, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0070 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int waitForAndManageProcess(com.facebook.forker.Process r22, com.facebook.common.dextricks.DexStore.ExternalProcessProgressListener r23) {
            /*
                r21 = this;
                r6 = r21
                long r19 = java.lang.System.nanoTime()
                r0 = 1000000(0xf4240, double:4.940656E-318)
                long r17 = r19 / r0
                r5 = -2147483648(0xffffffff80000000, float:-0.0)
                r4 = 0
                r15 = r19
                r13 = r17
                r9 = 0
                r10 = -2147483648(0xffffffff80000000, float:-0.0)
            L_0x0015:
                com.facebook.common.dextricks.OptimizationConfiguration$Provider r0 = r6.configProvider
                com.facebook.common.dextricks.OptimizationConfiguration r8 = r0.getInstantaneous()
                long r11 = r17 - r13
                r7 = r22
                if (r9 != 0) goto L_0x0088
                long r0 = r6.accumulatedRunNs
                long r2 = r19 - r15
                long r0 = r0 + r2
                r6.accumulatedRunNs = r0
                int r0 = r8.optTimeSliceMs
                long r2 = (long) r0
                int r0 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
                if (r0 < 0) goto L_0x0048
                int r0 = r8.yieldTimeSliceMs
                if (r0 <= 0) goto L_0x0046
                java.lang.Object[] r1 = new java.lang.Object[r4]
                java.lang.String r0 = "beginning yield"
                com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
                int r10 = r6.stopProcess(r7)
                r0 = -2147483647(0xffffffff80000001, float:-1.4E-45)
                if (r10 != r0) goto L_0x0045
                r10 = -2147483648(0xffffffff80000000, float:-0.0)
            L_0x0045:
                r9 = 1
            L_0x0046:
                r13 = r17
            L_0x0048:
                if (r10 != r5) goto L_0x005b
                int r10 = r8.processPollMs
                long r2 = r2 - r11
                long r0 = (long) r10
                int r8 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
                if (r8 >= 0) goto L_0x0053
                int r10 = (int) r2
            L_0x0053:
                if (r10 >= 0) goto L_0x0056
                r10 = 0
            L_0x0056:
                r0 = 4
                int r10 = r7.waitFor(r10, r0)
            L_0x005b:
                r6.checkShouldStop()
                long r2 = java.lang.System.nanoTime()
                r0 = 1000000(0xf4240, double:4.940656E-318)
                long r17 = r2 / r0
                r7 = r23
                if (r23 == 0) goto L_0x006e
                r7.onCheckpoint()
            L_0x006e:
                if (r10 == r5) goto L_0x0083
                java.lang.Integer r0 = java.lang.Integer.valueOf(r10)
                java.lang.Object[] r1 = new java.lang.Object[]{r0}
                java.lang.String r0 = "process exited with status %s"
                com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
                if (r23 == 0) goto L_0x0082
                r7.onComplete(r10)
            L_0x0082:
                return r10
            L_0x0083:
                r15 = r19
                r19 = r2
                goto L_0x0015
            L_0x0088:
                r0 = 1
                if (r9 != r0) goto L_0x00b1
                long r0 = r6.accumulatedYieldNs
                long r2 = r19 - r15
                long r0 = r0 + r2
                r6.accumulatedYieldNs = r0
                int r0 = r8.yieldTimeSliceMs
                long r2 = (long) r0
                int r0 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
                if (r0 < 0) goto L_0x0048
                int r0 = r8.optTimeSliceMs
                if (r0 <= 0) goto L_0x0046
                java.lang.Object[] r1 = new java.lang.Object[r4]
                java.lang.String r0 = "ending yield"
                com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
                int r10 = r6.resumeProcess(r7)
                r0 = -2147483646(0xffffffff80000002, float:-2.8E-45)
                if (r10 != r0) goto L_0x00af
                r10 = -2147483648(0xffffffff80000000, float:-0.0)
            L_0x00af:
                r9 = 0
                goto L_0x0046
            L_0x00b1:
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.OptimizationSession.waitForAndManageProcess(com.facebook.forker.Process, com.facebook.common.dextricks.DexStore$ExternalProcessProgressListener):int");
        }

        public OptimizationSession(Context context, OptimizationConfiguration.Provider provider, boolean z) {
            ReentrantLockFile reentrantLockFile;
            this.mContext = context.getApplicationContext() != null ? context.getApplicationContext() : context;
            OptimizationLog readOrMakeDefaultFromRoot = OptimizationLog.readOrMakeDefaultFromRoot(DexStore.this.root);
            int incrementOptimizationAttempts = incrementOptimizationAttempts(readOrMakeDefaultFromRoot);
            readOrMakeDefaultFromRoot.nrOptimizationsAttempted = incrementOptimizationAttempts;
            this.optimizationAttemptNumber = incrementOptimizationAttempts;
            writeCurrentStateWithCurrentStatusFromRoot(readOrMakeDefaultFromRoot, false);
            this.dexStoreConfig = DexStore.this.readConfig();
            this.inForeground = z;
            this.startUptimeMs = SystemClock.uptimeMillis();
            this.startRealtimeMs = SystemClock.elapsedRealtime();
            FileInputStream fileInputStream = null;
            try {
                this.configProvider = provider;
                this.config = provider.baseline;
                FileInputStream fileInputStream2 = new FileInputStream(new File(DexStore.this.root, DexStore.REGEN_STAMP_FILENAME));
                try {
                    reentrantLockFile = ReentrantLockFile.open(new File(DexStore.this.root, DexStore.ODEX_LOCK_FILENAME));
                } catch (Throwable th) {
                    th = th;
                    reentrantLockFile = null;
                    fileInputStream = fileInputStream2;
                    Fs.safeClose(fileInputStream);
                    Fs.safeClose(reentrantLockFile);
                    throw th;
                }
                try {
                    this.mRegenStampFile = fileInputStream2;
                    try {
                        this.mOptLockFile = reentrantLockFile;
                        Fs.safeClose((Closeable) null);
                        Fs.safeClose((Closeable) null);
                    } catch (Throwable th2) {
                        th = th2;
                        Fs.safeClose(fileInputStream);
                        Fs.safeClose(reentrantLockFile);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    fileInputStream = fileInputStream2;
                    Fs.safeClose(fileInputStream);
                    Fs.safeClose(reentrantLockFile);
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                reentrantLockFile = null;
                Fs.safeClose(fileInputStream);
                Fs.safeClose(reentrantLockFile);
                throw th;
            }
        }

        private void writeCurrentStateFromRoot(OptimizationLog optimizationLog, long j) {
            writeCurrentStateFromRoot(optimizationLog, j, shouldWriteOptimizationHistoryLog(optimizationLog));
        }

        private void writeCurrentStateFromRoot(OptimizationLog optimizationLog, long j, boolean z) {
            DexStore.writeTxFailedStatusLocked(DexStore.this, j);
            boolean z2 = false;
            if ((optimizationLog.flags & 2) != 0) {
                z2 = true;
            }
            optimizationLog.writeFromRoot(DexStore.this.root);
            if (z) {
                OptimizationHistoryLog.writeNewStatus(this.mContext, DexStore.this.root, z2, j);
            }
            DexStore.this.writeStatusLocked(j);
        }

        private void writeCurrentStateWithCurrentStatusFromRoot(OptimizationLog optimizationLog) {
            writeCurrentStateWithCurrentStatusFromRoot(optimizationLog, shouldWriteOptimizationHistoryLog(optimizationLog));
        }

        private void writeCurrentStateWithCurrentStatusFromRoot(OptimizationLog optimizationLog, boolean z) {
            writeCurrentStateFromRoot(optimizationLog, DexStore.readStatusLocked(DexStore.this), z);
        }
    }

    public class ProgressListener {
        public void onProgress(int i, int i2, boolean z) {
        }
    }

    public final class TmpDir implements Closeable {
        public File directory;
        private ReentrantLockFile.Lock mTmpDirLock;

        public TmpDir(ReentrantLockFile.Lock lock, File file) {
            this.mTmpDirLock = lock;
            this.directory = file;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
            if (r2 != null) goto L_0x0029;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r3 = this;
                com.facebook.common.dextricks.ReentrantLockFile$Lock r0 = r3.mTmpDirLock
                if (r0 == 0) goto L_0x0032
                com.facebook.common.dextricks.DexStore r0 = com.facebook.common.dextricks.DexStore.this
                com.facebook.common.dextricks.ReentrantLockFile r1 = r0.mLockFile
                r0 = 0
                com.facebook.common.dextricks.ReentrantLockFile$Lock r2 = r1.acquire(r0)
                com.facebook.common.dextricks.ReentrantLockFile$Lock r1 = r3.mTmpDirLock     // Catch:{ all -> 0x0024 }
                com.facebook.common.dextricks.ReentrantLockFile r0 = com.facebook.common.dextricks.ReentrantLockFile.this     // Catch:{ all -> 0x0024 }
                java.io.File r0 = r0.lockFileName     // Catch:{ all -> 0x0024 }
                r1.close()     // Catch:{ all -> 0x0024 }
                r1 = 0
                r3.mTmpDirLock = r1     // Catch:{ all -> 0x0024 }
                com.facebook.common.dextricks.Fs.deleteRecursiveNoThrow(r0)     // Catch:{ all -> 0x0024 }
                java.io.File r0 = r3.directory     // Catch:{ all -> 0x0024 }
                com.facebook.common.dextricks.Fs.deleteRecursiveNoThrow(r0)     // Catch:{ all -> 0x0024 }
                r3.directory = r1     // Catch:{ all -> 0x0024 }
                goto L_0x002d
            L_0x0024:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0026 }
            L_0x0026:
                r0 = move-exception
                if (r2 == 0) goto L_0x002c
                r2.close()     // Catch:{ all -> 0x002c }
            L_0x002c:
                throw r0
            L_0x002d:
                if (r2 == 0) goto L_0x0032
                r2.close()
            L_0x0032:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.TmpDir.close():void");
        }
    }

    private static boolean checkAndClearGk(Context context, String str) {
        try {
            return AnonymousClass08X.A07(context, str);
        } finally {
            AnonymousClass08X.A05(context, str, false);
        }
    }

    private void deleteFiles(String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null) {
                Mlog.safeFmt("deleting existing file %s/%s", this.root, strArr[i]);
                Fs.deleteRecursive(new File(this.root, strArr[i]));
            }
        }
    }

    private int findInArray(String[] strArr, String str) {
        for (int i = 0; i < strArr.length; i++) {
            if (str.equals(strArr[i])) {
                return i;
            }
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d A[Catch:{ Exception -> 0x0040, all -> 0x00af }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006e A[Catch:{ Exception -> 0x0040, all -> 0x00af }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0077 A[Catch:{ Exception -> 0x0040, all -> 0x00af }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] readCurrentDepBlock() {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            r5 = 0
            java.io.File r0 = r8.mApk     // Catch:{ Exception -> 0x000a }
            java.io.File r5 = determineOdexCacheName(r0)     // Catch:{ Exception -> 0x000a }
            goto L_0x0014
        L_0x000a:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r5}
            java.lang.String r0 = "error reading odex cache file %s"
            com.facebook.common.dextricks.Mlog.w(r2, r0, r1)
        L_0x0014:
            android.os.Parcel r3 = android.os.Parcel.obtain()
            if (r5 == 0) goto L_0x004a
            boolean r0 = r5.exists()     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x004a
            com.facebook.common.dextricks.DexStore[] r4 = r8.getParents()     // Catch:{ Exception -> 0x0040 }
            int r2 = r4.length     // Catch:{ Exception -> 0x0040 }
            r1 = 0
        L_0x0026:
            if (r1 >= r2) goto L_0x0034
            r0 = r4[r1]     // Catch:{ Exception -> 0x0040 }
            byte[] r0 = r0.readCurrentDepBlock()     // Catch:{ Exception -> 0x0040 }
            r3.writeByteArray(r0)     // Catch:{ Exception -> 0x0040 }
            int r1 = r1 + 1
            goto L_0x0026
        L_0x0034:
            java.lang.String r0 = r5.getPath()     // Catch:{ Exception -> 0x0040 }
            byte[] r0 = com.facebook.common.dextricks.DalvikInternals.readOdexDepBlock(r0)     // Catch:{ Exception -> 0x0040 }
            r3.writeByteArray(r0)     // Catch:{ Exception -> 0x0040 }
            goto L_0x004b
        L_0x0040:
            r0 = move-exception
            java.lang.String r1 = "could not read odex dep block: using modtime: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00af }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00af }
        L_0x004a:
            r7 = 0
        L_0x004b:
            if (r7 != 0) goto L_0x006a
            java.io.File r0 = r8.mApk     // Catch:{ all -> 0x00af }
            long r1 = r0.lastModified()     // Catch:{ all -> 0x00af }
            r4 = 0
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0096
            java.io.File r0 = r8.mApk     // Catch:{ all -> 0x00af }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x00af }
            r3.writeString(r0)     // Catch:{ all -> 0x00af }
            r3.writeLong(r1)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = android.os.Build.FINGERPRINT     // Catch:{ all -> 0x00af }
            r3.writeString(r0)     // Catch:{ all -> 0x00af }
        L_0x006a:
            boolean r0 = X.AnonymousClass018.A01     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x0071
            r3.writeByte(r6)     // Catch:{ all -> 0x00af }
        L_0x0071:
            boolean r0 = X.AnonymousClass096.A01()     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x0083
            java.lang.Class<X.096> r1 = X.AnonymousClass096.class
            monitor-enter(r1)     // Catch:{ all -> 0x00af }
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x00af }
            r3.writeString(r0)     // Catch:{ all -> 0x00af }
        L_0x0083:
            com.facebook.common.dextricks.DexStore$Config r0 = r8.readConfig()     // Catch:{ all -> 0x00af }
            byte[] r0 = r0.readDepBlock()     // Catch:{ all -> 0x00af }
            r3.writeByteArray(r0)     // Catch:{ all -> 0x00af }
            byte[] r0 = r3.marshall()     // Catch:{ all -> 0x00af }
            r3.recycle()
            return r0
        L_0x0096:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x00af }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00af }
            r1.<init>()     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "unable to get modtime of "
            r1.append(r0)     // Catch:{ all -> 0x00af }
            java.io.File r0 = r8.mApk     // Catch:{ all -> 0x00af }
            r1.append(r0)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x00af }
            r2.<init>(r0)     // Catch:{ all -> 0x00af }
            throw r2     // Catch:{ all -> 0x00af }
        L_0x00af:
            r0 = move-exception
            r3.recycle()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.readCurrentDepBlock():byte[]");
    }

    public static void setClock(DexStoreClock dexStoreClock) {
        Mlog.safeFmt("Setting dexstore clock override", new Object[0]);
        sDexStoreClock = dexStoreClock;
    }

    public static void setDexStoreTestHooks(DexStoreTestHooks dexStoreTestHooks) {
        Mlog.safeFmt("Setting dexstore test hooks", new Object[0]);
        sDexStoreTestHooks = dexStoreTestHooks;
    }

    private void setDifference(String[] strArr, String[] strArr2) {
        for (int i = 0; i < strArr.length; i++) {
            String str = strArr[i];
            if (str != null) {
                int i2 = 0;
                while (true) {
                    if (i2 < strArr2.length) {
                        String str2 = strArr2[i2];
                        if (str2 != null && str.equals(str2)) {
                            strArr[i] = null;
                            break;
                        }
                        i2++;
                    } else {
                        break;
                    }
                }
            }
        }
    }

    private void verifyCanaryClasses(DexManifest dexManifest) {
        int i = 0;
        while (true) {
            DexManifest.Dex[] dexArr = dexManifest.dexes;
            if (i < dexArr.length) {
                Class.forName(dexArr[i].canaryClass);
                i++;
            } else {
                return;
            }
        }
    }

    public static void writeTxFailedStatusLocked(DexStore dexStore, long j) {
        dexStore.writeStatusLocked((j << 4) | 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (r9.isDefault() == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00a5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a6, code lost:
        if (r6 != null) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ab, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean atomicReplaceConfig(com.facebook.common.dextricks.DexStore.Config r9) {
        /*
            r8 = this;
            r7 = 1
            r2 = 0
            if (r9 == 0) goto L_0x000b
            boolean r1 = r9.isDefault()
            r0 = 1
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            r0 = 0
            if (r9 == 0) goto L_0x0014
            r0 = 1
        L_0x0014:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Replacing config is default: %s nn: s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            com.facebook.common.dextricks.ReentrantLockFile r0 = r8.mLockFile
            com.facebook.common.dextricks.ReentrantLockFile$Lock r6 = r0.acquire(r2)
            java.io.File r5 = new java.io.File     // Catch:{ all -> 0x00a3 }
            java.io.File r1 = r8.root     // Catch:{ all -> 0x00a3 }
            java.lang.String r0 = "config"
            r5.<init>(r1, r0)     // Catch:{ all -> 0x00a3 }
            com.facebook.common.dextricks.DexStore$Config r1 = r8.readConfig()     // Catch:{ all -> 0x00a3 }
            boolean r4 = r9.equals(r1)     // Catch:{ all -> 0x00a3 }
            boolean r0 = r8.attemptedOptimizationSinceRegeneration()     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x003f
            goto L_0x0044
        L_0x003f:
            boolean r3 = r9.equalsForBootstrapPurposes(r1)     // Catch:{ all -> 0x00a3 }
            goto L_0x0045
        L_0x0044:
            r3 = r4
        L_0x0045:
            if (r3 == 0) goto L_0x0059
            if (r4 != 0) goto L_0x0059
            boolean r0 = r8.checkDeps()     // Catch:{ all -> 0x00a3 }
            if (r0 != 0) goto L_0x0050
            r3 = 0
        L_0x0050:
            if (r3 == 0) goto L_0x0059
            boolean r0 = r8.attemptedOptimizationSinceRegeneration()     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x0059
            r3 = 0
        L_0x0059:
            boolean r0 = r9.isDefault()     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x0063
            com.facebook.common.dextricks.Fs.deleteRecursive(r5)     // Catch:{ all -> 0x00a3 }
            goto L_0x0072
        L_0x0063:
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x00a3 }
            java.io.File r1 = r8.root     // Catch:{ all -> 0x00a3 }
            java.lang.String r0 = "config.tmp"
            r2.<init>(r1, r0)     // Catch:{ all -> 0x00a3 }
            r9.writeAndSync(r2)     // Catch:{ all -> 0x00a3 }
            com.facebook.common.dextricks.Fs.renameOrThrow(r2, r5)     // Catch:{ all -> 0x00a3 }
        L_0x0072:
            if (r3 != 0) goto L_0x0093
            java.io.File r0 = r8.root     // Catch:{ all -> 0x00a3 }
            java.lang.String r1 = r0.getAbsolutePath()     // Catch:{ all -> 0x00a3 }
            r0 = -1
            com.facebook.common.dextricks.DalvikInternals.fsyncNamed(r1, r0)     // Catch:{ all -> 0x00a3 }
            monitor-enter(r8)     // Catch:{ all -> 0x00a3 }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x0090 }
            java.io.File r1 = r8.root     // Catch:{ all -> 0x0090 }
            java.lang.String r0 = "regen_stamp"
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0090 }
            r2.delete()     // Catch:{ all -> 0x0090 }
            r8.touchRegenStamp()     // Catch:{ all -> 0x0090 }
            monitor-exit(r8)     // Catch:{ all -> 0x0090 }
            goto L_0x0093
        L_0x0090:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0090 }
            throw r0     // Catch:{ all -> 0x00a3 }
        L_0x0093:
            if (r3 == 0) goto L_0x009a
            if (r4 != 0) goto L_0x009a
            r8.saveDeps()     // Catch:{ all -> 0x00a3 }
        L_0x009a:
            if (r3 == 0) goto L_0x009d
            r7 = 0
        L_0x009d:
            if (r6 == 0) goto L_0x00a2
            r6.close()
        L_0x00a2:
            return r7
        L_0x00a3:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a5 }
        L_0x00a5:
            r0 = move-exception
            if (r6 == 0) goto L_0x00ab
            r6.close()     // Catch:{ all -> 0x00ab }
        L_0x00ab:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.atomicReplaceConfig(com.facebook.common.dextricks.DexStore$Config):boolean");
    }

    public synchronized long getLastRegenTime() {
        return new File(this.root, REGEN_STAMP_FILENAME).lastModified();
    }

    public synchronized boolean isLoaded() {
        boolean z;
        z = false;
        if (this.mLoadedManifest != null) {
            z = true;
        }
        return z;
    }

    public boolean isReoptimization(Context context) {
        try {
            return readConfig().tryPeriodicPgoCompilation && getCurrentOptHistoryLogFromRoot(this, context, this.root).lastCompilationSessionWasASuccess();
        } catch (IOException e) {
            Mlog.e(e, "failed to check if reoptimization. Failing back to not a reoptimization.", new Object[0]);
            return false;
        }
    }

    public synchronized DexErrorRecoveryInfo loadAll(int i, C000000c r4, Context context) {
        DexErrorRecoveryInfo dexErrorRecoveryInfo;
        try {
            dexErrorRecoveryInfo = loadAllImpl(i, r4, context);
        } catch (RecoverableDexException e) {
            try {
                dexErrorRecoveryInfo = loadAllImpl(i | 2, r4, context);
                dexErrorRecoveryInfo.regenRetryCause = e;
            } catch (RecoverableDexException e2) {
                throw new AssertionError(e2);
            }
        }
        return dexErrorRecoveryInfo;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        if (r1 != null) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0022 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long reportStatus() {
        /*
            r5 = this;
            r4 = 0
            java.lang.Object[] r1 = new java.lang.Object[r4]
            java.lang.String r0 = "DexStore::reportStatus()"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            r2 = 0
            com.facebook.common.dextricks.ReentrantLockFile r0 = r5.mLockFile     // Catch:{ all -> 0x0023 }
            com.facebook.common.dextricks.ReentrantLockFile$Lock r1 = r0.acquire(r4)     // Catch:{ all -> 0x0023 }
            long r2 = readStatusLocked(r5)     // Catch:{ all -> 0x001a }
            if (r1 == 0) goto L_0x002d
            r1.close()     // Catch:{ all -> 0x0023 }
            return r2
        L_0x001a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001c }
        L_0x001c:
            r0 = move-exception
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ all -> 0x0022 }
        L_0x0022:
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "DexStore::reportStatus caught Throwable "
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
        L_0x002d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.reportStatus():long");
    }

    public class NormalDexStoreClock implements DexStoreClock {
        public long now() {
            return System.currentTimeMillis();
        }

        public NormalDexStoreClock() {
        }
    }

    private byte adjustDesiredStateForConfig(byte b, Config config) {
        byte b2 = config.mode;
        if (b2 == 0) {
            return b;
        }
        if (b2 == 1) {
            Mlog.safeFmt("using fallback mode due to request in config file", new Object[0]);
            return 2;
        } else if (b2 == 2) {
            if (b == 2) {
                Mlog.safeFmt("ignoring configured turbo mode: already forced to fallback mode", new Object[0]);
                return b;
            } else if (b == 3) {
                Mlog.safeFmt("using Dalvik turbo as requested in config file", new Object[0]);
                return 4;
            } else if (b == 4 || b == 7) {
                Mlog.safeFmt("config file wants turbo mode: already using it", new Object[0]);
                return b;
            } else if (b == 8) {
                Mlog.safeFmt("using ART turbo as requested in config file", new Object[0]);
                return 7;
            } else if (b != 9) {
                Mlog.w("ignoring configured turbo mode: state not whitelisted: %s", Byte.valueOf(b));
                return b;
            } else {
                Mlog.safeFmt("ignoring configured turbo mode: no dex loading to do", new Object[0]);
                return b;
            }
        } else if (b2 != 3) {
            Mlog.w("ignoring unknown configured dex mode %s", Byte.valueOf(b2));
            return b;
        } else if (b != 2) {
            if (b != 3) {
                if (b == 4) {
                    Mlog.safeFmt("using Dalvik xdex as requested in config", new Object[0]);
                    return 3;
                } else if (b == 7) {
                    Mlog.safeFmt("using ART xdex as requested in config file", new Object[0]);
                    return STATE_ART_XDEX;
                } else if (b != 8) {
                    if (b != 9) {
                        Mlog.w("ignoring configured xdex mode: state not whitelisted: %s", Byte.valueOf(b));
                        return b;
                    }
                    Mlog.safeFmt("ignoring configured xdex mode: no dex loading to do", new Object[0]);
                    return b;
                }
            }
            Mlog.safeFmt("config file wants xdex mode: already using it", new Object[0]);
            return b;
        } else {
            Mlog.safeFmt("ignoring configured xdex mode: already forced to fallback", new Object[0]);
            return b;
        }
    }

    private void assertLockHeld() {
        boolean z = false;
        if (this.mLockFile.mLockOwner == Thread.currentThread()) {
            z = true;
        }
        Mlog.assertThat(z, "lock req", new Object[0]);
    }

    private static boolean canLoadCanaryClass(DexManifest dexManifest) {
        String str = dexManifest.dexes[0].canaryClass;
        try {
            Mlog.safeFmt("attempting to detect built-in ART multidex by classloading %s", str);
            Class.forName(dexManifest.dexes[0].canaryClass);
            Mlog.safeFmt("ART native multi-dex in use: found %s", str);
            return true;
        } catch (ClassNotFoundException unused) {
            Mlog.safeFmt("ART multi-dex not in use: cannot load %s", str);
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0025, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0026, code lost:
        if (r1 != null) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x002b */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x002b=Splitter:B:20:0x002b, B:10:0x001f=Splitter:B:10:0x001f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean checkAnyOptimizerRunningCurrently() {
        /*
            r4 = this;
            java.io.File r3 = new java.io.File
            java.io.File r1 = r4.root
            java.lang.String r0 = "odex_lock"
            r3.<init>(r1, r0)
            r2 = 0
            boolean r0 = r3.exists()     // Catch:{ IOException -> 0x002c }
            if (r0 == 0) goto L_0x002c
            com.facebook.common.dextricks.ReentrantLockFile r1 = com.facebook.common.dextricks.ReentrantLockFile.open(r3)     // Catch:{ IOException -> 0x002c }
            com.facebook.common.dextricks.ReentrantLockFile$Lock r0 = r1.tryAcquire(r2)     // Catch:{ all -> 0x0023 }
            if (r0 != 0) goto L_0x001c
            r2 = 1
            goto L_0x001f
        L_0x001c:
            com.facebook.common.dextricks.Fs.safeClose(r0)     // Catch:{ all -> 0x0023 }
        L_0x001f:
            r1.close()     // Catch:{ IOException -> 0x002c }
            return r2
        L_0x0023:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0025 }
        L_0x0025:
            r0 = move-exception
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ all -> 0x002b }
        L_0x002b:
            throw r0     // Catch:{ IOException -> 0x002c }
        L_0x002c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.checkAnyOptimizerRunningCurrently():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008b, code lost:
        if (r1 == false) goto L_0x008d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int checkDirty(com.facebook.common.dextricks.OdexScheme r9, java.lang.String[] r10) {
        /*
            r8 = this;
            java.lang.String[] r2 = r9.expectedFiles
            int r1 = r9.flags
            r0 = 2
            r1 = r1 & r0
            r5 = 0
            if (r1 == 0) goto L_0x000a
            r5 = 2
        L_0x000a:
            java.lang.Object r6 = r10.clone()
            java.lang.String[] r6 = (java.lang.String[]) r6
            r8.setDifference(r6, r2)
            r4 = 0
            r7 = 0
        L_0x0015:
            int r0 = r6.length
            r3 = 1
            if (r4 >= r0) goto L_0x004e
            r3 = r6[r4]
            if (r3 == 0) goto L_0x0042
            boolean r0 = com.facebook.common.dextricks.DexStoreUtils.isIgnoreDirtyFileName(r3)
            if (r0 != 0) goto L_0x0042
            java.io.File r1 = r8.root
            java.lang.String r0 = r9.toString()
            java.lang.Object[] r1 = new java.lang.Object[]{r3, r1, r0}
            java.lang.String r0 = "deleting unknown file %s in dex store %s with schema %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            java.io.File r3 = new java.io.File
            java.io.File r1 = r8.root
            r0 = r6[r4]
            r3.<init>(r1, r0)
            com.facebook.common.dextricks.Fs.deleteRecursive(r3)
            r7 = 1
        L_0x003f:
            int r4 = r4 + 1
            goto L_0x0015
        L_0x0042:
            java.io.File r0 = r8.root
            java.lang.Object[] r1 = new java.lang.Object[]{r3, r0}
            java.lang.String r0 = "Ignoring potentially excessive file %s in root: %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            goto L_0x003f
        L_0x004e:
            if (r7 == 0) goto L_0x0061
            int r0 = r9.flags
            r0 = r0 & r3
            if (r0 == 0) goto L_0x0061
            java.io.File r0 = r8.root
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "dex store %s had excess files and is non-incremental: regenerating"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            r5 = 2
        L_0x0061:
            boolean r0 = r8.checkDeps()
            if (r0 != 0) goto L_0x0073
            java.io.File r0 = r8.root
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "dex store %s dependencies have changed: regenerating all"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            r5 = 2
        L_0x0073:
            if (r5 >= r3) goto L_0x00a3
            com.facebook.common.dextricks.ExpectedFileInfo[] r4 = r9.expectedFileInfos
            java.lang.Object r3 = r2.clone()
            java.lang.String[] r3 = (java.lang.String[]) r3
            r8.setDifference(r3, r10)
            r2 = 0
        L_0x0081:
            int r0 = r3.length
            if (r2 >= r0) goto L_0x00a3
            r0 = r4[r2]
            if (r0 == 0) goto L_0x008d
            boolean r1 = r0.mIsOptional
            r0 = 1
            if (r1 != 0) goto L_0x008e
        L_0x008d:
            r0 = 0
        L_0x008e:
            r1 = r3[r2]
            if (r1 == 0) goto L_0x00a0
            if (r0 != 0) goto L_0x00a0
            java.io.File r0 = r8.root
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "missing file %s in dex store %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            r5 = 1
        L_0x00a0:
            int r2 = r2 + 1
            goto L_0x0081
        L_0x00a3:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.checkDirty(com.facebook.common.dextricks.OdexScheme, java.lang.String[]):int");
    }

    private byte determineDesiredState(byte b, DexManifest dexManifest) {
        Object[] objArr;
        String str;
        if (dexManifest.dexes.length == 0) {
            Mlog.safeFmt("no secondary dexes listed: using noop configuration", new Object[0]);
            return STATE_NOOP;
        }
        if (b == 5) {
            objArr = new Object[0];
            str = "recovering from bad class gen: using fallback";
        } else if ("Amazon".equals(Build.BRAND) && !C011108x.A00) {
            objArr = new Object[0];
            str = "avoiding optimizations on non-standard VM";
        } else if (!C011108x.A00) {
            return 3;
        } else {
            if (Build.VERSION.SDK_INT < 21) {
                objArr = new Object[0];
                str = "avoiding optimizations on pre-L VM";
            } else if (canLoadCanaryClass(dexManifest)) {
                return STATE_NOOP;
            } else {
                return STATE_ART_XDEX;
            }
        }
        Mlog.safeFmt(str, objArr);
        return 2;
    }

    private static File determineOdexCacheName(File file) {
        if (C011108x.A00) {
            return null;
        }
        String path = file.getPath();
        if (path.endsWith(".apk")) {
            File file2 = new File(AnonymousClass08S.A0J(path.substring(0, path.length() - 4), DexManifest.ODEX_EXT));
            if (file2.exists()) {
                return file2;
            }
        }
        return Fs.dexOptGenerateCacheFileName(Fs.findSystemDalvikCache(), file, "classes.dex");
    }

    public static synchronized DexStore dexStoreListHead() {
        DexStore dexStore;
        synchronized (DexStore.class) {
            dexStore = sListHead;
        }
        return dexStore;
    }

    public static synchronized DexStore findOpened(File file) {
        synchronized (DexStore.class) {
            File absoluteFile = file.getAbsoluteFile();
            for (DexStore dexStoreListHead = dexStoreListHead(); dexStoreListHead != null; dexStoreListHead = dexStoreListHead.next) {
                if (dexStoreListHead.root.equals(absoluteFile)) {
                    return dexStoreListHead;
                }
            }
            return null;
        }
    }

    public static long getAppUpgradeTimestamp(Context context) {
        if (sCachedLastAppUpdateTime == 0) {
            sCachedLastAppUpdateTime = genAppUpgradeTimestamp(context);
        }
        return sCachedLastAppUpdateTime;
    }

    public static DexStoreClock getClock(Class cls) {
        DexStoreClock dexStoreClock = sDexStoreClock;
        if (dexStoreClock == null) {
            return null;
        }
        return dexStoreClock;
    }

    public static OptimizationHistoryLog getCurrentOptHistoryLogFromRoot(DexStore dexStore, Context context, File file) {
        OptimizationHistoryLog optimizationHistoryLog = dexStore.mCachedOptimizationHistoryLog;
        if (optimizationHistoryLog == null || !optimizationHistoryLog.isLogFileAsFromRoot(file) || dexStore.mCachedOptimizationHistoryLog.isOutOfDate()) {
            OptimizationHistoryLog optimizationHistoryLog2 = dexStore.mCachedOptimizationHistoryLog;
            OptimizationHistoryLog readOrMakeDefaultFromRoot = OptimizationHistoryLog.readOrMakeDefaultFromRoot(context, file);
            dexStore.mCachedOptimizationHistoryLog = readOrMakeDefaultFromRoot;
            Mlog.safeFmt("Reading new history log for (same root: %s out of date: %s) root: %s \n old: %s \n new: %s", Boolean.valueOf(readOrMakeDefaultFromRoot.isLogFileAsFromRoot(file)), Boolean.valueOf(dexStore.mCachedOptimizationHistoryLog.isOutOfDate()), file.getAbsolutePath(), optimizationHistoryLog2, dexStore.mCachedOptimizationHistoryLog);
        }
        return dexStore.mCachedOptimizationHistoryLog;
    }

    private static String getNonSecondaryDexHashes(Context context) {
        ArrayList arrayList = new ArrayList();
        arrayList.add("secondary.dex01.Canary");
        arrayList.add("secondary.dex02.Canary");
        StringBuilder sb = new StringBuilder();
        try {
            for (String[] strArr : DexStoreUtils.getDexInfoFromManifest(context)) {
                String str = strArr[DexStoreUtils.CANARY_IDX];
                String str2 = strArr[DexStoreUtils.HASH_IDX];
                if (!arrayList.contains(str)) {
                    if (sb.length() != 0) {
                        sb.append(",");
                    }
                    sb.append(str2);
                    sb.append(DexManifest.DEX_EXT);
                }
            }
            return sb.toString();
        } catch (IOException unused) {
            return BuildConfig.FLAVOR;
        }
    }

    private static File getOatFileFromDexHash(File file, String str) {
        if (str == null) {
            return null;
        }
        return new File(file, AnonymousClass08S.A0P("prog-", str, ".oat"));
    }

    public static String getStatusDescription(long j) {
        int i = (int) (j & 15);
        StringBuilder sb = new StringBuilder();
        switch (i) {
            case 0:
                sb.append("STATE_INVALID");
                break;
            case 1:
                sb.append("STATE_TX_FAILED");
                break;
            case 2:
                sb.append("STATE_FALLBACK");
                break;
            case 3:
                sb.append("STATE_XDEX");
                break;
            case 4:
                sb.append("STATE_TURBO");
                break;
            case 5:
                sb.append("STATE_BAD_GEN");
                break;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                sb.append("STATE_REGEN_FORCED");
                break;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                sb.append("STATE_ART_TURBO");
                break;
            case 8:
                sb.append("STATE_ART_XDEX");
                break;
            case Process.SIGKILL /*9*/:
                sb.append("STATE_NOOP");
                break;
            default:
                sb.append(AnonymousClass08S.A09("BAD STATE ", i));
                break;
        }
        return sb.toString();
    }

    private static synchronized void installCrossDexHooks() {
        synchronized (DexStore.class) {
            if (sAttemptedCrossDexHookInstallation) {
                Throwable th = sCrossDexHookInstallationError;
                if (th != null) {
                    throw Fs.runtimeExFrom(th);
                }
            } else {
                sAttemptedCrossDexHookInstallation = true;
                try {
                    DalvikInternals.fixDvmForCrossDexHack();
                    Mlog.safeFmt("cross-dex hook installation succeeded", new Object[0]);
                } catch (Throwable th2) {
                    sCrossDexHookInstallationError = th2;
                    throw th2;
                }
            }
        }
    }

    public static long lastModifiedTime(File file) {
        if (file == null || !file.exists()) {
            return 0;
        }
        return file.lastModified();
    }

    private String[] listAndPruneRootFiles(Context context) {
        String[] list = this.root.list();
        if (list != null) {
            AnonymousClass095.A00(context);
            for (int i = 0; i < list.length; i++) {
                String str = list[i];
                if (str.equals(MDEX_LOCK_FILENAME) || str.equals(MDEX_STATUS_FILENAME) || str.equals(ODEX_LOCK_FILENAME) || str.equals(DEPS_FILENAME) || str.equals(REGEN_STAMP_FILENAME) || str.equals(OPTIMIZATION_LOG_FILENAME) || str.equals(OPTIMIZATION_HISTORY_LOG_FILENAME) || str.equals(CONFIG_FILENAME) || str.equals("classmap.bin") || str.equals("classmap.bin.hf") || "art_pgo_ref_profile.prof".equals(str)) {
                    list[i] = null;
                }
                if (str.equals(CONFIG_TMP_FILENAME)) {
                    Fs.deleteRecursive(new File(this.root, str));
                    list[i] = null;
                }
            }
            pruneTemporaryDirectoriesLocked(list);
            return list;
        }
        throw new IOException("unable to list directory " + this.root);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0120, code lost:
        if (com.facebook.common.dextricks.DexStore.sMergedDexConfig.mDexFiles.size() != r2) goto L_0x0122;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadDexFiles(boolean r24, com.facebook.common.dextricks.OdexScheme r25, com.facebook.common.dextricks.DexManifest r26, X.C000000c r27, android.content.Context r28, int r29) {
        /*
            r23 = this;
            r0 = r23
            r4 = r26
            boolean r1 = r4.locators
            r5 = 0
            r13 = 0
            if (r1 == 0) goto L_0x000b
            r13 = 2
        L_0x000b:
            r1 = r29 & 16
            if (r1 == 0) goto L_0x0011
            r13 = r13 | 4
        L_0x0011:
            java.lang.String r2 = "disable_dex_verifier"
            r1 = r28
            boolean r15 = checkAndClearGk(r1, r2)
            java.lang.String r2 = "disable_dex_collision_check"
            boolean r7 = checkAndClearGk(r1, r2)
            java.lang.String r2 = "disable_dex_isuptodate_check"
            boolean r6 = checkAndClearGk(r1, r2)
            java.lang.String r2 = "enable_turbo_loader"
            boolean r11 = checkAndClearGk(r1, r2)
            java.lang.String r2 = "madvise_rand_enabled"
            boolean r10 = checkAndClearGk(r1, r2)
            java.lang.String r2 = "fb4a_enable_io_logging_across_add_dexes"
            boolean r2 = checkAndClearGk(r1, r2)
            com.facebook.common.dextricks.DexStore.logDexAddPageFaults = r2
            java.lang.String r2 = "android_try_to_recover_from_adddex_io_exception"
            boolean r16 = X.AnonymousClass08X.A07(r1, r2)
            r3 = 4
            if (r15 == 0) goto L_0x0047
            int r2 = android.os.Build.VERSION.SDK_INT
            com.facebook.common.dextricks.DalvikInternals.installArtHacks(r3, r2)
        L_0x0047:
            if (r7 == 0) goto L_0x0050
            r3 = 32
            int r2 = android.os.Build.VERSION.SDK_INT
            com.facebook.common.dextricks.DalvikInternals.installArtHacks(r3, r2)
        L_0x0050:
            if (r6 == 0) goto L_0x0059
            r3 = 64
            int r2 = android.os.Build.VERSION.SDK_INT
            com.facebook.common.dextricks.DalvikInternals.installArtHacks(r3, r2)
        L_0x0059:
            if (r10 == 0) goto L_0x0062
            java.lang.String r2 = getNonSecondaryDexHashes(r1)
            com.facebook.common.dextricks.DalvikInternals.hookMmap(r2)
        L_0x0062:
            java.lang.String r2 = "disable_monitor_visitlocks"
            int r3 = X.AnonymousClass08X.A00(r1, r2, r5)
            r2 = 1
            if (r3 == r2) goto L_0x006c
            r2 = 0
        L_0x006c:
            if (r2 == 0) goto L_0x0075
            r3 = 128(0x80, float:1.794E-43)
            int r2 = android.os.Build.VERSION.SDK_INT
            com.facebook.common.dextricks.DalvikInternals.installArtHacks(r3, r2)
        L_0x0075:
            boolean r2 = com.facebook.common.dextricks.DexStore.logDexAddPageFaults
            if (r2 == 0) goto L_0x0085
            X.00Z r6 = X.AnonymousClass00Y.A00()
            long r2 = r6.A03
            com.facebook.common.dextricks.DexStore.majPageFaultsDelta = r2
            long r2 = r6.A02
            com.facebook.common.dextricks.DexStore.pageInBytesDelta = r2
        L_0x0085:
            com.facebook.common.dextricks.MultiDexClassLoader$Configuration r12 = new com.facebook.common.dextricks.MultiDexClassLoader$Configuration
            r14 = 1000(0x3e8, float:1.401E-42)
            X.00K r17 = X.AnonymousClass00J.A01(r1)
            r12.<init>(r13, r14, r15, r16, r17)
            java.util.ArrayList r2 = r12.mDexFiles
            int r2 = r2.size()
            java.io.File r6 = r0.root
            r3 = r25
            r3.configureClassLoader(r6, r12)
            com.facebook.common.dextricks.MultiDexClassLoader$Configuration r6 = com.facebook.common.dextricks.DexStore.sMergedDexConfig
            if (r6 != 0) goto L_0x00b4
            com.facebook.common.dextricks.MultiDexClassLoader$Configuration r17 = new com.facebook.common.dextricks.MultiDexClassLoader$Configuration
            r18 = 0
            r19 = 1000(0x3e8, float:1.401E-42)
            X.00K r22 = X.AnonymousClass00J.A01(r1)
            r20 = r15
            r21 = r16
            r17.<init>(r18, r19, r20, r21, r22)
            com.facebook.common.dextricks.DexStore.sMergedDexConfig = r17
        L_0x00b4:
            mergeConfiguration(r12, r4)
            java.util.ArrayList r8 = r0.primaryDexes     // Catch:{ all -> 0x0112 }
            java.util.ArrayList r7 = r0.auxiliaryDexes     // Catch:{ all -> 0x0112 }
            X.00K r6 = X.AnonymousClass00J.A01(r1)     // Catch:{ all -> 0x0112 }
            java.lang.ClassLoader r9 = com.facebook.common.dextricks.MultiDexClassLoader.install(r1, r8, r7, r6)     // Catch:{ all -> 0x0112 }
            if (r11 == 0) goto L_0x00e4
            boolean r6 = X.AnonymousClass0MP.A00     // Catch:{ all -> 0x0112 }
            if (r6 == 0) goto L_0x00e4
            java.lang.Class r7 = r9.getClass()     // Catch:{ all -> 0x0112 }
            java.lang.Class<com.facebook.common.dextricks.MultiDexClassLoaderArtNative> r6 = com.facebook.common.dextricks.MultiDexClassLoaderArtNative.class
            if (r7 != r6) goto L_0x00e4
            r11 = r9
            com.facebook.common.dextricks.MultiDexClassLoaderArtNative r11 = (com.facebook.common.dextricks.MultiDexClassLoaderArtNative) r11     // Catch:{ all -> 0x0112 }
            java.util.ArrayList r8 = r0.primaryDexes     // Catch:{ all -> 0x0112 }
            java.util.ArrayList r7 = r0.auxiliaryDexes     // Catch:{ all -> 0x0112 }
            com.facebook.common.dextricks.MultiDexClassLoader$Configuration r15 = com.facebook.common.dextricks.DexStore.sMergedDexConfig     // Catch:{ all -> 0x0112 }
            java.io.File r6 = r0.root     // Catch:{ all -> 0x0112 }
            r12 = r1
            r13 = r8
            r14 = r7
            r16 = r6
            r11.configureTurboLoader(r12, r13, r14, r15, r16)     // Catch:{ all -> 0x0112 }
        L_0x00e4:
            boolean r1 = r9 instanceof com.facebook.common.dextricks.MultiDexClassLoader     // Catch:{ all -> 0x0112 }
            if (r1 == 0) goto L_0x00ef
            com.facebook.common.dextricks.MultiDexClassLoader r9 = (com.facebook.common.dextricks.MultiDexClassLoader) r9     // Catch:{ all -> 0x0112 }
            com.facebook.common.dextricks.MultiDexClassLoader$Configuration r1 = com.facebook.common.dextricks.DexStore.sMergedDexConfig     // Catch:{ all -> 0x0112 }
            r9.configure(r1)     // Catch:{ all -> 0x0112 }
        L_0x00ef:
            if (r24 == 0) goto L_0x00f4
            r0.verifyCanaryClasses(r4)     // Catch:{ all -> 0x0112 }
        L_0x00f4:
            r0.mLoadedManifest = r4     // Catch:{ all -> 0x0112 }
            boolean r0 = com.facebook.common.dextricks.DexStore.logDexAddPageFaults
            if (r0 == 0) goto L_0x010c
            X.00Z r4 = X.AnonymousClass00Y.A00()
            long r2 = r4.A03
            long r0 = com.facebook.common.dextricks.DexStore.majPageFaultsDelta
            long r2 = r2 - r0
            com.facebook.common.dextricks.DexStore.majPageFaultsDelta = r2
            long r2 = r4.A02
            long r0 = com.facebook.common.dextricks.DexStore.pageInBytesDelta
            long r2 = r2 - r0
            com.facebook.common.dextricks.DexStore.pageInBytesDelta = r2
        L_0x010c:
            if (r10 == 0) goto L_0x0111
            com.facebook.common.dextricks.DalvikInternals.unhookMmap()
        L_0x0111:
            return
        L_0x0112:
            r4 = move-exception
            r1 = r29 & 2
            if (r1 != 0) goto L_0x0122
            com.facebook.common.dextricks.MultiDexClassLoader$Configuration r1 = com.facebook.common.dextricks.DexStore.sMergedDexConfig
            java.util.ArrayList r1 = r1.mDexFiles
            int r1 = r1.size()
            r7 = 0
            if (r1 == r2) goto L_0x0123
        L_0x0122:
            r7 = 1
        L_0x0123:
            if (r7 == 0) goto L_0x014b
            java.lang.String r6 = "fatal"
        L_0x0127:
            java.io.File r2 = r0.root
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r24)
            java.lang.Object[] r2 = new java.lang.Object[]{r6, r2, r3, r1}
            java.lang.String r1 = "%s error in store %s scheme %s regen %s"
            com.facebook.common.dextricks.Mlog.e(r4, r1, r2)
            if (r24 == 0) goto L_0x0145
            r1 = 5
            r0.writeStatusLocked(r1)
        L_0x013d:
            if (r7 == 0) goto L_0x014e
            com.facebook.common.dextricks.FatalDexError r0 = new com.facebook.common.dextricks.FatalDexError
            r0.<init>(r4)
            throw r0
        L_0x0145:
            r1 = 0
            r0.writeStatusLocked(r1)
            goto L_0x013d
        L_0x014b:
            java.lang.String r6 = "recoverable"
            goto L_0x0127
        L_0x014e:
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r0 = "retrying dex store load after reset"
            com.facebook.common.dextricks.Mlog.w(r0, r1)
            com.facebook.common.dextricks.DexStore$RecoverableDexException r0 = new com.facebook.common.dextricks.DexStore$RecoverableDexException
            r0.<init>(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.loadDexFiles(boolean, com.facebook.common.dextricks.OdexScheme, com.facebook.common.dextricks.DexManifest, X.00c, android.content.Context, int):void");
    }

    private static void logNonOptimalScheme(OdexScheme odexScheme) {
        boolean z = false;
        if (AnonymousClass018.A01) {
            z = true;
        }
        if (z) {
            Mlog.w("In ct-scan mode, scheme is not optimal: %s", odexScheme.getSchemeName());
        }
    }

    private static void logSchemeNeedsOptimization(OdexScheme odexScheme, long j) {
        boolean z = false;
        if (AnonymousClass018.A01) {
            z = true;
        }
        if (z) {
            Mlog.w("In ct-scan mode, scheme %s requires further optimization. Status: 0x%s", odexScheme.getSchemeName(), Long.toHexString(j));
        }
    }

    private static void mergeConfiguration(MultiDexClassLoader.Configuration configuration, DexManifest dexManifest) {
        sMergedDexConfig.mDexFiles.size();
        sMergedDexConfig.configFlags |= configuration.configFlags;
        Iterator it = configuration.mDexFiles.iterator();
        while (it.hasNext()) {
            sMergedDexConfig.addDex((DexFile) it.next());
        }
        configuration.mDexFiles.clear();
    }

    public static long nowTimestamp() {
        return sDexStoreClock.now();
    }

    private static boolean nukeIfUnexpected(File file, String str, HashSet hashSet) {
        if (str.startsWith("prog-")) {
            String[] split = str.split("\\.", 2);
            if (!hashSet.contains(split[0])) {
                Mlog.safeFmt("Found possible stale %s file, blowing it up: %s", split[1], str);
                Fs.deleteRecursiveNoThrow(new File(file, str));
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void optimizeInForegroundLocked(android.content.Context r22, com.facebook.common.dextricks.DexManifest r23, long r24) {
        /*
            r21 = this;
            r4 = r21
            r4.assertLockHeld()
            r3 = r22
            r0 = r24
            r2 = r23
            com.facebook.common.dextricks.OdexScheme r2 = r4.schemeForState(r3, r2, r0)
            com.facebook.common.dextricks.Prio r11 = com.facebook.common.dextricks.Prio.unchanged()
            com.facebook.common.dextricks.OptimizationConfiguration r10 = new com.facebook.common.dextricks.OptimizationConfiguration
            r12 = 0
            r13 = 1000(0x3e8, float:1.401E-42)
            r14 = 0
            r15 = 100
            r16 = 3600000(0x36ee80, float:5.044674E-39)
            r17 = 10
            r18 = 419430400(0x19000000, double:2.072261515E-315)
            r20 = 0
            r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r20)
            com.facebook.common.dextricks.OptimizationConfiguration$Provider r9 = new com.facebook.common.dextricks.OptimizationConfiguration$Provider
            r9.<init>(r10)
            com.facebook.common.dextricks.DexStore$DexStoreClock r0 = com.facebook.common.dextricks.DexStore.sDexStoreClock
            long r7 = r0.now()
            long r5 = r4.getNextRecommendedOptimizationAttemptTime(r10)
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0043
            java.lang.Object[] r1 = new java.lang.Object[r12]
            java.lang.String r0 = "... actually, not optimizing in foreground, since we failed optimization too recently"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return
        L_0x0043:
            com.facebook.common.dextricks.DexStore$OptimizationSession r1 = new com.facebook.common.dextricks.DexStore$OptimizationSession     // Catch:{ InterruptedException -> 0x0068, all -> 0x005f }
            r0 = 1
            r1.<init>(r3, r9, r0)     // Catch:{ InterruptedException -> 0x0068, all -> 0x005f }
            r2.optimize(r3, r4, r1)     // Catch:{ all -> 0x0053 }
            r1.noteOptimizationSuccess()     // Catch:{ all -> 0x0058 }
            r1.close()     // Catch:{ InterruptedException -> 0x0068, all -> 0x005f }
            return
        L_0x0053:
            r0 = move-exception
            r1.copeWithOptimizationFailure(r0)     // Catch:{ all -> 0x0058 }
            throw r0     // Catch:{ all -> 0x0058 }
        L_0x0058:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005a }
        L_0x005a:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x005e }
        L_0x005e:
            throw r0     // Catch:{ InterruptedException -> 0x0068, all -> 0x005f }
        L_0x005f:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[r12]
            java.lang.String r0 = "foreground optimization failed; proceeding"
            com.facebook.common.dextricks.Mlog.w(r2, r0, r1)
            return
        L_0x0068:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.optimizeInForegroundLocked(android.content.Context, com.facebook.common.dextricks.DexManifest, long):void");
    }

    private byte[] readSavedDepBlock() {
        File file = new File(this.root, DEPS_FILENAME);
        if (!file.exists()) {
            return null;
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            try {
                long length = randomAccessFile.length();
                if (length > 16777216) {
                    Mlog.safeFmt("saved dep block file is way too big (%s bytes): considering invalid", Long.valueOf(length));
                } else {
                    byte[] bArr = new byte[((int) length)];
                    int read = randomAccessFile.read(bArr);
                    if (((long) read) < length) {
                        Mlog.safeFmt("short read of dep block %s: wanted %s bytes; got %s: considering invalid", file, Long.valueOf(length), Integer.valueOf(read));
                    } else {
                        Mlog.safeFmt("read saved dep file %s (%s bytes)", file, Long.valueOf(length));
                        Fs.safeClose(randomAccessFile);
                        return bArr;
                    }
                }
                return null;
            } finally {
                Fs.safeClose(randomAccessFile);
            }
        } catch (FileNotFoundException e) {
            Mlog.w(e, "unable to open deps file %s", file);
            return null;
        }
    }

    public static long sanityCheckTimestamp(long j) {
        if (j > sDexStoreClock.now()) {
            return 0;
        }
        return j;
    }

    private OdexScheme schemeForState(Context context, DexManifest dexManifest, long j) {
        DexManifest.Dex[] dexArr = dexManifest.dexes;
        long j2 = j;
        byte b = (byte) ((int) (15 & j));
        if (b == 2) {
            return new OdexSchemeBoring(dexArr);
        }
        if (b == 3) {
            return new OdexSchemeXdex(dexArr);
        }
        if (b == 4) {
            return new OdexSchemeTurbo(dexArr);
        }
        if (b == 7) {
            return new OdexSchemeArtTurbo(dexArr);
        }
        if (b == 8) {
            return new OdexSchemeArtXdex(context, dexArr, this.mResProvider, j2);
        } else if (b != 9) {
            return new OdexSchemeInvalid(j);
        } else {
            return new OdexSchemeNoop();
        }
    }

    private void touchRegenStamp() {
        File file = new File(this.root, REGEN_STAMP_FILENAME);
        file.createNewFile();
        if (!file.setLastModified(sDexStoreClock.now())) {
            throw new IOException("could not set modtime of " + file);
        }
    }

    public void addChild(DexStore dexStore) {
        if (!this.mChildStores.contains(dexStore)) {
            this.mChildStores.add(dexStore);
        }
    }

    public boolean attemptedOptimizationSinceRegeneration() {
        return new File(this.root, OPTIMIZATION_LOG_FILENAME).exists();
    }

    public String findDexHashForCanaryClass(String str) {
        DexManifest dexManifest = this.mLoadedManifest;
        if (dexManifest != null) {
            for (DexManifest.Dex dex : dexManifest.dexes) {
                if (dex.canaryClass.equals(str)) {
                    return dex.hash;
                }
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        if (r2 != null) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void forceRegenerateOnNextLoad() {
        /*
            r3 = this;
            com.facebook.common.dextricks.ReentrantLockFile r1 = r3.mLockFile
            r0 = 0
            com.facebook.common.dextricks.ReentrantLockFile$Lock r2 = r1.acquire(r0)
            r0 = 6
            r3.writeStatusLocked(r0)     // Catch:{ all -> 0x0012 }
            if (r2 == 0) goto L_0x0011
            r2.close()
        L_0x0011:
            return
        L_0x0012:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0014 }
        L_0x0014:
            r0 = move-exception
            if (r2 == 0) goto L_0x001a
            r2.close()     // Catch:{ all -> 0x001a }
        L_0x001a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.forceRegenerateOnNextLoad():void");
    }

    public List getAllOatFiles(File file) {
        ArrayList arrayList = new ArrayList(r3);
        for (DexManifest.Dex dex : this.mLoadedManifest.dexes) {
            File oatFileFromDexHash = getOatFileFromDexHash(file, dex.hash);
            if (oatFileFromDexHash != null && oatFileFromDexHash.exists()) {
                arrayList.add(oatFileFromDexHash);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0032, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0033, code lost:
        if (r2 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0038, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.common.dextricks.DexStore.OptimizationLog getAndClearCompletedOptimizationLog() {
        /*
            r5 = this;
            java.io.File r4 = new java.io.File
            java.io.File r1 = r5.root
            java.lang.String r0 = "optimization_log"
            r4.<init>(r1, r0)
            boolean r0 = r4.exists()
            r3 = 0
            if (r0 == 0) goto L_0x0039
            com.facebook.common.dextricks.ReentrantLockFile r1 = r5.mLockFile
            r0 = 0
            com.facebook.common.dextricks.ReentrantLockFile$Lock r2 = r1.acquire(r0)
            com.facebook.common.dextricks.DexStore$OptimizationLog r1 = com.facebook.common.dextricks.DexStore.OptimizationLog.readOrMakeDefault(r4)     // Catch:{ all -> 0x0030 }
            int r0 = r1.flags     // Catch:{ all -> 0x0030 }
            r0 = r0 & 1
            if (r0 != 0) goto L_0x0027
            if (r2 == 0) goto L_0x0039
            r2.close()
            return r3
        L_0x0027:
            r4.delete()     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x002f
            r2.close()
        L_0x002f:
            return r1
        L_0x0030:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0032 }
        L_0x0032:
            r0 = move-exception
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch:{ all -> 0x0038 }
        L_0x0038:
            throw r0
        L_0x0039:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.getAndClearCompletedOptimizationLog():com.facebook.common.dextricks.DexStore$OptimizationLog");
    }

    public File[] getDependencyOdexFiles() {
        ArrayList arrayList = new ArrayList();
        File file = this.mApk;
        arrayList.add(file);
        arrayList.add(determineOdexCacheName(file));
        for (DexStore dependencyOdexFiles : getParents()) {
            for (File add : dependencyOdexFiles.getDependencyOdexFiles()) {
                arrayList.add(add);
            }
        }
        File[] fileArr = new File[arrayList.size()];
        arrayList.toArray(fileArr);
        return fileArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e9, code lost:
        if (r9 != null) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ee, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map getDiagnostics(android.content.Context r11) {
        /*
            r10 = this;
            java.util.LinkedHashMap r5 = new java.util.LinkedHashMap
            r5.<init>()
            long r0 = r10.reportStatus()
            java.io.File r2 = r10.root
            com.facebook.common.dextricks.DexStore$OptimizationHistoryLog r6 = getCurrentOptHistoryLogFromRoot(r10, r11, r2)
            com.facebook.common.dextricks.DexStore$Config r7 = r10.readConfig()
            com.facebook.common.dextricks.DexManifest r2 = r10.loadManifest()
            com.facebook.common.dextricks.OdexScheme r4 = r10.schemeForState(r11, r2, r0)
            boolean r2 = r4.loadNotOptimized(r0)
            java.lang.String r3 = java.lang.Boolean.toString(r2)
            java.lang.String r2 = "loadNotOptimized"
            r5.put(r2, r3)
            com.facebook.common.dextricks.OdexScheme$NeedOptimizationState r2 = r4.needOptimization(r0, r7, r6)
            java.lang.String r3 = r2.name()
            java.lang.String r2 = "needOptimization"
            r5.put(r2, r3)
            java.lang.String r3 = getStatusDescription(r0)
            java.lang.String r2 = "scheme"
            r5.put(r2, r3)
            java.lang.String r1 = java.lang.Long.toHexString(r0)
            java.lang.String r0 = "status"
            r5.put(r0, r1)
            com.facebook.common.dextricks.ReentrantLockFile r1 = r10.mLockFile
            r0 = 0
            com.facebook.common.dextricks.ReentrantLockFile$Lock r9 = r1.acquire(r0)
            com.facebook.common.dextricks.DexStore$DexStoreClock r0 = com.facebook.common.dextricks.DexStore.sDexStoreClock     // Catch:{ all -> 0x00e6 }
            long r3 = r0.now()     // Catch:{ all -> 0x00e6 }
            java.io.File r0 = r10.root     // Catch:{ all -> 0x00e6 }
            com.facebook.common.dextricks.DexStore$OptimizationLog r2 = com.facebook.common.dextricks.DexStore.OptimizationLog.readOrMakeDefaultFromRoot(r0)     // Catch:{ all -> 0x00e6 }
            if (r2 == 0) goto L_0x0088
            boolean r0 = r2.isNotDefault     // Catch:{ all -> 0x00e6 }
            if (r0 == 0) goto L_0x0088
            java.lang.String r1 = "optlog.flags"
            int r0 = r2.flags     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r1, r0)     // Catch:{ all -> 0x00e6 }
            java.lang.String r1 = "optlog.nrOptimizationsAttempted"
            int r0 = r2.nrOptimizationsAttempted     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r1, r0)     // Catch:{ all -> 0x00e6 }
            java.lang.String r1 = "optlog.nrOptimizationsFailed"
            int r0 = r2.nrOptimizationsFailed     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r1, r0)     // Catch:{ all -> 0x00e6 }
            java.lang.String r1 = "optlog.lastFailureExceptionJson"
            java.lang.String r0 = r2.lastFailureExceptionJson     // Catch:{ all -> 0x00e6 }
            r5.put(r1, r0)     // Catch:{ all -> 0x00e6 }
        L_0x0088:
            if (r7 == 0) goto L_0x00c0
            java.lang.String r1 = "config.enablePgoCompile"
            boolean r0 = r7.tryPeriodicPgoCompilation     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = java.lang.Boolean.toString(r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r1, r0)     // Catch:{ all -> 0x00e6 }
            boolean r0 = r7.tryPeriodicPgoCompilation     // Catch:{ all -> 0x00e6 }
            if (r0 == 0) goto L_0x00c0
            java.lang.String r2 = "config.minPgoDuration"
            long r0 = r7.minTimeBetweenPgoCompilationMs     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r2, r0)     // Catch:{ all -> 0x00e6 }
            java.lang.String r2 = "config.timeleft"
            if (r6 == 0) goto L_0x00bb
            boolean r0 = r6.isNotDefault()     // Catch:{ all -> 0x00e6 }
            if (r0 == 0) goto L_0x00bb
            long r0 = r6.lastSuccessfulOptimizationTimestampMs     // Catch:{ all -> 0x00e6 }
            long r7 = r7.minTimeBetweenPgoCompilationMs     // Catch:{ all -> 0x00e6 }
            long r0 = r0 + r7
            java.lang.String r0 = printRelativeTime(r3, r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r2, r0)     // Catch:{ all -> 0x00e6 }
            goto L_0x00c0
        L_0x00bb:
            java.lang.String r0 = "<no info>"
            r5.put(r2, r0)     // Catch:{ all -> 0x00e6 }
        L_0x00c0:
            if (r6 == 0) goto L_0x00e0
            boolean r0 = r6.isNotDefault()     // Catch:{ all -> 0x00e6 }
            if (r0 == 0) goto L_0x00e0
            java.lang.String r1 = "opthistlog.lastSuccess"
            boolean r0 = r6.lastCompilationSessionWasASuccess()     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = java.lang.Boolean.toString(r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r1, r0)     // Catch:{ all -> 0x00e6 }
            java.lang.String r2 = "opthistlog.lastCompilationTimestamp"
            long r0 = r6.lastSuccessfulOptimizationTimestampMs     // Catch:{ all -> 0x00e6 }
            java.lang.String r0 = printRelativeTime(r3, r0)     // Catch:{ all -> 0x00e6 }
            r5.put(r2, r0)     // Catch:{ all -> 0x00e6 }
        L_0x00e0:
            if (r9 == 0) goto L_0x00e5
            r9.close()
        L_0x00e5:
            return r5
        L_0x00e6:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00e8 }
        L_0x00e8:
            r0 = move-exception
            if (r9 == 0) goto L_0x00ee
            r9.close()     // Catch:{ all -> 0x00ee }
        L_0x00ee:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.getDiagnostics(android.content.Context):java.util.Map");
    }

    public long getNextRecommendedOptimizationAttemptTime(OptimizationConfiguration optimizationConfiguration) {
        long lastModified = new File(this.root, OPTIMIZATION_LOG_FILENAME).lastModified();
        if (lastModified > sDexStoreClock.now()) {
            Mlog.w("ignoring optimization log file from the future", new Object[0]);
            lastModified = 0;
        }
        if (lastModified > 0) {
            return ((long) optimizationConfiguration.timeBetweenOptimizationAttemptsMs) + lastModified;
        }
        return 0;
    }

    public String getOdexCachePath() {
        if (determineOdexCacheName(this.mApk) != null) {
            return determineOdexCacheName(this.mApk).getAbsolutePath();
        }
        return null;
    }

    public boolean hasChildren() {
        return this.mChildStores.isEmpty();
    }

    public DexManifest loadManifest() {
        if (this.mManifest == null) {
            synchronized (this) {
                if (this.mManifest == null) {
                    DexManifest loadManifestFrom = DexManifest.loadManifestFrom(this.mResProvider, DexStoreUtils.SECONDARY_DEX_MANIFEST, true);
                    this.id = loadManifestFrom.id;
                    this.mManifest = loadManifestFrom;
                }
            }
        }
        return this.mManifest;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007e, code lost:
        if (r9 != null) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0083, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.common.dextricks.DexStore.TmpDir makeTemporaryDirectory(java.lang.String r11) {
        /*
            r10 = this;
            com.facebook.common.dextricks.ReentrantLockFile r1 = r10.mLockFile
            r0 = 0
            com.facebook.common.dextricks.ReentrantLockFile$Lock r9 = r1.acquire(r0)
            r8 = 0
            java.lang.String r1 = ".tmpdir_lock"
            java.io.File r0 = r10.root     // Catch:{ all -> 0x006a }
            java.io.File r7 = java.io.File.createTempFile(r11, r1, r0)     // Catch:{ all -> 0x006a }
            java.io.File r6 = new java.io.File     // Catch:{ all -> 0x0067 }
            java.io.File r2 = r10.root     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = r7.getName()     // Catch:{ all -> 0x0067 }
            java.lang.String r1 = com.facebook.common.dextricks.Fs.stripLastExtension(r0)     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = ".tmpdir"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0067 }
            r6.<init>(r2, r0)     // Catch:{ all -> 0x0067 }
            com.facebook.common.dextricks.Fs.mkdirOrThrow(r6)     // Catch:{ all -> 0x0067 }
            com.facebook.common.dextricks.ReentrantLockFile r5 = com.facebook.common.dextricks.ReentrantLockFile.open(r7)     // Catch:{ all -> 0x0064 }
            r0 = 1
            com.facebook.common.dextricks.ReentrantLockFile$Lock r4 = r5.tryAcquire(r0)     // Catch:{ all -> 0x0062 }
            if (r4 == 0) goto L_0x0057
            com.facebook.common.dextricks.DexStore$TmpDir r3 = new com.facebook.common.dextricks.DexStore$TmpDir     // Catch:{ all -> 0x005f }
            r3.<init>(r4, r6)     // Catch:{ all -> 0x005f }
            java.lang.String r2 = "created tmpdir %s (lock file %s)"
            java.io.File r1 = r3.directory     // Catch:{ all -> 0x005f }
            java.io.File r0 = r5.lockFileName     // Catch:{ all -> 0x005f }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0}     // Catch:{ all -> 0x005f }
            com.facebook.common.dextricks.Mlog.safeFmt(r2, r0)     // Catch:{ all -> 0x005f }
            com.facebook.common.dextricks.Fs.safeClose(r8)     // Catch:{ all -> 0x007b }
            com.facebook.common.dextricks.Fs.safeClose(r8)     // Catch:{ all -> 0x007b }
            com.facebook.common.dextricks.Fs.deleteRecursiveNoThrow(r8)     // Catch:{ all -> 0x007b }
            com.facebook.common.dextricks.Fs.deleteRecursiveNoThrow(r8)     // Catch:{ all -> 0x007b }
            if (r9 == 0) goto L_0x0056
            r9.close()
        L_0x0056:
            return r3
        L_0x0057:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x005f }
            java.lang.String r0 = "should have been able to acquire tmpdir lock"
            r1.<init>(r0)     // Catch:{ all -> 0x005f }
            throw r1     // Catch:{ all -> 0x005f }
        L_0x005f:
            r0 = move-exception
            r8 = r4
            goto L_0x006e
        L_0x0062:
            r0 = move-exception
            goto L_0x006e
        L_0x0064:
            r0 = move-exception
            r5 = r8
            goto L_0x006e
        L_0x0067:
            r0 = move-exception
            r6 = r8
            goto L_0x006d
        L_0x006a:
            r0 = move-exception
            r7 = r8
            r6 = r8
        L_0x006d:
            r5 = r8
        L_0x006e:
            com.facebook.common.dextricks.Fs.safeClose(r8)     // Catch:{ all -> 0x007b }
            com.facebook.common.dextricks.Fs.safeClose(r5)     // Catch:{ all -> 0x007b }
            com.facebook.common.dextricks.Fs.deleteRecursiveNoThrow(r7)     // Catch:{ all -> 0x007b }
            com.facebook.common.dextricks.Fs.deleteRecursiveNoThrow(r6)     // Catch:{ all -> 0x007b }
            throw r0     // Catch:{ all -> 0x007b }
        L_0x007b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007d }
        L_0x007d:
            r0 = move-exception
            if (r9 == 0) goto L_0x0083
            r9.close()     // Catch:{ all -> 0x0083 }
        L_0x0083:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.makeTemporaryDirectory(java.lang.String):com.facebook.common.dextricks.DexStore$TmpDir");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0060, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x0064 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void optimize(android.content.Context r7, com.facebook.common.dextricks.OptimizationConfiguration.Provider r8) {
        /*
            r6 = this;
            com.facebook.common.dextricks.DexManifest r2 = r6.mLoadedManifest
            if (r2 != 0) goto L_0x0008
            com.facebook.common.dextricks.DexManifest r2 = r6.loadManifest()
        L_0x0008:
            r4 = 0
            java.lang.Object[] r1 = new java.lang.Object[r4]
            java.lang.String r0 = "[opt] loaded manifets"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            com.facebook.common.dextricks.ReentrantLockFile r0 = r6.mLockFile
            com.facebook.common.dextricks.ReentrantLockFile$Lock r5 = r0.acquireInterruptubly(r4)
            java.io.File r0 = r6.root
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "[opt] locked dex store %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            long r0 = readStatusLocked(r6)     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            com.facebook.common.dextricks.OdexScheme r3 = r6.schemeForState(r7, r2, r0)     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            java.lang.String r1 = "[opt] found scheme %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r3}     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            boolean r0 = r6.checkDeps()     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            if (r0 == 0) goto L_0x0065
            com.facebook.common.dextricks.DexStore$OptimizationSession r2 = new com.facebook.common.dextricks.DexStore$OptimizationSession     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            r2.<init>(r7, r8, r4)     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            java.lang.String r1 = "[opt] opened optimization session"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x005e }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x005e }
            r5.close()     // Catch:{ all -> 0x005e }
            r5 = 0
            r3.optimize(r7, r6, r2)     // Catch:{ all -> 0x0059 }
            r2.noteOptimizationSuccess()     // Catch:{ all -> 0x005e }
            java.lang.String r1 = "[opt] finished optimization session"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x005e }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x005e }
            r2.close()     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            return
        L_0x0059:
            r0 = move-exception
            r2.copeWithOptimizationFailure(r0)     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0064 }
        L_0x0064:
            throw r0     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
        L_0x0065:
            com.facebook.common.dextricks.DexStore$OptimizationCanceledException r1 = new com.facebook.common.dextricks.DexStore$OptimizationCanceledException     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            java.lang.String r0 = "attempt to optimize stale repository"
            r1.<init>(r0)     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
            throw r1     // Catch:{ InterruptedException -> 0x0078, all -> 0x006d }
        L_0x006d:
            r2 = move-exception
            java.lang.String r1 = "[opt] optimization failed!"
            java.lang.Object[] r0 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x007a }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x007a }
            throw r2     // Catch:{ all -> 0x007a }
        L_0x0078:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007a }
        L_0x007a:
            r0 = move-exception
            if (r5 == 0) goto L_0x0080
            r5.close()
        L_0x0080:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.optimize(android.content.Context, com.facebook.common.dextricks.OptimizationConfiguration$Provider):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r3 != null) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void pruneTemporaryDirectories() {
        /*
            r4 = this;
            com.facebook.common.dextricks.ReentrantLockFile r1 = r4.mLockFile
            r0 = 0
            com.facebook.common.dextricks.ReentrantLockFile$Lock r3 = r1.acquire(r0)
            java.io.File r0 = r4.root     // Catch:{ all -> 0x0031 }
            java.lang.String[] r0 = r0.list()     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x0018
            r4.pruneTemporaryDirectoriesLocked(r0)     // Catch:{ all -> 0x0031 }
            if (r3 == 0) goto L_0x0017
            r3.close()
        L_0x0017:
            return
        L_0x0018:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x0031 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0031 }
            r1.<init>()     // Catch:{ all -> 0x0031 }
            java.lang.String r0 = "unable to list directory "
            r1.append(r0)     // Catch:{ all -> 0x0031 }
            java.io.File r0 = r4.root     // Catch:{ all -> 0x0031 }
            r1.append(r0)     // Catch:{ all -> 0x0031 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0031 }
            r2.<init>(r0)     // Catch:{ all -> 0x0031 }
            throw r2     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r0 = move-exception
            if (r3 == 0) goto L_0x0039
            r3.close()     // Catch:{ all -> 0x0039 }
        L_0x0039:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.pruneTemporaryDirectories():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0050, code lost:
        if (r4 != null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0055, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.common.dextricks.DexStore.Config readConfig() {
        /*
            r6 = this;
            com.facebook.common.dextricks.ReentrantLockFile r0 = r6.mLockFile
            r5 = 0
            com.facebook.common.dextricks.ReentrantLockFile$Lock r4 = r0.acquire(r5)
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x004d }
            java.io.File r1 = r6.root     // Catch:{ all -> 0x004d }
            java.lang.String r0 = "config"
            r3.<init>(r1, r0)     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.DexStore$Config r0 = com.facebook.common.dextricks.DexStore.Config.read(r3)     // Catch:{ FileNotFoundException -> 0x003e, UnsupportedOperationException -> 0x002a, Exception -> 0x0015 }
            goto L_0x0047
        L_0x0015:
            r2 = move-exception
            java.lang.String r1 = "error reading dex store config file %s: deleting and proceeding"
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.Mlog.w(r2, r1, r0)     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.Fs.deleteRecursive(r3)     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.DexStore$Config$Builder r0 = new com.facebook.common.dextricks.DexStore$Config$Builder     // Catch:{ all -> 0x004d }
            r0.<init>()     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.DexStore$Config r0 = r0.build()     // Catch:{ all -> 0x004d }
            goto L_0x0047
        L_0x002a:
            java.lang.String r1 = "unsupported dex store config file %s: ignoring and deleting"
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.Fs.deleteRecursive(r3)     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.DexStore$Config$Builder r0 = new com.facebook.common.dextricks.DexStore$Config$Builder     // Catch:{ all -> 0x004d }
            r0.<init>()     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.DexStore$Config r0 = r0.build()     // Catch:{ all -> 0x004d }
            goto L_0x0047
        L_0x003e:
            com.facebook.common.dextricks.DexStore$Config$Builder r0 = new com.facebook.common.dextricks.DexStore$Config$Builder     // Catch:{ all -> 0x004d }
            r0.<init>()     // Catch:{ all -> 0x004d }
            com.facebook.common.dextricks.DexStore$Config r0 = r0.build()     // Catch:{ all -> 0x004d }
        L_0x0047:
            if (r4 == 0) goto L_0x004c
            r4.close()
        L_0x004c:
            return r0
        L_0x004d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004f }
        L_0x004f:
            r0 = move-exception
            if (r4 == 0) goto L_0x0055
            r4.close()     // Catch:{ all -> 0x0055 }
        L_0x0055:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.readConfig():com.facebook.common.dextricks.DexStore$Config");
    }

    public void setResProvider(ResProvider resProvider) {
        this.mResProvider = resProvider;
        this.mDexIteratorFactory = new DexIteratorFactory(resProvider);
    }

    private DexStore(File file, File file2, ResProvider resProvider, ArrayList arrayList, ArrayList arrayList2) {
        this.mApk = file2;
        this.root = file;
        Fs.mkdirOrThrow(file);
        this.mLockFile = ReentrantLockFile.open(new File(file, MDEX_LOCK_FILENAME));
        this.mResProvider = resProvider;
        this.mDexIteratorFactory = new DexIteratorFactory(resProvider);
        this.primaryDexes = arrayList;
        this.auxiliaryDexes = arrayList2;
    }

    private boolean checkDeps() {
        byte[] readCurrentDepBlock = readCurrentDepBlock();
        byte[] readSavedDepBlock = readSavedDepBlock();
        if (readSavedDepBlock == null || !Arrays.equals(readCurrentDepBlock, readSavedDepBlock)) {
            return false;
        }
        return true;
    }

    private static boolean checkGk(Context context, String str) {
        return AnonymousClass08X.A07(context, str);
    }

    private static long genAppUpgradeTimestamp(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 128).lastUpdateTime;
        } catch (PackageManager.NameNotFoundException | RuntimeException e) {
            Mlog.w(e, "Cannot get our app last update time", new Object[0]);
            return 0;
        }
    }

    public static DexStoreTestHooks getDexStoreTestHooks() {
        return sDexStoreTestHooks;
    }

    private DexStore[] getParents() {
        boolean z;
        loadManifest();
        if (this.mParentStores.isEmpty()) {
            DexManifest dexManifest = this.mManifest;
            if (!"dex".equals(dexManifest.id)) {
                for (String str : dexManifest.requires) {
                    if (!"dex".equals(str)) {
                        DexStore dexStoreListHead = dexStoreListHead();
                        while (true) {
                            if (dexStoreListHead != null) {
                                String str2 = dexStoreListHead.id;
                                if (str2 != null && str2.equals(str)) {
                                    this.mParentStores.add(dexStoreListHead);
                                    dexStoreListHead.addChild(this);
                                    z = true;
                                    break;
                                }
                                dexStoreListHead = dexStoreListHead.next;
                            } else {
                                z = false;
                                break;
                            }
                        }
                        if (!z) {
                            Mlog.safeFmt(AnonymousClass08S.A0S("unable to find required store ", str, " of store ", this.mManifest.id), new Object[0]);
                        }
                    }
                }
            }
        }
        List list = this.mParentStores;
        return (DexStore[]) list.toArray(new DexStore[list.size()]);
    }

    private static String printRelativeTime(long j, long j2) {
        return String.format("%d (%d ms ago)", Long.valueOf(j2), Long.valueOf(j - j2));
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c3, code lost:
        if (r6 != null) goto L_0x00c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c8, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void pruneTemporaryDirectoriesLocked(java.lang.String[] r9) {
        /*
            r8 = this;
            r8.assertLockHeld()
            r3 = 0
            r2 = 0
        L_0x0005:
            int r0 = r9.length
            if (r2 >= r0) goto L_0x00c9
            r5 = r9[r2]
            if (r5 == 0) goto L_0x00b7
            java.lang.String r6 = ".tmpdir_lock"
            boolean r0 = r5.endsWith(r6)
            java.lang.String r1 = ".tmpdir"
            r4 = 0
            if (r0 == 0) goto L_0x0094
            r9[r2] = r4
            java.lang.String r0 = com.facebook.common.dextricks.Fs.stripLastExtension(r5)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r1)
            int r1 = r8.findInArray(r9, r0)
            if (r1 < 0) goto L_0x0092
            r0 = r9[r1]
            r9[r1] = r4
        L_0x002b:
            r4 = r5
            r5 = r0
        L_0x002d:
            if (r4 == 0) goto L_0x0066
            if (r5 == 0) goto L_0x0066
            java.io.File r7 = new java.io.File
            java.io.File r0 = r8.root
            r7.<init>(r0, r4)
            com.facebook.common.dextricks.ReentrantLockFile r6 = com.facebook.common.dextricks.ReentrantLockFile.open(r7)
            com.facebook.common.dextricks.ReentrantLockFile$Lock r4 = r6.tryAcquire(r3)     // Catch:{ all -> 0x00c0 }
            if (r4 != 0) goto L_0x004c
            java.lang.String r1 = "tmpdir %s in use: not deleting"
            java.lang.Object[] r0 = new java.lang.Object[]{r5}     // Catch:{ all -> 0x00c0 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00c0 }
            goto L_0x00b4
        L_0x004c:
            java.lang.String r1 = "tmpdir %s (lockfile %s) is abandoned: deleting"
            java.lang.Object[] r0 = new java.lang.Object[]{r7, r5}     // Catch:{ all -> 0x00bb }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x00bb }
            com.facebook.common.dextricks.Fs.deleteRecursive(r7)     // Catch:{ all -> 0x00bb }
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x00bb }
            java.io.File r0 = r8.root     // Catch:{ all -> 0x00bb }
            r1.<init>(r0, r5)     // Catch:{ all -> 0x00bb }
            com.facebook.common.dextricks.Fs.deleteRecursive(r1)     // Catch:{ all -> 0x00bb }
            r4.close()     // Catch:{ all -> 0x00c0 }
            goto L_0x00b4
        L_0x0066:
            if (r4 == 0) goto L_0x007c
            java.lang.Object[] r1 = new java.lang.Object[]{r4}
            java.lang.String r0 = "tmpdir lockfile %s is orphaned: deleting"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            java.io.File r1 = new java.io.File
            java.io.File r0 = r8.root
            r1.<init>(r0, r4)
            com.facebook.common.dextricks.Fs.deleteRecursive(r1)
            goto L_0x00b7
        L_0x007c:
            if (r5 == 0) goto L_0x00b7
            java.lang.Object[] r1 = new java.lang.Object[]{r5}
            java.lang.String r0 = "tmpdir %s is orphaned without its lockfile: deleting"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            java.io.File r1 = new java.io.File
            java.io.File r0 = r8.root
            r1.<init>(r0, r5)
            com.facebook.common.dextricks.Fs.deleteRecursive(r1)
            goto L_0x00b7
        L_0x0092:
            r0 = r4
            goto L_0x002b
        L_0x0094:
            boolean r0 = r5.endsWith(r1)
            if (r0 == 0) goto L_0x00b1
            r9[r2] = r4
            java.lang.String r0 = com.facebook.common.dextricks.Fs.stripLastExtension(r5)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r6)
            int r1 = r8.findInArray(r9, r0)
            if (r1 < 0) goto L_0x002d
            r0 = r9[r1]
            r9[r1] = r4
            r4 = r0
            goto L_0x002d
        L_0x00b1:
            r5 = r4
            goto L_0x002d
        L_0x00b4:
            r6.close()
        L_0x00b7:
            int r2 = r2 + 1
            goto L_0x0005
        L_0x00bb:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x00c0 }
            throw r0     // Catch:{ all -> 0x00c0 }
        L_0x00c0:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x00c2:
            r0 = move-exception
            if (r6 == 0) goto L_0x00c8
            r6.close()     // Catch:{ all -> 0x00c8 }
        L_0x00c8:
            throw r0
        L_0x00c9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.pruneTemporaryDirectoriesLocked(java.lang.String[]):void");
    }

    public static long readStatusLocked(DexStore dexStore) {
        dexStore.assertLockHeld();
        File file = new File(dexStore.root, MDEX_STATUS_FILENAME);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                byte[] bArr = new byte[16];
                if (fileInputStream.read(bArr, 0, 16) < 16) {
                    Mlog.safeFmt("status file %s too short: treating as zero", file);
                } else {
                    ByteBuffer wrap = ByteBuffer.wrap(bArr);
                    long j = wrap.getLong();
                    long j2 = wrap.getLong();
                    Long valueOf = Long.valueOf(j);
                    Long valueOf2 = Long.valueOf(j2);
                    Mlog.safeFmt("read status:%x check:%x str:%s", valueOf, valueOf2, getStatusDescription(j));
                    long j3 = MDEX_STATUS_XOR ^ j;
                    if (j3 != j2) {
                        Mlog.e("check mismatch: status:%x expected-check:%x actual-check:%x", valueOf, Long.valueOf(j3), valueOf2);
                    } else {
                        Fs.safeClose(fileInputStream);
                        return j;
                    }
                }
                Fs.deleteRecursiveNoThrow(file);
                return 0;
            } finally {
                Fs.safeClose(fileInputStream);
            }
        } catch (FileNotFoundException unused) {
            Mlog.safeFmt("status file %s not found: treating as zero", file);
            return 0;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:30|(2:32|33)|34|35) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002b, code lost:
        if (r2 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x003d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x003e, code lost:
        if (r3 != null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0030 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x0043 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void runCompiler(com.facebook.common.dextricks.DexManifest r6, com.facebook.common.dextricks.OdexScheme r7, int r8, X.C000000c r9, android.content.Context r10) {
        /*
            r5 = this;
            com.facebook.common.dextricks.OdexScheme$Compiler r4 = r7.makeCompiler(r5, r8)
            com.facebook.common.dextricks.DexIteratorFactory r1 = r5.mDexIteratorFactory     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = r5.id     // Catch:{ all -> 0x0044 }
            com.facebook.common.dextricks.InputDexIterator r3 = r1.openDexIterator(r0, r6, r9, r10)     // Catch:{ all -> 0x0044 }
        L_0x000c:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0031
            com.facebook.common.dextricks.InputDex r2 = r3.next()     // Catch:{ all -> 0x003b }
            java.lang.String r1 = "compiling %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x0028 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x0028 }
            r4.compile(r2)     // Catch:{ all -> 0x0028 }
            if (r2 == 0) goto L_0x000c
            r2.close()     // Catch:{ all -> 0x003b }
            goto L_0x000c
        L_0x0028:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
            if (r2 == 0) goto L_0x0030
            r2.close()     // Catch:{ all -> 0x0030 }
        L_0x0030:
            throw r0     // Catch:{ all -> 0x003b }
        L_0x0031:
            r4.performFinishActions()     // Catch:{ all -> 0x003b }
            r3.close()     // Catch:{ all -> 0x0044 }
            r4.close()
            return
        L_0x003b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003d }
        L_0x003d:
            r0 = move-exception
            if (r3 == 0) goto L_0x0043
            r3.close()     // Catch:{ all -> 0x0043 }
        L_0x0043:
            throw r0     // Catch:{ all -> 0x0044 }
        L_0x0044:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0046 }
        L_0x0046:
            r0 = move-exception
            if (r4 == 0) goto L_0x004c
            r4.close()     // Catch:{ all -> 0x004c }
        L_0x004c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.runCompiler(com.facebook.common.dextricks.DexManifest, com.facebook.common.dextricks.OdexScheme, int, X.00c, android.content.Context):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002d, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void saveDeps() {
        /*
            r5 = this;
            byte[] r4 = r5.readCurrentDepBlock()
            java.io.File r3 = new java.io.File
            java.io.File r1 = r5.root
            java.lang.String r0 = "deps"
            r3.<init>(r1, r0)
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile
            java.lang.String r0 = "rw"
            r2.<init>(r3, r0)
            r2.write(r4)     // Catch:{ all -> 0x002b }
            long r0 = r2.getFilePointer()     // Catch:{ all -> 0x002b }
            r2.setLength(r0)     // Catch:{ all -> 0x002b }
            r2.close()
            java.lang.Object[] r1 = new java.lang.Object[]{r3}
            java.lang.String r0 = "saved deps file %s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return
        L_0x002b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002d }
        L_0x002d:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0031 }
        L_0x0031:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.saveDeps():void");
    }

    public File findOatFileForCanaryClass(File file, String str) {
        return getOatFileFromDexHash(file, findDexHashForCanaryClass(str));
    }

    public DexManifest getLoadedManifest() {
        return this.mLoadedManifest;
    }

    public String getMegaZipPath() {
        return this.mMegaZipPath;
    }

    public String[] getParentNames() {
        loadManifest();
        return this.mManifest.requires;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0063, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeStatusLocked(long r9) {
        /*
            r8 = this;
            r8.assertLockHeld()
            r1 = 15
            long r1 = r1 & r9
            int r0 = (int) r1
            byte r1 = (byte) r0
            r0 = 1
            if (r1 == r0) goto L_0x0015
            java.io.File r0 = r8.root
            java.lang.String r1 = r0.getAbsolutePath()
            r0 = -1
            com.facebook.common.dextricks.DalvikInternals.fsyncNamed(r1, r0)
        L_0x0015:
            java.io.File r7 = new java.io.File
            java.io.File r1 = r8.root
            java.lang.String r0 = "mdex_status2"
            r7.<init>(r1, r0)
            r3 = -374168170706063353(0xfaceb007faceb007, double:-3.5650790968196887E283)
            long r3 = r3 ^ r9
            java.lang.Long r2 = java.lang.Long.valueOf(r9)
            r6 = 0
            java.lang.Long r1 = java.lang.Long.valueOf(r3)
            java.lang.String r0 = getStatusDescription(r9)
            java.lang.Object[] r1 = new java.lang.Object[]{r2, r1, r0}
            java.lang.String r0 = "writing status:%x check:%x str:%s"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            r0 = 16
            r5 = 16
            byte[] r2 = new byte[r0]
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r2)
            r0.putLong(r9)
            r0.putLong(r3)
            java.io.FileOutputStream r1 = new java.io.FileOutputStream
            r1.<init>(r7)
            r1.write(r2, r6, r5)     // Catch:{ all -> 0x005d }
            java.io.FileDescriptor r0 = r1.getFD()     // Catch:{ all -> 0x005d }
            r0.sync()     // Catch:{ all -> 0x005d }
            r1.close()
            return
        L_0x005d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005f }
        L_0x005f:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0063 }
        L_0x0063:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.writeStatusLocked(long):void");
    }

    public final class OptimizationCanceledException extends InterruptedException {
        public OptimizationCanceledException(String str) {
            super(str);
        }
    }

    public final class RecoverableDexException extends Exception {
        public RecoverableDexException(Throwable th) {
            super(th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x011f, code lost:
        if (r11 != false) goto L_0x0122;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.common.dextricks.DexErrorRecoveryInfo loadAllCompressedOreoImpl(com.facebook.common.dextricks.DexErrorRecoveryInfo r20, com.facebook.common.dextricks.DexManifest r21, int r22, X.C000000c r23, android.content.Context r24) {
        /*
            r19 = this;
            r13 = r19
            java.lang.String r8 = ".zip"
            java.lang.String r7 = "z-"
            r0 = 32
            java.lang.String r3 = "DexStore.loadlAllOreo"
            r2 = 1789915691(0x6aaff22b, float:1.0635281E26)
            X.AnonymousClass00C.A01(r0, r3, r2)
            r10 = r22 & -2
            java.io.File r5 = new java.io.File     // Catch:{ all -> 0x01b4 }
            java.io.File r2 = r13.root     // Catch:{ all -> 0x01b4 }
            r3 = r21
            com.facebook.common.dextricks.DexManifest$Dex[] r1 = r3.dexes     // Catch:{ all -> 0x01b4 }
            r0 = 0
            r0 = r1[r0]     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = r0.hash     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = X.AnonymousClass08S.A0P(r7, r0, r8)     // Catch:{ all -> 0x01b4 }
            r5.<init>(r2, r0)     // Catch:{ all -> 0x01b4 }
            com.facebook.common.dextricks.OdexSchemeOreo r15 = new com.facebook.common.dextricks.OdexSchemeOreo     // Catch:{ all -> 0x01b4 }
            com.facebook.common.dextricks.DexManifest$Dex[] r0 = r3.dexes     // Catch:{ all -> 0x01b4 }
            r15.<init>(r0, r5)     // Catch:{ all -> 0x01b4 }
            boolean r0 = r15.needsUnpack()     // Catch:{ all -> 0x01b4 }
            if (r0 == 0) goto L_0x00a3
            java.io.File r0 = r13.root     // Catch:{ all -> 0x01b4 }
            java.lang.String[] r9 = r0.list()     // Catch:{ all -> 0x01b4 }
            if (r9 == 0) goto L_0x008a
            int r6 = r9.length     // Catch:{ all -> 0x01b4 }
            r4 = 0
        L_0x003d:
            if (r4 >= r6) goto L_0x007a
            r2 = r9[r4]     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = ".dex"
            boolean r0 = r2.endsWith(r0)     // Catch:{ all -> 0x01b4 }
            if (r0 != 0) goto L_0x006d
            java.lang.String r0 = ".zlock"
            boolean r0 = r2.endsWith(r0)     // Catch:{ all -> 0x01b4 }
            if (r0 != 0) goto L_0x006d
            java.lang.String r0 = ".prof"
            boolean r0 = r2.endsWith(r0)     // Catch:{ all -> 0x01b4 }
            if (r0 != 0) goto L_0x006d
            boolean r0 = r2.endsWith(r8)     // Catch:{ all -> 0x01b4 }
            if (r0 == 0) goto L_0x0065
            boolean r0 = r2.startsWith(r7)     // Catch:{ all -> 0x01b4 }
            if (r0 != 0) goto L_0x006d
        L_0x0065:
            java.lang.String r0 = "oat"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x01b4 }
            if (r0 == 0) goto L_0x0077
        L_0x006d:
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x01b4 }
            java.io.File r0 = r13.root     // Catch:{ all -> 0x01b4 }
            r1.<init>(r0, r2)     // Catch:{ all -> 0x01b4 }
            com.facebook.common.dextricks.Fs.deleteRecursiveNoThrow(r1)     // Catch:{ all -> 0x01b4 }
        L_0x0077:
            int r4 = r4 + 1
            goto L_0x003d
        L_0x007a:
            r16 = 0
            r14 = r3
            r12 = 1
            r18 = r24
            r17 = r23
            r13.runCompiler(r14, r15, r16, r17, r18)     // Catch:{ all -> 0x01b4 }
            r15.finalizeZip()     // Catch:{ all -> 0x01b4 }
            r4 = 1
            goto L_0x00a5
        L_0x008a:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x01b4 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b4 }
            r1.<init>()     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = "unable to list directory "
            r1.append(r0)     // Catch:{ all -> 0x01b4 }
            java.io.File r0 = r13.root     // Catch:{ all -> 0x01b4 }
            r1.append(r0)     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x01b4 }
            r2.<init>(r0)     // Catch:{ all -> 0x01b4 }
            throw r2     // Catch:{ all -> 0x01b4 }
        L_0x00a3:
            r12 = 1
            r4 = 0
        L_0x00a5:
            r15.initializeClassLoader()     // Catch:{ all -> 0x00ae }
            if (r4 == 0) goto L_0x00d6
            r13.verifyCanaryClasses(r3)     // Catch:{ all -> 0x00ae }
            goto L_0x00d6
        L_0x00ae:
            r6 = move-exception
            r0 = r10 & 2
            r5 = 0
            if (r0 == 0) goto L_0x00b5
            r5 = 1
        L_0x00b5:
            java.lang.String r3 = "Failed to teach app classloader about secondary dex files (%s); fatal: %b, regenerated: %b"
            java.io.File r2 = r13.root     // Catch:{ all -> 0x01b4 }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r5)     // Catch:{ all -> 0x01b4 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x01b4 }
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r1, r0}     // Catch:{ all -> 0x01b4 }
            com.facebook.common.dextricks.Mlog.e(r6, r3, r0)     // Catch:{ all -> 0x01b4 }
            if (r5 == 0) goto L_0x00d0
            com.facebook.common.dextricks.FatalDexError r0 = new com.facebook.common.dextricks.FatalDexError     // Catch:{ all -> 0x01b4 }
            r0.<init>(r6)     // Catch:{ all -> 0x01b4 }
            throw r0     // Catch:{ all -> 0x01b4 }
        L_0x00d0:
            com.facebook.common.dextricks.DexStore$RecoverableDexException r0 = new com.facebook.common.dextricks.DexStore$RecoverableDexException     // Catch:{ all -> 0x01b4 }
            r0.<init>(r6)     // Catch:{ all -> 0x01b4 }
            throw r0     // Catch:{ all -> 0x01b4 }
        L_0x00d6:
            java.io.IOException[] r9 = r15.mSuppressedExceptions     // Catch:{ all -> 0x01b4 }
            if (r9 == 0) goto L_0x0121
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = "ClassLoader suppressed exceptions"
            r2.<init>(r0)     // Catch:{ all -> 0x01b4 }
            java.lang.Class<java.lang.Throwable> r6 = java.lang.Throwable.class
            java.lang.String r1 = "addSuppressed"
            java.lang.Class[] r0 = new java.lang.Class[]{r6}     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            java.lang.reflect.Method r8 = r6.getMethod(r1, r0)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            r7 = 0
            r11 = 0
        L_0x00ef:
            int r0 = r9.length     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            if (r7 >= r0) goto L_0x011f
            r10 = r9[r7]     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            java.lang.String r6 = r10.getMessage()     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            java.lang.String r0 = "No original dex files found for dex location"
            int r0 = r6.indexOf(r0)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            r1 = -1
            if (r0 == r1) goto L_0x0109
            java.lang.String r0 = "/split_"
            int r0 = r6.indexOf(r0)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            if (r0 != r1) goto L_0x0111
        L_0x0109:
            java.lang.Object[] r0 = new java.lang.Object[]{r10}     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            r8.invoke(r2, r0)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0114 }
            r11 = 1
        L_0x0111:
            int r7 = r7 + 1
            goto L_0x00ef
        L_0x0114:
            r0 = move-exception
            java.lang.String r1 = "Unable to add suppressed exceptions: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x01b4 }
            com.facebook.common.dextricks.Mlog.e(r1, r0)     // Catch:{ all -> 0x01b4 }
            r11 = 1
        L_0x011f:
            if (r11 != 0) goto L_0x0122
        L_0x0121:
            r2 = 0
        L_0x0122:
            boolean r0 = canLoadCanaryClass(r3)     // Catch:{ all -> 0x01b4 }
            if (r0 != 0) goto L_0x0149
            if (r2 != 0) goto L_0x0131
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = "Failed to load canary class after classloader init"
            r2.<init>(r0)     // Catch:{ all -> 0x01b4 }
        L_0x0131:
            if (r4 == 0) goto L_0x0134
            goto L_0x0137
        L_0x0134:
            java.lang.String r1 = "OdexSchemeOreo reunpack"
            goto L_0x0139
        L_0x0137:
            java.lang.String r1 = "OdexSchemeOreo reunpack after unpack"
        L_0x0139:
            java.lang.String r0 = "Failed to load canary class, reunpacking"
            com.facebook.common.dextricks.DexTricksErrorReporter.reportSampledSoftError(r1, r0, r2)     // Catch:{ all -> 0x01b4 }
            com.facebook.common.dextricks.OdexSchemeOreo.sForceUnpack = r12     // Catch:{ all -> 0x01b4 }
            r0 = 0
            r13.mLoadedManifest = r0     // Catch:{ all -> 0x01b4 }
            com.facebook.common.dextricks.DexStore$RecoverableDexException r0 = new com.facebook.common.dextricks.DexStore$RecoverableDexException     // Catch:{ all -> 0x01b4 }
            r0.<init>(r2)     // Catch:{ all -> 0x01b4 }
            throw r0     // Catch:{ all -> 0x01b4 }
        L_0x0149:
            if (r2 == 0) goto L_0x0152
            java.lang.String r1 = "OdexSchemeOreo suppressed"
            java.lang.String r0 = "OdexSchemeOreo found suppressed exceptions when initializing classloader"
            com.facebook.common.dextricks.DexTricksErrorReporter.reportSampledSoftError(r1, r0, r2)     // Catch:{ all -> 0x01b4 }
        L_0x0152:
            r13.mLoadedManifest = r3     // Catch:{ all -> 0x01b4 }
            r1 = 0
            if (r4 == 0) goto L_0x0158
            r1 = 1
        L_0x0158:
            boolean r2 = com.facebook.common.dextricks.OreoFileUtils.hasVdexOdex(r5)     // Catch:{ all -> 0x01b4 }
            if (r2 == 0) goto L_0x0165
            boolean r0 = com.facebook.common.dextricks.OreoFileUtils.isTruncated(r5)     // Catch:{ all -> 0x01b4 }
            if (r0 == 0) goto L_0x0165
            goto L_0x0168
        L_0x0165:
            r1 = r1 | 8
            goto L_0x016a
        L_0x0168:
            r1 = r1 | 512(0x200, float:7.175E-43)
        L_0x016a:
            if (r2 == 0) goto L_0x016f
            r0 = 524288(0x80000, float:7.34684E-40)
            r1 = r1 | r0
        L_0x016f:
            boolean r0 = com.facebook.common.dextricks.OreoFileUtils.hasRefProfile(r5)     // Catch:{ all -> 0x01b4 }
            if (r0 == 0) goto L_0x0179
            r0 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 | r0
        L_0x0179:
            r15.registerCodeAndProfile()     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = r5.getPath()     // Catch:{ all -> 0x01b4 }
            r13.mMegaZipPath = r0     // Catch:{ all -> 0x01b4 }
            r3 = r20
            r3.loadResult = r1     // Catch:{ all -> 0x01b4 }
            long r0 = com.facebook.common.dextricks.OreoFileUtils.getOdexSize(r5)     // Catch:{ all -> 0x01b4 }
            r3.odexSize = r0     // Catch:{ all -> 0x01b4 }
            java.lang.String r0 = r15.getSchemeName()     // Catch:{ all -> 0x01b4 }
            r3.odexSchemeName = r0     // Catch:{ all -> 0x01b4 }
            r3.dexoptDuringColdStart = r4     // Catch:{ all -> 0x01b4 }
            r13.mLastDeri = r3     // Catch:{ all -> 0x01b4 }
            r2 = 1637988891(0x61a1ba1b, float:3.72917E20)
            r0 = 32
            X.AnonymousClass00C.A00(r0, r2)
            int r0 = r3.loadResult
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            long r0 = r3.odexSize
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            java.lang.Object[] r1 = new java.lang.Object[]{r2, r0}
            java.lang.String r0 = "OdexSchemeOreo loadResult=%d odexSize=%d"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            return r20
        L_0x01b4:
            r3 = move-exception
            r2 = -1540393142(0xffffffffa42f774a, float:-3.8048118E-17)
            r0 = 32
            X.AnonymousClass00C.A00(r0, r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.loadAllCompressedOreoImpl(com.facebook.common.dextricks.DexErrorRecoveryInfo, com.facebook.common.dextricks.DexManifest, int, X.00c, android.content.Context):com.facebook.common.dextricks.DexErrorRecoveryInfo");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:188:0x03ac, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x03ad, code lost:
        if (r3 != null) goto L_0x03af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0159, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x015a, code lost:
        if (r12 != null) goto L_0x015c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r12.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:192:0x03b2 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:71:0x015f */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x036d A[Catch:{ all -> 0x0319, all -> 0x015f, Exception -> 0x00f7, all -> 0x043e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.common.dextricks.DexErrorRecoveryInfo loadAllImpl(int r52, X.C000000c r53, android.content.Context r54) {
        /*
            r51 = this;
            r6 = r52
            r0 = r51
            com.facebook.common.dextricks.DexErrorRecoveryInfo r8 = new com.facebook.common.dextricks.DexErrorRecoveryInfo
            r8.<init>()
            boolean r1 = r51.isLoaded()
            r12 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0025
            com.facebook.common.dextricks.DexErrorRecoveryInfo r1 = r0.mLastDeri
            if (r1 != 0) goto L_0x0022
            java.io.File r1 = r0.root
            java.lang.Object[] r2 = new java.lang.Object[]{r1}
            java.lang.String r1 = "dex store %s has already been loaded, but did not save recovery info"
            com.facebook.common.dextricks.Mlog.w(r1, r2)
            r0.mLastDeri = r8
        L_0x0022:
            com.facebook.common.dextricks.DexErrorRecoveryInfo r0 = r0.mLastDeri
            return r0
        L_0x0025:
            com.facebook.common.dextricks.DexManifest r11 = r51.loadManifest()
            com.facebook.common.dextricks.DexStore[] r7 = r51.getParents()
            int r5 = r7.length
            r4 = 0
        L_0x002f:
            r50 = r53
            r9 = r54
            if (r4 >= r5) goto L_0x0057
            r2 = r7[r4]
            boolean r1 = r2.isLoaded()
            if (r1 != 0) goto L_0x0054
            r1 = r50
            com.facebook.common.dextricks.DexErrorRecoveryInfo r1 = r2.loadAll(r6, r1, r9)
            java.lang.String r2 = r2.id
            int r1 = r1.loadResult
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Object[] r2 = new java.lang.Object[]{r2, r1}
            java.lang.String r1 = "parent dex store %s loaded with result: %x"
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r2)
        L_0x0054:
            int r4 = r4 + 1
            goto L_0x002f
        L_0x0057:
            com.facebook.common.dextricks.ReentrantLockFile r1 = r0.mLockFile
            com.facebook.common.dextricks.ReentrantLockFile$Lock r47 = r1.acquire(r3)
            com.facebook.common.dextricks.DexStore r1 = r0.next     // Catch:{ all -> 0x043e }
            if (r1 != 0) goto L_0x0064
            canLoadCanaryClass(r11)     // Catch:{ all -> 0x043e }
        L_0x0064:
            boolean r1 = com.facebook.common.dextricks.DexStoreUtils.OREO_OR_NEWER     // Catch:{ all -> 0x043e }
            long r4 = readStatusLocked(r51)     // Catch:{ all -> 0x043e }
            r16 = 15
            long r1 = r4 & r16
            int r7 = (int) r1     // Catch:{ all -> 0x043e }
            byte r10 = (byte) r7     // Catch:{ all -> 0x043e }
            r1 = 10
            if (r10 < r1) goto L_0x0083
            java.lang.String r7 = "found invalid state %s: nuking dex store %s"
            java.lang.Byte r2 = java.lang.Byte.valueOf(r10)     // Catch:{ all -> 0x043e }
            java.io.File r1 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.Object[] r1 = new java.lang.Object[]{r2, r1}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r7, r1)     // Catch:{ all -> 0x043e }
        L_0x0083:
            r1 = 4
            if (r10 != r12) goto L_0x009e
            java.lang.String r13 = "found abandoned transaction (prev stateno %s status %x) on dex store %s: nuking store"
            long r14 = r4 >> r1
            long r1 = r14 & r16
            java.lang.Long r7 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x043e }
            java.lang.Long r2 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x043e }
            java.io.File r1 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.Object[] r1 = new java.lang.Object[]{r7, r2, r1}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r13, r1)     // Catch:{ all -> 0x043e }
            goto L_0x00a0
        L_0x009e:
            r1 = 0
            goto L_0x00a2
        L_0x00a0:
            r1 = 16
        L_0x00a2:
            r2 = 5
            if (r10 != r2) goto L_0x00ae
            java.lang.String r7 = "crashed last time while loading generated files; trying fallback"
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r7, r2)     // Catch:{ all -> 0x043e }
            r1 = r1 | 64
        L_0x00ae:
            r2 = 6
            if (r10 != r2) goto L_0x00ba
            java.lang.String r7 = "force dex regeneration requested"
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r7, r2)     // Catch:{ all -> 0x043e }
            r1 = r1 | 32
        L_0x00ba:
            com.facebook.common.dextricks.OdexScheme r7 = r0.schemeForState(r9, r11, r4)     // Catch:{ all -> 0x043e }
            java.lang.String[] r15 = r0.listAndPruneRootFiles(r9)     // Catch:{ all -> 0x043e }
            int r2 = r0.checkDirty(r7, r15)     // Catch:{ all -> 0x043e }
            if (r2 != 0) goto L_0x00d5
            java.lang.String r12 = "LA_LOAD_EXISTING"
        L_0x00ca:
            java.lang.String r14 = "current scheme: %s next step: %s"
            r13 = 1
            java.lang.Object[] r12 = new java.lang.Object[]{r7, r12}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r14, r12)     // Catch:{ all -> 0x043e }
            goto L_0x00dd
        L_0x00d5:
            if (r2 != r12) goto L_0x00da
            java.lang.String r12 = "LA_REGEN_MISSING"
            goto L_0x00ca
        L_0x00da:
            java.lang.String r12 = "LA_REGEN_ALL"
            goto L_0x00ca
        L_0x00dd:
            if (r2 != r13) goto L_0x00ee
            int r12 = r7.flags     // Catch:{ all -> 0x043e }
            r12 = r12 & r13
            if (r12 == 0) goto L_0x00ee
            java.lang.String r12 = "scheme %s is non-incremental: regenerating everything"
            java.lang.Object[] r2 = new java.lang.Object[]{r7}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r12, r2)     // Catch:{ all -> 0x043e }
            r2 = 2
        L_0x00ee:
            if (r2 != 0) goto L_0x0104
            r12 = 3
            if (r10 != r12) goto L_0x0104
            installCrossDexHooks()     // Catch:{ Exception -> 0x00f7 }
            goto L_0x0104
        L_0x00f7:
            r13 = move-exception
            java.lang.String r12 = "dex store %s needs xdex hooks, but we can't do it: regenerating"
            java.io.File r2 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.Object[] r2 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r13, r12, r2)     // Catch:{ all -> 0x043e }
            r2 = 2
        L_0x0104:
            if (r2 <= 0) goto L_0x0160
            r13 = 58
            com.facebook.common.dextricks.DexManifest$Dex[] r12 = r11.dexes     // Catch:{ all -> 0x043e }
            int r12 = r12.length     // Catch:{ all -> 0x043e }
            if (r12 <= r13) goto L_0x0122
            java.lang.String r14 = "too many dexes, forcing turbo mode: have %s but maximum per dex store is %s"
            java.lang.Integer r13 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x043e }
            r12 = 58
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x043e }
            java.lang.Object[] r12 = new java.lang.Object[]{r13, r12}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r14, r12)     // Catch:{ all -> 0x043e }
            r6 = r52 | 1
        L_0x0122:
            writeTxFailedStatusLocked(r0, r4)     // Catch:{ all -> 0x043e }
            java.io.File r14 = new java.io.File     // Catch:{ all -> 0x043e }
            java.io.File r13 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.String r12 = "regen_stamp"
            r14.<init>(r13, r12)     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Fs.deleteRecursive(r14)     // Catch:{ all -> 0x043e }
            java.io.FileOutputStream r12 = new java.io.FileOutputStream     // Catch:{ all -> 0x043e }
            r12.<init>(r14)     // Catch:{ all -> 0x043e }
            r12.close()     // Catch:{ all -> 0x043e }
            java.io.File r14 = new java.io.File     // Catch:{ all -> 0x043e }
            java.io.File r13 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.String r12 = "odex_lock"
            r14.<init>(r13, r12)     // Catch:{ all -> 0x043e }
            boolean r12 = r14.exists()     // Catch:{ all -> 0x043e }
            if (r12 == 0) goto L_0x0163
            com.facebook.common.dextricks.ReentrantLockFile r12 = com.facebook.common.dextricks.ReentrantLockFile.open(r14)     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.ReentrantLockFile$Lock r13 = r12.acquire(r3)     // Catch:{ all -> 0x0157 }
            r13.close()     // Catch:{ all -> 0x0157 }
            r12.close()     // Catch:{ all -> 0x043e }
            goto L_0x0163
        L_0x0157:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0159 }
        L_0x0159:
            r0 = move-exception
            if (r12 == 0) goto L_0x015f
            r12.close()     // Catch:{ all -> 0x015f }
        L_0x015f:
            throw r0     // Catch:{ all -> 0x043e }
        L_0x0160:
            r41 = 0
            goto L_0x0165
        L_0x0163:
            r41 = 1
        L_0x0165:
            if (r2 != 0) goto L_0x0171
            boolean r12 = r51.checkAnyOptimizerRunningCurrently()     // Catch:{ all -> 0x043e }
            if (r12 == 0) goto L_0x0171
            r12 = 1
            r8.dexoptDuringColdStart = r12     // Catch:{ all -> 0x043e }
            goto L_0x0172
        L_0x0171:
            r12 = 1
        L_0x0172:
            if (r2 != r12) goto L_0x0192
            r19 = 1
            r20 = r50
            r21 = r9
            r16 = r0
            r17 = r11
            r18 = r7
            r16.runCompiler(r17, r18, r19, r20, r21)     // Catch:{ Exception -> 0x0185 }
            r2 = 0
            goto L_0x0192
        L_0x0185:
            r13 = move-exception
            java.lang.String r12 = "incremental regeneration error in dex store %s: regenerating"
            java.io.File r2 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.Object[] r2 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r13, r12, r2)     // Catch:{ all -> 0x043e }
            r2 = 2
        L_0x0192:
            java.io.File r12 = r0.root     // Catch:{ FileNotFoundException -> 0x01a0 }
            com.facebook.common.dextricks.DexStore$Config r14 = com.facebook.common.dextricks.DexStore.Config.readFromRoot(r12)     // Catch:{ FileNotFoundException -> 0x01a0 }
            java.lang.String r13 = "loaded normal root config file"
            java.lang.Object[] r12 = new java.lang.Object[r3]     // Catch:{ FileNotFoundException -> 0x01a0 }
            com.facebook.common.dextricks.Mlog.safeFmt(r13, r12)     // Catch:{ FileNotFoundException -> 0x01a0 }
            goto L_0x01e2
        L_0x01a0:
            java.lang.String r13 = "no config file for repository %s found: using all-default configuration"
            java.io.File r12 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.Object[] r12 = new java.lang.Object[]{r12}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r13, r12)     // Catch:{ all -> 0x043e }
            boolean r29 = com.facebook.common.dextricks.DexStore.Config.enableOatmealByDefault()     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.DexStore$Config r14 = new com.facebook.common.dextricks.DexStore$Config     // Catch:{ all -> 0x043e }
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r23 = -1
            r24 = -1
            r25 = -1
            r26 = -1
            r27 = -1
            r28 = 0
            r30 = 0
            r31 = 0
            r32 = 0
            r33 = 0
            r34 = 0
            r35 = 0
            r36 = 0
            r37 = 0
            r38 = -1
            r40 = 0
            r17 = 0
            r16 = r14
            r16.<init>(r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r40)     // Catch:{ all -> 0x043e }
        L_0x01e2:
            r12 = 8
            if (r2 == 0) goto L_0x02ce
            r51.saveDeps()     // Catch:{ all -> 0x043e }
            byte r10 = r0.determineDesiredState(r10, r11)     // Catch:{ all -> 0x043e }
            r4 = r6 & 1
            r5 = 3
            if (r4 == 0) goto L_0x0207
            if (r10 == r5) goto L_0x01ff
            if (r10 != r12) goto L_0x0207
            java.lang.String r10 = "using ART turbo instead of ART xdex: DS_DO_NOT_OPTIMIZE"
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r10, r4)     // Catch:{ all -> 0x043e }
            r10 = 7
            goto L_0x0207
        L_0x01ff:
            java.lang.String r10 = "using Dalvik turbo instead of xdex: DS_DO_NOT_OPTIMIZE"
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r10, r4)     // Catch:{ all -> 0x043e }
            r10 = 4
        L_0x0207:
            byte r4 = r14.sync     // Catch:{ all -> 0x043e }
            if (r4 == 0) goto L_0x0235
            r12 = 1
            if (r4 == r12) goto L_0x021e
            r12 = 2
            if (r4 == r12) goto L_0x0212
            goto L_0x0228
        L_0x0212:
            java.lang.String r12 = "forcing synchronous optimization from config file"
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r12, r4)     // Catch:{ all -> 0x043e }
            r4 = r6 & -5
            r6 = r4 | 8
            goto L_0x0235
        L_0x021e:
            java.lang.String r12 = "forcing async optimization mode from config file: dangerous!"
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r12, r4)     // Catch:{ all -> 0x043e }
            r6 = r6 | 4
            goto L_0x0235
        L_0x0228:
            java.lang.String r12 = "config file has unknown sync control mode %s: ignoring"
            java.lang.Byte r4 = java.lang.Byte.valueOf(r4)     // Catch:{ all -> 0x043e }
            java.lang.Object[] r4 = new java.lang.Object[]{r4}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r12, r4)     // Catch:{ all -> 0x043e }
        L_0x0235:
            byte r10 = r0.adjustDesiredStateForConfig(r10, r14)     // Catch:{ all -> 0x043e }
            if (r10 != r5) goto L_0x024a
            installCrossDexHooks()     // Catch:{ Exception -> 0x023f }
            goto L_0x024a
        L_0x023f:
            r10 = move-exception
            java.lang.String r5 = "disabling cross-dex optimization: cannot install hooks"
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r10, r5, r4)     // Catch:{ all -> 0x043e }
            r8.xdexFailureCause = r10     // Catch:{ all -> 0x043e }
            r10 = 4
        L_0x024a:
            java.lang.String r5 = "desiredStateNo:%s"
            java.lang.Byte r4 = java.lang.Byte.valueOf(r10)     // Catch:{ all -> 0x043e }
            java.lang.Object[] r4 = new java.lang.Object[]{r4}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r5, r4)     // Catch:{ all -> 0x043e }
        L_0x0257:
            if (r2 == 0) goto L_0x02cd
            r4 = 2
            r7 = 0
            if (r2 < r4) goto L_0x025e
            r7 = 1
        L_0x025e:
            java.lang.String r5 = "incremental regen already handled"
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.assertThat(r7, r5, r4)     // Catch:{ all -> 0x043e }
            long r4 = (long) r10     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.OdexScheme r7 = r0.schemeForState(r9, r11, r4)     // Catch:{ all -> 0x043e }
            r0.deleteFiles(r15)     // Catch:{ Exception -> 0x02af }
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x02af }
            java.io.File r5 = r0.root     // Catch:{ Exception -> 0x02af }
            java.lang.String r4 = "optimization_log"
            r12.<init>(r5, r4)     // Catch:{ Exception -> 0x02af }
            r12.delete()     // Catch:{ Exception -> 0x02af }
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x02af }
            java.io.File r5 = r0.root     // Catch:{ Exception -> 0x02af }
            java.lang.String r4 = "classmap.bin"
            r12.<init>(r5, r4)     // Catch:{ Exception -> 0x02af }
            r12.delete()     // Catch:{ Exception -> 0x02af }
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x02af }
            java.io.File r5 = r0.root     // Catch:{ Exception -> 0x02af }
            java.lang.String r4 = "classmap.bin.hf"
            r12.<init>(r5, r4)     // Catch:{ Exception -> 0x02af }
            r12.delete()     // Catch:{ Exception -> 0x02af }
            int r4 = r7.flags     // Catch:{ Exception -> 0x02af }
            r4 = r4 & 16
            if (r4 == 0) goto L_0x029f
            java.lang.String r4 = "not running dex compiler: scheme says there is nothing to do"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x02b2 }
            com.facebook.common.dextricks.Mlog.safeFmt(r4, r3)     // Catch:{ Exception -> 0x02b2 }
            goto L_0x02c9
        L_0x029f:
            r19 = 0
            r20 = r50
            r16 = r0
            r17 = r11
            r18 = r7
            r21 = r9
            r16.runCompiler(r17, r18, r19, r20, r21)     // Catch:{ Exception -> 0x02af }
            goto L_0x02c9
        L_0x02af:
            r5 = move-exception
            r3 = 2
            goto L_0x02b4
        L_0x02b2:
            r5 = move-exception
            r3 = 2
        L_0x02b4:
            if (r10 == r3) goto L_0x02cc
            java.lang.String r4 = "dex store %s: failed turbodex: using fallback"
            java.io.File r3 = r0.root     // Catch:{ all -> 0x043e }
            java.lang.Object[] r3 = new java.lang.Object[]{r3}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r5, r4, r3)     // Catch:{ all -> 0x043e }
            r8.fallbackCause = r5     // Catch:{ all -> 0x043e }
            java.lang.String[] r15 = r0.listAndPruneRootFiles(r9)     // Catch:{ all -> 0x043e }
            r10 = 2
            goto L_0x02ca
        L_0x02c9:
            r2 = 0
        L_0x02ca:
            r3 = 0
            goto L_0x0257
        L_0x02cc:
            throw r5     // Catch:{ all -> 0x043e }
        L_0x02cd:
            long r4 = (long) r10     // Catch:{ all -> 0x043e }
        L_0x02ce:
            r12 = 1
            java.lang.String r10 = "next step should be LA_LOAD_EXISTING"
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.assertThat(r12, r10, r2)     // Catch:{ all -> 0x043e }
            r2 = r6 & 4
            r12 = 0
            if (r2 == 0) goto L_0x02dc
            r12 = 1
        L_0x02dc:
            if (r12 == 0) goto L_0x02ed
            r44 = r50
            r40 = r0
            r42 = r7
            r43 = r11
            r45 = r9
            r46 = r6
            r40.loadDexFiles(r41, r42, r43, r44, r45, r46)     // Catch:{ all -> 0x043e }
        L_0x02ed:
            if (r41 == 0) goto L_0x0385
            if (r12 == 0) goto L_0x0328
            java.lang.String r3 = "about to start syncer thread"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.DexStore$FinishRegenerationThread r3 = new com.facebook.common.dextricks.DexStore$FinishRegenerationThread     // Catch:{ all -> 0x043e }
            r44 = r3
            r45 = r0
            r46 = r7
            r48 = r4
            r44.<init>(r46, r47, r48)     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.ReentrantLockFile r2 = r0.mLockFile     // Catch:{ all -> 0x043e }
            r2.donateLock(r3)     // Catch:{ all -> 0x043e }
            r3.start()     // Catch:{ all -> 0x0319 }
            r47 = 0
            java.lang.String r3 = "started syncer thread"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            goto L_0x0385
        L_0x0319:
            r3 = move-exception
            java.lang.String r2 = "failed to start syncer thread"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.w(r2, r1)     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.ReentrantLockFile r0 = r0.mLockFile     // Catch:{ all -> 0x043e }
            r0.stealLock()     // Catch:{ all -> 0x043e }
            throw r3     // Catch:{ all -> 0x043e }
        L_0x0328:
            java.lang.String r3 = "fsyncing just-regenerated dex store %s in foreground as requested"
            java.io.File r2 = r0.root     // Catch:{ all -> 0x043e }
            r10 = 0
            java.lang.Object[] r2 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            java.io.File r3 = r0.root     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Prio r2 = com.facebook.common.dextricks.Prio.unchanged()     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Fs.fsyncRecursive(r3, r2)     // Catch:{ all -> 0x043e }
            r0.writeStatusLocked(r4)     // Catch:{ all -> 0x043e }
            java.lang.String r3 = "dex store sync completed"
            java.lang.Object[] r2 = new java.lang.Object[r10]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            int r2 = r7.flags     // Catch:{ all -> 0x043e }
            r2 = r2 & 4
            if (r2 != 0) goto L_0x0355
            java.lang.String r3 = "optimizing in foreground"
            java.lang.Object[] r2 = new java.lang.Object[r10]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            goto L_0x036a
        L_0x0355:
            r2 = r6 & 8
            if (r2 == 0) goto L_0x0361
            java.lang.String r3 = "optimizing in foreground despite expense: forced"
            java.lang.Object[] r2 = new java.lang.Object[r10]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            goto L_0x036a
        L_0x0361:
            java.lang.String r3 = "not optimizing in foreground: would be too expensive"
            java.lang.Object[] r2 = new java.lang.Object[r10]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            r2 = 0
            goto L_0x036b
        L_0x036a:
            r2 = 1
        L_0x036b:
            if (r2 == 0) goto L_0x0385
            r0.optimizeInForegroundLocked(r9, r11, r4)     // Catch:{ all -> 0x043e }
            long r4 = readStatusLocked(r51)     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.OdexScheme r7 = r0.schemeForState(r9, r11, r4)     // Catch:{ all -> 0x043e }
            java.lang.String r3 = "done optimizing in foreground: new status %x scheme %s"
            java.lang.Long r2 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x043e }
            java.lang.Object[] r2 = new java.lang.Object[]{r2, r7}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
        L_0x0385:
            if (r12 != 0) goto L_0x0397
            r15 = r0
            r16 = r41
            r17 = r7
            r18 = r11
            r19 = r50
            r20 = r9
            r21 = r6
            r15.loadDexFiles(r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x043e }
        L_0x0397:
            java.lang.String r2 = "dexopt"
            com.facebook.common.dextricks.DexStore$TmpDir r3 = r0.makeTemporaryDirectory(r2)     // Catch:{ all -> 0x03b3 }
            java.io.File r2 = r0.root     // Catch:{ all -> 0x03aa }
            int r2 = r7.loadInformationalStatus(r2, r4)     // Catch:{ all -> 0x03aa }
            r1 = r1 | r2
            if (r3 == 0) goto L_0x03bc
            r3.close()     // Catch:{ all -> 0x03b3 }
            goto L_0x03bc
        L_0x03aa:
            r2 = move-exception
            throw r2     // Catch:{ all -> 0x03ac }
        L_0x03ac:
            r2 = move-exception
            if (r3 == 0) goto L_0x03b2
            r3.close()     // Catch:{ all -> 0x03b2 }
        L_0x03b2:
            throw r2     // Catch:{ all -> 0x03b3 }
        L_0x03b3:
            r6 = move-exception
            java.lang.String r3 = "Failure while checking oat file provenance."
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.e(r6, r3, r2)     // Catch:{ all -> 0x043e }
        L_0x03bc:
            java.lang.String r2 = r7.getSchemeName()     // Catch:{ all -> 0x043e }
            r8.odexSchemeName = r2     // Catch:{ all -> 0x043e }
            if (r41 == 0) goto L_0x03c6
            r1 = r1 | 1
        L_0x03c6:
            int r2 = r7.flags     // Catch:{ all -> 0x043e }
            r2 = r2 & 8
            if (r2 == 0) goto L_0x03d1
            r1 = r1 | 8
            logNonOptimalScheme(r7)     // Catch:{ all -> 0x043e }
        L_0x03d1:
            java.io.File r2 = r0.root     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.DexStore$OptimizationHistoryLog r6 = getCurrentOptHistoryLogFromRoot(r0, r9, r2)     // Catch:{ all -> 0x043e }
            java.lang.String r3 = "Asking scheme %s needOptimization"
            java.lang.Class r2 = r7.getClass()     // Catch:{ all -> 0x043e }
            java.lang.String r10 = r2.getSimpleName()     // Catch:{ all -> 0x043e }
            r9 = 0
            java.lang.Object[] r2 = new java.lang.Object[]{r10}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.OdexScheme$NeedOptimizationState r6 = r7.needOptimization(r4, r14, r6)     // Catch:{ all -> 0x043e }
            boolean r2 = r6.needsOptimization()     // Catch:{ all -> 0x043e }
            if (r2 == 0) goto L_0x042d
            r1 = r1 | 2
            com.facebook.common.dextricks.OdexScheme$NeedOptimizationState r2 = com.facebook.common.dextricks.OdexScheme.NeedOptimizationState.NEED_REOPTIMIZATION     // Catch:{ all -> 0x043e }
            boolean r2 = r2.equals(r6)     // Catch:{ all -> 0x043e }
            if (r2 == 0) goto L_0x0409
            java.lang.String r3 = "Scheme %s currently needs reoptimization"
            java.lang.Object[] r2 = new java.lang.Object[]{r10}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            r2 = 1048576(0x100000, float:1.469368E-39)
            r1 = r1 | r2
        L_0x0409:
            boolean r2 = r7.loadNotOptimized(r4)     // Catch:{ all -> 0x043e }
            if (r2 == 0) goto L_0x0414
            r1 = r1 | 8
            logSchemeNeedsOptimization(r7, r4)     // Catch:{ all -> 0x043e }
        L_0x0414:
            java.lang.String r3 = "optimization needed state: %s"
            java.lang.Object[] r2 = new java.lang.Object[]{r6}     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            int r2 = r7.flags     // Catch:{ all -> 0x043e }
            r2 = r2 & 4
            if (r2 == 0) goto L_0x0434
            java.lang.String r3 = "... but optimization is very expensive"
            java.lang.Object[] r2 = new java.lang.Object[r9]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
            r1 = r1 | 4
            goto L_0x0434
        L_0x042d:
            java.lang.String r3 = "optimization needed: no"
            java.lang.Object[] r2 = new java.lang.Object[r9]     // Catch:{ all -> 0x043e }
            com.facebook.common.dextricks.Mlog.safeFmt(r3, r2)     // Catch:{ all -> 0x043e }
        L_0x0434:
            r8.loadResult = r1     // Catch:{ all -> 0x043e }
            r0.mLastDeri = r8     // Catch:{ all -> 0x043e }
            if (r47 == 0) goto L_0x043d
            r47.close()
        L_0x043d:
            return r8
        L_0x043e:
            r0 = move-exception
            if (r47 == 0) goto L_0x0444
            r47.close()
        L_0x0444:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStore.loadAllImpl(int, X.00c, android.content.Context):com.facebook.common.dextricks.DexErrorRecoveryInfo");
    }

    public static DexStore open(File file, File file2, ResProvider resProvider) {
        return open(file, file2, resProvider, new ArrayList(), new ArrayList());
    }

    public static synchronized DexStore open(File file, File file2, ResProvider resProvider, ArrayList arrayList, ArrayList arrayList2) {
        DexStore dexStoreListHead;
        synchronized (DexStore.class) {
            File absoluteFile = file.getAbsoluteFile();
            dexStoreListHead = dexStoreListHead();
            while (true) {
                if (dexStoreListHead != null) {
                    if (dexStoreListHead.root.equals(absoluteFile)) {
                        break;
                    }
                    dexStoreListHead = dexStoreListHead.next;
                } else {
                    dexStoreListHead = new DexStore(absoluteFile, file2, resProvider, arrayList, arrayList2);
                    sListHead = dexStoreListHead;
                    break;
                }
            }
        }
        return dexStoreListHead;
    }
}
