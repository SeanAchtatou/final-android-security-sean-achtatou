package com.facebook.common.dextricks;

import X.AnonymousClass095;
import X.AnonymousClass0Mm;
import android.content.Context;
import dalvik.system.DexFile;
import java.io.File;
import java.util.Arrays;
import java.util.List;

public final class DexStorePgoUtils {
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004b, code lost:
        if (com.facebook.common.jit.profile.PGOUtilsNative.A00 == null) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0073, code lost:
        if (com.facebook.common.jit.profile.PGOUtilsNative.A00 == null) goto L_0x0075;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A00(java.lang.String r6, dalvik.system.DexFile[] r7) {
        /*
            java.lang.String r1 = "PgoUtils"
            if (r6 != 0) goto L_0x000e
            java.lang.String r0 = "Cannot get pgo methods since no profile was given"
        L_0x0006:
            android.util.Log.e(r1, r0)
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x000e:
            if (r7 == 0) goto L_0x001c
            int r5 = r7.length
            if (r5 == 0) goto L_0x001c
            boolean r0 = com.facebook.common.jit.profile.PGOUtilsNative.A02
            if (r0 != 0) goto L_0x001f
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x001c:
            java.lang.String r0 = "Cannot get pgo methods since no dex files were given"
            goto L_0x0006
        L_0x001f:
            r4 = 0
            if (r7 == 0) goto L_0x0057
            boolean r0 = com.facebook.common.jit.profile.PGOUtilsNative.A01
            if (r0 != 0) goto L_0x0044
            r3 = 1
            java.lang.Class<dalvik.system.DexFile> r1 = dalvik.system.DexFile.class
            java.lang.String r0 = "mCookie"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch:{ NoSuchFieldException -> 0x0035 }
            com.facebook.common.jit.profile.PGOUtilsNative.A00 = r0     // Catch:{ NoSuchFieldException -> 0x0035 }
            r0.setAccessible(r3)     // Catch:{ NoSuchFieldException -> 0x0035 }
            goto L_0x0042
        L_0x0035:
            r2 = move-exception
            java.lang.String r1 = "PGOUtilsNative"
            java.lang.String r0 = "Could not get DexFile.mCookie"
            android.util.Log.w(r1, r0, r2)     // Catch:{ all -> 0x003e }
            goto L_0x0042
        L_0x003e:
            r0 = move-exception
            com.facebook.common.jit.profile.PGOUtilsNative.A01 = r3
            throw r0
        L_0x0042:
            com.facebook.common.jit.profile.PGOUtilsNative.A01 = r3
        L_0x0044:
            boolean r0 = com.facebook.common.jit.profile.PGOUtilsNative.A01
            if (r0 == 0) goto L_0x004d
            java.lang.reflect.Field r1 = com.facebook.common.jit.profile.PGOUtilsNative.A00
            r0 = 1
            if (r1 != 0) goto L_0x004e
        L_0x004d:
            r0 = 0
        L_0x004e:
            if (r0 != 0) goto L_0x0065
            java.lang.String r1 = "PGOUtilsNative"
            java.lang.String r0 = "Cannot get cookies from dex file on this platform"
            android.util.Log.w(r1, r0)
        L_0x0057:
            if (r4 != 0) goto L_0x008d
            java.lang.String r1 = "PGOUtilsNative"
            java.lang.String r0 = "Cannot get pgo methods on this platform"
            android.util.Log.w(r1, r0)
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x0065:
            java.lang.Object[] r4 = new java.lang.Object[r5]
            r3 = 0
        L_0x0068:
            if (r3 >= r5) goto L_0x0057
            r2 = r7[r3]
            boolean r0 = com.facebook.common.jit.profile.PGOUtilsNative.A01
            if (r0 == 0) goto L_0x0075
            java.lang.reflect.Field r1 = com.facebook.common.jit.profile.PGOUtilsNative.A00
            r0 = 1
            if (r1 != 0) goto L_0x0076
        L_0x0075:
            r0 = 0
        L_0x0076:
            if (r0 == 0) goto L_0x0094
            java.lang.reflect.Field r0 = com.facebook.common.jit.profile.PGOUtilsNative.A00     // Catch:{ IllegalAccessException -> 0x007f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IllegalAccessException -> 0x007f }
            goto L_0x0088
        L_0x007f:
            r2 = move-exception
            java.lang.String r1 = "PGOUtilsNative"
            java.lang.String r0 = "Error getting DexFile.mCookie"
            android.util.Log.w(r1, r0, r2)
            r0 = 0
        L_0x0088:
            r4[r3] = r0
            int r3 = r3 + 1
            goto L_0x0068
        L_0x008d:
            java.lang.Object r0 = com.facebook.common.jit.profile.PGOUtilsNative.nativeGetPgoMethodInfo(r6, r4)
            java.util.List r0 = (java.util.List) r0
            return r0
        L_0x0094:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Cannot get cookie from dex file"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStorePgoUtils.A00(java.lang.String, dalvik.system.DexFile[]):java.util.List");
    }

    private DexStorePgoUtils() {
    }

    public static void addCurrentMultidexCodePaths() {
        String[] strArr;
        DexFile[] configuredDexFiles = MultiDexClassLoader.getConfiguredDexFiles();
        if (configuredDexFiles == null) {
            strArr = new String[0];
        } else {
            int length = configuredDexFiles.length;
            strArr = new String[length];
            for (int i = 0; i < length; i++) {
                strArr[i] = configuredDexFiles[i].getName();
            }
        }
        Arrays.toString(strArr);
    }

    public static File createNewMainDexStoreReferencePgoProfile(Context context) {
        return AnonymousClass095.A00(context).A05(DexStoreUtils.getMainDexStoreLocation(context));
    }

    public static void forceCleanMainDexStoreProfiles(Context context) {
        File mainDexStoreLocation = DexStoreUtils.getMainDexStoreLocation(context);
        AnonymousClass095 A00 = AnonymousClass095.A00(context);
        File fileStreamPath = A00.A02.getFileStreamPath(AnonymousClass095.A02(A00));
        fileStreamPath.delete();
        fileStreamPath.exists();
        fileStreamPath.getAbsolutePath();
        if (mainDexStoreLocation == null) {
            return;
        }
        if (mainDexStoreLocation != null) {
            File file = new File(mainDexStoreLocation, "art_pgo_ref_profile.prof");
            file.delete();
            file.exists();
            file.getAbsolutePath();
            return;
        }
        throw new IllegalArgumentException();
    }

    public static File getCurrentMainDexStorePgoProfile(Context context) {
        File mainDexStoreReferencePgoProfile = getMainDexStoreReferencePgoProfile(context);
        if (mainDexStoreReferencePgoProfile == null || !mainDexStoreReferencePgoProfile.exists()) {
            return AnonymousClass095.A00(context).A04();
        }
        return mainDexStoreReferencePgoProfile;
    }

    public static File getMainDexStoreReferencePgoProfile(Context context) {
        File mainDexStoreLocation = DexStoreUtils.getMainDexStoreLocation(context);
        AnonymousClass095.A00(context);
        if (mainDexStoreLocation != null) {
            return new File(mainDexStoreLocation, "art_pgo_ref_profile.prof");
        }
        throw new IllegalArgumentException();
    }

    public static String getMainDexStoreReferencePgoProfileFilePath(Context context) {
        File mainDexStoreReferencePgoProfile = getMainDexStoreReferencePgoProfile(context);
        if (mainDexStoreReferencePgoProfile != null) {
            return mainDexStoreReferencePgoProfile.getAbsolutePath();
        }
        return null;
    }

    public static AnonymousClass0Mm getPgoDataForMainDexStore(Context context, boolean z) {
        File mainDexStoreLocation = DexStoreUtils.getMainDexStoreLocation(context);
        if (mainDexStoreLocation == null) {
            return null;
        }
        mainDexStoreLocation.getAbsolutePath();
        return null;
    }

    public static boolean isMainDexStoreProfileChangeSignificant(Context context, boolean z) {
        DexStoreUtils.getMainDexStoreLocation(context).getAbsolutePath();
        return false;
    }

    public static List getPgoMethodInfoWithDexStore(Context context) {
        String absolutePath;
        DexFile[] configuredDexFiles = MultiDexClassLoader.getConfiguredDexFiles();
        File A04 = AnonymousClass095.A00(context).A04();
        if (A04 == null) {
            absolutePath = null;
        } else {
            absolutePath = A04.getAbsolutePath();
        }
        return A00(absolutePath, configuredDexFiles);
    }

    public static List getPgoMethodInfoWithDexStore(String str) {
        return A00(str, MultiDexClassLoader.getConfiguredDexFiles());
    }
}
