package com.facebook.common.dextricks;

public class DexIteratorFactory {
    private static final String SECONDARY_XZS_FILENAME = "secondary.dex.jar.xzs";
    private static final String XZS_EXTENSION = ".dex.jar.xzs";
    private final ResProvider mResProvider;

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c A[Catch:{ all -> 0x0051 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.common.dextricks.InputDexIterator openDexIterator(java.lang.String r9, com.facebook.common.dextricks.DexManifest r10, X.C000000c r11, android.content.Context r12) {
        /*
            r8 = this;
            r3 = r10
            int r0 = r10.superpackFiles
            r7 = r11
            if (r0 <= 0) goto L_0x000b
            com.facebook.common.dextricks.SuperpackInputDexIterator r0 = r8.openSuperpackDexIterator(r10, r11, r12)
            return r0
        L_0x000b:
            r6 = 0
            if (r9 == 0) goto L_0x001d
            java.lang.String r0 = "dex"
            boolean r0 = r0.equals(r9)     // Catch:{ all -> 0x0051 }
            if (r0 != 0) goto L_0x001d
            java.lang.String r0 = ".dex.jar.xzs"
            java.lang.String r2 = r9.concat(r0)     // Catch:{ all -> 0x0051 }
            goto L_0x001f
        L_0x001d:
            java.lang.String r2 = "secondary.dex.jar.xzs"
        L_0x001f:
            com.facebook.common.dextricks.ResProvider r0 = r8.mResProvider     // Catch:{ FileNotFoundException -> 0x0025 }
            java.io.InputStream r6 = r0.open(r2)     // Catch:{ FileNotFoundException -> 0x0025 }
        L_0x0025:
            if (r6 == 0) goto L_0x003c
            java.lang.String r1 = "using solid xz dex store at %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x0051 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x0051 }
            com.facebook.common.dextricks.SolidXzInputDexIterator r2 = new com.facebook.common.dextricks.SolidXzInputDexIterator     // Catch:{ all -> 0x0051 }
            com.facebook.superpack.ditto.DittoPatch r4 = X.AnonymousClass096.A00(r12)     // Catch:{ all -> 0x0051 }
            com.facebook.common.dextricks.ResProvider r5 = r8.mResProvider     // Catch:{ all -> 0x0051 }
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0051 }
            return r2
        L_0x003c:
            java.lang.String r1 = "using discrete file inputs for store, no file at %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x0051 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x0051 }
            com.facebook.common.dextricks.DiscreteFileInputDexIterator r2 = new com.facebook.common.dextricks.DiscreteFileInputDexIterator     // Catch:{ all -> 0x0051 }
            com.facebook.superpack.ditto.DittoPatch r1 = X.AnonymousClass096.A00(r12)     // Catch:{ all -> 0x0051 }
            com.facebook.common.dextricks.ResProvider r0 = r8.mResProvider     // Catch:{ all -> 0x0051 }
            r2.<init>(r10, r1, r0)     // Catch:{ all -> 0x0051 }
            return r2
        L_0x0051:
            r0 = move-exception
            if (r6 == 0) goto L_0x0057
            com.facebook.common.dextricks.Fs.safeClose(r6)
        L_0x0057:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexIteratorFactory.openDexIterator(java.lang.String, com.facebook.common.dextricks.DexManifest, X.00c, android.content.Context):com.facebook.common.dextricks.InputDexIterator");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0049, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r1 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.common.dextricks.SuperpackInputDexIterator openSuperpackDexIterator(com.facebook.common.dextricks.DexManifest r7, X.C000000c r8, android.content.Context r9) {
        /*
            r6 = this;
            com.facebook.common.dextricks.SuperpackInputDexIterator$Builder r2 = new com.facebook.common.dextricks.SuperpackInputDexIterator$Builder
            r2.<init>(r7)
            r2.setTracer(r8)
            java.lang.String r5 = com.facebook.common.dextricks.SuperpackInputDexIterator.getArchiveExtension(r2)
            r4 = 0
            r3 = 0
        L_0x000e:
            int r0 = r7.superpackFiles
            if (r3 >= r0) goto L_0x0024
            com.facebook.common.dextricks.ResProvider r1 = r6.mResProvider
            java.lang.String r0 = "store-"
            java.lang.String r0 = X.AnonymousClass08S.A0A(r0, r3, r5)
            java.io.InputStream r0 = r1.open(r0)
            r2.addRawArchive(r0)
            int r3 = r3 + 1
            goto L_0x000e
        L_0x0024:
            com.facebook.common.dextricks.DexManifest$Dex[] r0 = r7.dexes
            int r0 = r0.length
            if (r4 >= r0) goto L_0x0033
            int r0 = r7.superpackFiles
            int r0 = r4 % r0
            r2.assignDexToArchive(r4, r0)
            int r4 = r4 + 1
            goto L_0x0024
        L_0x0033:
            boolean r0 = X.AnonymousClass096.A01()
            if (r0 == 0) goto L_0x0055
            r0 = 47448065(0x2d40001, float:3.1150603E-37)
            X.02K r1 = r8.AOr(r0)
            com.facebook.superpack.ditto.DittoPatch r0 = X.AnonymousClass096.A00(r9)     // Catch:{ all -> 0x0047 }
            r2.patch = r0     // Catch:{ all -> 0x0047 }
            goto L_0x0050
        L_0x0047:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            if (r1 == 0) goto L_0x004f
            r1.close()     // Catch:{ all -> 0x004f }
        L_0x004f:
            throw r0
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()
        L_0x0055:
            com.facebook.common.dextricks.SuperpackInputDexIterator r0 = r2.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexIteratorFactory.openSuperpackDexIterator(com.facebook.common.dextricks.DexManifest, X.00c, android.content.Context):com.facebook.common.dextricks.SuperpackInputDexIterator");
    }

    public DexIteratorFactory(ResProvider resProvider) {
        this.mResProvider = resProvider;
    }
}
