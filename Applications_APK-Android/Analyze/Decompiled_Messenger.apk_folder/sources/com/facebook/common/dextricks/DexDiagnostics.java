package com.facebook.common.dextricks;

import android.content.Context;
import android.os.Process;
import android.util.Log;
import java.io.File;

public class DexDiagnostics {
    public static final String APPLICATION_UID = "application_uid";
    public static final String DEX_DIRECTORIES = "dex_directories";
    public static final String DIRECTORY_OWNER_UID = "directory_owner_uid";
    public static final String LOADAVG_FILE = "loadavg_file";
    public static final String LOGCAT_DATA = "logcat_dump";
    public static final String MOUNTS_FILE = "mounts_file";
    public static final int OWNER_UNAVAILABLE = -2;
    public static final String UPTIME_FILE = "uptime_file";
    private final String mDataDirectory;

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String readProcFile(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            r1 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ all -> 0x003a }
            r4.<init>(r6)     // Catch:{ all -> 0x003a }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x003e }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ all -> 0x003e }
            r0.<init>(r4)     // Catch:{ all -> 0x003e }
            r3.<init>(r0)     // Catch:{ all -> 0x003e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0038 }
            r2.<init>()     // Catch:{ all -> 0x0038 }
        L_0x0015:
            java.lang.String r1 = r3.readLine()     // Catch:{ all -> 0x0038 }
            if (r1 == 0) goto L_0x002d
            if (r7 == 0) goto L_0x0024
            boolean r0 = r1.contains(r7)     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x0024
            goto L_0x0015
        L_0x0024:
            r2.append(r1)     // Catch:{ all -> 0x0038 }
            java.lang.String r0 = "\t"
            r2.append(r0)     // Catch:{ all -> 0x0038 }
            goto L_0x0015
        L_0x002d:
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0038 }
            r4.close()
            r3.close()
            return r0
        L_0x0038:
            r0 = move-exception
            goto L_0x0040
        L_0x003a:
            r0 = move-exception
            r3 = r1
            r4 = r1
            goto L_0x0040
        L_0x003e:
            r0 = move-exception
            r3 = r1
        L_0x0040:
            if (r4 == 0) goto L_0x0045
            r4.close()
        L_0x0045:
            if (r3 == 0) goto L_0x004a
            r3.close()
        L_0x004a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexDiagnostics.readProcFile(java.lang.String, java.lang.String):java.lang.String");
    }

    public String dumpAllDexDirectories() {
        String str = this.mDataDirectory;
        if (str == null) {
            return "n/a";
        }
        StringBuilder sb = new StringBuilder();
        dumpFileMetadata(new File(str), sb);
        return sb.toString();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:(3:19|20|(1:22))|26|27|28|29|37|(3:(2:41|65)(1:66)|42|38)|64) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x007f */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b4 A[SYNTHETIC, Splitter:B:45:0x00b4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String dumpLogcatData() {
        /*
            r10 = this;
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 16
            if (r1 < r0) goto L_0x00e2
            r4 = 0
            X.0OK r5 = new X.0OK     // Catch:{ RuntimeException -> 0x00d2 }
            r5.<init>()     // Catch:{ RuntimeException -> 0x00d2 }
            java.lang.String r6 = "unexpected error"
            X.0OJ r2 = r5.A00     // Catch:{ RuntimeException -> 0x00d2 }
            monitor-enter(r2)     // Catch:{ RuntimeException -> 0x00d2 }
            java.lang.Integer r1 = r2.A02     // Catch:{ all -> 0x00cf }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x00cf }
            if (r1 != r0) goto L_0x00c7
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x00cf }
            r2.A02 = r0     // Catch:{ all -> 0x00cf }
            monitor-exit(r2)     // Catch:{ all -> 0x00cf }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ RuntimeException -> 0x00d2 }
            r7.<init>()     // Catch:{ RuntimeException -> 0x00d2 }
            java.lang.String r0 = "logcat"
            r7.add(r0)     // Catch:{ RuntimeException -> 0x00d2 }
            java.util.List r0 = r2.A01     // Catch:{ RuntimeException -> 0x00d2 }
            r7.addAll(r0)     // Catch:{ RuntimeException -> 0x00d2 }
            java.lang.ProcessBuilder r1 = new java.lang.ProcessBuilder     // Catch:{ IOException -> 0x00b8 }
            java.lang.String[] r0 = new java.lang.String[r4]     // Catch:{ IOException -> 0x00b8 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x00b8 }
            java.lang.ProcessBuilder r0 = r1.command(r7)     // Catch:{ IOException -> 0x00b8 }
            java.lang.Process r0 = r0.start()     // Catch:{ IOException -> 0x00b8 }
            r2.A00 = r0     // Catch:{ IOException -> 0x00b8 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ RuntimeException -> 0x00d2 }
            X.0OI r0 = new X.0OI     // Catch:{ RuntimeException -> 0x00d2 }
            r0.<init>(r2)     // Catch:{ RuntimeException -> 0x00d2 }
            r1.<init>(r0)     // Catch:{ RuntimeException -> 0x00d2 }
            r1.start()     // Catch:{ RuntimeException -> 0x00d2 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ RuntimeException -> 0x00d2 }
            r2.<init>()     // Catch:{ RuntimeException -> 0x00d2 }
            r9 = 0
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            X.0OJ r0 = r5.A00     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.Process r0 = r0.A00     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            java.lang.String r0 = "US-ASCII"
            r7.<init>(r1, r0)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
            r8.<init>(r7)     // Catch:{ UnsupportedEncodingException -> 0x0088 }
        L_0x0068:
            java.lang.String r0 = r8.readLine()     // Catch:{ IOException -> 0x0072 }
            if (r0 == 0) goto L_0x007c
            r2.add(r0)     // Catch:{ IOException -> 0x0072 }
            goto L_0x0068
        L_0x0072:
            r1 = move-exception
            java.lang.Class r0 = X.AnonymousClass0OK.A01     // Catch:{ all -> 0x0085 }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x0085 }
            android.util.Log.e(r0, r6, r1)     // Catch:{ all -> 0x0085 }
        L_0x007c:
            r8.close()     // Catch:{ IOException -> 0x007f }
        L_0x007f:
            X.0OJ r0 = r5.A00     // Catch:{ RuntimeException -> 0x00d2 }
            r0.A00()     // Catch:{ RuntimeException -> 0x00d2 }
            goto L_0x0097
        L_0x0085:
            r1 = move-exception
            r9 = r8
            goto L_0x00b2
        L_0x0088:
            r1 = move-exception
            java.lang.Class r0 = X.AnonymousClass0OK.A01     // Catch:{ all -> 0x00b1 }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x00b1 }
            android.util.Log.e(r0, r6, r1)     // Catch:{ all -> 0x00b1 }
            X.0OJ r0 = r5.A00     // Catch:{ RuntimeException -> 0x00d2 }
            r0.A00()     // Catch:{ RuntimeException -> 0x00d2 }
        L_0x0097:
            r1 = 0
        L_0x0098:
            int r0 = r2.size()     // Catch:{ RuntimeException -> 0x00d2 }
            if (r1 >= r0) goto L_0x00e2
            if (r1 == 0) goto L_0x00a5
            java.lang.String r0 = "\n"
            r3.append(r0)     // Catch:{ RuntimeException -> 0x00d2 }
        L_0x00a5:
            java.lang.Object r0 = r2.get(r1)     // Catch:{ RuntimeException -> 0x00d2 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ RuntimeException -> 0x00d2 }
            r3.append(r0)     // Catch:{ RuntimeException -> 0x00d2 }
            int r1 = r1 + 1
            goto L_0x0098
        L_0x00b1:
            r1 = move-exception
        L_0x00b2:
            if (r9 == 0) goto L_0x00c1
            r9.close()     // Catch:{ IOException -> 0x00c1 }
            goto L_0x00c1
        L_0x00b8:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ RuntimeException -> 0x00d2 }
            java.lang.String r0 = "unable to start logcat process"
            r1.<init>(r0, r2)     // Catch:{ RuntimeException -> 0x00d2 }
            goto L_0x00c6
        L_0x00c1:
            X.0OJ r0 = r5.A00     // Catch:{ RuntimeException -> 0x00d2 }
            r0.A00()     // Catch:{ RuntimeException -> 0x00d2 }
        L_0x00c6:
            throw r1     // Catch:{ RuntimeException -> 0x00d2 }
        L_0x00c7:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00cf }
            java.lang.String r0 = "Cannot start logcat process twice"
            r1.<init>(r0)     // Catch:{ all -> 0x00cf }
            throw r1     // Catch:{ all -> 0x00cf }
        L_0x00cf:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00cf }
            throw r0     // Catch:{ RuntimeException -> 0x00d2 }
        L_0x00d2:
            r1 = move-exception
            r3.setLength(r4)
            java.lang.String r0 = "Exception during logcat dump: "
            r3.append(r0)
            java.lang.String r0 = r1.toString()
            r3.append(r0)
        L_0x00e2:
            java.lang.String r0 = r3.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexDiagnostics.dumpLogcatData():java.lang.String");
    }

    public int getDirectoryOwnerUid() {
        String str = this.mDataDirectory;
        if (str == null) {
            return -2;
        }
        try {
            return DalvikInternals.getOwnerUid(str);
        } catch (Throwable th) {
            Log.e("DexDiagnostics", "Unable to read directory owner uid", th);
            return -2;
        }
    }

    public DexDiagnostics(Context context) {
        String str;
        try {
            str = context.getApplicationInfo().dataDir;
        } catch (Throwable th) {
            Log.w("DexDiagnostics", "unable to find app data directory", th);
            str = null;
        }
        this.mDataDirectory = str;
    }

    private static StringBuilder dumpFileMetadata(File file, StringBuilder sb) {
        sb.append(file.getAbsolutePath());
        if (!file.exists()) {
            sb.append(" (deleted)\n");
        } else if (file.isDirectory()) {
            sb.append("\n");
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File dumpFileMetadata : listFiles) {
                    dumpFileMetadata(dumpFileMetadata, sb);
                }
            }
        } else {
            sb.append(" s:");
            sb.append(file.length());
            sb.append(" m:");
            sb.append(file.lastModified());
            sb.append("\n");
            return sb;
        }
        return sb;
    }

    public static String getDataDirectory(Context context) {
        return context.getApplicationInfo().dataDir;
    }

    public boolean isDirectoryOwnedByMe() {
        int directoryOwnerUid = getDirectoryOwnerUid();
        if (directoryOwnerUid == Process.myUid() || directoryOwnerUid == -2) {
            return true;
        }
        return false;
    }
}
