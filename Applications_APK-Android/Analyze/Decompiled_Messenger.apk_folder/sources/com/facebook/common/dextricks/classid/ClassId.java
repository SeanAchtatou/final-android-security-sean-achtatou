package com.facebook.common.dextricks.classid;

import X.AnonymousClass01q;
import X.AnonymousClass1Y3;
import android.os.Build;
import com.android.dex.Dex;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.ConcurrentHashMap;

public class ClassId {
    public static Field comAndroidDexDex_data;
    public static Field javaLangClass_dexCache;
    public static Field javaLangClass_dexClassDefIndex;
    public static Method javaLangClass_getDex;
    public static Method javaLangClass_getDexClassDefIndex;
    public static Field javaLangDexCache_dexFile;
    public static Method javaLangDexCache_getDex;
    public static final ConcurrentHashMap sDexKeyToDexSignature = new ConcurrentHashMap(0, 0.9f);
    public static final boolean sInitialized;

    private static native int getSignatureFromDexFile_8_0_0(long j);

    private static native int getSignatureFromDexFile_8_1_0(long j);

    private static native int getSignatureFromDexFile_9_0_0(long j);

    static {
        boolean z;
        AnonymousClass01q.A08("classid");
        Class<ClassId> cls = ClassId.class;
        synchronized (cls) {
            Class<Class> cls2 = Class.class;
            try {
                Field declaredField = Class.forName("com.android.dex.Dex").getDeclaredField("data");
                declaredField.setAccessible(true);
                comAndroidDexDex_data = declaredField;
            } catch (Exception unused) {
            }
            try {
                Field declaredField2 = cls2.getDeclaredField("dexClassDefIndex");
                Field declaredField3 = cls2.getDeclaredField("dexCache");
                Class<?> cls3 = Class.forName("java.lang.DexCache");
                declaredField2.setAccessible(true);
                declaredField3.setAccessible(true);
                javaLangClass_dexClassDefIndex = declaredField2;
                javaLangClass_dexCache = declaredField3;
                try {
                    Method declaredMethod = cls3.getDeclaredMethod("getDex", new Class[0]);
                    declaredMethod.setAccessible(true);
                    javaLangDexCache_getDex = declaredMethod;
                } catch (Exception unused2) {
                    Field declaredField4 = cls3.getDeclaredField("dexFile");
                    declaredField4.setAccessible(true);
                    javaLangDexCache_dexFile = declaredField4;
                }
                getClassDef(cls);
                if (getDexSignature(cls) == 0) {
                    throw new UnsupportedOperationException();
                }
            } catch (Exception unused3) {
                Method declaredMethod2 = cls2.getDeclaredMethod("getDexClassDefIndex", new Class[0]);
                Method declaredMethod3 = cls2.getDeclaredMethod("getDex", new Class[0]);
                declaredMethod2.setAccessible(true);
                declaredMethod3.setAccessible(true);
                javaLangClass_getDexClassDefIndex = declaredMethod2;
                javaLangClass_getDex = declaredMethod3;
                getClassDef(cls);
                if (getDexSignature(cls) == 0) {
                    throw new UnsupportedOperationException();
                }
            } catch (Exception unused4) {
                z = false;
            }
        }
        z = true;
        sInitialized = z;
    }

    private static int getClassDef(Class cls) {
        try {
            Field field = javaLangClass_dexClassDefIndex;
            if (field != null) {
                return ((Integer) field.get(cls)).intValue();
            }
            Method method = javaLangClass_getDexClassDefIndex;
            if (method != null) {
                return ((Integer) method.invoke(cls, new Object[0])).intValue();
            }
            throw new IllegalStateException();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static long getClassId(Class cls) {
        if (!sInitialized) {
            return -1;
        }
        return (((long) getDexSignature(cls)) & 4294967295L) | (((long) getClassDef(cls)) << 32);
    }

    private static int getDexSignature(Class cls) {
        int i;
        try {
            if (javaLangDexCache_dexFile != null) {
                Object obj = javaLangClass_dexCache.get(cls);
                if (obj == null) {
                    return 0;
                }
                Integer num = (Integer) sDexKeyToDexSignature.get(obj);
                if (num == null) {
                    long j = javaLangDexCache_dexFile.getLong(obj);
                    switch (Build.VERSION.SDK_INT) {
                        case AnonymousClass1Y3.A08 /*26*/:
                            i = getSignatureFromDexFile_8_0_0(j);
                            break;
                        case AnonymousClass1Y3.A09 /*27*/:
                            i = getSignatureFromDexFile_8_1_0(j);
                            break;
                        case 28:
                        case 29:
                            i = getSignatureFromDexFile_9_0_0(j);
                            break;
                        default:
                            i = 0;
                            break;
                    }
                    num = Integer.valueOf(i);
                    sDexKeyToDexSignature.put(obj, num);
                }
                return num.intValue();
            }
            Field field = javaLangClass_dexCache;
            if (field != null) {
                Object obj2 = field.get(cls);
                if (obj2 == null) {
                    return 0;
                }
                Integer num2 = (Integer) sDexKeyToDexSignature.get(obj2);
                if (num2 == null) {
                    num2 = Integer.valueOf(getSignatureForDex((Dex) javaLangDexCache_getDex.invoke(obj2, new Object[0])));
                    sDexKeyToDexSignature.put(obj2, num2);
                }
                return num2.intValue();
            }
            Method method = javaLangClass_getDex;
            if (method != null) {
                Dex dex = (Dex) method.invoke(cls, new Object[0]);
                if (dex == null) {
                    return 0;
                }
                Integer num3 = (Integer) sDexKeyToDexSignature.get(dex);
                if (num3 == null) {
                    num3 = Integer.valueOf(getSignatureForDex(dex));
                    sDexKeyToDexSignature.put(dex, num3);
                }
                return num3.intValue();
            }
            throw new IllegalStateException();
        } catch (IOException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static int getSignatureForDex(Dex dex) {
        ByteBuffer byteBuffer;
        Field field = comAndroidDexDex_data;
        if (!(field == null || (byteBuffer = (ByteBuffer) field.get(dex)) == null)) {
            ByteBuffer duplicate = byteBuffer.duplicate();
            if (duplicate.limit() >= 16) {
                duplicate.order(ByteOrder.LITTLE_ENDIAN);
                duplicate.position(12);
                int i = duplicate.getInt();
                if (i != 0) {
                    return i;
                }
            }
        }
        return dex.open(12).readInt();
    }
}
