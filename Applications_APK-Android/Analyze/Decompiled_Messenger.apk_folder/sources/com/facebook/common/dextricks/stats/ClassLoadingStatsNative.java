package com.facebook.common.dextricks.stats;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public final class ClassLoadingStatsNative extends ClassLoadingStats {
    private final HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public native void decrementDexFileQueries();

    public native int getClassLoadsAttempted();

    public native int getClassLoadsFailed();

    public native int getDexFileQueries();

    public native int getIncorrectDfaGuesses();

    public native int getLocatorAssistedClassLoads();

    public native int getTurboLoaderClassLocationFailures();

    public native int getTurboLoaderClassLocationSuccesses();

    public native int getTurboLoaderMapGenerationFailures();

    public native int getTurboLoaderMapGenerationSuccesses();

    public native void incrementClassLoadsAttempted();

    public native void incrementClassLoadsFailed();

    public native void incrementDexFileQueries(int i);

    public native void incrementIncorrectDfaGuesses();

    public native void incrementLocatorAssistedClassLoads();

    public native void incrementTurboLoaderClassLocationFailures();

    public native void incrementTurboLoaderClassLocationSuccesses();

    public native void incrementTurboLoaderMapGenerationFailures();

    public native void incrementTurboLoaderMapGenerationSuccesses();

    static {
        AnonymousClass01q.A08("dextricks");
    }
}
