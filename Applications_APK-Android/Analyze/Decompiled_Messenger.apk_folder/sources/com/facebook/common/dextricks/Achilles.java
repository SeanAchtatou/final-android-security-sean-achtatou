package com.facebook.common.dextricks;

import X.AnonymousClass01q;
import java.lang.Thread;

public class Achilles implements Runnable {
    public final Object ctx;
    public final long func;

    public static native long derp();

    public native void run();

    static {
        AnonymousClass01q.A08("achilles-jni");
    }

    public static void attack(Object obj, long j) {
        Thread thread = new Thread(new Achilles(obj, j));
        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                DexTricksErrorReporter.reportSampledSoftError("Achilles", "Exception in Achilles thread", th);
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            Mlog.safeFmt("Failed to join on achilles thread: %s", e);
        }
    }

    public Achilles(Object obj, long j) {
        this.ctx = obj;
        this.func = j;
    }
}
