package com.facebook.common.dextricks;

import android.os.Build;
import android.os.Process;
import java.io.Closeable;

public final class Prio {
    public final int cpuPriority;
    public final int ioPriority;

    public final class With implements Closeable {
        private final int savedCpuPriority;
        private final int savedIoPriority;

        public With() {
            int ioprio_get;
            int threadPriority;
            if (Build.VERSION.SDK_INT >= 26) {
                this.savedIoPriority = Integer.MIN_VALUE;
                this.savedCpuPriority = Integer.MIN_VALUE;
                return;
            }
            int myTid = Process.myTid();
            if (Prio.this.ioPriority == Integer.MIN_VALUE) {
                ioprio_get = Integer.MIN_VALUE;
            } else {
                ioprio_get = DalvikInternals.ioprio_get(1, 0);
            }
            this.savedIoPriority = ioprio_get;
            if (Prio.this.cpuPriority == Integer.MIN_VALUE) {
                threadPriority = Integer.MIN_VALUE;
            } else {
                threadPriority = Process.getThreadPriority(myTid);
            }
            this.savedCpuPriority = threadPriority;
            try {
                int i = Prio.this.cpuPriority;
                if (i != Integer.MIN_VALUE) {
                    Process.setThreadPriority(myTid, i);
                }
                int i2 = Prio.this.ioPriority;
                if (i2 != Integer.MIN_VALUE) {
                    DalvikInternals.ioprio_set(1, 0, i2);
                }
            } catch (Throwable th) {
                close();
                throw th;
            }
        }

        public void close() {
            int i = this.savedIoPriority;
            if (i != Integer.MIN_VALUE) {
                DalvikInternals.ioprio_set(1, 0, i);
            }
            if (this.savedCpuPriority != Integer.MIN_VALUE) {
                Process.setThreadPriority(Process.myTid(), this.savedCpuPriority);
            }
        }
    }

    public static Prio lowest() {
        return new Prio(DalvikInternals.IOPRIO_BACKGROUND, 19);
    }

    public static Prio unchanged() {
        return new Prio(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public Prio ioOnly() {
        return new Prio(this.ioPriority, Integer.MIN_VALUE);
    }

    public boolean isDefault() {
        if (this.ioPriority >= 0 || this.cpuPriority >= 0) {
            return false;
        }
        return true;
    }

    public With with() {
        return new With();
    }

    public Prio(int i, int i2) {
        this.ioPriority = i;
        this.cpuPriority = i2;
    }
}
