package com.facebook.common.dextricks;

import X.AnonymousClass08S;
import X.AnonymousClass095;
import android.content.Context;
import android.os.Build;
import java.io.File;
import java.io.InputStream;

public final class DexStoreUtils {
    public static int CANARY_IDX = 2;
    public static int HASH_IDX = 1;
    private static final String IGNORE_DIRTY_CHECK_PREFIX = "IGNORE_DIRTY_";
    private static final String MAIN_DEX_STORE_ID = "dex";
    public static final boolean OREO_OR_NEWER;
    public static final String SECONDARY_DEX_MANIFEST = "metadata.txt";

    public static String getMainDexStoreId() {
        return MAIN_DEX_STORE_ID;
    }

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 26) {
            z = true;
        }
        OREO_OR_NEWER = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0044 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List getDexInfoFromManifest(android.content.Context r6) {
        /*
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            android.content.res.AssetManager r1 = r6.getAssets()
            java.lang.String r0 = "secondary-program-dex-jars/metadata.txt"
            java.io.InputStream r4 = r1.open(r0)
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x0045 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ all -> 0x0045 }
            java.lang.String r0 = "UTF-8"
            r1.<init>(r4, r0)     // Catch:{ all -> 0x0045 }
            r3.<init>(r1)     // Catch:{ all -> 0x0045 }
        L_0x001b:
            java.lang.String r1 = r3.readLine()     // Catch:{ all -> 0x003e }
            if (r1 == 0) goto L_0x0037
            java.lang.String r0 = "."
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x003e }
            if (r0 != 0) goto L_0x001b
            java.lang.String r0 = " "
            java.lang.String[] r2 = r1.split(r0)     // Catch:{ all -> 0x003e }
            int r1 = r2.length     // Catch:{ all -> 0x003e }
            r0 = 3
            if (r1 != r0) goto L_0x001b
            r5.add(r2)     // Catch:{ all -> 0x003e }
            goto L_0x001b
        L_0x0037:
            r3.close()     // Catch:{ all -> 0x0045 }
            r4.close()
            return r5
        L_0x003e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0044 }
        L_0x0044:
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x0045:
            r0 = move-exception
            r4.close()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexStoreUtils.getDexInfoFromManifest(android.content.Context):java.util.List");
    }

    public static DexManifest getSecondaryDexManifest(ResProvider resProvider, boolean z) {
        return DexManifest.loadManifestFrom(resProvider, SECONDARY_DEX_MANIFEST, z);
    }

    public static boolean isIgnoreDirtyFile(File file) {
        if (file == null) {
            return false;
        }
        return isIgnoreDirtyFileName(file.getName());
    }

    public static boolean isIgnoreDirtyFileName(String str) {
        if (str == null || !str.startsWith(IGNORE_DIRTY_CHECK_PREFIX)) {
            return false;
        }
        return true;
    }

    public static boolean isMainDexStoreId(String str) {
        return MAIN_DEX_STORE_ID.equals(str);
    }

    public static boolean isSecondaryDexManifest(String str) {
        return SECONDARY_DEX_MANIFEST.equals(str);
    }

    public static File makeIgnoreDirtyCheckFile(File file, String str) {
        return new File(file, AnonymousClass08S.A0J(IGNORE_DIRTY_CHECK_PREFIX, str));
    }

    public static InputStream openSecondaryDexManifest(ResProvider resProvider) {
        return resProvider.open(SECONDARY_DEX_MANIFEST);
    }

    private DexStoreUtils() {
    }

    public static File createNewMainDexStoreReferencePgoProfile(Context context) {
        return AnonymousClass095.A00(context).A05(getMainDexStoreLocation(context));
    }

    public static File getCurrentMainDexStorePgoProfile(Context context) {
        File mainDexStoreReferencePgoProfile = getMainDexStoreReferencePgoProfile(context);
        if (mainDexStoreReferencePgoProfile == null || !mainDexStoreReferencePgoProfile.exists()) {
            return AnonymousClass095.A00(context).A04();
        }
        return mainDexStoreReferencePgoProfile;
    }

    public static File getMainDexStoreLocation(Context context) {
        String realpath;
        String str = context.getApplicationInfo().dataDir;
        if (OREO_OR_NEWER) {
            realpath = str;
        } else {
            realpath = DalvikInternals.realpath(str);
        }
        Fs.deleteRecursiveNoThrow(new File(AnonymousClass08S.A0J(realpath, "/app_secondary_program_dex")));
        Fs.deleteRecursiveNoThrow(new File(AnonymousClass08S.A0J(realpath, "/app_secondary_program_dex_opt")));
        if (!str.equals(realpath)) {
            Mlog.safeFmt("resolved non-canonical data directory %s to %s", str, realpath);
        }
        return new File(realpath, MAIN_DEX_STORE_ID);
    }

    public static File getMainDexStoreReferencePgoProfile(Context context) {
        File mainDexStoreLocation = getMainDexStoreLocation(context);
        AnonymousClass095.A00(context);
        if (mainDexStoreLocation != null) {
            return new File(mainDexStoreLocation, "art_pgo_ref_profile.prof");
        }
        throw new IllegalArgumentException();
    }
}
