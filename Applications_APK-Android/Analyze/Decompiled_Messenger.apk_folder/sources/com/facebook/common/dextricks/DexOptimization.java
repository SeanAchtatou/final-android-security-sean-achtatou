package com.facebook.common.dextricks;

import X.AnonymousClass02C;
import X.AnonymousClass08S;
import X.AnonymousClass08X;
import X.AnonymousClass0E1;
import X.AnonymousClass0F3;
import X.C000700l;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.DeadObjectException;
import android.os.Messenger;
import com.facebook.common.dextricks.OptimizationConfiguration;
import java.io.File;

public final class DexOptimization {
    private static final String ACTION_OPTIMIZE = "com.facebook.dexopt";
    private static final String ACTION_OPTIMIZE_PAUSE = "com.facebook.dexopt-pause";
    private static final int JOB_ID = -87105851;
    private static final String OPTIMIZE_UNPAUSE_TIME = "com.facebook.dexopt-unpause-time";
    private static final String OPT_KEY_CLIENT = "client";
    private static final String OPT_KEY_DEX_STORE_ROOT = "dexStoreRoot";
    public static final String PROCESS_NAME = "optsvc";

    public final class Client {
        private static volatile Messenger sOptListener;

        private static boolean getShouldDisableRestartProcessAfterDexOpt(Context context, DexStore dexStore, AnonymousClass0F3 r5) {
            return AnonymousClass08X.A08(context, Experiments.DO_NOT_RESTART_PROCESS_AFTER_DEX_OPT, false) || (r5.A0t && dexStore.isReoptimization(context));
        }

        private static Messenger getMessenger(Context context, DexStore dexStore) {
            if (sOptListener == null) {
                synchronized (Client.class) {
                    if (sOptListener == null) {
                        sOptListener = constructClientMessenger(context, dexStore);
                    }
                }
            }
            return sOptListener;
        }

        public static void pauseOptimization(Context context, int i) {
            Intent intent = new Intent(DexOptimization.ACTION_OPTIMIZE_PAUSE);
            intent.putExtra(DexOptimization.OPTIMIZE_UNPAUSE_TIME, System.nanoTime() + ((long) (i * 1000000)));
            context.sendBroadcast(intent);
            Mlog.safeFmt("send pause-optimization broadcast", new Object[0]);
        }

        public static void startBackgroundOptimization(Context context, DexStore dexStore) {
            try {
                Mlog.safeFmt("starting background optimization", new Object[0]);
                AnonymousClass0E1.enqueueWork(context, Service.class, (int) DexOptimization.JOB_ID, new Intent(DexOptimization.ACTION_OPTIMIZE).putExtra(DexOptimization.OPT_KEY_DEX_STORE_ROOT, dexStore.root.getAbsolutePath()).putExtra(DexOptimization.OPT_KEY_CLIENT, getMessenger(context, dexStore)));
            } catch (SecurityException e) {
                Mlog.w("Failure to start DexOptimization.Service", e);
            } catch (RuntimeException e2) {
                if (e2.getCause() instanceof DeadObjectException) {
                    Mlog.w("Failure to start DexOptimization.Service", e2);
                    return;
                }
                throw e2;
            }
        }

        private static Messenger constructClientMessenger(Context context, DexStore dexStore) {
            AnonymousClass0F3 A02 = AnonymousClass0F3.A02(context);
            return new Messenger(new DexOptimizationMessageHandler(context, getShouldDisableRestartProcessAfterDexOpt(context, dexStore, A02), A02.A0b, A02.A21));
        }
    }

    public final class Service extends AnonymousClass0E1 {
        public boolean isScreenOn;
        private OptSvcBroadcastReceiver mReceiver;
        public long unpauseTime;

        public final class OptSvcOptimizationConfigurationProvider extends OptimizationConfiguration.Provider {
            private OptimizationConfiguration mPausedConfig;
            private OptimizationConfiguration mScreenOffConfig;

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public OptSvcOptimizationConfigurationProvider() {
                /*
                    r4 = this;
                    com.facebook.common.dextricks.DexOptimization.Service.this = r5
                    com.facebook.common.dextricks.OptimizationConfiguration$Builder r1 = new com.facebook.common.dextricks.OptimizationConfiguration$Builder
                    r1.<init>()
                    com.facebook.common.dextricks.Prio r0 = com.facebook.common.dextricks.Prio.lowest()
                    r1.mPrio = r0
                    r0 = 1
                    r1.setInBackground(r0)
                    r3 = 300(0x12c, float:4.2E-43)
                    r1.mOptTimeSliceMs = r3
                    r0 = 1000(0x3e8, float:1.401E-42)
                    r1.mYieldTimeSliceMs = r0
                    com.facebook.common.dextricks.OptimizationConfiguration r0 = r1.build()
                    r4.<init>(r0)
                    com.facebook.common.dextricks.OptimizationConfiguration$Builder r1 = new com.facebook.common.dextricks.OptimizationConfiguration$Builder
                    com.facebook.common.dextricks.OptimizationConfiguration r2 = r4.baseline
                    r1.<init>(r2)
                    r0 = 900(0x384, float:1.261E-42)
                    r1.mOptTimeSliceMs = r0
                    r1.mYieldTimeSliceMs = r3
                    com.facebook.common.dextricks.OptimizationConfiguration r0 = r1.build()
                    r4.mScreenOffConfig = r0
                    com.facebook.common.dextricks.OptimizationConfiguration$Builder r1 = new com.facebook.common.dextricks.OptimizationConfiguration$Builder
                    r1.<init>(r2)
                    r0 = 0
                    r1.mOptTimeSliceMs = r0
                    r0 = 100
                    r1.mYieldTimeSliceMs = r0
                    com.facebook.common.dextricks.OptimizationConfiguration r0 = r1.build()
                    r4.mPausedConfig = r0
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexOptimization.Service.OptSvcOptimizationConfigurationProvider.<init>(com.facebook.common.dextricks.DexOptimization$Service):void");
            }

            public OptimizationConfiguration getInstantaneous() {
                long nanoTime = System.nanoTime();
                Service service = Service.this;
                if (nanoTime < service.unpauseTime) {
                    return this.mPausedConfig;
                }
                if (service.isScreenOn) {
                    return this.baseline;
                }
                return this.mScreenOffConfig;
            }
        }

        public final class OptSvcBroadcastReceiver extends BroadcastReceiver {
            public void onReceive(Context context, Intent intent) {
                int A01 = C000700l.A01(-1965829126);
                if (intent != null) {
                    String action = intent.getAction();
                    if ("android.intent.action.SCREEN_ON".equals(action)) {
                        Service.this.isScreenOn = true;
                        Mlog.safeFmt("[optsvc] noticed screen on", new Object[0]);
                    } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                        Service.this.isScreenOn = false;
                        Mlog.safeFmt("[optsvc] noticed screen off", new Object[0]);
                    } else if (DexOptimization.ACTION_OPTIMIZE_PAUSE.equals(action)) {
                        long longExtra = intent.getLongExtra(DexOptimization.OPTIMIZE_UNPAUSE_TIME, -1);
                        if (longExtra < 0) {
                            Mlog.w("bogus pause broadcast received", new Object[0]);
                            C000700l.A0D(intent, -1976892630, A01);
                            return;
                        }
                        Service service = Service.this;
                        long max = Math.max(service.unpauseTime, longExtra);
                        service.unpauseTime = max;
                        Mlog.safeFmt("pause broadcast received: will unpause at time %s (%s ms from now)", Long.valueOf(max), Long.valueOf((System.nanoTime() - max) / 1000000));
                    }
                }
                C000700l.A0D(intent, -1358566438, A01);
            }

            public OptSvcBroadcastReceiver() {
            }
        }

        public void onHandleWork(Intent intent) {
            SocketLock socketLock;
            if (DexOptimization.ACTION_OPTIMIZE.equals(intent.getAction())) {
                Mlog.safeFmt("optsvc received opt intent", new Object[0]);
                String stringExtra = intent.getStringExtra(DexOptimization.OPT_KEY_DEX_STORE_ROOT);
                Messenger messenger = (Messenger) intent.getParcelableExtra(DexOptimization.OPT_KEY_CLIENT);
                DexOptimizationMessageHandler.send(messenger, 1, 0);
                try {
                    socketLock = new SocketLock("com.facebook.dexopt.lock");
                    while (!socketLock.tryAcquire()) {
                        try {
                            Mlog.safeFmt("another application is optimizing; waiting", new Object[0]);
                            Thread.sleep(1000);
                        } catch (InterruptedException unused) {
                            Mlog.safeFmt("optimization canceled for dex store %s", stringExtra);
                            DexOptimizationMessageHandler.send(messenger, 2, 2);
                            Mlog.safeFmt("optsvc opt work finished", new Object[0]);
                            Fs.safeClose(socketLock);
                        } catch (Throwable th) {
                            th = th;
                            try {
                                Mlog.w(th, "optimization failed for dex store %s", stringExtra);
                                DexOptimizationMessageHandler.send(messenger, 2, 1);
                                Mlog.safeFmt("optsvc opt work finished", new Object[0]);
                                Fs.safeClose(socketLock);
                            } catch (Throwable th2) {
                                Mlog.safeFmt("optsvc opt work finished", new Object[0]);
                                Fs.safeClose(socketLock);
                                throw th2;
                            }
                        }
                    }
                    Mlog.safeFmt("acquired xappLock", new Object[0]);
                    DexStore findOpened = DexStore.findOpened(new File(stringExtra));
                    if (findOpened != null) {
                        findOpened.optimize(getApplicationContext(), new OptSvcOptimizationConfigurationProvider());
                        DexOptimizationMessageHandler.send(messenger, 2, 0);
                        Mlog.safeFmt("optsvc opt work finished", new Object[0]);
                        Fs.safeClose(socketLock);
                    }
                    throw new IllegalArgumentException(AnonymousClass08S.A0J("no such opened dex store: ", stringExtra));
                } catch (InterruptedException unused2) {
                    socketLock = null;
                    Mlog.safeFmt("optimization canceled for dex store %s", stringExtra);
                    DexOptimizationMessageHandler.send(messenger, 2, 2);
                    Mlog.safeFmt("optsvc opt work finished", new Object[0]);
                    Fs.safeClose(socketLock);
                } catch (Throwable th3) {
                    th = th3;
                    socketLock = null;
                    Mlog.w(th, "optimization failed for dex store %s", stringExtra);
                    DexOptimizationMessageHandler.send(messenger, 2, 1);
                    Mlog.safeFmt("optsvc opt work finished", new Object[0]);
                    Fs.safeClose(socketLock);
                }
            } else {
                Mlog.w("optsvc received intent with unknown action: %s", intent.getAction());
            }
        }

        public void onCreate() {
            int A00 = AnonymousClass02C.A00(this, 954401035);
            super.onCreate();
            Mlog.safeFmt("optsvc created", new Object[0]);
            this.isScreenOn = true;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction(DexOptimization.ACTION_OPTIMIZE_PAUSE);
            OptSvcBroadcastReceiver optSvcBroadcastReceiver = new OptSvcBroadcastReceiver();
            this.mReceiver = optSvcBroadcastReceiver;
            registerReceiver(optSvcBroadcastReceiver, intentFilter);
            this.mInterruptIfStopped = true;
            AnonymousClass02C.A02(1529376969, A00);
        }

        public void onDestroy() {
            int A04 = C000700l.A04(1172643408);
            Mlog.safeFmt("optsvc being shut down", new Object[0]);
            OptSvcBroadcastReceiver optSvcBroadcastReceiver = this.mReceiver;
            if (optSvcBroadcastReceiver != null) {
                unregisterReceiver(optSvcBroadcastReceiver);
                this.mReceiver = null;
            }
            super.onDestroy();
            C000700l.A0A(1823019804, A04);
        }
    }
}
