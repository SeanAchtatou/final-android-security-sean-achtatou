package com.facebook.common.dextricks;

import android.os.Build;
import android.util.Log;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class LogcatReader {
    private static final int DEFAULT_WAIT_TIME = 10000;
    private static final int GROUP_IDX_PID = 4;
    private static final int GROUP_IDX_TAG = 5;
    private static final int GROUP_IDX_TIMESTAMP = 1;
    private static final int GROUP_IDX_YEAR = 3;
    private static final Pattern LOGCAT_INFO_PARSER = Pattern.compile("^(((\\d{4})-)?\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\.\\d{3})\\s+(\\d+)\\s+\\d+\\s\\w\\s(\\w+)\\s:.*");
    private static final boolean MUST_PARSE_PID;
    private static final String TAG = "LogcatReader";
    private final String mTag;
    private final File mTmpDir;

    private static boolean shouldProcess(int i, String str, String str2) {
        if (str2 != null) {
            if (!MUST_PARSE_PID) {
                return true;
            }
            Matcher matcher = LOGCAT_INFO_PARSER.matcher(str2);
            if (matcher != null && matcher.matches()) {
                String group = matcher.group(4);
                if (matcher.group(5).equals(str)) {
                    try {
                        if (Integer.parseInt(group) == i) {
                            return true;
                        }
                    } catch (NumberFormatException e) {
                        Log.w(TAG, String.format("Could not process line since %s is not a number", group), e);
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public abstract void processLine(String str);

    public abstract void reset();

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT < 24) {
            z = true;
        }
        MUST_PARSE_PID = z;
    }

    public LogcatReader(String str, File file) {
        this.mTag = str;
        this.mTmpDir = file;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean readAndParseProcessFile(int r8, java.io.RandomAccessFile r9) {
        /*
            r7 = this;
            java.lang.String r6 = com.facebook.common.dextricks.Fs.readProgramOutputFile(r9)
            r5 = 0
            java.lang.String r4 = "LogcatReader"
            if (r6 != 0) goto L_0x000f
            java.lang.String r0 = "Cannot find logcat file to parse"
            android.util.Log.w(r4, r0)
            return r5
        L_0x000f:
            r1 = 0
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x003f }
            java.io.StringReader r0 = new java.io.StringReader     // Catch:{ all -> 0x003f }
            r0.<init>(r6)     // Catch:{ all -> 0x003f }
            r3.<init>(r0)     // Catch:{ all -> 0x003f }
            r2 = 1
        L_0x001b:
            java.lang.String r1 = r3.readLine()     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x002e
            java.lang.String r0 = r7.mTag     // Catch:{ all -> 0x003d }
            boolean r0 = shouldProcess(r8, r0, r1)     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x002c
            r7.processLine(r1)     // Catch:{ all -> 0x003d }
        L_0x002c:
            r5 = 1
            goto L_0x001b
        L_0x002e:
            if (r5 != 0) goto L_0x0039
            java.lang.String r0 = "Could not read out any logs. \n Read: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r6)     // Catch:{ all -> 0x003d }
            android.util.Log.w(r4, r0)     // Catch:{ all -> 0x003d }
        L_0x0039:
            r3.close()
            return r2
        L_0x003d:
            r0 = move-exception
            goto L_0x0041
        L_0x003f:
            r0 = move-exception
            r3 = r1
        L_0x0041:
            if (r3 == 0) goto L_0x0046
            r3.close()
        L_0x0046:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.LogcatReader.readAndParseProcessFile(int, java.io.RandomAccessFile):boolean");
    }

    public final boolean readAndParseProcess(int i) {
        return readAndParseProcess(i, DEFAULT_WAIT_TIME);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v3, resolved type: java.io.RandomAccessFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: com.facebook.forker.Process} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: com.facebook.forker.Process} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean readAndParseProcess(int r9, int r10) {
        /*
            r8 = this;
            java.lang.String r5 = "LogcatReader"
            r8.reset()
            com.facebook.forker.ProcessBuilder r2 = new com.facebook.forker.ProcessBuilder
            r7 = 0
            java.lang.String r1 = "-v"
            r3 = 1
            java.lang.String r0 = "threadtime"
            java.lang.String[] r1 = new java.lang.String[]{r1, r0}
            java.lang.String r0 = "/system/bin/logcat"
            r2.<init>(r0, r1)
            java.lang.String r1 = "-d"
            java.util.ArrayList r0 = r2.mArgv
            r0.add(r1)
            boolean r0 = com.facebook.common.dextricks.LogcatReader.MUST_PARSE_PID
            if (r0 != 0) goto L_0x003f
            java.lang.String r0 = "--pid="
            java.lang.String r1 = X.AnonymousClass08S.A09(r0, r9)
        L_0x0027:
            java.util.ArrayList r0 = r2.mArgv
            r0.add(r1)
            java.lang.String r0 = r8.mTag
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "%s:V"
            java.lang.String r1 = java.lang.String.format(r0, r1)
            java.util.ArrayList r0 = r2.mArgv
            r0.add(r1)
            r4 = 0
            goto L_0x0042
        L_0x003f:
            java.lang.String r1 = "-s"
            goto L_0x0027
        L_0x0042:
            java.io.File r0 = r8.mTmpDir     // Catch:{ IOException | InterruptedException -> 0x0098, all -> 0x00bf }
            java.io.RandomAccessFile r6 = com.facebook.common.dextricks.Fs.openUnlinkedTemporaryFile(r0)     // Catch:{ IOException | InterruptedException -> 0x0098, all -> 0x00bf }
            java.io.FileDescriptor r0 = r6.getFD()     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            int r1 = com.facebook.forker.Fd.fileno(r0)     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            int[] r0 = r2.mStreamDispositions     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            r0[r3] = r1     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            r2.toString()     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            com.facebook.forker.Process r4 = r2.create()     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            java.lang.System.currentTimeMillis()     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            r0 = 4
            if (r10 <= 0) goto L_0x0066
            int r2 = r4.waitFor(r10, r0)     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            goto L_0x006a
        L_0x0066:
            int r2 = r4.waitFor()     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
        L_0x006a:
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r0 = 0
            if (r2 != r1) goto L_0x0070
            r0 = 1
        L_0x0070:
            java.lang.System.currentTimeMillis()     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            if (r0 == 0) goto L_0x0085
            java.lang.String r1 = "Failed to run logcat for %s because we timed out"
            java.lang.String r0 = r8.mTag     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            android.util.Log.w(r5, r0)     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            goto L_0x00b2
        L_0x0085:
            boolean r0 = r8.readAndParseProcessFile(r9, r6)     // Catch:{ IOException | InterruptedException -> 0x0094, all -> 0x0092 }
            com.facebook.common.dextricks.Fs.safeClose(r6)
            if (r4 == 0) goto L_0x0091
            r4.destroy()
        L_0x0091:
            return r0
        L_0x0092:
            r0 = move-exception
            goto L_0x00c1
        L_0x0094:
            r3 = move-exception
            r2 = r4
            r4 = r6
            goto L_0x009a
        L_0x0098:
            r3 = move-exception
            r2 = r4
        L_0x009a:
            java.lang.String r1 = "Failed to run logcat for tag %s"
            java.lang.String r0 = r8.mTag     // Catch:{ all -> 0x00bb }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00bb }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ all -> 0x00bb }
            android.util.Log.w(r5, r0, r3)     // Catch:{ all -> 0x00bb }
            com.facebook.common.dextricks.Fs.safeClose(r4)
            if (r2 == 0) goto L_0x00ba
            r2.destroy()
            return r7
        L_0x00b2:
            com.facebook.common.dextricks.Fs.safeClose(r6)
            if (r4 == 0) goto L_0x00ba
            r4.destroy()
        L_0x00ba:
            return r7
        L_0x00bb:
            r0 = move-exception
            r6 = r4
            r4 = r2
            goto L_0x00c1
        L_0x00bf:
            r0 = move-exception
            r6 = r4
        L_0x00c1:
            com.facebook.common.dextricks.Fs.safeClose(r6)
            if (r4 == 0) goto L_0x00c9
            r4.destroy()
        L_0x00c9:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.LogcatReader.readAndParseProcess(int, int):boolean");
    }
}
