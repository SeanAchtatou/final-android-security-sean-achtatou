package com.facebook.common.dextricks;

import X.AnonymousClass018;
import X.AnonymousClass08Q;
import X.AnonymousClass08S;
import X.C000000c;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import com.facebook.common.dextricks.DexManifest;
import com.facebook.common.dextricks.DexStore;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public final class DexLibLoader {
    public static final int LOAD_ALL_ASYNC_OPTIMIZATION = 4;
    public static final int LOAD_ALL_BETA_BUILD = 1;
    public static final int LOAD_ALL_INSTRUMENTATION_TEST = 16;
    public static final int LOAD_ALL_OPEN_ONLY = 2;
    public static final int LOAD_SECONDARY = 8;
    public static boolean deoptTaint;
    private static DexErrorRecoveryInfo mDeri;
    private static DexStore sMainDexStore;

    public final class ApkResProvider extends ResProvider {
        private static final String SECONDARY_PROGRAM_DEX_JARS = "secondary-program-dex-jars";
        private ZipFile mApkZip;
        private final Context mContext;

        public void close() {
            Fs.safeClose(this.mApkZip);
        }

        public void markRootRelative() {
            if (this.mApkZip == null) {
                this.mApkZip = new ZipFile(this.mContext.getApplicationInfo().sourceDir);
            }
        }

        public InputStream open(String str) {
            ZipFile zipFile = this.mApkZip;
            if (zipFile == null || DexStoreUtils.SECONDARY_DEX_MANIFEST.equals(str)) {
                return this.mContext.getAssets().open(AnonymousClass08S.A0J("secondary-program-dex-jars/", str));
            }
            ZipEntry entry = zipFile.getEntry(str);
            if (entry != null) {
                return this.mApkZip.getInputStream(entry);
            }
            throw new FileNotFoundException(AnonymousClass08S.A0J("cannot find root-relative resource: ", str));
        }

        public ApkResProvider(Context context) {
            this.mContext = context;
        }
    }

    public final class ExoPackageResProvider extends ResProvider {
        private static final String EXOPACKAGE_DIR = "/data/local/tmp/exopackage";
        private final File mDirectory;

        public InputStream open(String str) {
            return new FileInputStream(new File(this.mDirectory, str));
        }

        public ExoPackageResProvider(Context context) {
            this.mDirectory = new File(AnonymousClass08S.A0P("/data/local/tmp/exopackage/", context.getPackageName(), "/secondary-dex"));
        }
    }

    public static synchronized long getLastCompilationTime(Context context) {
        long j;
        synchronized (DexLibLoader.class) {
            j = 0;
            DexStore dexStore = sMainDexStore;
            if (dexStore != null) {
                j = dexStore.getLastRegenTime();
            }
        }
        return j;
    }

    public static synchronized DexStore getMainDexStore() {
        DexStore dexStore;
        synchronized (DexLibLoader.class) {
            dexStore = sMainDexStore;
            if (dexStore == null) {
                throw new IllegalStateException("main dex store not loaded");
            }
        }
        return dexStore;
    }

    public static DexErrorRecoveryInfo getMainDexStoreLoadInformation() {
        DexErrorRecoveryInfo dexErrorRecoveryInfo = mDeri;
        if (dexErrorRecoveryInfo != null) {
            return dexErrorRecoveryInfo;
        }
        throw new AssertionError("main dex store not yet loaded");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:21|(1:23)(1:24)|25|26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01b7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01b8, code lost:
        if (r3 != null) goto L_0x01ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01bd, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004e, code lost:
        if (X.AnonymousClass093.A01 != false) goto L_0x0050;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.facebook.common.dextricks.DexErrorRecoveryInfo loadAllImpl(android.content.Context r14, int r15, X.C000000c r16, com.facebook.common.dextricks.DexStore.Config r17) {
        /*
            r13 = r15 & 1
            r12 = 1
            r5 = 0
            r3 = 0
            if (r13 == 0) goto L_0x0008
            r3 = 1
        L_0x0008:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r3)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r15)
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "DLL.loadAll betaBuild:%s flags:0x%08x"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            r0 = r15 & 16
            r2 = r17
            if (r0 == 0) goto L_0x0034
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r0 = "DLL.loadAll instrumentation test mode"
            com.facebook.common.dextricks.Mlog.safeFmt(r0, r1)
            setupMainDexStoreConfigForInstrumentationTests(r14, r2)
        L_0x0029:
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            goto L_0x003e
        L_0x0034:
            if (r17 == 0) goto L_0x0029
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Do not specify a config outside of test mode."
            r1.<init>(r0)
            throw r1
        L_0x003e:
            java.lang.Class<com.facebook.common.dextricks.DexLibLoader> r0 = com.facebook.common.dextricks.DexLibLoader.class
            java.lang.ClassLoader r6 = r0.getClassLoader()     // Catch:{ 0MR -> 0x0105 }
            java.lang.System.currentTimeMillis()     // Catch:{ 0MR -> 0x0105 }
            boolean r2 = X.AnonymousClass093.A02     // Catch:{ 0MR -> 0x0105 }
            if (r2 == 0) goto L_0x0050
            boolean r1 = X.AnonymousClass093.A01     // Catch:{ 0MR -> 0x0105 }
            r0 = 0
            if (r1 == 0) goto L_0x0051
        L_0x0050:
            r0 = 1
        L_0x0051:
            if (r0 == 0) goto L_0x00fd
            if (r2 == 0) goto L_0x0077
            boolean r0 = X.AnonymousClass093.A01     // Catch:{ 0MR -> 0x0105 }
            if (r0 == 0) goto L_0x0077
            android.os.StrictMode$VmPolicy r4 = android.os.StrictMode.getVmPolicy()     // Catch:{ 0MR -> 0x0105 }
            android.os.StrictMode$VmPolicy$Builder r2 = new android.os.StrictMode$VmPolicy$Builder     // Catch:{ 0MR -> 0x0105 }
            if (r4 == 0) goto L_0x0065
            r2.<init>(r4)     // Catch:{ 0MR -> 0x0105 }
            goto L_0x0068
        L_0x0065:
            r2.<init>()     // Catch:{ 0MR -> 0x0105 }
        L_0x0068:
            java.lang.reflect.Method r1 = X.AnonymousClass093.A00     // Catch:{ IllegalAccessException | IllegalArgumentException | InvocationTargetException -> 0x006f }
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ IllegalAccessException | IllegalArgumentException | InvocationTargetException -> 0x006f }
            r1.invoke(r2, r0)     // Catch:{ IllegalAccessException | IllegalArgumentException | InvocationTargetException -> 0x006f }
        L_0x006f:
            android.os.StrictMode$VmPolicy r0 = r2.build()     // Catch:{ 0MR -> 0x0105 }
            android.os.StrictMode.setVmPolicy(r0)     // Catch:{ 0MR -> 0x0105 }
            goto L_0x0078
        L_0x0077:
            r4 = 0
        L_0x0078:
            java.lang.String r0 = "dalvik.system.BaseDexClassLoader"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.String r0 = "pathList"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            r0.setAccessible(r12)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.Object r6 = r0.get(r6)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.String r0 = "dalvik.system.DexPathList"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.String r0 = "dexElements"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            r0.setAccessible(r12)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.Object r11 = r0.get(r6)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.Object[] r11 = (java.lang.Object[]) r11     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.String r0 = "dalvik.system.DexPathList$Element"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.String r0 = "dexFile"
            java.lang.reflect.Field r10 = r1.getDeclaredField(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            r10.setAccessible(r12)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            android.content.pm.ApplicationInfo r0 = r14.getApplicationInfo()     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.String r9 = r0.sourceDir     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            if (r9 == 0) goto L_0x00e2
            int r6 = r11.length     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            r2 = 0
        L_0x00b9:
            if (r2 >= r6) goto L_0x00d9
            r0 = r11[r2]     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.Object r1 = r10.get(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            dalvik.system.DexFile r1 = (dalvik.system.DexFile) r1     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            if (r1 == 0) goto L_0x00d6
            java.lang.String r0 = r1.getName()     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            boolean r0 = r9.equals(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            if (r0 == 0) goto L_0x00d3
            r8.add(r1)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            goto L_0x00d6
        L_0x00d3:
            r7.add(r1)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
        L_0x00d6:
            int r2 = r2 + 1
            goto L_0x00b9
        L_0x00d9:
            if (r4 == 0) goto L_0x00de
            android.os.StrictMode.setVmPolicy(r4)     // Catch:{ 0MR -> 0x0105 }
        L_0x00de:
            java.lang.System.currentTimeMillis()     // Catch:{ 0MR -> 0x0105 }
            goto L_0x0113
        L_0x00e2:
            X.0MR r1 = new X.0MR     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            java.lang.String r0 = "Cannot find a primary dex name"
            r1.<init>(r0)     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
            throw r1     // Catch:{ ClassCastException | ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x00ea }
        L_0x00ea:
            r2 = move-exception
            X.0MR r1 = new X.0MR     // Catch:{ all -> 0x00f3 }
            java.lang.String r0 = "Dalvik system code not of expected form"
            r1.<init>(r0, r2)     // Catch:{ all -> 0x00f3 }
            throw r1     // Catch:{ all -> 0x00f3 }
        L_0x00f3:
            r1 = move-exception
            if (r4 == 0) goto L_0x00f9
            android.os.StrictMode.setVmPolicy(r4)     // Catch:{ 0MR -> 0x0105 }
        L_0x00f9:
            java.lang.System.currentTimeMillis()     // Catch:{ 0MR -> 0x0105 }
            goto L_0x0104
        L_0x00fd:
            X.0MR r1 = new X.0MR     // Catch:{ 0MR -> 0x0105 }
            java.lang.String r0 = "Cannot call needed hidden apis on this platform"
            r1.<init>(r0)     // Catch:{ 0MR -> 0x0105 }
        L_0x0104:
            throw r1     // Catch:{ 0MR -> 0x0105 }
        L_0x0105:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r0 = "failure to locate primary/auxiliary dexes: perf loss"
            com.facebook.common.dextricks.Mlog.w(r2, r0, r1)
            r8.clear()
            r7.clear()
        L_0x0113:
            java.io.File r4 = com.facebook.common.dextricks.DexStoreUtils.getMainDexStoreLocation(r14)
            java.io.File r2 = new java.io.File
            android.content.pm.ApplicationInfo r0 = r14.getApplicationInfo()
            java.lang.String r0 = r0.sourceDir
            r2.<init>(r0)
            r6 = r16
            com.facebook.common.dextricks.ResProvider r3 = obtainResProvider(r6, r14, r3)
            if (r3 != 0) goto L_0x0137
            java.lang.String r1 = "Nothing to do in DexLibLoader.loadAll: no resProvider"
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.DexErrorRecoveryInfo r0 = new com.facebook.common.dextricks.DexErrorRecoveryInfo     // Catch:{ all -> 0x01b5 }
            r0.<init>()     // Catch:{ all -> 0x01b5 }
            return r0
        L_0x0137:
            java.lang.String r1 = "opening dex store %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r4}     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.DexStore r4 = com.facebook.common.dextricks.DexStore.open(r4, r2, r3, r8, r7)     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.DexLibLoader.sMainDexStore = r4     // Catch:{ all -> 0x01b5 }
            r0 = r15 & 2
            if (r0 != 0) goto L_0x01a9
            r2 = 0
            if (r13 == 0) goto L_0x014e
            r2 = 1
        L_0x014e:
            r0 = r15 & 8
            if (r0 == 0) goto L_0x0154
            r2 = r2 | 16
        L_0x0154:
            r0 = r15 & 4
            if (r0 == 0) goto L_0x015a
            r2 = r2 | 4
        L_0x015a:
            boolean r0 = shouldSynchronouslyGenerateOatFiles()     // Catch:{ all -> 0x01b5 }
            if (r0 == 0) goto L_0x017d
            java.lang.String r1 = "disabling background optimization"
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.Mlog.w(r1, r0)     // Catch:{ all -> 0x01b5 }
            r2 = r2 & -5
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x01b5 }
            r0 = 26
            if (r1 < r0) goto L_0x0179
            com.facebook.common.dextricks.DexStore$Config r0 = r4.readConfig()     // Catch:{ all -> 0x01b5 }
            boolean r0 = r0.enableOatmeal     // Catch:{ all -> 0x01b5 }
            if (r0 != 0) goto L_0x0179
            r2 = r2 | r12
            goto L_0x017b
        L_0x0179:
            r2 = r2 & -2
        L_0x017b:
            r2 = r2 | 8
        L_0x017d:
            com.facebook.common.dextricks.DexErrorRecoveryInfo r2 = r4.loadAll(r2, r6, r14)     // Catch:{ all -> 0x01b5 }
            int r4 = r2.loadResult     // Catch:{ all -> 0x01b5 }
            r0 = r4 & 8
            if (r0 == 0) goto L_0x01b1
            boolean r1 = X.AnonymousClass018.A01     // Catch:{ all -> 0x01b5 }
            r0 = 0
            if (r1 == 0) goto L_0x018d
            r0 = 1
        L_0x018d:
            if (r0 != 0) goto L_0x0199
            java.lang.String r1 = "running deoptimized code"
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.DexLibLoader.deoptTaint = r12     // Catch:{ all -> 0x01b5 }
            goto L_0x01b1
        L_0x0199:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x01b5 }
            java.lang.String r1 = "In ct-scan mode, but not running optimized code. Out of disk space? Bad config? Load result: 0x"
            java.lang.String r0 = java.lang.Integer.toHexString(r4)     // Catch:{ all -> 0x01b5 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x01b5 }
            r2.<init>(r0)     // Catch:{ all -> 0x01b5 }
            throw r2     // Catch:{ all -> 0x01b5 }
        L_0x01a9:
            java.lang.String r1 = "skipping actual loadAll as requested"
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x01b5 }
            com.facebook.common.dextricks.Mlog.safeFmt(r1, r0)     // Catch:{ all -> 0x01b5 }
            r2 = 0
        L_0x01b1:
            r3.close()
            return r2
        L_0x01b5:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x01b7 }
        L_0x01b7:
            r0 = move-exception
            if (r3 == 0) goto L_0x01bd
            r3.close()     // Catch:{ all -> 0x01bd }
        L_0x01bd:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.dextricks.DexLibLoader.loadAllImpl(android.content.Context, int, X.00c, com.facebook.common.dextricks.DexStore$Config):com.facebook.common.dextricks.DexErrorRecoveryInfo");
    }

    private static ResProvider obtainResProviderInternal(Context context, boolean z) {
        ApkResProvider apkResProvider = new ApkResProvider(context);
        try {
            apkResProvider.open(DexStoreUtils.SECONDARY_DEX_MANIFEST).close();
            return apkResProvider;
        } catch (Resources.NotFoundException | FileNotFoundException unused) {
            if (z) {
                ExoPackageResProvider exoPackageResProvider = new ExoPackageResProvider(context);
                try {
                    exoPackageResProvider.open(DexStoreUtils.SECONDARY_DEX_MANIFEST).close();
                    Mlog.safeFmt("using exopackage", new Object[0]);
                    return exoPackageResProvider;
                } catch (FileNotFoundException unused2) {
                    Mlog.safeFmt("using exo res provider failed", new Object[0]);
                    return null;
                }
            }
            return null;
        }
    }

    private static boolean shouldSynchronouslyGenerateOatFiles() {
        boolean z = false;
        if (AnonymousClass018.A01) {
            z = true;
        }
        if (z || Build.VERSION.SDK_INT >= 26 || AnonymousClass018.A00) {
            return true;
        }
        return false;
    }

    public static void ensureConfig(Context context, DexStore.Config config, boolean z) {
        File mainDexStoreLocation = DexStoreUtils.getMainDexStoreLocation(context);
        Fs.mkdirOrThrow(mainDexStoreLocation);
        if (config != null) {
            File file = new File(mainDexStoreLocation, "config");
            if (!file.exists() || z) {
                config.writeAndSync(file);
            }
        }
    }

    public static ResProvider obtainResProvider(C000000c r0, Context context, boolean z) {
        try {
            return obtainResProviderInternal(context, z);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void setupMainDexStoreConfigForInstrumentationTests(Context context, DexStore.Config config) {
        File mainDexStoreLocation = DexStoreUtils.getMainDexStoreLocation(context);
        Fs.deleteRecursive(mainDexStoreLocation);
        Fs.mkdirOrThrow(mainDexStoreLocation);
        if (config != null) {
            config.writeAndSync(new File(mainDexStoreLocation, "config"));
        }
    }

    public static RuntimeException verifyCanaryClasses() {
        try {
            for (DexStore dexStoreListHead = DexStore.dexStoreListHead(); dexStoreListHead != null; dexStoreListHead = dexStoreListHead.next) {
                DexManifest dexManifest = dexStoreListHead.mLoadedManifest;
                if (dexManifest != null) {
                    int i = 0;
                    while (true) {
                        DexManifest.Dex[] dexArr = dexManifest.dexes;
                        if (i >= dexArr.length) {
                            break;
                        }
                        Class.forName(dexArr[i].canaryClass);
                        i++;
                    }
                }
            }
            return null;
        } catch (Throwable th) {
            return Fs.runtimeExFrom(th);
        }
    }

    public static void loadAll(Context context) {
        loadAll(context, false);
    }

    public static synchronized void loadAll(Context context, int i, C000000c r4) {
        synchronized (DexLibLoader.class) {
            loadAll(context, i, r4, null);
        }
    }

    public static synchronized void loadAll(Context context, int i, C000000c r5, DexStore.Config config) {
        synchronized (DexLibLoader.class) {
            if (mDeri == null) {
                if (r5 == null) {
                    r5 = AnonymousClass08Q.A00;
                }
                try {
                    mDeri = loadAllImpl(context, i, r5, config);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            } else {
                throw new AssertionError("loadAll already loaded dex files");
            }
        }
    }

    public static void loadAll(Context context, boolean z) {
        loadAll(context, 1, (C000000c) null);
    }

    public static void loadAll(Context context, boolean z, C000000c r3) {
        loadAll(context, 1, r3);
    }
}
