package com.facebook.common.dextricks;

import X.AnonymousClass08S;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import dalvik.system.BaseDexClassLoader;
import dalvik.system.PathClassLoader;
import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.zip.ZipFile;

public final class MemoryReductionHack {
    private static final String TAG = "MemoryReductionHack";

    public static void freeApkZip(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            ClassLoader classLoader = context.getClassLoader();
            if (classLoader instanceof PathClassLoader) {
                removeZipFromPathClassLoader(applicationInfo.sourceDir, (PathClassLoader) classLoader);
            } else {
                Log.w(TAG, "system classloader of unexpected type");
            }
        } catch (PackageManager.NameNotFoundException unused) {
            Log.w(TAG, "Couldn't retrieve the application info");
        } catch (IllegalAccessException unused2) {
            Log.w(TAG, "Couldn't update the Loader (IllegalAccessException)");
        } catch (NoSuchFieldException unused3) {
            Log.w(TAG, "Couldn't update the Loader (NoSuchFieldException)");
        } catch (RuntimeException e) {
            String message = e.getMessage();
            if (message == null || !message.contains("Package manager has died")) {
                throw e;
            }
            Log.w(TAG, "Couldn't retrieve the applicaiton info because PackageManager died", e.getCause());
        }
    }

    private static Object getDexPathList(BaseDexClassLoader baseDexClassLoader) {
        return getField(baseDexClassLoader, BaseDexClassLoader.class, "pathList");
    }

    public static void removeZipFromPathClassLoader(String str, PathClassLoader pathClassLoader) {
        File file;
        ZipFile zipFile;
        Object dexElementsArray = getDexElementsArray(getDexPathList(pathClassLoader));
        int length = Array.getLength(dexElementsArray);
        int i = 0;
        while (i < length) {
            Object obj = Array.get(dexElementsArray, i);
            try {
                file = (File) getField(obj, obj.getClass(), "file");
            } catch (NoSuchFieldException unused) {
                file = (File) getField(obj, obj.getClass(), "zip");
            }
            try {
                zipFile = (ZipFile) getField(obj, obj.getClass(), "zipFile");
            } catch (NoSuchFieldException unused2) {
                zipFile = null;
            }
            if (file == null || !str.equals(file.getPath())) {
                i++;
            } else {
                if (zipFile != null) {
                    setField(obj, obj.getClass(), "zipFile", null);
                }
                if (Build.VERSION.SDK_INT >= 18) {
                    setField(obj, obj.getClass(), "initialized", true);
                    return;
                }
                return;
            }
        }
        Log.w(TAG, AnonymousClass08S.A0J("Could not find zipFile entry corresponding to path ", str));
    }

    private static Object getDexElementsArray(Object obj) {
        return getField(obj, obj.getClass(), "dexElements");
    }

    private static Object getField(Object obj, Class cls, String str) {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        return declaredField.get(obj);
    }

    private static void setField(Object obj, Class cls, String str, Object obj2) {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        declaredField.set(obj, obj2);
    }
}
