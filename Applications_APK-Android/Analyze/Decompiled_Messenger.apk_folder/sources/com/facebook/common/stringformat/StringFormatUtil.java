package com.facebook.common.stringformat;

import X.AnonymousClass08S;
import java.util.Formatter;
import java.util.MissingFormatArgumentException;
import java.util.UnknownFormatConversionException;

public class StringFormatUtil {
    private static final Object[] A00 = {null};

    private static int A04(StringBuilder sb, String str, int i, Object obj, Object obj2, Object obj3, Object obj4) {
        int i2;
        int A05;
        boolean z = false;
        if (sb == null) {
            z = true;
        }
        int i3 = 0;
        if (i == 0) {
            i3 = -1;
        }
        int i4 = i3;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            i2 = -200;
            if (i4 >= i) {
                break;
            }
            Object obj5 = obj2;
            Object obj6 = obj3;
            Object obj7 = obj4;
            if (i4 == -1) {
                A05 = A05(sb, str, i5, null, false);
            } else if (i4 == 0) {
                A05 = A05(sb, str, i5, obj, true);
            } else if (i4 == 1) {
                A05 = A05(sb, str, i5, obj5, true);
            } else if (i4 == 2) {
                A05 = A05(sb, str, i5, obj6, true);
            } else if (i4 == 3) {
                A05 = A05(sb, str, i5, obj7, true);
            } else {
                throw new AssertionError();
            }
            if (A05 == -1) {
                return -1;
            }
            if (z) {
                i6 += A05;
            }
            i5 = A00(str, i5);
            i2 = -200;
            if (i4 != i3 || i5 != -200 || !z) {
                if (i5 < 0) {
                    break;
                }
                i4++;
            } else {
                return -2;
            }
        }
        if (i5 != i2 && i5 != -201) {
            return A03(sb, str, i5, i6, z);
        }
        if (z) {
            return i6;
        }
        return -3;
    }

    private static int A06(StringBuilder sb, String str, Object... objArr) {
        boolean z;
        int i = 0;
        boolean z2 = false;
        if (sb == null) {
            z2 = true;
        }
        if (objArr == null || objArr.length == 0) {
            objArr = A00;
            z = true;
        } else {
            z = false;
        }
        int length = objArr.length;
        int i2 = 0;
        int i3 = 0;
        boolean z3 = false;
        while (i < length) {
            int A05 = A05(sb, str, i2, objArr[i], !z);
            if (A05 != -1) {
                if (z2) {
                    i3 += A05;
                }
                i2 = A00(str, i2);
                if (i2 == -200) {
                    break;
                }
                z3 = true;
                if (i2 == -201) {
                    break;
                }
                i++;
            } else {
                return -1;
            }
        }
        if (z2 && !z3) {
            return -2;
        }
        if (i2 != -200 && i2 != -201) {
            return A03(sb, str, i2, i3, z2);
        }
        if (z2) {
            return i3;
        }
        return -3;
    }

    private static String A08(String str, Object... objArr) {
        try {
            return String.format(null, str, objArr);
        } catch (MissingFormatArgumentException | UnknownFormatConversionException e) {
            throw new RuntimeException(AnonymousClass08S.A0P(e.getMessage(), ": ", str));
        }
    }

    public static void appendFormatStrLocaleSafe(StringBuilder sb, String str, Object... objArr) {
        int A02 = A02(str, -1, null, null, null, null, objArr);
        if (A02 == -1) {
            new Formatter(sb).format(null, str, objArr);
        } else if (A02 == -2) {
            sb.append(str);
        } else {
            sb.ensureCapacity(A02);
            A06(sb, str, objArr);
        }
    }

    private static int A01(String str, int i) {
        int i2 = i + 1;
        if (str.length() <= i2) {
            return -101;
        }
        char charAt = str.charAt(i2);
        if (charAt == 's' || charAt == 'd' || charAt == '%') {
            return -100;
        }
        return -101;
    }

    private static int A02(String str, int i, Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        if (i == 0) {
            return A04(null, str, 0, null, null, null, null);
        }
        if (i == 1) {
            return A04(null, str, 1, obj, null, null, null);
        }
        if (i == 2) {
            return A04(null, str, 2, obj, obj2, null, null);
        }
        if (i == 3) {
            return A04(null, str, 3, obj, obj2, obj3, null);
        }
        if (i != 4) {
            return A06(null, str, objArr);
        }
        return A04(null, str, 4, obj, obj2, obj3, obj4);
    }

    private static int A00(String str, int i) {
        int length = str.length();
        boolean z = false;
        while (i < length) {
            if (str.charAt(i) == '%' && A01(str, i) == -100) {
                int i2 = i + 1;
                if (str.charAt(i2) != '%') {
                    return i + 2;
                }
                i = i2;
                z = true;
            }
            i++;
        }
        if (z) {
            return -201;
        }
        return -200;
    }

    private static int A03(StringBuilder sb, String str, int i, int i2, boolean z) {
        int length = str.length();
        int i3 = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt != '%' || (length > (i = i + 1) && str.charAt(i) == '%')) {
                if (sb == null) {
                    i3++;
                } else {
                    sb.append(charAt);
                }
                i++;
            } else if (z) {
                return -1;
            } else {
                throw new AssertionError();
            }
        }
        int i4 = i2 + i3;
        if (!z) {
            return -3;
        }
        return i4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x007e, code lost:
        if (r8 == null) goto L_0x0065;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00bd A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0023 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int A05(java.lang.StringBuilder r8, java.lang.String r9, int r10, java.lang.Object r11, boolean r12) {
        /*
            int r3 = r9.length()
            r7 = 0
        L_0x0005:
            if (r10 >= r3) goto L_0x0046
            char r0 = r9.charAt(r10)
            r6 = 37
            r5 = 1
            if (r0 != r6) goto L_0x00b2
            int r1 = A01(r9, r10)
            r0 = -100
            r2 = -1
            if (r1 != r0) goto L_0x0023
            int r4 = r10 + 1
            char r1 = r9.charAt(r4)
            if (r12 != 0) goto L_0x0024
            if (r1 == r6) goto L_0x0024
        L_0x0023:
            return r2
        L_0x0024:
            r0 = 115(0x73, float:1.61E-43)
            if (r1 != r0) goto L_0x0056
            boolean r0 = r11 instanceof java.util.Formattable
            if (r0 != 0) goto L_0x00a0
            r1 = 0
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L_0x004f
            r1 = r11
            java.lang.String r1 = (java.lang.String) r1
        L_0x0034:
            if (r1 != 0) goto L_0x0038
            java.lang.String r1 = "null"
        L_0x0038:
            if (r8 != 0) goto L_0x004a
            int r1 = r1.length()
        L_0x003e:
            r0 = 1
        L_0x003f:
            if (r1 == r2) goto L_0x0023
            if (r8 != 0) goto L_0x0044
            int r7 = r7 + r1
        L_0x0044:
            if (r0 == 0) goto L_0x00b6
        L_0x0046:
            if (r8 == 0) goto L_0x0049
            r7 = -3
        L_0x0049:
            return r7
        L_0x004a:
            r8.append(r1)
            r1 = -3
            goto L_0x003e
        L_0x004f:
            if (r11 == 0) goto L_0x0034
            java.lang.String r1 = r11.toString()
            goto L_0x0034
        L_0x0056:
            r0 = 100
            if (r1 != r0) goto L_0x00a4
            r1 = 4
            if (r11 != 0) goto L_0x0069
            if (r8 == 0) goto L_0x0065
            java.lang.String r0 = "null"
            r8.append(r0)
        L_0x0064:
            r1 = 0
        L_0x0065:
            if (r8 == 0) goto L_0x003e
            r1 = -3
            goto L_0x003e
        L_0x0069:
            boolean r0 = r11 instanceof java.lang.Integer
            if (r0 == 0) goto L_0x0072
            if (r8 != 0) goto L_0x0081
            r1 = 11
            goto L_0x0065
        L_0x0072:
            boolean r0 = r11 instanceof java.lang.Short
            if (r0 == 0) goto L_0x007a
            if (r8 != 0) goto L_0x0081
            r1 = 6
            goto L_0x0065
        L_0x007a:
            boolean r0 = r11 instanceof java.lang.Byte
            if (r0 == 0) goto L_0x008c
            if (r8 != 0) goto L_0x0081
            goto L_0x0065
        L_0x0081:
            r0 = r11
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            r8.append(r0)
            goto L_0x0064
        L_0x008c:
            boolean r0 = r11 instanceof java.lang.Long
            if (r0 == 0) goto L_0x00a0
            if (r8 != 0) goto L_0x0095
            r1 = 20
            goto L_0x0065
        L_0x0095:
            r0 = r11
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            r8.append(r0)
            goto L_0x0064
        L_0x00a0:
            if (r8 != 0) goto L_0x00bd
            r1 = -1
            goto L_0x003e
        L_0x00a4:
            if (r1 != r6) goto L_0x00af
            if (r8 == 0) goto L_0x00ab
            r8.append(r6)
        L_0x00ab:
            r10 = r4
            r0 = 0
            r1 = 1
            goto L_0x003f
        L_0x00af:
            r0 = 1
            r1 = -1
            goto L_0x003f
        L_0x00b2:
            if (r8 != 0) goto L_0x00b9
            int r7 = r7 + 1
        L_0x00b6:
            int r10 = r10 + r5
            goto L_0x0005
        L_0x00b9:
            r8.append(r0)
            goto L_0x00b6
        L_0x00bd:
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.stringformat.StringFormatUtil.A05(java.lang.StringBuilder, java.lang.String, int, java.lang.Object, boolean):int");
    }

    private static String A07(String str, int i, Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        Object[] objArr2;
        int A02 = A02(str, i, obj, obj2, obj3, obj4, objArr);
        if (A02 == -1) {
            if (i == 0) {
                objArr2 = new Object[0];
            } else if (i == 1) {
                objArr2 = new Object[]{obj};
            } else if (i == 2) {
                objArr2 = new Object[]{obj, obj2};
            } else if (i == 3) {
                objArr2 = new Object[]{obj, obj2, obj3};
            } else if (i != 4) {
                return A08(str, objArr);
            } else {
                objArr2 = new Object[]{obj, obj2, obj3, obj4};
            }
            return A08(str, objArr2);
        } else if (A02 == -2) {
            return str;
        } else {
            StringBuilder sb = new StringBuilder(A02);
            if (i == -1) {
                A06(sb, str, objArr);
            } else {
                A04(sb, str, i, obj, obj2, obj3, obj4);
            }
            return sb.toString();
        }
    }

    public static String formatStrLocaleSafe(String str) {
        return A07(str, 0, null, null, null, null, null);
    }

    public static String formatStrLocaleSafe(String str, Object obj) {
        return A07(str, 1, obj, null, null, null, null);
    }

    public static String formatStrLocaleSafe(String str, Object obj, Object obj2) {
        return A07(str, 2, obj, obj2, null, null, null);
    }

    public static String formatStrLocaleSafe(String str, Object obj, Object obj2, Object obj3) {
        return A07(str, 3, obj, obj2, obj3, null, null);
    }

    public static String formatStrLocaleSafe(String str, Object obj, Object obj2, Object obj3, Object obj4) {
        return A07(str, 4, obj, obj2, obj3, obj4, null);
    }

    public static String formatStrLocaleSafe(String str, Object... objArr) {
        return A07(str, -1, null, null, null, null, objArr);
    }
}
