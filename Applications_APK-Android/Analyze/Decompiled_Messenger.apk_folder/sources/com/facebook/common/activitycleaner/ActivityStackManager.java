package com.facebook.common.activitycleaner;

import X.AnonymousClass09P;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass0t6;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y7;
import X.C04350Ue;
import X.C07940eQ;
import X.C14690tp;
import android.app.Activity;
import com.google.common.collect.MapMakerInternalMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
public final class ActivityStackManager {
    public static final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) C04350Ue.A02.A09("user_left_app_at"));
    private static volatile ActivityStackManager A07;
    public int A00;
    public AnonymousClass0t6 A01;
    public AnonymousClass0UN A02;
    public final HashSet A03;
    public final LinkedList A04 = new LinkedList();
    public final Map A05;

    public static final ActivityStackManager A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (ActivityStackManager.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new ActivityStackManager(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public Activity A01() {
        synchronized (this.A04) {
            if (this.A04.isEmpty()) {
                return null;
            }
            C14690tp r0 = (C14690tp) this.A04.getLast();
            return r0.A00();
        }
    }

    public List A02() {
        ArrayList arrayList;
        synchronized (this.A04) {
            arrayList = new ArrayList(this.A04);
        }
        return arrayList;
    }

    public void A03() {
        int size;
        int i;
        synchronized (this.A04) {
            ListIterator listIterator = this.A04.listIterator(0);
            while (listIterator.hasNext()) {
                if (((C14690tp) listIterator.next()).A00() == null) {
                    listIterator.remove();
                }
            }
            size = this.A04.size();
            i = this.A00;
        }
        ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A02)).Bz2("activity_stack_size", Integer.toString(size));
        ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A02)).Bz2("activity_creation_count", Integer.toString(i));
    }

    public void A04(Activity activity) {
        synchronized (this.A04) {
            C14690tp r1 = (C14690tp) this.A05.get(activity);
            if (r1 != null) {
                this.A04.remove(r1);
                this.A05.remove(activity);
                this.A03.remove(r1);
            }
        }
    }

    private ActivityStackManager(AnonymousClass1XY r3) {
        C07940eQ r1 = new C07940eQ();
        r1.A06(MapMakerInternalMap.Strength.WEAK);
        this.A05 = r1.A02();
        this.A03 = new HashSet();
        this.A02 = new AnonymousClass0UN(3, r3);
    }
}
