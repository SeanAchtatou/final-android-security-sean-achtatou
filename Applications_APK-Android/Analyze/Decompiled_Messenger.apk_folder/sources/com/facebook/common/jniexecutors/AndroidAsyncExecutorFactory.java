package com.facebook.common.jniexecutors;

import X.AnonymousClass01q;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.jni.HybridData;
import java.util.concurrent.ScheduledExecutorService;

public class AndroidAsyncExecutorFactory {
    private final HybridData mHybridData;

    private static native HybridData initHybrid(ScheduledExecutorService scheduledExecutorService);

    static {
        AnonymousClass01q.A08(TurboLoader.Locator.$const$string(81));
    }

    public AndroidAsyncExecutorFactory(ScheduledExecutorService scheduledExecutorService) {
        this.mHybridData = initHybrid(scheduledExecutorService);
    }
}
