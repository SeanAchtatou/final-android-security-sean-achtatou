package com.facebook.common.jniexecutors;

import X.AnonymousClass06c;
import X.AnonymousClass07J;
import X.C30531iD;
import android.util.Log;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import com.facebook.jni.HybridData;

public class PooledNativeRunnable extends NativeRunnable {
    private static final AnonymousClass07J sPool;

    public PooledNativeRunnable() {
        super(null);
    }

    static {
        Class<PooledNativeRunnable> cls = PooledNativeRunnable.class;
        AnonymousClass06c r2 = new AnonymousClass06c(null, cls, AwakeTimeSinceBootClock.INSTANCE);
        r2.A03 = new C30531iD(cls);
        sPool = r2.A00();
    }

    public static PooledNativeRunnable allocate(HybridData hybridData) {
        PooledNativeRunnable pooledNativeRunnable = (PooledNativeRunnable) sPool.A01();
        pooledNativeRunnable.mHybridData = hybridData;
        return pooledNativeRunnable;
    }

    public void run() {
        try {
            super.run();
        } catch (Exception e) {
            Log.e("PooledNativeRunnable", "run crashed", e);
        }
        sPool.A02(this);
    }
}
