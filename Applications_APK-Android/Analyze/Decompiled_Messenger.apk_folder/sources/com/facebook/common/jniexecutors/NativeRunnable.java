package com.facebook.common.jniexecutors;

import X.AnonymousClass01q;
import X.AnonymousClass08S;
import X.C005505z;
import X.C012409l;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.jni.HybridData;

public class NativeRunnable implements Runnable, C012409l {
    public HybridData mHybridData;
    public volatile String mNativeExecutor;

    private native void nativeRun();

    public Object getInnerRunnable() {
        return this;
    }

    static {
        AnonymousClass01q.A08(TurboLoader.Locator.$const$string(81));
    }

    public String getRunnableName() {
        String str = this.mNativeExecutor;
        if (str == null) {
            return "NativeRunnable";
        }
        return AnonymousClass08S.A0J("NativeRunnable - ", str);
    }

    public void run() {
        C005505z.A05("%s", this, -693752208);
        try {
            nativeRun();
        } finally {
            C005505z.A00(570492259);
        }
    }

    public NativeRunnable(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public String toString() {
        return getRunnableName();
    }
}
