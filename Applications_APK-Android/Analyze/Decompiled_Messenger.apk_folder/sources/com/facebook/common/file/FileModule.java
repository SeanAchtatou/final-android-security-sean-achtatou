package com.facebook.common.file;

import X.AnonymousClass0UV;
import X.AnonymousClass0WD;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.C012109i;
import X.C09680iI;
import com.facebook.inject.InjectorModule;

@InjectorModule
public class FileModule extends AnonymousClass0UV {
    private static volatile C09680iI A00;
    private static volatile C012109i A01;

    public static final C09680iI A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C09680iI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C09680iI();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C012109i A01(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C012109i.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = C012109i.A01();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static C09680iI getInstanceForTest_FileUtil(AnonymousClass1XX r1) {
        return (C09680iI) r1.getInstance(C09680iI.class);
    }
}
