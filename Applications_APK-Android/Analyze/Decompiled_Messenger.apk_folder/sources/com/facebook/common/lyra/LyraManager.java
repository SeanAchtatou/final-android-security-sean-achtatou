package com.facebook.common.lyra;

import X.AnonymousClass01q;

public class LyraManager {
    public static native void installLibraryIdentifierFunction();

    public static native boolean nativeInstallLyraHook(boolean z);

    static {
        AnonymousClass01q.A08("lyramanager");
    }
}
