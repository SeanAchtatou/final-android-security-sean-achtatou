package com.facebook.common.iopri.loader;

import java.util.concurrent.atomic.AtomicBoolean;

public final class DalvikGcHooksLoader {
    public static final AtomicBoolean A00 = new AtomicBoolean(false);

    public static native boolean nativeInitialize();

    private DalvikGcHooksLoader() {
    }
}
