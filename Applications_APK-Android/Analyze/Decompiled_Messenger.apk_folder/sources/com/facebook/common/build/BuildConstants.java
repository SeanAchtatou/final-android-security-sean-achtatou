package com.facebook.common.build;

public class BuildConstants {
    public static final String[] A00 = {"aura", "browser", "videoplayer", "adnw"};

    public static final int A00() {
        return 200372525;
    }

    public static final String A01() {
        return "com.facebook.katana";
    }

    public static final boolean isInternalBuild() {
        return false;
    }
}
