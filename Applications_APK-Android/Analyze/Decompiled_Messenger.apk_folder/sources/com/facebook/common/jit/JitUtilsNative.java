package com.facebook.common.jit;

import X.AnonymousClass01q;
import X.C010708t;
import android.content.Context;
import android.util.Log;
import com.facebook.common.jit.common.MethodInfo;
import java.util.List;

public final class JitUtilsNative {
    public static final boolean PLATFORM_SUPPORTED;

    private static native boolean nativeDisableJit();

    private static native boolean nativeEnableJit(long j, boolean z, boolean z2, boolean z3);

    private static native Object nativeGetDefaultJITStatus();

    public static native String nativeGetErrorMessage();

    private static native Object nativeGetJITStatus();

    private static native Object nativeGetPgoMethodInfo(String str, Object[] objArr);

    private static native boolean nativeInitialize(boolean z, MethodInfo[] methodInfoArr, MethodInfo methodInfo, int i, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str);

    public static native boolean nativeIsJitEnabled();

    private static native boolean nativeIsProfileChangeSignificant(String str, String str2, boolean z);

    public static native void nativeMarkAsPriorityThread();

    public static native boolean nativeStartJit();

    private static native void nativeStartPerfSensitiveScenario(int i);

    private static native boolean nativeStopJit();

    private static native void nativeStopPerfSensitiveScenario();

    private static native void nativeUnmarkAsPriorityThread();

    static {
        boolean z;
        try {
            AnonymousClass01q.A08("fbjitjni");
            z = true;
        } catch (UnsatisfiedLinkError e) {
            C010708t.A08(JitUtilsNative.class, "Error loading JitUtils", e);
            z = false;
        }
        PLATFORM_SUPPORTED = z;
    }

    private JitUtilsNative() {
    }

    public static boolean initialize(Context context, boolean z, List list, int i, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        Class<JitUtilsNative> cls = JitUtilsNative.class;
        if (!PLATFORM_SUPPORTED) {
            return false;
        }
        String absolutePath = context.getDir("jitutils", 0).getAbsolutePath();
        Class cls2 = Boolean.TYPE;
        MethodInfo method = MethodInfo.getMethod(cls, "initialize", Context.class, cls2, List.class, Integer.TYPE, cls2, cls2, cls2, cls2, cls2, cls2);
        if (method == null) {
            Log.e("JitUtilsNative", String.format("Jit initing failed: Cannot find %s.%s function.", cls.getName(), "initialize"));
            return false;
        }
        List list2 = list;
        return nativeInitialize(z, (MethodInfo[]) list2.toArray(new MethodInfo[list2.size()]), method, i, z2, z3, z4, z5, z6, z7, absolutePath);
    }
}
