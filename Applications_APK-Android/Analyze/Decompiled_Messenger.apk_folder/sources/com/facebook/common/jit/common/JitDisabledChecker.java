package com.facebook.common.jit.common;

import X.AnonymousClass00e;
import android.app.Application;
import android.content.pm.PackageManager;
import android.text.SpannedString;
import com.facebook.common.dextricks.DexStore;

public final class JitDisabledChecker {
    public static final boolean VM_SAFE_MODE_ENABLED;
    public static final boolean sIsJitDisabled;

    private static boolean testCompileMethod(int i) {
        if (new SpannedString("Test").length() > i) {
            return true;
        }
        return false;
    }

    static {
        Application A00 = AnonymousClass00e.A00();
        boolean z = false;
        try {
            if ((A00.getPackageManager().getApplicationInfo(A00.getPackageName(), 0).flags & DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET) != 0) {
                z = true;
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        VM_SAFE_MODE_ENABLED = z;
        sIsJitDisabled = z;
    }

    private JitDisabledChecker() {
    }
}
