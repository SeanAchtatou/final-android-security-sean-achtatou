package com.facebook.common.jit.jitoptions;

import X.AnonymousClass01q;
import X.C000700l;
import X.C010708t;
import android.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;

public class FbJitOptions {
    private static final boolean PLATFORM_IS_SUPPORTED;
    private final AtomicBoolean mFreed;
    private final long mPtr;

    private static native void nativeApplyAllowMultipleVerifications(long j, boolean z);

    private static native void nativeApplyAlwaysVerifyOnJitCompile(long j, boolean z);

    private static native void nativeApplyArenaAllocRetryFix(long j, boolean z);

    private static native void nativeApplyArenaMallocFallback(long j, boolean z);

    private static native void nativeApplyArenaRetryFastHook(long j, boolean z);

    private static native void nativeApplyBarrierFix(long j, boolean z);

    private static native void nativeApplyCallIntoJavaFromJitThreadFix(long j, boolean z);

    private static native void nativeApplyCustomJitPriorities(long j, boolean z);

    private static native void nativeApplyCustomQCCompilerFix(long j, boolean z);

    private static native void nativeApplyDisableProfilerThreadTimeoutCheck(long j, boolean z);

    private static native void nativeApplyDoNotStartProfilerWithJit(long j, boolean z);

    private static native void nativeApplyEnableThreadPoolPriority(long j, boolean z);

    private static native void nativeApplyFailOnSoftVerificationFailure(long j, boolean z);

    private static native void nativeApplyFlushInstructionCacheFix(long j, boolean z);

    private static native void nativeApplyGcJitFix(long j, boolean z);

    private static native void nativeApplyGenInvokeNoLineFix(long j, boolean z);

    private static native void nativeApplyLogGenInvokeNoInlineBug(long j, boolean z);

    private static native void nativeApplyMirMethodLoweringInfoResolveFix(long j, boolean z);

    private static native void nativeApplyMmapRaceFix(long j, boolean z);

    private static native void nativeApplyMobileConfigInlining(long j, boolean z);

    private static native void nativeApplyMprotectFix(long j, boolean z);

    private static native void nativeApplyMprotectJitCreationOverride(long j, boolean z);

    private static native void nativeApplyNotifyJitActivityFix(long j, boolean z);

    private static native void nativeApplyOverrideJitCompilerCompileMethod(long j, boolean z);

    private static native void nativeApplyOverrideProfilerThread(long j, boolean z);

    private static native void nativeApplyPerformMoveBugFix(long j, boolean z);

    private static native void nativeApplyProfileSaverProfilingThreadHooks(long j, boolean z);

    private static native void nativeApplyProfilerThreadTimeoutSeconds(long j, int i);

    private static native void nativeApplyThreadListRaceFix(long j, boolean z);

    private static native void nativeApplyThreadPoolPriority(long j, int i);

    private static native void nativeApplyThreadPoolThreadCount(long j, int i);

    private static native void nativeApplyUseRemapMprotectPtSafeTL(long j, boolean z);

    private static native void nativeDestroy(long j);

    private static native long nativeInit();

    private static native void nativeSetAppDir(long j, String str);

    private static native void nativeSetCodeCacheCapacity(long j, int i);

    private static native void nativeSetCodeCacheCapacityRatio(long j, double d);

    private static native void nativeSetCodeCacheInitialCapacity(long j, int i);

    private static native void nativeSetCodeCacheInitialCapacityRatio(long j, double d);

    private static native void nativeSetCodeCacheMaxCapacity(long j, int i);

    private static native void nativeSetCodeCacheMaxCapacityRatio(long j, double d);

    private static native void nativeSetCodePath(long j, String[] strArr);

    private static native void nativeSetCompileThreshold(long j, int i);

    private static native void nativeSetCompileThresholdRatio(long j, double d);

    private static native void nativeSetDumpInfoOnShutdown(long j, boolean z);

    private static native void nativeSetForeignDexPath(long j, String str);

    private static native void nativeSetHotStartupMethodSamples(long j, int i);

    private static native void nativeSetInvokeTransitionWeight(long j, int i);

    private static native void nativeSetInvokeTransitionWeightRatio(long j, double d);

    private static native void nativeSetMaxCodeCacheInitialCapacity(long j, int i);

    private static native void nativeSetMaxNotificationBeforeWake(long j, int i);

    private static native void nativeSetMinClassesToSave(long j, int i);

    private static native void nativeSetMinMethodsToSave(long j, int i);

    private static native void nativeSetMinNewClassesForCompilation(long j, int i);

    private static native void nativeSetMinNewMethodsForCompilation(long j, int i);

    private static native void nativeSetMinNotificationBeforeWake(long j, int i);

    private static native void nativeSetMinSavePeriodMs(long j, int i);

    private static native void nativeSetOldProfilerBackoffCoefficient(long j, float f);

    private static native void nativeSetOldProfilerDurationSec(long j, int i);

    private static native void nativeSetOldProfilerIntervalUS(long j, long j2);

    private static native void nativeSetOldProfilerPeriodSec(long j, int i);

    private static native void nativeSetOldProfilerStartImmediately(long j, boolean z);

    private static native void nativeSetOldProfilerTopKChangeThreshold(long j, float f);

    private static native void nativeSetOldProfilerTopKThreshold(long j, float f);

    private static native void nativeSetOsrThreshold(long j, int i);

    private static native void nativeSetOsrThresholdRatio(long j, double d);

    private static native void nativeSetPriorityThreadWeight(long j, int i);

    private static native void nativeSetPriorityThreadWeightRatio(long j, double d);

    private static native void nativeSetProfileBootClassPath(long j, boolean z);

    private static native void nativeSetProfilePath(long j, String str);

    private static native void nativeSetProfileSaverProfilingThreadIoPrioClass(long j, int i);

    private static native void nativeSetProfileSaverProfilingThreadIoPrioPriority(long j, int i);

    private static native void nativeSetProfilerOptionEnabled(long j, boolean z);

    private static native void nativeSetSaveProfilingInfo(long j, boolean z);

    private static native void nativeSetSaveResolvedClassesDelayMs(long j, int i);

    private static native void nativeSetUseJit(long j, boolean z);

    private static native void nativeSetWarmupThreshold(long j, int i);

    private static native void nativeSetWarmupThresholdRatio(long j, double d);

    static {
        boolean z;
        try {
            AnonymousClass01q.A08("fbjitoptionsjni");
            z = true;
        } catch (UnsatisfiedLinkError e) {
            C010708t.A0L("FbJitOptions", "Error loading JitUtils", e);
            z = false;
        }
        PLATFORM_IS_SUPPORTED = z;
    }

    private boolean innerFree() {
        boolean andSet = this.mFreed.getAndSet(true);
        if (!andSet) {
            nativeDestroy(this.mPtr);
        }
        return andSet;
    }

    public void finalize() {
        int A03 = C000700l.A03(-740738852);
        try {
            super.finalize();
        } finally {
            if (!innerFree()) {
                Log.e("FbJitOptions", "Fb Jit options needs to be freed explicitly. Doing fallback clean!");
            }
            C000700l.A09(-955332036, A03);
        }
    }
}
