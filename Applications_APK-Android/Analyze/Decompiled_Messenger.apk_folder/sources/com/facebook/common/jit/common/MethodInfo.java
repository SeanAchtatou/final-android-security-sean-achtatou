package com.facebook.common.jit.common;

import X.AnonymousClass08S;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MethodInfo {
    public static final Map PRIMITIVE_TO_SIGNATURE;
    public static final Map SIGNATURE_TO_PRIMITIVE;
    public final Class cls;
    public final Constructor constructor;
    public final Method method;
    public final String name;
    public final String signature;

    public boolean equals(Object obj) {
        boolean equals;
        boolean equals2;
        boolean equals3;
        boolean equals4;
        boolean equals5;
        if (obj == null || !(obj instanceof MethodInfo)) {
            return false;
        }
        MethodInfo methodInfo = (MethodInfo) obj;
        Class cls2 = this.cls;
        Class cls3 = methodInfo.cls;
        if (cls2 == null) {
            equals = false;
            if (cls3 == null) {
                equals = true;
            }
        } else {
            equals = cls2.equals(cls3);
        }
        if (!equals) {
            return false;
        }
        Method method2 = this.method;
        Method method3 = methodInfo.method;
        if (method2 == null) {
            equals2 = false;
            if (method3 == null) {
                equals2 = true;
            }
        } else {
            equals2 = method2.equals(method3);
        }
        if (!equals2) {
            return false;
        }
        Constructor constructor2 = this.constructor;
        Constructor constructor3 = methodInfo.constructor;
        if (constructor2 == null) {
            equals3 = false;
            if (constructor3 == null) {
                equals3 = true;
            }
        } else {
            equals3 = constructor2.equals(constructor3);
        }
        if (!equals3) {
            return false;
        }
        String str = this.name;
        String str2 = methodInfo.name;
        if (str == null) {
            equals4 = false;
            if (str2 == null) {
                equals4 = true;
            }
        } else {
            equals4 = str.equals(str2);
        }
        if (!equals4) {
            return false;
        }
        String str3 = this.signature;
        String str4 = methodInfo.signature;
        if (str3 == null) {
            equals5 = false;
            if (str4 == null) {
                equals5 = true;
            }
        } else {
            equals5 = str3.equals(str4);
        }
        return equals5;
    }

    static {
        HashMap hashMap = new HashMap(9);
        PRIMITIVE_TO_SIGNATURE = hashMap;
        hashMap.put(Byte.TYPE, "B");
        Map map = PRIMITIVE_TO_SIGNATURE;
        map.put(Character.TYPE, "C");
        map.put(Short.TYPE, "S");
        map.put(Integer.TYPE, "I");
        map.put(Long.TYPE, "J");
        map.put(Float.TYPE, "F");
        map.put(Double.TYPE, "D");
        map.put(Void.TYPE, "V");
        map.put(Boolean.TYPE, "Z");
        HashMap hashMap2 = new HashMap(9);
        SIGNATURE_TO_PRIMITIVE = hashMap2;
        hashMap2.put('B', Byte.TYPE);
        Map map2 = SIGNATURE_TO_PRIMITIVE;
        map2.put('C', Character.TYPE);
        map2.put('S', Short.TYPE);
        map2.put('I', Integer.TYPE);
        map2.put('J', Long.TYPE);
        map2.put('F', Float.TYPE);
        map2.put('D', Double.TYPE);
        map2.put('V', Void.TYPE);
        map2.put('Z', Boolean.TYPE);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:8:0x0032 */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v12, types: [java.lang.Class] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean addArrayType(java.util.List r5, java.lang.Class r6, int r7) {
        /*
            if (r7 <= 0) goto L_0x0050
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r4 = 0
            r1 = 0
        L_0x0009:
            if (r1 >= r7) goto L_0x0013
            r0 = 91
            r2.append(r0)
            int r1 = r1 + 1
            goto L_0x0009
        L_0x0013:
            java.lang.String r3 = r2.toString()
            java.util.Map r0 = com.facebook.common.jit.common.MethodInfo.PRIMITIVE_TO_SIGNATURE
            java.lang.Object r0 = r0.get(r6)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = X.AnonymousClass08S.A0J(r3, r0)
            goto L_0x0032
        L_0x0026:
            java.lang.String r2 = "L"
            java.lang.String r1 = r6.getName()
            java.lang.String r0 = ";"
            java.lang.String r0 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)
        L_0x0032:
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x0037 }
            goto L_0x0048
        L_0x0037:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Cannot find array class: %s"
            java.lang.String r1 = java.lang.String.format(r0, r1)
            java.lang.String r0 = "JitMethodInfo"
            android.util.Log.w(r0, r1, r2)
            r0 = 0
        L_0x0048:
            if (r0 != 0) goto L_0x004b
            return r4
        L_0x004b:
            r5.add(r0)
            r0 = 1
            return r0
        L_0x0050:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Array count "
            java.lang.String r0 = " is not valid"
            java.lang.String r0 = X.AnonymousClass08S.A0A(r1, r7, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.jit.common.MethodInfo.addArrayType(java.util.List, java.lang.Class, int):boolean");
    }

    private static String getSignature(Class[] clsArr, Class cls2) {
        StringBuilder sb = new StringBuilder("(");
        for (Class signatureFromType : clsArr) {
            sb.append(getSignatureFromType(signatureFromType));
        }
        sb.append(')');
        sb.append(getSignatureFromType(cls2));
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static String getSignatureFromType(Class cls2) {
        String str = (String) PRIMITIVE_TO_SIGNATURE.get(cls2);
        if (str != null) {
            return str;
        }
        if (cls2.isArray()) {
            return AnonymousClass08S.A0J("[", getSignatureFromType(cls2.getComponentType()));
        }
        return AnonymousClass08S.A0P("L", cls2.getName().replace('.', '/'), ";");
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int hashCode = this.cls.hashCode() * 31;
        Method method2 = this.method;
        int i4 = 0;
        if (method2 != null) {
            i = method2.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 31;
        Constructor constructor2 = this.constructor;
        if (constructor2 != null) {
            i2 = constructor2.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        String str = this.name;
        if (str != null) {
            i3 = str.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 31;
        String str2 = this.signature;
        if (str2 != null) {
            i4 = str2.hashCode();
        }
        return i7 + i4;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("[ MethodInfo ");
        sb.append("cls: ");
        Class cls2 = this.cls;
        String str2 = "<null>";
        if (cls2 != null) {
            str = cls2.getName();
        } else {
            str = str2;
        }
        sb.append(str);
        sb.append(", ");
        Method method2 = this.method;
        if (method2 != null) {
            sb.append("method: ");
            if (method2 != null) {
                str2 = method2.getName();
            }
        } else {
            Constructor constructor2 = this.constructor;
            if (constructor2 != null) {
                sb.append("constructor: ");
                if (constructor2 != null) {
                    str2 = constructor2.getName();
                }
            }
            sb.append("name: ");
            sb.append(this.name);
            sb.append(", ");
            sb.append("signature: ");
            sb.append(this.signature);
            sb.append(']');
            return sb.toString();
        }
        sb.append(str2);
        sb.append(", ");
        sb.append("name: ");
        sb.append(this.name);
        sb.append(", ");
        sb.append("signature: ");
        sb.append(this.signature);
        sb.append(']');
        return sb.toString();
    }

    private MethodInfo() {
        this.cls = null;
        this.method = null;
        this.constructor = null;
        this.name = null;
        this.signature = null;
    }

    private MethodInfo(Class cls2, String str, String str2) {
        this.cls = cls2;
        this.method = null;
        this.constructor = null;
        this.name = str;
        this.signature = str2;
    }

    private MethodInfo(Class cls2, Constructor constructor2, String str, String str2) {
        this.cls = cls2;
        this.method = null;
        this.constructor = constructor2;
        this.name = str;
        this.signature = str2;
    }

    private MethodInfo(Class cls2, Method method2, String str, String str2) {
        this.cls = cls2;
        this.method = method2;
        this.constructor = null;
        this.name = str;
        this.signature = str2;
    }

    public static MethodInfo getMethod(Class cls2, String str, Class... clsArr) {
        Constructor constructor2;
        Constructor constructor3;
        Constructor constructor4;
        Method method2;
        Method method3;
        Method method4 = null;
        if (!(cls2 == null || str == null)) {
            if ("<clinit>".equals(str)) {
                return new MethodInfo(cls2, str, getSignature(clsArr, Void.TYPE));
            }
            if (!"<init>".equals(str)) {
                Class cls3 = cls2;
                Class cls4 = null;
                while (true) {
                    if (cls3 == null || cls3 == cls4) {
                        Arrays.toString(clsArr);
                    } else {
                        try {
                            method3 = cls2.getDeclaredMethod(str, clsArr);
                            method2 = method3;
                        } catch (NoSuchMethodException unused) {
                            method3 = null;
                            method2 = null;
                        }
                        if (method3 != null) {
                            method4 = method2;
                            break;
                        }
                        cls4 = cls3;
                        cls3 = cls3.getSuperclass();
                    }
                }
                if (method4 != null) {
                    return new MethodInfo(cls2, method4, str, getSignature(method4.getParameterTypes(), method4.getReturnType()));
                }
            } else {
                try {
                    constructor2 = cls2.getDeclaredConstructor(clsArr);
                    constructor3 = constructor2;
                } catch (NoSuchMethodException unused2) {
                    constructor2 = null;
                    constructor3 = null;
                }
                if (constructor2 == null) {
                    Class<?> enclosingClass = cls2.getEnclosingClass();
                    if (enclosingClass != null) {
                        int length = clsArr.length;
                        int i = length + 1;
                        new Class[i][0] = enclosingClass;
                        if (i < i) {
                            throw new IllegalArgumentException("Dest array is not big enough");
                        } else if (length >= length) {
                            for (int i2 = 0; i2 < length; i2++) {
                            }
                            try {
                                constructor4 = cls2.getDeclaredConstructor(clsArr);
                            } catch (NoSuchMethodException unused3) {
                                constructor4 = null;
                            }
                            Constructor constructor5 = constructor4;
                            if (constructor4 != null) {
                                return new MethodInfo(cls2, constructor5, str, getSignature(constructor4.getParameterTypes(), Void.TYPE));
                            }
                        } else {
                            throw new IllegalArgumentException("Src array lacks the num of needed elements");
                        }
                    }
                } else if (constructor2 != null) {
                    return new MethodInfo(cls2, constructor3, str, getSignature(constructor3.getParameterTypes(), Void.TYPE));
                } else {
                    Arrays.toString(clsArr);
                    return null;
                }
            }
            Arrays.toString(clsArr);
            return null;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0082, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00b4, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        android.util.Log.w("JitMethodInfo", java.lang.String.format("Cannot find class: %s for method: %s sig: %s", r11, r12, r13), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00c2, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c3, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c4, code lost:
        android.util.Log.w("JitMethodInfo", java.lang.String.format("Programming Error: class: %s for method: %s sig: %s", r11, r12, r13), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00d1, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b4 A[ExcHandler: ClassNotFoundException | NoClassDefFoundError (r2v0 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00c3 A[ExcHandler: Error | RuntimeException (r2v1 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:58:0x00b7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.common.jit.common.MethodInfo getMethod(java.lang.String r11, java.lang.String r12, java.lang.String r13) {
        /*
            r10 = 0
            java.lang.String r3 = "JitMethodInfo"
            if (r11 == 0) goto L_0x00d2
            if (r12 == 0) goto L_0x00d2
            if (r13 == 0) goto L_0x00d2
            java.lang.Class r7 = java.lang.Class.forName(r11)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            if (r13 == 0) goto L_0x009e
            r0 = 0
            char r1 = r13.charAt(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            r0 = 40
            if (r1 != r0) goto L_0x009e
            r0 = 41
            int r6 = r13.indexOf(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            if (r6 < 0) goto L_0x009e
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            r5.<init>()     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            r2 = 1
        L_0x0026:
            if (r2 >= r6) goto L_0x0091
            char r4 = r13.charAt(r2)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            r8 = 0
        L_0x002d:
            r0 = 91
            if (r4 != r0) goto L_0x003a
            int r8 = r8 + 1
            int r2 = r2 + 1
            char r4 = r13.charAt(r2)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            goto L_0x002d
        L_0x003a:
            r9 = 0
            if (r8 <= 0) goto L_0x003e
            r9 = 1
        L_0x003e:
            java.util.Map r1 = com.facebook.common.jit.common.MethodInfo.SIGNATURE_TO_PRIMITIVE     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.Character r0 = java.lang.Character.valueOf(r4)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.Class r0 = (java.lang.Class) r0     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            if (r0 == 0) goto L_0x005b
            if (r9 == 0) goto L_0x0055
            boolean r0 = addArrayType(r5, r0, r8)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            if (r0 != 0) goto L_0x0058
            goto L_0x009e
        L_0x0055:
            r5.add(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
        L_0x0058:
            int r2 = r2 + 1
            goto L_0x0026
        L_0x005b:
            r0 = 76
            if (r4 != r0) goto L_0x009e
            r0 = 59
            int r4 = r13.indexOf(r0, r2)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            if (r4 < 0) goto L_0x009e
            int r1 = r2 + 1
            if (r4 > r1) goto L_0x0071
            r0 = r10
        L_0x006c:
            if (r0 == 0) goto L_0x009e
            if (r9 == 0) goto L_0x008b
            goto L_0x0084
        L_0x0071:
            java.lang.String r2 = r13.substring(r1, r4)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            r1 = 47
            r0 = 46
            java.lang.String r0 = r2.replace(r1, r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x0082, ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            goto L_0x006c
        L_0x0082:
            r0 = r10
            goto L_0x006c
        L_0x0084:
            boolean r0 = addArrayType(r5, r0, r8)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            if (r0 != 0) goto L_0x008e
            goto L_0x009e
        L_0x008b:
            r5.add(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
        L_0x008e:
            int r2 = r4 + 1
            goto L_0x0026
        L_0x0091:
            int r0 = r5.size()     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.Object[] r0 = r5.toArray(r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.Class[] r0 = (java.lang.Class[]) r0     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            goto L_0x009f
        L_0x009e:
            r0 = r10
        L_0x009f:
            if (r0 != 0) goto L_0x00af
            java.lang.String r1 = "Cannot find class: %s for method: %s sig: %s. Cannot parse sig"
            java.lang.Object[] r0 = new java.lang.Object[]{r11, r12, r13}     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            android.util.Log.w(r3, r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            return r10
        L_0x00af:
            com.facebook.common.jit.common.MethodInfo r0 = getMethod(r7, r12, r0)     // Catch:{ ClassNotFoundException | NoClassDefFoundError -> 0x00b4 }
            return r0
        L_0x00b4:
            r2 = move-exception
            java.lang.String r1 = "Cannot find class: %s for method: %s sig: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r11, r12, r13}     // Catch:{ Error | RuntimeException -> 0x00c3, Error | RuntimeException -> 0x00c3 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ Error | RuntimeException -> 0x00c3, Error | RuntimeException -> 0x00c3 }
            android.util.Log.w(r3, r0, r2)     // Catch:{ Error | RuntimeException -> 0x00c3, Error | RuntimeException -> 0x00c3 }
            return r10
        L_0x00c3:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r11, r12, r13}
            java.lang.String r0 = "Programming Error: class: %s for method: %s sig: %s"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            android.util.Log.w(r3, r0, r2)
            return r10
        L_0x00d2:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.jit.common.MethodInfo.getMethod(java.lang.String, java.lang.String, java.lang.String):com.facebook.common.jit.common.MethodInfo");
    }
}
