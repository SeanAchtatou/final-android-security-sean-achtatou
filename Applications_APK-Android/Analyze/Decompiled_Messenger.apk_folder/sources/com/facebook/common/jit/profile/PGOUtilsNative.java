package com.facebook.common.jit.profile;

import java.lang.reflect.Field;

public final class PGOUtilsNative {
    public static Field A00;
    public static boolean A01;
    public static final boolean A02;

    public static native void nativeAddProfilerCodePaths(String[] strArr);

    private static native boolean nativeChangeOldPgoProfilerOptions(int i, int i2, int i3, double d, double d2);

    private static native boolean nativeDisableProfiler();

    private static native void nativeForcePgoProfileSave();

    private static native String nativeGetErrorMessage();

    public static native boolean nativeGetPgoData(String str, String str2, int[] iArr, double[] dArr, boolean z);

    public static native Object nativeGetPgoMethodInfo(String str, Object[] objArr);

    private static native boolean nativeInitialize(boolean z, boolean z2, int i, String str);

    public static native boolean nativeIsProfileChangeSignificant(String str, String str2, boolean z);

    private static native boolean nativeIsProfilerStarted();

    private static native boolean nativeStartProfiler(long j);

    static {
        boolean z;
        try {
            z = ((IPgoLoader) Class.forName("com.facebook.common.jit.profile.PgoLibLoader").newInstance()).load();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
            z = false;
        }
        A02 = z;
    }

    private PGOUtilsNative() {
    }
}
