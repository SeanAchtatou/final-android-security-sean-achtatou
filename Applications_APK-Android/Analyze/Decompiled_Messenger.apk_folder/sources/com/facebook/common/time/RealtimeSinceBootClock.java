package com.facebook.common.time;

import X.AnonymousClass069;
import android.os.SystemClock;

public class RealtimeSinceBootClock implements AnonymousClass069 {
    public static final RealtimeSinceBootClock A00 = new RealtimeSinceBootClock();

    private RealtimeSinceBootClock() {
    }

    public static RealtimeSinceBootClock get() {
        return A00;
    }

    public long now() {
        return SystemClock.elapsedRealtime();
    }
}
