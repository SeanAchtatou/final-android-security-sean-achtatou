package com.facebook.common.time;

import X.AnonymousClass069;
import X.AnonymousClass09O;
import android.os.SystemClock;

public class AwakeTimeSinceBootClock implements AnonymousClass069, AnonymousClass09O {
    public static final AwakeTimeSinceBootClock INSTANCE = new AwakeTimeSinceBootClock();

    private AwakeTimeSinceBootClock() {
    }

    public static AwakeTimeSinceBootClock get() {
        return INSTANCE;
    }

    public long now() {
        return SystemClock.uptimeMillis();
    }

    public long nowNanos() {
        return System.nanoTime();
    }
}
