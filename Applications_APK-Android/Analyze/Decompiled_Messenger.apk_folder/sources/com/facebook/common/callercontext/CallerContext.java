package com.facebook.common.callercontext;

import X.AnonymousClass07V;
import X.AnonymousClass217;
import X.C05520Zg;
import X.C10610kX;
import X.C11510nI;
import X.C14620th;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public class CallerContext implements Parcelable {
    public static boolean A05;
    public static final CallerContext A06 = new CallerContext();
    public static final Parcelable.Creator CREATOR = new C10610kX();
    public final ContextChain A00;
    public final String A01;
    public final String A02;
    public final String A03;
    private final String A04;

    public static CallerContext A09(String str) {
        A0D(str);
        return new CallerContext(str, (String) null, (String) null, (String) null, (ContextChain) null);
    }

    public static CallerContext A0B(String str, String str2) {
        A0D(str);
        return new CallerContext(str, (String) null, str2, (String) null, (ContextChain) null);
    }

    public static CallerContext A0C(String str, String str2, String str3) {
        A0D(str);
        return new CallerContext(str, str2, str3, str2, (ContextChain) null);
    }

    public int describeContents() {
        return 0;
    }

    public static CallerContext A00(Context context) {
        if (context == null) {
            return null;
        }
        Class<?> cls = context.getClass();
        if (!C11510nI.class.isAssignableFrom(cls)) {
            return A0A(AnonymousClass07V.A00(cls), "unknown");
        }
        C11510nI r2 = (C11510nI) context;
        if (r2 == null) {
            return null;
        }
        return A06(r2.getClass(), r2.Act());
    }

    public static CallerContext A01(CallerContext callerContext, ContextChain contextChain) {
        if (callerContext == null) {
            if (contextChain == null) {
                return null;
            }
            return A03(contextChain);
        } else if (contextChain == null) {
            return callerContext;
        } else {
            return new CallerContext(callerContext.A02, callerContext.A01, callerContext.A04, callerContext.A03, contextChain);
        }
    }

    public static CallerContext A02(CallerContext callerContext, String str) {
        if (callerContext.A04 != null) {
            return callerContext;
        }
        return new CallerContext(callerContext.A02, callerContext.A01, str, callerContext.A03);
    }

    public static CallerContext A03(ContextChain contextChain) {
        return new CallerContext(contextChain.A03, (String) null, (String) null, (String) null, contextChain);
    }

    public static CallerContext A04(Class cls) {
        return new CallerContext(cls, (String) null, (String) null, (String) null, (ContextChain) null);
    }

    public static CallerContext A05(Class cls) {
        return new CallerContext(AnonymousClass07V.A00(cls), null, null, null);
    }

    public static CallerContext A06(Class cls, String str) {
        return new CallerContext(cls, str, str, str, (ContextChain) null);
    }

    public static CallerContext A07(Class cls, String str) {
        return new CallerContext(cls, (String) null, str, (String) null, (ContextChain) null);
    }

    public static CallerContext A08(Class cls, String str, String str2) {
        return new CallerContext(cls, str, str2, str, (ContextChain) null);
    }

    public static void A0D(String str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("callingClassName for the CallerContext cannot be null nor empty.");
        }
    }

    public String A0F() {
        String str = this.A01;
        if (str == null) {
            return "unknown";
        }
        return str;
    }

    public String A0G() {
        String str = this.A04;
        if (str == null) {
            return "unknown";
        }
        return str;
    }

    public String A0H() {
        String str = this.A03;
        if (str == null) {
            return "unknown";
        }
        return str;
    }

    public boolean equals(Object obj) {
        if (A05) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
        } else if (!(obj instanceof CallerContext)) {
            return false;
        }
        CallerContext callerContext = (CallerContext) obj;
        if (!C14620th.A01(this.A02, callerContext.A02) || !C14620th.A01(this.A01, callerContext.A01) || !C14620th.A01(this.A04, callerContext.A04) || !C14620th.A01(this.A03, callerContext.A03) || !C14620th.A01(this.A00, callerContext.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A02, this.A01, this.A04, this.A03, this.A00});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A04);
        parcel.writeString(this.A01);
        parcel.writeString(this.A03);
        parcel.writeParcelable(this.A00, i);
    }

    public static CallerContext A0A(String str, String str2) {
        A0D(str);
        return new CallerContext(str, str2, str2, str2);
    }

    public AnonymousClass217 A0E() {
        AnonymousClass217 A002 = C14620th.A00(this);
        AnonymousClass217.A00(A002, "Calling Class Name", this.A02);
        AnonymousClass217.A00(A002, "Analytics Tag", this.A01);
        AnonymousClass217.A00(A002, "Feature tag", this.A04);
        AnonymousClass217.A00(A002, "Module Analytics Tag", this.A03);
        AnonymousClass217.A00(A002, "Context Chain", this.A00);
        return A002;
    }

    public String toString() {
        return A0E().toString();
    }

    private CallerContext() {
        this.A02 = null;
        this.A01 = null;
        this.A03 = null;
        this.A04 = null;
        this.A00 = null;
    }

    public CallerContext(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A04 = parcel.readString();
        this.A01 = parcel.readString();
        this.A03 = parcel.readString();
        this.A00 = (ContextChain) parcel.readParcelable(ContextChain.class.getClassLoader());
    }

    public CallerContext(CallerContext callerContext) {
        this.A02 = callerContext.A02;
        this.A01 = callerContext.A01;
        this.A03 = callerContext.A03;
        this.A04 = callerContext.A04;
        this.A00 = callerContext.A00;
    }

    public CallerContext(Class cls, String str, String str2, String str3, ContextChain contextChain) {
        C05520Zg.A02(cls);
        this.A02 = AnonymousClass07V.A00(cls);
        this.A01 = str;
        this.A04 = str2;
        this.A03 = str3;
        this.A00 = contextChain;
    }

    public CallerContext(String str, String str2, String str3, String str4) {
        this.A02 = str;
        this.A01 = str2;
        this.A04 = str3;
        this.A03 = str4;
        this.A00 = null;
    }

    public CallerContext(String str, String str2, String str3, String str4, ContextChain contextChain) {
        this.A02 = str;
        this.A01 = str2;
        this.A04 = str3;
        this.A03 = str4;
        this.A00 = contextChain;
    }
}
