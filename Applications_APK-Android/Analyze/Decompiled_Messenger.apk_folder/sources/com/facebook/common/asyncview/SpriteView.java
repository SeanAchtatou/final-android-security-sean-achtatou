package com.facebook.common.asyncview;

import X.AnonymousClass08S;
import X.AnonymousClass0L3;
import X.AnonymousClass1Y3;
import X.C009408b;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.SurfaceHolder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL10;

public class SpriteView extends C009408b {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public Matrix A08;
    public AnonymousClass0L3 A09;
    public EGL10 A0A;
    public EGLDisplay A0B;
    public EGLSurface A0C;
    public GL10 A0D;
    public boolean A0E;
    public float[] A0F = new float[16];
    private byte A0G = 0;
    private int A0H;
    private int A0I;
    private EGLConfig A0J;
    private EGLContext A0K;
    public final ArrayList A0L = new ArrayList();
    public final float[] A0M = new float[16];
    public final float[] A0N = new float[16];
    public final int[] A0O = new int[1];

    public class Sprite {
        public SpriteView A00;
        public float A01 = 1.0f;
        public float A02 = 0.0f;
        public float A03 = 0.0f;
        public float A04 = 0.0f;
        public float A05 = 0.0f;
        public float A06 = 0.0f;
        public float A07 = 1.0f;
        public float A08 = 1.0f;
        public float A09 = 0.0f;
        public float A0A = 0.0f;
        public float A0B = 0.0f;
        public int A0C;
        public int A0D;
        public Matrix A0E;
        public Paint A0F;
        public RectF A0G;
        public boolean A0H;
        public boolean A0I;
        public float[] A0J;
        public final float A0K;
        public final float A0L;
        public final Bitmap A0M;

        private void A00() {
            SpriteView spriteView = this.A00;
            if (spriteView != null) {
                spriteView.A0N();
            }
        }

        public void setAlpha(float f) {
            if (this.A01 != f) {
                this.A01 = f;
                this.A0I = true;
                A00();
            }
        }

        public void setPivotX(float f) {
            if (this.A02 != f) {
                this.A02 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setPivotY(float f) {
            if (this.A03 != f) {
                this.A03 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setRotation(float f) {
            if (this.A06 != f) {
                this.A06 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setRotationX(float f) {
            if (this.A04 != f) {
                this.A04 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setRotationY(float f) {
            if (this.A05 != f) {
                this.A05 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setScaleX(float f) {
            if (this.A07 != f) {
                this.A07 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setScaleY(float f) {
            if (this.A08 != f) {
                this.A08 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setTranslationX(float f) {
            if (this.A09 != f) {
                this.A09 = f;
                this.A0H = true;
                A00();
            }
        }

        public void setTranslationY(float f) {
            if (this.A0A != f) {
                this.A0A = f;
                this.A0H = true;
                A00();
            }
        }

        public void setTranslationZ(float f) {
            if (this.A0B != f) {
                this.A0B = f;
                this.A0H = true;
                A00();
            }
        }

        public Sprite(Bitmap bitmap, float f, float f2) {
            if (bitmap != null) {
                this.A0M = bitmap;
                this.A0L = f;
                this.A0K = f2;
                return;
            }
            throw new IllegalArgumentException("bitmap cannot be null");
        }
    }

    private int A02(int i, String str) {
        int i2;
        try {
            i2 = GLES20.glCreateShader(i);
            try {
                A06();
                GLES20.glShaderSource(i2, str);
                A06();
                GLES20.glCompileShader(i2);
                A06();
                int[] iArr = this.A0O;
                GLES20.glGetShaderiv(i2, 35713, iArr, 0);
                A06();
                if (iArr[0] != 0) {
                    return i2;
                }
                String glGetShaderInfoLog = GLES20.glGetShaderInfoLog(i2);
                A06();
                Log.e("fb-SpriteView", AnonymousClass08S.A0J("shader compilation failed: ", glGetShaderInfoLog));
                throw new GLException(1281, "shader compilation failed");
            } catch (Throwable th) {
                th = th;
                GLES20.glDeleteShader(i2);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            i2 = 0;
            GLES20.glDeleteShader(i2);
            throw th;
        }
    }

    private int A03(int i, float[] fArr) {
        int i2;
        try {
            int[] iArr = this.A0O;
            GLES20.glGenBuffers(1, iArr, 0);
            A06();
            i2 = iArr[0];
            try {
                GLES20.glBindBuffer(i, i2);
                A06();
                GLES20.glBufferData(i, fArr.length << 2, FloatBuffer.wrap(fArr), 35044);
                A06();
                GLES20.glBindBuffer(i, 0);
                A06();
                return i2;
            } catch (Throwable th) {
                th = th;
                int[] iArr2 = this.A0O;
                iArr2[0] = i2;
                GLES20.glDeleteBuffers(1, iArr2, 0);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            i2 = 0;
            int[] iArr22 = this.A0O;
            iArr22[0] = i2;
            GLES20.glDeleteBuffers(1, iArr22, 0);
            throw th;
        }
    }

    private void A0B(Sprite sprite) {
        sprite.A0J = null;
        int i = sprite.A0D;
        int[] iArr = this.A0O;
        iArr[0] = i;
        GLES20.glDeleteBuffers(1, iArr, 0);
        sprite.A0D = 0;
        int i2 = sprite.A0C;
        int[] iArr2 = this.A0O;
        iArr2[0] = i2;
        GLES20.glDeleteTextures(1, iArr2, 0);
        sprite.A0C = 0;
    }

    private void A04() {
        if (this.A0A != null || this.A0B != null || this.A0J != null || this.A0K != null || this.A0C != null || this.A0D != null || this.A03 != 0 || this.A02 != 0 || this.A01 != 0 || this.A07 != 0 || this.A00 != 0 || this.A06 != 0 || this.A04 != 0 || this.A05 != 0) {
            throw new AssertionError();
        }
    }

    private void A05() {
        String str;
        int eglGetError = this.A0A.eglGetError();
        switch (eglGetError) {
            case 12288:
                break;
            case 12289:
                str = "EGL_NOT_INITIALIZED";
                throw new AnonymousClass0L3(eglGetError, str);
            case AnonymousClass1Y3.BUe /*12290*/:
                str = "EGL_BAD_ACCESS";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12291:
                str = "EGL_BAD_ALLOC";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12292:
                str = "EGL_BAD_ATTRIBUTE";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12293:
                str = "EGL_BAD_CONFIG";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12294:
                str = "EGL_BAD_CONTEXT";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12295:
                str = "EGL_BAD_CURRENT_SURFACE";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12296:
                str = "EGL_BAD_DISPLAY";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12297:
                str = "EGL_BAD_MATCH";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12298:
                str = "EGL_BAD_NATIVE_PIXMAP";
                throw new AnonymousClass0L3(eglGetError, str);
            case AnonymousClass1Y3.BUf /*12299*/:
                str = "EGL_BAD_NATIVE_WINDOW";
                throw new AnonymousClass0L3(eglGetError, str);
            case AnonymousClass1Y3.BUg /*12300*/:
                str = "EGL_BAD_PARAMETER";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12301:
                str = "EGL_BAD_SURFACE";
                throw new AnonymousClass0L3(eglGetError, str);
            case 12302:
                str = "EGL_CONTEXT_LOST";
                throw new AnonymousClass0L3(eglGetError, str);
            default:
                str = AnonymousClass08S.A09("unknown EGL error ", eglGetError);
                throw new AnonymousClass0L3(eglGetError, str);
        }
    }

    private void A06() {
        int glGetError = this.A0D.glGetError();
        if (glGetError != 0) {
            throw new GLException(glGetError);
        }
    }

    private void A07() {
        ArrayList arrayList = this.A0L;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            A0B((Sprite) arrayList.get(i));
        }
        int i2 = this.A05;
        int[] iArr = this.A0O;
        iArr[0] = i2;
        GLES20.glDeleteBuffers(1, iArr, 0);
        this.A05 = 0;
        GLES20.glDeleteProgram(this.A03);
        this.A03 = 0;
        this.A04 = 0;
        this.A01 = 0;
        this.A07 = 0;
        this.A06 = 0;
        this.A00 = 0;
        this.A02 = 0;
        this.A0D = null;
        if (this.A0C != null) {
            EGLDisplay eGLDisplay = this.A0B;
            if (eGLDisplay != null) {
                EGL10 egl10 = this.A0A;
                EGLSurface eGLSurface = EGL10.EGL_NO_SURFACE;
                egl10.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, EGL10.EGL_NO_CONTEXT);
                this.A0A.eglDestroySurface(this.A0B, this.A0C);
                this.A0C = null;
            } else {
                throw new AssertionError();
            }
        }
        EGLContext eGLContext = this.A0K;
        if (eGLContext != null) {
            if (!this.A0A.eglDestroyContext(this.A0B, eGLContext)) {
                Log.e("fb-SpriteView", "eglDestroyContext failed");
            }
            this.A0K = null;
        }
        this.A0J = null;
        EGLDisplay eGLDisplay2 = this.A0B;
        if (eGLDisplay2 != null) {
            this.A0A.eglTerminate(eGLDisplay2);
            this.A0B = null;
        }
        this.A0A = null;
    }

    private void A09(int i, int i2) {
        if (this.A0A == null) {
            float f = ((float) i) / 2.0f;
            float f2 = ((float) i2) / 2.0f;
            Matrix matrix = new Matrix();
            matrix.postScale(f, -f2);
            matrix.postTranslate(f, f2);
            float[] fArr = this.A0F;
            if (fArr.length == 16) {
                float f3 = fArr[0];
                float f4 = fArr[5];
                float f5 = fArr[10];
                float f6 = fArr[12];
                float f7 = fArr[13];
                if (fArr[14] == 0.0f && ((f5 == 1.0f || f5 == -1.0f) && fArr[1] == 0.0f && fArr[2] == 0.0f && fArr[4] == 0.0f && fArr[6] == 0.0f && fArr[8] == 0.0f && fArr[9] == 0.0f && fArr[3] == 0.0f && fArr[7] == 0.0f && fArr[11] == 0.0f && fArr[15] == 1.0f)) {
                    Matrix matrix2 = new Matrix();
                    float[] fArr2 = new float[9];
                    matrix2.getValues(fArr2);
                    fArr2[2] = f6;
                    fArr2[5] = f7;
                    fArr2[0] = f3;
                    fArr2[4] = f4;
                    matrix2.setValues(fArr2);
                    matrix.preConcat(matrix2);
                    this.A08 = matrix;
                    Iterator it = this.A0L.iterator();
                    while (it.hasNext()) {
                        Sprite sprite = (Sprite) it.next();
                        sprite.A0H = true;
                        sprite.A0I = true;
                    }
                    return;
                }
                throw new UnsupportedOperationException("GL matrix is not 2D-compatible");
            }
            throw new AssertionError("bad GL matrix");
        }
        throw new AssertionError("cannot start software rendeirng with EGL active");
    }

    private void A0A(SurfaceHolder surfaceHolder) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        A04();
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        this.A0A = egl10;
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        this.A0B = eglGetDisplay;
        A05();
        if (eglGetDisplay != EGL10.EGL_NO_DISPLAY) {
            if (this.A0A.eglInitialize(eglGetDisplay, new int[2])) {
                int[] iArr = {AnonymousClass1Y3.BUp, 8, 12323, 8, 12322, 8, AnonymousClass1Y3.BUo, 8, 12325, 0, AnonymousClass1Y3.BUq, 0, 12352, 4, 12344};
                int[] iArr2 = new int[1];
                if (this.A0A.eglChooseConfig(this.A0B, iArr, null, 0, iArr2)) {
                    int i9 = iArr2[0];
                    EGLConfig[] eGLConfigArr = new EGLConfig[i9];
                    if (this.A0A.eglChooseConfig(this.A0B, iArr, eGLConfigArr, i9, iArr2)) {
                        int length = eGLConfigArr.length;
                        int i10 = 0;
                        while (true) {
                            if (i10 >= length) {
                                break;
                            }
                            EGLConfig eGLConfig = eGLConfigArr[i10];
                            if (this.A0A.eglGetConfigAttrib(this.A0B, eGLConfig, 12325, iArr2)) {
                                i3 = iArr2[0];
                            } else {
                                i3 = 0;
                            }
                            if (this.A0A.eglGetConfigAttrib(this.A0B, eGLConfig, AnonymousClass1Y3.BUq, iArr2)) {
                                i4 = iArr2[0];
                            } else {
                                i4 = 0;
                            }
                            if (i3 >= 0 && i4 >= 0) {
                                if (this.A0A.eglGetConfigAttrib(this.A0B, eGLConfig, AnonymousClass1Y3.BUp, iArr2)) {
                                    i5 = iArr2[0];
                                } else {
                                    i5 = 0;
                                }
                                if (this.A0A.eglGetConfigAttrib(this.A0B, eGLConfig, 12323, iArr2)) {
                                    i6 = iArr2[0];
                                } else {
                                    i6 = 0;
                                }
                                if (this.A0A.eglGetConfigAttrib(this.A0B, eGLConfig, 12322, iArr2)) {
                                    i7 = iArr2[0];
                                } else {
                                    i7 = 0;
                                }
                                if (this.A0A.eglGetConfigAttrib(this.A0B, eGLConfig, AnonymousClass1Y3.BUo, iArr2)) {
                                    i8 = iArr2[0];
                                } else {
                                    i8 = 0;
                                }
                                if (i5 == 8 && i6 == 8 && i7 == 8 && i8 == 8) {
                                    this.A0J = eGLConfig;
                                    break;
                                }
                            }
                            i10++;
                        }
                        EGLConfig eGLConfig2 = this.A0J;
                        if (eGLConfig2 != null) {
                            EGL10 egl102 = this.A0A;
                            EGLDisplay eGLDisplay = this.A0B;
                            EGLContext eGLContext = EGL10.EGL_NO_CONTEXT;
                            EGLContext eglCreateContext = egl102.eglCreateContext(eGLDisplay, eGLConfig2, eGLContext, new int[]{12440, 2, 12344});
                            this.A0K = eglCreateContext;
                            if (eglCreateContext == null || eglCreateContext == eGLContext) {
                                this.A0K = null;
                                A05();
                                throw new AnonymousClass0L3(-1, "eglCreateContext failed");
                            }
                            this.A0D = (GL10) eglCreateContext.getGL();
                            EGLSurface eglCreateWindowSurface = this.A0A.eglCreateWindowSurface(this.A0B, this.A0J, surfaceHolder, null);
                            this.A0C = eglCreateWindowSurface;
                            if (eglCreateWindowSurface == null) {
                                A05();
                                throw new AnonymousClass0L3(-1, "eglCreateWindowSurface failed");
                            } else if (this.A0A.eglMakeCurrent(this.A0B, eglCreateWindowSurface, eglCreateWindowSurface, this.A0K)) {
                                GLES20.glDisable(AnonymousClass1Y3.AO4);
                                A06();
                                GLES20.glEnable(AnonymousClass1Y3.AP0);
                                A06();
                                GLES20.glBlendFunc(1, 771);
                                A06();
                                GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                                A06();
                                int i11 = 0;
                                try {
                                    i = GLES20.glCreateProgram();
                                    try {
                                        A06();
                                        i2 = A02(35633, "precision mediump float;attribute vec4 vPosition;attribute vec2 vTexCoord;uniform mat4 mModel;uniform mat4 mView;varying vec2 texCoord;void main() {  gl_Position = mView * mModel * vPosition;  texCoord = vTexCoord; }");
                                    } catch (Throwable th) {
                                        th = th;
                                        i2 = 0;
                                        GLES20.glDeleteShader(i2);
                                        GLES20.glDeleteShader(i11);
                                        GLES20.glDeleteProgram(i);
                                        throw th;
                                    }
                                    try {
                                        GLES20.glAttachShader(i, i2);
                                        A06();
                                        i11 = A02(35632, "precision mediump float;varying vec2 texCoord;uniform float alpha;uniform sampler2D tex;void main() {  gl_FragColor = texture2D(tex, vec2(texCoord.x, 1.0-texCoord.y)) * alpha;}");
                                        GLES20.glAttachShader(i, i11);
                                        A06();
                                        GLES20.glLinkProgram(i);
                                        A06();
                                        GLES20.glDeleteShader(i2);
                                        GLES20.glDeleteShader(i11);
                                        this.A03 = i;
                                        this.A00 = GLES20.glGetUniformLocation(i, "alpha");
                                        A06();
                                        this.A06 = GLES20.glGetUniformLocation(this.A03, "tex");
                                        A06();
                                        this.A02 = GLES20.glGetAttribLocation(this.A03, "vPosition");
                                        A06();
                                        this.A04 = GLES20.glGetAttribLocation(this.A03, "vTexCoord");
                                        A06();
                                        this.A01 = GLES20.glGetUniformLocation(this.A03, "mModel");
                                        A06();
                                        this.A07 = GLES20.glGetUniformLocation(this.A03, "mView");
                                        A06();
                                        this.A05 = A03(34962, new float[]{0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f});
                                        ArrayList arrayList = this.A0L;
                                        int size = arrayList.size();
                                        for (int i12 = 0; i12 < size; i12++) {
                                            A0C((Sprite) arrayList.get(i12));
                                        }
                                        return;
                                    } catch (Throwable th2) {
                                        th = th2;
                                        GLES20.glDeleteShader(i2);
                                        GLES20.glDeleteShader(i11);
                                        GLES20.glDeleteProgram(i);
                                        throw th;
                                    }
                                } catch (Throwable th3) {
                                    th = th3;
                                    i2 = 0;
                                    i = 0;
                                    GLES20.glDeleteShader(i2);
                                    GLES20.glDeleteShader(i11);
                                    GLES20.glDeleteProgram(i);
                                    throw th;
                                }
                            } else {
                                A05();
                                throw new AnonymousClass0L3(-1, "eglMakeCurrent failed");
                            }
                        } else {
                            throw new AnonymousClass0L3(-1, "could not find suitable EGL config");
                        }
                    }
                }
                A05();
                throw new AnonymousClass0L3(-1, "eglChooseConfig failed");
            }
            A05();
            throw new AnonymousClass0L3(-1, "eglInitialize failed");
        }
        this.A0B = null;
        throw new AnonymousClass0L3(-1, "eglGetDisplay failed");
    }

    private void A0C(Sprite sprite) {
        int i;
        if (sprite.A0C == 0 && sprite.A0D == 0 && sprite.A0J == null) {
            sprite.A0H = true;
            try {
                Bitmap bitmap = sprite.A0M;
                try {
                    int[] iArr = this.A0O;
                    this.A0D.glGenTextures(1, iArr, 0);
                    A06();
                    i = iArr[0];
                    try {
                        GLES20.glBindTexture(AnonymousClass1Y3.ATN, i);
                        A06();
                        GLES20.glTexParameteri(AnonymousClass1Y3.ATN, AnonymousClass1Y3.BPK, 33071);
                        A06();
                        GLES20.glTexParameteri(AnonymousClass1Y3.ATN, 10243, 33071);
                        A06();
                        GLES20.glTexParameteri(AnonymousClass1Y3.ATN, AnonymousClass1Y3.BPJ, AnonymousClass1Y3.BKs);
                        A06();
                        GLUtils.texImage2D(AnonymousClass1Y3.ATN, 0, bitmap, 0);
                        A06();
                        GLES20.glBindTexture(AnonymousClass1Y3.ATN, 0);
                        A06();
                        sprite.A0C = i;
                        float f = sprite.A0L / 2.0f;
                        float f2 = sprite.A0K / 2.0f;
                        float f3 = -f;
                        float f4 = -f2;
                        sprite.A0D = A03(34962, new float[]{f3, f4, f, f4, f3, f2, f, f2});
                        sprite.A0J = new float[16];
                    } catch (Throwable th) {
                        th = th;
                        int[] iArr2 = this.A0O;
                        iArr2[0] = i;
                        GLES20.glDeleteTextures(1, iArr2, 0);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    i = 0;
                    int[] iArr22 = this.A0O;
                    iArr22[0] = i;
                    GLES20.glDeleteTextures(1, iArr22, 0);
                    throw th;
                }
            } catch (Throwable th3) {
                A0B(sprite);
                throw th3;
            }
        } else {
            throw new AssertionError();
        }
    }

    public void A0J() {
        if (this.A0G == 0) {
            A07();
        }
        super.A0J();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x01df, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x01e0, code lost:
        android.util.Log.w("fb-SpriteView", "OpenGL error: falling back to software", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x01e9, code lost:
        if ((r2 instanceof X.AnonymousClass0L3) != false) goto L_0x01eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01eb, code lost:
        r5.A09 = (X.AnonymousClass0L3) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01ef, code lost:
        A07();
        r5.A0G = 1;
        A09(r5.A0I, r5.A0H);
        r14 = 1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01df A[ExcHandler: GLException (r2v6 'e' android.opengl.GLException A[CUSTOM_DECLARE]), Splitter:B:27:0x01cb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Q(android.view.SurfaceHolder r31, long r32) {
        /*
            r30 = this;
            java.lang.String r21 = "fb-SpriteView"
            r29 = r31
            r6 = r32
            r5 = r30
            r1 = r29
            r2 = r6
            super.A0Q(r1, r2)
            byte r14 = r5.A0G
            r12 = 1
            if (r14 != 0) goto L_0x01ff
        L_0x0013:
            r20 = 0
            r0 = 16384(0x4000, float:2.2959E-41)
            android.opengl.GLES20.glClear(r0)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            java.util.ArrayList r0 = r5.A0L     // Catch:{ 0L3 -> 0x01ca }
            r28 = r0
            int r19 = r28.size()     // Catch:{ 0L3 -> 0x01ca }
            r10 = 0
        L_0x0026:
            r0 = r19
            if (r10 >= r0) goto L_0x01bd
            r0 = r28
            java.lang.Object r18 = r0.get(r10)     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            com.facebook.common.asyncview.SpriteView$Sprite r0 = (com.facebook.common.asyncview.SpriteView.Sprite) r0     // Catch:{ 0L3 -> 0x01ca }
            r18 = r0
            boolean r0 = r0.A0H     // Catch:{ 0L3 -> 0x01ca }
            if (r0 == 0) goto L_0x00a8
            r0 = r18
            float[] r8 = r0.A0J     // Catch:{ 0L3 -> 0x01ca }
            r4 = 0
            android.opengl.Matrix.setIdentityM(r8, r4)     // Catch:{ 0L3 -> 0x01ca }
            float[] r0 = r5.A0M     // Catch:{ 0L3 -> 0x01ca }
            r26 = r0
            float[] r3 = r5.A0N     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.Matrix.setIdentityM(r0, r4)     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            float r6 = r0.A09     // Catch:{ 0L3 -> 0x01ca }
            float r0 = r0.A02     // Catch:{ 0L3 -> 0x01ca }
            float r6 = r6 - r0
            r0 = r18
            float r2 = r0.A0A     // Catch:{ 0L3 -> 0x01ca }
            float r0 = r0.A03     // Catch:{ 0L3 -> 0x01ca }
            float r2 = r2 - r0
            r0 = r18
            float r1 = r0.A0B     // Catch:{ 0L3 -> 0x01ca }
            r0 = r26
            android.opengl.Matrix.translateM(r0, r4, r6, r2, r1)     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            float r1 = r0.A04     // Catch:{ 0L3 -> 0x01ca }
            r2 = 0
            int r0 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0117
            r0 = r18
            float r0 = r0.A05     // Catch:{ 0L3 -> 0x01ca }
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0117
            r0 = r18
            float r0 = r0.A06     // Catch:{ 0L3 -> 0x01ca }
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0117
            r1 = 16
            r0 = r26
            java.lang.System.arraycopy(r0, r4, r8, r4, r1)     // Catch:{ 0L3 -> 0x01ca }
        L_0x0082:
            r0 = r18
            float r3 = r0.A07     // Catch:{ 0L3 -> 0x01ca }
            r1 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0094
            r0 = r18
            float r0 = r0.A08     // Catch:{ 0L3 -> 0x01ca }
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x009b
        L_0x0094:
            r0 = r18
            float r0 = r0.A08     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.Matrix.scaleM(r8, r4, r3, r0, r1)     // Catch:{ 0L3 -> 0x01ca }
        L_0x009b:
            r0 = r18
            float r1 = r0.A02     // Catch:{ 0L3 -> 0x01ca }
            float r0 = r0.A03     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.Matrix.translateM(r8, r4, r1, r0, r2)     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            r0.A0H = r4     // Catch:{ 0L3 -> 0x01ca }
        L_0x00a8:
            int r0 = r5.A03     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.GLES20.glUseProgram(r0)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            int r3 = r5.A07     // Catch:{ 0L3 -> 0x01ca }
            float[] r2 = r5.A0F     // Catch:{ 0L3 -> 0x01ca }
            r1 = 0
            android.opengl.GLES20.glUniformMatrix4fv(r3, r12, r1, r2, r1)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            int r3 = r5.A01     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            float[] r2 = r0.A0J     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.GLES20.glUniformMatrix4fv(r3, r12, r1, r2, r1)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            int r1 = r5.A00     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            float r0 = r0.A01     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.GLES20.glUniform1f(r1, r0)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            r0 = 33984(0x84c0, float:4.7622E-41)
            android.opengl.GLES20.glActiveTexture(r0)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            int r0 = r0.A0C     // Catch:{ 0L3 -> 0x01ca }
            r3 = 3553(0xde1, float:4.979E-42)
            android.opengl.GLES20.glBindTexture(r3, r0)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            r0 = r18
            int r2 = r0.A0D     // Catch:{ 0L3 -> 0x01ca }
            int r1 = r5.A02     // Catch:{ 0L3 -> 0x01ca }
            r5.A08(r2, r1)     // Catch:{ 0L3 -> 0x01ca }
            int r2 = r5.A05     // Catch:{ 0L3 -> 0x01ca }
            int r1 = r5.A04     // Catch:{ 0L3 -> 0x01ca }
            r5.A08(r2, r1)     // Catch:{ 0L3 -> 0x01ca }
            int r0 = r5.A06     // Catch:{ 0L3 -> 0x01ca }
            r2 = 0
            android.opengl.GLES20.glUniform1i(r0, r2)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            r1 = 5
            r0 = 4
            android.opengl.GLES20.glDrawArrays(r1, r2, r0)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.GLES20.glBindTexture(r3, r2)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            android.opengl.GLES20.glUseProgram(r2)     // Catch:{ 0L3 -> 0x01ca }
            r5.A06()     // Catch:{ 0L3 -> 0x01ca }
            goto L_0x01b9
        L_0x0117:
            r0 = r18
            float r7 = r0.A05     // Catch:{ 0L3 -> 0x01ca }
            float r0 = r0.A06     // Catch:{ 0L3 -> 0x01ca }
            float r9 = -r0
            r0 = 1016003124(0x3c8efa34, float:0.01745329)
            float r1 = r1 * r0
            float r7 = r7 * r0
            float r9 = r9 * r0
            double r0 = (double) r1     // Catch:{ 0L3 -> 0x01ca }
            r24 = r0
            double r0 = java.lang.Math.sin(r0)     // Catch:{ 0L3 -> 0x01ca }
            float r6 = (float) r0     // Catch:{ 0L3 -> 0x01ca }
            double r0 = (double) r7     // Catch:{ 0L3 -> 0x01ca }
            r22 = r0
            double r0 = java.lang.Math.sin(r0)     // Catch:{ 0L3 -> 0x01ca }
            float r7 = (float) r0     // Catch:{ 0L3 -> 0x01ca }
            double r0 = (double) r9     // Catch:{ 0L3 -> 0x01ca }
            r15 = r0
            double r0 = java.lang.Math.sin(r0)     // Catch:{ 0L3 -> 0x01ca }
            float r9 = (float) r0     // Catch:{ 0L3 -> 0x01ca }
            r0 = r24
            double r0 = java.lang.Math.cos(r0)     // Catch:{ 0L3 -> 0x01ca }
            float r11 = (float) r0     // Catch:{ 0L3 -> 0x01ca }
            r0 = r22
            double r0 = java.lang.Math.cos(r0)     // Catch:{ 0L3 -> 0x01ca }
            float r13 = (float) r0     // Catch:{ 0L3 -> 0x01ca }
            r0 = r15
            double r0 = java.lang.Math.cos(r0)     // Catch:{ 0L3 -> 0x01ca }
            float r15 = (float) r0     // Catch:{ 0L3 -> 0x01ca }
            float r1 = r11 * r7
            float r17 = r6 * r7
            float r0 = r13 * r15
            r3[r4] = r0     // Catch:{ 0L3 -> 0x01ca }
            float r0 = -r13
            float r0 = r0 * r9
            r3[r12] = r0     // Catch:{ 0L3 -> 0x01ca }
            r0 = 2
            r3[r0] = r7     // Catch:{ 0L3 -> 0x01ca }
            r0 = 3
            r3[r0] = r2     // Catch:{ 0L3 -> 0x01ca }
            r16 = 4
            float r7 = r17 * r15
            float r0 = r11 * r9
            float r7 = r7 + r0
            r3[r16] = r7     // Catch:{ 0L3 -> 0x01ca }
            r16 = 5
            r0 = r17
            float r7 = -r0
            float r7 = r7 * r9
            float r0 = r11 * r15
            float r7 = r7 + r0
            r3[r16] = r7     // Catch:{ 0L3 -> 0x01ca }
            r7 = 6
            float r0 = -r6
            float r0 = r0 * r13
            r3[r7] = r0     // Catch:{ 0L3 -> 0x01ca }
            r0 = 7
            r3[r0] = r2     // Catch:{ 0L3 -> 0x01ca }
            r16 = 8
            float r7 = -r1
            float r7 = r7 * r15
            float r0 = r6 * r9
            float r7 = r7 + r0
            r3[r16] = r7     // Catch:{ 0L3 -> 0x01ca }
            r0 = 9
            float r1 = r1 * r9
            float r6 = r6 * r15
            float r1 = r1 + r6
            r3[r0] = r1     // Catch:{ 0L3 -> 0x01ca }
            r0 = 10
            float r11 = r11 * r13
            r3[r0] = r11     // Catch:{ 0L3 -> 0x01ca }
            r0 = 11
            r3[r0] = r2     // Catch:{ 0L3 -> 0x01ca }
            r0 = 12
            r3[r0] = r2     // Catch:{ 0L3 -> 0x01ca }
            r0 = 13
            r3[r0] = r2     // Catch:{ 0L3 -> 0x01ca }
            r0 = 14
            r3[r0] = r2     // Catch:{ 0L3 -> 0x01ca }
            r1 = 15
            r0 = 1065353216(0x3f800000, float:1.0)
            r3[r1] = r0     // Catch:{ 0L3 -> 0x01ca }
            r23 = 0
            r25 = 0
            r27 = 0
            r22 = r8
            r24 = r26
            r26 = r3
            android.opengl.Matrix.multiplyMM(r22, r23, r24, r25, r26, r27)     // Catch:{ 0L3 -> 0x01ca }
            goto L_0x0082
        L_0x01b9:
            int r10 = r10 + 1
            goto L_0x0026
        L_0x01bd:
            javax.microedition.khronos.egl.EGL10 r2 = r5.A0A     // Catch:{ 0L3 -> 0x01ca }
            javax.microedition.khronos.egl.EGLDisplay r1 = r5.A0B     // Catch:{ 0L3 -> 0x01ca }
            javax.microedition.khronos.egl.EGLSurface r0 = r5.A0C     // Catch:{ 0L3 -> 0x01ca }
            r2.eglSwapBuffers(r1, r0)     // Catch:{ 0L3 -> 0x01ca }
            r5.A05()     // Catch:{ 0L3 -> 0x01ca }
            goto L_0x01d3
        L_0x01ca:
            r2 = move-exception
            int r1 = r2.error     // Catch:{ GLException -> 0x01df, GLException -> 0x01df }
            r0 = 12302(0x300e, float:1.7239E-41)
            if (r1 != r0) goto L_0x01de
            r20 = 1
        L_0x01d3:
            if (r20 == 0) goto L_0x01fd
            r5.A07()     // Catch:{ GLException -> 0x01df, GLException -> 0x01df }
            r0 = r29
            r5.A0A(r0)     // Catch:{ GLException -> 0x01df, GLException -> 0x01df }
            goto L_0x01fd
        L_0x01de:
            throw r2     // Catch:{ GLException -> 0x01df, GLException -> 0x01df }
        L_0x01df:
            r2 = move-exception
            java.lang.String r1 = "OpenGL error: falling back to software"
            r0 = r21
            android.util.Log.w(r0, r1, r2)
            boolean r0 = r2 instanceof X.AnonymousClass0L3
            if (r0 == 0) goto L_0x01ef
            X.0L3 r2 = (X.AnonymousClass0L3) r2
            r5.A09 = r2
        L_0x01ef:
            r5.A07()
            r5.A0G = r12
            int r1 = r5.A0I
            int r0 = r5.A0H
            r5.A09(r1, r0)
            r14 = 1
            goto L_0x01ff
        L_0x01fd:
            if (r20 != 0) goto L_0x0013
        L_0x01ff:
            if (r14 != r12) goto L_0x02c8
            boolean r0 = r5.A0E
            if (r0 != 0) goto L_0x02d2
            android.graphics.Canvas r6 = r29.lockCanvas()
            if (r6 != 0) goto L_0x021f
            r5.A0E = r12
            java.lang.String r1 = "fb-SpriteView"
            java.lang.String r0 = "failure to lock canvas: stopping all drawing!"
            android.util.Log.e(r1, r0)
            X.0L3 r2 = r5.A09
            if (r2 == 0) goto L_0x02d2
            int r1 = r2.error
            r0 = 12301(0x300d, float:1.7237E-41)
            if (r1 == r0) goto L_0x02d2
            throw r2
        L_0x021f:
            r6.save()
            android.graphics.Matrix r0 = r5.A08
            r6.setMatrix(r0)
            android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.CLEAR
            r8 = 0
            r6.drawColor(r8, r0)
            java.util.ArrayList r9 = r5.A0L
            int r7 = r9.size()
        L_0x0233:
            if (r8 >= r7) goto L_0x02bf
            java.lang.Object r11 = r9.get(r8)
            com.facebook.common.asyncview.SpriteView$Sprite r11 = (com.facebook.common.asyncview.SpriteView.Sprite) r11
            android.graphics.Paint r5 = r11.A0F
            boolean r0 = r11.A0I
            if (r0 == 0) goto L_0x0254
            if (r5 != 0) goto L_0x024b
            android.graphics.Paint r5 = new android.graphics.Paint
            r0 = 2
            r5.<init>(r0)
            r11.A0F = r5
        L_0x024b:
            float r1 = r11.A01
            r0 = 1132396544(0x437f0000, float:255.0)
            float r1 = r1 * r0
            int r0 = (int) r1
            r5.setAlpha(r0)
        L_0x0254:
            android.graphics.Matrix r10 = r11.A0E
            boolean r0 = r11.A0H
            if (r0 == 0) goto L_0x0288
            if (r10 != 0) goto L_0x0263
            android.graphics.Matrix r10 = new android.graphics.Matrix
            r10.<init>()
            r11.A0E = r10
        L_0x0263:
            r10.reset()
            float r1 = r11.A09
            float r0 = r11.A0A
            r10.setTranslate(r1, r0)
            float r2 = r11.A06
            float r1 = r11.A02
            float r0 = r11.A03
            r10.preRotate(r2, r1, r0)
            float r3 = r11.A07
            float r2 = r11.A08
            float r1 = r11.A02
            float r0 = r11.A03
            r10.preScale(r3, r2, r1, r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
            r10.preScale(r1, r0)
        L_0x0288:
            android.graphics.RectF r4 = r11.A0G
            if (r4 != 0) goto L_0x02a7
            android.graphics.RectF r4 = new android.graphics.RectF
            r4.<init>()
            r11.A0G = r4
            float r3 = r11.A0L
            float r0 = -r3
            r2 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 / r2
            r4.left = r0
            float r1 = r11.A0K
            float r0 = -r1
            float r0 = r0 / r2
            r4.top = r0
            float r3 = r3 / r2
            r4.right = r3
            float r1 = r1 / r2
            r4.bottom = r1
        L_0x02a7:
            r6.save()
            r6.concat(r10)     // Catch:{ all -> 0x02ba }
            android.graphics.Bitmap r1 = r11.A0M     // Catch:{ all -> 0x02ba }
            r0 = 0
            r6.drawBitmap(r1, r0, r4, r5)     // Catch:{ all -> 0x02ba }
            r6.restore()
            int r8 = r8 + 1
            goto L_0x0233
        L_0x02ba:
            r0 = move-exception
            r6.restore()
            throw r0
        L_0x02bf:
            r6.restore()
            r0 = r29
            r0.unlockCanvasAndPost(r6)
            return
        L_0x02c8:
            if (r14 == 0) goto L_0x02d2
            java.lang.AssertionError r1 = new java.lang.AssertionError
            java.lang.String r0 = "unknown draw style"
            r1.<init>(r0)
            throw r1
        L_0x02d2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.asyncview.SpriteView.A0Q(android.view.SurfaceHolder, long):void");
    }

    public SpriteView(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, int):void}
     arg types: [int, int, int, int, int, int]
     candidates:
      ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, java.nio.Buffer):void}
      ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, int):void} */
    private void A08(int i, int i2) {
        GLES20.glBindBuffer(34962, i);
        A06();
        GLES20.glEnableVertexAttribArray(i2);
        A06();
        GLES20.glVertexAttribPointer(i2, 2, 5126, false, 8, 0);
        A06();
    }

    public void A0K() {
        A04();
        this.A0L.clear();
        this.A0E = false;
        this.A09 = null;
        this.A0I = 0;
        this.A0H = 0;
        super.A0K();
    }

    public void A0P(SurfaceHolder surfaceHolder, int i, int i2) {
        super.A0P(surfaceHolder, i, i2);
        if (this.A0G == 0) {
            try {
                A0A(surfaceHolder);
            } catch (AnonymousClass0L3 | GLException e) {
                Log.w("fb-SpriteView", "failure to initialize OpenGL: switching to software", e);
                this.A0G = 1;
                A07();
            } catch (Throwable th) {
                A07();
                throw th;
            }
        }
        if (!(this.A0I == i && this.A0H == i2)) {
            this.A0I = i;
            this.A0H = i2;
            android.opengl.Matrix.setIdentityM(this.A0F, 0);
            float f = ((float) i) / 2.0f;
            float f2 = ((float) i2) / 2.0f;
            android.opengl.Matrix.setIdentityM(this.A0F, 0);
            android.opengl.Matrix.orthoM(this.A0F, 0, -f, f, -f2, f2, -1.0f, 1.0f);
        }
        if (this.A0G == 1) {
            A09(i, i2);
        }
    }

    public void A0R(Sprite sprite) {
        A0M();
        if (sprite.A00 == null) {
            sprite.A00 = this;
            sprite.A0H = true;
            sprite.A0I = true;
            if (this.A0A != null) {
                A0C(sprite);
            }
            this.A0L.add(sprite);
            A0N();
            return;
        }
        throw new IllegalStateException("sprite already in scene");
    }
}
