package com.facebook.common.json;

import X.AnonymousClass08S;
import X.AnonymousClass0jE;
import X.AnonymousClass1Y3;
import X.C04300To;
import X.C05360Yq;
import X.C14550ta;
import X.C182811d;
import X.C26791c3;
import X.C28231eT;
import X.C28271eX;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class FbJsonField {
    public final Field A00;
    public final Method A01;

    public final class ImmutableListJsonField extends FbJsonField {
        private C28231eT A00;
        private Class A01;

        public void deserialize(Object obj, C28271eX r5, C26791c3 r6) {
            try {
                ImmutableList A002 = C14550ta.A00(r5, r6, this.A01, this.A00);
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, A002);
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.set(obj, A002);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }

        public ImmutableListJsonField(Field field, Method method, Class cls, C28231eT r4) {
            super(field, method);
            this.A01 = cls;
            this.A00 = r4;
        }
    }

    public class BeanJsonField extends FbJsonField {
        public void deserialize(Object obj, C28271eX r5, C26791c3 r6) {
            Type genericType;
            try {
                Method method = this.A01;
                if (method != null) {
                    genericType = method.getGenericParameterTypes()[0];
                } else {
                    genericType = this.A00.getGenericType();
                }
                Object A01 = C14550ta.A01(genericType, r5, r6);
                if (A01 != null) {
                    Method method2 = this.A01;
                    if (method2 != null) {
                        method2.setAccessible(true);
                        this.A01.invoke(obj, A01);
                        return;
                    }
                    this.A00.setAccessible(true);
                    this.A00.set(obj, A01);
                }
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }

        public BeanJsonField(Field field, Method method) {
            super(field, method);
        }
    }

    public final class DoubleJsonField extends FbJsonField {
        public void deserialize(Object obj, C28271eX r6, C26791c3 r7) {
            double d = 0.0d;
            try {
                if (r6.getCurrentToken() == C182811d.VALUE_NULL) {
                    r6.skipChildren();
                } else {
                    d = r6.getValueAsDouble();
                }
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, Double.valueOf(d));
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.setDouble(obj, d);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }

        public DoubleJsonField(Field field, Method method) {
            super(field, method);
        }
    }

    public abstract void deserialize(Object obj, C28271eX r2, C26791c3 r3);

    public final class LongJsonField extends FbJsonField {
        public LongJsonField(Field field, Method method) {
            super(field, method);
        }

        public void deserialize(Object obj, C28271eX r6, C26791c3 r7) {
            try {
                long valueAsLong = r6.getValueAsLong();
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, Long.valueOf(valueAsLong));
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.setLong(obj, valueAsLong);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }
    }

    public final class StringJsonField extends FbJsonField {
        public void deserialize(Object obj, C28271eX r5, C26791c3 r6) {
            try {
                String A02 = C14550ta.A02(r5);
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, A02);
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.set(obj, A02);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }

        public StringJsonField(Field field, Method method) {
            super(field, method);
        }
    }

    public final class BoolJsonField extends FbJsonField {
        public BoolJsonField(Field field, Method method) {
            super(field, method);
        }

        public void deserialize(Object obj, C28271eX r5, C26791c3 r6) {
            try {
                boolean valueAsBoolean = r5.getValueAsBoolean();
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, Boolean.valueOf(valueAsBoolean));
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.setBoolean(obj, valueAsBoolean);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }
    }

    public final class FloatJsonField extends FbJsonField {
        public void deserialize(Object obj, C28271eX r5, C26791c3 r6) {
            float f = 0.0f;
            try {
                if (r5.getCurrentToken() == C182811d.VALUE_NULL) {
                    r5.skipChildren();
                } else {
                    f = r5.getFloatValue();
                }
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, Float.valueOf(f));
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.setFloat(obj, f);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }

        public FloatJsonField(Field field, Method method) {
            super(field, method);
        }
    }

    public final class IntJsonField extends FbJsonField {
        public IntJsonField(Field field, Method method) {
            super(field, method);
        }

        public void deserialize(Object obj, C28271eX r5, C26791c3 r6) {
            try {
                int valueAsInt = r5.getValueAsInt();
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, Integer.valueOf(valueAsInt));
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.setInt(obj, valueAsInt);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }
    }

    public final class ListJsonField extends FbJsonField {
        private C28231eT A00;
        private JsonDeserializer A01;
        private Class A02;

        public ListJsonField(Field field, Method method, Class cls, C28231eT r4) {
            super(field, method);
            this.A02 = cls;
            this.A00 = r4;
        }

        public void deserialize(Object obj, C28271eX r5, C26791c3 r6) {
            Object obj2;
            ArrayListDeserializer arrayListDeserializer;
            try {
                if (r5.getCurrentToken() == C182811d.VALUE_NULL) {
                    obj2 = C04300To.A00();
                } else {
                    if (this.A01 == null) {
                        Class cls = this.A02;
                        if (cls != null) {
                            arrayListDeserializer = new ArrayListDeserializer(cls);
                        } else {
                            C28231eT r0 = this.A00;
                            if (r0 != null) {
                                arrayListDeserializer = new ArrayListDeserializer(((AnonymousClass0jE) r5.getCodec()).findDeserializer(r6, r0._type));
                            } else {
                                throw new IllegalArgumentException(C05360Yq.$const$string(AnonymousClass1Y3.A7Z));
                            }
                        }
                        this.A01 = arrayListDeserializer;
                    }
                    obj2 = (List) this.A01.deserialize(r5, r6);
                }
                Method method = this.A01;
                if (method != null) {
                    method.setAccessible(true);
                    this.A01.invoke(obj, obj2);
                    return;
                }
                this.A00.setAccessible(true);
                this.A00.set(obj, obj2);
            } catch (Exception e) {
                Throwables.propagateIfPossible(e, IOException.class);
                throw Throwables.propagate(e);
            }
        }
    }

    public static FbJsonField jsonFieldWithCreator(Field field) {
        return new BeanJsonField(field, null);
    }

    public FbJsonField(Field field, Method method) {
        this.A00 = field;
        this.A01 = method;
    }

    public static FbJsonField jsonField(Field field) {
        return jsonField(field, (Class) null, (C28231eT) null);
    }

    public static FbJsonField jsonField(Field field, C28231eT r2) {
        return jsonField(field, (Class) null, r2);
    }

    public static FbJsonField jsonField(Field field, Class cls) {
        return jsonField(field, cls, (C28231eT) null);
    }

    private static FbJsonField jsonField(Field field, Class cls, C28231eT r5) {
        Class<?> type = field.getType();
        if (type == String.class) {
            return new StringJsonField(field, null);
        }
        if (type == Integer.TYPE) {
            return new IntJsonField(field, null);
        }
        if (type == Long.TYPE) {
            return new LongJsonField(field, null);
        }
        if (type == Boolean.TYPE) {
            return new BoolJsonField(field, null);
        }
        if (type == Float.TYPE) {
            return new FloatJsonField(field, null);
        }
        if (type == Double.TYPE) {
            return new DoubleJsonField(field, null);
        }
        if (type == ImmutableList.class) {
            return new ImmutableListJsonField(field, null, cls, r5);
        }
        if (type == List.class || type == ArrayList.class) {
            return new ListJsonField(field, null, cls, r5);
        }
        return new BeanJsonField(field, null);
    }

    public static FbJsonField jsonField(Method method) {
        return jsonField(method, (Class) null, (C28231eT) null);
    }

    public static FbJsonField jsonField(Method method, C28231eT r2) {
        return jsonField(method, (Class) null, r2);
    }

    public static FbJsonField jsonField(Method method, Class cls) {
        return jsonField(method, cls, (C28231eT) null);
    }

    private static FbJsonField jsonField(Method method, Class cls, C28231eT r6) {
        Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length == 1) {
            Class<?> cls2 = parameterTypes[0];
            if (cls2 == String.class) {
                return new StringJsonField(null, method);
            }
            if (cls2 == Integer.TYPE) {
                return new IntJsonField(null, method);
            }
            if (cls2 == Long.TYPE) {
                return new LongJsonField(null, method);
            }
            if (cls2 == Boolean.TYPE) {
                return new BoolJsonField(null, method);
            }
            if (cls2 == Float.TYPE) {
                return new FloatJsonField(null, method);
            }
            if (cls2 == Double.TYPE) {
                return new DoubleJsonField(null, method);
            }
            if (cls2 == ImmutableList.class) {
                return new ImmutableListJsonField(null, method, cls, r6);
            }
            if (cls2 == List.class || cls2 == ArrayList.class) {
                return new ListJsonField(null, method, cls, r6);
            }
            return new BeanJsonField(null, method);
        }
        throw new RuntimeException(AnonymousClass08S.A0P("Invalid setter type ", method.getName(), " Only setter with on input parameter is supported."));
    }
}
