package com.facebook.common.json;

import X.AnonymousClass0jE;
import X.AnonymousClass0tY;
import X.C10030jR;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C37561vs;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.lang.reflect.Type;

public class ImmutableListDeserializer extends JsonDeserializer {
    public final Class mContainedClass;
    public JsonDeserializer mValueDeserializer;
    public final C10030jR mValueType;

    public /* bridge */ /* synthetic */ Object deserialize(C28271eX r4, C26791c3 r5) {
        AnonymousClass0jE r2 = (AnonymousClass0jE) r4.getCodec();
        if (!r4.hasCurrentToken() || r4.getCurrentToken() == C182811d.VALUE_NULL) {
            r4.skipChildren();
            return RegularImmutableList.A02;
        } else if (r4.getCurrentToken() == C182811d.START_ARRAY) {
            if (this.mValueDeserializer == null) {
                Type type = this.mContainedClass;
                if (type == null) {
                    type = this.mValueType;
                }
                this.mValueDeserializer = r2.findDeserializer(r5, type);
            }
            ImmutableList.Builder builder = ImmutableList.builder();
            while (AnonymousClass0tY.A00(r4) != C182811d.END_ARRAY) {
                Object deserialize = this.mValueDeserializer.deserialize(r4, r5);
                if (deserialize != null) {
                    builder.add(deserialize);
                }
            }
            return builder.build();
        } else {
            throw new C37561vs("Failed to deserialize to a list - missing start_array token", r4.getCurrentLocation());
        }
    }

    public ImmutableListDeserializer(C10030jR r3) {
        this.mContainedClass = null;
        this.mValueType = r3.containedType(0);
        this.mValueDeserializer = null;
    }

    public ImmutableListDeserializer(JsonDeserializer jsonDeserializer) {
        this.mContainedClass = null;
        this.mValueType = null;
        this.mValueDeserializer = jsonDeserializer;
    }

    public ImmutableListDeserializer(Class cls) {
        this.mContainedClass = cls;
        this.mValueType = null;
        this.mValueDeserializer = null;
    }
}
