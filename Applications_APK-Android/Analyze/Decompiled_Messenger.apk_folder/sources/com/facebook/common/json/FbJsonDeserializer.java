package com.facebook.common.json;

import X.AnonymousClass08S;
import X.AnonymousClass0tY;
import X.C14550ta;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C29611gh;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Throwables;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.lang.reflect.Constructor;

public class FbJsonDeserializer extends JsonDeserializer {
    public Class mClass;

    public FbJsonField getField(String str) {
        return null;
    }

    public boolean isCachable() {
        return true;
    }

    public Object callDefaultConstructor() {
        try {
            Constructor declaredConstructor = this.mClass.getDeclaredConstructor(new Class[0]);
            declaredConstructor.setAccessible(true);
            return declaredConstructor.newInstance(new Object[0]);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(AnonymousClass08S.A0J(this.mClass.getName(), " missing default constructor"), e);
        }
    }

    public static String getJsonParserText(C28271eX r9) {
        Object inputSource = r9.getInputSource();
        StringBuilder sb = new StringBuilder("current token: ");
        sb.append(r9.getText());
        sb.append("\n");
        if (inputSource instanceof InputStream) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            r9.releaseBuffered(byteArrayOutputStream);
            byteArrayOutputStream.flush();
            char[] charArray = byteArrayOutputStream.toString().toCharArray();
            sb.append(charArray, 0, Math.min(charArray.length, 100 - sb.length()));
            byteArrayOutputStream.close();
            InputStream inputStream = (InputStream) inputSource;
            while (true) {
                int read = inputStream.read();
                if (read == -1 || sb.length() >= 100) {
                    break;
                }
                sb.append((char) read);
            }
        } else if (inputSource instanceof Reader) {
            StringWriter stringWriter = new StringWriter();
            r9.releaseBuffered(stringWriter);
            stringWriter.flush();
            char[] charArray2 = stringWriter.toString().toCharArray();
            sb.append(charArray2, 0, Math.min(charArray2.length, 100 - sb.length()));
            stringWriter.close();
            Reader reader = (Reader) inputSource;
            while (true) {
                int read2 = reader.read();
                if (read2 == -1 || sb.length() >= 100) {
                    reader.close();
                } else {
                    sb.append((char) read2);
                }
            }
            reader.close();
        }
        if (sb.length() == 100) {
            sb.append("...");
        }
        return sb.toString();
    }

    public Object deserialize(C28271eX r4, C26791c3 r5) {
        try {
            Object callDefaultConstructor = callDefaultConstructor();
            while (AnonymousClass0tY.A00(r4) != C182811d.END_OBJECT) {
                if (r4.getCurrentToken() == C182811d.FIELD_NAME) {
                    String currentName = r4.getCurrentName();
                    r4.nextToken();
                    FbJsonField field = getField(currentName);
                    if (field != null) {
                        field.deserialize(callDefaultConstructor, r4, r5);
                    } else {
                        r4.skipChildren();
                    }
                }
            }
            if (callDefaultConstructor instanceof C29611gh) {
                ((C29611gh) callDefaultConstructor).BxV();
            }
            return callDefaultConstructor;
        } catch (Exception e) {
            Throwables.propagateIfPossible(e, IOException.class);
            C14550ta.A0D(this.mClass, r4, e);
            throw new RuntimeException("not reached");
        }
    }

    public final void init(Class cls) {
        this.mClass = cls;
    }
}
