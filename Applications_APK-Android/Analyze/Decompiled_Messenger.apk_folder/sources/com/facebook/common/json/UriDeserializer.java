package com.facebook.common.json;

import X.C26791c3;
import android.net.Uri;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;

public class UriDeserializer extends FromStringDeserializer {
    public UriDeserializer() {
        super(Uri.class);
    }

    public /* bridge */ /* synthetic */ Object _deserialize(String str, C26791c3 r3) {
        return Uri.parse(str);
    }
}
