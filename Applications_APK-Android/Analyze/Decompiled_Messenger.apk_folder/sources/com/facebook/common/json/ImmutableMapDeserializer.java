package com.facebook.common.json;

import X.AnonymousClass08S;
import X.AnonymousClass0jE;
import X.AnonymousClass0tY;
import X.C10030jR;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C37561vs;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableMap;

public class ImmutableMapDeserializer extends JsonDeserializer {
    public JsonDeserializer mKeyDeserializer;
    public boolean mKeyDeserializerResolved = false;
    public final Class mKeyType;
    public JsonDeserializer mValueDeserializer;
    public final C10030jR mValueType;

    public ImmutableMapDeserializer(C10030jR r5) {
        boolean z = false;
        Class<String> cls = r5.containedType(0)._class;
        this.mKeyType = cls;
        Preconditions.checkArgument((cls == String.class || Enum.class.isAssignableFrom(cls)) ? true : z, "Map keys must be a String or an enum.");
        this.mValueType = r5.containedType(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jE.findDeserializer(X.1c3, java.lang.Class):com.fasterxml.jackson.databind.JsonDeserializer
     arg types: [X.1c3, java.lang.Class<java.lang.String>]
     candidates:
      X.0jE.findDeserializer(X.1c3, X.0jR):com.fasterxml.jackson.databind.JsonDeserializer
      X.0jE.findDeserializer(X.1c3, java.lang.reflect.Type):com.fasterxml.jackson.databind.JsonDeserializer
      X.0jE.findDeserializer(X.1c3, java.lang.Class):com.fasterxml.jackson.databind.JsonDeserializer */
    public /* bridge */ /* synthetic */ Object deserialize(C28271eX r8, C26791c3 r9) {
        AnonymousClass0jE r6 = (AnonymousClass0jE) r8.getCodec();
        if (!r8.hasCurrentToken() || r8.getCurrentToken() == C182811d.VALUE_NULL) {
            r8.skipChildren();
            return RegularImmutableMap.A03;
        } else if (r8.getCurrentToken() == C182811d.START_OBJECT) {
            if (!this.mKeyDeserializerResolved) {
                Class<String> cls = this.mKeyType;
                if (cls != String.class) {
                    this.mKeyDeserializer = r6.findDeserializer(r9, (Class) cls);
                }
                this.mKeyDeserializerResolved = true;
            }
            if (this.mValueDeserializer == null) {
                this.mValueDeserializer = r6.findDeserializer(r9, this.mValueType);
            }
            ImmutableMap.Builder builder = ImmutableMap.builder();
            while (AnonymousClass0tY.A00(r8) != C182811d.END_OBJECT) {
                if (r8.getCurrentToken() == C182811d.FIELD_NAME) {
                    String currentName = r8.getCurrentName();
                    r8.nextToken();
                    Object deserialize = this.mValueDeserializer.deserialize(r8, r9);
                    if (deserialize != null) {
                        if (this.mKeyDeserializer != null) {
                            C28271eX createParser = r6.getFactory().createParser(AnonymousClass08S.A0P("\"", currentName, "\""));
                            createParser.nextToken();
                            builder.put(this.mKeyDeserializer.deserialize(createParser, r9), deserialize);
                        } else {
                            builder.put(currentName, deserialize);
                        }
                    }
                }
            }
            return builder.build();
        } else {
            throw new C37561vs("Failed to deserialize to a map - missing start_object token", r8.getCurrentLocation());
        }
    }
}
