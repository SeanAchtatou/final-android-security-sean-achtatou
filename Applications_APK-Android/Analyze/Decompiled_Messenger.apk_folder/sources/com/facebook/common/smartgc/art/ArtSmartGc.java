package com.facebook.common.smartgc.art;

import X.AnonymousClass01q;
import X.AnonymousClass084;
import X.AnonymousClass0A0;
import X.AnonymousClass0N9;
import X.C013809z;
import X.C70903bT;
import android.util.Log;
import java.lang.reflect.Method;

public class ArtSmartGc implements AnonymousClass0A0 {
    public static final boolean CAN_RUN_ON_THIS_PLATFORM;
    private static final Object LOCK = new Object();
    public static Method sConcurrentGc;
    private static String sDataDir;
    public static Boolean sHadErrorInitalizing;
    public static boolean sReflectionInited;
    private static Method sRequestConcurrentGc;
    private static boolean sSetUpHookInited;
    private static AnonymousClass0N9 sSetupSmartGcConfig;
    public static Object sVmRuntimeInstance;

    private static native void nativeBadTimeToDoGc(boolean z, boolean z2, boolean z3);

    private static native String nativeGetErrorMessage();

    private static native boolean nativeInitialize(String str, int i, int i2, int i3, boolean z, boolean z2);

    private static native void nativeNotAsBadTimeToDoGc();

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (android.os.Build.VERSION.SDK_INT > 26) goto L_0x000d;
     */
    static {
        /*
            boolean r0 = X.C011108x.A00
            r5 = 1
            r4 = 0
            if (r0 == 0) goto L_0x000d
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            r1 = 1
            if (r2 <= r0) goto L_0x000e
        L_0x000d:
            r1 = 0
        L_0x000e:
            com.facebook.common.smartgc.art.ArtSmartGc.CAN_RUN_ON_THIS_PLATFORM = r1
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            com.facebook.common.smartgc.art.ArtSmartGc.LOCK = r0
            if (r1 == 0) goto L_0x0052
            java.lang.String r0 = "dalvik.system.VMRuntime"
            java.lang.Class r3 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = "getRuntime"
            java.lang.Class[] r0 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0050 }
            java.lang.reflect.Method r2 = r3.getDeclaredMethod(r1, r0)     // Catch:{ Exception -> 0x0050 }
            r2.setAccessible(r5)     // Catch:{ Exception -> 0x0050 }
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0050 }
            java.lang.Object r0 = r2.invoke(r1, r0)     // Catch:{ Exception -> 0x0050 }
            com.facebook.common.smartgc.art.ArtSmartGc.sVmRuntimeInstance = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = "concurrentGC"
            java.lang.Class[] r0 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0050 }
            java.lang.reflect.Method r0 = r3.getDeclaredMethod(r1, r0)     // Catch:{ Exception -> 0x0050 }
            com.facebook.common.smartgc.art.ArtSmartGc.sConcurrentGc = r0     // Catch:{ Exception -> 0x0050 }
            r0.setAccessible(r5)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = "requestConcurrentGC"
            java.lang.Class[] r0 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0050 }
            java.lang.reflect.Method r0 = r3.getDeclaredMethod(r1, r0)     // Catch:{ Exception -> 0x0050 }
            com.facebook.common.smartgc.art.ArtSmartGc.sRequestConcurrentGc = r0     // Catch:{ Exception -> 0x0050 }
            r0.setAccessible(r5)     // Catch:{ Exception -> 0x0050 }
            com.facebook.common.smartgc.art.ArtSmartGc.sReflectionInited = r5     // Catch:{ Exception -> 0x0050 }
            return
        L_0x0050:
            com.facebook.common.smartgc.art.ArtSmartGc.sReflectionInited = r4
        L_0x0052:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.smartgc.art.ArtSmartGc.<clinit>():void");
    }

    private static void init() {
        if (sHadErrorInitalizing == null) {
            synchronized (LOCK) {
                if (sHadErrorInitalizing == null) {
                    boolean z = false;
                    if (!CAN_RUN_ON_THIS_PLATFORM) {
                        sHadErrorInitalizing = false;
                    } else if (sDataDir != null) {
                        try {
                            AnonymousClass01q.A08("artsmartgc");
                            int[] A01 = C70903bT.A01("(HeapTaskDaemon)", "(GCDaemon)", "(HeapTrimmerDaem)");
                            int i = A01[0];
                            int i2 = A01[1];
                            int i3 = A01[2];
                            String str = sDataDir;
                            AnonymousClass0N9 r0 = sSetupSmartGcConfig;
                            if (!nativeInitialize(str, i, i2, i3, r0.A02, r0.A01)) {
                                z = true;
                            }
                            sHadErrorInitalizing = Boolean.valueOf(z);
                        } catch (UnsatisfiedLinkError e) {
                            Log.e("ArtSmartGc", "Cannot Init ART Smart GC", e);
                            sHadErrorInitalizing = true;
                        }
                    } else {
                        throw new IllegalStateException("setupHook must be called first");
                    }
                }
            }
        }
    }

    public /* bridge */ /* synthetic */ void badTimeToDoGc(AnonymousClass084 r4) {
        C013809z r42 = (C013809z) r4;
        if (CAN_RUN_ON_THIS_PLATFORM) {
            init();
            if (!sHadErrorInitalizing.booleanValue()) {
                nativeBadTimeToDoGc(r42.A00, r42.A02, r42.A01);
                return;
            }
            return;
        }
        throw new IllegalStateException("This platform is not supported");
    }

    public String getErrorMessage() {
        Boolean bool;
        if (!CAN_RUN_ON_THIS_PLATFORM || (bool = sHadErrorInitalizing) == null || bool == Boolean.FALSE) {
            return null;
        }
        return nativeGetErrorMessage();
    }

    public void manualGcConcurrent() {
        if (sReflectionInited) {
            Method method = sConcurrentGc;
            try {
                method.invoke(sVmRuntimeInstance, new Object[0]);
            } catch (Exception unused) {
                method.toString();
            }
        }
    }

    public void notAsBadTimeToDoGc() {
        if (CAN_RUN_ON_THIS_PLATFORM) {
            init();
            if (!sHadErrorInitalizing.booleanValue()) {
                nativeNotAsBadTimeToDoGc();
                return;
            }
            return;
        }
        throw new IllegalStateException("This platform is not supported");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        if (r1 != false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setUpHook(android.content.Context r3, X.AnonymousClass0N9 r4) {
        /*
            r2 = this;
            boolean r0 = com.facebook.common.smartgc.art.ArtSmartGc.sSetUpHookInited
            if (r0 == 0) goto L_0x0015
            if (r4 == 0) goto L_0x0014
            X.0N9 r0 = com.facebook.common.smartgc.art.ArtSmartGc.sSetupSmartGcConfig
            if (r0 == 0) goto L_0x000f
            boolean r1 = r0.A00
            r0 = 0
            if (r1 == 0) goto L_0x0010
        L_0x000f:
            r0 = 1
        L_0x0010:
            if (r0 == 0) goto L_0x0014
            com.facebook.common.smartgc.art.ArtSmartGc.sSetupSmartGcConfig = r4
        L_0x0014:
            return
        L_0x0015:
            r1 = 0
            java.lang.String r0 = "artsmartgc"
            java.io.File r0 = r3.getDir(r0, r1)
            java.lang.String r0 = r0.getAbsolutePath()
            com.facebook.common.smartgc.art.ArtSmartGc.sDataDir = r0
            if (r4 != 0) goto L_0x0029
            X.0N9 r4 = new X.0N9
            r4.<init>()
        L_0x0029:
            com.facebook.common.smartgc.art.ArtSmartGc.sSetupSmartGcConfig = r4
            r0 = 1
            com.facebook.common.smartgc.art.ArtSmartGc.sSetUpHookInited = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.smartgc.art.ArtSmartGc.setUpHook(android.content.Context, X.0N9):void");
    }

    public boolean isPlatformSupported() {
        return CAN_RUN_ON_THIS_PLATFORM;
    }

    public void manualGcComplete() {
    }

    public void manualGcForAlloc() {
    }
}
