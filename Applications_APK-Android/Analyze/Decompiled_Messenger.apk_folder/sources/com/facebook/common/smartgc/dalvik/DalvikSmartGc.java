package com.facebook.common.smartgc.dalvik;

import X.AnonymousClass01q;
import X.AnonymousClass084;
import X.AnonymousClass0A0;
import X.AnonymousClass0N9;
import X.C011108x;
import X.C013709y;
import android.content.Context;

public class DalvikSmartGc implements AnonymousClass0A0 {
    public static final boolean CAN_RUN_ON_THIS_PLATFORM;
    private static final boolean HAD_ERROR_INITING = (!nativeInitialize());

    private static native void nativeBadTimeToDoGc(boolean z, int i, boolean z2, boolean z3, boolean z4);

    private static native void nativeConcurrentGc(boolean z, int i);

    private static native String nativeGetErrorMessage();

    private static native boolean nativeInitialize();

    private static native void nativeManualGcComplete();

    private static native void nativeManualGcConcurrent();

    private static native void nativeManualGcForAlloc();

    private static native void nativeNotAsBadTimeToDoGc();

    public void setUpHook(Context context, AnonymousClass0N9 r2) {
    }

    static {
        boolean z = !C011108x.A00;
        CAN_RUN_ON_THIS_PLATFORM = z;
        if (z) {
            AnonymousClass01q.A08("dalviksmartgc");
        }
    }

    private static void validateIsPlatformSupported() {
        if (!CAN_RUN_ON_THIS_PLATFORM) {
            throw new IllegalStateException("This platform is not supported");
        }
    }

    public /* bridge */ /* synthetic */ void badTimeToDoGc(AnonymousClass084 r4) {
        C013709y r42 = (C013709y) r4;
        validateIsPlatformSupported();
        nativeBadTimeToDoGc(false, 0, r42.A00, r42.A01, false);
    }

    public String getErrorMessage() {
        if (!CAN_RUN_ON_THIS_PLATFORM || !HAD_ERROR_INITING) {
            return null;
        }
        return nativeGetErrorMessage();
    }

    public boolean isPlatformSupported() {
        return CAN_RUN_ON_THIS_PLATFORM;
    }

    public void manualGcComplete() {
        validateIsPlatformSupported();
        nativeManualGcComplete();
    }

    public void manualGcConcurrent() {
        validateIsPlatformSupported();
        nativeManualGcConcurrent();
    }

    public void manualGcForAlloc() {
        validateIsPlatformSupported();
        nativeManualGcForAlloc();
    }

    public void notAsBadTimeToDoGc() {
        validateIsPlatformSupported();
        nativeNotAsBadTimeToDoGc();
    }
}
