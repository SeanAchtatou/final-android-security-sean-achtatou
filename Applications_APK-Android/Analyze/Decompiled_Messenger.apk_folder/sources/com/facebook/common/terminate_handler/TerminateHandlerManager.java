package com.facebook.common.terminate_handler;

import X.AnonymousClass01q;

public class TerminateHandlerManager {
    public static native void nativeInstallTerminateHandler();

    static {
        AnonymousClass01q.A08("terminate_handler_manager");
    }
}
