package com.facebook.common.systempropertiesinterceptor;

import X.AnonymousClass01q;
import X.C011108x;
import android.os.Build;

public class SystemPropertiesInterceptorNative {
    private static final boolean IS_SUPPORTED;

    private static native void nativeClearForTest();

    private static native String nativeGetPropertyForTest(String str, String str2);

    private static native void nativeModifyKeyDoubleValue(String str, double d, double d2, double d3, double d4, boolean z, double d5);

    private static native void nativeModifyKeyIntValue(String str, int i, int i2, int i3, double d, boolean z, int i4);

    private static native void nativeOverwriteKeyStringValue(String str, String str2);

    private static native void nativeSetOrigPropertyForTest(String str, String str2);

    private static native boolean nativeStartInterception();

    static {
        boolean z = false;
        if (C011108x.A00 && Build.VERSION.SDK_INT < 26) {
            try {
                AnonymousClass01q.A08("syspropsinterceptor-jni");
                z = true;
            } catch (RuntimeException | UnsatisfiedLinkError unused) {
            }
        }
        IS_SUPPORTED = z;
    }

    private SystemPropertiesInterceptorNative() {
    }
}
