package com.facebook.common.memory.leaklistener;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass164;
import X.AnonymousClass165;
import X.AnonymousClass166;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass2SE;
import X.C138056cU;
import X.C28601f4;
import X.C30559Eym;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.facebook.common.errorreporting.memory.LeakMemoryDumper;
import java.util.Collection;
import java.util.concurrent.ScheduledExecutorService;
import javax.inject.Singleton;

@Singleton
public final class MemoryLeakListener extends AnonymousClass164 implements AnonymousClass165, C28601f4 {
    private static volatile MemoryLeakListener A02;
    public AnonymousClass2SE A00;
    private AnonymousClass0UN A01;

    public void BNK(Fragment fragment, int i, int i2, Intent intent) {
    }

    public void BNh(Fragment fragment, Bundle bundle) {
    }

    public void BNp(Fragment fragment, boolean z) {
    }

    public void BNx(Fragment fragment, View view, Bundle bundle) {
    }

    public void BNy(Fragment fragment) {
    }

    public void BOY(Fragment fragment, Fragment fragment2) {
    }

    public void BP9(C138056cU r1) {
    }

    public void BPa(Fragment fragment, Bundle bundle) {
    }

    public void BPf(Bundle bundle) {
    }

    public void BTL(Fragment fragment, Configuration configuration) {
    }

    public void Bi0(Fragment fragment) {
    }

    public void BmO(Fragment fragment) {
    }

    public void Bmy(Fragment fragment, Bundle bundle) {
    }

    public void BoL(Fragment fragment, boolean z) {
    }

    public void BpQ(Fragment fragment) {
    }

    public void Bq5(Fragment fragment) {
    }

    public static final MemoryLeakListener A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (MemoryLeakListener.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new MemoryLeakListener(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static boolean A01(MemoryLeakListener memoryLeakListener) {
        AnonymousClass166 r3 = (AnonymousClass166) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BOT, memoryLeakListener.A01);
        if (r3.A00 == null) {
            r3.A00 = Boolean.valueOf(r3.A02.A00.AbO(137, false));
        }
        if (r3.A00.booleanValue() || r3.A01()) {
            return true;
        }
        return false;
    }

    public void BVy(Fragment fragment) {
        this.A00.A05(fragment);
    }

    private MemoryLeakListener(AnonymousClass1XY r5) {
        this.A01 = new AnonymousClass0UN(3, r5);
        if (A01(this)) {
            this.A00 = new AnonymousClass2SE((ScheduledExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ae9, this.A01), this);
        }
    }

    public void BcU(Collection collection) {
        if (!collection.isEmpty()) {
            for (C30559Eym eym : this.A00) {
                StringBuilder sb = new StringBuilder();
                for (Object next : collection) {
                    sb.append(next.toString());
                    sb.append(",");
                    sb.append(next.getClass().getName());
                    sb.append("\n");
                }
                eym.handleLeak(this, sb.toString());
            }
            if (((AnonymousClass166) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BOT, this.A01)).A01()) {
                LeakMemoryDumper leakMemoryDumper = (LeakMemoryDumper) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ApW, this.A01);
                if (!leakMemoryDumper.A02.get()) {
                    leakMemoryDumper.A01 = true;
                }
            }
            ((LeakMemoryDumper) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ApW, this.A01)).A01();
        }
    }
}
