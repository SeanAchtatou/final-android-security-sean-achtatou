package com.facebook.common.memory.manager;

import X.AnonymousClass01P;
import X.AnonymousClass07A;
import X.AnonymousClass0UN;
import X.AnonymousClass0Ud;
import X.AnonymousClass0WD;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YQ;
import X.C000700l;
import X.C001901i;
import X.C010708t;
import X.C07940eQ;
import X.C133746Nn;
import X.C14320t5;
import X.C16490xB;
import X.C188388oq;
import X.C25051Yd;
import X.C52032iJ;
import android.util.SparseArray;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.device.resourcemonitor.ResourceManager;
import com.google.common.base.Preconditions;
import com.google.common.collect.MapMakerInternalMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
public final class MemoryManager implements C14320t5, AnonymousClass1YQ {
    public static final Class A08 = MemoryManager.class;
    private static volatile MemoryManager A09;
    public boolean A00 = false;
    public AnonymousClass0UN A01;
    private long A02 = 0;
    private SparseArray A03 = new SparseArray();
    public final Map A04;
    public final Random A05;
    private final Set A06 = new HashSet();
    private final AtomicBoolean A07 = new AtomicBoolean(false);

    public static synchronized List A01(MemoryManager memoryManager) {
        List unmodifiableList;
        synchronized (memoryManager) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(memoryManager.A06));
        }
        return unmodifiableList;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(15:1|2|(1:4)|5|(2:7|(2:9|10))(1:11)|12|13|(2:16|14)|37|17|(1:19)|(2:21|22)|23|24|25) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:27|(2:29|30)|31|32|33) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x002e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0065 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0072 */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003e A[Catch:{ all -> 0x006c }, LOOP:0: B:14:0x0038->B:16:0x003e, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005d A[Catch:{ all -> 0x006c }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0062 A[SYNTHETIC, Splitter:B:21:0x0062] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0072=Splitter:B:31:0x0072, B:23:0x0065=Splitter:B:23:0x0065} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A05(X.C133746Nn r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            X.6Nn r0 = X.C133746Nn.OnSystemMemoryCriticallyLowWhileAppInForeground     // Catch:{ all -> 0x0078 }
            r3 = 0
            r6 = 0
            if (r8 != r0) goto L_0x0008
            r6 = 1
        L_0x0008:
            r2 = 5
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x0078 }
            X.0UN r0 = r7.A01     // Catch:{ all -> 0x0078 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0078 }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ all -> 0x0078 }
            r0 = 567060237387586(0x203bd00040742, double:2.801649824157883E-309)
            long r4 = r2.At0(r0)     // Catch:{ all -> 0x0078 }
            int r1 = (int) r4     // Catch:{ all -> 0x0078 }
            if (r6 == 0) goto L_0x002d
            int r0 = android.os.Process.myTid()     // Catch:{ all -> 0x0078 }
            int r4 = android.os.Process.getThreadPriority(r0)     // Catch:{ all -> 0x0078 }
            if (r4 <= r1) goto L_0x002e
            android.os.Process.setThreadPriority(r1)     // Catch:{ SecurityException -> 0x002e }
            goto L_0x002e
        L_0x002d:
            r4 = 0
        L_0x002e:
            java.util.Map r0 = r7.A04     // Catch:{ all -> 0x006c }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x006c }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x006c }
        L_0x0038:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x006c }
            if (r0 == 0) goto L_0x0048
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x006c }
            X.0xB r0 = (X.C16490xB) r0     // Catch:{ all -> 0x006c }
            r0.trim(r8)     // Catch:{ all -> 0x006c }
            goto L_0x0038
        L_0x0048:
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x006c }
            X.0UN r0 = r7.A01     // Catch:{ all -> 0x006c }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x006c }
            X.1YI r1 = (X.AnonymousClass1YI) r1     // Catch:{ all -> 0x006c }
            r0 = 590(0x24e, float:8.27E-43)
            com.facebook.common.util.TriState r1 = r1.Ab9(r0)     // Catch:{ all -> 0x006c }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x006c }
            if (r1 != r0) goto L_0x0060
            X.AnonymousClass1YH.A03()     // Catch:{ all -> 0x006c }
        L_0x0060:
            if (r6 == 0) goto L_0x0065
            android.os.Process.setThreadPriority(r4)     // Catch:{ Exception -> 0x0065 }
        L_0x0065:
            java.util.concurrent.atomic.AtomicBoolean r0 = r7.A07     // Catch:{ all -> 0x0078 }
            r0.set(r3)     // Catch:{ all -> 0x0078 }
            monitor-exit(r7)
            return
        L_0x006c:
            r1 = move-exception
            if (r6 == 0) goto L_0x0072
            android.os.Process.setThreadPriority(r4)     // Catch:{ Exception -> 0x0072 }
        L_0x0072:
            java.util.concurrent.atomic.AtomicBoolean r0 = r7.A07     // Catch:{ all -> 0x0078 }
            r0.set(r3)     // Catch:{ all -> 0x0078 }
            throw r1     // Catch:{ all -> 0x0078 }
        L_0x0078:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.memory.manager.MemoryManager.A05(X.6Nn):void");
    }

    public synchronized void C0d(C16490xB r3) {
        Preconditions.checkNotNull(r3, "MemoryTrimmable cannot be null.");
        this.A04.put(r3, Boolean.TRUE);
    }

    public String getSimpleName() {
        return "MemoryManager";
    }

    public static final MemoryManager A00(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (MemoryManager.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new MemoryManager(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static void A02(MemoryManager memoryManager) {
        ((ResourceManager) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AQe, memoryManager.A01)).A0A.put(new C52032iJ(memoryManager), 1);
    }

    public static void A03(MemoryManager memoryManager, boolean z, int i) {
        C133746Nn r3;
        synchronized (AnonymousClass01P.A0Z) {
            try {
                if (AnonymousClass01P.A0Y == null) {
                    C010708t.A0J(TurboLoader.Locator.$const$string(47), TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0s));
                } else {
                    C001901i r1 = AnonymousClass01P.A0Y.A0B;
                    synchronized (r1) {
                        try {
                            C001901i.A03(r1);
                        } catch (Throwable th) {
                            while (true) {
                                th = th;
                                break;
                            }
                        }
                    }
                    C001901i.A02(r1);
                }
            } catch (Throwable th2) {
                while (true) {
                    th = th2;
                }
            }
        }
        if (((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, memoryManager.A01)).Aem(285585260746492L) && i == 15) {
            r3 = C133746Nn.OnSystemMemoryCriticallyLowWhileAppInForeground;
        } else if (i == 20) {
            r3 = C133746Nn.OnAppBackgrounded;
        } else if (((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, memoryManager.A01)).A0G()) {
            r3 = C133746Nn.OnSystemLowMemoryWhileAppInBackground;
        } else {
            r3 = C133746Nn.OnSystemLowMemoryWhileAppInForeground;
        }
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BKH, memoryManager.A01), new C188388oq(memoryManager, r3, z, i), 45154314);
        return;
        throw th;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(com.facebook.common.memory.manager.MemoryManager r10, int r11) {
        /*
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r10.A01
            r0 = 5
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 285585260812029(0x103bd000616fd, double:1.41097866325836E-309)
            boolean r0 = r2.Aem(r0)
            r4 = 1
            if (r0 == 0) goto L_0x0018
            return r4
        L_0x0018:
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r10.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 592(0x250, float:8.3E-43)
            r5 = 0
            boolean r0 = r1.AbO(r0, r5)
            if (r0 != 0) goto L_0x0144
            r0 = 5
            if (r11 == r0) goto L_0x0141
            r0 = 10
            if (r11 == r0) goto L_0x012b
            r0 = 15
            if (r11 == r0) goto L_0x0128
            r0 = 20
            if (r11 == r0) goto L_0x0128
            r0 = 40
            if (r11 == r0) goto L_0x0112
            r0 = 60
            if (r11 == r0) goto L_0x0141
            r0 = 80
            if (r11 == r0) goto L_0x0128
            java.lang.Class r2 = com.facebook.common.memory.manager.MemoryManager.A08
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Ignoring unknown trim level: %d"
            X.C010708t.A0B(r2, r0, r1)
            r0 = 0
        L_0x0057:
            if (r0 == 0) goto L_0x0144
            int r1 = X.AnonymousClass1Y3.B5j
            X.0UN r0 = r10.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0Ud r0 = (X.AnonymousClass0Ud) r0
            boolean r0 = r0.A0G()
            if (r0 == 0) goto L_0x010d
            r8 = 5000(0x1388, double:2.4703E-320)
        L_0x006b:
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r10.A01
            r0 = 5
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 285585260877566(0x103bd000716fe, double:1.410978663582155E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x00a5
            int r1 = X.AnonymousClass1Y3.B5j
            X.0UN r0 = r10.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0Ud r0 = (X.AnonymousClass0Ud) r0
            boolean r0 = r0.A0G()
            r2 = 5
            if (r0 == 0) goto L_0x00f9
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r10.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 567060237715268(0x203bd00090744, double:2.801649825776847E-309)
            long r8 = r2.At0(r0)
        L_0x00a5:
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r10.A01
            r0 = 5
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 285585260615419(0x103bd000316fb, double:1.410978662286977E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x00f6
            android.util.SparseArray r2 = r10.A03
            r0 = 0
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            java.lang.Object r0 = r2.get(r11, r0)
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
        L_0x00cd:
            r6 = 2
            int r3 = X.AnonymousClass1Y3.AgK
            X.0UN r2 = r10.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.06B r2 = (X.AnonymousClass06B) r2
            long r2 = r2.now()
            long r6 = r2 - r0
            int r0 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r0 < 0) goto L_0x0144
            java.util.concurrent.atomic.AtomicBoolean r0 = r10.A07
            boolean r0 = r0.compareAndSet(r5, r4)
            if (r0 == 0) goto L_0x0144
            r10.A02 = r2
            android.util.SparseArray r1 = r10.A03
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            r1.put(r11, r0)
            return r4
        L_0x00f6:
            long r0 = r10.A02
            goto L_0x00cd
        L_0x00f9:
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r10.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 567060237649731(0x203bd00080743, double:2.80164982545305E-309)
            long r8 = r2.At0(r0)
            goto L_0x00a5
        L_0x010d:
            r8 = 60000(0xea60, double:2.9644E-319)
            goto L_0x006b
        L_0x0112:
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r10.A01
            r0 = 5
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 285585260418808(0x103bd000016f8, double:1.41097866131559E-309)
            boolean r0 = r2.Aem(r0)
            goto L_0x0057
        L_0x0128:
            r0 = 1
            goto L_0x0057
        L_0x012b:
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r10.A01
            r0 = 5
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 285585260549882(0x103bd000216fa, double:1.41097866196318E-309)
            boolean r0 = r2.Aem(r0)
            goto L_0x0057
        L_0x0141:
            r0 = 0
            goto L_0x0057
        L_0x0144:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.memory.manager.MemoryManager.A04(com.facebook.common.memory.manager.MemoryManager, int):boolean");
    }

    private MemoryManager(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(9, r3);
        C07940eQ r1 = new C07940eQ();
        r1.A06(MapMakerInternalMap.Strength.WEAK);
        this.A04 = r1.A02();
        this.A05 = new Random();
    }

    public void init() {
        int A032 = C000700l.A03(-791265931);
        if (((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, this.A01)).Aem(285224484935202L)) {
            C000700l.A09(-1078927986, A032);
            return;
        }
        if (!this.A00) {
            A02(this);
            this.A00 = true;
        }
        C000700l.A09(-221151454, A032);
    }
}
