package com.facebook.common.perftest;

import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import com.facebook.common.perftest.base.PerfTestConfigBase;
import javax.inject.Singleton;

@Singleton
public class PerfTestConfig extends PerfTestConfigBase {
    private static volatile PerfTestConfig A00;

    public static final PerfTestConfig A01(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (PerfTestConfig.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new PerfTestConfig();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
