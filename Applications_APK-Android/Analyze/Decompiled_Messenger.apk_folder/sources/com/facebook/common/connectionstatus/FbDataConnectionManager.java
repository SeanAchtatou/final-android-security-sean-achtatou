package com.facebook.common.connectionstatus;

import X.AnonymousClass06U;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass1IG;
import X.AnonymousClass1Vv;
import X.AnonymousClass1Vx;
import X.AnonymousClass1Vy;
import X.AnonymousClass1W1;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1Y6;
import X.AnonymousClass1Y7;
import X.AnonymousClass1YQ;
import X.C000700l;
import X.C04460Ut;
import X.C06600bl;
import X.C09340h3;
import X.C30181hd;
import X.C30191he;
import X.C31121jB;
import X.C37511vn;
import X.C48552ac;
import android.net.NetworkInfo;
import android.text.TextUtils;
import com.facebook.prefs.shared.FbSharedPreferences;
import io.card.payment.BuildConfig;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
public final class FbDataConnectionManager implements AnonymousClass1YQ, AnonymousClass1Vv, C30181hd {
    private static volatile FbDataConnectionManager A07;
    public AnonymousClass0UN A00;
    public final AnonymousClass1Vx A01 = new AnonymousClass1Vx(this);
    public final AnonymousClass06U A02;
    public final AtomicReference A03;
    public final AtomicReference A04;
    public volatile NetworkInfo A05;
    private volatile boolean A06;

    public String getSimpleName() {
        return "FbDataConnectionManager";
    }

    public static final FbDataConnectionManager A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (FbDataConnectionManager.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new FbDataConnectionManager(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0095, code lost:
        if (r8.compareTo((java.lang.Enum) r2) <= 0) goto L_0x0097;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(com.facebook.common.connectionstatus.FbDataConnectionManager r11) {
        /*
            int r1 = X.AnonymousClass1Y3.AKb
            X.0UN r0 = r11.A00
            r4 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0Ut r0 = (X.C04460Ut) r0
            if (r0 == 0) goto L_0x004a
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            java.lang.String r0 = "com.facebook.common.connectionstatus.FbDataConnectionManager.DATA_CONNECTION_STATE_CHANGE"
            android.content.Intent r2 = r3.setAction(r0)
            java.util.concurrent.atomic.AtomicReference r0 = r11.A03
            java.lang.Object r1 = r0.get()
            java.io.Serializable r1 = (java.io.Serializable) r1
            java.lang.String r0 = "com.facebook.common.connectionstatus.FbDataConnectionManager.BANDWIDTH_STATE"
            android.content.Intent r2 = r2.putExtra(r0, r1)
            java.util.concurrent.atomic.AtomicReference r0 = r11.A04
            java.lang.Object r1 = r0.get()
            java.io.Serializable r1 = (java.io.Serializable) r1
            java.lang.String r0 = "com.facebook.common.connectionstatus.FbDataConnectionManager.LATENCY_STATE"
            android.content.Intent r2 = r2.putExtra(r0, r1)
            boolean r1 = A02(r11)
            java.lang.String r0 = "com.facebook.common.connectionstatus.FbDataConnectionManager.CONNECTION_STATE"
            r2.putExtra(r0, r1)
            int r1 = X.AnonymousClass1Y3.AKb
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0Ut r0 = (X.C04460Ut) r0
            r0.C4x(r3)
        L_0x004a:
            r2 = 5
            int r1 = X.AnonymousClass1Y3.AyU
            X.0UN r0 = r11.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1IG r4 = (X.AnonymousClass1IG) r4
            java.lang.String r1 = r11.A09()
            java.util.concurrent.atomic.AtomicReference r0 = r11.A03
            java.lang.Object r3 = r0.get()
            X.1Vy r3 = (X.AnonymousClass1Vy) r3
            java.util.concurrent.atomic.AtomicReference r0 = r11.A04
            java.lang.Object r8 = r0.get()
            X.1Vy r8 = (X.AnonymousClass1Vy) r8
            boolean r7 = A02(r11)
            r5 = 9
            int r2 = X.AnonymousClass1Y3.B5j
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r2, r0)
            X.0Ud r0 = (X.AnonymousClass0Ud) r0
            boolean r0 = r0.A0G()
            r6 = r0 ^ 1
            r3.toString()
            r8.toString()
            r5 = 1
            if (r7 == 0) goto L_0x0097
            X.1Vy r2 = X.AnonymousClass1Vy.A01
            int r0 = r3.compareTo(r2)
            if (r0 <= 0) goto L_0x0097
            int r2 = r8.compareTo(r2)
            r0 = 1
            if (r2 > 0) goto L_0x0098
        L_0x0097:
            r0 = 0
        L_0x0098:
            X.1jC r2 = r4.A01
            boolean r0 = r2.A01(r0, r6)
            if (r0 == 0) goto L_0x00a9
            long[] r0 = r2.A02()
            if (r0 == 0) goto L_0x00a9
            X.AnonymousClass1IG.A02(r4, r0)
        L_0x00a9:
            java.util.concurrent.atomic.AtomicBoolean r0 = r4.A03
            boolean r0 = r0.getAndSet(r5)
            if (r0 != 0) goto L_0x00cb
            int r5 = X.AnonymousClass1Y3.Azd
            X.0UN r2 = r4.A00
            r0 = 1
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r5, r2)
            java.util.concurrent.ScheduledExecutorService r5 = (java.util.concurrent.ScheduledExecutorService) r5
            X.3hV r6 = new X.3hV
            r6.<init>(r4)
            java.util.concurrent.TimeUnit r11 = java.util.concurrent.TimeUnit.MILLISECONDS
            r7 = 1000(0x3e8, double:4.94E-321)
            r9 = 3600000(0x36ee80, double:1.7786363E-317)
            r5.scheduleAtFixedRate(r6, r7, r9, r11)
        L_0x00cb:
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r0 = r4.A00
            r5 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r2, r0)
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0
            boolean r0 = r0.BFQ()
            if (r0 == 0) goto L_0x0115
            X.1Vy r0 = X.AnonymousClass1Vy.UNKNOWN
            boolean r0 = r3.equals(r0)
            if (r0 != 0) goto L_0x0115
            java.util.concurrent.ConcurrentMap r0 = r4.A02
            java.lang.Object r0 = r0.get(r1)
            boolean r0 = r3.equals(r0)
            if (r0 != 0) goto L_0x0115
            java.util.concurrent.ConcurrentMap r0 = r4.A02
            r0.put(r1, r3)
            X.1Y7 r0 = X.AnonymousClass1IG.A07
            X.063 r2 = r0.A09(r1)
            X.1Y7 r2 = (X.AnonymousClass1Y7) r2
            int r1 = X.AnonymousClass1Y3.B6q
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0
            X.1hn r1 = r0.edit()
            java.lang.String r0 = r3.toString()
            r1.BzC(r2, r0)
            r1.commit()
        L_0x0115:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.common.connectionstatus.FbDataConnectionManager.A01(com.facebook.common.connectionstatus.FbDataConnectionManager):void");
    }

    public static boolean A02(FbDataConnectionManager fbDataConnectionManager) {
        return ((C09340h3) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AeN, fbDataConnectionManager.A00)).A0Q();
    }

    public double A03() {
        return ((AnonymousClass1W1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B1i, this.A00)).A07();
    }

    public double A04() {
        C48552ac r0 = ((C31121jB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BH9, this.A00)).A01;
        if (r0 == null) {
            return -1.0d;
        }
        return r0.Adp();
    }

    public int A05(double d) {
        if (d <= 0.0d || d >= 1.0d) {
            throw new IllegalArgumentException("Confidence must be between 0 and 1, exclusive.");
        }
        int A072 = (int) ((AnonymousClass1W1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B1i, this.A00)).A07();
        if (A072 <= 0) {
            return -1;
        }
        return A072;
    }

    public String A09() {
        if (this.A05 == null) {
            this.A05 = ((C09340h3) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AeN, this.A00)).A0E();
        }
        NetworkInfo networkInfo = this.A05;
        if (networkInfo != null) {
            int type = networkInfo.getType();
            if (type == 0) {
                return C37511vn.A01(networkInfo.getSubtype());
            }
            if (type == 1) {
                if (((C09340h3) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AeN, this.A00)).A0P()) {
                    return "HOTSPOT";
                }
                return "WIFI";
            }
        }
        return "UNKNOWN";
    }

    public void BPP(AnonymousClass1Vy r2) {
        this.A03.set(r2);
        A01(this);
    }

    public void BcS(AnonymousClass1Vy r2) {
        this.A04.set(r2);
        A01(this);
    }

    private FbDataConnectionManager(AnonymousClass1XY r4) {
        AnonymousClass1Vy r1 = AnonymousClass1Vy.UNKNOWN;
        this.A03 = new AtomicReference(r1);
        this.A04 = new AtomicReference(r1);
        this.A06 = false;
        this.A05 = null;
        this.A00 = new AnonymousClass0UN(10, r4);
        this.A02 = new C30191he(this);
    }

    public AnonymousClass1Vy A06() {
        init();
        return (AnonymousClass1Vy) this.A03.get();
    }

    public AnonymousClass1Vy A07() {
        init();
        return (AnonymousClass1Vy) this.A04.get();
    }

    public AnonymousClass1Vy A08() {
        AnonymousClass1Vy r2;
        init();
        AnonymousClass1Vy A062 = A06();
        if (!A062.equals(AnonymousClass1Vy.UNKNOWN)) {
            return A062;
        }
        if (this.A05 == null) {
            this.A05 = ((C09340h3) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AeN, this.A00)).A0E();
        }
        NetworkInfo networkInfo = this.A05;
        if (networkInfo == null) {
            return AnonymousClass1Vy.UNKNOWN;
        }
        AnonymousClass1IG r5 = (AnonymousClass1IG) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AyU, this.A00);
        String A09 = A09();
        int i = AnonymousClass1Y3.B6q;
        if (!((FbSharedPreferences) AnonymousClass1XX.A02(0, i, r5.A00)).BFQ()) {
            r2 = AnonymousClass1Vy.UNKNOWN;
        } else if (r5.A02.containsKey(A09)) {
            r2 = (AnonymousClass1Vy) r5.A02.get(A09);
        } else {
            String B4F = ((FbSharedPreferences) AnonymousClass1XX.A02(0, i, r5.A00)).B4F((AnonymousClass1Y7) AnonymousClass1IG.A07.A09(A09), BuildConfig.FLAVOR);
            r2 = AnonymousClass1Vy.UNKNOWN;
            if (!TextUtils.isEmpty(B4F)) {
                try {
                    r2 = AnonymousClass1Vy.valueOf(B4F);
                } catch (IllegalArgumentException unused) {
                }
            }
            r5.A02.put(A09, r2);
        }
        if (!r2.equals(AnonymousClass1Vy.UNKNOWN)) {
            return r2;
        }
        if (C37511vn.A04(networkInfo.getType(), networkInfo.getSubtype())) {
            return AnonymousClass1Vy.POOR;
        }
        return AnonymousClass1Vy.GOOD;
    }

    public void init() {
        int A032 = C000700l.A03(-1701222912);
        if (!this.A06 && !((AnonymousClass1Y6) AnonymousClass1XX.A02(7, AnonymousClass1Y3.APr, this.A00)).BHM()) {
            synchronized (this) {
                try {
                    if (!this.A06) {
                        this.A03.set(((AnonymousClass1W1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B1i, this.A00)).A08(this));
                        AtomicReference atomicReference = this.A04;
                        C31121jB r2 = (C31121jB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BH9, this.A00);
                        if (this != null) {
                            r2.A06.add(this);
                        }
                        atomicReference.set((AnonymousClass1Vy) r2.A02.get());
                        C04460Ut r0 = (C04460Ut) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKb, this.A00);
                        if (r0 != null) {
                            C06600bl BMm = r0.BMm();
                            BMm.A02("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED", this.A02);
                            BMm.A00().A00();
                        }
                        this.A06 = true;
                    }
                } finally {
                    C000700l.A09(-1297918281, A032);
                }
            }
        }
    }
}
