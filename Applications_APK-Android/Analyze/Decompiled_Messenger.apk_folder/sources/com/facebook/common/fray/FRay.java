package com.facebook.common.fray;

import X.AnonymousClass01q;

public class FRay {
    public static volatile boolean sEnabled;

    private static native int disableNative();

    private static native int enableNative(int i);

    public static native long[] getExecutedFunctionIds();

    static {
        AnonymousClass01q.A08("fray");
    }
}
