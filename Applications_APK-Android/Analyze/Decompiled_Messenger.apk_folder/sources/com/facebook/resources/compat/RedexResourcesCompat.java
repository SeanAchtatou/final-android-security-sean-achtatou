package com.facebook.resources.compat;

import android.content.res.Resources;
import android.text.TextUtils;

public class RedexResourcesCompat {
    public static int getIdentifier(Resources resources, String str, String str2, String str3) {
        if (resources != null) {
            int i = 0;
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            if (str.charAt(0) == '^') {
                return resources.getIdentifier(str, str2, str3);
            }
            int indexOf = str.indexOf(58);
            if (indexOf > -1) {
                str3 = str.substring(0, indexOf);
            }
            int indexOf2 = str.indexOf(47);
            if (indexOf2 > -1) {
                if (indexOf > 0) {
                    i = indexOf + 1;
                }
                str2 = str.substring(i, indexOf2);
            }
            if (indexOf > -1 || indexOf2 > -1) {
                str = str.substring(Math.max(indexOf, indexOf2) + 1);
            }
            int identifier = resources.getIdentifier(str, str2, str3);
            if (identifier != 0) {
                return identifier;
            }
            if (str2 != null) {
                int length = str2.length();
                StringBuilder sb = new StringBuilder(length + 1);
                sb.append(str2);
                for (int i2 = 2; i2 <= 6; i2++) {
                    sb.setLength(length);
                    sb.append(i2);
                    int identifier2 = resources.getIdentifier(str, sb.toString(), str3);
                    if (identifier2 != 0) {
                        return identifier2;
                    }
                }
            }
            return 0;
        }
        throw new NullPointerException("Resources null");
    }

    public static String getResourceTypeName(Resources resources, int i) {
        String resourceTypeName = resources.getResourceTypeName(i);
        if (TextUtils.isEmpty(resourceTypeName)) {
            return resourceTypeName;
        }
        int length = resourceTypeName.length();
        int i2 = length - 1;
        int i3 = 0;
        while (i2 >= 0 && Character.isDigit(resourceTypeName.charAt(i2))) {
            i3++;
            i2--;
        }
        if (i3 != 0) {
            return resourceTypeName.substring(0, length - i3);
        }
        return resourceTypeName;
    }
}
