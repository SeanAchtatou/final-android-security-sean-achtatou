package com.facebook.resources.impl;

import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass14J;
import X.AnonymousClass15L;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C04980Xe;
import X.C23901Rk;
import X.C25781aO;
import X.C25791aP;
import X.C37881wV;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
public final class DrawableCounterLogger {
    private static volatile DrawableCounterLogger A04;
    public AnonymousClass0UN A00;
    private final C25791aP A01 = new C25781aO();
    private final C25791aP A02 = new C25781aO();
    private final List A03 = new ArrayList();

    public static void A01(DrawableCounterLogger drawableCounterLogger) {
        int[] iArr;
        int[] iArr2;
        AnonymousClass15L[] r1;
        synchronized (drawableCounterLogger) {
            C25791aP r12 = drawableCounterLogger.A01;
            iArr = null;
            if (r12.isEmpty()) {
                r1 = null;
                iArr2 = null;
            } else {
                iArr = r12.CJ1();
                r1 = (AnonymousClass15L[]) drawableCounterLogger.A03.toArray(new AnonymousClass15L[0]);
                iArr2 = drawableCounterLogger.A02.CJ1();
                drawableCounterLogger.A01.clear();
                drawableCounterLogger.A03.clear();
                drawableCounterLogger.A02.clear();
            }
        }
        if (iArr != null && r1 != null) {
            C23901Rk.A00(new C37881wV(drawableCounterLogger, iArr, r1, iArr2), (Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, drawableCounterLogger.A00));
        }
    }

    public static final DrawableCounterLogger A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (DrawableCounterLogger.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new DrawableCounterLogger(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void A02(int i) {
        int size;
        if (((C04980Xe) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BC6, this.A00)).A03("counters")) {
            synchronized (this) {
                this.A01.AMO(i);
                AnonymousClass15L A012 = ((AnonymousClass14J) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Asq, this.A00)).A01();
                int size2 = this.A03.size() - 1;
                if (size2 < 0 || this.A03.get(size2) != A012) {
                    this.A03.add(A012);
                    this.A02.AMO(1);
                } else {
                    C25791aP r1 = this.A02;
                    r1.C5c(size2, r1.Ab3(size2) + 1);
                }
                size = this.A01.size();
            }
            if (size >= 128) {
                A01(this);
            }
        }
    }

    private DrawableCounterLogger(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
