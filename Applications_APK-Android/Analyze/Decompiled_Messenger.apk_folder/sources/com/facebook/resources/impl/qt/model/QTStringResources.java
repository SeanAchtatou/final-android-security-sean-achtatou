package com.facebook.resources.impl.qt.model;

import X.AnonymousClass0lk;
import X.AnonymousClass8N6;
import X.C04310Tq;
import X.C08650fi;

public final class QTStringResources extends C08650fi {
    public final AnonymousClass8N6 A00;
    public final AnonymousClass0lk A01;
    public final String A02;
    public final C04310Tq A03;

    public QTStringResources(AnonymousClass0lk r1, AnonymousClass8N6 r2, String str, C04310Tq r4) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = str;
        this.A03 = r4;
    }
}
