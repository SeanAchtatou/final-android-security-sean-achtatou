package com.facebook.resources.impl.qt;

import X.AnonymousClass067;
import X.AnonymousClass06B;
import X.AnonymousClass0UN;
import X.AnonymousClass0WD;
import X.AnonymousClass0XJ;
import X.AnonymousClass1XY;
import X.C04310Tq;
import X.C08700fo;
import X.C08710fp;
import X.C09400hF;
import X.C09450hO;
import javax.inject.Singleton;

@Singleton
public final class QTStringResourcesProvider {
    private static volatile QTStringResourcesProvider A06;
    public AnonymousClass0UN A00;
    public final AnonymousClass06B A01 = AnonymousClass067.A02();
    public final C09400hF A02;
    public final C08700fo A03;
    public final C08710fp A04;
    public final C04310Tq A05;

    public static final QTStringResourcesProvider A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (QTStringResourcesProvider.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new QTStringResourcesProvider(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private QTStringResourcesProvider(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A03 = new C08700fo(r3);
        this.A04 = C08710fp.A00(r3);
        this.A02 = C09450hO.A00(r3);
        this.A05 = AnonymousClass0XJ.A0K(r3);
    }
}
