package com.facebook.resources.ui;

import X.AnonymousClass1S1;
import X.C000700l;
import X.C24031Rx;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public class FbTextView extends C24031Rx implements AnonymousClass1S1 {
    private boolean A00 = false;

    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        boolean z = false;
        A02(false);
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        if (this.A00 && getVisibility() == 0) {
            z = true;
        }
        A02(z);
    }

    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        boolean z = false;
        A02(false);
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        if (this.A00 && getVisibility() == 0) {
            z = true;
        }
        A02(z);
    }

    private void A02(boolean z) {
        for (Drawable drawable : getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setVisible(z, false);
            }
        }
    }

    public void onAttachedToWindow() {
        int A06 = C000700l.A06(115317600);
        super.onAttachedToWindow();
        boolean z = true;
        this.A00 = true;
        if (getVisibility() != 0) {
            z = false;
        }
        A02(z);
        C000700l.A0C(-2049096741, A06);
    }

    public void onDetachedFromWindow() {
        int A06 = C000700l.A06(696275721);
        super.onDetachedFromWindow();
        this.A00 = false;
        A02(false);
        C000700l.A0C(-400404299, A06);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000c, code lost:
        if (getVisibility() != 0) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setVisibility(int r3) {
        /*
            r2 = this;
            super.setVisibility(r3)
            boolean r0 = r2.A00
            if (r0 == 0) goto L_0x000e
            int r1 = r2.getVisibility()
            r0 = 1
            if (r1 == 0) goto L_0x000f
        L_0x000e:
            r0 = 0
        L_0x000f:
            r2.A02(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.resources.ui.FbTextView.setVisibility(int):void");
    }

    public FbTextView(Context context) {
        super(context);
    }

    public FbTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FbTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public FbTextView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
