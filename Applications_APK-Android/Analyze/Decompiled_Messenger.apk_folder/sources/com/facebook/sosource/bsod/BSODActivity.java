package com.facebook.sosource.bsod;

import X.C000700l;
import X.C03920Qk;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;

public class BSODActivity extends Activity {
    public static void A00(Context context, int i, int i2, int i3, int i4, int i5) {
        String str;
        C03920Qk r9 = new C03920Qk(i, i2, i3, i4, i5);
        Resources resources = context.getResources();
        Intent intent = new Intent(context, BSODActivity.class);
        intent.addFlags(335544320);
        if (resources != null) {
            str = resources.getString(r9.A00);
            String string = resources.getString(r9.A01, str);
            intent.putExtra("com.facebook.sosource.bsod.application_name", str);
            intent.putExtra("com.facebook.sosource.bsod.bsod_cause_text", string);
        } else {
            str = null;
        }
        Notification.Builder autoCancel = new Notification.Builder(context).setSmallIcon(r9.A02).setAutoCancel(true);
        if (resources != null) {
            String string2 = resources.getString(r9.A04, str);
            String string3 = resources.getString(r9.A03, str);
            autoCancel.setContentText(string3);
            autoCancel.setContentTitle(string2);
            autoCancel.setTicker(string3);
        } else {
            autoCancel.setContentText("Unknown error. Please open for details.");
            autoCancel.setContentTitle("Application Error");
            autoCancel.setTicker("Unknown error. Please open for details.");
        }
        autoCancel.setContentIntent(PendingIntent.getActivity(context, 0, intent, 134217728));
        ((NotificationManager) context.getSystemService("notification")).notify(1, autoCancel.build());
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [android.graphics.drawable.Drawable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r16) {
        /*
            r15 = this;
            r0 = 444714414(0x1a81cdae, float:5.368544E-23)
            int r2 = X.C000700l.A00(r0)
            r0 = r16
            super.onCreate(r0)
            android.content.res.Resources r13 = r15.getResources()
            android.content.Intent r4 = r15.getIntent()
            r0 = 2132410555(0x7f1a00bb, float:2.047049E38)
            r15.setContentView(r0)
            r0 = 2131296864(0x7f090260, float:1.8211657E38)
            android.view.View r12 = r15.findViewById(r0)
            android.widget.TextView r12 = (android.widget.TextView) r12
            r0 = 2131296867(0x7f090263, float:1.8211663E38)
            android.view.View r3 = r15.findViewById(r0)
            android.widget.TextView r3 = (android.widget.TextView) r3
            r0 = 2131296865(0x7f090261, float:1.8211659E38)
            android.view.View r9 = r15.findViewById(r0)
            android.widget.TextView r9 = (android.widget.TextView) r9
            r0 = 2131296913(0x7f090291, float:1.8211756E38)
            android.view.View r7 = r15.findViewById(r0)
            android.widget.Button r7 = (android.widget.Button) r7
            r0 = 2131296866(0x7f090262, float:1.821166E38)
            android.view.View r5 = r15.findViewById(r0)
            android.widget.ImageView r5 = (android.widget.ImageView) r5
            java.lang.String r0 = "com.facebook.sosource.bsod.application_name"
            java.lang.String r14 = r4.getStringExtra(r0)
            if (r14 == 0) goto L_0x011f
            java.lang.String r0 = "com.facebook.sosource.bsod.bsod_cause_text"
            java.lang.String r11 = r4.getStringExtra(r0)
            java.lang.String r0 = "com.facebook.sosource.bsod.bsod_cta_label"
            java.lang.String r8 = r4.getStringExtra(r0)
            java.lang.String r0 = "com.facebook.sosource.bsod.bsod_cta_action_label"
            java.lang.String r6 = r4.getStringExtra(r0)
            if (r11 != 0) goto L_0x006a
            r0 = 2131820610(0x7f110042, float:1.927394E38)
            java.lang.String r11 = r13.getString(r0)
        L_0x006a:
            r1 = 0
            if (r12 == 0) goto L_0x007b
            r10 = 2131820610(0x7f110042, float:1.927394E38)
            java.lang.Object[] r0 = new java.lang.Object[]{r14}
            java.lang.String r0 = r13.getString(r10, r0)
            r12.setText(r0)
        L_0x007b:
            if (r9 == 0) goto L_0x0098
            if (r11 == 0) goto L_0x0111
            r9.setText(r11)
            java.lang.CharSequence r0 = r9.getText()
            java.lang.String r0 = r0.toString()
            android.text.Spanned r0 = android.text.Html.fromHtml(r0)
            r9.setText(r0)
            android.text.method.MovementMethod r0 = android.text.method.LinkMovementMethod.getInstance()
            r9.setMovementMethod(r0)
        L_0x0098:
            java.lang.String r0 = "com.facebook.sosource.bsod.bsod_msg_icon"
            int r0 = r4.getIntExtra(r0, r1)
            if (r5 == 0) goto L_0x00a8
            if (r0 == 0) goto L_0x00a8
            r5.setImageResource(r0)
            r5.setVisibility(r1)
        L_0x00a8:
            java.lang.String r0 = "com.facebook.sosource.bsod.bsod_title_text"
            java.lang.String r0 = r4.getStringExtra(r0)
            if (r0 == 0) goto L_0x00b8
            if (r3 == 0) goto L_0x00b8
            r3.setText(r0)
            r3.setVisibility(r1)
        L_0x00b8:
            if (r7 == 0) goto L_0x00f4
            if (r6 == 0) goto L_0x00f4
            if (r8 == 0) goto L_0x00f4
            r4 = 0
            android.graphics.drawable.Drawable r1 = r7.getBackground()
            boolean r0 = r1 instanceof android.graphics.drawable.GradientDrawable
            if (r0 == 0) goto L_0x00fb
            r4 = r1
            android.graphics.drawable.GradientDrawable r4 = (android.graphics.drawable.GradientDrawable) r4
        L_0x00ca:
            r3 = 0
            if (r4 == 0) goto L_0x00e6
            r4.mutate()
            r4.setShape(r3)
            android.content.res.Resources r0 = r15.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            float r1 = r0.density
            r0 = 1086324736(0x40c00000, float:6.0)
            float r1 = r1 * r0
            r0 = 1056964608(0x3f000000, float:0.5)
            float r1 = r1 + r0
            r4.setCornerRadius(r1)
        L_0x00e6:
            r7.setText(r8)
            X.0Qn r0 = new X.0Qn
            r0.<init>(r15, r6)
            r7.setOnClickListener(r0)
            r7.setVisibility(r3)
        L_0x00f4:
            r0 = -443158044(0xffffffffe595f1e4, float:-8.851184E22)
            X.C000700l.A07(r0, r2)
            return
        L_0x00fb:
            boolean r0 = r1 instanceof android.graphics.drawable.ColorDrawable
            if (r0 == 0) goto L_0x00ca
            android.graphics.drawable.ColorDrawable r1 = (android.graphics.drawable.ColorDrawable) r1
            android.graphics.drawable.GradientDrawable r4 = new android.graphics.drawable.GradientDrawable
            r4.<init>()
            int r0 = r1.getColor()
            r4.setColor(r0)
            r7.setBackground(r4)
            goto L_0x00ca
        L_0x0111:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Missing description"
            r1.<init>(r0)
            r0 = 927405545(0x374715e9, float:1.1866426E-5)
            X.C000700l.A07(r0, r2)
            throw r1
        L_0x011f:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Missing app name"
            r1.<init>(r0)
            r0 = 1765954956(0x6942558c, float:1.4683475E25)
            X.C000700l.A07(r0, r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.sosource.bsod.BSODActivity.onCreate(android.os.Bundle):void");
    }

    public void onStop() {
        int A00 = C000700l.A00(459820724);
        super.onStop();
        try {
            System.exit(10);
        } catch (Throwable unused) {
        }
        C000700l.A07(-1829284441, A00);
    }
}
