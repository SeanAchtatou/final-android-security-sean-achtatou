package com.facebook.zero;

import X.AnonymousClass0UN;
import X.AnonymousClass0US;
import X.AnonymousClass0VB;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1ZS;
import com.facebook.secure.intentswitchoff.FbReceiverSwitchOffDI;
import javax.inject.Singleton;

@Singleton
public final class LocalZeroTokenManagerReceiverRegistration extends AnonymousClass1ZS {
    private static volatile LocalZeroTokenManagerReceiverRegistration A02;
    public AnonymousClass0UN A00;
    public boolean A01 = false;

    public static final LocalZeroTokenManagerReceiverRegistration A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (LocalZeroTokenManagerReceiverRegistration.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A02 = new LocalZeroTokenManagerReceiverRegistration(applicationInjector, FbReceiverSwitchOffDI.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.B9g, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private LocalZeroTokenManagerReceiverRegistration(AnonymousClass1XY r3, FbReceiverSwitchOffDI fbReceiverSwitchOffDI, AnonymousClass0US r5) {
        super(fbReceiverSwitchOffDI, r5);
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
