package com.facebook.zero.common;

import X.C06850cB;
import X.C101764t7;
import X.C28361eg;
import X.C32671m8;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class ZeroToken implements Parcelable {
    public static final ZeroToken A0J;
    public static final Parcelable.Creator CREATOR = new C32671m8();
    public final int A00;
    public final int A01;
    public final ZeroTrafficEnforcementConfig A02;
    public final ImmutableList A03;
    public final ImmutableList A04;
    public final ImmutableMap A05;
    public final ImmutableSet A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;

    public int describeContents() {
        return 0;
    }

    static {
        RegularImmutableSet regularImmutableSet = RegularImmutableSet.A05;
        ImmutableList immutableList = RegularImmutableList.A02;
        A0J = new ZeroToken(null, null, null, null, null, null, 0, regularImmutableSet, immutableList, null, immutableList, null, 0, null, null, null, null, null, ZeroTrafficEnforcementConfig.A00);
    }

    public boolean A01(ZeroToken zeroToken) {
        if (!Objects.equal(this.A08, zeroToken.A08) || !Objects.equal(this.A07, zeroToken.A07) || !Objects.equal(this.A0G, zeroToken.A0G) || !Objects.equal(this.A0F, zeroToken.A0F) || !Objects.equal(this.A0A, zeroToken.A0A) || !Objects.equal(this.A09, zeroToken.A09) || !Objects.equal(Integer.valueOf(this.A01), Integer.valueOf(zeroToken.A01)) || !Objects.equal(this.A06, zeroToken.A06) || !Objects.equal(this.A04, zeroToken.A04) || !Objects.equal(this.A0I, zeroToken.A0I) || !Objects.equal(this.A03, zeroToken.A03) || !Objects.equal(this.A0C, zeroToken.A0C) || !Objects.equal(this.A05, zeroToken.A05) || !Objects.equal(this.A0E, zeroToken.A0E) || !Objects.equal(this.A0D, zeroToken.A0D) || !Objects.equal(this.A0B, zeroToken.A0B)) {
            return false;
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ZeroToken)) {
            return false;
        }
        ZeroToken zeroToken = (ZeroToken) obj;
        if (!A01(zeroToken) || !Objects.equal(this.A0H, zeroToken.A0H)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.A07;
        String str2 = this.A0G;
        String str3 = this.A0F;
        String str4 = this.A0A;
        String str5 = this.A08;
        String str6 = this.A09;
        Integer valueOf = Integer.valueOf(this.A01);
        ImmutableSet immutableSet = this.A06;
        ImmutableList immutableList = this.A04;
        String str7 = this.A0I;
        ImmutableList immutableList2 = this.A03;
        String str8 = this.A0H;
        String str9 = this.A0C;
        ImmutableMap immutableMap = this.A05;
        String str10 = this.A0E;
        return Arrays.hashCode(new Object[]{str, str2, str3, str4, str5, str6, valueOf, immutableSet, immutableList, str7, immutableList2, str8, str9, immutableMap, str10, this.A0D, this.A0B});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A07);
        parcel.writeString(this.A0G);
        parcel.writeString(this.A0F);
        parcel.writeString(this.A0A);
        parcel.writeString(this.A08);
        parcel.writeString(this.A09);
        parcel.writeInt(this.A01);
        parcel.writeStringList(ImmutableList.copyOf((Iterable) this.A06));
        parcel.writeTypedList(this.A04);
        parcel.writeString(this.A0I);
        parcel.writeTypedList(this.A03);
        parcel.writeString(this.A0H);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A0C);
        Bundle bundle = new Bundle();
        bundle.putSerializable("zero_pool_pricing_map_serializable", this.A05);
        parcel.writeBundle(bundle);
        parcel.writeString(this.A0E);
        parcel.writeString(this.A0D);
        parcel.writeString(this.A0B);
        parcel.writeString(C101764t7.A01(this.A02));
    }

    public static boolean A00(String str) {
        if (C06850cB.A0B(str) || str.equals("0") || str.equals(ErrorReportingConstants.ANR_DEFAULT_RECOVERY_DELAY_VAL)) {
            return false;
        }
        return true;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("campaignId", this.A07);
        stringHelper.add("status", this.A0G);
        stringHelper.add("regStatus", this.A0F);
        stringHelper.add("carrierName", this.A0A);
        stringHelper.add("carrierId", this.A08);
        stringHelper.add("carrierLogoUrl", this.A09);
        stringHelper.add("ttl", this.A01);
        stringHelper.add("enabledUiFeatures", this.A06);
        stringHelper.add("rewriteRules", this.A04);
        stringHelper.add("unregistered_reason", this.A0I);
        stringHelper.add("backupRewriteRules", this.A03);
        stringHelper.add("tokenHash", this.A0H);
        stringHelper.add("requestTime", this.A00);
        stringHelper.add("fastTokenHash", this.A0C);
        stringHelper.add("poolPricingMap", this.A05);
        stringHelper.add("mqttHost", this.A0E);
        stringHelper.add("fbnsHost", this.A0D);
        stringHelper.add("eligibilityHash", this.A0B);
        stringHelper.add("ZeroTrafficEnforcementConfig", this.A02);
        return stringHelper.toString();
    }

    public ZeroToken(Parcel parcel) {
        this.A07 = parcel.readString();
        this.A0G = parcel.readString();
        this.A0F = parcel.readString();
        this.A0A = parcel.readString();
        this.A08 = parcel.readString();
        this.A09 = parcel.readString();
        this.A01 = parcel.readInt();
        this.A06 = ImmutableSet.A0A(C28361eg.A00(parcel.createStringArrayList()));
        this.A04 = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ZeroUrlRewriteRule.CREATOR));
        this.A0I = parcel.readString();
        this.A03 = ImmutableList.copyOf((Collection) parcel.createTypedArrayList(ZeroUrlRewriteRule.CREATOR));
        this.A0H = parcel.readString();
        this.A00 = parcel.readInt();
        this.A0C = parcel.readString();
        HashMap hashMap = (HashMap) parcel.readBundle().getSerializable("zero_pool_pricing_map_serializable");
        ImmutableMap.Builder builder = ImmutableMap.builder();
        if (hashMap != null) {
            for (Map.Entry entry : hashMap.entrySet()) {
                builder.put(entry.getKey(), entry.getValue());
            }
            this.A05 = builder.build();
        } else {
            this.A05 = null;
        }
        this.A0E = parcel.readString();
        this.A0D = parcel.readString();
        this.A0B = parcel.readString();
        this.A02 = C101764t7.A00(parcel.readString());
    }

    public ZeroToken(String str, String str2, String str3, String str4, String str5, String str6, int i, ImmutableSet immutableSet, ImmutableList immutableList, String str7, ImmutableList immutableList2, String str8, int i2, String str9, ImmutableMap immutableMap, String str10, String str11, String str12, ZeroTrafficEnforcementConfig zeroTrafficEnforcementConfig) {
        this.A07 = str;
        this.A0G = str2;
        this.A0F = str3;
        this.A0A = str4;
        this.A08 = str5;
        this.A09 = str6;
        this.A06 = immutableSet;
        this.A04 = immutableList;
        this.A0I = str7;
        this.A03 = immutableList2;
        this.A05 = immutableMap;
        this.A0E = str10;
        this.A0D = str11;
        this.A00 = i2;
        this.A01 = i;
        this.A0H = str8;
        this.A0C = str9;
        this.A0B = str12;
        this.A02 = zeroTrafficEnforcementConfig;
    }
}
