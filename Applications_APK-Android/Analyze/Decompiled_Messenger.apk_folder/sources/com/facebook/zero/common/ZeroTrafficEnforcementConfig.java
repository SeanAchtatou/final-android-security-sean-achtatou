package com.facebook.zero.common;

import com.facebook.common.json.AutoGenJsonDeserializer;
import com.facebook.common.json.AutoGenJsonSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

@AutoGenJsonSerializer
@AutoGenJsonDeserializer
public class ZeroTrafficEnforcementConfig {
    public static final ZeroTrafficEnforcementConfig A00 = new ZeroTrafficEnforcementConfig();
    @JsonProperty("pass_rate")
    public final double mPassRate;
    @JsonProperty("subnets_whitelist")
    public final ImmutableList<String> mSubnetsWhiteList;
    @JsonProperty("torque_enabled")
    public final boolean mTorqueEnabled;

    public String toString() {
        return "ZeroTrafficEnforcementConfig:{" + "TorqueEnabled=" + this.mTorqueEnabled + ", PassRate=" + this.mPassRate + ", SubnetsWhiteList=" + this.mSubnetsWhiteList + "}";
    }

    public ZeroTrafficEnforcementConfig() {
        this.mSubnetsWhiteList = RegularImmutableList.A02;
        this.mPassRate = 0.0d;
        this.mTorqueEnabled = false;
    }

    public ZeroTrafficEnforcementConfig(ImmutableList immutableList, double d, boolean z) {
        this.mSubnetsWhiteList = immutableList;
        this.mPassRate = d;
        this.mTorqueEnabled = z;
    }
}
