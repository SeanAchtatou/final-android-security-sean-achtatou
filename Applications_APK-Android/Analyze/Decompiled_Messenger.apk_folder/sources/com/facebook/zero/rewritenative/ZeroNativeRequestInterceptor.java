package com.facebook.zero.rewritenative;

import X.AnonymousClass01q;
import X.AnonymousClass0WA;
import X.AnonymousClass0WD;
import X.AnonymousClass0YI;
import X.AnonymousClass19P;
import X.AnonymousClass1ES;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.AnonymousClass1YI;
import X.C04990Xf;
import X.C05000Xg;
import X.C14250su;
import X.C14560tb;
import X.C14730tt;
import X.C183312f;
import X.C28191eP;
import X.C31861kZ;
import X.C31871ka;
import com.facebook.http.common.BootstrapRequestName;
import com.facebook.jni.HybridData;
import com.facebook.tigon.RequestInterceptor;
import com.facebook.zero.common.ZeroUrlRewriteRule;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
public class ZeroNativeRequestInterceptor extends RequestInterceptor implements C14250su {
    private static volatile ZeroNativeRequestInterceptor $ul_$xXXcom_facebook_zero_rewritenative_ZeroNativeRequestInterceptor$xXXINSTANCE;
    private AnonymousClass0YI mGKListener;
    private final AnonymousClass1YI mGkStore;
    private final C183312f mRuleObserver;
    private final C28191eP mZeroTokenManager;

    private native HybridData initHybrid(boolean z, boolean z2, boolean z3, boolean z4);

    private native void setData(ZeroNativeDataBuilder zeroNativeDataBuilder);

    /* access modifiers changed from: private */
    public native void setDefaultBootstrapRequests(Set set);

    private native void setDialtoneEnabled(boolean z);

    /* access modifiers changed from: private */
    public native void setUseBackupRewriteRules(boolean z);

    /* access modifiers changed from: private */
    public native void setUseSessionlessBackupRewriteRules(boolean z);

    /* access modifiers changed from: private */
    public native void setZeroRatingEnabled(boolean z);

    private native void updateConfig(boolean z, String str, String str2, String str3, String str4);

    public void gqlConfigUpdated(C31871ka r7) {
        updateConfig(true, r7.A01, r7.A02, r7.A00, r7.A03);
    }

    public void onBeforeDialtoneStateChanged(boolean z) {
    }

    public static final ZeroNativeRequestInterceptor $ul_$xXXcom_facebook_zero_rewritenative_ZeroNativeRequestInterceptor$xXXFACTORY_METHOD(AnonymousClass1XY r7) {
        if ($ul_$xXXcom_facebook_zero_rewritenative_ZeroNativeRequestInterceptor$xXXINSTANCE == null) {
            synchronized (ZeroNativeRequestInterceptor.class) {
                AnonymousClass0WD A00 = AnonymousClass0WD.A00($ul_$xXXcom_facebook_zero_rewritenative_ZeroNativeRequestInterceptor$xXXINSTANCE, r7);
                if (A00 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        $ul_$xXXcom_facebook_zero_rewritenative_ZeroNativeRequestInterceptor$xXXINSTANCE = new ZeroNativeRequestInterceptor(applicationInjector, C14560tb.A00(applicationInjector), C14730tt.A00(applicationInjector), C04990Xf.A00(applicationInjector));
                        A00.A01();
                    } catch (Throwable th) {
                        A00.A01();
                        throw th;
                    }
                }
            }
        }
        return $ul_$xXXcom_facebook_zero_rewritenative_ZeroNativeRequestInterceptor$xXXINSTANCE;
    }

    static {
        AnonymousClass01q.A08("rewritenativeinterceptor");
    }

    private static ZeroNativeDataBuilder generateBuilder(Set set, List list) {
        ZeroNativeDataBuilder zeroNativeDataBuilder = new ZeroNativeDataBuilder();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("^(https?)://api\\.([0-9a-zA-Z\\.-]*)?facebook\\.com\\/method\\/mobile\\.zeroBalanceDetection");
        int size = arrayList.size();
        int[] iArr = new int[size];
        int i = 0;
        for (String A03 : arrayList) {
            iArr[i] = zeroNativeDataBuilder.mFlatBufferBuilder.A03(A03);
            i++;
        }
        AnonymousClass19P r2 = zeroNativeDataBuilder.mFlatBufferBuilder;
        r2.A0C(4, size, 4);
        for (int i2 = size - 1; i2 >= 0; i2--) {
            r2.A04(iArr[i2]);
        }
        zeroNativeDataBuilder.mWhitelistVector = r2.A01();
        if (list != null) {
            int size2 = list.size();
            int[] iArr2 = new int[size2];
            Iterator it = list.iterator();
            int i3 = 0;
            while (it.hasNext()) {
                ZeroUrlRewriteRule zeroUrlRewriteRule = (ZeroUrlRewriteRule) it.next();
                AnonymousClass19P r7 = zeroNativeDataBuilder.mFlatBufferBuilder;
                int A032 = r7.A03(zeroUrlRewriteRule.A01);
                int A033 = zeroNativeDataBuilder.mFlatBufferBuilder.A03(zeroUrlRewriteRule.A02);
                r7.A06(2);
                r7.A0B(1, A033, 0);
                r7.A0B(0, A032, 0);
                iArr2[i3] = r7.A00();
                i3++;
            }
            AnonymousClass19P r22 = zeroNativeDataBuilder.mFlatBufferBuilder;
            r22.A0C(4, size2, 4);
            for (int i4 = size2 - 1; i4 >= 0; i4--) {
                r22.A04(iArr2[i4]);
            }
            zeroNativeDataBuilder.mRuleVector = r22.A01();
        }
        if (set != null) {
            int size3 = set.size();
            int[] iArr3 = new int[size3];
            Iterator it2 = set.iterator();
            int i5 = 0;
            while (it2.hasNext()) {
                iArr3[i5] = zeroNativeDataBuilder.mFlatBufferBuilder.A03((String) it2.next());
                i5++;
            }
            AnonymousClass19P r23 = zeroNativeDataBuilder.mFlatBufferBuilder;
            r23.A0C(4, size3, 4);
            for (int i6 = size3 - 1; i6 >= 0; i6--) {
                r23.A04(iArr3[i6]);
            }
            zeroNativeDataBuilder.mFeaturesVector = r23.A01();
        }
        zeroNativeDataBuilder.buildNative();
        return zeroNativeDataBuilder;
    }

    private ZeroNativeDataBuilder lazyLoadBuilder() {
        ZeroNativeDataBuilder generateBuilder = generateBuilder(this.mZeroTokenManager.A0J(), this.mZeroTokenManager.A0I());
        this.mRuleObserver.A00 = this;
        return generateBuilder;
    }

    public void rulesChanged(ImmutableList immutableList) {
        setData(generateBuilder(this.mZeroTokenManager.A0J(), immutableList));
    }

    public ZeroNativeRequestInterceptor(AnonymousClass1XY r7, C14560tb r8, AnonymousClass1ES r9, C05000Xg r10) {
        this.mZeroTokenManager = C28191eP.A00(r7);
        this.mRuleObserver = C183312f.A00(r7);
        AnonymousClass1YI A00 = AnonymousClass0WA.A00(r7);
        this.mGkStore = A00;
        this.mHybridData = initHybrid(A00.AbO(582, false), this.mGkStore.AbO(852, true), this.mGkStore.AbO(AnonymousClass1Y3.A14, true), r9.A0L());
        if (this.mGkStore.AbO(AnonymousClass1Y3.A6o, true)) {
            setDefaultBootstrapRequests(BootstrapRequestName.A00);
        }
        this.mGKListener = new C31861kZ(this);
        C31871ka A01 = r8.A01();
        updateConfig(true, A01.A01, A01.A02, A01.A00, A01.A03);
        r10.A00(this.mGKListener, 582);
        r10.A00(this.mGKListener, 852);
        r10.A00(this.mGKListener, AnonymousClass1Y3.A14);
        r10.A00(this.mGKListener, AnonymousClass1Y3.A6o);
        r8.A00 = this;
        r9.A0F(this);
    }

    public void onAfterDialtoneStateChanged(boolean z) {
        setDialtoneEnabled(z);
    }
}
