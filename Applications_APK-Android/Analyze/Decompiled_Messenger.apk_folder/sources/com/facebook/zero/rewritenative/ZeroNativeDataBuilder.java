package com.facebook.zero.rewritenative;

import X.AnonymousClass01q;
import X.AnonymousClass19P;
import com.facebook.common.dextricks.DexStore;
import com.facebook.jni.HybridData;

public class ZeroNativeDataBuilder {
    public int mFeaturesVector = -1;
    public AnonymousClass19P mFlatBufferBuilder = new AnonymousClass19P(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET);
    private HybridData mHybridData;
    public int mRuleVector = -1;
    public int mWhitelistVector = -1;

    private static native HybridData initHybrid(byte[] bArr);

    static {
        AnonymousClass01q.A08("rewritenativeinterceptor");
    }

    public void buildNative() {
        if (this.mFeaturesVector == -1) {
            this.mFlatBufferBuilder.A0C(4, 0, 4);
            this.mFeaturesVector = this.mFlatBufferBuilder.A01();
        }
        if (this.mWhitelistVector == -1) {
            this.mFlatBufferBuilder.A0C(4, 0, 4);
            this.mWhitelistVector = this.mFlatBufferBuilder.A01();
        }
        if (this.mRuleVector == -1) {
            this.mFlatBufferBuilder.A0C(4, 0, 4);
            this.mRuleVector = this.mFlatBufferBuilder.A01();
        }
        AnonymousClass19P r5 = this.mFlatBufferBuilder;
        int i = this.mFeaturesVector;
        int i2 = this.mWhitelistVector;
        int i3 = this.mRuleVector;
        r5.A06(3);
        r5.A0B(2, i3, 0);
        r5.A0B(1, i2, 0);
        r5.A0B(0, i, 0);
        this.mFlatBufferBuilder.A05(r5.A00());
        AnonymousClass19P r3 = this.mFlatBufferBuilder;
        int i4 = r3.A06;
        byte[] bArr = new byte[(r3.A07.capacity() - r3.A06)];
        r3.A07.position(i4);
        r3.A07.get(bArr);
        this.mHybridData = initHybrid(bArr);
    }
}
