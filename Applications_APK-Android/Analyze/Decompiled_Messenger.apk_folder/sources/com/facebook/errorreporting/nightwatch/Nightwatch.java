package com.facebook.errorreporting.nightwatch;

import X.AnonymousClass01q;

public class Nightwatch {
    public static native int start(String str, String str2, boolean z, boolean z2);

    static {
        AnonymousClass01q.A08("fbnightwatch");
    }
}
