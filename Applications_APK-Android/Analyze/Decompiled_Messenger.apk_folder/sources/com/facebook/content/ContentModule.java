package com.facebook.content;

import X.AnonymousClass062;
import X.AnonymousClass0UN;
import X.AnonymousClass0UV;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C28001e6;
import android.content.Context;
import com.facebook.inject.InjectorModule;

@InjectorModule
public class ContentModule extends AnonymousClass0UV {

    public class ContentModuleSelendroidInjector implements AnonymousClass062 {
        public AnonymousClass0UN A00;

        public SecureContextHelper getSecureContextHelper() {
            return (SecureContextHelper) AnonymousClass1XX.A03(AnonymousClass1Y3.A7S, this.A00);
        }

        public ContentModuleSelendroidInjector(Context context) {
            this.A00 = new AnonymousClass0UN(0, AnonymousClass1XX.get(context));
        }
    }

    public static SecureContextHelper getInstanceForTest_SecureContextHelper(AnonymousClass1XX r1) {
        return (SecureContextHelper) r1.getInstance(SecureContextHelper.class);
    }

    public static final SecureContextHelper A00(AnonymousClass1XY r0) {
        return C28001e6.A01(r0);
    }
}
