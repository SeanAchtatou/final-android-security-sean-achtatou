package com.facebook.content;

import X.C28131eJ;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;

public interface SecureContextHelper {
    C28131eJ AZR();

    C28131eJ BLv();

    C28131eJ BLw();

    void CGs(Intent intent, int i, Activity activity);

    void CGt(Intent intent, int i, Fragment fragment);

    void CHH(Intent intent, Context context);

    void CHJ(Intent intent, int i, Activity activity);

    void CHK(Intent intent, int i, Fragment fragment);

    void startFacebookActivity(Intent intent, Context context);
}
