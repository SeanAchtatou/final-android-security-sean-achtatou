package com.facebook.interstitial.triggers;

import X.AnonymousClass07K;
import X.C20991Ep;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableMap;
import java.util.HashMap;
import java.util.Map;

public final class InterstitialTriggerContext implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C20991Ep();
    public final ImmutableMap A00;

    public int describeContents() {
        return 0;
    }

    public String A00(String str) {
        return (String) this.A00.get(str);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof InterstitialTriggerContext)) {
            return false;
        }
        InterstitialTriggerContext interstitialTriggerContext = (InterstitialTriggerContext) obj;
        ImmutableMap immutableMap = this.A00;
        if (immutableMap == null || !immutableMap.equals(interstitialTriggerContext.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return AnonymousClass07K.A00(this.A00);
    }

    public String toString() {
        return this.A00.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeMap(this.A00);
    }

    public InterstitialTriggerContext() {
        this.A00 = RegularImmutableMap.A03;
    }

    public InterstitialTriggerContext(Parcel parcel) {
        this.A00 = ImmutableMap.copyOf(parcel.readHashMap(HashMap.class.getClassLoader()));
    }

    public InterstitialTriggerContext(Map map) {
        this.A00 = ImmutableMap.copyOf(map);
    }
}
