package com.facebook.profilo.multiprocess;

import X.AnonymousClass08A;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class ProfiloIPCParcelable implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass08A();
    public final IBinder A00;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStrongBinder(this.A00);
    }

    public ProfiloIPCParcelable(IBinder iBinder) {
        this.A00 = iBinder;
    }

    public ProfiloIPCParcelable(Parcel parcel) {
        this.A00 = parcel.readStrongBinder();
    }
}
