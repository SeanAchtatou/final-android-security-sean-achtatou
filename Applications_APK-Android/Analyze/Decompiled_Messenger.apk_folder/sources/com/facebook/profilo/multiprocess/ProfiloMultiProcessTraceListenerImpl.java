package com.facebook.profilo.multiprocess;

import X.AnonymousClass054;
import X.AnonymousClass0S9;
import X.C000700l;
import X.C003904y;
import X.C004505k;
import X.C04110Rx;
import android.os.Binder;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener;
import com.facebook.profilo.ipc.IProfiloMultiProcessTraceService;
import com.facebook.profilo.ipc.TraceConfigData;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.writer.NativeTraceWriterCallbacks;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public final class ProfiloMultiProcessTraceListenerImpl extends IProfiloMultiProcessTraceListener.Stub implements NativeTraceWriterCallbacks, C003904y {
    private IProfiloMultiProcessTraceService A00;
    public final HashMap A01 = new HashMap();

    private synchronized void A01() {
        int A03 = C000700l.A03(69579572);
        this.A00 = null;
        C000700l.A09(-235434113, A03);
    }

    public ProfiloMultiProcessTraceListenerImpl() {
        int A03 = C000700l.A03(-1288247727);
        C000700l.A09(1858616048, A03);
    }

    private boolean A02() {
        int A03 = C000700l.A03(1197914342);
        boolean z = false;
        if (Binder.getCallingUid() == Process.myUid()) {
            z = true;
        }
        if (!z) {
            Log.e("ProfiloMultiProcessTraceListenerImpl", "UID of caller is different from UID of listener");
        }
        C000700l.A09(-547652694, A03);
        return z;
    }

    public void BTF(TraceConfigData traceConfigData) {
        int A03 = C000700l.A03(-1703065419);
        C004505k A002 = C004505k.A00();
        AnonymousClass0S9 r2 = new AnonymousClass0S9(traceConfigData);
        A002.A09.BgU();
        synchronized (A002) {
            AnonymousClass054 r0 = AnonymousClass054.A07;
            if (r0 != null) {
                int i = r0.A02.get();
                boolean z = false;
                if (i != 0) {
                    z = true;
                }
                if (z) {
                    A002.A02 = A002.A01;
                }
            }
            C004505k.A04(A002, r2);
            A002.A09.BTJ();
        }
        C000700l.A09(1751344673, A03);
    }

    public void Bds(Throwable th) {
        C000700l.A09(-295160527, C000700l.A03(-1544670502));
    }

    public void Bl2(IProfiloMultiProcessTraceService iProfiloMultiProcessTraceService) {
        int A03 = C000700l.A03(-992970326);
        if (!A02()) {
            C000700l.A09(-414715261, A03);
            return;
        }
        synchronized (this) {
            try {
                this.A00 = iProfiloMultiProcessTraceService;
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(-1188091436, A03);
                    throw th;
                }
            }
        }
        try {
            synchronized (this) {
                this.A00.C0a(this);
            }
        } catch (RemoteException unused) {
            A01();
        } catch (Throwable th2) {
            C000700l.A09(-743416045, A03);
            throw th2;
        }
        C000700l.A09(-1144711224, A03);
    }

    public void CMr(long j) {
        Long valueOf;
        CountDownLatch countDownLatch;
        int A03 = C000700l.A03(1290806219);
        if (!A02()) {
            C000700l.A09(1930330186, A03);
            return;
        }
        synchronized (this.A01) {
            try {
                HashMap hashMap = this.A01;
                valueOf = Long.valueOf(j);
                countDownLatch = (CountDownLatch) hashMap.get(valueOf);
            } finally {
                while (true) {
                    C000700l.A09(-303834439, A03);
                }
            }
        }
        if (countDownLatch == null) {
            C000700l.A09(-1786122101, A03);
            return;
        }
        try {
            countDownLatch.await(5, TimeUnit.MINUTES);
        } catch (Exception unused) {
        }
        synchronized (this.A01) {
            try {
                this.A01.remove(valueOf);
            } catch (Throwable th) {
                while (true) {
                    throw th;
                }
            }
        }
        C000700l.A09(1508858733, A03);
    }

    public void onTraceAbort(TraceContext traceContext) {
        int A03 = C000700l.A03(-1487147206);
        if (!A02()) {
            C000700l.A09(-1773653082, A03);
            return;
        }
        AnonymousClass054 r3 = AnonymousClass054.A07;
        if (r3 == null) {
            C000700l.A09(-181490466, A03);
            return;
        }
        AnonymousClass054.A05(r3, C04110Rx.A00, Long.valueOf(traceContext.A05), 0, traceContext.A04, 2);
        synchronized (this.A01) {
            try {
                this.A01.remove(Long.valueOf(traceContext.A05));
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(-1050354720, A03);
                    throw th;
                }
            }
        }
        C000700l.A09(1140658230, A03);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00ab, code lost:
        X.C000700l.A09(-1630590431, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b1, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onTraceStart(com.facebook.profilo.ipc.TraceContext r29) {
        /*
            r28 = this;
            r0 = 96175303(0x5bb84c7, float:1.763417E-35)
            int r12 = X.C000700l.A03(r0)
            r14 = r28
            boolean r0 = r14.A02()
            if (r0 != 0) goto L_0x0016
            r0 = -1228103772(0xffffffffb6cc9fa4, float:-6.0982584E-6)
            X.C000700l.A09(r0, r12)
            return
        L_0x0016:
            X.054 r11 = X.AnonymousClass054.A07
            if (r11 != 0) goto L_0x0021
            r0 = -1199604506(0xffffffffb87f7ce6, float:-6.091306E-5)
            X.C000700l.A09(r0, r12)
            return
        L_0x0021:
            java.util.HashMap r10 = r14.A01
            monitor-enter(r10)
            r13 = r29
            long r0 = r13.A05     // Catch:{ all -> 0x00b2 }
            java.lang.Long r1 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x00b2 }
            r13.A07 = r1     // Catch:{ all -> 0x00b2 }
            java.util.HashMap r0 = r14.A01     // Catch:{ all -> 0x00b2 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x00b2 }
            if (r0 != 0) goto L_0x00aa
            int r9 = X.C04110Rx.A00     // Catch:{ all -> 0x00b2 }
            int r8 = r13.A03     // Catch:{ all -> 0x00b2 }
            android.util.SparseArray r0 = r11.A01     // Catch:{ all -> 0x00b2 }
            java.lang.Object r7 = r0.get(r9)     // Catch:{ all -> 0x00b2 }
            X.04j r7 = (X.AnonymousClass04j) r7     // Catch:{ all -> 0x00b2 }
            if (r7 == 0) goto L_0x008d
            java.util.concurrent.atomic.AtomicReference r0 = r11.A03     // Catch:{ all -> 0x00b2 }
            java.lang.Object r6 = r0.get()     // Catch:{ all -> 0x00b2 }
            X.05d r6 = (X.AnonymousClass05d) r6     // Catch:{ all -> 0x00b2 }
            if (r6 != 0) goto L_0x004f
            goto L_0x0082
        L_0x004f:
            com.facebook.profilo.ipc.TraceContext r5 = new com.facebook.profilo.ipc.TraceContext     // Catch:{ all -> 0x00b2 }
            long r3 = r13.A05     // Catch:{ all -> 0x00b2 }
            java.lang.String r0 = r13.A09     // Catch:{ all -> 0x00b2 }
            r18 = r0
            java.lang.Object r0 = r13.A07     // Catch:{ all -> 0x00b2 }
            r21 = r0
            long r1 = r13.A04     // Catch:{ all -> 0x00b2 }
            int r0 = r13.A02     // Catch:{ all -> 0x00b2 }
            r17 = r0
            int r0 = r13.A03     // Catch:{ all -> 0x00b2 }
            r16 = r0
            int r15 = r13.A00     // Catch:{ all -> 0x00b2 }
            com.facebook.profilo.ipc.TraceContext$TraceConfigExtras r0 = r13.A06     // Catch:{ all -> 0x00b2 }
            r26 = r15
            r27 = r0
            r22 = r1
            r24 = r17
            r25 = r16
            r19 = r9
            r20 = r7
            r15 = r5
            r16 = r3
            r15.<init>(r16, r18, r19, r20, r21, r22, r24, r25, r26, r27)     // Catch:{ all -> 0x00b2 }
            boolean r0 = X.AnonymousClass054.A04(r11, r8, r5, r6)     // Catch:{ all -> 0x00b2 }
            goto L_0x0083
        L_0x0082:
            r0 = 0
        L_0x0083:
            if (r0 != 0) goto L_0x0099
            monitor-exit(r10)     // Catch:{ all -> 0x00b2 }
            r0 = -2115359557(0xffffffff81ea2cbb, float:-8.602221E-38)
            X.C000700l.A09(r0, r12)
            return
        L_0x008d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00b2 }
            java.lang.String r0 = "Unregistered controller for id = "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r9)     // Catch:{ all -> 0x00b2 }
            r1.<init>(r0)     // Catch:{ all -> 0x00b2 }
            throw r1     // Catch:{ all -> 0x00b2 }
        L_0x0099:
            java.util.HashMap r3 = r14.A01     // Catch:{ all -> 0x00b2 }
            long r0 = r13.A05     // Catch:{ all -> 0x00b2 }
            java.lang.Long r2 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.CountDownLatch r1 = new java.util.concurrent.CountDownLatch     // Catch:{ all -> 0x00b2 }
            r0 = 1
            r1.<init>(r0)     // Catch:{ all -> 0x00b2 }
            r3.put(r2, r1)     // Catch:{ all -> 0x00b2 }
        L_0x00aa:
            monitor-exit(r10)     // Catch:{ all -> 0x00b2 }
            r0 = -1630590431(0xffffffff9ecf2a21, float:-2.1934402E-20)
            X.C000700l.A09(r0, r12)
            return
        L_0x00b2:
            r1 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00b2 }
            r0 = 502393630(0x1df1eb1e, float:6.4035274E-21)
            X.C000700l.A09(r0, r12)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.multiprocess.ProfiloMultiProcessTraceListenerImpl.onTraceStart(com.facebook.profilo.ipc.TraceContext):void");
    }

    public void onTraceStop(TraceContext traceContext) {
        int A03 = C000700l.A03(-1949919233);
        if (!A02()) {
            C000700l.A09(1377863948, A03);
            return;
        }
        AnonymousClass054 r5 = AnonymousClass054.A07;
        if (r5 == null) {
            C000700l.A09(2030762139, A03);
            return;
        }
        if (!r5.A0B(C04110Rx.A00, Long.valueOf(traceContext.A05), traceContext.A04)) {
            synchronized (this.A01) {
                try {
                    this.A01.remove(Long.valueOf(traceContext.A05));
                } finally {
                    C000700l.A09(1197349285, A03);
                }
            }
        }
    }

    public void onTraceWriteAbort(long j, int i) {
        int A03 = C000700l.A03(-223102228);
        synchronized (this.A01) {
            try {
                HashMap hashMap = this.A01;
                Long valueOf = Long.valueOf(j);
                CountDownLatch countDownLatch = (CountDownLatch) hashMap.get(valueOf);
                if (countDownLatch != null) {
                    countDownLatch.countDown();
                    this.A01.remove(valueOf);
                }
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(1090392525, A03);
                    throw th;
                }
            }
        }
        try {
            synchronized (this) {
                IProfiloMultiProcessTraceService iProfiloMultiProcessTraceService = this.A00;
                if (iProfiloMultiProcessTraceService == null) {
                    C000700l.A09(-687786239, A03);
                    return;
                }
                iProfiloMultiProcessTraceService.BsY(j, i);
            }
        } catch (RemoteException unused) {
            A01();
        } catch (Throwable th2) {
            C000700l.A09(-902001980, A03);
            throw th2;
        }
        C000700l.A09(-1171929524, A03);
    }

    public void onTraceWriteEnd(long j, int i) {
        CountDownLatch countDownLatch;
        int A03 = C000700l.A03(1949299043);
        int A032 = C000700l.A03(-2121771009);
        synchronized (this.A01) {
            try {
                countDownLatch = (CountDownLatch) this.A01.get(Long.valueOf(j));
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(-1039508583, A032);
                    throw th;
                }
            }
        }
        if (countDownLatch == null) {
            C000700l.A09(1957562891, A032);
        } else {
            countDownLatch.countDown();
            C000700l.A09(795111174, A032);
        }
        C000700l.A09(69623980, A03);
    }

    public void onTraceWriteStart(long j, int i, String str) {
        C000700l.A09(1249966188, C000700l.A03(880530637));
    }
}
