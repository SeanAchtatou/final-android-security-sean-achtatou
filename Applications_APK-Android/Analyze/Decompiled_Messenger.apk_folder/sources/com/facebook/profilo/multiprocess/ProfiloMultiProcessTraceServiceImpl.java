package com.facebook.profilo.multiprocess;

import X.AnonymousClass05d;
import X.C000700l;
import X.C004505k;
import X.C004605l;
import android.os.Binder;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener;
import com.facebook.profilo.ipc.IProfiloMultiProcessTraceService;
import com.facebook.profilo.ipc.TraceConfigData;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;

public final class ProfiloMultiProcessTraceServiceImpl extends IProfiloMultiProcessTraceService.Stub implements C004605l {
    private final SparseArray A00 = new SparseArray(0);

    public ProfiloMultiProcessTraceServiceImpl() {
        int A03 = C000700l.A03(-285691361);
        C000700l.A09(-1011155476, A03);
    }

    public static TraceConfigData A00() {
        AnonymousClass05d r5 = C004505k.A00().A00;
        if (r5 == null) {
            return null;
        }
        return new TraceConfigData(r5.Ai2(), r5.AiX().B6p(), r5.AiX().B6I());
    }

    public static void A01(ProfiloMultiProcessTraceServiceImpl profiloMultiProcessTraceServiceImpl, int i, TraceContext traceContext, TraceConfigData traceConfigData) {
        SparseArray clone;
        int A03 = C000700l.A03(438940586);
        synchronized (profiloMultiProcessTraceServiceImpl.A00) {
            try {
                clone = profiloMultiProcessTraceServiceImpl.A00.clone();
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(-356207578, A03);
                    throw th;
                }
            }
        }
        int size = clone.size();
        for (int i2 = 0; i2 < size; i2++) {
            IProfiloMultiProcessTraceListener iProfiloMultiProcessTraceListener = (IProfiloMultiProcessTraceListener) clone.valueAt(i2);
            if (iProfiloMultiProcessTraceListener != null) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 3) {
                                if (i == 4 && traceConfigData != null) {
                                    try {
                                        iProfiloMultiProcessTraceListener.BTF(traceConfigData);
                                    } catch (RemoteException unused) {
                                        synchronized (profiloMultiProcessTraceServiceImpl.A00) {
                                            profiloMultiProcessTraceServiceImpl.A00.remove(clone.keyAt(i2));
                                        }
                                    } catch (Throwable th2) {
                                        C000700l.A09(-1287638191, A03);
                                        throw th2;
                                    }
                                }
                            } else if (traceContext != null) {
                                iProfiloMultiProcessTraceListener.CMr(traceContext.A05);
                            }
                        } else if (traceContext != null) {
                            iProfiloMultiProcessTraceListener.onTraceAbort(traceContext);
                        }
                    } else if (traceContext != null) {
                        iProfiloMultiProcessTraceListener.onTraceStop(traceContext);
                    }
                } else if (traceContext != null) {
                    iProfiloMultiProcessTraceListener.onTraceStart(traceContext);
                }
            }
        }
        C000700l.A09(1169506940, A03);
    }

    private boolean A02() {
        int A03 = C000700l.A03(-1944644427);
        boolean z = false;
        if (Binder.getCallingUid() == Process.myUid()) {
            z = true;
        }
        if (!z) {
            Log.e("ProfiloMultiProcessTraceServiceImpl", "UID of caller is different from UID of listener");
        }
        C000700l.A09(-2104514617, A03);
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        r6 = X.AnonymousClass054.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        if (r6 != null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        X.C000700l.A09(520329523, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        r5 = r6.A08().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004c, code lost:
        if (r5.hasNext() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004e, code lost:
        r4 = (com.facebook.profilo.ipc.TraceContext) r5.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0058, code lost:
        if (r4.A05 != r14) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005a, code lost:
        X.AnonymousClass054.A05(r6, r4.A01, r4.A07, 0, r4.A04, r16 | Integer.MIN_VALUE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0068, code lost:
        X.C000700l.A09(97671473, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BsY(long r14, int r16) {
        /*
            r13 = this;
            r0 = 1936890469(0x73729a65, float:1.9220998E31)
            int r2 = X.C000700l.A03(r0)
            boolean r0 = r13.A02()
            if (r0 != 0) goto L_0x0014
            r0 = 1154176488(0x44cb55e8, float:1626.6846)
            X.C000700l.A09(r0, r2)
            return
        L_0x0014:
            int r1 = android.os.Binder.getCallingPid()
            android.util.SparseArray r3 = r13.A00
            monitor-enter(r3)
            android.util.SparseArray r0 = r13.A00     // Catch:{ all -> 0x006f }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x006f }
            com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener r0 = (com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener) r0     // Catch:{ all -> 0x006f }
            if (r0 != 0) goto L_0x0034
            java.lang.String r1 = "ProfiloMultiProcessTraceServiceImpl"
            java.lang.String r0 = "Unknown listener sent trace abort in secondary"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x006f }
            monitor-exit(r3)     // Catch:{ all -> 0x006f }
            r0 = -773353956(0xffffffffd1e78e1c, float:-1.24315206E11)
            X.C000700l.A09(r0, r2)
            return
        L_0x0034:
            monitor-exit(r3)     // Catch:{ all -> 0x006f }
            X.054 r6 = X.AnonymousClass054.A07
            if (r6 != 0) goto L_0x0040
            r0 = 520329523(0x1f039933, float:2.7867052E-20)
            X.C000700l.A09(r0, r2)
            return
        L_0x0040:
            java.util.List r0 = r6.A08()
            java.util.Iterator r5 = r0.iterator()
        L_0x0048:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0068
            java.lang.Object r4 = r5.next()
            com.facebook.profilo.ipc.TraceContext r4 = (com.facebook.profilo.ipc.TraceContext) r4
            long r0 = r4.A05
            int r3 = (r0 > r14 ? 1 : (r0 == r14 ? 0 : -1))
            if (r3 != 0) goto L_0x0048
            int r7 = r4.A01
            java.lang.Object r8 = r4.A07
            long r10 = r4.A04
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r12 = r16 | r0
            r9 = 0
            X.AnonymousClass054.A05(r6, r7, r8, r9, r10, r12)
        L_0x0068:
            r0 = 97671473(0x5d25931, float:1.9781069E-35)
            X.C000700l.A09(r0, r2)
            return
        L_0x006f:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x006f }
            r0 = -1332906130(0xffffffffb08d776e, float:-1.0293044E-9)
            X.C000700l.A09(r0, r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.multiprocess.ProfiloMultiProcessTraceServiceImpl.BsY(long, int):void");
    }

    public void Bsc(TraceContext traceContext) {
        int A03 = C000700l.A03(1254792233);
        A01(this, 0, traceContext, null);
        C000700l.A09(749827866, A03);
    }

    public void Bsd(TraceContext traceContext) {
        C000700l.A09(476565859, C000700l.A03(-1014289658));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
        r0 = A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (r0 == null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r6.BTF(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003d, code lost:
        monitor-enter(r5.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r5.A00.remove(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0045, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0047, code lost:
        X.C000700l.A09(-1490498473, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004d, code lost:
        throw r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void C0a(com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener r6) {
        /*
            r5 = this;
            r0 = 805729410(0x30067482, float:4.8914484E-10)
            int r3 = X.C000700l.A03(r0)
            boolean r0 = r5.A02()
            if (r0 != 0) goto L_0x0014
            r0 = -379711468(0xffffffffe95e1014, float:-1.6778591E25)
            X.C000700l.A09(r0, r3)
            return
        L_0x0014:
            int r4 = android.os.Binder.getCallingPid()
            android.util.SparseArray r2 = r5.A00
            monitor-enter(r2)
            android.util.SparseArray r0 = r5.A00     // Catch:{ all -> 0x0091 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0091 }
            if (r0 == 0) goto L_0x002b
            monitor-exit(r2)     // Catch:{ all -> 0x0091 }
            r0 = 1405122721(0x53c078a1, float:1.65331508E12)
            X.C000700l.A09(r0, r3)
            return
        L_0x002b:
            android.util.SparseArray r0 = r5.A00     // Catch:{ all -> 0x0091 }
            r0.put(r4, r6)     // Catch:{ all -> 0x0091 }
            monitor-exit(r2)     // Catch:{ all -> 0x0091 }
            com.facebook.profilo.ipc.TraceConfigData r0 = A00()
            if (r0 == 0) goto L_0x004e
            r6.BTF(r0)     // Catch:{ RemoteException -> 0x003b }
            goto L_0x004e
        L_0x003b:
            android.util.SparseArray r2 = r5.A00
            monitor-enter(r2)
            android.util.SparseArray r0 = r5.A00     // Catch:{ all -> 0x0045 }
            r0.remove(r4)     // Catch:{ all -> 0x0045 }
            monitor-exit(r2)     // Catch:{ all -> 0x0045 }
            goto L_0x004e
        L_0x0045:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0045 }
            r0 = -1490498473(0xffffffffa728cc57, float:-2.3425456E-15)
            X.C000700l.A09(r0, r3)
            throw r1
        L_0x004e:
            X.054 r0 = X.AnonymousClass054.A07
            if (r0 != 0) goto L_0x0059
            r0 = -165993526(0xfffffffff61b23ca, float:-7.8665225E32)
            X.C000700l.A09(r0, r3)
            return
        L_0x0059:
            java.util.List r1 = r0.A08()
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x008a
            java.util.Iterator r1 = r1.iterator()
        L_0x0067:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x008a
            java.lang.Object r0 = r1.next()
            com.facebook.profilo.ipc.TraceContext r0 = (com.facebook.profilo.ipc.TraceContext) r0
            r6.onTraceStart(r0)     // Catch:{ RemoteException -> 0x0077 }
            goto L_0x0067
        L_0x0077:
            android.util.SparseArray r2 = r5.A00
            monitor-enter(r2)
            android.util.SparseArray r0 = r5.A00     // Catch:{ all -> 0x0081 }
            r0.remove(r4)     // Catch:{ all -> 0x0081 }
            monitor-exit(r2)     // Catch:{ all -> 0x0081 }
            goto L_0x0067
        L_0x0081:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0081 }
            r0 = -1685209922(0xffffffff9b8dbcbe, float:-2.3448465E-22)
            X.C000700l.A09(r0, r3)
            throw r1
        L_0x008a:
            r0 = -311171583(0xffffffffed73e601, float:-4.7176822E27)
            X.C000700l.A09(r0, r3)
            return
        L_0x0091:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0091 }
            r0 = 1276855034(0x4c1b42fa, float:4.0700904E7)
            X.C000700l.A09(r0, r3)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.multiprocess.ProfiloMultiProcessTraceServiceImpl.C0a(com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener):void");
    }

    public void onTraceAbort(TraceContext traceContext) {
        int A03 = C000700l.A03(704364162);
        A01(this, 2, traceContext, null);
        C000700l.A09(-2037965405, A03);
    }

    public void onTraceStop(TraceContext traceContext) {
        int size;
        int A03 = C000700l.A03(-790101250);
        TraceContext traceContext2 = traceContext;
        A01(this, 1, traceContext2, null);
        A01(this, 3, traceContext2, null);
        synchronized (this.A00) {
            try {
                size = this.A00.size();
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(-1040840966, A03);
                    throw th;
                }
            }
        }
        Logger.writeBytesEntry(0, 1, 57, Logger.writeBytesEntry(0, 1, 56, Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126500, 0, 0), "num_connected_processes"), String.valueOf(size));
        C000700l.A09(-273491228, A03);
    }
}
