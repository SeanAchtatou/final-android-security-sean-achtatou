package com.facebook.profilo.config.v2;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;

public class ConfigParser {
    private final HybridData mHybridData;

    private static native HybridData initHybrid(String str);

    public native Config parseConfig();

    static {
        AnonymousClass01q.A08("profilo_configjni");
    }

    public ConfigParser(String str) {
        this.mHybridData = initHybrid(str);
    }
}
