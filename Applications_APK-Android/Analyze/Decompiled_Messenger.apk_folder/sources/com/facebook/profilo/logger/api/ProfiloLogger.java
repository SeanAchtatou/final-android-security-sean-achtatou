package com.facebook.profilo.logger.api;

import X.AnonymousClass00n;
import com.facebook.common.dextricks.classid.ClassId;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;

public final class ProfiloLogger {
    public static volatile boolean sHasProfilo;

    public static int asyncCallEnd(int i, int i2) {
        if (!sHasProfilo) {
            return -1;
        }
        return Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 15, 0, 0, i2, i, 0);
    }

    public static int classLoadEnd(Class cls) {
        if (!sHasProfilo || !TraceEvents.isEnabled(AnonymousClass00n.A02)) {
            return -1;
        }
        int i = AnonymousClass00n.A02;
        if (!ClassId.sInitialized) {
            return -1;
        }
        return Logger.writeStandardEntry(i, 6, 81, 0, 0, 0, 0, ClassId.getClassId(cls));
    }

    public static int classLoadFailed() {
        if (!sHasProfilo || !TraceEvents.isEnabled(AnonymousClass00n.A02)) {
            return -1;
        }
        int i = AnonymousClass00n.A02;
        if (!ClassId.sInitialized) {
            return -1;
        }
        return Logger.writeStandardEntry(i, 6, 82, 0, 0, 0, 0, 0);
    }

    public static int classLoadStart() {
        if (!sHasProfilo || !TraceEvents.isEnabled(AnonymousClass00n.A02)) {
            return -1;
        }
        int i = AnonymousClass00n.A02;
        if (!ClassId.sInitialized) {
            return -1;
        }
        return Logger.writeStandardEntry(i, 6, 80, 0, 0, 0, 0, 0);
    }

    public static int waitEnd(int i, int i2) {
        if (!sHasProfilo) {
            return -1;
        }
        return Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 54, 0, 0, i2, i, 0);
    }
}
