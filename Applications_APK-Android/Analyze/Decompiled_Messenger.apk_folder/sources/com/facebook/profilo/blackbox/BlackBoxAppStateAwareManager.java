package com.facebook.profilo.blackbox;

import X.AnonymousClass04v;
import X.AnonymousClass050;
import X.AnonymousClass054;
import X.AnonymousClass072;
import X.AnonymousClass0WD;
import X.AnonymousClass1XY;
import X.C003704n;
import android.util.Log;
import com.facebook.profilo.ipc.TraceContext;
import javax.inject.Singleton;

@Singleton
public final class BlackBoxAppStateAwareManager extends AnonymousClass04v {
    private static volatile BlackBoxAppStateAwareManager A03;
    public volatile boolean A00;
    public volatile boolean A01;
    public volatile boolean A02 = false;

    public void BTJ() {
        this.A02 = true;
        if (!this.A00 || !A01()) {
            Log.w("Profilo/BlackBoxState", "Start after config update");
            AnonymousClass050.A03();
            return;
        }
        Log.w("Profilo/BlackBoxState", "Cannot start after config update: backgrounded");
        this.A01 = true;
    }

    public static final BlackBoxAppStateAwareManager A00(AnonymousClass1XY r3) {
        if (A03 == null) {
            synchronized (BlackBoxAppStateAwareManager.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A03 = new BlackBoxAppStateAwareManager();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static boolean A01() {
        AnonymousClass072 r0;
        AnonymousClass054 r2 = AnonymousClass054.A07;
        if (r2 == null || (r0 = (AnonymousClass072) r2.A06(C003704n.A01)) == null) {
            return false;
        }
        return r0.A06;
    }

    public void BgU() {
        Log.w("Profilo/BlackBoxState", "Abort on config update");
        this.A02 = false;
        AnonymousClass050.A02();
    }

    public void onTraceAbort(TraceContext traceContext) {
        if ((traceContext.A03 & 2) != 0 && traceContext.A00 != 2) {
            Log.w("Profilo/BlackBoxState", "Trace aborted implicitly. Restarting...");
            AnonymousClass050.A03();
        }
    }
}
