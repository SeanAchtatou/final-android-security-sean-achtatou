package com.facebook.profilo.blackbox.breakpad;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import com.facebook.profilo.writer.NativeTraceWriterCallbacks;

public class CrashDumpTraceWriter {
    private final NativeTraceWriterCallbacks mCallbacks;
    private final HybridData mHybridData;

    private static native HybridData initHybrid(String str, String str2, NativeTraceWriterCallbacks nativeTraceWriterCallbacks);

    public native void nativeWriteTrace(String str, long j, int i);

    static {
        AnonymousClass01q.A08("profilo_crash_dump_writer");
    }

    public CrashDumpTraceWriter(String str, String str2, NativeTraceWriterCallbacks nativeTraceWriterCallbacks) {
        this.mCallbacks = nativeTraceWriterCallbacks;
        this.mHybridData = initHybrid(str, str2, nativeTraceWriterCallbacks);
    }
}
