package com.facebook.profilo.blackbox.breakpad;

import X.AnonymousClass01q;
import X.AnonymousClass04v;
import X.AnonymousClass0PD;
import X.AnonymousClass0UN;
import X.AnonymousClass1XX;
import X.AnonymousClass1XY;
import X.AnonymousClass1Y3;
import X.C003704n;
import X.C004505k;
import com.facebook.common.build.BuildConstants;
import com.facebook.profilo.ipc.TraceContext;
import java.io.File;
import java.io.IOException;

public class BreakpadTraceListener extends AnonymousClass04v {
    private static boolean sInitialized;
    private AnonymousClass0UN $ul_mInjectionContext;

    private static native void nativeOnTraceStart(String str, String str2, String str3, long j, long j2, int i);

    private static native void nativeOnTraceStop();

    public static final BreakpadTraceListener $ul_$xXXcom_facebook_profilo_blackbox_breakpad_BreakpadTraceListener$xXXFACTORY_METHOD(AnonymousClass1XY r1) {
        return new BreakpadTraceListener(r1);
    }

    private static synchronized void ensureLibInitialized() {
        synchronized (BreakpadTraceListener.class) {
            if (!sInitialized) {
                AnonymousClass01q.A08("profilo_breakpad");
                sInitialized = true;
            }
        }
    }

    public void onTraceAbort(TraceContext traceContext) {
        if (traceContext.A01 == C003704n.A01 && traceContext.A04 == 15859716) {
            ensureLibInitialized();
            nativeOnTraceStop();
        }
    }

    public void onTraceStart(TraceContext traceContext) {
        if (traceContext.A01 == C003704n.A01 && traceContext.A04 == 15859716) {
            ensureLibInitialized();
            long j = traceContext.A05;
            File A01 = AnonymousClass0PD.A01((AnonymousClass0PD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B7V, this.$ul_mInjectionContext));
            String[] strArr = null;
            if (A01 != null) {
                try {
                    StringBuilder sb = new StringBuilder(A01.getCanonicalPath());
                    sb.append(File.separatorChar);
                    sb.append(j);
                    sb.append(".pdmp");
                    String sb2 = sb.toString();
                    sb.append("_tmp");
                    strArr = new String[]{sb.toString(), sb2};
                } catch (IOException unused) {
                }
            }
            if (strArr != null) {
                long j2 = 0;
                boolean z = false;
                if (C004505k.A0F.get() != null) {
                    z = true;
                }
                if (z) {
                    j2 = C004505k.A00().A00.Ai2();
                }
                nativeOnTraceStart(traceContext.A09, strArr[0], strArr[1], traceContext.A05, j2, BuildConstants.A00());
            }
        }
    }

    public void onTraceStop(TraceContext traceContext) {
        if (traceContext.A01 == C003704n.A01 && traceContext.A04 == 15859716) {
            ensureLibInitialized();
            nativeOnTraceStop();
        }
    }

    public BreakpadTraceListener(AnonymousClass1XY r3) {
        this.$ul_mInjectionContext = new AnonymousClass0UN(1, r3);
    }
}
