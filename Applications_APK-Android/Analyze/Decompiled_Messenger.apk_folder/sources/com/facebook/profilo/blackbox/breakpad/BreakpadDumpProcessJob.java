package com.facebook.profilo.blackbox.breakpad;

import X.AnonymousClass0UN;
import X.AnonymousClass1XY;

public final class BreakpadDumpProcessJob {
    private AnonymousClass0UN A00;

    public static final BreakpadDumpProcessJob A00(AnonymousClass1XY r1) {
        return new BreakpadDumpProcessJob(r1);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:68:0x00b6 */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: java.util.List} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:76:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r11 = this;
            java.lang.Class<com.facebook.profilo.blackbox.breakpad.BreakpadDumpProcessJob> r6 = com.facebook.profilo.blackbox.breakpad.BreakpadDumpProcessJob.class
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r11.A00
            r2 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 667(0x29b, float:9.35E-43)
            boolean r0 = r1.AbO(r0, r2)
            if (r0 == 0) goto L_0x0129
            java.util.concurrent.atomic.AtomicReference r0 = X.C004505k.A0F
            java.lang.Object r1 = r0.get()
            r0 = 0
            if (r1 == 0) goto L_0x001f
            r0 = 1
        L_0x001f:
            if (r0 == 0) goto L_0x0129
            r2 = 1
            int r1 = X.AnonymousClass1Y3.B7V
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0PD r0 = (X.AnonymousClass0PD) r0
            java.io.File r4 = X.AnonymousClass0PD.A01(r0)
            if (r4 == 0) goto L_0x00b2
            java.io.File r1 = X.AnonymousClass0PD.A01(r0)
            if (r1 == 0) goto L_0x0057
            X.0PC r0 = new X.0PC
            r0.<init>()
            java.io.File[] r3 = r1.listFiles(r0)
            if (r3 == 0) goto L_0x0057
            int r2 = r3.length
            r0 = 2
            if (r2 < r0) goto L_0x0057
            java.util.Comparator r0 = X.AnonymousClass0PD.A02
            java.util.Arrays.sort(r3, r0)
            r1 = 1
        L_0x004d:
            if (r1 >= r2) goto L_0x0057
            r0 = r3[r1]
            r0.delete()
            int r1 = r1 + 1
            goto L_0x004d
        L_0x0057:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            X.0PB r0 = new X.0PB
            r0.<init>()
            java.io.File[] r10 = r4.listFiles(r0)
            if (r10 == 0) goto L_0x00b2
            int r9 = r10.length
            r8 = 0
        L_0x0069:
            if (r8 >= r9) goto L_0x008d
            r7 = r10[r8]
            java.lang.String r1 = r7.getName()
            java.lang.String r0 = ".pdmp"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x0089
            long r4 = r7.length()
            r1 = 0
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0089
            r3.add(r7)
        L_0x0086:
            int r8 = r8 + 1
            goto L_0x0069
        L_0x0089:
            r7.delete()
            goto L_0x0086
        L_0x008d:
            int r0 = r3.size()
            r2 = 5
            if (r0 <= r2) goto L_0x00b6
            java.util.Comparator r0 = X.AnonymousClass0PD.A02
            java.util.Collections.sort(r3, r0)
        L_0x0099:
            int r0 = r3.size()
            if (r0 <= r2) goto L_0x00b6
            int r0 = r3.size()
            int r1 = r0 + -1
            java.lang.Object r0 = r3.get(r1)
            java.io.File r0 = (java.io.File) r0
            r0.delete()
            r3.remove(r1)
            goto L_0x0099
        L_0x00b2:
            java.util.List r3 = java.util.Collections.emptyList()
        L_0x00b6:
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0129
            X.05k r2 = X.C004505k.A00()
            if (r2 != 0) goto L_0x00c8
            java.lang.String r0 = "TraceOrchestrator is not initialized"
            X.C010708t.A07(r6, r0)
            return
        L_0x00c8:
            com.facebook.profilo.blackbox.breakpad.CrashDumpTraceWriter r8 = new com.facebook.profilo.blackbox.breakpad.CrashDumpTraceWriter     // Catch:{ IOException -> 0x0123 }
            X.05m r0 = r2.A03     // Catch:{ IOException -> 0x0123 }
            java.io.File r0 = r0.A03     // Catch:{ IOException -> 0x0123 }
            java.lang.String r1 = r0.getCanonicalPath()     // Catch:{ IOException -> 0x0123 }
            java.lang.String r0 = r2.A0A     // Catch:{ IOException -> 0x0123 }
            r8.<init>(r1, r0, r2)     // Catch:{ IOException -> 0x0123 }
            java.util.Iterator r9 = r3.iterator()     // Catch:{ IOException -> 0x0123 }
        L_0x00db:
            boolean r0 = r9.hasNext()     // Catch:{ IOException -> 0x0123 }
            if (r0 == 0) goto L_0x0129
            java.lang.Object r7 = r9.next()     // Catch:{ IOException -> 0x0123 }
            java.io.File r7 = (java.io.File) r7     // Catch:{ IOException -> 0x0123 }
            java.lang.String r2 = r7.getName()     // Catch:{ IOException -> 0x0123 }
            java.lang.String r0 = ".pdmp"
            boolean r0 = r2.endsWith(r0)     // Catch:{ IOException -> 0x0123 }
            if (r0 != 0) goto L_0x00f6
            r2 = 0
            goto L_0x0108
        L_0x00f6:
            r1 = 0
            int r0 = r2.length()     // Catch:{ NumberFormatException -> 0x0106 }
            int r0 = r0 + -5
            java.lang.String r0 = r2.substring(r1, r0)     // Catch:{ NumberFormatException -> 0x0106 }
            long r2 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x0106 }
            goto L_0x0108
        L_0x0106:
            r2 = 0
        L_0x0108:
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x011f
            java.lang.String r1 = r7.getCanonicalPath()     // Catch:{ Exception -> 0x0119 }
            r0 = 15859716(0xf20004, float:2.2224196E-38)
            r8.nativeWriteTrace(r1, r2, r0)     // Catch:{ Exception -> 0x0119 }
            goto L_0x011f
        L_0x0119:
            r2 = move-exception
            java.lang.String r0 = "Exception while processing a native crash dump."
            X.C010708t.A0A(r6, r0, r2)     // Catch:{ IOException -> 0x0123 }
        L_0x011f:
            r7.delete()     // Catch:{ IOException -> 0x0123 }
            goto L_0x00db
        L_0x0123:
            r1 = move-exception
            java.lang.String r0 = "Unable to resolve trace folder path"
            X.C010708t.A0A(r6, r0, r1)
        L_0x0129:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.blackbox.breakpad.BreakpadDumpProcessJob.A01():void");
    }

    private BreakpadDumpProcessJob(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
