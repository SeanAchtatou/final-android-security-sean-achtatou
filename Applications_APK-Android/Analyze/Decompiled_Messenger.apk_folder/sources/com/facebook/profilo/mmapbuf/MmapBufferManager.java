package com.facebook.profilo.mmapbuf;

import X.AnonymousClass01q;
import X.AnonymousClass08S;
import android.content.Context;
import android.content.pm.PackageManager;
import com.facebook.jni.HybridData;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class MmapBufferManager {
    private AtomicBoolean mAllocated = new AtomicBoolean(false);
    private final long mConfigId;
    public final Context mContext;
    public AtomicBoolean mEnabled = new AtomicBoolean(false);
    private final File mFolder;
    private final HybridData mHybridData = initHybrid();
    public volatile String mMmapFileName;

    private static native HybridData initHybrid();

    private native boolean nativeAllocateBuffer(int i, String str, int i2, long j);

    public native void nativeDeallocateBuffer();

    public native void nativeUpdateHeader(int i, int i2, long j, long j2);

    public native void nativeUpdateSessionId(String str);

    static {
        AnonymousClass01q.A08("profilo_mmapbuf");
    }

    public boolean allocateBuffer(int i) {
        int i2;
        PackageManager packageManager;
        File file = this.mFolder;
        if (file == null) {
            throw new IllegalStateException("Mmap folder is not set");
        } else if (!this.mAllocated.compareAndSet(false, true) || (!file.exists() && !file.mkdirs())) {
            return false;
        } else {
            String A0J = AnonymousClass08S.A0J(UUID.randomUUID().toString(), ".buff");
            try {
                String A0P = AnonymousClass08S.A0P(file.getCanonicalPath(), File.separator, A0J);
                this.mMmapFileName = A0J;
                Context context = this.mContext;
                if (context == null || (packageManager = context.getPackageManager()) == null) {
                    i2 = 0;
                } else {
                    try {
                        i2 = packageManager.getPackageInfo(this.mContext.getPackageName(), 0).versionCode;
                    } catch (PackageManager.NameNotFoundException | RuntimeException unused) {
                        i2 = 0;
                    }
                }
                boolean nativeAllocateBuffer = nativeAllocateBuffer(i, A0P, i2, this.mConfigId);
                this.mEnabled.set(nativeAllocateBuffer);
                return nativeAllocateBuffer;
            } catch (IOException unused2) {
                return false;
            }
        }
    }

    public MmapBufferManager(long j, File file, Context context) {
        this.mConfigId = j;
        this.mFolder = file;
        this.mContext = context;
    }
}
