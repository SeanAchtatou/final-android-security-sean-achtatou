package com.facebook.profilo.mmapbuf.writer;

import X.AnonymousClass01q;
import com.facebook.jni.HybridData;
import com.facebook.profilo.writer.NativeTraceWriterCallbacks;

public class MmapBufferTraceWriter {
    private final NativeTraceWriterCallbacks mCallbacks;
    private final HybridData mHybridData;

    private static native HybridData initHybrid(String str, String str2, NativeTraceWriterCallbacks nativeTraceWriterCallbacks);

    public native void nativeWriteTrace(String str, int i);

    static {
        AnonymousClass01q.A08("profilo_mmap_file_writer");
    }

    public MmapBufferTraceWriter(String str, String str2, NativeTraceWriterCallbacks nativeTraceWriterCallbacks) {
        this.mCallbacks = nativeTraceWriterCallbacks;
        this.mHybridData = initHybrid(str, str2, nativeTraceWriterCallbacks);
    }
}
