package com.facebook.profilo.mmapbuf;

import X.AnonymousClass0UN;
import X.AnonymousClass1XY;

public final class MmapBufferProcessJob {
    private AnonymousClass0UN A00;

    public static final MmapBufferProcessJob A00(AnonymousClass1XY r1) {
        return new MmapBufferProcessJob(r1);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:85:0x00b5 */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: java.util.List} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        if (r2.exists() != false) goto L_0x004b;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:100:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r11 = this;
            java.lang.Class<com.facebook.profilo.mmapbuf.MmapBufferProcessJob> r5 = com.facebook.profilo.mmapbuf.MmapBufferProcessJob.class
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r11.A00
            r2 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 669(0x29d, float:9.37E-43)
            boolean r0 = r1.AbO(r0, r2)
            if (r0 == 0) goto L_0x0151
            java.util.concurrent.atomic.AtomicReference r0 = X.C004505k.A0F
            java.lang.Object r1 = r0.get()
            r0 = 0
            if (r1 == 0) goto L_0x001f
            r0 = 1
        L_0x001f:
            if (r0 == 0) goto L_0x0151
            X.0PO r1 = new X.0PO
            r1.<init>()
            java.io.File r2 = r1.A01
            if (r2 != 0) goto L_0x004b
            X.05m r0 = r1.A00
            if (r0 != 0) goto L_0x0038
            X.05k r0 = X.C004505k.A00()
            if (r0 == 0) goto L_0x0038
            X.05m r0 = r0.A03
            r1.A00 = r0
        L_0x0038:
            X.05m r0 = r1.A00
            r1 = 0
            if (r0 == 0) goto L_0x0084
            java.io.File r2 = r0.A05
            boolean r0 = r2.isDirectory()
            if (r0 == 0) goto L_0x0084
            boolean r0 = r2.exists()
            if (r0 == 0) goto L_0x0084
        L_0x004b:
            if (r2 == 0) goto L_0x00b1
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.io.File[] r10 = r2.listFiles()
            if (r10 == 0) goto L_0x00b1
            int r9 = r10.length
            r8 = 0
        L_0x005a:
            if (r8 >= r9) goto L_0x0086
            r3 = r10[r8]
            java.lang.String r1 = r3.getName()
            java.lang.String r0 = ".buff"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x007a
            long r6 = r3.length()
            r1 = 0
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x007a
            r4.add(r3)
        L_0x0077:
            int r8 = r8 + 1
            goto L_0x005a
        L_0x007a:
            boolean r0 = r3.delete()
            if (r0 != 0) goto L_0x0077
            r3.deleteOnExit()
            goto L_0x0077
        L_0x0084:
            r2 = r1
            goto L_0x004b
        L_0x0086:
            int r0 = r4.size()
            r3 = 5
            if (r0 <= r3) goto L_0x00b5
            java.util.Comparator r0 = X.AnonymousClass0PO.A02
            java.util.Collections.sort(r4, r0)
        L_0x0092:
            int r0 = r4.size()
            if (r0 <= r3) goto L_0x00b5
            int r0 = r4.size()
            int r2 = r0 + -1
            java.lang.Object r1 = r4.get(r2)
            java.io.File r1 = (java.io.File) r1
            boolean r0 = r1.delete()
            if (r0 != 0) goto L_0x00ad
            r1.deleteOnExit()
        L_0x00ad:
            r4.remove(r2)
            goto L_0x0092
        L_0x00b1:
            java.util.List r4 = java.util.Collections.emptyList()
        L_0x00b5:
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x0151
            X.05k r3 = X.C004505k.A00()
            if (r3 != 0) goto L_0x00c7
            java.lang.String r0 = "TraceOrchestrator is not initialized"
            X.C010708t.A07(r5, r0)
            return
        L_0x00c7:
            com.facebook.profilo.mmapbuf.MmapBufferManager r6 = r3.A04
            if (r6 == 0) goto L_0x00f2
            java.util.Iterator r2 = r4.iterator()
        L_0x00cf:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00f2
            java.lang.Object r1 = r2.next()
            java.io.File r1 = (java.io.File) r1
            java.util.concurrent.atomic.AtomicBoolean r0 = r6.mEnabled
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x00cf
            java.lang.String r1 = r1.getName()
            java.lang.String r0 = r6.mMmapFileName
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00cf
            r2.remove()
        L_0x00f2:
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r11.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 284026187288312(0x1025200000ef8, double:1.403275816584267E-309)
            boolean r7 = r2.Aem(r0)
            com.facebook.profilo.mmapbuf.writer.MmapBufferTraceWriter r6 = new com.facebook.profilo.mmapbuf.writer.MmapBufferTraceWriter     // Catch:{ IOException -> 0x014b }
            X.05m r0 = r3.A03     // Catch:{ IOException -> 0x014b }
            java.io.File r0 = r0.A03     // Catch:{ IOException -> 0x014b }
            java.lang.String r1 = r0.getCanonicalPath()     // Catch:{ IOException -> 0x014b }
            java.lang.String r0 = r3.A0A     // Catch:{ IOException -> 0x014b }
            r6.<init>(r1, r0, r3)     // Catch:{ IOException -> 0x014b }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ IOException -> 0x014b }
        L_0x0119:
            boolean r0 = r4.hasNext()     // Catch:{ IOException -> 0x014b }
            if (r0 == 0) goto L_0x0151
            java.lang.Object r3 = r4.next()     // Catch:{ IOException -> 0x014b }
            java.io.File r3 = (java.io.File) r3     // Catch:{ IOException -> 0x014b }
            if (r7 != 0) goto L_0x0141
            X.054 r0 = X.AnonymousClass054.A07     // Catch:{ IOException -> 0x014b }
            if (r0 != 0) goto L_0x0130
            java.lang.String r0 = "TraceControl is not initialized"
            X.C010708t.A07(r5, r0)     // Catch:{ IOException -> 0x014b }
        L_0x0130:
            java.lang.String r1 = r3.getCanonicalPath()     // Catch:{ Exception -> 0x013b }
            r0 = 8126539(0x7c004b, float:1.1387707E-38)
            r6.nativeWriteTrace(r1, r0)     // Catch:{ Exception -> 0x013b }
            goto L_0x0141
        L_0x013b:
            r1 = move-exception
            java.lang.String r0 = "Exception while processing a file-backed buffer."
            X.C010708t.A0A(r5, r0, r1)     // Catch:{ IOException -> 0x014b }
        L_0x0141:
            boolean r0 = r3.delete()     // Catch:{ IOException -> 0x014b }
            if (r0 != 0) goto L_0x0119
            r3.deleteOnExit()     // Catch:{ IOException -> 0x014b }
            goto L_0x0119
        L_0x014b:
            r1 = move-exception
            java.lang.String r0 = "Unable to resolve trace folder path"
            X.C010708t.A0A(r5, r0, r1)
        L_0x0151:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.mmapbuf.MmapBufferProcessJob.A01():void");
    }

    private MmapBufferProcessJob(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
