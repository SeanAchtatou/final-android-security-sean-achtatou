package com.facebook.profilo.ipc;

import X.C000700l;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public interface IProfiloMultiProcessTraceListener extends IInterface {

    public abstract class Stub extends Binder implements IProfiloMultiProcessTraceListener {

        public final class Proxy implements IProfiloMultiProcessTraceListener {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(-1543957421);
                this.A00 = iBinder;
                C000700l.A09(-1889045360, A03);
            }

            public void BTF(TraceConfigData traceConfigData) {
                int A03 = C000700l.A03(492212424);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
                    if (traceConfigData != null) {
                        obtain.writeInt(1);
                        traceConfigData.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1173589606, A03);
                }
            }

            public void Bl2(IProfiloMultiProcessTraceService iProfiloMultiProcessTraceService) {
                IBinder iBinder;
                int A03 = C000700l.A03(1362976392);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
                    if (iProfiloMultiProcessTraceService != null) {
                        iBinder = iProfiloMultiProcessTraceService.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-696220556, A03);
                }
            }

            public void CMr(long j) {
                int A03 = C000700l.A03(998603740);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
                    obtain.writeLong(j);
                    this.A00.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-187866108, A03);
                }
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(-257488506);
                IBinder iBinder = this.A00;
                C000700l.A09(110362939, A03);
                return iBinder;
            }

            public void onTraceAbort(TraceContext traceContext) {
                int A03 = C000700l.A03(1986484698);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
                    if (traceContext != null) {
                        obtain.writeInt(1);
                        traceContext.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(1267779512, A03);
                }
            }

            public void onTraceStart(TraceContext traceContext) {
                int A03 = C000700l.A03(-1597150464);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
                    if (traceContext != null) {
                        obtain.writeInt(1);
                        traceContext.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1368883312, A03);
                }
            }

            public void onTraceStop(TraceContext traceContext) {
                int A03 = C000700l.A03(-1403041515);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
                    if (traceContext != null) {
                        obtain.writeInt(1);
                        traceContext.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.A00.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(219451110, A03);
                }
            }
        }

        public static IProfiloMultiProcessTraceListener A00(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IProfiloMultiProcessTraceListener)) {
                return new Proxy(iBinder);
            }
            return (IProfiloMultiProcessTraceListener) queryLocalInterface;
        }

        public Stub() {
            int A03 = C000700l.A03(-1700600152);
            attachInterface(this, "com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener");
            C000700l.A09(752003360, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(2043779956, C000700l.A03(-2001458459));
            return this;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r1v1 */
        /* JADX WARN: Type inference failed for: r1v2, types: [com.facebook.profilo.ipc.IProfiloMultiProcessTraceService] */
        /* JADX WARN: Type inference failed for: r1v6, types: [com.facebook.profilo.ipc.TraceContext] */
        /* JADX WARN: Type inference failed for: r1v9, types: [com.facebook.profilo.ipc.TraceContext] */
        /* JADX WARN: Type inference failed for: r1v12, types: [com.facebook.profilo.ipc.TraceContext] */
        /* JADX WARN: Type inference failed for: r1v15, types: [com.facebook.profilo.ipc.TraceConfigData] */
        /* JADX WARN: Type inference failed for: r1v19 */
        /* JADX WARN: Type inference failed for: r1v20 */
        /* JADX WARN: Type inference failed for: r1v21 */
        /* JADX WARN: Type inference failed for: r1v22 */
        /* JADX WARN: Type inference failed for: r1v23 */
        /* JADX WARN: Type inference failed for: r1v24 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTransact(int r6, android.os.Parcel r7, android.os.Parcel r8, int r9) {
            /*
                r5 = this;
                r0 = -1498424839(0xffffffffa6afd9f9, float:-1.2202146E-15)
                int r2 = X.C000700l.A03(r0)
                r1 = 1598968902(0x5f4e5446, float:1.4867585E19)
                r4 = 1
                java.lang.String r0 = "com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener"
                if (r6 == r1) goto L_0x00d4
                r1 = 0
                switch(r6) {
                    case 1: goto L_0x00aa;
                    case 2: goto L_0x008c;
                    case 3: goto L_0x006e;
                    case 4: goto L_0x0050;
                    case 5: goto L_0x003c;
                    case 6: goto L_0x001e;
                    default: goto L_0x0013;
                }
            L_0x0013:
                boolean r1 = super.onTransact(r6, r7, r8, r9)
                r0 = 258023784(0xf612168, float:1.109979E-29)
                X.C000700l.A09(r0, r2)
                return r1
            L_0x001e:
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                if (r0 == 0) goto L_0x002f
                android.os.Parcelable$Creator r0 = com.facebook.profilo.ipc.TraceConfigData.CREATOR
                java.lang.Object r1 = r0.createFromParcel(r7)
                com.facebook.profilo.ipc.TraceConfigData r1 = (com.facebook.profilo.ipc.TraceConfigData) r1
            L_0x002f:
                r5.BTF(r1)
                r8.writeNoException()
                r0 = -1953110707(0xffffffff8b95e54d, float:-5.7737726E-32)
                X.C000700l.A09(r0, r2)
                return r4
            L_0x003c:
                r7.enforceInterface(r0)
                long r0 = r7.readLong()
                r5.CMr(r0)
                r8.writeNoException()
                r0 = 1226330111(0x49184fff, float:623871.94)
                X.C000700l.A09(r0, r2)
                return r4
            L_0x0050:
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                if (r0 == 0) goto L_0x0061
                android.os.Parcelable$Creator r0 = com.facebook.profilo.ipc.TraceContext.CREATOR
                java.lang.Object r1 = r0.createFromParcel(r7)
                com.facebook.profilo.ipc.TraceContext r1 = (com.facebook.profilo.ipc.TraceContext) r1
            L_0x0061:
                r5.onTraceAbort(r1)
                r8.writeNoException()
                r0 = 559449267(0x215884b3, float:7.3359273E-19)
                X.C000700l.A09(r0, r2)
                return r4
            L_0x006e:
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                if (r0 == 0) goto L_0x007f
                android.os.Parcelable$Creator r0 = com.facebook.profilo.ipc.TraceContext.CREATOR
                java.lang.Object r1 = r0.createFromParcel(r7)
                com.facebook.profilo.ipc.TraceContext r1 = (com.facebook.profilo.ipc.TraceContext) r1
            L_0x007f:
                r5.onTraceStop(r1)
                r8.writeNoException()
                r0 = -352606854(0xffffffffeafba57a, float:-1.5211091E26)
                X.C000700l.A09(r0, r2)
                return r4
            L_0x008c:
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                if (r0 == 0) goto L_0x009d
                android.os.Parcelable$Creator r0 = com.facebook.profilo.ipc.TraceContext.CREATOR
                java.lang.Object r1 = r0.createFromParcel(r7)
                com.facebook.profilo.ipc.TraceContext r1 = (com.facebook.profilo.ipc.TraceContext) r1
            L_0x009d:
                r5.onTraceStart(r1)
                r8.writeNoException()
                r0 = 1294838050(0x4d2da922, float:1.82096416E8)
                X.C000700l.A09(r0, r2)
                return r4
            L_0x00aa:
                r7.enforceInterface(r0)
                android.os.IBinder r3 = r7.readStrongBinder()
                if (r3 == 0) goto L_0x00c1
                java.lang.String r0 = "com.facebook.profilo.ipc.IProfiloMultiProcessTraceService"
                android.os.IInterface r1 = r3.queryLocalInterface(r0)
                if (r1 == 0) goto L_0x00ce
                boolean r0 = r1 instanceof com.facebook.profilo.ipc.IProfiloMultiProcessTraceService
                if (r0 == 0) goto L_0x00ce
                com.facebook.profilo.ipc.IProfiloMultiProcessTraceService r1 = (com.facebook.profilo.ipc.IProfiloMultiProcessTraceService) r1
            L_0x00c1:
                r5.Bl2(r1)
                r8.writeNoException()
                r0 = 1964285259(0x75149d4b, float:1.8839117E32)
                X.C000700l.A09(r0, r2)
                return r4
            L_0x00ce:
                com.facebook.profilo.ipc.IProfiloMultiProcessTraceService$Stub$Proxy r1 = new com.facebook.profilo.ipc.IProfiloMultiProcessTraceService$Stub$Proxy
                r1.<init>(r3)
                goto L_0x00c1
            L_0x00d4:
                r8.writeString(r0)
                r0 = -436317488(0xffffffffe5fe52d0, float:-1.5012609E23)
                X.C000700l.A09(r0, r2)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener.Stub.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }
    }

    void BTF(TraceConfigData traceConfigData);

    void Bl2(IProfiloMultiProcessTraceService iProfiloMultiProcessTraceService);

    void CMr(long j);

    void onTraceAbort(TraceContext traceContext);

    void onTraceStart(TraceContext traceContext);

    void onTraceStop(TraceContext traceContext);
}
