package com.facebook.profilo.ipc;

import X.AnonymousClass0I7;
import android.os.Parcel;
import android.os.Parcelable;

public final class TraceConfigData implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0I7();
    public int A00;
    public int A01;
    public long A02;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A02);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
    }

    public TraceConfigData() {
    }

    public TraceConfigData(long j, int i, int i2) {
        this.A02 = j;
        this.A01 = i;
        this.A00 = i2;
    }

    public TraceConfigData(Parcel parcel) {
        this.A02 = parcel.readLong();
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
    }
}
