package com.facebook.profilo.ipc;

import X.C000700l;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener;

public interface IProfiloMultiProcessTraceService extends IInterface {

    public abstract class Stub extends Binder implements IProfiloMultiProcessTraceService {

        public final class Proxy implements IProfiloMultiProcessTraceService {
            private IBinder A00;

            public Proxy(IBinder iBinder) {
                int A03 = C000700l.A03(-1878909366);
                this.A00 = iBinder;
                C000700l.A09(2058609843, A03);
            }

            public void BsY(long j, int i) {
                int A03 = C000700l.A03(1767577486);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceService");
                    obtain.writeLong(j);
                    obtain.writeInt(i);
                    this.A00.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(578514478, A03);
                }
            }

            public void C0a(IProfiloMultiProcessTraceListener iProfiloMultiProcessTraceListener) {
                IBinder iBinder;
                int A03 = C000700l.A03(-876090529);
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.facebook.profilo.ipc.IProfiloMultiProcessTraceService");
                    if (iProfiloMultiProcessTraceListener != null) {
                        iBinder = iProfiloMultiProcessTraceListener.asBinder();
                    } else {
                        iBinder = null;
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.A00.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                    C000700l.A09(-1200062581, A03);
                }
            }

            public IBinder asBinder() {
                int A03 = C000700l.A03(-1467117008);
                IBinder iBinder = this.A00;
                C000700l.A09(463775479, A03);
                return iBinder;
            }
        }

        public Stub() {
            int A03 = C000700l.A03(835381719);
            attachInterface(this, "com.facebook.profilo.ipc.IProfiloMultiProcessTraceService");
            C000700l.A09(-1535492582, A03);
        }

        public IBinder asBinder() {
            C000700l.A09(1315957551, C000700l.A03(1431318740));
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            int A03 = C000700l.A03(-1530826872);
            if (i == 1) {
                parcel.enforceInterface("com.facebook.profilo.ipc.IProfiloMultiProcessTraceService");
                C0a(IProfiloMultiProcessTraceListener.Stub.A00(parcel.readStrongBinder()));
                parcel2.writeNoException();
                C000700l.A09(-2007215651, A03);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("com.facebook.profilo.ipc.IProfiloMultiProcessTraceService");
                BsY(parcel.readLong(), parcel.readInt());
                parcel2.writeNoException();
                C000700l.A09(1727197235, A03);
                return true;
            } else if (i != 1598968902) {
                boolean onTransact = super.onTransact(i, parcel, parcel2, i2);
                C000700l.A09(-1169041843, A03);
                return onTransact;
            } else {
                parcel2.writeString("com.facebook.profilo.ipc.IProfiloMultiProcessTraceService");
                C000700l.A09(8790207, A03);
                return true;
            }
        }
    }

    void BsY(long j, int i);

    void C0a(IProfiloMultiProcessTraceListener iProfiloMultiProcessTraceListener);
}
