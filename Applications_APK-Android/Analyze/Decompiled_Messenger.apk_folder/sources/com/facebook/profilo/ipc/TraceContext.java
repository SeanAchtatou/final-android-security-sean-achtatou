package com.facebook.profilo.ipc;

import X.AnonymousClass0HV;
import X.AnonymousClass0HW;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public final class TraceContext implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0HV();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public TraceConfigExtras A06;
    public Object A07;
    public Object A08;
    public String A09;

    public final class TraceConfigExtras implements Parcelable {
        public static final TraceConfigExtras A03 = new TraceConfigExtras(null, null, null);
        public static final Parcelable.Creator CREATOR = new AnonymousClass0HW();
        public final TreeMap A00;
        public final TreeMap A01;
        private final TreeMap A02;

        public int describeContents() {
            return 0;
        }

        public int A00(String str, int i) {
            Integer num;
            TreeMap treeMap = this.A02;
            if (treeMap == null || (num = (Integer) treeMap.get(str)) == null) {
                return i;
            }
            return num.intValue();
        }

        public void writeToParcel(Parcel parcel, int i) {
            Bundle bundle = new Bundle();
            TreeMap treeMap = this.A02;
            if (treeMap != null) {
                for (Map.Entry entry : treeMap.entrySet()) {
                    bundle.putInt((String) entry.getKey(), ((Integer) entry.getValue()).intValue());
                }
            }
            parcel.writeBundle(bundle);
            Bundle bundle2 = new Bundle();
            TreeMap treeMap2 = this.A00;
            if (treeMap2 != null) {
                for (Map.Entry entry2 : treeMap2.entrySet()) {
                    bundle2.putBoolean((String) entry2.getKey(), ((Boolean) entry2.getValue()).booleanValue());
                }
            }
            parcel.writeBundle(bundle2);
            Bundle bundle3 = new Bundle();
            TreeMap treeMap3 = this.A01;
            if (treeMap3 != null) {
                for (Map.Entry entry3 : treeMap3.entrySet()) {
                    bundle3.putIntArray((String) entry3.getKey(), (int[]) entry3.getValue());
                }
            }
            parcel.writeBundle(bundle3);
        }

        public TraceConfigExtras(Parcel parcel) {
            Class<?> cls = getClass();
            Bundle readBundle = parcel.readBundle(cls.getClassLoader());
            Set<String> keySet = readBundle.keySet();
            if (!keySet.isEmpty()) {
                this.A02 = new TreeMap();
                for (String next : keySet) {
                    this.A02.put(next, Integer.valueOf(readBundle.getInt(next)));
                }
            } else {
                this.A02 = null;
            }
            Bundle readBundle2 = parcel.readBundle(cls.getClassLoader());
            Set<String> keySet2 = readBundle2.keySet();
            if (!keySet2.isEmpty()) {
                this.A00 = new TreeMap();
                for (String next2 : keySet2) {
                    this.A00.put(next2, Boolean.valueOf(readBundle2.getBoolean(next2)));
                }
            } else {
                this.A00 = null;
            }
            Bundle readBundle3 = parcel.readBundle(cls.getClassLoader());
            if (!readBundle3.keySet().isEmpty()) {
                this.A01 = new TreeMap();
                for (String next3 : keySet2) {
                    this.A01.put(next3, readBundle3.getIntArray(next3));
                }
                return;
            }
            this.A01 = null;
        }

        public TraceConfigExtras(TreeMap treeMap, TreeMap treeMap2, TreeMap treeMap3) {
            this.A02 = treeMap;
            this.A00 = treeMap2;
            this.A01 = treeMap3;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A05);
        parcel.writeString(this.A09);
        parcel.writeInt(this.A01);
        parcel.writeLong(this.A04);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A00);
        this.A06.writeToParcel(parcel, i);
    }

    public TraceContext() {
    }

    public TraceContext(long j, String str, int i, Object obj, Object obj2, long j2, int i2, int i3, int i4, TraceConfigExtras traceConfigExtras) {
        this.A05 = j;
        this.A09 = str;
        this.A01 = i;
        this.A08 = obj;
        this.A07 = obj2;
        this.A04 = j2;
        this.A02 = i2;
        this.A03 = i3;
        this.A00 = i4;
        this.A06 = traceConfigExtras;
    }

    public TraceContext(Parcel parcel) {
        this.A05 = parcel.readLong();
        this.A09 = parcel.readString();
        this.A01 = parcel.readInt();
        this.A08 = null;
        this.A07 = null;
        this.A04 = parcel.readLong();
        this.A02 = parcel.readInt();
        this.A03 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A06 = (TraceConfigExtras) TraceConfigExtras.CREATOR.createFromParcel(parcel);
    }

    public TraceContext(TraceContext traceContext, int i) {
        this(traceContext.A05, traceContext.A09, traceContext.A01, traceContext.A08, traceContext.A07, traceContext.A04, traceContext.A02, traceContext.A03, i, traceContext.A06);
    }
}
