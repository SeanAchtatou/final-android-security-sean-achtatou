package com.facebook.profilo.provider.mappings;

import X.C000700l;
import X.C000900o;
import com.facebook.profilo.core.ProvidersRegistry;

public final class MemoryMappingsProvider extends C000900o {
    public static final int PROVIDER_MAPPINGS = ProvidersRegistry.A00.A02("memory_mappings");

    private static native void nativeLogMappings();

    public MemoryMappingsProvider() {
        super("profilo_mappings");
    }

    public void disable() {
        int A03 = C000700l.A03(-885041157);
        nativeLogMappings();
        C000700l.A09(2064528385, A03);
    }

    public void enable() {
        C000700l.A09(-704850538, C000700l.A03(-1170798414));
    }

    public int getSupportedProviders() {
        return PROVIDER_MAPPINGS;
    }

    public int getTracingProviders() {
        return PROVIDER_MAPPINGS;
    }
}
