package com.facebook.profilo.provider.stacktrace;

import X.AnonymousClass01q;

public class StackTraceWhitelist {
    public static native void nativeAddToWhitelist(int i);

    private static native void nativeRemoveFromWhitelist(int i);

    static {
        AnonymousClass01q.A08("profilo_stacktrace");
    }
}
