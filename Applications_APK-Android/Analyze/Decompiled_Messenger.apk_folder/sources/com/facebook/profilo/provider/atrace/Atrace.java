package com.facebook.profilo.provider.atrace;

import X.AnonymousClass05L;

public final class Atrace {
    private static boolean sHasHook;
    private static boolean sHookFailed;

    public static native void enableSystraceNative(boolean z);

    private static native boolean installSystraceHook(int i, boolean z);

    public static native boolean isEnabled();

    public static native void restoreSystraceNative(boolean z);

    public static synchronized boolean hasHacks(boolean z) {
        boolean z2;
        synchronized (Atrace.class) {
            if (!sHasHook && !sHookFailed) {
                boolean installSystraceHook = installSystraceHook(AnonymousClass05L.A00, z);
                sHasHook = installSystraceHook;
                boolean z3 = false;
                if (!installSystraceHook) {
                    z3 = true;
                }
                sHookFailed = z3;
            }
            z2 = sHasHook;
        }
        return z2;
    }
}
