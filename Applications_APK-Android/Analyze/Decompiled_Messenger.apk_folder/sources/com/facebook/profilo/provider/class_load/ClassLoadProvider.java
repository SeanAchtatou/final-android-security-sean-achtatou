package com.facebook.profilo.provider.class_load;

import X.AnonymousClass0RX;
import X.C000700l;
import X.C000900o;
import X.C03450Nu;
import X.C03460Nv;
import android.util.Log;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;
import java.util.ArrayList;
import java.util.Iterator;

public final class ClassLoadProvider extends C000900o {
    public static final int A01 = ProvidersRegistry.A00.A02("class_load");
    private C03460Nv A00 = new AnonymousClass0RX();

    public ClassLoadProvider() {
        super(null);
    }

    public void disable() {
        int A03 = C000700l.A03(-1042262447);
        C03450Nu A002 = C03450Nu.A00();
        if (A002 != null) {
            C03460Nv r3 = this.A00;
            synchronized (A002) {
                ArrayList arrayList = new ArrayList(((ArrayList) A002.A03.get()).size() - 1);
                Iterator it = ((ArrayList) A002.A03.get()).iterator();
                while (it.hasNext()) {
                    C03460Nv r0 = (C03460Nv) it.next();
                    if (r0 != r3) {
                        arrayList.add(r0);
                    }
                }
                A002.A03.set(arrayList);
                if (((ArrayList) A002.A03.get()).isEmpty()) {
                    if (!A002.A00.toString().startsWith("dalvik.system.PathClassLoader") || !(A002.A00.getParent() instanceof C03450Nu)) {
                        Log.w("PluginClassLoader", "Non-standard class loader chain. PluginClassLoader will not be uninstalled");
                    } else {
                        try {
                            A002.A02.set(A002.A00, A002.A01);
                            A002.A04 = false;
                            Log.w("PluginClassLoader", "Uninstalled PluginClassLoader");
                        } catch (IllegalAccessException unused) {
                            Log.w("PluginClassLoader", "Failed to uninstall PluginClassLoader");
                        }
                    }
                }
            }
        }
        C000700l.A09(-1174448314, A03);
    }

    public void enable() {
        int A03 = C000700l.A03(849025068);
        C03450Nu A002 = C03450Nu.A00();
        if (A002 != null) {
            A002.A01(this.A00);
        }
        C000700l.A09(-1867853427, A03);
    }

    public int getSupportedProviders() {
        return A01;
    }

    public int getTracingProviders() {
        boolean z;
        C03450Nu A002 = C03450Nu.A00();
        if (!TraceEvents.isEnabled(A01) || A002 == null) {
            return 0;
        }
        synchronized (A002) {
            z = A002.A04;
        }
        if (z) {
            return A01;
        }
        return 0;
    }
}
