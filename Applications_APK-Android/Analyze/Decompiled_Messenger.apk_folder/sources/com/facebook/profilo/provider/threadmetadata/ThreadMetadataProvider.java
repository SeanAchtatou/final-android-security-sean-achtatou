package com.facebook.profilo.provider.threadmetadata;

import X.AnonymousClass09F;
import X.C004505k;
import com.facebook.profilo.ipc.TraceContext;

public final class ThreadMetadataProvider extends AnonymousClass09F {
    private static native void nativeLogThreadMetadata();

    public ThreadMetadataProvider() {
        super("profilo_threadmetadata");
    }

    public void logOnTraceEnd(TraceContext traceContext, C004505k r2) {
        nativeLogThreadMetadata();
    }
}
