package com.facebook.profilo.provider.stacktrace;

import X.AnonymousClass01q;
import java.util.concurrent.atomic.AtomicReference;

public class ArtCompatibility {
    private static final AtomicReference sIsCompatible = new AtomicReference(null);

    private static native boolean nativeCheck(int i);

    static {
        AnonymousClass01q.A08("profilo_stacktrace");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:105|106) */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x016f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0044, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00fc, code lost:
        if (r0 == false) goto L_0x00fe;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:105:0x0173 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:105:0x0173=Splitter:B:105:0x0173, B:15:0x003d=Splitter:B:15:0x003d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isCompatible(android.content.Context r6) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r5 = 0
            r0 = 21
            if (r1 >= r0) goto L_0x0008
            return r5
        L_0x0008:
            java.util.concurrent.atomic.AtomicReference r0 = com.facebook.profilo.provider.stacktrace.ArtCompatibility.sIsCompatible
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x0017
            boolean r0 = r0.booleanValue()
            return r0
        L_0x0017:
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0174 }
            java.io.File r3 = r6.getFilesDir()     // Catch:{ IOException -> 0x0174 }
            java.lang.String r0 = "ProfiloArtUnwindcCompat_"
            java.lang.String r1 = android.os.Build.VERSION.RELEASE     // Catch:{ IOException -> 0x0174 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ IOException -> 0x0174 }
            r2.<init>(r3, r0)     // Catch:{ IOException -> 0x0174 }
            boolean r0 = r2.exists()     // Catch:{ IOException -> 0x0174 }
            if (r0 == 0) goto L_0x004a
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0174 }
            r4.<init>(r2)     // Catch:{ IOException -> 0x0174 }
            int r1 = r4.read()     // Catch:{ all -> 0x0042 }
            r0 = 49
            r3 = 0
            if (r1 != r0) goto L_0x003d
            r3 = 1
        L_0x003d:
            r4.close()     // Catch:{ IOException -> 0x0174 }
            goto L_0x0154
        L_0x0042:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0044 }
        L_0x0044:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x0173 }
            goto L_0x0173
        L_0x004a:
            int r0 = r1.hashCode()     // Catch:{ IOException -> 0x0174 }
            switch(r0) {
                case 52407: goto L_0x00f4;
                case 52408: goto L_0x00e9;
                case 53368: goto L_0x00de;
                case 54329: goto L_0x00d4;
                case 54330: goto L_0x00ca;
                case 50364602: goto L_0x00bf;
                case 50364603: goto L_0x00b4;
                case 50365562: goto L_0x00a9;
                case 50365563: goto L_0x009e;
                case 51288123: goto L_0x0093;
                case 52211643: goto L_0x0089;
                case 52212604: goto L_0x007f;
                case 52212605: goto L_0x0074;
                case 52212606: goto L_0x0069;
                case 53135164: goto L_0x005e;
                case 53136125: goto L_0x0053;
                default: goto L_0x0051;
            }     // Catch:{ IOException -> 0x0174 }
        L_0x0051:
            goto L_0x00fe
        L_0x0053:
            java.lang.String r0 = "8.1.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 0
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x005e:
            java.lang.String r0 = "8.0.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 1
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x0069:
            java.lang.String r0 = "7.1.2"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 2
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x0074:
            java.lang.String r0 = "7.1.1"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 3
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x007f:
            java.lang.String r0 = "7.1.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 5
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x0089:
            java.lang.String r0 = "7.0.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 7
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x0093:
            java.lang.String r0 = "6.0.1"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 9
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x009e:
            java.lang.String r0 = "5.1.1"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 12
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00a9:
            java.lang.String r0 = "5.1.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 11
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00b4:
            java.lang.String r0 = "5.0.2"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 15
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00bf:
            java.lang.String r0 = "5.0.1"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 14
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00ca:
            java.lang.String r0 = "7.1"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 4
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00d4:
            java.lang.String r0 = "7.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 6
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00de:
            java.lang.String r0 = "6.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 8
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00e9:
            java.lang.String r0 = "5.1"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 10
            if (r0 != 0) goto L_0x00ff
            goto L_0x00fe
        L_0x00f4:
            java.lang.String r0 = "5.0"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0174 }
            r1 = 13
            if (r0 != 0) goto L_0x00ff
        L_0x00fe:
            r1 = -1
        L_0x00ff:
            switch(r1) {
                case 0: goto L_0x013c;
                case 1: goto L_0x0135;
                case 2: goto L_0x012e;
                case 3: goto L_0x0127;
                case 4: goto L_0x0120;
                case 5: goto L_0x0120;
                case 6: goto L_0x0119;
                case 7: goto L_0x0119;
                case 8: goto L_0x0112;
                case 9: goto L_0x0112;
                case 10: goto L_0x010b;
                case 11: goto L_0x010b;
                case 12: goto L_0x010b;
                case 13: goto L_0x0104;
                case 14: goto L_0x0104;
                case 15: goto L_0x0104;
                default: goto L_0x0102;
            }     // Catch:{ IOException -> 0x0174 }
        L_0x0102:
            r3 = 0
        L_0x0103:
            goto L_0x0143
        L_0x0104:
            r0 = 1024(0x400, float:1.435E-42)
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x010b:
            r0 = 2048(0x800, float:2.87E-42)
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x0112:
            r0 = 16
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x0119:
            r0 = 32
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x0120:
            r0 = 64
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x0127:
            r0 = 128(0x80, float:1.794E-43)
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x012e:
            r0 = 256(0x100, float:3.59E-43)
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x0135:
            r0 = 4096(0x1000, float:5.74E-42)
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x013c:
            r0 = 8192(0x2000, float:1.14794E-41)
            boolean r3 = nativeCheck(r0)     // Catch:{ IOException -> 0x0174 }
            goto L_0x0103
        L_0x0143:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0174 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0174 }
            r0 = 48
            if (r3 == 0) goto L_0x014e
            r0 = 49
        L_0x014e:
            r1.write(r0)     // Catch:{ all -> 0x016d }
            r1.close()     // Catch:{ IOException -> 0x0174 }
        L_0x0154:
            java.util.concurrent.atomic.AtomicReference r2 = com.facebook.profilo.provider.stacktrace.ArtCompatibility.sIsCompatible     // Catch:{ IOException -> 0x0174 }
            r1 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)     // Catch:{ IOException -> 0x0174 }
            boolean r0 = r2.compareAndSet(r1, r0)     // Catch:{ IOException -> 0x0174 }
            if (r0 == 0) goto L_0x0162
            return r3
        L_0x0162:
            java.lang.Object r0 = r2.get()     // Catch:{ IOException -> 0x0174 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ IOException -> 0x0174 }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x0174 }
            return r0
        L_0x016d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x016f }
        L_0x016f:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0173 }
        L_0x0173:
            throw r0     // Catch:{ IOException -> 0x0174 }
        L_0x0174:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.provider.stacktrace.ArtCompatibility.isCompatible(android.content.Context):boolean");
    }
}
