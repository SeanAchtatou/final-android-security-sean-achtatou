package com.facebook.profilo.provider.perfevents;

import X.AnonymousClass0Ph;
import X.C000700l;

public class PerfEventsSession {
    public long mNativeHandle;
    public final Runnable mSessionRunnable = new AnonymousClass0Ph(this);
    public Thread mThread;

    public static native long nativeAttach(boolean z, int i, int i2, float f);

    public static native void nativeDetach(long j);

    public static native void nativeRun(long j);

    private static native void nativeStop(long j);

    public synchronized void stop() {
        long j = this.mNativeHandle;
        if (j != 0) {
            nativeStop(j);
            try {
                this.mThread.join();
                this.mThread = null;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } else if (this.mThread != null) {
            throw new IllegalStateException("Inconsistent state: have thread but no handle");
        }
    }

    public void finalize() {
        int A03 = C000700l.A03(1734357246);
        stop();
        synchronized (this) {
            try {
                long j = this.mNativeHandle;
                if (j != 0) {
                    nativeDetach(j);
                }
            } catch (Throwable th) {
                while (true) {
                    C000700l.A09(1111556274, A03);
                    throw th;
                }
            }
        }
        super.finalize();
        C000700l.A09(76533778, A03);
    }
}
