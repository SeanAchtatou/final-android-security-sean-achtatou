package com.facebook.profilo.provider.memory;

import X.C000700l;
import X.C000900o;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.ipc.TraceContext;

public final class MemoryAllocationProvider extends C000900o {
    public static final int PROVIDER_MEMORY = ProvidersRegistry.A00.A02("memory_allocation");

    private static native void nativeInitialize(int i);

    private static native boolean nativeIsTracingEnabled();

    private static native void nativeStartProfiling();

    private static native void nativeStopProfiling();

    public MemoryAllocationProvider() {
        super("profilo_memory");
    }

    public void disable() {
        int A03 = C000700l.A03(-526372287);
        nativeStopProfiling();
        C000700l.A09(502832503, A03);
    }

    public void enable() {
        int A00;
        int A03 = C000700l.A03(-22906218);
        TraceContext traceContext = this.A00;
        if (traceContext == null) {
            A00 = 0;
        } else {
            A00 = traceContext.A06.A00("provider.memory_allocation.allocation_sample_rate", 500);
        }
        nativeInitialize(A00);
        nativeStartProfiling();
        C000700l.A09(65702128, A03);
    }

    public int getSupportedProviders() {
        return PROVIDER_MEMORY;
    }

    public int getTracingProviders() {
        if (!nativeIsTracingEnabled() || !TraceEvents.isEnabled(PROVIDER_MEMORY)) {
            return 0;
        }
        return PROVIDER_MEMORY;
    }
}
