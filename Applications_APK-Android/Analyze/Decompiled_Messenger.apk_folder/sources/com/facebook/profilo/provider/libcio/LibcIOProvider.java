package com.facebook.profilo.provider.libcio;

import X.C000700l;
import X.C000900o;
import com.facebook.profilo.core.ProvidersRegistry;

public final class LibcIOProvider extends C000900o {
    public static final int PROVIDER_LIBC_IO = ProvidersRegistry.A00.A02("libc_io");

    public native void nativeCleanup();

    public native void nativeInitialize();

    public native boolean nativeIsTracingEnabled();

    public LibcIOProvider() {
        super("profilo_libcio");
    }

    public void disable() {
        int A03 = C000700l.A03(-50977711);
        nativeCleanup();
        C000700l.A09(-1560096535, A03);
    }

    public void enable() {
        int A03 = C000700l.A03(1483191554);
        nativeInitialize();
        C000700l.A09(25905291, A03);
    }

    public int getSupportedProviders() {
        return PROVIDER_LIBC_IO;
    }

    public int getTracingProviders() {
        if (nativeIsTracingEnabled()) {
            return PROVIDER_LIBC_IO;
        }
        return 0;
    }
}
