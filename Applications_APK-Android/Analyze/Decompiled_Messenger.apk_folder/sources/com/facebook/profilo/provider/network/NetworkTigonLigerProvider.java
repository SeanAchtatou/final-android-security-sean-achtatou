package com.facebook.profilo.provider.network;

import X.AnonymousClass00n;
import X.AnonymousClass08Y;
import X.C000700l;
import X.C000900o;
import X.C03740Pg;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.tigon.TigonXplatService;
import com.facebook.tigon.tigonliger.TigonLigerService;
import com.facebook.tigon.tigonvideo.TigonVideoService;
import java.util.concurrent.Executor;

public final class NetworkTigonLigerProvider extends C000900o {
    public static final int A04;
    public static final int A05;
    private C03740Pg A00;
    private Executor A01;
    private final TigonLigerService A02;
    private final TigonVideoService A03;

    static {
        AnonymousClass08Y r1 = ProvidersRegistry.A00;
        A04 = r1.A02("liger_http2");
        A05 = r1.A02("liger_http2_egress");
    }

    public NetworkTigonLigerProvider(TigonXplatService tigonXplatService, TigonXplatService tigonXplatService2, Executor executor) {
        super("profilo_network");
        if (!(tigonXplatService == null && tigonXplatService2 == null) && (tigonXplatService == null || tigonXplatService2 == null)) {
            this.A03 = (TigonVideoService) tigonXplatService;
            this.A02 = (TigonLigerService) tigonXplatService2;
            this.A01 = executor;
            return;
        }
        throw new IllegalArgumentException("Need exactly one of TigonVideoService or TigonLigerService");
    }

    private C03740Pg A00() {
        C03740Pg networkTigonLigerHybrid;
        C03740Pg r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        TigonVideoService tigonVideoService = this.A03;
        if (tigonVideoService != null) {
            networkTigonLigerHybrid = new NetworkTigonVideoHybrid(tigonVideoService, this.A01);
        } else {
            TigonLigerService tigonLigerService = this.A02;
            if (tigonLigerService != null) {
                networkTigonLigerHybrid = new NetworkTigonLigerHybrid(tigonLigerService, this.A01);
            } else {
                throw new IllegalStateException("Neither TigonVideoService nor TigonLigerService set");
            }
        }
        this.A00 = networkTigonLigerHybrid;
        return networkTigonLigerHybrid;
    }

    public int getSupportedProviders() {
        return A04 | AnonymousClass00n.A05 | A05;
    }

    public int getTracingProviders() {
        C03740Pg r0 = this.A00;
        int i = 0;
        if (r0 == null) {
            return 0;
        }
        if (r0.nativeIsTigonObserverEnabled()) {
            i = 0 | AnonymousClass00n.A05;
        }
        if (this.A00.nativeIsIngressLigerCodecLoggerEnabled()) {
            i |= A04;
        }
        if (this.A00.nativeIsEgressLigerCodecLoggerEnabled()) {
            return i | A05;
        }
        return i;
    }

    public void disable() {
        int A032 = C000700l.A03(2125425550);
        A00().nativeDisable();
        C000700l.A09(-1480913666, A032);
    }

    public void enable() {
        int A032 = C000700l.A03(-313598717);
        A00().nativeEnable(TraceEvents.isEnabled(AnonymousClass00n.A05), TraceEvents.isEnabled(A04), TraceEvents.isEnabled(A05));
        C000700l.A09(-1492366710, A032);
    }
}
