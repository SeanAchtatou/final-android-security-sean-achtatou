package com.facebook.profilo.provider.network;

import X.C03740Pg;
import com.facebook.jni.HybridData;
import com.facebook.tigon.tigonliger.TigonLigerService;
import java.util.concurrent.Executor;

public final class NetworkTigonLigerHybrid extends C03740Pg {
    private HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public native void nativeDisable();

    public native void nativeEnable(boolean z, boolean z2, boolean z3);

    public native boolean nativeIsEgressLigerCodecLoggerEnabled();

    public native boolean nativeIsIngressLigerCodecLoggerEnabled();

    public native boolean nativeIsTigonObserverEnabled();

    public native void setTigonService(TigonLigerService tigonLigerService, Executor executor);

    public NetworkTigonLigerHybrid(TigonLigerService tigonLigerService, Executor executor) {
        setTigonService(tigonLigerService, executor);
    }
}
