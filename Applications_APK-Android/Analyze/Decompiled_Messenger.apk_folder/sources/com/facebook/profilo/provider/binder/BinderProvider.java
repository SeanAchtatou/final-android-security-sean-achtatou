package com.facebook.profilo.provider.binder;

import X.C000700l;
import X.C000900o;
import com.facebook.profilo.core.ProvidersRegistry;

public final class BinderProvider extends C000900o {
    public static final int PROVIDER_BINDER = ProvidersRegistry.A00.A02("binder");

    public native void nativeDisable();

    public native void nativeEnable();

    public native boolean nativeIsTracingEnabled();

    public BinderProvider() {
        super("profilo_binder");
    }

    public void disable() {
        int A03 = C000700l.A03(-1506648972);
        nativeDisable();
        C000700l.A09(21814045, A03);
    }

    public void enable() {
        int A03 = C000700l.A03(1326119801);
        nativeEnable();
        C000700l.A09(673328004, A03);
    }

    public int getSupportedProviders() {
        return PROVIDER_BINDER;
    }

    public int getTracingProviders() {
        if (nativeIsTracingEnabled()) {
            return PROVIDER_BINDER;
        }
        return 0;
    }
}
