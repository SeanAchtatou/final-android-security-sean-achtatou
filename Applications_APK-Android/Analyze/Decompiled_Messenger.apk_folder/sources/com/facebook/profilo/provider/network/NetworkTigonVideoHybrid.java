package com.facebook.profilo.provider.network;

import X.C03740Pg;
import com.facebook.jni.HybridData;
import com.facebook.tigon.tigonvideo.TigonVideoService;
import java.util.concurrent.Executor;

public final class NetworkTigonVideoHybrid extends C03740Pg {
    private HybridData mHybridData = initHybrid();

    private static native HybridData initHybrid();

    public native void nativeDisable();

    public native void nativeEnable(boolean z, boolean z2, boolean z3);

    public native boolean nativeIsEgressLigerCodecLoggerEnabled();

    public native boolean nativeIsIngressLigerCodecLoggerEnabled();

    public native boolean nativeIsTigonObserverEnabled();

    public native void setTigonService(TigonVideoService tigonVideoService, Executor executor);

    public NetworkTigonVideoHybrid(TigonVideoService tigonVideoService, Executor executor) {
        setTigonService(tigonVideoService, executor);
    }
}
