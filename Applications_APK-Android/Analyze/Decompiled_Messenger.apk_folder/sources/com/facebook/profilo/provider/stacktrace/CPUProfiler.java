package com.facebook.profilo.provider.stacktrace;

import X.AnonymousClass01q;

public class CPUProfiler {
    private static volatile int sAvailableTracers;
    public static volatile boolean sInitialized;

    private static native boolean nativeInitialize(int i);

    public static native void nativeLoggerLoop();

    public static native void nativeResetFrameworkNamesSet();

    public static native boolean nativeStartProfiling(int i, int i2, boolean z);

    public static native void nativeStopProfiling();

    static {
        AnonymousClass01q.A08("profilo_stacktrace");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00bd, code lost:
        if (r2.equals("5.0") == false) goto L_0x00bf;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean init(android.content.Context r4) {
        /*
            java.lang.Class<com.facebook.profilo.provider.stacktrace.CPUProfiler> r3 = com.facebook.profilo.provider.stacktrace.CPUProfiler.class
            monitor-enter(r3)
            boolean r0 = com.facebook.profilo.provider.stacktrace.CPUProfiler.sInitialized     // Catch:{ all -> 0x0101 }
            if (r0 == 0) goto L_0x000a
            r0 = 1
            monitor-exit(r3)
            return r0
        L_0x000a:
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0101 }
            r1 = 1
            r0 = 21
            if (r2 < r0) goto L_0x00df
            boolean r0 = com.facebook.profilo.provider.stacktrace.ArtCompatibility.isCompatible(r4)     // Catch:{ all -> 0x0101 }
            if (r0 == 0) goto L_0x00c3
            java.lang.String r2 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x0101 }
            int r0 = r2.hashCode()     // Catch:{ all -> 0x0101 }
            switch(r0) {
                case 52407: goto L_0x00b5;
                case 52408: goto L_0x00aa;
                case 53368: goto L_0x00a0;
                case 54329: goto L_0x0096;
                case 54330: goto L_0x008c;
                case 50364602: goto L_0x0081;
                case 50364603: goto L_0x0076;
                case 50365562: goto L_0x006b;
                case 50365563: goto L_0x0060;
                case 51288123: goto L_0x0055;
                case 52212604: goto L_0x004b;
                case 52212605: goto L_0x0041;
                case 52212606: goto L_0x0037;
                case 53135164: goto L_0x002d;
                case 53136125: goto L_0x0022;
                default: goto L_0x0020;
            }     // Catch:{ all -> 0x0101 }
        L_0x0020:
            goto L_0x00bf
        L_0x0022:
            java.lang.String r0 = "8.1.0"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 0
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x002d:
            java.lang.String r0 = "8.0.0"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            if (r0 == 0) goto L_0x00bf
            goto L_0x00c0
        L_0x0037:
            java.lang.String r0 = "7.1.2"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 2
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x0041:
            java.lang.String r0 = "7.1.1"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 3
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x004b:
            java.lang.String r0 = "7.1.0"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 5
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x0055:
            java.lang.String r0 = "6.0.1"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 8
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x0060:
            java.lang.String r0 = "5.1.1"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 11
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x006b:
            java.lang.String r0 = "5.1.0"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 10
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x0076:
            java.lang.String r0 = "5.0.2"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 14
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x0081:
            java.lang.String r0 = "5.0.1"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 13
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x008c:
            java.lang.String r0 = "7.1"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 4
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x0096:
            java.lang.String r0 = "7.0"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 6
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x00a0:
            java.lang.String r0 = "6.0"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 7
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x00aa:
            java.lang.String r0 = "5.1"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 9
            if (r0 != 0) goto L_0x00c0
            goto L_0x00bf
        L_0x00b5:
            java.lang.String r0 = "5.0"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0101 }
            r1 = 12
            if (r0 != 0) goto L_0x00c0
        L_0x00bf:
            r1 = -1
        L_0x00c0:
            switch(r1) {
                case 0: goto L_0x00dd;
                case 1: goto L_0x00da;
                case 2: goto L_0x00d7;
                case 3: goto L_0x00d4;
                case 4: goto L_0x00d1;
                case 5: goto L_0x00d1;
                case 6: goto L_0x00ce;
                case 7: goto L_0x00cb;
                case 8: goto L_0x00cb;
                case 9: goto L_0x00c8;
                case 10: goto L_0x00c8;
                case 11: goto L_0x00c8;
                case 12: goto L_0x00c5;
                case 13: goto L_0x00c5;
                case 14: goto L_0x00c5;
                default: goto L_0x00c3;
            }     // Catch:{ all -> 0x0101 }
        L_0x00c3:
            r1 = 0
            goto L_0x00df
        L_0x00c5:
            r1 = 1024(0x400, float:1.435E-42)
            goto L_0x00df
        L_0x00c8:
            r1 = 2048(0x800, float:2.87E-42)
            goto L_0x00df
        L_0x00cb:
            r1 = 16
            goto L_0x00df
        L_0x00ce:
            r1 = 32
            goto L_0x00df
        L_0x00d1:
            r1 = 64
            goto L_0x00df
        L_0x00d4:
            r1 = 128(0x80, float:1.794E-43)
            goto L_0x00df
        L_0x00d7:
            r1 = 256(0x100, float:3.59E-43)
            goto L_0x00df
        L_0x00da:
            r1 = 4096(0x1000, float:5.74E-42)
            goto L_0x00df
        L_0x00dd:
            r1 = 8192(0x2000, float:1.14794E-41)
        L_0x00df:
            r2 = r1 | 512(0x200, float:7.175E-43)
            java.lang.String r0 = "os.arch"
            java.lang.String r1 = java.lang.System.getProperty(r0)     // Catch:{ all -> 0x0101 }
            if (r1 == 0) goto L_0x00f1
            java.lang.String r0 = "arm"
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x0101 }
            if (r0 == 0) goto L_0x00f3
        L_0x00f1:
            r2 = r2 | 4
        L_0x00f3:
            com.facebook.profilo.provider.stacktrace.CPUProfiler.sAvailableTracers = r2     // Catch:{ all -> 0x0101 }
            int r0 = com.facebook.profilo.provider.stacktrace.CPUProfiler.sAvailableTracers     // Catch:{ all -> 0x0101 }
            boolean r0 = nativeInitialize(r0)     // Catch:{ all -> 0x0101 }
            com.facebook.profilo.provider.stacktrace.CPUProfiler.sInitialized = r0     // Catch:{ all -> 0x0101 }
            boolean r0 = com.facebook.profilo.provider.stacktrace.CPUProfiler.sInitialized     // Catch:{ all -> 0x0101 }
            monitor-exit(r3)
            return r0
        L_0x0101:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.provider.stacktrace.CPUProfiler.init(android.content.Context):boolean");
    }
}
