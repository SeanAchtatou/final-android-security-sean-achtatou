package com.facebook.profilo.provider.stacktrace;

import X.AnonymousClass08Y;
import X.C000700l;
import X.C000900o;
import X.C004505k;
import android.app.Application;
import android.content.Context;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.ipc.TraceContext;

public final class StackFrameThread extends C000900o {
    public static final int PROVIDER_NATIVE_STACK_TRACE;
    public static final int PROVIDER_STACK_FRAME;
    public static final int PROVIDER_WALL_TIME_STACK_TRACE;
    public final Context mContext;
    public volatile boolean mEnabled;
    private Thread mProfilerThread;
    private TraceContext mSavedTraceContext;
    public int mSystemClockTimeIntervalMs = -1;

    private static native int nativeSystemClockTickIntervalMs();

    static {
        AnonymousClass08Y r1 = ProvidersRegistry.A00;
        PROVIDER_STACK_FRAME = r1.A02("stack_trace");
        PROVIDER_WALL_TIME_STACK_TRACE = r1.A02("wall_time_stack_trace");
        PROVIDER_NATIVE_STACK_TRACE = r1.A02("native_stack_trace");
    }

    public StackFrameThread(Context context) {
        super("profilo_stacktrace");
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null || !(context instanceof Application)) {
            this.mContext = applicationContext;
        } else {
            this.mContext = context;
        }
    }

    public int getSupportedProviders() {
        return PROVIDER_NATIVE_STACK_TRACE | PROVIDER_STACK_FRAME | PROVIDER_WALL_TIME_STACK_TRACE;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        if ((r2 & r1) != 0) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getTracingProviders() {
        /*
            r4 = this;
            com.facebook.profilo.ipc.TraceContext r1 = r4.mSavedTraceContext
            boolean r0 = r4.mEnabled
            r3 = 0
            if (r0 == 0) goto L_0x001d
            if (r1 == 0) goto L_0x001d
            int r2 = r1.A02
            int r1 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_WALL_TIME_STACK_TRACE
            r0 = r2 & r1
            if (r0 != 0) goto L_0x0017
            int r1 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_STACK_FRAME
            r0 = r2 & r1
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r3 = r3 | r1
        L_0x0018:
            int r0 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_NATIVE_STACK_TRACE
            r2 = r2 & r0
            r2 = r2 | r3
            return r2
        L_0x001d:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.provider.stacktrace.StackFrameThread.getTracingProviders():int");
    }

    public void onTraceStarted(TraceContext traceContext, C004505k r3) {
        if (CPUProfiler.sInitialized) {
            CPUProfiler.nativeResetFrameworkNamesSet();
        }
    }

    public void disable() {
        int A03 = C000700l.A03(-1136144109);
        if (!this.mEnabled) {
            this.mProfilerThread = null;
            C000700l.A09(1610381143, A03);
            return;
        }
        this.mSavedTraceContext = null;
        this.mEnabled = false;
        synchronized (CPUProfiler.class) {
            if (CPUProfiler.sInitialized) {
                CPUProfiler.nativeStopProfiling();
            }
        }
        Thread thread = this.mProfilerThread;
        if (thread != null) {
            try {
                thread.join();
                this.mProfilerThread = null;
            } catch (InterruptedException e) {
                RuntimeException runtimeException = new RuntimeException(e);
                C000700l.A09(-831141173, A03);
                throw runtimeException;
            }
        }
        C000700l.A09(-1057524221, A03);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x009f, code lost:
        if (com.facebook.profilo.provider.stacktrace.CPUProfiler.nativeStartProfiling(r1, r6, r8) == false) goto L_0x00a1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void enable() {
        /*
            r21 = this;
            r0 = 1018280768(0x3cb1bb40, float:0.021695733)
            int r3 = X.C000700l.A03(r0)
            r4 = r21
            com.facebook.profilo.ipc.TraceContext r2 = r4.A00
            int r5 = r2.A02
            int r1 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_STACK_FRAME
            int r0 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_WALL_TIME_STACK_TRACE
            r1 = r1 | r0
            r1 = r1 & r5
            r0 = 0
            if (r1 == 0) goto L_0x0018
            r0 = 16369(0x3ff1, float:2.2938E-41)
        L_0x0018:
            int r11 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_NATIVE_STACK_TRACE
            r5 = r5 & r11
            if (r5 == 0) goto L_0x001f
            r0 = r0 | 4
        L_0x001f:
            if (r0 != 0) goto L_0x0028
            r0 = 813362116(0x307aebc4, float:9.1284513E-10)
            X.C000700l.A09(r0, r3)
            return
        L_0x0028:
            java.lang.Thread r0 = r4.mProfilerThread
            if (r0 == 0) goto L_0x003a
            java.lang.String r1 = "StackFrameThread"
            java.lang.String r0 = "Duplicate attempt to enable sampling profiler."
            android.util.Log.e(r1, r0)
            r0 = -769628773(0xffffffffd220659b, float:-1.72224856E11)
            X.C000700l.A09(r0, r3)
            return
        L_0x003a:
            com.facebook.profilo.ipc.TraceContext$TraceConfigExtras r5 = r2.A06
            r1 = 0
            java.lang.String r0 = "provider.stack_trace.cpu_sampling_rate_ms"
            int r6 = r5.A00(r0, r1)
            int r9 = r2.A02
            r10 = r4
            monitor-enter(r10)
            monitor-enter(r4)     // Catch:{ all -> 0x00ea }
            android.content.Context r0 = r4.mContext     // Catch:{ Exception -> 0x0050 }
            boolean r0 = com.facebook.profilo.provider.stacktrace.CPUProfiler.init(r0)     // Catch:{ Exception -> 0x0050 }
            monitor-exit(r4)     // Catch:{ all -> 0x00ea }
            goto L_0x005c
        L_0x0050:
            r5 = move-exception
            java.lang.String r1 = "StackFrameThread"
            java.lang.String r0 = r5.getMessage()     // Catch:{ all -> 0x00e4 }
            android.util.Log.e(r1, r0, r5)     // Catch:{ all -> 0x00e4 }
            monitor-exit(r4)     // Catch:{ all -> 0x00ea }
            r0 = 0
        L_0x005c:
            if (r0 == 0) goto L_0x00bd
            if (r6 > 0) goto L_0x0062
            r6 = 11
        L_0x0062:
            int r0 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_WALL_TIME_STACK_TRACE     // Catch:{ all -> 0x00ea }
            r0 = r0 & r9
            r5 = 1
            if (r0 == 0) goto L_0x006a
            r8 = 1
            goto L_0x007c
        L_0x006a:
            int r1 = r4.mSystemClockTimeIntervalMs     // Catch:{ all -> 0x00ea }
            r0 = -1
            if (r1 != r0) goto L_0x0075
            int r0 = nativeSystemClockTickIntervalMs()     // Catch:{ all -> 0x00ea }
            r4.mSystemClockTimeIntervalMs = r0     // Catch:{ all -> 0x00ea }
        L_0x0075:
            int r0 = r4.mSystemClockTimeIntervalMs     // Catch:{ all -> 0x00ea }
            int r6 = java.lang.Math.max(r6, r0)     // Catch:{ all -> 0x00ea }
            r8 = 0
        L_0x007c:
            int r7 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_STACK_FRAME     // Catch:{ all -> 0x00ea }
            int r0 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_WALL_TIME_STACK_TRACE     // Catch:{ all -> 0x00ea }
            r7 = r7 | r0
            r7 = r7 & r9
            r1 = 0
            if (r7 == 0) goto L_0x0087
            r1 = 16369(0x3ff1, float:2.2938E-41)
        L_0x0087:
            r9 = r9 & r11
            if (r9 == 0) goto L_0x008c
            r1 = r1 | 4
        L_0x008c:
            java.lang.Class<com.facebook.profilo.provider.stacktrace.CPUProfiler> r7 = com.facebook.profilo.provider.stacktrace.CPUProfiler.class
            monitor-enter(r7)     // Catch:{ all -> 0x00ea }
            int r0 = android.os.Process.myPid()     // Catch:{ all -> 0x00e7 }
            com.facebook.profilo.provider.stacktrace.StackTraceWhitelist.nativeAddToWhitelist(r0)     // Catch:{ all -> 0x00e7 }
            boolean r0 = com.facebook.profilo.provider.stacktrace.CPUProfiler.sInitialized     // Catch:{ all -> 0x00e7 }
            if (r0 == 0) goto L_0x00a1
            boolean r1 = com.facebook.profilo.provider.stacktrace.CPUProfiler.nativeStartProfiling(r1, r6, r8)     // Catch:{ all -> 0x00e7 }
            r0 = 1
            if (r1 != 0) goto L_0x00a2
        L_0x00a1:
            r0 = 0
        L_0x00a2:
            monitor-exit(r7)     // Catch:{ all -> 0x00ea }
            if (r0 == 0) goto L_0x00bd
            r11 = 0
            r12 = 7
            r13 = 52
            r14 = 0
            r16 = 0
            r17 = 8126495(0x7c001f, float:1.1387645E-38)
            r18 = 0
            long r0 = (long) r6     // Catch:{ all -> 0x00ea }
            r19 = r0
            com.facebook.profilo.logger.Logger.writeStandardEntry(r11, r12, r13, r14, r16, r17, r18, r19)     // Catch:{ all -> 0x00ea }
            r4.mEnabled = r5     // Catch:{ all -> 0x00ea }
            boolean r0 = r4.mEnabled     // Catch:{ all -> 0x00ea }
            goto L_0x00c0
        L_0x00bd:
            monitor-exit(r10)
            r0 = 0
            goto L_0x00c1
        L_0x00c0:
            monitor-exit(r10)
        L_0x00c1:
            if (r0 != 0) goto L_0x00ca
            r0 = 169862066(0xa1fe3b2, float:7.698396E-33)
            X.C000700l.A09(r0, r3)
            return
        L_0x00ca:
            r4.mSavedTraceContext = r2
            java.lang.Thread r2 = new java.lang.Thread
            X.0He r1 = new X.0He
            r1.<init>()
            java.lang.String r0 = "Prflo:Profiler"
            r2.<init>(r1, r0)
            r4.mProfilerThread = r2
            r2.start()
            r0 = -158407692(0xfffffffff68ee3f4, float:-1.4490812E33)
            X.C000700l.A09(r0, r3)
            return
        L_0x00e4:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00ea }
            goto L_0x00e9
        L_0x00e7:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00ea }
        L_0x00e9:
            throw r0     // Catch:{ all -> 0x00ea }
        L_0x00ea:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.provider.stacktrace.StackFrameThread.enable():void");
    }
}
