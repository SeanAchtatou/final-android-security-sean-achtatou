package com.facebook.profilo.provider.device_info;

import X.AnonymousClass08S;
import X.AnonymousClass09F;
import X.AnonymousClass0VQ;
import X.AnonymousClass8Y7;
import X.C004505k;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;
import java.io.File;

public final class DeviceInfoProvider extends AnonymousClass09F {
    private final Context A00;

    public DeviceInfoProvider(Context context) {
        super(null);
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null || !(context instanceof Application)) {
            this.A00 = applicationContext;
        } else {
            this.A00 = context;
        }
    }

    private static void A01(int i, long j) {
        Logger.writeStandardEntry(0, 7, 52, 0, 0, i, 0, j);
    }

    private static void A02(int i, String str, String str2) {
        int writeStandardEntry = Logger.writeStandardEntry(0, 7, 52, 0, 0, i, 0, 0);
        if (str != null) {
            writeStandardEntry = Logger.writeBytesEntry(0, 1, 56, writeStandardEntry, str);
        }
        Logger.writeBytesEntry(0, 1, 57, writeStandardEntry, str2);
    }

    static {
        ProvidersRegistry.A00.A02("device_info");
    }

    private void A00() {
        long j;
        A02(8126483, "os_ver", Build.VERSION.RELEASE);
        A02(8126478, "device_type", Build.MODEL);
        A02(8126479, "brand", Build.BRAND);
        A02(8126480, "manufacturer", Build.MANUFACTURER);
        A02(8126481, "year_class", Integer.toString(AnonymousClass8Y7.A00(this.A00)));
        A02(8126537, "os_sdk", Integer.toString(Build.VERSION.SDK_INT));
        A01(8126503, (long) AnonymousClass0VQ.A00());
        A01(8126502, AnonymousClass0VQ.A04(this.A00));
        try {
            A02(8126527, "Kernel version", System.getProperty("os.version", "undefined"));
        } catch (SecurityException e) {
            Log.w("Profilo/DeviceInfo", AnonymousClass08S.A0J("SecurityException: ", e.getMessage()));
        }
        if (new File("/proc/sys/kernel/perf_event_paranoid").exists()) {
            j = 1;
        } else {
            j = 0;
        }
        A01(8126490, j);
    }

    public void logOnTraceEnd(TraceContext traceContext, C004505k r2) {
        A00();
    }
}
