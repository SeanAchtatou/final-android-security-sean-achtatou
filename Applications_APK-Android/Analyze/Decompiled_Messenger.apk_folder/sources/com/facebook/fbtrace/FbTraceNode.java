package com.facebook.fbtrace;

import X.AnonymousClass08S;
import X.AnonymousClass0HU;
import X.C10820ku;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public final class FbTraceNode implements Parcelable {
    public static final FbTraceNode A03 = new FbTraceNode("invalid_id", "invalid_id", "invalid_id");
    public static final Class A04 = FbTraceNode.class;
    public static final Parcelable.Creator CREATOR = new C10820ku();
    public final String A00;
    public final String A01;
    public final String A02;

    public int describeContents() {
        return 0;
    }

    public static FbTraceNode A00(FbTraceNode fbTraceNode) {
        if (Objects.equal(fbTraceNode, A03)) {
            return A03;
        }
        return new FbTraceNode(fbTraceNode.A02, AnonymousClass0HU.A01(), fbTraceNode.A00);
    }

    public static FbTraceNode A01(String str) {
        if (Objects.equal(str, "invalid_id")) {
            return A03;
        }
        return new FbTraceNode(str, AnonymousClass0HU.A01(), null);
    }

    public String A02() {
        return AnonymousClass08S.A0J(this.A02, this.A00);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }

    public FbTraceNode(String str, String str2, String str3) {
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(str2);
        this.A02 = str;
        this.A00 = str2;
        this.A01 = str3;
    }
}
