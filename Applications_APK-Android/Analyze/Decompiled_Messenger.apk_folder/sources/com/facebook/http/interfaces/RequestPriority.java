package com.facebook.http.interfaces;

import X.C10940l7;
import android.os.Parcel;
import android.os.Parcelable;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class RequestPriority extends Enum implements Parcelable {
    public static final RequestPriority A00;
    public static final Class A01 = RequestPriority.class;
    private static final /* synthetic */ RequestPriority[] A02;
    public static final RequestPriority A03;
    public static final RequestPriority A04;
    public static final RequestPriority A05;
    public static final RequestPriority A06;
    public static final Parcelable.Creator CREATOR = new C10940l7();
    public final int requestPriority;

    public int describeContents() {
        return 0;
    }

    static {
        RequestPriority requestPriority2 = new RequestPriority("INTERACTIVE", 0, 0);
        A04 = requestPriority2;
        RequestPriority requestPriority3 = new RequestPriority("NON_INTERACTIVE", 1, 1);
        A05 = requestPriority3;
        RequestPriority requestPriority4 = new RequestPriority("CAN_WAIT", 2, 2);
        A03 = requestPriority4;
        RequestPriority requestPriority5 = new RequestPriority("UNNECESSARY", 3, 3);
        RequestPriority requestPriority6 = new RequestPriority("UNUSED_REQUEST_PRIORITY", 4, 999);
        A06 = requestPriority6;
        A02 = new RequestPriority[]{requestPriority2, requestPriority3, requestPriority4, requestPriority5, requestPriority6};
        A00 = requestPriority3;
    }

    public static RequestPriority valueOf(String str) {
        return (RequestPriority) Enum.valueOf(RequestPriority.class, str);
    }

    public static RequestPriority[] values() {
        return (RequestPriority[]) A02.clone();
    }

    private RequestPriority(String str, int i, int i2) {
        this.requestPriority = i2;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name());
    }
}
