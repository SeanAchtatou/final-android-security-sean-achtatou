package com.facebook.animated.webp;

import X.AnonymousClass07B;
import X.AnonymousClass9K8;
import X.C000700l;
import X.C05520Zg;
import X.C23031Ny;
import X.C23041Nz;
import X.C23541Px;
import X.C80983tZ;
import X.DJ8;
import java.nio.ByteBuffer;

public class WebPImage implements C23031Ny, C23041Nz {
    private long mNativeContext;

    public static native WebPImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer);

    private static native WebPImage nativeCreateFromNativeMemory(long j, int i);

    private native void nativeDispose();

    private native void nativeFinalize();

    private native int nativeGetDuration();

    /* access modifiers changed from: private */
    /* renamed from: nativeGetFrame */
    public native WebPFrame getFrame(int i);

    private native int nativeGetFrameCount();

    private native int[] nativeGetFrameDurations();

    private native int nativeGetHeight();

    private native int nativeGetLoopCount();

    private native int nativeGetSizeInBytes();

    private native int nativeGetWidth();

    public boolean doesRenderSupportScaling() {
        return true;
    }

    public void finalize() {
        int A03 = C000700l.A03(-1112863209);
        nativeFinalize();
        C000700l.A09(461527715, A03);
    }

    public int getDuration() {
        return nativeGetDuration();
    }

    public int getFrameCount() {
        return nativeGetFrameCount();
    }

    public int[] getFrameDurations() {
        return nativeGetFrameDurations();
    }

    public C80983tZ getFrameInfo(int i) {
        Integer num;
        DJ8 dj8;
        WebPFrame nativeGetFrame = getFrame(i);
        try {
            int xOffset = nativeGetFrame.getXOffset();
            int yOffset = nativeGetFrame.getYOffset();
            int width = nativeGetFrame.getWidth();
            int height = nativeGetFrame.getHeight();
            if (nativeGetFrame.isBlendWithPreviousFrame()) {
                num = AnonymousClass07B.A00;
            } else {
                num = AnonymousClass07B.A01;
            }
            if (nativeGetFrame.shouldDisposeToBackgroundColor()) {
                dj8 = DJ8.A01;
            } else {
                dj8 = DJ8.A00;
            }
            return new C80983tZ(xOffset, yOffset, width, height, num, dj8);
        } finally {
            nativeGetFrame.dispose();
        }
    }

    public int getHeight() {
        return nativeGetHeight();
    }

    public int getLoopCount() {
        return nativeGetLoopCount();
    }

    public int getSizeInBytes() {
        return nativeGetSizeInBytes();
    }

    public int getWidth() {
        return nativeGetWidth();
    }

    public void dispose() {
        nativeDispose();
    }

    public WebPImage() {
    }

    public WebPImage(long j) {
        this.mNativeContext = j;
    }

    public C23031Ny decode(long j, int i, C23541Px r8) {
        AnonymousClass9K8.A00();
        boolean z = false;
        if (j != 0) {
            z = true;
        }
        C05520Zg.A04(z);
        return nativeCreateFromNativeMemory(j, i);
    }

    public C23031Ny decode(ByteBuffer byteBuffer, C23541Px r3) {
        AnonymousClass9K8.A00();
        byteBuffer.rewind();
        return nativeCreateFromDirectByteBuffer(byteBuffer);
    }
}
