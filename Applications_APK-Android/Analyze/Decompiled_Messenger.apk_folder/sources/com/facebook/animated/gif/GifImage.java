package com.facebook.animated.gif;

import X.AnonymousClass01u;
import X.AnonymousClass07B;
import X.C000700l;
import X.C05520Zg;
import X.C23031Ny;
import X.C23041Nz;
import X.C23541Px;
import X.C80983tZ;
import X.DJ8;
import java.nio.ByteBuffer;

public class GifImage implements C23031Ny, C23041Nz {
    private static volatile boolean sInitialized;
    private long mNativeContext;

    public static native GifImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer, int i, boolean z);

    private static native GifImage nativeCreateFromNativeMemory(long j, int i, int i2, boolean z);

    private native void nativeDispose();

    private native void nativeFinalize();

    private native int nativeGetDuration();

    /* access modifiers changed from: private */
    /* renamed from: nativeGetFrame */
    public native GifFrame getFrame(int i);

    private native int nativeGetFrameCount();

    private native int[] nativeGetFrameDurations();

    private native int nativeGetHeight();

    private native int nativeGetLoopCount();

    private native int nativeGetSizeInBytes();

    private native int nativeGetWidth();

    public boolean doesRenderSupportScaling() {
        return false;
    }

    public static synchronized void ensure() {
        synchronized (GifImage.class) {
            if (!sInitialized) {
                sInitialized = true;
                AnonymousClass01u.A01("gifimage");
            }
        }
    }

    public void finalize() {
        int A03 = C000700l.A03(-535249900);
        nativeFinalize();
        C000700l.A09(-1983417286, A03);
    }

    public int getDuration() {
        return nativeGetDuration();
    }

    public int getFrameCount() {
        return nativeGetFrameCount();
    }

    public int[] getFrameDurations() {
        return nativeGetFrameDurations();
    }

    public C80983tZ getFrameInfo(int i) {
        DJ8 dj8;
        GifFrame nativeGetFrame = getFrame(i);
        try {
            int xOffset = nativeGetFrame.getXOffset();
            int yOffset = nativeGetFrame.getYOffset();
            int width = nativeGetFrame.getWidth();
            int height = nativeGetFrame.getHeight();
            Integer num = AnonymousClass07B.A00;
            int disposalMode = nativeGetFrame.getDisposalMode();
            if (disposalMode == 0) {
                dj8 = DJ8.A00;
            } else if (disposalMode == 1) {
                dj8 = DJ8.A00;
            } else if (disposalMode == 2) {
                dj8 = DJ8.A01;
            } else if (disposalMode == 3) {
                dj8 = DJ8.A02;
            } else {
                dj8 = DJ8.A00;
            }
            return new C80983tZ(xOffset, yOffset, width, height, num, dj8);
        } finally {
            nativeGetFrame.dispose();
        }
    }

    public int getHeight() {
        return nativeGetHeight();
    }

    public int getLoopCount() {
        int nativeGetLoopCount = nativeGetLoopCount();
        if (nativeGetLoopCount == -1) {
            return 1;
        }
        if (nativeGetLoopCount != 0) {
            return nativeGetLoopCount + 1;
        }
        return 0;
    }

    public int getSizeInBytes() {
        return nativeGetSizeInBytes();
    }

    public int getWidth() {
        return nativeGetWidth();
    }

    public void dispose() {
        nativeDispose();
    }

    public GifImage() {
    }

    public GifImage(long j) {
        this.mNativeContext = j;
    }

    public C23031Ny decode(long j, int i, C23541Px r8) {
        ensure();
        boolean z = false;
        if (j != 0) {
            z = true;
        }
        C05520Zg.A04(z);
        return nativeCreateFromNativeMemory(j, i, r8.A00, r8.A08);
    }

    public C23031Ny decode(ByteBuffer byteBuffer, C23541Px r4) {
        ensure();
        byteBuffer.rewind();
        return nativeCreateFromDirectByteBuffer(byteBuffer, r4.A00, r4.A08);
    }
}
