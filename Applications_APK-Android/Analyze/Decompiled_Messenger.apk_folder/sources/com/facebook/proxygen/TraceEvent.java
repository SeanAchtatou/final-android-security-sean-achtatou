package com.facebook.proxygen;

import X.AnonymousClass08S;
import X.C000700l;
import java.util.Map;

public class TraceEvent extends NativeHandleImpl {
    public final long mEnd;
    private final int mID;
    public final String mName;
    private final int mParentID;
    public final long mStart;

    public native void close();

    public native Map getMetaData();

    public long getEnd() {
        return this.mEnd;
    }

    public int getId() {
        return this.mID;
    }

    public String getName() {
        return this.mName;
    }

    public int getParentID() {
        return this.mParentID;
    }

    public long getStart() {
        return this.mStart;
    }

    public String toPrettyJson() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{\n  \"name\":\"" + this.mName + "\",\n" + "  \"id\":" + this.mID + ",\n" + "  \"parentID\":" + this.mParentID + ",\n" + "  \"start\":" + this.mStart + ",\n" + "  \"end\":" + this.mEnd + ",\n" + "  \"metaData\":{\n");
        boolean z = true;
        for (Map.Entry entry : getMetaData().entrySet()) {
            if (!z) {
                stringBuffer.append(",\n");
            }
            stringBuffer.append(AnonymousClass08S.A0P("    \"", (String) entry.getKey(), "\":\""));
            int length = ((String) entry.getValue()).length();
            String str2 = (String) entry.getValue();
            if (length > 100) {
                str2 = str2.substring(0, 97);
                str = "...\"";
            } else {
                str = "\"";
            }
            stringBuffer.append(AnonymousClass08S.A0J(str2, str));
            z = false;
        }
        stringBuffer.append("\n  }\n}");
        return stringBuffer.toString();
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("TraceEvent(name='" + this.mName + "', " + "id='" + this.mID + "', " + "parentID='" + this.mParentID + "', " + "start='" + this.mStart + "', " + "end='" + this.mEnd + "', " + "metaData='{");
        for (Map.Entry entry : getMetaData().entrySet()) {
            stringBuffer.append(AnonymousClass08S.A0S((String) entry.getKey(), ": ", (String) entry.getValue(), ", "));
        }
        stringBuffer.append("}')");
        return stringBuffer.toString();
    }

    public void finalize() {
        int A03 = C000700l.A03(1705944894);
        close();
        super.finalize();
        C000700l.A09(1066213217, A03);
    }

    public TraceEvent(String str) {
        this.mName = str;
        this.mID = 0;
        this.mParentID = 0;
        this.mStart = 0;
        this.mEnd = 0;
    }

    public TraceEvent(String str, int i, int i2, long j, long j2) {
        this.mName = str;
        this.mID = i;
        this.mParentID = i2;
        this.mStart = j;
        this.mEnd = j2;
    }
}
