package com.facebook.proxygen;

public class HTTPRequestError {
    public ProxygenError mErrCode;
    public String mErrMsg;
    private HTTPRequestStage mErrStage;

    public enum HTTPRequestStage {
        ProcessRequest,
        DNSResolution,
        TCPConnection,
        TLSSetup,
        SendRequest,
        RecvResponse,
        Unknown,
        ZeroRttSent,
        WaitingRequest,
        RecvRequest,
        SendResponse,
        Max
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public final class ProxygenError extends Enum {
        private static final /* synthetic */ ProxygenError[] $VALUES;
        public static final ProxygenError AddressFamilyNotSupported;
        public static final ProxygenError AddressPrivate;
        public static final ProxygenError AuthRequired;
        public static final ProxygenError BadDecompress;
        public static final ProxygenError BadSocket;
        public static final ProxygenError Canceled;
        public static final ProxygenError ClientRenegotiation;
        public static final ProxygenError ClientSilent;
        public static final ProxygenError ClientTransactionGone;
        public static final ProxygenError Configuration;
        public static final ProxygenError ConnRefused;
        public static final ProxygenError Connect;
        public static final ProxygenError ConnectTimeout;
        public static final ProxygenError Connection;
        public static final ProxygenError ConnectionReset;
        public static final ProxygenError CreatingStream;
        public static final ProxygenError DNSNoResults;
        public static final ProxygenError DNSOtherCancelled;
        public static final ProxygenError DNSOtherClient;
        public static final ProxygenError DNSOtherServer;
        public static final ProxygenError DNSgetaddrinfo;
        public static final ProxygenError DNSshutdown;
        public static final ProxygenError DNSthreadpool;
        public static final ProxygenError DNSunimplemented;
        public static final ProxygenError Dropped;
        public static final ProxygenError DuplicatedStreamId;
        public static final ProxygenError EOF;
        public static final ProxygenError EarlyDataFailed;
        public static final ProxygenError EarlyDataRejected;
        public static final ProxygenError EgressEOMSeenOnParentStream;
        public static final ProxygenError Forbidden;
        public static final ProxygenError Handshake;
        public static final ProxygenError IngressStateTransition;
        public static final ProxygenError InvalidRedirect;
        public static final ProxygenError MalformedInput;
        public static final ProxygenError Max;
        public static final ProxygenError MaxConcurrentOutgoingStreamLimitReached;
        public static final ProxygenError MaxConnects;
        public static final ProxygenError MaxRedirects;
        public static final ProxygenError Message;
        public static final ProxygenError MethodNotSupported;
        public static final ProxygenError Network;
        public static final ProxygenError NetworkSwitch;
        public static final ProxygenError NoServer;
        public static final ProxygenError None;
        public static final ProxygenError ParentStreamNotExist;
        public static final ProxygenError ParseBody;
        public static final ProxygenError ParseHeader;
        public static final ProxygenError ParseResponse;
        public static final ProxygenError PushNotSupported;
        public static final ProxygenError Read;
        public static final ProxygenError ResponseAction;
        public static final ProxygenError SSL;
        public static final ProxygenError Shutdown;
        public static final ProxygenError StreamAbort;
        public static final ProxygenError StreamUnacknowledged;
        public static final ProxygenError Timeout;
        public static final ProxygenError TransportIsDraining;
        public static final ProxygenError Unauthorized;
        public static final ProxygenError Unknown;
        public static final ProxygenError UnsupportedExpectation;
        public static final ProxygenError UnsupportedScheme;
        public static final ProxygenError Write;
        public static final ProxygenError WriteTimeout;

        static {
            ProxygenError proxygenError = new ProxygenError("None", 0);
            None = proxygenError;
            ProxygenError proxygenError2 = new ProxygenError("Message", 1);
            Message = proxygenError2;
            ProxygenError proxygenError3 = new ProxygenError("Connect", 2);
            Connect = proxygenError3;
            ProxygenError proxygenError4 = new ProxygenError("ConnectTimeout", 3);
            ConnectTimeout = proxygenError4;
            ProxygenError proxygenError5 = new ProxygenError("Read", 4);
            Read = proxygenError5;
            ProxygenError proxygenError6 = new ProxygenError("Write", 5);
            Write = proxygenError6;
            ProxygenError proxygenError7 = new ProxygenError("Timeout", 6);
            Timeout = proxygenError7;
            ProxygenError proxygenError8 = new ProxygenError("Handshake", 7);
            Handshake = proxygenError8;
            ProxygenError proxygenError9 = new ProxygenError("NoServer", 8);
            NoServer = proxygenError9;
            ProxygenError proxygenError10 = new ProxygenError("MaxRedirects", 9);
            MaxRedirects = proxygenError10;
            ProxygenError proxygenError11 = new ProxygenError("InvalidRedirect", 10);
            InvalidRedirect = proxygenError11;
            ProxygenError proxygenError12 = new ProxygenError("ResponseAction", 11);
            ResponseAction = proxygenError12;
            ProxygenError proxygenError13 = new ProxygenError("MaxConnects", 12);
            MaxConnects = proxygenError13;
            ProxygenError proxygenError14 = new ProxygenError("Dropped", 13);
            Dropped = proxygenError14;
            ProxygenError proxygenError15 = new ProxygenError("Connection", 14);
            Connection = proxygenError15;
            ProxygenError proxygenError16 = new ProxygenError("ConnectionReset", 15);
            ConnectionReset = proxygenError16;
            ProxygenError proxygenError17 = new ProxygenError("ParseHeader", 16);
            ParseHeader = proxygenError17;
            ProxygenError proxygenError18 = new ProxygenError("ParseBody", 17);
            ParseBody = proxygenError18;
            ProxygenError proxygenError19 = new ProxygenError("EOF", 18);
            EOF = proxygenError19;
            ProxygenError proxygenError20 = new ProxygenError("ClientRenegotiation", 19);
            ClientRenegotiation = proxygenError20;
            ProxygenError proxygenError21 = new ProxygenError("Unknown", 20);
            Unknown = proxygenError21;
            ProxygenError proxygenError22 = new ProxygenError("BadDecompress", 21);
            BadDecompress = proxygenError22;
            ProxygenError proxygenError23 = new ProxygenError("SSL", 22);
            SSL = proxygenError23;
            ProxygenError proxygenError24 = new ProxygenError("StreamAbort", 23);
            StreamAbort = proxygenError24;
            ProxygenError proxygenError25 = new ProxygenError("StreamUnacknowledged", 24);
            StreamUnacknowledged = proxygenError25;
            ProxygenError proxygenError26 = new ProxygenError("WriteTimeout", 25);
            WriteTimeout = proxygenError26;
            ProxygenError proxygenError27 = new ProxygenError("AddressPrivate", 26);
            AddressPrivate = proxygenError27;
            ProxygenError proxygenError28 = new ProxygenError("AddressFamilyNotSupported", 27);
            AddressFamilyNotSupported = proxygenError28;
            ProxygenError proxygenError29 = new ProxygenError("DNSNoResults", 28);
            DNSNoResults = proxygenError29;
            ProxygenError proxygenError30 = new ProxygenError("MalformedInput", 29);
            MalformedInput = proxygenError30;
            ProxygenError proxygenError31 = new ProxygenError("UnsupportedExpectation", 30);
            UnsupportedExpectation = proxygenError31;
            ProxygenError proxygenError32 = new ProxygenError("MethodNotSupported", 31);
            MethodNotSupported = proxygenError32;
            ProxygenError proxygenError33 = new ProxygenError("UnsupportedScheme", 32);
            UnsupportedScheme = proxygenError33;
            ProxygenError proxygenError34 = new ProxygenError("Shutdown", 33);
            Shutdown = proxygenError34;
            ProxygenError proxygenError35 = new ProxygenError("IngressStateTransition", 34);
            IngressStateTransition = proxygenError35;
            ProxygenError proxygenError36 = new ProxygenError("ClientSilent", 35);
            ClientSilent = proxygenError36;
            ProxygenError proxygenError37 = new ProxygenError("Canceled", 36);
            Canceled = proxygenError37;
            ProxygenError proxygenError38 = new ProxygenError("ParseResponse", 37);
            ParseResponse = proxygenError38;
            ProxygenError proxygenError39 = new ProxygenError("ConnRefused", 38);
            ConnRefused = proxygenError39;
            ProxygenError proxygenError40 = new ProxygenError("DNSOtherServer", 39);
            DNSOtherServer = proxygenError40;
            ProxygenError proxygenError41 = new ProxygenError("DNSOtherClient", 40);
            DNSOtherClient = proxygenError41;
            ProxygenError proxygenError42 = new ProxygenError("DNSOtherCancelled", 41);
            DNSOtherCancelled = proxygenError42;
            ProxygenError proxygenError43 = new ProxygenError("DNSshutdown", 42);
            DNSshutdown = proxygenError43;
            ProxygenError proxygenError44 = new ProxygenError("DNSgetaddrinfo", 43);
            DNSgetaddrinfo = proxygenError44;
            ProxygenError proxygenError45 = new ProxygenError("DNSthreadpool", 44);
            DNSthreadpool = proxygenError45;
            ProxygenError proxygenError46 = new ProxygenError("DNSunimplemented", 45);
            DNSunimplemented = proxygenError46;
            ProxygenError proxygenError47 = new ProxygenError("Network", 46);
            Network = proxygenError47;
            ProxygenError proxygenError48 = new ProxygenError("Configuration", 47);
            Configuration = proxygenError48;
            ProxygenError proxygenError49 = new ProxygenError("EarlyDataRejected", 48);
            EarlyDataRejected = proxygenError49;
            ProxygenError proxygenError50 = new ProxygenError("EarlyDataFailed", 49);
            EarlyDataFailed = proxygenError50;
            ProxygenError proxygenError51 = new ProxygenError("AuthRequired", 50);
            AuthRequired = proxygenError51;
            ProxygenError proxygenError52 = new ProxygenError("Unauthorized", 51);
            Unauthorized = proxygenError52;
            ProxygenError proxygenError53 = new ProxygenError("EgressEOMSeenOnParentStream", 52);
            EgressEOMSeenOnParentStream = proxygenError53;
            ProxygenError proxygenError54 = new ProxygenError("TransportIsDraining", 53);
            TransportIsDraining = proxygenError54;
            ProxygenError proxygenError55 = new ProxygenError("ParentStreamNotExist", 54);
            ParentStreamNotExist = proxygenError55;
            ProxygenError proxygenError56 = new ProxygenError("CreatingStream", 55);
            CreatingStream = proxygenError56;
            ProxygenError proxygenError57 = new ProxygenError("PushNotSupported", 56);
            PushNotSupported = proxygenError57;
            ProxygenError proxygenError58 = new ProxygenError("MaxConcurrentOutgoingStreamLimitReached", 57);
            MaxConcurrentOutgoingStreamLimitReached = proxygenError58;
            ProxygenError proxygenError59 = new ProxygenError("BadSocket", 58);
            BadSocket = proxygenError59;
            ProxygenError proxygenError60 = new ProxygenError("DuplicatedStreamId", 59);
            DuplicatedStreamId = proxygenError60;
            ProxygenError proxygenError61 = new ProxygenError("ClientTransactionGone", 60);
            ClientTransactionGone = proxygenError61;
            ProxygenError proxygenError62 = new ProxygenError("NetworkSwitch", 61);
            NetworkSwitch = proxygenError62;
            ProxygenError proxygenError63 = new ProxygenError("Forbidden", 62);
            Forbidden = proxygenError63;
            ProxygenError proxygenError64 = new ProxygenError("Max", 63);
            Max = proxygenError64;
            ProxygenError[] proxygenErrorArr = new ProxygenError[64];
            System.arraycopy(new ProxygenError[]{proxygenError, proxygenError2, proxygenError3, proxygenError4, proxygenError5, proxygenError6, proxygenError7, proxygenError8, proxygenError9, proxygenError10, proxygenError11, proxygenError12, proxygenError13, proxygenError14, proxygenError15, proxygenError16, proxygenError17, proxygenError18, proxygenError19, proxygenError20, proxygenError21, proxygenError22, proxygenError23, proxygenError24, proxygenError25, proxygenError26, proxygenError27}, 0, proxygenErrorArr, 0, 27);
            System.arraycopy(new ProxygenError[]{proxygenError28, proxygenError29, proxygenError30, proxygenError31, proxygenError32, proxygenError33, proxygenError34, proxygenError35, proxygenError36, proxygenError37, proxygenError38, proxygenError39, proxygenError40, proxygenError41, proxygenError42, proxygenError43, proxygenError44, proxygenError45, proxygenError46, proxygenError47, proxygenError48, proxygenError49, proxygenError50, proxygenError51, proxygenError52, proxygenError53, proxygenError54}, 0, proxygenErrorArr, 27, 27);
            System.arraycopy(new ProxygenError[]{proxygenError55, proxygenError56, proxygenError57, proxygenError58, proxygenError59, proxygenError60, proxygenError61, proxygenError62, proxygenError63, proxygenError64}, 0, proxygenErrorArr, 54, 10);
            $VALUES = proxygenErrorArr;
        }

        public static ProxygenError valueOf(String str) {
            return (ProxygenError) Enum.valueOf(ProxygenError.class, str);
        }

        public static ProxygenError[] values() {
            return (ProxygenError[]) $VALUES.clone();
        }

        private ProxygenError(String str, int i) {
        }
    }

    public ProxygenError getErrCode() {
        return this.mErrCode;
    }

    public String getErrMsg() {
        return this.mErrMsg;
    }

    public HTTPRequestStage getErrStage() {
        return this.mErrStage;
    }

    public HTTPRequestError(String str, HTTPRequestStage hTTPRequestStage, ProxygenError proxygenError) {
        this.mErrMsg = str;
        this.mErrStage = hTTPRequestStage;
        this.mErrCode = proxygenError;
    }
}
