package com.facebook.proxygen;

public class ConnectionParams {
    public String clientId;
    public boolean enableTopicEncoding;
    public int keepaliveSecs;
    public PublishFormat publishFormat;

    public enum PublishFormat {
        RAW(0),
        ZLIB(1),
        ZLIB_OPTIONAL(2);
        
        private final int value;

        public int getValue() {
            return this.value;
        }

        private PublishFormat(int i) {
            this.value = i;
        }
    }

    public ConnectionParams setClientId(String str) {
        this.clientId = str;
        return this;
    }

    public ConnectionParams setEnableTopicEncoding(boolean z) {
        this.enableTopicEncoding = z;
        return this;
    }

    public ConnectionParams setKeepaliveSec(int i) {
        this.keepaliveSecs = i;
        return this;
    }

    public ConnectionParams setPublishFormat(PublishFormat publishFormat2) {
        this.publishFormat = publishFormat2;
        return this;
    }
}
