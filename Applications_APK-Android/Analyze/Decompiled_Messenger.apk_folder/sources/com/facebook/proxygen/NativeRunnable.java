package com.facebook.proxygen;

import X.C000700l;

public class NativeRunnable extends NativeHandleImpl implements Runnable {
    public native void close();

    public native void run();

    public void finalize() {
        int A03 = C000700l.A03(-1202878750);
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(1283126462, A03);
        }
    }
}
