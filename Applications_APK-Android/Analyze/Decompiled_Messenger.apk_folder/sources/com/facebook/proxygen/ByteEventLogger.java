package com.facebook.proxygen;

public interface ByteEventLogger {
    void onBytesReceived(String str, long j);

    void onBytesSent(String str, long j);
}
