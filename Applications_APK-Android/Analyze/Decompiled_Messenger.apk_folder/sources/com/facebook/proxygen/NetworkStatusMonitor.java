package com.facebook.proxygen;

import X.AnonymousClass1Y3;
import X.C000700l;
import java.util.HashMap;

public class NetworkStatusMonitor extends NativeHandleImpl {
    private BandwidthEstimate mBandwidthEstimate;
    private int mBandwidthSplitSize = 10000;
    public int mCacheDurationInSeconds = 2;
    private final NetworkStatus mCacheStatus;
    private boolean mCalcBandwidthOnEvbThreadEnabled = true;
    private final EventBase mEventBase;
    private int mGoodDL = 2000000;
    private int mGoodRtt = 50;
    private int mGoodUL = 400000;
    private boolean mInitialized;
    private int mMaxPriority = 7;
    private int mModerateDL = 550000;
    private int mModerateRtt = AnonymousClass1Y3.A1B;
    private int mModerateUL = 110000;
    private int mPoorDL = 150000;
    private int mPoorRtt = 1500;
    private int mPoorUL = 30000;
    private ProxygenRadioMeter mRadioMeter;

    private native BandwidthEstimate getBandwidthEstimateNative();

    private native void getNetworkStatusNative();

    public native void close();

    public native HashMap getAccumulativeRadioCounterData();

    public native long getConnectionLevelTraceDurationNative();

    public native long[] getInboundConnectionLevelTraceDataNative();

    public native long[] getOutboundConnectionLevelTraceDataNative();

    public native HashMap getRadioData();

    public native void init(EventBase eventBase, ProxygenRadioMeter proxygenRadioMeter, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, boolean z);

    public native void setNetworkType(int i, int i2);

    public native long startConnectionLevelTracingNative();

    public native void stopConnectionLevelTracingNative();

    public BandwidthEstimate getBandwidthEstimate() {
        if (!this.mInitialized) {
            return null;
        }
        BandwidthEstimate bandwidthEstimateNative = getBandwidthEstimateNative();
        this.mBandwidthEstimate = bandwidthEstimateNative;
        return bandwidthEstimateNative;
    }

    public NetworkStatus getNetworkStatus() {
        if (!this.mInitialized) {
            return null;
        }
        getNetworkStatusNative();
        return this.mCacheStatus;
    }

    public NetworkStatusMonitor(EventBase eventBase) {
        this.mEventBase = eventBase;
    }

    public void finalize() {
        int A03 = C000700l.A03(-634317542);
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(1406549435, A03);
        }
    }

    public void setCacheDuration(int i) {
        this.mCacheDurationInSeconds = i;
    }

    public void setCalcBandwidthOnEvbThreadEnabled(boolean z) {
        this.mCalcBandwidthOnEvbThreadEnabled = z;
    }

    public void setRadioMeter(ProxygenRadioMeter proxygenRadioMeter) {
        this.mRadioMeter = proxygenRadioMeter;
    }

    public void init(int i) {
        EventBase eventBase = this.mEventBase;
        ProxygenRadioMeter proxygenRadioMeter = this.mRadioMeter;
        int i2 = this.mMaxPriority;
        int i3 = this.mBandwidthSplitSize;
        int i4 = this.mCacheDurationInSeconds;
        int i5 = this.mPoorRtt;
        int i6 = this.mModerateRtt;
        int i7 = this.mGoodRtt;
        int i8 = this.mPoorUL;
        int i9 = this.mModerateUL;
        int i10 = this.mGoodUL;
        int i11 = this.mPoorDL;
        int i12 = this.mModerateDL;
        int i13 = i12;
        int i14 = i11;
        int i15 = i10;
        int i16 = i9;
        int i17 = i8;
        int i18 = i7;
        int i19 = i6;
        int i20 = i5;
        int i21 = i4;
        int i22 = i3;
        int i23 = i2;
        init(eventBase, proxygenRadioMeter, i, i23, i22, i21, i20, i19, i18, i17, i16, i15, i14, i13, this.mGoodDL, this.mCalcBandwidthOnEvbThreadEnabled);
        this.mInitialized = true;
    }
}
