package com.facebook.proxygen;

import X.C000700l;
import io.card.payment.BuildConfig;
import java.util.concurrent.Executor;

public class HTTPClient implements NativeHandle {
    public AnalyticsLogger mAnalyticsLogger;
    private final boolean mAttemptEarlyDataInQuicPreconnect;
    private final double mBackgroundDNSSampleRate;
    private String mBypassProxyDomains;
    private final String mCdnOverride;
    private final boolean mCircularLogSinkEnabled;
    private boolean mClosed;
    private final ConnQualityConfig mConnQualityConfig;
    private final Executor mDNSResolverExecutor;
    private final boolean mDeadConnTimeoutDryRun;
    private final boolean mDisableTcpPreconnectWithQuic;
    private final boolean mDnsCacheEnabled;
    private final int mDnsRequestsOutstanding;
    private final boolean mEnableBackgroundQuicConnection;
    private final boolean mEnableDzCompression;
    private final boolean mEnableHTTP2;
    private final boolean mEnableHTTP2Weights;
    private final boolean mEnableHTTP3;
    private final boolean mEnableLigerPreconnect;
    private final boolean mEnableLigerWorkerThread;
    private final boolean mEnableLithiumZstd;
    private final boolean mEnablePreconnectWithQuic;
    private final boolean mEnableSimplePreconnectForReinit;
    private final boolean mEnableTransportCallbacks;
    private final boolean mEnableZstd;
    private final int mEvbLoggingSamplingFreq;
    public final EventBase mEventBase;
    private final boolean mFBHostOnly;
    private final boolean mFizzHTTP2StaticOverride;
    private final FizzSettings mFizzSettings;
    private final boolean mFlowControlSelectedTxnsOnly;
    private final String mH2PubSubHostnames;
    private final int mHTTP2WeightHighPri;
    private final int mHTTP2WeightLowPri;
    private final int mHTTPSessionMaxReadBufferSize;
    private final long mHappyEyeballsConnectionDelayMillis;
    private final int mIdleHTTPSessionsLowWaterMark;
    private final int mIdleTimeoutForUnusedQuicSession;
    private final int mIdleTimeoutForUnusedTCPSession;
    private final int mIdleTimeoutForUsedQuicSession;
    private final int mIdleTimeoutForUsedTCPSession;
    private boolean mInitialized;
    private final boolean mInlinePersistenceLoading;
    private final boolean mIsCompressionFilterEnabled;
    private final boolean mIsHTTPSEnforced;
    private final boolean mIsPerDomainLimitEnabled;
    private final boolean mIsSSLSessionCacheEnabled;
    public boolean mIsSandbox;
    private final boolean mLargerFlowControlWindow;
    private final int mMaxConcurrentOutgoingStreams;
    private final int mMaxIdleHTTP2Sessions;
    private final int mMaxIdleHTTPSessions;
    private final int mMaxPriorityLevelForSession;
    public NetworkStatusMonitor mNetworkStatusMonitor;
    private final long mNewConnTimeoutMillis;
    private final int mPerDomainMaxConns;
    private final Executor mPersistentCachesExecutor;
    private final PersistentSSLCacheSettings mPersistentDNSCacheSettings;
    private final PersistentSSLCacheSettings mPersistentSSLCacheSettings;
    private final String mPingDomainBlacklist;
    private final int mPingTimeoutMaxMs;
    private final int mPingTimeoutMinMs;
    private final int mPingTimeoutRtoMultiple;
    private final int mPosixResolverPoolSize;
    private final String mPreConnects;
    private final String mPreconnectFilePath;
    private boolean mProxyFallbackEnabled;
    private String mProxyHost;
    private String mProxyPassword;
    private int mProxyPort;
    private String mProxyUsername;
    private final FizzSettings mQuicFizzSettings;
    private final QuicSettings mQuicSettings;
    private final long mQuicTraceLoggingSampleSalt;
    private final int mQuicTraceLoggingSampleWeight;
    private boolean mReInitToRefreshSettings;
    private final boolean mRedirectFilter;
    private final RootCACallbacks mRootCACallbacks;
    private final SSLKeyMaterialCallback mSSLKeyMaterialCallback;
    private final SSLVerificationSettings mSSLVerificationSettings;
    private String mSecureProxyHost;
    private String mSecureProxyPassword;
    private int mSecureProxyPort;
    private String mSecureProxyUsername;
    private final int mSessionFlowControlWindow;
    private final long mSessionWriteTimeoutMillis;
    private long mSettings;
    private final boolean mShouldOverrideFizzHTTP2Static;
    private final boolean mShouldOverrideTlsHTTP2;
    private final boolean mStaleAnswersEnabled;
    private final int mStreamFlowControlWindow;
    private final boolean mSupportH2PubSub;
    private final boolean mTlsHTTP2Override;
    private final long mTransactionIdleTimeoutMillis;
    private final boolean mUseInjectedPreconnect;
    private final boolean mUseLoadBalancing;
    private String mUserAgent;
    private byte[][] mUserInstalledCertificates;

    public class Builder {
        public AnalyticsLogger mAnalyticsLogger;
        public boolean mAttemptEarlyDataInQuicPreconnect = false;
        public double mBackgroundDNSSampleRate = 0.0d;
        public String mBypassProxyDomains;
        public String mCdnOverride = null;
        public boolean mCircularLogSinkEnabled = true;
        public final boolean mClosed;
        public ConnQualityConfig mConnQualityConfig;
        public Executor mDNSResolverExecutor;
        public boolean mDeadConnTimeoutDryRun;
        public boolean mDisableTcpPreconnectWithQuic = false;
        public boolean mDnsCacheEnabled = false;
        public int mDnsRequestsOutstanding = 1;
        public boolean mEnableBackgroundQuicConnection = true;
        public boolean mEnableDzCompression = false;
        public boolean mEnableHTTP2 = true;
        public boolean mEnableHTTP2Weights = false;
        public boolean mEnableHTTP3 = false;
        public boolean mEnableLigerPreconnect = false;
        public boolean mEnableLigerWorkerThread = false;
        public boolean mEnableLithiumZstd = false;
        public boolean mEnablePreconnectWithQuic = false;
        public boolean mEnableSimplePreconnectForReinit = false;
        public boolean mEnableTransportCallbacks = false;
        public boolean mEnableZstd = false;
        public int mEvbLoggingSamplingFreq = 0;
        public EventBase mEventBase;
        public boolean mFBHostOnly = false;
        public boolean mFizzHTTP2StaticOverride = true;
        public FizzSettings mFizzSettings;
        public boolean mFlowControlSelectedTxnsOnly = false;
        public String mH2PubSubHostnames = BuildConfig.FLAVOR;
        public int mHTTP2WeightHighPri = 0;
        public int mHTTP2WeightLowPri = 0;
        public int mHTTPSessionMaxReadBufferSize = 0;
        public long mHappyEyeballsConnectionDelayMillis = 150;
        public int mIdleHTTPSessionsLowWaterMark = 0;
        public int mIdleTimeoutForUnusedQuicSession = 55000;
        public int mIdleTimeoutForUnusedTCPSession = 55000;
        public int mIdleTimeoutForUsedQuicSession = 55000;
        public int mIdleTimeoutForUsedTCPSession = 55000;
        public final boolean mInitialized;
        public boolean mInlinePersistenceLoading = true;
        public boolean mIsCompressionFilterEnabled = true;
        public boolean mIsHTTPSEnforced = false;
        public boolean mIsPerDomainLimitEnabled = false;
        public boolean mIsSSLSessionCacheEnabled = true;
        public boolean mIsSandbox = false;
        public boolean mLargerFlowControlWindow = false;
        public int mMaxConcurrentOutgoingStreams = 0;
        public int mMaxIdleHTTP2Sessions = 2;
        public int mMaxIdleHTTPSessions = 6;
        public int mMaxPriorityLevelForSession = 0;
        public NetworkStatusMonitor mNetworkStatusMonitor;
        public long mNewConnTimeoutMillis = 30000;
        public int mPerDomainMaxConns = 0;
        public Executor mPersistentCachesExecutor;
        public PersistentSSLCacheSettings mPersistentDNSCacheSettings;
        public PersistentSSLCacheSettings mPersistentSSLCacheSettings;
        public String mPingDomainBlacklist;
        public int mPingTimeoutMaxMs;
        public int mPingTimeoutMinMs;
        public int mPingTimeoutRtoMultiple;
        public int mPosixResolverPoolSize = 4;
        public String mPreConnects;
        public String mPreconnectFilePath = BuildConfig.FLAVOR;
        public boolean mProxyFallbackEnabled;
        public String mProxyHost;
        public String mProxyPassword;
        public int mProxyPort;
        public String mProxyUsername;
        public FizzSettings mQuicFizzSettings;
        public QuicSettings mQuicSettings;
        public long mQuicTraceLoggingSampleSalt = 0;
        public int mQuicTraceLoggingSampleWeight = 0;
        public boolean mReInitToRefreshSettings;
        public boolean mRedirectFilter = false;
        public RootCACallbacks mRootCACallbacks;
        public SSLKeyMaterialCallback mSSLKeyMaterialCallback;
        public SSLVerificationSettings mSSLVerificationSettings;
        public String mSecureProxyHost;
        public String mSecureProxyPassword;
        public int mSecureProxyPort;
        public String mSecureProxyUsername;
        public int mSessionFlowControlWindow = 65535;
        public long mSessionWriteTimeoutMillis = 60000;
        public final long mSettings;
        public boolean mShouldOverrideFizzHTTP2Static = false;
        public boolean mShouldOverrideTlsHTTP2 = false;
        public boolean mStaleAnswersEnabled = true;
        public int mStreamFlowControlWindow = 65535;
        public boolean mSupportH2PubSub = false;
        public boolean mTlsHTTP2Override = true;
        public long mTransactionIdleTimeoutMillis = 60000;
        public boolean mUseInjectedPreconnect = false;
        public boolean mUseLoadBalancing = false;
        public String mUserAgent;
        public byte[][] mUserInstalledCertificates;

        public HTTPClient build() {
            if (this.mFBHostOnly || this.mRootCACallbacks != null) {
                return new HTTPClient(this);
            }
            throw new NullPointerException("You must call setRootCACallbacks().");
        }

        public Builder configurePingTimeout(int i, int i2, int i3, String str, boolean z) {
            this.mPingTimeoutMinMs = i;
            this.mPingTimeoutMaxMs = i2;
            this.mPingTimeoutRtoMultiple = i3;
            this.mPingDomainBlacklist = str;
            this.mDeadConnTimeoutDryRun = z;
            return this;
        }

        public Builder setBypassProxyDomains(String str) {
            if (HTTPClient.stringEquals(str, this.mBypassProxyDomains)) {
                return this;
            }
            this.mBypassProxyDomains = str;
            this.mReInitToRefreshSettings = true;
            return this;
        }

        public Builder setFizzHTTP2StaticOverride(boolean z, boolean z2) {
            this.mShouldOverrideFizzHTTP2Static = z;
            this.mFizzHTTP2StaticOverride = z2;
            return this;
        }

        public Builder setFlowControl(boolean z, int i, int i2, boolean z2) {
            this.mLargerFlowControlWindow = z;
            this.mSessionFlowControlWindow = i;
            this.mStreamFlowControlWindow = i2;
            this.mFlowControlSelectedTxnsOnly = z2;
            return this;
        }

        public Builder setIsSandbox(boolean z) {
            if (this.mIsSandbox != z) {
                this.mReInitToRefreshSettings = true;
                this.mIsSandbox = z;
            }
            return this;
        }

        public Builder setProxy(String str, int i, String str2, String str3) {
            if (HTTPClient.stringEquals(str, this.mProxyHost) && i == this.mProxyPort && HTTPClient.stringEquals(str2, this.mProxyUsername) && HTTPClient.stringEquals(str3, this.mProxyPassword)) {
                return this;
            }
            this.mProxyHost = str;
            this.mProxyPort = i;
            this.mProxyUsername = str2;
            this.mProxyPassword = str3;
            this.mReInitToRefreshSettings = true;
            return this;
        }

        public Builder setQuicTraceLoggingSampleParam(long j, int i) {
            this.mQuicTraceLoggingSampleSalt = j;
            this.mQuicTraceLoggingSampleWeight = i;
            return this;
        }

        public Builder setSecureProxy(String str, int i, String str2, String str3) {
            if (HTTPClient.stringEquals(str, this.mSecureProxyHost) && i == this.mSecureProxyPort && HTTPClient.stringEquals(str2, this.mSecureProxyUsername) && HTTPClient.stringEquals(str3, this.mSecureProxyPassword)) {
                return this;
            }
            this.mSecureProxyHost = str;
            this.mSecureProxyPort = i;
            this.mSecureProxyUsername = str2;
            this.mSecureProxyPassword = str3;
            this.mReInitToRefreshSettings = true;
            return this;
        }

        public Builder setTlsHTTP2Override(boolean z, boolean z2) {
            this.mShouldOverrideTlsHTTP2 = z;
            this.mTlsHTTP2Override = z2;
            return this;
        }

        public Builder setAnalyticsLogger(AnalyticsLogger analyticsLogger) {
            this.mAnalyticsLogger = analyticsLogger;
            return this;
        }

        public Builder setAttemptEarlyDataInQuicPreconnect(boolean z) {
            this.mAttemptEarlyDataInQuicPreconnect = z;
            return this;
        }

        public Builder setBackgroundDNSSampleRate(double d) {
            this.mBackgroundDNSSampleRate = d;
            return this;
        }

        public Builder setCdnOverride(String str) {
            this.mCdnOverride = str;
            return this;
        }

        public Builder setCircularLogSinkEnabled(boolean z) {
            this.mCircularLogSinkEnabled = z;
            return this;
        }

        public Builder setCompressionFilter(boolean z) {
            this.mIsCompressionFilterEnabled = z;
            return this;
        }

        public Builder setConnQualityConfig(ConnQualityConfig connQualityConfig) {
            this.mConnQualityConfig = connQualityConfig;
            return this;
        }

        public Builder setDNSCacheEnabled(boolean z) {
            this.mDnsCacheEnabled = z;
            return this;
        }

        public Builder setDNSResolverExecutor(Executor executor) {
            this.mDNSResolverExecutor = executor;
            return this;
        }

        public Builder setDisableTcpPreconnectWithQuic(boolean z) {
            this.mDisableTcpPreconnectWithQuic = z;
            return this;
        }

        public Builder setDnsRequestsOutstanding(int i) {
            this.mDnsRequestsOutstanding = i;
            return this;
        }

        public Builder setEnableBackgroundQuicConnection(boolean z) {
            this.mEnableBackgroundQuicConnection = z;
            return this;
        }

        public Builder setEnableDzCompression(boolean z) {
            this.mEnableDzCompression = z;
            return this;
        }

        public Builder setEnableHTTP2(boolean z) {
            this.mEnableHTTP2 = z;
            return this;
        }

        public Builder setEnableHTTP2Weights(boolean z) {
            this.mEnableHTTP2Weights = z;
            return this;
        }

        public Builder setEnableHTTP3(boolean z) {
            this.mEnableHTTP3 = z;
            return this;
        }

        public Builder setEnableLigerPreconnect(boolean z) {
            this.mEnableLigerPreconnect = z;
            return this;
        }

        public Builder setEnableLigerWorkerThread(boolean z) {
            this.mEnableLigerWorkerThread = z;
            return this;
        }

        public Builder setEnableLithiumZstd(boolean z) {
            this.mEnableLithiumZstd = z;
            return this;
        }

        public Builder setEnablePreconnectWithQuic(boolean z) {
            this.mEnablePreconnectWithQuic = z;
            return this;
        }

        public Builder setEnableSimplePreconnectForReinit(boolean z) {
            this.mEnableSimplePreconnectForReinit = z;
            return this;
        }

        public Builder setEnableZstd(boolean z) {
            this.mEnableZstd = z;
            return this;
        }

        public Builder setEvbLoggingSamplingFreq(int i) {
            this.mEvbLoggingSamplingFreq = i;
            return this;
        }

        public Builder setEventBase(EventBase eventBase) {
            this.mEventBase = eventBase;
            return this;
        }

        public Builder setFBHostOnly(boolean z) {
            this.mFBHostOnly = z;
            return this;
        }

        public Builder setFizzSettings(FizzSettings fizzSettings) {
            this.mFizzSettings = fizzSettings;
            return this;
        }

        public Builder setH2PubSubHostnames(String str) {
            this.mH2PubSubHostnames = str;
            return this;
        }

        public Builder setHTTP2WeightHighPri(int i) {
            this.mHTTP2WeightHighPri = i;
            return this;
        }

        public Builder setHTTP2WeightLowPri(int i) {
            this.mHTTP2WeightLowPri = i;
            return this;
        }

        public Builder setHTTPSEnforced(boolean z) {
            this.mIsHTTPSEnforced = z;
            return this;
        }

        public Builder setHTTPSessionMaxReadBufferSize(int i) {
            this.mHTTPSessionMaxReadBufferSize = i;
            return this;
        }

        public Builder setHappyEyeballsConnectionDelayMillis(long j) {
            this.mHappyEyeballsConnectionDelayMillis = j;
            return this;
        }

        public Builder setIdleHTTPSessionsLowWaterMark(int i) {
            this.mIdleHTTPSessionsLowWaterMark = i;
            return this;
        }

        public Builder setIdleTimeoutForUnusedQuicSession(int i) {
            this.mIdleTimeoutForUnusedQuicSession = i;
            return this;
        }

        public Builder setIdleTimeoutForUnusedTCPSession(int i) {
            this.mIdleTimeoutForUnusedTCPSession = i;
            return this;
        }

        public Builder setIdleTimeoutForUsedQuicSession(int i) {
            this.mIdleTimeoutForUsedQuicSession = i;
            return this;
        }

        public Builder setIdleTimeoutForUsedTCPSession(int i) {
            this.mIdleTimeoutForUsedTCPSession = i;
            return this;
        }

        public Builder setInlinePersistenceLoading(boolean z) {
            this.mInlinePersistenceLoading = z;
            return this;
        }

        public Builder setLoadBalancing(boolean z) {
            this.mUseLoadBalancing = z;
            return this;
        }

        public Builder setMaxConcurrentOutgoingStreams(int i) {
            this.mMaxConcurrentOutgoingStreams = i;
            return this;
        }

        public Builder setMaxIdleHTTP2Sessions(int i) {
            this.mMaxIdleHTTP2Sessions = i;
            return this;
        }

        public Builder setMaxIdleHTTPSessions(int i) {
            this.mMaxIdleHTTPSessions = i;
            return this;
        }

        public Builder setMaxPriorityLevelForSession(int i) {
            this.mMaxPriorityLevelForSession = i;
            return this;
        }

        public Builder setNetworkStatusMonitor(NetworkStatusMonitor networkStatusMonitor) {
            this.mNetworkStatusMonitor = networkStatusMonitor;
            return this;
        }

        public Builder setNewConnectionTimeoutMillis(long j) {
            this.mNewConnTimeoutMillis = j;
            return this;
        }

        public Builder setPerDomainLimitEnabled(boolean z) {
            this.mIsPerDomainLimitEnabled = z;
            return this;
        }

        public Builder setPerDomainMaxConns(int i) {
            this.mPerDomainMaxConns = i;
            return this;
        }

        public Builder setPersistentCachesExecutor(Executor executor) {
            this.mPersistentCachesExecutor = executor;
            return this;
        }

        public Builder setPersistentDNSCacheSettings(PersistentSSLCacheSettings persistentSSLCacheSettings) {
            this.mPersistentDNSCacheSettings = persistentSSLCacheSettings;
            return this;
        }

        public Builder setPersistentSSLCacheSettings(PersistentSSLCacheSettings persistentSSLCacheSettings) {
            this.mPersistentSSLCacheSettings = persistentSSLCacheSettings;
            return this;
        }

        public Builder setPosixDnsResolverPoolSize(int i) {
            this.mPosixResolverPoolSize = i;
            return this;
        }

        public Builder setPreConnects(String str) {
            this.mPreConnects = str;
            return this;
        }

        public Builder setPreconnectFilePath(String str) {
            this.mPreconnectFilePath = str;
            return this;
        }

        public Builder setProxyFallbackEnabled(boolean z) {
            this.mProxyFallbackEnabled = z;
            return this;
        }

        public Builder setQuicFizzSettings(FizzSettings fizzSettings) {
            this.mQuicFizzSettings = fizzSettings;
            return this;
        }

        public Builder setQuicSettings(QuicSettings quicSettings) {
            this.mQuicSettings = quicSettings;
            return this;
        }

        public Builder setRedirectFilter(boolean z) {
            this.mRedirectFilter = z;
            return this;
        }

        public Builder setRootCACallbacks(RootCACallbacks rootCACallbacks) {
            this.mRootCACallbacks = rootCACallbacks;
            return this;
        }

        public Builder setSSLKeyMaterialCallback(SSLKeyMaterialCallback sSLKeyMaterialCallback) {
            this.mSSLKeyMaterialCallback = sSLKeyMaterialCallback;
            return this;
        }

        public Builder setSSLSessionCache(boolean z) {
            this.mIsSSLSessionCacheEnabled = z;
            return this;
        }

        public Builder setSSLVerificationSettings(SSLVerificationSettings sSLVerificationSettings) {
            this.mSSLVerificationSettings = sSLVerificationSettings;
            return this;
        }

        public Builder setSessionWriteTimeoutMillis(long j) {
            this.mSessionWriteTimeoutMillis = j;
            return this;
        }

        public Builder setStaleAnswersEnabled(boolean z) {
            this.mStaleAnswersEnabled = z;
            return this;
        }

        public Builder setSupportH2PubSub(boolean z) {
            this.mSupportH2PubSub = z;
            return this;
        }

        public Builder setTransactionIdleTimeoutMillis(long j) {
            this.mTransactionIdleTimeoutMillis = j;
            return this;
        }

        public Builder setTransportCallbackEnabled(boolean z) {
            this.mEnableTransportCallbacks = z;
            return this;
        }

        public Builder setUseInjectedPreconnect(boolean z) {
            this.mUseInjectedPreconnect = z;
            return this;
        }

        public Builder setUserAgent(String str) {
            this.mUserAgent = str;
            return this;
        }

        public Builder setUserInstalledCertificates(byte[][] bArr) {
            this.mUserInstalledCertificates = bArr;
            return this;
        }

        public Builder setZlibFilter(boolean z) {
            this.mIsCompressionFilterEnabled = z;
            return this;
        }
    }

    private native void close(EventBase eventBase);

    private native void connect(EventBase eventBase, String[] strArr);

    private native void init(EventBase eventBase, boolean z, boolean z2, boolean z3, boolean z4, String str, boolean z5, int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z6, boolean z7, String str2, int i8, String str3, String str4, String str5, int i9, String str6, String str7, String str8, boolean z8, PersistentSSLCacheSettings persistentSSLCacheSettings, SSLVerificationSettings sSLVerificationSettings, byte[][] bArr, long j, long j2, long j3, long j4, PersistentSSLCacheSettings persistentSSLCacheSettings2, boolean z9, boolean z10, int i10, NetworkStatusMonitor networkStatusMonitor, AnalyticsLogger analyticsLogger, String str9, boolean z11, int i11, int i12, boolean z12, int i13, boolean z13, boolean z14, boolean z15, boolean z16, boolean z17, boolean z18, boolean z19, boolean z20, boolean z21, FizzSettings fizzSettings, boolean z22, int i14, int i15, int i16, int i17, double d, int i18, int i19, RootCACallbacks rootCACallbacks, SSLKeyMaterialCallback sSLKeyMaterialCallback, int i20, FizzSettings fizzSettings2, QuicSettings quicSettings, long j5, int i21, boolean z23, String str10, boolean z24, boolean z25, boolean z26, boolean z27, String str11, boolean z28, boolean z29, boolean z30, Executor executor, Executor executor2, boolean z31, boolean z32, boolean z33, int i22, int i23, int i24, String str12, boolean z34, ConnQualityConfig connQualityConfig);

    public native synchronized void make(JniHandler jniHandler, NativeReadBuffer nativeReadBuffer, boolean z, TraceEventContext traceEventContext, boolean z2, NetworkStatusMonitor networkStatusMonitor, int i);

    public void nonBlockingInit() {
        callNativeInit(false, false);
    }

    public void reinit() {
        callNativeInit(true, true);
    }

    public static boolean stringEquals(String str, String str2) {
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    private String workaroundSamsungLocalProxyBug(String str) {
        if (stringEquals(str, "[::1]")) {
            return "::1";
        }
        return str;
    }

    public synchronized void callNativeInit(boolean z, boolean z2) {
        EventBase eventBase = this.mEventBase;
        boolean z3 = this.mIsCompressionFilterEnabled;
        boolean z4 = this.mIsSSLSessionCacheEnabled;
        String str = this.mPreConnects;
        boolean z5 = this.mIsPerDomainLimitEnabled;
        int i = this.mPerDomainMaxConns;
        int i2 = this.mMaxIdleHTTPSessions;
        int i3 = this.mMaxIdleHTTP2Sessions;
        int i4 = this.mIdleTimeoutForUsedTCPSession;
        int i5 = this.mIdleTimeoutForUsedQuicSession;
        int i6 = this.mIdleTimeoutForUnusedTCPSession;
        int i7 = this.mIdleTimeoutForUnusedQuicSession;
        EventBase eventBase2 = eventBase;
        boolean z6 = z3;
        boolean z7 = z4;
        String str2 = str;
        boolean z8 = z5;
        int i8 = i;
        int i9 = i2;
        int i10 = i3;
        int i11 = i4;
        int i12 = i5;
        int i13 = i6;
        int i14 = i7;
        init(eventBase2, z, z2, z6, z7, str2, z8, i8, i9, i10, i11, i12, i13, i14, this.mIsHTTPSEnforced, this.mIsSandbox, this.mProxyHost, this.mProxyPort, this.mProxyUsername, this.mProxyPassword, this.mSecureProxyHost, this.mSecureProxyPort, this.mSecureProxyUsername, this.mSecureProxyPassword, this.mBypassProxyDomains, this.mProxyFallbackEnabled, this.mPersistentSSLCacheSettings, this.mSSLVerificationSettings, this.mUserInstalledCertificates, this.mHappyEyeballsConnectionDelayMillis, this.mNewConnTimeoutMillis, this.mSessionWriteTimeoutMillis, this.mTransactionIdleTimeoutMillis, this.mPersistentDNSCacheSettings, this.mDnsCacheEnabled, this.mStaleAnswersEnabled, this.mDnsRequestsOutstanding, this.mNetworkStatusMonitor, this.mAnalyticsLogger, this.mUserAgent, this.mLargerFlowControlWindow, this.mSessionFlowControlWindow, this.mStreamFlowControlWindow, this.mFlowControlSelectedTxnsOnly, this.mMaxPriorityLevelForSession, this.mUseLoadBalancing, this.mEnableLigerPreconnect, this.mEnableSimplePreconnectForReinit, this.mRedirectFilter, this.mEnableHTTP2, this.mShouldOverrideTlsHTTP2, this.mTlsHTTP2Override, this.mShouldOverrideFizzHTTP2Static, this.mFizzHTTP2StaticOverride, this.mFizzSettings, this.mEnableHTTP2Weights, this.mHTTP2WeightLowPri, this.mHTTP2WeightHighPri, this.mEvbLoggingSamplingFreq, this.mHTTPSessionMaxReadBufferSize, this.mBackgroundDNSSampleRate, this.mPosixResolverPoolSize, this.mMaxConcurrentOutgoingStreams, this.mRootCACallbacks, this.mSSLKeyMaterialCallback, this.mIdleHTTPSessionsLowWaterMark, this.mQuicFizzSettings, this.mQuicSettings, this.mQuicTraceLoggingSampleSalt, this.mQuicTraceLoggingSampleWeight, this.mSupportH2PubSub, this.mH2PubSubHostnames, this.mEnableBackgroundQuicConnection, this.mEnablePreconnectWithQuic, this.mDisableTcpPreconnectWithQuic, this.mAttemptEarlyDataInQuicPreconnect, this.mCdnOverride, this.mInlinePersistenceLoading, this.mEnableHTTP3, this.mEnableLigerWorkerThread, this.mPersistentCachesExecutor, this.mDNSResolverExecutor, this.mEnableZstd, this.mEnableLithiumZstd, this.mEnableDzCompression, this.mPingTimeoutMinMs, this.mPingTimeoutMaxMs, this.mPingTimeoutRtoMultiple, this.mPingDomainBlacklist, this.mDeadConnTimeoutDryRun, this.mConnQualityConfig);
        this.mInitialized = true;
        this.mReInitToRefreshSettings = false;
    }

    public void connectToPreconnectHostnames() {
        String str;
        if (this.mInitialized && !this.mClosed && (str = this.mPreConnects) != null && str.length() > 0) {
            connect(this.mEventBase, str.split(","));
        }
    }

    public EventBase getEventBase() {
        return this.mEventBase;
    }

    public long getNativeHandle() {
        return this.mSettings;
    }

    public boolean isSandbox() {
        return this.mIsSandbox;
    }

    public boolean reInitializeIfNeeded() {
        if (!this.mReInitToRefreshSettings || !this.mInitialized) {
            this.mReInitToRefreshSettings = false;
            return false;
        }
        callNativeInit(true, true);
        this.mClosed = false;
        this.mReInitToRefreshSettings = false;
        return true;
    }

    public HTTPClient setBypassProxyDomains(String str) {
        if (stringEquals(str, this.mBypassProxyDomains)) {
            return this;
        }
        this.mBypassProxyDomains = str;
        this.mReInitToRefreshSettings = true;
        return this;
    }

    public HTTPClient setIsSandbox(boolean z) {
        if (this.mIsSandbox != z) {
            this.mReInitToRefreshSettings = true;
            this.mIsSandbox = z;
        }
        return this;
    }

    public HTTPClient(Builder builder) {
        this.mEventBase = builder.mEventBase;
        this.mIsCompressionFilterEnabled = builder.mIsCompressionFilterEnabled;
        this.mIsSSLSessionCacheEnabled = builder.mIsSSLSessionCacheEnabled;
        this.mPreConnects = builder.mPreConnects;
        this.mIsPerDomainLimitEnabled = builder.mIsPerDomainLimitEnabled;
        this.mPerDomainMaxConns = builder.mPerDomainMaxConns;
        this.mMaxIdleHTTPSessions = builder.mMaxIdleHTTPSessions;
        this.mMaxIdleHTTP2Sessions = builder.mMaxIdleHTTP2Sessions;
        this.mIdleTimeoutForUsedTCPSession = builder.mIdleTimeoutForUsedTCPSession;
        this.mIdleTimeoutForUsedQuicSession = builder.mIdleTimeoutForUsedQuicSession;
        this.mIdleTimeoutForUnusedTCPSession = builder.mIdleTimeoutForUnusedTCPSession;
        this.mIdleTimeoutForUnusedQuicSession = builder.mIdleTimeoutForUnusedQuicSession;
        this.mIsHTTPSEnforced = builder.mIsHTTPSEnforced;
        this.mIsSandbox = builder.mIsSandbox;
        this.mProxyHost = builder.mProxyHost;
        this.mProxyPort = builder.mProxyPort;
        this.mProxyUsername = builder.mProxyUsername;
        this.mProxyPassword = builder.mProxyPassword;
        this.mSecureProxyHost = builder.mSecureProxyHost;
        this.mSecureProxyPort = builder.mSecureProxyPort;
        this.mSecureProxyUsername = builder.mSecureProxyUsername;
        this.mSecureProxyPassword = builder.mSecureProxyPassword;
        this.mUserAgent = builder.mUserAgent;
        this.mBypassProxyDomains = builder.mBypassProxyDomains;
        this.mProxyFallbackEnabled = builder.mProxyFallbackEnabled;
        this.mPersistentSSLCacheSettings = builder.mPersistentSSLCacheSettings;
        this.mSSLVerificationSettings = builder.mSSLVerificationSettings;
        this.mDnsCacheEnabled = builder.mDnsCacheEnabled;
        this.mStaleAnswersEnabled = builder.mStaleAnswersEnabled;
        this.mDnsRequestsOutstanding = builder.mDnsRequestsOutstanding;
        this.mUserInstalledCertificates = builder.mUserInstalledCertificates;
        this.mHappyEyeballsConnectionDelayMillis = builder.mHappyEyeballsConnectionDelayMillis;
        this.mNewConnTimeoutMillis = builder.mNewConnTimeoutMillis;
        this.mSessionWriteTimeoutMillis = builder.mSessionWriteTimeoutMillis;
        this.mTransactionIdleTimeoutMillis = builder.mTransactionIdleTimeoutMillis;
        this.mPersistentDNSCacheSettings = builder.mPersistentDNSCacheSettings;
        this.mCircularLogSinkEnabled = builder.mCircularLogSinkEnabled;
        this.mNetworkStatusMonitor = builder.mNetworkStatusMonitor;
        this.mAnalyticsLogger = builder.mAnalyticsLogger;
        this.mEnableTransportCallbacks = builder.mEnableTransportCallbacks;
        this.mRedirectFilter = builder.mRedirectFilter;
        this.mLargerFlowControlWindow = builder.mLargerFlowControlWindow;
        this.mSessionFlowControlWindow = builder.mSessionFlowControlWindow;
        this.mStreamFlowControlWindow = builder.mStreamFlowControlWindow;
        this.mFlowControlSelectedTxnsOnly = builder.mFlowControlSelectedTxnsOnly;
        this.mUseLoadBalancing = builder.mUseLoadBalancing;
        this.mEnableLigerPreconnect = builder.mEnableLigerPreconnect;
        this.mEnableSimplePreconnectForReinit = builder.mEnableSimplePreconnectForReinit;
        this.mUseInjectedPreconnect = builder.mUseInjectedPreconnect;
        this.mPreconnectFilePath = builder.mPreconnectFilePath;
        this.mEnableHTTP2 = builder.mEnableHTTP2;
        this.mEnableHTTP2Weights = builder.mEnableHTTP2Weights;
        this.mHTTP2WeightLowPri = builder.mHTTP2WeightLowPri;
        this.mHTTP2WeightHighPri = builder.mHTTP2WeightHighPri;
        this.mEvbLoggingSamplingFreq = builder.mEvbLoggingSamplingFreq;
        this.mFizzSettings = builder.mFizzSettings;
        this.mHTTPSessionMaxReadBufferSize = builder.mHTTPSessionMaxReadBufferSize;
        this.mQuicFizzSettings = builder.mQuicFizzSettings;
        this.mQuicSettings = builder.mQuicSettings;
        this.mQuicTraceLoggingSampleSalt = builder.mQuicTraceLoggingSampleSalt;
        this.mQuicTraceLoggingSampleWeight = builder.mQuicTraceLoggingSampleWeight;
        this.mReInitToRefreshSettings = builder.mReInitToRefreshSettings;
        this.mInitialized = builder.mInitialized;
        this.mClosed = builder.mClosed;
        this.mSettings = builder.mSettings;
        this.mMaxPriorityLevelForSession = builder.mMaxPriorityLevelForSession;
        this.mMaxConcurrentOutgoingStreams = builder.mMaxConcurrentOutgoingStreams;
        this.mIdleHTTPSessionsLowWaterMark = builder.mIdleHTTPSessionsLowWaterMark;
        this.mShouldOverrideTlsHTTP2 = builder.mShouldOverrideTlsHTTP2;
        this.mTlsHTTP2Override = builder.mTlsHTTP2Override;
        this.mShouldOverrideFizzHTTP2Static = builder.mShouldOverrideFizzHTTP2Static;
        this.mFizzHTTP2StaticOverride = builder.mFizzHTTP2StaticOverride;
        this.mBackgroundDNSSampleRate = builder.mBackgroundDNSSampleRate;
        this.mPosixResolverPoolSize = builder.mPosixResolverPoolSize;
        this.mFBHostOnly = builder.mFBHostOnly;
        this.mRootCACallbacks = builder.mRootCACallbacks;
        this.mSSLKeyMaterialCallback = builder.mSSLKeyMaterialCallback;
        this.mSupportH2PubSub = builder.mSupportH2PubSub;
        this.mH2PubSubHostnames = builder.mH2PubSubHostnames;
        this.mEnableBackgroundQuicConnection = builder.mEnableBackgroundQuicConnection;
        this.mEnablePreconnectWithQuic = builder.mEnablePreconnectWithQuic;
        this.mDisableTcpPreconnectWithQuic = builder.mDisableTcpPreconnectWithQuic;
        this.mAttemptEarlyDataInQuicPreconnect = builder.mAttemptEarlyDataInQuicPreconnect;
        this.mCdnOverride = builder.mCdnOverride;
        this.mInlinePersistenceLoading = builder.mInlinePersistenceLoading;
        this.mEnableHTTP3 = builder.mEnableHTTP3;
        this.mEnableLigerWorkerThread = builder.mEnableLigerWorkerThread;
        this.mPersistentCachesExecutor = builder.mPersistentCachesExecutor;
        this.mDNSResolverExecutor = builder.mDNSResolverExecutor;
        this.mEnableZstd = builder.mEnableZstd;
        this.mEnableLithiumZstd = builder.mEnableLithiumZstd;
        this.mEnableDzCompression = builder.mEnableDzCompression;
        this.mPingTimeoutMinMs = builder.mPingTimeoutMinMs;
        this.mPingTimeoutMaxMs = builder.mPingTimeoutMaxMs;
        this.mPingTimeoutRtoMultiple = builder.mPingTimeoutRtoMultiple;
        this.mPingDomainBlacklist = builder.mPingDomainBlacklist;
        this.mDeadConnTimeoutDryRun = builder.mDeadConnTimeoutDryRun;
        this.mConnQualityConfig = builder.mConnQualityConfig;
    }

    public void finalize() {
        int A03 = C000700l.A03(1146249023);
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(608926229, A03);
        }
    }

    public HTTPClient setProxy(String str, int i, String str2, String str3) {
        String workaroundSamsungLocalProxyBug = workaroundSamsungLocalProxyBug(str);
        if (stringEquals(workaroundSamsungLocalProxyBug, this.mProxyHost) && i == this.mProxyPort && stringEquals(str2, this.mProxyUsername) && stringEquals(str3, this.mProxyPassword)) {
            return this;
        }
        this.mProxyHost = workaroundSamsungLocalProxyBug;
        this.mProxyPort = i;
        this.mProxyUsername = str2;
        this.mProxyPassword = str3;
        this.mReInitToRefreshSettings = true;
        return this;
    }

    public HTTPClient setSecureProxy(String str, int i, String str2, String str3) {
        String workaroundSamsungLocalProxyBug = workaroundSamsungLocalProxyBug(str);
        if (stringEquals(workaroundSamsungLocalProxyBug, this.mSecureProxyHost) && i == this.mSecureProxyPort && stringEquals(str2, this.mSecureProxyUsername) && stringEquals(str3, this.mSecureProxyPassword)) {
            return this;
        }
        this.mSecureProxyHost = workaroundSamsungLocalProxyBug;
        this.mSecureProxyPort = i;
        this.mSecureProxyUsername = str2;
        this.mSecureProxyPassword = str3;
        this.mReInitToRefreshSettings = true;
        return this;
    }

    public HTTPClient setAnalyticsLogger(AnalyticsLogger analyticsLogger) {
        this.mAnalyticsLogger = analyticsLogger;
        return this;
    }

    public void setNativeHandle(long j) {
        this.mSettings = j;
    }

    public HTTPClient setNetworkStatusMonitor(NetworkStatusMonitor networkStatusMonitor) {
        this.mNetworkStatusMonitor = networkStatusMonitor;
        return this;
    }

    public HTTPClient setProxyFallbackEnabled(boolean z) {
        this.mProxyFallbackEnabled = z;
        return this;
    }

    public HTTPClient setUserAgent(String str) {
        this.mUserAgent = str;
        return this;
    }

    public HTTPClient setUserInstalledCertificates(byte[][] bArr) {
        this.mUserInstalledCertificates = bArr;
        return this;
    }

    public void close() {
        if (this.mInitialized && !this.mClosed) {
            close(this.mEventBase);
            this.mClosed = true;
        }
    }

    public void connect(String[] strArr) {
        if (this.mInitialized && !this.mClosed) {
            connect(this.mEventBase, strArr);
        }
    }

    public void init() {
        callNativeInit(false, true);
    }

    public void make(JniHandler jniHandler, NativeReadBuffer nativeReadBuffer, TraceEventContext traceEventContext) {
        int i;
        JniHandler jniHandler2 = jniHandler;
        if (this.mEnableTransportCallbacks) {
            i = jniHandler.getEnabledCallbackFlag();
        } else {
            i = 0;
        }
        NativeReadBuffer nativeReadBuffer2 = nativeReadBuffer;
        make(jniHandler2, nativeReadBuffer2, traceEventContext.mSamplePolicy.isSampled(), traceEventContext, this.mCircularLogSinkEnabled, this.mNetworkStatusMonitor, i);
    }
}
