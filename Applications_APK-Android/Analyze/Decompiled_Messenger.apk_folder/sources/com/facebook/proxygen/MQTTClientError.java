package com.facebook.proxygen;

public class MQTTClientError {
    public int mConnAckCode;
    public String mErrMsg;
    public MQTTErrorType mErrType;

    public enum MQTTErrorType {
        PARSE_ERROR,
        TRANSPORT_CONNECT_ERROR,
        MQTT_CONNECT_ERROR,
        CONNECTION_CLOSED,
        READ_ERROR,
        WRITE_ERROR,
        EOF,
        PING_TIMEOUT,
        DISCONNECT,
        COMPRESSION_ERROR,
        STOPPED_BEFORE_MQTT_CONNECT,
        UNKNOWN
    }

    public int getConnAckCode() {
        return this.mConnAckCode;
    }

    public String getMessage() {
        return this.mErrMsg;
    }

    public MQTTErrorType getType() {
        return this.mErrType;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = this.mConnAckCode;
        if (i != 0) {
            sb.append("conAckCode=");
            sb.append(i);
        }
        sb.append("errType=");
        sb.append(this.mErrType.name());
        sb.append("errMsg=");
        sb.append(this.mErrMsg);
        return sb.toString();
    }

    public MQTTClientError(MQTTErrorType mQTTErrorType, String str) {
        this.mErrType = mQTTErrorType;
        this.mErrMsg = str;
    }

    public void setConnAckCode(int i) {
        this.mConnAckCode = i;
    }
}
