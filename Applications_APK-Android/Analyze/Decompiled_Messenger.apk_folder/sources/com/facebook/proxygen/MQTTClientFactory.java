package com.facebook.proxygen;

import X.C000700l;
import java.util.concurrent.Executor;

public class MQTTClientFactory extends NativeHandleImpl {
    private final Executor mAppExecutor;
    public Executor mDnsResolverExecutor;
    public boolean mEnableLargePayload;
    private final EventBase mEventbase;
    public FizzSettings mFizzSettings;
    private HTTPClient mHttpClient;
    public Executor mPersistentCachesExecutor;
    public PersistentSSLCacheSettings mPersistentDNSCacheSettings;
    public PersistentSSLCacheSettings mPersistentSSLCacheSettings;
    private RootCACallbacks mRootCACallbacks;
    public SPDYTransportSettings mSPDYTransportSettings;
    private final MQTTClientSettings mSettings;

    private native void init(EventBase eventBase, Executor executor, int i, int i2, boolean z, boolean z2, boolean z3, PersistentSSLCacheSettings persistentSSLCacheSettings, PersistentSSLCacheSettings persistentSSLCacheSettings2, FizzSettings fizzSettings, SPDYTransportSettings sPDYTransportSettings, HTTPClient hTTPClient, String str, int i3, String str2, int i4, String str3, String str4, boolean z4, RootCACallbacks rootCACallbacks, Executor executor2, Executor executor3);

    public native void close();

    public native void networkReset();

    public MQTTClientFactory(EventBase eventBase, Executor executor, MQTTClientSettings mQTTClientSettings, RootCACallbacks rootCACallbacks) {
        this.mSettings = mQTTClientSettings;
        this.mEventbase = eventBase;
        this.mAppExecutor = executor;
        this.mRootCACallbacks = rootCACallbacks;
    }

    public void finalize() {
        int A03 = C000700l.A03(1430334678);
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(-924334434, A03);
        }
    }

    public MQTTClientFactory enableLargePayload(boolean z) {
        this.mEnableLargePayload = z;
        return this;
    }

    public MQTTClientFactory setDnsResolverExecutor(Executor executor) {
        this.mDnsResolverExecutor = executor;
        return this;
    }

    public MQTTClientFactory setFizzSettings(FizzSettings fizzSettings) {
        this.mFizzSettings = fizzSettings;
        return this;
    }

    public MQTTClientFactory setHTTPClient(HTTPClient hTTPClient) {
        this.mHttpClient = hTTPClient;
        return this;
    }

    public MQTTClientFactory setPersistentCachesExecutor(Executor executor) {
        this.mPersistentCachesExecutor = executor;
        return this;
    }

    public MQTTClientFactory setPersistentDNSCacheSettings(PersistentSSLCacheSettings persistentSSLCacheSettings) {
        this.mPersistentDNSCacheSettings = persistentSSLCacheSettings;
        return this;
    }

    public MQTTClientFactory setPersistentSSLCacheSettings(PersistentSSLCacheSettings persistentSSLCacheSettings) {
        this.mPersistentSSLCacheSettings = persistentSSLCacheSettings;
        return this;
    }

    public MQTTClientFactory setSPDYTransportSettings(SPDYTransportSettings sPDYTransportSettings) {
        this.mSPDYTransportSettings = sPDYTransportSettings;
        return this;
    }

    public void init() {
        EventBase eventBase = this.mEventbase;
        Executor executor = this.mAppExecutor;
        MQTTClientSettings mQTTClientSettings = this.mSettings;
        int i = mQTTClientSettings.connectTimeout;
        int i2 = mQTTClientSettings.pingRespTimeout;
        boolean z = mQTTClientSettings.verifyCertificates;
        boolean z2 = mQTTClientSettings.zlibCompression;
        boolean z3 = this.mEnableLargePayload;
        PersistentSSLCacheSettings persistentSSLCacheSettings = this.mPersistentSSLCacheSettings;
        PersistentSSLCacheSettings persistentSSLCacheSettings2 = this.mPersistentDNSCacheSettings;
        FizzSettings fizzSettings = this.mFizzSettings;
        SPDYTransportSettings sPDYTransportSettings = this.mSPDYTransportSettings;
        HTTPClient hTTPClient = this.mHttpClient;
        String str = mQTTClientSettings.proxyAddress;
        int i3 = mQTTClientSettings.proxyPort;
        PersistentSSLCacheSettings persistentSSLCacheSettings3 = persistentSSLCacheSettings;
        PersistentSSLCacheSettings persistentSSLCacheSettings4 = persistentSSLCacheSettings2;
        FizzSettings fizzSettings2 = fizzSettings;
        SPDYTransportSettings sPDYTransportSettings2 = sPDYTransportSettings;
        HTTPClient hTTPClient2 = hTTPClient;
        String str2 = str;
        int i4 = i3;
        EventBase eventBase2 = eventBase;
        Executor executor2 = executor;
        int i5 = i;
        int i6 = i2;
        boolean z4 = z;
        boolean z5 = z2;
        boolean z6 = z3;
        init(eventBase2, executor2, i5, i6, z4, z5, z6, persistentSSLCacheSettings3, persistentSSLCacheSettings4, fizzSettings2, sPDYTransportSettings2, hTTPClient2, str2, i4, mQTTClientSettings.secureProxyAddress, mQTTClientSettings.secureProxyPort, mQTTClientSettings.bypassProxyDomains, mQTTClientSettings.proxyUserAgent, mQTTClientSettings.proxyFallbackEnabled, this.mRootCACallbacks, this.mPersistentCachesExecutor, this.mDnsResolverExecutor);
    }
}
