package com.facebook.proxygen;

import X.C000700l;

public class EventBase extends NativeHandleImpl {
    private long mJniEnv;

    private native void close();

    private native void init();

    public native void loopForever();

    public native void runInThread(Runnable runnable);

    public native void terminate();

    public EventBase() {
        init();
    }

    public void finalize() {
        int A03 = C000700l.A03(65408226);
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(-611237107, A03);
        }
    }
}
