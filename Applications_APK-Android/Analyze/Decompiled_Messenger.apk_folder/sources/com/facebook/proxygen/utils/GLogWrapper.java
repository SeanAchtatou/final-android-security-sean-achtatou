package com.facebook.proxygen.utils;

import X.C000700l;
import com.facebook.proxygen.NativeHandleImpl;
import com.facebook.proxygen.utils.GLogHandler;

public class GLogWrapper extends NativeHandleImpl {
    private native void init(GLogHandler gLogHandler);

    private native void setMinLogLevel(int i);

    public native void close();

    public native void setVLogLevel(int i);

    public GLogWrapper(BLogHandler bLogHandler) {
        init(new GLogHandler(bLogHandler));
    }

    public void finalize() {
        int A03 = C000700l.A03(-869303059);
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(1440036205, A03);
        }
    }

    public void setMinLogLevel(GLogHandler.GLogSeverity gLogSeverity) {
        setMinLogLevel(gLogSeverity.ordinal());
    }
}
