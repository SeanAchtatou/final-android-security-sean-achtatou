package com.facebook.proxygen.utils;

import X.AnonymousClass09P;
import com.facebook.proxygen.utils.GLogHandler;

public class BLogWrapper implements BLogHandler {
    private static final Class TAG = BLogWrapper.class;
    private final AnonymousClass09P mErrorReporter;
    private GLogWrapper mGLogWrapper;

    public void init() {
        this.mGLogWrapper = new GLogWrapper(this);
    }

    public void log(GLogHandler.GLogSeverity gLogSeverity, String str) {
        if (gLogSeverity == GLogHandler.GLogSeverity.FATAL) {
            this.mErrorReporter.CGS(TAG.getSimpleName(), str);
        }
    }

    public BLogWrapper(AnonymousClass09P r1) {
        this.mErrorReporter = r1;
    }
}
