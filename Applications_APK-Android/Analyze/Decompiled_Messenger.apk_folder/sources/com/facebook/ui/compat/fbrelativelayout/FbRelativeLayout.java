package com.facebook.ui.compat.fbrelativelayout;

import X.C008407o;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class FbRelativeLayout extends RelativeLayout {
    private void A01(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C008407o.A1G);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        if (resourceId != 0) {
            setContentDescription(context.getText(resourceId));
        }
        obtainStyledAttributes.recycle();
    }

    public void onMeasure(int i, int i2) {
        if (Build.VERSION.SDK_INT == 17 && getLayoutDirection() == 1) {
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) childAt.getLayoutParams();
                int[] rules = layoutParams.getRules();
                boolean z = false;
                if (!((rules[18] == 0 && rules[19] == 0) || (rules[5] == 0 && rules[7] == 0))) {
                    layoutParams.addRule(5, 0);
                    layoutParams.addRule(7, 0);
                    z = true;
                }
                if (!z) {
                    boolean z2 = false;
                    if (!((rules[20] == 0 && rules[21] == 0) || (rules[9] == 0 && rules[11] == 0))) {
                        layoutParams.addRule(9, 0);
                        layoutParams.addRule(11, 0);
                        z2 = true;
                    }
                    if (!z2) {
                        boolean z3 = true;
                        if ((rules[16] == 0 && rules[17] == 0) || (rules[0] == 0 && rules[1] == 0)) {
                            z3 = false;
                        } else {
                            layoutParams.addRule(0, 0);
                            layoutParams.addRule(1, 0);
                        }
                        if (!z3) {
                        }
                    }
                }
                childAt.setLayoutParams(layoutParams);
            }
        }
        super.onMeasure(i, i2);
    }

    public FbRelativeLayout(Context context) {
        super(context);
    }

    public FbRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01(context, attributeSet);
    }

    public FbRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01(context, attributeSet);
    }

    public FbRelativeLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01(context, attributeSet);
    }
}
