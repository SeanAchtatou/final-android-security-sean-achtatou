package com.facebook.litho;

import X.AnonymousClass0p4;
import X.AnonymousClass11F;
import X.AnonymousClass1IM;
import X.AnonymousClass1JE;
import X.C17770zR;
import X.C22291Kt;
import android.graphics.drawable.Drawable;

public class ComponentBuilderCBuilderShape0_0S0100000 extends AnonymousClass11F {
    public Object A00;
    public final int A01;

    public ComponentBuilderCBuilderShape0_0S0100000(int i) {
        this.A01 = i;
    }

    public AnonymousClass11F A2x() {
        switch (this.A01) {
            case 0:
            case 1:
            case 2:
                return this;
            default:
                return super.A2x();
        }
    }

    public /* bridge */ /* synthetic */ C17770zR A31() {
        switch (this.A01) {
            case 0:
                return (C22291Kt) this.A00;
            case 1:
                return (AnonymousClass1JE) this.A00;
            case 2:
                return (AnonymousClass1IM) this.A00;
            default:
                return super.A31();
        }
    }

    public void A32(C17770zR r2) {
        switch (this.A01) {
            case 0:
                this.A00 = (C22291Kt) r2;
                return;
            case 1:
                this.A00 = (AnonymousClass1JE) r2;
                return;
            case 2:
                this.A00 = (AnonymousClass1IM) r2;
                return;
            default:
                super.A32(r2);
                return;
        }
    }

    public void A33(int i) {
        ((C22291Kt) this.A00).A00 = i;
    }

    public void A34(int i) {
        ((C22291Kt) this.A00).A00 = this.A02.A04(i, 0);
    }

    public void A35(int i) {
        ((C22291Kt) this.A00).A00 = this.A02.A02(i);
    }

    public void A36(int i) {
        ((C22291Kt) this.A00).A01 = this.A02.A07(i);
    }

    public void A37(Drawable drawable) {
        ((C22291Kt) this.A00).A01 = drawable;
    }

    public static void A00(ComponentBuilderCBuilderShape0_0S0100000 componentBuilderCBuilderShape0_0S0100000, AnonymousClass0p4 r1, int i, int i2, C22291Kt r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0100000.A00 = r4;
    }

    public static void A01(ComponentBuilderCBuilderShape0_0S0100000 componentBuilderCBuilderShape0_0S0100000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass1JE r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0100000.A00 = r4;
    }

    public static void A02(ComponentBuilderCBuilderShape0_0S0100000 componentBuilderCBuilderShape0_0S0100000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass1IM r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0100000.A00 = r4;
    }
}
