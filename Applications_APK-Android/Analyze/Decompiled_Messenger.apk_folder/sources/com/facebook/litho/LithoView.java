package com.facebook.litho;

import X.AnonymousClass07B;
import X.AnonymousClass0p4;
import X.AnonymousClass10N;
import X.AnonymousClass117;
import X.AnonymousClass11C;
import X.AnonymousClass11J;
import X.AnonymousClass1IK;
import X.AnonymousClass1Ri;
import X.AnonymousClass1TA;
import X.AnonymousClass45A;
import X.C000700l;
import X.C15320v6;
import X.C17560z6;
import X.C17570z7;
import X.C17600zA;
import X.C17770zR;
import X.C17830zY;
import X.C188915v;
import X.C29056EIu;
import X.C33181nA;
import X.C636638e;
import X.C636738f;
import X.C636938h;
import X.C72603ed;
import X.C73913gy;
import X.C73923gz;
import X.C74263hX;
import X.C88344Jx;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LithoView extends ComponentHost {
    public static final int[] A0L = new int[2];
    public int A00;
    public int A01;
    public int A02;
    public ComponentTree A03;
    public ComponentTree A04;
    public C636938h A05;
    public AnonymousClass45A A06;
    public C72603ed A07;
    public String A08;
    public String A09;
    public Map A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public final Rect A0G;
    public final AnonymousClass0p4 A0H;
    public final C17600zA A0I;
    private final AccessibilityManager A0J;
    private final C17570z7 A0K;

    public void setHasTransientState(boolean z) {
        ComponentTree componentTree;
        ComponentTree componentTree2;
        int i = this.A02;
        if (z) {
            if (i == 0 && (componentTree2 = this.A03) != null && componentTree2.A0d) {
                A0W(new Rect(0, 0, getWidth(), getHeight()), false);
            }
            this.A02++;
        } else {
            int i2 = i - 1;
            this.A02 = i2;
            if (i2 == 0 && (componentTree = this.A03) != null && componentTree.A0d) {
                A0R();
            }
            if (this.A02 < 0) {
                this.A02 = 0;
            }
        }
        super.setHasTransientState(z);
    }

    public String toString() {
        return LithoViewTestHelper.viewToString(this, true);
    }

    public static LithoView A00(Context context, C17770zR r2, boolean z) {
        return A01(new AnonymousClass0p4(context), r2, z);
    }

    public static LithoView A01(AnonymousClass0p4 r2, C17770zR r3, boolean z) {
        LithoView lithoView = new LithoView(r2);
        AnonymousClass11J A022 = ComponentTree.A02(r2, r3);
        A022.A0C = z;
        lithoView.A0b(A022.A00());
        return lithoView;
    }

    private void A02() {
        ComponentTree componentTree = this.A03;
        if (componentTree != null && componentTree.A0d && (getParent() instanceof View)) {
            int width = ((View) getParent()).getWidth();
            int height = ((View) getParent()).getHeight();
            int translationX = (int) getTranslationX();
            int translationY = (int) getTranslationY();
            int top = getTop() + translationY;
            int bottom = getBottom() + translationY;
            int left = getLeft() + translationX;
            int right = getRight() + translationX;
            if (left < 0 || top < 0 || right > width || bottom > height || this.A0G.width() != getWidth() || this.A0G.height() != getHeight()) {
                Rect rect = new Rect();
                if (getLocalVisibleRect(rect)) {
                    A0W(rect, true);
                }
            }
        }
    }

    private void A03() {
        if (!this.A0E) {
            this.A0E = true;
            ComponentTree componentTree = this.A03;
            if (componentTree != null) {
                componentTree.A0J();
            }
            A0N(C17560z6.A01(getContext()));
            AccessibilityManager accessibilityManager = this.A0J;
            C17570z7 r1 = this.A0K;
            if (r1 != null) {
                accessibilityManager.addAccessibilityStateChangeListener(new AnonymousClass11C(r1));
            }
        }
    }

    private void A04() {
        if (this.A0E) {
            this.A0E = false;
            this.A0I.A0P();
            ComponentTree componentTree = this.A03;
            if (componentTree != null) {
                componentTree.A0K();
            }
            AccessibilityManager accessibilityManager = this.A0J;
            C17570z7 r1 = this.A0K;
            if (r1 != null) {
                accessibilityManager.removeAccessibilityStateChangeListener(new AnonymousClass11C(r1));
            }
        }
    }

    private void A06(boolean z) {
        List A0N = this.A0I.A0N();
        for (int size = A0N.size() - 1; size >= 0; size--) {
            ((LithoView) A0N.get(size)).A0e(z);
        }
    }

    public boolean A0Q() {
        ComponentTree componentTree = this.A03;
        if (componentTree == null || !componentTree.A0L) {
            return super.A0Q();
        }
        return false;
    }

    public void A0R() {
        ComponentTree componentTree = this.A03;
        if (componentTree != null && componentTree.A09 != null) {
            if (componentTree.A0d) {
                componentTree.A0L();
                return;
            }
            throw new IllegalStateException("To perform incremental mounting, you need first to enable it when creating the ComponentTree.");
        }
    }

    public void A0S() {
        C17600zA r3 = this.A0I;
        long[] jArr = r3.A0F;
        if (jArr != null) {
            int length = jArr.length;
            for (int i = 0; i < length; i++) {
                C17830zY A0M = r3.A0M(i);
                if (A0M != null && !A0M.A09) {
                    C17770zR r0 = A0M.A04;
                    Object A002 = A0M.A00();
                    C17600zA.A0E(r3, r0, A002);
                    A0M.A09 = true;
                    if ((A002 instanceof View) && !(A002 instanceof ComponentHost)) {
                        View view = (View) A002;
                        if (view.isLayoutRequested()) {
                            C17600zA.A0J(view, view.getLeft(), view.getTop(), view.getRight(), view.getBottom(), true);
                        }
                    }
                }
            }
        }
    }

    public void A0T() {
        ComponentTree componentTree = this.A03;
        if (componentTree != null) {
            componentTree.A0M();
            this.A03 = null;
            this.A08 = "release_CT";
        }
    }

    public void A0U() {
        C17600zA r1 = this.A0I;
        r1.A09 = true;
        r1.A0G.setEmpty();
        this.A0G.setEmpty();
    }

    public void A0V() {
        C17600zA r3 = this.A0I;
        long[] jArr = r3.A0F;
        if (jArr != null) {
            for (int length = jArr.length - 1; length >= 0; length--) {
                C17600zA.A0C(r3, length, r3.A0I);
            }
            r3.A0G.setEmpty();
            r3.A0C = true;
        }
        this.A0G.setEmpty();
    }

    public void A0W(Rect rect, boolean z) {
        boolean z2;
        ComponentTree componentTree = this.A03;
        if (componentTree != null) {
            if (componentTree.A09 != null) {
                z2 = true;
            } else if (isLayoutRequested()) {
                z2 = false;
            } else {
                throw new RuntimeException("Trying to incrementally mount a component with a null main thread LayoutState on a LithoView that hasn't requested layout!");
            }
            if (z2) {
                ComponentTree componentTree2 = this.A03;
                if (componentTree2.A0d) {
                    componentTree2.A0Q(rect, z);
                    return;
                }
                throw new IllegalStateException("To perform incremental mounting, you need first to enable it when creating the ComponentTree.");
            }
        }
    }

    public void A0X(C17770zR r2) {
        ComponentTree componentTree = this.A03;
        if (componentTree == null) {
            A0b(ComponentTree.A02(this.A0H, r2).A00());
        } else {
            componentTree.A0R(r2);
        }
    }

    public void A0Y(C17770zR r2) {
        ComponentTree componentTree = this.A03;
        if (componentTree == null) {
            A0b(ComponentTree.A02(this.A0H, r2).A00());
        } else {
            componentTree.A0S(r2);
        }
    }

    public void A0Z(C17770zR r3) {
        ComponentTree componentTree = this.A03;
        if (componentTree == null) {
            AnonymousClass11J A022 = ComponentTree.A02(this.A0H, r3);
            A022.A0C = false;
            A0b(A022.A00());
            return;
        }
        componentTree.A0S(r3);
    }

    public void A0a(C17770zR r3) {
        ComponentTree componentTree = this.A03;
        if (componentTree == null) {
            AnonymousClass11J A022 = ComponentTree.A02(this.A0H, r3);
            A022.A0C = false;
            A0b(A022.A00());
            return;
        }
        componentTree.A0R(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        if (r2 != r7.A0R) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0b(com.facebook.litho.ComponentTree r7) {
        /*
            r6 = this;
            boolean r0 = r6.A0F
            if (r0 != 0) goto L_0x017a
            r4 = 0
            r6.A04 = r4
            com.facebook.litho.ComponentTree r0 = r6.A03
            if (r0 != r7) goto L_0x0013
            boolean r0 = r6.A0E
            if (r0 == 0) goto L_0x0012
            r6.A0S()
        L_0x0012:
            return
        L_0x0013:
            if (r0 == 0) goto L_0x001e
            if (r7 == 0) goto L_0x001e
            int r2 = r0.A0R
            int r1 = r7.A0R
            r0 = 0
            if (r2 == r1) goto L_0x001f
        L_0x001e:
            r0 = 1
        L_0x001f:
            r6.A0D = r0
            r6.A0U()
            com.facebook.litho.ComponentTree r0 = r6.A03
            if (r0 == 0) goto L_0x00cb
            boolean r0 = X.AnonymousClass07c.unmountAllWhenComponentTreeSetToNull
            if (r0 == 0) goto L_0x0031
            if (r7 != 0) goto L_0x0031
            r6.A0V()
        L_0x0031:
            java.util.Map r0 = r6.A0A
            if (r0 == 0) goto L_0x003d
            com.facebook.litho.ComponentTree r0 = r6.A03
            java.lang.String r0 = r0.A0I()
            r6.A09 = r0
        L_0x003d:
            if (r7 == 0) goto L_0x00ba
            com.facebook.litho.LithoView r0 = r7.getLithoView()
            if (r0 == 0) goto L_0x00ba
            java.util.Map r0 = r6.A0A
            if (r0 == 0) goto L_0x00ba
            java.lang.String r1 = "LithoView:SetAlreadyAttachedComponentTree"
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x00ba
            com.facebook.litho.ComponentTree r3 = r6.A03
            java.util.Map r0 = r6.A0A
            java.lang.Object r1 = r0.get(r1)
            X.38f r1 = (X.C636738f) r1
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r0 = r1.A01
            r2.append(r0)
            java.lang.String r0 = "-"
            r2.append(r0)
            java.lang.String r5 = "LithoView:SetAlreadyAttachedComponentTree"
            r2.append(r5)
            java.lang.String r0 = ", currentView="
            r2.append(r0)
            com.facebook.litho.LithoView r0 = r3.getLithoView()
            java.lang.String r0 = com.facebook.litho.LithoViewTestHelper.A00(r0)
            r2.append(r0)
            java.lang.String r0 = ", newComponent.LV="
            r2.append(r0)
            com.facebook.litho.LithoView r0 = r7.getLithoView()
            java.lang.String r0 = com.facebook.litho.LithoViewTestHelper.A00(r0)
            r2.append(r0)
            java.lang.String r0 = ", currentComponent="
            r2.append(r0)
            java.lang.String r0 = r3.A0I()
            r2.append(r0)
            java.lang.String r0 = ", newComponent="
            r2.append(r0)
            java.lang.String r0 = r7.A0I()
            r2.append(r0)
            java.lang.String r3 = r2.toString()
            boolean r0 = r1.A03
            if (r0 == 0) goto L_0x0139
            java.lang.Integer r2 = X.AnonymousClass07B.A0C
        L_0x00b1:
            int r1 = r1.A00
            X.0gT r0 = X.C09070gU.A00()
            r0.AYF(r2, r5, r3, r1)
        L_0x00ba:
            boolean r0 = r6.A0E
            if (r0 == 0) goto L_0x00c3
            com.facebook.litho.ComponentTree r0 = r6.A03
            r0.A0K()
        L_0x00c3:
            com.facebook.litho.ComponentTree r1 = r6.A03
            boolean r0 = r1.A0K
            if (r0 != 0) goto L_0x0172
            r1.A0C = r4
        L_0x00cb:
            r6.A03 = r7
            if (r7 == 0) goto L_0x0165
            boolean r0 = r7.A0X()
            if (r0 != 0) goto L_0x0145
            com.facebook.litho.ComponentTree r3 = r6.A03
            boolean r0 = r3.A0K
            if (r0 == 0) goto L_0x012a
            com.facebook.litho.LithoView r1 = r3.A0C
            if (r1 == 0) goto L_0x0126
            r1.A0b(r4)
        L_0x00e2:
            X.0p4 r2 = r3.A0S
            android.content.Context r1 = r2.A09
            android.content.Context r0 = r2.A02()
            if (r1 == r0) goto L_0x015a
            android.content.Context r1 = r6.getContext()
            X.0p4 r0 = r3.A0S
            android.content.Context r0 = r0.A09
            android.content.Context r2 = X.C21861Iz.A00(r1)
            android.content.Context r1 = X.C21861Iz.A00(r0)
            r0 = 0
            if (r2 != r1) goto L_0x0100
            r0 = 1
        L_0x0100:
            if (r0 != 0) goto L_0x015a
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Base view context differs, view context is: "
            r1.<init>(r0)
            android.content.Context r0 = r6.getContext()
            r1.append(r0)
            java.lang.String r0 = ", ComponentTree context is: "
            r1.append(r0)
            X.0p4 r0 = r3.A0S
            android.content.Context r0 = r0.A09
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0126:
            r3.A0K()
            goto L_0x00e2
        L_0x012a:
            com.facebook.litho.LithoView r1 = r3.A0C
            if (r1 == 0) goto L_0x00e2
            boolean r0 = r1.A0E
            if (r0 != 0) goto L_0x013d
            r1.A03 = r4
            java.lang.String r0 = "clear_CT"
            r1.A08 = r0
            goto L_0x00e2
        L_0x0139:
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            goto L_0x00b1
        L_0x013d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Trying to clear the ComponentTree while attached."
            r1.<init>(r0)
            throw r1
        L_0x0145:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = "Setting a released ComponentTree to a LithoView, released component was: "
            com.facebook.litho.ComponentTree r1 = r6.A03
            monitor-enter(r1)
            java.lang.String r0 = r1.A0H     // Catch:{ all -> 0x0157 }
            monitor-exit(r1)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r2, r0)
            r3.<init>(r0)
            throw r3
        L_0x0157:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x015a:
            r3.A0C = r6
            boolean r0 = r6.A0E
            if (r0 == 0) goto L_0x016e
            com.facebook.litho.ComponentTree r0 = r6.A03
            r0.A0J()
        L_0x0165:
            com.facebook.litho.ComponentTree r0 = r6.A03
            if (r0 != 0) goto L_0x016b
            java.lang.String r4 = "set_CT"
        L_0x016b:
            r6.A08 = r4
            return
        L_0x016e:
            r6.requestLayout()
            goto L_0x0165
        L_0x0172:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Clearing the LithoView while the ComponentTree is attached"
            r1.<init>(r0)
            throw r1
        L_0x017a:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Cannot update ComponentTree while in the middle of measure"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.LithoView.A0b(com.facebook.litho.ComponentTree):void");
    }

    public void A0d(List list) {
        if (list == null) {
            this.A0A = null;
            return;
        }
        this.A0A = new HashMap();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            C636738f r2 = (C636738f) list.get(i);
            this.A0A.put(r2.A02, r2);
        }
    }

    public void A0e(boolean z) {
        ComponentTree componentTree = this.A03;
        if (componentTree != null && componentTree.A0d) {
            if (!z) {
                A06(false);
                this.A0I.A0O();
            } else if (getLocalVisibleRect(new Rect())) {
                ComponentTree componentTree2 = this.A03;
                if (componentTree2.A0d) {
                    if (componentTree2.A0C != null) {
                        if (componentTree2.A09 == null) {
                            Log.w("ComponentTree", "Main Thread Layout state is not found");
                        } else {
                            Rect rect = new Rect();
                            if (componentTree2.A0C.getLocalVisibleRect(rect)) {
                                LithoView lithoView = componentTree2.A0C;
                                lithoView.A0I.A0Q(componentTree2.A09, rect, null);
                            }
                        }
                    }
                    A06(true);
                    return;
                }
                throw new IllegalStateException("Calling processVisibilityOutputs() but incremental mount is not enabled");
            }
        }
    }

    public boolean A0f() {
        ComponentTree componentTree = this.A03;
        if (componentTree == null || !componentTree.A0d) {
            return false;
        }
        return true;
    }

    public Deque findTestItems(String str) {
        Map map = this.A0I.A0S;
        if (map != null) {
            Deque deque = (Deque) map.get(str);
            if (deque == null) {
                return new LinkedList();
            }
            return deque;
        }
        throw new UnsupportedOperationException("Trying to access TestItems while ComponentsConfiguration.isEndToEndTestRun is false.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0020, code lost:
        if (r7.A00 != -1) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x014f, code lost:
        if (((X.AnonymousClass1JR) r10).BBw() == false) goto L_0x0151;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r21, int r22) {
        /*
            r20 = this;
            r11 = r22
            r7 = r20
            android.content.res.Resources r1 = r7.getResources()
            android.content.Context r0 = r7.getContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            r2 = r21
            int r10 = X.AnonymousClass1JI.A00(r1, r0, r2)
            int r6 = r7.A01
            r5 = 1
            r4 = 0
            r3 = -1
            if (r6 != r3) goto L_0x0022
            int r1 = r7.A00
            r0 = 0
            if (r1 == r3) goto L_0x0023
        L_0x0022:
            r0 = 1
        L_0x0023:
            if (r6 != r3) goto L_0x0029
            int r6 = r7.getWidth()
        L_0x0029:
            int r2 = r7.A00
            if (r2 != r3) goto L_0x0031
            int r2 = r7.getHeight()
        L_0x0031:
            r7.A01 = r3
            r7.A00 = r3
            if (r0 == 0) goto L_0x0041
            X.0zA r0 = r7.A0I
            boolean r0 = r0.A09
            if (r0 != 0) goto L_0x0041
            r7.setMeasuredDimension(r6, r2)
            return
        L_0x0041:
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            boolean r0 = r1 instanceof X.AnonymousClass1JR
            if (r0 == 0) goto L_0x0059
            X.1JR r1 = (X.AnonymousClass1JR) r1
            int r0 = r1.B9v()
            if (r0 == r3) goto L_0x0052
            r10 = r0
        L_0x0052:
            int r0 = r1.Aoh()
            if (r0 == r3) goto L_0x0059
            r11 = r0
        L_0x0059:
            int r8 = android.view.View.MeasureSpec.getSize(r10)
            int r1 = android.view.View.MeasureSpec.getSize(r11)
            com.facebook.litho.ComponentTree r9 = r7.A04
            if (r9 == 0) goto L_0x006f
            com.facebook.litho.ComponentTree r0 = r7.A03
            if (r0 != 0) goto L_0x006f
            r7.A0b(r9)
            r0 = 0
            r7.A04 = r0
        L_0x006f:
            boolean r0 = r7.A0C
            if (r0 != 0) goto L_0x0087
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            r9 = 1073741824(0x40000000, float:2.0)
            if (r0 != r9) goto L_0x0087
            int r0 = android.view.View.MeasureSpec.getMode(r11)
            if (r0 != r9) goto L_0x0087
            r7.A0B = r5
            r7.setMeasuredDimension(r8, r1)
            return
        L_0x0087:
            r7.A0F = r5
            com.facebook.litho.ComponentTree r9 = r7.A03
            if (r9 == 0) goto L_0x00d7
            r0 = 0
            if (r0 != 0) goto L_0x00d7
            boolean r8 = r7.A0C
            r7.A0C = r4
            int r13 = r7.getPaddingRight()
            int r0 = r7.getPaddingLeft()
            int r13 = r13 + r0
            int r12 = android.view.View.MeasureSpec.getMode(r10)
            if (r12 == 0) goto L_0x00b0
            int r0 = android.view.View.MeasureSpec.getSize(r10)
            int r0 = r0 - r13
            int r0 = java.lang.Math.max(r4, r0)
            int r10 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r12)
        L_0x00b0:
            int r13 = r7.getPaddingTop()
            int r0 = r7.getPaddingBottom()
            int r13 = r13 + r0
            int r12 = android.view.View.MeasureSpec.getMode(r11)
            if (r12 == 0) goto L_0x00cc
            int r0 = android.view.View.MeasureSpec.getSize(r11)
            int r0 = r0 - r13
            int r0 = java.lang.Math.max(r4, r0)
            int r11 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r12)
        L_0x00cc:
            int[] r0 = com.facebook.litho.LithoView.A0L
            r9.A0P(r10, r11, r0, r8)
            r8 = r0[r4]
            r1 = r0[r5]
            r7.A0B = r4
        L_0x00d7:
            if (r1 != 0) goto L_0x00e5
            com.facebook.litho.ComponentTree r0 = r7.A03
            if (r0 == 0) goto L_0x0133
            X.15v r0 = r0.A09
            if (r0 == 0) goto L_0x0133
            X.0yx r0 = r0.A0D
            if (r0 != 0) goto L_0x0133
        L_0x00e5:
            r0 = 0
            if (r0 != 0) goto L_0x0131
            com.facebook.litho.ComponentTree r9 = r7.A03
            if (r9 == 0) goto L_0x0131
            boolean r0 = r7.A0D
            if (r0 == 0) goto L_0x00f4
            boolean r0 = r9.A0p
            if (r0 != 0) goto L_0x0131
        L_0x00f4:
            if (r5 == 0) goto L_0x0129
            com.facebook.litho.ComponentTree r10 = r7.A03
            X.15v r9 = r10.A09
            if (r9 == 0) goto L_0x010b
            X.0zP r0 = r9.A0H
            if (r0 == 0) goto L_0x010b
            com.facebook.litho.LithoView r0 = r10.A0C
            X.0zA r5 = r0.A0I
            boolean r0 = r5.A09
            if (r0 == 0) goto L_0x010b
            r5.A0R(r9, r10)
        L_0x010b:
            com.facebook.litho.ComponentTree r10 = r7.A03
            boolean r9 = r7.A0D
            X.0zL r5 = r10.A0G
            X.1M8 r0 = X.C17700zK.A04
            int r0 = com.facebook.litho.ComponentTree.A00(r10, r6, r9, r5, r0)
            if (r0 == r3) goto L_0x011a
            r8 = r0
        L_0x011a:
            com.facebook.litho.ComponentTree r9 = r7.A03
            boolean r6 = r7.A0D
            X.0zL r5 = r9.A0F
            X.1M8 r0 = X.C17700zK.A01
            int r0 = com.facebook.litho.ComponentTree.A00(r9, r2, r6, r5, r0)
            if (r0 == r3) goto L_0x0129
            r1 = r0
        L_0x0129:
            r7.setMeasuredDimension(r8, r1)
            r7.A0D = r4
            r7.A0F = r4
            return
        L_0x0131:
            r5 = 0
            goto L_0x00f4
        L_0x0133:
            java.util.Map r9 = r7.A0A
            r0 = 300(0x12c, float:4.2E-43)
            java.lang.String r13 = X.C99084oO.$const$string(r0)
            if (r9 != 0) goto L_0x018f
            r0 = 0
        L_0x013e:
            if (r0 == 0) goto L_0x00e5
            android.view.ViewGroup$LayoutParams r10 = r7.getLayoutParams()
            boolean r9 = r10 instanceof X.AnonymousClass1JR
            if (r9 == 0) goto L_0x0151
            X.1JR r10 = (X.AnonymousClass1JR) r10
            boolean r10 = r10.BBw()
            r9 = 1
            if (r10 != 0) goto L_0x0152
        L_0x0151:
            r9 = 0
        L_0x0152:
            if (r9 != 0) goto L_0x00e5
            java.lang.String r11 = r0.A01
            java.lang.String r12 = "-"
            java.lang.String r14 = ", current="
            com.facebook.litho.ComponentTree r9 = r7.A03
            if (r9 != 0) goto L_0x018a
            java.lang.String r10 = "null_"
            java.lang.String r9 = r7.A08
            java.lang.String r15 = X.AnonymousClass08S.A0J(r10, r9)
        L_0x0166:
            java.lang.String r16 = ", previous="
            java.lang.String r9 = r7.A09
            java.lang.String r18 = ", view="
            java.lang.String r19 = com.facebook.litho.LithoViewTestHelper.A00(r7)
            r17 = r9
            java.lang.String r11 = X.AnonymousClass08S.A0W(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            boolean r9 = r0.A03
            if (r9 == 0) goto L_0x0187
            java.lang.Integer r10 = X.AnonymousClass07B.A0C
        L_0x017c:
            int r9 = r0.A00
            X.0gT r0 = X.C09070gU.A00()
            r0.AYF(r10, r13, r11, r9)
            goto L_0x00e5
        L_0x0187:
            java.lang.Integer r10 = X.AnonymousClass07B.A01
            goto L_0x017c
        L_0x018a:
            java.lang.String r15 = r9.A0I()
            goto L_0x0166
        L_0x018f:
            java.lang.Object r0 = r9.get(r13)
            X.38f r0 = (X.C636738f) r0
            goto L_0x013e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.LithoView.onMeasure(int, int):void");
    }

    public static void A05(ComponentHost componentHost) {
        int childCount = componentHost.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = componentHost.getChildAt(i);
            if (childAt.isLayoutRequested()) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec(childAt.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(childAt.getHeight(), 1073741824));
                childAt.layout(childAt.getLeft(), childAt.getTop(), childAt.getRight(), childAt.getBottom());
            }
            if (childAt instanceof ComponentHost) {
                A05((ComponentHost) childAt);
            }
        }
    }

    public void A0c(Class cls) {
        C188915v r0;
        AnonymousClass10N r1;
        if (!A0f()) {
            ComponentTree componentTree = this.A03;
            if (componentTree == null) {
                r0 = null;
            } else {
                r0 = componentTree.A09;
            }
            if (r0 != null && cls != null) {
                for (int i = 0; i < r0.A0N.size(); i++) {
                    AnonymousClass117 r2 = (AnonymousClass117) r0.A0N.get(i);
                    if (cls == AnonymousClass1TA.class) {
                        AnonymousClass10N r12 = r2.A09;
                        if (r12 != null) {
                            C33181nA.A04(r12);
                        }
                    } else if (cls == C73913gy.class) {
                        AnonymousClass10N r13 = r2.A06;
                        if (r13 != null) {
                            C33181nA.A02(r13);
                        }
                    } else if (cls == C74263hX.class) {
                        AnonymousClass10N r14 = r2.A04;
                        if (r14 != null) {
                            C33181nA.A00(r14);
                        }
                    } else if (cls == C73923gz.class) {
                        AnonymousClass10N r15 = r2.A07;
                        if (r15 != null) {
                            C33181nA.A03(r15);
                        }
                    } else if (cls == C88344Jx.class && (r1 = r2.A05) != null) {
                        C33181nA.A01(r1);
                    }
                }
                for (LithoView A0c : this.A0I.A0N()) {
                    A0c.A0c(cls);
                }
                return;
            }
            return;
        }
        throw new IllegalStateException("dispatchVisibilityEvent - Can't manually trigger visibility events when incremental mount is enabled");
    }

    public void draw(Canvas canvas) {
        int A022;
        try {
            canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            super.draw(canvas);
            AnonymousClass45A r7 = this.A06;
            if (r7 != null && (A022 = RecyclerView.A02(r7.A00)) != -1) {
                AnonymousClass1Ri r1 = r7.A01.A00;
                long uptimeMillis = SystemClock.uptimeMillis();
                AnonymousClass1IK r6 = (AnonymousClass1IK) r1.A0h.get(A022);
                AnonymousClass10N B0i = r6.A03().B0i();
                if (B0i != null && r6.A0C.get() == 0) {
                    C15320v6.postOnAnimation(r1.A08, new C29056EIu(B0i, AnonymousClass07B.A00, uptimeMillis));
                    r6.A0C.set(2);
                }
                r7.A00.A06 = null;
            }
        } catch (Throwable th) {
            ComponentTree componentTree = this.A03;
            if (componentTree != null) {
                synchronized (componentTree) {
                    if (componentTree.A05 != null) {
                        ComponentTree componentTree2 = this.A03;
                        synchronized (componentTree2) {
                            throw new C636638e("Component root of the crashing hierarchy:", componentTree2.A05, th);
                        }
                    }
                }
            }
            throw th;
        }
    }

    public void offsetLeftAndRight(int i) {
        super.offsetLeftAndRight(i);
        A02();
    }

    public void offsetTopAndBottom(int i) {
        super.offsetTopAndBottom(i);
        A02();
    }

    public void onAttachedToWindow() {
        int A062 = C000700l.A06(1951311280);
        super.onAttachedToWindow();
        A03();
        C000700l.A0C(-1575280644, A062);
    }

    public void onDetachedFromWindow() {
        int A062 = C000700l.A06(1655018590);
        super.onDetachedFromWindow();
        A04();
        C000700l.A0C(-850075741, A062);
    }

    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        A03();
    }

    public void onStartTemporaryDetach() {
        super.onStartTemporaryDetach();
        A04();
    }

    public void setTranslationX(float f) {
        if (f != getTranslationX()) {
            super.setTranslationX(f);
            A02();
        }
    }

    public void setTranslationY(float f) {
        if (f != getTranslationY()) {
            super.setTranslationY(f);
            A02();
        }
    }

    public LithoView(AnonymousClass0p4 r2) {
        this(r2, (AttributeSet) null);
    }

    public LithoView(AnonymousClass0p4 r3, AttributeSet attributeSet) {
        super(r3, attributeSet);
        this.A0G = new Rect();
        this.A0F = false;
        this.A0D = false;
        this.A01 = -1;
        this.A00 = -1;
        this.A07 = null;
        this.A06 = null;
        this.A0K = new C17570z7(this);
        this.A0H = r3;
        this.A0I = new C17600zA(this);
        this.A0J = (AccessibilityManager) r3.A09.getSystemService("accessibility");
    }

    public LithoView(Context context) {
        this(context, (AttributeSet) null);
    }

    public LithoView(Context context, AttributeSet attributeSet) {
        this(new AnonymousClass0p4(context), attributeSet);
    }
}
