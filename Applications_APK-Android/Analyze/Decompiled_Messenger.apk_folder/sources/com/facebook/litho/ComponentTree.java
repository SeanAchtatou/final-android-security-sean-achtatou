package com.facebook.litho;

import X.AnonymousClass07B;
import X.AnonymousClass07c;
import X.AnonymousClass0m5;
import X.AnonymousClass0p4;
import X.AnonymousClass10L;
import X.AnonymousClass11J;
import X.AnonymousClass11N;
import X.AnonymousClass16I;
import X.AnonymousClass16M;
import X.AnonymousClass1JC;
import X.AnonymousClass1KE;
import X.AnonymousClass1M8;
import X.AnonymousClass1RE;
import X.AnonymousClass22J;
import X.BTK;
import X.C07070ca;
import X.C09070gU;
import X.C110275Ns;
import X.C15320v6;
import X.C17600zA;
import X.C17680zI;
import X.C17710zL;
import X.C17760zQ;
import X.C17770zR;
import X.C17850za;
import X.C188915v;
import X.C198219u;
import X.C21801It;
import X.C21811Iu;
import X.C21821Iv;
import X.C21831Iw;
import X.C21851Iy;
import X.C31441jh;
import X.C31551js;
import X.C35561rQ;
import X.C61362yl;
import X.C637038i;
import X.C70213aH;
import X.C72613ef;
import X.C72643ei;
import X.C88554Kv;
import X.E3o;
import android.graphics.Rect;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.ViewParent;
import androidx.viewpager.widget.ViewPager;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ComponentTree {
    public static final ThreadLocal A0r = new ThreadLocal();
    public static final AtomicInteger A0s = new AtomicInteger(0);
    public static volatile Looper A0t;
    public static volatile Looper A0u;
    public int A00 = -1;
    public int A01 = -1;
    public int A02 = -1;
    public int A03;
    public int A04 = -1;
    public C17770zR A05;
    public AnonymousClass16I A06;
    public AnonymousClass16M A07;
    public C188915v A08;
    public C188915v A09;
    public C31551js A0A;
    public C31551js A0B = new AnonymousClass11N(Looper.getMainLooper());
    public LithoView A0C;
    public AnonymousClass22J A0D;
    public AnonymousClass1JC A0E;
    public C17710zL A0F;
    public C17710zL A0G;
    public String A0H;
    public Deque A0I;
    public List A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    private int A0N;
    private C31551js A0O;
    private AnonymousClass1KE A0P;
    private boolean A0Q;
    public final int A0R;
    public final AnonymousClass0p4 A0S;
    public final C637038i A0T;
    public final C21811Iu A0U = new C21811Iu();
    public final AnonymousClass1RE A0V = new AnonymousClass1RE();
    public final C21821Iv A0W = new C21821Iv();
    public final Object A0X = new Object();
    public final Object A0Y = new Object();
    public final Runnable A0Z = new C21801It(this);
    public final String A0a;
    public final boolean A0b;
    public final boolean A0c;
    public final boolean A0d;
    public final boolean A0e;
    public final boolean A0f;
    public final boolean A0g;
    public final boolean A0h;
    public final boolean A0i;
    private final C21831Iw A0j;
    private final Object A0k = new Object();
    private final Runnable A0l = new AnonymousClass0m5(this);
    private final List A0m = new ArrayList();
    private final boolean A0n;
    public volatile C198219u A0o;
    public volatile boolean A0p;
    private volatile C61362yl A0q;

    public static int A00(ComponentTree componentTree, int i, boolean z, C17710zL r6, AnonymousClass1M8 r7) {
        C17680zI r1;
        if (r6 != null) {
            if (!componentTree.A0p && (r1 = r6.A00) != null) {
                C188915v r0 = componentTree.A09;
                return (int) r1.A03.C3j(new E3o(r0, r7), new C70213aH(r0.A0H, r7));
            } else if (!componentTree.A0p || z) {
                return -1;
            } else {
                return i;
            }
        }
        return -1;
    }

    public static void A09(ComponentTree componentTree, C17770zR r11, int i, int i2, boolean z, AnonymousClass10L r15, int i3, String str, AnonymousClass1KE r18) {
        int i4 = i2;
        ComponentTree componentTree2 = componentTree;
        C17770zR r1 = r11;
        int i5 = i;
        componentTree2.A07(r1, i5, i4, z, r15, i3, str, r18, false);
    }

    public static boolean A0H(C17760zQ r6) {
        if (r6 != null) {
            if (r6 instanceof C17850za) {
                ArrayList arrayList = ((C17850za) r6).A00;
                int size = arrayList.size();
                int i = 0;
                while (i < size) {
                    if (!A0H((C17760zQ) arrayList.get(i))) {
                        i++;
                    }
                }
            } else if (r6 instanceof C17680zI) {
                Integer num = ((C17680zI) r6).A01.A00.A00;
                if (num == AnonymousClass07B.A00 || num == AnonymousClass07B.A0i) {
                    return true;
                }
            } else if (r6 instanceof C31441jh) {
                C31441jh r62 = (C31441jh) r6;
                r62.A00();
                ArrayList arrayList2 = r62.A05;
                int size2 = arrayList2.size();
                int i2 = 0;
                while (i2 < size2) {
                    if (!A0H((C17760zQ) arrayList2.get(i2))) {
                        i2++;
                    }
                }
            } else {
                throw new RuntimeException("Unhandled transition type: " + r6);
            }
            return true;
        }
        return false;
    }

    public synchronized String A0I() {
        String str;
        C17770zR r0 = this.A05;
        if (r0 == null) {
            str = null;
        } else {
            str = r0.A1A();
        }
        return str;
    }

    public void A0N(int i, int i2) {
        A07(null, i, i2, true, null, 3, null, null, false);
    }

    public void A0O(int i, int i2, AnonymousClass10L r13) {
        A07(null, i, i2, false, r13, 2, null, null, false);
    }

    public void A0R(C17770zR r10) {
        C17770zR r1 = r10;
        if (r10 != null) {
            A09(this, r1, -1, -1, false, null, 0, null, null);
            return;
        }
        throw new IllegalArgumentException("Root component can't be null");
    }

    public void A0S(C17770zR r10) {
        C17770zR r1 = r10;
        if (r10 != null) {
            A09(this, r1, -1, -1, true, null, 1, null, null);
            return;
        }
        throw new IllegalArgumentException("Root component can't be null");
    }

    public void A0T(C17770zR r10, int i, int i2) {
        C17770zR r1 = r10;
        if (r10 != null) {
            A09(this, r1, i, i2, false, null, 0, null, null);
            return;
        }
        throw new IllegalArgumentException("Root component can't be null");
    }

    public void A0U(C17770zR r10, int i, int i2, AnonymousClass10L r13) {
        C17770zR r1 = r10;
        if (r10 != null) {
            A09(this, r1, i, i2, false, r13, 0, null, null);
            return;
        }
        throw new IllegalArgumentException("Root component can't be null");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0039, code lost:
        r8 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003d, code lost:
        if (r13 == false) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003f, code lost:
        r8 = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0040, code lost:
        A07(r3, -1, -1, r6, null, r8, r14, r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0045, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0049, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0W(boolean r13, java.lang.String r14, boolean r15) {
        /*
            r12 = this;
            monitor-enter(r12)
            X.0zR r1 = r12.A05     // Catch:{ all -> 0x004a }
            if (r1 == 0) goto L_0x0048
            boolean r0 = r12.A0Q     // Catch:{ all -> 0x004a }
            r6 = r13
            if (r0 == 0) goto L_0x0013
            int r1 = r12.A0N     // Catch:{ all -> 0x004a }
            r0 = 2
            if (r1 == r0) goto L_0x0048
            if (r13 == 0) goto L_0x0046
            r0 = 1
            goto L_0x0046
        L_0x0013:
            X.0zR r3 = r1.A16()     // Catch:{ all -> 0x004a }
            X.1KE r0 = r12.A0P     // Catch:{ all -> 0x004a }
            if (r0 != 0) goto L_0x001c
            goto L_0x0021
        L_0x001c:
            X.1KE r10 = X.AnonymousClass1KE.A00(r0)     // Catch:{ all -> 0x004a }
            goto L_0x0022
        L_0x0021:
            r10 = 0
        L_0x0022:
            r11 = r15
            if (r15 == 0) goto L_0x0038
            int r0 = r12.A03     // Catch:{ all -> 0x004a }
            int r1 = r0 + 1
            r12.A03 = r1     // Catch:{ all -> 0x004a }
            r0 = 50
            if (r1 != r0) goto L_0x0038
            java.lang.Integer r2 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x004a }
            java.lang.String r1 = "ComponentTree:StateUpdatesWhenLayoutInProgressExceedsThreshold"
            java.lang.String r0 = "State Updates when create layout in progress exceeds threshold"
            X.C09070gU.A01(r2, r1, r0)     // Catch:{ all -> 0x004a }
        L_0x0038:
            monitor-exit(r12)     // Catch:{ all -> 0x004a }
            r4 = -1
            r5 = -1
            r7 = 0
            r8 = 4
            if (r13 == 0) goto L_0x0040
            r8 = 5
        L_0x0040:
            r2 = r12
            r9 = r14
            r2.A07(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            return
        L_0x0046:
            r12.A0N = r0     // Catch:{ all -> 0x004a }
        L_0x0048:
            monitor-exit(r12)     // Catch:{ all -> 0x004a }
            return
        L_0x004a:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x004a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0W(boolean, java.lang.String, boolean):void");
    }

    public synchronized boolean A0X() {
        return this.A0M;
    }

    private C61362yl A01() {
        C61362yl r0;
        C61362yl r02 = this.A0q;
        if (r02 != null) {
            return r02;
        }
        synchronized (this) {
            r0 = this.A0q;
            if (r0 == null) {
                r0 = new C61362yl();
                this.A0q = r0;
            }
        }
        return r0;
    }

    public static AnonymousClass11J A02(AnonymousClass0p4 r1, C17770zR r2) {
        AnonymousClass11J r0 = new AnonymousClass11J(r1);
        if (r2 != null) {
            r0.A00 = r2;
            return r0;
        }
        throw new NullPointerException("Creating a ComponentTree with a null root is not allowed!");
    }

    public static C31551js A04(C31551js r4) {
        Looper looper;
        if (r4 == null) {
            if (AnonymousClass07c.threadPoolForBackgroundThreadsConfig == null) {
                synchronized (ComponentTree.class) {
                    if (A0t == null) {
                        HandlerThread handlerThread = new HandlerThread("ComponentLayoutThread", AnonymousClass07c.DEFAULT_BACKGROUND_THREAD_PRIORITY);
                        handlerThread.start();
                        A0t = handlerThread.getLooper();
                    }
                    looper = A0t;
                }
                r4 = new AnonymousClass11N(looper);
            } else {
                if (BTK.A01 == null) {
                    synchronized (BTK.class) {
                        if (BTK.A01 == null) {
                            BTK.A01 = new BTK(BTK.A02);
                        }
                    }
                }
                r4 = BTK.A01;
            }
        }
        return C21851Iy.A00(r4);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:695:0x0de7 */
    /* JADX WARN: Type inference failed for: r4v2 */
    /* JADX WARN: Type inference failed for: r4v4, types: [boolean] */
    /* JADX WARN: Type inference failed for: r4v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0307, code lost:
        if (A0H(r15) != false) goto L_0x0309;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:314:0x068d, code lost:
        if (r15 != null) goto L_0x068f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:438:0x098b, code lost:
        if (r0 != false) goto L_0x0e33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:493:0x0a52, code lost:
        if (r0.A0Y != r2.A06) goto L_0x0a54;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:507:0x0aa6, code lost:
        if (r11 != r0) goto L_0x0aa8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:535:0x0b07, code lost:
        if (r0 == false) goto L_0x0b09;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:537:0x0b0a, code lost:
        if (r11 != false) goto L_0x0b0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:576:0x0bce, code lost:
        if (((android.view.View) r6.A00()).isLayoutRequested() == false) goto L_0x0bd0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00c8, code lost:
        if (r4 >= (r1 - r0)) goto L_0x00ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:603:0x0c81, code lost:
        if (r9 != false) goto L_0x0b28;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:669:0x0d91, code lost:
        if (r0 == false) goto L_0x0b09;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:682:0x0db2, code lost:
        if (r0 != false) goto L_0x0ab3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:686:0x0dbd, code lost:
        if (r10 == 2) goto L_0x0ab1;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=?, for r4v3, types: [int, boolean] */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x04b5 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x04c2 A[LOOP:16: B:238:0x04c0->B:239:0x04c2, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x04d2  */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x04e0  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x04e9  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x069b  */
    /* JADX WARNING: Removed duplicated region for block: B:347:0x071f  */
    /* JADX WARNING: Removed duplicated region for block: B:350:0x0740  */
    /* JADX WARNING: Removed duplicated region for block: B:358:0x0765 A[LOOP:22: B:357:0x0763->B:358:0x0765, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:360:0x0774  */
    /* JADX WARNING: Removed duplicated region for block: B:362:0x0779  */
    /* JADX WARNING: Removed duplicated region for block: B:482:0x0a28  */
    /* JADX WARNING: Removed duplicated region for block: B:488:0x0a40  */
    /* JADX WARNING: Removed duplicated region for block: B:514:0x0ab5  */
    /* JADX WARNING: Removed duplicated region for block: B:540:0x0b0f  */
    /* JADX WARNING: Removed duplicated region for block: B:548:0x0b34  */
    /* JADX WARNING: Removed duplicated region for block: B:554:0x0b50  */
    /* JADX WARNING: Removed duplicated region for block: B:603:0x0c81  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:834:0x11ca  */
    /* JADX WARNING: Removed duplicated region for block: B:837:0x11d5  */
    /* JADX WARNING: Removed duplicated region for block: B:863:0x1240  */
    /* JADX WARNING: Removed duplicated region for block: B:955:0x1230 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:958:0x0a2b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A06(android.graphics.Rect r41, boolean r42) {
        /*
            r40 = this;
            r19 = r41
            r3 = r40
            X.15v r2 = r3.A09
            if (r2 != 0) goto L_0x0010
            java.lang.String r1 = "ComponentTree"
            java.lang.String r0 = "Main Thread Layout state is not found"
            android.util.Log.w(r1, r0)
            return
        L_0x0010:
            com.facebook.litho.LithoView r0 = r3.A0C
            X.0zA r0 = r0.A0I
            boolean r0 = r0.A09
            r22 = r0
            if (r0 != 0) goto L_0x0031
            boolean r0 = r3.A0p
            if (r0 == 0) goto L_0x0031
            boolean r0 = X.AnonymousClass07c.incrementalMountWhenNotVisible
            if (r0 == 0) goto L_0x0031
            if (r41 == 0) goto L_0x0031
            com.facebook.litho.LithoView r0 = r3.A0C
            android.graphics.Rect r1 = r0.A0G
            r0 = r19
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0031
        L_0x0030:
            return
        L_0x0031:
            r1 = 1
            r3.A0L = r1
            boolean r0 = r3.A0p
            if (r0 != 0) goto L_0x0040
            com.facebook.litho.LithoView r0 = r3.A0C
            X.0zA r0 = r0.A0I
            r0.A0A = r1
            r3.A0p = r1
        L_0x0040:
            com.facebook.litho.LithoView r8 = r3.A0C
            int r0 = r8.A02
            r5 = 0
            if (r0 <= 0) goto L_0x0066
            com.facebook.litho.ComponentTree r0 = r8.A03
            if (r0 == 0) goto L_0x0066
            boolean r0 = r0.A0d
            if (r0 == 0) goto L_0x0066
            X.0zA r0 = r8.A0I
            boolean r0 = r0.A09
            if (r0 == 0) goto L_0x11ba
            android.graphics.Rect r19 = new android.graphics.Rect
            int r4 = r8.getWidth()
            int r1 = r8.getHeight()
            r0 = r19
            r0.<init>(r5, r5, r4, r1)
            r42 = 0
        L_0x0066:
            android.graphics.Rect r1 = r8.A0G
            if (r19 != 0) goto L_0x01a9
            r1.setEmpty()
        L_0x006d:
            X.38h r1 = r8.A05
            if (r1 == 0) goto L_0x01a5
            X.1r0 r0 = r1.A00
            boolean r0 = X.C35301r0.A01(r0)
            if (r0 == 0) goto L_0x01a5
            boolean[] r0 = r1.A04
            if (r0 == 0) goto L_0x01a5
            boolean r0 = r0[r5]
            if (r0 != 0) goto L_0x01a5
            X.1r0 r5 = r1.A00
            java.lang.String r4 = r1.A01
            java.lang.String r1 = "_firstmount"
            java.lang.String r0 = "_start"
            r5.A03(r1, r0, r4)
            r21 = 1
        L_0x008e:
            X.38h r6 = r8.A05
            r1 = 0
            if (r6 == 0) goto L_0x01a1
            X.1r0 r0 = r6.A00
            boolean r0 = X.C35301r0.A01(r0)
            if (r0 == 0) goto L_0x01a1
            boolean[] r0 = r6.A04
            if (r0 == 0) goto L_0x01a1
            boolean r0 = r0[r1]
            if (r0 == 0) goto L_0x01a1
            boolean[] r0 = r6.A05
            if (r0 == 0) goto L_0x01a1
            boolean r0 = r0[r1]
            if (r0 != 0) goto L_0x01a1
            android.view.ViewParent r5 = r8.getParent()
            android.view.ViewGroup r5 = (android.view.ViewGroup) r5
            if (r5 == 0) goto L_0x01a1
            boolean r0 = r6.A02
            if (r0 != 0) goto L_0x00ca
            boolean r0 = r6.A03
            if (r0 == 0) goto L_0x0193
            int r4 = r8.getBottom()
            int r1 = r5.getHeight()
            int r0 = r5.getPaddingBottom()
        L_0x00c7:
            int r1 = r1 - r0
            if (r4 < r1) goto L_0x01a1
        L_0x00ca:
            X.1r0 r5 = r6.A00
            java.lang.String r4 = r6.A01
            java.lang.String r1 = "_lastmount"
            java.lang.String r0 = "_start"
            r5.A03(r1, r0, r4)
            r20 = 1
        L_0x00d7:
            X.0zA r0 = r8.A0I
            r39 = r0
            r37 = r42
            if (r2 == 0) goto L_0x1240
            boolean r0 = r0.A0B
            if (r0 == 0) goto L_0x00f8
            java.lang.Integer r5 = X.AnonymousClass07B.A0C
            java.lang.String r4 = "Trying to mount while already mounting! "
            r0 = r39
            X.0zY r1 = r0.A0N
            java.lang.String r0 = X.C17600zA.A04(r0, r1)
            java.lang.String r1 = X.AnonymousClass08S.A0J(r4, r0)
            java.lang.String r0 = "MountState:InvalidReentrantMounts"
            X.C09070gU.A01(r5, r0, r1)
        L_0x00f8:
            r10 = 1
            r0 = r39
            r0.A0B = r10
            com.facebook.litho.LithoView r0 = r0.A0M
            com.facebook.litho.ComponentTree r7 = r0.A03
            r4 = 0
            r18 = 0
            if (r19 == 0) goto L_0x0108
            r18 = 1
        L_0x0108:
            boolean r17 = X.C27041cY.A02()
            if (r17 == 0) goto L_0x013f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            if (r18 == 0) goto L_0x0190
            java.lang.String r0 = "incrementalMount"
        L_0x0114:
            r1.<init>(r0)
            java.lang.String r1 = r1.toString()
            X.0gP r0 = X.C27041cY.A00
            X.0gQ r5 = r0.APo(r1)
            int r1 = r2.A00
            java.lang.String r0 = "treeId"
            r5.AOn(r0, r1)
            java.lang.String r1 = r7.A0I()
            java.lang.String r0 = "component"
            r5.AOo(r0, r1)
            X.0p4 r0 = r7.A0S
            java.lang.String r1 = r0.A0B()
            java.lang.String r0 = "logTag"
            r5.AOo(r0, r1)
            r5.flush()
        L_0x013f:
            X.0p4 r5 = r7.A0S
            X.38i r23 = r5.A05()
            int r0 = r2.A00
            r36 = r0
            r0 = r39
            int r1 = r0.A02
            r16 = 0
            r0 = r36
            if (r0 == r1) goto L_0x0159
            r1 = r16
            r0 = r39
            r0.A05 = r1
        L_0x0159:
            if (r23 == 0) goto L_0x0168
            r1 = 6
            r0 = r23
            X.3eb r0 = r0.BLg(r5, r1)
            r1 = r23
            X.3eb r16 = X.C637138j.A00(r5, r1, r0)
        L_0x0168:
            r0 = r39
            boolean r0 = r0.A09
            if (r0 == 0) goto L_0x0780
            r12 = r39
            if (r0 == 0) goto L_0x1228
            boolean r28 = X.C27041cY.A02()
            if (r28 == 0) goto L_0x0185
            X.0p4 r0 = r7.A0S
            java.lang.String r1 = r0.A0B()
            if (r1 != 0) goto L_0x0186
            java.lang.String r0 = "MountState.updateTransitions"
            X.C27041cY.A01(r0)
        L_0x0185:
            goto L_0x01b0
        L_0x0186:
            java.lang.String r0 = "MountState.updateTransitions:"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r1)
            X.C27041cY.A01(r0)
            goto L_0x0185
        L_0x0190:
            java.lang.String r0 = "mount"
            goto L_0x0114
        L_0x0193:
            int r4 = r8.getRight()
            int r1 = r5.getWidth()
            int r0 = r5.getPaddingRight()
            goto L_0x00c7
        L_0x01a1:
            r20 = 0
            goto L_0x00d7
        L_0x01a5:
            r21 = 0
            goto L_0x008e
        L_0x01a9:
            r0 = r19
            r1.set(r0)
            goto L_0x006d
        L_0x01b0:
            int r1 = r2.A00     // Catch:{ all -> 0x1221 }
            int r0 = r12.A02     // Catch:{ all -> 0x1221 }
            if (r0 == r1) goto L_0x0251
            X.0zb r0 = r12.A07     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x024b
            java.util.Map r0 = r12.A0R     // Catch:{ all -> 0x1221 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x01c4:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x01d4
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x1221 }
            X.0zM r0 = (X.C17720zM) r0     // Catch:{ all -> 0x1221 }
            X.C17600zA.A0H(r12, r0)     // Catch:{ all -> 0x1221 }
            goto L_0x01c4
        L_0x01d4:
            java.util.Map r0 = r12.A0R     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            java.util.HashSet r0 = r12.A0Q     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            X.0zb r6 = r12.A07     // Catch:{ all -> 0x1221 }
            X.1jf r0 = r6.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r0.A02     // Catch:{ all -> 0x1221 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r9 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x01ec:
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x1221 }
            r5 = 0
            if (r0 == 0) goto L_0x020a
            java.lang.Object r1 = r9.next()     // Catch:{ all -> 0x1221 }
            X.0zP r1 = (X.C17750zP) r1     // Catch:{ all -> 0x1221 }
            X.1jf r0 = r6.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r0.A02     // Catch:{ all -> 0x1221 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x1221 }
            X.0zc r0 = (X.C17870zc) r0     // Catch:{ all -> 0x1221 }
            X.C17860zb.A08(r6, r0, r5)     // Catch:{ all -> 0x1221 }
            X.C17860zb.A05(r0)     // Catch:{ all -> 0x1221 }
            goto L_0x01ec
        L_0x020a:
            X.1jf r1 = r6.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r1.A01     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r1.A03     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r1.A00     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r1.A02     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            X.0eJ r0 = r6.A01     // Catch:{ all -> 0x1221 }
            r0.A06()     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r6.A08     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            java.util.ArrayList r0 = r6.A07     // Catch:{ all -> 0x1221 }
            int r0 = r0.size()     // Catch:{ all -> 0x1221 }
            int r1 = r0 + -1
        L_0x0232:
            if (r1 < 0) goto L_0x0242
            java.util.ArrayList r0 = r6.A07     // Catch:{ all -> 0x1221 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x1221 }
            X.3aL r0 = (X.C70253aL) r0     // Catch:{ all -> 0x1221 }
            r0.A08()     // Catch:{ all -> 0x1221 }
            int r1 = r1 + -1
            goto L_0x0232
        L_0x0242:
            java.util.ArrayList r0 = r6.A07     // Catch:{ all -> 0x1221 }
            r0.clear()     // Catch:{ all -> 0x1221 }
            r6.A00 = r5     // Catch:{ all -> 0x1221 }
            r12.A0E = r5     // Catch:{ all -> 0x1221 }
        L_0x024b:
            boolean r0 = r12.A0A     // Catch:{ all -> 0x1221 }
            if (r0 != 0) goto L_0x0251
            goto L_0x04b3
        L_0x0251:
            java.util.Map r0 = r12.A0R     // Catch:{ all -> 0x1221 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x1221 }
            if (r0 != 0) goto L_0x027d
            java.util.Map r0 = r2.A0i     // Catch:{ all -> 0x1221 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x0263:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x027d
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x1221 }
            X.0zP r1 = (X.C17750zP) r1     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r12.A0R     // Catch:{ all -> 0x1221 }
            java.lang.Object r0 = r0.remove(r1)     // Catch:{ all -> 0x1221 }
            X.0zM r0 = (X.C17720zM) r0     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0263
            X.C17600zA.A0H(r12, r0)     // Catch:{ all -> 0x1221 }
            goto L_0x0263
        L_0x027d:
            boolean r0 = X.C17600zA.A0K(r12, r2)     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x03f3
            r12.A0R(r2, r7)     // Catch:{ all -> 0x1221 }
            X.0zQ r15 = r12.A06     // Catch:{ all -> 0x1221 }
            r0 = 0
            if (r15 == 0) goto L_0x028c
            r0 = 1
        L_0x028c:
            if (r0 == 0) goto L_0x03f3
            X.0zb r0 = r12.A07     // Catch:{ all -> 0x1221 }
            if (r0 != 0) goto L_0x0299
            X.0zb r0 = new X.0zb     // Catch:{ all -> 0x1221 }
            r0.<init>(r12)     // Catch:{ all -> 0x1221 }
            r12.A07 = r0     // Catch:{ all -> 0x1221 }
        L_0x0299:
            X.0zb r14 = r12.A07     // Catch:{ all -> 0x1221 }
            X.15v r0 = r12.A05     // Catch:{ all -> 0x1221 }
            boolean r27 = X.C27041cY.A02()     // Catch:{ all -> 0x1221 }
            if (r27 == 0) goto L_0x02a8
            java.lang.String r1 = "TransitionManager.setupTransition"
            X.C27041cY.A01(r1)     // Catch:{ all -> 0x1221 }
        L_0x02a8:
            X.1jf r1 = r14.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r1 = r1.A02     // Catch:{ all -> 0x1221 }
            java.util.Collection r1 = r1.values()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r6 = r1.iterator()     // Catch:{ all -> 0x1221 }
        L_0x02b4:
            boolean r1 = r6.hasNext()     // Catch:{ all -> 0x1221 }
            if (r1 == 0) goto L_0x02c3
            java.lang.Object r1 = r6.next()     // Catch:{ all -> 0x1221 }
            X.0zc r1 = (X.C17870zc) r1     // Catch:{ all -> 0x1221 }
            r1.A04 = r4     // Catch:{ all -> 0x1221 }
            goto L_0x02b4
        L_0x02c3:
            java.util.Map r13 = r2.A0i     // Catch:{ all -> 0x1221 }
            r11 = 0
            r9 = 3
            if (r0 != 0) goto L_0x02f6
            java.util.Set r0 = r13.entrySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r6 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x02d1:
            boolean r0 = r6.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0371
            java.lang.Object r5 = r6.next()     // Catch:{ all -> 0x1221 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x1221 }
            java.lang.Object r1 = r5.getKey()     // Catch:{ all -> 0x1221 }
            X.0zP r1 = (X.C17750zP) r1     // Catch:{ all -> 0x1221 }
            boolean r0 = X.AnonymousClass07c.onlyProcessAutogeneratedTransitionIdsWhenNecessary     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x02ec
            int r0 = r1.A00     // Catch:{ all -> 0x1221 }
            if (r0 != r9) goto L_0x02ec
            goto L_0x02d1
        L_0x02ec:
            java.lang.Object r0 = r5.getValue()     // Catch:{ all -> 0x1221 }
            X.0zM r0 = (X.C17720zM) r0     // Catch:{ all -> 0x1221 }
            X.C17860zb.A07(r14, r1, r11, r0)     // Catch:{ all -> 0x1221 }
            goto L_0x02d1
        L_0x02f6:
            java.util.Map r7 = r0.A0i     // Catch:{ all -> 0x1221 }
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ all -> 0x1221 }
            r6.<init>()     // Catch:{ all -> 0x1221 }
            boolean r0 = X.AnonymousClass07c.onlyProcessAutogeneratedTransitionIdsWhenNecessary     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0309
            boolean r0 = A0H(r15)     // Catch:{ all -> 0x1221 }
            r26 = 1
            if (r0 == 0) goto L_0x030b
        L_0x0309:
            r26 = 0
        L_0x030b:
            java.util.Set r0 = r13.keySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r25 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x0313:
            boolean r0 = r25.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0344
            java.lang.Object r5 = r25.next()     // Catch:{ all -> 0x1221 }
            X.0zP r5 = (X.C17750zP) r5     // Catch:{ all -> 0x1221 }
            int r0 = r5.A00     // Catch:{ all -> 0x1221 }
            r24 = 0
            if (r0 != r9) goto L_0x0327
            r24 = 1
        L_0x0327:
            if (r26 == 0) goto L_0x032c
            if (r24 == 0) goto L_0x032c
            goto L_0x0313
        L_0x032c:
            java.lang.Object r1 = r13.get(r5)     // Catch:{ all -> 0x1221 }
            X.0zM r1 = (X.C17720zM) r1     // Catch:{ all -> 0x1221 }
            java.lang.Object r0 = r7.get(r5)     // Catch:{ all -> 0x1221 }
            X.0zM r0 = (X.C17720zM) r0     // Catch:{ all -> 0x1221 }
            if (r1 == 0) goto L_0x0341
            r6.add(r5)     // Catch:{ all -> 0x1221 }
        L_0x033d:
            X.C17860zb.A07(r14, r5, r0, r1)     // Catch:{ all -> 0x1221 }
            goto L_0x0313
        L_0x0341:
            if (r24 == 0) goto L_0x033d
            goto L_0x0313
        L_0x0344:
            java.util.Set r0 = r7.keySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x034c:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0371
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x1221 }
            X.0zP r1 = (X.C17750zP) r1     // Catch:{ all -> 0x1221 }
            boolean r0 = r6.contains(r1)     // Catch:{ all -> 0x1221 }
            if (r0 != 0) goto L_0x034c
            boolean r0 = X.AnonymousClass07c.onlyProcessAutogeneratedTransitionIdsWhenNecessary     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0367
            int r0 = r1.A00     // Catch:{ all -> 0x1221 }
            if (r0 != r9) goto L_0x0367
            goto L_0x034c
        L_0x0367:
            java.lang.Object r0 = r7.get(r1)     // Catch:{ all -> 0x1221 }
            X.0zM r0 = (X.C17720zM) r0     // Catch:{ all -> 0x1221 }
            X.C17860zb.A07(r14, r1, r0, r11)     // Catch:{ all -> 0x1221 }
            goto L_0x034c
        L_0x0371:
            X.3aL r0 = X.C17860zb.A01(r14, r15)     // Catch:{ all -> 0x1221 }
            r14.A00 = r0     // Catch:{ all -> 0x1221 }
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ all -> 0x1221 }
            r6.<init>()     // Catch:{ all -> 0x1221 }
            X.1jf r0 = r14.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r0.A02     // Catch:{ all -> 0x1221 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x0388:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x03b0
            java.lang.Object r5 = r7.next()     // Catch:{ all -> 0x1221 }
            X.0zP r5 = (X.C17750zP) r5     // Catch:{ all -> 0x1221 }
            X.1jf r0 = r14.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r0.A02     // Catch:{ all -> 0x1221 }
            java.lang.Object r1 = r0.get(r5)     // Catch:{ all -> 0x1221 }
            X.0zc r1 = (X.C17870zc) r1     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r1.A06     // Catch:{ all -> 0x1221 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0388
            X.C17860zb.A08(r14, r1, r11)     // Catch:{ all -> 0x1221 }
            X.C17860zb.A05(r1)     // Catch:{ all -> 0x1221 }
            r6.add(r5)     // Catch:{ all -> 0x1221 }
            goto L_0x0388
        L_0x03b0:
            java.util.Iterator r5 = r6.iterator()     // Catch:{ all -> 0x1221 }
        L_0x03b4:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x03c6
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x1221 }
            X.0zP r1 = (X.C17750zP) r1     // Catch:{ all -> 0x1221 }
            X.1jf r0 = r14.A03     // Catch:{ all -> 0x1221 }
            r0.A00(r1)     // Catch:{ all -> 0x1221 }
            goto L_0x03b4
        L_0x03c6:
            if (r27 == 0) goto L_0x03cb
            X.C27041cY.A00()     // Catch:{ all -> 0x1221 }
        L_0x03cb:
            java.util.Map r0 = r2.A0i     // Catch:{ all -> 0x1221 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x03d5:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x03f3
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x1221 }
            X.0zP r1 = (X.C17750zP) r1     // Catch:{ all -> 0x1221 }
            X.0zb r0 = r12.A07     // Catch:{ all -> 0x1221 }
            X.1jf r0 = r0.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r0.A02     // Catch:{ all -> 0x1221 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x03d5
            java.util.HashSet r0 = r12.A0Q     // Catch:{ all -> 0x1221 }
            r0.add(r1)     // Catch:{ all -> 0x1221 }
            goto L_0x03d5
        L_0x03f3:
            X.0zb r5 = r12.A07     // Catch:{ all -> 0x1221 }
            if (r5 == 0) goto L_0x0442
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x1221 }
            X.1jf r0 = r5.A03     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r0.A02     // Catch:{ all -> 0x1221 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x1221 }
            r1.<init>(r0)     // Catch:{ all -> 0x1221 }
            java.util.Iterator r7 = r1.iterator()     // Catch:{ all -> 0x1221 }
        L_0x0408:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0442
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x1221 }
            X.0zc r0 = (X.C17870zc) r0     // Catch:{ all -> 0x1221 }
            boolean r1 = r0.A05     // Catch:{ all -> 0x1221 }
            if (r1 == 0) goto L_0x0408
            r0.A05 = r4     // Catch:{ all -> 0x1221 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x1221 }
            java.util.Map r0 = r0.A06     // Catch:{ all -> 0x1221 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x1221 }
            r1.<init>(r0)     // Catch:{ all -> 0x1221 }
            java.util.Iterator r6 = r1.iterator()     // Catch:{ all -> 0x1221 }
        L_0x0429:
            boolean r0 = r6.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0408
            java.lang.Object r0 = r6.next()     // Catch:{ all -> 0x1221 }
            X.3aG r0 = (X.C70203aG) r0     // Catch:{ all -> 0x1221 }
            X.3aL r1 = r0.A02     // Catch:{ all -> 0x1221 }
            if (r1 == 0) goto L_0x0429
            r1.A08()     // Catch:{ all -> 0x1221 }
            X.2c1 r0 = r5.A05     // Catch:{ all -> 0x1221 }
            X.C49322c1.A00(r0, r1)     // Catch:{ all -> 0x1221 }
            goto L_0x0429
        L_0x0442:
            r0 = 0
            r12.A0E = r0     // Catch:{ all -> 0x1221 }
            java.util.HashSet r0 = r12.A0Q     // Catch:{ all -> 0x1221 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x1221 }
            if (r0 != 0) goto L_0x04b3
            java.util.Map r0 = r2.A0i     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x049c
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x1221 }
            java.util.Iterator r9 = r0.iterator()     // Catch:{ all -> 0x1221 }
        L_0x0459:
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x049f
            java.lang.Object r5 = r9.next()     // Catch:{ all -> 0x1221 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x1221 }
            java.util.HashSet r1 = r12.A0Q     // Catch:{ all -> 0x1221 }
            java.lang.Object r0 = r5.getKey()     // Catch:{ all -> 0x1221 }
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x0459
            int[] r0 = r12.A0E     // Catch:{ all -> 0x1221 }
            if (r0 != 0) goto L_0x047f
            java.util.List r0 = r2.A0e     // Catch:{ all -> 0x1221 }
            int r0 = r0.size()     // Catch:{ all -> 0x1221 }
            int[] r0 = new int[r0]     // Catch:{ all -> 0x1221 }
            r12.A0E = r0     // Catch:{ all -> 0x1221 }
        L_0x047f:
            java.lang.Object r7 = r5.getValue()     // Catch:{ all -> 0x1221 }
            X.0zM r7 = (X.C17720zM) r7     // Catch:{ all -> 0x1221 }
            r6 = 0
            short r5 = r7.A00     // Catch:{ all -> 0x1221 }
        L_0x0488:
            if (r6 >= r5) goto L_0x0459
            java.lang.Object r0 = r7.A03(r6)     // Catch:{ all -> 0x1221 }
            X.0zN r0 = (X.C17730zN) r0     // Catch:{ all -> 0x1221 }
            long r0 = r0.A02     // Catch:{ all -> 0x1221 }
            int r1 = r2.A09(r0)     // Catch:{ all -> 0x1221 }
            X.C17600zA.A0G(r12, r2, r1, r10)     // Catch:{ all -> 0x1221 }
            int r6 = r6 + 1
            goto L_0x0488
        L_0x049c:
            r0 = 0
            r12.A0E = r0     // Catch:{ all -> 0x1221 }
        L_0x049f:
            boolean r0 = X.C46182Pb.A00     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x04b3
            int[] r5 = r12.A0E     // Catch:{ all -> 0x1221 }
            if (r0 == 0) goto L_0x04b3
            if (r5 == 0) goto L_0x04b3
            r1 = 0
        L_0x04aa:
            int r0 = r5.length     // Catch:{ all -> 0x1221 }
            if (r1 >= r0) goto L_0x04b3
            r2.A0A(r1)     // Catch:{ all -> 0x1221 }
            int r1 = r1 + 1
            goto L_0x04aa
        L_0x04b3:
            if (r28 == 0) goto L_0x04b8
            X.C27041cY.A00()
        L_0x04b8:
            X.0zB r0 = r12.A0I
            int r0 = r0.A01()
            int r1 = r0 + -1
        L_0x04c0:
            if (r1 < 0) goto L_0x04d0
            X.0zB r0 = r12.A0I
            java.lang.Object r0 = r0.A06(r1)
            com.facebook.litho.ComponentHost r0 = (com.facebook.litho.ComponentHost) r0
            r0.A0O(r10)
            int r1 = r1 + -1
            goto L_0x04c0
        L_0x04d0:
            if (r16 == 0) goto L_0x04d9
            java.lang.String r0 = "PREPARE_MOUNT_START"
            r5 = r16
            r5.A00(r0)
        L_0x04d9:
            r5 = r12
            boolean r24 = X.C27041cY.A02()
            if (r24 == 0) goto L_0x04e5
            java.lang.String r0 = "prepareMount"
            X.C27041cY.A01(r0)
        L_0x04e5:
            long[] r0 = r12.A0F
            if (r0 == 0) goto L_0x0688
            int r0 = r0.length
            if (r0 <= 0) goto L_0x0505
            boolean r0 = X.C17600zA.A0L(r12, r2, r4)
            if (r0 == 0) goto L_0x0505
            java.lang.Integer r6 = X.AnonymousClass07B.A01
            java.lang.String r1 = "Disppear animations cannot target the root LithoView! "
            X.0zY r0 = r12.A0N
            java.lang.String r0 = X.C17600zA.A04(r12, r0)
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.String r0 = "MountState:DisappearAnimTargetingRoot"
            X.C09070gU.A01(r6, r0, r1)
        L_0x0505:
            r15 = 0
            r7 = 1
        L_0x0507:
            long[] r0 = r5.A0F
            int r0 = r0.length
            if (r7 >= r0) goto L_0x068d
            boolean r0 = X.C17600zA.A0L(r5, r2, r7)
            if (r0 == 0) goto L_0x0684
            X.15v r0 = r5.A05
            int r6 = X.C17600zA.A00(r0, r7)
            r9 = r7
        L_0x0519:
            if (r9 > r6) goto L_0x052f
            X.0zY r0 = r5.A0M(r9)
            if (r0 != 0) goto L_0x052c
            X.15v r0 = r5.A05
            X.0zN r1 = r0.A0A(r9)
            X.15v r0 = r5.A05
            X.C17600zA.A0D(r5, r9, r1, r0)
        L_0x052c:
            int r9 = r9 + 1
            goto L_0x0519
        L_0x052f:
            X.0zY r14 = r5.A0M(r7)
            X.0zB r9 = r5.A0I
            r0 = 0
            java.lang.Object r9 = r9.A07(r0)
            com.facebook.litho.ComponentHost r9 = (com.facebook.litho.ComponentHost) r9
            X.0zY r1 = r5.A0M(r7)
            com.facebook.litho.ComponentHost r11 = r1.A05
            if (r11 == r9) goto L_0x0594
            java.lang.Object r0 = r1.A00()
            r26 = 0
            r27 = 0
        L_0x054d:
            if (r11 == r9) goto L_0x0562
            int r12 = r11.getLeft()
            int r26 = r26 + r12
            int r12 = r11.getTop()
            int r27 = r27 + r12
            android.view.ViewParent r11 = r11.getParent()
            com.facebook.litho.ComponentHost r11 = (com.facebook.litho.ComponentHost) r11
            goto L_0x054d
        L_0x0562:
            boolean r11 = r0 instanceof android.view.View
            if (r11 == 0) goto L_0x05cf
            r11 = r0
            android.view.View r11 = (android.view.View) r11
            int r12 = r11.getLeft()
            int r26 = r26 + r12
            int r12 = r11.getTop()
            int r27 = r27 + r12
            int r28 = r11.getWidth()
            int r28 = r28 + r26
            int r29 = r11.getHeight()
        L_0x057f:
            int r29 = r29 + r27
            com.facebook.litho.ComponentHost r11 = r1.A05
            r11.A0J(r7, r1)
            r30 = 0
            r25 = r0
            X.C17600zA.A0J(r25, r26, r27, r28, r29, r30)
            android.graphics.Rect r0 = X.C17600zA.A0V
            r9.A0K(r7, r1, r0)
            r1.A05 = r9
        L_0x0594:
            r13 = r7
            if (r7 == 0) goto L_0x1219
            r5.A01 = r7
            r5.A00 = r6
        L_0x059b:
            if (r13 > r6) goto L_0x05e9
            X.0zY r12 = r5.A0M(r13)
            X.0zB r9 = r5.A0J
            long[] r0 = r5.A0F
            r0 = r0[r13]
            r9.A0B(r0)
            X.0zR r11 = r12.A04
            if (r11 == 0) goto L_0x05b9
            boolean r9 = r11.A0v()
            if (r9 == 0) goto L_0x05b9
            X.0zB r9 = r5.A0H
            r9.A0B(r0)
        L_0x05b9:
            boolean r0 = r11 instanceof X.C17670zH
            if (r0 == 0) goto L_0x05cc
            X.0zB r0 = r5.A0I
            java.lang.Object r1 = r12.A00()
            com.facebook.litho.ComponentHost r1 = (com.facebook.litho.ComponentHost) r1
            int r1 = r0.A03(r1)
            r0.A0A(r1)
        L_0x05cc:
            int r13 = r13 + 1
            goto L_0x059b
        L_0x05cf:
            r11 = r0
            android.graphics.drawable.Drawable r11 = (android.graphics.drawable.Drawable) r11
            android.graphics.Rect r11 = r11.getBounds()
            int r12 = r11.left
            int r26 = r26 + r12
            int r28 = r11.width()
            int r28 = r28 + r26
            int r12 = r11.top
            int r27 = r27 + r12
            int r29 = r11.height()
            goto L_0x057f
        L_0x05e9:
            X.0zP r1 = r14.A07
            java.util.Map r0 = r5.A0R
            java.lang.Object r11 = r0.get(r1)
            X.0zM r11 = (X.C17720zM) r11
            if (r11 != 0) goto L_0x05ff
            X.0zM r11 = new X.0zM
            r11.<init>()
            java.util.Map r0 = r5.A0R
            r0.put(r1, r11)
        L_0x05ff:
            long[] r0 = r5.A0F
            r0 = r0[r7]
            r12 = 0
            int r9 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r9 != 0) goto L_0x067c
            r9 = 3
        L_0x060a:
            r11.A04(r9, r14)
            com.facebook.litho.ComponentHost r9 = r14.A05
            java.lang.Object r1 = r14.A00()
            boolean r0 = r1 instanceof android.graphics.drawable.Drawable
            if (r0 == 0) goto L_0x0660
            X.0eJ r0 = r9.A01
            if (r0 != 0) goto L_0x0622
            X.0eJ r0 = new X.0eJ
            r0.<init>()
            r9.A01 = r0
        L_0x0622:
            X.0eJ r1 = r9.A01
            X.0eJ r0 = r9.A03
            X.C15070ug.A01(r7, r1, r0)
        L_0x0629:
            com.facebook.litho.ComponentHost.A0A(r9)
            X.0eJ r1 = r9.A02
            X.0eJ r0 = r9.A04
            X.C15070ug.A01(r7, r1, r0)
            com.facebook.litho.ComponentHost.A0B(r9)
            java.util.ArrayList r0 = r9.A0E
            if (r0 != 0) goto L_0x0641
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r9.A0E = r0
        L_0x0641:
            java.util.ArrayList r0 = r9.A0E
            r0.add(r14)
            if (r15 != 0) goto L_0x064e
            java.util.ArrayList r15 = new java.util.ArrayList
            r0 = 2
            r15.<init>(r0)
        L_0x064e:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)
            r15.add(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            r15.add(r0)
            int r7 = r6 + 1
            goto L_0x0507
        L_0x0660:
            boolean r0 = r1 instanceof android.view.View
            if (r0 == 0) goto L_0x0629
            X.0eJ r0 = r9.A06
            if (r0 != 0) goto L_0x066f
            X.0eJ r0 = new X.0eJ
            r0.<init>()
            r9.A06 = r0
        L_0x066f:
            X.0eJ r1 = r9.A06
            X.0eJ r0 = r9.A05
            X.C15070ug.A01(r7, r1, r0)
            r9.A0I = r10
            r9.A0I(r7, r14)
            goto L_0x0629
        L_0x067c:
            r9 = 16
            long r0 = r0 >> r9
            r12 = 7
            long r0 = r0 & r12
            int r9 = (int) r0
            goto L_0x060a
        L_0x0684:
            int r7 = r7 + 1
            goto L_0x0507
        L_0x0688:
            java.util.List r15 = java.util.Collections.emptyList()
            goto L_0x068f
        L_0x068d:
            if (r15 == 0) goto L_0x0688
        L_0x068f:
            X.0zC r1 = r5.A0P
            r1.A01 = r4
            r1.A00 = r4
            r1.A02 = r4
            long[] r0 = r5.A0F
            if (r0 == 0) goto L_0x071d
            r11 = 0
            r9 = 0
        L_0x069d:
            long[] r1 = r5.A0F
            int r0 = r1.length
            if (r11 >= r0) goto L_0x071b
            r0 = r1[r11]
            int r0 = r2.A09(r0)
            if (r0 >= 0) goto L_0x0716
            r7 = 0
        L_0x06ab:
            r1 = -1
            if (r7 != 0) goto L_0x0713
            r6 = -1
        L_0x06af:
            X.0zY r13 = r5.A0M(r11)
            int r0 = r15.size()
            if (r0 <= r9) goto L_0x06da
            r25 = r15
            r26 = r9
            java.lang.Object r0 = r25.get(r26)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            if (r0 != r11) goto L_0x06da
            int r0 = r9 + 1
            java.lang.Object r0 = r15.get(r0)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r11 = r0.intValue()
            int r9 = r9 + 2
        L_0x06d7:
            int r11 = r11 + 1
            goto L_0x069d
        L_0x06da:
            if (r6 != r1) goto L_0x06ea
            X.0zB r0 = r5.A0I
            X.C17600zA.A0C(r5, r11, r0)
        L_0x06e1:
            X.0zC r1 = r5.A0P
            int r0 = r1.A02
            int r0 = r0 + 1
            r1.A02 = r0
            goto L_0x06d7
        L_0x06ea:
            long r0 = r7.A07
            if (r13 == 0) goto L_0x06e1
            com.facebook.litho.ComponentHost r12 = r13.A05
            X.0zB r7 = r5.A0I
            java.lang.Object r0 = r7.A07(r0)
            if (r12 == r0) goto L_0x06fc
            X.C17600zA.A0C(r5, r11, r7)
            goto L_0x06e1
        L_0x06fc:
            if (r6 == r11) goto L_0x070a
            r12.A0M(r13, r11, r6)
            X.0zC r1 = r5.A0P
            int r0 = r1.A00
            int r0 = r0 + 1
            r1.A00 = r0
            goto L_0x06d7
        L_0x070a:
            X.0zC r1 = r5.A0P
            int r0 = r1.A01
            int r0 = r0 + 1
            r1.A01 = r0
            goto L_0x06d7
        L_0x0713:
            int r6 = r7.A00
            goto L_0x06af
        L_0x0716:
            X.0zN r7 = r2.A0A(r0)
            goto L_0x06ab
        L_0x071b:
            X.0zC r1 = r5.A0P
        L_0x071d:
            if (r16 == 0) goto L_0x0736
            int r0 = r1.A02
            java.lang.String r6 = "unmounted_count"
            r11 = r16
            r11.A01(r6, r0)
            int r0 = r1.A00
            java.lang.String r6 = "moved_count"
            r11.A01(r6, r0)
            int r0 = r1.A01
            java.lang.String r1 = "unchanged_count"
            r11.A01(r1, r0)
        L_0x0736:
            X.0zB r6 = r5.A0I
            r0 = 0
            java.lang.Object r6 = r6.A07(r0)
            if (r6 != 0) goto L_0x0751
            com.facebook.litho.LithoView r11 = r5.A0M
            r11.A0O(r10)
            X.0zB r9 = r5.A0I
            r9.A0D(r0, r11)
            X.0zB r7 = r5.A0J
            X.0zY r6 = r5.A0N
            r7.A0D(r0, r6)
        L_0x0751:
            java.util.List r0 = r2.A0e
            int r9 = r0.size()
            long[] r0 = r5.A0F
            if (r0 == 0) goto L_0x075e
            int r0 = r0.length
            if (r9 == r0) goto L_0x0762
        L_0x075e:
            long[] r0 = new long[r9]
            r5.A0F = r0
        L_0x0762:
            r7 = 0
        L_0x0763:
            if (r7 >= r9) goto L_0x0772
            long[] r6 = r5.A0F
            X.0zN r0 = r2.A0A(r7)
            long r0 = r0.A02
            r6[r7] = r0
            int r7 = r7 + 1
            goto L_0x0763
        L_0x0772:
            if (r24 == 0) goto L_0x0777
            X.C27041cY.A00()
        L_0x0777:
            if (r16 == 0) goto L_0x0780
            java.lang.String r0 = "PREPARE_MOUNT_END"
            r5 = r16
            r5.A00(r0)
        L_0x0780:
            r0 = r39
            X.0zD r6 = r0.A0O
            r6.A01 = r4
            r6.A03 = r4
            r6.A04 = r4
            r6.A02 = r4
            r0 = 0
            r6.A00 = r0
            boolean r0 = r6.A0E
            if (r0 == 0) goto L_0x07c1
            java.util.List r0 = r6.A07
            r0.clear()
            java.util.List r0 = r6.A08
            r0.clear()
            java.util.List r0 = r6.A0A
            r0.clear()
            java.util.List r0 = r6.A0C
            r0.clear()
            java.util.List r0 = r6.A05
            r0.clear()
            java.util.List r0 = r6.A06
            r0.clear()
            java.util.List r0 = r6.A09
            r0.clear()
            java.util.List r0 = r6.A0B
            r0.clear()
            java.util.List r0 = r6.A0D
            r0.clear()
        L_0x07c1:
            r6.A0F = r4
            if (r16 == 0) goto L_0x081a
            r0 = r23
            r1 = r16
            boolean r0 = r0.BHH(r1)
            if (r0 == 0) goto L_0x081a
            r0 = r39
            X.0zD r5 = r0.A0O
            r5.A0F = r10
            boolean r0 = r5.A0E
            if (r0 != 0) goto L_0x081a
            r5.A0E = r10
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A07 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A08 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A0A = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A0C = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A05 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A06 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A09 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A0B = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A0D = r0
        L_0x081a:
            if (r18 == 0) goto L_0x098d
            r6 = r39
            r7 = r19
            r24 = r42
            android.graphics.Rect r0 = r6.A0G
            boolean r0 = r0.isEmpty()
            r5 = 0
            if (r0 != 0) goto L_0x0983
            int r9 = r7.left
            android.graphics.Rect r1 = r6.A0G
            int r0 = r1.left
            if (r9 != r0) goto L_0x0983
            int r0 = r7.right
            int r1 = r1.right
            if (r0 != r1) goto L_0x0983
            java.util.ArrayList r0 = r2.A0d
            r15 = r0
            java.util.ArrayList r11 = r2.A0c
            java.util.List r0 = r2.A0e
            int r9 = r0.size()
            int r0 = r7.top
            if (r0 > 0) goto L_0x084e
            android.graphics.Rect r0 = r6.A0G
            int r0 = r0.top
            if (r0 <= 0) goto L_0x08c2
        L_0x084e:
            int r0 = r6.A03
            if (r0 >= r9) goto L_0x0885
            int r1 = r7.top
            java.lang.Object r0 = r11.get(r0)
            X.0zN r0 = (X.C17730zN) r0
            android.graphics.Rect r0 = r0.A08
            int r0 = r0.bottom
            if (r1 < r0) goto L_0x0885
            int r0 = r6.A03
            java.lang.Object r0 = r11.get(r0)
            X.0zN r0 = (X.C17730zN) r0
            long r0 = r0.A02
            int r1 = r2.A09(r0)
            int[] r0 = r6.A0E
            r12 = 0
            if (r0 == 0) goto L_0x0878
            r0 = r0[r1]
            if (r0 <= 0) goto L_0x0878
            r12 = 1
        L_0x0878:
            if (r12 != 0) goto L_0x087f
            X.0zB r0 = r6.A0I
            X.C17600zA.A0C(r6, r1, r0)
        L_0x087f:
            int r0 = r6.A03
            int r0 = r0 + r10
            r6.A03 = r0
            goto L_0x084e
        L_0x0885:
            int r0 = r6.A03
            if (r0 <= 0) goto L_0x08c2
            int r1 = r7.top
            int r0 = r0 - r10
            java.lang.Object r0 = r11.get(r0)
            X.0zN r0 = (X.C17730zN) r0
            android.graphics.Rect r0 = r0.A08
            int r0 = r0.bottom
            if (r1 >= r0) goto L_0x08c2
            int r0 = r6.A03
            int r0 = r0 - r10
            r6.A03 = r0
            java.lang.Object r12 = r11.get(r0)
            X.0zN r12 = (X.C17730zN) r12
            long r0 = r12.A02
            int r13 = r2.A09(r0)
            X.0zY r13 = r6.A0M(r13)
            if (r13 != 0) goto L_0x0885
            int r0 = r2.A09(r0)
            X.C17600zA.A0D(r6, r0, r12, r2)
            java.util.Set r13 = r6.A0U
            long r0 = r12.A02
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r13.add(r0)
            goto L_0x0885
        L_0x08c2:
            com.facebook.litho.LithoView r0 = r6.A0M
            int r1 = r0.getHeight()
            int r0 = r7.bottom
            if (r0 < r1) goto L_0x08d2
            android.graphics.Rect r0 = r6.A0G
            int r0 = r0.bottom
            if (r0 >= r1) goto L_0x0946
        L_0x08d2:
            int r0 = r6.A04
            if (r0 >= r9) goto L_0x0910
            int r1 = r7.bottom
            java.lang.Object r0 = r15.get(r0)
            X.0zN r0 = (X.C17730zN) r0
            android.graphics.Rect r0 = r0.A08
            int r0 = r0.top
            if (r1 <= r0) goto L_0x0910
            int r0 = r6.A04
            java.lang.Object r12 = r15.get(r0)
            X.0zN r12 = (X.C17730zN) r12
            long r0 = r12.A02
            int r11 = r2.A09(r0)
            X.0zY r11 = r6.A0M(r11)
            if (r11 != 0) goto L_0x090a
            int r0 = r2.A09(r0)
            X.C17600zA.A0D(r6, r0, r12, r2)
            java.util.Set r11 = r6.A0U
            long r0 = r12.A02
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r11.add(r0)
        L_0x090a:
            int r0 = r6.A04
            int r0 = r0 + r10
            r6.A04 = r0
            goto L_0x08d2
        L_0x0910:
            int r0 = r6.A04
            if (r0 <= 0) goto L_0x0946
            int r1 = r7.bottom
            int r0 = r0 - r10
            java.lang.Object r0 = r15.get(r0)
            X.0zN r0 = (X.C17730zN) r0
            android.graphics.Rect r0 = r0.A08
            int r0 = r0.top
            if (r1 > r0) goto L_0x0946
            int r0 = r6.A04
            int r0 = r0 - r10
            r6.A04 = r0
            java.lang.Object r0 = r15.get(r0)
            X.0zN r0 = (X.C17730zN) r0
            long r0 = r0.A02
            int r1 = r2.A09(r0)
            int[] r0 = r6.A0E
            r9 = 0
            if (r0 == 0) goto L_0x093e
            r0 = r0[r1]
            if (r0 <= 0) goto L_0x093e
            r9 = 1
        L_0x093e:
            if (r9 != 0) goto L_0x0910
            X.0zB r0 = r6.A0I
            X.C17600zA.A0C(r6, r1, r0)
            goto L_0x0910
        L_0x0946:
            X.0zB r0 = r6.A0H
            int r11 = r0.A01()
        L_0x094c:
            if (r5 >= r11) goto L_0x0985
            X.0zB r0 = r6.A0H
            java.lang.Object r10 = r0.A06(r5)
            X.0zY r10 = (X.C17830zY) r10
            long r0 = r0.A04(r5)
            java.util.Set r9 = r6.A0U
            java.lang.Long r7 = java.lang.Long.valueOf(r0)
            boolean r7 = r9.contains(r7)
            if (r7 != 0) goto L_0x0980
            int r1 = r2.A09(r0)
            r0 = -1
            if (r1 == r0) goto L_0x0980
            X.0zR r0 = r10.A04
            boolean r0 = X.C17770zR.A09(r0)
            if (r0 == 0) goto L_0x0980
            java.lang.Object r0 = r10.A00()
            android.view.View r0 = (android.view.View) r0
            r1 = r24
            X.C17600zA.A05(r0, r1)
        L_0x0980:
            int r5 = r5 + 1
            goto L_0x094c
        L_0x0983:
            r0 = 0
            goto L_0x098b
        L_0x0985:
            java.util.Set r0 = r6.A0U
            r0.clear()
            r0 = 1
        L_0x098b:
            if (r0 != 0) goto L_0x0e33
        L_0x098d:
            r0 = r39
            X.0zB r5 = r0.A0J
            r0 = 0
            java.lang.Object r26 = r5.A07(r0)
            r0 = r26
            X.0zY r0 = (X.C17830zY) r0
            r26 = r0
            java.util.List r0 = r2.A0e
            int r25 = r0.size()
            r7 = 0
        L_0x09a4:
            r0 = r25
            if (r7 >= r0) goto L_0x0de7
            X.0zN r5 = r2.A0A(r7)
            X.0zR r0 = r5.A09
            r35 = r0
            if (r17 == 0) goto L_0x09b9
            java.lang.String r0 = r0.A1A()
            X.C27041cY.A01(r0)
        L_0x09b9:
            r0 = r39
            X.0zY r6 = r0.A0M(r7)
            r10 = 0
            if (r6 == 0) goto L_0x09c3
            r10 = 1
        L_0x09c3:
            if (r18 == 0) goto L_0x09f9
            r9 = 0
            if (r6 == 0) goto L_0x09d9
            java.lang.Object r1 = r6.A00()
            boolean r0 = r1 instanceof com.facebook.litho.ComponentHost
            if (r0 == 0) goto L_0x09d9
            com.facebook.litho.ComponentHost r1 = (com.facebook.litho.ComponentHost) r1
            int r0 = r1.A0C()
            if (r0 <= 0) goto L_0x09d9
            r9 = 1
        L_0x09d9:
            if (r9 != 0) goto L_0x09f9
            android.graphics.Rect r0 = r5.A08
            r11 = r19
            boolean r0 = android.graphics.Rect.intersects(r11, r0)
            if (r0 != 0) goto L_0x09f9
            r0 = r39
            int[] r0 = r0.A0E
            r1 = 0
            if (r0 == 0) goto L_0x09f1
            r0 = r0[r7]
            if (r0 <= 0) goto L_0x09f1
            r1 = 1
        L_0x09f1:
            if (r1 != 0) goto L_0x09f9
            if (r6 == 0) goto L_0x0de4
            r0 = r26
            if (r6 != r0) goto L_0x0de4
        L_0x09f9:
            r0 = 1
        L_0x09fa:
            if (r0 == 0) goto L_0x0a30
            if (r10 != 0) goto L_0x0a30
            r9 = r39
            X.C17600zA.A0D(r9, r7, r5, r2)
            int[] r0 = r9.A0E
            r1 = 0
            if (r0 == 0) goto L_0x0a0d
            r0 = r0[r7]
            if (r0 <= 0) goto L_0x0a0d
            r1 = 1
        L_0x0a0d:
            if (r1 == 0) goto L_0x0a26
            if (r18 == 0) goto L_0x0a26
            r0 = r35
            boolean r0 = r0.A0v()
            if (r0 == 0) goto L_0x0a26
            X.0zY r0 = r9.A0M(r7)
            java.lang.Object r0 = r0.A00()
            android.view.View r0 = (android.view.View) r0
            X.C17600zA.A05(r0, r4)
        L_0x0a26:
            if (r17 == 0) goto L_0x0a2b
            X.C27041cY.A00()
        L_0x0a2b:
            int r7 = r7 + 1
            r4 = 0
            goto L_0x09a4
        L_0x0a30:
            if (r0 != 0) goto L_0x0a3e
            if (r10 == 0) goto L_0x0a3e
            r0 = r39
            X.0zB r0 = r0.A0I
            r4 = r39
            X.C17600zA.A0C(r4, r7, r0)
            goto L_0x0a26
        L_0x0a3e:
            if (r10 == 0) goto L_0x0a26
            r0 = r39
            boolean r0 = r0.A09
            if (r0 == 0) goto L_0x0c51
            r0 = r39
            X.15v r0 = r0.A05
            if (r0 == 0) goto L_0x0a54
            int r1 = r0.A0Y
            int r0 = r2.A06
            r29 = 1
            if (r1 == r0) goto L_0x0a56
        L_0x0a54:
            r29 = 0
        L_0x0a56:
            long r14 = java.lang.System.nanoTime()
            X.0zP r0 = r6.A07
            r28 = r0
            r33 = r39
            r32 = r36
            X.0zR r9 = r5.A09
            r27 = r9
            X.0zR r1 = r6.A04
            r31 = r1
            if (r9 == 0) goto L_0x1238
            int r10 = r5.A01
            int r11 = r5.A06
            int r0 = r6.A03
            r24 = 1
            if (r11 != r0) goto L_0x0ab1
            android.graphics.Rect r0 = r5.A08
            r30 = r0
            java.lang.Object r13 = r6.A00()
            int r11 = r0.width()
            boolean r12 = r13 instanceof android.graphics.drawable.Drawable
            if (r12 == 0) goto L_0x0ddb
            r0 = r13
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            android.graphics.Rect r0 = r0.getBounds()
            int r0 = r0.width()
        L_0x0a91:
            if (r11 != r0) goto L_0x0aa8
            r0 = r30
            int r11 = r0.height()
            if (r12 == 0) goto L_0x0dd3
            android.graphics.drawable.Drawable r13 = (android.graphics.drawable.Drawable) r13
            android.graphics.Rect r0 = r13.getBounds()
            int r0 = r0.height()
        L_0x0aa5:
            r12 = 1
            if (r11 == r0) goto L_0x0aa9
        L_0x0aa8:
            r12 = 0
        L_0x0aa9:
            if (r12 != 0) goto L_0x0d9a
            boolean r0 = r9.A0z()
            if (r0 == 0) goto L_0x0d9a
        L_0x0ab1:
            r24 = 1
        L_0x0ab3:
            if (r24 != 0) goto L_0x0b0c
            X.0zF r10 = r5.A0C
            X.0zF r9 = r6.A08
            r11 = 1
            if (r9 != 0) goto L_0x0abe
            if (r10 != 0) goto L_0x0b09
        L_0x0abe:
            if (r9 == 0) goto L_0x0c88
            if (r9 == r10) goto L_0x0c85
            if (r10 == 0) goto L_0x0b06
            android.graphics.drawable.Drawable r1 = r9.A04
            android.graphics.drawable.Drawable r0 = r10.A04
            boolean r0 = X.C49982dB.A00(r1, r0)
            if (r0 == 0) goto L_0x0b06
            android.graphics.drawable.Drawable r1 = r9.A05
            android.graphics.drawable.Drawable r0 = r10.A05
            boolean r0 = X.C49982dB.A00(r1, r0)
            if (r0 == 0) goto L_0x0b06
            android.graphics.Rect r1 = r9.A03
            android.graphics.Rect r0 = r10.A03
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0b06
            android.graphics.Rect r1 = r9.A02
            android.graphics.Rect r0 = r10.A02
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0b06
            X.0zG r1 = r9.A06
            X.0zG r0 = r10.A06
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0b06
            int r1 = r9.A00
            int r0 = r10.A00
            if (r1 != r0) goto L_0x0b06
            android.animation.StateListAnimator r0 = r9.A01
            android.animation.StateListAnimator r1 = r10.A01
            boolean r0 = X.C49992dC.A00(r0, r1)
            if (r0 != 0) goto L_0x0c85
        L_0x0b06:
            r0 = 0
        L_0x0b07:
            if (r0 != 0) goto L_0x0c88
        L_0x0b09:
            r9 = 0
            if (r11 == 0) goto L_0x0b0d
        L_0x0b0c:
            r9 = 1
        L_0x0b0d:
            if (r24 == 0) goto L_0x0c81
            r0 = r33
            int r1 = r0.A02
            r0 = r32
            if (r1 == r0) goto L_0x0b28
            X.0zR r0 = r6.A04
            boolean r0 = r0 instanceof X.C17670zH
            if (r0 == 0) goto L_0x0b28
            java.lang.Object r0 = r6.A00()
            com.facebook.litho.ComponentHost r0 = (com.facebook.litho.ComponentHost) r0
            r10 = r33
            X.C17600zA.A0F(r10, r0)
        L_0x0b28:
            X.C17600zA.A09(r6)
            com.facebook.litho.ComponentHost r0 = r6.A05
            r0.A0I(r7, r6)
        L_0x0b30:
            boolean r0 = r6.A09
            if (r0 == 0) goto L_0x0b4a
            r0 = r31
            X.0p4 r0 = r0.A03
            if (r0 != 0) goto L_0x0b3e
            r0 = r33
            X.0p4 r0 = r0.A0K
        L_0x0b3e:
            java.lang.Object r1 = r6.A00()
            r10 = r31
            r10.A0k(r0, r1)
            r0 = 0
            r6.A09 = r0
        L_0x0b4a:
            X.0zR r0 = r5.A09
            r6.A04 = r0
            if (r0 == 0) goto L_0x1230
            int r0 = r5.A04
            r6.A01 = r0
            int r0 = r5.A05
            r6.A00 = r0
            int r0 = r5.A06
            r6.A03 = r0
            X.0zP r0 = r5.A0B
            r6.A07 = r0
            r0 = 0
            r6.A06 = r0
            r6.A08 = r0
            X.38U r0 = r5.A03
            X.1jd r0 = r5.A0A
            if (r0 == 0) goto L_0x0b6d
            r6.A06 = r0
        L_0x0b6d:
            X.0zF r0 = r5.A0C
            if (r0 == 0) goto L_0x0b73
            r6.A08 = r0
        L_0x0b73:
            if (r24 == 0) goto L_0x0c75
            com.facebook.litho.ComponentHost r0 = r6.A05
            r0.A0H(r7, r6)
            X.0zR r10 = r5.A09
            boolean r0 = r10 instanceof X.C17670zH
            if (r0 != 0) goto L_0x0b9c
            java.lang.Object r9 = r6.A00()
            r1 = r33
            r0 = r31
            X.0p4 r0 = r0.A03
            if (r0 != 0) goto L_0x0b8e
            X.0p4 r0 = r1.A0K
        L_0x0b8e:
            r11 = r31
            r11.A0l(r0, r9)
            X.0p4 r0 = r10.A03
            if (r0 != 0) goto L_0x0b99
            X.0p4 r0 = r1.A0K
        L_0x0b99:
            r10.A0h(r0, r9)
        L_0x0b9c:
            X.C17600zA.A0A(r6)
        L_0x0b9f:
            java.lang.Object r0 = r6.A00()
            r9 = r33
            r10 = r27
            X.C17600zA.A0E(r9, r10, r0)
            r0 = 1
            r6.A09 = r0
            long r0 = r5.A02
            r10 = 0
            int r9 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r9 == 0) goto L_0x0be9
            android.graphics.Rect r9 = X.C17600zA.A0V
            r5.A00(r9)
            X.0zR r0 = r5.A09
            boolean r0 = X.C17770zR.A09(r0)
            if (r0 == 0) goto L_0x0bd0
            java.lang.Object r0 = r6.A00()
            android.view.View r0 = (android.view.View) r0
            boolean r0 = r0.isLayoutRequested()
            r34 = 1
            if (r0 != 0) goto L_0x0bd2
        L_0x0bd0:
            r34 = 0
        L_0x0bd2:
            java.lang.Object r29 = r6.A00()
            int r5 = r9.left
            int r1 = r9.top
            int r0 = r9.right
            int r9 = r9.bottom
            r30 = r5
            r31 = r1
            r32 = r0
            r33 = r9
            X.C17600zA.A0J(r29, r30, r31, r32, r33, r34)
        L_0x0be9:
            boolean r0 = r6.A02()
            if (r0 == 0) goto L_0x0bf4
            com.facebook.litho.ComponentHost r0 = r6.A05
            r0.A0G()
        L_0x0bf4:
            java.lang.Object r0 = r6.A00()
            boolean r0 = r0 instanceof android.graphics.drawable.Drawable
            if (r0 == 0) goto L_0x0c0b
            com.facebook.litho.ComponentHost r5 = r6.A05
            java.lang.Object r1 = r6.A00()
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            int r0 = r6.A01
            X.1jd r4 = r6.A06
            X.C15070ug.A02(r5, r1, r0, r4)
        L_0x0c0b:
            if (r24 == 0) goto L_0x0c1b
            r0 = r39
            X.0zb r4 = r0.A07
            if (r4 == 0) goto L_0x0c1b
            if (r28 == 0) goto L_0x0c1b
            r1 = 0
            r0 = r28
            r4.A09(r0, r1)
        L_0x0c1b:
            r0 = r39
            X.0zD r1 = r0.A0O
            boolean r0 = r1.A0F
            if (r0 == 0) goto L_0x0c51
            if (r24 == 0) goto L_0x0c6e
            java.util.List r1 = r1.A0A
            java.lang.String r0 = r35.A1A()
            r1.add(r0)
            r0 = r39
            X.0zD r0 = r0.A0O
            java.util.List r9 = r0.A0B
            long r4 = java.lang.System.nanoTime()
            long r4 = r4 - r14
            double r0 = (double) r4
            r4 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r0 = r0 / r4
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            r9.add(r0)
            r0 = r39
            X.0zD r1 = r0.A0O
            int r0 = r1.A04
            int r0 = r0 + 1
            r1.A04 = r0
        L_0x0c51:
            if (r18 == 0) goto L_0x0a26
            boolean r0 = r35.A0v()
            if (r0 == 0) goto L_0x0a26
            X.0zR r0 = r6.A04
            boolean r0 = X.C17770zR.A09(r0)
            if (r0 == 0) goto L_0x0a26
            java.lang.Object r1 = r6.A00()
            android.view.View r1 = (android.view.View) r1
            r0 = r37
            X.C17600zA.A05(r1, r0)
            goto L_0x0a26
        L_0x0c6e:
            int r0 = r1.A02
            int r0 = r0 + 1
            r1.A02 = r0
            goto L_0x0c51
        L_0x0c75:
            if (r9 == 0) goto L_0x0b9f
            com.facebook.litho.ComponentHost r0 = r6.A05
            r0.A0H(r7, r6)
            X.C17600zA.A0A(r6)
            goto L_0x0b9f
        L_0x0c81:
            if (r9 == 0) goto L_0x0b30
            goto L_0x0b28
        L_0x0c85:
            r0 = 1
            goto L_0x0b07
        L_0x0c88:
            X.1jd r10 = r5.A0A
            X.1jd r9 = r6.A06
            if (r9 != 0) goto L_0x0c90
            if (r10 != 0) goto L_0x0b09
        L_0x0c90:
            if (r9 == 0) goto L_0x0d97
            if (r9 == r10) goto L_0x0d95
            if (r9 == 0) goto L_0x0d90
            if (r10 == 0) goto L_0x0d90
            int r1 = r9.A0A
            int r0 = r10.A0A
            if (r1 != r0) goto L_0x0d90
            java.lang.String r1 = r9.A0U
            java.lang.String r0 = r10.A0U
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            float r1 = r9.A00
            float r0 = r10.A00
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0d90
            X.10N r1 = r9.A0E
            X.10N r0 = r10.A0E
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            boolean r1 = r9.A0W
            boolean r0 = r10.A0W
            if (r1 != r0) goto L_0x0d90
            boolean r1 = r9.A0V
            boolean r0 = r10.A0V
            if (r1 != r0) goto L_0x0d90
            java.lang.CharSequence r1 = r9.A0S
            java.lang.CharSequence r0 = r10.A0S
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0F
            X.10N r0 = r10.A0F
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            int r1 = r9.A08
            int r0 = r10.A08
            if (r1 != r0) goto L_0x0d90
            X.10N r1 = r9.A0G
            X.10N r0 = r10.A0G
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            int r1 = r9.A09
            int r0 = r10.A09
            if (r1 != r0) goto L_0x0d90
            X.10N r1 = r9.A0H
            X.10N r0 = r10.A0H
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0I
            X.10N r0 = r10.A0I
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0J
            X.10N r0 = r10.A0J
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0K
            X.10N r0 = r10.A0K
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0L
            X.10N r0 = r10.A0L
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0M
            X.10N r0 = r10.A0M
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            android.view.ViewOutlineProvider r1 = r9.A0D
            android.view.ViewOutlineProvider r0 = r10.A0D
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0N
            X.10N r0 = r10.A0N
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            float r1 = r9.A01
            float r0 = r10.A01
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0d90
            float r1 = r9.A04
            float r0 = r10.A04
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0d90
            int r1 = r9.A0B
            int r0 = r10.A0B
            if (r1 != r0) goto L_0x0d90
            X.10N r1 = r9.A0O
            X.10N r0 = r10.A0O
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            X.10N r1 = r9.A0P
            X.10N r0 = r10.A0P
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            float r1 = r9.A05
            float r0 = r10.A05
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0d90
            X.10N r1 = r9.A0Q
            X.10N r0 = r10.A0Q
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            java.lang.Object r1 = r9.A0T
            java.lang.Object r0 = r10.A0T
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 == 0) goto L_0x0d90
            android.util.SparseArray r0 = r9.A0C
            android.util.SparseArray r1 = r10.A0C
            boolean r0 = X.C49992dC.A00(r0, r1)
            if (r0 != 0) goto L_0x0d95
        L_0x0d90:
            r0 = 0
        L_0x0d91:
            if (r0 != 0) goto L_0x0d97
            goto L_0x0b09
        L_0x0d95:
            r0 = 1
            goto L_0x0d91
        L_0x0d97:
            r11 = 0
            goto L_0x0b09
        L_0x0d9a:
            if (r29 == 0) goto L_0x0dc1
            r0 = r24
            if (r10 != r0) goto L_0x0dbc
            boolean r0 = r1 instanceof X.C15060uf
            if (r0 == 0) goto L_0x0db8
            boolean r0 = r9 instanceof X.C15060uf
            if (r0 == 0) goto L_0x0db8
            boolean r10 = r1.A10()
            if (r10 == 0) goto L_0x0db6
            boolean r0 = r1.A12(r1, r9)
        L_0x0db2:
            if (r0 == 0) goto L_0x0db8
            goto L_0x0ab3
        L_0x0db6:
            r0 = 1
            goto L_0x0db2
        L_0x0db8:
            r24 = 0
            goto L_0x0ab3
        L_0x0dbc:
            r0 = 2
            if (r10 != r0) goto L_0x0dc1
            goto L_0x0ab1
        L_0x0dc1:
            boolean r0 = r1.A0r()
            if (r0 == 0) goto L_0x0ab1
            boolean r10 = r1.A10()
            if (r10 == 0) goto L_0x0ab1
            boolean r24 = r1.A12(r1, r9)
            goto L_0x0ab3
        L_0x0dd3:
            android.view.View r13 = (android.view.View) r13
            int r0 = r13.getHeight()
            goto L_0x0aa5
        L_0x0ddb:
            r0 = r13
            android.view.View r0 = (android.view.View) r0
            int r0 = r0.getWidth()
            goto L_0x0a91
        L_0x0de4:
            r0 = 0
            goto L_0x09fa
        L_0x0de7:
            if (r18 == 0) goto L_0x0e33
            r12 = r39
            r11 = r19
            boolean r0 = r11.isEmpty()
            if (r0 != 0) goto L_0x0e33
            java.util.ArrayList r10 = r2.A0d
            java.util.ArrayList r9 = r2.A0c
            java.util.List r0 = r2.A0e
            int r7 = r0.size()
            java.util.List r0 = r2.A0e
            int r0 = r0.size()
            r12.A04 = r0
            r6 = 0
            r5 = 0
        L_0x0e07:
            if (r5 >= r7) goto L_0x0e19
            int r1 = r11.bottom
            java.lang.Object r0 = r10.get(r5)
            X.0zN r0 = (X.C17730zN) r0
            android.graphics.Rect r0 = r0.A08
            int r0 = r0.top
            if (r1 > r0) goto L_0x0e9a
            r12.A04 = r5
        L_0x0e19:
            java.util.List r0 = r2.A0e
            int r0 = r0.size()
            r12.A03 = r0
        L_0x0e21:
            if (r6 >= r7) goto L_0x0e33
            int r1 = r11.top
            java.lang.Object r0 = r9.get(r6)
            X.0zN r0 = (X.C17730zN) r0
            android.graphics.Rect r0 = r0.A08
            int r0 = r0.bottom
            if (r1 >= r0) goto L_0x0e97
            r12.A03 = r6
        L_0x0e33:
            r12 = r39
            X.0zb r0 = r12.A07
            if (r0 == 0) goto L_0x0f0f
            boolean r15 = X.C27041cY.A02()
            if (r15 == 0) goto L_0x0e44
            java.lang.String r0 = "updateAnimatingMountContent"
            X.C27041cY.A01(r0)
        L_0x0e44:
            java.util.LinkedHashMap r11 = new java.util.LinkedHashMap
            java.util.HashSet r0 = r12.A0Q
            int r0 = r0.size()
            r11.<init>(r0)
            X.0zB r0 = r12.A0J
            int r10 = r0.A01()
            r9 = 0
        L_0x0e56:
            if (r9 >= r10) goto L_0x0e9e
            X.0zB r0 = r12.A0J
            java.lang.Object r7 = r0.A06(r9)
            X.0zY r7 = (X.C17830zY) r7
            X.0zP r6 = r7.A07
            r1 = 0
            if (r6 == 0) goto L_0x0e66
            r1 = 1
        L_0x0e66:
            if (r1 == 0) goto L_0x0e8c
            long r0 = r0.A04(r9)
            r13 = 0
            int r5 = (r0 > r13 ? 1 : (r0 == r13 ? 0 : -1))
            if (r5 != 0) goto L_0x0e8f
            r5 = 3
        L_0x0e73:
            java.lang.Object r1 = r11.get(r6)
            X.0zM r1 = (X.C17720zM) r1
            if (r1 != 0) goto L_0x0e85
            X.0zM r1 = new X.0zM
            r1.<init>()
            X.0zP r0 = r7.A07
            r11.put(r0, r1)
        L_0x0e85:
            java.lang.Object r0 = r7.A00()
            r1.A05(r5, r0)
        L_0x0e8c:
            int r9 = r9 + 1
            goto L_0x0e56
        L_0x0e8f:
            r5 = 16
            long r0 = r0 >> r5
            r13 = 7
            long r0 = r0 & r13
            int r5 = (int) r0
            goto L_0x0e73
        L_0x0e97:
            int r6 = r6 + 1
            goto L_0x0e21
        L_0x0e9a:
            int r5 = r5 + 1
            goto L_0x0e07
        L_0x0e9e:
            java.util.Set r0 = r11.entrySet()
            java.util.Iterator r6 = r0.iterator()
        L_0x0ea6:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0ec4
            java.lang.Object r0 = r6.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            X.0zb r5 = r12.A07
            java.lang.Object r1 = r0.getKey()
            X.0zP r1 = (X.C17750zP) r1
            java.lang.Object r0 = r0.getValue()
            X.0zM r0 = (X.C17720zM) r0
            r5.A09(r1, r0)
            goto L_0x0ea6
        L_0x0ec4:
            java.util.Map r0 = r12.A0R
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r11 = r0.iterator()
        L_0x0ece:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x0f0a
            java.lang.Object r10 = r11.next()
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10
            java.lang.Object r9 = r10.getValue()
            X.0zM r9 = (X.C17720zM) r9
            X.0zM r7 = new X.0zM
            r7.<init>()
            short r6 = r9.A00
            r5 = 0
        L_0x0ee8:
            if (r5 >= r6) goto L_0x0efe
            int r1 = r9.A01(r5)
            java.lang.Object r0 = r9.A03(r5)
            X.0zY r0 = (X.C17830zY) r0
            java.lang.Object r0 = r0.A00()
            r7.A04(r1, r0)
            int r5 = r5 + 1
            goto L_0x0ee8
        L_0x0efe:
            X.0zb r0 = r12.A07
            java.lang.Object r1 = r10.getKey()
            X.0zP r1 = (X.C17750zP) r1
            r0.A09(r1, r7)
            goto L_0x0ece
        L_0x0f0a:
            if (r15 == 0) goto L_0x0f0f
            X.C27041cY.A00()
        L_0x0f0f:
            boolean r0 = X.C17600zA.A0K(r12, r2)
            if (r0 == 0) goto L_0x0f9a
            X.0zQ r1 = r12.A06
            r0 = 0
            if (r1 == 0) goto L_0x0f1b
            r0 = 1
        L_0x0f1b:
            if (r0 == 0) goto L_0x0f9a
            X.0zb r10 = r12.A07
            boolean r12 = X.C27041cY.A02()
            if (r12 == 0) goto L_0x0f2a
            java.lang.String r0 = "runTransitions"
            X.C27041cY.A01(r0)
        L_0x0f2a:
            java.util.Map r0 = r10.A09
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r11 = r0.iterator()
        L_0x0f34:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x0f6d
            java.lang.Object r1 = r11.next()
            X.3aH r1 = (X.C70213aH) r1
            java.util.Map r0 = r10.A09
            java.lang.Object r0 = r0.get(r1)
            java.lang.Float r0 = (java.lang.Float) r0
            float r9 = r0.floatValue()
            X.0zP r5 = r1.A00
            X.1jf r0 = r10.A03
            java.util.Map r0 = r0.A02
            java.lang.Object r0 = r0.get(r5)
            X.0zc r0 = (X.C17870zc) r0
            X.0zM r7 = r0.A02
            if (r7 == 0) goto L_0x0f34
            X.1M8 r6 = r1.A01
            short r5 = r7.A00
            r1 = 0
        L_0x0f61:
            if (r1 >= r5) goto L_0x0f34
            java.lang.Object r0 = r7.A03(r1)
            r6.C5f(r0, r9)
            int r1 = r1 + 1
            goto L_0x0f61
        L_0x0f6d:
            java.util.Map r0 = r10.A09
            r0.clear()
            boolean r0 = X.C46182Pb.A00
            if (r0 == 0) goto L_0x0f80
            if (r0 != 0) goto L_0x0f80
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Trying to debug log animations without debug flag set!"
            r1.<init>(r0)
            throw r1
        L_0x0f80:
            X.3aL r0 = r10.A00
            if (r0 == 0) goto L_0x0f95
            X.2c3 r1 = r10.A04
            java.util.concurrent.CopyOnWriteArrayList r0 = r0.A00
            r0.add(r1)
            X.3aL r1 = r10.A00
            X.2PZ r0 = r10.A06
            r1.A09(r0)
            r0 = 0
            r10.A00 = r0
        L_0x0f95:
            if (r12 == 0) goto L_0x0f9a
            X.C27041cY.A00()
        L_0x0f9a:
            if (r42 == 0) goto L_0x0fc1
            if (r17 == 0) goto L_0x0fa3
            java.lang.String r0 = "processVisibilityOutputs"
            X.C27041cY.A01(r0)
        L_0x0fa3:
            if (r16 == 0) goto L_0x0fac
            java.lang.String r1 = "EVENT_PROCESS_VISIBILITY_OUTPUTS_START"
            r0 = r16
            r0.A00(r1)
        L_0x0fac:
            r6 = r39
            r1 = r19
            r0 = r16
            r6.A0Q(r2, r1, r0)
            if (r16 == 0) goto L_0x0fbc
            java.lang.String r1 = "EVENT_PROCESS_VISIBILITY_OUTPUTS_END"
            r0.A00(r1)
        L_0x0fbc:
            if (r17 == 0) goto L_0x0fc1
            X.C27041cY.A00()
        L_0x0fc1:
            r1 = 0
            r0 = r39
            r0.A06 = r1
            r0.A0D = r4
            boolean r11 = r0.A09
            r0.A09 = r4
            r0.A0C = r4
            r0.A0A = r4
            if (r19 == 0) goto L_0x0fd9
            android.graphics.Rect r5 = r0.A0G
            r0 = r19
            r5.set(r0)
        L_0x0fd9:
            r0 = r39
            r0.A05 = r1
            r1 = r36
            r0.A02 = r1
            r0.A05 = r2
            r14 = r0
            java.util.Map r0 = r0.A0S
            if (r0 == 0) goto L_0x105d
            r0.clear()
            r13 = 0
            java.util.List r0 = r2.A0f
            if (r0 != 0) goto L_0x1058
            r12 = 0
        L_0x0ff1:
            if (r13 >= r12) goto L_0x105d
            java.util.List r0 = r2.A0f
            if (r0 != 0) goto L_0x1051
            r10 = 0
        L_0x0ff8:
            long r5 = r10.A00
            long r0 = r10.A01
            r18 = -1
            r9 = 0
            int r7 = (r0 > r18 ? 1 : (r0 == r18 ? 0 : -1))
            if (r7 != 0) goto L_0x1048
            r0 = r9
        L_0x1004:
            com.facebook.litho.TestItem r7 = new com.facebook.litho.TestItem
            r7.<init>()
            int r1 = (r5 > r18 ? 1 : (r5 == r18 ? 0 : -1))
            if (r1 != 0) goto L_0x103f
            r1 = r9
        L_0x100e:
            r7.A00 = r1
            android.graphics.Rect r5 = r10.A03
            android.graphics.Rect r1 = r7.A03
            r1.set(r5)
            java.lang.String r1 = r10.A02
            r7.A02 = r1
            if (r0 == 0) goto L_0x1021
            java.lang.Object r9 = r0.A00()
        L_0x1021:
            r7.A01 = r9
            java.util.Map r0 = r14.A0S
            java.lang.Object r0 = r0.get(r1)
            java.util.Deque r0 = (java.util.Deque) r0
            if (r0 != 0) goto L_0x1032
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
        L_0x1032:
            r0.add(r7)
            java.util.Map r1 = r14.A0S
            java.lang.String r5 = r10.A02
            r1.put(r5, r0)
            int r13 = r13 + 1
            goto L_0x0ff1
        L_0x103f:
            X.0zB r1 = r14.A0I
            java.lang.Object r1 = r1.A07(r5)
            com.facebook.litho.ComponentHost r1 = (com.facebook.litho.ComponentHost) r1
            goto L_0x100e
        L_0x1048:
            X.0zB r7 = r14.A0J
            java.lang.Object r0 = r7.A07(r0)
            X.0zY r0 = (X.C17830zY) r0
            goto L_0x1004
        L_0x1051:
            java.lang.Object r10 = r0.get(r13)
            X.2FU r10 = (X.AnonymousClass2FU) r10
            goto L_0x0ff8
        L_0x1058:
            int r12 = r0.size()
            goto L_0x0ff1
        L_0x105d:
            X.0zB r0 = r14.A0I
            int r0 = r0.A01()
            int r5 = r0 + -1
        L_0x1065:
            if (r5 < 0) goto L_0x1075
            X.0zB r0 = r14.A0I
            java.lang.Object r0 = r0.A06(r5)
            com.facebook.litho.ComponentHost r0 = (com.facebook.litho.ComponentHost) r0
            r0.A0O(r4)
            int r5 = r5 + -1
            goto L_0x1065
        L_0x1075:
            if (r16 == 0) goto L_0x1184
            X.0zD r1 = r14.A0O
            boolean r0 = r1.A0F
            if (r0 == 0) goto L_0x11e7
            int r0 = r1.A01
            if (r0 == 0) goto L_0x11e7
            java.util.List r0 = r1.A07
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x11e7
            X.0zD r0 = r14.A0O
            int r5 = r0.A01
            java.lang.String r1 = "mounted_count"
            r0 = r16
            r0.A01(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A07
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.String[] r5 = (java.lang.String[]) r5
            java.lang.String r1 = "mounted_content"
            r0 = r16
            r0.A05(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A06
            java.lang.Double[] r0 = new java.lang.Double[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.Double[] r5 = (java.lang.Double[]) r5
            java.lang.String r1 = "mounted_time_ms"
            r0 = r16
            r0.A04(r1, r5)
            X.0zD r0 = r14.A0O
            int r5 = r0.A03
            java.lang.String r1 = "unmounted_count"
            r0 = r16
            r0.A01(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A08
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.String[] r5 = (java.lang.String[]) r5
            java.lang.String r1 = "unmounted_content"
            r0 = r16
            r0.A05(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A09
            java.lang.Double[] r0 = new java.lang.Double[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.Double[] r5 = (java.lang.Double[]) r5
            java.lang.String r1 = "unmounted_time_ms"
            r0 = r16
            r0.A04(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A05
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.String[] r5 = (java.lang.String[]) r5
            java.lang.String r1 = "mounted_extras"
            r0 = r16
            r0.A05(r1, r5)
            X.0zD r0 = r14.A0O
            int r5 = r0.A04
            java.lang.String r1 = "updated_count"
            r0 = r16
            r0.A01(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A0A
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.String[] r5 = (java.lang.String[]) r5
            java.lang.String r1 = "updated_content"
            r0 = r16
            r0.A05(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A0B
            java.lang.Double[] r0 = new java.lang.Double[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.Double[] r5 = (java.lang.Double[]) r5
            java.lang.String r1 = "updated_time_ms"
            r0 = r16
            r0.A04(r1, r5)
            X.0zD r0 = r14.A0O
            double r0 = r0.A00
            java.lang.String r27 = "visibility_handlers_total_time_ms"
            r5 = r16
            com.facebook.quicklog.QuickPerformanceLogger r7 = r5.A02
            int r6 = r5.A01
            int r5 = r5.A00
            r24 = r7
            r25 = r6
            r26 = r5
            r28 = r0
            r24.markerAnnotate(r25, r26, r27, r28)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A0C
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.String[] r5 = (java.lang.String[]) r5
            java.lang.String r1 = "visibility_handler"
            r0 = r16
            r0.A05(r1, r5)
            X.0zD r0 = r14.A0O
            java.util.List r1 = r0.A0D
            java.lang.Double[] r0 = new java.lang.Double[r4]
            java.lang.Object[] r5 = r1.toArray(r0)
            java.lang.Double[] r5 = (java.lang.Double[]) r5
            java.lang.String r1 = "visibility_handler_time_ms"
            r0 = r16
            r0.A04(r1, r5)
            X.0zD r0 = r14.A0O
            int r5 = r0.A02
            java.lang.String r1 = "no_op_count"
            r0 = r16
            r0.A01(r1, r5)
            java.lang.String r1 = "is_dirty"
            r0.A03(r1, r11)
            r1 = r0
            r0 = r23
            r0.BJI(r1)
        L_0x1184:
            if (r17 == 0) goto L_0x1189
            X.C27041cY.A00()
        L_0x1189:
            java.util.concurrent.atomic.AtomicLong r5 = X.C32121lB.A03
            r0 = 1
            r5.addAndGet(r0)
            r14.A0B = r4
            if (r21 == 0) goto L_0x11a6
            X.38h r7 = r8.A05
            X.1r0 r6 = r7.A00
            java.lang.String r5 = r7.A01
            java.lang.String r1 = "_firstmount"
            java.lang.String r0 = "_end"
            r6.A03(r1, r0, r5)
            boolean[] r1 = r7.A04
            r0 = 1
            r1[r4] = r0
        L_0x11a6:
            if (r20 == 0) goto L_0x11ba
            X.38h r7 = r8.A05
            X.1r0 r6 = r7.A00
            java.lang.String r5 = r7.A01
            java.lang.String r1 = "_lastmount"
            java.lang.String r0 = "_end"
            r6.A03(r1, r0, r5)
            boolean[] r1 = r7.A05
            r0 = 1
            r1[r4] = r0
        L_0x11ba:
            if (r22 == 0) goto L_0x11f4
            java.util.List r4 = r2.A0L
            if (r4 == 0) goto L_0x11f4
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x11f4
            X.22J r0 = r3.A0D
            if (r0 != 0) goto L_0x11d1
            X.22J r0 = new X.22J
            r0.<init>()
            r3.A0D = r0
        L_0x11d1:
            X.22J r2 = r3.A0D
            if (r4 == 0) goto L_0x11f4
            r1 = 0
            int r0 = r4.size()
            if (r1 >= r0) goto L_0x11ef
            r4.get(r1)
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Trying to record previous render data for component that doesn't support it"
            r1.<init>(r0)
            throw r1
        L_0x11e7:
            r1 = r16
            r0 = r23
            r0.ARu(r1)
            goto L_0x1184
        L_0x11ef:
            java.util.Set r0 = r2.A00
            r0.clear()
        L_0x11f4:
            r0 = 0
            r3.A0L = r0
            r0 = 0
            r3.A0F = r0
            r3.A0G = r0
            if (r22 == 0) goto L_0x0030
            com.facebook.litho.LithoView r0 = r3.A0C
            X.3ed r4 = r0.A07
            if (r4 == 0) goto L_0x0030
            com.facebook.litho.LithoView r1 = r4.A00
            r0 = 0
            r1.A07 = r0
            r3 = 0
        L_0x120a:
            java.lang.Class[] r2 = r4.A01
            int r0 = r2.length
            if (r3 >= r0) goto L_0x0030
            com.facebook.litho.LithoView r1 = r4.A00
            r0 = r2[r3]
            r1.A0c(r0)
            int r3 = r3 + 1
            goto L_0x120a
        L_0x1219:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Cannot remove disappearing item mappings for root LithoView!"
            r1.<init>(r0)
            throw r1
        L_0x1221:
            r0 = move-exception
            if (r28 == 0) goto L_0x1227
            X.C27041cY.A00()
        L_0x1227:
            throw r0
        L_0x1228:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Should only process transitions on dirty mounts"
            r1.<init>(r0)
            throw r1
        L_0x1230:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Trying to update a MountItem with a null Component!"
            r1.<init>(r0)
            throw r1
        L_0x1238:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Trying to update a MountItem with a null Component."
            r1.<init>(r0)
            throw r1
        L_0x1240:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Trying to mount a null layoutState"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A06(android.graphics.Rect, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0107, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0111, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0114, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        if (r7 == r15.A04) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0044, code lost:
        if (r6 == r15.A00) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004a, code lost:
        if (r0 == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x005e, code lost:
        if (r15.A00 == -1) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x007e, code lost:
        if (r1 == false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0081, code lost:
        if (r0 != false) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00b1, code lost:
        if (r24 == false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00b3, code lost:
        if (r25 == null) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00bc, code lost:
        throw new java.lang.IllegalArgumentException("The layout can't be calculated asynchronously if we need the Size back");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x00bd, code lost:
        r18 = r27;
        r16 = r26;
        r19 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x00c3, code lost:
        if (r24 == false) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x00c5, code lost:
        r3 = r15.A0X;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x00c7, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        r1 = r15.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x00ca, code lost:
        if (r1 == null) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x00cc, code lost:
        r15.A0A.C1H(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x00d1, code lost:
        r15.A06 = new X.AnonymousClass16I(r15, r16, r5, r18, r19);
        r2 = io.card.payment.BuildConfig.FLAVOR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x00e2, code lost:
        if (r15.A0A.BHG() == false) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x00e4, code lost:
        r2 = "calculateLayout ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x00e6, code lost:
        if (r4 == null) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x00e8, code lost:
        r2 = X.AnonymousClass08S.A0J(r2, r4.A1A());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x00f0, code lost:
        r15.A0A.BxL(r15.A06, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x00f7, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x00f8, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x00f9, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x00fc, code lost:
        A0A(r0, r1, r16, r18, r5, r19);
     */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0086 A[Catch:{ all -> 0x0112 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x009d A[Catch:{ all -> 0x0112 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A07(X.C17770zR r21, int r22, int r23, boolean r24, X.AnonymousClass10L r25, int r26, java.lang.String r27, X.AnonymousClass1KE r28, boolean r29) {
        /*
            r20 = this;
            r4 = r21
            r15 = r20
            monitor-enter(r20)
            boolean r0 = r15.A0M     // Catch:{ all -> 0x0112 }
            if (r0 != 0) goto L_0x0110
            X.1JC r0 = r15.A0E     // Catch:{ all -> 0x0112 }
            boolean r0 = r0.A0A()     // Catch:{ all -> 0x0112 }
            if (r0 == 0) goto L_0x001f
            if (r21 == 0) goto L_0x001f
            X.0zR r4 = r4.A16()     // Catch:{ all -> 0x0112 }
            java.util.concurrent.atomic.AtomicInteger r0 = X.C17770zR.A0F     // Catch:{ all -> 0x0112 }
            int r0 = r0.incrementAndGet()     // Catch:{ all -> 0x0112 }
            r4.A00 = r0     // Catch:{ all -> 0x0112 }
        L_0x001f:
            r14 = 1
            r13 = 0
            if (r4 == 0) goto L_0x0024
            r13 = 1
        L_0x0024:
            r12 = 0
            r5 = r28
            if (r28 == 0) goto L_0x002a
            r12 = 1
        L_0x002a:
            r8 = -1
            r11 = 0
            r7 = r22
            if (r7 == r8) goto L_0x0031
            r11 = 1
        L_0x0031:
            r10 = 0
            r6 = r23
            if (r6 == r8) goto L_0x0037
            r10 = 1
        L_0x0037:
            if (r11 == 0) goto L_0x003e
            int r0 = r15.A04     // Catch:{ all -> 0x0112 }
            r2 = 0
            if (r7 != r0) goto L_0x003f
        L_0x003e:
            r2 = 1
        L_0x003f:
            if (r10 == 0) goto L_0x0046
            int r1 = r15.A00     // Catch:{ all -> 0x0112 }
            r0 = 0
            if (r6 != r1) goto L_0x0047
        L_0x0046:
            r0 = 1
        L_0x0047:
            if (r2 == 0) goto L_0x004c
            r2 = 1
            if (r0 != 0) goto L_0x004d
        L_0x004c:
            r2 = 0
        L_0x004d:
            X.15v r3 = r15.A08     // Catch:{ all -> 0x0112 }
            if (r3 != 0) goto L_0x0053
            X.15v r3 = r15.A09     // Catch:{ all -> 0x0112 }
        L_0x0053:
            if (r11 == 0) goto L_0x0060
            if (r10 == 0) goto L_0x0060
            int r0 = r15.A04     // Catch:{ all -> 0x0112 }
            if (r0 == r8) goto L_0x0060
            int r1 = r15.A00     // Catch:{ all -> 0x0112 }
            r0 = 1
            if (r1 != r8) goto L_0x0061
        L_0x0060:
            r0 = 0
        L_0x0061:
            if (r2 != 0) goto L_0x0083
            if (r0 == 0) goto L_0x0089
            if (r3 == 0) goto L_0x0089
            int r9 = r15.A04     // Catch:{ all -> 0x0112 }
            int r8 = r15.A00     // Catch:{ all -> 0x0112 }
            int r0 = r3.A07     // Catch:{ all -> 0x0112 }
            float r2 = (float) r0     // Catch:{ all -> 0x0112 }
            int r0 = r3.A04     // Catch:{ all -> 0x0112 }
            float r1 = (float) r0     // Catch:{ all -> 0x0112 }
            int r0 = (int) r2     // Catch:{ all -> 0x0112 }
            boolean r2 = X.C15050ue.A00(r9, r7, r0)     // Catch:{ all -> 0x0112 }
            int r0 = (int) r1     // Catch:{ all -> 0x0112 }
            boolean r1 = X.C15050ue.A00(r8, r6, r0)     // Catch:{ all -> 0x0112 }
            if (r2 == 0) goto L_0x0080
            r0 = 1
            if (r1 != 0) goto L_0x0081
        L_0x0080:
            r0 = 0
        L_0x0081:
            if (r0 == 0) goto L_0x0089
        L_0x0083:
            r2 = 1
        L_0x0084:
            if (r13 == 0) goto L_0x0095
            if (r3 == 0) goto L_0x0094
            goto L_0x008b
        L_0x0089:
            r2 = 0
            goto L_0x0084
        L_0x008b:
            int r1 = r4.A00     // Catch:{ all -> 0x0112 }
            X.0zR r0 = r3.A0B     // Catch:{ all -> 0x0112 }
            int r0 = r0.A00     // Catch:{ all -> 0x0112 }
            if (r1 != r0) goto L_0x0094
            goto L_0x0095
        L_0x0094:
            r14 = 0
        L_0x0095:
            r1 = r25
            if (r14 == 0) goto L_0x00a0
            if (r2 == 0) goto L_0x00a0
            if (r25 == 0) goto L_0x0110
            if (r3 == 0) goto L_0x00a0
            goto L_0x0108
        L_0x00a0:
            if (r11 == 0) goto L_0x00a4
            r15.A04 = r7     // Catch:{ all -> 0x0112 }
        L_0x00a4:
            if (r10 == 0) goto L_0x00a8
            r15.A00 = r6     // Catch:{ all -> 0x0112 }
        L_0x00a8:
            if (r13 == 0) goto L_0x00ac
            r15.A05 = r4     // Catch:{ all -> 0x0112 }
        L_0x00ac:
            if (r12 == 0) goto L_0x00b0
            r15.A0P = r5     // Catch:{ all -> 0x0112 }
        L_0x00b0:
            monitor-exit(r20)     // Catch:{ all -> 0x0112 }
            if (r24 == 0) goto L_0x00bd
            if (r25 == 0) goto L_0x00bd
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "The layout can't be calculated asynchronously if we need the Size back"
            r1.<init>(r0)
            throw r1
        L_0x00bd:
            r18 = r27
            r16 = r26
            r19 = r29
            if (r24 == 0) goto L_0x00fc
            java.lang.Object r3 = r15.A0X
            monitor-enter(r3)
            X.16I r1 = r15.A06     // Catch:{ all -> 0x00f9 }
            if (r1 == 0) goto L_0x00d1
            X.1js r0 = r15.A0A     // Catch:{ all -> 0x00f9 }
            r0.C1H(r1)     // Catch:{ all -> 0x00f9 }
        L_0x00d1:
            X.16I r14 = new X.16I     // Catch:{ all -> 0x00f9 }
            r17 = r5
            r14.<init>(r15, r16, r17, r18, r19)     // Catch:{ all -> 0x00f9 }
            r15.A06 = r14     // Catch:{ all -> 0x00f9 }
            java.lang.String r2 = ""
            X.1js r0 = r15.A0A     // Catch:{ all -> 0x00f9 }
            boolean r0 = r0.BHG()     // Catch:{ all -> 0x00f9 }
            if (r0 == 0) goto L_0x00f0
            java.lang.String r2 = "calculateLayout "
            if (r4 == 0) goto L_0x00f0
            java.lang.String r0 = r4.A1A()     // Catch:{ all -> 0x00f9 }
            java.lang.String r2 = X.AnonymousClass08S.A0J(r2, r0)     // Catch:{ all -> 0x00f9 }
        L_0x00f0:
            X.1js r1 = r15.A0A     // Catch:{ all -> 0x00f9 }
            X.16I r0 = r15.A06     // Catch:{ all -> 0x00f9 }
            r1.BxL(r0, r2)     // Catch:{ all -> 0x00f9 }
            monitor-exit(r3)     // Catch:{ all -> 0x00f9 }
            return
        L_0x00f9:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00f9 }
            goto L_0x0114
        L_0x00fc:
            r0 = r15
            r2 = r16
            r3 = r18
            r4 = r5
            r5 = r19
            A0A(r0, r1, r2, r3, r4, r5)
            return
        L_0x0108:
            int r0 = r3.A04     // Catch:{ all -> 0x0112 }
            r1.A00 = r0     // Catch:{ all -> 0x0112 }
            int r0 = r3.A07     // Catch:{ all -> 0x0112 }
            r1.A01 = r0     // Catch:{ all -> 0x0112 }
        L_0x0110:
            monitor-exit(r20)     // Catch:{ all -> 0x0112 }
            return
        L_0x0112:
            r0 = move-exception
            monitor-exit(r20)     // Catch:{ all -> 0x0112 }
        L_0x0114:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A07(X.0zR, int, int, boolean, X.10L, int, java.lang.String, X.1KE, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0029, code lost:
        if (r1 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002b, code lost:
        r0 = r5.A0o;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        if (r0 == null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002f, code lost:
        r0.A00.A0U();
        r5.A0o = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0039, code lost:
        if (r5.A0K == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
        r2 = r5.A0C.getMeasuredWidth();
        r1 = r5.A0C.getMeasuredHeight();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0047, code lost:
        if (r2 != 0) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0049, code lost:
        if (r1 != 0) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0054, code lost:
        if ((!A0G(r5.A09, r3, r2, r1)) == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0056, code lost:
        r5.A0C.requestLayout();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005c, code lost:
        A0D(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A08(com.facebook.litho.ComponentTree r5) {
        /*
            boolean r0 = r5.A0K
            if (r0 != 0) goto L_0x0011
            X.19u r0 = r5.A0o
            if (r0 == 0) goto L_0x0010
            X.1Ri r0 = r0.A00
            r0.A0U()
            r0 = 0
            r5.A0o = r0
        L_0x0010:
            return
        L_0x0011:
            monitor-enter(r5)
            X.0zR r0 = r5.A05     // Catch:{ all -> 0x0060 }
            if (r0 != 0) goto L_0x0018
            monitor-exit(r5)     // Catch:{ all -> 0x0060 }
            goto L_0x004c
        L_0x0018:
            X.15v r2 = r5.A09     // Catch:{ all -> 0x0060 }
            r5.A05()     // Catch:{ all -> 0x0060 }
            X.15v r0 = r5.A09     // Catch:{ all -> 0x0060 }
            r4 = 1
            r1 = 0
            if (r0 == r2) goto L_0x0024
            r1 = 1
        L_0x0024:
            X.0zR r0 = r5.A05     // Catch:{ all -> 0x0060 }
            int r3 = r0.A00     // Catch:{ all -> 0x0060 }
            monitor-exit(r5)     // Catch:{ all -> 0x0060 }
            if (r1 == 0) goto L_0x0010
            X.19u r0 = r5.A0o
            if (r0 == 0) goto L_0x0037
            X.1Ri r0 = r0.A00
            r0.A0U()
            r0 = 0
            r5.A0o = r0
        L_0x0037:
            boolean r0 = r5.A0K
            if (r0 == 0) goto L_0x0010
            com.facebook.litho.LithoView r0 = r5.A0C
            int r2 = r0.getMeasuredWidth()
            com.facebook.litho.LithoView r0 = r5.A0C
            int r1 = r0.getMeasuredHeight()
            if (r2 != 0) goto L_0x004d
            if (r1 != 0) goto L_0x004d
            return
        L_0x004c:
            return
        L_0x004d:
            X.15v r0 = r5.A09
            boolean r0 = A0G(r0, r3, r2, r1)
            r0 = r0 ^ r4
            if (r0 == 0) goto L_0x005c
            com.facebook.litho.LithoView r0 = r5.A0C
            r0.requestLayout()
            return
        L_0x005c:
            A0D(r5)
            return
        L_0x0060:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0060 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A08(com.facebook.litho.ComponentTree):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0135, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0136, code lost:
        r2 = io.card.payment.BuildConfig.FLAVOR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x013f, code lost:
        if (r11.A0B.BHG() == false) goto L_0x014b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0141, code lost:
        r2 = "postBackgroundLayoutStateUpdated";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0143, code lost:
        r11.A0B.BxL(r11.A0Z, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x014b, code lost:
        r2 = io.card.payment.BuildConfig.FLAVOR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x014e, code lost:
        if (r2 == null) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0150, code lost:
        r11.A01().A00(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0158, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        if (r11.A00 == -1) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
        if (r11.A0E(r11.A08) != false) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0055, code lost:
        r9 = r11.A03(r11.A0S, r13, r14, r15, r11.A0n, r17, r21, r19, r20);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0061, code lost:
        if (r9 != null) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0065, code lost:
        if (r11.A0M != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0067, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0070, code lost:
        throw new java.lang.IllegalStateException("LayoutState is null, but only async operations can return a null LayoutState");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0071, code lost:
        if (r1 == null) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0073, code lost:
        r1.A01 = r9.A07;
        r1.A00 = r9.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007b, code lost:
        monitor-enter(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r11.A02 = -1;
        r11.A01 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0087, code lost:
        if (r11.A0E(r11.A09) != false) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0089, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0090, code lost:
        if (r11.A0E(r11.A08) == false) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0092, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0093, code lost:
        r10 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0095, code lost:
        if (r0 != false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0097, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00a0, code lost:
        if (A0F(r9, r11.A04, r11.A00) != false) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a2, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00a3, code lost:
        if (r6 == false) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00a5, code lost:
        r1 = r9.A0G;
        r9.A0G = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a9, code lost:
        if (r1 == null) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00ab, code lost:
        r0 = r11.A0E;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ad, code lost:
        if (r0 == null) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00af, code lost:
        r0.A08(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b4, code lost:
        if (r11.A0J == null) goto L_0x00c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00b6, code lost:
        r5 = r9.A07;
        r4 = r9.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00ba, code lost:
        r3 = r9.A0K;
        r9.A0K = null;
        r2 = r9.A0O;
        r9.A0O = null;
        r11.A08 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c5, code lost:
        r5 = 0;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c8, code lost:
        r3 = null;
        r2 = null;
        r10 = false;
        r5 = 0;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00cd, code lost:
        if (r22 != false) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00cf, code lost:
        r11.A03 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00d1, code lost:
        monitor-exit(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00d2, code lost:
        if (r6 == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00d4, code lost:
        monitor-enter(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r0 = r11.A0J;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00d7, code lost:
        if (r0 == null) goto L_0x00de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00d9, code lost:
        r7 = new java.util.ArrayList(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00de, code lost:
        monitor-exit(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00df, code lost:
        if (r7 == null) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00e1, code lost:
        r1 = r7.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00e9, code lost:
        if (r1.hasNext() == false) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00eb, code lost:
        ((X.C35561rQ) r1.next()).BoH(r5, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00f5, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00fa, code lost:
        if (r11.A0q == null) goto L_0x014e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x00fc, code lost:
        r11.A0q.A00(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0101, code lost:
        if (r3 == null) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0103, code lost:
        r11.A0B(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0106, code lost:
        if (r10 == false) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x010c, code lost:
        if (X.C191216w.A00() == false) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x010e, code lost:
        A08(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0111, code lost:
        r1 = r11.A0O;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0113, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0115, code lost:
        r1.C1H(r11.A0l);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0120, code lost:
        if (r11.A0O.BHG() == false) goto L_0x0136;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0122, code lost:
        r2 = "preallocateLayout ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0124, code lost:
        if (r13 == null) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0126, code lost:
        r2 = X.AnonymousClass08S.A0J(r2, r13.A1A());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x012e, code lost:
        r11.A0O.BxL(r11.A0l, r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0A(com.facebook.litho.ComponentTree r17, X.AnonymousClass10L r18, int r19, java.lang.String r20, X.AnonymousClass1KE r21, boolean r22) {
        /*
            r11 = r17
            java.lang.Object r2 = r11.A0X
            monitor-enter(r2)
            X.16I r1 = r11.A06     // Catch:{ all -> 0x016e }
            r7 = 0
            if (r1 == 0) goto L_0x0011
            X.1js r0 = r11.A0A     // Catch:{ all -> 0x016e }
            r0.C1H(r1)     // Catch:{ all -> 0x016e }
            r11.A06 = r7     // Catch:{ all -> 0x016e }
        L_0x0011:
            monitor-exit(r2)     // Catch:{ all -> 0x016e }
            monitor-enter(r11)
            int r0 = r11.A04     // Catch:{ all -> 0x016b }
            r2 = -1
            if (r0 == r2) goto L_0x001d
            int r1 = r11.A00     // Catch:{ all -> 0x016b }
            r0 = 1
            if (r1 != r2) goto L_0x001e
        L_0x001d:
            r0 = 0
        L_0x001e:
            if (r0 == 0) goto L_0x0169
            X.0zR r0 = r11.A05     // Catch:{ all -> 0x016b }
            if (r0 == 0) goto L_0x0169
            X.15v r0 = r11.A09     // Catch:{ all -> 0x016b }
            boolean r0 = r11.A0E(r0)     // Catch:{ all -> 0x016b }
            if (r0 != 0) goto L_0x0035
            X.15v r0 = r11.A08     // Catch:{ all -> 0x016b }
            boolean r1 = r11.A0E(r0)     // Catch:{ all -> 0x016b }
            r0 = 0
            if (r1 == 0) goto L_0x0036
        L_0x0035:
            r0 = 1
        L_0x0036:
            r1 = r18
            if (r0 == 0) goto L_0x003e
            if (r18 == 0) goto L_0x0169
            goto L_0x015b
        L_0x003e:
            int r14 = r11.A04     // Catch:{ all -> 0x016b }
            int r15 = r11.A00     // Catch:{ all -> 0x016b }
            r11.A02 = r14     // Catch:{ all -> 0x016b }
            r11.A01 = r15     // Catch:{ all -> 0x016b }
            X.0zR r0 = r11.A05     // Catch:{ all -> 0x016b }
            X.0zR r13 = r0.A16()     // Catch:{ all -> 0x016b }
            X.15v r0 = r11.A09     // Catch:{ all -> 0x016b }
            r17 = r7
            if (r0 == 0) goto L_0x0054
            r17 = r0
        L_0x0054:
            monitor-exit(r11)     // Catch:{ all -> 0x016b }
            X.0p4 r12 = r11.A0S
            boolean r0 = r11.A0n
            r18 = r21
            r16 = r0
            X.15v r9 = r11.A03(r12, r13, r14, r15, r16, r17, r18, r19, r20)
            if (r9 != 0) goto L_0x0071
            boolean r0 = r11.A0M
            if (r0 != 0) goto L_0x016a
            if (r1 == 0) goto L_0x016a
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "LayoutState is null, but only async operations can return a null LayoutState"
            r1.<init>(r0)
            throw r1
        L_0x0071:
            if (r1 == 0) goto L_0x007b
            int r0 = r9.A07
            r1.A01 = r0
            int r0 = r9.A04
            r1.A00 = r0
        L_0x007b:
            monitor-enter(r11)
            r0 = -1
            r11.A02 = r0     // Catch:{ all -> 0x0158 }
            r11.A01 = r0     // Catch:{ all -> 0x0158 }
            X.15v r0 = r11.A09     // Catch:{ all -> 0x0158 }
            boolean r0 = r11.A0E(r0)     // Catch:{ all -> 0x0158 }
            if (r0 != 0) goto L_0x0092
            X.15v r0 = r11.A08     // Catch:{ all -> 0x0158 }
            boolean r1 = r11.A0E(r0)     // Catch:{ all -> 0x0158 }
            r0 = 0
            if (r1 == 0) goto L_0x0093
        L_0x0092:
            r0 = 1
        L_0x0093:
            r10 = 1
            r8 = 0
            if (r0 != 0) goto L_0x00a2
            int r1 = r11.A04     // Catch:{ all -> 0x0158 }
            int r0 = r11.A00     // Catch:{ all -> 0x0158 }
            boolean r0 = A0F(r9, r1, r0)     // Catch:{ all -> 0x0158 }
            r6 = 1
            if (r0 != 0) goto L_0x00a3
        L_0x00a2:
            r6 = 0
        L_0x00a3:
            if (r6 == 0) goto L_0x00c8
            X.1JC r1 = r9.A0G     // Catch:{ all -> 0x0158 }
            r9.A0G = r7     // Catch:{ all -> 0x0158 }
            if (r1 == 0) goto L_0x00b2
            X.1JC r0 = r11.A0E     // Catch:{ all -> 0x0158 }
            if (r0 == 0) goto L_0x00b2
            r0.A08(r1)     // Catch:{ all -> 0x0158 }
        L_0x00b2:
            java.util.List r0 = r11.A0J     // Catch:{ all -> 0x0158 }
            if (r0 == 0) goto L_0x00c5
            int r5 = r9.A07     // Catch:{ all -> 0x0158 }
            int r4 = r9.A04     // Catch:{ all -> 0x0158 }
        L_0x00ba:
            java.util.List r3 = r9.A0K     // Catch:{ all -> 0x0158 }
            r9.A0K = r7     // Catch:{ all -> 0x0158 }
            java.util.Map r2 = r9.A0O     // Catch:{ all -> 0x0158 }
            r9.A0O = r7     // Catch:{ all -> 0x0158 }
            r11.A08 = r9     // Catch:{ all -> 0x0158 }
            goto L_0x00cd
        L_0x00c5:
            r5 = 0
            r4 = 0
            goto L_0x00ba
        L_0x00c8:
            r3 = r7
            r2 = r7
            r10 = 0
            r5 = 0
            r4 = 0
        L_0x00cd:
            if (r22 != 0) goto L_0x00d1
            r11.A03 = r8     // Catch:{ all -> 0x0158 }
        L_0x00d1:
            monitor-exit(r11)     // Catch:{ all -> 0x0158 }
            if (r6 == 0) goto L_0x0101
            monitor-enter(r11)
            java.util.List r0 = r11.A0J     // Catch:{ all -> 0x00f5 }
            if (r0 == 0) goto L_0x00de
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x00f5 }
            r7.<init>(r0)     // Catch:{ all -> 0x00f5 }
        L_0x00de:
            monitor-exit(r11)     // Catch:{ all -> 0x00f5 }
            if (r7 == 0) goto L_0x00f8
            java.util.Iterator r1 = r7.iterator()
        L_0x00e5:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00f8
            java.lang.Object r0 = r1.next()
            X.1rQ r0 = (X.C35561rQ) r0
            r0.BoH(r5, r4)
            goto L_0x00e5
        L_0x00f5:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x00f5 }
            goto L_0x0170
        L_0x00f8:
            X.2yl r0 = r11.A0q
            if (r0 == 0) goto L_0x014e
            X.2yl r0 = r11.A0q
            r0.A00(r2)
        L_0x0101:
            if (r3 == 0) goto L_0x0106
            r11.A0B(r3)
        L_0x0106:
            if (r10 == 0) goto L_0x0111
            boolean r0 = X.C191216w.A00()
            if (r0 == 0) goto L_0x0139
            A08(r11)
        L_0x0111:
            X.1js r1 = r11.A0O
            if (r1 == 0) goto L_0x016a
            java.lang.Runnable r0 = r11.A0l
            r1.C1H(r0)
            X.1js r0 = r11.A0O
            boolean r0 = r0.BHG()
            if (r0 == 0) goto L_0x0136
            java.lang.String r2 = "preallocateLayout "
            if (r13 == 0) goto L_0x012e
            java.lang.String r0 = r13.A1A()
            java.lang.String r2 = X.AnonymousClass08S.A0J(r2, r0)
        L_0x012e:
            X.1js r1 = r11.A0O
            java.lang.Runnable r0 = r11.A0l
            r1.BxL(r0, r2)
            return
        L_0x0136:
            java.lang.String r2 = ""
            goto L_0x012e
        L_0x0139:
            X.1js r0 = r11.A0B
            boolean r0 = r0.BHG()
            if (r0 == 0) goto L_0x014b
            java.lang.String r2 = "postBackgroundLayoutStateUpdated"
        L_0x0143:
            X.1js r1 = r11.A0B
            java.lang.Runnable r0 = r11.A0Z
            r1.BxL(r0, r2)
            goto L_0x0111
        L_0x014b:
            java.lang.String r2 = ""
            goto L_0x0143
        L_0x014e:
            if (r2 == 0) goto L_0x0101
            X.2yl r0 = r11.A01()
            r0.A00(r2)
            goto L_0x0101
        L_0x0158:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0158 }
            goto L_0x0170
        L_0x015b:
            X.15v r2 = r11.A08     // Catch:{ all -> 0x016b }
            if (r2 != 0) goto L_0x0161
            X.15v r2 = r11.A09     // Catch:{ all -> 0x016b }
        L_0x0161:
            int r0 = r2.A07     // Catch:{ all -> 0x016b }
            r1.A01 = r0     // Catch:{ all -> 0x016b }
            int r0 = r2.A04     // Catch:{ all -> 0x016b }
            r1.A00 = r0     // Catch:{ all -> 0x016b }
        L_0x0169:
            monitor-exit(r11)     // Catch:{ all -> 0x016b }
        L_0x016a:
            return
        L_0x016b:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x016b }
            goto L_0x0170
        L_0x016e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x016e }
        L_0x0170:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0A(com.facebook.litho.ComponentTree, X.10L, int, java.lang.String, X.1KE, boolean):void");
    }

    private void A0B(List list) {
        this.A0V.A00();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C17770zR r3 = (C17770zR) it.next();
            this.A0U.A01(r3.A03, r3, r3.A06);
            synchronized (this.A0V) {
                r3.A1H(this.A0V);
            }
        }
        this.A0U.A00();
    }

    public static boolean A0C(ComponentTree componentTree) {
        if (componentTree.A0E(componentTree.A09) || (!A0F(componentTree.A08, componentTree.A04, componentTree.A00) && A0F(componentTree.A09, componentTree.A04, componentTree.A00))) {
            return true;
        }
        return false;
    }

    public static boolean A0D(ComponentTree componentTree) {
        C17600zA r1 = componentTree.A0C.A0I;
        if (!r1.A09 && !r1.A0C) {
            return false;
        }
        if (componentTree.A0d) {
            componentTree.A0L();
            return true;
        }
        componentTree.A0Q(null, true);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        if (r1 == false) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r5.A0B() == false) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        if (r0 == false) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A0E(X.C188915v r5) {
        /*
            r4 = this;
            X.0zR r0 = r4.A05
            if (r0 == 0) goto L_0x0039
            int r1 = r0.A00
            int r2 = r4.A04
            int r3 = r4.A00
            if (r5 == 0) goto L_0x0035
            X.0zR r0 = r5.A0B
            int r0 = r0.A00
            if (r0 != r1) goto L_0x002b
            int r1 = r5.A08
            int r0 = r5.A07
            boolean r2 = X.C15050ue.A00(r1, r2, r0)
            int r1 = r5.A05
            int r0 = r5.A04
            boolean r0 = X.C15050ue.A00(r1, r3, r0)
            if (r2 == 0) goto L_0x0027
            r1 = 1
            if (r0 != 0) goto L_0x0028
        L_0x0027:
            r1 = 0
        L_0x0028:
            r0 = 1
            if (r1 != 0) goto L_0x002c
        L_0x002b:
            r0 = 0
        L_0x002c:
            if (r0 == 0) goto L_0x0035
            boolean r0 = r5.A0B()
            r1 = 1
            if (r0 != 0) goto L_0x0036
        L_0x0035:
            r1 = 0
        L_0x0036:
            r0 = 1
            if (r1 != 0) goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0E(X.15v):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0015, code lost:
        if (r1 == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0F(X.C188915v r3, int r4, int r5) {
        /*
            if (r3 == 0) goto L_0x0021
            int r1 = r3.A08
            int r0 = r3.A07
            boolean r2 = X.C15050ue.A00(r1, r4, r0)
            int r1 = r3.A05
            int r0 = r3.A04
            boolean r1 = X.C15050ue.A00(r1, r5, r0)
            if (r2 == 0) goto L_0x0017
            r0 = 1
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            if (r0 == 0) goto L_0x0021
            boolean r1 = r3.A0B()
            r0 = 1
            if (r1 != 0) goto L_0x0022
        L_0x0021:
            r0 = 0
        L_0x0022:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0F(X.15v, int, int):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r2.A04 != r5) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean A0G(X.C188915v r2, int r3, int r4, int r5) {
        /*
            if (r2 == 0) goto L_0x001f
            X.0zR r0 = r2.A0B
            int r1 = r0.A00
            r0 = 0
            if (r1 != r3) goto L_0x000a
            r0 = 1
        L_0x000a:
            if (r0 == 0) goto L_0x001f
            int r0 = r2.A07
            if (r0 != r4) goto L_0x0015
            int r1 = r2.A04
            r0 = 1
            if (r1 == r5) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            if (r0 == 0) goto L_0x001f
            boolean r1 = r2.A0B()
            r0 = 1
            if (r1 != 0) goto L_0x0020
        L_0x001f:
            r0 = 0
        L_0x0020:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0G(X.15v, int, int, int):boolean");
    }

    public void A0J() {
        int i;
        LithoView lithoView = this.A0C;
        if (lithoView != null) {
            C21831Iw r4 = this.A0j;
            if (r4 != null && r4.A01.A0d) {
                for (ViewParent parent = lithoView.getParent(); parent != null; parent = parent.getParent()) {
                    if (parent instanceof ViewPager) {
                        ViewPager viewPager = (ViewPager) parent;
                        C72613ef r1 = new C72613ef(r4.A01, viewPager);
                        try {
                            viewPager.A0U(r1);
                        } catch (ConcurrentModificationException unused) {
                            C15320v6.postOnAnimation(viewPager, new C72643ei(viewPager, r1));
                        }
                        r4.A00.add(r1);
                    }
                }
            }
            synchronized (this) {
                this.A0K = true;
                A05();
                C17770zR r0 = this.A05;
                if (r0 != null) {
                    i = r0.A00;
                } else {
                    throw new IllegalStateException("Trying to attach a ComponentTree with a null root. Is released: " + this.A0M + ", Released Component name is: " + this.A0H);
                }
            }
            int measuredWidth = this.A0C.getMeasuredWidth();
            int measuredHeight = this.A0C.getMeasuredHeight();
            if (measuredWidth != 0 || measuredHeight != 0) {
                if (!(true ^ A0G(this.A09, i, measuredWidth, measuredHeight))) {
                    LithoView lithoView2 = this.A0C;
                    if (!lithoView2.A0I.A09) {
                        lithoView2.A0S();
                        return;
                    }
                }
                this.A0C.requestLayout();
                return;
            }
            return;
        }
        throw new IllegalStateException("Trying to attach a ComponentTree without a set View");
    }

    public void A0K() {
        C21831Iw r5 = this.A0j;
        if (r5 != null) {
            int size = r5.A00.size();
            for (int i = 0; i < size; i++) {
                C72613ef r2 = (C72613ef) r5.A00.get(i);
                ViewPager viewPager = (ViewPager) r2.A01.get();
                if (viewPager != null) {
                    C15320v6.postOnAnimation(viewPager, new C110275Ns(r2, viewPager));
                }
            }
            r5.A00.clear();
        }
        synchronized (this) {
            this.A0K = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        if (r4.A0C.getLocalVisibleRect(r3) == false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0L() {
        /*
            r4 = this;
            boolean r0 = r4.A0d
            if (r0 == 0) goto L_0x004f
            com.facebook.litho.LithoView r0 = r4.A0C
            if (r0 == 0) goto L_0x0028
            android.graphics.Rect r3 = new android.graphics.Rect
            r3.<init>()
            boolean r0 = X.AnonymousClass07c.incrementalMountWhenNotVisible
            r2 = 1
            if (r0 == 0) goto L_0x0029
            boolean r0 = r4.A0K
            if (r0 == 0) goto L_0x001f
            com.facebook.litho.LithoView r0 = r4.A0C
            boolean r1 = r0.getLocalVisibleRect(r3)
            r0 = 1
            if (r1 != 0) goto L_0x0020
        L_0x001f:
            r0 = 0
        L_0x0020:
            if (r0 != 0) goto L_0x0025
            r3.setEmpty()
        L_0x0025:
            r4.A0Q(r3, r2)
        L_0x0028:
            return
        L_0x0029:
            com.facebook.litho.LithoView r0 = r4.A0C
            boolean r0 = r0.getLocalVisibleRect(r3)
            if (r0 != 0) goto L_0x0025
            boolean r0 = r4.A0p
            if (r0 != 0) goto L_0x004d
            X.0zL r0 = r4.A0F
            if (r0 == 0) goto L_0x003f
            int r0 = r3.height()
            if (r0 == 0) goto L_0x0049
        L_0x003f:
            X.0zL r0 = r4.A0G
            if (r0 == 0) goto L_0x004d
            int r0 = r3.width()
            if (r0 != 0) goto L_0x004d
        L_0x0049:
            r0 = 1
        L_0x004a:
            if (r0 == 0) goto L_0x0028
            goto L_0x0025
        L_0x004d:
            r0 = 0
            goto L_0x004a
        L_0x004f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Calling incrementalMountComponent() but incremental mount is not enabled"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0L():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00f5, code lost:
        r3 = 0;
        r2 = r4.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00fa, code lost:
        if (r3 >= r2) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x00fc, code lost:
        r1 = (X.C17770zR) r4.get(r3);
        r1.A0a(r1.A03);
        r3 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0M() {
        /*
            r9 = this;
            boolean r0 = r9.A0L
            if (r0 != 0) goto L_0x0120
            monitor-enter(r9)
            X.1js r1 = r9.A0B     // Catch:{ all -> 0x011d }
            java.lang.Runnable r0 = r9.A0Z     // Catch:{ all -> 0x011d }
            r1.C1H(r0)     // Catch:{ all -> 0x011d }
            java.lang.Object r2 = r9.A0X     // Catch:{ all -> 0x011d }
            monitor-enter(r2)     // Catch:{ all -> 0x011d }
            X.16I r1 = r9.A06     // Catch:{ all -> 0x0117 }
            r7 = 0
            if (r1 == 0) goto L_0x001b
            X.1js r0 = r9.A0A     // Catch:{ all -> 0x0117 }
            r0.C1H(r1)     // Catch:{ all -> 0x0117 }
            r9.A06 = r7     // Catch:{ all -> 0x0117 }
        L_0x001b:
            monitor-exit(r2)     // Catch:{ all -> 0x0117 }
            java.lang.Object r2 = r9.A0Y     // Catch:{ all -> 0x011d }
            monitor-enter(r2)     // Catch:{ all -> 0x011d }
            X.16M r1 = r9.A07     // Catch:{ all -> 0x0114 }
            if (r1 == 0) goto L_0x002a
            X.1js r0 = r9.A0A     // Catch:{ all -> 0x0114 }
            r0.C1H(r1)     // Catch:{ all -> 0x0114 }
            r9.A07 = r7     // Catch:{ all -> 0x0114 }
        L_0x002a:
            monitor-exit(r2)     // Catch:{ all -> 0x0114 }
            java.lang.Object r2 = r9.A0k     // Catch:{ all -> 0x011d }
            monitor-enter(r2)     // Catch:{ all -> 0x011d }
            r1 = 0
        L_0x002f:
            java.util.List r0 = r9.A0m     // Catch:{ all -> 0x0111 }
            int r0 = r0.size()     // Catch:{ all -> 0x0111 }
            if (r1 >= r0) goto L_0x0045
            java.util.List r0 = r9.A0m     // Catch:{ all -> 0x0111 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0111 }
            X.1KH r0 = (X.AnonymousClass1KH) r0     // Catch:{ all -> 0x0111 }
            r0.A01()     // Catch:{ all -> 0x0111 }
            int r1 = r1 + 1
            goto L_0x002f
        L_0x0045:
            java.util.List r0 = r9.A0m     // Catch:{ all -> 0x0111 }
            r0.clear()     // Catch:{ all -> 0x0111 }
            monitor-exit(r2)     // Catch:{ all -> 0x0111 }
            X.1js r1 = r9.A0O     // Catch:{ all -> 0x011d }
            if (r1 == 0) goto L_0x0054
            java.lang.Runnable r0 = r9.A0l     // Catch:{ all -> 0x011d }
            r1.C1H(r0)     // Catch:{ all -> 0x011d }
        L_0x0054:
            r0 = 1
            r9.A0M = r0     // Catch:{ all -> 0x011d }
            X.0zR r0 = r9.A05     // Catch:{ all -> 0x011d }
            java.lang.String r0 = r0.A1A()     // Catch:{ all -> 0x011d }
            r9.A0H = r0     // Catch:{ all -> 0x011d }
            com.facebook.litho.LithoView r0 = r9.A0C     // Catch:{ all -> 0x011d }
            if (r0 == 0) goto L_0x0066
            r0.A0b(r7)     // Catch:{ all -> 0x011d }
        L_0x0066:
            r9.A05 = r7     // Catch:{ all -> 0x011d }
            r8 = r9
            monitor-enter(r8)     // Catch:{ all -> 0x011d }
            boolean r0 = A0C(r9)     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x0073
            X.15v r0 = r9.A09     // Catch:{ all -> 0x011a }
            goto L_0x0075
        L_0x0073:
            X.15v r0 = r9.A08     // Catch:{ all -> 0x011a }
        L_0x0075:
            if (r0 == 0) goto L_0x00bd
            X.1Iv r6 = r9.A0W     // Catch:{ all -> 0x011a }
            X.38Y r5 = r0.A0I     // Catch:{ all -> 0x011a }
            if (r5 == 0) goto L_0x00bd
            java.util.Map r0 = r5.A00     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x00bd
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x011a }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x011a }
        L_0x0089:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x00bd
            java.lang.Object r1 = r4.next()     // Catch:{ all -> 0x011a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x011a }
            java.util.Map r0 = r5.A00     // Catch:{ all -> 0x011a }
            java.lang.Object r3 = r0.get(r1)     // Catch:{ all -> 0x011a }
            X.38b r3 = (X.AnonymousClass38b) r3     // Catch:{ all -> 0x011a }
            java.util.List r0 = r3.A02     // Catch:{ all -> 0x011a }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x011a }
        L_0x00a3:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x0089
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x011a }
            X.0zR r1 = (X.C17770zR) r1     // Catch:{ all -> 0x011a }
            java.lang.String r0 = r3.A01     // Catch:{ all -> 0x011a }
            boolean r0 = r6.A00(r0, r1)     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x00a3
            java.lang.String r0 = r3.A01     // Catch:{ all -> 0x011a }
            r1.A0q(r0)     // Catch:{ all -> 0x011a }
            goto L_0x00a3
        L_0x00bd:
            X.1Iv r0 = r9.A0W     // Catch:{ all -> 0x011a }
            java.util.Map r0 = r0.A00     // Catch:{ all -> 0x011a }
            r0.clear()     // Catch:{ all -> 0x011a }
            monitor-exit(r8)     // Catch:{ all -> 0x011d }
            r9.A09 = r7     // Catch:{ all -> 0x011d }
            r9.A08 = r7     // Catch:{ all -> 0x011d }
            r9.A0E = r7     // Catch:{ all -> 0x011d }
            r9.A0D = r7     // Catch:{ all -> 0x011d }
            r9.A0J = r7     // Catch:{ all -> 0x011d }
            monitor-exit(r9)     // Catch:{ all -> 0x011d }
            X.1RE r1 = r9.A0V
            monitor-enter(r1)
            X.1RE r0 = r9.A0V     // Catch:{ all -> 0x010e }
            r0.A00()     // Catch:{ all -> 0x010e }
            monitor-exit(r1)     // Catch:{ all -> 0x010e }
            X.2yl r0 = r9.A0q
            if (r0 == 0) goto L_0x010d
            X.2yl r1 = r9.A0q
            monitor-enter(r1)
            java.util.Map r0 = r1.A00     // Catch:{ all -> 0x010a }
            if (r0 != 0) goto L_0x00e6
            monitor-exit(r1)     // Catch:{ all -> 0x010a }
            return
        L_0x00e6:
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x010a }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x010a }
            r4.<init>(r0)     // Catch:{ all -> 0x010a }
            java.util.Map r0 = r1.A00     // Catch:{ all -> 0x010a }
            r0.clear()     // Catch:{ all -> 0x010a }
            monitor-exit(r1)     // Catch:{ all -> 0x010a }
            r3 = 0
            int r2 = r4.size()
        L_0x00fa:
            if (r3 >= r2) goto L_0x010d
            java.lang.Object r1 = r4.get(r3)
            X.0zR r1 = (X.C17770zR) r1
            X.0p4 r0 = r1.A03
            r1.A0a(r0)
            int r3 = r3 + 1
            goto L_0x00fa
        L_0x010a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x010a }
            goto L_0x011f
        L_0x010d:
            return
        L_0x010e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x010e }
            goto L_0x011f
        L_0x0111:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0111 }
            goto L_0x011c
        L_0x0114:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0114 }
            goto L_0x011c
        L_0x0117:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0117 }
            goto L_0x011c
        L_0x011a:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x011d }
        L_0x011c:
            throw r0     // Catch:{ all -> 0x011d }
        L_0x011d:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x011d }
        L_0x011f:
            throw r0
        L_0x0120:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Releasing a ComponentTree that is currently being mounted"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0M():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x006d, code lost:
        if (r3 != false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0070, code lost:
        if (r5 != false) goto L_0x007e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x008a A[Catch:{ all -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x008c A[Catch:{ all -> 0x012e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0P(int r20, int r21, int[] r22, boolean r23) {
        /*
            r19 = this;
            r0 = r19
            monitor-enter(r0)
            r1 = 1
            r0.A0Q = r1     // Catch:{ all -> 0x012e }
            r7 = r20
            r0.A04 = r7     // Catch:{ all -> 0x012e }
            r8 = r21
            r0.A00 = r8     // Catch:{ all -> 0x012e }
            r0.A05()     // Catch:{ all -> 0x012e }
            X.15v r4 = r0.A09     // Catch:{ all -> 0x012e }
            int r3 = r0.A04     // Catch:{ all -> 0x012e }
            int r2 = r0.A00     // Catch:{ all -> 0x012e }
            boolean r3 = A0F(r4, r3, r2)     // Catch:{ all -> 0x012e }
            r2 = 0
            if (r3 == 0) goto L_0x0072
            X.15v r4 = r0.A09     // Catch:{ all -> 0x012e }
            X.0zR r3 = r0.A05     // Catch:{ all -> 0x012e }
            int r5 = r3.A00     // Catch:{ all -> 0x012e }
            X.0zR r3 = r4.A0B     // Catch:{ all -> 0x012e }
            int r4 = r3.A00     // Catch:{ all -> 0x012e }
            r3 = 0
            if (r4 != r5) goto L_0x002c
            r3 = 1
        L_0x002c:
            if (r3 != 0) goto L_0x007e
            java.lang.Object r4 = r0.A0X     // Catch:{ all -> 0x012e }
            monitor-enter(r4)     // Catch:{ all -> 0x012e }
            X.16I r3 = r0.A06     // Catch:{ all -> 0x0069 }
            monitor-exit(r4)
            if (r3 != 0) goto L_0x006f
            int r6 = r0.A02     // Catch:{ all -> 0x012e }
            r5 = 0
            r4 = -1
            if (r6 == r4) goto L_0x0070
            int r3 = r0.A01     // Catch:{ all -> 0x012e }
            if (r3 == r4) goto L_0x0070
            int r3 = r0.A04     // Catch:{ all -> 0x012e }
            if (r3 == r6) goto L_0x0050
            int r3 = android.view.View.MeasureSpec.getMode(r3)     // Catch:{ all -> 0x012e }
            if (r3 != 0) goto L_0x0052
            int r3 = android.view.View.MeasureSpec.getMode(r6)     // Catch:{ all -> 0x012e }
            if (r3 != 0) goto L_0x0052
        L_0x0050:
            r3 = 1
            goto L_0x0053
        L_0x0052:
            r3 = 0
        L_0x0053:
            if (r3 == 0) goto L_0x0070
            int r3 = r0.A00     // Catch:{ all -> 0x012e }
            int r4 = r0.A01     // Catch:{ all -> 0x012e }
            if (r3 == r4) goto L_0x0067
            int r3 = android.view.View.MeasureSpec.getMode(r3)     // Catch:{ all -> 0x012e }
            if (r3 != 0) goto L_0x006c
            int r3 = android.view.View.MeasureSpec.getMode(r4)     // Catch:{ all -> 0x012e }
            if (r3 != 0) goto L_0x006c
        L_0x0067:
            r3 = 1
            goto L_0x006d
        L_0x0069:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0069 }
            throw r1     // Catch:{ all -> 0x012e }
        L_0x006c:
            r3 = 0
        L_0x006d:
            if (r3 == 0) goto L_0x0070
        L_0x006f:
            r5 = 1
        L_0x0070:
            if (r5 != 0) goto L_0x007e
        L_0x0072:
            r4 = 1
        L_0x0073:
            r3 = 0
            r14 = 0
            if (r3 != 0) goto L_0x0080
            if (r23 != 0) goto L_0x0080
            if (r4 != 0) goto L_0x0080
            r6 = r14
            r11 = r14
            goto L_0x0090
        L_0x007e:
            r4 = 0
            goto L_0x0073
        L_0x0080:
            X.0zR r3 = r0.A05     // Catch:{ all -> 0x012e }
            X.0zR r6 = r3.A16()     // Catch:{ all -> 0x012e }
            X.1KE r3 = r0.A0P     // Catch:{ all -> 0x012e }
            if (r3 != 0) goto L_0x008c
            r11 = r14
            goto L_0x0090
        L_0x008c:
            X.1KE r11 = X.AnonymousClass1KE.A00(r3)     // Catch:{ all -> 0x012e }
        L_0x0090:
            monitor-exit(r0)     // Catch:{ all -> 0x012e }
            if (r6 == 0) goto L_0x00ef
            X.15v r3 = r0.A09
            if (r3 == 0) goto L_0x00a0
            monitor-enter(r0)
            r0.A09 = r14     // Catch:{ all -> 0x009c }
            monitor-exit(r0)     // Catch:{ all -> 0x009c }
            goto L_0x00a0
        L_0x009c:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x009c }
            goto L_0x0130
        L_0x00a0:
            X.0p4 r5 = r0.A0S
            boolean r9 = r0.A0n
            r10 = 0
            r12 = 6
            r13 = 0
            r4 = r0
            X.15v r6 = r4.A03(r5, r6, r7, r8, r9, r10, r11, r12, r13)
            if (r6 == 0) goto L_0x00c9
            monitor-enter(r0)
            X.1JC r4 = r6.A0G     // Catch:{ all -> 0x00c6 }
            r6.A0G = r14     // Catch:{ all -> 0x00c6 }
            java.util.Map r5 = r6.A0O     // Catch:{ all -> 0x00c6 }
            r6.A0O = r14     // Catch:{ all -> 0x00c6 }
            if (r4 == 0) goto L_0x00be
            X.1JC r3 = r0.A0E     // Catch:{ all -> 0x00c6 }
            r3.A08(r4)     // Catch:{ all -> 0x00c6 }
        L_0x00be:
            java.util.List r4 = r6.A0K     // Catch:{ all -> 0x00c6 }
            r6.A0K = r14     // Catch:{ all -> 0x00c6 }
            r0.A09 = r6     // Catch:{ all -> 0x00c6 }
            monitor-exit(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x00d1
        L_0x00c6:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x0130
        L_0x00c9:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "LayoutState cannot be null for measure, this means a LayoutStateFuture was released incorrectly."
            r1.<init>(r0)
            throw r1
        L_0x00d1:
            if (r5 == 0) goto L_0x00da
            X.2yl r3 = r0.A01()
            r3.A00(r5)
        L_0x00da:
            if (r4 == 0) goto L_0x00df
            r0.A0B(r4)
        L_0x00df:
            com.facebook.litho.LithoView r3 = r0.A0C
            r3.A0U()
            X.19u r3 = r0.A0o
            if (r3 == 0) goto L_0x00ef
            X.1Ri r3 = r3.A00
            r3.A0U()
            r0.A0o = r14
        L_0x00ef:
            X.15v r4 = r0.A09
            int r3 = r4.A07
            r22[r2] = r3
            int r3 = r4.A04
            r22[r1] = r3
            monitor-enter(r0)
            r0.A0Q = r2     // Catch:{ all -> 0x012b }
            int r3 = r0.A0N     // Catch:{ all -> 0x012b }
            if (r3 == 0) goto L_0x010f
            r0.A0N = r2     // Catch:{ all -> 0x012b }
            X.0zR r2 = r0.A05     // Catch:{ all -> 0x012b }
            X.0zR r10 = r2.A16()     // Catch:{ all -> 0x012b }
            X.1KE r2 = r0.A0P     // Catch:{ all -> 0x012b }
            if (r2 != 0) goto L_0x0114
            r17 = r14
            goto L_0x0118
        L_0x010f:
            r10 = r14
            r17 = r14
            r3 = 0
            goto L_0x0118
        L_0x0114:
            X.1KE r17 = X.AnonymousClass1KE.A00(r2)     // Catch:{ all -> 0x012b }
        L_0x0118:
            monitor-exit(r0)     // Catch:{ all -> 0x012b }
            if (r3 == 0) goto L_0x012a
            r13 = 0
            if (r3 != r1) goto L_0x011f
            r13 = 1
        L_0x011f:
            r18 = 0
            r9 = r0
            r11 = -1
            r12 = -1
            r15 = 6
            r16 = r14
            r9.A07(r10, r11, r12, r13, r14, r15, r16, r17, r18)
        L_0x012a:
            return
        L_0x012b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x012b }
            goto L_0x0130
        L_0x012e:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x012e }
        L_0x0130:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A0P(int, int, int[], boolean):void");
    }

    public void A0Q(Rect rect, boolean z) {
        String str;
        if (this.A0L) {
            C88554Kv r2 = new C88554Kv(rect, z);
            Deque deque = this.A0I;
            if (deque == null) {
                this.A0I = new ArrayDeque();
            } else if (deque.size() > 25) {
                StringBuilder sb = new StringBuilder("Reentrant mounts exceed max attempts, view=");
                LithoView lithoView = this.A0C;
                if (lithoView != null) {
                    str = LithoViewTestHelper.A00(lithoView);
                } else {
                    str = null;
                }
                sb.append(str);
                sb.append(", component=");
                Object obj = this.A05;
                if (obj == null) {
                    obj = A0I();
                }
                sb.append(obj);
                C09070gU.A01(AnonymousClass07B.A0C, "ComponentTree:ReentrantMountsExceedMaxAttempts", sb.toString());
                this.A0I.clear();
                return;
            }
            this.A0I.add(r2);
            return;
        }
        A06(rect, z);
        Deque deque2 = this.A0I;
        if (deque2 != null) {
            ArrayDeque arrayDeque = new ArrayDeque(deque2);
            this.A0I.clear();
            while (!arrayDeque.isEmpty()) {
                C88554Kv r22 = (C88554Kv) arrayDeque.pollFirst();
                this.A0C.A0U();
                A06(r22.A00, r22.A01);
            }
        }
    }

    public void A0V(C35561rQ r2) {
        if (r2 != null) {
            synchronized (this) {
                if (this.A0J == null) {
                    this.A0J = new ArrayList();
                }
                this.A0J.add(r2);
            }
        }
    }

    public LithoView getLithoView() {
        return this.A0C;
    }

    public ComponentTree(AnonymousClass11J r6) {
        Looper looper;
        AnonymousClass0p4 r2 = new AnonymousClass0p4(r6.A0F, new AnonymousClass1JC(null), null, null);
        r2.A05 = this;
        r2.A04 = null;
        this.A0S = r2;
        this.A05 = r6.A00;
        this.A0d = r6.A09;
        this.A0n = r6.A0B;
        this.A0A = r6.A03;
        this.A0h = r6.A0D;
        this.A0O = r6.A04;
        this.A0f = true;
        this.A0p = r6.A08;
        A0V(r6.A01);
        this.A0i = r6.A0E;
        this.A0g = r6.A07;
        this.A0b = r6.A0C;
        if (this.A0O == null && 0 != 0) {
            synchronized (ComponentTree.class) {
                if (A0u == null) {
                    HandlerThread handlerThread = new HandlerThread("PreallocateMountContentThread");
                    handlerThread.start();
                    A0u = handlerThread.getLooper();
                }
                looper = A0u;
            }
            this.A0O = new AnonymousClass11N(looper);
        }
        AnonymousClass1JC r0 = r6.A05;
        this.A0E = r0 == null ? new AnonymousClass1JC(null) : r0;
        this.A0R = A0s.getAndIncrement();
        this.A0j = new C21831Iw(this);
        this.A0B = C21851Iy.A00(this.A0B);
        this.A0A = A04(this.A0A);
        C31551js r02 = this.A0O;
        if (r02 != null) {
            this.A0O = C21851Iy.A00(r02);
        }
        this.A0T = r6.A02;
        this.A0a = r6.A06;
        this.A0c = C07070ca.A03(this.A0S.A09);
        this.A0e = r6.A0A;
    }

    private void A05() {
        if (A0C(this)) {
            this.A08 = null;
            return;
        }
        LithoView lithoView = this.A0C;
        if (lithoView != null) {
            lithoView.A0U();
        }
        this.A09 = this.A08;
        this.A08 = null;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: X.15v} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: X.3eb} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: X.15v} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: X.3eb} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: X.15v} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: X.3eb} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: X.15v} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: X.3eb} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: X.3eb} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v7, resolved type: X.15v} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: X.3eb} */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0306, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0307, code lost:
        r5 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:?, code lost:
        X.C27041cY.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x0317, code lost:
        throw ((java.lang.RuntimeException) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0321, code lost:
        throw new java.lang.RuntimeException(r2.getMessage(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x0322, code lost:
        r2 = th;
        r5 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x0325, code lost:
        X.C27041cY.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:0x032a, code lost:
        r5.A03("wait_for_result", r6);
        r5.A03("is_main_thread", X.C191216w.A00());
        r4.BJI(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0078, code lost:
        if (r3 == false) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x008e, code lost:
        if (r2 == 6) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0091, code lost:
        if (r0 == false) goto L_0x0093;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:90:0x0167 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x029b  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x02b3  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x0306 A[ExcHandler: InterruptedException | CancellationException | ExecutionException (e java.lang.Throwable), Splitter:B:81:0x014e] */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x030a A[SYNTHETIC, Splitter:B:204:0x030a] */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x0315 A[Catch:{ all -> 0x0322 }] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x0318 A[Catch:{ all -> 0x0322 }] */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x0325  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x032a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.C188915v A03(X.AnonymousClass0p4 r27, X.C17770zR r28, int r29, int r30, boolean r31, X.C188915v r32, X.AnonymousClass1KE r33, int r34, java.lang.String r35) {
        /*
            r26 = this;
            X.1KH r7 = new X.1KH
            r8 = r26
            r13 = r31
            r12 = r30
            r11 = r29
            r10 = r28
            r2 = r34
            r9 = r27
            r17 = r35
            r15 = r33
            r14 = r32
            r16 = r2
            r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            java.lang.Object r5 = r8.A0k
            monitor-enter(r5)
            r4 = 0
            r3 = 0
        L_0x0020:
            java.util.List r0 = r8.A0m     // Catch:{ all -> 0x033c }
            int r0 = r0.size()     // Catch:{ all -> 0x033c }
            if (r3 >= r0) goto L_0x0040
            java.util.List r0 = r8.A0m     // Catch:{ all -> 0x033c }
            java.lang.Object r1 = r0.get(r3)     // Catch:{ all -> 0x033c }
            X.1KH r1 = (X.AnonymousClass1KH) r1     // Catch:{ all -> 0x033c }
            boolean r0 = r1.A0H     // Catch:{ all -> 0x033c }
            if (r0 != 0) goto L_0x003b
            boolean r0 = r1.equals(r7)     // Catch:{ all -> 0x033c }
            if (r0 == 0) goto L_0x003b
            goto L_0x003e
        L_0x003b:
            int r3 = r3 + 1
            goto L_0x0020
        L_0x003e:
            r4 = 1
            r7 = r1
        L_0x0040:
            if (r4 != 0) goto L_0x0047
            java.util.List r0 = r8.A0m     // Catch:{ all -> 0x033c }
            r0.add(r7)     // Catch:{ all -> 0x033c }
        L_0x0047:
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A07     // Catch:{ all -> 0x033c }
            r0.incrementAndGet()     // Catch:{ all -> 0x033c }
            monitor-exit(r5)     // Catch:{ all -> 0x033c }
            java.util.concurrent.atomic.AtomicInteger r3 = r7.A08
            int r1 = android.os.Process.myTid()
            r0 = -1
            boolean r0 = r3.compareAndSet(r0, r1)
            if (r0 == 0) goto L_0x005f
            java.util.concurrent.FutureTask r0 = r7.A06
            r0.run()
        L_0x005f:
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A08
            int r10 = r0.get()
            int r0 = android.os.Process.myTid()
            r20 = 1
            r3 = 0
            if (r10 == r0) goto L_0x006f
            r3 = 1
        L_0x006f:
            java.util.concurrent.FutureTask r0 = r7.A06
            boolean r0 = r0.isDone()
            if (r0 != 0) goto L_0x007a
            r6 = 1
            if (r3 != 0) goto L_0x007b
        L_0x007a:
            r6 = 0
        L_0x007b:
            r5 = 0
            if (r6 == 0) goto L_0x0098
            boolean r0 = X.C191216w.A00()
            if (r0 != 0) goto L_0x0098
            if (r34 == 0) goto L_0x0090
            r0 = 2
            if (r2 == r0) goto L_0x0090
            r0 = 4
            if (r2 == r0) goto L_0x0090
            r1 = 6
            r0 = 0
            if (r2 != r1) goto L_0x0091
        L_0x0090:
            r0 = 1
        L_0x0091:
            if (r0 != 0) goto L_0x0098
        L_0x0093:
            java.lang.Object r2 = r8.A0k
            monitor-enter(r2)
            goto L_0x02bd
        L_0x0098:
            boolean r0 = X.C191216w.A00()
            if (r0 == 0) goto L_0x00dc
            if (r6 == 0) goto L_0x00dc
            com.facebook.litho.ComponentTree r0 = r7.A0I
            boolean r0 = r0.A0g
            if (r0 == 0) goto L_0x00c7
            boolean r0 = r7.A0A
            if (r0 != 0) goto L_0x00c7
            r0 = 1
            r7.A0G = r0
            java.lang.String r1 = "interruptCalculateLayout"
            X.1dB r2 = X.C27461dE.A00
            if (r2 == 0) goto L_0x00da
            boolean r0 = X.C25231Yv.A01()
            if (r0 == 0) goto L_0x00da
            java.lang.String r0 = r2.A01
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r1)
            int r0 = r2.A00
            X.1XV r0 = X.C27401d8.A01(r1, r0)
        L_0x00c5:
            r7.A0F = r0
        L_0x00c7:
            r1 = -4
            int r9 = android.os.Process.getThreadPriority(r10)
            r0 = 0
        L_0x00cd:
            if (r0 != 0) goto L_0x00df
            if (r1 >= r9) goto L_0x00df
            android.os.Process.setThreadPriority(r10, r1)     // Catch:{ SecurityException -> 0x00d5 }
            goto L_0x00d8
        L_0x00d5:
            int r1 = r1 + 1
            goto L_0x00cd
        L_0x00d8:
            r0 = 1
            goto L_0x00cd
        L_0x00da:
            r0 = 0
            goto L_0x00c5
        L_0x00dc:
            r9 = 0
            r12 = 0
            goto L_0x00e0
        L_0x00df:
            r12 = 1
        L_0x00e0:
            com.facebook.litho.ComponentTree r0 = r7.A0I
            X.38i r4 = r0.A0T
            if (r4 != 0) goto L_0x00ec
            X.0p4 r0 = r0.A0S
            X.38i r4 = r0.A05()
        L_0x00ec:
            if (r3 == 0) goto L_0x00f7
            boolean r0 = X.C27041cY.A02()
            if (r0 == 0) goto L_0x00f7
        L_0x00f4:
            if (r20 == 0) goto L_0x013c
            goto L_0x00fa
        L_0x00f7:
            r20 = 0
            goto L_0x00f4
        L_0x00fa:
            java.lang.String r1 = "LayoutStateFuture.get"
            X.0gP r0 = X.C27041cY.A00     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            X.0gQ r1 = r0.APo(r1)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            java.lang.String r11 = "treeId"
            com.facebook.litho.ComponentTree r0 = r7.A0I     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            int r0 = r0.A0R     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r1.AOn(r11, r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            java.lang.String r3 = "root"
            X.0zR r0 = r7.A03     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            java.lang.String r0 = r0.A1A()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r1.AOo(r3, r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            java.lang.String r2 = "runningThreadId"
            r1.AOn(r2, r10)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r1.flush()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            java.lang.String r1 = "LayoutStateFuture.wait"
            X.0gP r0 = X.C27041cY.A00     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            X.0gQ r1 = r0.APo(r1)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            com.facebook.litho.ComponentTree r0 = r7.A0I     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            int r0 = r0.A0R     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r1.AOn(r11, r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            X.0zR r0 = r7.A03     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            java.lang.String r0 = r0.A1A()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r1.AOo(r3, r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r1.AOn(r2, r10)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r1.flush()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
        L_0x013c:
            if (r4 == 0) goto L_0x014d
            com.facebook.litho.ComponentTree r0 = r7.A0I     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            X.0p4 r1 = r0.A0S     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            r0 = 21
            X.3eb r0 = r4.BLg(r1, r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            X.3eb r3 = X.C637138j.A00(r1, r4, r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x02e2 }
            goto L_0x014e
        L_0x014d:
            r3 = r5
        L_0x014e:
            java.util.concurrent.FutureTask r0 = r7.A06     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            java.lang.Object r2 = r0.get()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            X.15v r2 = (X.C188915v) r2     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            if (r20 == 0) goto L_0x015b
            X.C27041cY.A00()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
        L_0x015b:
            if (r3 == 0) goto L_0x0162
            java.lang.String r0 = "FUTURE_TASK_END"
            r3.A00(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
        L_0x0162:
            if (r12 == 0) goto L_0x0167
            android.os.Process.setThreadPriority(r10, r9)     // Catch:{ IllegalArgumentException | SecurityException -> 0x0167, InterruptedException | CancellationException | ExecutionException -> 0x0306, InterruptedException | CancellationException | ExecutionException -> 0x0306 }
        L_0x0167:
            boolean r0 = r7.A0G     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            if (r0 == 0) goto L_0x0294
            boolean r0 = r2.A0o     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            if (r0 == 0) goto L_0x0294
            boolean r0 = X.C191216w.A00()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            if (r0 == 0) goto L_0x0264
            java.lang.String r1 = "continuePartialLayoutState"
            java.lang.Object r0 = r7.A0E     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            java.lang.Object r19 = X.C27461dE.A01(r1, r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            r7.A0E = r5     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            boolean r0 = r7.A0H     // Catch:{ all -> 0x02fb }
            r18 = 0
            if (r0 != 0) goto L_0x0260
            X.0p4 r17 = X.AnonymousClass1KH.A00(r7)     // Catch:{ all -> 0x02fb }
            int r0 = r7.A01     // Catch:{ all -> 0x02fb }
            r22 = r0
            java.lang.String r9 = r7.A05     // Catch:{ all -> 0x02fb }
            boolean r0 = r2.A0o     // Catch:{ all -> 0x02fb }
            if (r0 == 0) goto L_0x02f3
            X.1kq r1 = new X.1kq     // Catch:{ all -> 0x02fb }
            r10 = 0
            r1.<init>(r2, r5)     // Catch:{ all -> 0x02fb }
            r0 = r17
            r0.A06 = r1     // Catch:{ all -> 0x02fb }
            X.0zR r0 = r2.A0B     // Catch:{ all -> 0x02fb }
            r21 = r0
            int r12 = r2.A00     // Catch:{ all -> 0x02fb }
            int r0 = r2.A08     // Catch:{ all -> 0x02fb }
            r23 = r0
            int r0 = r2.A05     // Catch:{ all -> 0x02fb }
            r24 = r0
            X.38i r11 = r17.A05()     // Catch:{ all -> 0x02fb }
            boolean r16 = X.C27041cY.A02()     // Catch:{ all -> 0x02fb }
            if (r16 == 0) goto L_0x01fb
            if (r9 == 0) goto L_0x01c0
            java.lang.String r13 = "extra:"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r13, r9)     // Catch:{ all -> 0x02fb }
            X.C27041cY.A01(r0)     // Catch:{ all -> 0x02fb }
        L_0x01c0:
            java.lang.String r15 = "LayoutState.resumeCalculate_"
            java.lang.String r14 = r21.A1A()     // Catch:{ all -> 0x02fb }
            java.lang.String r13 = "_"
            r0 = r22
            java.lang.String r0 = X.C188915v.A02(r0)     // Catch:{ all -> 0x02fb }
            java.lang.String r13 = X.AnonymousClass08S.A0S(r15, r14, r13, r0)     // Catch:{ all -> 0x02fb }
            X.0gP r0 = X.C27041cY.A00     // Catch:{ all -> 0x02fb }
            X.0gQ r13 = r0.APo(r13)     // Catch:{ all -> 0x02fb }
            java.lang.String r0 = "treeId"
            r13.AOn(r0, r12)     // Catch:{ all -> 0x02fb }
            r0 = r21
            int r12 = r0.A00     // Catch:{ all -> 0x02fb }
            java.lang.String r0 = "rootId"
            r13.AOn(r0, r12)     // Catch:{ all -> 0x02fb }
            java.lang.String r12 = android.view.View.MeasureSpec.toString(r23)     // Catch:{ all -> 0x02fb }
            java.lang.String r0 = "widthSpec"
            r13.AOo(r0, r12)     // Catch:{ all -> 0x02fb }
            java.lang.String r12 = android.view.View.MeasureSpec.toString(r24)     // Catch:{ all -> 0x02fb }
            java.lang.String r0 = "heightSpec"
            r13.AOo(r0, r12)     // Catch:{ all -> 0x02fb }
            r13.flush()     // Catch:{ all -> 0x02fb }
        L_0x01fb:
            if (r11 == 0) goto L_0x0209
            r10 = 19
            r0 = r17
            X.3eb r10 = r11.BLg(r0, r10)     // Catch:{ all -> 0x02e7 }
            X.3eb r10 = X.C637138j.A00(r0, r11, r10)     // Catch:{ all -> 0x02e7 }
        L_0x0209:
            if (r10 == 0) goto L_0x021d
            java.lang.String r12 = "component"
            java.lang.String r0 = r21.A1A()     // Catch:{ all -> 0x02e7 }
            r10.A02(r12, r0)     // Catch:{ all -> 0x02e7 }
            java.lang.String r12 = "calculate_layout_state_source"
            java.lang.String r0 = X.C188915v.A02(r22)     // Catch:{ all -> 0x02e7 }
            r10.A02(r12, r0)     // Catch:{ all -> 0x02e7 }
        L_0x021d:
            X.0yx r13 = r2.A0D     // Catch:{ all -> 0x02e7 }
            X.1Lz r12 = r2.A0C     // Catch:{ all -> 0x02e7 }
            r14 = r17
            X.0yx r0 = X.AnonymousClass0p4.A0F     // Catch:{ all -> 0x02e7 }
            if (r13 == r0) goto L_0x0241
            X.C31951kr.A05(r13)     // Catch:{ all -> 0x02e7 }
            if (r10 == 0) goto L_0x0231
            java.lang.String r0 = "start_measure"
            r10.A00(r0)     // Catch:{ all -> 0x02e7 }
        L_0x0231:
            r21 = r14
            r22 = r13
            r25 = r12
            X.C31951kr.A04(r21, r22, r23, r24, r25)     // Catch:{ all -> 0x02e7 }
            if (r10 == 0) goto L_0x0241
            java.lang.String r0 = "end_measure"
            r10.A00(r0)     // Catch:{ all -> 0x02e7 }
        L_0x0241:
            X.C188915v.A04(r14, r2)     // Catch:{ all -> 0x02e7 }
            r1.A01 = r5     // Catch:{ all -> 0x02e7 }
            r1.A00 = r5     // Catch:{ all -> 0x02e7 }
            if (r10 == 0) goto L_0x024d
            r11.BJI(r10)     // Catch:{ all -> 0x02e7 }
        L_0x024d:
            if (r16 == 0) goto L_0x0257
            X.C27041cY.A00()     // Catch:{ all -> 0x02fb }
            if (r9 == 0) goto L_0x0257
            X.C27041cY.A00()     // Catch:{ all -> 0x02fb }
        L_0x0257:
            monitor-enter(r7)     // Catch:{ all -> 0x02fb }
            boolean r0 = r7.A0H     // Catch:{ all -> 0x02e4 }
            if (r0 == 0) goto L_0x025d
            r2 = r5
        L_0x025d:
            monitor-exit(r7)     // Catch:{ all -> 0x02e4 }
            r18 = r2
        L_0x0260:
            X.C27461dE.A02(r19)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            goto L_0x0299
        L_0x0264:
            java.lang.String r2 = "offerPartialLayoutState"
            java.lang.Object r10 = r7.A0F     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            X.1dB r1 = X.C27461dE.A00     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            if (r1 == 0) goto L_0x026f
            if (r10 == 0) goto L_0x026f
            goto L_0x0271
        L_0x026f:
            r0 = 0
            goto L_0x028f
        L_0x0271:
            boolean r0 = X.C25231Yv.A01()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            if (r0 == 0) goto L_0x026f
            boolean r0 = r10 instanceof X.AnonymousClass1XV     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            if (r0 == 0) goto L_0x026f
            X.1XV r10 = (X.AnonymousClass1XV) r10     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            java.lang.String r0 = r1.A01     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            java.lang.String r9 = X.AnonymousClass08S.A0J(r0, r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            int r2 = r1.A00     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            X.1d9 r1 = X.C27401d8.A00     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            r0 = 1
            X.1XV r0 = r1.A02(r10, r9, r0, r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            r0.close()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
        L_0x028f:
            r7.A0E = r0     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            r7.A0F = r5     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            goto L_0x0297
        L_0x0294:
            r18 = r2
            goto L_0x0299
        L_0x0297:
            r18 = r5
        L_0x0299:
            if (r20 == 0) goto L_0x029e
            X.C27041cY.A00()
        L_0x029e:
            if (r3 == 0) goto L_0x02b1
            java.lang.String r0 = "wait_for_result"
            r3.A03(r0, r6)
            boolean r1 = X.C191216w.A00()
            java.lang.String r0 = "is_main_thread"
            r3.A03(r0, r1)
            r4.BJI(r3)
        L_0x02b1:
            if (r18 == 0) goto L_0x0093
            monitor-enter(r7)
            boolean r0 = r7.A0H     // Catch:{ all -> 0x0300 }
            monitor-exit(r7)
            if (r0 != 0) goto L_0x0093
            r5 = r18
            goto L_0x0093
        L_0x02bd:
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A07     // Catch:{ all -> 0x02df }
            int r0 = r0.decrementAndGet()     // Catch:{ all -> 0x02df }
            if (r0 < 0) goto L_0x02d7
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A07     // Catch:{ all -> 0x02df }
            int r0 = r0.get()     // Catch:{ all -> 0x02df }
            if (r0 != 0) goto L_0x02d5
            r7.A01()     // Catch:{ all -> 0x02df }
            java.util.List r0 = r8.A0m     // Catch:{ all -> 0x02df }
            r0.remove(r7)     // Catch:{ all -> 0x02df }
        L_0x02d5:
            monitor-exit(r2)     // Catch:{ all -> 0x02df }
            return r5
        L_0x02d7:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x02df }
            java.lang.String r0 = "LayoutStateFuture ref count is below 0"
            r1.<init>(r0)     // Catch:{ all -> 0x02df }
            throw r1     // Catch:{ all -> 0x02df }
        L_0x02df:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x02df }
            goto L_0x033e
        L_0x02e2:
            r2 = move-exception
            goto L_0x0308
        L_0x02e4:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x02e4 }
            throw r0     // Catch:{ all -> 0x02fb }
        L_0x02e7:
            r1 = move-exception
            if (r16 == 0) goto L_0x02fa
            X.C27041cY.A00()     // Catch:{ all -> 0x02fb }
            if (r9 == 0) goto L_0x02fa
            X.C27041cY.A00()     // Catch:{ all -> 0x02fb }
            goto L_0x02fa
        L_0x02f3:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x02fb }
            java.lang.String r0 = "Can not resume a finished LayoutState calculation"
            r1.<init>(r0)     // Catch:{ all -> 0x02fb }
        L_0x02fa:
            throw r1     // Catch:{ all -> 0x02fb }
        L_0x02fb:
            r0 = move-exception
            X.C27461dE.A02(r19)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
            throw r0     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0306, all -> 0x0303 }
        L_0x0300:
            r2 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0300 }
            goto L_0x033b
        L_0x0303:
            r2 = move-exception
            r5 = r3
            goto L_0x0323
        L_0x0306:
            r2 = move-exception
            r5 = r3
        L_0x0308:
            if (r20 == 0) goto L_0x030d
            X.C27041cY.A00()     // Catch:{ all -> 0x0322 }
        L_0x030d:
            java.lang.Throwable r1 = r2.getCause()     // Catch:{ all -> 0x0322 }
            boolean r0 = r1 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0322 }
            if (r0 == 0) goto L_0x0318
            java.lang.RuntimeException r1 = (java.lang.RuntimeException) r1     // Catch:{ all -> 0x0322 }
            throw r1     // Catch:{ all -> 0x0322 }
        L_0x0318:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0322 }
            java.lang.String r0 = r2.getMessage()     // Catch:{ all -> 0x0322 }
            r1.<init>(r0, r2)     // Catch:{ all -> 0x0322 }
            throw r1     // Catch:{ all -> 0x0322 }
        L_0x0322:
            r2 = move-exception
        L_0x0323:
            if (r20 == 0) goto L_0x0328
            X.C27041cY.A00()
        L_0x0328:
            if (r5 == 0) goto L_0x033b
            java.lang.String r0 = "wait_for_result"
            r5.A03(r0, r6)
            boolean r1 = X.C191216w.A00()
            java.lang.String r0 = "is_main_thread"
            r5.A03(r0, r1)
            r4.BJI(r5)
        L_0x033b:
            throw r2
        L_0x033c:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x033c }
        L_0x033e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentTree.A03(X.0p4, X.0zR, int, int, boolean, X.15v, X.1KE, int, java.lang.String):X.15v");
    }
}
