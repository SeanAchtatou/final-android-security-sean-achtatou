package com.facebook.litho.annotations;

public @interface Comparable {
    int type();
}
