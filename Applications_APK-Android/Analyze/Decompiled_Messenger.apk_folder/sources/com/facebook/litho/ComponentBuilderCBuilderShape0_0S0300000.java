package com.facebook.litho;

import X.AnonymousClass0p4;
import X.AnonymousClass10H;
import X.AnonymousClass10K;
import X.AnonymousClass10M;
import X.AnonymousClass11F;
import X.AnonymousClass1GN;
import X.AnonymousClass1I8;
import X.AnonymousClass1JL;
import X.AnonymousClass1JY;
import X.AnonymousClass1L2;
import X.AnonymousClass1NO;
import X.AnonymousClass1Y3;
import X.AnonymousClass5FC;
import X.C14910uL;
import X.C14920uM;
import X.C16930y3;
import X.C17030yD;
import X.C17540z4;
import X.C17770zR;
import X.C20131Ba;
import X.C21641Id;
import X.C21911Je;
import X.C22331Kx;
import X.C32071l6;
import X.C34891qL;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.TextUtils;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.proxygen.TraceFieldType;
import java.util.BitSet;
import org.webrtc.audio.WebRtcAudioRecord;

public class ComponentBuilderCBuilderShape0_0S0300000 extends AnonymousClass11F {
    public Object A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public ComponentBuilderCBuilderShape0_0S0300000(int i) {
        int i2;
        String str;
        String str2;
        int i3;
        String str3;
        int i4;
        String str4;
        this.A03 = i;
        switch (i) {
            case 0:
                i3 = 1;
                str3 = "text";
                this.A01 = new String[]{str3};
                this.A00 = new BitSet(i3);
                return;
            case 1:
                i3 = 1;
                str3 = "drawable";
                this.A01 = new String[]{str3};
                this.A00 = new BitSet(i3);
                return;
            case 2:
                i4 = 1;
                str4 = "color";
                this.A01 = new String[]{str4};
                this.A02 = new BitSet(i4);
                return;
            case 3:
                i4 = 1;
                str4 = "text";
                this.A01 = new String[]{str4};
                this.A02 = new BitSet(i4);
                return;
            case 4:
                i4 = 1;
                str4 = "clickListener";
                this.A01 = new String[]{str4};
                this.A02 = new BitSet(i4);
                return;
            case 5:
                this.A01 = new String[]{"id", "normalColor", "pressedColor"};
                this.A00 = new BitSet(3);
                return;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                i2 = 2;
                str = "iconName";
                str2 = "iconSize";
                this.A01 = new String[]{str, str2};
                this.A00 = new BitSet(i2);
                return;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                i3 = 1;
                str3 = "userKey";
                this.A01 = new String[]{str3};
                this.A00 = new BitSet(i3);
                return;
            case 8:
                i2 = 2;
                str = "profileConfig";
                str2 = TraceFieldType.Uri;
                this.A01 = new String[]{str, str2};
                this.A00 = new BitSet(i2);
                return;
            case Process.SIGKILL:
                this.A01 = new String[]{"text", "textColor", "textSize", "typeface"};
                this.A00 = new BitSet(4);
                return;
            case AnonymousClass1Y3.A01:
                i2 = 2;
                str = "text";
                str2 = "textStyle";
                this.A01 = new String[]{str, str2};
                this.A00 = new BitSet(i2);
                return;
            default:
                i4 = 1;
                str4 = "params";
                this.A01 = new String[]{str4};
                this.A02 = new BitSet(i4);
                return;
        }
    }

    public /* bridge */ /* synthetic */ AnonymousClass11F A1h(float f) {
        if (2 - this.A03 != 0) {
            return super.A1h(f);
        }
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = this;
        ((C21641Id) componentBuilderCBuilderShape0_0S0300000.A00).A00 = f;
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public /* bridge */ /* synthetic */ AnonymousClass11F A1i(boolean z) {
        if (5 - this.A03 != 0) {
            return super.A1i(z);
        }
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = this;
        ((AnonymousClass1I8) componentBuilderCBuilderShape0_0S0300000.A02).A06 = z;
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public AnonymousClass11F A2x() {
        switch (this.A03) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 8:
            case Process.SIGKILL:
            case AnonymousClass1Y3.A01:
            case AnonymousClass1Y3.A02:
                return this;
            default:
                return super.A2x();
        }
    }

    public /* bridge */ /* synthetic */ AnonymousClass11F A2z(CharSequence charSequence) {
        if (5 - this.A03 != 0) {
            return super.A2z(charSequence);
        }
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = this;
        ((AnonymousClass1I8) componentBuilderCBuilderShape0_0S0300000.A02).A04 = charSequence;
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public /* bridge */ /* synthetic */ C17770zR A31() {
        switch (this.A03) {
            case 0:
                AnonymousClass11F.A0C(1, (BitSet) this.A00, (String[]) this.A01);
                return (C22331Kx) this.A02;
            case 1:
                return A33();
            case 2:
                return A34();
            case 3:
                return A35();
            case 4:
                return A36();
            case 5:
                AnonymousClass11F.A0C(3, (BitSet) this.A00, (String[]) this.A01);
                return (AnonymousClass1I8) this.A02;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return A37();
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                AnonymousClass11F.A0C(1, (BitSet) this.A00, (String[]) this.A01);
                return (AnonymousClass5FC) this.A02;
            case 8:
                AnonymousClass11F.A0C(2, (BitSet) this.A00, (String[]) this.A01);
                return (C32071l6) this.A02;
            case Process.SIGKILL:
                return A38();
            case AnonymousClass1Y3.A01:
                return A39();
            case AnonymousClass1Y3.A02:
                return A3A();
            default:
                return super.A31();
        }
    }

    public void A32(C17770zR r2) {
        switch (this.A03) {
            case 0:
                this.A02 = (C22331Kx) r2;
                return;
            case 1:
                this.A02 = (AnonymousClass10H) r2;
                return;
            case 2:
                this.A00 = (C21641Id) r2;
                return;
            case 3:
                this.A00 = (C14910uL) r2;
                return;
            case 4:
                this.A00 = (AnonymousClass1GN) r2;
                return;
            case 5:
                this.A02 = (AnonymousClass1I8) r2;
                return;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                this.A02 = (AnonymousClass1NO) r2;
                return;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                this.A02 = (AnonymousClass5FC) r2;
                return;
            case 8:
                this.A02 = (C32071l6) r2;
                return;
            case Process.SIGKILL:
                this.A02 = (AnonymousClass1L2) r2;
                return;
            case AnonymousClass1Y3.A01:
                this.A02 = (C17030yD) r2;
                return;
            case AnonymousClass1Y3.A02:
                this.A00 = (C21911Je) r2;
                return;
            default:
                super.A32(r2);
                return;
        }
    }

    public AnonymousClass10H A33() {
        AnonymousClass11F.A0C(1, (BitSet) this.A00, (String[]) this.A01);
        return (AnonymousClass10H) this.A02;
    }

    public C21641Id A34() {
        AnonymousClass11F.A0C(1, (BitSet) this.A02, (String[]) this.A01);
        return (C21641Id) this.A00;
    }

    public C14910uL A35() {
        AnonymousClass11F.A0C(1, (BitSet) this.A02, (String[]) this.A01);
        return (C14910uL) this.A00;
    }

    public AnonymousClass1GN A36() {
        AnonymousClass11F.A0C(1, (BitSet) this.A02, (String[]) this.A01);
        return (AnonymousClass1GN) this.A00;
    }

    public AnonymousClass1NO A37() {
        AnonymousClass11F.A0C(2, (BitSet) this.A00, (String[]) this.A01);
        return (AnonymousClass1NO) this.A02;
    }

    public AnonymousClass1L2 A38() {
        AnonymousClass11F.A0C(4, (BitSet) this.A00, (String[]) this.A01);
        return (AnonymousClass1L2) this.A02;
    }

    public C17030yD A39() {
        AnonymousClass11F.A0C(2, (BitSet) this.A00, (String[]) this.A01);
        return (C17030yD) this.A02;
    }

    public C21911Je A3A() {
        AnonymousClass11F.A0C(1, (BitSet) this.A02, (String[]) this.A01);
        return (C21911Je) this.A00;
    }

    public void A3B(float f) {
        ((C14910uL) this.A00).A02 = (float) this.A02.A00(f);
    }

    public void A3C(float f) {
        ((C14910uL) this.A00).A02 = (float) this.A02.A01(f);
    }

    public void A3D(float f) {
        ((C14910uL) this.A00).A0N = this.A02.A00(f);
    }

    public void A3E(float f) {
        ((C14910uL) this.A00).A0N = this.A02.A01(f);
    }

    public void A3F(int i) {
        ((C21641Id) this.A00).A01 = i;
        ((BitSet) this.A02).set(0);
    }

    public void A3G(int i) {
        ((C21641Id) this.A00).A01 = this.A02.A02(i);
        ((BitSet) this.A02).set(0);
    }

    public void A3H(int i) {
        ((AnonymousClass10H) this.A02).A00 = this.A02.A07(i);
        ((BitSet) this.A00).set(0);
    }

    public void A3I(int i) {
        ((C14910uL) this.A00).A0F = i;
    }

    public void A3J(int i) {
        ((C17030yD) this.A02).A01 = i;
    }

    public void A3K(int i) {
        ((C14910uL) this.A00).A0M = i;
    }

    public void A3L(int i) {
        ((C14910uL) this.A00).A0M = this.A02.A04(i, 0);
    }

    public void A3M(int i) {
        ((C14910uL) this.A00).A0M = this.A02.A02(i);
    }

    public void A3N(int i) {
        ((C14910uL) this.A00).A0c = this.A02.A09(i);
        ((BitSet) this.A02).set(0);
    }

    public void A3O(int i) {
        ((AnonymousClass1L2) this.A02).A0C = this.A02.A09(i);
        ((BitSet) this.A00).set(0);
    }

    public void A3P(int i) {
        ((C17030yD) this.A02).A07 = this.A02.A09(i);
        ((BitSet) this.A00).set(0);
    }

    public void A3Q(int i) {
        ((C14910uL) this.A00).A0N = this.A02.A03(i);
    }

    public void A3R(int i) {
        ((C14910uL) this.A00).A0O = i;
    }

    public void A3S(int i, Object... objArr) {
        String str;
        C17030yD r1 = (C17030yD) this.A02;
        C17540z4 r0 = this.A02;
        if (i != 0) {
            str = r0.A00.getString(i, objArr);
        } else {
            str = null;
        }
        r1.A07 = str;
        ((BitSet) this.A00).set(0);
    }

    public void A3T(Typeface typeface) {
        ((C14910uL) this.A00).A0Q = typeface;
    }

    public void A3U(Drawable drawable) {
        ((AnonymousClass10H) this.A02).A00 = drawable;
        ((BitSet) this.A00).set(0);
    }

    public void A3V(Layout.Alignment alignment) {
        ((C17030yD) this.A02).A02 = alignment;
    }

    public void A3W(Layout.Alignment alignment) {
        ((C14910uL) this.A00).A0R = alignment;
    }

    public void A3X(TextUtils.TruncateAt truncateAt) {
        ((C14910uL) this.A00).A0U = truncateAt;
    }

    public void A3Y(TextUtils.TruncateAt truncateAt) {
        ((C17030yD) this.A02).A03 = truncateAt;
    }

    public void A3Z(C34891qL r3) {
        ((AnonymousClass1NO) this.A02).A00 = r3;
        ((BitSet) this.A00).set(0);
    }

    public void A3a(C14920uM r2) {
        ((C14910uL) this.A00).A0Y = r2;
    }

    public void A3b(C20131Ba r3) {
        ((AnonymousClass1GN) this.A00).A02 = r3;
        ((BitSet) this.A02).set(0);
    }

    public void A3c(MigColorScheme migColorScheme) {
        ((C17030yD) this.A02).A05 = migColorScheme;
    }

    public void A3d(C16930y3 r3) {
        ((AnonymousClass1L2) this.A02).A09 = r3;
        ((BitSet) this.A00).set(1);
    }

    public void A3e(AnonymousClass1JL r3) {
        ((AnonymousClass1L2) this.A02).A0A = r3;
        ((BitSet) this.A00).set(2);
    }

    public void A3f(AnonymousClass10K r3) {
        ((C17030yD) this.A02).A06 = r3;
        ((BitSet) this.A00).set(1);
    }

    public void A3g(AnonymousClass10M r3) {
        ((AnonymousClass1L2) this.A02).A0B = r3;
        ((BitSet) this.A00).set(3);
    }

    public void A3h(AnonymousClass1JY r3) {
        ((C21911Je) this.A00).A0B = r3;
        ((BitSet) this.A02).set(0);
    }

    public void A3i(CharSequence charSequence) {
        ((C22331Kx) this.A02).A0S = charSequence;
        ((BitSet) this.A00).set(0);
    }

    public void A3j(CharSequence charSequence) {
        ((C14910uL) this.A00).A0c = charSequence;
        ((BitSet) this.A02).set(0);
    }

    public void A3k(CharSequence charSequence) {
        ((AnonymousClass1L2) this.A02).A0C = charSequence;
        ((BitSet) this.A00).set(0);
    }

    public void A3l(CharSequence charSequence) {
        ((C17030yD) this.A02).A07 = charSequence;
        ((BitSet) this.A00).set(0);
    }

    public void A3m(Integer num) {
        ((AnonymousClass1NO) this.A02).A03 = num;
        ((BitSet) this.A00).set(1);
    }

    public void A3n(boolean z) {
        ((C14910uL) this.A00).A0h = z;
    }

    public void A3o(boolean z) {
        ((C14910uL) this.A00).A0j = z;
    }

    public static void A00(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, C22331Kx r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A01(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass10H r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A02(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, C21641Id r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A00 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A02).clear();
    }

    public static void A03(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, C14910uL r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A00 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A02).clear();
    }

    public static void A04(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass1GN r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A00 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A02).clear();
    }

    public static void A05(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass1I8 r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A06(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass1NO r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A07(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass5FC r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A08(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, C32071l6 r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A09(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, AnonymousClass1L2 r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A0A(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, C17030yD r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A02 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A00).clear();
    }

    public static void A0B(ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000, AnonymousClass0p4 r1, int i, int i2, C21911Je r4) {
        super.A2L(r1, i, i2, r4);
        componentBuilderCBuilderShape0_0S0300000.A00 = r4;
        ((BitSet) componentBuilderCBuilderShape0_0S0300000.A02).clear();
    }
}
