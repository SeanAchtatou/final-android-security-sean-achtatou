package com.facebook.litho;

import X.AnonymousClass0p4;
import X.AnonymousClass10N;
import X.AnonymousClass1MS;
import X.AnonymousClass2Rb;
import X.C000700l;
import X.C07870eJ;
import X.C15070ug;
import X.C15320v6;
import X.C17550z5;
import X.C17560z6;
import X.C17650zF;
import X.C17750zP;
import X.C17830zY;
import X.C17880zd;
import X.C17890ze;
import X.C17970zp;
import X.C31401jd;
import X.C31411je;
import X.C33181nA;
import X.C46662Ra;
import X.C49352c4;
import X.C51302gG;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComponentHost extends ViewGroup {
    public SparseArray A00;
    public C07870eJ A01;
    public C07870eJ A02;
    public C07870eJ A03;
    public C07870eJ A04;
    public C07870eJ A05;
    public C07870eJ A06;
    public C17880zd A07;
    public C31411je A08;
    public C17890ze A09;
    public C17970zp A0A;
    public AnonymousClass10N A0B;
    public C49352c4 A0C;
    public Object A0D;
    public ArrayList A0E;
    public boolean A0F = false;
    public boolean A0G = false;
    public boolean A0H;
    public boolean A0I;
    public int[] A0J = new int[0];
    private AnonymousClass2Rb A0K;
    private CharSequence A0L;
    private boolean A0M = true;
    private boolean A0N = false;
    private boolean A0O;
    private boolean A0P;
    private boolean A0Q;
    private boolean A0R;
    private final C17550z5 A0S = new C17550z5(this);

    public C17830zY A0D() {
        for (int i = 0; i < A0C(); i++) {
            C17830zY r1 = (C17830zY) this.A02.A05(i);
            if (r1 != null && r1.A02()) {
                return r1;
            }
        }
        return null;
    }

    public void A0P(boolean z, int i, int i2, int i3, int i4) {
        LithoView lithoView;
        ComponentTree componentTree;
        if ((this instanceof LithoView) && (componentTree = (lithoView = (LithoView) this).A03) != null) {
            if (!componentTree.A0X()) {
                if (lithoView.A0B || lithoView.A03.A09 == null) {
                    lithoView.A03.A0P(View.MeasureSpec.makeMeasureSpec(Math.max(0, ((i3 - i) - lithoView.getPaddingRight()) - lithoView.getPaddingLeft()), 1073741824), View.MeasureSpec.makeMeasureSpec(Math.max(0, ((i4 - i2) - lithoView.getPaddingTop()) - lithoView.getPaddingBottom()), 1073741824), LithoView.A0L, false);
                    lithoView.A0D = false;
                    lithoView.A0B = false;
                }
                boolean A0D2 = ComponentTree.A0D(lithoView.A03);
                if (!A0D2 && lithoView.A0f()) {
                    lithoView.A0R();
                }
                if (!A0D2) {
                    LithoView.A05(lithoView);
                    return;
                }
                return;
            }
            throw new IllegalStateException("Trying to layout a LithoView holding onto a released ComponentTree");
        }
    }

    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.A0H = true;
        A0P(z, i, i2, i3, i4);
        this.A0H = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r5 != null) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean requestFocus(int r4, android.graphics.Rect r5) {
        /*
            r3 = this;
            r2 = 1
            r1 = 0
            r0 = 130(0x82, float:1.82E-43)
            if (r4 != r0) goto L_0x0009
            r0 = 1
            if (r5 == 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x0013
            boolean r0 = r3.A0O
            if (r0 == 0) goto L_0x0013
            r3.A0R = r2
            return r1
        L_0x0013:
            boolean r0 = super.requestFocus(r4, r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentHost.requestFocus(int, android.graphics.Rect):boolean");
    }

    public void requestLayout() {
        ViewParent viewParent = this;
        while (viewParent instanceof ComponentHost) {
            if (((ComponentHost) viewParent).A0Q()) {
                viewParent = viewParent.getParent();
            } else {
                return;
            }
        }
        super.requestLayout();
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public boolean verifyDrawable(Drawable drawable) {
        return true;
    }

    public ComponentHost(AnonymousClass0p4 r3, AttributeSet attributeSet) {
        super(r3.A09, attributeSet);
        setWillNotDraw(false);
        setChildrenDrawingOrderEnabled(true);
        A0N(C17560z6.A01(r3.A09));
        this.A02 = new C07870eJ();
        this.A06 = new C07870eJ();
        this.A01 = new C07870eJ();
        this.A0E = new ArrayList();
    }

    private List A07() {
        int i;
        CharSequence charSequence;
        ArrayList arrayList = new ArrayList();
        C07870eJ r0 = this.A01;
        if (r0 == null) {
            i = 0;
        } else {
            i = r0.A01();
        }
        for (int i2 = 0; i2 < i; i2++) {
            C31401jd r02 = ((C17830zY) this.A01.A05(i2)).A06;
            if (!(r02 == null || (charSequence = r02.A0S) == null)) {
                arrayList.add(charSequence);
            }
        }
        CharSequence contentDescription = getContentDescription();
        if (contentDescription != null) {
            arrayList.add(contentDescription);
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0007, code lost:
        if (r5.A04(r2) == null) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A08(int r2, int r3, X.C07870eJ r4, X.C07870eJ r5) {
        /*
            if (r5 == 0) goto L_0x0009
            java.lang.Object r1 = r5.A04(r2)
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x0017
            java.lang.Object r0 = r5.A04(r2)
            r5.A07(r2)
        L_0x0013:
            r4.A09(r3, r0)
            return
        L_0x0017:
            java.lang.Object r0 = r4.A04(r2)
            r4.A07(r2)
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentHost.A08(int, int, X.0eJ, X.0eJ):void");
    }

    public static void A09(int i, C07870eJ r2, C07870eJ r3) {
        Object A042;
        if (r2 != null && r3 != null && (A042 = r2.A04(i)) != null) {
            r3.A09(i, A042);
        }
    }

    public static void A0A(ComponentHost componentHost) {
        if (componentHost.A02 == null) {
            componentHost.A02 = new C07870eJ();
        }
    }

    public static void A0B(ComponentHost componentHost) {
        C07870eJ r0 = componentHost.A04;
        if (r0 != null && r0.A01() == 0) {
            componentHost.A04 = null;
        }
        C07870eJ r02 = componentHost.A05;
        if (r02 != null && r02.A01() == 0) {
            componentHost.A05 = null;
        }
    }

    public int A0C() {
        C07870eJ r0 = this.A02;
        if (r0 == null) {
            return 0;
        }
        return r0.A01();
    }

    public List A0E() {
        int A012;
        C07870eJ r0 = this.A01;
        if (r0 == null) {
            A012 = 0;
        } else {
            A012 = r0.A01();
        }
        ArrayList arrayList = null;
        for (int i = 0; i < A012; i++) {
            C17830zY r1 = (C17830zY) this.A01.A05(i);
            if ((r1.A01 & 4) != 0) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add((Drawable) r1.A00());
            }
        }
        return arrayList;
    }

    public void A0F() {
        if (!this.A0F) {
            if (Build.VERSION.SDK_INT >= 18) {
                this.A0G = getClipChildren();
            } else {
                this.A0G = this.A0M;
            }
            setClipChildren(false);
            this.A0F = true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r1 == false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G() {
        /*
            r3 = this;
            boolean r0 = r3.A0N
            if (r0 == 0) goto L_0x0027
            boolean r0 = r3.A0O
            if (r0 == 0) goto L_0x000c
            r0 = 1
            r3.A0P = r0
            return
        L_0x000c:
            X.2Rb r2 = r3.A0K
            if (r2 == 0) goto L_0x0027
            X.0zY r0 = r3.A0D()
            if (r0 == 0) goto L_0x001f
            X.0zR r0 = r0.A04
            boolean r1 = r0.A0y()
            r0 = 1
            if (r1 != 0) goto L_0x0020
        L_0x001f:
            r0 = 0
        L_0x0020:
            if (r0 == 0) goto L_0x0027
            r1 = -1
            r0 = 1
            X.C51282gE.A09(r2, r1, r0)
        L_0x0027:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentHost.A0G():void");
    }

    public void A0H(int i, C17830zY r7) {
        Rect rect;
        C17650zF r1 = r7.A08;
        if (r1 != null) {
            Rect rect2 = r1.A02;
            if (rect2 == null || rect2.isEmpty()) {
                rect = null;
            } else {
                rect = r1.A02;
            }
            if (rect != null && !equals(r7.A00())) {
                if (this.A0C == null) {
                    C49352c4 r0 = new C49352c4(this);
                    this.A0C = r0;
                    setTouchDelegate(r0);
                }
                C49352c4 r02 = this.A0C;
                View view = (View) r7.A00();
                C07870eJ r3 = r02.A01;
                C46662Ra r2 = (C46662Ra) C46662Ra.A05.ALa();
                if (r2 == null) {
                    r2 = new C46662Ra();
                }
                r2.A01 = view;
                r2.A00 = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
                r2.A03.set(rect);
                r2.A04.set(rect);
                Rect rect3 = r2.A04;
                int i2 = -r2.A00;
                rect3.inset(i2, i2);
                r3.A09(i, r2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0I(int r8, X.C17830zY r9) {
        /*
            r7 = this;
            X.0zF r1 = r9.A08
            if (r1 == 0) goto L_0x0077
            X.2c4 r0 = r7.A0C
            if (r0 == 0) goto L_0x0077
            android.graphics.Rect r0 = r1.A02
            if (r0 == 0) goto L_0x007a
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x007a
            android.graphics.Rect r0 = r1.A02
        L_0x0014:
            if (r0 == 0) goto L_0x0077
            java.lang.Object r0 = r9.A00()
            boolean r0 = r7.equals(r0)
            if (r0 != 0) goto L_0x0077
            X.2c4 r6 = r7.A0C
            X.0eJ r5 = r6.A00
            if (r5 == 0) goto L_0x0078
            boolean r0 = r5.A01
            if (r0 == 0) goto L_0x002d
            X.C07870eJ.A00(r5)
        L_0x002d:
            int[] r1 = r5.A02
            int r0 = r5.A00
            int r4 = X.AnonymousClass04d.A02(r1, r0, r8)
            if (r4 < 0) goto L_0x0078
            java.lang.Object r3 = r5.A05(r4)
            X.2Ra r3 = (X.C46662Ra) r3
            java.lang.Object[] r2 = r5.A03
            r1 = r2[r4]
            java.lang.Object r0 = X.C07870eJ.A04
            if (r1 == r0) goto L_0x004a
            r2[r4] = r0
            r0 = 1
            r5.A01 = r0
        L_0x004a:
            r3.A00()
            r0 = 1
        L_0x004e:
            if (r0 != 0) goto L_0x0077
            X.0eJ r5 = r6.A01
            boolean r0 = r5.A01
            if (r0 == 0) goto L_0x0059
            X.C07870eJ.A00(r5)
        L_0x0059:
            int[] r1 = r5.A02
            int r0 = r5.A00
            int r4 = X.AnonymousClass04d.A02(r1, r0, r8)
            java.lang.Object r3 = r5.A05(r4)
            X.2Ra r3 = (X.C46662Ra) r3
            java.lang.Object[] r2 = r5.A03
            r1 = r2[r4]
            java.lang.Object r0 = X.C07870eJ.A04
            if (r1 == r0) goto L_0x0074
            r2[r4] = r0
            r0 = 1
            r5.A01 = r0
        L_0x0074:
            r3.A00()
        L_0x0077:
            return
        L_0x0078:
            r0 = 0
            goto L_0x004e
        L_0x007a:
            r0 = 0
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentHost.A0I(int, X.0zY):void");
    }

    public void A0L(C17830zY r5) {
        if (this.A0E == null) {
            this.A0E = new ArrayList();
        }
        if (this.A0E.remove(r5)) {
            Object A002 = r5.A00();
            if (A002 instanceof Drawable) {
                Drawable drawable = (Drawable) r5.A00();
                drawable.setCallback(null);
                invalidate(drawable.getBounds());
                A0B(this);
            } else if (A002 instanceof View) {
                View view = (View) A002;
                this.A0I = true;
                if (this.A0H) {
                    super.removeViewInLayout(view);
                } else {
                    super.removeView(view);
                }
            }
            if (r5.A02()) {
                r5.A05.A0G();
                return;
            }
            return;
        }
        C17750zP r3 = r5.A07;
        throw new RuntimeException("Tried to remove non-existent disappearing item, transitionId: " + r3);
    }

    public void A0M(C17830zY r6, int i, int i2) {
        Rect rect;
        C49352c4 r2;
        C07870eJ r0;
        if (r6 == null && (r0 = this.A04) != null) {
            r6 = (C17830zY) r0.A04(i);
        }
        if (r6 != null) {
            C17650zF r1 = r6.A08;
            if (r1 != null) {
                Rect rect2 = r1.A02;
                if (rect2 == null || rect2.isEmpty()) {
                    rect = null;
                } else {
                    rect = r1.A02;
                }
                if (!(rect == null || (r2 = this.A0C) == null)) {
                    if (r2.A01.A04(i2) != null) {
                        if (r2.A00 == null) {
                            C07870eJ r12 = (C07870eJ) C49352c4.A02.ALa();
                            if (r12 == null) {
                                r12 = new C07870eJ(4);
                            }
                            r2.A00 = r12;
                        }
                        A09(i2, r2.A01, r2.A00);
                    }
                    A08(i, i2, r2.A01, r2.A00);
                    C07870eJ r13 = r2.A00;
                    if (r13 != null && r13.A01() == 0) {
                        C49352c4.A02.C0w(r13);
                        r2.A00 = null;
                    }
                }
            }
            Object A002 = r6.A00();
            if (this.A06 == null) {
                this.A06 = new C07870eJ();
            }
            if (A002 instanceof Drawable) {
                if (this.A01 == null) {
                    this.A01 = new C07870eJ();
                }
                C07870eJ r22 = this.A01;
                if (r22.A04(i2) != null) {
                    if (this.A03 == null) {
                        this.A03 = new C07870eJ(4);
                    }
                    A09(i2, r22, this.A03);
                }
                A08(i, i2, this.A01, this.A03);
                invalidate();
                A0B(this);
            } else if (A002 instanceof View) {
                this.A0I = true;
                View view = (View) A002;
                if (Build.VERSION.SDK_INT >= 19) {
                    view.cancelPendingInputEvents();
                }
                C15320v6.dispatchStartTemporaryDetach(view);
                C07870eJ r23 = this.A06;
                if (r23.A04(i2) != null) {
                    if (this.A05 == null) {
                        this.A05 = new C07870eJ(4);
                    }
                    A09(i2, r23, this.A05);
                }
                A08(i, i2, this.A06, this.A05);
            }
            A0A(this);
            C07870eJ r24 = this.A02;
            if (r24.A04(i2) != null) {
                if (this.A04 == null) {
                    this.A04 = new C07870eJ(4);
                }
                A09(i2, r24, this.A04);
            }
            A08(i, i2, this.A02, this.A04);
            A0B(this);
            if (A002 instanceof View) {
                C15320v6.dispatchFinishTemporaryDetach((View) A002);
            }
        }
    }

    public void A0N(boolean z) {
        AnonymousClass2Rb r0;
        if (z != this.A0N) {
            if (z && this.A0K == null) {
                this.A0K = new AnonymousClass2Rb(this, null, isFocusable(), C15320v6.getImportantForAccessibility(this));
            }
            if (z) {
                r0 = this.A0K;
            } else {
                r0 = null;
            }
            C15320v6.setAccessibilityDelegate(this, r0);
            this.A0N = z;
            if (z) {
                int childCount = getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = getChildAt(i);
                    if (childAt instanceof ComponentHost) {
                        ((ComponentHost) childAt).A0N(true);
                    } else {
                        C31401jd r3 = (C31401jd) childAt.getTag(2131297259);
                        if (r3 != null) {
                            C15320v6.setAccessibilityDelegate(childAt, new AnonymousClass2Rb(childAt, r3, childAt.isFocusable(), C15320v6.getImportantForAccessibility(childAt)));
                        }
                    }
                }
            }
        }
    }

    public void A0O(boolean z) {
        if (this.A0O != z) {
            this.A0O = z;
            if (!z) {
                if (this.A0Q) {
                    invalidate();
                    this.A0Q = false;
                }
                if (this.A0P) {
                    A0G();
                    this.A0P = false;
                }
                if (this.A0R) {
                    View rootView = getRootView();
                    if (rootView != null) {
                        rootView.requestFocus();
                    }
                    this.A0R = false;
                }
            }
        }
    }

    public boolean A0Q() {
        return !this.A0H;
    }

    public boolean addViewInLayout(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        throw new UnsupportedOperationException("Adding Views manually within LithoViews is not supported");
    }

    public void attachViewToParent(View view, int i, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("Adding Views manually within LithoViews is not supported");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008e, code lost:
        if (X.C17600zA.A03(r1) != null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d7, code lost:
        if (X.C17600zA.A03(r4) != null) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0025, code lost:
        if (r4.A00 >= r4.A01) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dispatchDraw(android.graphics.Canvas r22) {
        /*
            r21 = this;
            r1 = r21
            X.0z5 r3 = r1.A0S
            r7 = r22
            r3.A02 = r7
            r2 = 0
            r3.A00 = r2
            com.facebook.litho.ComponentHost r0 = r3.A03
            X.0eJ r0 = r0.A02
            if (r0 == 0) goto L_0x0015
            int r2 = r0.A01()
        L_0x0015:
            r3.A01 = r2
            super.dispatchDraw(r7)
            X.0z5 r4 = r1.A0S
            android.graphics.Canvas r0 = r4.A02
            if (r0 == 0) goto L_0x0027
            int r3 = r4.A00
            int r2 = r4.A01
            r0 = 1
            if (r3 < r2) goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            if (r0 == 0) goto L_0x002d
            X.C17550z5.A00(r4)
        L_0x002d:
            X.0z5 r2 = r1.A0S
            r0 = 0
            r2.A02 = r0
            java.util.ArrayList r0 = r1.A0E
            r4 = 0
            if (r0 != 0) goto L_0x0052
            r3 = 0
        L_0x0038:
            if (r4 >= r3) goto L_0x0057
            java.util.ArrayList r0 = r1.A0E
            java.lang.Object r0 = r0.get(r4)
            X.0zY r0 = (X.C17830zY) r0
            java.lang.Object r2 = r0.A00()
            boolean r0 = r2 instanceof android.graphics.drawable.Drawable
            if (r0 == 0) goto L_0x004f
            android.graphics.drawable.Drawable r2 = (android.graphics.drawable.Drawable) r2
            r2.draw(r7)
        L_0x004f:
            int r4 = r4 + 1
            goto L_0x0038
        L_0x0052:
            int r3 = r0.size()
            goto L_0x0038
        L_0x0057:
            boolean r0 = X.AnonymousClass07c.debugHighlightInteractiveBounds
            if (r0 == 0) goto L_0x0118
            android.graphics.Paint r0 = X.C51292gF.A00
            if (r0 != 0) goto L_0x006c
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            X.C51292gF.A00 = r2
            r0 = 1724029951(0x66c29bff, float:4.5950836E23)
            r2.setColor(r0)
        L_0x006c:
            android.graphics.Paint r0 = X.C51292gF.A03
            if (r0 != 0) goto L_0x007d
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            X.C51292gF.A03 = r2
            r0 = 1154744270(0x44d3ffce, float:1695.9939)
            r2.setColor(r0)
        L_0x007d:
            X.0zd r0 = X.C17600zA.A01(r1)
            if (r0 != 0) goto L_0x0090
            X.0ze r0 = X.C17600zA.A02(r1)
            if (r0 != 0) goto L_0x0090
            X.0zp r2 = X.C17600zA.A03(r1)
            r0 = 0
            if (r2 == 0) goto L_0x0091
        L_0x0090:
            r0 = 1
        L_0x0091:
            if (r0 == 0) goto L_0x00a4
            r8 = 0
            r9 = 0
            int r0 = r1.getWidth()
            float r10 = (float) r0
            int r0 = r1.getHeight()
            float r11 = (float) r0
            android.graphics.Paint r12 = X.C51292gF.A00
            r7.drawRect(r8, r9, r10, r11, r12)
        L_0x00a4:
            int r0 = r1.A0C()
            int r3 = r0 + -1
        L_0x00aa:
            if (r3 < 0) goto L_0x00f8
            X.0eJ r0 = r1.A02
            java.lang.Object r4 = r0.A05(r3)
            X.0zY r4 = (X.C17830zY) r4
            X.0zR r2 = r4.A04
            boolean r0 = X.C17770zR.A09(r2)
            if (r0 == 0) goto L_0x00f5
            boolean r0 = r2 instanceof X.C17670zH
            if (r0 != 0) goto L_0x00f5
            java.lang.Object r4 = r4.A00()
            android.view.View r4 = (android.view.View) r4
            X.0zd r0 = X.C17600zA.A01(r4)
            if (r0 != 0) goto L_0x00d9
            X.0ze r0 = X.C17600zA.A02(r4)
            if (r0 != 0) goto L_0x00d9
            X.0zp r2 = X.C17600zA.A03(r4)
            r0 = 0
            if (r2 == 0) goto L_0x00da
        L_0x00d9:
            r0 = 1
        L_0x00da:
            if (r0 == 0) goto L_0x00f5
            int r0 = r4.getLeft()
            float r8 = (float) r0
            int r0 = r4.getTop()
            float r9 = (float) r0
            int r0 = r4.getRight()
            float r10 = (float) r0
            int r0 = r4.getBottom()
            float r11 = (float) r0
            android.graphics.Paint r12 = X.C51292gF.A03
            r7.drawRect(r8, r9, r10, r11, r12)
        L_0x00f5:
            int r3 = r3 + -1
            goto L_0x00aa
        L_0x00f8:
            X.2c4 r4 = r1.A0C
            if (r4 == 0) goto L_0x0118
            android.graphics.Paint r3 = X.C51292gF.A03
            X.0eJ r0 = r4.A01
            int r0 = r0.A01()
            int r2 = r0 + -1
        L_0x0106:
            if (r2 < 0) goto L_0x0118
            X.0eJ r0 = r4.A01
            java.lang.Object r0 = r0.A05(r2)
            X.2Ra r0 = (X.C46662Ra) r0
            android.graphics.Rect r0 = r0.A03
            r7.drawRect(r0, r3)
            int r2 = r2 + -1
            goto L_0x0106
        L_0x0118:
            boolean r0 = X.AnonymousClass07c.debugHighlightMountBounds
            if (r0 == 0) goto L_0x025b
            android.content.res.Resources r6 = r1.getResources()
            android.graphics.Rect r0 = X.C51292gF.A04
            if (r0 != 0) goto L_0x012b
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            X.C51292gF.A04 = r0
        L_0x012b:
            android.graphics.Paint r0 = X.C51292gF.A01
            r4 = 1
            if (r0 != 0) goto L_0x014e
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            X.C51292gF.A01 = r2
            android.graphics.Paint$Style r0 = android.graphics.Paint.Style.STROKE
            r2.setStyle(r0)
            android.graphics.Paint r3 = X.C51292gF.A01
            android.util.DisplayMetrics r0 = r6.getDisplayMetrics()
            float r0 = r0.density
            float r2 = (float) r4
            float r2 = r2 * r0
            r0 = 1056964608(0x3f000000, float:0.5)
            float r2 = r2 + r0
            int r0 = (int) r2
            float r0 = (float) r0
            r3.setStrokeWidth(r0)
        L_0x014e:
            android.graphics.Paint r0 = X.C51292gF.A02
            if (r0 != 0) goto L_0x0171
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            X.C51292gF.A02 = r2
            android.graphics.Paint$Style r0 = android.graphics.Paint.Style.FILL
            r2.setStyle(r0)
            android.graphics.Paint r3 = X.C51292gF.A02
            r2 = 2
            android.util.DisplayMetrics r0 = r6.getDisplayMetrics()
            float r0 = r0.density
            float r2 = (float) r2
            float r2 = r2 * r0
            r0 = 1056964608(0x3f000000, float:0.5)
            float r2 = r2 + r0
            int r0 = (int) r2
            float r0 = (float) r0
            r3.setStrokeWidth(r0)
        L_0x0171:
            int r0 = r1.A0C()
            int r0 = r0 - r4
        L_0x0176:
            if (r0 < 0) goto L_0x025b
            X.0eJ r2 = r1.A02
            java.lang.Object r2 = r2.A05(r0)
            X.0zY r2 = (X.C17830zY) r2
            X.0zR r4 = r2.A04
            java.lang.Object r5 = r2.A00()
            boolean r3 = r4 instanceof X.C15060uf
            r2 = 0
            if (r3 != 0) goto L_0x018c
            r2 = 1
        L_0x018c:
            if (r2 == 0) goto L_0x0246
            boolean r2 = r5 instanceof android.view.View
            if (r2 == 0) goto L_0x024a
            android.view.View r5 = (android.view.View) r5
            android.graphics.Rect r3 = X.C51292gF.A04
            int r2 = r5.getLeft()
            r3.left = r2
            android.graphics.Rect r3 = X.C51292gF.A04
            int r2 = r5.getTop()
            r3.top = r2
            android.graphics.Rect r3 = X.C51292gF.A04
            int r2 = r5.getRight()
            r3.right = r2
            android.graphics.Rect r3 = X.C51292gF.A04
            int r2 = r5.getBottom()
            r3.bottom = r2
        L_0x01b4:
            android.graphics.Paint r3 = X.C51292gF.A01
            boolean r4 = r4 instanceof X.C17670zH
            r2 = -1711341568(0xffffffff99ff0000, float:-2.6366382E-23)
            if (r4 == 0) goto L_0x01bf
            r2 = -1711341313(0xffffffff99ff00ff, float:-2.6366784E-23)
        L_0x01bf:
            r3.setColor(r2)
            android.graphics.Paint r12 = X.C51292gF.A01
            android.graphics.Rect r5 = X.C51292gF.A04
            float r2 = r12.getStrokeWidth()
            int r2 = (int) r2
            int r3 = r2 >> 1
            int r2 = r5.left
            int r2 = r2 + r3
            float r8 = (float) r2
            int r2 = r5.top
            int r2 = r2 + r3
            float r9 = (float) r2
            int r2 = r5.right
            int r2 = r2 - r3
            float r10 = (float) r2
            int r2 = r5.bottom
            int r2 = r2 - r3
            float r11 = (float) r2
            r7.drawRect(r8, r9, r10, r11, r12)
            android.graphics.Paint r3 = X.C51292gF.A02
            r2 = -16776961(0xffffffffff0000ff, float:-1.7014636E38)
            if (r4 == 0) goto L_0x01ea
            r2 = -16711681(0xffffffffff00ffff, float:-1.714704E38)
        L_0x01ea:
            r3.setColor(r2)
            android.graphics.Paint r8 = X.C51292gF.A02
            android.graphics.Rect r4 = X.C51292gF.A04
            float r2 = r8.getStrokeWidth()
            int r11 = (int) r2
            android.graphics.Rect r2 = X.C51292gF.A04
            int r3 = r2.width()
            android.graphics.Rect r2 = X.C51292gF.A04
            int r2 = r2.height()
            int r2 = java.lang.Math.min(r3, r2)
            int r5 = r2 / 3
            r3 = 12
            android.util.DisplayMetrics r2 = r6.getDisplayMetrics()
            float r2 = r2.density
            float r3 = (float) r3
            float r3 = r3 * r2
            r2 = 1056964608(0x3f000000, float:0.5)
            float r3 = r3 + r2
            int r2 = (int) r3
            int r13 = java.lang.Math.min(r5, r2)
            int r9 = r4.left
            int r10 = r4.top
            r12 = r11
            X.C51292gF.A00(r7, r8, r9, r10, r11, r12, r13)
            int r9 = r4.left
            int r10 = r4.bottom
            int r12 = -r11
            X.C51292gF.A00(r7, r8, r9, r10, r11, r12, r13)
            int r3 = r4.right
            int r2 = r4.top
            r14 = r7
            r15 = r8
            r17 = r2
            r18 = r12
            r19 = r11
            r20 = r13
            r16 = r3
            X.C51292gF.A00(r14, r15, r16, r17, r18, r19, r20)
            int r9 = r4.right
            int r2 = r4.bottom
            r10 = r2
            r11 = r12
            X.C51292gF.A00(r7, r8, r9, r10, r11, r12, r13)
        L_0x0246:
            int r0 = r0 + -1
            goto L_0x0176
        L_0x024a:
            boolean r2 = r5 instanceof android.graphics.drawable.Drawable
            if (r2 == 0) goto L_0x01b4
            android.graphics.drawable.Drawable r5 = (android.graphics.drawable.Drawable) r5
            android.graphics.Rect r3 = X.C51292gF.A04
            android.graphics.Rect r2 = r5.getBounds()
            r3.set(r2)
            goto L_0x01b4
        L_0x025b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentHost.dispatchDraw(android.graphics.Canvas):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r1 == false) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        if (r2.A0Q(r4) != false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchHoverEvent(android.view.MotionEvent r4) {
        /*
            r3 = this;
            X.2Rb r2 = r3.A0K
            if (r2 == 0) goto L_0x001c
            X.0zY r0 = r3.A0D()
            if (r0 == 0) goto L_0x0013
            X.0zR r0 = r0.A04
            boolean r1 = r0.A0y()
            r0 = 1
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            if (r0 == 0) goto L_0x001c
            boolean r0 = r2.A0Q(r4)
            if (r0 != 0) goto L_0x0023
        L_0x001c:
            boolean r1 = super.dispatchHoverEvent(r4)
            r0 = 0
            if (r1 == 0) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentHost.dispatchHoverEvent(android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0078, code lost:
        if (r3.A00 >= r3.A01) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getChildDrawingOrder(int r9, int r10) {
        /*
            r8 = this;
            boolean r0 = r8.A0I
            if (r0 == 0) goto L_0x006d
            int r1 = r8.getChildCount()
            int[] r0 = r8.A0J
            int r0 = r0.length
            if (r0 >= r1) goto L_0x0013
            int r0 = r1 + 5
            int[] r0 = new int[r0]
            r8.A0J = r0
        L_0x0013:
            X.0eJ r0 = r8.A06
            r6 = 0
            if (r0 != 0) goto L_0x0039
            r4 = 0
        L_0x0019:
            r3 = 0
            r7 = 0
        L_0x001b:
            if (r3 >= r4) goto L_0x003e
            X.0eJ r0 = r8.A06
            java.lang.Object r0 = r0.A05(r3)
            X.0zY r0 = (X.C17830zY) r0
            java.lang.Object r0 = r0.A00()
            android.view.View r0 = (android.view.View) r0
            int[] r2 = r8.A0J
            int r1 = r7 + 1
            int r0 = r8.indexOfChild(r0)
            r2[r7] = r0
            int r3 = r3 + 1
            r7 = r1
            goto L_0x001b
        L_0x0039:
            int r4 = r0.A01()
            goto L_0x0019
        L_0x003e:
            java.util.ArrayList r0 = r8.A0E
            if (r0 != 0) goto L_0x0066
            r5 = 0
        L_0x0043:
            r4 = 0
        L_0x0044:
            if (r4 >= r5) goto L_0x006b
            java.util.ArrayList r0 = r8.A0E
            java.lang.Object r0 = r0.get(r4)
            X.0zY r0 = (X.C17830zY) r0
            java.lang.Object r3 = r0.A00()
            boolean r0 = r3 instanceof android.view.View
            if (r0 == 0) goto L_0x0063
            int[] r2 = r8.A0J
            int r1 = r7 + 1
            android.view.View r3 = (android.view.View) r3
            int r0 = r8.indexOfChild(r3)
            r2[r7] = r0
            r7 = r1
        L_0x0063:
            int r4 = r4 + 1
            goto L_0x0044
        L_0x0066:
            int r5 = r0.size()
            goto L_0x0043
        L_0x006b:
            r8.A0I = r6
        L_0x006d:
            X.0z5 r3 = r8.A0S
            android.graphics.Canvas r0 = r3.A02
            if (r0 == 0) goto L_0x007a
            int r2 = r3.A00
            int r1 = r3.A01
            r0 = 1
            if (r2 < r1) goto L_0x007b
        L_0x007a:
            r0 = 0
        L_0x007b:
            if (r0 == 0) goto L_0x0080
            X.C17550z5.A00(r3)
        L_0x0080:
            int[] r0 = r8.A0J
            r0 = r0[r10]
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.litho.ComponentHost.getChildDrawingOrder(int, int):int");
    }

    public boolean getClipChildren() {
        if (Build.VERSION.SDK_INT < 18) {
            return this.A0M;
        }
        return super.getClipChildren();
    }

    public CharSequence getContentDescription() {
        return this.A0L;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        AnonymousClass10N r2 = this.A0B;
        if (r2 == null) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        if (C33181nA.A03 == null) {
            C33181nA.A03 = new C51302gG();
        }
        C51302gG r1 = C33181nA.A03;
        r1.A00 = motionEvent;
        r1.A01 = this;
        Object AXa = r2.A00.Alq().AXa(r2, r1);
        C51302gG r12 = C33181nA.A03;
        r12.A00 = null;
        r12.A01 = null;
        if (AXa == null || !((Boolean) AXa).booleanValue()) {
            return false;
        }
        return true;
    }

    public boolean performAccessibilityAction(int i, Bundle bundle) {
        List textItems;
        if (i == 512 || i == 256) {
            CharSequence charSequence = null;
            if (!TextUtils.isEmpty(getContentDescription())) {
                charSequence = getContentDescription();
            } else {
                if (!A07().isEmpty()) {
                    textItems = A07();
                } else if (!getTextContent().getTextItems().isEmpty()) {
                    textItems = getTextContent().getTextItems();
                }
                charSequence = TextUtils.join(", ", textItems);
            }
            if (charSequence == null) {
                return false;
            }
            this.A0L = charSequence;
            super.setContentDescription(charSequence);
        }
        return super.performAccessibilityAction(i, bundle);
    }

    public void removeAllViewsInLayout() {
        throw new UnsupportedOperationException("Removing Views manually within LithoViews is not supported");
    }

    public void removeDetachedView(View view, boolean z) {
        throw new UnsupportedOperationException("Removing Views manually within LithoViews is not supported");
    }

    public void removeView(View view) {
        throw new UnsupportedOperationException("Removing Views manually within LithoViews is not supported");
    }

    public void removeViewAt(int i) {
        throw new UnsupportedOperationException("Removing Views manually within LithoViews is not supported");
    }

    public void removeViewInLayout(View view) {
        throw new UnsupportedOperationException("Removing Views manually within LithoViews is not supported");
    }

    public void removeViews(int i, int i2) {
        throw new UnsupportedOperationException("Removing Views manually within LithoViews is not supported");
    }

    public void removeViewsInLayout(int i, int i2) {
        throw new UnsupportedOperationException("Removing Views manually within LithoViews is not supported");
    }

    public void setClipChildren(boolean z) {
        if (this.A0F) {
            this.A0G = z;
            return;
        }
        if (Build.VERSION.SDK_INT < 18) {
            this.A0M = z;
        }
        super.setClipChildren(z);
    }

    public void setContentDescription(CharSequence charSequence) {
        this.A0L = charSequence;
        if (!TextUtils.isEmpty(charSequence) && C15320v6.getImportantForAccessibility(this) == 0) {
            C15320v6.setImportantForAccessibility(this, 1);
        }
        A0G();
    }

    public void A0J(int i, C17830zY r4) {
        Object A002 = r4.A00();
        if (A002 instanceof Drawable) {
            if (this.A01 == null) {
                this.A01 = new C07870eJ();
            }
            Drawable drawable = (Drawable) r4.A00();
            drawable.setCallback(null);
            invalidate(drawable.getBounds());
            A0B(this);
            C15070ug.A01(i, this.A01, this.A03);
        } else if (A002 instanceof View) {
            View view = (View) A002;
            this.A0I = true;
            if (this.A0H) {
                super.removeViewInLayout(view);
            } else {
                super.removeView(view);
            }
            if (this.A06 == null) {
                this.A06 = new C07870eJ();
            }
            C15070ug.A01(i, this.A06, this.A05);
            this.A0I = true;
            A0I(i, r4);
        }
        A0A(this);
        C15070ug.A01(i, this.A02, this.A04);
        A0B(this);
        if (r4.A02()) {
            r4.A05.A0G();
        }
    }

    public void A0K(int i, C17830zY r9, Rect rect) {
        Object A002 = r9.A00();
        if (A002 instanceof Drawable) {
            if (this.A01 == null) {
                this.A01 = new C07870eJ();
            }
            this.A01.A09(i, r9);
            Drawable drawable = (Drawable) r9.A00();
            int i2 = r9.A01;
            C31401jd r4 = r9.A06;
            boolean z = false;
            if (getVisibility() == 0) {
                z = true;
            }
            drawable.setVisible(z, false);
            drawable.setCallback(this);
            C15070ug.A02(this, drawable, i2, r4);
            invalidate(rect);
        } else if (A002 instanceof View) {
            if (this.A06 == null) {
                this.A06 = new C07870eJ();
            }
            this.A06.A09(i, r9);
            View view = (View) A002;
            boolean z2 = true;
            if ((r9.A01 & 1) != 1) {
                z2 = false;
            }
            view.setDuplicateParentStateEnabled(z2);
            this.A0I = true;
            if (!(view instanceof ComponentHost) || view.getParent() != this) {
                if (view.getLayoutParams() == null) {
                    view.setLayoutParams(generateDefaultLayoutParams());
                }
                if (this.A0H) {
                    super.addViewInLayout(view, -1, view.getLayoutParams(), true);
                } else {
                    super.addView(view, -1, view.getLayoutParams());
                }
            } else {
                C15320v6.dispatchFinishTemporaryDetach(view);
                view.setVisibility(0);
            }
            A0H(i, r9);
        }
        A0A(this);
        this.A02.A09(i, r9);
        if (r9.A02()) {
            r9.A05.A0G();
        }
    }

    public void drawableStateChanged() {
        int i;
        super.drawableStateChanged();
        C07870eJ r0 = this.A01;
        if (r0 == null) {
            i = 0;
        } else {
            i = r0.A01();
        }
        for (int i2 = 0; i2 < i; i2++) {
            C17830zY r02 = (C17830zY) this.A01.A05(i2);
            C15070ug.A02(this, (Drawable) r02.A00(), r02.A01, r02.A06);
        }
    }

    public TextContent getTextContent() {
        List arrayList;
        A0A(this);
        C07870eJ r4 = this.A02;
        int A012 = r4.A01();
        if (A012 == 1) {
            arrayList = Collections.singletonList(((C17830zY) r4.A05(0)).A00());
        } else {
            arrayList = new ArrayList(A012);
            for (int i = 0; i < A012; i++) {
                arrayList.add(((C17830zY) r4.A05(i)).A00());
            }
        }
        return C15070ug.A00(arrayList);
    }

    public void jumpDrawablesToCurrentState() {
        int i;
        super.jumpDrawablesToCurrentState();
        C07870eJ r0 = this.A01;
        if (r0 == null) {
            i = 0;
        } else {
            i = r0.A01();
        }
        for (int i2 = 0; i2 < i; i2++) {
            ((Drawable) ((C17830zY) this.A01.A05(i2)).A00()).jumpToCurrentState();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int A012;
        int A052 = C000700l.A05(483675907);
        boolean z = true;
        if (isEnabled()) {
            C07870eJ r0 = this.A01;
            if (r0 == null) {
                A012 = 0;
            } else {
                A012 = r0.A01();
            }
            int i = A012 - 1;
            while (true) {
                if (i < 0) {
                    break;
                }
                C17830zY r3 = (C17830zY) this.A01.A05(i);
                if (r3.A00() instanceof AnonymousClass1MS) {
                    boolean z2 = false;
                    if ((r3.A01 & 2) == 2) {
                        z2 = true;
                    }
                    if (!z2) {
                        AnonymousClass1MS r1 = (AnonymousClass1MS) r3.A00();
                        if (r1.CE9(motionEvent) && r1.BsU(motionEvent, this)) {
                            break;
                        }
                    } else {
                        continue;
                    }
                }
                i--;
            }
        }
        z = false;
        if (!z) {
            z = super.onTouchEvent(motionEvent);
        }
        C000700l.A0B(-1079944834, A052);
        return z;
    }

    public void setAccessibilityDelegate(View.AccessibilityDelegate accessibilityDelegate) {
        super.setAccessibilityDelegate(accessibilityDelegate);
        this.A0N = false;
    }

    public void setTag(int i, Object obj) {
        super.setTag(i, obj);
        if (i == 2131297259 && obj != null) {
            A0N(C17560z6.A01(getContext()));
            AnonymousClass2Rb r0 = this.A0K;
            if (r0 != null) {
                r0.A00 = (C31401jd) obj;
            }
        }
    }

    public void setVisibility(int i) {
        int A012;
        super.setVisibility(i);
        C07870eJ r0 = this.A01;
        if (r0 == null) {
            A012 = 0;
        } else {
            A012 = r0.A01();
        }
        for (int i2 = 0; i2 < A012; i2++) {
            Drawable drawable = (Drawable) ((C17830zY) this.A01.A05(i2)).A00();
            boolean z = false;
            if (i == 0) {
                z = true;
            }
            drawable.setVisible(z, false);
        }
    }

    public void addView(View view) {
        throw new UnsupportedOperationException("Adding Views manually within LithoViews is not supported");
    }

    public void addView(View view, int i) {
        throw new UnsupportedOperationException("Adding Views manually within LithoViews is not supported");
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("Adding Views manually within LithoViews is not supported");
    }

    public Object getTag() {
        Object obj = this.A0D;
        if (obj != null) {
            return obj;
        }
        return super.getTag();
    }

    public Object getTag(int i) {
        Object obj;
        SparseArray sparseArray = this.A00;
        if (sparseArray == null || (obj = sparseArray.get(i)) == null) {
            return super.getTag(i);
        }
        return obj;
    }

    public void invalidate() {
        if (this.A0O) {
            this.A0Q = true;
        } else {
            super.invalidate();
        }
    }

    public void invalidate(int i, int i2, int i3, int i4) {
        if (this.A0O) {
            this.A0Q = true;
        } else {
            super.invalidate(i, i2, i3, i4);
        }
    }

    public void invalidate(Rect rect) {
        if (this.A0O) {
            this.A0Q = true;
        } else {
            super.invalidate(rect);
        }
    }
}
