package com.fasterxml.jackson.databind.deser.std;

import X.C26791c3;
import X.C28271eX;
import X.C64433Bp;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberDeserializers$IntegerDeserializer extends NumberDeserializers$PrimitiveOrWrapperDeserializer {
    public static final NumberDeserializers$IntegerDeserializer primitiveInstance = new NumberDeserializers$IntegerDeserializer(Integer.class, 0);
    private static final long serialVersionUID = 1;
    public static final NumberDeserializers$IntegerDeserializer wrapperInstance = new NumberDeserializers$IntegerDeserializer(Integer.TYPE, null);

    private NumberDeserializers$IntegerDeserializer(Class cls, Integer num) {
        super(cls, num);
    }

    public /* bridge */ /* synthetic */ Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return _parseInteger(r2, r3);
    }

    private Integer deserialize(C28271eX r2, C26791c3 r3) {
        return _parseInteger(r2, r3);
    }
}
