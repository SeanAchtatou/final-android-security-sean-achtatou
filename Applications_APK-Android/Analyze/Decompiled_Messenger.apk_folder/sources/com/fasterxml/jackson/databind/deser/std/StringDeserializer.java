package com.fasterxml.jackson.databind.deser.std;

import X.C10340jw;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C64433Bp;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class StringDeserializer extends StdScalarDeserializer {
    public static final StringDeserializer instance = new StringDeserializer();
    private static final long serialVersionUID = 1;

    public StringDeserializer() {
        super(String.class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.deser.std.StringDeserializer.deserialize(X.1eX, X.1c3):java.lang.String
     arg types: [X.1eX, X.1c3]
     candidates:
      com.fasterxml.jackson.databind.deser.std.StringDeserializer.deserialize(X.1eX, X.1c3):java.lang.Object
      com.fasterxml.jackson.databind.JsonDeserializer.deserialize(X.1eX, X.1c3):java.lang.Object
      com.fasterxml.jackson.databind.deser.std.StringDeserializer.deserialize(X.1eX, X.1c3):java.lang.String */
    public /* bridge */ /* synthetic */ Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return deserialize(r2, r3);
    }

    /* access modifiers changed from: private */
    public String deserialize(C28271eX r4, C26791c3 r5) {
        String valueAsString = r4.getValueAsString();
        if (valueAsString != null) {
            return valueAsString;
        }
        C182811d currentToken = r4.getCurrentToken();
        if (currentToken == C182811d.VALUE_EMBEDDED_OBJECT) {
            Object embeddedObject = r4.getEmbeddedObject();
            if (embeddedObject == null) {
                return null;
            }
            if (embeddedObject instanceof byte[]) {
                return C10340jw.MIME_NO_LINEFEEDS.encode((byte[]) embeddedObject, false);
            }
            return embeddedObject.toString();
        }
        throw r5.mappingException(this._valueClass, currentToken);
    }
}
