package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import X.CY1;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public class ToStringSerializer extends StdSerializer {
    public static final ToStringSerializer instance = new ToStringSerializer();

    public boolean isEmpty(Object obj) {
        String obj2;
        return obj == null || (obj2 = obj.toString()) == null || obj2.length() == 0;
    }

    public ToStringSerializer() {
        super(Object.class);
    }

    public void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeString(obj.toString());
    }

    public void serializeWithType(Object obj, C11710np r2, C11260mU r3, CY1 cy1) {
        cy1.writeTypePrefixForScalar(obj, r2);
        serialize(obj, r2, r3);
        cy1.writeTypeSuffixForScalar(obj, r2);
    }
}
