package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import X.CY1;

public abstract class StdScalarSerializer extends StdSerializer {
    public void serializeWithType(Object obj, C11710np r2, C11260mU r3, CY1 cy1) {
        cy1.writeTypePrefixForScalar(obj, r2);
        serialize(obj, r2, r3);
        cy1.writeTypeSuffixForScalar(obj, r2);
    }

    public StdScalarSerializer(Class cls) {
        super(cls);
    }

    public StdScalarSerializer(Class cls, boolean z) {
        super(cls);
    }
}
