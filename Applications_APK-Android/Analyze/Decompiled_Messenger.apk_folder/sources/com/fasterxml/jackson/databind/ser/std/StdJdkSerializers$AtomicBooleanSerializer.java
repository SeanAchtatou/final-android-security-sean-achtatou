package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import java.util.concurrent.atomic.AtomicBoolean;

public final class StdJdkSerializers$AtomicBooleanSerializer extends StdScalarSerializer {
    public StdJdkSerializers$AtomicBooleanSerializer() {
        super(AtomicBoolean.class, false);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeBoolean(((AtomicBoolean) obj).get());
    }
}
