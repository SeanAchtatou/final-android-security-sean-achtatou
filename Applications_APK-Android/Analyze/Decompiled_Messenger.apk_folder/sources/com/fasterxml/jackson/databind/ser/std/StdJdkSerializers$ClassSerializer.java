package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;

public final class StdJdkSerializers$ClassSerializer extends StdScalarSerializer {
    public StdJdkSerializers$ClassSerializer() {
        super(Class.class, false);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeString(((Class) obj).getName());
    }
}
