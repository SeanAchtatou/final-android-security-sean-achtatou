package com.fasterxml.jackson.databind.node;

import X.C10540kM;
import java.io.Serializable;

public class JsonNodeFactory implements C10540kM, Serializable {
    private static final JsonNodeFactory decimalsAsIs = new JsonNodeFactory(true);
    private static final JsonNodeFactory decimalsNormalized;
    public static final JsonNodeFactory instance;
    private static final long serialVersionUID = -3271940633258788634L;
    public final boolean _cfgBigDecimalExact;

    static {
        JsonNodeFactory jsonNodeFactory = new JsonNodeFactory(false);
        decimalsNormalized = jsonNodeFactory;
        instance = jsonNodeFactory;
    }

    public TextNode textNode(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return TextNode.EMPTY_STRING_NODE;
        }
        return new TextNode(str);
    }

    public JsonNodeFactory() {
        this(false);
    }

    public JsonNodeFactory(boolean z) {
        this._cfgBigDecimalExact = z;
    }
}
