package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import java.io.File;

public final class StdJdkSerializers$FileSerializer extends StdScalarSerializer {
    public StdJdkSerializers$FileSerializer() {
        super(File.class);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeString(((File) obj).getAbsolutePath());
    }
}
