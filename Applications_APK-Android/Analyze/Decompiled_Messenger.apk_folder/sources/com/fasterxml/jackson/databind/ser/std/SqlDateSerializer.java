package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.sql.Date;

@JacksonStdImpl
public class SqlDateSerializer extends StdScalarSerializer {
    public SqlDateSerializer() {
        super(Date.class);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeString(((Date) obj).toString());
    }
}
