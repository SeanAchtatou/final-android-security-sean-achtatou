package com.fasterxml.jackson.databind;

import X.C10010jO;
import X.C11980oL;
import X.C22314Asw;
import com.facebook.acra.ACRA;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;

public abstract class JsonNode implements Iterable, C10010jO {
    public boolean asBoolean(boolean z) {
        return z;
    }

    public double asDouble(double d) {
        return d;
    }

    public int asInt(int i) {
        return i;
    }

    public long asLong(long j) {
        return j;
    }

    public abstract String asText();

    public byte[] binaryValue() {
        return null;
    }

    public boolean booleanValue() {
        return false;
    }

    public boolean canConvertToInt() {
        return false;
    }

    public abstract JsonNode deepCopy();

    public double doubleValue() {
        return 0.0d;
    }

    public abstract boolean equals(Object obj);

    public abstract JsonNode findPath(String str);

    public abstract JsonNode findValue(String str);

    public float floatValue() {
        return 0.0f;
    }

    public abstract JsonNode get(int i);

    public JsonNode get(String str) {
        return null;
    }

    public abstract C11980oL getNodeType();

    public int intValue() {
        return 0;
    }

    public boolean isInt() {
        return false;
    }

    public boolean isLong() {
        return false;
    }

    public long longValue() {
        return 0;
    }

    public Number numberValue() {
        return null;
    }

    public abstract JsonNode path(String str);

    public int size() {
        return 0;
    }

    public String textValue() {
        return null;
    }

    public abstract String toString();

    public BigInteger bigIntegerValue() {
        return BigInteger.ZERO;
    }

    public BigDecimal decimalValue() {
        return BigDecimal.ZERO;
    }

    public Iterator elements() {
        return C22314Asw.instance;
    }

    public Iterator fieldNames() {
        return C22314Asw.instance;
    }

    public Iterator fields() {
        return C22314Asw.instance;
    }

    public boolean has(String str) {
        if (get(str) != null) {
            return true;
        }
        return false;
    }

    public boolean hasNonNull(String str) {
        JsonNode jsonNode = get(str);
        if (jsonNode == null || jsonNode.isNull()) {
            return false;
        }
        return true;
    }

    public final boolean isArray() {
        if (getNodeType() == C11980oL.ARRAY) {
            return true;
        }
        return false;
    }

    public final boolean isBoolean() {
        if (getNodeType() == C11980oL.BOOLEAN) {
            return true;
        }
        return false;
    }

    public final boolean isNull() {
        if (getNodeType() == C11980oL.NULL) {
            return true;
        }
        return false;
    }

    public final boolean isNumber() {
        if (getNodeType() == C11980oL.NUMBER) {
            return true;
        }
        return false;
    }

    public final boolean isObject() {
        if (getNodeType() == C11980oL.OBJECT) {
            return true;
        }
        return false;
    }

    public final boolean isTextual() {
        if (getNodeType() == C11980oL.STRING) {
            return true;
        }
        return false;
    }

    public final boolean isValueNode() {
        switch (getNodeType().ordinal()) {
            case 0:
            case 3:
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return false;
            case 1:
            case 2:
            case 4:
            case 5:
            default:
                return true;
        }
    }

    public final Iterator iterator() {
        return elements();
    }

    public boolean asBoolean() {
        return asBoolean(false);
    }

    public double asDouble() {
        return asDouble(0.0d);
    }

    public int asInt() {
        return asInt(0);
    }

    public long asLong() {
        return asLong(0);
    }
}
