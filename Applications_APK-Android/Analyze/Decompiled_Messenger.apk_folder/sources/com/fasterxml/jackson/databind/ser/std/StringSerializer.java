package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class StringSerializer extends NonTypedScalarSerializerBase {
    public StringSerializer() {
        super(String.class);
    }

    public /* bridge */ /* synthetic */ boolean isEmpty(Object obj) {
        String str = (String) obj;
        if (str == null || str.length() == 0) {
            return true;
        }
        return false;
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r2, C11260mU r3) {
        r2.writeString((String) obj);
    }
}
