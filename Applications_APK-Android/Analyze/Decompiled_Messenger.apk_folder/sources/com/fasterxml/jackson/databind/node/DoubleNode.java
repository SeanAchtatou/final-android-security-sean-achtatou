package com.fasterxml.jackson.databind.node;

import X.C11260mU;
import X.C11710np;
import X.C14680to;
import X.C182811d;
import X.C29501gW;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class DoubleNode extends C14680to {
    public final double _value;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return Double.compare(this._value, ((DoubleNode) obj)._value) == 0;
    }

    public String asText() {
        return Double.toString(this._value);
    }

    public C182811d asToken() {
        return C182811d.VALUE_NUMBER_FLOAT;
    }

    public boolean canConvertToInt() {
        double d = this._value;
        if (d < -2.147483648E9d || d > 2.147483647E9d) {
            return false;
        }
        return true;
    }

    public BigDecimal decimalValue() {
        return BigDecimal.valueOf(this._value);
    }

    public double doubleValue() {
        return this._value;
    }

    public float floatValue() {
        return (float) this._value;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this._value);
        return ((int) doubleToLongBits) ^ ((int) (doubleToLongBits >> 32));
    }

    public int intValue() {
        return (int) this._value;
    }

    public long longValue() {
        return (long) this._value;
    }

    public C29501gW numberType() {
        return C29501gW.DOUBLE;
    }

    public Number numberValue() {
        return Double.valueOf(this._value);
    }

    public final void serialize(C11710np r3, C11260mU r4) {
        r3.writeNumber(this._value);
    }

    public DoubleNode(double d) {
        this._value = d;
    }

    public BigInteger bigIntegerValue() {
        return decimalValue().toBigInteger();
    }
}
