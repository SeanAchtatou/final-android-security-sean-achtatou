package com.fasterxml.jackson.databind.ser.std;

import X.AnonymousClass08S;
import X.AnonymousClass1Y3;
import X.AnonymousClass2Rd;
import X.C10240jm;
import X.C11260mU;
import X.C11700no;
import X.C11710np;
import X.C182811d;
import X.C21471AFm;
import X.CXm;
import X.CY1;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.webrtc.audio.WebRtcAudioRecord;

@JacksonStdImpl
public class TokenBufferSerializer extends StdSerializer {
    public TokenBufferSerializer() {
        super(C11700no.class);
    }

    public /* bridge */ /* synthetic */ void serializeWithType(Object obj, C11710np r2, C11260mU r3, CY1 cy1) {
        C11700no r1 = (C11700no) obj;
        cy1.writeTypePrefixForScalar(r1, r2);
        serialize(r1, r2, r3);
        cy1.writeTypeSuffixForScalar(r1, r2);
    }

    private static void serialize(C11700no r7, C11710np r8, C11260mU r9) {
        AnonymousClass2Rd r2 = r7._first;
        int i = -1;
        while (true) {
            i++;
            if (i >= 16) {
                r2 = r2._next;
                if (r2 != null) {
                    i = 0;
                } else {
                    return;
                }
            }
            long j = r2._tokenTypes;
            if (i > 0) {
                j >>= i << 2;
            }
            C182811d r0 = AnonymousClass2Rd.TOKEN_TYPES_BY_INDEX[((int) j) & 15];
            if (r0 != null) {
                switch (CXm.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r0.ordinal()]) {
                    case 1:
                        r8.writeStartObject();
                        continue;
                    case 2:
                        r8.writeEndObject();
                        continue;
                    case 3:
                        r8.writeStartArray();
                        continue;
                    case 4:
                        r8.writeEndArray();
                        continue;
                    case 5:
                        Object obj = r2._tokens[i];
                        if (obj instanceof C10240jm) {
                            r8.writeFieldName((C10240jm) obj);
                            continue;
                        } else {
                            r8.writeFieldName((String) obj);
                        }
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        Object obj2 = r2._tokens[i];
                        if (obj2 instanceof C10240jm) {
                            r8.writeString((C10240jm) obj2);
                            continue;
                        } else {
                            r8.writeString((String) obj2);
                        }
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                        Object obj3 = r2._tokens[i];
                        if (obj3 instanceof Integer) {
                            r8.writeNumber(((Integer) obj3).intValue());
                            continue;
                        } else if (obj3 instanceof BigInteger) {
                            r8.writeNumber((BigInteger) obj3);
                        } else if (obj3 instanceof Long) {
                            r8.writeNumber(((Long) obj3).longValue());
                        } else if (obj3 instanceof Short) {
                            r8.writeNumber(((Short) obj3).shortValue());
                        } else {
                            r8.writeNumber(((Number) obj3).intValue());
                        }
                    case 8:
                        Object obj4 = r2._tokens[i];
                        if (obj4 instanceof Double) {
                            r8.writeNumber(((Double) obj4).doubleValue());
                            continue;
                        } else if (obj4 instanceof BigDecimal) {
                            r8.writeNumber((BigDecimal) obj4);
                        } else if (obj4 instanceof Float) {
                            r8.writeNumber(((Float) obj4).floatValue());
                        } else if (obj4 != null) {
                            if (obj4 instanceof String) {
                                r8.writeNumber((String) obj4);
                            } else {
                                throw new C21471AFm(AnonymousClass08S.A0P("Unrecognized value type for VALUE_NUMBER_FLOAT: ", obj4.getClass().getName(), ", can not serialize"));
                            }
                        }
                        break;
                    case Process.SIGKILL:
                        r8.writeBoolean(true);
                        continue;
                    case AnonymousClass1Y3.A01:
                        r8.writeBoolean(false);
                        continue;
                    case AnonymousClass1Y3.A02:
                        break;
                    case AnonymousClass1Y3.A03:
                        r8.writeObject(r2._tokens[i]);
                        continue;
                    default:
                        throw new RuntimeException("Internal error: should never end up through this code path");
                }
                r8.writeNull();
            } else {
                return;
            }
        }
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r2, C11260mU r3) {
        serialize((C11700no) obj, r2, r3);
    }
}
