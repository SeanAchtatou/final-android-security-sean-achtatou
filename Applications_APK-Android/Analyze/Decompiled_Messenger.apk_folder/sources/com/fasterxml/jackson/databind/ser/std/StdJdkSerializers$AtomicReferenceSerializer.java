package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import java.util.concurrent.atomic.AtomicReference;

public final class StdJdkSerializers$AtomicReferenceSerializer extends StdSerializer {
    public StdJdkSerializers$AtomicReferenceSerializer() {
        super(AtomicReference.class, false);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r4.defaultSerializeValue(((AtomicReference) obj).get(), r3);
    }
}
