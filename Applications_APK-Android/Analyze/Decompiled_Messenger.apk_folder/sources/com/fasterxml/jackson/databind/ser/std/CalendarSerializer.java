package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.text.DateFormat;
import java.util.Calendar;

@JacksonStdImpl
public class CalendarSerializer extends DateTimeSerializerBase {
    public static final CalendarSerializer instance = new CalendarSerializer();

    public /* bridge */ /* synthetic */ long _timestamp(Object obj) {
        Calendar calendar = (Calendar) obj;
        if (calendar == null) {
            return 0;
        }
        return calendar.getTimeInMillis();
    }

    public /* bridge */ /* synthetic */ DateTimeSerializerBase withFormat(boolean z, DateFormat dateFormat) {
        CalendarSerializer calendarSerializer;
        if (z) {
            return calendarSerializer;
        }
        calendarSerializer = new CalendarSerializer(false, dateFormat);
        return calendarSerializer;
    }

    public CalendarSerializer() {
        this(false, null);
    }

    private CalendarSerializer(boolean z, DateFormat dateFormat) {
        super(Calendar.class, z, dateFormat);
    }

    public void serialize(Calendar calendar, C11710np r4, C11260mU r5) {
        long timeInMillis;
        if (this._useTimestamp) {
            if (calendar == null) {
                timeInMillis = 0;
            } else {
                timeInMillis = calendar.getTimeInMillis();
            }
            r4.writeNumber(timeInMillis);
            return;
        }
        DateFormat dateFormat = this._customFormat;
        if (dateFormat != null) {
            synchronized (dateFormat) {
                r4.writeString(this._customFormat.format(calendar));
            }
            return;
        }
        r5.defaultSerializeDateValue(calendar.getTime(), r4);
    }
}
