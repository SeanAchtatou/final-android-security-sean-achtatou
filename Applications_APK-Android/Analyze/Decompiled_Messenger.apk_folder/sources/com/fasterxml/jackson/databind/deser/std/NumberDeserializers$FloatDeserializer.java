package com.fasterxml.jackson.databind.deser.std;

import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberDeserializers$FloatDeserializer extends NumberDeserializers$PrimitiveOrWrapperDeserializer {
    public static final NumberDeserializers$FloatDeserializer primitiveInstance = new NumberDeserializers$FloatDeserializer(Float.class, Float.valueOf(0.0f));
    private static final long serialVersionUID = 1;
    public static final NumberDeserializers$FloatDeserializer wrapperInstance = new NumberDeserializers$FloatDeserializer(Float.TYPE, null);

    private NumberDeserializers$FloatDeserializer(Class cls, Float f) {
        super(cls, f);
    }

    /* access modifiers changed from: private */
    public Float deserialize(C28271eX r4, C26791c3 r5) {
        float f;
        Object nullValue;
        C182811d currentToken = r4.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            f = r4.getFloatValue();
        } else {
            if (currentToken == C182811d.VALUE_STRING) {
                String trim = r4.getText().trim();
                if (trim.length() == 0) {
                    nullValue = getEmptyValue();
                } else {
                    char charAt = trim.charAt(0);
                    if (charAt != '-') {
                        if (charAt != 'I') {
                            if (charAt == 'N' && "NaN".equals(trim)) {
                                f = Float.NaN;
                            }
                        } else if ("Infinity".equals(trim) || "INF".equals(trim)) {
                            f = Float.POSITIVE_INFINITY;
                        }
                    } else if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                        f = Float.NEGATIVE_INFINITY;
                    }
                    try {
                        return Float.valueOf(Float.parseFloat(trim));
                    } catch (IllegalArgumentException unused) {
                        throw r5.weirdStringException(trim, this._valueClass, "not a valid Float value");
                    }
                }
            } else if (currentToken == C182811d.VALUE_NULL) {
                nullValue = getNullValue();
            } else {
                throw r5.mappingException(this._valueClass, currentToken);
            }
            return (Float) nullValue;
        }
        return Float.valueOf(f);
    }
}
