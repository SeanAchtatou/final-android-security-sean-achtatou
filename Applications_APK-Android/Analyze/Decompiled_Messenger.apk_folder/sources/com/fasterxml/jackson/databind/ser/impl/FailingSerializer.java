package com.fasterxml.jackson.databind.ser.impl;

import X.C11260mU;
import X.C11710np;
import X.C21471AFm;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public final class FailingSerializer extends StdSerializer {
    public final String _msg;

    public FailingSerializer(String str) {
        super(Object.class);
        this._msg = str;
    }

    public void serialize(Object obj, C11710np r4, C11260mU r5) {
        throw new C21471AFm(this._msg);
    }
}
