package com.fasterxml.jackson.databind.annotation;

import X.C422228t;
import X.C422328u;
import X.C422528x;
import com.fasterxml.jackson.databind.JsonDeserializer;

public @interface JsonDeserialize {
    Class as() default C422228t.class;

    Class builder() default C422228t.class;

    Class contentAs() default C422228t.class;

    Class contentConverter() default C422328u.class;

    Class contentUsing() default JsonDeserializer.None.class;

    Class converter() default C422328u.class;

    Class keyAs() default C422228t.class;

    Class keyUsing() default C422528x.class;

    Class using() default JsonDeserializer.None.class;
}
