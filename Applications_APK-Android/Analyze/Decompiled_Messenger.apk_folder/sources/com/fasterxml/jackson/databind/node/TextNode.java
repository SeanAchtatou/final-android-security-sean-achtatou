package com.fasterxml.jackson.databind.node;

import X.AnonymousClass08S;
import X.AnonymousClass11z;
import X.C10350jx;
import X.C11260mU;
import X.C11710np;
import X.C11980oL;
import X.C14610tg;
import X.C182811d;
import X.C29491gV;
import X.C37561vs;
import X.C64443Bu;
import io.card.payment.BuildConfig;

public final class TextNode extends C14610tg {
    public static final TextNode EMPTY_STRING_NODE = new TextNode(BuildConfig.FLAVOR);
    public final String _value;

    public void _reportInvalidBase64(C10350jx r6, char c, int i, String str) {
        String A0P;
        if (c <= ' ') {
            A0P = AnonymousClass08S.A0R("Illegal white space character (code 0x", Integer.toHexString(c), ") as character #", i + 1, " of 4-char base64 unit: can only used between units");
        } else {
            char c2 = r6._paddingChar;
            boolean z = false;
            if (c == c2) {
                z = true;
            }
            if (z) {
                A0P = "Unexpected padding character ('" + c2 + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
            } else if (!Character.isDefined(c) || Character.isISOControl(c)) {
                A0P = AnonymousClass08S.A0P("Illegal character (code 0x", Integer.toHexString(c), ") in base64 content");
            } else {
                A0P = "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
            }
        }
        if (str != null) {
            A0P = AnonymousClass08S.A0P(A0P, ": ", str);
        }
        throw new C37561vs(A0P, C64443Bu.NA);
    }

    public boolean asBoolean(boolean z) {
        String str = this._value;
        if (str == null || !"true".equals(str.trim())) {
            return z;
        }
        return true;
    }

    public double asDouble(double d) {
        String str = this._value;
        if (str == null) {
            return d;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return d;
        }
        try {
            return C29491gV.parseDouble(trim);
        } catch (NumberFormatException unused) {
            return d;
        }
    }

    public int asInt(int i) {
        return C29491gV.parseAsInt(this._value, i);
    }

    public long asLong(long j) {
        return C29491gV.parseAsLong(this._value, j);
    }

    public String asText() {
        return this._value;
    }

    public C182811d asToken() {
        return C182811d.VALUE_STRING;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        r5 = r11 + 1;
        r4 = r8.charAt(r11);
        r3 = r9.decodeBase64Char(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        if (r3 >= 0) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        _reportInvalidBase64(r9, r4, 1, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003d, code lost:
        r11 = (r10 << 6) | r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r5 < r6) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0044, code lost:
        if (r9._usesPadding != false) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        r7.append(r11 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        return r7.toByteArray();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0050, code lost:
        r10 = r5 + 1;
        r3 = r8.charAt(r5);
        r1 = r9.decodeBase64Char(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005c, code lost:
        if (r1 >= 0) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005e, code lost:
        if (r1 == -2) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0060, code lost:
        _reportInvalidBase64(r9, r3, 2, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0065, code lost:
        if (r10 >= r6) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0067, code lost:
        r4 = r10 + 1;
        r3 = r8.charAt(r10);
        r2 = r9._paddingChar;
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0070, code lost:
        if (r3 != r2) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0072, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0073, code lost:
        if (r0 != false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0075, code lost:
        _reportInvalidBase64(r9, r3, 3, X.AnonymousClass08S.A07("expected padding character '", r2, "'"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0080, code lost:
        r7.append(r11 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0086, code lost:
        r5 = (r11 << 6) | r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0089, code lost:
        if (r10 < r6) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008d, code lost:
        if (r9._usesPadding != false) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008f, code lost:
        r7.appendTwoBytes(r5 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0095, code lost:
        r4 = r10 + 1;
        r3 = r8.charAt(r10);
        r1 = r9.decodeBase64Char(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009f, code lost:
        if (r1 >= 0) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a1, code lost:
        if (r1 == -2) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a3, code lost:
        _reportInvalidBase64(r9, r3, 3, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a8, code lost:
        r7.appendTwoBytes(r5 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00af, code lost:
        r7.appendThreeBytes((r5 << 6) | r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001f, code lost:
        r10 = r9.decodeBase64Char(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0023, code lost:
        if (r10 >= 0) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
        _reportInvalidBase64(r9, r3, 0, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002a, code lost:
        if (r11 >= r6) goto L_0x00ba;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] binaryValue() {
        /*
            r12 = this;
            X.0jx r9 = X.C10340jw.MIME_NO_LINEFEEDS
            X.A21 r7 = new X.A21
            r1 = 100
            r0 = 0
            r7.<init>(r0, r1)
            java.lang.String r8 = r12._value
            int r6 = r8.length()
            r4 = 0
        L_0x0011:
            if (r4 >= r6) goto L_0x004b
        L_0x0013:
            int r11 = r4 + 1
            char r3 = r8.charAt(r4)
            if (r11 >= r6) goto L_0x004b
            r0 = 32
            if (r3 <= r0) goto L_0x00b7
            int r10 = r9.decodeBase64Char(r3)
            if (r10 >= 0) goto L_0x002a
            r1 = 0
            r0 = 0
            r12._reportInvalidBase64(r9, r3, r1, r0)
        L_0x002a:
            if (r11 >= r6) goto L_0x00ba
            int r5 = r11 + 1
            char r4 = r8.charAt(r11)
            int r3 = r9.decodeBase64Char(r4)
            if (r3 >= 0) goto L_0x003d
            r1 = 1
            r0 = 0
            r12._reportInvalidBase64(r9, r4, r1, r0)
        L_0x003d:
            int r11 = r10 << 6
            r11 = r11 | r3
            if (r5 < r6) goto L_0x0050
            boolean r0 = r9._usesPadding
            if (r0 != 0) goto L_0x00ba
            int r0 = r11 >> 4
            r7.append(r0)
        L_0x004b:
            byte[] r0 = r7.toByteArray()
            return r0
        L_0x0050:
            int r10 = r5 + 1
            char r3 = r8.charAt(r5)
            int r1 = r9.decodeBase64Char(r3)
            r5 = 3
            r0 = -2
            if (r1 >= 0) goto L_0x0086
            if (r1 == r0) goto L_0x0065
            r1 = 2
            r0 = 0
            r12._reportInvalidBase64(r9, r3, r1, r0)
        L_0x0065:
            if (r10 >= r6) goto L_0x00ba
            int r4 = r10 + 1
            char r3 = r8.charAt(r10)
            char r2 = r9._paddingChar
            r0 = 0
            if (r3 != r2) goto L_0x0073
            r0 = 1
        L_0x0073:
            if (r0 != 0) goto L_0x0080
            java.lang.String r1 = "expected padding character '"
            java.lang.String r0 = "'"
            java.lang.String r0 = X.AnonymousClass08S.A07(r1, r2, r0)
            r12._reportInvalidBase64(r9, r3, r5, r0)
        L_0x0080:
            int r0 = r11 >> 4
            r7.append(r0)
            goto L_0x0011
        L_0x0086:
            int r5 = r11 << 6
            r5 = r5 | r1
            if (r10 < r6) goto L_0x0095
            boolean r0 = r9._usesPadding
            if (r0 != 0) goto L_0x00ba
            int r0 = r5 >> 2
            r7.appendTwoBytes(r0)
            goto L_0x004b
        L_0x0095:
            int r4 = r10 + 1
            char r3 = r8.charAt(r10)
            int r1 = r9.decodeBase64Char(r3)
            if (r1 >= 0) goto L_0x00af
            if (r1 == r0) goto L_0x00a8
            r1 = 3
            r0 = 0
            r12._reportInvalidBase64(r9, r3, r1, r0)
        L_0x00a8:
            int r0 = r5 >> 2
            r7.appendTwoBytes(r0)
            goto L_0x0011
        L_0x00af:
            int r0 = r5 << 6
            r0 = r0 | r1
            r7.appendThreeBytes(r0)
            goto L_0x0011
        L_0x00b7:
            r4 = r11
            goto L_0x0013
        L_0x00ba:
            X.1vs r2 = new X.1vs
            X.3Bu r1 = X.C64443Bu.NA
            java.lang.String r0 = "Unexpected end-of-String when base64 content"
            r2.<init>(r0, r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.node.TextNode.binaryValue():byte[]");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return ((TextNode) obj)._value.equals(this._value);
    }

    public C11980oL getNodeType() {
        return C11980oL.STRING;
    }

    public int hashCode() {
        return this._value.hashCode();
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        String str = this._value;
        if (str == null) {
            r2.writeNull();
        } else {
            r2.writeString(str);
        }
    }

    public String textValue() {
        return this._value;
    }

    public String toString() {
        String str = this._value;
        int length = str.length();
        StringBuilder sb = new StringBuilder(length + 2 + (length >> 4));
        sb.append('\"');
        AnonymousClass11z.appendQuoted(sb, str);
        sb.append('\"');
        return sb.toString();
    }

    public TextNode(String str) {
        this._value = str;
    }
}
