package com.fasterxml.jackson.databind.node;

import X.C11260mU;
import X.C11710np;
import X.C11980oL;
import X.C182811d;
import X.C185013u;
import X.C185113v;
import X.C29651gl;
import X.CY1;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ArrayNode extends C185013u {
    public final List _children = new ArrayList();

    public JsonNode get(String str) {
        return null;
    }

    public C182811d asToken() {
        return C182811d.START_ARRAY;
    }

    public /* bridge */ /* synthetic */ JsonNode deepCopy() {
        ArrayNode arrayNode = new ArrayNode(this._nodeFactory);
        for (JsonNode deepCopy : this._children) {
            arrayNode._children.add(deepCopy.deepCopy());
        }
        return arrayNode;
    }

    public Iterator elements() {
        return this._children.iterator();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this._children.equals(((ArrayNode) obj)._children);
    }

    public JsonNode findValue(String str) {
        for (JsonNode findValue : this._children) {
            JsonNode findValue2 = findValue.findValue(str);
            if (findValue2 != null) {
                return findValue2;
            }
        }
        return null;
    }

    public C11980oL getNodeType() {
        return C11980oL.ARRAY;
    }

    public int hashCode() {
        return this._children.hashCode();
    }

    public int size() {
        return this._children.size();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 16);
        sb.append('[');
        int size = this._children.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append(',');
            }
            sb.append(((JsonNode) this._children.get(i)).toString());
        }
        sb.append(']');
        return sb.toString();
    }

    public ArrayNode(JsonNodeFactory jsonNodeFactory) {
        super(jsonNodeFactory);
    }

    public ArrayNode addNull() {
        this._children.add(nullNode());
        return this;
    }

    public void serialize(C11710np r3, C11260mU r4) {
        r3.writeStartArray();
        for (JsonNode jsonNode : this._children) {
            ((C185113v) jsonNode).serialize(r3, r4);
        }
        r3.writeEndArray();
    }

    public void serializeWithType(C11710np r3, C11260mU r4, CY1 cy1) {
        cy1.writeTypePrefixForArray(this, r3);
        for (JsonNode jsonNode : this._children) {
            ((C185113v) jsonNode).serialize(r3, r4);
        }
        cy1.writeTypeSuffixForArray(this, r3);
    }

    public JsonNode path(String str) {
        return C29651gl.instance;
    }

    public ArrayNode add(JsonNode jsonNode) {
        if (jsonNode == null) {
            jsonNode = nullNode();
        }
        this._children.add(jsonNode);
        return this;
    }

    public ArrayNode add(String str) {
        if (str == null) {
            addNull();
            return this;
        }
        this._children.add(this._nodeFactory.textNode(str));
        return this;
    }

    public JsonNode get(int i) {
        if (i < 0 || i >= this._children.size()) {
            return null;
        }
        return (JsonNode) this._children.get(i);
    }
}
