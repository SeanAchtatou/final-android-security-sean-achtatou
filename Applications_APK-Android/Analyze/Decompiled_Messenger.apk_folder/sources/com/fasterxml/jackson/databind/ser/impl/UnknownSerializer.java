package com.fasterxml.jackson.databind.ser.impl;

import X.AnonymousClass08S;
import X.C10480kE;
import X.C11260mU;
import X.C11710np;
import X.C37701w6;
import X.CY1;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class UnknownSerializer extends StdSerializer {
    public UnknownSerializer() {
        super(Object.class);
    }

    public void serialize(Object obj, C11710np r6, C11260mU r7) {
        if (r7.isEnabled(C10480kE.FAIL_ON_EMPTY_BEANS)) {
            throw new C37701w6(AnonymousClass08S.A0P("No serializer found for class ", obj.getClass().getName(), " and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) )"));
        }
        r6.writeStartObject();
        r6.writeEndObject();
    }

    public final void serializeWithType(Object obj, C11710np r6, C11260mU r7, CY1 cy1) {
        if (r7.isEnabled(C10480kE.FAIL_ON_EMPTY_BEANS)) {
            throw new C37701w6(AnonymousClass08S.A0P("No serializer found for class ", obj.getClass().getName(), " and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) )"));
        }
        cy1.writeTypePrefixForObject(obj, r6);
        cy1.writeTypeSuffixForObject(obj, r6);
    }
}
