package com.fasterxml.jackson.databind.deser.std;

import X.C11700no;
import X.C26791c3;
import X.C28271eX;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public class JacksonDeserializers$TokenBufferDeserializer extends StdScalarDeserializer {
    public static final JacksonDeserializers$TokenBufferDeserializer instance = new JacksonDeserializers$TokenBufferDeserializer();

    public JacksonDeserializers$TokenBufferDeserializer() {
        super(C11700no.class);
    }

    private static C11700no deserialize(C28271eX r2, C26791c3 r3) {
        C11700no r1 = new C11700no(r2.getCodec());
        r1.copyCurrentStructure(r2);
        return r1;
    }
}
