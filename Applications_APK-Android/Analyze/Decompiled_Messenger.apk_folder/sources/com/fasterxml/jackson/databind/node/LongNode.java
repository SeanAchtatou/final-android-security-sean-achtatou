package com.fasterxml.jackson.databind.node;

import X.C11260mU;
import X.C11710np;
import X.C14680to;
import X.C182811d;
import X.C29501gW;
import X.C48502aX;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class LongNode extends C14680to {
    public final long _value;

    public boolean equals(Object obj) {
        if (obj != this) {
            return obj != null && obj.getClass() == getClass() && ((LongNode) obj)._value == this._value;
        }
        return true;
    }

    public boolean isLong() {
        return true;
    }

    public boolean asBoolean(boolean z) {
        if (this._value != 0) {
            return true;
        }
        return false;
    }

    public String asText() {
        long j = this._value;
        if (j > 2147483647L || j < -2147483648L) {
            return Long.toString(j);
        }
        return C48502aX.toString((int) j);
    }

    public C182811d asToken() {
        return C182811d.VALUE_NUMBER_INT;
    }

    public BigInteger bigIntegerValue() {
        return BigInteger.valueOf(this._value);
    }

    public boolean canConvertToInt() {
        long j = this._value;
        if (j < -2147483648L || j > 2147483647L) {
            return false;
        }
        return true;
    }

    public BigDecimal decimalValue() {
        return BigDecimal.valueOf(this._value);
    }

    public double doubleValue() {
        return (double) this._value;
    }

    public float floatValue() {
        return (float) this._value;
    }

    public int hashCode() {
        long j = this._value;
        return ((int) j) ^ ((int) (j >> 32));
    }

    public int intValue() {
        return (int) this._value;
    }

    public long longValue() {
        return this._value;
    }

    public C29501gW numberType() {
        return C29501gW.LONG;
    }

    public Number numberValue() {
        return Long.valueOf(this._value);
    }

    public final void serialize(C11710np r3, C11260mU r4) {
        r3.writeNumber(this._value);
    }

    public LongNode(long j) {
        this._value = j;
    }
}
