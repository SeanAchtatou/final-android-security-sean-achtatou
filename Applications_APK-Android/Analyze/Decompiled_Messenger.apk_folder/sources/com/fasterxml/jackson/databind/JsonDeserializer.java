package com.fasterxml.jackson.databind;

import X.AnonymousClass08S;
import X.C25133CaS;
import X.C26791c3;
import X.C28271eX;
import X.C64433Bp;
import java.util.Collection;

public abstract class JsonDeserializer {
    public abstract Object deserialize(C28271eX r1, C26791c3 r2);

    public Collection getKnownPropertyNames() {
        return null;
    }

    public Object getNullValue() {
        return null;
    }

    public boolean isCachable() {
        return false;
    }

    public JsonDeserializer unwrappingDeserializer(C25133CaS caS) {
        return this;
    }

    public abstract class None extends JsonDeserializer {
        private None() {
        }
    }

    public Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return r4.deserializeTypedFromAny(r2, r3);
    }

    public Object getEmptyValue() {
        return getNullValue();
    }

    public Object deserialize(C28271eX r7, C26791c3 r8, Object obj) {
        throw new UnsupportedOperationException(AnonymousClass08S.A0T("Can not update object of type ", obj.getClass().getName(), " (by deserializer of type ", getClass().getName(), ")"));
    }
}
