package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberSerializers$LongSerializer extends StdScalarSerializer {
    public static final NumberSerializers$LongSerializer instance = new NumberSerializers$LongSerializer();

    public NumberSerializers$LongSerializer() {
        super(Long.class);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
        r4.writeNumber(((Long) obj).longValue());
    }
}
