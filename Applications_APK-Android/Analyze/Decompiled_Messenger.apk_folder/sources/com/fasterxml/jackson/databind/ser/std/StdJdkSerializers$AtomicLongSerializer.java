package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import java.util.concurrent.atomic.AtomicLong;

public final class StdJdkSerializers$AtomicLongSerializer extends StdScalarSerializer {
    public StdJdkSerializers$AtomicLongSerializer() {
        super(AtomicLong.class, false);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
        r4.writeNumber(((AtomicLong) obj).get());
    }
}
