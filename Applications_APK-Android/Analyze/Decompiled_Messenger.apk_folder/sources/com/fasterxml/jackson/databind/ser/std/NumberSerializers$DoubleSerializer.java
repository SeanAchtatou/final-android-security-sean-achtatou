package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberSerializers$DoubleSerializer extends NonTypedScalarSerializerBase {
    public static final NumberSerializers$DoubleSerializer instance = new NumberSerializers$DoubleSerializer();

    public NumberSerializers$DoubleSerializer() {
        super(Double.class);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
        r4.writeNumber(((Double) obj).doubleValue());
    }
}
