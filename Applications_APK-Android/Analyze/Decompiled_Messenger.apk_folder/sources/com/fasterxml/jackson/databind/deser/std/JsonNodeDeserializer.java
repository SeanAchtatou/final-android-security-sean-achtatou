package com.fasterxml.jackson.databind.deser.std;

import X.C182811d;
import X.C183211w;
import X.C26791c3;
import X.C28271eX;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonNodeDeserializer extends BaseNodeDeserializer {
    public static final JsonNodeDeserializer instance = new JsonNodeDeserializer();

    /* access modifiers changed from: private */
    public JsonNode deserialize(C28271eX r3, C26791c3 r4) {
        int i = C183211w.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r3.getCurrentToken().ordinal()];
        if (i == 1) {
            return deserializeObject(r3, r4, r4._config._nodeFactory);
        }
        if (i != 2) {
            return deserializeAny(r3, r4, r4._config._nodeFactory);
        }
        return deserializeArray(r3, r4, r4._config._nodeFactory);
    }

    public final class ObjectDeserializer extends BaseNodeDeserializer {
        public static final ObjectDeserializer _instance = new ObjectDeserializer();
        private static final long serialVersionUID = 1;

        /* access modifiers changed from: private */
        public ObjectNode deserialize(C28271eX r3, C26791c3 r4) {
            JsonNodeFactory jsonNodeFactory;
            if (r3.getCurrentToken() == C182811d.START_OBJECT) {
                r3.nextToken();
                jsonNodeFactory = r4._config._nodeFactory;
            } else if (r3.getCurrentToken() == C182811d.FIELD_NAME) {
                jsonNodeFactory = r4._config._nodeFactory;
            } else {
                throw r4.mappingException(ObjectNode.class);
            }
            return deserializeObject(r3, r4, jsonNodeFactory);
        }
    }

    public final class ArrayDeserializer extends BaseNodeDeserializer {
        public static final ArrayDeserializer _instance = new ArrayDeserializer();
        private static final long serialVersionUID = 1;

        /* access modifiers changed from: private */
        public ArrayNode deserialize(C28271eX r2, C26791c3 r3) {
            if (r2.isExpectedStartArrayToken()) {
                return deserializeArray(r2, r3, r3._config._nodeFactory);
            }
            throw r3.mappingException(ArrayNode.class);
        }
    }
}
