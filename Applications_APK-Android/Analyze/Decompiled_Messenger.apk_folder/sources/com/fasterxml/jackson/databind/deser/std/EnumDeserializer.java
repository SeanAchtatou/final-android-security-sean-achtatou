package com.fasterxml.jackson.databind.deser.std;

import X.AnonymousClass08S;
import X.AnonymousClass1c1;
import X.C182811d;
import X.C21472AFn;
import X.C26791c3;
import X.C28271eX;
import X.C29081fq;
import X.C29141fw;
import X.C29171fz;
import java.lang.reflect.Method;

public class EnumDeserializer extends StdScalarDeserializer {
    private static final long serialVersionUID = -5893263645879532318L;
    public final C29171fz _resolver;

    public class FactoryBasedDeserializer extends StdScalarDeserializer {
        private static final long serialVersionUID = -7775129435872564122L;
        public final Class _enumClass;
        public final Method _factory;
        public final Class _inputType;

        public FactoryBasedDeserializer(Class cls, C29141fw r3, Class cls2) {
            super(Enum.class);
            this._enumClass = cls;
            this._factory = r3._method;
            this._inputType = cls2;
        }

        public Object deserialize(C28271eX r4, C26791c3 r5) {
            Object obj;
            Class<Long> cls = this._inputType;
            if (cls == null) {
                obj = r4.getText();
            } else if (cls == Integer.class) {
                obj = Integer.valueOf(r4.getValueAsInt());
            } else if (cls == Long.class) {
                obj = Long.valueOf(r4.getValueAsLong());
            } else {
                throw r5.mappingException(this._enumClass);
            }
            try {
                return this._factory.invoke(this._enumClass, obj);
            } catch (Exception e) {
                C29081fq.unwrapAndThrowAsIAE(e);
                return null;
            }
        }
    }

    public boolean isCachable() {
        return true;
    }

    public EnumDeserializer(C29171fz r2) {
        super(Enum.class);
        this._resolver = r2;
    }

    /* access modifiers changed from: private */
    public Enum deserialize(C28271eX r13, C26791c3 r14) {
        Enum enumR;
        String str;
        C182811d currentToken = r13.getCurrentToken();
        if (currentToken == C182811d.VALUE_STRING || currentToken == C182811d.FIELD_NAME) {
            String text = r13.getText();
            enumR = (Enum) this._resolver._enumsById.get(text);
            if (enumR == null) {
                if (r14.isEnabled(AnonymousClass1c1.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT) && (text.length() == 0 || text.trim().length() == 0)) {
                    return null;
                }
                if (!r14.isEnabled(AnonymousClass1c1.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                    throw r14.weirdStringException(text, this._resolver._enumClass, "value not one of declared Enum instance names");
                }
            }
        } else if (currentToken != C182811d.VALUE_NUMBER_INT) {
            throw r14.mappingException(this._resolver._enumClass);
        } else if (!r14.isEnabled(AnonymousClass1c1.FAIL_ON_NUMBERS_FOR_ENUMS)) {
            int intValue = r13.getIntValue();
            C29171fz r3 = this._resolver;
            if (intValue >= 0) {
                Enum[] enumArr = r3._enums;
                if (intValue < enumArr.length) {
                    enumR = enumArr[intValue];
                    if (enumR == null && !r14.isEnabled(AnonymousClass1c1.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                        Class cls = r3._enumClass;
                        String A0A = AnonymousClass08S.A0A("index value outside legal index range [0..", r3._enums.length - 1, "]");
                        C28271eX r4 = r14._parser;
                        String name = cls.getName();
                        str = C26791c3._desc(r4.getText());
                        throw new C21472AFn(AnonymousClass08S.A0U("Can not construct instance of ", name, " from number value (", str, "): ", A0A), r4.getTokenLocation(), null, cls);
                    }
                }
            }
            enumR = null;
            Class cls2 = r3._enumClass;
            String A0A2 = AnonymousClass08S.A0A("index value outside legal index range [0..", r3._enums.length - 1, "]");
            C28271eX r42 = r14._parser;
            String name2 = cls2.getName();
            try {
                str = C26791c3._desc(r42.getText());
            } catch (Exception unused) {
                str = "[N/A]";
            }
            throw new C21472AFn(AnonymousClass08S.A0U("Can not construct instance of ", name2, " from number value (", str, "): ", A0A2), r42.getTokenLocation(), null, cls2);
        } else {
            throw r14.mappingException("Not allowed to deserialize Enum value out of JSON number (disable DeserializationConfig.DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS to allow)");
        }
        return enumR;
    }
}
