package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import X.CY1;

public abstract class NonTypedScalarSerializerBase extends StdScalarSerializer {
    public NonTypedScalarSerializerBase(Class cls) {
        super(cls);
    }

    public final void serializeWithType(Object obj, C11710np r2, C11260mU r3, CY1 cy1) {
        serialize(obj, r2, r3);
    }
}
