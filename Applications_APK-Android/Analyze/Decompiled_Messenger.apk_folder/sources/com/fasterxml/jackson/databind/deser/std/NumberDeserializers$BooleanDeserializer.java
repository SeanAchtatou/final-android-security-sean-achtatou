package com.fasterxml.jackson.databind.deser.std;

import X.C26791c3;
import X.C28271eX;
import X.C64433Bp;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberDeserializers$BooleanDeserializer extends NumberDeserializers$PrimitiveOrWrapperDeserializer {
    public static final NumberDeserializers$BooleanDeserializer primitiveInstance = new NumberDeserializers$BooleanDeserializer(Boolean.class, Boolean.FALSE);
    private static final long serialVersionUID = 1;
    public static final NumberDeserializers$BooleanDeserializer wrapperInstance = new NumberDeserializers$BooleanDeserializer(Boolean.TYPE, null);

    private NumberDeserializers$BooleanDeserializer(Class cls, Boolean bool) {
        super(cls, bool);
    }

    public /* bridge */ /* synthetic */ Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return _parseBoolean(r2, r3);
    }

    private Boolean deserialize(C28271eX r2, C26791c3 r3) {
        return _parseBoolean(r2, r3);
    }
}
