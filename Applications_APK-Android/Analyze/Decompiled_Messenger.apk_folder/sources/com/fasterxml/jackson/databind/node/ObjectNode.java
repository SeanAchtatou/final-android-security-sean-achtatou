package com.fasterxml.jackson.databind.node;

import X.AnonymousClass11z;
import X.C11260mU;
import X.C11710np;
import X.C11980oL;
import X.C182811d;
import X.C185013u;
import X.C185113v;
import X.C28941fc;
import X.C29651gl;
import X.CY1;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ObjectNode extends C185013u {
    public final Map _children = new LinkedHashMap();

    public JsonNode get(int i) {
        return null;
    }

    public C182811d asToken() {
        return C182811d.START_OBJECT;
    }

    public Iterator elements() {
        return this._children.values().iterator();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this._children.equals(((ObjectNode) obj)._children);
    }

    public Iterator fieldNames() {
        return this._children.keySet().iterator();
    }

    public Iterator fields() {
        return this._children.entrySet().iterator();
    }

    public JsonNode findValue(String str) {
        for (Map.Entry entry : this._children.entrySet()) {
            if (str.equals(entry.getKey())) {
                return (JsonNode) entry.getValue();
            }
            JsonNode findValue = ((JsonNode) entry.getValue()).findValue(str);
            if (findValue != null) {
                return findValue;
            }
        }
        return null;
    }

    public C11980oL getNodeType() {
        return C11980oL.OBJECT;
    }

    public int hashCode() {
        return this._children.hashCode();
    }

    public JsonNode path(String str) {
        JsonNode jsonNode = (JsonNode) this._children.get(str);
        if (jsonNode != null) {
            return jsonNode;
        }
        return C29651gl.instance;
    }

    public ArrayNode putArray(String str) {
        ArrayNode arrayNode = new ArrayNode(this._nodeFactory);
        this._children.put(str, arrayNode);
        return arrayNode;
    }

    public ObjectNode putObject(String str) {
        ObjectNode objectNode = new ObjectNode(this._nodeFactory);
        this._children.put(str, objectNode);
        return objectNode;
    }

    public int size() {
        return this._children.size();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 32);
        sb.append("{");
        int i = 0;
        for (Map.Entry entry : this._children.entrySet()) {
            if (i > 0) {
                sb.append(",");
            }
            i++;
            sb.append('\"');
            AnonymousClass11z.appendQuoted(sb, (String) entry.getKey());
            sb.append('\"');
            sb.append(':');
            sb.append(((JsonNode) entry.getValue()).toString());
        }
        sb.append("}");
        return sb.toString();
    }

    public ObjectNode(JsonNodeFactory jsonNodeFactory) {
        super(jsonNodeFactory);
    }

    public void serialize(C11710np r4, C11260mU r5) {
        r4.writeStartObject();
        for (Map.Entry entry : this._children.entrySet()) {
            r4.writeFieldName((String) entry.getKey());
            ((C185113v) entry.getValue()).serialize(r4, r5);
        }
        r4.writeEndObject();
    }

    public void serializeWithType(C11710np r4, C11260mU r5, CY1 cy1) {
        cy1.writeTypePrefixForObject(this, r4);
        for (Map.Entry entry : this._children.entrySet()) {
            r4.writeFieldName((String) entry.getKey());
            ((C185113v) entry.getValue()).serialize(r4, r5);
        }
        cy1.writeTypeSuffixForObject(this, r4);
    }

    public ObjectNode deepCopy() {
        ObjectNode objectNode = new ObjectNode(this._nodeFactory);
        for (Map.Entry entry : this._children.entrySet()) {
            objectNode._children.put(entry.getKey(), ((JsonNode) entry.getValue()).deepCopy());
        }
        return objectNode;
    }

    public JsonNode get(String str) {
        return (JsonNode) this._children.get(str);
    }

    public JsonNode put(String str, JsonNode jsonNode) {
        if (jsonNode == null) {
            jsonNode = nullNode();
        }
        return (JsonNode) this._children.put(str, jsonNode);
    }

    public ObjectNode put(String str, double d) {
        this._children.put(str, new DoubleNode(d));
        return this;
    }

    public ObjectNode put(String str, float f) {
        this._children.put(str, new C28941fc(f));
        return this;
    }

    public ObjectNode put(String str, int i) {
        this._children.put(str, numberNode(i));
        return this;
    }

    public ObjectNode put(String str, long j) {
        this._children.put(str, new LongNode(j));
        return this;
    }

    public ObjectNode put(String str, Boolean bool) {
        if (bool == null) {
            this._children.put(str, nullNode());
            return this;
        }
        this._children.put(str, booleanNode(bool.booleanValue()));
        return this;
    }

    public ObjectNode put(String str, Double d) {
        if (d == null) {
            this._children.put(str, nullNode());
            return this;
        }
        this._children.put(str, new DoubleNode(d.doubleValue()));
        return this;
    }

    public ObjectNode put(String str, Float f) {
        if (f == null) {
            this._children.put(str, nullNode());
            return this;
        }
        this._children.put(str, new C28941fc(f.floatValue()));
        return this;
    }

    public ObjectNode put(String str, Integer num) {
        if (num == null) {
            this._children.put(str, nullNode());
            return this;
        }
        this._children.put(str, numberNode(num.intValue()));
        return this;
    }

    public ObjectNode put(String str, Long l) {
        if (l == null) {
            this._children.put(str, nullNode());
            return this;
        }
        this._children.put(str, new LongNode(l.longValue()));
        return this;
    }

    public ObjectNode put(String str, String str2) {
        if (str2 == null) {
            this._children.put(str, nullNode());
            return this;
        }
        this._children.put(str, this._nodeFactory.textNode(str2));
        return this;
    }

    public ObjectNode put(String str, boolean z) {
        this._children.put(str, booleanNode(z));
        return this;
    }
}
