package com.fasterxml.jackson.databind;

import X.AnonymousClass08S;
import X.C11260mU;
import X.C11280mW;
import X.C11710np;
import X.C25133CaS;
import X.CY1;

public abstract class JsonSerializer implements C11280mW {

    public abstract class None extends JsonSerializer {
    }

    public Class handledType() {
        return null;
    }

    public boolean isEmpty(Object obj) {
        return obj == null;
    }

    public boolean isUnwrappingSerializer() {
        return false;
    }

    public abstract void serialize(Object obj, C11710np r2, C11260mU r3);

    public JsonSerializer unwrappingSerializer(C25133CaS caS) {
        return this;
    }

    public boolean usesObjectId() {
        return false;
    }

    public void serializeWithType(Object obj, C11710np r5, C11260mU r6, CY1 cy1) {
        Class<?> handledType = handledType();
        if (handledType == null) {
            handledType = obj.getClass();
        }
        throw new UnsupportedOperationException(AnonymousClass08S.A0J("Type id handling not implemented for type ", handledType.getName()));
    }
}
