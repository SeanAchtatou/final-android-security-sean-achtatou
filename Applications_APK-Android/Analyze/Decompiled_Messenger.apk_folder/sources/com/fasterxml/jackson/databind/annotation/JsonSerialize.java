package com.fasterxml.jackson.databind.annotation;

import X.AnonymousClass290;
import X.C422228t;
import X.C422328u;
import X.C422728z;
import com.fasterxml.jackson.databind.JsonSerializer;

public @interface JsonSerialize {
    Class as() default C422228t.class;

    Class contentAs() default C422228t.class;

    Class contentConverter() default C422328u.class;

    Class contentUsing() default JsonSerializer.None.class;

    Class converter() default C422328u.class;

    C422728z include() default C422728z.ALWAYS;

    Class keyAs() default C422228t.class;

    Class keyUsing() default JsonSerializer.None.class;

    AnonymousClass290 typing() default AnonymousClass290.DYNAMIC;

    Class using() default JsonSerializer.None.class;
}
