package com.fasterxml.jackson.databind.ser.std;

import X.C10480kE;
import X.C11260mU;
import X.C11700no;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.math.BigDecimal;
import java.math.BigInteger;

@JacksonStdImpl
public final class NumberSerializers$NumberSerializer extends StdScalarSerializer {
    public static final NumberSerializers$NumberSerializer instance = new NumberSerializers$NumberSerializer();

    public NumberSerializers$NumberSerializer() {
        super(Number.class);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
        Number number = (Number) obj;
        if (number instanceof BigDecimal) {
            if (!r5.isEnabled(C10480kE.WRITE_BIGDECIMAL_AS_PLAIN) || (r4 instanceof C11700no)) {
                r4.writeNumber((BigDecimal) number);
            } else {
                r4.writeNumber(((BigDecimal) number).toPlainString());
            }
        } else if (number instanceof BigInteger) {
            r4.writeNumber((BigInteger) number);
        } else {
            if (!(number instanceof Integer)) {
                if (number instanceof Long) {
                    r4.writeNumber(number.longValue());
                    return;
                } else if (number instanceof Double) {
                    r4.writeNumber(number.doubleValue());
                    return;
                } else if (number instanceof Float) {
                    r4.writeNumber(number.floatValue());
                    return;
                } else if (!(number instanceof Byte) && !(number instanceof Short)) {
                    r4.writeNumber(number.toString());
                    return;
                }
            }
            r4.writeNumber(number.intValue());
        }
    }
}
