package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11690nn;
import X.C11710np;
import java.text.DateFormat;

public abstract class DateTimeSerializerBase extends StdScalarSerializer implements C11690nn {
    public final DateFormat _customFormat;
    public final boolean _useTimestamp;

    public abstract long _timestamp(Object obj);

    public abstract void serialize(Object obj, C11710np r2, C11260mU r3);

    public abstract DateTimeSerializerBase withFormat(boolean z, DateFormat dateFormat);

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r2 == X.CX7.NUMBER_FLOAT) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fasterxml.jackson.databind.JsonSerializer createContextual(X.C11260mU r7, X.CX9 r8) {
        /*
            r6 = this;
            if (r8 == 0) goto L_0x007f
            X.0jc r1 = r7.getAnnotationIntrospector()
            X.12m r0 = r8.getMember()
            X.CX6 r5 = r1.findFormat(r0)
            if (r5 == 0) goto L_0x007f
            X.CX7 r2 = r5.shape
            X.CX7 r0 = X.CX7.NUMBER
            if (r2 == r0) goto L_0x001f
            X.CX7 r0 = X.CX7.NUMBER_INT
            if (r2 == r0) goto L_0x001f
            X.CX7 r1 = X.CX7.NUMBER_FLOAT
            r0 = 0
            if (r2 != r1) goto L_0x0020
        L_0x001f:
            r0 = 1
        L_0x0020:
            if (r0 == 0) goto L_0x0029
            r1 = 1
            r0 = 0
            com.fasterxml.jackson.databind.ser.std.DateTimeSerializerBase r0 = r6.withFormat(r1, r0)
            return r0
        L_0x0029:
            java.util.TimeZone r3 = r5.timezone
            java.lang.String r2 = r5.pattern
            int r0 = r2.length()
            r4 = 0
            if (r0 <= 0) goto L_0x0053
            java.util.Locale r0 = r5.locale
            if (r0 != 0) goto L_0x003e
            X.0k8 r0 = r7._config
            X.0jr r0 = r0._base
            java.util.Locale r0 = r0._locale
        L_0x003e:
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            r1.<init>(r2, r0)
            if (r3 != 0) goto L_0x004b
            X.0k8 r0 = r7._config
            X.0jr r0 = r0._base
            java.util.TimeZone r3 = r0._timeZone
        L_0x004b:
            r1.setTimeZone(r3)
            com.fasterxml.jackson.databind.ser.std.DateTimeSerializerBase r0 = r6.withFormat(r4, r1)
            return r0
        L_0x0053:
            if (r3 == 0) goto L_0x007f
            X.0k8 r0 = r7._config
            X.0jr r0 = r0._base
            java.text.DateFormat r2 = r0._dateFormat
            java.lang.Class r1 = r2.getClass()
            java.lang.Class<X.0jv> r0 = X.C10330jv.class
            if (r1 != r0) goto L_0x0075
            java.text.DateFormat r0 = X.C10330jv.DATE_FORMAT_ISO8601
            java.lang.Object r0 = r0.clone()
            java.text.DateFormat r0 = (java.text.DateFormat) r0
            if (r3 == 0) goto L_0x0070
            r0.setTimeZone(r3)
        L_0x0070:
            com.fasterxml.jackson.databind.ser.std.DateTimeSerializerBase r0 = r6.withFormat(r4, r0)
            return r0
        L_0x0075:
            java.lang.Object r0 = r2.clone()
            java.text.DateFormat r0 = (java.text.DateFormat) r0
            r0.setTimeZone(r3)
            goto L_0x0070
        L_0x007f:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.DateTimeSerializerBase.createContextual(X.0mU, X.CX9):com.fasterxml.jackson.databind.JsonSerializer");
    }

    public boolean isEmpty(Object obj) {
        if (obj == null || _timestamp(obj) == 0) {
            return true;
        }
        return false;
    }

    public DateTimeSerializerBase(Class cls, boolean z, DateFormat dateFormat) {
        super(cls);
        this._useTimestamp = z;
        this._customFormat = dateFormat;
    }
}
