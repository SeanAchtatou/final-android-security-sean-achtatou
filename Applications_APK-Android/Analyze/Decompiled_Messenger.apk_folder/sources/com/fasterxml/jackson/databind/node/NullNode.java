package com.fasterxml.jackson.databind.node;

import X.C11260mU;
import X.C11710np;
import X.C11980oL;
import X.C14610tg;
import X.C182811d;

public final class NullNode extends C14610tg {
    public static final NullNode instance = new NullNode();

    public String asText() {
        return "null";
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public C182811d asToken() {
        return C182811d.VALUE_NULL;
    }

    public C11980oL getNodeType() {
        return C11980oL.NULL;
    }

    public final void serialize(C11710np r1, C11260mU r2) {
        r2.defaultSerializeNull(r1);
    }

    private NullNode() {
    }
}
