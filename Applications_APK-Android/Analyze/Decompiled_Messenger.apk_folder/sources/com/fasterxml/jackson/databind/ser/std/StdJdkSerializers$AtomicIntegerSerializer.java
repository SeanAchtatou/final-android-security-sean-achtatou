package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import java.util.concurrent.atomic.AtomicInteger;

public final class StdJdkSerializers$AtomicIntegerSerializer extends StdScalarSerializer {
    public StdJdkSerializers$AtomicIntegerSerializer() {
        super(AtomicInteger.class, false);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeNumber(((AtomicInteger) obj).get());
    }
}
