package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberSerializers$IntegerSerializer extends NonTypedScalarSerializerBase {
    public NumberSerializers$IntegerSerializer() {
        super(Integer.class);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeNumber(((Integer) obj).intValue());
    }
}
