package com.fasterxml.jackson.databind.deser.std;

import X.C26791c3;
import X.C28271eX;
import X.C64433Bp;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberDeserializers$DoubleDeserializer extends NumberDeserializers$PrimitiveOrWrapperDeserializer {
    public static final NumberDeserializers$DoubleDeserializer primitiveInstance = new NumberDeserializers$DoubleDeserializer(Double.class, Double.valueOf(0.0d));
    private static final long serialVersionUID = 1;
    public static final NumberDeserializers$DoubleDeserializer wrapperInstance = new NumberDeserializers$DoubleDeserializer(Double.TYPE, null);

    private NumberDeserializers$DoubleDeserializer(Class cls, Double d) {
        super(cls, d);
    }

    public /* bridge */ /* synthetic */ Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return _parseDouble(r2, r3);
    }

    private Double deserialize(C28271eX r2, C26791c3 r3) {
        return _parseDouble(r2, r3);
    }
}
