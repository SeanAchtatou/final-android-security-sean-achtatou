package com.fasterxml.jackson.databind.ser.std;

import X.AnonymousClass0mX;
import X.C10030jR;
import X.C10140jc;
import X.C11260mU;
import X.C11280mW;
import X.C11710np;
import X.C422428v;
import X.CX9;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

public abstract class StdSerializer extends JsonSerializer implements C11280mW, AnonymousClass0mX {
    public final Class _handledType;

    public abstract void serialize(Object obj, C11710np r2, C11260mU r3);

    public static boolean isDefaultSerializer(JsonSerializer jsonSerializer) {
        if (jsonSerializer == null || jsonSerializer.getClass().getAnnotation(JacksonStdImpl.class) == null) {
            return false;
        }
        return true;
    }

    public Class handledType() {
        return this._handledType;
    }

    public static JsonSerializer findConvertingContentSerializer(C11260mU r3, CX9 cx9, JsonSerializer jsonSerializer) {
        Object findSerializationContentConverter;
        C10140jc annotationIntrospector = r3.getAnnotationIntrospector();
        if (annotationIntrospector == null || cx9 == null || (findSerializationContentConverter = annotationIntrospector.findSerializationContentConverter(cx9.getMember())) == null) {
            return jsonSerializer;
        }
        C422428v converterInstance = r3.converterInstance(cx9.getMember(), findSerializationContentConverter);
        C10030jR outputType = converterInstance.getOutputType(r3.getTypeFactory());
        if (jsonSerializer == null) {
            jsonSerializer = r3.findValueSerializer(outputType, cx9);
        }
        return new StdDelegatingSerializer(converterInstance, outputType, jsonSerializer);
    }

    public StdSerializer(C10030jR r2) {
        this._handledType = r2._class;
    }

    public StdSerializer(Class cls) {
        this._handledType = cls;
    }

    public StdSerializer(Class cls, boolean z) {
        this._handledType = cls;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r0 != false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void wrapAndThrow(X.C11260mU r1, java.lang.Throwable r2, java.lang.Object r3, int r4) {
        /*
        L_0x0000:
            boolean r0 = r2 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x000f
            java.lang.Throwable r0 = r2.getCause()
            if (r0 == 0) goto L_0x000f
            java.lang.Throwable r2 = r2.getCause()
            goto L_0x0000
        L_0x000f:
            boolean r0 = r2 instanceof java.lang.Error
            if (r0 != 0) goto L_0x003f
            if (r1 == 0) goto L_0x001e
            X.0kE r0 = X.C10480kE.WRAP_EXCEPTIONS
            boolean r0 = r1.isEnabled(r0)
            r1 = 0
            if (r0 == 0) goto L_0x001f
        L_0x001e:
            r1 = 1
        L_0x001f:
            boolean r0 = r2 instanceof java.io.IOException
            if (r0 == 0) goto L_0x002c
            if (r1 == 0) goto L_0x0029
            boolean r0 = r2 instanceof X.C37701w6
            if (r0 != 0) goto L_0x0035
        L_0x0029:
            java.io.IOException r2 = (java.io.IOException) r2
            throw r2
        L_0x002c:
            if (r1 != 0) goto L_0x0035
            boolean r0 = r2 instanceof java.lang.RuntimeException
            if (r0 == 0) goto L_0x0035
            java.lang.RuntimeException r2 = (java.lang.RuntimeException) r2
            throw r2
        L_0x0035:
            X.8ap r0 = new X.8ap
            r0.<init>(r3, r4)
            X.1w6 r0 = X.C37701w6.wrapWithPath(r2, r0)
            throw r0
        L_0x003f:
            java.lang.Error r2 = (java.lang.Error) r2
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.StdSerializer.wrapAndThrow(X.0mU, java.lang.Throwable, java.lang.Object, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r0 != false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void wrapAndThrow(X.C11260mU r1, java.lang.Throwable r2, java.lang.Object r3, java.lang.String r4) {
        /*
        L_0x0000:
            boolean r0 = r2 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x000f
            java.lang.Throwable r0 = r2.getCause()
            if (r0 == 0) goto L_0x000f
            java.lang.Throwable r2 = r2.getCause()
            goto L_0x0000
        L_0x000f:
            boolean r0 = r2 instanceof java.lang.Error
            if (r0 != 0) goto L_0x003f
            if (r1 == 0) goto L_0x001e
            X.0kE r0 = X.C10480kE.WRAP_EXCEPTIONS
            boolean r0 = r1.isEnabled(r0)
            r1 = 0
            if (r0 == 0) goto L_0x001f
        L_0x001e:
            r1 = 1
        L_0x001f:
            boolean r0 = r2 instanceof java.io.IOException
            if (r0 == 0) goto L_0x002c
            if (r1 == 0) goto L_0x0029
            boolean r0 = r2 instanceof X.C37701w6
            if (r0 != 0) goto L_0x0035
        L_0x0029:
            java.io.IOException r2 = (java.io.IOException) r2
            throw r2
        L_0x002c:
            if (r1 != 0) goto L_0x0035
            boolean r0 = r2 instanceof java.lang.RuntimeException
            if (r0 == 0) goto L_0x0035
            java.lang.RuntimeException r2 = (java.lang.RuntimeException) r2
            throw r2
        L_0x0035:
            X.8ap r0 = new X.8ap
            r0.<init>(r3, r4)
            X.1w6 r0 = X.C37701w6.wrapWithPath(r2, r0)
            throw r0
        L_0x003f:
            java.lang.Error r2 = (java.lang.Error) r2
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.std.StdSerializer.wrapAndThrow(X.0mU, java.lang.Throwable, java.lang.Object, java.lang.String):void");
    }
}
