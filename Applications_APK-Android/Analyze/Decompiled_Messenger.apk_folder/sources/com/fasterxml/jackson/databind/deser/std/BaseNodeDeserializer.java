package com.fasterxml.jackson.databind.deser.std;

import X.AnonymousClass11x;
import X.AnonymousClass14O;
import X.AnonymousClass1Y3;
import X.AnonymousClass1c1;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C28961fe;
import X.C28971ff;
import X.C28981fg;
import X.C29501gW;
import X.C64433Bp;
import X.CXH;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.math.BigDecimal;
import org.webrtc.audio.WebRtcAudioRecord;

public abstract class BaseNodeDeserializer extends StdDeserializer {
    public BaseNodeDeserializer() {
        super(JsonNode.class);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final JsonNode deserializeAny(C28271eX r4, C26791c3 r5, JsonNodeFactory jsonNodeFactory) {
        switch (AnonymousClass11x.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r4.getCurrentToken().ordinal()]) {
            case 1:
            case 5:
                return deserializeObject(r4, r5, jsonNodeFactory);
            case 2:
                return deserializeArray(r4, r5, jsonNodeFactory);
            case 3:
                return jsonNodeFactory.textNode(r4.getText());
            case 4:
            default:
                throw r5.mappingException(this._valueClass);
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                Object embeddedObject = r4.getEmbeddedObject();
                if (embeddedObject != null) {
                    if (embeddedObject.getClass() != byte[].class) {
                        return new C28981fg(embeddedObject);
                    }
                    byte[] bArr = (byte[]) embeddedObject;
                    if (bArr == null) {
                        return null;
                    }
                    if (bArr.length == 0) {
                        return CXH.EMPTY_BINARY_NODE;
                    }
                    return new CXH(bArr);
                }
                break;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                C29501gW numberType = r4.getNumberType();
                if (numberType == C29501gW.BIG_INTEGER || r5.isEnabled(AnonymousClass1c1.USE_BIG_INTEGER_FOR_INTS)) {
                    return new C28971ff(r4.getBigIntegerValue());
                }
                if (numberType != C29501gW.INT) {
                    return new LongNode(r4.getLongValue());
                }
                int intValue = r4.getIntValue();
                if (intValue > 10 || intValue < -1) {
                    return new AnonymousClass14O(intValue);
                }
                return AnonymousClass14O.CANONICALS[intValue - -1];
            case 8:
                if (r4.getNumberType() != C29501gW.BIG_DECIMAL && !r5.isEnabled(AnonymousClass1c1.USE_BIG_DECIMAL_FOR_FLOATS)) {
                    return new DoubleNode(r4.getDoubleValue());
                }
                BigDecimal decimalValue = r4.getDecimalValue();
                if (jsonNodeFactory._cfgBigDecimalExact) {
                    return new C28961fe(decimalValue);
                }
                if (decimalValue.compareTo(BigDecimal.ZERO) == 0) {
                    return C28961fe.ZERO;
                }
                return new C28961fe(decimalValue.stripTrailingZeros());
            case Process.SIGKILL:
                return BooleanNode.TRUE;
            case AnonymousClass1Y3.A01:
                return BooleanNode.FALSE;
            case AnonymousClass1Y3.A02:
                break;
        }
        return NullNode.instance;
    }

    public final ArrayNode deserializeArray(C28271eX r4, C26791c3 r5, JsonNodeFactory jsonNodeFactory) {
        ArrayNode arrayNode = new ArrayNode(jsonNodeFactory);
        while (true) {
            C182811d nextToken = r4.nextToken();
            if (nextToken != null) {
                int i = AnonymousClass11x.$SwitchMap$com$fasterxml$jackson$core$JsonToken[nextToken.ordinal()];
                if (i == 1) {
                    arrayNode.add(deserializeObject(r4, r5, jsonNodeFactory));
                } else if (i == 2) {
                    arrayNode.add(deserializeArray(r4, r5, jsonNodeFactory));
                } else if (i == 3) {
                    arrayNode.add(jsonNodeFactory.textNode(r4.getText()));
                } else if (i == 4) {
                    return arrayNode;
                } else {
                    arrayNode.add(deserializeAny(r4, r5, jsonNodeFactory));
                }
            } else {
                throw r5.mappingException("Unexpected end-of-input when binding data into ArrayNode");
            }
        }
    }

    public final ObjectNode deserializeObject(C28271eX r5, C26791c3 r6, JsonNodeFactory jsonNodeFactory) {
        Object deserializeObject;
        ObjectNode objectNode = new ObjectNode(jsonNodeFactory);
        C182811d currentToken = r5.getCurrentToken();
        if (currentToken == C182811d.START_OBJECT) {
            currentToken = r5.nextToken();
        }
        while (currentToken == C182811d.FIELD_NAME) {
            String currentName = r5.getCurrentName();
            int i = AnonymousClass11x.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r5.nextToken().ordinal()];
            if (i == 1) {
                deserializeObject = deserializeObject(r5, r6, jsonNodeFactory);
            } else if (i == 2) {
                deserializeObject = deserializeArray(r5, r6, jsonNodeFactory);
            } else if (i != 3) {
                deserializeObject = deserializeAny(r5, r6, jsonNodeFactory);
            } else {
                deserializeObject = jsonNodeFactory.textNode(r5.getText());
            }
            if (deserializeObject == null) {
                deserializeObject = objectNode.nullNode();
            }
            objectNode._children.put(currentName, deserializeObject);
            currentToken = r5.nextToken();
        }
        return objectNode;
    }

    public /* bridge */ /* synthetic */ Object getNullValue() {
        return NullNode.instance;
    }

    public Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return r4.deserializeTypedFromAny(r2, r3);
    }
}
