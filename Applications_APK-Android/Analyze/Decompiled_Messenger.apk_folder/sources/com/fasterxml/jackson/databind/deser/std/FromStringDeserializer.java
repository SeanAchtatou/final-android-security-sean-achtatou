package com.fasterxml.jackson.databind.deser.std;

import X.AnonymousClass08S;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;

public abstract class FromStringDeserializer extends StdScalarDeserializer {
    private static final long serialVersionUID = 1;

    public abstract Object _deserialize(String str, C26791c3 r2);

    public Object _deserializeEmbedded(Object obj, C26791c3 r6) {
        throw r6.mappingException(AnonymousClass08S.A0S("Don't know how to convert embedded Object of type ", obj.getClass().getName(), " into ", this._valueClass.getName()));
    }

    public final Object deserialize(C28271eX r5, C26791c3 r6) {
        String valueAsString = r5.getValueAsString();
        if (valueAsString != null) {
            if (valueAsString.length() != 0) {
                String trim = valueAsString.trim();
                if (trim.length() != 0) {
                    try {
                        Object _deserialize = _deserialize(trim, r6);
                        if (_deserialize != null) {
                            return _deserialize;
                        }
                    } catch (IllegalArgumentException unused) {
                    }
                    throw r6.weirdStringException(trim, this._valueClass, "not a valid textual representation");
                }
            }
        } else if (r5.getCurrentToken() == C182811d.VALUE_EMBEDDED_OBJECT) {
            Object embeddedObject = r5.getEmbeddedObject();
            if (embeddedObject != null) {
                if (this._valueClass.isAssignableFrom(embeddedObject.getClass())) {
                    return embeddedObject;
                }
                return _deserializeEmbedded(embeddedObject, r6);
            }
        } else {
            throw r6.mappingException(this._valueClass);
        }
        return null;
    }

    public FromStringDeserializer(Class cls) {
        super(cls);
    }
}
