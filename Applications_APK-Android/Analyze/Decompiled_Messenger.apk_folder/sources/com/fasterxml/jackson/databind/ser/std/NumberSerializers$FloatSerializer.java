package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberSerializers$FloatSerializer extends StdScalarSerializer {
    public static final NumberSerializers$FloatSerializer instance = new NumberSerializers$FloatSerializer();

    public NumberSerializers$FloatSerializer() {
        super(Float.class);
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r3, C11260mU r4) {
        r3.writeNumber(((Float) obj).floatValue());
    }
}
