package com.fasterxml.jackson.databind.deser.std;

import X.AnonymousClass1Y3;
import X.AnonymousClass1c1;
import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C29641gk;
import X.C64433Bp;
import X.CZ3;
import X.CZD;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.webrtc.audio.WebRtcAudioRecord;

@JacksonStdImpl
public class UntypedObjectDeserializer extends StdDeserializer {
    public static final Object[] NO_OBJECTS = new Object[0];
    public static final UntypedObjectDeserializer instance = new UntypedObjectDeserializer();
    private static final long serialVersionUID = 1;

    public UntypedObjectDeserializer() {
        super(Object.class);
    }

    public Object deserialize(C28271eX r9, C26791c3 r10) {
        switch (C29641gk.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r9.getCurrentToken().ordinal()]) {
            case 1:
            case 3:
                return mapObject(r9, r10);
            case 2:
                if (r10.isEnabled(AnonymousClass1c1.USE_JAVA_ARRAY_FOR_JSON_ARRAY)) {
                    if (r9.nextToken() == C182811d.END_ARRAY) {
                        return NO_OBJECTS;
                    }
                    CZ3 leaseObjectBuffer = r10.leaseObjectBuffer();
                    Object[] resetAndStart = leaseObjectBuffer.resetAndStart();
                    int i = 0;
                    while (true) {
                        Object deserialize = deserialize(r9, r10);
                        if (i >= resetAndStart.length) {
                            resetAndStart = leaseObjectBuffer.appendCompletedChunk(resetAndStart);
                            i = 0;
                        }
                        int i2 = i + 1;
                        resetAndStart[i] = deserialize;
                        if (r9.nextToken() == C182811d.END_ARRAY) {
                            int i3 = leaseObjectBuffer._bufferedEntryCount + i2;
                            Object[] objArr = new Object[i3];
                            CZ3._copyTo(leaseObjectBuffer, objArr, i3, resetAndStart, i2);
                            return objArr;
                        }
                        i = i2;
                    }
                } else if (r9.nextToken() == C182811d.END_ARRAY) {
                    return new ArrayList(4);
                } else {
                    CZ3 leaseObjectBuffer2 = r10.leaseObjectBuffer();
                    Object[] resetAndStart2 = leaseObjectBuffer2.resetAndStart();
                    int i4 = 0;
                    int i5 = 0;
                    while (true) {
                        Object deserialize2 = deserialize(r9, r10);
                        i4++;
                        if (i5 >= resetAndStart2.length) {
                            resetAndStart2 = leaseObjectBuffer2.appendCompletedChunk(resetAndStart2);
                            i5 = 0;
                        }
                        int i6 = i5 + 1;
                        resetAndStart2[i5] = deserialize2;
                        if (r9.nextToken() == C182811d.END_ARRAY) {
                            ArrayList arrayList = new ArrayList(i4 + (i4 >> 3) + 1);
                            CZD czd = leaseObjectBuffer2._bufferHead;
                            while (true) {
                                int i7 = 0;
                                if (czd != null) {
                                    Object[] objArr2 = czd._data;
                                    int length = objArr2.length;
                                    while (i7 < length) {
                                        arrayList.add(objArr2[i7]);
                                        i7++;
                                    }
                                    czd = czd._next;
                                } else {
                                    while (i7 < i6) {
                                        arrayList.add(resetAndStart2[i7]);
                                        i7++;
                                    }
                                    return arrayList;
                                }
                            }
                        } else {
                            i5 = i6;
                        }
                    }
                }
            case 4:
                return r9.getEmbeddedObject();
            case 5:
                return r9.getText();
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                if (r10.isEnabled(AnonymousClass1c1.USE_BIG_INTEGER_FOR_INTS)) {
                    return r9.getBigIntegerValue();
                }
                return r9.getNumberValue();
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                if (r10.isEnabled(AnonymousClass1c1.USE_BIG_DECIMAL_FOR_FLOATS)) {
                    return r9.getDecimalValue();
                }
                return Double.valueOf(r9.getDoubleValue());
            case 8:
                return Boolean.TRUE;
            case Process.SIGKILL:
                return Boolean.FALSE;
            case AnonymousClass1Y3.A01:
                return null;
            default:
                throw r10.mappingException(Object.class);
        }
    }

    private Object mapObject(C28271eX r8, C26791c3 r9) {
        C182811d currentToken = r8.getCurrentToken();
        if (currentToken == C182811d.START_OBJECT) {
            currentToken = r8.nextToken();
        }
        C182811d r0 = C182811d.FIELD_NAME;
        if (currentToken != r0) {
            return new LinkedHashMap(4);
        }
        String text = r8.getText();
        r8.nextToken();
        Object deserialize = deserialize(r8, r9);
        if (r8.nextToken() != r0) {
            LinkedHashMap linkedHashMap = new LinkedHashMap(4);
            linkedHashMap.put(text, deserialize);
            return linkedHashMap;
        }
        String text2 = r8.getText();
        r8.nextToken();
        Object deserialize2 = deserialize(r8, r9);
        if (r8.nextToken() != r0) {
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(4);
            linkedHashMap2.put(text, deserialize);
            linkedHashMap2.put(text2, deserialize2);
            return linkedHashMap2;
        }
        LinkedHashMap linkedHashMap3 = new LinkedHashMap();
        linkedHashMap3.put(text, deserialize);
        linkedHashMap3.put(text2, deserialize2);
        do {
            String text3 = r8.getText();
            r8.nextToken();
            linkedHashMap3.put(text3, deserialize(r8, r9));
        } while (r8.nextToken() != C182811d.END_OBJECT);
        return linkedHashMap3;
    }

    public Object deserializeWithType(C28271eX r3, C26791c3 r4, C64433Bp r5) {
        switch (C29641gk.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r3.getCurrentToken().ordinal()]) {
            case 1:
            case 2:
            case 3:
                return r5.deserializeTypedFromAny(r3, r4);
            case 4:
                return r3.getEmbeddedObject();
            case 5:
                return r3.getText();
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                if (r4.isEnabled(AnonymousClass1c1.USE_BIG_INTEGER_FOR_INTS)) {
                    return r3.getBigIntegerValue();
                }
                return r3.getNumberValue();
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                if (r4.isEnabled(AnonymousClass1c1.USE_BIG_DECIMAL_FOR_FLOATS)) {
                    return r3.getDecimalValue();
                }
                return Double.valueOf(r3.getDoubleValue());
            case 8:
                return Boolean.TRUE;
            case Process.SIGKILL:
                return Boolean.FALSE;
            case AnonymousClass1Y3.A01:
                return null;
            default:
                throw r4.mappingException(Object.class);
        }
    }
}
