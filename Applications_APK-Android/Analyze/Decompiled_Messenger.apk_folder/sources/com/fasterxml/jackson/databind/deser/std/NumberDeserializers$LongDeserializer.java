package com.fasterxml.jackson.databind.deser.std;

import X.C182811d;
import X.C26791c3;
import X.C28271eX;
import X.C29491gV;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;

@JacksonStdImpl
public final class NumberDeserializers$LongDeserializer extends NumberDeserializers$PrimitiveOrWrapperDeserializer {
    public static final NumberDeserializers$LongDeserializer primitiveInstance = new NumberDeserializers$LongDeserializer(Long.class, 0L);
    private static final long serialVersionUID = 1;
    public static final NumberDeserializers$LongDeserializer wrapperInstance = new NumberDeserializers$LongDeserializer(Long.TYPE, null);

    private NumberDeserializers$LongDeserializer(Class cls, Long l) {
        super(cls, l);
    }

    /* access modifiers changed from: private */
    public Long deserialize(C28271eX r4, C26791c3 r5) {
        Object nullValue;
        long j;
        C182811d currentToken = r4.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            return Long.valueOf(r4.getLongValue());
        }
        if (currentToken == C182811d.VALUE_STRING) {
            String trim = r4.getText().trim();
            int length = trim.length();
            if (length == 0) {
                nullValue = getEmptyValue();
            } else {
                if (length <= 9) {
                    j = (long) C29491gV.parseInt(trim);
                } else {
                    try {
                        j = Long.parseLong(trim);
                    } catch (IllegalArgumentException unused) {
                        throw r5.weirdStringException(trim, this._valueClass, "not a valid Long value");
                    }
                }
                return Long.valueOf(j);
            }
        } else if (currentToken == C182811d.VALUE_NULL) {
            nullValue = getNullValue();
        } else {
            throw r5.mappingException(this._valueClass, currentToken);
        }
        return (Long) nullValue;
    }
}
