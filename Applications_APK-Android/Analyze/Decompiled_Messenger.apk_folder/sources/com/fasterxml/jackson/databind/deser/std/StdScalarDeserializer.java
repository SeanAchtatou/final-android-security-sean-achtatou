package com.fasterxml.jackson.databind.deser.std;

import X.C26791c3;
import X.C28271eX;
import X.C64433Bp;

public abstract class StdScalarDeserializer extends StdDeserializer {
    private static final long serialVersionUID = 1;

    public Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return r4.deserializeTypedFromScalar(r2, r3);
    }

    public StdScalarDeserializer(Class cls) {
        super(cls);
    }
}
