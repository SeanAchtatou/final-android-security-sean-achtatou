package com.fasterxml.jackson.databind.ser.std;

import X.C11260mU;
import X.C11710np;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.text.DateFormat;
import java.util.Date;

@JacksonStdImpl
public class DateSerializer extends DateTimeSerializerBase {
    public static final DateSerializer instance = new DateSerializer();

    public /* bridge */ /* synthetic */ long _timestamp(Object obj) {
        Date date = (Date) obj;
        if (date == null) {
            return 0;
        }
        return date.getTime();
    }

    public /* bridge */ /* synthetic */ void serialize(Object obj, C11710np r4, C11260mU r5) {
        long time;
        Date date = (Date) obj;
        if (this._useTimestamp) {
            if (date == null) {
                time = 0;
            } else {
                time = date.getTime();
            }
            r4.writeNumber(time);
            return;
        }
        DateFormat dateFormat = this._customFormat;
        if (dateFormat != null) {
            synchronized (dateFormat) {
                r4.writeString(this._customFormat.format(date));
            }
            return;
        }
        r5.defaultSerializeDateValue(date, r4);
    }

    public /* bridge */ /* synthetic */ DateTimeSerializerBase withFormat(boolean z, DateFormat dateFormat) {
        DateSerializer dateSerializer;
        if (z) {
            return dateSerializer;
        }
        dateSerializer = new DateSerializer(false, dateFormat);
        return dateSerializer;
    }

    public DateSerializer() {
        this(false, null);
    }

    private DateSerializer(boolean z, DateFormat dateFormat) {
        super(Date.class, z, dateFormat);
    }
}
