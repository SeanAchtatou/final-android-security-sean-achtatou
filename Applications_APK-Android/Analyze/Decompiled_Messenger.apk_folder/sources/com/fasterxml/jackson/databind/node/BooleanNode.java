package com.fasterxml.jackson.databind.node;

import X.C11260mU;
import X.C11710np;
import X.C11980oL;
import X.C14610tg;
import X.C182811d;

public final class BooleanNode extends C14610tg {
    public static final BooleanNode FALSE = new BooleanNode(false);
    public static final BooleanNode TRUE = new BooleanNode(true);
    private final boolean _value;

    public boolean equals(Object obj) {
        if (obj != this) {
            return obj != null && obj.getClass() == getClass() && this._value == ((BooleanNode) obj)._value;
        }
        return true;
    }

    public double asDouble(double d) {
        if (this._value) {
            return 1.0d;
        }
        return 0.0d;
    }

    public long asLong(long j) {
        if (this._value) {
            return 1;
        }
        return 0;
    }

    public String asText() {
        if (this._value) {
            return "true";
        }
        return "false";
    }

    public C182811d asToken() {
        if (this._value) {
            return C182811d.VALUE_TRUE;
        }
        return C182811d.VALUE_FALSE;
    }

    public boolean booleanValue() {
        return this._value;
    }

    public C11980oL getNodeType() {
        return C11980oL.BOOLEAN;
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        r2.writeBoolean(this._value);
    }

    private BooleanNode(boolean z) {
        this._value = z;
    }

    public int asInt(int i) {
        return this._value ? 1 : 0;
    }

    public boolean asBoolean() {
        return this._value;
    }

    public boolean asBoolean(boolean z) {
        return this._value;
    }
}
