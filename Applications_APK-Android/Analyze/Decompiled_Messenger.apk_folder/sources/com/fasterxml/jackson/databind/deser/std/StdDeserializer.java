package com.fasterxml.jackson.databind.deser.std;

import X.AnonymousClass08S;
import X.AnonymousClass1c1;
import X.AnonymousClass2KN;
import X.C10030jR;
import X.C10140jc;
import X.C181018ap;
import X.C182811d;
import X.C21470AFl;
import X.C26791c3;
import X.C28271eX;
import X.C29491gV;
import X.C29501gW;
import X.C422428v;
import X.C64433Bp;
import X.CX9;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public abstract class StdDeserializer extends JsonDeserializer implements Serializable {
    private static final long serialVersionUID = 1;
    public final Class _valueClass;

    public void handleUnknownProperty(C28271eX r9, C26791c3 r10, Object obj, String str) {
        Collection knownPropertyNames;
        Class<?> cls;
        if (obj == null) {
            obj = this._valueClass;
        }
        AnonymousClass2KN r0 = r10._config._problemHandlers;
        if (r0 != null) {
            while (r0 != null) {
                r0 = r0._next;
            }
        }
        if (!r10.isEnabled(AnonymousClass1c1.FAIL_ON_UNKNOWN_PROPERTIES)) {
            r9.skipChildren();
            return;
        }
        if (this == null) {
            knownPropertyNames = null;
        } else {
            knownPropertyNames = getKnownPropertyNames();
        }
        C28271eX r4 = r10._parser;
        if (obj != null) {
            if (obj instanceof Class) {
                cls = (Class) obj;
            } else {
                cls = obj.getClass();
            }
            C21470AFl aFl = new C21470AFl(AnonymousClass08S.A0T("Unrecognized field \"", str, "\" (class ", cls.getName(), "), not marked as ignorable"), r4.getCurrentLocation(), cls, str, knownPropertyNames);
            aFl.prependPath(new C181018ap(obj, str));
            throw aFl;
        }
        throw new IllegalArgumentException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
        if ("0".equals(r1) == false) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        r0 = java.lang.Boolean.TRUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        if (r3.getLongValue() == 0) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final boolean _parseBooleanFromNumber(X.C28271eX r3, X.C26791c3 r4) {
        /*
            X.1gW r1 = r3.getNumberType()
            X.1gW r0 = X.C29501gW.LONG
            if (r1 != r0) goto L_0x0019
            long r3 = r3.getLongValue()
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x002e
        L_0x0012:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
        L_0x0014:
            boolean r0 = r0.booleanValue()
            return r0
        L_0x0019:
            java.lang.String r1 = r3.getText()
            java.lang.String r0 = "0.0"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0012
            java.lang.String r0 = "0"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x002e
            goto L_0x0012
        L_0x002e:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.std.StdDeserializer._parseBooleanFromNumber(X.1eX, X.1c3):boolean");
    }

    public static final String _parseString(C28271eX r2, C26791c3 r3) {
        String valueAsString = r2.getValueAsString();
        if (valueAsString != null) {
            return valueAsString;
        }
        throw r3.mappingException(String.class, r2.getCurrentToken());
    }

    public static JsonDeserializer findConvertingContentDeserializer(C26791c3 r3, CX9 cx9, JsonDeserializer jsonDeserializer) {
        Object findDeserializationContentConverter;
        C10140jc annotationIntrospector = r3.getAnnotationIntrospector();
        if (annotationIntrospector == null || cx9 == null || (findDeserializationContentConverter = annotationIntrospector.findDeserializationContentConverter(cx9.getMember())) == null) {
            return jsonDeserializer;
        }
        C422428v converterInstance = r3.converterInstance(cx9.getMember(), findDeserializationContentConverter);
        C10030jR inputType = converterInstance.getInputType(r3.getTypeFactory());
        if (jsonDeserializer == null) {
            jsonDeserializer = r3.findContextualValueDeserializer(inputType, cx9);
        }
        return new StdDelegatingDeserializer(converterInstance, inputType, jsonDeserializer);
    }

    public final Boolean _parseBoolean(C28271eX r4, C26791c3 r5) {
        Object emptyValue;
        C182811d currentToken = r4.getCurrentToken();
        if (currentToken != C182811d.VALUE_TRUE) {
            if (currentToken != C182811d.VALUE_FALSE) {
                if (currentToken != C182811d.VALUE_NUMBER_INT) {
                    if (currentToken == C182811d.VALUE_NULL) {
                        emptyValue = getNullValue();
                    } else if (currentToken == C182811d.VALUE_STRING) {
                        String trim = r4.getText().trim();
                        if (!"true".equals(trim)) {
                            if (!"false".equals(trim)) {
                                if (trim.length() == 0) {
                                    emptyValue = getEmptyValue();
                                } else {
                                    throw r5.weirdStringException(trim, this._valueClass, "only \"true\" or \"false\" recognized");
                                }
                            }
                        }
                    } else {
                        throw r5.mappingException(this._valueClass, currentToken);
                    }
                    return (Boolean) emptyValue;
                } else if (r4.getNumberType() != C29501gW.INT) {
                    return Boolean.valueOf(_parseBooleanFromNumber(r4, r5));
                } else {
                    if (r4.getIntValue() == 0) {
                        return Boolean.FALSE;
                    }
                    return Boolean.TRUE;
                }
            }
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public final boolean _parseBooleanPrimitive(C28271eX r5, C26791c3 r6) {
        C182811d currentToken = r5.getCurrentToken();
        if (currentToken == C182811d.VALUE_TRUE) {
            return true;
        }
        if (currentToken == C182811d.VALUE_FALSE || currentToken == C182811d.VALUE_NULL) {
            return false;
        }
        if (currentToken == C182811d.VALUE_NUMBER_INT) {
            if (r5.getNumberType() != C29501gW.INT) {
                return _parseBooleanFromNumber(r5, r6);
            }
            if (r5.getIntValue() == 0) {
                return false;
            }
            return true;
        } else if (currentToken == C182811d.VALUE_STRING) {
            String trim = r5.getText().trim();
            if ("true".equals(trim)) {
                return true;
            }
            if ("false".equals(trim) || trim.length() == 0) {
                return Boolean.FALSE.booleanValue();
            }
            throw r6.weirdStringException(trim, this._valueClass, "only \"true\" or \"false\" recognized");
        } else {
            throw r6.mappingException(this._valueClass, currentToken);
        }
    }

    public Date _parseDate(C28271eX r6, C26791c3 r7) {
        C182811d currentToken = r6.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT) {
            return new Date(r6.getLongValue());
        }
        if (currentToken == C182811d.VALUE_NULL) {
            return (Date) getNullValue();
        }
        if (currentToken == C182811d.VALUE_STRING) {
            try {
                String trim = r6.getText().trim();
                if (trim.length() == 0) {
                    return (Date) getEmptyValue();
                }
                return r7.parseDate(trim);
            } catch (IllegalArgumentException e) {
                throw r7.weirdStringException(null, this._valueClass, AnonymousClass08S.A0P("not a valid representation (error: ", e.getMessage(), ")"));
            }
        } else {
            throw r7.mappingException(this._valueClass, currentToken);
        }
    }

    public final Double _parseDouble(C28271eX r4, C26791c3 r5) {
        double d;
        Object nullValue;
        double d2;
        C182811d currentToken = r4.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            d = r4.getDoubleValue();
        } else {
            if (currentToken == C182811d.VALUE_STRING) {
                String trim = r4.getText().trim();
                if (trim.length() == 0) {
                    nullValue = getEmptyValue();
                } else {
                    char charAt = trim.charAt(0);
                    if (charAt != '-') {
                        if (charAt != 'I') {
                            if (charAt == 'N' && "NaN".equals(trim)) {
                                d = Double.NaN;
                            }
                        } else if ("Infinity".equals(trim) || "INF".equals(trim)) {
                            d = Double.POSITIVE_INFINITY;
                        }
                    } else if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                        d = Double.NEGATIVE_INFINITY;
                    }
                    try {
                        if ("2.2250738585072012e-308".equals(trim)) {
                            d2 = Double.MIN_VALUE;
                        } else {
                            d2 = Double.parseDouble(trim);
                        }
                        return Double.valueOf(d2);
                    } catch (IllegalArgumentException unused) {
                        throw r5.weirdStringException(trim, this._valueClass, "not a valid Double value");
                    }
                }
            } else if (currentToken == C182811d.VALUE_NULL) {
                nullValue = getNullValue();
            } else {
                throw r5.mappingException(this._valueClass, currentToken);
            }
            return (Double) nullValue;
        }
        return Double.valueOf(d);
    }

    public final double _parseDoublePrimitive(C28271eX r6, C26791c3 r7) {
        C182811d currentToken = r6.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            return r6.getDoubleValue();
        }
        if (currentToken == C182811d.VALUE_STRING) {
            String trim = r6.getText().trim();
            if (trim.length() != 0) {
                char charAt = trim.charAt(0);
                if (charAt != '-') {
                    if (charAt != 'I') {
                        if (charAt == 'N' && "NaN".equals(trim)) {
                            return Double.NaN;
                        }
                    } else if ("Infinity".equals(trim) || "INF".equals(trim)) {
                        return Double.POSITIVE_INFINITY;
                    }
                } else if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                    return Double.NEGATIVE_INFINITY;
                }
                try {
                    if ("2.2250738585072012e-308".equals(trim)) {
                        return Double.MIN_VALUE;
                    }
                    return Double.parseDouble(trim);
                } catch (IllegalArgumentException unused) {
                    throw r7.weirdStringException(trim, this._valueClass, "not a valid double value");
                }
            }
        } else if (currentToken != C182811d.VALUE_NULL) {
            throw r7.mappingException(this._valueClass, currentToken);
        }
        return 0.0d;
    }

    public final float _parseFloatPrimitive(C28271eX r4, C26791c3 r5) {
        C182811d currentToken = r4.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            return r4.getFloatValue();
        }
        if (currentToken == C182811d.VALUE_STRING) {
            String trim = r4.getText().trim();
            if (trim.length() != 0) {
                char charAt = trim.charAt(0);
                if (charAt != '-') {
                    if (charAt != 'I') {
                        if (charAt == 'N' && "NaN".equals(trim)) {
                            return Float.NaN;
                        }
                    } else if ("Infinity".equals(trim) || "INF".equals(trim)) {
                        return Float.POSITIVE_INFINITY;
                    }
                } else if ("-Infinity".equals(trim) || "-INF".equals(trim)) {
                    return Float.NEGATIVE_INFINITY;
                }
                try {
                    return Float.parseFloat(trim);
                } catch (IllegalArgumentException unused) {
                    throw r5.weirdStringException(trim, this._valueClass, "not a valid float value");
                }
            }
        } else if (currentToken != C182811d.VALUE_NULL) {
            throw r5.mappingException(this._valueClass, currentToken);
        }
        return 0.0f;
    }

    public final int _parseIntPrimitive(C28271eX r7, C26791c3 r8) {
        C182811d currentToken = r7.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            return r7.getIntValue();
        }
        if (currentToken == C182811d.VALUE_STRING) {
            String trim = r7.getText().trim();
            try {
                int length = trim.length();
                if (length > 9) {
                    long parseLong = Long.parseLong(trim);
                    if (parseLong >= -2147483648L && parseLong <= 2147483647L) {
                        return (int) parseLong;
                    }
                    Class cls = this._valueClass;
                    throw r8.weirdStringException(trim, cls, "Overflow: numeric value (" + trim + ") out of range of int (" + Integer.MIN_VALUE + " - " + Integer.MAX_VALUE + ")");
                } else if (length != 0) {
                    return C29491gV.parseInt(trim);
                }
            } catch (IllegalArgumentException unused) {
                throw r8.weirdStringException(trim, this._valueClass, "not a valid int value");
            }
        } else if (currentToken != C182811d.VALUE_NULL) {
            throw r8.mappingException(this._valueClass, currentToken);
        }
        return 0;
    }

    public final Integer _parseInteger(C28271eX r8, C26791c3 r9) {
        C182811d currentToken = r8.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            return Integer.valueOf(r8.getIntValue());
        }
        if (currentToken == C182811d.VALUE_STRING) {
            String trim = r8.getText().trim();
            try {
                int length = trim.length();
                if (length > 9) {
                    long parseLong = Long.parseLong(trim);
                    if (parseLong >= -2147483648L && parseLong <= 2147483647L) {
                        return Integer.valueOf((int) parseLong);
                    }
                    Class cls = this._valueClass;
                    throw r9.weirdStringException(trim, cls, "Overflow: numeric value (" + trim + ") out of range of Integer (" + Integer.MIN_VALUE + " - " + Integer.MAX_VALUE + ")");
                } else if (length == 0) {
                    return (Integer) getEmptyValue();
                } else {
                    return Integer.valueOf(C29491gV.parseInt(trim));
                }
            } catch (IllegalArgumentException unused) {
                throw r9.weirdStringException(trim, this._valueClass, "not a valid Integer value");
            }
        } else if (currentToken == C182811d.VALUE_NULL) {
            return (Integer) getNullValue();
        } else {
            throw r9.mappingException(this._valueClass, currentToken);
        }
    }

    public final long _parseLongPrimitive(C28271eX r6, C26791c3 r7) {
        C182811d currentToken = r6.getCurrentToken();
        if (currentToken == C182811d.VALUE_NUMBER_INT || currentToken == C182811d.VALUE_NUMBER_FLOAT) {
            return r6.getLongValue();
        }
        if (currentToken == C182811d.VALUE_STRING) {
            String trim = r6.getText().trim();
            int length = trim.length();
            if (length != 0) {
                if (length > 9) {
                    return Long.parseLong(trim);
                }
                try {
                    return (long) C29491gV.parseInt(trim);
                } catch (IllegalArgumentException unused) {
                    throw r7.weirdStringException(trim, this._valueClass, "not a valid long value");
                }
            }
        } else if (currentToken != C182811d.VALUE_NULL) {
            throw r7.mappingException(this._valueClass, currentToken);
        }
        return 0;
    }

    public final short _parseShortPrimitive(C28271eX r4, C26791c3 r5) {
        int _parseIntPrimitive = _parseIntPrimitive(r4, r5);
        if (_parseIntPrimitive >= -32768 && _parseIntPrimitive <= 32767) {
            return (short) _parseIntPrimitive;
        }
        throw r5.weirdStringException(String.valueOf(_parseIntPrimitive), this._valueClass, "overflow, value can not be represented as 16-bit value");
    }

    public Object deserializeWithType(C28271eX r2, C26791c3 r3, C64433Bp r4) {
        return r4.deserializeTypedFromAny(r2, r3);
    }

    public StdDeserializer(C10030jR r2) {
        Class cls;
        if (r2 == null) {
            cls = null;
        } else {
            cls = r2._class;
        }
        this._valueClass = cls;
    }

    public StdDeserializer(Class cls) {
        this._valueClass = cls;
    }
}
