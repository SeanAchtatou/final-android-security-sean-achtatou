package com.fasterxml.jackson.annotation;

import X.C10180jg;

public @interface JsonAutoDetect {
    C10180jg creatorVisibility() default C10180jg.DEFAULT;

    C10180jg fieldVisibility() default C10180jg.DEFAULT;

    C10180jg getterVisibility() default C10180jg.DEFAULT;

    C10180jg isGetterVisibility() default C10180jg.DEFAULT;

    C10180jg setterVisibility() default C10180jg.DEFAULT;
}
