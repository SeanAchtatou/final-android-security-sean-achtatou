package com.fasterxml.jackson.annotation;

import X.BlQ;
import X.C30361Eum;
import X.CXW;

public @interface JsonTypeInfo {
    Class defaultImpl() default C30361Eum.class;

    CXW include() default CXW.PROPERTY;

    String property() default "";

    BlQ use();

    boolean visible() default false;
}
