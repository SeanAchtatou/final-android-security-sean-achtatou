package com.google.common.collect;

import X.C25001Xy;
import java.util.Iterator;
import java.util.NoSuchElementException;

public enum Iterators$EmptyModifiableIterator implements Iterator {
    INSTANCE;

    public boolean hasNext() {
        return false;
    }

    public void remove() {
        C25001Xy.A04(false);
    }

    public Object next() {
        throw new NoSuchElementException();
    }
}
