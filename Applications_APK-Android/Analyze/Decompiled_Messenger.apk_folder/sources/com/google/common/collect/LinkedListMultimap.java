package com.google.common.collect;

import X.AnonymousClass0TG;
import X.AnonymousClass0V1;
import X.B8U;
import X.B8V;
import X.C04300To;
import X.C04510Uz;
import X.C22695B8h;
import X.C22697B8j;
import X.C24931Xr;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LinkedListMultimap extends C04510Uz implements AnonymousClass0V1, Serializable {
    private static final long serialVersionUID = 0;
    public transient int A00;
    public transient int A01;
    public transient C22695B8h A02;
    public transient C22695B8h A03;
    public transient Map A04 = AnonymousClass0TG.A04();

    public void clear() {
        this.A02 = null;
        this.A03 = null;
        this.A04.clear();
        this.A01 = 0;
        this.A00++;
    }

    public static C22695B8h A00(LinkedListMultimap linkedListMultimap, Object obj, Object obj2, C22695B8h b8h) {
        C22695B8h b8h2 = new C22695B8h(obj, obj2);
        if (linkedListMultimap.A02 == null) {
            linkedListMultimap.A03 = b8h2;
            linkedListMultimap.A02 = b8h2;
        } else {
            if (b8h == null) {
                C22695B8h b8h3 = linkedListMultimap.A03;
                b8h3.A00 = b8h2;
                b8h2.A02 = b8h3;
                linkedListMultimap.A03 = b8h2;
                C22697B8j b8j = (C22697B8j) linkedListMultimap.A04.get(obj);
                if (b8j != null) {
                    b8j.A00++;
                    C22695B8h b8h4 = b8j.A02;
                    b8h4.A01 = b8h2;
                    b8h2.A03 = b8h4;
                    b8j.A02 = b8h2;
                }
            } else {
                ((C22697B8j) linkedListMultimap.A04.get(obj)).A00++;
                b8h2.A02 = b8h.A02;
                b8h2.A03 = b8h.A03;
                b8h2.A00 = b8h;
                b8h2.A01 = b8h;
                C22695B8h b8h5 = b8h.A03;
                if (b8h5 == null) {
                    ((C22697B8j) linkedListMultimap.A04.get(obj)).A01 = b8h2;
                } else {
                    b8h5.A01 = b8h2;
                }
                C22695B8h b8h6 = b8h.A02;
                if (b8h6 == null) {
                    linkedListMultimap.A02 = b8h2;
                } else {
                    b8h6.A00 = b8h2;
                }
                b8h.A02 = b8h2;
                b8h.A03 = b8h2;
            }
            linkedListMultimap.A01++;
            return b8h2;
        }
        linkedListMultimap.A04.put(obj, new C22697B8j(b8h2));
        linkedListMultimap.A00++;
        linkedListMultimap.A01++;
        return b8h2;
    }

    public static void A01(LinkedListMultimap linkedListMultimap, C22695B8h b8h) {
        C22695B8h b8h2 = b8h.A02;
        if (b8h2 != null) {
            b8h2.A00 = b8h.A00;
        } else {
            linkedListMultimap.A02 = b8h.A00;
        }
        C22695B8h b8h3 = b8h.A00;
        if (b8h3 != null) {
            b8h3.A02 = b8h2;
        } else {
            linkedListMultimap.A03 = b8h2;
        }
        if (b8h.A03 == null && b8h.A01 == null) {
            ((C22697B8j) linkedListMultimap.A04.remove(b8h.A05)).A00 = 0;
            linkedListMultimap.A00++;
        } else {
            C22697B8j b8j = (C22697B8j) linkedListMultimap.A04.get(b8h.A05);
            b8j.A00--;
            C22695B8h b8h4 = b8h.A03;
            if (b8h4 == null) {
                b8j.A01 = b8h.A01;
            } else {
                b8h4.A01 = b8h.A01;
            }
            C22695B8h b8h5 = b8h.A01;
            if (b8h5 == null) {
                b8j.A02 = b8h4;
            } else {
                b8h5.A03 = b8h4;
            }
        }
        linkedListMultimap.A01--;
    }

    /* renamed from: AbL */
    public List AbK(Object obj) {
        return new B8V(this, obj);
    }

    /* renamed from: C1O */
    public List C1N(Object obj) {
        B8U b8u = new B8U(this, obj);
        ArrayList A002 = C04300To.A00();
        C24931Xr.A09(A002, b8u);
        List unmodifiableList = Collections.unmodifiableList(A002);
        C24931Xr.A08(new B8U(this, obj));
        return unmodifiableList;
    }

    public Collection C2O(Object obj, Iterable iterable) {
        B8U b8u = new B8U(this, obj);
        ArrayList A002 = C04300To.A00();
        C24931Xr.A09(A002, b8u);
        List unmodifiableList = Collections.unmodifiableList(A002);
        B8U b8u2 = new B8U(this, obj);
        Iterator it = iterable.iterator();
        while (b8u2.hasNext() && it.hasNext()) {
            b8u2.next();
            b8u2.set(it.next());
        }
        while (b8u2.hasNext()) {
            b8u2.next();
            b8u2.remove();
        }
        while (it.hasNext()) {
            b8u2.add(it.next());
        }
        return unmodifiableList;
    }

    public boolean containsKey(Object obj) {
        return this.A04.containsKey(obj);
    }

    public int size() {
        return this.A01;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.A04 = new LinkedHashMap();
        int readInt = objectInputStream.readInt();
        for (int i = 0; i < readInt; i++) {
            Byx(objectInputStream.readObject(), objectInputStream.readObject());
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(size());
        for (Map.Entry entry : (List) super.AYj()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeObject(entry.getValue());
        }
    }

    public /* bridge */ /* synthetic */ Collection AYj() {
        return (List) super.AYj();
    }

    public boolean containsValue(Object obj) {
        return ((List) super.values()).contains(obj);
    }

    public /* bridge */ /* synthetic */ Collection values() {
        return (List) super.values();
    }
}
