package com.google.common.collect;

import X.B7I;
import X.B7X;
import X.B7Y;
import X.C06950cM;
import X.C07740e5;
import X.C07940eQ;
import X.C24931Xr;
import X.C25541a0;
import X.C25551a1;
import X.C25571a3;
import X.C45552Mn;
import X.C51862hz;
import com.google.common.base.Equivalence;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.MapMakerInternalMap.Segment;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;

public class MapMakerInternalMap<K, V, E extends C07740e5<K, V, E>, S extends Segment<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    public static final C25551a1 A07 = new C25541a0();
    private static final long serialVersionUID = 5;
    public transient Collection A00;
    public transient Set A01;
    public transient Set A02;
    public final transient int A03;
    public final transient int A04;
    public final transient Segment[] A05;
    public final transient C25571a3 A06;
    public final int concurrencyLevel;
    public final Equivalence keyEquivalence;

    public abstract class Segment extends ReentrantLock {
        public volatile int count;
        public final MapMakerInternalMap map;
        public final int maxSegmentSize;
        public int modCount;
        public final AtomicInteger readCount = new AtomicInteger();
        public volatile AtomicReferenceArray table;
        public int threshold;

        public Segment A05() {
            return !(this instanceof WeakKeyWeakValueSegment) ? !(this instanceof WeakKeyStrongValueSegment) ? (WeakKeyDummyValueSegment) this : (WeakKeyStrongValueSegment) this : (WeakKeyWeakValueSegment) this;
        }

        public void A07() {
            if (this instanceof WeakKeyWeakValueSegment) {
                do {
                } while (((WeakKeyWeakValueSegment) this).queueForKeys.poll() != null);
            } else if (this instanceof WeakKeyStrongValueSegment) {
                do {
                } while (((WeakKeyStrongValueSegment) this).queueForKeys.poll() != null);
            } else if (this instanceof WeakKeyDummyValueSegment) {
                do {
                } while (((WeakKeyDummyValueSegment) this).queueForKeys.poll() != null);
            }
        }

        public void A08() {
            if (this instanceof WeakKeyWeakValueSegment) {
                WeakKeyWeakValueSegment weakKeyWeakValueSegment = (WeakKeyWeakValueSegment) this;
                weakKeyWeakValueSegment.A0A(weakKeyWeakValueSegment.queueForKeys);
                weakKeyWeakValueSegment.A0B(weakKeyWeakValueSegment.queueForValues);
            } else if (this instanceof WeakKeyStrongValueSegment) {
                WeakKeyStrongValueSegment weakKeyStrongValueSegment = (WeakKeyStrongValueSegment) this;
                weakKeyStrongValueSegment.A0A(weakKeyStrongValueSegment.queueForKeys);
            } else if (this instanceof WeakKeyDummyValueSegment) {
                WeakKeyDummyValueSegment weakKeyDummyValueSegment = (WeakKeyDummyValueSegment) this;
                weakKeyDummyValueSegment.A0A(weakKeyDummyValueSegment.queueForKeys);
            }
        }

        /* JADX INFO: finally extract failed */
        public void A0A(ReferenceQueue referenceQueue) {
            int i = 0;
            do {
                Reference poll = referenceQueue.poll();
                if (poll != null) {
                    C07740e5 r7 = (C07740e5) poll;
                    MapMakerInternalMap mapMakerInternalMap = this.map;
                    int AoV = r7.AoV();
                    Segment A01 = MapMakerInternalMap.A01(mapMakerInternalMap, AoV);
                    A01.lock();
                    try {
                        AtomicReferenceArray atomicReferenceArray = A01.table;
                        int length = AoV & (atomicReferenceArray.length() - 1);
                        C07740e5 r2 = (C07740e5) atomicReferenceArray.get(length);
                        C07740e5 r1 = r2;
                        while (true) {
                            if (r1 == null) {
                                break;
                            } else if (r1 == r7) {
                                A01.modCount++;
                                atomicReferenceArray.set(length, A00(A01, r2, r1));
                                A01.count--;
                                break;
                            } else {
                                r1 = r1.Avg();
                            }
                        }
                        A01.unlock();
                        i++;
                    } catch (Throwable th) {
                        A01.unlock();
                        throw th;
                    }
                } else {
                    return;
                }
            } while (i != 16);
        }

        /* JADX INFO: finally extract failed */
        public void A0B(ReferenceQueue referenceQueue) {
            int i = 0;
            do {
                Reference poll = referenceQueue.poll();
                if (poll != null) {
                    C25551a1 r9 = (C25551a1) poll;
                    MapMakerInternalMap mapMakerInternalMap = this.map;
                    C07740e5 AlY = r9.AlY();
                    int AoV = AlY.AoV();
                    Segment A01 = MapMakerInternalMap.A01(mapMakerInternalMap, AoV);
                    Object key = AlY.getKey();
                    A01.lock();
                    try {
                        AtomicReferenceArray atomicReferenceArray = A01.table;
                        int length = (atomicReferenceArray.length() - 1) & AoV;
                        C07740e5 r7 = (C07740e5) atomicReferenceArray.get(length);
                        C07740e5 r2 = r7;
                        while (true) {
                            if (r2 == null) {
                                break;
                            }
                            Object key2 = r2.getKey();
                            if (r2.AoV() != AoV || key2 == null || !A01.map.keyEquivalence.equivalent(key, key2)) {
                                r2 = r2.Avg();
                            } else if (((C45552Mn) r2).B8I() == r9) {
                                A01.modCount++;
                                atomicReferenceArray.set(length, A00(A01, r7, r2));
                                A01.count--;
                            }
                        }
                        A01.unlock();
                        i++;
                    } catch (Throwable th) {
                        A01.unlock();
                        throw th;
                    }
                } else {
                    return;
                }
            } while (i != 16);
        }

        public static C07740e5 A00(Segment segment, C07740e5 r4, C07740e5 r5) {
            int i = segment.count;
            C07740e5 Avg = r5.Avg();
            while (r4 != r5) {
                C25571a3 r0 = segment.map.A06;
                segment.A05();
                C07740e5 AUU = r0.AUU(segment, r4, Avg);
                if (AUU != null) {
                    Avg = AUU;
                } else {
                    i--;
                }
                r4 = r4.Avg();
            }
            segment.count = i;
            return Avg;
        }

        public static void A03(Segment segment, C07740e5 r2, Object obj) {
            C25571a3 r0 = segment.map.A06;
            segment.A05();
            r0.CCs(segment, r2, obj);
        }

        public C07740e5 A04(Object obj, int i) {
            if (this.count == 0) {
                return null;
            }
            AtomicReferenceArray atomicReferenceArray = this.table;
            for (C07740e5 r2 = (C07740e5) atomicReferenceArray.get((atomicReferenceArray.length() - 1) & i); r2 != null; r2 = r2.Avg()) {
                if (r2.AoV() == i) {
                    Object key = r2.getKey();
                    if (key == null) {
                        A02(this);
                    } else if (this.map.keyEquivalence.equivalent(obj, key)) {
                        return r2;
                    }
                }
            }
            return null;
        }

        public void A09() {
            if ((this.readCount.incrementAndGet() & 63) == 0) {
                A01(this);
            }
        }

        public Segment(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
            this.map = mapMakerInternalMap;
            this.maxSegmentSize = i2;
            AtomicReferenceArray atomicReferenceArray = new AtomicReferenceArray(i);
            int length = (atomicReferenceArray.length() * 3) >> 2;
            this.threshold = length;
            if (length == this.maxSegmentSize) {
                this.threshold = length + 1;
            }
            this.table = atomicReferenceArray;
        }

        public static void A01(Segment segment) {
            if (segment.tryLock()) {
                try {
                    segment.A08();
                    segment.readCount.set(0);
                } finally {
                    segment.unlock();
                }
            }
        }

        public static void A02(Segment segment) {
            if (segment.tryLock()) {
                try {
                    segment.A08();
                } finally {
                    segment.unlock();
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
            r13.modCount++;
            r0 = r13.map.A06;
            A05();
            r0 = r0.BLe(r13, r14, r15, r6);
            A03(r13, r0, r2);
            r4.set(r1, r0);
            r13.count = r5;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object A06(java.lang.Object r14, int r15, java.lang.Object r16, boolean r17) {
            /*
                r13 = this;
                r13.lock()
                A01(r13)     // Catch:{ all -> 0x00ff }
                int r0 = r13.count     // Catch:{ all -> 0x00ff }
                int r5 = r0 + 1
                int r0 = r13.threshold     // Catch:{ all -> 0x00ff }
                if (r5 <= r0) goto L_0x008f
                java.util.concurrent.atomic.AtomicReferenceArray r9 = r13.table     // Catch:{ all -> 0x00ff }
                int r8 = r9.length()     // Catch:{ all -> 0x00ff }
                r0 = 1073741824(0x40000000, float:2.0)
                if (r8 >= r0) goto L_0x008b
                int r7 = r13.count     // Catch:{ all -> 0x00ff }
                int r0 = r8 << 1
                java.util.concurrent.atomic.AtomicReferenceArray r6 = new java.util.concurrent.atomic.AtomicReferenceArray     // Catch:{ all -> 0x00ff }
                r6.<init>(r0)     // Catch:{ all -> 0x00ff }
                int r0 = r6.length()     // Catch:{ all -> 0x00ff }
                int r0 = r0 * 3
                int r0 = r0 / 4
                r13.threshold = r0     // Catch:{ all -> 0x00ff }
                int r0 = r6.length()     // Catch:{ all -> 0x00ff }
                int r11 = r0 + -1
                r5 = 0
            L_0x0032:
                if (r5 >= r8) goto L_0x0087
                java.lang.Object r4 = r9.get(r5)     // Catch:{ all -> 0x00ff }
                X.0e5 r4 = (X.C07740e5) r4     // Catch:{ all -> 0x00ff }
                if (r4 == 0) goto L_0x0084
                X.0e5 r2 = r4.Avg()     // Catch:{ all -> 0x00ff }
                int r1 = r4.AoV()     // Catch:{ all -> 0x00ff }
                r1 = r1 & r11
                if (r2 != 0) goto L_0x004b
                r6.set(r1, r4)     // Catch:{ all -> 0x00ff }
                goto L_0x0084
            L_0x004b:
                r3 = r4
            L_0x004c:
                if (r2 == 0) goto L_0x005c
                int r0 = r2.AoV()     // Catch:{ all -> 0x00ff }
                r0 = r0 & r11
                if (r0 == r1) goto L_0x0057
                r3 = r2
                r1 = r0
            L_0x0057:
                X.0e5 r2 = r2.Avg()     // Catch:{ all -> 0x00ff }
                goto L_0x004c
            L_0x005c:
                r6.set(r1, r3)     // Catch:{ all -> 0x00ff }
            L_0x005f:
                if (r4 == r3) goto L_0x0084
                int r2 = r4.AoV()     // Catch:{ all -> 0x00ff }
                r2 = r2 & r11
                java.lang.Object r10 = r6.get(r2)     // Catch:{ all -> 0x00ff }
                X.0e5 r10 = (X.C07740e5) r10     // Catch:{ all -> 0x00ff }
                com.google.common.collect.MapMakerInternalMap r0 = r13.map     // Catch:{ all -> 0x00ff }
                X.1a3 r1 = r0.A06     // Catch:{ all -> 0x00ff }
                r13.A05()     // Catch:{ all -> 0x00ff }
                X.0e5 r0 = r1.AUU(r13, r4, r10)     // Catch:{ all -> 0x00ff }
                if (r0 == 0) goto L_0x0081
                r6.set(r2, r0)     // Catch:{ all -> 0x00ff }
            L_0x007c:
                X.0e5 r4 = r4.Avg()     // Catch:{ all -> 0x00ff }
                goto L_0x005f
            L_0x0081:
                int r7 = r7 + -1
                goto L_0x007c
            L_0x0084:
                int r5 = r5 + 1
                goto L_0x0032
            L_0x0087:
                r13.table = r6     // Catch:{ all -> 0x00ff }
                r13.count = r7     // Catch:{ all -> 0x00ff }
            L_0x008b:
                int r0 = r13.count     // Catch:{ all -> 0x00ff }
                int r5 = r0 + 1
            L_0x008f:
                java.util.concurrent.atomic.AtomicReferenceArray r4 = r13.table     // Catch:{ all -> 0x00ff }
                int r0 = r4.length()     // Catch:{ all -> 0x00ff }
                int r1 = r0 + -1
                r1 = r1 & r15
                java.lang.Object r6 = r4.get(r1)     // Catch:{ all -> 0x00ff }
                X.0e5 r6 = (X.C07740e5) r6     // Catch:{ all -> 0x00ff }
                r3 = r6
            L_0x009f:
                r8 = 0
                r2 = r16
                if (r3 == 0) goto L_0x00e2
                java.lang.Object r7 = r3.getKey()     // Catch:{ all -> 0x00ff }
                int r0 = r3.AoV()     // Catch:{ all -> 0x00ff }
                if (r0 != r15) goto L_0x00ce
                if (r7 == 0) goto L_0x00ce
                com.google.common.collect.MapMakerInternalMap r0 = r13.map     // Catch:{ all -> 0x00ff }
                com.google.common.base.Equivalence r0 = r0.keyEquivalence     // Catch:{ all -> 0x00ff }
                boolean r0 = r0.equivalent(r14, r7)     // Catch:{ all -> 0x00ff }
                if (r0 == 0) goto L_0x00ce
                java.lang.Object r1 = r3.getValue()     // Catch:{ all -> 0x00ff }
                if (r1 != 0) goto L_0x00d3
                int r0 = r13.modCount     // Catch:{ all -> 0x00ff }
                int r0 = r0 + 1
                r13.modCount = r0     // Catch:{ all -> 0x00ff }
                A03(r13, r3, r2)     // Catch:{ all -> 0x00ff }
                int r0 = r13.count     // Catch:{ all -> 0x00ff }
                r13.count = r0     // Catch:{ all -> 0x00ff }
                goto L_0x00fb
            L_0x00ce:
                X.0e5 r3 = r3.Avg()     // Catch:{ all -> 0x00ff }
                goto L_0x009f
            L_0x00d3:
                if (r17 != 0) goto L_0x00de
                int r0 = r13.modCount     // Catch:{ all -> 0x00ff }
                int r0 = r0 + 1
                r13.modCount = r0     // Catch:{ all -> 0x00ff }
                A03(r13, r3, r2)     // Catch:{ all -> 0x00ff }
            L_0x00de:
                r13.unlock()
                return r1
            L_0x00e2:
                int r0 = r13.modCount     // Catch:{ all -> 0x00ff }
                int r0 = r0 + 1
                r13.modCount = r0     // Catch:{ all -> 0x00ff }
                com.google.common.collect.MapMakerInternalMap r0 = r13.map     // Catch:{ all -> 0x00ff }
                X.1a3 r0 = r0.A06     // Catch:{ all -> 0x00ff }
                r13.A05()     // Catch:{ all -> 0x00ff }
                X.0e5 r0 = r0.BLe(r13, r14, r15, r6)     // Catch:{ all -> 0x00ff }
                A03(r13, r0, r2)     // Catch:{ all -> 0x00ff }
                r4.set(r1, r0)     // Catch:{ all -> 0x00ff }
                r13.count = r5     // Catch:{ all -> 0x00ff }
            L_0x00fb:
                r13.unlock()
                return r8
            L_0x00ff:
                r0 = move-exception
                r13.unlock()
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.MapMakerInternalMap.Segment.A06(java.lang.Object, int, java.lang.Object, boolean):java.lang.Object");
        }
    }

    public enum Strength {
        STRONG {
        },
        WEAK {
        };

        public Equivalence A00() {
            return !(this instanceof AnonymousClass2) ? Equivalence.Equals.INSTANCE : Equivalence.Identity.INSTANCE;
        }
    }

    public final class StrongKeyWeakValueSegment extends Segment {
        public final ReferenceQueue queueForValues = new ReferenceQueue();

        public void A07() {
            do {
            } while (this.queueForValues.poll() != null);
        }

        public void A08() {
            A0B(this.queueForValues);
        }

        public StrongKeyWeakValueSegment(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }

        public Segment A05() {
            return this;
        }
    }

    public final class SerializationProxy<K, V> extends AbstractSerializationProxy<K, V> {
        private static final long serialVersionUID = 3;

        private Object readResolve() {
            return this.A00;
        }

        private void readObject(ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            int readInt = objectInputStream.readInt();
            C07940eQ r1 = new C07940eQ();
            r1.A04(readInt);
            r1.A06(this.keyStrength);
            r1.A07(this.valueStrength);
            r1.A05(this.keyEquivalence);
            r1.A03(this.concurrencyLevel);
            this.A00 = r1.A02();
            while (true) {
                Object readObject = objectInputStream.readObject();
                if (readObject != null) {
                    this.A00.put(readObject, objectInputStream.readObject());
                } else {
                    return;
                }
            }
        }

        private void writeObject(ObjectOutputStream objectOutputStream) {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeInt(this.A00.size());
            for (Map.Entry entry : this.A00.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject(null);
        }

        public SerializationProxy(Strength strength, Strength strength2, Equivalence equivalence, Equivalence equivalence2, int i, ConcurrentMap concurrentMap) {
            super(strength, strength2, equivalence, equivalence2, i, concurrentMap);
        }
    }

    public boolean containsValue(Object obj) {
        Object value;
        Object obj2 = obj;
        if (obj == null) {
            return false;
        }
        Segment[] segmentArr = this.A05;
        long j = -1;
        int i = 0;
        while (i < 3) {
            long j2 = 0;
            for (Segment segment : segmentArr) {
                AtomicReferenceArray atomicReferenceArray = segment.table;
                for (int i2 = 0; i2 < atomicReferenceArray.length(); i2++) {
                    for (C07740e5 r2 = (C07740e5) atomicReferenceArray.get(i2); r2 != null; r2 = r2.Avg()) {
                        Object obj3 = null;
                        if (r2.getKey() == null || (value = r2.getValue()) == null) {
                            Segment.A02(segment);
                        } else {
                            obj3 = value;
                        }
                        if (obj3 != null && this.A06.CLa().A00().equivalent(obj2, obj3)) {
                            return true;
                        }
                    }
                }
                j2 += (long) segment.modCount;
            }
            if (j2 == j) {
                return false;
            }
            i++;
            j = j2;
        }
        return false;
    }

    public final class WeakKeyDummyValueSegment extends Segment {
        public final ReferenceQueue queueForKeys = new ReferenceQueue();

        public WeakKeyDummyValueSegment(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }
    }

    public final class WeakKeyStrongValueSegment extends Segment {
        public final ReferenceQueue queueForKeys = new ReferenceQueue();

        public WeakKeyStrongValueSegment(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }
    }

    public final class WeakKeyWeakValueSegment extends Segment {
        public final ReferenceQueue queueForKeys = new ReferenceQueue();
        public final ReferenceQueue queueForValues = new ReferenceQueue();

        public WeakKeyWeakValueSegment(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }
    }

    public final class StrongKeyDummyValueSegment extends Segment {
        public Segment A05() {
            return this;
        }

        public StrongKeyDummyValueSegment(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }
    }

    public final class StrongKeyStrongValueSegment extends Segment {
        public Segment A05() {
            return this;
        }

        public StrongKeyStrongValueSegment(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }
    }

    public abstract class AbstractSerializationProxy<K, V> extends B7I<K, V> implements Serializable {
        private static final long serialVersionUID = 3;
        public transient ConcurrentMap A00;
        public final int concurrencyLevel;
        public final Equivalence keyEquivalence;
        public final Strength keyStrength;
        public final Equivalence valueEquivalence;
        public final Strength valueStrength;

        public AbstractSerializationProxy(Strength strength, Strength strength2, Equivalence equivalence, Equivalence equivalence2, int i, ConcurrentMap concurrentMap) {
            this.keyStrength = strength;
            this.valueStrength = strength2;
            this.keyEquivalence = equivalence;
            this.valueEquivalence = equivalence2;
            this.concurrencyLevel = i;
            this.A00 = concurrentMap;
        }

        public /* bridge */ /* synthetic */ Map A02() {
            return A02();
        }
    }

    public static int A00(MapMakerInternalMap mapMakerInternalMap, Object obj) {
        int doHash;
        Equivalence equivalence = mapMakerInternalMap.keyEquivalence;
        if (obj == null) {
            doHash = 0;
        } else {
            doHash = equivalence.doHash(obj);
        }
        int i = doHash + ((doHash << 15) ^ -12931);
        int i2 = i ^ (i >>> 10);
        int i3 = i2 + (i2 << 3);
        int i4 = i3 ^ (i3 >>> 6);
        int i5 = i4 + (i4 << 2) + (i4 << 14);
        return i5 ^ (i5 >>> 16);
    }

    public static Segment A01(MapMakerInternalMap mapMakerInternalMap, int i) {
        return mapMakerInternalMap.A05[(i >>> mapMakerInternalMap.A04) & mapMakerInternalMap.A03];
    }

    public static ArrayList A02(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        C24931Xr.A09(arrayList, collection.iterator());
        return arrayList;
    }

    public void clear() {
        Segment[] segmentArr = this.A05;
        int length = segmentArr.length;
        for (int i = 0; i < length; i++) {
            Segment segment = segmentArr[i];
            if (segment.count != 0) {
                segment.lock();
                try {
                    AtomicReferenceArray atomicReferenceArray = segment.table;
                    for (int i2 = 0; i2 < atomicReferenceArray.length(); i2++) {
                        atomicReferenceArray.set(i2, null);
                    }
                    segment.A07();
                    segment.readCount.set(0);
                    segment.modCount++;
                    segment.count = 0;
                } finally {
                    segment.unlock();
                }
            }
        }
    }

    public boolean containsKey(Object obj) {
        C07740e5 A042;
        if (obj == null) {
            return false;
        }
        int A002 = A00(this, obj);
        Segment A012 = A01(this, A002);
        try {
            boolean z = false;
            if (!(A012.count == 0 || (A042 = A012.A04(obj, A002)) == null || A042.getValue() == null)) {
                z = true;
            }
            return z;
        } finally {
            A012.A09();
        }
    }

    public Set entrySet() {
        Set set = this.A01;
        if (set != null) {
            return set;
        }
        B7Y b7y = new B7Y(this);
        this.A01 = b7y;
        return b7y;
    }

    public Object get(Object obj) {
        if (obj == null) {
            return null;
        }
        int A002 = A00(this, obj);
        Segment A012 = A01(this, A002);
        try {
            C07740e5 A042 = A012.A04(obj, A002);
            if (A042 == null) {
                return null;
            }
            Object value = A042.getValue();
            if (value == null) {
                Segment.A02(A012);
            }
            A012.A09();
            return value;
        } finally {
            A012.A09();
        }
    }

    public boolean isEmpty() {
        Segment[] segmentArr = this.A05;
        long j = 0;
        int i = 0;
        while (true) {
            int length = segmentArr.length;
            if (i < length) {
                if (segmentArr[i].count != 0) {
                    break;
                }
                j += (long) segmentArr[i].modCount;
                i++;
            } else if (j == 0) {
                return true;
            } else {
                int i2 = 0;
                while (true) {
                    if (i2 < length) {
                        if (segmentArr[i2].count != 0) {
                            break;
                        }
                        j -= (long) segmentArr[i2].modCount;
                        i2++;
                    } else if (j != 0) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Set keySet() {
        Set set = this.A02;
        if (set != null) {
            return set;
        }
        C51862hz r0 = new C51862hz(this);
        this.A02 = r0;
        return r0;
    }

    public int size() {
        Segment[] segmentArr = this.A05;
        long j = 0;
        for (Segment segment : segmentArr) {
            j += (long) segment.count;
        }
        return C06950cM.A01(j);
    }

    public Collection values() {
        Collection collection = this.A00;
        if (collection != null) {
            return collection;
        }
        B7X b7x = new B7X(this);
        this.A00 = b7x;
        return b7x;
    }

    public Object writeReplace() {
        C25571a3 r0 = this.A06;
        return new SerializationProxy(r0.BHg(), r0.CLa(), this.keyEquivalence, r0.CLa().A00(), this.concurrencyLevel, this);
    }

    public MapMakerInternalMap(C07940eQ r7, C25571a3 r8) {
        int i = r7.A03;
        this.concurrencyLevel = Math.min(i == -1 ? 4 : i, 65536);
        this.keyEquivalence = (Equivalence) MoreObjects.firstNonNull(r7.A05, r7.A00().A00());
        this.A06 = r8;
        int i2 = r7.A04;
        int min = Math.min(i2 == -1 ? 16 : i2, 1073741824);
        int i3 = 0;
        int i4 = 1;
        int i5 = 1;
        int i6 = 0;
        while (i5 < this.concurrencyLevel) {
            i6++;
            i5 <<= 1;
        }
        this.A04 = 32 - i6;
        this.A03 = i5 - 1;
        this.A05 = new Segment[i5];
        int i7 = min / i5;
        while (i4 < (i5 * i7 < min ? i7 + 1 : i7)) {
            i4 <<= 1;
        }
        while (true) {
            Segment[] segmentArr = this.A05;
            if (i3 < segmentArr.length) {
                segmentArr[i3] = this.A06.BLh(this, i4, -1);
                i3++;
            } else {
                return;
            }
        }
    }

    public Object put(Object obj, Object obj2) {
        Preconditions.checkNotNull(obj);
        Preconditions.checkNotNull(obj2);
        int A002 = A00(this, obj);
        return A01(this, A002).A06(obj, A002, obj2, false);
    }

    public void putAll(Map map) {
        for (Map.Entry entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public Object putIfAbsent(Object obj, Object obj2) {
        Preconditions.checkNotNull(obj);
        Preconditions.checkNotNull(obj2);
        int A002 = A00(this, obj);
        return A01(this, A002).A06(obj, A002, obj2, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        if (r0 != false) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object remove(java.lang.Object r10) {
        /*
            r9 = this;
            if (r10 != 0) goto L_0x0004
            r0 = 0
            return r0
        L_0x0004:
            int r2 = A00(r9, r10)
            com.google.common.collect.MapMakerInternalMap$Segment r4 = A01(r9, r2)
            r4.lock()
            com.google.common.collect.MapMakerInternalMap.Segment.A01(r4)     // Catch:{ all -> 0x006c }
            java.util.concurrent.atomic.AtomicReferenceArray r5 = r4.table     // Catch:{ all -> 0x006c }
            int r0 = r5.length()     // Catch:{ all -> 0x006c }
            int r3 = r0 + -1
            r3 = r3 & r2
            java.lang.Object r7 = r5.get(r3)     // Catch:{ all -> 0x006c }
            X.0e5 r7 = (X.C07740e5) r7     // Catch:{ all -> 0x006c }
            r6 = r7
        L_0x0022:
            r8 = 0
            if (r6 == 0) goto L_0x0068
            java.lang.Object r1 = r6.getKey()     // Catch:{ all -> 0x006c }
            int r0 = r6.AoV()     // Catch:{ all -> 0x006c }
            if (r0 != r2) goto L_0x0049
            if (r1 == 0) goto L_0x0049
            com.google.common.collect.MapMakerInternalMap r0 = r4.map     // Catch:{ all -> 0x006c }
            com.google.common.base.Equivalence r0 = r0.keyEquivalence     // Catch:{ all -> 0x006c }
            boolean r0 = r0.equivalent(r10, r1)     // Catch:{ all -> 0x006c }
            if (r0 == 0) goto L_0x0049
            java.lang.Object r2 = r6.getValue()     // Catch:{ all -> 0x006c }
            if (r2 != 0) goto L_0x0051
            java.lang.Object r1 = r6.getValue()     // Catch:{ all -> 0x006c }
            r0 = 0
            if (r1 != 0) goto L_0x004f
            goto L_0x004e
        L_0x0049:
            X.0e5 r6 = r6.Avg()     // Catch:{ all -> 0x006c }
            goto L_0x0022
        L_0x004e:
            r0 = 1
        L_0x004f:
            if (r0 == 0) goto L_0x0068
        L_0x0051:
            int r0 = r4.modCount     // Catch:{ all -> 0x006c }
            int r0 = r0 + 1
            r4.modCount = r0     // Catch:{ all -> 0x006c }
            X.0e5 r1 = com.google.common.collect.MapMakerInternalMap.Segment.A00(r4, r7, r6)     // Catch:{ all -> 0x006c }
            int r0 = r4.count     // Catch:{ all -> 0x006c }
            int r0 = r0 + -1
            r5.set(r3, r1)     // Catch:{ all -> 0x006c }
            r4.count = r0     // Catch:{ all -> 0x006c }
            r4.unlock()
            return r2
        L_0x0068:
            r4.unlock()
            return r8
        L_0x006c:
            r0 = move-exception
            r4.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.MapMakerInternalMap.remove(java.lang.Object):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0061, code lost:
        if (r0 != false) goto L_0x0063;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean remove(java.lang.Object r11, java.lang.Object r12) {
        /*
            r10 = this;
            if (r11 == 0) goto L_0x007d
            if (r12 == 0) goto L_0x007d
            int r9 = A00(r10, r11)
            com.google.common.collect.MapMakerInternalMap$Segment r3 = A01(r10, r9)
            r3.lock()
            com.google.common.collect.MapMakerInternalMap.Segment.A01(r3)     // Catch:{ all -> 0x0078 }
            java.util.concurrent.atomic.AtomicReferenceArray r4 = r3.table     // Catch:{ all -> 0x0078 }
            int r2 = r4.length()     // Catch:{ all -> 0x0078 }
            r8 = 1
            int r2 = r2 - r8
            r2 = r2 & r9
            java.lang.Object r6 = r4.get(r2)     // Catch:{ all -> 0x0078 }
            X.0e5 r6 = (X.C07740e5) r6     // Catch:{ all -> 0x0078 }
            r5 = r6
        L_0x0022:
            r7 = 0
            if (r5 == 0) goto L_0x0074
            java.lang.Object r1 = r5.getKey()     // Catch:{ all -> 0x0078 }
            int r0 = r5.AoV()     // Catch:{ all -> 0x0078 }
            if (r0 != r9) goto L_0x0052
            if (r1 == 0) goto L_0x0052
            com.google.common.collect.MapMakerInternalMap r0 = r3.map     // Catch:{ all -> 0x0078 }
            com.google.common.base.Equivalence r0 = r0.keyEquivalence     // Catch:{ all -> 0x0078 }
            boolean r0 = r0.equivalent(r11, r1)     // Catch:{ all -> 0x0078 }
            if (r0 == 0) goto L_0x0052
            java.lang.Object r1 = r5.getValue()     // Catch:{ all -> 0x0078 }
            com.google.common.collect.MapMakerInternalMap r0 = r3.map     // Catch:{ all -> 0x0078 }
            X.1a3 r0 = r0.A06     // Catch:{ all -> 0x0078 }
            com.google.common.collect.MapMakerInternalMap$Strength r0 = r0.CLa()     // Catch:{ all -> 0x0078 }
            com.google.common.base.Equivalence r0 = r0.A00()     // Catch:{ all -> 0x0078 }
            boolean r0 = r0.equivalent(r12, r1)     // Catch:{ all -> 0x0078 }
            if (r0 == 0) goto L_0x0059
            goto L_0x0057
        L_0x0052:
            X.0e5 r5 = r5.Avg()     // Catch:{ all -> 0x0078 }
            goto L_0x0022
        L_0x0057:
            r7 = 1
            goto L_0x0063
        L_0x0059:
            java.lang.Object r1 = r5.getValue()     // Catch:{ all -> 0x0078 }
            r0 = 0
            if (r1 != 0) goto L_0x0061
            r0 = 1
        L_0x0061:
            if (r0 == 0) goto L_0x0074
        L_0x0063:
            int r0 = r3.modCount     // Catch:{ all -> 0x0078 }
            int r0 = r0 + r8
            r3.modCount = r0     // Catch:{ all -> 0x0078 }
            X.0e5 r1 = com.google.common.collect.MapMakerInternalMap.Segment.A00(r3, r6, r5)     // Catch:{ all -> 0x0078 }
            int r0 = r3.count     // Catch:{ all -> 0x0078 }
            int r0 = r0 - r8
            r4.set(r2, r1)     // Catch:{ all -> 0x0078 }
            r3.count = r0     // Catch:{ all -> 0x0078 }
        L_0x0074:
            r3.unlock()
            return r7
        L_0x0078:
            r0 = move-exception
            r3.unlock()
            throw r0
        L_0x007d:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.MapMakerInternalMap.remove(java.lang.Object, java.lang.Object):boolean");
    }

    public Object replace(Object obj, Object obj2) {
        Preconditions.checkNotNull(obj);
        Preconditions.checkNotNull(obj2);
        int A002 = A00(this, obj);
        Segment A012 = A01(this, A002);
        A012.lock();
        try {
            Segment.A01(A012);
            AtomicReferenceArray atomicReferenceArray = A012.table;
            int length = (atomicReferenceArray.length() - 1) & A002;
            C07740e5 r6 = (C07740e5) atomicReferenceArray.get(length);
            C07740e5 r5 = r6;
            while (true) {
                if (r5 == null) {
                    break;
                }
                Object key = r5.getKey();
                if (r5.AoV() != A002 || key == null || !A012.map.keyEquivalence.equivalent(obj, key)) {
                    r5 = r5.Avg();
                } else {
                    Object value = r5.getValue();
                    if (value == null) {
                        boolean z = false;
                        if (r5.getValue() == null) {
                            z = true;
                        }
                        if (z) {
                            A012.modCount++;
                            atomicReferenceArray.set(length, Segment.A00(A012, r6, r5));
                            A012.count--;
                        }
                    } else {
                        A012.modCount++;
                        Segment.A03(A012, r5, obj2);
                        A012.unlock();
                        return value;
                    }
                }
            }
            return null;
        } finally {
            A012.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean replace(Object obj, Object obj2, Object obj3) {
        Preconditions.checkNotNull(obj);
        Preconditions.checkNotNull(obj3);
        if (obj2 == null) {
            return false;
        }
        int A002 = A00(this, obj);
        Segment A012 = A01(this, A002);
        A012.lock();
        try {
            Segment.A01(A012);
            AtomicReferenceArray atomicReferenceArray = A012.table;
            int length = (atomicReferenceArray.length() - 1) & A002;
            C07740e5 r6 = (C07740e5) atomicReferenceArray.get(length);
            C07740e5 r3 = r6;
            while (true) {
                if (r3 == null) {
                    break;
                }
                Object key = r3.getKey();
                if (r3.AoV() != A002 || key == null || !A012.map.keyEquivalence.equivalent(obj, key)) {
                    r3 = r3.Avg();
                } else {
                    Object value = r3.getValue();
                    if (value == null) {
                        boolean z = false;
                        if (r3.getValue() == null) {
                            z = true;
                        }
                        if (z) {
                            A012.modCount++;
                            atomicReferenceArray.set(length, Segment.A00(A012, r6, r3));
                            A012.count--;
                        }
                    } else if (A012.map.A06.CLa().A00().equivalent(obj2, value)) {
                        A012.modCount++;
                        Segment.A03(A012, r3, obj3);
                        A012.unlock();
                        return true;
                    }
                }
            }
            A012.unlock();
            return false;
        } catch (Throwable th) {
            A012.unlock();
            throw th;
        }
    }
}
