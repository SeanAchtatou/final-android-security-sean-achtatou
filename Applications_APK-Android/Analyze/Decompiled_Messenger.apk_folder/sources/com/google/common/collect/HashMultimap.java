package com.google.common.collect;

import X.AnonymousClass0TG;
import X.AnonymousClass0V0;
import X.C22684B7r;
import com.google.common.base.Preconditions;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public final class HashMultimap extends HashMultimapGwtSerializationDependencies {
    private static final long serialVersionUID = 0;
    public transient int A00 = 2;

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.A00 = 2;
        int readInt = objectInputStream.readInt();
        A0E(AnonymousClass0TG.A04());
        C22684B7r.A01(this, objectInputStream, readInt);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        C22684B7r.A02(this, objectOutputStream);
    }

    public HashMultimap() {
        super(new HashMap());
    }

    public HashMultimap(int i, int i2) {
        super(new HashMap(AnonymousClass0TG.A00(i)));
        Preconditions.checkArgument(i2 >= 0);
        this.A00 = i2;
    }

    public HashMultimap(AnonymousClass0V0 r3) {
        super(new HashMap(AnonymousClass0TG.A00(r3.keySet().size())));
        A03(r3);
    }
}
