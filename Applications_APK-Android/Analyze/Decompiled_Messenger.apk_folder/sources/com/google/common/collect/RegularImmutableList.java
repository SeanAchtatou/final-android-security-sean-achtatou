package com.google.common.collect;

import X.C24931Xr;
import X.C24961Xu;
import com.google.common.base.Preconditions;

public class RegularImmutableList<E> extends ImmutableList<E> {
    public static final ImmutableList A02 = new RegularImmutableList(new Object[0], 0);
    public final transient Object[] A00;
    private final transient int A01;

    public int copyIntoArray(Object[] objArr, int i) {
        System.arraycopy(this.A00, 0, objArr, i, this.A01);
        return i + this.A01;
    }

    public Object get(int i) {
        Preconditions.checkElementIndex(i, this.A01);
        return this.A00[i];
    }

    public int size() {
        return this.A01;
    }

    public RegularImmutableList(Object[] objArr, int i) {
        this.A00 = objArr;
        this.A01 = i;
    }

    public C24961Xu listIterator(int i) {
        return C24931Xr.A06(this.A00, 0, this.A01, i);
    }
}
