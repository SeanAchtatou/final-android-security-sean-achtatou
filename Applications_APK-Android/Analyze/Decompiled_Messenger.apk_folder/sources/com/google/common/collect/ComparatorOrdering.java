package com.google.common.collect;

import X.C181810n;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.Comparator;

public final class ComparatorOrdering extends C181810n implements Serializable {
    private static final long serialVersionUID = 0;
    public final Comparator comparator;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ComparatorOrdering) {
            return this.comparator.equals(((ComparatorOrdering) obj).comparator);
        }
        return false;
    }

    public int hashCode() {
        return this.comparator.hashCode();
    }

    public String toString() {
        return this.comparator.toString();
    }

    public ComparatorOrdering(Comparator comparator2) {
        Preconditions.checkNotNull(comparator2);
        this.comparator = comparator2;
    }
}
