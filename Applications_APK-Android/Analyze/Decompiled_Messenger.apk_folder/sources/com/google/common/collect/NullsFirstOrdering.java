package com.google.common.collect;

import X.C181810n;
import java.io.Serializable;

public final class NullsFirstOrdering extends C181810n implements Serializable {
    private static final long serialVersionUID = 0;
    public final C181810n ordering;

    public C181810n A02() {
        return this;
    }

    public C181810n A03() {
        return this.ordering.A03();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof NullsFirstOrdering) {
            return this.ordering.equals(((NullsFirstOrdering) obj).ordering);
        }
        return false;
    }

    public int hashCode() {
        return this.ordering.hashCode() ^ 957692532;
    }

    public String toString() {
        return this.ordering + ".nullsFirst()";
    }

    public NullsFirstOrdering(C181810n r1) {
        this.ordering = r1;
    }
}
