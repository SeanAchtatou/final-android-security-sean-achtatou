package com.google.common.collect;

import X.AnonymousClass0V1;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class AbstractListMultimap extends AbstractMapBasedMultimap implements AnonymousClass0V1 {
    private static final long serialVersionUID = 6588350623831699109L;

    public List A0F() {
        return new ArrayList(((ArrayListMultimap) this).A00);
    }

    /* renamed from: AbL */
    public List AbK(Object obj) {
        return (List) super.AbK(obj);
    }

    /* renamed from: C1O */
    public List C1N(Object obj) {
        return (List) super.C1N(obj);
    }

    public /* bridge */ /* synthetic */ Collection C2O(Object obj, Iterable iterable) {
        return (List) super.C2O(obj, iterable);
    }

    public AbstractListMultimap(Map map) {
        super(map);
    }
}
