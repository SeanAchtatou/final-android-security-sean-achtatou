package com.google.common.collect;

import X.C24971Xv;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableMap;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.Collection;

public abstract class ImmutableCollection<E> extends AbstractCollection<E> implements Serializable {
    private static final Object[] A00 = new Object[0];

    public boolean A0G() {
        ImmutableCollection immutableCollection;
        if (this instanceof ImmutableMultimap.Values) {
            return true;
        }
        if (this instanceof ImmutableMultimap.EntryCollection) {
            return ((ImmutableMultimap.EntryCollection) this).multimap.A01.isPartialView();
        }
        if ((this instanceof RegularImmutableMap.KeysOrValuesAsList) || (this instanceof RegularImmutableMap.EntrySet.AnonymousClass1)) {
            return true;
        }
        if (this instanceof RegularImmutableList) {
            return false;
        }
        if (this instanceof ImmutableSet.Indexed.AnonymousClass1) {
            immutableCollection = ImmutableSet.Indexed.this;
        } else if (this instanceof ImmutableList.SubList) {
            return true;
        } else {
            if (this instanceof ImmutableList.ReverseImmutableList) {
                immutableCollection = ((ImmutableList.ReverseImmutableList) this).A00;
            } else if (this instanceof ImmutableAsList) {
                immutableCollection = ((ImmutableAsList) this).A0H();
            } else if ((this instanceof SingletonImmutableSet) || (this instanceof RegularImmutableSet)) {
                return false;
            } else {
                boolean z = this instanceof RegularImmutableMap.KeySet;
                return true;
            }
        }
        return immutableCollection.A0G();
    }

    public abstract boolean contains(Object obj);

    public abstract C24971Xv iterator();

    public final boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public Object writeReplace() {
        return new ImmutableList.SerializedForm(toArray());
    }

    public ImmutableList asList() {
        if (isEmpty()) {
            return RegularImmutableList.A02;
        }
        return ImmutableList.asImmutableList(toArray());
    }

    public int copyIntoArray(Object[] objArr, int i) {
        C24971Xv it = iterator();
        while (it.hasNext()) {
            objArr[i] = it.next();
            i++;
        }
        return i;
    }

    public final Object[] toArray() {
        int size = size();
        if (size == 0) {
            return A00;
        }
        Object[] objArr = new Object[size];
        copyIntoArray(objArr, 0);
        return objArr;
    }

    public final Object[] toArray(Object[] objArr) {
        Preconditions.checkNotNull(objArr);
        int size = size();
        int length = objArr.length;
        if (length < size) {
            objArr = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), size);
        } else if (length > size) {
            objArr[size] = null;
        }
        copyIntoArray(objArr, 0);
        return objArr;
    }
}
