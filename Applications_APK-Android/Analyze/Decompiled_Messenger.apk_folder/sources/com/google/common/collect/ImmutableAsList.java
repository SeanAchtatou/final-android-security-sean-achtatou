package com.google.common.collect;

import java.io.Serializable;

public abstract class ImmutableAsList<E> extends ImmutableList<E> {

    public class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        public final ImmutableCollection collection;

        public Object readResolve() {
            return this.collection.asList();
        }

        public SerializedForm(ImmutableCollection immutableCollection) {
            this.collection = immutableCollection;
        }
    }

    public abstract ImmutableCollection A0H();

    public Object writeReplace() {
        return new SerializedForm(A0H());
    }

    public boolean contains(Object obj) {
        return A0H().contains(obj);
    }

    public boolean isEmpty() {
        return A0H().isEmpty();
    }

    public int size() {
        return A0H().size();
    }
}
