package com.google.common.collect;

public class EmptyImmutableListMultimap extends ImmutableListMultimap {
    public static final EmptyImmutableListMultimap A00 = new EmptyImmutableListMultimap();
    private static final long serialVersionUID = 0;

    private EmptyImmutableListMultimap() {
        super(RegularImmutableMap.A03, 0);
    }

    private Object readResolve() {
        return A00;
    }
}
