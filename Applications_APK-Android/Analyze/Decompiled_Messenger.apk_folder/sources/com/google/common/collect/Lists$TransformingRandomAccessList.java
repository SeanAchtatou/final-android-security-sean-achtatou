package com.google.common.collect;

import X.AnonymousClass0t2;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public class Lists$TransformingRandomAccessList<F, T> extends AbstractList<T> implements RandomAccess, Serializable {
    private static final long serialVersionUID = 0;
    public final List fromList;
    public final Function function;

    public void clear() {
        this.fromList.clear();
    }

    public Object get(int i) {
        return this.function.apply(this.fromList.get(i));
    }

    public boolean isEmpty() {
        return this.fromList.isEmpty();
    }

    public ListIterator listIterator(int i) {
        return new AnonymousClass0t2(this, this.fromList.listIterator(i));
    }

    public Object remove(int i) {
        return this.function.apply(this.fromList.remove(i));
    }

    public int size() {
        return this.fromList.size();
    }

    public Lists$TransformingRandomAccessList(List list, Function function2) {
        Preconditions.checkNotNull(list);
        this.fromList = list;
        Preconditions.checkNotNull(function2);
        this.function = function2;
    }

    public Iterator iterator() {
        return listIterator();
    }
}
