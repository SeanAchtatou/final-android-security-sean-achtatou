package com.google.common.collect;

import X.AnonymousClass08S;
import X.AnonymousClass1Y1;
import X.C24971Xv;
import com.google.common.base.Preconditions;

public final class SingletonImmutableSet<E> extends ImmutableSet<E> {
    public transient int A00;
    public final transient Object A01;

    public int size() {
        return 1;
    }

    public ImmutableList A0H() {
        return ImmutableList.of(this.A01);
    }

    public boolean contains(Object obj) {
        return this.A01.equals(obj);
    }

    public int copyIntoArray(Object[] objArr, int i) {
        objArr[i] = this.A01;
        return i + 1;
    }

    public final int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int hashCode = this.A01.hashCode();
        this.A00 = hashCode;
        return hashCode;
    }

    public String toString() {
        return AnonymousClass08S.A0K("[", this.A01.toString(), ']');
    }

    public SingletonImmutableSet(Object obj) {
        Preconditions.checkNotNull(obj);
        this.A01 = obj;
    }

    public SingletonImmutableSet(Object obj, int i) {
        this.A01 = obj;
        this.A00 = i;
    }

    public C24971Xv iterator() {
        return new AnonymousClass1Y1(this.A01);
    }
}
