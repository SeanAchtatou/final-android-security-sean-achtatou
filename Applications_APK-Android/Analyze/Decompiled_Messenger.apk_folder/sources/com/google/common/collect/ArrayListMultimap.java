package com.google.common.collect;

import X.AnonymousClass0TG;
import X.C22298Ase;
import X.C22684B7r;
import X.C25001Xy;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public final class ArrayListMultimap extends ArrayListMultimapGwtSerializationDependencies {
    private static final long serialVersionUID = 0;
    public transient int A00;

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.A00 = 3;
        int readInt = objectInputStream.readInt();
        A0E(AnonymousClass0TG.A04());
        C22684B7r.A01(this, objectInputStream, readInt);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        C22684B7r.A02(this, objectOutputStream);
    }

    public ArrayListMultimap() {
        super(new HashMap());
        this.A00 = 3;
    }

    public ArrayListMultimap(int i, int i2) {
        super(new HashMap(AnonymousClass0TG.A00(i)));
        C25001Xy.A00(i2, C22298Ase.$const$string(11));
        this.A00 = i2;
    }
}
