package com.google.common.collect;

import X.AnonymousClass0TG;
import X.C30141hY;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class AbstractSetMultimap extends AbstractMapBasedMultimap implements C30141hY {
    private static final long serialVersionUID = 7431625294878419160L;

    public Set A0F() {
        return new HashSet(AnonymousClass0TG.A00(((HashMultimap) this).A00));
    }

    public Set A0G(Object obj, Iterable iterable) {
        return (Set) super.C2O(obj, iterable);
    }

    /* renamed from: AYk */
    public Set AYj() {
        return (Set) super.AYj();
    }

    /* renamed from: AbM */
    public Set AbK(Object obj) {
        return (Set) super.AbK(obj);
    }

    /* renamed from: C1P */
    public Set C1N(Object obj) {
        return (Set) super.C1N(obj);
    }

    public /* bridge */ /* synthetic */ Collection C2O(Object obj, Iterable iterable) {
        return (Set) super.C2O(obj, iterable);
    }

    public AbstractSetMultimap(Map map) {
        super(map);
    }
}
