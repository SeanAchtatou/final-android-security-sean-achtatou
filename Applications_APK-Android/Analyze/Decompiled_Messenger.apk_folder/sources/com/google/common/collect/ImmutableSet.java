package com.google.common.collect;

import X.AnonymousClass0U8;
import X.B8L;
import X.C07410dQ;
import X.C07910eN;
import X.C24971Xv;
import X.C25011Xz;
import com.google.common.base.Preconditions;
import com.google.common.collect.RegularImmutableMultiset;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;

public abstract class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E> {
    private transient ImmutableList A00;

    public class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        public final Object[] elements;

        public Object readResolve() {
            return ImmutableSet.A0B(this.elements);
        }

        public SerializedForm(Object[] objArr) {
            this.elements = objArr;
        }
    }

    public abstract class Indexed<E> extends ImmutableSet<E> {
        public Object A0J(int i) {
            if (!(this instanceof RegularImmutableMultiset.ElementSet)) {
                return ImmutableMultiset.this.A0J(i);
            }
            B8L b8l = RegularImmutableMultiset.this.A01;
            Preconditions.checkElementIndex(i, b8l.A01);
            return b8l.A05[i];
        }

        public ImmutableList A0H() {
            return new ImmutableList<E>() {
                public Object get(int i) {
                    return Indexed.this.A0J(i);
                }

                public int size() {
                    return Indexed.this.size();
                }
            };
        }

        public C24971Xv iterator() {
            return asList().iterator();
        }
    }

    public static int A00(int i) {
        boolean z = true;
        if (i < 751619276) {
            int highestOneBit = Integer.highestOneBit(i - 1) << 1;
            while (((double) highestOneBit) * 0.7d < ((double) i)) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        if (i >= 1073741824) {
            z = false;
        }
        Preconditions.checkArgument(z, "collection too large");
        return 1073741824;
    }

    public static ImmutableSet A02(int i, Object... objArr) {
        Object[] objArr2 = objArr;
        if (i == 0) {
            return RegularImmutableSet.A05;
        }
        if (i == 1) {
            return A04(objArr[0]);
        }
        int A002 = A00(i);
        Object[] objArr3 = new Object[A002];
        int i2 = A002 - 1;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i; i5++) {
            Object obj = objArr[i5];
            AnonymousClass0U8.A00(obj, i5);
            int hashCode = obj.hashCode();
            int A003 = C07910eN.A00(hashCode);
            while (true) {
                int i6 = A003 & i2;
                Object obj2 = objArr3[i6];
                if (obj2 != null) {
                    if (obj2.equals(obj)) {
                        break;
                    }
                    A003++;
                } else {
                    objArr[i4] = obj;
                    objArr3[i6] = obj;
                    i3 += hashCode;
                    i4++;
                    break;
                }
            }
        }
        Arrays.fill(objArr, i4, i, (Object) null);
        if (i4 == 1) {
            return new SingletonImmutableSet(objArr[0], i3);
        }
        if (A00(i4) < (A002 >> 1)) {
            return A02(i4, objArr);
        }
        if (i4 < (objArr.length >> 1)) {
            objArr2 = Arrays.copyOf(objArr, i4);
        }
        return new RegularImmutableSet(objArr2, i3, objArr3, i2, i4);
    }

    public static ImmutableSet A05(Object obj, Object obj2) {
        return A02(2, obj, obj2);
    }

    public static ImmutableSet A06(Object obj, Object obj2, Object obj3) {
        return A02(3, obj, obj2, obj3);
    }

    public static ImmutableSet A07(Object obj, Object obj2, Object obj3, Object obj4) {
        return A02(4, obj, obj2, obj3, obj4);
    }

    public static ImmutableSet A08(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        return A02(5, obj, obj2, obj3, obj4, obj5);
    }

    public static ImmutableSet A09(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object... objArr) {
        int length = objArr.length;
        int i = length + 6;
        Object[] objArr2 = new Object[i];
        objArr2[0] = obj;
        objArr2[1] = obj2;
        objArr2[2] = obj3;
        objArr2[3] = obj4;
        objArr2[4] = obj5;
        objArr2[5] = obj6;
        System.arraycopy(objArr, 0, objArr2, 6, length);
        return A02(i, objArr2);
    }

    public static ImmutableSet A0B(Object[] objArr) {
        int length = objArr.length;
        if (length == 0) {
            return RegularImmutableSet.A05;
        }
        if (length != 1) {
            return A02(length, (Object[]) objArr.clone());
        }
        return A04(objArr[0]);
    }

    public boolean A0I() {
        return !(this instanceof SingletonImmutableSet) ? this instanceof RegularImmutableSet : ((SingletonImmutableSet) this).A00 != 0;
    }

    public abstract C24971Xv iterator();

    public static C07410dQ A01() {
        return new C07410dQ();
    }

    public static ImmutableSet A03(Iterable iterable) {
        if (iterable instanceof Collection) {
            return A0A((Collection) iterable);
        }
        Iterator it = iterable.iterator();
        if (!it.hasNext()) {
            return RegularImmutableSet.A05;
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return A04(next);
        }
        C07410dQ r0 = new C07410dQ();
        r0.A01(next);
        r0.A02(it);
        return r0.build();
    }

    public static ImmutableSet A04(Object obj) {
        return new SingletonImmutableSet(obj);
    }

    public static ImmutableSet A0A(Collection collection) {
        if ((collection instanceof ImmutableSet) && !(collection instanceof SortedSet)) {
            ImmutableSet immutableSet = (ImmutableSet) collection;
            if (!immutableSet.A0G()) {
                return immutableSet;
            }
        }
        Object[] array = collection.toArray();
        return A02(array.length, array);
    }

    public ImmutableList asList() {
        ImmutableList immutableList = this.A00;
        if (immutableList != null) {
            return immutableList;
        }
        ImmutableList A0H = A0H();
        this.A00 = A0H;
        return A0H;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ImmutableSet) || !A0I() || !((ImmutableSet) obj).A0I() || hashCode() == obj.hashCode()) {
            return C25011Xz.A0A(this, obj);
        }
        return false;
    }

    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public ImmutableList A0H() {
        return ImmutableList.asImmutableList(toArray());
    }

    public int hashCode() {
        return C25011Xz.A00(this);
    }
}
