package com.google.common.collect;

import X.C07910eN;
import X.C24971Xv;
import X.C25001Xy;
import com.google.common.base.Preconditions;
import java.util.AbstractMap;
import java.util.Map;

public final class RegularImmutableMap<K, V> extends ImmutableMap<K, V> {
    public static final ImmutableMap A03 = new RegularImmutableMap(null, new Object[0], 0);
    private static final long serialVersionUID = 0;
    private final transient int A00;
    private final transient int[] A01;
    private final transient Object[] A02;

    public class EntrySet<K, V> extends ImmutableSet<Map.Entry<K, V>> {
        public final transient int A00;
        public final transient int A01;
        public final transient Object[] A02;
        private final transient ImmutableMap A03;

        public ImmutableList A0H() {
            return new ImmutableList<Map.Entry<K, V>>() {
                public Object get(int i) {
                    Preconditions.checkElementIndex(i, EntrySet.this.A01);
                    EntrySet entrySet = EntrySet.this;
                    Object[] objArr = entrySet.A02;
                    int i2 = i << 1;
                    int i3 = entrySet.A00;
                    return new AbstractMap.SimpleImmutableEntry(objArr[i3 + i2], objArr[i2 + (i3 ^ 1)]);
                }

                public int size() {
                    return EntrySet.this.A01;
                }
            };
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object value = entry.getValue();
            if (value == null || !value.equals(this.A03.get(key))) {
                return false;
            }
            return true;
        }

        public int size() {
            return this.A01;
        }

        public EntrySet(ImmutableMap immutableMap, Object[] objArr, int i, int i2) {
            this.A03 = immutableMap;
            this.A02 = objArr;
            this.A00 = i;
            this.A01 = i2;
        }

        public C24971Xv iterator() {
            return asList().iterator();
        }
    }

    public final class KeySet<K> extends ImmutableSet<K> {
        private final transient ImmutableList A00;
        private final transient ImmutableMap A01;

        public ImmutableList asList() {
            return this.A00;
        }

        public boolean contains(Object obj) {
            if (this.A01.get(obj) != null) {
                return true;
            }
            return false;
        }

        public int size() {
            return this.A01.size();
        }

        public KeySet(ImmutableMap immutableMap, ImmutableList immutableList) {
            this.A01 = immutableMap;
            this.A00 = immutableList;
        }

        public C24971Xv iterator() {
            return asList().iterator();
        }
    }

    public final class KeysOrValuesAsList extends ImmutableList<Object> {
        private final transient int A00;
        private final transient int A01;
        private final transient Object[] A02;

        public Object get(int i) {
            Preconditions.checkElementIndex(i, this.A01);
            return this.A02[(i << 1) + this.A00];
        }

        public int size() {
            return this.A01;
        }

        public KeysOrValuesAsList(Object[] objArr, int i, int i2) {
            this.A02 = objArr;
            this.A00 = i;
            this.A01 = i2;
        }
    }

    public static Object A01(int[] iArr, Object[] objArr, int i, int i2, Object obj) {
        if (obj == null) {
            return null;
        }
        if (i == 1) {
            if (objArr[i2].equals(obj)) {
                return objArr[i2 ^ 1];
            }
            return null;
        } else if (iArr == null) {
            return null;
        } else {
            int length = iArr.length - 1;
            int A002 = C07910eN.A00(obj.hashCode());
            while (true) {
                int i3 = A002 & length;
                int i4 = iArr[i3];
                if (i4 == -1) {
                    return null;
                }
                if (objArr[i4].equals(obj)) {
                    return objArr[i4 ^ 1];
                }
                A002 = i3 + 1;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0034, code lost:
        r8[r1] = r2;
        r3 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int[] A02(java.lang.Object[] r10, int r11, int r12, int r13) {
        /*
            r0 = 1
            if (r11 != r0) goto L_0x000e
            r1 = r10[r13]
            r0 = r13 ^ 1
            r0 = r10[r0]
            X.C25001Xy.A03(r1, r0)
            r0 = 0
            return r0
        L_0x000e:
            int r9 = r12 + -1
            int[] r8 = new int[r12]
            r7 = -1
            java.util.Arrays.fill(r8, r7)
            r3 = 0
        L_0x0017:
            if (r3 >= r11) goto L_0x0074
            int r1 = r3 << 1
            int r2 = r1 + r13
            r6 = r10[r2]
            r0 = r13 ^ 1
            int r1 = r1 + r0
            r5 = r10[r1]
            X.C25001Xy.A03(r6, r5)
            int r0 = r6.hashCode()
            int r1 = X.C07910eN.A00(r0)
        L_0x002f:
            r1 = r1 & r9
            r4 = r8[r1]
            if (r4 != r7) goto L_0x0039
            r8[r1] = r2
            int r3 = r3 + 1
            goto L_0x0017
        L_0x0039:
            r0 = r10[r4]
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x0044
            int r1 = r1 + 1
            goto L_0x002f
        L_0x0044:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "Multiple entries with same key: "
            r2.<init>(r0)
            r2.append(r6)
            java.lang.String r1 = "="
            r2.append(r1)
            r2.append(r5)
            java.lang.String r0 = " and "
            r2.append(r0)
            r0 = r10[r4]
            r2.append(r0)
            r2.append(r1)
            r0 = r4 ^ 1
            r0 = r10[r0]
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            throw r3
        L_0x0074:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.RegularImmutableMap.A02(java.lang.Object[], int, int, int):int[]");
    }

    public boolean isPartialView() {
        return false;
    }

    public static RegularImmutableMap A00(int i, Object[] objArr) {
        if (i == 0) {
            return (RegularImmutableMap) A03;
        }
        if (i == 1) {
            C25001Xy.A03(objArr[0], objArr[1]);
            return new RegularImmutableMap(null, objArr, 1);
        }
        Preconditions.checkPositionIndex(i, objArr.length >> 1);
        return new RegularImmutableMap(A02(objArr, i, ImmutableSet.A00(i), 0), objArr, i);
    }

    public ImmutableSet createEntrySet() {
        return new EntrySet(this, this.A02, 0, this.A00);
    }

    public ImmutableSet createKeySet() {
        return new KeySet(this, new KeysOrValuesAsList(this.A02, 0, this.A00));
    }

    public ImmutableCollection createValues() {
        return new KeysOrValuesAsList(this.A02, 1, this.A00);
    }

    public Object get(Object obj) {
        return A01(this.A01, this.A02, this.A00, 0, obj);
    }

    public int size() {
        return this.A00;
    }

    private RegularImmutableMap(int[] iArr, Object[] objArr, int i) {
        this.A01 = iArr;
        this.A02 = objArr;
        this.A00 = i;
    }
}
