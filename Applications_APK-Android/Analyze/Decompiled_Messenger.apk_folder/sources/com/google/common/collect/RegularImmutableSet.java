package com.google.common.collect;

import X.C07910eN;
import X.C24931Xr;
import X.C24971Xv;

public final class RegularImmutableSet<E> extends ImmutableSet<E> {
    public static final RegularImmutableSet A05 = new RegularImmutableSet(new Object[0], 0, null, 0, 0);
    public final transient Object[] A00;
    public final transient Object[] A01;
    private final transient int A02;
    private final transient int A03;
    private final transient int A04;

    public ImmutableList A0H() {
        Object[] objArr = this.A00;
        int i = this.A04;
        if (i == 0) {
            return RegularImmutableList.A02;
        }
        return new RegularImmutableList(objArr, i);
    }

    public boolean contains(Object obj) {
        Object[] objArr = this.A01;
        if (obj != null && objArr != null) {
            int A022 = C07910eN.A02(obj);
            while (true) {
                int i = A022 & this.A03;
                Object obj2 = objArr[i];
                if (obj2 == null) {
                    break;
                } else if (obj2.equals(obj)) {
                    return true;
                } else {
                    A022 = i + 1;
                }
            }
        }
        return false;
    }

    public int copyIntoArray(Object[] objArr, int i) {
        System.arraycopy(this.A00, 0, objArr, i, this.A04);
        return i + this.A04;
    }

    public int hashCode() {
        return this.A02;
    }

    public int size() {
        return this.A04;
    }

    public RegularImmutableSet(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.A00 = objArr;
        this.A01 = objArr2;
        this.A03 = i2;
        this.A02 = i;
        this.A04 = i3;
    }

    public C24971Xv iterator() {
        return C24931Xr.A06(this.A00, 0, this.A04, 0);
    }
}
