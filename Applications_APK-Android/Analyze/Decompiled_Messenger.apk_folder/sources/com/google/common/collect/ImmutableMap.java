package com.google.common.collect;

import X.C181810n;
import X.C24971Xv;
import X.C24991Xx;
import X.C25001Xy;
import X.C25011Xz;
import com.facebook.acra.util.StatFsUtil;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

public abstract class ImmutableMap<K, V> implements Map<K, V>, Serializable {
    public static final Map.Entry[] EMPTY_ENTRY_ARRAY = new Map.Entry[0];
    private transient ImmutableSet entrySet;
    private transient ImmutableSet keySet;
    private transient ImmutableCollection values;

    public class Builder {
        public Object[] alternatingKeysAndValues;
        public boolean entriesUsed;
        public int size;
        public final Comparator valueComparator;

        public void sortEntries() {
            int i;
            if (this.valueComparator != null) {
                if (this.entriesUsed) {
                    this.alternatingKeysAndValues = Arrays.copyOf(this.alternatingKeysAndValues, this.size << 1);
                }
                Map.Entry[] entryArr = new Map.Entry[this.size];
                int i2 = 0;
                while (true) {
                    i = this.size;
                    if (i2 >= i) {
                        break;
                    }
                    Object[] objArr = this.alternatingKeysAndValues;
                    int i3 = i2 << 1;
                    entryArr[i2] = new AbstractMap.SimpleImmutableEntry(objArr[i3], objArr[i3 + 1]);
                    i2++;
                }
                Arrays.sort(entryArr, 0, i, new ByFunctionOrdering(Maps$EntryFunction.VALUE, C181810n.A00(this.valueComparator)));
                for (int i4 = 0; i4 < this.size; i4++) {
                    int i5 = i4 << 1;
                    this.alternatingKeysAndValues[i5] = entryArr[i4].getKey();
                    this.alternatingKeysAndValues[i5 + 1] = entryArr[i4].getValue();
                }
            }
        }

        public ImmutableMap build() {
            sortEntries();
            this.entriesUsed = true;
            return RegularImmutableMap.A00(this.size, this.alternatingKeysAndValues);
        }

        public Builder() {
            this(4);
        }

        public Builder(int i) {
            this.alternatingKeysAndValues = new Object[(i << 1)];
            this.size = 0;
            this.entriesUsed = false;
        }

        public Builder put(Object obj, Object obj2) {
            int i = (this.size + 1) << 1;
            Object[] objArr = this.alternatingKeysAndValues;
            int length = objArr.length;
            if (i > length) {
                this.alternatingKeysAndValues = Arrays.copyOf(objArr, C24991Xx.A01(length, i));
                this.entriesUsed = false;
            }
            C25001Xy.A03(obj, obj2);
            Object[] objArr2 = this.alternatingKeysAndValues;
            int i2 = this.size;
            int i3 = i2 << 1;
            objArr2[i3] = obj;
            objArr2[i3 + 1] = obj2;
            this.size = i2 + 1;
            return this;
        }

        public Builder put(Map.Entry entry) {
            put(entry.getKey(), entry.getValue());
            return this;
        }

        public Builder putAll(Iterable iterable) {
            int size2;
            Object[] objArr;
            int length;
            if ((iterable instanceof Collection) && (size2 = (this.size + ((Collection) iterable).size()) << 1) > (length = (objArr = this.alternatingKeysAndValues).length)) {
                this.alternatingKeysAndValues = Arrays.copyOf(objArr, C24991Xx.A01(length, size2));
                this.entriesUsed = false;
            }
            Iterator it = iterable.iterator();
            while (it.hasNext()) {
                put((Map.Entry) it.next());
            }
            return this;
        }

        public Builder putAll(Map map) {
            putAll(map.entrySet());
            return this;
        }
    }

    public class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        private final Object[] keys;
        private final Object[] values;

        public Object A00(Builder builder) {
            int i = 0;
            while (true) {
                Object[] objArr = this.keys;
                if (i >= objArr.length) {
                    return builder.build();
                }
                builder.put(objArr[i], this.values[i]);
                i++;
            }
        }

        public Object readResolve() {
            return A00(new Builder(this.keys.length));
        }

        public SerializedForm(ImmutableMap immutableMap) {
            this.keys = new Object[immutableMap.size()];
            this.values = new Object[immutableMap.size()];
            C24971Xv it = immutableMap.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                this.keys[i] = entry.getKey();
                this.values[i] = entry.getValue();
                i++;
            }
        }
    }

    public abstract ImmutableSet createEntrySet();

    public abstract ImmutableSet createKeySet();

    public abstract ImmutableCollection createValues();

    public abstract Object get(Object obj);

    public abstract boolean isPartialView();

    public static Builder builder() {
        return new Builder();
    }

    public static ImmutableMap copyOf(Map map) {
        if ((map instanceof ImmutableMap) && !(map instanceof SortedMap)) {
            ImmutableMap immutableMap = (ImmutableMap) map;
            if (!immutableMap.isPartialView()) {
                return immutableMap;
            }
        }
        Set entrySet2 = map.entrySet();
        int i = 4;
        if (entrySet2 instanceof Collection) {
            i = entrySet2.size();
        }
        Builder builder = new Builder(i);
        builder.putAll(entrySet2);
        return builder.build();
    }

    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    public final Object put(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    public final void putAll(Map map) {
        throw new UnsupportedOperationException();
    }

    public final Object remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public Object writeReplace() {
        return new SerializedForm(this);
    }

    public boolean containsKey(Object obj) {
        if (get(obj) != null) {
            return true;
        }
        return false;
    }

    public boolean containsValue(Object obj) {
        return values().contains(obj);
    }

    public int hashCode() {
        return C25011Xz.A00(entrySet());
    }

    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        }
        return false;
    }

    public String toString() {
        int size = size();
        C25001Xy.A00(size, "size");
        StringBuilder sb = new StringBuilder((int) Math.min(((long) size) * 8, (long) StatFsUtil.IN_GIGA_BYTE));
        sb.append('{');
        boolean z = true;
        for (Map.Entry entry : entrySet()) {
            if (!z) {
                sb.append(", ");
            }
            z = false;
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
        }
        sb.append('}');
        return sb.toString();
    }

    public ImmutableSet entrySet() {
        ImmutableSet immutableSet = this.entrySet;
        if (immutableSet != null) {
            return immutableSet;
        }
        ImmutableSet createEntrySet = createEntrySet();
        this.entrySet = createEntrySet;
        return createEntrySet;
    }

    public ImmutableSet keySet() {
        ImmutableSet immutableSet = this.keySet;
        if (immutableSet != null) {
            return immutableSet;
        }
        ImmutableSet createKeySet = createKeySet();
        this.keySet = createKeySet;
        return createKeySet;
    }

    public static ImmutableMap of() {
        return RegularImmutableMap.A03;
    }

    public static ImmutableMap of(Object obj, Object obj2) {
        C25001Xy.A03(obj, obj2);
        return RegularImmutableMap.A00(1, new Object[]{obj, obj2});
    }

    public static ImmutableMap of(Object obj, Object obj2, Object obj3, Object obj4) {
        C25001Xy.A03(obj, obj2);
        C25001Xy.A03(obj3, obj4);
        return RegularImmutableMap.A00(2, new Object[]{obj, obj2, obj3, obj4});
    }

    public static ImmutableMap of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8) {
        C25001Xy.A03(obj, obj2);
        C25001Xy.A03(obj3, obj4);
        C25001Xy.A03(obj5, obj6);
        C25001Xy.A03(obj7, obj8);
        return RegularImmutableMap.A00(4, new Object[]{obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8});
    }

    public static ImmutableMap of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
        C25001Xy.A03(obj, obj2);
        C25001Xy.A03(obj3, obj4);
        C25001Xy.A03(obj5, obj6);
        C25001Xy.A03(obj7, obj8);
        C25001Xy.A03(obj9, obj10);
        return RegularImmutableMap.A00(5, new Object[]{obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10});
    }

    public ImmutableCollection values() {
        ImmutableCollection immutableCollection = this.values;
        if (immutableCollection != null) {
            return immutableCollection;
        }
        ImmutableCollection createValues = createValues();
        this.values = createValues;
        return createValues;
    }
}
