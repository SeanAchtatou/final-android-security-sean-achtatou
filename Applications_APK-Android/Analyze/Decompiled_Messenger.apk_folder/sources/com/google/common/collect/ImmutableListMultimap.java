package com.google.common.collect;

import X.AnonymousClass08S;
import X.AnonymousClass0V0;
import X.AnonymousClass0V1;
import X.B83;
import X.C22684B7r;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ImmutableListMultimap extends ImmutableMultimap implements AnonymousClass0V1 {
    private static final long serialVersionUID = 0;

    /* renamed from: A0F */
    public ImmutableList AbL(Object obj) {
        ImmutableList immutableList = (ImmutableList) this.A01.get(obj);
        if (immutableList == null) {
            return RegularImmutableList.A02;
        }
        return immutableList;
    }

    public /* bridge */ /* synthetic */ Collection C1N(Object obj) {
        throw new UnsupportedOperationException();
    }

    public /* bridge */ /* synthetic */ List C1O(Object obj) {
        throw new UnsupportedOperationException();
    }

    public /* bridge */ /* synthetic */ Collection C2O(Object obj, Iterable iterable) {
        throw new UnsupportedOperationException();
    }

    public ImmutableListMultimap(ImmutableMap immutableMap, int i) {
        super(immutableMap, i);
    }

    public static ImmutableListMultimap A01(AnonymousClass0V0 r6) {
        if (r6.isEmpty()) {
            return EmptyImmutableListMultimap.A00;
        }
        if (r6 instanceof ImmutableListMultimap) {
            ImmutableListMultimap immutableListMultimap = (ImmutableListMultimap) r6;
            if (!immutableListMultimap.A01.isPartialView()) {
                return immutableListMultimap;
            }
        }
        ImmutableMap.Builder builder = new ImmutableMap.Builder(r6.AOp().size());
        int i = 0;
        for (Map.Entry entry : r6.AOp().entrySet()) {
            ImmutableList copyOf = ImmutableList.copyOf((Collection) entry.getValue());
            if (!copyOf.isEmpty()) {
                builder.put(entry.getKey(), copyOf);
                i += copyOf.size();
            }
        }
        return new ImmutableListMultimap(builder.build(), i);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            ImmutableMap.Builder builder = ImmutableMap.builder();
            int i = 0;
            int i2 = 0;
            while (i < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    ImmutableList.Builder builder2 = ImmutableList.builder();
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        builder2.add(objectInputStream.readObject());
                    }
                    builder.put(readObject, builder2.build());
                    i2 += readInt2;
                    i++;
                } else {
                    throw new InvalidObjectException(AnonymousClass08S.A09("Invalid value count ", readInt2));
                }
            }
            try {
                B83.A01.A01(this, builder.build());
                B83.A02.A00(this, i2);
            } catch (IllegalArgumentException e) {
                throw ((InvalidObjectException) new InvalidObjectException(e.getMessage()).initCause(e));
            }
        } else {
            throw new InvalidObjectException(AnonymousClass08S.A09("Invalid key count ", readInt));
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        C22684B7r.A02(this, objectOutputStream);
    }
}
