package com.google.common.collect;

import X.C181810n;
import java.io.Serializable;

public final class NullsLastOrdering extends C181810n implements Serializable {
    private static final long serialVersionUID = 0;
    public final C181810n ordering;

    public C181810n A03() {
        return this;
    }

    public C181810n A02() {
        return this.ordering.A02();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof NullsLastOrdering) {
            return this.ordering.equals(((NullsLastOrdering) obj).ordering);
        }
        return false;
    }

    public int hashCode() {
        return this.ordering.hashCode() ^ -921210296;
    }

    public String toString() {
        return this.ordering + ".nullsLast()";
    }

    public NullsLastOrdering(C181810n r1) {
        this.ordering = r1;
    }
}
