package com.google.common.collect;

import X.C181810n;
import com.google.common.base.Preconditions;
import java.io.Serializable;

public final class ReverseOrdering extends C181810n implements Serializable {
    private static final long serialVersionUID = 0;
    public final C181810n forwardOrder;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ReverseOrdering) {
            return this.forwardOrder.equals(((ReverseOrdering) obj).forwardOrder);
        }
        return false;
    }

    public int hashCode() {
        return -this.forwardOrder.hashCode();
    }

    public String toString() {
        return this.forwardOrder + ".reverse()";
    }

    public ReverseOrdering(C181810n r1) {
        Preconditions.checkNotNull(r1);
        this.forwardOrder = r1;
    }
}
