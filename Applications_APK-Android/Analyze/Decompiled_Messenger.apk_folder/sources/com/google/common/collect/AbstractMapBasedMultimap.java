package com.google.common.collect;

import X.AnonymousClass118;
import X.AnonymousClass11A;
import X.AnonymousClass11B;
import X.C04510Uz;
import X.C25011Xz;
import X.C29781EiT;
import X.C29782EiU;
import X.C53442kn;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedSet;

public abstract class AbstractMapBasedMultimap extends C04510Uz implements Serializable {
    private static final long serialVersionUID = 2447537837011683357L;
    public transient int A00;
    public transient Map A01;

    public Collection A0A() {
        if (!(this instanceof AbstractSetMultimap)) {
            AbstractListMultimap abstractListMultimap = (AbstractListMultimap) this;
            return !(abstractListMultimap instanceof ArrayListMultimap) ? abstractListMultimap.A0F() : ((ArrayListMultimap) abstractListMultimap).A0F();
        }
        AbstractSetMultimap abstractSetMultimap = (AbstractSetMultimap) this;
        return !(abstractSetMultimap instanceof HashMultimap) ? abstractSetMultimap.A0F() : ((HashMultimap) abstractSetMultimap).A0F();
    }

    public static Collection A00(Collection collection) {
        if (collection instanceof NavigableSet) {
            return C25011Xz.A07((NavigableSet) collection);
        }
        if (collection instanceof SortedSet) {
            return Collections.unmodifiableSortedSet((SortedSet) collection);
        }
        if (collection instanceof Set) {
            return Collections.unmodifiableSet((Set) collection);
        }
        if (collection instanceof List) {
            return Collections.unmodifiableList((List) collection);
        }
        return Collections.unmodifiableCollection(collection);
    }

    public Collection A0B() {
        if (this instanceof AbstractSetMultimap) {
            return RegularImmutableSet.A05;
        }
        if (!(this instanceof AbstractListMultimap)) {
            return A00(A0A());
        }
        return RegularImmutableList.A02;
    }

    /* JADX WARN: Type inference failed for: r0v7, types: [java.util.Collection, X.118] */
    public Collection A0D(Object obj, Collection collection) {
        if (collection instanceof NavigableSet) {
            return new C29781EiT(this, obj, (NavigableSet) collection, null);
        }
        if (collection instanceof SortedSet) {
            return new C29782EiU(this, obj, (SortedSet) collection, null);
        }
        if (collection instanceof Set) {
            return new C53442kn(this, obj, (Set) collection);
        }
        if (!(collection instanceof List)) {
            return new AnonymousClass11B(this, obj, collection, null);
        }
        List list = (List) collection;
        if (list instanceof RandomAccess) {
            return new AnonymousClass118(this, obj, list, null);
        }
        return new AnonymousClass11A(this, obj, list, null);
    }

    public final void A0E(Map map) {
        this.A01 = map;
        this.A00 = 0;
        for (Collection collection : map.values()) {
            Preconditions.checkArgument(!collection.isEmpty());
            this.A00 += collection.size();
        }
    }

    public Collection AbK(Object obj) {
        Collection collection = (Collection) this.A01.get(obj);
        if (collection == null) {
            collection = A0C(obj);
        }
        return A0D(obj, collection);
    }

    public Collection C1N(Object obj) {
        Collection collection = (Collection) this.A01.remove(obj);
        if (collection == null) {
            return A0B();
        }
        Collection A0A = A0A();
        A0A.addAll(collection);
        this.A00 -= collection.size();
        collection.clear();
        return A00(A0A);
    }

    public void clear() {
        for (Collection clear : this.A01.values()) {
            clear.clear();
        }
        this.A01.clear();
        this.A00 = 0;
    }

    public boolean containsKey(Object obj) {
        return this.A01.containsKey(obj);
    }

    public int size() {
        return this.A00;
    }

    public AbstractMapBasedMultimap(Map map) {
        Preconditions.checkArgument(map.isEmpty());
        this.A01 = map;
    }

    public Collection A0C(Object obj) {
        return A0A();
    }

    public Collection C2O(Object obj, Iterable iterable) {
        Iterator it = iterable.iterator();
        if (!it.hasNext()) {
            return C1N(obj);
        }
        Collection collection = (Collection) this.A01.get(obj);
        if (collection == null) {
            collection = A0C(obj);
            this.A01.put(obj, collection);
        }
        Collection A0A = A0A();
        A0A.addAll(collection);
        this.A00 -= collection.size();
        collection.clear();
        while (it.hasNext()) {
            if (collection.add(it.next())) {
                this.A00++;
            }
        }
        return A00(A0A);
    }
}
