package com.google.common.collect;

import X.AnonymousClass0U8;
import X.AnonymousClass0jB;
import X.C24931Xr;
import X.C24961Xu;
import X.C24971Xv;
import X.C24981Xw;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public abstract class ImmutableList<E> extends ImmutableCollection<E> implements List<E>, RandomAccess {

    public class ReverseImmutableList<E> extends ImmutableList<E> {
        public final transient ImmutableList A00;

        public boolean contains(Object obj) {
            return this.A00.contains(obj);
        }

        public int indexOf(Object obj) {
            int lastIndexOf = this.A00.lastIndexOf(obj);
            if (lastIndexOf >= 0) {
                return (size() - 1) - lastIndexOf;
            }
            return -1;
        }

        public int lastIndexOf(Object obj) {
            int indexOf = this.A00.indexOf(obj);
            if (indexOf >= 0) {
                return (size() - 1) - indexOf;
            }
            return -1;
        }

        public ImmutableList reverse() {
            return this.A00;
        }

        public int size() {
            return this.A00.size();
        }

        public ReverseImmutableList(ImmutableList immutableList) {
            this.A00 = immutableList;
        }

        public Object get(int i) {
            Preconditions.checkElementIndex(i, size());
            return this.A00.get((size() - 1) - i);
        }

        public /* bridge */ /* synthetic */ Iterator iterator() {
            return ImmutableList.super.iterator();
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return listIterator(0);
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return ImmutableList.super.listIterator(i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
         arg types: [int, int]
         candidates:
          com.google.common.collect.ImmutableList.subList(int, int):java.util.List
          ClspMth{java.util.List.subList(int, int):java.util.List<E>}
          com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
        public ImmutableList subList(int i, int i2) {
            Preconditions.checkPositionIndexes(i, i2, size());
            return this.A00.subList(size() - i2, size() - i).reverse();
        }
    }

    public class SubList extends ImmutableList<E> {
        public final transient int A00;
        public final transient int A01;

        public SubList(int i, int i2) {
            this.A01 = i;
            this.A00 = i2;
        }

        public Object get(int i) {
            Preconditions.checkElementIndex(i, this.A00);
            return ImmutableList.this.get(i + this.A01);
        }

        public int size() {
            return this.A00;
        }

        public /* bridge */ /* synthetic */ Iterator iterator() {
            return ImmutableList.super.iterator();
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return listIterator(0);
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return ImmutableList.super.listIterator(i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
         arg types: [int, int]
         candidates:
          com.google.common.collect.ImmutableList.subList(int, int):java.util.List
          ClspMth{java.util.List.subList(int, int):java.util.List<E>}
          com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
        public ImmutableList subList(int i, int i2) {
            Preconditions.checkPositionIndexes(i, i2, this.A00);
            ImmutableList immutableList = ImmutableList.this;
            int i3 = this.A01;
            return immutableList.subList(i + i3, i2 + i3);
        }
    }

    public class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        public final Object[] elements;

        public Object readResolve() {
            return ImmutableList.copyOf(this.elements);
        }

        public SerializedForm(Object[] objArr) {
            this.elements = objArr;
        }
    }

    public static ImmutableList asImmutableList(Object[] objArr) {
        int length = objArr.length;
        if (length == 0) {
            return RegularImmutableList.A02;
        }
        return new RegularImmutableList(objArr, length);
    }

    public static ImmutableList construct(Object... objArr) {
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            AnonymousClass0U8.A00(objArr[i], i);
        }
        return asImmutableList(objArr);
    }

    public final ImmutableList asList() {
        return this;
    }

    public static Builder builder() {
        return new Builder();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Use SerializedForm");
    }

    public final void add(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(int i, Collection collection) {
        throw new UnsupportedOperationException();
    }

    public int indexOf(Object obj) {
        if (obj != null) {
            if (this instanceof RandomAccess) {
                int size = size();
                int i = 0;
                if (obj == null) {
                    while (i < size) {
                        if (get(i) == null) {
                            return i;
                        }
                        i++;
                    }
                } else {
                    while (i < size) {
                        if (obj.equals(get(i))) {
                            return i;
                        }
                        i++;
                    }
                }
            } else {
                ListIterator listIterator = listIterator();
                while (listIterator.hasNext()) {
                    if (Objects.equal(obj, listIterator.next())) {
                        return listIterator.previousIndex();
                    }
                }
            }
        }
        return -1;
    }

    public int lastIndexOf(Object obj) {
        if (obj != null) {
            if (!(this instanceof RandomAccess)) {
                ListIterator listIterator = listIterator(size());
                while (listIterator.hasPrevious()) {
                    if (Objects.equal(obj, listIterator.previous())) {
                        return listIterator.nextIndex();
                    }
                }
            } else if (obj == null) {
                for (int size = size() - 1; size >= 0; size--) {
                    if (get(size) == null) {
                        return size;
                    }
                }
            } else {
                for (int size2 = size() - 1; size2 >= 0; size2--) {
                    if (obj.equals(get(size2))) {
                        return size2;
                    }
                }
            }
        }
        return -1;
    }

    public final Object remove(int i) {
        throw new UnsupportedOperationException();
    }

    public final Object set(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public boolean contains(Object obj) {
        if (indexOf(obj) >= 0) {
            return true;
        }
        return false;
    }

    public int copyIntoArray(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    public boolean equals(Object obj) {
        Preconditions.checkNotNull(this);
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        List list = (List) obj;
        int size = size();
        if (size != list.size()) {
            return false;
        }
        if (!(this instanceof RandomAccess) || !(list instanceof RandomAccess)) {
            return C24931Xr.A0A(iterator(), list.iterator());
        }
        for (int i = 0; i < size; i++) {
            if (!Objects.equal(get(i), list.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (((i * 31) + get(i2).hashCode()) ^ -1) ^ -1;
        }
        return i;
    }

    public ImmutableList reverse() {
        if (size() <= 1) {
            return this;
        }
        return new ReverseImmutableList(this);
    }

    public static ImmutableList copyOf(Iterable iterable) {
        Preconditions.checkNotNull(iterable);
        if (iterable instanceof Collection) {
            return copyOf((Collection) iterable);
        }
        return copyOf(iterable.iterator());
    }

    public static ImmutableList copyOf(Collection collection) {
        if (!(collection instanceof ImmutableCollection)) {
            return construct(collection.toArray());
        }
        ImmutableList asList = ((ImmutableCollection) collection).asList();
        return asList.A0G() ? asImmutableList(asList.toArray()) : asList;
    }

    public static ImmutableList copyOf(Iterator it) {
        if (!it.hasNext()) {
            return RegularImmutableList.A02;
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return of(next);
        }
        Builder builder = new Builder();
        builder.add(next);
        builder.addAll(it);
        return builder.build();
    }

    public static ImmutableList copyOf(Object[] objArr) {
        if (objArr.length == 0) {
            return RegularImmutableList.A02;
        }
        return construct((Object[]) objArr.clone());
    }

    public C24971Xv iterator() {
        return listIterator(0);
    }

    public C24961Xu listIterator(int i) {
        return new AnonymousClass0jB(this, size(), i);
    }

    public /* bridge */ /* synthetic */ ListIterator listIterator() {
        return listIterator(0);
    }

    public static ImmutableList of() {
        return RegularImmutableList.A02;
    }

    public static ImmutableList of(Object obj) {
        return construct(obj);
    }

    public static ImmutableList of(Object obj, Object obj2) {
        return construct(obj, obj2);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3) {
        return construct(obj, obj2, obj3);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4) {
        return construct(obj, obj2, obj3, obj4);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        return construct(obj, obj2, obj3, obj4, obj5);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6) {
        return construct(obj, obj2, obj3, obj4, obj5, obj6);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7) {
        return construct(obj, obj2, obj3, obj4, obj5, obj6, obj7);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8) {
        return construct(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
        return construct(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11) {
        return construct(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11);
    }

    public static ImmutableList of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object... objArr) {
        Object[] objArr2 = objArr;
        int length = objArr2.length;
        Object[] objArr3 = new Object[(length + 12)];
        objArr3[0] = obj;
        objArr3[1] = obj2;
        objArr3[2] = obj3;
        objArr3[3] = obj4;
        objArr3[4] = obj5;
        objArr3[5] = obj6;
        objArr3[6] = obj7;
        objArr3[7] = obj8;
        objArr3[8] = obj9;
        objArr3[9] = obj10;
        objArr3[10] = obj11;
        objArr3[11] = obj12;
        System.arraycopy(objArr2, 0, objArr3, 12, length);
        return construct(objArr3);
    }

    public ImmutableList subList(int i, int i2) {
        Preconditions.checkPositionIndexes(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        if (i3 == 0) {
            return RegularImmutableList.A02;
        }
        return new SubList(i, i3);
    }

    public final class Builder extends C24981Xw {
        public Builder() {
            this(4);
        }

        public Builder(int i) {
            super(i);
        }

        public Builder add(Object obj) {
            super.add(obj);
            return this;
        }

        public Builder add(Object... objArr) {
            super.add(objArr);
            return this;
        }

        public Builder addAll(Iterable iterable) {
            super.addAll(iterable);
            return this;
        }

        public Builder addAll(Iterator it) {
            super.addAll(it);
            return this;
        }

        public ImmutableList build() {
            this.A01 = true;
            Object[] objArr = this.A02;
            int i = this.A00;
            if (i == 0) {
                return RegularImmutableList.A02;
            }
            return new RegularImmutableList(objArr, i);
        }
    }
}
