package com.google.common.collect;

import X.AnonymousClass0UG;
import X.AnonymousClass0V0;
import X.C04510Uz;
import X.C24971Xv;
import X.C42992Cq;
import X.C80623so;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public abstract class ImmutableMultimap extends C04510Uz implements Serializable {
    private static final long serialVersionUID = 0;
    public final transient int A00;
    public final transient ImmutableMap A01;

    public class EntryCollection<K, V> extends ImmutableCollection<Map.Entry<K, V>> {
        private static final long serialVersionUID = 0;
        public final ImmutableMultimap multimap;

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.multimap.AUB(entry.getKey(), entry.getValue());
        }

        public int size() {
            return this.multimap.size();
        }

        public EntryCollection(ImmutableMultimap immutableMultimap) {
            this.multimap = immutableMultimap;
        }

        public C24971Xv iterator() {
            return new C80623so(this.multimap);
        }
    }

    public final class Values<K, V> extends ImmutableCollection<V> {
        private static final long serialVersionUID = 0;
        private final transient ImmutableMultimap A00;

        public boolean contains(Object obj) {
            return this.A00.containsValue(obj);
        }

        public int copyIntoArray(Object[] objArr, int i) {
            C24971Xv it = this.A00.A01.values().iterator();
            while (it.hasNext()) {
                i = ((ImmutableCollection) it.next()).copyIntoArray(objArr, i);
            }
            return i;
        }

        public int size() {
            return this.A00.size();
        }

        public Values(ImmutableMultimap immutableMultimap) {
            this.A00 = immutableMultimap;
        }

        public C24971Xv iterator() {
            return new C42992Cq(this.A00);
        }
    }

    public class Keys extends ImmutableMultiset<K> {
        public boolean A0G() {
            return true;
        }

        public Keys() {
        }

        public int AUa(Object obj) {
            Collection collection = (Collection) ImmutableMultimap.this.A01.get(obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        public boolean contains(Object obj) {
            return ImmutableMultimap.this.containsKey(obj);
        }

        public int size() {
            return ImmutableMultimap.this.size();
        }
    }

    /* renamed from: A0C */
    public ImmutableCollection AbK(Object obj) {
        return ((ImmutableListMultimap) this).AbL(obj);
    }

    public static ImmutableMultimap A00(AnonymousClass0V0 r2) {
        if (r2 instanceof ImmutableMultimap) {
            ImmutableMultimap immutableMultimap = (ImmutableMultimap) r2;
            if (!immutableMultimap.A01.isPartialView()) {
                return immutableMultimap;
            }
        }
        return ImmutableListMultimap.A01(r2);
    }

    /* renamed from: A0D */
    public ImmutableCollection C1N(Object obj) {
        boolean z = this instanceof ImmutableListMultimap;
        throw new UnsupportedOperationException();
    }

    /* renamed from: A0E */
    public ImmutableCollection C2O(Object obj, Iterable iterable) {
        boolean z = this instanceof ImmutableListMultimap;
        throw new UnsupportedOperationException();
    }

    public void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean containsKey(Object obj) {
        return this.A01.containsKey(obj);
    }

    public boolean containsValue(Object obj) {
        if (obj == null || !super.containsValue(obj)) {
            return false;
        }
        return true;
    }

    public int size() {
        return this.A00;
    }

    public ImmutableMultimap(ImmutableMap immutableMap, int i) {
        this.A01 = immutableMap;
        this.A00 = i;
    }

    /* renamed from: A0A */
    public ImmutableCollection AYj() {
        return (ImmutableCollection) super.AYj();
    }

    public ImmutableCollection A0B() {
        return (ImmutableCollection) super.values();
    }

    public AnonymousClass0UG BHh() {
        return (ImmutableMultiset) super.BHh();
    }

    public /* bridge */ /* synthetic */ Collection values() {
        return (ImmutableCollection) super.values();
    }
}
