package com.google.common.base;

import io.card.payment.BuildConfig;

public final class Strings {
    public static String nullToEmpty(String str) {
        return str == null ? BuildConfig.FLAVOR : str;
    }

    public static String emptyToNull(String str) {
        if (Platform.stringIsNullOrEmpty(str)) {
            return null;
        }
        return str;
    }
}
