package com.google.common.base;

public interface Predicate {
    boolean apply(Object obj);

    boolean equals(Object obj);
}
