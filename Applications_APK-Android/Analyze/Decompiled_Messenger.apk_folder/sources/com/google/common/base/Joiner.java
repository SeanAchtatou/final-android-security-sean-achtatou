package com.google.common.base;

import java.io.IOException;
import java.util.AbstractList;
import java.util.Iterator;

public class Joiner {
    public final String separator;

    public final class MapJoiner {
        public final Joiner joiner;
        public final String keyValueSeparator;

        public MapJoiner(Joiner joiner2, String str) {
            this.joiner = joiner2;
            Preconditions.checkNotNull(str);
            this.keyValueSeparator = str;
        }
    }

    public static Joiner on(String str) {
        return new Joiner(str);
    }

    public Joiner skipNulls() {
        return new Joiner(this) {
            /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
                jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
                	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
                	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
                */
            /* JADX WARNING: Removed duplicated region for block: B:14:0x0033 A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
            public java.lang.Appendable appendTo(java.lang.Appendable r3, java.util.Iterator r4) {
                /*
                    r2 = this;
                    java.lang.String r0 = "appendable"
                    com.google.common.base.Preconditions.checkNotNull(r3, r0)
                    java.lang.String r0 = "parts"
                    com.google.common.base.Preconditions.checkNotNull(r4, r0)
                L_0x000a:
                    boolean r0 = r4.hasNext()
                    if (r0 == 0) goto L_0x001f
                    java.lang.Object r1 = r4.next()
                    if (r1 == 0) goto L_0x000a
                L_0x0016:
                    com.google.common.base.Joiner r0 = com.google.common.base.Joiner.this
                    java.lang.CharSequence r0 = r0.toString(r1)
                    r3.append(r0)
                L_0x001f:
                    boolean r0 = r4.hasNext()
                    if (r0 == 0) goto L_0x0033
                    java.lang.Object r1 = r4.next()
                    if (r1 == 0) goto L_0x001f
                    com.google.common.base.Joiner r0 = com.google.common.base.Joiner.this
                    java.lang.String r0 = r0.separator
                    r3.append(r0)
                    goto L_0x0016
                L_0x0033:
                    return r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.common.base.Joiner.AnonymousClass2.appendTo(java.lang.Appendable, java.util.Iterator):java.lang.Appendable");
            }

            public Joiner useForNull(String str) {
                throw new UnsupportedOperationException("already specified skipNulls");
            }

            public MapJoiner withKeyValueSeparator(String str) {
                throw new UnsupportedOperationException("can't use .skipNulls() with maps");
            }
        };
    }

    public MapJoiner withKeyValueSeparator(String str) {
        return new MapJoiner(this, str);
    }

    public Appendable appendTo(Appendable appendable, Iterator it) {
        Preconditions.checkNotNull(appendable);
        if (it.hasNext()) {
            while (true) {
                appendable.append(toString(it.next()));
                if (!it.hasNext()) {
                    break;
                }
                appendable.append(this.separator);
            }
        }
        return appendable;
    }

    public CharSequence toString(Object obj) {
        Preconditions.checkNotNull(obj);
        if (obj instanceof CharSequence) {
            return (CharSequence) obj;
        }
        return obj.toString();
    }

    public Joiner useForNull(final String str) {
        Preconditions.checkNotNull(str);
        return new Joiner(this) {
            public Joiner skipNulls() {
                throw new UnsupportedOperationException("already specified useForNull");
            }

            public CharSequence toString(Object obj) {
                if (obj == null) {
                    return str;
                }
                return Joiner.this.toString(obj);
            }

            public Joiner useForNull(String str) {
                throw new UnsupportedOperationException("already specified useForNull");
            }
        };
    }

    public Joiner(Joiner joiner) {
        this.separator = joiner.separator;
    }

    public Joiner(String str) {
        Preconditions.checkNotNull(str);
        this.separator = str;
    }

    public final String join(Iterable iterable) {
        Iterator it = iterable.iterator();
        StringBuilder sb = new StringBuilder();
        try {
            appendTo(sb, it);
            return sb.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public final String join(final Object obj, final Object obj2, final Object... objArr) {
        Preconditions.checkNotNull(objArr);
        return join(new AbstractList<Object>() {
            public Object get(int i) {
                if (i == 0) {
                    return obj;
                }
                if (i != 1) {
                    return objArr[i - 2];
                }
                return obj2;
            }

            public int size() {
                return objArr.length + 2;
            }
        });
    }
}
