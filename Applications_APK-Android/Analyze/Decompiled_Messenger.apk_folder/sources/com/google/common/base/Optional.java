package com.google.common.base;

import java.io.Serializable;

public abstract class Optional implements Serializable {
    private static final long serialVersionUID = 0;

    public abstract boolean equals(Object obj);

    public abstract Object get();

    public abstract int hashCode();

    public abstract boolean isPresent();

    public abstract Object or(Object obj);

    public abstract Object orNull();

    public abstract String toString();

    public abstract Optional transform(Function function);

    public static Optional absent() {
        return Absent.INSTANCE;
    }

    public static Optional fromNullable(Object obj) {
        if (obj == null) {
            return absent();
        }
        return new Present(obj);
    }

    public static Optional of(Object obj) {
        Preconditions.checkNotNull(obj);
        return new Present(obj);
    }
}
