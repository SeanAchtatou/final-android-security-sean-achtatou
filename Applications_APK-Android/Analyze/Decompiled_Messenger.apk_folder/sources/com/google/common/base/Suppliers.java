package com.google.common.base;

import java.io.Serializable;
import java.util.Arrays;

public final class Suppliers {

    public class MemoizingSupplier implements Supplier, Serializable {
        private static final long serialVersionUID = 0;
        public final Supplier delegate;
        public volatile transient boolean initialized;
        public transient Object value;

        public Object get() {
            if (!this.initialized) {
                synchronized (this) {
                    if (!this.initialized) {
                        Object obj = this.delegate.get();
                        this.value = obj;
                        this.initialized = true;
                        return obj;
                    }
                }
            }
            return this.value;
        }

        public String toString() {
            return "Suppliers.memoize(" + this.delegate + ")";
        }

        public MemoizingSupplier(Supplier supplier) {
            Preconditions.checkNotNull(supplier);
            this.delegate = supplier;
        }
    }

    public class NonSerializableMemoizingSupplier implements Supplier {
        public volatile Supplier delegate;
        public volatile boolean initialized;
        public Object value;

        public Object get() {
            if (!this.initialized) {
                synchronized (this) {
                    if (!this.initialized) {
                        Object obj = this.delegate.get();
                        this.value = obj;
                        this.initialized = true;
                        this.delegate = null;
                        return obj;
                    }
                }
            }
            return this.value;
        }

        public String toString() {
            return "Suppliers.memoize(" + this.delegate + ")";
        }

        public NonSerializableMemoizingSupplier(Supplier supplier) {
            Preconditions.checkNotNull(supplier);
            this.delegate = supplier;
        }
    }

    public class SupplierOfInstance implements Supplier, Serializable {
        private static final long serialVersionUID = 0;
        public final Object instance;

        public boolean equals(Object obj) {
            if (obj instanceof SupplierOfInstance) {
                return Objects.equal(this.instance, ((SupplierOfInstance) obj).instance);
            }
            return false;
        }

        public int hashCode() {
            return Arrays.hashCode(new Object[]{this.instance});
        }

        public String toString() {
            return "Suppliers.ofInstance(" + this.instance + ")";
        }

        public SupplierOfInstance(Object obj) {
            this.instance = obj;
        }

        public Object get() {
            return this.instance;
        }
    }

    public static Supplier memoize(Supplier supplier) {
        if ((supplier instanceof NonSerializableMemoizingSupplier) || (supplier instanceof MemoizingSupplier)) {
            return supplier;
        }
        if (supplier instanceof Serializable) {
            return new MemoizingSupplier(supplier);
        }
        return new NonSerializableMemoizingSupplier(supplier);
    }
}
