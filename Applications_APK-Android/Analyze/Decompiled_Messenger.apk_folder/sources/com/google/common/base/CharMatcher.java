package com.google.common.base;

import X.AnonymousClass08S;
import X.AnonymousClass1Y3;
import com.facebook.forker.Process;
import io.card.payment.BuildConfig;
import java.util.Arrays;

public abstract class CharMatcher implements Predicate {
    public static final CharMatcher ANY = Any.INSTANCE;
    public static final CharMatcher ASCII = Ascii.INSTANCE;
    public static final CharMatcher BREAKING_WHITESPACE = BreakingWhitespace.INSTANCE;
    public static final CharMatcher DIGIT = Digit.INSTANCE;
    public static final CharMatcher INVISIBLE = Invisible.INSTANCE;
    public static final CharMatcher JAVA_DIGIT = JavaDigit.INSTANCE;
    public static final CharMatcher JAVA_ISO_CONTROL = JavaIsoControl.INSTANCE;
    public static final CharMatcher JAVA_LETTER = JavaLetter.INSTANCE;
    public static final CharMatcher JAVA_LETTER_OR_DIGIT = JavaLetterOrDigit.INSTANCE;
    public static final CharMatcher JAVA_LOWER_CASE = JavaLowerCase.INSTANCE;
    public static final CharMatcher JAVA_UPPER_CASE = JavaUpperCase.INSTANCE;
    public static final CharMatcher NONE = None.INSTANCE;
    public static final CharMatcher SINGLE_WIDTH = SingleWidth.INSTANCE;
    public static final CharMatcher WHITESPACE = Whitespace.INSTANCE;

    public final class Any extends NamedFastMatcher {
        public static final Any INSTANCE = new Any();

        public boolean matches(char c) {
            return true;
        }

        private Any() {
            super("CharMatcher.any()");
        }

        public int indexIn(CharSequence charSequence, int i) {
            int length = charSequence.length();
            Preconditions.checkPositionIndex(i, length);
            if (i == length) {
                return -1;
            }
            return i;
        }

        public String trimFrom(CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return BuildConfig.FLAVOR;
        }
    }

    public final class Ascii extends NamedFastMatcher {
        public static final Ascii INSTANCE = new Ascii();

        public boolean matches(char c) {
            return c <= 127;
        }

        public Ascii() {
            super("CharMatcher.ascii()");
        }
    }

    public final class BreakingWhitespace extends CharMatcher {
        public static final CharMatcher INSTANCE = new BreakingWhitespace();

        public boolean matches(char c) {
            if (!(c == ' ' || c == 133 || c == 5760)) {
                if (c != 8199) {
                    if (!(c == 8287 || c == 12288 || c == 8232 || c == 8233)) {
                        switch (c) {
                            case Process.SIGKILL:
                            case AnonymousClass1Y3.A01:
                            case AnonymousClass1Y3.A02:
                            case AnonymousClass1Y3.A03:
                            case 13:
                                break;
                            default:
                                if (c < 8192 || c > 8202) {
                                    return false;
                                }
                                break;
                        }
                    }
                } else {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return "CharMatcher.breakingWhitespace()";
        }

        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }

        private BreakingWhitespace() {
        }
    }

    public final class Digit extends RangesMatcher {
        public static final Digit INSTANCE = new Digit();

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private Digit() {
            /*
                r6 = this;
                java.lang.String r5 = "0٠۰߀०০੦૦୦௦౦೦൦๐໐༠၀႐០᠐᥆᧐᭐᮰᱀᱐꘠꣐꤀꩐０"
                char[] r4 = r5.toCharArray()
                r3 = 31
                char[] r2 = new char[r3]
                r1 = 0
            L_0x000b:
                if (r1 >= r3) goto L_0x0019
                char r0 = r5.charAt(r1)
                int r0 = r0 + 9
                char r0 = (char) r0
                r2[r1] = r0
                int r1 = r1 + 1
                goto L_0x000b
            L_0x0019:
                java.lang.String r0 = "CharMatcher.digit()"
                r6.<init>(r0, r4, r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.base.CharMatcher.Digit.<init>():void");
        }
    }

    public abstract class FastMatcher extends CharMatcher {
        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }
    }

    public final class Invisible extends RangesMatcher {
        public static final Invisible INSTANCE = new Invisible();

        private Invisible() {
            super("CharMatcher.invisible()", "\u0000­؀؜۝܏ ᠎   ⁦⁧⁨⁩⁪　?﻿￹￺".toCharArray(), "  ­؄؜۝܏ ᠎‏ ⁤⁦⁧⁨⁩⁯　﻿￹￻".toCharArray());
        }
    }

    public final class Is extends FastMatcher {
        private final char match;

        public boolean matches(char c) {
            if (c == this.match) {
                return true;
            }
            return false;
        }

        public String toString() {
            return AnonymousClass08S.A0P("CharMatcher.is('", CharMatcher.showCharacter(this.match), "')");
        }

        public Is(char c) {
            this.match = c;
        }
    }

    public final class JavaDigit extends CharMatcher {
        public static final JavaDigit INSTANCE = new JavaDigit();

        public String toString() {
            return "CharMatcher.javaDigit()";
        }

        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }

        public boolean matches(char c) {
            return Character.isDigit(c);
        }

        private JavaDigit() {
        }
    }

    public final class JavaIsoControl extends NamedFastMatcher {
        public static final JavaIsoControl INSTANCE = new JavaIsoControl();

        public boolean matches(char c) {
            if (c > 31) {
                return c >= 127 && c <= 159;
            }
            return true;
        }

        private JavaIsoControl() {
            super("CharMatcher.javaIsoControl()");
        }
    }

    public final class JavaLetter extends CharMatcher {
        public static final JavaLetter INSTANCE = new JavaLetter();

        public String toString() {
            return "CharMatcher.javaLetter()";
        }

        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }

        public boolean matches(char c) {
            return Character.isLetter(c);
        }

        private JavaLetter() {
        }
    }

    public final class JavaLetterOrDigit extends CharMatcher {
        public static final JavaLetterOrDigit INSTANCE = new JavaLetterOrDigit();

        public String toString() {
            return "CharMatcher.javaLetterOrDigit()";
        }

        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }

        public boolean matches(char c) {
            return Character.isLetterOrDigit(c);
        }

        private JavaLetterOrDigit() {
        }
    }

    public final class JavaLowerCase extends CharMatcher {
        public static final JavaLowerCase INSTANCE = new JavaLowerCase();

        public String toString() {
            return "CharMatcher.javaLowerCase()";
        }

        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }

        public boolean matches(char c) {
            return Character.isLowerCase(c);
        }

        private JavaLowerCase() {
        }
    }

    public final class JavaUpperCase extends CharMatcher {
        public static final JavaUpperCase INSTANCE = new JavaUpperCase();

        public String toString() {
            return "CharMatcher.javaUpperCase()";
        }

        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }

        public boolean matches(char c) {
            return Character.isUpperCase(c);
        }

        private JavaUpperCase() {
        }
    }

    public abstract class NamedFastMatcher extends FastMatcher {
        private final String description;

        public final String toString() {
            return this.description;
        }

        public NamedFastMatcher(String str) {
            Preconditions.checkNotNull(str);
            this.description = str;
        }
    }

    public final class None extends NamedFastMatcher {
        public static final None INSTANCE = new None();

        public boolean matches(char c) {
            return false;
        }

        private None() {
            super("CharMatcher.none()");
        }

        public int indexIn(CharSequence charSequence, int i) {
            Preconditions.checkPositionIndex(i, charSequence.length());
            return -1;
        }

        public String trimFrom(CharSequence charSequence) {
            return charSequence.toString();
        }

        public String trimTrailingFrom(CharSequence charSequence) {
            return charSequence.toString();
        }
    }

    public class RangesMatcher extends CharMatcher {
        private final String description;
        private final char[] rangeEnds;
        private final char[] rangeStarts;

        public /* bridge */ /* synthetic */ boolean apply(Object obj) {
            return matches(((Character) obj).charValue());
        }

        public boolean matches(char c) {
            int i;
            int binarySearch = Arrays.binarySearch(this.rangeStarts, c);
            if (binarySearch >= 0 || ((i = (binarySearch ^ -1) - 1) >= 0 && c <= this.rangeEnds[i])) {
                return true;
            }
            return false;
        }

        public String toString() {
            return this.description;
        }

        public RangesMatcher(String str, char[] cArr, char[] cArr2) {
            this.description = str;
            this.rangeStarts = cArr;
            this.rangeEnds = cArr2;
            int length = cArr.length;
            Preconditions.checkArgument(length == cArr2.length);
            int i = 0;
            while (i < length) {
                Preconditions.checkArgument(cArr[i] <= cArr2[i]);
                int i2 = i + 1;
                if (i2 < length) {
                    Preconditions.checkArgument(cArr2[i] < cArr[i2]);
                }
                i = i2;
            }
        }
    }

    public final class SingleWidth extends RangesMatcher {
        public static final SingleWidth INSTANCE = new SingleWidth();

        private SingleWidth() {
            super("CharMatcher.singleWidth()", "\u0000־א׳؀ݐ฀Ḁ℀ﭐﹰ｡".toCharArray(), "ӹ־ת״ۿݿ๿₯℺﷿﻿ￜ".toCharArray());
        }
    }

    public final class Whitespace extends NamedFastMatcher {
        public static final Whitespace INSTANCE = new Whitespace();
        public static final int SHIFT = Integer.numberOfLeadingZeros(31);

        public Whitespace() {
            super("CharMatcher.whitespace()");
        }

        public boolean matches(char c) {
            if (" 　\r   　 \u000b　   　 \t     \f 　 　　 \n 　".charAt((48906 * c) >>> SHIFT) == c) {
                return true;
            }
            return false;
        }
    }

    public final class InRange extends FastMatcher {
        private final char endInclusive;
        private final char startInclusive;

        public boolean matches(char c) {
            if (this.startInclusive > c || c > this.endInclusive) {
                return false;
            }
            return true;
        }

        public String toString() {
            return AnonymousClass08S.A0T("CharMatcher.inRange('", CharMatcher.showCharacter(this.startInclusive), "', '", CharMatcher.showCharacter(this.endInclusive), "')");
        }

        public InRange(char c, char c2) {
            Preconditions.checkArgument(c2 >= c);
            this.startInclusive = c;
            this.endInclusive = c2;
        }
    }

    public static String showCharacter(char c) {
        char[] cArr = {'\\', 'u', 0, 0, 0, 0};
        for (int i = 0; i < 4; i++) {
            cArr[5 - i] = "0123456789ABCDEF".charAt(c & 15);
            c = (char) (c >> 4);
        }
        return String.copyValueOf(cArr);
    }

    public abstract boolean matches(char c);

    public /* bridge */ /* synthetic */ boolean apply(Object obj) {
        return matches(((Character) obj).charValue());
    }

    public int indexIn(CharSequence charSequence, int i) {
        int length = charSequence.length();
        Preconditions.checkPositionIndex(i, length);
        while (i < length) {
            if (matches(charSequence.charAt(i))) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public String trimFrom(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        while (i < length && matches(charSequence.charAt(i))) {
            i++;
        }
        do {
            length--;
            if (length <= i) {
                break;
            }
        } while (matches(charSequence.charAt(length)));
        return charSequence.subSequence(i, length + 1).toString();
    }

    public String trimTrailingFrom(CharSequence charSequence) {
        for (int length = charSequence.length() - 1; length >= 0; length--) {
            if (!matches(charSequence.charAt(length))) {
                return charSequence.subSequence(0, length + 1).toString();
            }
        }
        return BuildConfig.FLAVOR;
    }
}
