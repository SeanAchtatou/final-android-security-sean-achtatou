package com.google.common.base;

import X.AnonymousClass1Y3;
import com.facebook.acra.LogCatCollector;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.nio.charset.Charset;

public final class Charsets {
    public static final Charset ISO_8859_1 = Charset.forName(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0q));
    public static final Charset US_ASCII = Charset.forName("US-ASCII");
    public static final Charset UTF_16 = Charset.forName("UTF-16");
    public static final Charset UTF_16BE = Charset.forName("UTF-16BE");
    public static final Charset UTF_16LE = Charset.forName(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0y));
    public static final Charset UTF_8 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
}
