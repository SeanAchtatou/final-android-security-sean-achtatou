package com.google.common.base;

import java.io.Serializable;

public abstract class Equivalence {

    public final class Equals extends Equivalence implements Serializable {
        public static final Equals INSTANCE = new Equals();
        private static final long serialVersionUID = 1;

        private Object readResolve() {
            return INSTANCE;
        }

        public boolean doEquivalent(Object obj, Object obj2) {
            return obj.equals(obj2);
        }

        public int doHash(Object obj) {
            return obj.hashCode();
        }
    }

    public final class Identity extends Equivalence implements Serializable {
        public static final Identity INSTANCE = new Identity();
        private static final long serialVersionUID = 1;

        public boolean doEquivalent(Object obj, Object obj2) {
            return false;
        }

        private Object readResolve() {
            return INSTANCE;
        }

        public int doHash(Object obj) {
            return System.identityHashCode(obj);
        }
    }

    public abstract boolean doEquivalent(Object obj, Object obj2);

    public abstract int doHash(Object obj);

    public final boolean equivalent(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return doEquivalent(obj, obj2);
    }
}
