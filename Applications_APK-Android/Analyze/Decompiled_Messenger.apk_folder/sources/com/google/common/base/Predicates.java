package com.google.common.base;

import X.AnonymousClass08S;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public final class Predicates {
    public static final Joiner COMMA_JOINER = new Joiner(String.valueOf(','));

    public class CompositionPredicate implements Predicate, Serializable {
        private static final long serialVersionUID = 0;
        public final Function f;
        public final Predicate p;

        public boolean apply(Object obj) {
            return this.p.apply(this.f.apply(obj));
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof CompositionPredicate)) {
                return false;
            }
            CompositionPredicate compositionPredicate = (CompositionPredicate) obj;
            if (!this.f.equals(compositionPredicate.f) || !this.p.equals(compositionPredicate.p)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.f.hashCode() ^ this.p.hashCode();
        }

        public String toString() {
            return this.p + "(" + this.f + ")";
        }

        public CompositionPredicate(Predicate predicate, Function function) {
            Preconditions.checkNotNull(predicate);
            this.p = predicate;
            Preconditions.checkNotNull(function);
            this.f = function;
        }
    }

    public class InPredicate implements Predicate, Serializable {
        private static final long serialVersionUID = 0;
        private final Collection target;

        public boolean apply(Object obj) {
            try {
                return this.target.contains(obj);
            } catch (ClassCastException | NullPointerException unused) {
                return false;
            }
        }

        public boolean equals(Object obj) {
            if (obj instanceof InPredicate) {
                return this.target.equals(((InPredicate) obj).target);
            }
            return false;
        }

        public int hashCode() {
            return this.target.hashCode();
        }

        public String toString() {
            return "Predicates.in(" + this.target + ")";
        }

        public InPredicate(Collection collection) {
            Preconditions.checkNotNull(collection);
            this.target = collection;
        }
    }

    public class NotPredicate implements Predicate, Serializable {
        private static final long serialVersionUID = 0;
        public final Predicate predicate;

        public boolean apply(Object obj) {
            return !this.predicate.apply(obj);
        }

        public boolean equals(Object obj) {
            if (obj instanceof NotPredicate) {
                return this.predicate.equals(((NotPredicate) obj).predicate);
            }
            return false;
        }

        public int hashCode() {
            return this.predicate.hashCode() ^ -1;
        }

        public String toString() {
            return "Predicates.not(" + this.predicate + ")";
        }

        public NotPredicate(Predicate predicate2) {
            Preconditions.checkNotNull(predicate2);
            this.predicate = predicate2;
        }
    }

    public enum ObjectPredicate implements Predicate {
        ALWAYS_TRUE {
            public boolean apply(Object obj) {
                return true;
            }

            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        },
        ALWAYS_FALSE {
            public boolean apply(Object obj) {
                return false;
            }

            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        },
        IS_NULL {
            public boolean apply(Object obj) {
                return obj == null;
            }

            public String toString() {
                return "Predicates.isNull()";
            }
        },
        NOT_NULL {
            public boolean apply(Object obj) {
                return obj != null;
            }

            public String toString() {
                return "Predicates.notNull()";
            }
        }
    }

    public class AndPredicate implements Predicate, Serializable {
        private static final long serialVersionUID = 0;
        private final List components;

        public boolean apply(Object obj) {
            for (int i = 0; i < this.components.size(); i++) {
                if (!((Predicate) this.components.get(i)).apply(obj)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object obj) {
            if (obj instanceof AndPredicate) {
                return this.components.equals(((AndPredicate) obj).components);
            }
            return false;
        }

        public int hashCode() {
            return this.components.hashCode() + 306654252;
        }

        public String toString() {
            return AnonymousClass08S.A0P("Predicates.and(", Predicates.COMMA_JOINER.join(this.components), ")");
        }

        public AndPredicate(List list) {
            this.components = list;
        }
    }

    public class InstanceOfPredicate implements Predicate, Serializable {
        private static final long serialVersionUID = 0;
        private final Class clazz;

        public boolean apply(Object obj) {
            return this.clazz.isInstance(obj);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof InstanceOfPredicate) || this.clazz != ((InstanceOfPredicate) obj).clazz) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.clazz.hashCode();
        }

        public String toString() {
            return AnonymousClass08S.A0P("Predicates.instanceOf(", this.clazz.getName(), ")");
        }

        public InstanceOfPredicate(Class cls) {
            Preconditions.checkNotNull(cls);
            this.clazz = cls;
        }
    }

    public class IsEqualToPredicate implements Predicate, Serializable {
        private static final long serialVersionUID = 0;
        private final Object target;

        public boolean apply(Object obj) {
            return this.target.equals(obj);
        }

        public boolean equals(Object obj) {
            if (obj instanceof IsEqualToPredicate) {
                return this.target.equals(((IsEqualToPredicate) obj).target);
            }
            return false;
        }

        public int hashCode() {
            return this.target.hashCode();
        }

        public String toString() {
            return "Predicates.equalTo(" + this.target + ")";
        }

        public IsEqualToPredicate(Object obj) {
            this.target = obj;
        }
    }

    public static Predicate and(Predicate predicate, Predicate predicate2) {
        Preconditions.checkNotNull(predicate);
        Preconditions.checkNotNull(predicate2);
        return new AndPredicate(Arrays.asList(predicate, predicate2));
    }

    public static Predicate equalTo(Object obj) {
        if (obj == null) {
            return ObjectPredicate.IS_NULL;
        }
        return new IsEqualToPredicate(obj);
    }

    public static Predicate in(Collection collection) {
        return new InPredicate(collection);
    }
}
