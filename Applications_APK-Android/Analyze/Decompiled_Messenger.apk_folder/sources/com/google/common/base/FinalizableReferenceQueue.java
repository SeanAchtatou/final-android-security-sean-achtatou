package com.google.common.base;

import X.AnonymousClass08S;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FinalizableReferenceQueue implements Closeable {
    public static final Logger logger = Logger.getLogger(FinalizableReferenceQueue.class.getName());
    private static final Method startFinalizer;
    public final PhantomReference frqRef;
    public final ReferenceQueue queue;
    public final boolean threadStarted;

    public class DecoupledLoader implements FinalizerLoader {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
         arg types: [int, int]
         candidates:
          ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
          ClspMth{java.lang.String.replace(char, char):java.lang.String} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        public Class loadFinalizer() {
            try {
                String A0J = AnonymousClass08S.A0J("com.google.common.base.internal.Finalizer".replace('.', '/'), ".class");
                URL resource = getClass().getClassLoader().getResource(A0J);
                if (resource != null) {
                    String url = resource.toString();
                    if (url.endsWith(A0J)) {
                        return new URLClassLoader(new URL[]{new URL(resource, url.substring(0, url.length() - A0J.length()))}, null).loadClass("com.google.common.base.internal.Finalizer");
                    }
                    throw new IOException(AnonymousClass08S.A0J("Unsupported path style: ", url));
                }
                throw new FileNotFoundException(A0J);
            } catch (Exception e) {
                FinalizableReferenceQueue.logger.log(Level.WARNING, "Could not load Finalizer in its own class loader. Loading Finalizer in the current class loader instead. As a result, you will not be able to garbage collect this class loader. To support reclaiming this class loader, either resolve the underlying issue, or move Guava to your system class path.", (Throwable) e);
                return null;
            }
        }
    }

    public class DirectLoader implements FinalizerLoader {
        public Class loadFinalizer() {
            try {
                return Class.forName("com.google.common.base.internal.Finalizer");
            } catch (ClassNotFoundException e) {
                throw new AssertionError(e);
            }
        }
    }

    public interface FinalizerLoader {
        Class loadFinalizer();
    }

    public class SystemLoader implements FinalizerLoader {
        public Class loadFinalizer() {
            try {
                ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
                if (systemClassLoader != null) {
                    try {
                        return systemClassLoader.loadClass("com.google.common.base.internal.Finalizer");
                    } catch (ClassNotFoundException unused) {
                    }
                }
                return null;
            } catch (SecurityException unused2) {
                FinalizableReferenceQueue.logger.info("Not allowed to access system class loader.");
                return null;
            }
        }
    }

    static {
        FinalizerLoader[] finalizerLoaderArr = {new SystemLoader(), new DecoupledLoader(), new DirectLoader()};
        int i = 0;
        while (i < 3) {
            Class loadFinalizer = finalizerLoaderArr[i].loadFinalizer();
            if (loadFinalizer != null) {
                try {
                    startFinalizer = loadFinalizer.getMethod("startFinalizer", Class.class, ReferenceQueue.class, PhantomReference.class);
                    return;
                } catch (NoSuchMethodException e) {
                    throw new AssertionError(e);
                }
            } else {
                i++;
            }
        }
        throw new AssertionError();
    }

    public void close() {
        this.frqRef.enqueue();
        if (!this.threadStarted) {
            while (true) {
                Reference poll = this.queue.poll();
                if (poll != null) {
                    poll.clear();
                    try {
                        ((FinalizableReference) poll).finalizeReferent();
                    } catch (Throwable th) {
                        logger.log(Level.SEVERE, "Error cleaning up after reference.", th);
                    }
                } else {
                    return;
                }
            }
        }
    }

    public FinalizableReferenceQueue() {
        ReferenceQueue referenceQueue = new ReferenceQueue();
        this.queue = referenceQueue;
        PhantomReference phantomReference = new PhantomReference(this, referenceQueue);
        this.frqRef = phantomReference;
        boolean z = true;
        try {
            startFinalizer.invoke(null, FinalizableReference.class, this.queue, phantomReference);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        } catch (Throwable th) {
            logger.log(Level.INFO, "Failed to start reference finalizer thread. Reference cleanup will only occur when new references are created.", th);
            z = false;
        }
        this.threadStarted = z;
    }
}
