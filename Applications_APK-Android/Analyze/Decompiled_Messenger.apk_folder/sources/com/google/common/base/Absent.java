package com.google.common.base;

import X.AnonymousClass1Y3;
import com.facebook.common.dextricks.turboloader.TurboLoader;

public final class Absent extends Optional {
    public static final Absent INSTANCE = new Absent();
    private static final long serialVersionUID = 0;

    public boolean equals(Object obj) {
        return obj == this;
    }

    public int hashCode() {
        return 2040732332;
    }

    public boolean isPresent() {
        return false;
    }

    public Object orNull() {
        return null;
    }

    public String toString() {
        return TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0u);
    }

    private Object readResolve() {
        return INSTANCE;
    }

    public Object get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    public Object or(Object obj) {
        Preconditions.checkNotNull(obj, "use Optional.orNull() instead of Optional.or(null)");
        return obj;
    }

    public Optional transform(Function function) {
        Preconditions.checkNotNull(function);
        return Optional.absent();
    }

    private Absent() {
    }
}
