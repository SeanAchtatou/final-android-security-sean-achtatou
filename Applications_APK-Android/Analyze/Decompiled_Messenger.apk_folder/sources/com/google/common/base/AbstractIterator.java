package com.google.common.base;

import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class AbstractIterator implements Iterator {
    public Object next;
    public State state = State.NOT_READY;

    public enum State {
        READY,
        NOT_READY,
        DONE,
        FAILED
    }

    public abstract Object computeNext();

    public final boolean hasNext() {
        State state2 = this.state;
        State state3 = State.FAILED;
        boolean z = false;
        if (state2 != state3) {
            z = true;
        }
        Preconditions.checkState(z);
        switch (state2.ordinal()) {
            case 0:
                return true;
            case 1:
            default:
                this.state = state3;
                this.next = computeNext();
                if (this.state == State.DONE) {
                    return false;
                }
                this.state = State.READY;
                return true;
            case 2:
                return false;
        }
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final Object next() {
        if (hasNext()) {
            this.state = State.NOT_READY;
            Object obj = this.next;
            this.next = null;
            return obj;
        }
        throw new NoSuchElementException();
    }
}
