package com.google.common.base;

import X.AnonymousClass1Y3;
import com.facebook.common.dextricks.turboloader.TurboLoader;

public final class Present extends Optional {
    private static final long serialVersionUID = 0;
    private final Object reference;

    public boolean isPresent() {
        return true;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Present) {
            return this.reference.equals(((Present) obj).reference);
        }
        return false;
    }

    public Object get() {
        return this.reference;
    }

    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    public Object or(Object obj) {
        Preconditions.checkNotNull(obj, "use Optional.orNull() instead of Optional.or(null)");
        return this.reference;
    }

    public Object orNull() {
        return this.reference;
    }

    public String toString() {
        return TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0v) + this.reference + ")";
    }

    public Optional transform(Function function) {
        Object apply = function.apply(this.reference);
        Preconditions.checkNotNull(apply, "the Function passed to Optional.transform() must not return null.");
        return new Present(apply);
    }

    public Present(Object obj) {
        this.reference = obj;
    }
}
