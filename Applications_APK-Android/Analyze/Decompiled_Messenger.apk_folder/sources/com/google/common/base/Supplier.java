package com.google.common.base;

public interface Supplier {
    Object get();
}
