package com.google.common.base;

import com.google.common.base.AbstractIterator;
import com.google.common.base.CharMatcher;
import java.util.Iterator;

public final class Splitter {
    public final int limit;
    public final boolean omitEmptyStrings;
    public final Strategy strategy;
    public final CharMatcher trimmer;

    public abstract class SplittingIterator extends AbstractIterator {
        public int limit;
        public int offset = 0;
        public final boolean omitEmptyStrings;
        public final CharSequence toSplit;
        public final CharMatcher trimmer;

        public abstract int separatorEnd(int i);

        public abstract int separatorStart(int i);

        public /* bridge */ /* synthetic */ Object computeNext() {
            int i;
            int i2 = this.offset;
            while (true) {
                int i3 = this.offset;
                if (i3 != -1) {
                    int separatorStart = separatorStart(i3);
                    if (separatorStart == -1) {
                        separatorStart = this.toSplit.length();
                        this.offset = -1;
                    } else {
                        this.offset = separatorEnd(separatorStart);
                    }
                    int i4 = this.offset;
                    if (i4 == i2) {
                        int i5 = i4 + 1;
                        this.offset = i5;
                        if (i5 > this.toSplit.length()) {
                            this.offset = -1;
                        }
                    } else {
                        while (i2 < separatorStart && this.trimmer.matches(this.toSplit.charAt(i2))) {
                            i2++;
                        }
                        while (i > i2) {
                            int i6 = i - 1;
                            if (!this.trimmer.matches(this.toSplit.charAt(i6))) {
                                break;
                            }
                            separatorStart = i6;
                        }
                        if (!this.omitEmptyStrings || i2 != i) {
                            int i7 = this.limit;
                        } else {
                            i2 = this.offset;
                        }
                    }
                } else {
                    this.state = AbstractIterator.State.DONE;
                    return null;
                }
            }
            int i72 = this.limit;
            if (i72 == 1) {
                i = this.toSplit.length();
                this.offset = -1;
                while (i > i2) {
                    int i8 = i - 1;
                    if (!this.trimmer.matches(this.toSplit.charAt(i8))) {
                        break;
                    }
                    i = i8;
                }
            } else {
                this.limit = i72 - 1;
            }
            return this.toSplit.subSequence(i2, i).toString();
        }

        public SplittingIterator(Splitter splitter, CharSequence charSequence) {
            this.trimmer = splitter.trimmer;
            this.omitEmptyStrings = splitter.omitEmptyStrings;
            this.limit = splitter.limit;
            this.toSplit = charSequence;
        }
    }

    public interface Strategy {
        Iterator iterator(Splitter splitter, CharSequence charSequence);
    }

    public Splitter limit(int i) {
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkArgument(z, "must be greater than zero: %s", i);
        return new Splitter(this.strategy, this.omitEmptyStrings, this.trimmer, i);
    }

    private Splitter(Strategy strategy2, boolean z, CharMatcher charMatcher, int i) {
        this.strategy = strategy2;
        this.omitEmptyStrings = z;
        this.trimmer = charMatcher;
        this.limit = i;
    }

    public static Splitter on(char c) {
        final CharMatcher.Is is = new CharMatcher.Is(c);
        Preconditions.checkNotNull(is);
        return new Splitter(new Strategy() {
            public /* bridge */ /* synthetic */ Iterator iterator(Splitter splitter, CharSequence charSequence) {
                return new SplittingIterator(splitter, charSequence) {
                    public int separatorEnd(int i) {
                        return i + 1;
                    }

                    public int separatorStart(int i) {
                        return CharMatcher.this.indexIn(this.toSplit, i);
                    }
                };
            }
        }, false, CharMatcher.None.INSTANCE, Integer.MAX_VALUE);
    }

    public static Splitter on(final String str) {
        int length = str.length();
        boolean z = false;
        if (length != 0) {
            z = true;
        }
        Preconditions.checkArgument(z, "The separator may not be the empty string.");
        if (length == 1) {
            return on(str.charAt(0));
        }
        return new Splitter(new Strategy() {
            public /* bridge */ /* synthetic */ Iterator iterator(Splitter splitter, CharSequence charSequence) {
                return new SplittingIterator(splitter, charSequence) {
                    public int separatorEnd(int i) {
                        return i + str.length();
                    }

                    public int separatorStart(int i) {
                        int length = str.length();
                        int length2 = this.toSplit.length() - length;
                        while (i <= length2) {
                            int i2 = 0;
                            while (i2 < length) {
                                if (this.toSplit.charAt(i2 + i) != str.charAt(i2)) {
                                    i++;
                                } else {
                                    i2++;
                                }
                            }
                            return i;
                        }
                        return -1;
                    }
                };
            }
        }, false, CharMatcher.None.INSTANCE, Integer.MAX_VALUE);
    }
}
