package com.google.common.base;

import io.card.payment.BuildConfig;
import java.util.Arrays;

public final class MoreObjects {

    public final class ToStringHelper {
        private final String className;
        private final ValueHolder holderHead;
        public ValueHolder holderTail;
        public boolean omitNullValues = false;

        public final class ValueHolder {
            public String name;
            public ValueHolder next;
            public Object value;
        }

        private ToStringHelper addHolder(String str, Object obj) {
            ValueHolder valueHolder = new ValueHolder();
            this.holderTail.next = valueHolder;
            this.holderTail = valueHolder;
            valueHolder.value = obj;
            Preconditions.checkNotNull(str);
            valueHolder.name = str;
            return this;
        }

        public ToStringHelper addValue(Object obj) {
            ValueHolder valueHolder = new ValueHolder();
            this.holderTail.next = valueHolder;
            this.holderTail = valueHolder;
            valueHolder.value = obj;
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
         arg types: [java.lang.String, int, int]
         candidates:
          ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
          ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
          ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
          ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
        public String toString() {
            boolean z = this.omitNullValues;
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.className);
            sb.append('{');
            String str = BuildConfig.FLAVOR;
            for (ValueHolder valueHolder = this.holderHead.next; valueHolder != null; valueHolder = valueHolder.next) {
                Object obj = valueHolder.value;
                if (!z || obj != null) {
                    sb.append(str);
                    String str2 = valueHolder.name;
                    if (str2 != null) {
                        sb.append(str2);
                        sb.append('=');
                    }
                    if (obj == null || !obj.getClass().isArray()) {
                        sb.append(obj);
                    } else {
                        String deepToString = Arrays.deepToString(new Object[]{obj});
                        sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
                    }
                    str = ", ";
                }
            }
            sb.append('}');
            return sb.toString();
        }

        public ToStringHelper(String str) {
            ValueHolder valueHolder = new ValueHolder();
            this.holderHead = valueHolder;
            this.holderTail = valueHolder;
            Preconditions.checkNotNull(str);
            this.className = str;
        }

        public ToStringHelper add(String str, float f) {
            addHolder(str, String.valueOf(f));
            return this;
        }

        public ToStringHelper add(String str, int i) {
            addHolder(str, String.valueOf(i));
            return this;
        }

        public ToStringHelper add(String str, long j) {
            addHolder(str, String.valueOf(j));
            return this;
        }

        public ToStringHelper add(String str, Object obj) {
            addHolder(str, obj);
            return this;
        }

        public ToStringHelper add(String str, boolean z) {
            addHolder(str, String.valueOf(z));
            return this;
        }
    }

    public static Object firstNonNull(Object obj, Object obj2) {
        if (obj != null) {
            return obj;
        }
        Preconditions.checkNotNull(obj2);
        return obj2;
    }

    public static ToStringHelper toStringHelper(Class cls) {
        return new ToStringHelper(cls.getSimpleName());
    }

    public static ToStringHelper toStringHelper(Object obj) {
        return new ToStringHelper(obj.getClass().getSimpleName());
    }
}
