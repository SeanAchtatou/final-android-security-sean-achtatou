package com.google.common.base;

import java.util.logging.Logger;

public final class Platform {
    private static final Logger logger = Logger.getLogger(Platform.class.getName());
    private static final JdkPatternCompiler patternCompiler = new JdkPatternCompiler();

    public final class JdkPatternCompiler {
    }

    public static boolean stringIsNullOrEmpty(String str) {
        if (str == null || str.isEmpty()) {
            return true;
        }
        return false;
    }

    private Platform() {
    }
}
