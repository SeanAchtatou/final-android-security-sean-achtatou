package com.google.firebase.messaging;

import X.AnonymousClass1WN;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.regex.Pattern;

public final class FirebaseMessaging {
    private static FirebaseMessaging A01;
    public static final Pattern A02 = Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");
    public final FirebaseInstanceId A00;

    public static synchronized FirebaseMessaging A00() {
        FirebaseMessaging firebaseMessaging;
        synchronized (FirebaseMessaging.class) {
            if (A01 == null) {
                A01 = new FirebaseMessaging(FirebaseInstanceId.getInstance(AnonymousClass1WN.A00()));
            }
            firebaseMessaging = A01;
        }
        return firebaseMessaging;
    }

    private FirebaseMessaging(FirebaseInstanceId firebaseInstanceId) {
        this.A00 = firebaseInstanceId;
    }
}
