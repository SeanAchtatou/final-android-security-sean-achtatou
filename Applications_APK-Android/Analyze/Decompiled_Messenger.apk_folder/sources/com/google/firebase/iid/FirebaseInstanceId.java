package com.google.firebase.iid;

import X.AnonymousClass1WN;
import X.AnonymousClass1Wh;
import X.AnonymousClass1X1;
import X.AnonymousClass1X2;
import X.AnonymousClass1X5;
import X.AnonymousClass1X6;
import X.AnonymousClass24B;
import X.C24591We;
import X.C24701Wt;
import X.C24721Wv;
import X.C24731Ww;
import X.C24741Wx;
import X.C24751Wy;
import X.C30332EuG;
import X.C53092kB;
import X.C53102kC;
import X.C53112kD;
import X.C53122kE;
import X.C53232kQ;
import X.C70893bS;
import X.C72733es;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseInstanceId {
    public static C24721Wv A08;
    private static ScheduledThreadPoolExecutor A09;
    private static final long A0A = TimeUnit.HOURS.toSeconds(8);
    public C24741Wx A00;
    private boolean A01 = false;
    public final AnonymousClass1WN A02;
    public final C24701Wt A03;
    public final AnonymousClass1X5 A04;
    public final Executor A05;
    private final AnonymousClass1X2 A06;
    private final AnonymousClass1X1 A07;

    public static void A03() {
        boolean isLoggable = Log.isLoggable("FirebaseInstanceId", 3);
    }

    public static final synchronized void A05(FirebaseInstanceId firebaseInstanceId) {
        synchronized (firebaseInstanceId) {
            if (!firebaseInstanceId.A01) {
                firebaseInstanceId.A09(0);
            }
        }
    }

    public final synchronized void A08() {
        A08.A03();
        if (this.A06.A00()) {
            A05(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final synchronized void A09(long j) {
        A06(new C70893bS(this, this.A07, Math.min(Math.max(30L, j << 1), A0A)), j);
        this.A01 = true;
    }

    public final synchronized void A0A(String str) {
        String A022;
        int i;
        AnonymousClass1X1 r4 = this.A07;
        synchronized (r4) {
            synchronized (r4.A01) {
                A022 = r4.A01.A02();
                C24721Wv r2 = r4.A01;
                StringBuilder sb = new StringBuilder(String.valueOf(A022).length() + 1 + String.valueOf(str).length());
                sb.append(A022);
                sb.append(",");
                sb.append(str);
                r2.A04(sb.toString());
            }
            C53122kE r3 = new C53122kE();
            Map map = r4.A02;
            if (TextUtils.isEmpty(A022)) {
                i = 0;
            } else {
                i = A022.split(",").length - 1;
            }
            map.put(Integer.valueOf(r4.A00 + i), r3);
        }
        A05(this);
    }

    public final synchronized void A0B(boolean z) {
        this.A01 = z;
    }

    public static AnonymousClass1X6 A00(String str, String str2) {
        AnonymousClass1X6 r6;
        C24721Wv r3 = A08;
        synchronized (r3) {
            String string = r3.A01.getString(C24721Wv.A01(BuildConfig.FLAVOR, str, str2), null);
            if (!TextUtils.isEmpty(string)) {
                if (string.startsWith("{")) {
                    try {
                        JSONObject jSONObject = new JSONObject(string);
                        r6 = new AnonymousClass1X6(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
                    } catch (JSONException e) {
                        String valueOf = String.valueOf(e);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 23);
                        sb.append("Failed to parse token: ");
                        sb.append(valueOf);
                        Log.w("FirebaseInstanceId", sb.toString());
                    }
                } else {
                    r6 = new AnonymousClass1X6(string, null, 0);
                }
            }
            r6 = null;
        }
        return r6;
    }

    public static final Object A01(FirebaseInstanceId firebaseInstanceId, C53112kD r4) {
        try {
            return C53232kQ.A01(r4, 30000, TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                if ("INSTANCE_ID_RESET".equals(cause.getMessage())) {
                    firebaseInstanceId.A08();
                }
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(e);
            }
        } catch (InterruptedException | TimeoutException unused) {
            throw new IOException(AnonymousClass24B.$const$string(158));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x007f A[SYNTHETIC, Splitter:B:39:0x007f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02() {
        /*
            X.1Wv r3 = com.google.firebase.iid.FirebaseInstanceId.A08
            java.lang.String r4 = ""
            monitor-enter(r3)
            java.util.Map r0 = r3.A03     // Catch:{ all -> 0x00d5 }
            java.lang.Object r1 = r0.get(r4)     // Catch:{ all -> 0x00d5 }
            X.3dr r1 = (X.C72203dr) r1     // Catch:{ all -> 0x00d5 }
            if (r1 != 0) goto L_0x00a4
            X.1Ww r6 = r3.A02     // Catch:{ 1wX -> 0x0085 }
            android.content.Context r5 = r3.A00     // Catch:{ 1wX -> 0x0085 }
            r9 = 0
            java.io.File r2 = X.C24731Ww.A04(r5, r4)     // Catch:{ 1wX -> 0x0067 }
            boolean r0 = r2.exists()     // Catch:{ 1wX -> 0x0067 }
            if (r0 != 0) goto L_0x001f
            goto L_0x0038
        L_0x001f:
            X.3dr r0 = X.C24731Ww.A02(r2)     // Catch:{ 1wX | IOException -> 0x0025 }
            r1 = r0
            goto L_0x003a
        L_0x0025:
            r1 = move-exception
            r0 = 3
            java.lang.String r8 = "FirebaseInstanceId"
            boolean r0 = android.util.Log.isLoggable(r8, r0)     // Catch:{ 1wX -> 0x0067 }
            if (r0 == 0) goto L_0x0032
            java.lang.String.valueOf(r1)     // Catch:{ 1wX -> 0x0067 }
        L_0x0032:
            X.3dr r0 = X.C24731Ww.A02(r2)     // Catch:{ IOException -> 0x0042 }
            r1 = r0
            goto L_0x003a
        L_0x0038:
            r0 = 0
            r1 = r9
        L_0x003a:
            if (r0 == 0) goto L_0x0040
            X.C24731Ww.A06(r5, r4, r1)     // Catch:{ 1wX -> 0x0067 }
            goto L_0x007d
        L_0x0040:
            r0 = r9
            goto L_0x0068
        L_0x0042:
            r7 = move-exception
            java.lang.String r2 = java.lang.String.valueOf(r7)     // Catch:{ 1wX -> 0x0067 }
            int r0 = r2.length()     // Catch:{ 1wX -> 0x0067 }
            int r0 = r0 + 45
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ 1wX -> 0x0067 }
            r1.<init>(r0)     // Catch:{ 1wX -> 0x0067 }
            java.lang.String r0 = "IID file exists, but failed to read from it: "
            r1.append(r0)     // Catch:{ 1wX -> 0x0067 }
            r1.append(r2)     // Catch:{ 1wX -> 0x0067 }
            java.lang.String r0 = r1.toString()     // Catch:{ 1wX -> 0x0067 }
            android.util.Log.w(r8, r0)     // Catch:{ 1wX -> 0x0067 }
            X.1wX r0 = new X.1wX     // Catch:{ 1wX -> 0x0067 }
            r0.<init>(r7)     // Catch:{ 1wX -> 0x0067 }
            throw r0     // Catch:{ 1wX -> 0x0067 }
        L_0x0067:
            r0 = move-exception
        L_0x0068:
            java.lang.String r1 = "com.google.android.gms.appid"
            r2 = 0
            android.content.SharedPreferences r1 = r5.getSharedPreferences(r1, r2)     // Catch:{ 1wX -> 0x0079 }
            X.3dr r1 = X.C24731Ww.A01(r1, r4)     // Catch:{ 1wX -> 0x0079 }
            if (r1 == 0) goto L_0x007a
            X.C24731Ww.A00(r5, r4, r1, r2)     // Catch:{ 1wX -> 0x0079 }
            goto L_0x007d
        L_0x0079:
            r0 = move-exception
        L_0x007a:
            if (r0 != 0) goto L_0x0084
            r1 = r9
        L_0x007d:
            if (r1 != 0) goto L_0x009f
            X.3dr r1 = r6.A0A(r5, r4)     // Catch:{ 1wX -> 0x0085 }
            goto L_0x009f
        L_0x0084:
            throw r0     // Catch:{ 1wX -> 0x0085 }
        L_0x0085:
            java.lang.String r1 = "FirebaseInstanceId"
            java.lang.String r0 = "Stored data is corrupt, generating new identity"
            android.util.Log.w(r1, r0)     // Catch:{ all -> 0x00d5 }
            X.1WN r0 = X.AnonymousClass1WN.A00()     // Catch:{ all -> 0x00d5 }
            com.google.firebase.iid.FirebaseInstanceId r0 = getInstance(r0)     // Catch:{ all -> 0x00d5 }
            r0.A08()     // Catch:{ all -> 0x00d5 }
            X.1Ww r1 = r3.A02     // Catch:{ all -> 0x00d5 }
            android.content.Context r0 = r3.A00     // Catch:{ all -> 0x00d5 }
            X.3dr r1 = r1.A0A(r0, r4)     // Catch:{ all -> 0x00d5 }
        L_0x009f:
            java.util.Map r0 = r3.A03     // Catch:{ all -> 0x00d5 }
            r0.put(r4, r1)     // Catch:{ all -> 0x00d5 }
        L_0x00a4:
            monitor-exit(r3)
            java.security.KeyPair r0 = r1.A01
            java.security.PublicKey r0 = r0.getPublic()
            byte[] r1 = r0.getEncoded()
            java.lang.String r0 = "SHA1"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x00cc }
            byte[] r3 = r0.digest(r1)     // Catch:{ NoSuchAlgorithmException -> 0x00cc }
            r2 = 0
            byte r0 = r3[r2]     // Catch:{ NoSuchAlgorithmException -> 0x00cc }
            r0 = r0 & 15
            int r0 = r0 + 112
            byte r0 = (byte) r0     // Catch:{ NoSuchAlgorithmException -> 0x00cc }
            r3[r2] = r0     // Catch:{ NoSuchAlgorithmException -> 0x00cc }
            r1 = 8
            r0 = 11
            java.lang.String r0 = android.util.Base64.encodeToString(r3, r2, r1, r0)     // Catch:{ NoSuchAlgorithmException -> 0x00cc }
            return r0
        L_0x00cc:
            java.lang.String r1 = "FirebaseInstanceId"
            java.lang.String r0 = "Unexpected error, device missing required algorithms"
            android.util.Log.w(r1, r0)
            r0 = 0
            return r0
        L_0x00d5:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.FirebaseInstanceId.A02():java.lang.String");
    }

    public static final void A04(FirebaseInstanceId firebaseInstanceId) {
        boolean z;
        if (!firebaseInstanceId.A0C(A00(C24701Wt.A01(firebaseInstanceId.A02), "*"))) {
            AnonymousClass1X1 r2 = firebaseInstanceId.A07;
            synchronized (r2) {
                z = false;
                if (AnonymousClass1X1.A00(r2) != null) {
                    z = true;
                }
            }
            if (!z) {
                return;
            }
        }
        A05(firebaseInstanceId);
    }

    public static void A06(Runnable runnable, long j) {
        synchronized (FirebaseInstanceId.class) {
            if (A09 == null) {
                A09 = new ScheduledThreadPoolExecutor(1, new C53092kB("FirebaseInstanceId"));
            }
            A09.schedule(runnable, j, TimeUnit.SECONDS);
        }
    }

    public static FirebaseInstanceId getInstance(AnonymousClass1WN r2) {
        AnonymousClass1WN.A02(r2);
        return (FirebaseInstanceId) r2.A03.A02(FirebaseInstanceId.class);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001c, code lost:
        if (r6.equals(r8.A02) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0C(X.AnonymousClass1X6 r8) {
        /*
            r7 = this;
            if (r8 == 0) goto L_0x0023
            X.1Wt r0 = r7.A03
            java.lang.String r6 = r0.A05()
            long r4 = java.lang.System.currentTimeMillis()
            long r2 = r8.A00
            long r0 = X.AnonymousClass1X6.A03
            long r2 = r2 + r0
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x001e
            java.lang.String r0 = r8.A02
            boolean r1 = r6.equals(r0)
            r0 = 0
            if (r1 != 0) goto L_0x001f
        L_0x001e:
            r0 = 1
        L_0x001f:
            if (r0 != 0) goto L_0x0023
            r0 = 0
            return r0
        L_0x0023:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.FirebaseInstanceId.A0C(X.1X6):boolean");
    }

    public FirebaseInstanceId(AnonymousClass1WN r10, C24701Wt r11, Executor executor, Executor executor2, AnonymousClass1Wh r14, C24591We r15) {
        AnonymousClass1WN r4 = r10;
        if (C24701Wt.A01(r10) != null) {
            synchronized (FirebaseInstanceId.class) {
                if (A08 == null) {
                    AnonymousClass1WN.A02(r10);
                    A08 = new C24721Wv(r10.A00, new C24731Ww());
                }
            }
            this.A02 = r10;
            C24701Wt r5 = r11;
            this.A03 = r11;
            Executor executor3 = executor;
            if (this.A00 == null) {
                AnonymousClass1WN.A02(r10);
                C24741Wx r2 = (C24741Wx) r10.A03.A02(C24741Wx.class);
                if (r2 != null) {
                    if (r2.A00.A03() != 0) {
                        this.A00 = r2;
                    }
                }
                AnonymousClass1WN.A02(r10);
                this.A00 = new C24741Wx(r4, r5, executor3, new C24751Wy(r10.A00, r11), r15);
            }
            this.A00 = this.A00;
            this.A05 = executor2;
            this.A07 = new AnonymousClass1X1(A08);
            AnonymousClass1X2 r1 = new AnonymousClass1X2(this, r14);
            this.A06 = r1;
            this.A04 = new AnonymousClass1X5(executor);
            if (r1.A00()) {
                A04(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
    }

    public String A07(String str, String str2) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            if (str2.isEmpty() || str2.equalsIgnoreCase("fcm") || str2.equalsIgnoreCase("gcm")) {
                str2 = "*";
            }
            C53102kC r2 = new C53102kC();
            r2.A0K(null);
            return ((C30332EuG) A01(this, r2.A06(this.A05, new C72733es(this, str, str2)))).A00;
        }
        throw new IOException(TurboLoader.Locator.$const$string(114));
    }
}
