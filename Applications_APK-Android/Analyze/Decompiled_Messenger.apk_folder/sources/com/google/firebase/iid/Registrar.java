package com.google.firebase.iid;

import X.AnonymousClass09E;
import X.AnonymousClass1WN;
import X.AnonymousClass1WX;
import X.AnonymousClass1WY;
import X.AnonymousClass1Wh;
import X.AnonymousClass1Wj;
import X.C24551Wa;
import X.C24591We;
import X.C24601Wf;
import X.C24611Wk;
import X.C24621Wl;
import java.util.Arrays;
import java.util.List;

public final class Registrar {
    public final List getComponents() {
        Class<FirebaseInstanceId> cls = FirebaseInstanceId.class;
        AnonymousClass1WY r7 = new AnonymousClass1WY(cls, new Class[0]);
        r7.A01(new C24601Wf(AnonymousClass1WN.class, 1, 0));
        r7.A01(new C24601Wf(AnonymousClass1Wh.class, 1, 0));
        r7.A01(new C24601Wf(C24591We.class, 1, 0));
        C24551Wa r1 = AnonymousClass1Wj.A00;
        AnonymousClass09E.A02(r1, "Null factory");
        r7.A02 = r1;
        boolean z = false;
        if (r7.A00 == 0) {
            z = true;
        }
        AnonymousClass09E.A09(z, "Instantiation type has already been set.");
        r7.A00 = 1;
        AnonymousClass1WX A00 = r7.A00();
        AnonymousClass1WY r3 = new AnonymousClass1WY(C24611Wk.class, new Class[0]);
        r3.A01(new C24601Wf(cls, 1, 0));
        C24551Wa r12 = C24621Wl.A00;
        AnonymousClass09E.A02(r12, "Null factory");
        r3.A02 = r12;
        return Arrays.asList(A00, r3.A00());
    }
}
