package androidx.activity;

import X.AnonymousClass0qM;
import X.C11410n6;
import X.C14820uB;
import X.C27641dW;
import X.C27661dY;
import X.C27751dh;
import X.C27901dw;
import X.C31701k7;

public class OnBackPressedDispatcher$LifecycleOnBackPressedCancellable implements C27901dw, C27661dY {
    private C27901dw A00;
    private final C27751dh A01;
    private final AnonymousClass0qM A02;
    public final /* synthetic */ C27641dW A03;

    public OnBackPressedDispatcher$LifecycleOnBackPressedCancellable(C27641dW r1, AnonymousClass0qM r2, C27751dh r3) {
        this.A03 = r1;
        this.A02 = r2;
        this.A01 = r3;
        r2.A06(this);
    }

    public void Bpu(C11410n6 r5, C14820uB r6) {
        if (r6 == C14820uB.ON_START) {
            C27641dW r3 = this.A03;
            C27751dh r2 = this.A01;
            r3.A00.add(r2);
            C31701k7 r1 = new C31701k7(r3, r2);
            r2.A00.add(r1);
            this.A00 = r1;
        } else if (r6 == C14820uB.ON_STOP) {
            C27901dw r0 = this.A00;
            if (r0 != null) {
                r0.cancel();
            }
        } else if (r6 == C14820uB.ON_DESTROY) {
            cancel();
        }
    }

    public void cancel() {
        this.A02.A07(this);
        this.A01.A00.remove(this);
        C27901dw r0 = this.A00;
        if (r0 != null) {
            r0.cancel();
            this.A00 = null;
        }
    }
}
