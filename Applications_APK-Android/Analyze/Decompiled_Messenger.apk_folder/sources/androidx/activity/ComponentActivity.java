package androidx.activity;

import X.AnonymousClass0n8;
import X.AnonymousClass0qM;
import X.AnonymousClass1XL;
import X.C000700l;
import X.C11410n6;
import X.C12990qL;
import X.C14400tF;
import X.C14820uB;
import X.C27521dK;
import X.C27531dL;
import X.C27541dM;
import X.C27621dU;
import X.C27631dV;
import X.C27641dW;
import X.C27651dX;
import X.C27661dY;
import X.C27911dx;
import X.C37101uh;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class ComponentActivity extends androidx.core.app.ComponentActivity implements C11410n6, AnonymousClass0n8, C27521dK, C27531dL, C27541dM {
    private C27911dx A00;
    public final C12990qL A01 = new C12990qL(this);
    private final C27641dW A02 = new C27641dW(new C27651dX(this));
    private final C27621dU A03 = new C27621dU(this);

    public final C27641dW AwJ() {
        return this.A02;
    }

    public final C27631dV B1u() {
        return this.A03.A00;
    }

    public void onBackPressed() {
        this.A02.A00();
    }

    public final Object onRetainNonConfigurationInstance() {
        C37101uh r0;
        C27911dx r1 = this.A00;
        if (r1 == null && (r0 = (C37101uh) getLastNonConfigurationInstance()) != null) {
            r1 = r0.A00;
        }
        if (r1 == null) {
            return null;
        }
        C37101uh r02 = new C37101uh();
        r02.A00 = r1;
        return r02;
    }

    public ComponentActivity() {
        if (AsK() != null) {
            if (Build.VERSION.SDK_INT >= 19) {
                AsK().A06(new C27661dY() {
                    public void Bpu(C11410n6 r2, C14820uB r3) {
                        View view;
                        if (r3 == C14820uB.ON_STOP) {
                            Window window = ComponentActivity.this.getWindow();
                            if (window != null) {
                                view = window.peekDecorView();
                            } else {
                                view = null;
                            }
                            if (view != null) {
                                view.cancelPendingInputEvents();
                            }
                        }
                    }
                });
            }
            AsK().A06(new C27661dY() {
                public void Bpu(C11410n6 r2, C14820uB r3) {
                    if (r3 == C14820uB.ON_DESTROY && !ComponentActivity.this.isChangingConfigurations()) {
                        ComponentActivity.this.B9I().A00();
                    }
                }
            });
            int i = Build.VERSION.SDK_INT;
            if (19 <= i && i <= 23) {
                AsK().A06(new ImmLeaksCleaner(this));
                return;
            }
            return;
        }
        throw new IllegalStateException("getLifecycle() returned null in ComponentActivity's constructor. Please make sure you are lazily constructing your Lifecycle in the first call to getLifecycle() rather than relying on field initialization.");
    }

    public C27911dx B9I() {
        if (getApplication() != null) {
            if (this.A00 == null) {
                C37101uh r0 = (C37101uh) getLastNonConfigurationInstance();
                if (r0 != null) {
                    this.A00 = r0.A00;
                }
                if (this.A00 == null) {
                    this.A00 = new C27911dx();
                }
            }
            return this.A00;
        }
        throw new IllegalStateException("Your activity is not yet attached to the Application instance. You can't request ViewModel before onCreate call.");
    }

    public void onCreate(Bundle bundle) {
        int A002 = C000700l.A00(950917542);
        super.onCreate(bundle);
        this.A03.A00(bundle);
        C14400tF.A00(this);
        C000700l.A07(-1508650169, A002);
    }

    public void onSaveInstanceState(Bundle bundle) {
        AnonymousClass0qM AsK = AsK();
        if (AsK instanceof C12990qL) {
            C12990qL.A04((C12990qL) AsK, AnonymousClass1XL.CREATED);
        }
        super.onSaveInstanceState(bundle);
        this.A03.A01(bundle);
    }
}
