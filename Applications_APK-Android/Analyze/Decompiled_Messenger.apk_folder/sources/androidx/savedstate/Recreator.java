package androidx.savedstate;

import X.AnonymousClass08S;
import X.C11410n6;
import X.C14820uB;
import X.C27521dK;
import X.C27631dV;
import X.C27661dY;
import X.E4E;
import android.os.Bundle;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;

public final class Recreator implements C27661dY {
    public final C27521dK A00;

    public void Bpu(C11410n6 r6, C14820uB r7) {
        Bundle bundle;
        if (r7 == C14820uB.ON_CREATE) {
            r6.AsK().A07(this);
            C27631dV B1u = this.A00.B1u();
            if (B1u.A03) {
                Bundle bundle2 = B1u.A01;
                if (bundle2 != null) {
                    bundle = bundle2.getBundle("androidx.savedstate.Restarter");
                    B1u.A01.remove("androidx.savedstate.Restarter");
                    if (B1u.A01.isEmpty()) {
                        B1u.A01 = null;
                    }
                } else {
                    bundle = null;
                }
                if (bundle != null) {
                    ArrayList<String> stringArrayList = bundle.getStringArrayList("classes_to_restore");
                    if (stringArrayList != null) {
                        Iterator<String> it = stringArrayList.iterator();
                        while (it.hasNext()) {
                            String next = it.next();
                            try {
                                Class<? extends U> asSubclass = Class.forName(next, false, Recreator.class.getClassLoader()).asSubclass(E4E.class);
                                try {
                                    Constructor<? extends U> declaredConstructor = asSubclass.getDeclaredConstructor(new Class[0]);
                                    declaredConstructor.setAccessible(true);
                                    try {
                                        ((E4E) declaredConstructor.newInstance(new Object[0])).BlI(this.A00);
                                    } catch (Exception e) {
                                        throw new RuntimeException(AnonymousClass08S.A0J("Failed to instantiate ", next), e);
                                    }
                                } catch (NoSuchMethodException e2) {
                                    throw new IllegalStateException(AnonymousClass08S.A0P("Class", asSubclass.getSimpleName(), " must have default constructor in order to be automatically recreated"), e2);
                                }
                            } catch (ClassNotFoundException e3) {
                                throw new RuntimeException(AnonymousClass08S.A0P("Class ", next, " wasn't found"), e3);
                            }
                        }
                        return;
                    }
                    throw new IllegalStateException("Bundle with restored state for the component \"androidx.savedstate.Restarter\" must contain list of strings by the key \"classes_to_restore\"");
                }
                return;
            }
            throw new IllegalStateException("You can consumeRestoredStateForKey only after super.onCreate of corresponding component");
        }
        throw new AssertionError("Next event must be ON_CREATE");
    }

    public Recreator(C27521dK r1) {
        this.A00 = r1;
    }
}
