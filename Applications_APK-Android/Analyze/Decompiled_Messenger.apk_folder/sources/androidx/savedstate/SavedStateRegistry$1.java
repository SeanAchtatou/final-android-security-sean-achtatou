package androidx.savedstate;

import X.C11410n6;
import X.C14820uB;
import X.C27631dV;
import X.C27661dY;

public class SavedStateRegistry$1 implements C27661dY {
    public final /* synthetic */ C27631dV A00;

    public SavedStateRegistry$1(C27631dV r1) {
        this.A00 = r1;
    }

    public void Bpu(C11410n6 r3, C14820uB r4) {
        if (r4 == C14820uB.ON_START) {
            this.A00.A00 = true;
        } else if (r4 == C14820uB.ON_STOP) {
            this.A00.A00 = false;
        }
    }
}
