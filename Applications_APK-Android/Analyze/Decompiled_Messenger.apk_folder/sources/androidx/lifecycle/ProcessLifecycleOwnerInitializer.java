package androidx.lifecycle;

import X.AnonymousClass1XG;
import X.AnonymousClass1XH;
import X.AnonymousClass1XJ;
import X.AnonymousClass1XQ;
import X.C14820uB;
import android.app.Application;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

public class ProcessLifecycleOwnerInitializer extends ContentProvider {
    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public boolean onCreate() {
        Context context = getContext();
        if (!AnonymousClass1XG.A00.getAndSet(true)) {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new AnonymousClass1XH());
        }
        Context context2 = getContext();
        AnonymousClass1XJ r2 = AnonymousClass1XJ.A08;
        r2.A02 = new Handler();
        r2.A07.A08(C14820uB.ON_CREATE);
        ((Application) context2.getApplicationContext()).registerActivityLifecycleCallbacks(new AnonymousClass1XQ(r2));
        return true;
    }
}
