package androidx.fragment.app;

import X.AnonymousClass0Eh;
import X.AnonymousClass1XL;
import X.C13060qW;
import X.C16280wn;
import X.C29471gT;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;

public final class BackStackState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0Eh();
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final CharSequence A04;
    public final CharSequence A05;
    public final String A06;
    public final ArrayList A07;
    public final ArrayList A08;
    public final ArrayList A09;
    public final boolean A0A;
    public final int[] A0B;
    public final int[] A0C;
    public final int[] A0D;

    public int describeContents() {
        return 0;
    }

    public C16280wn A00(C13060qW r10) {
        C16280wn r2 = new C16280wn(r10);
        int i = 0;
        int i2 = 0;
        while (true) {
            int[] iArr = this.A0D;
            if (i < iArr.length) {
                C29471gT r7 = new C29471gT();
                int i3 = i + 1;
                r7.A00 = iArr[i];
                C13060qW.A0I(2);
                String str = (String) this.A07.get(i2);
                if (str != null) {
                    r7.A05 = r10.A0Q.A00(str);
                } else {
                    r7.A05 = null;
                }
                r7.A07 = AnonymousClass1XL.values()[this.A0C[i2]];
                r7.A06 = AnonymousClass1XL.values()[this.A0B[i2]];
                int[] iArr2 = this.A0D;
                int i4 = i3 + 1;
                int i5 = iArr2[i3];
                r7.A01 = i5;
                int i6 = i4 + 1;
                int i7 = iArr2[i4];
                r7.A02 = i7;
                int i8 = i6 + 1;
                int i9 = iArr2[i6];
                r7.A03 = i9;
                i = i8 + 1;
                int i10 = iArr2[i8];
                r7.A04 = i10;
                r2.A07 = i5;
                r2.A08 = i7;
                r2.A09 = i9;
                r2.A0A = i10;
                r2.A0D(r7);
                i2++;
            } else {
                r2.A0B = this.A03;
                r2.A04 = this.A06;
                r2.A00 = this.A02;
                r2.A05 = true;
                r2.A01 = this.A01;
                r2.A03 = this.A05;
                r2.A00 = this.A00;
                r2.A02 = this.A04;
                r2.A0D = this.A08;
                r2.A0E = this.A09;
                r2.A0F = this.A0A;
                r2.A0N(1);
                return r2;
            }
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(this.A0D);
        parcel.writeStringList(this.A07);
        parcel.writeIntArray(this.A0C);
        parcel.writeIntArray(this.A0B);
        parcel.writeInt(this.A03);
        parcel.writeString(this.A06);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A01);
        TextUtils.writeToParcel(this.A05, parcel, 0);
        parcel.writeInt(this.A00);
        TextUtils.writeToParcel(this.A04, parcel, 0);
        parcel.writeStringList(this.A08);
        parcel.writeStringList(this.A09);
        parcel.writeInt(this.A0A ? 1 : 0);
    }

    public BackStackState(C16280wn r9) {
        String str;
        int size = r9.A0C.size();
        this.A0D = new int[(size * 5)];
        if (r9.A05) {
            this.A07 = new ArrayList(size);
            this.A0C = new int[size];
            this.A0B = new int[size];
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                C29471gT r5 = (C29471gT) r9.A0C.get(i2);
                int i3 = i + 1;
                this.A0D[i] = r5.A00;
                ArrayList arrayList = this.A07;
                Fragment fragment = r5.A05;
                if (fragment != null) {
                    str = fragment.A0V;
                } else {
                    str = null;
                }
                arrayList.add(str);
                int[] iArr = this.A0D;
                int i4 = i3 + 1;
                iArr[i3] = r5.A01;
                int i5 = i4 + 1;
                iArr[i4] = r5.A02;
                int i6 = i5 + 1;
                iArr[i5] = r5.A03;
                i = i6 + 1;
                iArr[i6] = r5.A04;
                this.A0C[i2] = r5.A07.ordinal();
                this.A0B[i2] = r5.A06.ordinal();
            }
            this.A03 = r9.A0B;
            this.A06 = r9.A04;
            this.A02 = r9.A00;
            this.A01 = r9.A01;
            this.A05 = r9.A03;
            this.A00 = r9.A00;
            this.A04 = r9.A02;
            this.A08 = r9.A0D;
            this.A09 = r9.A0E;
            this.A0A = r9.A0F;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    public BackStackState(Parcel parcel) {
        this.A0D = parcel.createIntArray();
        this.A07 = parcel.createStringArrayList();
        this.A0C = parcel.createIntArray();
        this.A0B = parcel.createIntArray();
        this.A03 = parcel.readInt();
        this.A06 = parcel.readString();
        this.A02 = parcel.readInt();
        this.A01 = parcel.readInt();
        this.A05 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A00 = parcel.readInt();
        this.A04 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A08 = parcel.createStringArrayList();
        this.A09 = parcel.createStringArrayList();
        this.A0A = parcel.readInt() != 0;
    }
}
