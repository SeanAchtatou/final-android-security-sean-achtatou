package androidx.fragment.app;

import X.C02100Cx;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class FragmentManagerState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C02100Cx();
    public int A00;
    public String A01 = null;
    public ArrayList A02;
    public ArrayList A03;
    public BackStackState[] A04;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.A02);
        parcel.writeStringList(this.A03);
        parcel.writeTypedArray(this.A04, i);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A01);
    }

    public FragmentManagerState() {
    }

    public FragmentManagerState(Parcel parcel) {
        this.A02 = parcel.createTypedArrayList(FragmentState.CREATOR);
        this.A03 = parcel.createStringArrayList();
        this.A04 = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
        this.A00 = parcel.readInt();
        this.A01 = parcel.readString();
    }
}
