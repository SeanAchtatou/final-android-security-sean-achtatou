package androidx.fragment.app;

import X.AnonymousClass08S;
import X.AnonymousClass0n8;
import X.AnonymousClass0qX;
import X.AnonymousClass0qZ;
import X.AnonymousClass1XL;
import X.AnonymousClass2NS;
import X.C000700l;
import X.C07870eJ;
import X.C11430n9;
import X.C11440nA;
import X.C12990qL;
import X.C13010qQ;
import X.C13020qR;
import X.C13060qW;
import X.C14820uB;
import X.C99084oO;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.activity.ComponentActivity;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class FragmentActivity extends ComponentActivity implements C11430n9, C11440nA {
    public boolean A00;
    public boolean A01;
    public boolean A02;
    public boolean A03 = true;
    public int A04;
    public C07870eJ A05;
    public final C12990qL A06 = new C12990qL(this);
    public final AnonymousClass0qZ A07;

    public void A11(Fragment fragment) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0056, code lost:
        if (android.os.Build.VERSION.SDK_INT >= 16) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A12(androidx.fragment.app.Fragment r8, android.content.Intent r9, int r10, android.os.Bundle r11) {
        /*
            r7 = this;
            r5 = 1
            r7.A02 = r5
            r3 = -1
            r4 = 0
            if (r10 != r3) goto L_0x0008
            goto L_0x0052
        L_0x0008:
            A0y(r10)     // Catch:{ all -> 0x0062 }
            X.0eJ r6 = r7.A05     // Catch:{ all -> 0x0062 }
            int r0 = r6.A01()     // Catch:{ all -> 0x0062 }
            r3 = 65534(0xfffe, float:9.1833E-41)
            if (r0 < r3) goto L_0x001e
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0062 }
            java.lang.String r0 = "Too many pending Fragment activity results."
            r1.<init>(r0)     // Catch:{ all -> 0x0062 }
            throw r1     // Catch:{ all -> 0x0062 }
        L_0x001e:
            int r2 = r7.A04     // Catch:{ all -> 0x0062 }
            boolean r0 = r6.A01     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x0027
            X.C07870eJ.A00(r6)     // Catch:{ all -> 0x0062 }
        L_0x0027:
            int[] r1 = r6.A02     // Catch:{ all -> 0x0062 }
            int r0 = r6.A00     // Catch:{ all -> 0x0062 }
            int r0 = X.AnonymousClass04d.A02(r1, r0, r2)     // Catch:{ all -> 0x0062 }
            if (r0 < 0) goto L_0x0037
            int r0 = r2 + 1
            int r0 = r0 % r3
            r7.A04 = r0     // Catch:{ all -> 0x0062 }
            goto L_0x001e
        L_0x0037:
            java.lang.String r0 = r8.A0V     // Catch:{ all -> 0x0062 }
            r6.A09(r2, r0)     // Catch:{ all -> 0x0062 }
            int r0 = r7.A04     // Catch:{ all -> 0x0062 }
            int r0 = r0 + 1
            int r0 = r0 % r3
            r7.A04 = r0     // Catch:{ all -> 0x0062 }
            int r2 = r2 + r5
            int r3 = r2 << 16
            r0 = 65535(0xffff, float:9.1834E-41)
            r10 = r10 & r0
            int r3 = r3 + r10
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0062 }
            r0 = 16
            if (r1 < r0) goto L_0x005c
            goto L_0x0058
        L_0x0052:
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0062 }
            r0 = 16
            if (r1 < r0) goto L_0x005c
        L_0x0058:
            r7.startActivityForResult(r9, r3, r11)     // Catch:{ all -> 0x0062 }
            goto L_0x005f
        L_0x005c:
            r7.startActivityForResult(r9, r3)     // Catch:{ all -> 0x0062 }
        L_0x005f:
            r7.A02 = r4
            return
        L_0x0062:
            r0 = move-exception
            r7.A02 = r4
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentActivity.A12(androidx.fragment.app.Fragment, android.content.Intent, int, android.os.Bundle):void");
    }

    public final void CLZ(int i) {
        if (i != -1) {
            A0y(i);
        }
    }

    private static void A0y(int i) {
        if ((i & -65536) != 0) {
            throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        }
    }

    public void A10() {
        this.A06.A08(C14820uB.ON_RESUME);
        this.A07.A02();
    }

    public C13060qW B4m() {
        return this.A07.A00.A03;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        this.A07.A00.A03.A0Z();
        int i3 = i >> 16;
        if (i3 != 0) {
            int i4 = i3 - 1;
            C07870eJ r0 = this.A05;
            String str = (String) r0.A04(i4);
            r0.A07(i4);
            if (str == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
                return;
            }
            Fragment A0R = this.A07.A00.A03.A0R(str);
            if (A0R == null) {
                Log.w("FragmentActivity", AnonymousClass08S.A0J("Activity result no fragment exists for who: ", str));
            } else {
                A0R.BNH(i & 65535, i2, intent);
            }
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        if (i != 0) {
            return super.onCreatePanelMenu(i, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i, menu);
        AnonymousClass0qZ r0 = this.A07;
        return onCreatePanelMenu | r0.A00.A03.A16(menu, getMenuInflater());
    }

    public void onMultiWindowModeChanged(boolean z) {
        this.A07.A00.A03.A10(z);
    }

    public void onPanelClosed(int i, Menu menu) {
        if (i == 0) {
            this.A07.A00.A03.A0f(menu);
        }
        super.onPanelClosed(i, menu);
    }

    public void onPictureInPictureModeChanged(boolean z) {
        this.A07.A00.A03.A11(z);
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        if (i == 0) {
            return super.onPreparePanel(0, view, menu) | this.A07.A00.A03.A15(menu);
        }
        return super.onPreparePanel(i, view, menu);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        this.A07.A00.A03.A0Z();
        int i2 = (i >> 16) & 65535;
        if (i2 != 0) {
            int i3 = i2 - 1;
            C07870eJ r0 = this.A05;
            String str = (String) r0.A04(i3);
            r0.A07(i3);
            if (str == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
            } else if (this.A07.A00.A03.A0R(str) == null) {
                Log.w("FragmentActivity", AnonymousClass08S.A0J("Activity result no fragment exists for who: ", str));
            }
        }
    }

    public void onStateNotSaved() {
        this.A07.A00.A03.A0Z();
    }

    public FragmentActivity() {
        C13010qQ r2 = new C13010qQ(this);
        AnonymousClass0qX.A01(r2, C99084oO.$const$string(424));
        this.A07 = new AnonymousClass0qZ(r2);
    }

    private static boolean A0z(C13060qW r4, AnonymousClass1XL r5) {
        Object A032;
        boolean z = false;
        for (Fragment fragment : r4.A0U()) {
            if (fragment != null) {
                C13020qR r0 = fragment.A0N;
                if (r0 == null) {
                    A032 = null;
                } else {
                    A032 = r0.A03();
                }
                if (A032 != null) {
                    z |= A0z(fragment.A17(), r5);
                }
                boolean z2 = false;
                if (fragment.AsK().A05().compareTo((Enum) AnonymousClass1XL.STARTED) >= 0) {
                    z2 = true;
                }
                if (z2) {
                    C12990qL.A04(fragment.A03, r5);
                    z = true;
                }
            }
        }
        return z;
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String A0J = AnonymousClass08S.A0J(str, "  ");
        printWriter.print(A0J);
        printWriter.print("mCreated=");
        printWriter.print(this.A00);
        printWriter.print(" mResumed=");
        printWriter.print(this.A01);
        printWriter.print(" mStopped=");
        printWriter.print(this.A03);
        if (getApplication() != null) {
            new AnonymousClass2NS(this, B9I()).A05(A0J, fileDescriptor, printWriter, strArr);
        }
        this.A07.A00.A03.A0z(str, fileDescriptor, printWriter, strArr);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A07.A00.A03.A0Z();
        this.A07.A00.A03.A0c(configuration);
    }

    public void onCreate(Bundle bundle) {
        int length;
        int A002 = C000700l.A00(-1607969077);
        C13020qR r1 = this.A07.A00;
        r1.A03.A0v(r1, r1, null);
        if (bundle != null) {
            Parcelable parcelable = bundle.getParcelable(TurboLoader.Locator.$const$string(22));
            C13020qR r12 = this.A07.A00;
            if (r12 instanceof AnonymousClass0n8) {
                r12.A03.A0e(parcelable);
                if (bundle.containsKey("android:support:next_request_index")) {
                    this.A04 = bundle.getInt("android:support:next_request_index");
                    int[] intArray = bundle.getIntArray("android:support:request_indicies");
                    String[] stringArray = bundle.getStringArray("android:support:request_fragment_who");
                    if (intArray == null || stringArray == null || (length = intArray.length) != stringArray.length) {
                        Log.w("FragmentActivity", "Invalid requestCode mapping in savedInstanceState.");
                    } else {
                        this.A05 = new C07870eJ(length);
                        for (int i = 0; i < length; i++) {
                            this.A05.A09(intArray[i], stringArray[i]);
                        }
                    }
                }
            } else {
                throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
            }
        }
        if (this.A05 == null) {
            this.A05 = new C07870eJ();
            this.A04 = 0;
        }
        super.onCreate(bundle);
        this.A06.A08(C14820uB.ON_CREATE);
        this.A07.A01();
        C000700l.A07(-31364971, A002);
    }

    public void onDestroy() {
        int A002 = C000700l.A00(-657998352);
        super.onDestroy();
        this.A07.A00.A03.A0W();
        this.A06.A08(C14820uB.ON_DESTROY);
        C000700l.A07(878966625, A002);
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.A07.A00.A03.A0X();
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        if (i == 0) {
            return this.A07.A00.A03.A18(menuItem);
        }
        if (i != 6) {
            return false;
        }
        return this.A07.A00.A03.A17(menuItem);
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.A07.A00.A03.A0Z();
    }

    public void onPause() {
        int A002 = C000700l.A00(1017292864);
        super.onPause();
        this.A01 = false;
        C13060qW.A0A(this.A07.A00.A03, 3);
        this.A06.A08(C14820uB.ON_PAUSE);
        C000700l.A07(1576098307, A002);
    }

    public void onPostResume() {
        super.onPostResume();
        A10();
    }

    public void onResume() {
        int A002 = C000700l.A00(561736250);
        super.onResume();
        this.A01 = true;
        this.A07.A00.A03.A0Z();
        this.A07.A00.A03.A12(true);
        C000700l.A07(-1018825767, A002);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        do {
        } while (A0z(B4m(), AnonymousClass1XL.CREATED));
        this.A06.A08(C14820uB.ON_STOP);
        Parcelable A0M = this.A07.A00.A03.A0M();
        if (A0M != null) {
            bundle.putParcelable(TurboLoader.Locator.$const$string(22), A0M);
        }
        if (this.A05.A01() > 0) {
            bundle.putInt("android:support:next_request_index", this.A04);
            C07870eJ r4 = this.A05;
            int[] iArr = new int[r4.A01()];
            String[] strArr = new String[r4.A01()];
            for (int i = 0; i < r4.A01(); i++) {
                iArr[i] = r4.A02(i);
                strArr[i] = (String) r4.A05(i);
            }
            bundle.putIntArray("android:support:request_indicies", iArr);
            bundle.putStringArray("android:support:request_fragment_who", strArr);
        }
    }

    public void onStart() {
        int A002 = C000700l.A00(1455024966);
        super.onStart();
        this.A03 = false;
        if (!this.A00) {
            this.A00 = true;
            this.A07.A00();
        }
        this.A07.A00.A03.A0Z();
        this.A07.A00.A03.A12(true);
        this.A06.A08(C14820uB.ON_START);
        C13060qW r1 = this.A07.A00.A03;
        r1.A0G = false;
        r1.A0H = false;
        C13060qW.A0A(r1, 3);
        C000700l.A07(-2036869785, A002);
    }

    public void onStop() {
        int A002 = C000700l.A00(1355157239);
        super.onStop();
        this.A03 = true;
        do {
        } while (A0z(B4m(), AnonymousClass1XL.CREATED));
        this.A07.A03();
        this.A06.A08(C14820uB.ON_STOP);
        C000700l.A07(853652186, A002);
    }

    public void CIW() {
        invalidateOptionsMenu();
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView = this.A07.A00.A03.A0O.onCreateView(view, str, context, attributeSet);
        return onCreateView == null ? super.onCreateView(view, str, context, attributeSet) : onCreateView;
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View onCreateView = this.A07.A00.A03.A0O.onCreateView(null, str, context, attributeSet);
        return onCreateView == null ? super.onCreateView(str, context, attributeSet) : onCreateView;
    }

    public void startActivityForResult(Intent intent, int i) {
        if (!this.A02 && i != -1) {
            A0y(i);
        }
        super.startActivityForResult(intent, i);
    }

    public void startActivityForResult(Intent intent, int i, Bundle bundle) {
        if (!this.A02 && i != -1) {
            A0y(i);
        }
        super.startActivityForResult(intent, i, bundle);
    }

    public void startIntentSenderForResult(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4) {
        if (i != -1) {
            A0y(i);
        }
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
    }

    public void startIntentSenderForResult(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) {
        if (i != -1) {
            A0y(i);
        }
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
    }
}
