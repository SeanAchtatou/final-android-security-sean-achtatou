package androidx.fragment.app;

import X.AnonymousClass00S;
import X.AnonymousClass08S;
import X.AnonymousClass0n8;
import X.AnonymousClass0qM;
import X.AnonymousClass14W;
import X.AnonymousClass1XL;
import X.AnonymousClass2NQ;
import X.AnonymousClass2NS;
import X.C000700l;
import X.C11410n6;
import X.C123135r6;
import X.C12990qL;
import X.C13020qR;
import X.C13050qV;
import X.C13060qW;
import X.C14820uB;
import X.C24934CPl;
import X.C27521dK;
import X.C27541dM;
import X.C27621dU;
import X.C27631dV;
import X.C27661dY;
import X.C27801dm;
import X.C27911dx;
import X.C27931dz;
import X.C29441gQ;
import X.C29881h8;
import X.C29891h9;
import X.C407222r;
import X.C99624pN;
import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

public class Fragment implements C11410n6, AnonymousClass0n8, C27521dK, ComponentCallbacks, View.OnCreateContextMenuListener, C27541dM {
    public static final Object A0j = new Object();
    public int A00;
    public Bundle A01;
    public AnonymousClass1XL A02 = AnonymousClass1XL.RESUMED;
    public C12990qL A03;
    public C29441gQ A04 = new C29441gQ();
    public Boolean A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public float A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F = -1;
    public Bundle A0G;
    public SparseArray A0H;
    public View A0I;
    public ViewGroup A0J;
    public AnonymousClass2NQ A0K;
    public Fragment A0L;
    public Fragment A0M;
    public C13020qR A0N;
    public C13060qW A0O = new C13050qV();
    public C13060qW A0P;
    public C29881h8 A0Q;
    public C27621dU A0R;
    public Boolean A0S = null;
    public String A0T;
    public String A0U = null;
    public String A0V = UUID.randomUUID().toString();
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e = true;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i = true;

    public final class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new C123135r6();
        public final Bundle A00;

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeBundle(this.A00);
        }

        public SavedState(Bundle bundle) {
            this.A00 = bundle;
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            Bundle readBundle = parcel.readBundle();
            this.A00 = readBundle;
            if (classLoader != null && readBundle != null) {
                readBundle.setClassLoader(classLoader);
            }
        }
    }

    public void A1K(Context context, AttributeSet attributeSet, Bundle bundle) {
        Activity activity;
        this.A0X = true;
        C13020qR r0 = this.A0N;
        if (r0 == null) {
            activity = null;
        } else {
            activity = r0.A00;
        }
        if (activity != null) {
            this.A0X = false;
            this.A0X = true;
        }
    }

    public void A1L(Intent intent) {
        C13020qR r1 = this.A0N;
        if (r1 != null) {
            r1.A06(this, intent, -1, null);
            return;
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    public boolean A1f(MenuItem menuItem) {
        return false;
    }

    public boolean A1g(MenuItem menuItem) {
        return false;
    }

    public Animation A1i(int i, boolean z, int i2) {
        return null;
    }

    public void A1s(Context context) {
        Activity activity;
        this.A0X = true;
        C13020qR r0 = this.A0N;
        if (r0 == null) {
            activity = null;
        } else {
            activity = r0.A00;
        }
        if (activity != null) {
            this.A0X = false;
            int A022 = C000700l.A02(894618012);
            this.A0X = true;
            C000700l.A08(-1276121473, A022);
        }
    }

    public void A1u(Bundle bundle) {
    }

    public void A1v(View view, Bundle bundle) {
    }

    public void A1w(Fragment fragment) {
    }

    public void A1x(boolean z) {
    }

    public void BNH(int i, int i2, Intent intent) {
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.A0X = true;
    }

    public void onLowMemory() {
        this.A0X = true;
    }

    public void startActivityForResult(Intent intent, int i) {
        C13020qR r0 = this.A0N;
        if (r0 != null) {
            r0.A06(this, intent, i, null);
            return;
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    public static AnonymousClass2NQ A01(Fragment fragment) {
        if (fragment.A0K == null) {
            fragment.A0K = new AnonymousClass2NQ();
        }
        return fragment.A0K;
    }

    public static Fragment A02(Context context, String str, Bundle bundle) {
        try {
            Fragment fragment = (Fragment) C27801dm.A00(context.getClassLoader(), str).getConstructor(new Class[0]).newInstance(new Object[0]);
            if (bundle != null) {
                bundle.setClassLoader(fragment.getClass().getClassLoader());
                fragment.A1P(bundle);
            }
            return fragment;
        } catch (InstantiationException e) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": make sure class name exists, is public, and has an empty constructor that is public"), e);
        } catch (IllegalAccessException e2) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": make sure class name exists, is public, and has an empty constructor that is public"), e2);
        } catch (NoSuchMethodException e3) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": could not find Fragment constructor"), e3);
        } catch (InvocationTargetException e4) {
            throw new C99624pN(AnonymousClass08S.A0P("Unable to instantiate fragment ", str, ": calling Fragment constructor caused an exception"), e4);
        }
    }

    public static void A03(Fragment fragment) {
        fragment.A03 = new C12990qL(fragment);
        fragment.A0R = new C27621dU(fragment);
        if (Build.VERSION.SDK_INT >= 19) {
            fragment.A03.A06(new C27661dY() {
                public void Bpu(C11410n6 r2, C14820uB r3) {
                    View view;
                    if (r3 == C14820uB.ON_STOP && (view = Fragment.this.A0I) != null) {
                        view.cancelPendingInputEvents();
                    }
                }
            });
        }
    }

    public LayoutInflater A13(Bundle bundle) {
        C13020qR r0 = this.A0N;
        if (r0 != null) {
            LayoutInflater A022 = r0.A02();
            AnonymousClass14W.A00(A022, this.A0O.A0O);
            return A022;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }

    public final View A14() {
        View view = this.A0I;
        if (view != null) {
            return view;
        }
        throw new IllegalStateException("Fragment " + this + " did not return a View from onCreateView() or this was called before onCreateView().");
    }

    public final FragmentActivity A15() {
        C13020qR r0 = this.A0N;
        if (r0 == null) {
            return null;
        }
        return (FragmentActivity) r0.A00;
    }

    public final C13060qW A17() {
        if (this.A0N != null) {
            return this.A0O;
        }
        throw new IllegalStateException("Fragment " + this + " has not been attached yet.");
    }

    public final C13060qW A18() {
        C13060qW r0 = this.A0P;
        if (r0 != null) {
            return r0;
        }
        throw new IllegalStateException("Fragment " + this + " not associated with a fragment manager.");
    }

    public Object A19() {
        Object obj;
        AnonymousClass2NQ r0 = this.A0K;
        if (r0 == null || (obj = r0.A00) == A0j) {
            return null;
        }
        return obj;
    }

    public void A1C() {
        this.A0O.A0W();
        this.A03.A08(C14820uB.ON_DESTROY);
        this.A0F = 0;
        this.A0X = false;
        this.A07 = false;
        A1l();
        if (!this.A0X) {
            throw new C29891h9("Fragment " + this + " did not call through to super.onDestroy()");
        }
    }

    public void A1D() {
        C13060qW.A0A(this.A0O, 1);
        if (this.A0I != null) {
            C29881h8 r0 = this.A0Q;
            r0.A00.A08(C14820uB.ON_DESTROY);
        }
        this.A0F = 1;
        this.A0X = false;
        A1m();
        if (this.A0X) {
            new AnonymousClass2NS(this, B9I()).A04();
            this.A09 = false;
            return;
        }
        throw new C29891h9("Fragment " + this + " did not call through to super.onDestroyView()");
    }

    public void A1E() {
        C13060qW.A0A(this.A0O, 3);
        if (this.A0I != null) {
            C29881h8 r0 = this.A0Q;
            r0.A00.A08(C14820uB.ON_PAUSE);
        }
        this.A03.A08(C14820uB.ON_PAUSE);
        this.A0F = 3;
        this.A0X = false;
        A1o();
        if (!this.A0X) {
            throw new C29891h9("Fragment " + this + " did not call through to super.onPause()");
        }
    }

    public void A1F() {
        this.A0O.A0Z();
        this.A0O.A12(true);
        this.A0F = 4;
        this.A0X = false;
        A1p();
        if (this.A0X) {
            C12990qL r0 = this.A03;
            C14820uB r1 = C14820uB.ON_RESUME;
            r0.A08(r1);
            if (this.A0I != null) {
                this.A0Q.A00.A08(r1);
            }
            C13060qW r12 = this.A0O;
            r12.A0G = false;
            r12.A0H = false;
            C13060qW.A0A(r12, 4);
            return;
        }
        throw new C29891h9("Fragment " + this + " did not call through to super.onResume()");
    }

    public void A1G() {
        this.A0O.A0Z();
        this.A0O.A12(true);
        this.A0F = 3;
        this.A0X = false;
        A1q();
        if (this.A0X) {
            C12990qL r0 = this.A03;
            C14820uB r1 = C14820uB.ON_START;
            r0.A08(r1);
            if (this.A0I != null) {
                this.A0Q.A00.A08(r1);
            }
            C13060qW r12 = this.A0O;
            r12.A0G = false;
            r12.A0H = false;
            C13060qW.A0A(r12, 3);
            return;
        }
        throw new C29891h9("Fragment " + this + " did not call through to super.onStart()");
    }

    public void A1H() {
        C13060qW r1 = this.A0O;
        r1.A0H = true;
        C13060qW.A0A(r1, 2);
        if (this.A0I != null) {
            C29881h8 r0 = this.A0Q;
            r0.A00.A08(C14820uB.ON_STOP);
        }
        this.A03.A08(C14820uB.ON_STOP);
        this.A0F = 2;
        this.A0X = false;
        A1r();
        if (!this.A0X) {
            throw new C29891h9("Fragment " + this + " did not call through to super.onStop()");
        }
    }

    public void A1I() {
        C24934CPl cPl;
        C13060qW r0 = this.A0P;
        if (r0 == null || r0.A06 == null) {
            A01(this);
        } else if (Looper.myLooper() != this.A0P.A06.A02.getLooper()) {
            AnonymousClass00S.A03(this.A0P.A06.A02, new C407222r(this), -1049127768);
        } else {
            AnonymousClass2NQ r2 = this.A0K;
            if (r2 == null) {
                cPl = null;
            } else {
                cPl = r2.A06;
                r2.A06 = null;
            }
            if (cPl != null) {
                cPl.BpY();
            }
        }
    }

    public void A1J(int i) {
        if (this.A0K != null || i != 0) {
            A01(this).A01 = i;
        }
    }

    public void A1M(Bundle bundle) {
        this.A0O.A0Z();
        this.A0F = 2;
        this.A0X = false;
        A1t(bundle);
        if (this.A0X) {
            C13060qW r1 = this.A0O;
            r1.A0G = false;
            r1.A0H = false;
            C13060qW.A0A(r1, 2);
            return;
        }
        throw new C29891h9("Fragment " + this + " did not call through to super.onActivityCreated()");
    }

    public void A1N(Bundle bundle) {
        this.A0O.A0Z();
        this.A0F = 1;
        this.A0X = false;
        this.A0R.A00(bundle);
        A1h(bundle);
        this.A07 = true;
        if (this.A0X) {
            this.A03.A08(C14820uB.ON_CREATE);
            return;
        }
        throw new C29891h9("Fragment " + this + " did not call through to super.onCreate()");
    }

    public void A1O(Bundle bundle) {
        Parcelable parcelable;
        if (bundle != null && (parcelable = bundle.getParcelable(TurboLoader.Locator.$const$string(22))) != null) {
            this.A0O.A0e(parcelable);
            C13060qW r1 = this.A0O;
            r1.A0G = false;
            r1.A0H = false;
            C13060qW.A0A(r1, 1);
        }
    }

    public void A1P(Bundle bundle) {
        boolean A13;
        C13060qW r0 = this.A0P;
        if (r0 != null) {
            if (r0 == null) {
                A13 = false;
            } else {
                A13 = r0.A13();
            }
            if (A13) {
                throw new IllegalStateException("Fragment already added and state has been saved");
            }
        }
        this.A0G = bundle;
    }

    public void A1Q(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.A0O.A0Z();
        this.A09 = true;
        this.A0Q = new C29881h8();
        View A1k = A1k(layoutInflater, viewGroup, bundle);
        this.A0I = A1k;
        if (A1k != null) {
            C29881h8 r1 = this.A0Q;
            if (r1.A00 == null) {
                r1.A00 = new C12990qL(r1);
            }
            this.A04.A09(this.A0Q);
            return;
        }
        boolean z = false;
        if (this.A0Q.A00 != null) {
            z = true;
        }
        if (!z) {
            this.A0Q = null;
            return;
        }
        throw new IllegalStateException("Called getViewLifecycleOwner() but onCreateView() returned null");
    }

    public void A1S(SavedState savedState) {
        Bundle bundle;
        if (this.A0P == null) {
            if (savedState == null || (bundle = savedState.A00) == null) {
                bundle = null;
            }
            this.A01 = bundle;
            return;
        }
        throw new IllegalStateException("Fragment already added");
    }

    public void A1T(boolean z) {
        if (this.A0a != z) {
            this.A0a = z;
            if (A1X() && !this.A0b) {
                this.A0N.A04();
            }
        }
    }

    public void A1U(boolean z) {
        if (this.A0e != z) {
            this.A0e = z;
            if (this.A0a && A1X() && !this.A0b) {
                this.A0N.A04();
            }
        }
    }

    public void A1V(boolean z) {
        this.A0h = z;
        C13060qW r0 = this.A0P;
        if (r0 == null) {
            this.A0A = true;
        } else if (z) {
            r0.A0i(this);
        } else {
            r0.A0q(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        if (r3 != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1W(boolean r3) {
        /*
            r2 = this;
            boolean r0 = r2.A0i
            r1 = 3
            if (r0 != 0) goto L_0x0012
            if (r3 == 0) goto L_0x0012
            int r0 = r2.A0F
            if (r0 >= r1) goto L_0x0012
            X.0qW r0 = r2.A0P
            if (r0 == 0) goto L_0x0012
            r0.A0o(r2)
        L_0x0012:
            r2.A0i = r3
            int r0 = r2.A0F
            if (r0 >= r1) goto L_0x001b
            r0 = 1
            if (r3 == 0) goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            r2.A06 = r0
            android.os.Bundle r0 = r2.A01
            if (r0 == 0) goto L_0x0028
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            r2.A05 = r0
        L_0x0028:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.Fragment.A1W(boolean):void");
    }

    public final boolean A1X() {
        if (this.A0N == null || !this.A0W) {
            return false;
        }
        return true;
    }

    public final boolean A1Y() {
        Fragment fragment = this.A0L;
        if (fragment == null) {
            return false;
        }
        if (fragment.A0f || fragment.A1Y()) {
            return true;
        }
        return false;
    }

    public final boolean A1Z() {
        if (this.A0F >= 4) {
            return true;
        }
        return false;
    }

    public boolean A1b(Menu menu, MenuInflater menuInflater) {
        boolean z = false;
        if (this.A0b) {
            return false;
        }
        if (this.A0a && this.A0e) {
            z = true;
        }
        return z | this.A0O.A16(menu, menuInflater);
    }

    public Context A1j() {
        C13020qR r0 = this.A0N;
        if (r0 == null) {
            return null;
        }
        return r0.A01;
    }

    public AnonymousClass0qM AsK() {
        return this.A03;
    }

    public final C27631dV B1u() {
        return this.A0R.A00;
    }

    public C27911dx B9I() {
        C13060qW r0 = this.A0P;
        if (r0 != null) {
            C27931dz r3 = r0.A07;
            C27911dx r2 = (C27911dx) r3.A03.get(this.A0V);
            if (r2 != null) {
                return r2;
            }
            C27911dx r22 = new C27911dx();
            r3.A03.put(this.A0V, r22);
            return r22;
        }
        throw new IllegalStateException("Can't access ViewModels from detached fragment");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append(getClass().getSimpleName());
        sb.append("{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("}");
        sb.append(" (");
        sb.append(this.A0V);
        sb.append(")");
        int i = this.A0E;
        if (i != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(i));
        }
        String str = this.A0T;
        if (str != null) {
            sb.append(" ");
            sb.append(str);
        }
        sb.append('}');
        return sb.toString();
    }

    public Fragment() {
        A03(this);
    }

    public final Context A11() {
        Context A1j = A1j();
        if (A1j != null) {
            return A1j;
        }
        throw new IllegalStateException("Fragment " + this + " not attached to a context.");
    }

    public final Resources A12() {
        return A11().getResources();
    }

    public final FragmentActivity A16() {
        FragmentActivity A15 = A15();
        if (A15 != null) {
            return A15;
        }
        throw new IllegalStateException("Fragment " + this + " not attached to an activity.");
    }

    public final String A1A(int i) {
        return A12().getString(i);
    }

    public final String A1B(int i, Object... objArr) {
        return A12().getString(i, objArr);
    }

    public void A1R(C24934CPl cPl) {
        A01(this);
        AnonymousClass2NQ r1 = this.A0K;
        C24934CPl cPl2 = r1.A06;
        if (cPl == cPl2) {
            return;
        }
        if (cPl == null || cPl2 == null) {
            if (0 != 0) {
                r1.A06 = cPl;
            }
            if (cPl != null) {
                cPl.CH7();
                return;
            }
            return;
        }
        throw new IllegalStateException("Trying to set a replacement startPostponedEnterTransition on " + this);
    }

    public final boolean A1a() {
        View view;
        if (!A1X() || this.A0b || (view = this.A0I) == null || view.getWindowToken() == null || this.A0I.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    public void A1c(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i;
        View view;
        View view2;
        int i2;
        int i3;
        String str2;
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.A0E));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.A0D));
        printWriter.print(" mTag=");
        printWriter.println(this.A0T);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.A0F);
        printWriter.print(" mWho=");
        printWriter.print(this.A0V);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.A0C);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.A0W);
        printWriter.print(" mRemoving=");
        printWriter.print(this.A0f);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.A0Z);
        printWriter.print(" mInLayout=");
        printWriter.println(this.A0d);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.A0b);
        printWriter.print(" mDetached=");
        printWriter.print(this.A0Y);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.A0e);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.A0a);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.A0h);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.A0i);
        if (this.A0P != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.A0P);
        }
        if (this.A0N != null) {
            printWriter.print(str);
            printWriter.print("mHost=");
            printWriter.println(this.A0N);
        }
        if (this.A0L != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.A0L);
        }
        if (this.A0G != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.A0G);
        }
        if (this.A01 != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.A01);
        }
        if (this.A0H != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.A0H);
        }
        Fragment fragment = this.A0M;
        if (fragment == null) {
            C13060qW r0 = this.A0P;
            if (r0 == null || (str2 = this.A0U) == null) {
                fragment = null;
            } else {
                fragment = r0.A0Q.A00(str2);
            }
        }
        if (fragment != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(fragment);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.A00);
        }
        AnonymousClass2NQ r02 = this.A0K;
        if (r02 == null) {
            i = 0;
        } else {
            i = r02.A01;
        }
        if (i != 0) {
            printWriter.print(str);
            printWriter.print("mNextAnim=");
            AnonymousClass2NQ r03 = this.A0K;
            if (r03 == null) {
                i3 = 0;
            } else {
                i3 = r03.A01;
            }
            printWriter.println(i3);
        }
        if (this.A0J != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.A0J);
        }
        if (this.A0I != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.A0I);
        }
        AnonymousClass2NQ r04 = this.A0K;
        if (r04 == null) {
            view = null;
        } else {
            view = r04.A05;
        }
        if (view != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            AnonymousClass2NQ r05 = this.A0K;
            if (r05 == null) {
                view2 = null;
            } else {
                view2 = r05.A05;
            }
            printWriter.println(view2);
            printWriter.print(str);
            printWriter.print("mStateAfterAnimating=");
            AnonymousClass2NQ r06 = this.A0K;
            if (r06 == null) {
                i2 = 0;
            } else {
                i2 = r06.A03;
            }
            printWriter.println(i2);
        }
        if (A1j() != null) {
            new AnonymousClass2NS(this, B9I()).A05(str, fileDescriptor, printWriter, strArr);
        }
        printWriter.print(str);
        printWriter.println("Child " + this.A0O + ":");
        this.A0O.A0z(AnonymousClass08S.A0J(str, "  "), fileDescriptor, printWriter, strArr);
    }

    public LayoutInflater A1d(Bundle bundle) {
        return A13(bundle);
    }

    public void A1e(Bundle bundle) {
        int A022 = C000700l.A02(865006028);
        this.A0X = true;
        C000700l.A08(881477546, A022);
    }

    public void A1h(Bundle bundle) {
        int A022 = C000700l.A02(412399288);
        this.A0X = true;
        A1O(bundle);
        C13060qW r2 = this.A0O;
        boolean z = false;
        if (r2.A01 >= 1) {
            z = true;
        }
        if (!z) {
            r2.A0G = false;
            r2.A0H = false;
            C13060qW.A0A(r2, 1);
        }
        C000700l.A08(1111400336, A022);
    }

    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        C000700l.A08(1196706451, C000700l.A02(-1027310901));
        return null;
    }

    public void A1l() {
        int A022 = C000700l.A02(1429640738);
        this.A0X = true;
        C000700l.A08(55621516, A022);
    }

    public void A1m() {
        int A022 = C000700l.A02(-961299403);
        this.A0X = true;
        C000700l.A08(223467279, A022);
    }

    public void A1n() {
        int A022 = C000700l.A02(1887423784);
        this.A0X = true;
        C000700l.A08(1766004772, A022);
    }

    public void A1o() {
        int A022 = C000700l.A02(-741365511);
        this.A0X = true;
        C000700l.A08(257018534, A022);
    }

    public void A1p() {
        int A022 = C000700l.A02(339993235);
        this.A0X = true;
        C000700l.A08(-70928354, A022);
    }

    public void A1q() {
        int A022 = C000700l.A02(-179177744);
        this.A0X = true;
        C000700l.A08(84446793, A022);
    }

    public void A1r() {
        int A022 = C000700l.A02(1602857852);
        this.A0X = true;
        C000700l.A08(1867857833, A022);
    }

    public void A1t(Bundle bundle) {
        int A022 = C000700l.A02(-1986149221);
        this.A0X = true;
        C000700l.A08(1469501862, A022);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        A16().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }
}
