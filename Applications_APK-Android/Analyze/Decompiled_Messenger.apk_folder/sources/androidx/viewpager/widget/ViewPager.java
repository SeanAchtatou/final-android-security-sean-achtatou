package androidx.viewpager.widget;

import X.AnonymousClass08S;
import X.AnonymousClass1Y3;
import X.AnonymousClass560;
import X.AnonymousClass8ND;
import X.C000700l;
import X.C1061855z;
import X.C1068259l;
import X.C110325Nx;
import X.C15320v6;
import X.C1756488p;
import X.C25964Cpf;
import X.C25967Cpi;
import X.C25970Cpn;
import X.C25971Cpo;
import X.C27470DdD;
import X.C72633eh;
import X.C95574ge;
import X.D3J;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import androidx.customview.view.AbsSavedState;
import com.facebook.common.dextricks.DexStore;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ViewPager extends ViewGroup {
    public static final int[] A0q = {16842931};
    private static final AnonymousClass560 A0r = new AnonymousClass560();
    private static final Comparator A0s = new C25967Cpi();
    private static final Interpolator sInterpolator = new C95574ge();
    public float A00 = -3.4028235E38f;
    public float A01;
    public float A02;
    public float A03 = Float.MAX_VALUE;
    public int A04 = -1;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public long A0A;
    public VelocityTracker A0B;
    public D3J A0C;
    public C72633eh A0D;
    public C72633eh A0E;
    public C110325Nx A0F;
    public List A0G;
    public List A0H;
    public boolean A0I = true;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    private float A0P;
    private float A0Q;
    private int A0R;
    private int A0S;
    private int A0T;
    private int A0U;
    private int A0V;
    private int A0W;
    private int A0X;
    private int A0Y = 1;
    private int A0Z = -1;
    private int A0a = 0;
    private int A0b;
    private int A0c;
    private Parcelable A0d = null;
    private EdgeEffect A0e;
    private EdgeEffect A0f;
    private C1068259l A0g;
    private ClassLoader A0h = null;
    private ArrayList A0i;
    private boolean A0j;
    private boolean A0k = true;
    private boolean A0l;
    public final ArrayList A0m = new ArrayList();
    private final Rect A0n = new Rect();
    private final C25964Cpf A0o = new C25964Cpf();
    private final Runnable A0p = new C25970Cpn(this);
    public int mGutterSize;
    public Scroller mScroller;

    public final class SavedState extends AbsSavedState {
        public static final Parcelable.Creator CREATOR = new AnonymousClass8ND();
        public int A00;
        public Parcelable A01;
        public ClassLoader A02;

        public String toString() {
            return AnonymousClass08S.A0R("FragmentPager.SavedState{", Integer.toHexString(System.identityHashCode(this)), " position=", this.A00, "}");
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.A00);
            parcel.writeParcelable(this.A01, i);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.A00 = parcel.readInt();
            this.A01 = parcel.readParcelable(classLoader);
            this.A02 = classLoader;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public @interface DecorView {
    }

    private C25964Cpf A04(View view) {
        for (int i = 0; i < this.A0m.size(); i++) {
            C25964Cpf cpf = (C25964Cpf) this.A0m.get(i);
            if (this.A0C.A0E(view, cpf.A00)) {
                return cpf;
            }
        }
        return null;
    }

    private void A06() {
        setWillNotDraw(false);
        setDescendantFocusability(DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED);
        setFocusable(true);
        Context context = getContext();
        this.mScroller = new Scroller(context, sInterpolator);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f = context.getResources().getDisplayMetrics().density;
        this.A0c = viewConfiguration.getScaledPagingTouchSlop();
        this.A0X = (int) (400.0f * f);
        this.A07 = viewConfiguration.getScaledMaximumFlingVelocity();
        this.A0e = new EdgeEffect(context);
        this.A0f = new EdgeEffect(context);
        this.A0W = (int) (25.0f * f);
        this.A0S = (int) (2.0f * f);
        this.A0U = (int) (f * 16.0f);
        C15320v6.setAccessibilityDelegate(this, new C25971Cpo(this));
        if (C15320v6.getImportantForAccessibility(this) == 0) {
            C15320v6.setImportantForAccessibility(this, 1);
        }
        C15320v6.setOnApplyWindowInsetsListener(this, new C1756488p(this));
    }

    /* JADX WARN: Type inference failed for: r1v8, types: [android.view.ViewParent] */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (r1 == r0) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a0, code lost:
        if (r9 >= 0) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b8, code lost:
        if (r9 >= 0) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c6, code lost:
        if (r9 >= 0) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c9, code lost:
        r12 = null;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0B(androidx.viewpager.widget.ViewPager r15, int r16) {
        /*
            r4 = r15
            int r0 = r15.A05
            r1 = r16
            if (r0 == r1) goto L_0x00e1
            X.Cpf r7 = r15.A0K(r0)
            r15.A05 = r1
        L_0x000d:
            X.D3J r0 = r15.A0C
            if (r0 == 0) goto L_0x0357
            boolean r0 = r15.A0N
            if (r0 != 0) goto L_0x0357
            android.os.IBinder r0 = r15.getWindowToken()
            if (r0 == 0) goto L_0x0356
            X.D3J r0 = r15.A0C
            r0.A0D(r15)
            int r2 = r15.A0Y
            int r1 = r15.A05
            int r1 = r1 - r2
            r0 = 0
            int r10 = java.lang.Math.max(r0, r1)
            X.D3J r0 = r15.A0C
            int r5 = r0.A08()
            int r1 = r5 + -1
            int r0 = r15.A05
            int r0 = r0 + r2
            int r11 = java.lang.Math.min(r1, r0)
            int r0 = r15.A0V
            if (r5 != r0) goto L_0x0301
            r2 = 0
        L_0x003e:
            java.util.ArrayList r0 = r15.A0m
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x00de
            java.util.ArrayList r0 = r15.A0m
            java.lang.Object r8 = r0.get(r2)
            X.Cpf r8 = (X.C25964Cpf) r8
            int r1 = r8.A04
            int r0 = r15.A05
            if (r1 < r0) goto L_0x00da
            if (r1 != r0) goto L_0x00de
        L_0x0056:
            if (r8 != 0) goto L_0x0060
            if (r5 <= 0) goto L_0x0060
            int r0 = r15.A05
            X.Cpf r8 = r15.A03(r0, r2)
        L_0x0060:
            r16 = 0
            if (r8 == 0) goto L_0x0281
            int r9 = r2 + -1
            if (r9 < 0) goto L_0x00d8
            java.util.ArrayList r0 = r15.A0m
            java.lang.Object r12 = r0.get(r9)
            X.Cpf r12 = (X.C25964Cpf) r12
        L_0x0070:
            int r6 = A00(r15)
            r15 = 1073741824(0x40000000, float:2.0)
            if (r6 > 0) goto L_0x00cb
            r3 = 0
        L_0x0079:
            int r0 = r4.A05
            int r1 = r0 + -1
            r14 = 0
        L_0x007e:
            if (r1 < 0) goto L_0x00e4
            int r0 = (r14 > r3 ? 1 : (r14 == r3 ? 0 : -1))
            if (r0 < 0) goto L_0x00ad
            if (r1 >= r10) goto L_0x00ad
            if (r12 == 0) goto L_0x00e4
            int r0 = r12.A04
            if (r1 != r0) goto L_0x00aa
            boolean r0 = r12.A01
            if (r0 != 0) goto L_0x00aa
            java.util.ArrayList r0 = r4.A0m
            r0.remove(r9)
            X.D3J r13 = r4.A0C
            java.lang.Object r0 = r12.A00
            r13.A0H(r4, r1, r0)
            int r9 = r9 + -1
            int r2 = r2 + -1
            if (r9 < 0) goto L_0x00c9
        L_0x00a2:
            java.util.ArrayList r0 = r4.A0m
            java.lang.Object r12 = r0.get(r9)
            X.Cpf r12 = (X.C25964Cpf) r12
        L_0x00aa:
            int r1 = r1 + -1
            goto L_0x007e
        L_0x00ad:
            if (r12 == 0) goto L_0x00bb
            int r0 = r12.A04
            if (r1 != r0) goto L_0x00bb
            float r0 = r12.A03
            float r14 = r14 + r0
            int r9 = r9 + -1
            if (r9 < 0) goto L_0x00c9
            goto L_0x00a2
        L_0x00bb:
            int r0 = r9 + 1
            X.Cpf r0 = r4.A03(r1, r0)
            float r0 = r0.A03
            float r14 = r14 + r0
            int r2 = r2 + 1
            if (r9 < 0) goto L_0x00c9
            goto L_0x00a2
        L_0x00c9:
            r12 = 0
            goto L_0x00aa
        L_0x00cb:
            float r0 = r8.A03
            float r3 = r15 - r0
            int r0 = r4.getPaddingLeft()
            float r1 = (float) r0
            float r0 = (float) r6
            float r1 = r1 / r0
            float r3 = r3 + r1
            goto L_0x0079
        L_0x00d8:
            r12 = 0
            goto L_0x0070
        L_0x00da:
            int r2 = r2 + 1
            goto L_0x003e
        L_0x00de:
            r8 = 0
            goto L_0x0056
        L_0x00e1:
            r7 = 0
            goto L_0x000d
        L_0x00e4:
            float r9 = r8.A03
            int r3 = r2 + 1
            r10 = r3
            int r0 = (r9 > r15 ? 1 : (r9 == r15 ? 0 : -1))
            if (r0 >= 0) goto L_0x0156
            java.util.ArrayList r0 = r4.A0m
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x0154
            java.util.ArrayList r0 = r4.A0m
            java.lang.Object r13 = r0.get(r3)
            X.Cpf r13 = (X.C25964Cpf) r13
        L_0x00fd:
            if (r6 > 0) goto L_0x014b
            r1 = 0
        L_0x0100:
            int r12 = r4.A05
        L_0x0102:
            int r12 = r12 + 1
            if (r12 >= r5) goto L_0x0156
            int r0 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0135
            if (r12 <= r11) goto L_0x0135
            if (r13 == 0) goto L_0x0156
            int r0 = r13.A04
            if (r12 != r0) goto L_0x0102
            boolean r0 = r13.A01
            if (r0 != 0) goto L_0x0102
            java.util.ArrayList r0 = r4.A0m
            r0.remove(r3)
            X.D3J r6 = r4.A0C
            java.lang.Object r0 = r13.A00
            r6.A0H(r4, r12, r0)
        L_0x0122:
            java.util.ArrayList r0 = r4.A0m
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x0133
            java.util.ArrayList r0 = r4.A0m
            java.lang.Object r13 = r0.get(r3)
            X.Cpf r13 = (X.C25964Cpf) r13
            goto L_0x0102
        L_0x0133:
            r13 = 0
            goto L_0x0102
        L_0x0135:
            if (r13 == 0) goto L_0x0141
            int r0 = r13.A04
            if (r12 != r0) goto L_0x0141
            float r0 = r13.A03
            float r9 = r9 + r0
            int r3 = r3 + 1
            goto L_0x0122
        L_0x0141:
            X.Cpf r0 = r4.A03(r12, r3)
            int r3 = r3 + 1
            float r0 = r0.A03
            float r9 = r9 + r0
            goto L_0x0122
        L_0x014b:
            int r0 = r4.getPaddingRight()
            float r1 = (float) r0
            float r0 = (float) r6
            float r1 = r1 / r0
            float r1 = r1 + r15
            goto L_0x0100
        L_0x0154:
            r13 = 0
            goto L_0x00fd
        L_0x0156:
            X.D3J r0 = r4.A0C
            int r12 = r0.A08()
            int r1 = A00(r4)
            if (r1 <= 0) goto L_0x01b3
            int r0 = r4.A08
            float r9 = (float) r0
            float r0 = (float) r1
            float r9 = r9 / r0
        L_0x0167:
            if (r7 == 0) goto L_0x01f2
            int r1 = r7.A04
            int r0 = r8.A04
            if (r1 >= r0) goto L_0x01b5
            r5 = 0
            float r3 = r7.A02
            float r0 = r7.A03
            float r3 = r3 + r0
            float r3 = r3 + r9
        L_0x0176:
            int r1 = r1 + 1
            int r0 = r8.A04
            if (r1 > r0) goto L_0x01f2
            java.util.ArrayList r0 = r4.A0m
            int r0 = r0.size()
            if (r5 >= r0) goto L_0x01f2
        L_0x0184:
            java.util.ArrayList r0 = r4.A0m
            java.lang.Object r6 = r0.get(r5)
            X.Cpf r6 = (X.C25964Cpf) r6
            int r0 = r6.A04
            if (r1 <= r0) goto L_0x019d
            java.util.ArrayList r0 = r4.A0m
            int r0 = r0.size()
            int r0 = r0 + -1
            if (r5 >= r0) goto L_0x019d
            int r5 = r5 + 1
            goto L_0x0184
        L_0x019d:
            int r0 = r6.A04
            if (r1 >= r0) goto L_0x01ac
            X.D3J r0 = r4.A0C
            float r0 = r0.A06(r1)
            float r0 = r0 + r9
            float r3 = r3 + r0
            int r1 = r1 + 1
            goto L_0x019d
        L_0x01ac:
            r6.A02 = r3
            float r0 = r6.A03
            float r0 = r0 + r9
            float r3 = r3 + r0
            goto L_0x0176
        L_0x01b3:
            r9 = 0
            goto L_0x0167
        L_0x01b5:
            if (r1 <= r0) goto L_0x01f2
            java.util.ArrayList r0 = r4.A0m
            int r0 = r0.size()
            int r6 = r0 + -1
            float r5 = r7.A02
            int r3 = r1 + -1
        L_0x01c3:
            int r0 = r8.A04
            if (r3 < r0) goto L_0x01f2
            if (r6 < 0) goto L_0x01f2
        L_0x01c9:
            java.util.ArrayList r0 = r4.A0m
            java.lang.Object r1 = r0.get(r6)
            X.Cpf r1 = (X.C25964Cpf) r1
            int r0 = r1.A04
            if (r3 >= r0) goto L_0x01da
            if (r6 <= 0) goto L_0x01da
            int r6 = r6 + -1
            goto L_0x01c9
        L_0x01da:
            int r0 = r1.A04
            if (r3 <= r0) goto L_0x01e9
            X.D3J r0 = r4.A0C
            float r0 = r0.A06(r3)
            float r0 = r0 + r9
            float r5 = r5 - r0
            int r3 = r3 + -1
            goto L_0x01da
        L_0x01e9:
            float r0 = r1.A03
            float r0 = r0 + r9
            float r5 = r5 - r0
            r1.A02 = r5
            int r3 = r3 + -1
            goto L_0x01c3
        L_0x01f2:
            java.util.ArrayList r0 = r4.A0m
            int r7 = r0.size()
            float r5 = r8.A02
            int r1 = r8.A04
            int r11 = r1 + -1
            r0 = -8388609(0xffffffffff7fffff, float:-3.4028235E38)
            if (r1 != 0) goto L_0x0204
            r0 = r5
        L_0x0204:
            r4.A00 = r0
            int r6 = r12 + -1
            r12 = 1065353216(0x3f800000, float:1.0)
            if (r1 != r6) goto L_0x023d
            float r0 = r8.A03
            float r0 = r0 + r5
            float r0 = r0 - r12
        L_0x0210:
            r4.A03 = r0
            int r3 = r2 + -1
        L_0x0214:
            if (r3 < 0) goto L_0x0241
            java.util.ArrayList r0 = r4.A0m
            java.lang.Object r2 = r0.get(r3)
            X.Cpf r2 = (X.C25964Cpf) r2
        L_0x021e:
            int r1 = r2.A04
            if (r11 <= r1) goto L_0x022e
            X.D3J r0 = r4.A0C
            int r1 = r11 + -1
            float r0 = r0.A06(r11)
            float r0 = r0 + r9
            float r5 = r5 - r0
            r11 = r1
            goto L_0x021e
        L_0x022e:
            float r0 = r2.A03
            float r0 = r0 + r9
            float r5 = r5 - r0
            r2.A02 = r5
            if (r1 != 0) goto L_0x0238
            r4.A00 = r5
        L_0x0238:
            int r3 = r3 + -1
            int r11 = r11 + -1
            goto L_0x0214
        L_0x023d:
            r0 = 2139095039(0x7f7fffff, float:3.4028235E38)
            goto L_0x0210
        L_0x0241:
            float r5 = r8.A02
            float r0 = r8.A03
            float r5 = r5 + r0
            float r5 = r5 + r9
            int r0 = r8.A04
            int r3 = r0 + 1
        L_0x024b:
            if (r10 >= r7) goto L_0x0278
            java.util.ArrayList r0 = r4.A0m
            java.lang.Object r2 = r0.get(r10)
            X.Cpf r2 = (X.C25964Cpf) r2
        L_0x0255:
            int r0 = r2.A04
            if (r3 >= r0) goto L_0x0265
            X.D3J r0 = r4.A0C
            int r1 = r3 + 1
            float r0 = r0.A06(r3)
            float r0 = r0 + r9
            float r5 = r5 + r0
            r3 = r1
            goto L_0x0255
        L_0x0265:
            if (r0 != r6) goto L_0x026d
            float r0 = r2.A03
            float r0 = r0 + r5
            float r0 = r0 - r12
            r4.A03 = r0
        L_0x026d:
            r2.A02 = r5
            float r0 = r2.A03
            float r0 = r0 + r9
            float r5 = r5 + r0
            int r10 = r10 + 1
            int r3 = r3 + 1
            goto L_0x024b
        L_0x0278:
            X.D3J r2 = r4.A0C
            int r1 = r4.A05
            java.lang.Object r0 = r8.A00
            r2.A0I(r4, r1, r0)
        L_0x0281:
            X.D3J r0 = r4.A0C
            r0.A0C(r4)
            int r5 = r4.getChildCount()
            r3 = 0
        L_0x028b:
            if (r3 >= r5) goto L_0x02b4
            android.view.View r1 = r4.getChildAt(r3)
            android.view.ViewGroup$LayoutParams r2 = r1.getLayoutParams()
            X.55z r2 = (X.C1061855z) r2
            r2.A01 = r3
            boolean r0 = r2.A05
            if (r0 != 0) goto L_0x02b1
            float r0 = r2.A00
            int r0 = (r0 > r16 ? 1 : (r0 == r16 ? 0 : -1))
            if (r0 != 0) goto L_0x02b1
            X.Cpf r1 = r4.A04(r1)
            if (r1 == 0) goto L_0x02b1
            float r0 = r1.A03
            r2.A00 = r0
            int r0 = r1.A04
            r2.A02 = r0
        L_0x02b1:
            int r3 = r3 + 1
            goto L_0x028b
        L_0x02b4:
            r4.A07()
            boolean r0 = r4.hasFocus()
            if (r0 == 0) goto L_0x0356
            android.view.View r0 = r4.findFocus()
            if (r0 == 0) goto L_0x02d1
        L_0x02c3:
            android.view.ViewParent r1 = r0.getParent()
            if (r1 == r4) goto L_0x02d3
            boolean r0 = r1 instanceof android.view.View
            if (r0 == 0) goto L_0x02d1
            r0 = r1
            android.view.View r0 = (android.view.View) r0
            goto L_0x02c3
        L_0x02d1:
            r0 = 0
            goto L_0x02d7
        L_0x02d3:
            X.Cpf r0 = r4.A04(r0)
        L_0x02d7:
            if (r0 == 0) goto L_0x02df
            int r1 = r0.A04
            int r0 = r4.A05
            if (r1 == r0) goto L_0x0356
        L_0x02df:
            r3 = 0
        L_0x02e0:
            int r0 = r4.getChildCount()
            if (r3 >= r0) goto L_0x0356
            android.view.View r2 = r4.getChildAt(r3)
            X.Cpf r0 = r4.A04(r2)
            if (r0 == 0) goto L_0x02fe
            int r1 = r0.A04
            int r0 = r4.A05
            if (r1 != r0) goto L_0x02fe
            r0 = 2
            boolean r0 = r2.requestFocus(r0)
            if (r0 == 0) goto L_0x02fe
            return
        L_0x02fe:
            int r3 = r3 + 1
            goto L_0x02e0
        L_0x0301:
            android.content.res.Resources r1 = r15.getResources()     // Catch:{ NotFoundException -> 0x030e }
            int r0 = r15.getId()     // Catch:{ NotFoundException -> 0x030e }
            java.lang.String r3 = r1.getResourceName(r0)     // Catch:{ NotFoundException -> 0x030e }
            goto L_0x0316
        L_0x030e:
            int r0 = r15.getId()
            java.lang.String r3 = java.lang.Integer.toHexString(r0)
        L_0x0316:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            r1.<init>(r0)
            int r0 = r15.A0V
            r1.append(r0)
            java.lang.String r0 = ", found: "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = " Pager id: "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " Pager class: "
            r1.append(r0)
            java.lang.Class r0 = r15.getClass()
            r1.append(r0)
            java.lang.String r0 = " Problematic adapter: "
            r1.append(r0)
            X.D3J r0 = r15.A0C
            java.lang.Class r0 = r0.getClass()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0356:
            return
        L_0x0357:
            r15.A07()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A0B(androidx.viewpager.widget.ViewPager, int):void");
    }

    private boolean A0F() {
        this.A04 = -1;
        this.A0K = false;
        this.A0M = false;
        VelocityTracker velocityTracker = this.A0B;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.A0B = null;
        }
        this.A0e.onRelease();
        this.A0f.onRelease();
        if (this.A0e.isFinished() || this.A0f.isFinished()) {
            return true;
        }
        return false;
    }

    public C25964Cpf A0K(int i) {
        for (int i2 = 0; i2 < this.A0m.size(); i2++) {
            C25964Cpf cpf = (C25964Cpf) this.A0m.get(i2);
            if (cpf.A04 == i) {
                return cpf;
            }
        }
        return null;
    }

    public void A0N(int i) {
        this.A0N = false;
        A0S(i, !this.A0k, false, 0);
    }

    public void A0O(int i) {
        if (i < 1) {
            Log.w("ViewPager", AnonymousClass08S.A0B("Requested offscreen page limit ", i, " too small; defaulting to ", 1));
            i = 1;
        }
        if (i != this.A0Y) {
            this.A0Y = i;
            A0B(this, this.A05);
        }
    }

    public void A0R(int i, boolean z) {
        this.A0N = false;
        A0S(i, z, false, 0);
    }

    public void A0W(boolean z, C110325Nx r8) {
        int i = 1;
        boolean z2 = false;
        if (r8 != null) {
            z2 = true;
        }
        boolean z3 = false;
        if (this.A0F != null) {
            z3 = true;
        }
        boolean z4 = false;
        if (z2 != z3) {
            z4 = true;
        }
        this.A0F = r8;
        setChildrenDrawingOrderEnabled(z2);
        if (z2) {
            if (z) {
                i = 2;
            }
            this.A06 = i;
            this.A09 = 2;
        } else {
            this.A06 = 0;
        }
        if (z4) {
            A0B(this, this.A05);
        }
    }

    public void addTouchables(ArrayList arrayList) {
        C25964Cpf A042;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (A042 = A04(childAt)) != null && A042.A04 == this.A05) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    public void computeScroll() {
        this.A0L = true;
        if (this.mScroller.isFinished() || !this.mScroller.computeScrollOffset()) {
            A0D(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.mScroller.getCurrX();
        int currY = this.mScroller.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!A0H(currX)) {
                this.mScroller.abortAnimation();
                scrollTo(0, currY);
            }
        }
        C15320v6.postInvalidateOnAnimation(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0065, code lost:
        if (r2 == 80) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ae, code lost:
        if (r11 == false) goto L_0x0075;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r16, int r17) {
        /*
            r15 = this;
            r0 = 0
            r1 = r16
            int r1 = getDefaultSize(r0, r1)
            r2 = r17
            int r0 = getDefaultSize(r0, r2)
            r15.setMeasuredDimension(r1, r0)
            int r7 = r15.getMeasuredWidth()
            int r1 = r7 / 10
            int r0 = r15.A0U
            int r0 = java.lang.Math.min(r1, r0)
            r15.mGutterSize = r0
            int r0 = r15.getPaddingLeft()
            int r7 = r7 - r0
            int r0 = r15.getPaddingRight()
            int r7 = r7 - r0
            int r5 = r15.getMeasuredHeight()
            int r0 = r15.getPaddingTop()
            int r5 = r5 - r0
            int r0 = r15.getPaddingBottom()
            int r5 = r5 - r0
            int r4 = r15.getChildCount()
            r3 = 0
        L_0x003b:
            r10 = 8
            r11 = 1
            r9 = 1073741824(0x40000000, float:2.0)
            if (r3 >= r4) goto L_0x00b1
            android.view.View r6 = r15.getChildAt(r3)
            int r0 = r6.getVisibility()
            if (r0 == r10) goto L_0x009c
            android.view.ViewGroup$LayoutParams r1 = r6.getLayoutParams()
            X.55z r1 = (X.C1061855z) r1
            if (r1 == 0) goto L_0x009c
            boolean r0 = r1.A05
            if (r0 == 0) goto L_0x009c
            int r0 = r1.A04
            r10 = r0 & 7
            r2 = r0 & 112(0x70, float:1.57E-43)
            r0 = 48
            if (r2 == r0) goto L_0x0067
            r0 = 80
            r14 = 0
            if (r2 != r0) goto L_0x0068
        L_0x0067:
            r14 = 1
        L_0x0068:
            r0 = 3
            if (r10 == r0) goto L_0x006f
            r0 = 5
            if (r10 == r0) goto L_0x006f
            r11 = 0
        L_0x006f:
            r13 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r14 == 0) goto L_0x00ac
            r13 = 1073741824(0x40000000, float:2.0)
        L_0x0075:
            r12 = -2147483648(0xffffffff80000000, float:-0.0)
        L_0x0077:
            int r10 = r1.width
            r2 = -1
            r0 = -2
            if (r10 == r0) goto L_0x00aa
            if (r10 != r2) goto L_0x0080
            r10 = r7
        L_0x0080:
            r13 = 1073741824(0x40000000, float:2.0)
        L_0x0082:
            int r1 = r1.height
            if (r1 == r0) goto L_0x00a7
            r0 = r5
            if (r1 == r2) goto L_0x008a
            r0 = r1
        L_0x008a:
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r13)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            r6.measure(r1, r0)
            if (r14 == 0) goto L_0x009f
            int r0 = r6.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x009c:
            int r3 = r3 + 1
            goto L_0x003b
        L_0x009f:
            if (r11 == 0) goto L_0x009c
            int r0 = r6.getMeasuredWidth()
            int r7 = r7 - r0
            goto L_0x009c
        L_0x00a7:
            r0 = r5
            r9 = r12
            goto L_0x008a
        L_0x00aa:
            r10 = r7
            goto L_0x0082
        L_0x00ac:
            r12 = 1073741824(0x40000000, float:2.0)
            if (r11 != 0) goto L_0x0077
            goto L_0x0075
        L_0x00b1:
            android.view.View.MeasureSpec.makeMeasureSpec(r7, r9)
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r9)
            r15.A0l = r11
            int r0 = r15.A05
            A0B(r15, r0)
            r5 = 0
            r15.A0l = r5
            int r4 = r15.getChildCount()
        L_0x00c6:
            if (r5 >= r4) goto L_0x00ed
            android.view.View r3 = r15.getChildAt(r5)
            int r0 = r3.getVisibility()
            if (r0 == r10) goto L_0x00ea
            android.view.ViewGroup$LayoutParams r2 = r3.getLayoutParams()
            X.55z r2 = (X.C1061855z) r2
            if (r2 == 0) goto L_0x00de
            boolean r0 = r2.A05
            if (r0 != 0) goto L_0x00ea
        L_0x00de:
            float r1 = (float) r7
            float r0 = r2.A00
            float r1 = r1 * r0
            int r0 = (int) r1
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            r3.measure(r0, r6)
        L_0x00ea:
            int r5 = r5 + 1
            goto L_0x00c6
        L_0x00ed:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onMeasure(int, int):void");
    }

    private Rect A02(Rect rect, View view) {
        if (rect == null) {
            rect = new Rect();
        }
        if (view == null) {
            rect.set(0, 0, 0, 0);
            return rect;
        }
        rect.left = view.getLeft();
        rect.right = view.getRight();
        rect.top = view.getTop();
        rect.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect.left += viewGroup.getLeft();
            rect.right += viewGroup.getRight();
            rect.top += viewGroup.getTop();
            rect.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect;
    }

    private C25964Cpf A03(int i, int i2) {
        C25964Cpf cpf = new C25964Cpf();
        cpf.A04 = i;
        cpf.A00 = this.A0C.A0G(this, i);
        cpf.A03 = this.A0C.A06(i);
        if (i2 < 0 || i2 >= this.A0m.size()) {
            this.A0m.add(cpf);
            return cpf;
        }
        this.A0m.add(i2, cpf);
        return cpf;
    }

    private void A07() {
        if (this.A06 != 0) {
            ArrayList arrayList = this.A0i;
            if (arrayList == null) {
                this.A0i = new ArrayList();
            } else {
                arrayList.clear();
            }
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                this.A0i.add(getChildAt(i));
            }
            Collections.sort(this.A0i, A0r);
        }
    }

    private void A08(int i) {
        C72633eh r0 = this.A0E;
        if (r0 != null) {
            r0.Bhk(i);
        }
        List list = this.A0H;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                C72633eh r02 = (C72633eh) this.A0H.get(i2);
                if (r02 != null) {
                    r02.Bhk(i);
                }
            }
        }
        C72633eh r03 = this.A0D;
        if (r03 != null) {
            r03.Bhk(i);
        }
    }

    public static void A0C(ViewPager viewPager, int i, int i2, int i3, int i4) {
        int i5;
        float f;
        if (i2 <= 0 || viewPager.A0m.isEmpty()) {
            C25964Cpf A0K2 = viewPager.A0K(viewPager.A05);
            if (A0K2 != null) {
                f = Math.min(A0K2.A02, viewPager.A03);
            } else {
                f = 0.0f;
            }
            i5 = (int) (f * ((float) ((i - viewPager.getPaddingLeft()) - viewPager.getPaddingRight())));
            if (i5 != viewPager.getScrollX()) {
                viewPager.A0D(false);
            } else {
                return;
            }
        } else if (!viewPager.mScroller.isFinished()) {
            viewPager.mScroller.setFinalX(viewPager.A0I() * A00(viewPager));
            return;
        } else {
            i5 = (int) ((((float) viewPager.getScrollX()) / ((float) (((i2 - viewPager.getPaddingLeft()) - viewPager.getPaddingRight()) + i4))) * ((float) (((i - viewPager.getPaddingLeft()) - viewPager.getPaddingRight()) + i3)));
        }
        viewPager.scrollTo(i5, viewPager.getScrollY());
    }

    private void A0D(boolean z) {
        boolean z2 = false;
        if (this.A0a == 2) {
            z2 = true;
        }
        if (z2) {
            if (this.A0O) {
                this.A0O = false;
            }
            if (!this.mScroller.isFinished()) {
                this.mScroller.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.mScroller.getCurrX();
                int currY = this.mScroller.getCurrY();
                if (!(scrollX == currX && scrollY == currY)) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        A0H(currX);
                    }
                }
            }
        }
        this.A0N = false;
        for (int i = 0; i < this.A0m.size(); i++) {
            C25964Cpf cpf = (C25964Cpf) this.A0m.get(i);
            if (cpf.A01) {
                cpf.A01 = false;
                z2 = true;
            }
        }
        if (!z2) {
            return;
        }
        if (z) {
            C15320v6.postOnAnimation(this, this.A0p);
        } else {
            this.A0p.run();
        }
    }

    private boolean A0E() {
        D3J d3j = this.A0C;
        if (d3j == null || this.A05 >= d3j.A08() - 1) {
            return false;
        }
        A0R(this.A05 + 1, true);
        return true;
    }

    private boolean A0G(float f) {
        boolean z;
        boolean z2;
        this.A02 = f;
        float scrollX = ((float) getScrollX()) + (this.A02 - f);
        float A002 = (float) A00(this);
        float f2 = this.A00 * A002;
        float f3 = this.A03 * A002;
        boolean z3 = false;
        C25964Cpf cpf = (C25964Cpf) this.A0m.get(0);
        ArrayList arrayList = this.A0m;
        C25964Cpf cpf2 = (C25964Cpf) arrayList.get(arrayList.size() - 1);
        if (cpf.A04 != 0) {
            f2 = cpf.A02 * A002;
            z = false;
        } else {
            z = true;
        }
        if (cpf2.A04 != this.A0C.A08() - 1) {
            f3 = cpf2.A02 * A002;
            z2 = false;
        } else {
            z2 = true;
        }
        if (scrollX < f2) {
            if (z) {
                this.A0e.onPull(Math.abs(f2 - scrollX) / A002);
                z3 = true;
            }
            scrollX = f2;
        } else if (scrollX > f3) {
            if (z2) {
                this.A0f.onPull(Math.abs(scrollX - f3) / A002);
                z3 = true;
            }
            scrollX = f3;
        }
        int i = (int) scrollX;
        this.A02 += scrollX - ((float) i);
        scrollTo(i, getScrollY());
        A0H(i);
        return z3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r8.A0j != false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A0H(int r9) {
        /*
            r8 = this;
            java.util.ArrayList r0 = r8.A0m
            int r0 = r0.size()
            java.lang.String r5 = "onPageScrolled did not call superclass implementation"
            r7 = 0
            if (r0 != 0) goto L_0x001a
            boolean r0 = r8.A0k
            if (r0 != 0) goto L_0x0019
            r8.A0j = r7
            r0 = 0
            r8.A0Q(r7, r0, r7)
            boolean r0 = r8.A0j
            if (r0 == 0) goto L_0x0042
        L_0x0019:
            return r7
        L_0x001a:
            X.Cpf r6 = A05(r8)
            int r1 = A00(r8)
            int r0 = r8.A08
            int r4 = r1 + r0
            float r3 = (float) r0
            float r0 = (float) r1
            float r3 = r3 / r0
            int r2 = r6.A04
            float r1 = (float) r9
            float r1 = r1 / r0
            float r0 = r6.A02
            float r1 = r1 - r0
            float r0 = r6.A03
            float r0 = r0 + r3
            float r1 = r1 / r0
            float r0 = (float) r4
            float r0 = r0 * r1
            int r0 = (int) r0
            r8.A0j = r7
            r8.A0Q(r2, r1, r0)
            boolean r0 = r8.A0j
            if (r0 == 0) goto L_0x0042
            r0 = 1
            return r0
        L_0x0042:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A0H(int):boolean");
    }

    public int A0I() {
        return this.A05;
    }

    public D3J A0J() {
        return this.A0C;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001e, code lost:
        if (r11.A0m.size() >= r6) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0L() {
        /*
            r11 = this;
            X.D3J r0 = r11.A0C
            int r6 = r0.A08()
            r11.A0V = r6
            java.util.ArrayList r0 = r11.A0m
            int r1 = r0.size()
            int r0 = r11.A0Y
            int r0 = r0 << 1
            r10 = 1
            int r0 = r0 + r10
            r5 = 0
            if (r1 >= r0) goto L_0x0020
            java.util.ArrayList r0 = r11.A0m
            int r0 = r0.size()
            r9 = 1
            if (r0 < r6) goto L_0x0021
        L_0x0020:
            r9 = 0
        L_0x0021:
            int r4 = r11.A05
            r3 = 0
            r8 = 0
        L_0x0025:
            java.util.ArrayList r0 = r11.A0m
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x007a
            java.util.ArrayList r0 = r11.A0m
            java.lang.Object r7 = r0.get(r3)
            X.Cpf r7 = (X.C25964Cpf) r7
            X.D3J r1 = r11.A0C
            java.lang.Object r0 = r7.A00
            int r2 = r1.A0F(r0)
            r0 = -1
            if (r2 == r0) goto L_0x006c
            r0 = -2
            if (r2 != r0) goto L_0x006e
            java.util.ArrayList r0 = r11.A0m
            r0.remove(r3)
            int r3 = r3 + -1
            if (r8 != 0) goto L_0x0052
            X.D3J r0 = r11.A0C
            r0.A0D(r11)
            r8 = 1
        L_0x0052:
            X.D3J r2 = r11.A0C
            int r1 = r7.A04
            java.lang.Object r0 = r7.A00
            r2.A0H(r11, r1, r0)
            int r1 = r11.A05
            int r0 = r7.A04
            if (r1 != r0) goto L_0x006b
            int r0 = r6 + -1
            int r0 = java.lang.Math.min(r1, r0)
            int r4 = java.lang.Math.max(r5, r0)
        L_0x006b:
            r9 = 1
        L_0x006c:
            int r3 = r3 + r10
            goto L_0x0025
        L_0x006e:
            int r1 = r7.A04
            if (r1 == r2) goto L_0x006c
            int r0 = r11.A05
            if (r1 != r0) goto L_0x0077
            r4 = r2
        L_0x0077:
            r7.A04 = r2
            goto L_0x006b
        L_0x007a:
            if (r8 == 0) goto L_0x0081
            X.D3J r0 = r11.A0C
            r0.A0C(r11)
        L_0x0081:
            java.util.ArrayList r1 = r11.A0m
            java.util.Comparator r0 = androidx.viewpager.widget.ViewPager.A0s
            java.util.Collections.sort(r1, r0)
            if (r9 == 0) goto L_0x00ab
            int r3 = r11.getChildCount()
            r2 = 0
        L_0x008f:
            if (r2 >= r3) goto L_0x00a5
            android.view.View r0 = r11.getChildAt(r2)
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            X.55z r1 = (X.C1061855z) r1
            boolean r0 = r1.A05
            if (r0 != 0) goto L_0x00a2
            r0 = 0
            r1.A00 = r0
        L_0x00a2:
            int r2 = r2 + 1
            goto L_0x008f
        L_0x00a5:
            r11.A0S(r4, r5, r10, r5)
            r11.requestLayout()
        L_0x00ab:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A0L():void");
    }

    public void A0M(float f) {
        if (!this.A0J) {
            throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
        } else if (this.A0C != null) {
            this.A02 += f;
            float scrollX = ((float) getScrollX()) - f;
            float A002 = (float) A00(this);
            float f2 = this.A00 * A002;
            float f3 = this.A03 * A002;
            C25964Cpf cpf = (C25964Cpf) this.A0m.get(0);
            ArrayList arrayList = this.A0m;
            C25964Cpf cpf2 = (C25964Cpf) arrayList.get(arrayList.size() - 1);
            if (cpf.A04 != 0) {
                f2 = cpf.A02 * A002;
            }
            if (cpf2.A04 != this.A0C.A08() - 1) {
                f3 = cpf2.A02 * A002;
            }
            if (scrollX < f2) {
                scrollX = f2;
            } else if (scrollX > f3) {
                scrollX = f3;
            }
            int i = (int) scrollX;
            this.A02 += scrollX - ((float) i);
            scrollTo(i, getScrollY());
            A0H(i);
            MotionEvent obtain = MotionEvent.obtain(this.A0A, SystemClock.uptimeMillis(), 2, this.A02, 0.0f, 0);
            this.A0B.addMovement(obtain);
            obtain.recycle();
        }
    }

    public void A0P(int i) {
        int i2;
        if (this.A0a != i) {
            this.A0a = i;
            if (this.A0F != null) {
                boolean z = false;
                if (i != 0) {
                    z = true;
                }
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    if (z) {
                        i2 = this.A09;
                    } else {
                        i2 = 0;
                    }
                    getChildAt(i3).setLayerType(i2, null);
                }
            }
            C72633eh r0 = this.A0E;
            if (r0 != null) {
                r0.Bhi(i);
            }
            List list = this.A0H;
            if (list != null) {
                int size = list.size();
                for (int i4 = 0; i4 < size; i4++) {
                    C72633eh r02 = (C72633eh) this.A0H.get(i4);
                    if (r02 != null) {
                        r02.Bhi(i);
                    }
                }
            }
            C72633eh r03 = this.A0D;
            if (r03 != null) {
                r03.Bhi(i);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Q(int r13, float r14, int r15) {
        /*
            r12 = this;
            int r0 = r12.A0T
            r6 = 0
            r5 = 1
            if (r0 <= 0) goto L_0x0068
            int r11 = r12.getScrollX()
            int r7 = r12.getPaddingLeft()
            int r10 = r12.getPaddingRight()
            int r9 = r12.getWidth()
            int r4 = r12.getChildCount()
            r3 = 0
        L_0x001b:
            if (r3 >= r4) goto L_0x0068
            android.view.View r2 = r12.getChildAt(r3)
            android.view.ViewGroup$LayoutParams r1 = r2.getLayoutParams()
            X.55z r1 = (X.C1061855z) r1
            boolean r0 = r1.A05
            if (r0 == 0) goto L_0x0052
            int r0 = r1.A04
            r8 = r0 & 7
            if (r8 == r5) goto L_0x005b
            r0 = 3
            if (r8 == r0) goto L_0x0055
            r1 = 5
            r0 = r7
            if (r8 != r1) goto L_0x0046
            int r1 = r9 - r10
            int r0 = r2.getMeasuredWidth()
            int r1 = r1 - r0
            int r0 = r2.getMeasuredWidth()
            int r10 = r10 + r0
        L_0x0044:
            r0 = r7
            r7 = r1
        L_0x0046:
            int r7 = r7 + r11
            int r1 = r2.getLeft()
            int r7 = r7 - r1
            if (r7 == 0) goto L_0x0051
            r2.offsetLeftAndRight(r7)
        L_0x0051:
            r7 = r0
        L_0x0052:
            int r3 = r3 + 1
            goto L_0x001b
        L_0x0055:
            int r0 = r2.getWidth()
            int r0 = r0 + r7
            goto L_0x0046
        L_0x005b:
            int r0 = r2.getMeasuredWidth()
            int r0 = r9 - r0
            int r0 = r0 >> 1
            int r1 = java.lang.Math.max(r0, r7)
            goto L_0x0044
        L_0x0068:
            X.3eh r0 = r12.A0E
            if (r0 == 0) goto L_0x006f
            r0.Bhj(r13, r14, r15)
        L_0x006f:
            java.util.List r0 = r12.A0H
            if (r0 == 0) goto L_0x008a
            r2 = 0
            int r1 = r0.size()
        L_0x0078:
            if (r2 >= r1) goto L_0x008a
            java.util.List r0 = r12.A0H
            java.lang.Object r0 = r0.get(r2)
            X.3eh r0 = (X.C72633eh) r0
            if (r0 == 0) goto L_0x0087
            r0.Bhj(r13, r14, r15)
        L_0x0087:
            int r2 = r2 + 1
            goto L_0x0078
        L_0x008a:
            X.3eh r0 = r12.A0D
            if (r0 == 0) goto L_0x0091
            r0.Bhj(r13, r14, r15)
        L_0x0091:
            X.5Nx r0 = r12.A0F
            if (r0 == 0) goto L_0x00c1
            int r4 = r12.getScrollX()
            int r3 = r12.getChildCount()
        L_0x009d:
            if (r6 >= r3) goto L_0x00c1
            android.view.View r2 = r12.getChildAt(r6)
            android.view.ViewGroup$LayoutParams r0 = r2.getLayoutParams()
            X.55z r0 = (X.C1061855z) r0
            boolean r0 = r0.A05
            if (r0 != 0) goto L_0x00be
            int r0 = r2.getLeft()
            int r0 = r0 - r4
            float r1 = (float) r0
            int r0 = A00(r12)
            float r0 = (float) r0
            float r1 = r1 / r0
            X.5Nx r0 = r12.A0F
            r0.CJM(r2, r1)
        L_0x00be:
            int r6 = r6 + 1
            goto L_0x009d
        L_0x00c1:
            r12.A0j = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A0Q(int, float, int):void");
    }

    public void A0S(int i, boolean z, boolean z2, int i2) {
        D3J d3j = this.A0C;
        if (d3j != null && d3j.A08() > 0 && (z2 || this.A05 != i || this.A0m.size() == 0)) {
            boolean z3 = true;
            if (i < 0) {
                i = 0;
            } else if (i >= this.A0C.A08()) {
                i = this.A0C.A08() - 1;
            }
            int i3 = this.A0Y;
            int i4 = this.A05;
            if (i > i4 + i3 || i < i4 - i3) {
                for (int i5 = 0; i5 < this.A0m.size(); i5++) {
                    ((C25964Cpf) this.A0m.get(i5)).A01 = true;
                }
            }
            if (this.A05 == i) {
                z3 = false;
            }
            if (this.A0k) {
                this.A05 = i;
                if (z3) {
                    A08(i);
                }
                requestLayout();
                return;
            }
            A0B(this, i);
            A09(i, z, i2, z3);
        } else if (this.A0O) {
            this.A0O = false;
        }
    }

    public void A0T(D3J d3j) {
        D3J d3j2 = this.A0C;
        if (d3j2 != null) {
            synchronized (d3j2) {
                try {
                    d3j2.A00 = null;
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                        break;
                    }
                }
            }
            this.A0C.A0D(this);
            for (int i = 0; i < this.A0m.size(); i++) {
                C25964Cpf cpf = (C25964Cpf) this.A0m.get(i);
                this.A0C.A0H(this, cpf.A04, cpf.A00);
            }
            this.A0C.A0C(this);
            this.A0m.clear();
            int i2 = 0;
            while (i2 < getChildCount()) {
                if (!((C1061855z) getChildAt(i2).getLayoutParams()).A05) {
                    removeViewAt(i2);
                    i2--;
                }
                i2++;
            }
            this.A05 = 0;
            scrollTo(0, 0);
        }
        D3J d3j3 = this.A0C;
        this.A0C = d3j;
        this.A0V = 0;
        if (d3j != null) {
            if (this.A0g == null) {
                this.A0g = new C1068259l(this);
            }
            D3J d3j4 = this.A0C;
            C1068259l r0 = this.A0g;
            synchronized (d3j4) {
                try {
                    d3j4.A00 = r0;
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
            this.A0N = false;
            boolean z = this.A0k;
            this.A0k = true;
            this.A0V = this.A0C.A08();
            if (this.A0Z >= 0) {
                this.A0C.A0B(this.A0d, this.A0h);
                A0S(this.A0Z, false, true, 0);
                this.A0Z = -1;
                this.A0d = null;
                this.A0h = null;
            } else if (!z) {
                A0B(this, this.A05);
            } else {
                requestLayout();
            }
        }
        List list = this.A0G;
        if (list != null && !list.isEmpty()) {
            int size = this.A0G.size();
            for (int i3 = 0; i3 < size; i3++) {
                ((C27470DdD) this.A0G.get(i3)).BNX(this, d3j3, d3j);
            }
        }
    }

    public void A0U(C72633eh r2) {
        if (this.A0H == null) {
            this.A0H = new ArrayList();
        }
        this.A0H.add(r2);
    }

    public boolean A0Y(View view, boolean z, int i, int i2, int i3) {
        int i4;
        int i5 = i;
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                int i6 = i2 + scrollX;
                if (i6 >= childAt.getLeft() && i6 < childAt.getRight() && (i4 = i3 + scrollY) >= childAt.getTop() && i4 < childAt.getBottom() && A0Y(childAt, true, i5, i6 - childAt.getLeft(), i4 - childAt.getTop())) {
                    return true;
                }
            }
        }
        if (!z || !view.canScrollHorizontally(-i)) {
            return false;
        }
        return true;
    }

    public boolean canScrollHorizontally(int i) {
        if (this.A0C == null) {
            return false;
        }
        int A002 = A00(this);
        int scrollX = getScrollX();
        if (i < 0) {
            if (scrollX > ((int) (((float) A002) * this.A00))) {
                return true;
            }
            return false;
        } else if (i <= 0 || scrollX >= ((int) (((float) A002) * this.A03))) {
            return false;
        }
        return true;
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (!(layoutParams instanceof C1061855z) || !super.checkLayoutParams(layoutParams)) {
            return false;
        }
        return true;
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new C1061855z();
    }

    public int getChildDrawingOrder(int i, int i2) {
        if (this.A06 == 2) {
            i2 = (i - 1) - i2;
        }
        return ((C1061855z) ((View) this.A0i.get(i2)).getLayoutParams()).A01;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r19) {
        /*
            r18 = this;
            r1 = r18
            r13 = r1
            r17 = r19
            r0 = r17
            super.onDraw(r0)
            int r0 = r1.A08
            if (r0 <= 0) goto L_0x009c
            r0 = 0
            if (r0 == 0) goto L_0x009c
            java.util.ArrayList r0 = r1.A0m
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x009c
            X.D3J r0 = r1.A0C
            if (r0 == 0) goto L_0x009c
            int r14 = r1.getScrollX()
            int r12 = r1.getWidth()
            int r0 = r1.A08
            float r11 = (float) r0
            float r10 = (float) r12
            float r11 = r11 / r10
            java.util.ArrayList r0 = r1.A0m
            r9 = 0
            java.lang.Object r8 = r0.get(r9)
            X.Cpf r8 = (X.C25964Cpf) r8
            float r7 = r8.A02
            java.util.ArrayList r0 = r1.A0m
            int r6 = r0.size()
            int r5 = r8.A04
            java.util.ArrayList r1 = r1.A0m
            int r0 = r6 + -1
            java.lang.Object r0 = r1.get(r0)
            X.Cpf r0 = (X.C25964Cpf) r0
            int r4 = r0.A04
        L_0x0049:
            if (r5 >= r4) goto L_0x009c
        L_0x004b:
            int r0 = r8.A04
            if (r5 <= r0) goto L_0x005c
            if (r9 >= r6) goto L_0x005c
            java.util.ArrayList r0 = r13.A0m
            int r9 = r9 + 1
            java.lang.Object r8 = r0.get(r9)
            X.Cpf r8 = (X.C25964Cpf) r8
            goto L_0x004b
        L_0x005c:
            if (r5 != r0) goto L_0x008f
            float r7 = r8.A02
            float r0 = r8.A03
            float r7 = r7 + r0
            float r16 = r7 * r10
            float r7 = r7 + r11
        L_0x0066:
            int r0 = r13.A08
            float r0 = (float) r0
            float r0 = r0 + r16
            float r1 = (float) r14
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0085
            r15 = 0
            int r3 = java.lang.Math.round(r16)
            int r2 = r13.A0b
            int r1 = java.lang.Math.round(r0)
            int r0 = r13.A0R
            r15.setBounds(r3, r2, r1, r0)
            r0 = r17
            r15.draw(r0)
        L_0x0085:
            int r0 = r14 + r12
            float r0 = (float) r0
            int r0 = (r16 > r0 ? 1 : (r16 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x009c
            int r5 = r5 + 1
            goto L_0x0049
        L_0x008f:
            X.D3J r0 = r13.A0C
            float r0 = r0.A06(r5)
            float r16 = r7 + r0
            float r16 = r16 * r10
            float r0 = r0 + r11
            float r7 = r7 + r0
            goto L_0x0066
        L_0x009c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onDraw(android.graphics.Canvas):void");
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        float f;
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent2.getAction() & 255;
        if (action == 3 || action == 1) {
            A0F();
            return false;
        }
        if (action != 0) {
            if (this.A0K) {
                return true;
            }
            if (this.A0M) {
                return false;
            }
        }
        if (action == 0) {
            float x = motionEvent2.getX();
            this.A01 = x;
            this.A02 = x;
            float y = motionEvent2.getY();
            this.A0P = y;
            this.A0Q = y;
            this.A04 = motionEvent2.getPointerId(0);
            this.A0M = false;
            this.A0L = true;
            this.mScroller.computeScrollOffset();
            if (this.A0a != 2 || Math.abs(this.mScroller.getFinalX() - this.mScroller.getCurrX()) <= this.A0S) {
                A0D(false);
                this.A0K = false;
            } else {
                this.mScroller.abortAnimation();
                this.A0N = false;
                A0B(this, this.A05);
                this.A0K = true;
                ViewParent parent = getParent();
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true);
                }
                A0P(1);
            }
        } else if (action == 2) {
            int i = this.A04;
            if (i != -1) {
                int findPointerIndex = motionEvent2.findPointerIndex(i);
                float x2 = motionEvent2.getX(findPointerIndex);
                float f2 = x2 - this.A02;
                float abs = Math.abs(f2);
                float y2 = motionEvent2.getY(findPointerIndex);
                float abs2 = Math.abs(y2 - this.A0P);
                if (f2 != 0.0f) {
                    float f3 = this.A02;
                    boolean z = false;
                    if (!this.A0I && ((f3 < ((float) this.mGutterSize) && f2 > 0.0f) || (f3 > ((float) (getWidth() - this.mGutterSize)) && f2 < 0.0f))) {
                        z = true;
                    }
                    if (!z) {
                        if (A0Y(this, false, (int) f2, (int) x2, (int) y2)) {
                            this.A02 = x2;
                            this.A0Q = y2;
                            this.A0M = true;
                            return false;
                        }
                    }
                }
                float f4 = (float) this.A0c;
                if (abs > f4 && abs * 0.5f > abs2) {
                    this.A0K = true;
                    ViewParent parent2 = getParent();
                    if (parent2 != null) {
                        parent2.requestDisallowInterceptTouchEvent(true);
                    }
                    A0P(1);
                    if (f2 > 0.0f) {
                        f = this.A01 + ((float) this.A0c);
                    } else {
                        f = this.A01 - ((float) this.A0c);
                    }
                    this.A02 = f;
                    this.A0Q = y2;
                    if (!this.A0O) {
                        this.A0O = true;
                    }
                } else if (abs2 > f4) {
                    this.A0M = true;
                }
                if (this.A0K && A0G(x2)) {
                    C15320v6.postInvalidateOnAnimation(this);
                }
            }
        } else if (action == 6) {
            A0A(motionEvent2);
        }
        if (this.A0B == null) {
            this.A0B = VelocityTracker.obtain();
        }
        this.A0B.addMovement(motionEvent2);
        return this.A0K;
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        boolean z2;
        C25964Cpf A042;
        int max;
        int max2;
        int childCount = getChildCount();
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i7 = 0;
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                C1061855z r3 = (C1061855z) childAt.getLayoutParams();
                if (r3.A05) {
                    int i9 = r3.A04;
                    int i10 = i9 & 7;
                    int i11 = i9 & 112;
                    if (i10 == 1) {
                        max = Math.max((i5 - childAt.getMeasuredWidth()) >> 1, paddingLeft);
                    } else if (i10 != 3) {
                        max = paddingLeft;
                        if (i10 == 5) {
                            max = (i5 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                        }
                    } else {
                        max = paddingLeft;
                        paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                    }
                    if (i11 == 16) {
                        max2 = Math.max((i6 - childAt.getMeasuredHeight()) >> 1, paddingTop);
                    } else if (i11 != 48) {
                        max2 = paddingTop;
                        if (i11 == 80) {
                            max2 = (i6 - paddingBottom) - childAt.getMeasuredHeight();
                            paddingBottom += childAt.getMeasuredHeight();
                        }
                    } else {
                        max2 = paddingTop;
                        paddingTop = childAt.getMeasuredHeight() + paddingTop;
                    }
                    int i12 = max + scrollX;
                    childAt.layout(i12, max2, childAt.getMeasuredWidth() + i12, childAt.getMeasuredHeight() + max2);
                    i7++;
                }
            }
        }
        int i13 = (i5 - paddingLeft) - paddingRight;
        for (int i14 = 0; i14 < childCount; i14++) {
            View childAt2 = getChildAt(i14);
            if (childAt2.getVisibility() != 8) {
                C1061855z r13 = (C1061855z) childAt2.getLayoutParams();
                if (!r13.A05 && (A042 = A04(childAt2)) != null) {
                    float f = (float) i13;
                    int i15 = ((int) (A042.A02 * f)) + paddingLeft;
                    if (r13.A03) {
                        r13.A03 = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (f * r13.A00), 1073741824), View.MeasureSpec.makeMeasureSpec((i6 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i15, paddingTop, childAt2.getMeasuredWidth() + i15, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.A0b = paddingTop;
        this.A0R = i6 - paddingBottom;
        this.A0T = i7;
        if (this.A0k) {
            z2 = false;
            A09(this.A05, false, 0, false);
        } else {
            z2 = false;
        }
        this.A0k = z2;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.A00);
        D3J d3j = this.A0C;
        if (d3j != null) {
            d3j.A0B(savedState.A01, savedState.A02);
            A0S(savedState.A00, false, true, 0);
            return;
        }
        this.A0Z = savedState.A00;
        this.A0d = savedState.A01;
        this.A0h = savedState.A02;
    }

    public void removeView(View view) {
        if (this.A0l) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    public static int A00(ViewPager viewPager) {
        return (viewPager.getMeasuredWidth() - viewPager.getPaddingLeft()) - viewPager.getPaddingRight();
    }

    public static int A01(ViewPager viewPager, int i, float f, int i2, int i3) {
        if (Math.abs(i3) <= viewPager.A0W || Math.abs(i2) <= viewPager.A0X) {
            float f2 = 0.6f;
            if (i >= viewPager.A05) {
                f2 = 0.4f;
            }
            i += (int) (f + f2);
        } else if (i2 <= 0) {
            i++;
        }
        if (viewPager.A0m.size() <= 0) {
            return i;
        }
        ArrayList arrayList = viewPager.A0m;
        return Math.max(((C25964Cpf) viewPager.A0m.get(0)).A04, Math.min(i, ((C25964Cpf) arrayList.get(arrayList.size() - 1)).A04));
    }

    public static C25964Cpf A05(ViewPager viewPager) {
        float f;
        float f2;
        int i;
        int A002 = A00(viewPager);
        if (A002 > 0) {
            f = ((float) viewPager.getScrollX()) / ((float) A002);
        } else {
            f = 0.0f;
        }
        if (A002 > 0) {
            f2 = ((float) viewPager.A08) / ((float) A002);
        } else {
            f2 = 0.0f;
        }
        C25964Cpf cpf = null;
        int i2 = 0;
        boolean z = true;
        int i3 = -1;
        float f3 = 0.0f;
        float f4 = 0.0f;
        while (i2 < viewPager.A0m.size()) {
            C25964Cpf cpf2 = (C25964Cpf) viewPager.A0m.get(i2);
            if (!z && cpf2.A04 != (i = i3 + 1)) {
                cpf2 = viewPager.A0o;
                cpf2.A02 = f3 + f4 + f2;
                cpf2.A04 = i;
                cpf2.A03 = viewPager.A0C.A06(i);
                i2--;
            }
            f3 = cpf2.A02;
            float f5 = cpf2.A03 + f3 + f2;
            if (!z && f < f3) {
                break;
            } else if (f < f5 || i2 == viewPager.A0m.size() - 1) {
                return cpf2;
            } else {
                i3 = cpf2.A04;
                f4 = cpf2.A03;
                i2++;
                cpf = cpf2;
                z = false;
            }
        }
        return cpf;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
        if (r1 != false) goto L_0x003b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A09(int r15, boolean r16, int r17, boolean r18) {
        /*
            r14 = this;
            X.Cpf r1 = r14.A0K(r15)
            r6 = 0
            if (r1 == 0) goto L_0x00e1
            int r0 = A00(r14)
            float r3 = (float) r0
            float r2 = r14.A00
            float r1 = r1.A02
            float r0 = r14.A03
            float r0 = java.lang.Math.min(r1, r0)
            float r0 = java.lang.Math.max(r2, r0)
            float r3 = r3 * r0
            int r11 = (int) r3
        L_0x001c:
            if (r16 == 0) goto L_0x00e4
            int r0 = r14.getChildCount()
            if (r0 != 0) goto L_0x0030
            boolean r0 = r14.A0O
            if (r0 == r6) goto L_0x002a
            r14.A0O = r6
        L_0x002a:
            if (r18 == 0) goto L_0x002f
            r14.A08(r15)
        L_0x002f:
            return
        L_0x0030:
            android.widget.Scroller r0 = r14.mScroller
            if (r0 == 0) goto L_0x003b
            boolean r1 = r0.isFinished()
            r0 = 1
            if (r1 == 0) goto L_0x003c
        L_0x003b:
            r0 = 0
        L_0x003c:
            if (r0 == 0) goto L_0x0071
            boolean r0 = r14.A0L
            if (r0 == 0) goto L_0x006a
            android.widget.Scroller r0 = r14.mScroller
            int r9 = r0.getCurrX()
        L_0x0048:
            android.widget.Scroller r0 = r14.mScroller
            r0.abortAnimation()
            boolean r0 = r14.A0O
            if (r0 == r6) goto L_0x0053
            r14.A0O = r6
        L_0x0053:
            int r10 = r14.getScrollY()
            int r11 = r11 - r9
            int r12 = r6 - r10
            if (r11 != 0) goto L_0x0076
            if (r12 != 0) goto L_0x0076
            r14.A0D(r6)
            int r0 = r14.A05
            A0B(r14, r0)
            r14.A0P(r6)
            goto L_0x002a
        L_0x006a:
            android.widget.Scroller r0 = r14.mScroller
            int r9 = r0.getStartX()
            goto L_0x0048
        L_0x0071:
            int r9 = r14.getScrollX()
            goto L_0x0053
        L_0x0076:
            r1 = 1
            boolean r0 = r14.A0O
            if (r0 == r1) goto L_0x007d
            r14.A0O = r1
        L_0x007d:
            r0 = 2
            r14.A0P(r0)
            int r1 = A00(r14)
            int r2 = r1 >> 1
            int r0 = java.lang.Math.abs(r11)
            float r7 = (float) r0
            r4 = 1065353216(0x3f800000, float:1.0)
            float r0 = r7 * r4
            float r3 = (float) r1
            float r0 = r0 / r3
            float r1 = java.lang.Math.min(r4, r0)
            float r2 = (float) r2
            r0 = 1056964608(0x3f000000, float:0.5)
            float r1 = r1 - r0
            r0 = 1055999547(0x3ef1463b, float:0.47123894)
            float r1 = r1 * r0
            double r0 = (double) r1
            double r0 = java.lang.Math.sin(r0)
            float r8 = (float) r0
            float r8 = r8 * r2
            float r2 = r2 + r8
            int r0 = java.lang.Math.abs(r17)
            if (r0 <= 0) goto L_0x00cd
            r1 = 1148846080(0x447a0000, float:1000.0)
            float r0 = (float) r0
            float r2 = r2 / r0
            float r0 = java.lang.Math.abs(r2)
            float r0 = r0 * r1
            int r0 = java.lang.Math.round(r0)
            int r1 = r0 << 2
        L_0x00bb:
            r0 = 600(0x258, float:8.41E-43)
            int r13 = java.lang.Math.min(r1, r0)
            r14.A0L = r6
            android.widget.Scroller r8 = r14.mScroller
            r8.startScroll(r9, r10, r11, r12, r13)
            X.C15320v6.postInvalidateOnAnimation(r14)
            goto L_0x002a
        L_0x00cd:
            X.D3J r1 = r14.A0C
            int r0 = r14.A05
            float r0 = r1.A06(r0)
            float r3 = r3 * r0
            int r0 = r14.A08
            float r0 = (float) r0
            float r3 = r3 + r0
            float r7 = r7 / r3
            float r7 = r7 + r4
            r0 = 1120403456(0x42c80000, float:100.0)
            float r7 = r7 * r0
            int r1 = (int) r7
            goto L_0x00bb
        L_0x00e1:
            r11 = 0
            goto L_0x001c
        L_0x00e4:
            if (r18 == 0) goto L_0x00e9
            r14.A08(r15)
        L_0x00e9:
            r14.A0D(r6)
            r14.scrollTo(r11, r6)
            r14.A0H(r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A09(int, boolean, int, boolean):void");
    }

    private void A0A(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.A04) {
            int i = 0;
            if (actionIndex == 0) {
                i = 1;
            }
            this.A02 = motionEvent.getX(i);
            this.A04 = motionEvent.getPointerId(i);
            VelocityTracker velocityTracker = this.A0B;
            if (velocityTracker != null) {
                velocityTracker.clear();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0081, code lost:
        if (r1 >= r0) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x009e, code lost:
        if (r8 != 2) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b5, code lost:
        if (r1 <= r0) goto L_0x00b7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0X(int r8) {
        /*
            r7 = this;
            android.view.View r6 = r7.findFocus()
            r4 = 1
            r5 = 0
            r3 = 0
            if (r6 == r7) goto L_0x005d
            if (r6 == 0) goto L_0x004c
            android.view.ViewParent r1 = r6.getParent()
        L_0x000f:
            boolean r0 = r1 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x004a
            if (r1 != r7) goto L_0x0045
            r0 = 1
        L_0x0016:
            if (r0 != 0) goto L_0x004c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r0 = r6.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r2.append(r0)
            android.view.ViewParent r1 = r6.getParent()
        L_0x002c:
            boolean r0 = r1 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x004e
            java.lang.String r0 = " => "
            r2.append(r0)
            java.lang.Class r0 = r1.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r2.append(r0)
            android.view.ViewParent r1 = r1.getParent()
            goto L_0x002c
        L_0x0045:
            android.view.ViewParent r1 = r1.getParent()
            goto L_0x000f
        L_0x004a:
            r0 = 0
            goto L_0x0016
        L_0x004c:
            r3 = r6
            goto L_0x005d
        L_0x004e:
            java.lang.String r1 = "arrowScroll tried to find focus based on non-child current focused view "
            java.lang.String r0 = r2.toString()
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)
            java.lang.String r0 = "ViewPager"
            android.util.Log.e(r0, r1)
        L_0x005d:
            android.view.FocusFinder r0 = android.view.FocusFinder.getInstance()
            android.view.View r2 = r0.findNextFocus(r7, r3, r8)
            r1 = 66
            r0 = 17
            if (r2 == 0) goto L_0x0097
            if (r2 == r3) goto L_0x0097
            if (r8 != r0) goto L_0x00a1
            android.graphics.Rect r0 = r7.A0n
            android.graphics.Rect r0 = r7.A02(r0, r2)
            int r1 = r0.left
            android.graphics.Rect r0 = r7.A0n
            android.graphics.Rect r0 = r7.A02(r0, r3)
            int r0 = r0.left
            if (r3 == 0) goto L_0x00bc
            if (r1 < r0) goto L_0x00bc
        L_0x0083:
            int r1 = r7.A05
            if (r1 <= 0) goto L_0x008d
            r0 = 1
            int r1 = r1 - r0
            r7.A0R(r1, r0)
            r5 = 1
        L_0x008d:
            if (r5 == 0) goto L_0x0096
            int r0 = android.view.SoundEffectConstants.getContantForFocusDirection(r8)
            r7.playSoundEffect(r0)
        L_0x0096:
            return r5
        L_0x0097:
            if (r8 == r0) goto L_0x0083
            if (r8 == r4) goto L_0x0083
            if (r8 == r1) goto L_0x00b7
            r0 = 2
            if (r8 != r0) goto L_0x008d
            goto L_0x00b7
        L_0x00a1:
            if (r8 != r1) goto L_0x008d
            android.graphics.Rect r0 = r7.A0n
            android.graphics.Rect r0 = r7.A02(r0, r2)
            int r1 = r0.left
            android.graphics.Rect r0 = r7.A0n
            android.graphics.Rect r0 = r7.A02(r0, r3)
            int r0 = r0.left
            if (r3 == 0) goto L_0x00bc
            if (r1 > r0) goto L_0x00bc
        L_0x00b7:
            boolean r5 = r7.A0E()
            goto L_0x008d
        L_0x00bc:
            boolean r5 = r2.requestFocus()
            goto L_0x008d
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A0X(int):boolean");
    }

    public void addFocusables(ArrayList arrayList, int i, int i2) {
        C25964Cpf A042;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                if (childAt.getVisibility() == 0 && (A042 = A04(childAt)) != null && A042.A04 == this.A05) {
                    childAt.addFocusables(arrayList, i, i2);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if ((i2 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) {
            arrayList.add(this);
        }
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (!checkLayoutParams(layoutParams)) {
            layoutParams = generateLayoutParams(layoutParams);
        }
        C1061855z r3 = (C1061855z) layoutParams;
        boolean z = r3.A05;
        boolean z2 = false;
        if (view.getClass().getAnnotation(DecorView.class) != null) {
            z2 = true;
        }
        boolean z3 = z | z2;
        r3.A05 = z3;
        if (!this.A0l) {
            super.addView(view, i, layoutParams);
        } else if (!z3) {
            r3.A03 = true;
            addViewInLayout(view, i, layoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        if (r4.hasModifiers(1) != false) goto L_0x002a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r4) {
        /*
            r3 = this;
            boolean r0 = super.dispatchKeyEvent(r4)
            if (r0 != 0) goto L_0x0031
            int r0 = r4.getAction()
            if (r0 != 0) goto L_0x005d
            int r2 = r4.getKeyCode()
            r0 = 21
            r1 = 2
            if (r2 == r0) goto L_0x0045
            r0 = 22
            if (r2 == r0) goto L_0x0033
            r0 = 61
            if (r2 != r0) goto L_0x005d
            boolean r0 = r4.hasNoModifiers()
            if (r0 != 0) goto L_0x002a
            r1 = 1
            boolean r0 = r4.hasModifiers(r1)
            if (r0 == 0) goto L_0x005d
        L_0x002a:
            boolean r1 = r3.A0X(r1)
        L_0x002e:
            r0 = 0
            if (r1 == 0) goto L_0x0032
        L_0x0031:
            r0 = 1
        L_0x0032:
            return r0
        L_0x0033:
            boolean r0 = r4.hasModifiers(r1)
            if (r0 == 0) goto L_0x003e
            boolean r1 = r3.A0E()
            goto L_0x002e
        L_0x003e:
            r0 = 66
            boolean r1 = r3.A0X(r0)
            goto L_0x002e
        L_0x0045:
            boolean r0 = r4.hasModifiers(r1)
            if (r0 == 0) goto L_0x0056
            int r1 = r3.A05
            if (r1 <= 0) goto L_0x005d
            r0 = 1
            int r1 = r1 - r0
            r3.A0R(r1, r0)
            r1 = 1
            goto L_0x002e
        L_0x0056:
            r0 = 17
            boolean r1 = r3.A0X(r0)
            goto L_0x002e
        L_0x005d:
            r1 = 0
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        C25964Cpf A042;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (A042 = A04(childAt)) != null && A042.A04 == this.A05 && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        D3J d3j;
        super.draw(canvas);
        int overScrollMode = getOverScrollMode();
        boolean z = false;
        if (overScrollMode == 0 || (overScrollMode == 1 && (d3j = this.A0C) != null && d3j.A08() > 1)) {
            if (!this.A0e.isFinished()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.A00 * ((float) width));
                this.A0e.setSize(height, width);
                z = false | this.A0e.draw(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.A0f.isFinished()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.A03 + 1.0f)) * ((float) width2));
                this.A0f.setSize(height2, width2);
                z |= this.A0f.draw(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.A0e.finish();
            this.A0f.finish();
        }
        if (z) {
            C15320v6.postInvalidateOnAnimation(this);
        }
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = null;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    public void onAttachedToWindow() {
        int A062 = C000700l.A06(-782663130);
        super.onAttachedToWindow();
        this.A0k = true;
        C000700l.A0C(-1814596170, A062);
    }

    public void onDetachedFromWindow() {
        int A062 = C000700l.A06(1408290646);
        removeCallbacks(this.A0p);
        Scroller scroller = this.mScroller;
        if (scroller != null && !scroller.isFinished()) {
            this.mScroller.abortAnimation();
        }
        super.onDetachedFromWindow();
        C000700l.A0C(753494652, A062);
    }

    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2;
        int i3;
        C25964Cpf A042;
        int childCount = getChildCount();
        int i4 = -1;
        if ((i & 2) != 0) {
            i4 = childCount;
            i2 = 0;
            i3 = 1;
        } else {
            i2 = childCount - 1;
            i3 = -1;
        }
        while (i2 != i4) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (A042 = A04(childAt)) != null && A042.A04 == this.A05 && childAt.requestFocus(i, rect)) {
                return true;
            }
            i2 += i3;
        }
        return false;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.A00 = this.A05;
        D3J d3j = this.A0C;
        if (d3j != null) {
            savedState.A01 = d3j.A09();
        }
        return savedState;
    }

    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int A062 = C000700l.A06(-2075028882);
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            int i5 = this.A08;
            A0C(this, i, i3, i5, i5);
        }
        C000700l.A0C(-1549034334, A062);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f;
        int A052 = C000700l.A05(-649692108);
        if (this.A0J) {
            C000700l.A0B(-1853883758, A052);
            return true;
        }
        boolean z = false;
        if (motionEvent.getAction() != 0 || motionEvent.getEdgeFlags() == 0) {
            D3J d3j = this.A0C;
            if (d3j == null || d3j.A08() == 0) {
                C000700l.A0B(-576202369, A052);
                return false;
            }
            if (this.A0B == null) {
                this.A0B = VelocityTracker.obtain();
            }
            this.A0B.addMovement(motionEvent);
            int action = motionEvent.getAction() & 255;
            if (action == 0) {
                this.mScroller.abortAnimation();
                this.A0N = false;
                A0B(this, this.A05);
                float x = motionEvent.getX();
                this.A01 = x;
                this.A02 = x;
                float y = motionEvent.getY();
                this.A0P = y;
                this.A0Q = y;
                this.A04 = motionEvent.getPointerId(0);
            } else if (action != 1) {
                if (action == 2) {
                    if (!this.A0K) {
                        int findPointerIndex = motionEvent.findPointerIndex(this.A04);
                        if (findPointerIndex == -1) {
                            z = A0F();
                        } else {
                            float x2 = motionEvent.getX(findPointerIndex);
                            float abs = Math.abs(x2 - this.A02);
                            float y2 = motionEvent.getY(findPointerIndex);
                            float abs2 = Math.abs(y2 - this.A0Q);
                            if (abs > ((float) this.A0c) && abs > abs2) {
                                this.A0K = true;
                                ViewParent parent = getParent();
                                if (parent != null) {
                                    parent.requestDisallowInterceptTouchEvent(true);
                                }
                                float f2 = this.A01;
                                if (x2 - f2 > 0.0f) {
                                    f = f2 + ((float) this.A0c);
                                } else {
                                    f = f2 - ((float) this.A0c);
                                }
                                this.A02 = f;
                                this.A0Q = y2;
                                A0P(1);
                                if (!this.A0O) {
                                    this.A0O = true;
                                }
                                ViewParent parent2 = getParent();
                                if (parent2 != null) {
                                    parent2.requestDisallowInterceptTouchEvent(true);
                                }
                            }
                        }
                    }
                    if (this.A0K) {
                        z = false | A0G(motionEvent.getX(motionEvent.findPointerIndex(this.A04)));
                    }
                } else if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.A02 = motionEvent.getX(actionIndex);
                        this.A04 = motionEvent.getPointerId(actionIndex);
                    } else if (action == 6) {
                        A0A(motionEvent);
                        this.A02 = motionEvent.getX(motionEvent.findPointerIndex(this.A04));
                    }
                } else if (this.A0K) {
                    A09(this.A05, true, 0, false);
                    z = A0F();
                }
            } else if (this.A0K) {
                VelocityTracker velocityTracker = this.A0B;
                velocityTracker.computeCurrentVelocity(AnonymousClass1Y3.A87, (float) this.A07);
                int xVelocity = (int) velocityTracker.getXVelocity(this.A04);
                this.A0N = true;
                int A002 = A00(this);
                int scrollX = getScrollX();
                C25964Cpf A053 = A05(this);
                float f3 = (float) A002;
                A0S(A01(this, A053.A04, ((((float) scrollX) / f3) - A053.A02) / (A053.A03 + (((float) this.A08) / f3)), xVelocity, (int) (motionEvent.getX(motionEvent.findPointerIndex(this.A04)) - this.A01)), true, true, xVelocity);
                z = A0F();
            }
            if (z) {
                C15320v6.postInvalidateOnAnimation(this);
            }
            C000700l.A0B(-878972735, A052);
            return true;
        }
        C000700l.A0B(-961796525, A052);
        return false;
    }

    public boolean verifyDrawable(Drawable drawable) {
        if (super.verifyDrawable(drawable) || drawable == null) {
            return true;
        }
        return false;
    }

    public void A0V(C72633eh r1) {
        this.A0E = r1;
    }

    public ViewPager(Context context) {
        super(context);
        A06();
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A06();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C1061855z(getContext(), attributeSet);
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }
}
