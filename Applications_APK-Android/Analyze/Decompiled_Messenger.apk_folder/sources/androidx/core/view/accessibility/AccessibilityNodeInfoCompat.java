package androidx.core.view.accessibility;

import X.AnonymousClass08S;
import X.AnonymousClass4CJ;
import X.AnonymousClass5E2;
import X.AnonymousClass8NI;
import X.C25973Cpq;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.facebook.common.dextricks.DexStore;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class AccessibilityNodeInfoCompat {
    public static int A03;
    public int A00 = -1;
    private int A01 = -1;
    public final AccessibilityNodeInfo A02;

    private static String A02(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case DexStore.LOAD_RESULT_OATMEAL_QUICKENED:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED:
                return "ACTION_SCROLL_FORWARD";
            case DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED:
                return "ACTION_SCROLL_BACKWARD";
            case DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET:
                return "ACTION_COPY";
            case DexStore.LOAD_RESULT_PGO:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP:
                return "ACTION_SET_SELECTION";
            case DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED:
                return "ACTION_EXPAND";
            case DexStore.LOAD_RESULT_WITH_VDEX_ODEX:
                return "ACTION_COLLAPSE";
            case 2097152:
                return "ACTION_SET_TEXT";
            case 16908354:
                return "ACTION_MOVE_WINDOW";
            default:
                switch (i) {
                    case 16908342:
                        return "ACTION_SHOW_ON_SCREEN";
                    case 16908343:
                        return "ACTION_SCROLL_TO_POSITION";
                    case 16908344:
                        return "ACTION_SCROLL_UP";
                    case 16908345:
                        return "ACTION_SCROLL_LEFT";
                    case 16908346:
                        return "ACTION_SCROLL_DOWN";
                    case 16908347:
                        return "ACTION_SCROLL_RIGHT";
                    case 16908348:
                        return "ACTION_CONTEXT_CLICK";
                    case 16908349:
                        return "ACTION_SET_PROGRESS";
                    default:
                        switch (i) {
                            case 16908356:
                                return "ACTION_SHOW_TOOLTIP";
                            case 16908357:
                                return "ACTION_HIDE_TOOLTIP";
                            case 16908358:
                                return "ACTION_PAGE_UP";
                            case 16908359:
                                return "ACTION_PAGE_DOWN";
                            case 16908360:
                                return "ACTION_PAGE_LEFT";
                            case 16908361:
                                return "ACTION_PAGE_RIGHT";
                            default:
                                return "ACTION_UNKNOWN";
                        }
                }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r5.A02 != null) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x002b
            r2 = 0
            if (r5 == 0) goto L_0x0014
            boolean r0 = r5 instanceof androidx.core.view.accessibility.AccessibilityNodeInfoCompat
            if (r0 == 0) goto L_0x0014
            androidx.core.view.accessibility.AccessibilityNodeInfoCompat r5 = (androidx.core.view.accessibility.AccessibilityNodeInfoCompat) r5
            android.view.accessibility.AccessibilityNodeInfo r1 = r4.A02
            if (r1 != 0) goto L_0x0015
            android.view.accessibility.AccessibilityNodeInfo r0 = r5.A02
            if (r0 == 0) goto L_0x001e
        L_0x0014:
            return r2
        L_0x0015:
            android.view.accessibility.AccessibilityNodeInfo r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x001e
            return r2
        L_0x001e:
            int r1 = r4.A01
            int r0 = r5.A01
            if (r1 != r0) goto L_0x0014
            int r1 = r4.A00
            int r0 = r5.A00
            if (r1 == r0) goto L_0x002b
            return r2
        L_0x002b:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.core.view.accessibility.AccessibilityNodeInfoCompat.equals(java.lang.Object):boolean");
    }

    private Bundle A00() {
        if (Build.VERSION.SDK_INT >= 19) {
            return this.A02.getExtras();
        }
        return new Bundle();
    }

    private List A03(String str) {
        if (Build.VERSION.SDK_INT < 19) {
            return new ArrayList();
        }
        ArrayList<Integer> integerArrayList = this.A02.getExtras().getIntegerArrayList(str);
        if (integerArrayList != null) {
            return integerArrayList;
        }
        ArrayList arrayList = new ArrayList();
        this.A02.getExtras().putIntegerArrayList(str, arrayList);
        return arrayList;
    }

    public AnonymousClass8NI A05() {
        AccessibilityNodeInfo.CollectionInfo collectionInfo;
        if (Build.VERSION.SDK_INT < 19 || (collectionInfo = this.A02.getCollectionInfo()) == null) {
            return null;
        }
        return new AnonymousClass8NI(collectionInfo);
    }

    public CharSequence A06() {
        if (!(!A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty())) {
            return this.A02.getText();
        }
        List A032 = A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        List A033 = A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        List A034 = A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        List A035 = A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        SpannableString spannableString = new SpannableString(TextUtils.substring(this.A02.getText(), 0, this.A02.getText().length()));
        for (int i = 0; i < A032.size(); i++) {
            spannableString.setSpan(new AnonymousClass5E2(((Integer) A035.get(i)).intValue(), this, A00().getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), ((Integer) A032.get(i)).intValue(), ((Integer) A033.get(i)).intValue(), ((Integer) A034.get(i)).intValue());
        }
        return spannableString;
    }

    public List A07() {
        List<AccessibilityNodeInfo.AccessibilityAction> list;
        if (Build.VERSION.SDK_INT >= 21) {
            list = this.A02.getActionList();
        } else {
            list = null;
        }
        if (list == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            arrayList.add(new C25973Cpq(list.get(i), 0, null, null, null));
        }
        return arrayList;
    }

    public void A08() {
        this.A02.recycle();
    }

    public void A09(int i) {
        this.A02.addAction(i);
    }

    public void A0A(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.A02.setMovementGranularities(i);
        }
    }

    public void A0B(int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.A02.performAction(i, bundle);
        }
    }

    public void A0C(View view) {
        if (Build.VERSION.SDK_INT >= 22) {
            this.A02.setTraversalAfter(view);
        }
    }

    public void A0D(View view) {
        if (Build.VERSION.SDK_INT >= 22) {
            this.A02.setTraversalBefore(view);
        }
    }

    public void A0E(View view, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.A02.addChild(view, i);
        }
    }

    public void A0F(View view, int i) {
        this.A00 = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.A02.setParent(view, i);
        }
    }

    public void A0G(View view, int i) {
        this.A01 = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.A02.setSource(view, i);
        }
    }

    public void A0H(C25973Cpq cpq) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.A02.addAction((AccessibilityNodeInfo.AccessibilityAction) cpq.A00);
        }
    }

    public void A0I(C25973Cpq cpq) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.A02.removeAction((AccessibilityNodeInfo.AccessibilityAction) cpq.A00);
        }
    }

    public void A0J(CharSequence charSequence) {
        this.A02.setClassName(charSequence);
    }

    public void A0K(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.A02.setError(charSequence);
        }
    }

    public void A0L(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.A02.setHintText(charSequence);
        } else if (i >= 19) {
            this.A02.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }

    public void A0M(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            this.A02.setPaneTitle(charSequence);
        } else if (i >= 19) {
            this.A02.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }

    public void A0N(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.A02.getExtras().putCharSequence("AccessibilityNodeInfo.roleDescription", charSequence);
        }
    }

    public void A0O(CharSequence charSequence, View view) {
        ClickableSpan[] clickableSpanArr;
        int length;
        int i;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 19 && i2 < 26) {
            if (i2 >= 19) {
                this.A02.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
                this.A02.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
                this.A02.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
                this.A02.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
            }
            SparseArray sparseArray = (SparseArray) view.getTag(2131300901);
            if (sparseArray != null) {
                ArrayList arrayList = new ArrayList();
                for (int i3 = 0; i3 < sparseArray.size(); i3++) {
                    if (((WeakReference) sparseArray.valueAt(i3)).get() == null) {
                        arrayList.add(Integer.valueOf(i3));
                    }
                }
                for (int i4 = 0; i4 < arrayList.size(); i4++) {
                    sparseArray.remove(((Integer) arrayList.get(i4)).intValue());
                }
            }
            if (charSequence instanceof Spanned) {
                clickableSpanArr = (ClickableSpan[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), ClickableSpan.class);
            } else {
                clickableSpanArr = null;
            }
            if (clickableSpanArr != null && (length = clickableSpanArr.length) > 0) {
                A00().putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", 2131296275);
                SparseArray sparseArray2 = (SparseArray) view.getTag(2131300901);
                if (sparseArray2 == null) {
                    sparseArray2 = new SparseArray();
                    view.setTag(2131300901, sparseArray2);
                }
                for (int i5 = 0; i5 < length; i5++) {
                    ClickableSpan clickableSpan = clickableSpanArr[i5];
                    if (sparseArray2 != null) {
                        int i6 = 0;
                        while (true) {
                            if (i6 >= sparseArray2.size()) {
                                break;
                            } else if (clickableSpan.equals((ClickableSpan) ((WeakReference) sparseArray2.valueAt(i6)).get())) {
                                i = sparseArray2.keyAt(i6);
                                break;
                            } else {
                                i6++;
                            }
                        }
                        sparseArray2.put(i, new WeakReference(clickableSpanArr[i5]));
                        ClickableSpan clickableSpan2 = clickableSpanArr[i5];
                        Spanned spanned = (Spanned) charSequence;
                        A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(Integer.valueOf(spanned.getSpanStart(clickableSpan2)));
                        A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(Integer.valueOf(spanned.getSpanEnd(clickableSpan2)));
                        A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(Integer.valueOf(spanned.getSpanFlags(clickableSpan2)));
                        A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(Integer.valueOf(i));
                    }
                    i = A03;
                    A03 = i + 1;
                    sparseArray2.put(i, new WeakReference(clickableSpanArr[i5]));
                    ClickableSpan clickableSpan22 = clickableSpanArr[i5];
                    Spanned spanned2 = (Spanned) charSequence;
                    A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(Integer.valueOf(spanned2.getSpanStart(clickableSpan22)));
                    A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(Integer.valueOf(spanned2.getSpanEnd(clickableSpan22)));
                    A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(Integer.valueOf(spanned2.getSpanFlags(clickableSpan22)));
                    A03("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(Integer.valueOf(i));
                }
            }
        }
    }

    public void A0P(Object obj) {
        AccessibilityNodeInfo.CollectionInfo collectionInfo;
        if (Build.VERSION.SDK_INT >= 19) {
            AccessibilityNodeInfo accessibilityNodeInfo = this.A02;
            if (obj == null) {
                collectionInfo = null;
            } else {
                collectionInfo = (AccessibilityNodeInfo.CollectionInfo) ((AnonymousClass8NI) obj).A00;
            }
            accessibilityNodeInfo.setCollectionInfo(collectionInfo);
        }
    }

    public void A0Q(Object obj) {
        AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo;
        if (Build.VERSION.SDK_INT >= 19) {
            AccessibilityNodeInfo accessibilityNodeInfo = this.A02;
            if (obj == null) {
                collectionItemInfo = null;
            } else {
                collectionItemInfo = (AccessibilityNodeInfo.CollectionItemInfo) ((AnonymousClass4CJ) obj).A00;
            }
            accessibilityNodeInfo.setCollectionItemInfo(collectionItemInfo);
        }
    }

    public void A0R(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.A02.setAccessibilityFocused(z);
        }
    }

    public void A0S(boolean z) {
        this.A02.setClickable(z);
    }

    public void A0T(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.A02.setContentInvalid(z);
        }
    }

    public void A0U(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.A02.setDismissable(z);
        }
    }

    public void A0V(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.A02.setHeading(z);
        } else {
            A04(2, z);
        }
    }

    public void A0W(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.A02.setMultiLine(z);
        }
    }

    public void A0X(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.A02.setScreenReaderFocusable(z);
        } else {
            A04(1, z);
        }
    }

    public void A0Y(boolean z) {
        if (Build.VERSION.SDK_INT >= 26) {
            this.A02.setShowingHintText(z);
        } else {
            A04(4, z);
        }
    }

    public void A0Z(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.A02.setVisibleToUser(z);
        }
    }

    public boolean A0a() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.A02.isAccessibilityFocused();
        }
        return false;
    }

    public boolean A0b() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.A02.isVisibleToUser();
        }
        return false;
    }

    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.A02;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        this.A02.getBoundsInParent(rect);
        sb.append("; boundsInParent: " + rect);
        this.A02.getBoundsInScreen(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(this.A02.getPackageName());
        sb.append("; className: ");
        sb.append(this.A02.getClassName());
        sb.append("; text: ");
        sb.append(A06());
        sb.append("; contentDescription: ");
        sb.append(this.A02.getContentDescription());
        sb.append("; viewId: ");
        if (Build.VERSION.SDK_INT >= 18) {
            str = this.A02.getViewIdResourceName();
        } else {
            str = null;
        }
        sb.append(str);
        sb.append("; checkable: ");
        sb.append(this.A02.isCheckable());
        sb.append("; checked: ");
        sb.append(this.A02.isChecked());
        sb.append("; focusable: ");
        sb.append(this.A02.isFocusable());
        sb.append("; focused: ");
        sb.append(this.A02.isFocused());
        sb.append("; selected: ");
        sb.append(this.A02.isSelected());
        sb.append("; clickable: ");
        sb.append(this.A02.isClickable());
        sb.append("; longClickable: ");
        sb.append(this.A02.isLongClickable());
        sb.append("; enabled: ");
        sb.append(this.A02.isEnabled());
        sb.append("; password: ");
        sb.append(this.A02.isPassword());
        sb.append(AnonymousClass08S.A0X("; scrollable: ", this.A02.isScrollable()));
        sb.append("; [");
        if (Build.VERSION.SDK_INT >= 21) {
            List A07 = A07();
            for (int i = 0; i < A07.size(); i++) {
                C25973Cpq cpq = (C25973Cpq) A07.get(i);
                String A022 = A02(cpq.A00());
                if (A022.equals("ACTION_UNKNOWN") && cpq.A01() != null) {
                    A022 = cpq.A01().toString();
                }
                sb.append(A022);
                if (i != A07.size() - 1) {
                    sb.append(", ");
                }
            }
        } else {
            int actions = this.A02.getActions();
            while (actions != 0) {
                int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(actions);
                actions &= numberOfTrailingZeros ^ -1;
                sb.append(A02(numberOfTrailingZeros));
                if (actions != 0) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public AccessibilityNodeInfoCompat(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.A02 = accessibilityNodeInfo;
    }

    public static AccessibilityNodeInfoCompat A01() {
        return new AccessibilityNodeInfoCompat(AccessibilityNodeInfo.obtain());
    }

    private void A04(int i, boolean z) {
        Bundle A002 = A00();
        if (A002 != null) {
            int i2 = A002.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & (i ^ -1);
            if (!z) {
                i = 0;
            }
            A002.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i | i2);
        }
    }
}
