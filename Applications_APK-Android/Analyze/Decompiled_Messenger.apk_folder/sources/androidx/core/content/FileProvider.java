package androidx.core.content;

import X.AnonymousClass01R;
import X.AnonymousClass08S;
import X.AnonymousClass0T8;
import X.AnonymousClass0T9;
import X.AnonymousClass24B;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParserException;

public class FileProvider extends ContentProvider {
    private static HashMap A01 = new HashMap();
    public static final File A02 = new File("/");
    private static final String[] A03 = {"_display_name", "_size"};
    private AnonymousClass0T9 A00;

    public boolean onCreate() {
        return true;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static AnonymousClass0T9 A00(Context context, String str) {
        AnonymousClass0T8 r6;
        IllegalArgumentException illegalArgumentException;
        IllegalArgumentException illegalArgumentException2;
        File file;
        synchronized (A01) {
            AnonymousClass0T9 r62 = (AnonymousClass0T9) A01.get(str);
            r6 = r62;
            if (r62 == null) {
                try {
                    AnonymousClass0T8 r63 = new AnonymousClass0T8(str);
                    ProviderInfo resolveContentProvider = context.getPackageManager().resolveContentProvider(str, 128);
                    if (resolveContentProvider != null) {
                        XmlResourceParser loadXmlMetaData = resolveContentProvider.loadXmlMetaData(context.getPackageManager(), "android.support.FILE_PROVIDER_PATHS");
                        if (loadXmlMetaData != null) {
                            while (true) {
                                int next = loadXmlMetaData.next();
                                if (next == 1) {
                                    A01.put(str, r63);
                                    r6 = r63;
                                    break;
                                } else if (next == 2) {
                                    String name = loadXmlMetaData.getName();
                                    file = null;
                                    String attributeValue = loadXmlMetaData.getAttributeValue(null, "name");
                                    String attributeValue2 = loadXmlMetaData.getAttributeValue(null, "path");
                                    if ("root-path".equals(name)) {
                                        file = A02;
                                    } else if ("files-path".equals(name)) {
                                        file = context.getFilesDir();
                                    } else if ("cache-path".equals(name)) {
                                        file = context.getCacheDir();
                                    } else if ("external-path".equals(name)) {
                                        file = Environment.getExternalStorageDirectory();
                                    } else if ("external-files-path".equals(name)) {
                                        File[] A08 = AnonymousClass01R.A08(context, null);
                                        if (A08.length > 0) {
                                            file = A08[0];
                                        }
                                    } else if ("external-cache-path".equals(name)) {
                                        File[] A07 = AnonymousClass01R.A07(context);
                                        if (A07.length > 0) {
                                            file = A07[0];
                                        }
                                    } else if (Build.VERSION.SDK_INT >= 21 && "external-media-path".equals(name)) {
                                        File[] externalMediaDirs = context.getExternalMediaDirs();
                                        if (externalMediaDirs.length > 0) {
                                            file = externalMediaDirs[0];
                                        }
                                    }
                                    if (file != null) {
                                        String[] strArr = {attributeValue2};
                                        for (int i = 0; i < 1; i++) {
                                            String str2 = strArr[i];
                                            if (str2 != null) {
                                                file = new File(file, str2);
                                            }
                                        }
                                        if (TextUtils.isEmpty(attributeValue)) {
                                            illegalArgumentException2 = new IllegalArgumentException("Name must not be empty");
                                            break;
                                        }
                                        r63.A00.put(attributeValue, file.getCanonicalFile());
                                    } else {
                                        continue;
                                    }
                                }
                            }
                        } else {
                            illegalArgumentException2 = new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
                        }
                    } else {
                        illegalArgumentException2 = new IllegalArgumentException(AnonymousClass08S.A0J("Couldn't find meta-data for provider with authority ", str));
                    }
                    throw illegalArgumentException2;
                } catch (IOException e) {
                    throw new IllegalArgumentException("Failed to resolve canonical path for " + file, e);
                } catch (IOException e2) {
                    illegalArgumentException = new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e2);
                    throw illegalArgumentException;
                } catch (XmlPullParserException e3) {
                    illegalArgumentException = new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e3);
                    throw illegalArgumentException;
                }
            }
        }
        return r6;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return this.A00.Amn(uri).delete() ? 1 : 0;
    }

    public String getType(Uri uri) {
        String mimeTypeFromExtension;
        File Amn = this.A00.Amn(uri);
        int lastIndexOf = Amn.getName().lastIndexOf(46);
        if (lastIndexOf < 0 || (mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(Amn.getName().substring(lastIndexOf + 1))) == null) {
            return AnonymousClass24B.$const$string(8);
        }
        return mimeTypeFromExtension;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException("No external inserts");
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) {
        int i;
        File Amn = this.A00.Amn(uri);
        if ("r".equals(str)) {
            i = 268435456;
        } else if ("w".equals(str) || "wt".equals(str)) {
            i = 738197504;
        } else if ("wa".equals(str)) {
            i = 704643072;
        } else if ("rw".equals(str)) {
            i = 939524096;
        } else if ("rwt".equals(str)) {
            i = 1006632960;
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0J("Invalid mode: ", str));
        }
        return ParcelFileDescriptor.open(Amn, i);
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        int i;
        File Amn = this.A00.Amn(uri);
        if (strArr == null) {
            strArr = A03;
        }
        String[] strArr3 = new String[r7];
        Object[] objArr = new Object[r7];
        int i2 = 0;
        for (String str3 : strArr) {
            if ("_display_name".equals(str3)) {
                strArr3[i2] = "_display_name";
                i = i2 + 1;
                objArr[i2] = Amn.getName();
            } else if ("_size".equals(str3)) {
                strArr3[i2] = "_size";
                i = i2 + 1;
                objArr[i2] = Long.valueOf(Amn.length());
            }
            i2 = i;
        }
        String[] strArr4 = new String[i2];
        System.arraycopy(strArr3, 0, strArr4, 0, i2);
        Object[] objArr2 = new Object[i2];
        System.arraycopy(objArr, 0, objArr2, 0, i2);
        MatrixCursor matrixCursor = new MatrixCursor(strArr4, 1);
        matrixCursor.addRow(objArr2);
        return matrixCursor;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException("No external updates");
    }

    public void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
        if (providerInfo.exported) {
            throw new SecurityException("Provider must not be exported");
        } else if (providerInfo.grantUriPermissions) {
            this.A00 = A00(context, providerInfo.authority);
        } else {
            throw new SecurityException("Provider must grant uri permissions");
        }
    }
}
