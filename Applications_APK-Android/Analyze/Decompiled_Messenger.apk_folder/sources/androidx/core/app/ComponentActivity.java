package androidx.core.app;

import X.AnonymousClass0n5;
import X.AnonymousClass0qM;
import X.AnonymousClass1XL;
import X.C000700l;
import X.C11410n6;
import X.C12990qL;
import X.C14400tF;
import X.C15320v6;
import X.C50002dD;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class ComponentActivity extends Activity implements C11410n6, AnonymousClass0n5 {
    private C12990qL A00 = new C12990qL(this);

    public AnonymousClass0qM AsK() {
        if (!(this instanceof androidx.activity.ComponentActivity)) {
            return this.A00;
        }
        return ((androidx.activity.ComponentActivity) this).A01;
    }

    public void onSaveInstanceState(Bundle bundle) {
        C12990qL.A04(this.A00, AnonymousClass1XL.CREATED);
        super.onSaveInstanceState(bundle);
    }

    public boolean CIV(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        View decorView = getWindow().getDecorView();
        if (decorView == null || !C15320v6.dispatchUnhandledKeyEventBeforeHierarchy(decorView, keyEvent)) {
            return C50002dD.A00(this, decorView, this, keyEvent);
        }
        return true;
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        View decorView = getWindow().getDecorView();
        if (decorView == null || !C15320v6.dispatchUnhandledKeyEventBeforeHierarchy(decorView, keyEvent)) {
            return super.dispatchKeyShortcutEvent(keyEvent);
        }
        return true;
    }

    public void onCreate(Bundle bundle) {
        int A002 = C000700l.A00(-1405646941);
        super.onCreate(bundle);
        C14400tF.A00(this);
        C000700l.A07(1408521919, A002);
    }
}
