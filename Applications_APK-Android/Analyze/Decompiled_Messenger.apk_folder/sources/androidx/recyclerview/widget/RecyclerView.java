package androidx.recyclerview.widget;

import X.AnonymousClass06K;
import X.AnonymousClass08S;
import X.AnonymousClass19T;
import X.AnonymousClass1A9;
import X.AnonymousClass1R1;
import X.AnonymousClass1R7;
import X.AnonymousClass1Y3;
import X.AnonymousClass2S2;
import X.C000700l;
import X.C15210uu;
import X.C15270v0;
import X.C15320v6;
import X.C15680vg;
import X.C15700vi;
import X.C15710vl;
import X.C15740vp;
import X.C15900wA;
import X.C15920wC;
import X.C15930wD;
import X.C16090wT;
import X.C16120wW;
import X.C16150wZ;
import X.C16220wh;
import X.C16250wk;
import X.C16260wl;
import X.C20221Bj;
import X.C20831Dz;
import X.C20931Ej;
import X.C21731Im;
import X.C24950CQi;
import X.C26821DCx;
import X.C32911mX;
import X.C33781o8;
import X.C37121uj;
import X.C37261ux;
import X.C37491vl;
import X.C60862xx;
import X.C61512z0;
import X.C621730h;
import X.C621830i;
import X.DDW;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import androidx.customview.view.AbsSavedState;
import com.facebook.common.dextricks.DexStore;
import io.card.payment.BuildConfig;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class RecyclerView extends ViewGroup implements C15700vi, C15210uu {
    public static final boolean A18;
    public static final boolean A19;
    public static final boolean A1A;
    public static final Interpolator A1B = new C20931Ej();
    public static final boolean A1C;
    public static final boolean A1D;
    public static final Class[] A1E;
    private static final boolean A1F;
    private static final int[] A1G = {16842987};
    private static final int[] A1H = {16843830};
    public C37261ux A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public int A09;
    public int A0A;
    public int A0B;
    public EdgeEffect A0C;
    public EdgeEffect A0D;
    public EdgeEffect A0E;
    public EdgeEffect A0F;
    public C16150wZ A0G;
    public C16220wh A0H;
    public C37121uj A0I;
    public C20831Dz A0J;
    public C20221Bj A0K;
    public AnonymousClass19T A0L;
    public DDW A0M;
    public C15710vl A0N;
    public AnonymousClass1A9 A0O;
    public C26821DCx A0P;
    public C16260wl A0Q;
    public List A0R;
    public List A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    private float A0Y;
    private float A0Z;
    private int A0a;
    private int A0b;
    private int A0c;
    private int A0d;
    private int A0e;
    private int A0f;
    private int A0g;
    private int A0h;
    private VelocityTracker A0i;
    private C15270v0 A0j;
    private C61512z0 A0k;
    private C16090wT A0l;
    private SavedState A0m;
    private Runnable A0n;
    private boolean A0o;
    public final RectF A0p;
    public final Runnable A0q;
    public final int[] A0r;
    public final int A0s;
    public final Rect A0t;
    public final Rect A0u;
    public final AccessibilityManager A0v;
    public final C15740vp A0w;
    public final C32911mX A0x;
    public final C15930wD A0y;
    public final C16120wW A0z;
    public final C15900wA A10;
    public final ArrayList A11;
    public final ArrayList A12;
    public final List A13;
    public final int[] A14;
    private final int A15;
    private final int[] A16;
    private final int[] A17;
    public C15920wC mViewFlinger;

    private void A0G(View view, View view2) {
        View view3 = view;
        View view4 = view;
        if (view2 != null) {
            view4 = view2;
        }
        this.A0t.set(0, 0, view4.getWidth(), view4.getHeight());
        ViewGroup.LayoutParams layoutParams = view4.getLayoutParams();
        if (layoutParams instanceof AnonymousClass1R7) {
            AnonymousClass1R7 r1 = (AnonymousClass1R7) layoutParams;
            if (!r1.A01) {
                Rect rect = r1.A02;
                Rect rect2 = this.A0t;
                rect2.left -= rect.left;
                rect2.right += rect.right;
                rect2.top -= rect.top;
                rect2.bottom += rect.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.A0t);
            offsetRectIntoDescendantCoords(view, this.A0t);
        }
        AnonymousClass19T r2 = this.A0L;
        Rect rect3 = this.A0t;
        boolean z = !this.A03;
        boolean z2 = false;
        if (view2 == null) {
            z2 = true;
        }
        r2.A1a(this, view3, rect3, z, z2);
    }

    public void A0q(int i, int i2) {
        A0r(i, i2, null);
    }

    public void A0v(C20831Dz r7) {
        A1A(false);
        C20831Dz r1 = this.A0J;
        if (r1 != null) {
            r1.CJm(this.A0x);
            this.A0J.BW8(this);
        }
        A0g();
        C16150wZ r12 = this.A0G;
        C16150wZ.A05(r12, r12.A05);
        C16150wZ.A05(r12, r12.A06);
        r12.A00 = 0;
        C20831Dz r5 = this.A0J;
        this.A0J = r7;
        if (r7 != null) {
            r7.C0O(this.A0x);
            r7.BOc(this);
        }
        C15740vp r13 = this.A0w;
        C20831Dz r4 = this.A0J;
        r13.A04.clear();
        C15740vp.A01(r13);
        if (r13.A02 == null) {
            r13.A02 = new C15680vg();
        }
        C15680vg r3 = r13.A02;
        if (r5 != null) {
            r3.A00--;
        }
        if (r3.A00 == 0) {
            for (int i = 0; i < r3.A01.size(); i++) {
                ((AnonymousClass1R1) r3.A01.valueAt(i)).A02.clear();
            }
        }
        if (r4 != null) {
            r3.A00++;
        }
        this.A0y.A0C = true;
        A19(false);
        requestLayout();
    }

    public void A16(C33781o8 r5, C60862xx r6) {
        long j;
        int i = (0 & DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED) | (r5.A00 & (8192 ^ -1));
        r5.A00 = i;
        if (this.A0y.A0D) {
            boolean z = false;
            if ((i & 2) != 0) {
                z = true;
            }
            if (z && !r5.A0E() && !r5.A0F()) {
                if (this.A0J.hasStableIds()) {
                    j = r5.A07;
                } else {
                    j = (long) r5.A04;
                }
                this.A10.A01.A0D(j, r5);
            }
        }
        this.A10.A04(r5, r6);
    }

    public final class SavedState extends AbsSavedState {
        public static final Parcelable.Creator CREATOR = new AnonymousClass2S2();
        public Parcelable A00;

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.A00, 0);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.A00 = parcel.readParcelable(classLoader == null ? AnonymousClass19T.class.getClassLoader() : classLoader);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public static C15270v0 A04(RecyclerView recyclerView) {
        if (recyclerView.A0j == null) {
            recyclerView.A0j = new C15270v0(recyclerView);
        }
        return recyclerView.A0j;
    }

    public static C33781o8 A05(View view) {
        if (view == null) {
            return null;
        }
        return ((AnonymousClass1R7) view.getLayoutParams()).mViewHolder;
    }

    public static RecyclerView A06(View view) {
        if (view instanceof ViewGroup) {
            if (view instanceof RecyclerView) {
                return (RecyclerView) view;
            }
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                RecyclerView A062 = A06(viewGroup.getChildAt(i));
                if (A062 != null) {
                    return A062;
                }
            }
        }
        return null;
    }

    private void A07() {
        int Ah4 = this.A0H.A01.Ah4();
        for (int i = 0; i < Ah4; i++) {
            C33781o8 A052 = A05(this.A0H.A01.Ah1(i));
            if (!A052.A0F()) {
                A052.A02 = -1;
                A052.A05 = -1;
            }
        }
        C15740vp r5 = this.A0w;
        int size = r5.A05.size();
        for (int i2 = 0; i2 < size; i2++) {
            C33781o8 r1 = (C33781o8) r5.A05.get(i2);
            r1.A02 = -1;
            r1.A05 = -1;
        }
        int size2 = r5.A04.size();
        for (int i3 = 0; i3 < size2; i3++) {
            C33781o8 r12 = (C33781o8) r5.A04.get(i3);
            r12.A02 = -1;
            r12.A05 = -1;
        }
        ArrayList arrayList = r5.A03;
        if (arrayList != null) {
            int size3 = arrayList.size();
            for (int i4 = 0; i4 < size3; i4++) {
                C33781o8 r13 = (C33781o8) r5.A03.get(i4);
                r13.A02 = -1;
                r13.A05 = -1;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:145:0x02f9, code lost:
        if (r14.A0H.A02.contains(r1) == false) goto L_0x02bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x0364, code lost:
        if (r1.A0H.hasFocusable() != false) goto L_0x03a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x03a7, code lost:
        r9 = r1.A0H;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0185, code lost:
        if (r1.A05.isEmpty() != false) goto L_0x0187;
     */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0255  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0298  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x02d1  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0307  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0352  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x03ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A08() {
        /*
            r14 = this;
            X.1Dz r0 = r14.A0J
            java.lang.String r1 = "RecyclerView"
            if (r0 != 0) goto L_0x000c
            java.lang.String r0 = "No adapter attached; skipping layout"
        L_0x0008:
            android.util.Log.e(r1, r0)
            return
        L_0x000c:
            X.19T r0 = r14.A0L
            if (r0 != 0) goto L_0x0013
            java.lang.String r0 = "No layout manager attached; skipping layout"
            goto L_0x0008
        L_0x0013:
            X.0wD r1 = r14.A0y
            r0 = 0
            r1.A09 = r0
            int r1 = r1.A04
            r0 = 1
            if (r1 != r0) goto L_0x0174
            r14.A09()
        L_0x0020:
            X.19T r0 = r14.A0L
            r0.A1J(r14)
            r14.A0A()
        L_0x0028:
            X.0wD r1 = r14.A0y
            r0 = 4
            r1.A01(r0)
            A0M(r14)
            int r0 = r14.A0A
            int r0 = r0 + 1
            r14.A0A = r0
            r2 = 1
            r1.A04 = r2
            boolean r0 = r1.A0B
            if (r0 == 0) goto L_0x0235
            X.0wh r0 = r14.A0H
            int r4 = r0.A02()
            int r4 = r4 - r2
        L_0x0045:
            if (r4 < 0) goto L_0x01c6
            X.0wh r0 = r14.A0H
            android.view.View r0 = r0.A03(r4)
            X.1o8 r5 = A05(r0)
            boolean r0 = r5.A0F()
            if (r0 != 0) goto L_0x00a6
            X.1Dz r0 = r14.A0J
            boolean r0 = r0.hasStableIds()
            if (r0 == 0) goto L_0x016f
            long r6 = r5.A07
        L_0x0061:
            X.2xx r3 = new X.2xx
            r3.<init>()
            r3.A00(r5)
            X.0wA r9 = r14.A10
            X.0zB r8 = r9.A01
            java.lang.Object r8 = r8.A07(r6)
            X.1o8 r8 = (X.C33781o8) r8
            if (r8 == 0) goto L_0x016a
            boolean r0 = r8.A0F()
            if (r0 != 0) goto L_0x016a
            X.04a r0 = r9.A00
            java.lang.Object r0 = r0.get(r8)
            X.1vl r0 = (X.C37491vl) r0
            r12 = 1
            if (r0 == 0) goto L_0x0167
            int r0 = r0.A00
            r0 = r0 & r2
            if (r0 == 0) goto L_0x0167
        L_0x008b:
            X.0wA r0 = r14.A10
            X.04a r0 = r0.A00
            java.lang.Object r0 = r0.get(r5)
            X.1vl r0 = (X.C37491vl) r0
            r11 = 1
            if (r0 == 0) goto L_0x0164
            int r0 = r0.A00
            r0 = r0 & r2
            if (r0 == 0) goto L_0x0164
        L_0x009d:
            if (r12 == 0) goto L_0x00a9
            if (r8 != r5) goto L_0x00a9
            X.0wA r0 = r14.A10
            r0.A05(r5, r3)
        L_0x00a6:
            int r4 = r4 + -1
            goto L_0x0045
        L_0x00a9:
            X.0wA r1 = r14.A10
            r0 = 4
            X.2xx r10 = X.C15900wA.A00(r1, r8, r0)
            X.0wA r0 = r14.A10
            r0.A05(r5, r3)
            X.0wA r1 = r14.A10
            r0 = 8
            X.2xx r9 = X.C15900wA.A00(r1, r5, r0)
            if (r10 != 0) goto L_0x0138
            X.0wh r0 = r14.A0H
            int r12 = r0.A02()
            r11 = 0
        L_0x00c6:
            if (r11 >= r12) goto L_0x0114
            X.0wh r0 = r14.A0H
            android.view.View r0 = r0.A03(r11)
            X.1o8 r3 = A05(r0)
            if (r3 == r5) goto L_0x0111
            X.1Dz r10 = r14.A0J
            boolean r0 = r10.hasStableIds()
            if (r0 == 0) goto L_0x010d
            long r0 = r3.A07
        L_0x00de:
            int r9 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r9 != 0) goto L_0x0111
            java.lang.String r4 = " \n View Holder 2:"
            if (r10 == 0) goto L_0x01a5
            boolean r0 = r10.hasStableIds()
            if (r0 == 0) goto L_0x01a5
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:"
            r1.<init>(r0)
            r1.append(r3)
            r1.append(r4)
            r1.append(r5)
            java.lang.String r0 = r14.A0d()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x010d:
            int r0 = r3.A04
            long r0 = (long) r0
            goto L_0x00de
        L_0x0111:
            int r11 = r11 + 1
            goto L_0x00c6
        L_0x0114:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Problem while matching changed view holders with the newones. The pre-layout information for the change holder "
            r1.<init>(r0)
            r1.append(r8)
            java.lang.String r0 = " cannot be found but it is necessary for "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r14.A0d()
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "RecyclerView"
            android.util.Log.e(r0, r1)
            goto L_0x00a6
        L_0x0138:
            r1 = 0
            r8.A0A(r1)
            if (r12 == 0) goto L_0x0141
            A0R(r14, r8)
        L_0x0141:
            if (r8 == r5) goto L_0x0157
            if (r11 == 0) goto L_0x0148
            A0R(r14, r5)
        L_0x0148:
            r8.A09 = r5
            A0R(r14, r8)
            X.0vp r0 = r14.A0w
            r0.A0A(r8)
            r5.A0A(r1)
            r5.A0A = r8
        L_0x0157:
            X.1Bj r0 = r14.A0K
            boolean r0 = r0.A0I(r8, r5, r10, r9)
            if (r0 == 0) goto L_0x00a6
            r14.A0f()
            goto L_0x00a6
        L_0x0164:
            r11 = 0
            goto L_0x009d
        L_0x0167:
            r12 = 0
            goto L_0x008b
        L_0x016a:
            r9.A05(r5, r3)
            goto L_0x00a6
        L_0x016f:
            int r0 = r5.A04
            long r6 = (long) r0
            goto L_0x0061
        L_0x0174:
            X.0wZ r1 = r14.A0G
            java.util.ArrayList r0 = r1.A06
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0187
            java.util.ArrayList r0 = r1.A05
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x0188
        L_0x0187:
            r0 = 0
        L_0x0188:
            if (r0 != 0) goto L_0x0020
            X.19T r0 = r14.A0L
            int r1 = r0.A04
            int r0 = r14.getWidth()
            if (r1 != r0) goto L_0x0020
            X.19T r0 = r14.A0L
            int r1 = r0.A01
            int r0 = r14.getHeight()
            if (r1 != r0) goto L_0x0020
            X.19T r0 = r14.A0L
            r0.A1J(r14)
            goto L_0x0028
        L_0x01a5:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:"
            r1.<init>(r0)
            r1.append(r3)
            r1.append(r4)
            r1.append(r5)
            java.lang.String r0 = r14.A0d()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x01c6:
            X.0wA r7 = r14.A10
            X.0wW r6 = r14.A0z
            X.04a r0 = r7.A00
            int r0 = r0.size()
            int r5 = r0 + -1
        L_0x01d2:
            if (r5 < 0) goto L_0x0235
            X.04a r0 = r7.A00
            java.lang.Object r4 = r0.A07(r5)
            X.1o8 r4 = (X.C33781o8) r4
            java.lang.Object r3 = r0.A08(r5)
            X.1vl r3 = (X.C37491vl) r3
            int r8 = r3.A00
            r1 = 3
            r0 = r8 & r1
            if (r0 == r1) goto L_0x0231
            r0 = r8 & 1
            if (r0 == 0) goto L_0x0206
            X.2xx r1 = r3.A02
            if (r1 == 0) goto L_0x0231
            X.2xx r0 = r3.A01
            r6.ByH(r4, r1, r0)
        L_0x01f6:
            r0 = 0
            r3.A00 = r0
            r0 = 0
            r3.A02 = r0
            r3.A01 = r0
            X.0bP r0 = X.C37491vl.A03
            r0.C0w(r3)
            int r5 = r5 + -1
            goto L_0x01d2
        L_0x0206:
            r1 = 14
            r0 = r8 & r1
            if (r0 == r1) goto L_0x0229
            r1 = 12
            r0 = r8 & r1
            if (r0 != r1) goto L_0x021a
            X.2xx r1 = r3.A02
            X.2xx r0 = r3.A01
            r6.ByN(r4, r1, r0)
            goto L_0x01f6
        L_0x021a:
            r0 = r8 & 4
            if (r0 == 0) goto L_0x0225
            X.2xx r1 = r3.A02
            r0 = 0
            r6.ByH(r4, r1, r0)
            goto L_0x01f6
        L_0x0225:
            r0 = r8 & 8
            if (r0 == 0) goto L_0x01f6
        L_0x0229:
            X.2xx r1 = r3.A02
            X.2xx r0 = r3.A01
            r6.ByG(r4, r1, r0)
            goto L_0x01f6
        L_0x0231:
            r6.CK5(r4)
            goto L_0x01f6
        L_0x0235:
            X.19T r1 = r14.A0L
            X.0vp r0 = r14.A0w
            r1.A1G(r0)
            X.0wD r1 = r14.A0y
            int r0 = r1.A03
            r1.A05 = r0
            r6 = 0
            r14.A0T = r6
            r14.A0U = r6
            r1.A0B = r6
            r1.A0A = r6
            X.19T r0 = r14.A0L
            r0.A0F = r6
            X.0vp r0 = r14.A0w
            java.util.ArrayList r0 = r0.A03
            if (r0 == 0) goto L_0x0258
            r0.clear()
        L_0x0258:
            X.19T r1 = r14.A0L
            boolean r0 = r1.A0E
            if (r0 == 0) goto L_0x0267
            r1.A03 = r6
            r1.A0E = r6
            X.0vp r0 = r14.A0w
            r0.A06()
        L_0x0267:
            X.19T r1 = r14.A0L
            X.0wD r0 = r14.A0y
            r1.A1o(r0)
            r5 = 1
            r14.A18(r2)
            A0S(r14, r6)
            X.0wA r1 = r14.A10
            X.04a r0 = r1.A00
            r0.clear()
            X.0zB r0 = r1.A01
            r0.A09()
            int[] r1 = r14.A14
            r4 = r1[r6]
            r3 = r1[r2]
            r14.A0T(r1)
            int[] r2 = r14.A14
            r1 = 0
            r0 = r2[r6]
            if (r0 != r4) goto L_0x0295
            r0 = r2[r5]
            if (r0 == r3) goto L_0x0296
        L_0x0295:
            r1 = 1
        L_0x0296:
            if (r1 == 0) goto L_0x029b
            r14.A0p(r6, r6)
        L_0x029b:
            boolean r0 = r14.A0X
            if (r0 == 0) goto L_0x02bf
            X.1Dz r0 = r14.A0J
            if (r0 == 0) goto L_0x02bf
            boolean r0 = r14.hasFocus()
            if (r0 == 0) goto L_0x02bf
            int r1 = r14.getDescendantFocusability()
            r0 = 393216(0x60000, float:5.51013E-40)
            if (r1 == r0) goto L_0x02bf
            int r1 = r14.getDescendantFocusability()
            r0 = 131072(0x20000, float:1.83671E-40)
            if (r1 != r0) goto L_0x02cb
            boolean r0 = r14.isFocused()
            if (r0 == 0) goto L_0x02cb
        L_0x02bf:
            X.0wD r2 = r14.A0y
            r0 = -1
            r2.A07 = r0
            r0 = -1
            r2.A01 = r0
            r2.A02 = r0
            return
        L_0x02cb:
            boolean r0 = r14.isFocused()
            if (r0 != 0) goto L_0x02fc
            android.view.View r1 = r14.getFocusedChild()
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1D
            if (r0 == 0) goto L_0x02f1
            android.view.ViewParent r0 = r1.getParent()
            if (r0 == 0) goto L_0x02e5
            boolean r0 = r1.hasFocus()
            if (r0 != 0) goto L_0x02f1
        L_0x02e5:
            X.0wh r0 = r14.A0H
            int r0 = r0.A02()
            if (r0 != 0) goto L_0x02fc
            r14.requestFocus()
            goto L_0x02bf
        L_0x02f1:
            X.0wh r0 = r14.A0H
            java.util.List r0 = r0.A02
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x02fc
            goto L_0x02bf
        L_0x02fc:
            X.0wD r0 = r14.A0y
            long r2 = r0.A07
            r10 = -1
            r9 = 0
            int r0 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r0 == 0) goto L_0x034d
            X.1Dz r4 = r14.A0J
            boolean r0 = r4.hasStableIds()
            if (r0 == 0) goto L_0x034d
            r1 = 0
            if (r4 == 0) goto L_0x0350
            boolean r0 = r4.hasStableIds()
            if (r0 == 0) goto L_0x0350
            X.0wh r0 = r14.A0H
            X.0wj r0 = r0.A01
            int r8 = r0.Ah4()
            r7 = 0
        L_0x0321:
            if (r7 >= r8) goto L_0x0350
            X.0wh r0 = r14.A0H
            X.0wj r0 = r0.A01
            android.view.View r0 = r0.Ah1(r7)
            X.1o8 r6 = A05(r0)
            if (r6 == 0) goto L_0x034a
            boolean r0 = r6.A0E()
            if (r0 != 0) goto L_0x034a
            long r4 = r6.A07
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x034a
            X.0wh r0 = r14.A0H
            android.view.View r1 = r6.A0H
            java.util.List r0 = r0.A02
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x034f
            r1 = r6
        L_0x034a:
            int r7 = r7 + 1
            goto L_0x0321
        L_0x034d:
            r1 = r9
            goto L_0x0350
        L_0x034f:
            r1 = r6
        L_0x0350:
            if (r1 == 0) goto L_0x0366
            X.0wh r0 = r14.A0H
            android.view.View r2 = r1.A0H
            java.util.List r0 = r0.A02
            boolean r0 = r0.contains(r2)
            if (r0 != 0) goto L_0x0366
            android.view.View r0 = r1.A0H
            boolean r0 = r0.hasFocusable()
            if (r0 != 0) goto L_0x03a7
        L_0x0366:
            X.0wh r0 = r14.A0H
            int r0 = r0.A02()
            if (r0 <= 0) goto L_0x03a9
            X.0wD r1 = r14.A0y
            int r4 = r1.A01
            r0 = -1
            if (r4 != r0) goto L_0x0376
            r4 = 0
        L_0x0376:
            int r3 = r1.A00()
            r2 = r4
        L_0x037b:
            if (r2 >= r3) goto L_0x038e
            X.1o8 r1 = r14.A0b(r2)
            if (r1 == 0) goto L_0x038e
            android.view.View r0 = r1.A0H
            boolean r0 = r0.hasFocusable()
            if (r0 != 0) goto L_0x03a7
            int r2 = r2 + 1
            goto L_0x037b
        L_0x038e:
            int r0 = java.lang.Math.min(r3, r4)
            int r2 = r0 + -1
        L_0x0394:
            if (r2 < 0) goto L_0x03a9
            X.1o8 r1 = r14.A0b(r2)
            if (r1 == 0) goto L_0x03a9
            android.view.View r0 = r1.A0H
            boolean r0 = r0.hasFocusable()
            if (r0 != 0) goto L_0x03a7
            int r2 = r2 + -1
            goto L_0x0394
        L_0x03a7:
            android.view.View r9 = r1.A0H
        L_0x03a9:
            if (r9 == 0) goto L_0x02bf
            X.0wD r0 = r14.A0y
            int r3 = r0.A02
            long r1 = (long) r3
            int r0 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r0 == 0) goto L_0x03c5
            android.view.View r1 = r9.findViewById(r3)
            if (r1 == 0) goto L_0x03c5
            boolean r0 = r1.isFocusable()
            if (r0 == 0) goto L_0x03c5
        L_0x03c0:
            r1.requestFocus()
            goto L_0x02bf
        L_0x03c5:
            r1 = r9
            goto L_0x03c0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A08():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01c4, code lost:
        if (r1 == 0) goto L_0x01c6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A09() {
        /*
            r7 = this;
            X.0wD r0 = r7.A0y
            r6 = 1
            r0.A01(r6)
            int r1 = r7.A0B
            r0 = 2
            if (r1 != r0) goto L_0x001b
            X.0wC r0 = r7.mViewFlinger
            android.widget.OverScroller r0 = r0.A03
            r0.getFinalX()
            r0.getCurrX()
            r0.getFinalY()
            r0.getCurrY()
        L_0x001b:
            X.0wD r0 = r7.A0y
            r4 = 0
            r0.A09 = r4
            A0M(r7)
            X.0wA r1 = r7.A10
            X.04a r0 = r1.A00
            r0.clear()
            X.0zB r0 = r1.A01
            r0.A09()
            int r0 = r7.A0A
            int r0 = r0 + 1
            r7.A0A = r0
            r7.A0C()
            boolean r0 = r7.A0X
            r3 = 0
            if (r0 == 0) goto L_0x0157
            boolean r0 = r7.hasFocus()
            if (r0 == 0) goto L_0x0157
            X.1Dz r0 = r7.A0J
            if (r0 == 0) goto L_0x0157
            android.view.View r0 = r7.getFocusedChild()
        L_0x004b:
            if (r0 == 0) goto L_0x0057
            android.view.View r0 = r7.A0a(r0)
            if (r0 == 0) goto L_0x0057
            X.1o8 r3 = r7.A0c(r0)
        L_0x0057:
            if (r3 != 0) goto L_0x0103
            X.0wD r2 = r7.A0y
            r0 = -1
            r2.A07 = r0
            r0 = -1
            r2.A01 = r0
            r2.A02 = r0
        L_0x0064:
            X.0wD r1 = r7.A0y
            boolean r0 = r1.A0B
            if (r0 == 0) goto L_0x0100
            boolean r0 = r7.A06
            if (r0 == 0) goto L_0x0100
        L_0x006e:
            r1.A0D = r6
            r7.A06 = r4
            r7.A05 = r4
            boolean r0 = r1.A0A
            r1.A08 = r0
            X.1Dz r0 = r7.A0J
            int r0 = r0.ArU()
            r1.A03 = r0
            int[] r0 = r7.A14
            r7.A0T(r0)
            X.0wD r0 = r7.A0y
            boolean r0 = r0.A0B
            if (r0 == 0) goto L_0x015a
            X.0wh r0 = r7.A0H
            int r6 = r0.A02()
            r5 = 0
        L_0x0092:
            if (r5 >= r6) goto L_0x015a
            X.0wh r0 = r7.A0H
            android.view.View r0 = r0.A03(r5)
            X.1o8 r3 = A05(r0)
            boolean r0 = r3.A0F()
            if (r0 != 0) goto L_0x00b2
            boolean r0 = r3.A0D()
            if (r0 == 0) goto L_0x00b5
            X.1Dz r0 = r7.A0J
            boolean r0 = r0.hasStableIds()
            if (r0 != 0) goto L_0x00b5
        L_0x00b2:
            int r5 = r5 + 1
            goto L_0x0092
        L_0x00b5:
            X.C20221Bj.A00(r3)
            r3.A06()
            X.2xx r1 = new X.2xx
            r1.<init>()
            r1.A00(r3)
            X.0wA r0 = r7.A10
            r0.A04(r3, r1)
            X.0wD r0 = r7.A0y
            boolean r0 = r0.A0D
            if (r0 == 0) goto L_0x00b2
            int r0 = r3.A00
            r1 = r0 & 2
            r0 = 0
            if (r1 == 0) goto L_0x00d6
            r0 = 1
        L_0x00d6:
            if (r0 == 0) goto L_0x00b2
            boolean r0 = r3.A0E()
            if (r0 != 0) goto L_0x00b2
            boolean r0 = r3.A0F()
            if (r0 != 0) goto L_0x00b2
            boolean r0 = r3.A0D()
            if (r0 != 0) goto L_0x00b2
            X.1Dz r0 = r7.A0J
            boolean r0 = r0.hasStableIds()
            if (r0 == 0) goto L_0x00fc
            long r1 = r3.A07
        L_0x00f4:
            X.0wA r0 = r7.A10
            X.0zB r0 = r0.A01
            r0.A0D(r1, r3)
            goto L_0x00b2
        L_0x00fc:
            int r0 = r3.A04
            long r1 = (long) r0
            goto L_0x00f4
        L_0x0100:
            r6 = 0
            goto L_0x006e
        L_0x0103:
            X.0wD r2 = r7.A0y
            X.1Dz r0 = r7.A0J
            boolean r0 = r0.hasStableIds()
            if (r0 == 0) goto L_0x0150
            long r0 = r3.A07
        L_0x010f:
            r2.A07 = r0
            boolean r0 = r7.A0T
            if (r0 == 0) goto L_0x0142
            r0 = -1
        L_0x0116:
            r2.A01 = r0
            X.0wD r5 = r7.A0y
            android.view.View r3 = r3.A0H
            int r2 = r3.getId()
        L_0x0120:
            boolean r0 = r3.isFocused()
            if (r0 != 0) goto L_0x0153
            boolean r0 = r3 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x0153
            boolean r0 = r3.hasFocus()
            if (r0 == 0) goto L_0x0153
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3
            android.view.View r3 = r3.getFocusedChild()
            int r1 = r3.getId()
            r0 = -1
            if (r1 == r0) goto L_0x0120
            int r2 = r3.getId()
            goto L_0x0120
        L_0x0142:
            boolean r0 = r3.A0E()
            if (r0 == 0) goto L_0x014b
            int r0 = r3.A02
            goto L_0x0116
        L_0x014b:
            int r0 = r3.A04()
            goto L_0x0116
        L_0x0150:
            r0 = -1
            goto L_0x010f
        L_0x0153:
            r5.A02 = r2
            goto L_0x0064
        L_0x0157:
            r0 = r3
            goto L_0x004b
        L_0x015a:
            X.0wD r0 = r7.A0y
            boolean r0 = r0.A0A
            if (r0 == 0) goto L_0x0211
            X.0wh r0 = r7.A0H
            X.0wj r0 = r0.A01
            int r5 = r0.Ah4()
            r3 = 0
        L_0x0169:
            if (r3 >= r5) goto L_0x0189
            X.0wh r0 = r7.A0H
            X.0wj r0 = r0.A01
            android.view.View r0 = r0.Ah1(r3)
            X.1o8 r2 = A05(r0)
            boolean r0 = r2.A0F()
            if (r0 != 0) goto L_0x0186
            int r1 = r2.A02
            r0 = -1
            if (r1 != r0) goto L_0x0186
            int r0 = r2.A04
            r2.A02 = r0
        L_0x0186:
            int r3 = r3 + 1
            goto L_0x0169
        L_0x0189:
            X.0wD r3 = r7.A0y
            boolean r2 = r3.A0C
            r3.A0C = r4
            X.19T r1 = r7.A0L
            X.0vp r0 = r7.A0w
            r1.A1m(r0, r3)
            X.0wD r0 = r7.A0y
            r0.A0C = r2
            r5 = 0
        L_0x019b:
            X.0wh r0 = r7.A0H
            int r0 = r0.A02()
            if (r5 >= r0) goto L_0x0211
            X.0wh r0 = r7.A0H
            android.view.View r0 = r0.A03(r5)
            X.1o8 r6 = A05(r0)
            boolean r0 = r6.A0F()
            if (r0 != 0) goto L_0x01e5
            X.0wA r0 = r7.A10
            X.04a r0 = r0.A00
            java.lang.Object r0 = r0.get(r6)
            X.1vl r0 = (X.C37491vl) r0
            if (r0 == 0) goto L_0x01c6
            int r0 = r0.A00
            r1 = r0 & 4
            r0 = 1
            if (r1 != 0) goto L_0x01c7
        L_0x01c6:
            r0 = 0
        L_0x01c7:
            if (r0 != 0) goto L_0x01e5
            X.C20221Bj.A00(r6)
            r1 = 8192(0x2000, float:1.14794E-41)
            int r0 = r6.A00
            r1 = r1 & r0
            r0 = 0
            if (r1 == 0) goto L_0x01d5
            r0 = 1
        L_0x01d5:
            r6.A06()
            X.2xx r3 = new X.2xx
            r3.<init>()
            r3.A00(r6)
            if (r0 == 0) goto L_0x01e8
            r7.A16(r6, r3)
        L_0x01e5:
            int r5 = r5 + 1
            goto L_0x019b
        L_0x01e8:
            X.0wA r2 = r7.A10
            X.04a r0 = r2.A00
            java.lang.Object r1 = r0.get(r6)
            X.1vl r1 = (X.C37491vl) r1
            if (r1 != 0) goto L_0x0208
            X.0bP r0 = X.C37491vl.A03
            java.lang.Object r1 = r0.ALa()
            X.1vl r1 = (X.C37491vl) r1
            if (r1 != 0) goto L_0x0203
            X.1vl r1 = new X.1vl
            r1.<init>()
        L_0x0203:
            X.04a r0 = r2.A00
            r0.put(r6, r1)
        L_0x0208:
            int r0 = r1.A00
            r0 = r0 | 2
            r1.A00 = r0
            r1.A02 = r3
            goto L_0x01e5
        L_0x0211:
            r7.A07()
            r0 = 1
            r7.A18(r0)
            A0S(r7, r4)
            X.0wD r1 = r7.A0y
            r0 = 2
            r1.A04 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A09():void");
    }

    private void A0B() {
        int Ah4 = this.A0H.A01.Ah4();
        for (int i = 0; i < Ah4; i++) {
            ((AnonymousClass1R7) this.A0H.A01.Ah1(i).getLayoutParams()).A01 = true;
        }
        C15740vp r4 = this.A0w;
        int size = r4.A05.size();
        for (int i2 = 0; i2 < size; i2++) {
            AnonymousClass1R7 r1 = (AnonymousClass1R7) ((C33781o8) r4.A05.get(i2)).A0H.getLayoutParams();
            if (r1 != null) {
                r1.A01 = true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
        if (r5.A06 != false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0072, code lost:
        if (r5.A0L.A1s() == false) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0027, code lost:
        if (r5.A0L.A1s() == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0C() {
        /*
            r5 = this;
            boolean r0 = r5.A0T
            if (r0 == 0) goto L_0x001c
            X.0wZ r1 = r5.A0G
            java.util.ArrayList r0 = r1.A05
            X.C16150wZ.A05(r1, r0)
            java.util.ArrayList r0 = r1.A06
            X.C16150wZ.A05(r1, r0)
            r0 = 0
            r1.A00 = r0
            boolean r0 = r5.A0U
            if (r0 == 0) goto L_0x001c
            X.19T r0 = r5.A0L
            r0.A1L(r5)
        L_0x001c:
            X.1Bj r0 = r5.A0K
            if (r0 == 0) goto L_0x0029
            X.19T r0 = r5.A0L
            boolean r0 = r0.A1s()
            r1 = 1
            if (r0 != 0) goto L_0x002a
        L_0x0029:
            r1 = 0
        L_0x002a:
            X.0wZ r0 = r5.A0G
            if (r1 == 0) goto L_0x007d
            r0.A09()
        L_0x0031:
            boolean r0 = r5.A05
            r3 = 0
            if (r0 != 0) goto L_0x003b
            boolean r0 = r5.A06
            r4 = 0
            if (r0 == 0) goto L_0x003c
        L_0x003b:
            r4 = 1
        L_0x003c:
            X.0wD r2 = r5.A0y
            boolean r0 = r5.A03
            if (r0 == 0) goto L_0x007b
            X.1Bj r0 = r5.A0K
            if (r0 == 0) goto L_0x007b
            boolean r1 = r5.A0T
            if (r1 != 0) goto L_0x0052
            if (r4 != 0) goto L_0x0052
            X.19T r0 = r5.A0L
            boolean r0 = r0.A0F
            if (r0 == 0) goto L_0x007b
        L_0x0052:
            if (r1 == 0) goto L_0x005c
            X.1Dz r0 = r5.A0J
            boolean r0 = r0.hasStableIds()
            if (r0 == 0) goto L_0x007b
        L_0x005c:
            r0 = 1
        L_0x005d:
            r2.A0B = r0
            if (r0 == 0) goto L_0x0078
            if (r4 == 0) goto L_0x0078
            boolean r0 = r5.A0T
            if (r0 != 0) goto L_0x0078
            X.1Bj r0 = r5.A0K
            if (r0 == 0) goto L_0x0074
            X.19T r0 = r5.A0L
            boolean r1 = r0.A1s()
            r0 = 1
            if (r1 != 0) goto L_0x0075
        L_0x0074:
            r0 = 0
        L_0x0075:
            if (r0 == 0) goto L_0x0078
            r3 = 1
        L_0x0078:
            r2.A0A = r3
            return
        L_0x007b:
            r0 = 0
            goto L_0x005d
        L_0x007d:
            r0.A08()
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0C():void");
    }

    private void A0D() {
        boolean z;
        VelocityTracker velocityTracker = this.A0i;
        if (velocityTracker != null) {
            velocityTracker.clear();
        }
        CHt(0);
        EdgeEffect edgeEffect = this.A0D;
        if (edgeEffect != null) {
            edgeEffect.onRelease();
            z = this.A0D.isFinished();
        } else {
            z = false;
        }
        EdgeEffect edgeEffect2 = this.A0F;
        if (edgeEffect2 != null) {
            edgeEffect2.onRelease();
            z |= this.A0F.isFinished();
        }
        EdgeEffect edgeEffect3 = this.A0E;
        if (edgeEffect3 != null) {
            edgeEffect3.onRelease();
            z |= this.A0E.isFinished();
        }
        EdgeEffect edgeEffect4 = this.A0C;
        if (edgeEffect4 != null) {
            edgeEffect4.onRelease();
            z |= this.A0C.isFinished();
        }
        if (z) {
            C15320v6.postInvalidateOnAnimation(this);
        }
    }

    public static void A0H(C33781o8 r3) {
        WeakReference weakReference = r3.A0C;
        if (weakReference != null) {
            View view = (View) weakReference.get();
            while (view != null) {
                if (view != r3.A0H) {
                    ViewParent parent = view.getParent();
                    if (parent instanceof View) {
                        view = (View) parent;
                    } else {
                        view = null;
                    }
                } else {
                    return;
                }
            }
            r3.A0C = null;
        }
    }

    public static void A0I(RecyclerView recyclerView) {
        if (recyclerView.A0C == null) {
            EdgeEffect edgeEffect = new EdgeEffect(recyclerView.getContext());
            recyclerView.A0C = edgeEffect;
            boolean z = recyclerView.A02;
            int measuredWidth = recyclerView.getMeasuredWidth();
            if (z) {
                edgeEffect.setSize((measuredWidth - recyclerView.getPaddingLeft()) - recyclerView.getPaddingRight(), (recyclerView.getMeasuredHeight() - recyclerView.getPaddingTop()) - recyclerView.getPaddingBottom());
            } else {
                edgeEffect.setSize(measuredWidth, recyclerView.getMeasuredHeight());
            }
        }
    }

    public static void A0J(RecyclerView recyclerView) {
        if (recyclerView.A0D == null) {
            EdgeEffect edgeEffect = new EdgeEffect(recyclerView.getContext());
            recyclerView.A0D = edgeEffect;
            boolean z = recyclerView.A02;
            int measuredHeight = recyclerView.getMeasuredHeight();
            if (z) {
                edgeEffect.setSize((measuredHeight - recyclerView.getPaddingTop()) - recyclerView.getPaddingBottom(), (recyclerView.getMeasuredWidth() - recyclerView.getPaddingLeft()) - recyclerView.getPaddingRight());
            } else {
                edgeEffect.setSize(measuredHeight, recyclerView.getMeasuredWidth());
            }
        }
    }

    public static void A0K(RecyclerView recyclerView) {
        if (recyclerView.A0E == null) {
            EdgeEffect edgeEffect = new EdgeEffect(recyclerView.getContext());
            recyclerView.A0E = edgeEffect;
            boolean z = recyclerView.A02;
            int measuredHeight = recyclerView.getMeasuredHeight();
            if (z) {
                edgeEffect.setSize((measuredHeight - recyclerView.getPaddingTop()) - recyclerView.getPaddingBottom(), (recyclerView.getMeasuredWidth() - recyclerView.getPaddingLeft()) - recyclerView.getPaddingRight());
            } else {
                edgeEffect.setSize(measuredHeight, recyclerView.getMeasuredWidth());
            }
        }
    }

    public static void A0L(RecyclerView recyclerView) {
        if (recyclerView.A0F == null) {
            EdgeEffect edgeEffect = new EdgeEffect(recyclerView.getContext());
            recyclerView.A0F = edgeEffect;
            boolean z = recyclerView.A02;
            int measuredWidth = recyclerView.getMeasuredWidth();
            if (z) {
                edgeEffect.setSize((measuredWidth - recyclerView.getPaddingLeft()) - recyclerView.getPaddingRight(), (recyclerView.getMeasuredHeight() - recyclerView.getPaddingTop()) - recyclerView.getPaddingBottom());
            } else {
                edgeEffect.setSize(measuredWidth, recyclerView.getMeasuredHeight());
            }
        }
    }

    public static void A0M(RecyclerView recyclerView) {
        int i = recyclerView.A0d + 1;
        recyclerView.A0d = i;
        if (i == 1 && !recyclerView.A0W) {
            recyclerView.A07 = false;
        }
    }

    public static void A0N(RecyclerView recyclerView) {
        C621830i r0;
        C15920wC r1 = recyclerView.mViewFlinger;
        r1.A06.removeCallbacks(r1);
        r1.A03.abortAnimation();
        AnonymousClass19T r02 = recyclerView.A0L;
        if (r02 != null && (r0 = r02.A07) != null) {
            r0.A01();
        }
    }

    public static void A0R(RecyclerView recyclerView, C33781o8 r7) {
        View view = r7.A0H;
        boolean z = false;
        if (view.getParent() == recyclerView) {
            z = true;
        }
        recyclerView.A0w.A0A(recyclerView.A0c(view));
        if (r7.A0B()) {
            recyclerView.A0H.A04(view, -1, view.getLayoutParams(), true);
            return;
        }
        C16220wh r2 = recyclerView.A0H;
        if (!z) {
            r2.A05(view, -1, true);
            return;
        }
        int BCT = r2.A01.BCT(view);
        if (BCT >= 0) {
            r2.A00.A04(BCT);
            r2.A02.add(view);
            r2.A01.BXh(view);
            return;
        }
        throw new IllegalArgumentException("view is not a child, cannot hide " + view);
    }

    public static void A0S(RecyclerView recyclerView, boolean z) {
        if (recyclerView.A0d < 1) {
            recyclerView.A0d = 1;
        }
        if (!z && !recyclerView.A0W) {
            recyclerView.A07 = false;
        }
        if (recyclerView.A0d == 1) {
            if (z && recyclerView.A07 && !recyclerView.A0W && recyclerView.A0L != null && recyclerView.A0J != null) {
                recyclerView.A08();
            }
            if (!recyclerView.A0W) {
                recyclerView.A07 = false;
            }
        }
        recyclerView.A0d--;
    }

    private void A0T(int[] iArr) {
        int A022 = this.A0H.A02();
        if (A022 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        for (int i3 = 0; i3 < A022; i3++) {
            C33781o8 A052 = A05(this.A0H.A03(i3));
            if (!A052.A0F()) {
                int A053 = A052.A05();
                if (A053 < i) {
                    i = A053;
                }
                if (A053 > i2) {
                    i2 = A053;
                }
            }
        }
        iArr[0] = i;
        iArr[1] = i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A0U(int r25, int r26, android.view.MotionEvent r27) {
        /*
            r24 = this;
            r7 = r24
            r7.A0e()
            X.1Dz r0 = r7.A0J
            r15 = 1
            r22 = 0
            r9 = r25
            r8 = r26
            if (r0 == 0) goto L_0x0134
            int[] r0 = r7.A0r
            r0[r22] = r22
            r0[r15] = r22
            r7.A0t(r9, r8, r0)
            int[] r0 = r7.A0r
            r17 = r0[r22]
            r18 = r0[r15]
            int r4 = r25 - r17
            int r2 = r26 - r18
        L_0x0023:
            java.util.ArrayList r0 = r7.A11
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x002e
            r7.invalidate()
        L_0x002e:
            int[] r1 = r7.A0r
            r1[r22] = r22
            r1[r15] = r22
            int[] r0 = r7.A17
            r6 = r18
            r5 = r17
            X.0v0 r16 = A04(r7)
            r19 = r4
            r20 = r2
            r21 = r0
            r23 = r1
            X.C15270v0.A01(r16, r17, r18, r19, r20, r21, r22, r23)
            int[] r1 = r7.A0r
            r0 = r1[r22]
            int r4 = r4 - r0
            r0 = r1[r15]
            int r2 = r2 - r0
            int r1 = r7.A0e
            int[] r0 = r7.A17
            r3 = r0[r22]
            int r1 = r1 - r3
            r7.A0e = r1
            int r1 = r7.A0f
            r0 = r0[r15]
            int r1 = r1 - r0
            r7.A0f = r1
            r10 = r27
            if (r27 == 0) goto L_0x006a
            float r1 = (float) r3
            float r0 = (float) r0
            r10.offsetLocation(r1, r0)
        L_0x006a:
            int[] r11 = r7.A16
            r1 = r11[r22]
            int[] r3 = r7.A17
            r0 = r3[r22]
            int r1 = r1 + r0
            r11[r22] = r1
            r1 = r11[r15]
            r0 = r3[r15]
            int r1 = r1 + r0
            r11[r15] = r1
            int r1 = r7.getOverScrollMode()
            r0 = 2
            if (r1 == r0) goto L_0x00e5
            if (r27 == 0) goto L_0x00e2
            r3 = 8194(0x2002, float:1.1482E-41)
            int r1 = r10.getSource()
            r1 = r1 & r3
            r0 = 0
            if (r1 != r3) goto L_0x0090
            r0 = 1
        L_0x0090:
            if (r0 != 0) goto L_0x00e2
            float r12 = r10.getX()
            float r11 = (float) r4
            float r10 = r10.getY()
            float r4 = (float) r2
            r3 = 1065353216(0x3f800000, float:1.0)
            r14 = 1
            r13 = 0
            int r0 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r0 >= 0) goto L_0x0118
            A0J(r7)
            android.widget.EdgeEffect r2 = r7.A0D
            float r1 = -r11
            int r0 = r7.getWidth()
            float r0 = (float) r0
            float r1 = r1 / r0
            int r0 = r7.getHeight()
            float r0 = (float) r0
            float r10 = r10 / r0
            float r0 = r3 - r10
            X.C73823gp.A00(r2, r1, r0)
        L_0x00bb:
            r1 = 1
        L_0x00bc:
            int r0 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r0 >= 0) goto L_0x00fb
            A0L(r7)
            android.widget.EdgeEffect r2 = r7.A0F
            float r1 = -r4
            int r0 = r7.getHeight()
            float r0 = (float) r0
            float r1 = r1 / r0
            int r0 = r7.getWidth()
            float r0 = (float) r0
            float r12 = r12 / r0
            X.C73823gp.A00(r2, r1, r12)
        L_0x00d5:
            if (r14 != 0) goto L_0x00df
            int r0 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r0 != 0) goto L_0x00df
            int r0 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x00e2
        L_0x00df:
            X.C15320v6.postInvalidateOnAnimation(r7)
        L_0x00e2:
            r7.A0n(r9, r8)
        L_0x00e5:
            if (r17 != 0) goto L_0x00e9
            if (r18 == 0) goto L_0x00ec
        L_0x00e9:
            r7.A0p(r5, r6)
        L_0x00ec:
            boolean r0 = r7.awakenScrollBars()
            if (r0 != 0) goto L_0x00f5
            r7.invalidate()
        L_0x00f5:
            if (r17 != 0) goto L_0x00fa
            if (r18 != 0) goto L_0x00fa
            r15 = 0
        L_0x00fa:
            return r15
        L_0x00fb:
            int r0 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r0 <= 0) goto L_0x0116
            A0I(r7)
            android.widget.EdgeEffect r2 = r7.A0C
            int r0 = r7.getHeight()
            float r0 = (float) r0
            float r1 = r4 / r0
            int r0 = r7.getWidth()
            float r0 = (float) r0
            float r12 = r12 / r0
            float r3 = r3 - r12
            X.C73823gp.A00(r2, r1, r3)
            goto L_0x00d5
        L_0x0116:
            r14 = r1
            goto L_0x00d5
        L_0x0118:
            int r0 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r0 <= 0) goto L_0x0132
            A0K(r7)
            android.widget.EdgeEffect r2 = r7.A0E
            int r0 = r7.getWidth()
            float r0 = (float) r0
            float r1 = r11 / r0
            int r0 = r7.getHeight()
            float r0 = (float) r0
            float r10 = r10 / r0
            X.C73823gp.A00(r2, r1, r10)
            goto L_0x00bb
        L_0x0132:
            r1 = 0
            goto L_0x00bc
        L_0x0134:
            r18 = 0
            r17 = 0
            r4 = 0
            r2 = 0
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0U(int, int, android.view.MotionEvent):boolean");
    }

    public int A0X(C33781o8 r8) {
        int i = r8.A00;
        boolean z = false;
        if ((524 & i) != 0) {
            z = true;
        }
        if (z) {
            return -1;
        }
        boolean z2 = true;
        if ((i & 1) == 0) {
            z2 = false;
        }
        if (!z2) {
            return -1;
        }
        C16150wZ r5 = this.A0G;
        int i2 = r8.A04;
        int size = r5.A05.size();
        for (int i3 = 0; i3 < size; i3++) {
            C21731Im r2 = (C21731Im) r5.A05.get(i3);
            int i4 = r2.A00;
            if (i4 != 1) {
                if (i4 == 2) {
                    int i5 = r2.A02;
                    if (i5 <= i2) {
                        int i6 = r2.A01;
                        if (i5 + i6 > i2) {
                            return -1;
                        }
                        i2 -= i6;
                    } else {
                        continue;
                    }
                } else if (i4 == 8) {
                    int i7 = r2.A02;
                    if (i7 == i2) {
                        i2 = r2.A01;
                    } else {
                        if (i7 < i2) {
                            i2--;
                        }
                        if (r2.A01 <= i2) {
                            i2++;
                        }
                    }
                }
            } else if (r2.A02 <= i2) {
                i2 += r2.A01;
            }
        }
        return i2;
    }

    public View A0Z(float f, float f2) {
        for (int A022 = this.A0H.A02() - 1; A022 >= 0; A022--) {
            View A032 = this.A0H.A03(A022);
            float translationX = A032.getTranslationX();
            float translationY = A032.getTranslationY();
            if (f >= ((float) A032.getLeft()) + translationX && f <= ((float) A032.getRight()) + translationX && f2 >= ((float) A032.getTop()) + translationY && f2 <= ((float) A032.getBottom()) + translationY) {
                return A032;
            }
        }
        return null;
    }

    public C33781o8 A0b(int i) {
        C33781o8 r1 = null;
        if (!this.A0T) {
            int Ah4 = this.A0H.A01.Ah4();
            for (int i2 = 0; i2 < Ah4; i2++) {
                C33781o8 A052 = A05(this.A0H.A01.Ah1(i2));
                if (A052 != null && !A052.A0E() && A0X(A052) == i) {
                    C16220wh r0 = this.A0H;
                    if (!r0.A02.contains(A052.A0H)) {
                        return A052;
                    }
                    r1 = A052;
                }
            }
        }
        return r1;
    }

    public String A0d() {
        return " " + super.toString() + ", adapter:" + this.A0J + ", layout:" + this.A0L + ", context:" + getContext();
    }

    public void A0f() {
        if (!this.A08 && this.A04) {
            C15320v6.postOnAnimation(this, this.A0n);
            this.A08 = true;
        }
    }

    public void A0g() {
        C20221Bj r0 = this.A0K;
        if (r0 != null) {
            r0.A07();
        }
        AnonymousClass19T r1 = this.A0L;
        if (r1 != null) {
            r1.A1F(this.A0w);
            this.A0L.A1G(this.A0w);
        }
        C15740vp r12 = this.A0w;
        r12.A04.clear();
        C15740vp.A01(r12);
    }

    public void A0h(int i) {
        AnonymousClass19T r0 = this.A0L;
        if (r0 != null) {
            r0.A1j(i);
            awakenScrollBars();
        }
    }

    public void A0i(int i) {
        int A022 = this.A0H.A02();
        for (int i2 = 0; i2 < A022; i2++) {
            this.A0H.A03(i2).offsetTopAndBottom(i);
        }
    }

    public void A0j(int i) {
        if (!this.A0W) {
            A0l(0);
            A0N(this);
            AnonymousClass19T r0 = this.A0L;
            if (r0 == null) {
                Log.e("RecyclerView", "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            r0.A1j(i);
            awakenScrollBars();
        }
    }

    public void A0k(int i) {
        C15740vp r0 = this.A0w;
        r0.A01 = i;
        r0.A06();
    }

    public void A0l(int i) {
        if (i != this.A0B) {
            this.A0B = i;
            if (i != 2) {
                A0N(this);
            }
            AnonymousClass19T r0 = this.A0L;
            if (r0 != null) {
                r0.A0z(i);
            }
            AnonymousClass1A9 r02 = this.A0O;
            if (r02 != null) {
                r02.A07(this, i);
            }
            List list = this.A0S;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    ((AnonymousClass1A9) this.A0S.get(size)).A07(this, i);
                }
            }
        }
    }

    public void A0m(int i) {
        if (!this.A0W) {
            AnonymousClass19T r1 = this.A0L;
            if (r1 == null) {
                Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            } else {
                r1.A1q(this, this.A0y, i);
            }
        }
    }

    public void A0n(int i, int i2) {
        boolean z;
        EdgeEffect edgeEffect = this.A0D;
        if (edgeEffect == null || edgeEffect.isFinished() || i <= 0) {
            z = false;
        } else {
            this.A0D.onRelease();
            z = this.A0D.isFinished();
        }
        EdgeEffect edgeEffect2 = this.A0E;
        if (edgeEffect2 != null && !edgeEffect2.isFinished() && i < 0) {
            this.A0E.onRelease();
            z |= this.A0E.isFinished();
        }
        EdgeEffect edgeEffect3 = this.A0F;
        if (edgeEffect3 != null && !edgeEffect3.isFinished() && i2 > 0) {
            this.A0F.onRelease();
            z |= this.A0F.isFinished();
        }
        EdgeEffect edgeEffect4 = this.A0C;
        if (edgeEffect4 != null && !edgeEffect4.isFinished() && i2 < 0) {
            this.A0C.onRelease();
            z |= this.A0C.isFinished();
        }
        if (z) {
            C15320v6.postInvalidateOnAnimation(this);
        }
    }

    public void A0p(int i, int i2) {
        this.A0a++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        AnonymousClass1A9 r0 = this.A0O;
        if (r0 != null) {
            r0.A08(this, i, i2);
        }
        List list = this.A0S;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                ((AnonymousClass1A9) this.A0S.get(size)).A08(this, i, i2);
            }
        }
        this.A0a--;
    }

    public void A0r(int i, int i2, Interpolator interpolator) {
        AnonymousClass19T r1 = this.A0L;
        if (r1 == null) {
            Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.A0W) {
            if (!r1.A1S()) {
                i = 0;
            }
            if (!r1.A1T()) {
                i2 = 0;
            }
            if (i != 0 || i2 != 0) {
                C15920wC r12 = this.mViewFlinger;
                int A002 = C15920wC.A00(r12, i, i2, 0, 0);
                if (interpolator == null) {
                    interpolator = A1B;
                }
                r12.A02(i, i2, A002, interpolator);
            }
        }
    }

    public void A0s(int i, int i2, boolean z) {
        int i3 = i + i2;
        int Ah4 = this.A0H.A01.Ah4();
        for (int i4 = 0; i4 < Ah4; i4++) {
            C33781o8 A052 = A05(this.A0H.A01.Ah1(i4));
            if (A052 != null && !A052.A0F()) {
                int i5 = A052.A04;
                if (i5 >= i3) {
                    A052.A08(-i2, z);
                } else if (i5 >= i) {
                    A052.A00 = 8 | A052.A00;
                    A052.A08(-i2, z);
                    A052.A04 = i - 1;
                }
                this.A0y.A0C = true;
            }
        }
        C15740vp r4 = this.A0w;
        for (int size = r4.A05.size() - 1; size >= 0; size--) {
            C33781o8 r2 = (C33781o8) r4.A05.get(size);
            if (r2 != null) {
                int i6 = r2.A04;
                if (i6 >= i3) {
                    r2.A08(-i2, z);
                } else if (i6 >= i) {
                    r2.A00 = 8 | r2.A00;
                    C15740vp.A02(r4, size);
                }
            }
        }
        requestLayout();
    }

    public void A0w(C61512z0 r2) {
        if (r2 != this.A0k) {
            this.A0k = r2;
            boolean z = false;
            if (r2 != null) {
                z = true;
            }
            setChildrenDrawingOrderEnabled(z);
        }
    }

    public void A0x(C20221Bj r3) {
        C20221Bj r0 = this.A0K;
        if (r0 != null) {
            r0.A07();
            this.A0K.A03 = null;
        }
        this.A0K = r3;
        if (r3 != null) {
            r3.A03 = this.A0l;
        }
    }

    public void A0y(C621730h r3) {
        AnonymousClass19T r1 = this.A0L;
        if (r1 != null) {
            r1.A1r("Cannot add item decoration during a scroll  or layout");
        }
        if (this.A11.isEmpty()) {
            setWillNotDraw(false);
        }
        this.A11.add(r3);
        A0B();
        requestLayout();
    }

    public void A0z(C621730h r4) {
        AnonymousClass19T r1 = this.A0L;
        if (r1 != null) {
            r1.A1r("Cannot remove item decoration during a scroll  or layout");
        }
        this.A11.remove(r4);
        if (this.A11.isEmpty()) {
            boolean z = false;
            if (getOverScrollMode() == 2) {
                z = true;
            }
            setWillNotDraw(z);
        }
        A0B();
        requestLayout();
    }

    public void A10(AnonymousClass19T r5) {
        if (r5 != this.A0L) {
            A0l(0);
            A0N(this);
            if (this.A0L != null) {
                C20221Bj r0 = this.A0K;
                if (r0 != null) {
                    r0.A07();
                }
                this.A0L.A1F(this.A0w);
                this.A0L.A1G(this.A0w);
                C15740vp r1 = this.A0w;
                r1.A04.clear();
                C15740vp.A01(r1);
                if (this.A04) {
                    AnonymousClass19T r12 = this.A0L;
                    C15740vp r02 = this.A0w;
                    r12.A0B = false;
                    r12.A1p(this, r02);
                }
                this.A0L.A1K(null);
                this.A0L = null;
            } else {
                C15740vp r13 = this.A0w;
                r13.A04.clear();
                C15740vp.A01(r13);
            }
            C16220wh r3 = this.A0H;
            C16250wk r2 = r3.A00;
            r2.A00 = 0;
            C16250wk r22 = r2.A01;
            if (r22 != null) {
                r22.A00 = 0;
                C16250wk r03 = r22.A01;
                if (r03 != null) {
                    r03.A02();
                }
            }
            for (int size = r3.A02.size() - 1; size >= 0; size--) {
                r3.A01.Bca((View) r3.A02.get(size));
                r3.A02.remove(size);
            }
            r3.A01.C1V();
            this.A0L = r5;
            if (r5 != null) {
                if (r5.A08 == null) {
                    r5.A1K(this);
                    if (this.A04) {
                        this.A0L.A0B = true;
                    }
                } else {
                    throw new IllegalArgumentException("LayoutManager " + r5 + " is already attached to a RecyclerView:" + r5.A08.A0d());
                }
            }
            this.A0w.A06();
            requestLayout();
        }
    }

    public void A11(C15710vl r2) {
        this.A12.remove(r2);
        if (this.A0N == r2) {
            this.A0N = null;
        }
    }

    public void A12(AnonymousClass1A9 r2) {
        if (this.A0S == null) {
            this.A0S = new ArrayList();
        }
        this.A0S.add(r2);
    }

    public void A13(AnonymousClass1A9 r2) {
        List list = this.A0S;
        if (list != null) {
            list.remove(r2);
        }
    }

    public void A17(String str) {
        boolean z = false;
        if (this.A0A > 0) {
            z = true;
        }
        if (z) {
            if (str == null) {
                throw new IllegalStateException(AnonymousClass08S.A0J("Cannot call this method while RecyclerView is computing a layout or scrolling", A0d()));
            }
            throw new IllegalStateException(str);
        } else if (this.A0a > 0) {
            Log.w("RecyclerView", "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException(AnonymousClass08S.A0J(BuildConfig.FLAVOR, A0d())));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1 == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A18(boolean r6) {
        /*
            r5 = this;
            int r1 = r5.A0A
            r0 = 1
            int r1 = r1 - r0
            r5.A0A = r1
            if (r1 >= r0) goto L_0x0064
            r0 = 0
            r5.A0A = r0
            if (r6 == 0) goto L_0x0064
            int r2 = r5.A09
            r5.A09 = r0
            if (r2 == 0) goto L_0x0030
            android.view.accessibility.AccessibilityManager r0 = r5.A0v
            if (r0 == 0) goto L_0x001e
            boolean r1 = r0.isEnabled()
            r0 = 1
            if (r1 != 0) goto L_0x001f
        L_0x001e:
            r0 = 0
        L_0x001f:
            if (r0 == 0) goto L_0x0030
            android.view.accessibility.AccessibilityEvent r1 = android.view.accessibility.AccessibilityEvent.obtain()
            r0 = 2048(0x800, float:2.87E-42)
            r1.setEventType(r0)
            X.AnonymousClass38L.A00(r1, r2)
            r5.sendAccessibilityEventUnchecked(r1)
        L_0x0030:
            java.util.List r0 = r5.A13
            int r0 = r0.size()
            int r4 = r0 + -1
        L_0x0038:
            if (r4 < 0) goto L_0x005f
            java.util.List r0 = r5.A13
            java.lang.Object r3 = r0.get(r4)
            X.1o8 r3 = (X.C33781o8) r3
            android.view.View r0 = r3.A0H
            android.view.ViewParent r0 = r0.getParent()
            if (r0 != r5) goto L_0x005c
            boolean r0 = r3.A0F()
            if (r0 != 0) goto L_0x005c
            int r2 = r3.A03
            r1 = -1
            if (r2 == r1) goto L_0x005c
            android.view.View r0 = r3.A0H
            X.C15320v6.setImportantForAccessibility(r0, r2)
            r3.A03 = r1
        L_0x005c:
            int r4 = r4 + -1
            goto L_0x0038
        L_0x005f:
            java.util.List r0 = r5.A13
            r0.clear()
        L_0x0064:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A18(boolean):void");
    }

    public void A19(boolean z) {
        this.A0U = z | this.A0U;
        this.A0T = true;
        int Ah4 = this.A0H.A01.Ah4();
        for (int i = 0; i < Ah4; i++) {
            C33781o8 A052 = A05(this.A0H.A01.Ah1(i));
            if (A052 != null && !A052.A0F()) {
                A052.A00 = 6 | A052.A00;
            }
        }
        A0B();
        C15740vp r5 = this.A0w;
        int size = r5.A05.size();
        for (int i2 = 0; i2 < size; i2++) {
            C33781o8 r2 = (C33781o8) r5.A05.get(i2);
            if (r2 != null) {
                r2.A00 = 6 | r2.A00;
                r2.A09(null);
            }
        }
        C20831Dz r0 = r5.A07.A0J;
        if (r0 == null || !r0.hasStableIds()) {
            C15740vp.A01(r5);
        }
    }

    public void A1A(boolean z) {
        if (z != this.A0W) {
            A17("Do not setLayoutFrozen in layout or scroll");
            if (!z) {
                this.A0W = false;
                if (!(!this.A07 || this.A0L == null || this.A0J == null)) {
                    requestLayout();
                }
                this.A07 = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.A0W = true;
            this.A0o = true;
            A0l(0);
            A0N(this);
        }
    }

    public boolean A1B() {
        if (this.A03 && !this.A0T) {
            boolean z = false;
            if (this.A0G.A05.size() > 0) {
                z = true;
            }
            if (z) {
                return true;
            }
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        if (r6 != false) goto L_0x0044;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A1C(int r15, int r16) {
        /*
            r14 = this;
            r5 = r16
            X.19T r1 = r14.A0L
            r7 = 0
            if (r1 != 0) goto L_0x000f
            java.lang.String r1 = "RecyclerView"
            java.lang.String r0 = "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument."
            android.util.Log.e(r1, r0)
            return r7
        L_0x000f:
            boolean r0 = r14.A0W
            if (r0 != 0) goto L_0x0035
            boolean r8 = r1.A1S()
            boolean r6 = r1.A1T()
            if (r8 == 0) goto L_0x0025
            int r1 = java.lang.Math.abs(r15)
            int r0 = r14.A0s
            if (r1 >= r0) goto L_0x0026
        L_0x0025:
            r15 = 0
        L_0x0026:
            if (r6 == 0) goto L_0x0030
            int r1 = java.lang.Math.abs(r5)
            int r0 = r14.A0s
            if (r1 >= r0) goto L_0x0031
        L_0x0030:
            r5 = 0
        L_0x0031:
            if (r15 != 0) goto L_0x0036
            if (r5 != 0) goto L_0x0036
        L_0x0035:
            return r7
        L_0x0036:
            float r4 = (float) r15
            float r2 = (float) r5
            boolean r0 = r14.dispatchNestedPreFling(r4, r2)
            if (r0 != 0) goto L_0x0035
            r3 = 1
            if (r8 != 0) goto L_0x0044
            r1 = 0
            if (r6 == 0) goto L_0x0045
        L_0x0044:
            r1 = 1
        L_0x0045:
            r14.dispatchNestedFling(r4, r2, r1)
            X.DDW r0 = r14.A0M
            if (r0 == 0) goto L_0x0053
            boolean r0 = r0.A06(r15, r5)
            if (r0 == 0) goto L_0x0053
            return r3
        L_0x0053:
            if (r1 == 0) goto L_0x0035
            if (r8 == 0) goto L_0x0058
            r7 = 1
        L_0x0058:
            if (r6 == 0) goto L_0x005c
            r7 = r7 | 2
        L_0x005c:
            r14.A1D(r7, r3)
            int r2 = r14.A15
            int r1 = -r2
            int r0 = java.lang.Math.min(r15, r2)
            int r8 = java.lang.Math.max(r1, r0)
            int r0 = java.lang.Math.min(r5, r2)
            int r9 = java.lang.Math.max(r1, r0)
            X.0wC r4 = r14.mViewFlinger
            androidx.recyclerview.widget.RecyclerView r1 = r4.A06
            r0 = 2
            r1.A0l(r0)
            r0 = 0
            r4.A01 = r0
            r4.A00 = r0
            android.view.animation.Interpolator r0 = r4.A02
            android.view.animation.Interpolator r2 = androidx.recyclerview.widget.RecyclerView.A1B
            if (r0 == r2) goto L_0x0094
            r4.A02 = r2
            android.widget.OverScroller r1 = new android.widget.OverScroller
            androidx.recyclerview.widget.RecyclerView r0 = r4.A06
            android.content.Context r0 = r0.getContext()
            r1.<init>(r0, r2)
            r4.A03 = r1
        L_0x0094:
            android.widget.OverScroller r5 = r4.A03
            r6 = 0
            r7 = 0
            r10 = -2147483648(0xffffffff80000000, float:-0.0)
            r11 = 2147483647(0x7fffffff, float:NaN)
            r12 = -2147483648(0xffffffff80000000, float:-0.0)
            r13 = 2147483647(0x7fffffff, float:NaN)
            r5.fling(r6, r7, r8, r9, r10, r11, r12, r13)
            r4.A01()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A1C(int, int):boolean");
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (!(layoutParams instanceof AnonymousClass1R7) || !this.A0L.A1Y((AnonymousClass1R7) layoutParams)) {
            return false;
        }
        return true;
    }

    public int computeHorizontalScrollExtent() {
        AnonymousClass19T r2 = this.A0L;
        if (r2 == null || !r2.A1S()) {
            return 0;
        }
        return r2.A0m(this.A0y);
    }

    public int computeHorizontalScrollOffset() {
        AnonymousClass19T r2 = this.A0L;
        if (r2 == null || !r2.A1S()) {
            return 0;
        }
        return r2.A0n(this.A0y);
    }

    public int computeHorizontalScrollRange() {
        AnonymousClass19T r2 = this.A0L;
        if (r2 == null || !r2.A1S()) {
            return 0;
        }
        return r2.A0o(this.A0y);
    }

    public int computeVerticalScrollExtent() {
        AnonymousClass19T r2 = this.A0L;
        if (r2 == null || !r2.A1T()) {
            return 0;
        }
        return r2.A0p(this.A0y);
    }

    public int computeVerticalScrollOffset() {
        AnonymousClass19T r2 = this.A0L;
        if (r2 == null || !r2.A1T()) {
            return 0;
        }
        return r2.A0q(this.A0y);
    }

    public int computeVerticalScrollRange() {
        AnonymousClass19T r2 = this.A0L;
        if (r2 == null || !r2.A1T()) {
            return 0;
        }
        return r2.A0r(this.A0y);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:104:0x014b, code lost:
        if (r12 > 0) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x014f, code lost:
        if (r10 > 0) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0152, code lost:
        if (r12 < 0) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0155, code lost:
        if (r10 < 0) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x015d, code lost:
        if ((r10 * r11) < 0) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0165, code lost:
        if ((r10 * r11) > 0) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x017e, code lost:
        if (r6 <= r5) goto L_0x0180;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        if (r13.A0W != false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View focusSearch(android.view.View r14, int r15) {
        /*
            r13 = this;
            X.19T r1 = r13.A0L
            X.1Dz r0 = r13.A0J
            r7 = 1
            r5 = 0
            if (r0 == 0) goto L_0x0017
            if (r1 == 0) goto L_0x0017
            int r1 = r13.A0A
            r0 = 0
            if (r1 <= 0) goto L_0x0010
            r0 = 1
        L_0x0010:
            if (r0 != 0) goto L_0x0017
            boolean r1 = r13.A0W
            r0 = 1
            if (r1 == 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            android.view.FocusFinder r4 = android.view.FocusFinder.getInstance()
            r3 = 0
            if (r0 == 0) goto L_0x009e
            r6 = 2
            if (r15 == r6) goto L_0x0024
            if (r15 != r7) goto L_0x009e
        L_0x0024:
            X.19T r0 = r13.A0L
            boolean r0 = r0.A1T()
            if (r0 == 0) goto L_0x009c
            r1 = 33
            if (r15 != r6) goto L_0x0032
            r1 = 130(0x82, float:1.82E-43)
        L_0x0032:
            android.view.View r0 = r4.findNextFocus(r13, r14, r1)
            r2 = 0
            if (r0 != 0) goto L_0x003a
            r2 = 1
        L_0x003a:
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1F
            if (r0 == 0) goto L_0x003f
            r15 = r1
        L_0x003f:
            if (r2 != 0) goto L_0x006b
            X.19T r1 = r13.A0L
            boolean r0 = r1.A1S()
            if (r0 == 0) goto L_0x006b
            androidx.recyclerview.widget.RecyclerView r0 = r1.A08
            int r0 = X.C15320v6.getLayoutDirection(r0)
            r2 = 0
            if (r0 != r7) goto L_0x0053
            r2 = 1
        L_0x0053:
            r0 = 0
            if (r15 != r6) goto L_0x0057
            r0 = 1
        L_0x0057:
            r2 = r2 ^ r0
            r1 = 17
            if (r2 == 0) goto L_0x005e
            r1 = 66
        L_0x005e:
            android.view.View r0 = r4.findNextFocus(r13, r14, r1)
            r2 = 0
            if (r0 != 0) goto L_0x0066
            r2 = 1
        L_0x0066:
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1F
            if (r0 == 0) goto L_0x006b
            r15 = r1
        L_0x006b:
            if (r2 == 0) goto L_0x0085
            r13.A0e()
            android.view.View r0 = r13.A0a(r14)
            if (r0 == 0) goto L_0x0192
            A0M(r13)
            X.19T r2 = r13.A0L
            X.0vp r1 = r13.A0w
            X.0wD r0 = r13.A0y
            r2.A1f(r14, r15, r1, r0)
            A0S(r13, r5)
        L_0x0085:
            android.view.View r4 = r4.findNextFocus(r13, r14, r15)
        L_0x0089:
            if (r4 == 0) goto L_0x00c4
            boolean r0 = r4.hasFocusable()
            if (r0 != 0) goto L_0x00c4
            android.view.View r0 = r13.getFocusedChild()
            if (r0 != 0) goto L_0x00c0
            android.view.View r0 = super.focusSearch(r14, r15)
            return r0
        L_0x009c:
            r2 = 0
            goto L_0x003f
        L_0x009e:
            android.view.View r4 = r4.findNextFocus(r13, r14, r15)
            if (r4 != 0) goto L_0x0089
            if (r0 == 0) goto L_0x0089
            r13.A0e()
            android.view.View r0 = r13.A0a(r14)
            if (r0 == 0) goto L_0x0192
            A0M(r13)
            X.19T r2 = r13.A0L
            X.0vp r1 = r13.A0w
            X.0wD r0 = r13.A0y
            android.view.View r4 = r2.A1f(r14, r15, r1, r0)
            A0S(r13, r5)
            goto L_0x0089
        L_0x00c0:
            r13.A0G(r4, r3)
            return r14
        L_0x00c4:
            r2 = 0
            if (r4 == 0) goto L_0x00d0
            if (r4 == r13) goto L_0x00d0
            android.view.View r0 = r13.A0a(r4)
            if (r0 != 0) goto L_0x00d7
            r2 = 0
        L_0x00d0:
            if (r2 != 0) goto L_0x00d6
            android.view.View r4 = super.focusSearch(r14, r15)
        L_0x00d6:
            return r4
        L_0x00d7:
            if (r14 == 0) goto L_0x014d
            android.view.View r0 = r13.A0a(r14)
            if (r0 == 0) goto L_0x014d
            android.graphics.Rect r5 = r13.A0t
            int r1 = r14.getWidth()
            int r0 = r14.getHeight()
            r5.set(r2, r2, r1, r0)
            android.graphics.Rect r5 = r13.A0u
            int r1 = r4.getWidth()
            int r0 = r4.getHeight()
            r5.set(r2, r2, r1, r0)
            android.graphics.Rect r0 = r13.A0t
            r13.offsetDescendantRectToMyCoords(r14, r0)
            android.graphics.Rect r0 = r13.A0u
            r13.offsetDescendantRectToMyCoords(r4, r0)
            X.19T r0 = r13.A0L
            androidx.recyclerview.widget.RecyclerView r0 = r0.A08
            int r0 = X.C15320v6.getLayoutDirection(r0)
            r12 = -1
            r11 = 1
            if (r0 != r7) goto L_0x0110
            r11 = -1
        L_0x0110:
            android.graphics.Rect r9 = r13.A0t
            int r6 = r9.left
            android.graphics.Rect r8 = r13.A0u
            int r5 = r8.left
            if (r6 < r5) goto L_0x011e
            int r0 = r9.right
            if (r0 > r5) goto L_0x0175
        L_0x011e:
            int r1 = r9.right
            int r0 = r8.right
            if (r1 >= r0) goto L_0x0175
            r10 = 1
        L_0x0125:
            int r6 = r9.top
            int r5 = r8.top
            if (r6 < r5) goto L_0x012f
            int r0 = r9.bottom
            if (r0 > r5) goto L_0x0168
        L_0x012f:
            int r1 = r9.bottom
            int r0 = r8.bottom
            if (r1 >= r0) goto L_0x0168
            r12 = 1
        L_0x0136:
            if (r15 == r7) goto L_0x0160
            r0 = 2
            if (r15 == r0) goto L_0x0158
            r0 = 17
            if (r15 == r0) goto L_0x0155
            r0 = 33
            if (r15 == r0) goto L_0x0152
            r0 = 66
            if (r15 == r0) goto L_0x014f
            r0 = 130(0x82, float:1.82E-43)
            if (r15 != r0) goto L_0x0182
            if (r12 <= 0) goto L_0x00d0
        L_0x014d:
            r2 = 1
            goto L_0x00d0
        L_0x014f:
            if (r10 <= 0) goto L_0x00d0
            goto L_0x014d
        L_0x0152:
            if (r12 >= 0) goto L_0x00d0
            goto L_0x014d
        L_0x0155:
            if (r10 >= 0) goto L_0x00d0
            goto L_0x014d
        L_0x0158:
            if (r12 > 0) goto L_0x014d
            if (r12 != 0) goto L_0x00d0
            int r10 = r10 * r11
            if (r10 < 0) goto L_0x00d0
            goto L_0x014d
        L_0x0160:
            if (r12 < 0) goto L_0x014d
            if (r12 != 0) goto L_0x00d0
            int r10 = r10 * r11
            if (r10 > 0) goto L_0x00d0
            goto L_0x014d
        L_0x0168:
            int r1 = r9.bottom
            int r0 = r8.bottom
            if (r1 > r0) goto L_0x0170
            if (r6 < r0) goto L_0x0173
        L_0x0170:
            if (r6 <= r5) goto L_0x0173
            goto L_0x0136
        L_0x0173:
            r12 = 0
            goto L_0x0136
        L_0x0175:
            int r1 = r9.right
            int r0 = r8.right
            if (r1 > r0) goto L_0x017d
            if (r6 < r0) goto L_0x0180
        L_0x017d:
            r10 = -1
            if (r6 > r5) goto L_0x0125
        L_0x0180:
            r10 = 0
            goto L_0x0125
        L_0x0182:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Invalid direction: "
            java.lang.String r0 = r13.A0d()
            java.lang.String r0 = X.AnonymousClass08S.A0A(r1, r15, r0)
            r2.<init>(r0)
            throw r2
        L_0x0192:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.focusSearch(android.view.View, int):android.view.View");
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        AnonymousClass19T r0 = this.A0L;
        if (r0 != null) {
            return r0.A0w();
        }
        throw new IllegalStateException(AnonymousClass08S.A0J("RecyclerView has no LayoutManager", A0d()));
    }

    public int getBaseline() {
        if (this.A0L != null) {
            return -1;
        }
        return super.getBaseline();
    }

    public int getChildDrawingOrder(int i, int i2) {
        C61512z0 r0 = this.A0k;
        if (r0 == null) {
            return super.getChildDrawingOrder(i, i2);
        }
        return r0.BaC(i, i2);
    }

    public boolean getClipToPadding() {
        return this.A02;
    }

    public boolean isAttachedToWindow() {
        return this.A04;
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f;
        float f2;
        if (this.A0L != null && !this.A0W && motionEvent.getAction() == 8) {
            if ((motionEvent.getSource() & 2) != 0) {
                f = 0.0f;
                if (this.A0L.A1T()) {
                    f = -motionEvent.getAxisValue(9);
                }
                if (this.A0L.A1S()) {
                    f2 = motionEvent.getAxisValue(10);
                }
                f2 = 0.0f;
            } else {
                if ((motionEvent.getSource() & 4194304) != 0) {
                    f2 = motionEvent.getAxisValue(26);
                    AnonymousClass19T r1 = this.A0L;
                    if (r1.A1T()) {
                        f = -f2;
                        f2 = 0.0f;
                    } else if (r1.A1S()) {
                        f = 0.0f;
                    }
                }
                f = 0.0f;
                f2 = 0.0f;
            }
            if (!(f == 0.0f && f2 == 0.0f)) {
                A0U((int) (f2 * this.A0Y), (int) (f * this.A0Z), motionEvent);
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        if (!this.A0W) {
            this.A0N = null;
            if (A0V(motionEvent)) {
                A0D();
                A0l(0);
                return true;
            }
            AnonymousClass19T r0 = this.A0L;
            if (r0 != null) {
                boolean A1S = r0.A1S();
                boolean A1T = r0.A1T();
                if (this.A0i == null) {
                    this.A0i = VelocityTracker.obtain();
                }
                this.A0i.addMovement(motionEvent);
                int actionMasked = motionEvent.getActionMasked();
                int actionIndex = motionEvent.getActionIndex();
                if (actionMasked == 0) {
                    if (this.A0o) {
                        this.A0o = false;
                    }
                    this.A0g = motionEvent.getPointerId(0);
                    int x = (int) (motionEvent.getX() + 0.5f);
                    this.A0e = x;
                    this.A0b = x;
                    int y = (int) (motionEvent.getY() + 0.5f);
                    this.A0f = y;
                    this.A0c = y;
                    if (this.A0B == 2) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        A0l(1);
                        CHt(1);
                    }
                    int[] iArr = this.A16;
                    iArr[1] = 0;
                    iArr[0] = 0;
                    int i = 0;
                    if (A1S) {
                        i = 1;
                    }
                    if (A1T) {
                        i |= 2;
                    }
                    A1D(i, 0);
                } else if (actionMasked == 1) {
                    this.A0i.clear();
                    CHt(0);
                } else if (actionMasked == 2) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.A0g);
                    if (findPointerIndex < 0) {
                        Log.e("RecyclerView", AnonymousClass08S.A0A("Error processing scroll; pointer index for id ", this.A0g, " not found. Did any MotionEvents get skipped?"));
                        return false;
                    }
                    int x2 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
                    int y2 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
                    if (this.A0B != 1) {
                        int i2 = x2 - this.A0b;
                        int i3 = y2 - this.A0c;
                        if (!A1S || Math.abs(i2) <= this.A0h) {
                            z = false;
                        } else {
                            this.A0e = x2;
                            z = true;
                        }
                        if (A1T && Math.abs(i3) > this.A0h) {
                            this.A0f = y2;
                            z = true;
                        }
                        if (z) {
                            A0l(1);
                        }
                    }
                } else if (actionMasked == 3) {
                    A0D();
                    A0l(0);
                } else if (actionMasked == 5) {
                    this.A0g = motionEvent.getPointerId(actionIndex);
                    int x3 = (int) (motionEvent.getX(actionIndex) + 0.5f);
                    this.A0e = x3;
                    this.A0b = x3;
                    int y3 = (int) (motionEvent.getY(actionIndex) + 0.5f);
                    this.A0f = y3;
                    this.A0c = y3;
                } else if (actionMasked == 6) {
                    A0E(motionEvent);
                }
                if (this.A0B == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        AnonymousClass06K.A01("RV OnLayout", 917921195);
        A08();
        AnonymousClass06K.A00(-1137219050);
        this.A03 = true;
    }

    public void onMeasure(int i, int i2) {
        AnonymousClass19T r4 = this.A0L;
        if (r4 == null) {
            A0o(i, i2);
            return;
        }
        boolean z = false;
        if (r4.A1V()) {
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            this.A0L.A1H(this.A0w, this.A0y, i, i2);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z = true;
            }
            if (!z && this.A0J != null) {
                if (this.A0y.A04 == 1) {
                    A09();
                }
                this.A0L.A10(i, i2);
                this.A0y.A09 = true;
                A0A();
                this.A0L.A11(i, i2);
                if (this.A0L.A1W()) {
                    this.A0L.A10(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                    this.A0y.A09 = true;
                    A0A();
                    this.A0L.A11(i, i2);
                }
            }
        } else if (this.A0V) {
            r4.A1H(this.A0w, this.A0y, i, i2);
        } else {
            if (this.A01) {
                A0M(this);
                this.A0A++;
                A0C();
                A18(true);
                C15930wD r1 = this.A0y;
                if (r1.A0A) {
                    r1.A08 = true;
                } else {
                    this.A0G.A08();
                    this.A0y.A08 = false;
                }
                this.A01 = false;
                A0S(this, false);
            } else if (this.A0y.A0A) {
                setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            C20831Dz r0 = this.A0J;
            if (r0 != null) {
                this.A0y.A03 = r0.ArU();
            } else {
                this.A0y.A03 = 0;
            }
            A0M(this);
            this.A0L.A1H(this.A0w, this.A0y, i, i2);
            A0S(this, false);
            this.A0y.A08 = false;
        }
    }

    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        boolean z = false;
        if (this.A0A > 0) {
            z = true;
        }
        if (z) {
            return false;
        }
        return super.onRequestFocusInDescendants(i, rect);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        this.A0m = savedState;
        super.onRestoreInstanceState(savedState.A00);
        AnonymousClass19T r1 = this.A0L;
        if (r1 != null && (parcelable2 = this.A0m.A00) != null) {
            r1.A15(parcelable2);
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = this.A0m;
        if (savedState2 != null) {
            savedState.A00 = savedState2.A00;
            return savedState;
        }
        AnonymousClass19T r0 = this.A0L;
        if (r0 != null) {
            savedState.A00 = r0.A0s();
            return savedState;
        }
        savedState.A00 = null;
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        float f;
        float f2;
        int i;
        int i2;
        boolean z2;
        int i3;
        int i4;
        int A052 = C000700l.A05(-1118027992);
        boolean z3 = false;
        if (this.A0W || this.A0o) {
            C000700l.A0B(525944476, A052);
            return false;
        }
        MotionEvent motionEvent2 = motionEvent;
        C15710vl r0 = this.A0N;
        if (r0 != null) {
            r0.BsS(this, motionEvent2);
            int action = motionEvent2.getAction();
            if (action == 3 || action == 1) {
                this.A0N = null;
            }
            z = true;
        } else if (motionEvent2.getAction() == 0) {
            z = false;
        } else {
            z = A0V(motionEvent2);
        }
        if (z) {
            A0D();
            A0l(0);
            C000700l.A0B(799432304, A052);
            return true;
        }
        AnonymousClass19T r02 = this.A0L;
        if (r02 == null) {
            C000700l.A0B(-1339912934, A052);
            return false;
        }
        boolean A1S = r02.A1S();
        boolean A1T = r02.A1T();
        if (this.A0i == null) {
            this.A0i = VelocityTracker.obtain();
        }
        MotionEvent obtain = MotionEvent.obtain(motionEvent2);
        int actionMasked = motionEvent2.getActionMasked();
        int actionIndex = motionEvent2.getActionIndex();
        if (actionMasked == 0) {
            int[] iArr = this.A16;
            iArr[1] = 0;
            iArr[0] = 0;
        }
        int[] iArr2 = this.A16;
        obtain.offsetLocation((float) iArr2[0], (float) iArr2[1]);
        if (actionMasked == 0) {
            this.A0g = motionEvent2.getPointerId(0);
            int x = (int) (motionEvent2.getX() + 0.5f);
            this.A0e = x;
            this.A0b = x;
            int y = (int) (motionEvent2.getY() + 0.5f);
            this.A0f = y;
            this.A0c = y;
            int i5 = 0;
            if (A1S) {
                i5 = 1;
            }
            if (A1T) {
                i5 |= 2;
            }
            A1D(i5, 0);
        } else if (actionMasked == 1) {
            this.A0i.addMovement(obtain);
            this.A0i.computeCurrentVelocity(AnonymousClass1Y3.A87, (float) this.A15);
            if (A1S) {
                f = -this.A0i.getXVelocity(this.A0g);
            } else {
                f = 0.0f;
            }
            if (A1T) {
                f2 = -this.A0i.getYVelocity(this.A0g);
            } else {
                f2 = 0.0f;
            }
            if ((f == 0.0f && f2 == 0.0f) || !A1C((int) f, (int) f2)) {
                A0l(0);
            }
            A0D();
            z3 = true;
        } else if (actionMasked == 2) {
            int findPointerIndex = motionEvent2.findPointerIndex(this.A0g);
            if (findPointerIndex < 0) {
                Log.e("RecyclerView", AnonymousClass08S.A0A("Error processing scroll; pointer index for id ", this.A0g, " not found. Did any MotionEvents get skipped?"));
                C000700l.A0B(-1010126193, A052);
                return false;
            }
            int x2 = (int) (motionEvent2.getX(findPointerIndex) + 0.5f);
            int y2 = (int) (motionEvent2.getY(findPointerIndex) + 0.5f);
            int i6 = this.A0e - x2;
            int i7 = this.A0f - y2;
            int[] iArr3 = this.A0r;
            iArr3[0] = 0;
            iArr3[1] = 0;
            if (A1E(i6, i7, iArr3, this.A17, 0)) {
                int[] iArr4 = this.A0r;
                i6 -= iArr4[0];
                i7 -= iArr4[1];
                int[] iArr5 = this.A17;
                obtain.offsetLocation((float) iArr5[0], (float) iArr5[1]);
                int[] iArr6 = this.A16;
                int i8 = iArr6[0];
                int[] iArr7 = this.A17;
                iArr6[0] = i8 + iArr7[0];
                iArr6[1] = iArr6[1] + iArr7[1];
            }
            if (this.A0B != 1) {
                if (!A1S || Math.abs(i2) <= (i4 = this.A0h)) {
                    z2 = false;
                } else {
                    if (i2 > 0) {
                        i2 -= i4;
                    } else {
                        i2 += i4;
                    }
                    z2 = true;
                }
                if (A1T && Math.abs(i) > (i3 = this.A0h)) {
                    if (i > 0) {
                        i -= i3;
                    } else {
                        i += i3;
                    }
                    z2 = true;
                }
                if (z2) {
                    A0l(1);
                }
            }
            if (this.A0B == 1) {
                int[] iArr8 = this.A17;
                this.A0e = x2 - iArr8[0];
                this.A0f = y2 - iArr8[1];
                int i9 = 0;
                if (A1S) {
                    i9 = i2;
                }
                int i10 = 0;
                if (A1T) {
                    i10 = i;
                }
                if (A0U(i9, i10, obtain)) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                C37261ux r03 = this.A00;
                if (!(r03 == null || (i2 == 0 && i == 0))) {
                    r03.A01(this, i2, i);
                }
            }
        } else if (actionMasked == 3) {
            A0D();
            A0l(0);
        } else if (actionMasked == 5) {
            this.A0g = motionEvent2.getPointerId(actionIndex);
            int x3 = (int) (motionEvent2.getX(actionIndex) + 0.5f);
            this.A0e = x3;
            this.A0b = x3;
            int y3 = (int) (motionEvent2.getY(actionIndex) + 0.5f);
            this.A0f = y3;
            this.A0c = y3;
        } else if (actionMasked == 6) {
            A0E(motionEvent2);
        }
        if (!z3) {
            this.A0i.addMovement(obtain);
        }
        obtain.recycle();
        C000700l.A0B(-1566780113, A052);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0015, code lost:
        if (r1 != false) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (r1 == false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void requestChildFocus(android.view.View r3, android.view.View r4) {
        /*
            r2 = this;
            X.19T r0 = r2.A0L
            X.30i r0 = r0.A07
            if (r0 == 0) goto L_0x000b
            boolean r1 = r0.A05
            r0 = 1
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            if (r0 != 0) goto L_0x0017
            int r0 = r2.A0A
            r1 = 0
            if (r0 <= 0) goto L_0x0014
            r1 = 1
        L_0x0014:
            r0 = 0
            if (r1 == 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            if (r0 != 0) goto L_0x001f
            if (r4 == 0) goto L_0x001f
            r2.A0G(r3, r4)
        L_0x001f:
            super.requestChildFocus(r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.requestChildFocus(android.view.View, android.view.View):void");
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        return this.A0L.A1Z(this, view, rect, z);
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        int size = this.A12.size();
        for (int i = 0; i < size; i++) {
            ((C15710vl) this.A12.get(i)).Bly(z);
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public void requestLayout() {
        if (this.A0d != 0 || this.A0W) {
            this.A07 = true;
        } else {
            super.requestLayout();
        }
    }

    public void scrollBy(int i, int i2) {
        AnonymousClass19T r2 = this.A0L;
        if (r2 == null) {
            Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.A0W) {
            boolean A1S = r2.A1S();
            boolean A1T = r2.A1T();
            if (A1S || A1T) {
                if (!A1S) {
                    i = 0;
                }
                if (!A1T) {
                    i2 = 0;
                }
                A0U(i, i2, null);
            }
        }
    }

    public void scrollTo(int i, int i2) {
        Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        boolean z;
        int i;
        boolean z2 = false;
        if (this.A0A > 0) {
            z2 = true;
        }
        if (z2) {
            if (accessibilityEvent == null || Build.VERSION.SDK_INT < 19) {
                i = 0;
            } else {
                i = accessibilityEvent.getContentChangeTypes();
            }
            if (i == 0) {
                i = 0;
            }
            this.A09 = i | this.A09;
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    public void setClipToPadding(boolean z) {
        if (z != this.A02) {
            this.A0C = null;
            this.A0F = null;
            this.A0E = null;
            this.A0D = null;
        }
        this.A02 = z;
        super.setClipToPadding(z);
        if (this.A03) {
            requestLayout();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001f, code lost:
        if (r2 == 20) goto L_0x0021;
     */
    static {
        /*
            r0 = 16843830(0x1010436, float:2.369658E-38)
            int[] r0 = new int[]{r0}
            androidx.recyclerview.widget.RecyclerView.A1H = r0
            r0 = 16842987(0x10100eb, float:2.3694217E-38)
            int[] r0 = new int[]{r0}
            androidx.recyclerview.widget.RecyclerView.A1G = r0
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 18
            if (r2 == r0) goto L_0x0021
            r0 = 19
            if (r2 == r0) goto L_0x0021
            r1 = 20
            r0 = 0
            if (r2 != r1) goto L_0x0022
        L_0x0021:
            r0 = 1
        L_0x0022:
            androidx.recyclerview.widget.RecyclerView.A19 = r0
            r1 = 23
            r0 = 0
            if (r2 < r1) goto L_0x002a
            r0 = 1
        L_0x002a:
            androidx.recyclerview.widget.RecyclerView.A18 = r0
            r1 = 16
            r0 = 0
            if (r2 < r1) goto L_0x0032
            r0 = 1
        L_0x0032:
            androidx.recyclerview.widget.RecyclerView.A1A = r0
            r1 = 21
            r0 = 0
            if (r2 < r1) goto L_0x003a
            r0 = 1
        L_0x003a:
            androidx.recyclerview.widget.RecyclerView.A1C = r0
            r1 = 15
            r0 = 0
            if (r2 > r1) goto L_0x0042
            r0 = 1
        L_0x0042:
            androidx.recyclerview.widget.RecyclerView.A1F = r0
            r0 = 0
            if (r2 > r1) goto L_0x0048
            r0 = 1
        L_0x0048:
            androidx.recyclerview.widget.RecyclerView.A1D = r0
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            java.lang.Class<android.util.AttributeSet> r2 = android.util.AttributeSet.class
            java.lang.Class r1 = java.lang.Integer.TYPE
            java.lang.Class[] r0 = new java.lang.Class[]{r3, r2, r1, r1}
            androidx.recyclerview.widget.RecyclerView.A1E = r0
            X.1Ej r0 = new X.1Ej
            r0.<init>()
            androidx.recyclerview.widget.RecyclerView.A1B = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.<clinit>():void");
    }

    public static int A02(View view) {
        C33781o8 A052 = A05(view);
        if (A052 != null) {
            return A052.A04();
        }
        return -1;
    }

    public static int A03(View view) {
        C33781o8 A052 = A05(view);
        if (A052 != null) {
            return A052.A05();
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x003a, code lost:
        if (r4.A0K == null) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0A() {
        /*
            r4 = this;
            A0M(r4)
            int r0 = r4.A0A
            int r0 = r0 + 1
            r4.A0A = r0
            X.0wD r1 = r4.A0y
            r0 = 6
            r1.A01(r0)
            X.0wZ r0 = r4.A0G
            r0.A08()
            X.0wD r1 = r4.A0y
            X.1Dz r0 = r4.A0J
            int r0 = r0.ArU()
            r1.A03 = r0
            X.0wD r2 = r4.A0y
            r3 = 0
            r2.A00 = r3
            r2.A08 = r3
            X.19T r1 = r4.A0L
            X.0vp r0 = r4.A0w
            r1.A1m(r0, r2)
            X.0wD r2 = r4.A0y
            r2.A0C = r3
            r0 = 0
            r4.A0m = r0
            boolean r0 = r2.A0B
            if (r0 == 0) goto L_0x003c
            X.1Bj r1 = r4.A0K
            r0 = 1
            if (r1 != 0) goto L_0x003d
        L_0x003c:
            r0 = 0
        L_0x003d:
            r2.A0B = r0
            r0 = 4
            r2.A04 = r0
            r0 = 1
            r4.A18(r0)
            A0S(r4, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0A():void");
    }

    private void A0E(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.A0g) {
            int i = 0;
            if (actionIndex == 0) {
                i = 1;
            }
            this.A0g = motionEvent.getPointerId(i);
            int x = (int) (motionEvent.getX(i) + 0.5f);
            this.A0e = x;
            this.A0b = x;
            int y = (int) (motionEvent.getY(i) + 0.5f);
            this.A0f = y;
            this.A0c = y;
        }
    }

    public static void A0F(View view, Rect rect) {
        AnonymousClass1R7 r6 = (AnonymousClass1R7) view.getLayoutParams();
        Rect rect2 = r6.A02;
        rect.set((view.getLeft() - rect2.left) - r6.leftMargin, (view.getTop() - rect2.top) - r6.topMargin, view.getRight() + rect2.right + r6.rightMargin, view.getBottom() + rect2.bottom + r6.bottomMargin);
    }

    private boolean A0V(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int size = this.A12.size();
        int i = 0;
        while (i < size) {
            C15710vl r2 = (C15710vl) this.A12.get(i);
            if (!r2.BbZ(this, motionEvent) || action == 1 || action == 3) {
                i++;
            } else {
                this.A0N = r2;
                return true;
            }
        }
        return false;
    }

    public Rect A0Y(View view) {
        AnonymousClass1R7 r7 = (AnonymousClass1R7) view.getLayoutParams();
        if (!r7.A01 || (this.A0y.A08 && (r7.A00() || r7.mViewHolder.A0D()))) {
            return r7.A02;
        }
        Rect rect = r7.A02;
        rect.set(0, 0, 0, 0);
        int size = this.A11.size();
        for (int i = 0; i < size; i++) {
            this.A0t.set(0, 0, 0, 0);
            ((C621730h) this.A11.get(i)).A05(this.A0t, view, this, this.A0y);
            int i2 = rect.left;
            Rect rect2 = this.A0t;
            rect.left = i2 + rect2.left;
            rect.top += rect2.top;
            rect.right += rect2.right;
            rect.bottom += rect2.bottom;
        }
        r7.A01 = false;
        return rect;
    }

    public View A0a(View view) {
        ViewParent parent = view.getParent();
        while (parent != null && parent != this && (parent instanceof View)) {
            view = (View) parent;
            parent = view.getParent();
        }
        if (parent != this) {
            return null;
        }
        return view;
    }

    public C33781o8 A0c(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return A05(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    public void A0e() {
        boolean z;
        int A062 = C000700l.A06(-512195364);
        if (!this.A03 || this.A0T) {
            AnonymousClass06K.A01("RV FullInvalidate", -991309226);
            A08();
            AnonymousClass06K.A00(-2032452842);
            C000700l.A0C(-1208408121, A062);
            return;
        }
        boolean z2 = false;
        if (this.A0G.A05.size() > 0) {
            z2 = true;
        }
        if (!z2) {
            C000700l.A0C(-835686034, A062);
            return;
        }
        C16150wZ r4 = this.A0G;
        int i = r4.A00;
        boolean z3 = false;
        if ((4 & i) != 0) {
            z3 = true;
        }
        if (z3) {
            boolean z4 = false;
            if ((11 & i) != 0) {
                z4 = true;
            }
            if (!z4) {
                AnonymousClass06K.A01("RV PartialInvalidate", -1668064105);
                A0M(this);
                this.A0A++;
                this.A0G.A09();
                if (!this.A07) {
                    int A022 = this.A0H.A02();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= A022) {
                            z = false;
                            break;
                        }
                        C33781o8 A052 = A05(this.A0H.A03(i2));
                        if (A052 != null && !A052.A0F()) {
                            boolean z5 = false;
                            if ((A052.A00 & 2) != 0) {
                                z5 = true;
                            }
                            if (z5) {
                                z = true;
                                break;
                            }
                        }
                        i2++;
                    }
                    if (z) {
                        A08();
                    } else {
                        this.A0G.A07();
                    }
                }
                A0S(this, true);
                A18(true);
                AnonymousClass06K.A00(-964509631);
                C000700l.A0C(-2039312869, A062);
            }
        }
        boolean z6 = false;
        if (r4.A05.size() > 0) {
            z6 = true;
        }
        if (z6) {
            AnonymousClass06K.A01("RV FullInvalidate", 1140900645);
            A08();
            AnonymousClass06K.A00(1445096224);
        }
        C000700l.A0C(-2039312869, A062);
    }

    public void A0o(int i, int i2) {
        setMeasuredDimension(AnonymousClass19T.A0H(i, getPaddingLeft() + getPaddingRight(), C15320v6.getMinimumWidth(this)), AnonymousClass19T.A0H(i2, getPaddingTop() + getPaddingBottom(), C15320v6.getMinimumHeight(this)));
    }

    public void A0t(int i, int i2, int[] iArr) {
        int i3;
        int i4;
        C33781o8 r0;
        A0M(this);
        this.A0A++;
        AnonymousClass06K.A01("RV Scroll", 1725658874);
        if (this.A0B == 2) {
            OverScroller overScroller = this.mViewFlinger.A03;
            overScroller.getFinalX();
            overScroller.getCurrX();
            overScroller.getFinalY();
            overScroller.getCurrY();
        }
        if (i != 0) {
            i3 = this.A0L.A1b(i, this.A0w, this.A0y);
        } else {
            i3 = 0;
        }
        if (i2 != 0) {
            i4 = this.A0L.A1c(i2, this.A0w, this.A0y);
        } else {
            i4 = 0;
        }
        AnonymousClass06K.A00(227204715);
        int A022 = this.A0H.A02();
        for (int i5 = 0; i5 < A022; i5++) {
            View A032 = this.A0H.A03(i5);
            C33781o8 A0c2 = A0c(A032);
            if (!(A0c2 == null || (r0 = A0c2.A0A) == null)) {
                View view = r0.A0H;
                int left = A032.getLeft();
                int top = A032.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
        A18(true);
        A0S(this, false);
        if (iArr != null) {
            iArr[0] = i3;
            iArr[1] = i4;
        }
    }

    public void A0u(View view) {
        C33781o8 A052 = A05(view);
        C20831Dz r0 = this.A0J;
        if (!(r0 == null || A052 == null)) {
            r0.A0C(A052);
        }
        List list = this.A0R;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                ((C24950CQi) this.A0R.get(size)).BS3(view);
            }
        }
    }

    public boolean A1D(int i, int i2) {
        return A04(this).A05(i, i2);
    }

    public boolean A1E(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        return A04(this).A06(i, i2, iArr, iArr2, i3);
    }

    public void CHt(int i) {
        A04(this).A02(i);
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return A04(this).A04(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return A04(this).A03(f, f2);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return A04(this).A06(i, i2, iArr, iArr2, 0);
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return C15270v0.A01(A04(this), i, i2, i3, i4, iArr, 0, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004c, code lost:
        if (r0.draw(r7) == false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007a, code lost:
        if (r1 == false) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b0, code lost:
        if (r1 == false) goto L_0x00b2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r7) {
        /*
            r6 = this;
            super.draw(r7)
            java.util.ArrayList r0 = r6.A11
            int r3 = r0.size()
            r5 = 0
            r2 = 0
        L_0x000b:
            if (r2 >= r3) goto L_0x001d
            java.util.ArrayList r0 = r6.A11
            java.lang.Object r1 = r0.get(r2)
            X.30h r1 = (X.C621730h) r1
            X.0wD r0 = r6.A0y
            r1.A04(r7, r6, r0)
            int r2 = r2 + 1
            goto L_0x000b
        L_0x001d:
            android.widget.EdgeEffect r0 = r6.A0D
            if (r0 == 0) goto L_0x012b
            boolean r0 = r0.isFinished()
            if (r0 != 0) goto L_0x012b
            int r2 = r7.save()
            boolean r0 = r6.A02
            if (r0 == 0) goto L_0x0128
            int r1 = r6.getPaddingBottom()
        L_0x0033:
            r0 = 1132920832(0x43870000, float:270.0)
            r7.rotate(r0)
            int r0 = r6.getHeight()
            int r0 = -r0
            int r0 = r0 + r1
            float r1 = (float) r0
            r0 = 0
            r7.translate(r1, r0)
            android.widget.EdgeEffect r0 = r6.A0D
            if (r0 == 0) goto L_0x004e
            boolean r0 = r0.draw(r7)
            r4 = 1
            if (r0 != 0) goto L_0x004f
        L_0x004e:
            r4 = 0
        L_0x004f:
            r7.restoreToCount(r2)
        L_0x0052:
            android.widget.EdgeEffect r0 = r6.A0F
            if (r0 == 0) goto L_0x0081
            boolean r0 = r0.isFinished()
            if (r0 != 0) goto L_0x0081
            int r2 = r7.save()
            boolean r0 = r6.A02
            if (r0 == 0) goto L_0x0071
            int r0 = r6.getPaddingLeft()
            float r1 = (float) r0
            int r0 = r6.getPaddingTop()
            float r0 = (float) r0
            r7.translate(r1, r0)
        L_0x0071:
            android.widget.EdgeEffect r0 = r6.A0F
            if (r0 == 0) goto L_0x007c
            boolean r1 = r0.draw(r7)
            r0 = 1
            if (r1 != 0) goto L_0x007d
        L_0x007c:
            r0 = 0
        L_0x007d:
            r4 = r4 | r0
            r7.restoreToCount(r2)
        L_0x0081:
            android.widget.EdgeEffect r0 = r6.A0E
            if (r0 == 0) goto L_0x00b7
            boolean r0 = r0.isFinished()
            if (r0 != 0) goto L_0x00b7
            int r3 = r7.save()
            int r2 = r6.getWidth()
            boolean r0 = r6.A02
            if (r0 == 0) goto L_0x0125
            int r1 = r6.getPaddingTop()
        L_0x009b:
            r0 = 1119092736(0x42b40000, float:90.0)
            r7.rotate(r0)
            int r0 = -r1
            float r1 = (float) r0
            int r0 = -r2
            float r0 = (float) r0
            r7.translate(r1, r0)
            android.widget.EdgeEffect r0 = r6.A0E
            if (r0 == 0) goto L_0x00b2
            boolean r1 = r0.draw(r7)
            r0 = 1
            if (r1 != 0) goto L_0x00b3
        L_0x00b2:
            r0 = 0
        L_0x00b3:
            r4 = r4 | r0
            r7.restoreToCount(r3)
        L_0x00b7:
            android.widget.EdgeEffect r0 = r6.A0C
            if (r0 == 0) goto L_0x0123
            boolean r0 = r0.isFinished()
            if (r0 != 0) goto L_0x0123
            int r3 = r7.save()
            r0 = 1127481344(0x43340000, float:180.0)
            r7.rotate(r0)
            boolean r0 = r6.A02
            if (r0 == 0) goto L_0x0113
            int r0 = r6.getWidth()
            int r1 = -r0
            int r0 = r6.getPaddingRight()
            int r1 = r1 + r0
            float r2 = (float) r1
            int r0 = r6.getHeight()
            int r1 = -r0
            int r0 = r6.getPaddingBottom()
            int r1 = r1 + r0
            float r0 = (float) r1
            r7.translate(r2, r0)
        L_0x00e7:
            android.widget.EdgeEffect r0 = r6.A0C
            if (r0 == 0) goto L_0x00f2
            boolean r0 = r0.draw(r7)
            if (r0 == 0) goto L_0x00f2
            r5 = 1
        L_0x00f2:
            r5 = r5 | r4
            r7.restoreToCount(r3)
        L_0x00f6:
            if (r5 != 0) goto L_0x010d
            X.1Bj r0 = r6.A0K
            if (r0 == 0) goto L_0x010d
            java.util.ArrayList r0 = r6.A11
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x010d
            X.1Bj r0 = r6.A0K
            boolean r0 = r0.A0C()
            if (r0 == 0) goto L_0x010d
            r5 = 1
        L_0x010d:
            if (r5 == 0) goto L_0x0112
            X.C15320v6.postInvalidateOnAnimation(r6)
        L_0x0112:
            return
        L_0x0113:
            int r0 = r6.getWidth()
            int r0 = -r0
            float r1 = (float) r0
            int r0 = r6.getHeight()
            int r0 = -r0
            float r0 = (float) r0
            r7.translate(r1, r0)
            goto L_0x00e7
        L_0x0123:
            r5 = r4
            goto L_0x00f6
        L_0x0125:
            r1 = 0
            goto L_0x009b
        L_0x0128:
            r1 = 0
            goto L_0x0033
        L_0x012b:
            r4 = 0
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.draw(android.graphics.Canvas):void");
    }

    public boolean drawChild(Canvas canvas, View view, long j) {
        return super.drawChild(canvas, view, j);
    }

    public boolean hasNestedScrollingParent() {
        if (C15270v0.A00(A04(this), 0) != null) {
            return true;
        }
        return false;
    }

    public boolean isNestedScrollingEnabled() {
        return A04(this).A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        if (r1 >= 30.0f) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAttachedToWindow() {
        /*
            r4 = this;
            r0 = 1664135630(0x6330b1ce, float:3.259439E21)
            int r3 = X.C000700l.A06(r0)
            super.onAttachedToWindow()
            r2 = 0
            r4.A0A = r2
            r1 = 1
            r4.A04 = r1
            boolean r0 = r4.A03
            if (r0 == 0) goto L_0x0071
            boolean r0 = r4.isLayoutRequested()
            if (r0 != 0) goto L_0x0071
        L_0x001a:
            r4.A03 = r1
            X.19T r1 = r4.A0L
            if (r1 == 0) goto L_0x0023
            r0 = 1
            r1.A0B = r0
        L_0x0023:
            r4.A08 = r2
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r0 == 0) goto L_0x0067
            java.lang.ThreadLocal r0 = X.C37261ux.A05
            java.lang.Object r0 = r0.get()
            X.1ux r0 = (X.C37261ux) r0
            r4.A00 = r0
            if (r0 != 0) goto L_0x0060
            X.1ux r0 = new X.1ux
            r0.<init>()
            r4.A00 = r0
            android.view.Display r1 = X.C15320v6.getDisplay(r4)
            boolean r0 = r4.isInEditMode()
            if (r0 != 0) goto L_0x006e
            if (r1 == 0) goto L_0x006e
            float r1 = r1.getRefreshRate()
            r0 = 1106247680(0x41f00000, float:30.0)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x006e
        L_0x0052:
            X.1ux r2 = r4.A00
            r0 = 1315859240(0x4e6e6b28, float:1.0E9)
            float r0 = r0 / r1
            long r0 = (long) r0
            r2.A00 = r0
            java.lang.ThreadLocal r0 = X.C37261ux.A05
            r0.set(r2)
        L_0x0060:
            X.1ux r0 = r4.A00
            java.util.ArrayList r0 = r0.A02
            r0.add(r4)
        L_0x0067:
            r0 = 1527479032(0x5b0b7af8, float:3.9260227E16)
            X.C000700l.A0C(r0, r3)
            return
        L_0x006e:
            r1 = 1114636288(0x42700000, float:60.0)
            goto L_0x0052
        L_0x0071:
            r1 = 0
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onAttachedToWindow():void");
    }

    public void onDetachedFromWindow() {
        C37261ux r0;
        int A062 = C000700l.A06(-345242235);
        super.onDetachedFromWindow();
        C20221Bj r02 = this.A0K;
        if (r02 != null) {
            r02.A07();
        }
        A0l(0);
        A0N(this);
        this.A04 = false;
        AnonymousClass19T r1 = this.A0L;
        if (r1 != null) {
            C15740vp r03 = this.A0w;
            r1.A0B = false;
            r1.A1p(this, r03);
        }
        this.A13.clear();
        removeCallbacks(this.A0n);
        do {
        } while (C37491vl.A03.ALa() != null);
        if (A1C && (r0 = this.A00) != null) {
            r0.A02.remove(this);
            this.A00 = null;
        }
        C000700l.A0C(1100369750, A062);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.A11.size();
        for (int i = 0; i < size; i++) {
            ((C621730h) this.A11.get(i)).A03(canvas, this, this.A0y);
        }
    }

    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int A062 = C000700l.A06(2130805072);
        super.onSizeChanged(i, i2, i3, i4);
        if (!(i == i3 && i2 == i4)) {
            this.A0C = null;
            this.A0F = null;
            this.A0E = null;
            this.A0D = null;
        }
        C000700l.A0C(-1566694734, A062);
    }

    public void removeDetachedView(View view, boolean z) {
        C33781o8 A052 = A05(view);
        if (A052 != null) {
            if (A052.A0B()) {
                A052.A00 &= -257;
            } else if (!A052.A0F()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + A052 + A0d());
            }
        }
        view.clearAnimation();
        A0u(view);
        super.removeDetachedView(view, z);
    }

    public void setNestedScrollingEnabled(boolean z) {
        C15270v0 A042 = A04(this);
        if (A042.A02) {
            C15320v6.stopNestedScroll(A042.A04);
        }
        A042.A02 = z;
    }

    public boolean startNestedScroll(int i) {
        return A04(this).A05(i, 0);
    }

    public void stopNestedScroll() {
        A04(this).A02(0);
    }

    public void A14(AnonymousClass1A9 r1) {
        this.A0O = r1;
    }

    public void A15(C26821DCx dCx) {
        this.A0P = dCx;
    }

    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    public void addFocusables(ArrayList arrayList, int i, int i2) {
        super.addFocusables(arrayList, i, i2);
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x022a, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x022b, code lost:
        r3.initCause(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x023d, code lost:
        throw new java.lang.IllegalStateException(X.AnonymousClass08S.A0P(r4.getPositionDescription(), ": Error creating LayoutManager ", r7), r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x023e, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x024e, code lost:
        throw new java.lang.IllegalStateException(X.AnonymousClass08S.A0P(r4.getPositionDescription(), ": Class is not a LayoutManager ", r7), r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x024f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x025f, code lost:
        throw new java.lang.IllegalStateException(X.AnonymousClass08S.A0P(r4.getPositionDescription(), ": Cannot access non-public constructor ", r7), r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0260, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x026e, code lost:
        throw new java.lang.IllegalStateException(X.AnonymousClass08S.A0P(r4.getPositionDescription(), ": Could not instantiate the LayoutManager: ", r7), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x026f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x027f, code lost:
        throw new java.lang.IllegalStateException(X.AnonymousClass08S.A0P(r4.getPositionDescription(), ": Unable to find LayoutManager ", r7), r3);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0260 A[ExcHandler: InstantiationException | InvocationTargetException (r2v2 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:40:0x01e4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RecyclerView(android.content.Context r20, android.util.AttributeSet r21, int r22) {
        /*
            r19 = this;
            r4 = r21
            r3 = r22
            r6 = r20
            r5 = r19
            r5.<init>(r6, r4, r3)
            X.1mX r0 = new X.1mX
            r0.<init>(r5)
            r5.A0x = r0
            X.0vp r0 = new X.0vp
            r0.<init>(r5)
            r5.A0w = r0
            X.0wA r0 = new X.0wA
            r0.<init>()
            r5.A10 = r0
            X.0wB r0 = new X.0wB
            r0.<init>(r5)
            r5.A0q = r0
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r5.A0t = r0
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r5.A0u = r0
            android.graphics.RectF r0 = new android.graphics.RectF
            r0.<init>()
            r5.A0p = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A11 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r5.A12 = r0
            r2 = 0
            r5.A0d = r2
            r5.A0T = r2
            r5.A0U = r2
            r5.A0A = r2
            r5.A0a = r2
            X.1lq r0 = new X.1lq
            r0.<init>()
            r5.A0K = r0
            r5.A0B = r2
            r10 = -1
            r5.A0g = r10
            r0 = 1
            r5.A0Y = r0
            r5.A0Z = r0
            r0 = 1
            r5.A0X = r0
            X.0wC r1 = new X.0wC
            r1.<init>(r5)
            r5.mViewFlinger = r1
            boolean r1 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r1 == 0) goto L_0x01cb
            X.1uj r1 = new X.1uj
            r1.<init>()
        L_0x0079:
            r5.A0I = r1
            X.0wD r1 = new X.0wD
            r1.<init>()
            r5.A0y = r1
            r5.A05 = r2
            r5.A06 = r2
            X.0wP r1 = new X.0wP
            r1.<init>(r5)
            r5.A0l = r1
            r5.A08 = r2
            r8 = 2
            int[] r1 = new int[r8]
            r5.A14 = r1
            int[] r1 = new int[r8]
            r5.A17 = r1
            int[] r1 = new int[r8]
            r5.A16 = r1
            int[] r1 = new int[r8]
            r5.A0r = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5.A13 = r1
            X.0wU r1 = new X.0wU
            r1.<init>(r5)
            r5.A0n = r1
            X.0wV r1 = new X.0wV
            r1.<init>(r5)
            r5.A0z = r1
            if (r21 == 0) goto L_0x01c7
            int[] r1 = androidx.recyclerview.widget.RecyclerView.A1G
            android.content.res.TypedArray r7 = r6.obtainStyledAttributes(r4, r1, r3, r2)
            boolean r1 = r7.getBoolean(r2, r0)
            r5.A02 = r1
            r7.recycle()
        L_0x00c6:
            r5.setScrollContainer(r0)
            r5.setFocusableInTouchMode(r0)
            android.view.ViewConfiguration r7 = android.view.ViewConfiguration.get(r6)
            int r1 = r7.getScaledTouchSlop()
            r5.A0h = r1
            float r1 = X.C16140wY.A00(r7, r6)
            r5.A0Y = r1
            float r1 = X.C16140wY.A01(r7, r6)
            r5.A0Z = r1
            int r1 = r7.getScaledMinimumFlingVelocity()
            r5.A0s = r1
            int r1 = r7.getScaledMaximumFlingVelocity()
            r5.A15 = r1
            int r7 = r5.getOverScrollMode()
            r1 = 0
            if (r7 != r8) goto L_0x00f6
            r1 = 1
        L_0x00f6:
            r5.setWillNotDraw(r1)
            X.1Bj r7 = r5.A0K
            X.0wT r1 = r5.A0l
            r7.A03 = r1
            X.0wZ r8 = new X.0wZ
            X.0wb r7 = new X.0wb
            r7.<init>(r5)
            r8.<init>(r7, r2)
            r5.A0G = r8
            X.0wh r7 = new X.0wh
            X.0wi r1 = new X.0wi
            r1.<init>(r5)
            r7.<init>(r1)
            r5.A0H = r7
            int r1 = X.C15320v6.getImportantForAutofill(r5)
            if (r1 != 0) goto L_0x0122
            r1 = 8
            X.C15320v6.setImportantForAutofill(r5, r1)
        L_0x0122:
            int r1 = X.C15320v6.getImportantForAccessibility(r5)
            if (r1 != 0) goto L_0x012b
            X.C15320v6.setImportantForAccessibility(r5, r0)
        L_0x012b:
            android.content.Context r7 = r5.getContext()
            java.lang.String r1 = "accessibility"
            java.lang.Object r1 = r7.getSystemService(r1)
            android.view.accessibility.AccessibilityManager r1 = (android.view.accessibility.AccessibilityManager) r1
            r5.A0v = r1
            X.0wl r7 = new X.0wl
            r7.<init>(r5)
            r5.A0Q = r7
            X.C15320v6.setAccessibilityDelegate(r5, r7)
            r8 = 262144(0x40000, float:3.67342E-40)
            if (r21 == 0) goto L_0x0290
            int[] r1 = X.C197819q.A00
            android.content.res.TypedArray r7 = r6.obtainStyledAttributes(r4, r1, r3, r2)
            r1 = 7
            java.lang.String r9 = r7.getString(r1)
            int r1 = r7.getInt(r0, r10)
            if (r1 != r10) goto L_0x015b
            r5.setDescendantFocusability(r8)
        L_0x015b:
            r1 = 2
            boolean r1 = r7.getBoolean(r1, r2)
            if (r1 == 0) goto L_0x01a5
            r1 = 5
            android.graphics.drawable.Drawable r12 = r7.getDrawable(r1)
            android.graphics.drawable.StateListDrawable r12 = (android.graphics.drawable.StateListDrawable) r12
            r1 = 6
            android.graphics.drawable.Drawable r13 = r7.getDrawable(r1)
            r1 = 3
            android.graphics.drawable.Drawable r14 = r7.getDrawable(r1)
            android.graphics.drawable.StateListDrawable r14 = (android.graphics.drawable.StateListDrawable) r14
            r1 = 4
            android.graphics.drawable.Drawable r15 = r7.getDrawable(r1)
            if (r12 == 0) goto L_0x0280
            if (r13 == 0) goto L_0x0280
            if (r14 == 0) goto L_0x0280
            if (r15 == 0) goto L_0x0280
            android.content.Context r1 = r5.getContext()
            android.content.res.Resources r8 = r1.getResources()
            X.30g r10 = new X.30g
            r1 = 2132148230(0x7f160006, float:1.9938432E38)
            int r16 = r8.getDimensionPixelSize(r1)
            r1 = 2132148275(0x7f160033, float:1.9938523E38)
            int r17 = r8.getDimensionPixelSize(r1)
            r1 = 2132148254(0x7f16001e, float:1.993848E38)
            int r18 = r8.getDimensionPixelOffset(r1)
            r11 = r5
            r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18)
        L_0x01a5:
            r7.recycle()
            java.lang.String r8 = ": Could not instantiate the LayoutManager: "
            if (r9 == 0) goto L_0x0294
            java.lang.String r7 = r9.trim()
            boolean r1 = r7.isEmpty()
            if (r1 != 0) goto L_0x0294
            char r1 = r7.charAt(r2)
            r9 = 46
            if (r1 != r9) goto L_0x01ce
            java.lang.String r1 = r6.getPackageName()
            java.lang.String r7 = X.AnonymousClass08S.A0J(r1, r7)
            goto L_0x01e4
        L_0x01c7:
            r5.A02 = r0
            goto L_0x00c6
        L_0x01cb:
            r1 = 0
            goto L_0x0079
        L_0x01ce:
            java.lang.String r1 = "."
            boolean r1 = r7.contains(r1)
            if (r1 != 0) goto L_0x01e4
            java.lang.Class<androidx.recyclerview.widget.RecyclerView> r1 = androidx.recyclerview.widget.RecyclerView.class
            java.lang.Package r1 = r1.getPackage()
            java.lang.String r1 = r1.getName()
            java.lang.String r7 = X.AnonymousClass08S.A07(r1, r9, r7)
        L_0x01e4:
            boolean r1 = r5.isInEditMode()     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            if (r1 == 0) goto L_0x01fe
            java.lang.Class r1 = r5.getClass()     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            java.lang.ClassLoader r1 = r1.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
        L_0x01f2:
            java.lang.Class r9 = r1.loadClass(r7)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            java.lang.Class<X.19T> r1 = X.AnonymousClass19T.class
            java.lang.Class r12 = r9.asSubclass(r1)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            r10 = 0
            goto L_0x0203
        L_0x01fe:
            java.lang.ClassLoader r1 = r6.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            goto L_0x01f2
        L_0x0203:
            java.lang.Class[] r1 = androidx.recyclerview.widget.RecyclerView.A1E     // Catch:{ NoSuchMethodException -> 0x0216, InstantiationException | InvocationTargetException -> 0x0260 }
            java.lang.reflect.Constructor r13 = r12.getConstructor(r1)     // Catch:{ NoSuchMethodException -> 0x0216, InstantiationException | InvocationTargetException -> 0x0260 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r3)     // Catch:{ NoSuchMethodException -> 0x0216, InstantiationException | InvocationTargetException -> 0x0260 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x0216, InstantiationException | InvocationTargetException -> 0x0260 }
            java.lang.Object[] r10 = new java.lang.Object[]{r6, r4, r9, r1}     // Catch:{ NoSuchMethodException -> 0x0216, InstantiationException | InvocationTargetException -> 0x0260 }
            goto L_0x021d
        L_0x0216:
            r9 = move-exception
            java.lang.Class[] r1 = new java.lang.Class[r2]     // Catch:{ NoSuchMethodException -> 0x022a, InstantiationException | InvocationTargetException -> 0x0260 }
            java.lang.reflect.Constructor r13 = r12.getConstructor(r1)     // Catch:{ NoSuchMethodException -> 0x022a, InstantiationException | InvocationTargetException -> 0x0260 }
        L_0x021d:
            r13.setAccessible(r0)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            java.lang.Object r1 = r13.newInstance(r10)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            X.19T r1 = (X.AnonymousClass19T) r1     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            r5.A10(r1)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            goto L_0x0294
        L_0x022a:
            r3 = move-exception
            r3.initCause(r9)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            java.lang.String r1 = r4.getPositionDescription()     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            java.lang.String r0 = ": Error creating LayoutManager "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r7)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            r2.<init>(r0, r3)     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
            throw r2     // Catch:{ ClassNotFoundException -> 0x026f, InstantiationException | InvocationTargetException -> 0x0260, IllegalAccessException -> 0x024f, ClassCastException -> 0x023e }
        L_0x023e:
            r3 = move-exception
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r4.getPositionDescription()
            java.lang.String r0 = ": Class is not a LayoutManager "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r7)
            r2.<init>(r0, r3)
            throw r2
        L_0x024f:
            r3 = move-exception
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r4.getPositionDescription()
            java.lang.String r0 = ": Cannot access non-public constructor "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r7)
            r2.<init>(r0, r3)
            throw r2
        L_0x0260:
            r2 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = r4.getPositionDescription()
            java.lang.String r0 = X.AnonymousClass08S.A0P(r0, r8, r7)
            r1.<init>(r0, r2)
            throw r1
        L_0x026f:
            r3 = move-exception
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r4.getPositionDescription()
            java.lang.String r0 = ": Unable to find LayoutManager "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r7)
            r2.<init>(r0, r3)
            throw r2
        L_0x0280:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Trying to set fast scroller without both required drawables."
            java.lang.String r0 = r5.A0d()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x0290:
            r5.setDescendantFocusability(r8)
            goto L_0x02a7
        L_0x0294:
            int r7 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r7 < r1) goto L_0x02a7
            int[] r1 = androidx.recyclerview.widget.RecyclerView.A1H
            android.content.res.TypedArray r1 = r6.obtainStyledAttributes(r4, r1, r3, r2)
            boolean r0 = r1.getBoolean(r2, r0)
            r1.recycle()
        L_0x02a7:
            r5.setNestedScrollingEnabled(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        AnonymousClass19T r1 = this.A0L;
        if (r1 != null) {
            return r1.A0x(getContext(), attributeSet);
        }
        throw new IllegalStateException(AnonymousClass08S.A0J("RecyclerView has no LayoutManager", A0d()));
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        AnonymousClass19T r0 = this.A0L;
        if (r0 != null) {
            return r0.A1g(layoutParams);
        }
        throw new IllegalStateException(AnonymousClass08S.A0J("RecyclerView has no LayoutManager", A0d()));
    }
}
