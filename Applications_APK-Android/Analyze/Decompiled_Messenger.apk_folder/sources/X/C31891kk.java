package X;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.facebook.common.stringformat.StringFormatUtil;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.1kk  reason: invalid class name and case insensitive filesystem */
public final class C31891kk {
    public AnonymousClass0UN A00;

    public static final C31891kk A00(AnonymousClass1XY r1) {
        return new C31891kk(r1);
    }

    public String A02() {
        StringBuilder sb = new StringBuilder();
        sb.append(System.getProperty("http.agent"));
        sb.append(" [");
        int i = AnonymousClass1Y3.BSZ;
        AnonymousClass0UN r3 = this.A00;
        String str = (String) AnonymousClass1XX.A02(4, i, r3);
        String A01 = A01(((C09400hF) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BMV, r3)).A02());
        String packageName = ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00)).getPackageName();
        String A012 = A01(Locale.getDefault().toString());
        int i2 = AnonymousClass1Y3.BMV;
        AnonymousClass0UN r32 = this.A00;
        Integer valueOf = Integer.valueOf(((C09400hF) AnonymousClass1XX.A02(2, i2, r32)).A01());
        String A013 = A01(((TelephonyManager) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BO1, r32)).getNetworkOperatorName());
        String A014 = A01(Build.MANUFACTURER);
        String A015 = A01(Build.BRAND);
        String A016 = A01(Build.MODEL);
        String A017 = A01(Build.VERSION.RELEASE);
        String A018 = A01(Build.CPU_ABI);
        String A019 = A01(Build.CPU_ABI2);
        DisplayMetrics displayMetrics = ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00)).getResources().getDisplayMetrics();
        Point point = new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
        WindowManager windowManager = (WindowManager) ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00)).getSystemService("window");
        if (!(windowManager == null || windowManager.getDefaultDisplay() == null)) {
            windowManager.getDefaultDisplay().getSize(point);
        }
        sb.append(StringFormatUtil.formatStrLocaleSafe("%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s:%s;%s/%s;FB_FW/1;", "FBAN", str, "FBAV", A01, "FBPN", packageName, "FBLC", A012, "FBBV", valueOf, "FBCR", A013, "FBMF", A014, "FBBD", A015, "FBDV", A016, "FBSV", A017, "FBCA", A018, A019, "FBDM", A01("{density=" + displayMetrics.density + ",width=" + point.x + ",height=" + point.y + "}")));
        for (C30379EvC evC : (Set) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A3s, this.A00)) {
            String name = evC.getName();
            String value = evC.getValue();
            if (name != null) {
                sb.append(name);
                sb.append("/");
                sb.append(A01(value));
                sb.append(";");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public C31891kk(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
    }

    private static String A01(String str) {
        if (C06850cB.A0B(str)) {
            return "null";
        }
        return C06850cB.A05(str).replace("/", "-").replace(";", "-");
    }
}
