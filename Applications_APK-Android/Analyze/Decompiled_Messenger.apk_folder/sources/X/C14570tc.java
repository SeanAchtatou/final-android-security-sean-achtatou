package X;

import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit;
import com.facebook.messaging.inbox2.activenow.loader.Entity;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem;
import com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem;
import com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem;
import com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.user.model.LastActive;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;

/* renamed from: X.0tc  reason: invalid class name and case insensitive filesystem */
public final class C14570tc {
    public AnonymousClass0UN A00;
    public final AnonymousClass06B A01 = AnonymousClass067.A02();
    public final C11920oF A02;
    public final C14760ty A03;
    public final FbSharedPreferences A04;
    public final AnonymousClass0r6 A05;
    public final Boolean A06;
    private final AnonymousClass0u2 A07;
    private final C14770tz A08;
    private final boolean A09;

    public static NavigationTrigger A02(InboxUnitItem inboxUnitItem, String str) {
        String str2 = str;
        String lowerCase = inboxUnitItem.A05().name().toLowerCase();
        if (str == null) {
            str2 = inboxUnitItem.A02.A0P(392918208);
        }
        return new NavigationTrigger("thread_list", lowerCase, str2, false, null, null, null);
    }

    private InboxUnitMontageActiveNowItem A01(AnonymousClass12V r12, Entity entity, AnonymousClass1H4 r14, RankingLoggingItem rankingLoggingItem) {
        User user;
        AnonymousClass1H4 r6 = r14;
        RankingLoggingItem rankingLoggingItem2 = rankingLoggingItem;
        Entity entity2 = entity;
        if (!((AnonymousClass10R) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BO4, this.A00)).A00.Aem(282325382071610L) || (user = entity.A02) == null) {
            return new InboxUnitMontageActiveNowItem(r12.A01, entity2, r6, true, rankingLoggingItem2, BuildConfig.FLAVOR, null);
        }
        C27161ck r4 = r12.A01;
        boolean A0Z = this.A05.A0Z(user.A0Q);
        UserKey userKey = entity.A02.A0Q;
        C06610bn r1 = new C06610bn();
        r1.A01 = AnonymousClass1HA.A02((AnonymousClass1HA) AnonymousClass1XX.A02(13, AnonymousClass1Y3.A8r, this.A00), userKey);
        return new InboxUnitMontageActiveNowItem(r4, entity2, r6, A0Z, rankingLoggingItem2, BuildConfig.FLAVOR, new UnifiedPresenceViewLoggerItem(r1));
    }

    private LastActive A03(UserKey userKey) {
        LastActive A0M = this.A05.A0M(userKey);
        if (A0M == null || ((int) ((AnonymousClass10R) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BO4, this.A00)).A00.At0(563800357536328L)) <= 0 || (this.A01.now() - A0M.A00) / 60000 >= ((long) ((int) ((AnonymousClass10R) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BO4, this.A00)).A00.At0(563800357536328L)))) {
            return null;
        }
        return A0M;
    }

    private boolean A04(ImmutableList immutableList, ImmutableList immutableList2) {
        if (!((C32421lj) AnonymousClass1XX.A02(11, AnonymousClass1Y3.A3B, this.A00)).A00.Aer(282325381809462L, AnonymousClass0XE.A07) || C013509w.A02(immutableList) || C013509w.A02(immutableList2)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0362, code lost:
        if (r5 == com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A3A) goto L_0x0364;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0202, code lost:
        if (r12.A0A() != false) goto L_0x0204;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0226, code lost:
        if (r0.A06.booleanValue() == false) goto L_0x0228;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0275, code lost:
        if (X.C33331nP.A03((X.C33331nP) X.AnonymousClass1XX.A02(16, r3, r0.A00), r12, false) == false) goto L_0x0279;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x02ed, code lost:
        if (r0.A06.booleanValue() == false) goto L_0x02ef;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.inbox2.items.InboxUnitItem A05(X.C27161ck r32, com.facebook.messaging.model.threads.ThreadSummary r33) {
        /*
            r31 = this;
            r0 = r31
            X.0ty r5 = r0.A03
            int r3 = X.AnonymousClass1Y3.ABB
            X.0UN r2 = r0.A00
            r1 = 15
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1rG r1 = (X.C35461rG) r1
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Yd r4 = (X.C25051Yd) r4
            r2 = 563912026096236(0x200e00000026c, double:2.786095593708827E-309)
            r1 = 60
            int r8 = r4.AqL(r2, r1)
            r10 = 0
            r12 = r33
            r6 = r12
            r7 = 1
            r9 = 1
            X.1H4 r14 = r5.A0G(r6, r7, r8, r9, r10)
            int r2 = X.AnonymousClass1Y3.A8q
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            X.1H6 r4 = (X.AnonymousClass1H6) r4
            int r2 = X.AnonymousClass1Y3.A8h
            X.0UN r1 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A03(r2, r1)
            X.1H8 r7 = (X.AnonymousClass1H8) r7
            int r2 = X.AnonymousClass1Y3.A8r
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            X.1HA r3 = (X.AnonymousClass1HA) r3
            X.1Gs r2 = r14.B6D()
            X.1Gs r1 = X.C21381Gs.A0Q
            r13 = 0
            if (r2 != r1) goto L_0x036e
            int r5 = X.AnonymousClass1Y3.A3x
            X.0UN r2 = r7.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r5, r2)
            X.0yd r1 = (X.C17270yd) r1
            com.google.common.collect.ImmutableList r1 = r1.A08(r12)
            java.lang.String r18 = r7.A02(r1)
        L_0x006a:
            boolean r1 = r12.A0A()
            if (r1 != 0) goto L_0x036b
            X.0u2 r1 = r0.A07
            java.util.Set r1 = r1.A00
            java.util.Iterator r2 = r1.iterator()
        L_0x0078:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x036b
            java.lang.Object r1 = r2.next()
            X.1TW r1 = (X.AnonymousClass1TW) r1
            X.1Qt r15 = r1.Afj(r12)
            if (r15 == 0) goto L_0x0078
        L_0x008a:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0S
            boolean r1 = r1.A0N()
            if (r1 == 0) goto L_0x00bb
            r5 = 7
            int r2 = X.AnonymousClass1Y3.BRG
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)
            X.0yl r1 = (X.C17350yl) r1
            boolean r1 = r1.A09()
            if (r1 == 0) goto L_0x00bb
            r5 = 8
            int r2 = X.AnonymousClass1Y3.Aqm
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r2, r1)
            com.facebook.messaging.montage.omnistore.cache.MontageCache r5 = (com.facebook.messaging.montage.omnistore.cache.MontageCache) r5
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0S
            long r1 = r1.A01
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r13 = r5.A04(r1)
        L_0x00bb:
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r9 = new com.facebook.messaging.inbox2.items.InboxUnitThreadItem
            r11 = 0
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0S
            boolean r1 = com.facebook.messaging.model.threadkey.ThreadKey.A0B(r1)
            if (r1 != 0) goto L_0x0359
            r1 = 0
        L_0x00c7:
            if (r1 == 0) goto L_0x0355
            X.1nO r16 = X.C33321nO.A02
        L_0x00cb:
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            X.06B r1 = (X.AnonymousClass06B) r1
            long r1 = r1.now()
            long r5 = r12.A0B
            long r1 = r1 - r5
            r5 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r5
            int r5 = (int) r1
            int r7 = r5 / 60
            int r5 = X.AnonymousClass1Y3.BTP
            X.0UN r2 = r4.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r5, r2)
            X.1FZ r1 = (X.AnonymousClass1FZ) r1
            int r5 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r5, r2)
            X.1Yd r6 = (X.C25051Yd) r6
            r1 = 564195494789873(0x20122000d02f1, double:2.787496115140803E-309)
            r5 = 1440(0x5a0, float:2.018E-42)
            int r1 = r6.AqO(r1, r5)
            r2 = 0
            if (r7 < r1) goto L_0x0117
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0S
            if (r1 == 0) goto L_0x0117
            boolean r1 = r1.A0L()
            if (r1 == 0) goto L_0x0117
            boolean r1 = r12.A0B()
            if (r1 != 0) goto L_0x0117
            r2 = 1
        L_0x0117:
            r17 = 0
            if (r2 == 0) goto L_0x0185
            r5 = 0
            com.google.common.collect.ImmutableList r8 = r4.A01(r12, r5)
            boolean r1 = r8.isEmpty()
            if (r1 != 0) goto L_0x0185
            int r6 = X.AnonymousClass1Y3.BTP
            X.0UN r1 = r4.A00
            r2 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r6, r1)
            X.1FZ r1 = (X.AnonymousClass1FZ) r1
            int r7 = X.AnonymousClass1Y3.AOJ
            X.0UN r6 = r1.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r7, r6)
            X.1Yd r1 = (X.C25051Yd) r1
            r6 = 282720517949281(0x10122000b0761, double:1.396824952931833E-309)
            r1.BJC(r6)
            int r6 = X.AnonymousClass1Y3.BTP
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r6, r1)
            X.1FZ r1 = (X.AnonymousClass1FZ) r1
            int r6 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r5, r6, r2)
            X.1Yd r6 = (X.C25051Yd) r6
            r1 = 282720517949281(0x10122000b0761, double:1.396824952931833E-309)
            X.0XE r7 = X.AnonymousClass0XE.A07
            boolean r1 = r6.Aer(r1, r7)
            if (r1 == 0) goto L_0x0185
            int r2 = X.AnonymousClass1Y3.AEg
            X.0UN r1 = r4.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r2, r1)
            android.content.res.Resources r5 = (android.content.res.Resources) r5
            r4 = 2131689672(0x7f0f00c8, float:1.9008366E38)
            int r2 = r8.size()
            int r1 = r8.size()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r17 = r5.getQuantityString(r4, r2, r1)
        L_0x0185:
            X.0bn r2 = new X.0bn
            r2.<init>()
            X.1HB r1 = r3.A01
            boolean r1 = r1.A02(r12)
            r2.A03 = r1
            java.lang.Long r1 = X.AnonymousClass1HA.A01(r3, r12)
            r2.A01 = r1
            java.lang.String r1 = X.AnonymousClass1HA.A03(r3, r12)
            r2.A02 = r1
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r6 = new com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem
            r6.<init>(r2)
            X.0tz r1 = r0.A08
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r12.A0S
            X.0u1 r1 = r1.A04
            com.facebook.messaging.model.threads.NotificationSetting r1 = r1.A04(r2)
            boolean r1 = r1.A03()
            r20 = r1 ^ 1
            X.0tz r1 = r0.A08
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r12.A0S
            X.0u1 r1 = r1.A04
            com.facebook.messaging.model.threads.NotificationSetting r1 = r1.A03(r2)
            boolean r1 = r1.A03()
            r21 = r1 ^ 1
            int r3 = X.AnonymousClass1Y3.AxE
            X.0UN r2 = r0.A00
            r1 = 17
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0t0 r3 = (X.C14300t0) r3
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0S
            long r1 = r1.A01
            java.lang.String r1 = java.lang.Long.toString(r1)
            com.facebook.user.model.UserKey r1 = com.facebook.user.model.UserKey.A01(r1)
            com.facebook.user.model.User r2 = r3.A03(r1)
            if (r2 != 0) goto L_0x034f
            r1 = 0
        L_0x01e2:
            if (r1 != 0) goto L_0x0204
            r3 = 0
            if (r2 == 0) goto L_0x01f0
            boolean r1 = r2.A17
            if (r1 != 0) goto L_0x01ef
            boolean r1 = r2.A1Y
            if (r1 == 0) goto L_0x01f0
        L_0x01ef:
            r3 = 1
        L_0x01f0:
            if (r3 != 0) goto L_0x0204
            if (r2 != 0) goto L_0x034b
            r1 = 0
        L_0x01f5:
            if (r1 != 0) goto L_0x0204
            if (r2 != 0) goto L_0x033b
            r1 = 0
        L_0x01fa:
            if (r1 != 0) goto L_0x0204
            boolean r1 = r12.A0A()
            r22 = 1
            if (r1 == 0) goto L_0x0206
        L_0x0204:
            r22 = 0
        L_0x0206:
            r3 = 14
            int r2 = X.AnonymousClass1Y3.AmY
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1hZ r1 = (X.C30151hZ) r1
            boolean r23 = r1.A01(r12)
            int r24 = r14.Aw7()
            boolean r1 = r12.A17
            if (r1 == 0) goto L_0x0228
            java.lang.Boolean r1 = r0.A06
            boolean r1 = r1.booleanValue()
            r25 = 1
            if (r1 != 0) goto L_0x022a
        L_0x0228:
            r25 = 0
        L_0x022a:
            r26 = 0
            if (r33 == 0) goto L_0x0279
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0S
            com.facebook.user.model.UserKey r4 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r1)
            r3 = 17
            int r2 = X.AnonymousClass1Y3.AxE
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0t0 r1 = (X.C14300t0) r1
            com.facebook.user.model.User r4 = r1.A03(r4)
            int r3 = X.AnonymousClass1Y3.BH8
            X.0UN r1 = r0.A00
            r2 = 16
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r3, r1)
            X.1nP r1 = (X.C33331nP) r1
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r12.A0S
            boolean r1 = r1.A05(r3, r12)
            if (r1 != 0) goto L_0x0277
            int r3 = X.AnonymousClass1Y3.BH8
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r3, r1)
            X.1nP r1 = (X.C33331nP) r1
            boolean r1 = r1.A07(r12, r4)
            if (r1 != 0) goto L_0x0277
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r3, r1)
            X.1nP r1 = (X.C33331nP) r1
            r2 = 0
            boolean r1 = X.C33331nP.A03(r1, r12, r2)
            if (r1 == 0) goto L_0x0279
        L_0x0277:
            r26 = 1
        L_0x0279:
            r27 = 0
            if (r33 == 0) goto L_0x02b6
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0S
            com.facebook.user.model.UserKey r4 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r1)
            r3 = 17
            int r2 = X.AnonymousClass1Y3.AxE
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0t0 r1 = (X.C14300t0) r1
            com.facebook.user.model.User r4 = r1.A03(r4)
            int r3 = X.AnonymousClass1Y3.BH8
            X.0UN r1 = r0.A00
            r2 = 16
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r3, r1)
            X.1nP r1 = (X.C33331nP) r1
            boolean r1 = r1.A06(r12, r4)
            if (r1 != 0) goto L_0x02b4
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r3, r1)
            X.1nP r1 = (X.C33331nP) r1
            r2 = 1
            boolean r1 = X.C33331nP.A03(r1, r12, r2)
            if (r1 == 0) goto L_0x02b6
        L_0x02b4:
            r27 = 1
        L_0x02b6:
            if (r33 == 0) goto L_0x02ef
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r12.A0S
            boolean r1 = com.facebook.messaging.model.threadkey.ThreadKey.A09(r4)
            if (r1 != 0) goto L_0x02c6
            boolean r1 = com.facebook.messaging.model.threadkey.ThreadKey.A0B(r4)
            if (r1 == 0) goto L_0x02ef
        L_0x02c6:
            int r3 = X.AnonymousClass1Y3.AxE
            X.0UN r2 = r0.A00
            r1 = 17
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0t0 r1 = (X.C14300t0) r1
            long r2 = r4.A01
            java.lang.String r2 = java.lang.Long.toString(r2)
            com.facebook.user.model.UserKey r2 = com.facebook.user.model.UserKey.A01(r2)
            com.facebook.user.model.User r1 = r1.A03(r2)
            if (r1 != 0) goto L_0x0336
            r1 = 0
        L_0x02e3:
            if (r1 != 0) goto L_0x02ef
            java.lang.Boolean r1 = r0.A06
            boolean r1 = r1.booleanValue()
            r28 = 1
            if (r1 != 0) goto L_0x02f1
        L_0x02ef:
            r28 = 0
        L_0x02f1:
            java.lang.Boolean r1 = r0.A06
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0333
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r12.A0S
            boolean r1 = r3.A0N()
            if (r1 == 0) goto L_0x0333
            r2 = 18
            int r1 = X.AnonymousClass1Y3.AL1
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3dw r0 = (X.C72243dw) r0
            java.lang.String r1 = r3.A0I()
            X.3dx r1 = r0.queryStatus(r1)
            if (r1 == 0) goto L_0x0333
            com.facebook.workshared.userstatus.omnistore.model.WorkchatUserStatusEvent r0 = r1.A01()
            if (r0 == 0) goto L_0x0333
            com.facebook.workshared.userstatus.omnistore.model.WorkchatUserStatusEvent r0 = r1.A01()
            java.lang.String r0 = r0.A06
        L_0x0323:
            boolean r1 = r12.A0A()
            r30 = r1 ^ 1
            r10 = r32
            r19 = r6
            r29 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30)
            return r9
        L_0x0333:
            java.lang.String r0 = ""
            goto L_0x0323
        L_0x0336:
            boolean r1 = r1.A0F()
            goto L_0x02e3
        L_0x033b:
            int r3 = X.AnonymousClass1Y3.AMd
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r3, r1)
            X.1HW r1 = (X.AnonymousClass1HW) r1
            boolean r1 = r1.A02(r2, r12)
            goto L_0x01fa
        L_0x034b:
            boolean r1 = r2.A1U
            goto L_0x01f5
        L_0x034f:
            boolean r1 = r2.A0F()
            goto L_0x01e2
        L_0x0355:
            X.1nO r16 = X.C33321nO.A01
            goto L_0x00cb
        L_0x0359:
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r5 = r12.A0G
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A0J
            if (r5 == r1) goto L_0x0364
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A3A
            r2 = 0
            if (r5 != r1) goto L_0x0365
        L_0x0364:
            r2 = 1
        L_0x0365:
            r1 = 0
            if (r2 == 0) goto L_0x00c7
            r1 = 1
            goto L_0x00c7
        L_0x036b:
            r15 = r13
            goto L_0x008a
        L_0x036e:
            r18 = r13
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14570tc.A05(X.1ck, com.facebook.messaging.model.threads.ThreadSummary):com.facebook.messaging.inbox2.items.InboxUnitItem");
    }

    public void A06(C32481lp r4, AnonymousClass12V r5) {
        Object obj = r5.A02;
        if (obj != null && !C013509w.A02(((DiscoverTabAttachmentUnit) obj).A01)) {
            A07(r4, r5);
            Object obj2 = r5.A02;
            DiscoverTabAttachmentUnit discoverTabAttachmentUnit = (DiscoverTabAttachmentUnit) obj2;
            if (discoverTabAttachmentUnit.A00.shouldAddUnitToInbox) {
                r4.A01((InboxUnitItem) obj2);
            } else {
                r4.A02(discoverTabAttachmentUnit.A01);
            }
        }
    }

    public void A07(C32481lp r8, AnonymousClass12V r9) {
        GSTModelShape1S0000000 A0S;
        String A3y;
        if (r9.A01.A0S() != null && !this.A09 && (A0S = r9.A01.A0S()) != null) {
            C27161ck r5 = r9.A01;
            String A3y2 = A0S.A3y();
            GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) r9.A01.A0J(704850766, GSTModelShape1S0000000.class, -1971421990);
            if (gSTModelShape1S0000000 == null) {
                A3y = null;
            } else {
                A3y = gSTModelShape1S0000000.A3y();
            }
            r8.A01(new NonInboxServiceSectionHeaderItem(r5, A3y2, A3y, r9.A01.getBooleanValue(-579238035)));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0067, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0068, code lost:
        if (r2 != null) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006d, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007c, code lost:
        if (r4.A00.isEmpty() != false) goto L_0x006e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x02cf  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0352  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x03f0  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.C32481lp r25, X.AnonymousClass12V r26, X.C34801qC r27, X.C15820w2 r28) {
        /*
            r24 = this;
            int r3 = X.AnonymousClass1Y3.BRG
            r0 = r24
            X.0UN r2 = r0.A00
            r1 = 7
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0yl r1 = (X.C17350yl) r1
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            r1 = 1
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 283154308925636(0x10187000008c4, double:1.39896816512076E-309)
            r3.BJC(r1)
            r4 = r27
            if (r27 == 0) goto L_0x006e
            r3 = 7
            int r2 = X.AnonymousClass1Y3.BRG
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0yl r1 = (X.C17350yl) r1
            boolean r1 = r1.A09()
            r23 = 1
            if (r1 == 0) goto L_0x007e
            int r3 = X.AnonymousClass1Y3.Aqm
            X.0UN r2 = r0.A00
            r1 = 8
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r3, r2)
            com.facebook.messaging.montage.omnistore.cache.MontageCache r5 = (com.facebook.messaging.montage.omnistore.cache.MontageCache) r5
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            java.lang.Integer r1 = r5.A01
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x006e
            int r3 = X.AnonymousClass1Y3.BP2
            X.0UN r2 = r5.A00
            r1 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1HC r1 = (X.AnonymousClass1HC) r1
            X.1HE r2 = r1.A01()
            java.util.Map r1 = r5.A03     // Catch:{ all -> 0x0065 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0065 }
            if (r2 == 0) goto L_0x0074
            goto L_0x0071
        L_0x0065:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0067 }
        L_0x0067:
            r0 = move-exception
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ all -> 0x006d }
        L_0x006d:
            throw r0
        L_0x006e:
            r23 = 0
            goto L_0x007e
        L_0x0071:
            r2.close()
        L_0x0074:
            if (r1 != 0) goto L_0x007e
            com.google.common.collect.ImmutableList r1 = r4.A00
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x006e
        L_0x007e:
            r1 = r26
            r2 = r28
            if (r23 != 0) goto L_0x01bb
            com.google.common.collect.ImmutableList r18 = com.google.common.collect.RegularImmutableList.A02
        L_0x0086:
            X.1ck r3 = r1.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r3.A0R()
            if (r3 == 0) goto L_0x01b8
            X.1ck r3 = r1.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r3.A0R()
            r3 = -866601374(0xffffffffcc58b662, float:-5.6809864E7)
            int r3 = r4.getIntValue(r3)
            if (r3 <= 0) goto L_0x01b8
            X.1ck r3 = r1.A01
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r3.A0R()
            r3 = -866601374(0xffffffffcc58b662, float:-5.6809864E7)
            int r13 = r4.getIntValue(r3)
        L_0x00aa:
            X.1qC r3 = r2.A00
            com.google.common.collect.ImmutableList r3 = r3.A00
            boolean r3 = X.C013509w.A02(r3)
            if (r3 == 0) goto L_0x00ce
            com.google.common.collect.ImmutableList r19 = com.google.common.collect.RegularImmutableList.A02
        L_0x00b6:
            r5 = 6
            int r4 = X.AnonymousClass1Y3.BHw
            X.0UN r3 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r5, r4, r3)
            X.1GE r3 = (X.AnonymousClass1GE) r3
            boolean r3 = r3.A02()
            if (r3 != 0) goto L_0x02c6
            boolean r3 = r19.isEmpty()
            if (r3 == 0) goto L_0x02c6
            return
        L_0x00ce:
            X.1qC r12 = r2.A00
            com.google.common.collect.ImmutableList r11 = r2.A01
            com.google.common.collect.ImmutableList r10 = r2.A02
            com.google.common.collect.ImmutableList$Builder r14 = com.google.common.collect.ImmutableList.builder()
            r9 = 0
        L_0x00d9:
            com.google.common.collect.ImmutableList r3 = r12.A00
            int r3 = r3.size()
            if (r9 >= r3) goto L_0x0165
            com.google.common.collect.ImmutableList r3 = r12.A00
            java.lang.Object r8 = r3.get(r9)
            com.facebook.user.model.User r8 = (com.facebook.user.model.User) r8
            com.facebook.user.model.UserKey r4 = r8.A0Q
            X.0r6 r3 = r0.A05
            boolean r3 = r3.A0a(r4)
            if (r3 == 0) goto L_0x0157
            X.1Gs r7 = X.C21381Gs.A02
        L_0x00f5:
            r5 = 17
            int r4 = X.AnonymousClass1Y3.AxE
            X.0UN r3 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r4, r3)
            X.0t0 r4 = (X.C14300t0) r4
            com.facebook.user.model.UserKey r3 = r8.A0Q
            com.facebook.user.model.User r3 = r4.A03(r3)
            if (r3 == 0) goto L_0x0114
            java.lang.Integer r4 = r3.A06()
            java.lang.Integer r3 = X.AnonymousClass07B.A00
            if (r4 == r3) goto L_0x0114
        L_0x0111:
            int r9 = r9 + 1
            goto L_0x00d9
        L_0x0114:
            boolean r3 = r0.A04(r11, r10)
            if (r3 == 0) goto L_0x013b
            com.google.common.base.Preconditions.checkNotNull(r8)
            com.facebook.messaging.inbox2.activenow.loader.Entity r5 = new com.facebook.messaging.inbox2.activenow.loader.Entity
            X.3i4 r4 = X.C74553i4.USER
            r3 = 0
            r5.<init>(r4, r8, r3)
            X.0ty r3 = r0.A03
            X.1H4 r4 = r3.A0I(r8, r7)
            com.google.common.collect.ImmutableList r3 = r12.A01
            java.lang.Object r3 = r3.get(r9)
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r3 = (com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem) r3
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r3 = r0.A01(r1, r5, r4, r3)
        L_0x0137:
            r14.add(r3)
            goto L_0x0111
        L_0x013b:
            com.facebook.user.model.UserKey r3 = r8.A0Q
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r6 = A00(r3, r11, r10)
            com.google.common.base.Preconditions.checkNotNull(r8)
            com.facebook.messaging.inbox2.activenow.loader.Entity r5 = new com.facebook.messaging.inbox2.activenow.loader.Entity
            X.3i4 r4 = X.C74553i4.USER
            r3 = 0
            r5.<init>(r4, r8, r3)
            X.0ty r3 = r0.A03
            X.1H4 r3 = r3.A0I(r8, r7)
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r3 = r0.A01(r1, r5, r3, r6)
            goto L_0x0137
        L_0x0157:
            X.0r6 r3 = r0.A05
            boolean r3 = r3.A0Z(r4)
            if (r3 == 0) goto L_0x0162
            X.1Gs r7 = X.C21381Gs.A01
            goto L_0x00f5
        L_0x0162:
            X.1Gs r7 = X.C21381Gs.A0Q
            goto L_0x00f5
        L_0x0165:
            com.google.common.collect.ImmutableList r5 = r14.build()
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.RegularImmutableList.A02
            r7 = 2
            r3 = 1
            com.google.common.base.Preconditions.checkState(r3)
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()
            X.1Xv r9 = r5.iterator()
            X.1Xv r8 = r4.iterator()
            r5 = 0
        L_0x017d:
            boolean r3 = r9.hasNext()
            if (r3 != 0) goto L_0x0189
            boolean r3 = r8.hasNext()
            if (r3 == 0) goto L_0x01b2
        L_0x0189:
            r4 = 0
        L_0x018a:
            if (r4 >= r7) goto L_0x01a0
            if (r5 >= r13) goto L_0x01a0
            boolean r3 = r9.hasNext()
            if (r3 == 0) goto L_0x01a0
            java.lang.Object r3 = r9.next()
            r6.add(r3)
            int r5 = r5 + 1
            int r4 = r4 + 1
            goto L_0x018a
        L_0x01a0:
            if (r5 >= r13) goto L_0x01b2
            boolean r3 = r8.hasNext()
            if (r3 == 0) goto L_0x017d
            java.lang.Object r3 = r8.next()
            r6.add(r3)
            int r5 = r5 + 1
            goto L_0x017d
        L_0x01b2:
            com.google.common.collect.ImmutableList r19 = r6.build()
            goto L_0x00b6
        L_0x01b8:
            r13 = 5
            goto L_0x00aa
        L_0x01bb:
            com.google.common.collect.ImmutableList r8 = r2.A01
            com.google.common.collect.ImmutableList r7 = r2.A02
            boolean r3 = r0.A04(r8, r7)
            if (r3 == 0) goto L_0x0242
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()
            r3 = 0
        L_0x01ca:
            com.google.common.collect.ImmutableList r5 = r4.A00
            int r5 = r5.size()
            if (r3 >= r5) goto L_0x023c
            com.google.common.collect.ImmutableList r5 = r4.A00
            java.lang.Object r12 = r5.get(r3)
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r12 = (com.facebook.messaging.montage.model.BasicMontageThreadInfo) r12
            com.facebook.user.model.UserKey r5 = r12.A03
            com.facebook.user.model.LastActive r16 = r0.A03(r5)
            X.1ck r10 = r1.A01
            X.0r6 r7 = r0.A05
            com.facebook.user.model.UserKey r5 = r12.A03
            boolean r14 = r7.A0Z(r5)
            com.google.common.collect.ImmutableList r5 = r4.A01
            java.lang.Object r7 = r5.get(r3)
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r7 = (com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem) r7
            r5 = 0
            if (r16 == 0) goto L_0x0213
            r9 = 13
            int r8 = X.AnonymousClass1Y3.A8r
            X.0UN r5 = r0.A00
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r9, r8, r5)
            X.1HA r9 = (X.AnonymousClass1HA) r9
            com.facebook.user.model.UserKey r5 = r12.A03
            X.0bn r8 = new X.0bn
            r8.<init>()
            java.lang.Long r5 = X.AnonymousClass1HA.A02(r9, r5)
            r8.A01 = r5
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r5 = new com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem
            r5.<init>(r8)
        L_0x0213:
            com.google.common.base.Preconditions.checkNotNull(r12)
            if (r7 != 0) goto L_0x0231
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r9 = new com.facebook.messaging.montage.inboxunit.InboxMontageItem
            r13 = 0
            X.12J r7 = new X.12J
            r7.<init>()
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r15 = new com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem
            r15.<init>(r7)
            r11 = 2
            r17 = r5
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
        L_0x022b:
            r6.add(r9)
            int r3 = r3 + 1
            goto L_0x01ca
        L_0x0231:
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r9 = new com.facebook.messaging.montage.inboxunit.InboxMontageItem
            r11 = 2
            r13 = 0
            r15 = r7
            r17 = r5
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
            goto L_0x022b
        L_0x023c:
            com.google.common.collect.ImmutableList r18 = r6.build()
            goto L_0x0086
        L_0x0242:
            com.google.common.collect.ImmutableList r13 = r4.A00
            com.google.common.collect.ImmutableList$Builder r12 = com.google.common.collect.ImmutableList.builder()
            int r11 = r13.size()
            r6 = 0
        L_0x024d:
            if (r6 >= r11) goto L_0x02c0
            java.lang.Object r5 = r13.get(r6)
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r5 = (com.facebook.messaging.montage.model.BasicMontageThreadInfo) r5
            com.facebook.user.model.UserKey r3 = r5.A03
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r20 = A00(r3, r8, r7)
            com.facebook.user.model.UserKey r3 = r5.A03
            com.facebook.user.model.LastActive r21 = r0.A03(r3)
            X.1ck r15 = r1.A01
            X.0r6 r4 = r0.A05
            com.facebook.user.model.UserKey r3 = r5.A03
            boolean r19 = r4.A0Z(r3)
            r4 = 0
            if (r21 == 0) goto L_0x028c
            r9 = 13
            int r4 = X.AnonymousClass1Y3.A8r
            X.0UN r3 = r0.A00
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r9, r4, r3)
            X.1HA r9 = (X.AnonymousClass1HA) r9
            com.facebook.user.model.UserKey r4 = r5.A03
            X.0bn r3 = new X.0bn
            r3.<init>()
            java.lang.Long r4 = X.AnonymousClass1HA.A02(r9, r4)
            r3.A01 = r4
            com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem r4 = new com.facebook.messaging.presence.unifiedpresence.UnifiedPresenceViewLoggerItem
            r4.<init>(r3)
        L_0x028c:
            com.google.common.base.Preconditions.checkNotNull(r5)
            if (r20 != 0) goto L_0x02b1
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r3 = new com.facebook.messaging.montage.inboxunit.InboxMontageItem
            r18 = 0
            r14 = r3
            X.12J r10 = new X.12J
            r10.<init>()
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r9 = new com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem
            r9.<init>(r10)
            r16 = 2
            r20 = r9
            r22 = r4
            r17 = r5
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22)
        L_0x02ab:
            r12.add(r3)
            int r6 = r6 + 1
            goto L_0x024d
        L_0x02b1:
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r3 = new com.facebook.messaging.montage.inboxunit.InboxMontageItem
            r16 = 2
            r18 = 0
            r14 = r3
            r17 = r5
            r22 = r4
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22)
            goto L_0x02ab
        L_0x02c0:
            com.google.common.collect.ImmutableList r18 = r12.build()
            goto L_0x0086
        L_0x02c6:
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem r4 = new com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem
            X.1ck r11 = r1.A01
            java.lang.Object r1 = r1.A02
            r5 = 0
            if (r1 == 0) goto L_0x02d2
            r5 = r1
            com.google.common.collect.ImmutableList r5 = (com.google.common.collect.ImmutableList) r5
        L_0x02d2:
            com.google.common.collect.ImmutableList r7 = r2.A01
            int r3 = X.AnonymousClass1Y3.BRG
            X.0UN r2 = r0.A00
            r1 = 7
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0yl r1 = (X.C17350yl) r1
            boolean r2 = r1.A0B()
            r1 = 0
            if (r2 == 0) goto L_0x043c
            r6 = 10
            int r3 = X.AnonymousClass1Y3.AD9
            X.0UN r2 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.113 r2 = (X.AnonymousClass113) r2
            int r6 = X.AnonymousClass1Y3.B6q
            X.0UN r3 = r2.A00
            r2 = 4
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r6, r3)
            com.facebook.prefs.shared.FbSharedPreferences r6 = (com.facebook.prefs.shared.FbSharedPreferences) r6
            X.1Y7 r3 = X.AnonymousClass129.A0G
            r2 = 0
            boolean r2 = r6.Aep(r3, r2)
            if (r2 != 0) goto L_0x043c
            if (r23 == 0) goto L_0x043c
            com.facebook.prefs.shared.FbSharedPreferences r3 = r0.A04
            X.1Y7 r2 = X.AnonymousClass129.A0I
            java.lang.String r3 = r3.B4F(r2, r1)
            if (r3 == 0) goto L_0x043c
            X.0oF r2 = r0.A02
            java.util.List r9 = r2.A04(r3)
            int r3 = X.AnonymousClass1Y3.AId
            X.0UN r2 = r0.A00
            r8 = 9
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r8, r3, r2)
            X.3jb r3 = (X.C75293jb) r3
            r2 = 624(0x270, float:8.74E-43)
            java.lang.String r2 = X.C99084oO.$const$string(r2)
            java.lang.String r6 = r3.A03(r2)
            boolean r2 = r9.isEmpty()
            if (r2 != 0) goto L_0x043c
            if (r6 == 0) goto L_0x043c
            int r3 = X.AnonymousClass1Y3.AId
            X.0UN r2 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r8, r3, r2)
            X.3jb r2 = (X.C75293jb) r2
            com.google.common.collect.ImmutableList r15 = r2.A01(r6)
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()
            java.util.Iterator r14 = r9.iterator()
        L_0x034c:
            boolean r2 = r14.hasNext()
            if (r2 == 0) goto L_0x03e6
            java.lang.Object r8 = r14.next()
            com.facebook.ui.media.attachments.model.MediaResource r8 = (com.facebook.ui.media.attachments.model.MediaResource) r8
            X.06B r2 = r0.A01
            long r12 = r2.now()
            long r2 = r8.A05
            long r12 = r12 - r2
            r9 = 86400000(0x5265c00, double:4.2687272E-316)
            int r2 = (r12 > r9 ? 1 : (r12 == r9 ? 0 : -1))
            if (r2 > 0) goto L_0x034c
            X.28p r3 = r8.A0L
            X.28p r2 = X.C421828p.VIDEO
            if (r3 != r2) goto L_0x03df
            X.2Ul r12 = X.C47342Ul.VIDEO
        L_0x0370:
            java.lang.String r2 = r8.A03()
            if (r2 == 0) goto L_0x034c
            X.1TG r9 = com.facebook.messaging.model.messages.Message.A00()
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r8)
            r9.A09(r2)
            java.lang.String r2 = r8.A03()
            r9.A06(r2)
            long r2 = r8.A05
            r9.A04 = r2
            com.facebook.messaging.model.messages.Message r10 = r9.A00()
            X.3uj r9 = new X.3uj
            r9.<init>()
            r9.A03 = r12
            r9.A02 = r10
            java.lang.String r2 = r8.A03()
            r9.A07 = r2
            long r2 = r8.A05
            r9.A01 = r2
            long r2 = r8.A07
            r9.A00 = r2
            java.lang.String r3 = r10.A0q
            java.util.Iterator r10 = r15.iterator()
        L_0x03ad:
            boolean r8 = r10.hasNext()
            r2 = 0
            if (r8 == 0) goto L_0x03d4
            java.lang.Object r8 = r10.next()
            java.util.List r8 = (java.util.List) r8
            java.lang.Object r2 = r8.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x03ad
            r2 = 2
            java.lang.Object r2 = r8.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            byte r2 = java.lang.Byte.parseByte(r2)
            if (r2 != 0) goto L_0x03ad
            r2 = 1
        L_0x03d4:
            r9.A09 = r2
            com.facebook.messaging.montage.model.MontageCard r2 = r9.A00()
            r6.add(r2)
            goto L_0x034c
        L_0x03df:
            X.28p r2 = X.C421828p.PHOTO
            if (r3 != r2) goto L_0x034c
            X.2Ul r12 = X.C47342Ul.PHOTO
            goto L_0x0370
        L_0x03e6:
            com.google.common.collect.ImmutableList r2 = r6.build()
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x043c
            int r8 = r2.size()
            r6 = 0
            r3 = 0
        L_0x03f6:
            if (r3 >= r8) goto L_0x045c
            java.lang.Object r0 = r2.get(r3)
            com.facebook.messaging.montage.model.MontageCard r0 = (com.facebook.messaging.montage.model.MontageCard) r0
            java.lang.String r1 = r0.A06
            java.util.Iterator r10 = r15.iterator()
        L_0x0404:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x0459
            java.lang.Object r9 = r10.next()
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r0 = r9.get(r6)
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0404
            r0 = 2
            java.lang.Object r0 = r9.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            byte r0 = java.lang.Byte.parseByte(r0)
            if (r0 != 0) goto L_0x0404
            r1 = 0
        L_0x042a:
            if (r2 != 0) goto L_0x0451
            X.3ys r0 = new X.3ys
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.RegularImmutableList.A02
            r0.<init>(r1)
        L_0x0433:
            com.facebook.messaging.montage.model.MontageInboxNuxItem r1 = new com.facebook.messaging.montage.model.MontageInboxNuxItem
            com.google.common.collect.ImmutableList r2 = r0.A00
            boolean r0 = r0.A01
            r1.<init>(r2, r0)
        L_0x043c:
            r20 = r5
            r21 = r7
            r22 = r1
            r17 = r11
            r16 = r4
            r16.<init>(r17, r18, r19, r20, r21, r22, r23)
            com.facebook.messaging.inbox2.items.InboxUnitItem r4 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r4
            r0 = r25
            r0.A01(r4)
            return
        L_0x0451:
            X.3ys r0 = new X.3ys
            r0.<init>(r2)
            r0.A01 = r1
            goto L_0x0433
        L_0x0459:
            int r3 = r3 + 1
            goto L_0x03f6
        L_0x045c:
            r1 = 1
            goto L_0x042a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14570tc.A08(X.1lp, X.12V, X.1qC, X.0w2):void");
    }

    public void A09(C32481lp r7, AnonymousClass12V r8, boolean z) {
        ImmutableList A0T = r8.A01.A0T();
        if (!A0T.isEmpty()) {
            GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) A0T.get(0);
            if (gSTModelShape1S0000000.A36() != null) {
                MessageRequestsSnippet messageRequestsSnippet = new MessageRequestsSnippet(gSTModelShape1S0000000.A36().getIntValue(-407761836), gSTModelShape1S0000000.A36().getIntValue(1949198463), gSTModelShape1S0000000.A36().A0P(-2061635299), -1);
                if (z) {
                    A07(r7, r8);
                }
                r7.A01(new MessageRequestsBannerInboxItem(r8.A01, ((C05760aH) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AZY, this.A00)).getString(2131827396), messageRequestsSnippet));
            }
        }
    }

    public C14570tc(AnonymousClass1XY r3, String str) {
        this.A00 = new AnonymousClass0UN(19, r3);
        this.A03 = C14760ty.A00(r3);
        this.A05 = C14890uJ.A00(r3);
        this.A08 = C14770tz.A00(r3);
        this.A07 = AnonymousClass0u2.A00(r3);
        C14900uK.A00(r3);
        this.A06 = AnonymousClass0UU.A08(r3);
        this.A04 = FbSharedPreferencesModule.A00(r3);
        this.A02 = C11920oF.A00(r3);
        this.A09 = "MESSENGER_INBOX2".equals(str);
    }

    private static RankingLoggingItem A00(UserKey userKey, ImmutableList immutableList, ImmutableList immutableList2) {
        RankingLoggingItem rankingLoggingItem;
        C24971Xv it = immutableList.iterator();
        C24971Xv it2 = immutableList2.iterator();
        while (it.hasNext()) {
            UserKey userKey2 = (UserKey) it.next();
            if (it2.hasNext()) {
                rankingLoggingItem = (RankingLoggingItem) it2.next();
            } else {
                rankingLoggingItem = null;
            }
            if (rankingLoggingItem != null && userKey.equals(userKey2)) {
                return rankingLoggingItem;
            }
        }
        return new RankingLoggingItem(new AnonymousClass12J());
    }
}
