package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0bK  reason: invalid class name and case insensitive filesystem */
public final class C06330bK {
    private int A00;
    private ArrayList A01;
    private List A02;

    public synchronized List A00() {
        List list;
        list = this.A02;
        this.A00++;
        return list;
    }

    public synchronized void A01() {
        this.A00--;
    }

    public synchronized void A02(Object obj) {
        if (this.A00 > 0) {
            ArrayList arrayList = this.A01;
            int size = arrayList.size();
            ArrayList arrayList2 = new ArrayList(size + 1);
            this.A01 = arrayList2;
            this.A02 = Collections.unmodifiableList(arrayList2);
            for (int i = 0; i < size; i++) {
                this.A01.add(arrayList.get(i));
            }
        }
        this.A01.add(obj);
    }

    public synchronized void A03(Object obj) {
        if (!this.A01.contains(obj)) {
            A02(obj);
        }
    }

    public synchronized void A04(Object obj) {
        if (this.A00 <= 0) {
            this.A01.remove(obj);
        } else {
            int indexOf = this.A01.indexOf(obj);
            if (indexOf >= 0) {
                ArrayList arrayList = this.A01;
                int size = arrayList.size();
                ArrayList arrayList2 = new ArrayList(size - 1);
                this.A01 = arrayList2;
                this.A02 = Collections.unmodifiableList(arrayList2);
                for (int i = 0; i < size; i++) {
                    if (i != indexOf) {
                        this.A01.add(arrayList.get(i));
                    }
                }
            }
        }
    }

    public C06330bK() {
        ArrayList arrayList = new ArrayList();
        this.A01 = arrayList;
        this.A02 = Collections.unmodifiableList(arrayList);
    }
}
