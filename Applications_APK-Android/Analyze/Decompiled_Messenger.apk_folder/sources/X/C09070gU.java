package X;

/* renamed from: X.0gU  reason: invalid class name and case insensitive filesystem */
public final class C09070gU {
    public static volatile C09060gT A00;

    public static C09060gT A00() {
        if (A00 == null) {
            synchronized (C09070gU.class) {
                if (A00 == null) {
                    A00 = new AnonymousClass8X9();
                }
            }
        }
        return A00;
    }

    public static void A01(Integer num, String str, String str2) {
        A00().AYE(num, str, str2);
    }

    private C09070gU() {
    }
}
