package X;

import android.os.Handler;

/* renamed from: X.1pR  reason: invalid class name and case insensitive filesystem */
public final class C34351pR {
    private AnonymousClass0UN A00;
    private AnonymousClass1SF A01;
    private C34461pc A02;
    private C34471pd A03;
    public final C06230bA A04;
    public final AnonymousClass1YI A05;
    public final C16900xx A06;
    public final C34411pX A07;
    public final C34361pS A08;
    public final AnonymousClass0AQ A09;
    private final Handler A0A;
    private final AnonymousClass09P A0B;
    private final AnonymousClass0ZS A0C;
    private final AnonymousClass069 A0D;
    private final C34371pT A0E;
    private final C09940iw A0F;
    private final AnonymousClass0US A0G;
    private final C33821oC A0H;
    private final C21041Eu A0I;
    private final C25051Yd A0J;
    private final C34481pe A0K;
    private final C34321pO A0L;
    private final C34331pP A0M;
    private final AnonymousClass13w A0N;
    private final C34391pV A0O;
    private final C11330mk A0P = this.A0Q.A01("mqtt_instance");
    private final AnonymousClass1CU A0Q;
    private final C34401pW A0R;
    private final C04310Tq A0S;
    private final C04310Tq A0T;

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x008d, code lost:
        if (r0.A0F.A02() != false) goto L_0x008f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(android.content.Context r66, X.AnonymousClass0AX r67, java.lang.Integer r68, X.C34261pE r69, X.AnonymousClass0AG r70) {
        /*
            r65 = this;
            r60 = 0
            X.1pc r1 = new X.1pc
            r1.<init>()
            r0 = r65
            r0.A02 = r1
            X.1pd r15 = new X.1pd
            int r3 = X.AnonymousClass1Y3.AZW
            X.0UN r2 = r0.A00
            r1 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r3, r2)
            java.util.concurrent.ExecutorService r5 = (java.util.concurrent.ExecutorService) r5
            X.1YI r3 = r0.A05
            r2 = 848(0x350, float:1.188E-42)
            boolean r18 = r3.AbO(r2, r1)
            X.1YI r3 = r0.A05
            r2 = 306(0x132, float:4.29E-43)
            boolean r19 = r3.AbO(r2, r1)
            X.1YI r3 = r0.A05
            r2 = 444(0x1bc, float:6.22E-43)
            boolean r20 = r3.AbO(r2, r1)
            X.0US r4 = r0.A0G
            X.1YI r3 = r0.A05
            r2 = 445(0x1bd, float:6.24E-43)
            boolean r22 = r3.AbO(r2, r1)
            r23 = 0
            r16 = r66
            r17 = r5
            r21 = r4
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23)
            r0.A03 = r15
            X.1ph r5 = new X.1ph
            r4 = r69
            r5.<init>(r4)
            X.1oW r13 = new X.1oW
            r13.<init>(r0)
            X.1pi r12 = new X.1pi
            r12.<init>(r0)
            X.1SF r3 = new X.1SF
            X.1pT r2 = r0.A0E
            r3.<init>(r2, r5)
            r0.A01 = r3
            X.1pj r11 = new X.1pj
            r11.<init>(r0)
            X.1YI r3 = r0.A05
            r2 = 848(0x350, float:1.188E-42)
            boolean r2 = r3.AbO(r2, r1)
            if (r2 == 0) goto L_0x020e
            X.1YI r3 = r0.A05
            r2 = 299(0x12b, float:4.19E-43)
            boolean r45 = r3.AbO(r2, r1)
        L_0x0078:
            X.1Yd r6 = r0.A0J
            r2 = 282342560105821(0x100ca0000055d, double:1.394957593071534E-309)
            boolean r2 = r6.Aem(r2)
            if (r2 == 0) goto L_0x008f
            X.0iw r2 = r0.A0F
            boolean r2 = r2.A02()
            r62 = 1
            if (r2 == 0) goto L_0x0091
        L_0x008f:
            r62 = 0
        L_0x0091:
            X.0ZS r2 = r0.A0C
            boolean r2 = r2.A09()
            if (r2 == 0) goto L_0x020b
            X.0ZS r6 = r0.A0C
            java.lang.String r7 = r6.A03()
            int r3 = X.AnonymousClass1Y3.B4l
            X.0UN r2 = r6.A00
            X.AnonymousClass1XX.A03(r3, r2)
            boolean r2 = r6.A09()
            if (r2 == 0) goto L_0x00d4
            int r3 = X.AnonymousClass1Y3.BTG
            X.0UN r2 = r6.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r3, r2)
            X.8B1 r2 = (X.AnonymousClass8B1) r2
            X.2U1 r2 = r2.A02()
            java.lang.Integer r3 = X.AnonymousClass8B1.A01(r2)
            java.lang.Integer r2 = X.AnonymousClass07B.A0C
            if (r3 != r2) goto L_0x0203
            java.lang.String r8 = "Qaag"
        L_0x00c4:
            r2 = 2
            java.lang.String r3 = "_"
            java.lang.String[] r7 = r7.split(r3, r2)
            r6 = r7[r1]
            r2 = 1
            r2 = r7[r2]
            java.lang.String r7 = X.AnonymousClass08S.A0T(r6, r3, r8, r3, r2)
        L_0x00d4:
            X.1pS r3 = r0.A08
            r22 = r3
            X.0AQ r2 = r0.A09
            r23 = r2
            X.1pc r2 = r0.A02
            r24 = r2
            X.13w r2 = r0.A0N
            r25 = r2
            X.1pl r10 = new X.1pl
            r10.<init>(r5)
            X.0Tq r2 = r0.A0T
            X.1pl r9 = new X.1pl
            r9.<init>(r2)
            android.os.Handler r2 = r0.A0A
            r28 = r2
            X.1SF r2 = r0.A01
            r29 = r2
            X.1pP r2 = r0.A0M
            r30 = r2
            X.09P r2 = r0.A0B
            r31 = r2
            X.0Tq r2 = r0.A0S
            X.1pl r8 = new X.1pl
            r8.<init>(r2)
            X.1pV r2 = r0.A0O
            r37 = r2
            X.1pd r2 = r0.A03
            r38 = r2
            X.1pW r15 = r0.A0R
            java.lang.String r40 = r3.AdF()
            X.1YI r3 = r0.A05
            r2 = 304(0x130, float:4.26E-43)
            boolean r43 = r3.AbO(r2, r1)
            X.1YI r3 = r0.A05
            r2 = 305(0x131, float:4.27E-43)
            boolean r44 = r3.AbO(r2, r1)
            X.0mk r14 = r0.A0P
            X.1Yd r5 = r0.A0J
            r2 = 282342560236895(0x100ca0002055f, double:1.394957593719126E-309)
            boolean r49 = r5.Aem(r2)
            X.1Yd r5 = r0.A0J
            r2 = 845292513788035(0x300ca00040083, double:4.176299917494563E-309)
            java.lang.String r50 = r5.B4C(r2)
            X.1Yd r5 = r0.A0J
            r2 = 282342560630114(0x100ca00080562, double:1.394957595661886E-309)
            boolean r52 = r5.Aem(r2)
            X.1Yd r6 = r0.A0J
            r2 = 563817537208926(0x200ca0006025e, double:2.78562875657749E-309)
            r5 = 60
            int r53 = r6.AqL(r2, r5)
            X.1Yd r5 = r0.A0J
            r2 = 285898793424832(0x10406000617c0, double:1.41252772018673E-309)
            boolean r54 = r5.Aem(r2)
            X.1Yd r5 = r0.A0J
            r2 = 285898793228223(0x10406000317bf, double:1.41252771921535E-309)
            boolean r55 = r5.Aem(r2)
            X.1Yd r3 = r0.A0J
            r5 = 567373770065809(0x2040600050791, double:2.80319888140947E-309)
            r2 = -1
            int r56 = r3.AqL(r5, r2)
            X.1Yd r3 = r0.A0J
            r5 = 567373770196882(0x2040600070792, double:2.803198882057055E-309)
            int r57 = r3.AqL(r5, r2)
            X.1Yd r3 = r0.A0J
            r5 = 567373770000272(0x2040600040790, double:2.803198881085673E-309)
            int r58 = r3.AqL(r5, r1)
            X.1Yd r3 = r0.A0J
            r5 = 567373770262419(0x2040600080793, double:2.80319888238085E-309)
            int r59 = r3.AqL(r5, r2)
            X.1YI r3 = r0.A05
            r2 = 302(0x12e, float:4.23E-43)
            boolean r61 = r3.AbO(r2, r1)
            X.1pe r5 = r0.A0K
            X.1YI r3 = r0.A05
            r2 = 118(0x76, float:1.65E-43)
            boolean r63 = r3.AbO(r2, r1)
            X.0Ac r1 = new X.0Ac
            r17 = r1
            r33 = 0
            r36 = 1
            r46 = 1
            r47 = 0
            r51 = 0
            r19 = r68
            r21 = r70
            r18 = r16
            r20 = r4
            r26 = r10
            r27 = r9
            r32 = r11
            r34 = r8
            r35 = r13
            r39 = r15
            r41 = r12
            r42 = r5
            r48 = r14
            r64 = r7
            r17.<init>(r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64)
            java.util.List r2 = java.util.Collections.emptyList()
            r3 = r67
            r3.A01(r1, r2)
            X.1po r6 = new X.1po
            X.0Ap r2 = r3.A0F
            X.069 r1 = r0.A0D
            r6.<init>(r2, r1)
            X.1pp r5 = new X.1pp
            r5.<init>(r0)
            X.1pX r3 = r0.A07
            X.1oC r2 = r0.A0H
            X.1Eu r1 = r0.A0I
            X.1pO r0 = r0.A0L
            r4.A02 = r3
            r4.A04 = r5
            r4.A03 = r6
            r4.A00 = r2
            r4.A01 = r1
            r4.A05 = r0
            return
        L_0x0203:
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            if (r3 != r2) goto L_0x00d4
            java.lang.String r8 = "Qaau"
            goto L_0x00c4
        L_0x020b:
            r7 = 0
            goto L_0x00d4
        L_0x020e:
            r45 = 0
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34351pR.A00(android.content.Context, X.0AX, java.lang.Integer, X.1pE, X.0AG):void");
    }

    public C34351pR(AnonymousClass1XY r3, AnonymousClass0AQ r4) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A08 = new C34361pS(r3);
        this.A0N = AnonymousClass13w.A00(r3);
        this.A0B = C04750Wa.A01(r3);
        this.A0E = C34371pT.A01(r3);
        this.A0O = C34391pV.A00(r3);
        this.A0R = C34401pW.A00(r3);
        this.A06 = C16900xx.A00(r3);
        this.A05 = AnonymousClass0WA.A00(r3);
        this.A0G = AnonymousClass0UQ.A00(AnonymousClass1Y3.AOm, r3);
        this.A04 = AnonymousClass1ZF.A02(r3);
        this.A0T = AnonymousClass0VG.A00(AnonymousClass1Y3.Aw9, r3);
        this.A0S = AnonymousClass0VG.A00(AnonymousClass1Y3.B66, r3);
        this.A07 = new C34411pX(r3);
        this.A0A = C29321gE.A00(r3);
        this.A0Q = AnonymousClass1CU.A00(r3);
        this.A0J = AnonymousClass0WT.A00(r3);
        this.A0H = C33821oC.A00(r3);
        this.A0I = C21041Eu.A00(r3);
        this.A0K = new C34481pe(r3);
        this.A0L = C34321pO.A00(r3);
        this.A0M = C34331pP.A00(r3);
        this.A0F = new C09940iw(r3);
        this.A0D = AnonymousClass067.A03(r3);
        this.A0C = AnonymousClass0ZS.A00(r3);
        this.A09 = r4;
    }
}
