package X;

import com.facebook.http.common.BootstrapRequestName;

/* renamed from: X.0YI  reason: invalid class name */
public abstract class AnonymousClass0YI implements AnonymousClass0YJ {
    public void A01(AnonymousClass1YI r7, int i) {
        if (this instanceof C31861kZ) {
            C31861kZ r1 = (C31861kZ) this;
            if (i == 582) {
                r1.A00.setZeroRatingEnabled(r7.AbO(i, false));
            } else if (i == 852) {
                r1.A00.setUseBackupRewriteRules(r7.AbO(i, true));
            } else if (i == 140) {
                r1.A00.setUseSessionlessBackupRewriteRules(r7.AbO(i, true));
            } else if (i != 839) {
            } else {
                if (r7.AbO(i, true)) {
                    r1.A00.setDefaultBootstrapRequests(BootstrapRequestName.A00);
                } else {
                    r1.A00.setDefaultBootstrapRequests(null);
                }
            }
        } else if (this instanceof AnonymousClass1Ui) {
            AnonymousClass1Ui r12 = (AnonymousClass1Ui) this;
            r12.A02.A02(r12);
        } else if (this instanceof C25391Zl) {
            C25391Zl r3 = (C25391Zl) this;
            if (r3 instanceof C06000ag) {
                C06000ag r32 = (C06000ag) r3;
                int i2 = 0;
                while (true) {
                    int[][] iArr = r32.A01;
                    if (i2 < iArr.length) {
                        int length = iArr[i2].length;
                        int i3 = 0;
                        while (true) {
                            if (i3 >= length) {
                                break;
                            } else if (iArr[i2][i3] != i) {
                                i3++;
                            } else if (i2 == 0) {
                                ((C198129Tk) AnonymousClass1XX.A03(AnonymousClass1Y3.AVt, r32.A00)).A01(r7, i);
                            } else if (i2 == 1) {
                                ((C126295wd) AnonymousClass1XX.A03(AnonymousClass1Y3.A0P, r32.A00)).A01(r7, i);
                            } else if (i2 == 2) {
                                ((C198149Tm) AnonymousClass1XX.A03(AnonymousClass1Y3.AxR, r32.A00)).A01(r7, i);
                            } else if (i2 == 3) {
                                ((C205589mZ) AnonymousClass1XX.A03(AnonymousClass1Y3.AwB, r32.A00)).A01(r7, i);
                            } else if (i2 == 4) {
                                ((C89964Re) AnonymousClass1XX.A03(AnonymousClass1Y3.Auk, r32.A00)).A01(r7, i);
                            } else if (i2 == 5) {
                                ((C89954Rd) AnonymousClass1XX.A03(AnonymousClass1Y3.AJ4, r32.A00)).A01(r7, i);
                            } else {
                                throw new UnsupportedOperationException("Something went wrong and this listener is missing from the generated code");
                            }
                        }
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        } else if (!(this instanceof C05610Zr)) {
            C04980Xe.A01(((AnonymousClass0YF) this).A00);
        } else {
            C05610Zr r13 = (C05610Zr) this;
            if (r7.AbO(i, false)) {
                C05820aO.A01(r13.A00);
                return;
            }
            C05820aO r2 = r13.A00;
            C013309u.A00 = null;
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r2.A00)).C1g("pending_foreground_services");
            r2.A01.clear();
        }
    }

    public void ARA(Object obj, Object obj2) {
        A01((AnonymousClass1YI) obj, ((Integer) obj2).intValue());
    }
}
