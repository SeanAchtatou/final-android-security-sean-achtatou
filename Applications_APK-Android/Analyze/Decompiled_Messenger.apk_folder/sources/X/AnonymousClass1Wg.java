package X;

import java.util.Set;

/* renamed from: X.1Wg  reason: invalid class name */
public final /* synthetic */ class AnonymousClass1Wg implements C24551Wa {
    public static final AnonymousClass1Wg A00 = new AnonymousClass1Wg();

    private AnonymousClass1Wg() {
    }

    public Object AUm(AnonymousClass1WU r5) {
        Set A03 = r5.A03(C24571Wc.class);
        C24691Ws r0 = C24691Ws.A01;
        if (r0 == null) {
            synchronized (C24691Ws.class) {
                r0 = C24691Ws.A01;
                if (r0 == null) {
                    r0 = new C24691Ws();
                    C24691Ws.A01 = r0;
                }
            }
        }
        return new C24591We(A03, r0);
    }
}
