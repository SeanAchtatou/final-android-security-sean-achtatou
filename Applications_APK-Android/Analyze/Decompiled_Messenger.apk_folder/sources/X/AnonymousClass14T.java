package X;

import android.content.Context;
import android.view.LayoutInflater;

/* renamed from: X.14T  reason: invalid class name */
public abstract class AnonymousClass14T extends AnonymousClass14U implements LayoutInflater.Factory {
    public AnonymousClass6b3 A00;
    public final Context A01;
    public final LayoutInflater A02;

    public AnonymousClass14T(Context context) {
        super(context);
        this.A01 = context;
        LayoutInflater cloneInContext = LayoutInflater.from(context).cloneInContext(context);
        this.A02 = cloneInContext;
        if (cloneInContext.getFactory() == null) {
            this.A02.setFactory(this);
        }
    }
}
