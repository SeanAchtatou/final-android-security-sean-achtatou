package X;

import android.hardware.Camera;

/* renamed from: X.0QK  reason: invalid class name */
public final class AnonymousClass0QK {
    static {
        AnonymousClass0FQ.A03 = true;
    }

    public static Camera A00(int i) {
        Camera open = Camera.open(i);
        if (AnonymousClass0FQ.A03()) {
            AnonymousClass0FQ.A02(open);
        }
        return open;
    }

    public static void A01(Camera camera) {
        camera.release();
        if (AnonymousClass0FQ.A03()) {
            AnonymousClass0FQ.A00(camera);
        }
    }

    public static void A02(Camera camera) {
        camera.startPreview();
        if (AnonymousClass0FQ.A03()) {
            try {
                AnonymousClass0FQ.A02.readLock().lock();
                int size = AnonymousClass0FQ.A01.size();
                for (int i = 0; i < size; i++) {
                    ((AnonymousClass0FU) AnonymousClass0FQ.A01.get(i)).Bpi(camera);
                }
            } finally {
                AnonymousClass0FQ.A02.readLock().unlock();
            }
        }
    }

    public static void A03(Camera camera) {
        camera.stopPreview();
        if (AnonymousClass0FQ.A03()) {
            try {
                AnonymousClass0FQ.A02.readLock().lock();
                int size = AnonymousClass0FQ.A01.size();
                for (int i = 0; i < size; i++) {
                    ((AnonymousClass0FU) AnonymousClass0FQ.A01.get(i)).Bq9(camera);
                }
            } finally {
                AnonymousClass0FQ.A02.readLock().unlock();
            }
        }
    }
}
