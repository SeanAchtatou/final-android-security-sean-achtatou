package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.18A  reason: invalid class name */
public final class AnonymousClass18A extends AnonymousClass18B {
    private static volatile AnonymousClass18A A00;

    public AnonymousClass18A() {
        super(new AnonymousClass18C(), C30241hj.A00());
    }

    public static final AnonymousClass18A A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass18A.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass18A();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
