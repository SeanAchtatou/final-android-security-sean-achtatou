package X;

import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.google.common.collect.ArrayListMultimap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/* renamed from: X.1jo  reason: invalid class name and case insensitive filesystem */
public abstract class C31511jo {
    public Collection A01() {
        Collection collection;
        AnonymousClass1F7 r1 = (AnonymousClass1F7) this;
        synchronized (r1) {
            if (r1.A01 == null) {
                String[] strArr = new String[90];
                System.arraycopy(new String[]{"6199", "6504", "7639", "7453", "7657", "7629", "7640", "7227", "6732", "7051", "6683", "7072", "6812", "6849", "3931", "7066", "7595", "6762", "6739", "5650", "5665", "5677", "5933", "6553", "6522", "6563", "6005"}, 0, strArr, 0, 27);
                System.arraycopy(new String[]{"6006", "5560", "5566", "5629", "5565", "7464", "6938", "5798", "5290", "5292", "5291", "4828", "7004", "7615", "4745", "4744", "4743", "2415", "4227", "4408", "3543", "3545", "5579", "5411", "7149", "6383", "7246"}, 0, strArr, 27, 27);
                System.arraycopy(new String[]{"6935", "7126", "7058", "6367", "6042", "6570", "6437", "7279", "6144", "6306", "7146", "6513", "6756", "4348", "7165", "7198", "7199", "7190", "7543", "7654", "6122", "5877", "5801", "4541", "6473", "5399", "7435"}, 0, strArr, 54, 27);
                System.arraycopy(new String[]{"1820", "4320", "6544", "6389", "4757", "5131", "6699", "7635", "6150"}, 0, strArr, 81, 9);
                r1.A01 = Collections.unmodifiableList(Arrays.asList(strArr));
            }
            collection = r1.A01;
        }
        return collection;
    }

    public Collection A02() {
        Collection collection;
        AnonymousClass1F7 r2 = (AnonymousClass1F7) this;
        synchronized (r2) {
            if (r2.A02 == null) {
                r2.A02 = Collections.unmodifiableList(Arrays.asList("6849", "3931"));
            }
            collection = r2.A02;
        }
        return collection;
    }

    public Collection A03(InterstitialTrigger.Action action) {
        Collection AbK;
        AnonymousClass1F7 r2 = (AnonymousClass1F7) this;
        synchronized (r2) {
            if (r2.A00 == null) {
                ArrayListMultimap arrayListMultimap = new ArrayListMultimap();
                r2.A00 = arrayListMultimap;
                arrayListMultimap.Byx(InterstitialTrigger.Action.A3U, "4828");
                r2.A00.Byx(InterstitialTrigger.Action.A3Y, "7004");
                r2.A00.Byx(InterstitialTrigger.Action.A3Z, "7615");
                r2.A00.Byx(InterstitialTrigger.Action.A43, "4745");
                r2.A00.Byx(InterstitialTrigger.Action.A44, "4744");
                r2.A00.Byx(InterstitialTrigger.Action.A45, "4743");
                r2.A00.Byx(InterstitialTrigger.Action.A3R, "2415");
                r2.A00.Byx(InterstitialTrigger.Action.A3h, "2415");
                r2.A00.Byx(InterstitialTrigger.Action.A46, "2415");
                r2.A00.Byx(InterstitialTrigger.Action.A3l, "2415");
                r2.A00.Byx(InterstitialTrigger.Action.A3V, "4227");
                r2.A00.Byx(InterstitialTrigger.Action.A3m, "4408");
                r2.A00.Byx(InterstitialTrigger.Action.A3u, "3543");
                r2.A00.Byx(InterstitialTrigger.Action.A3w, "3543");
                r2.A00.Byx(InterstitialTrigger.Action.A3x, "3545");
                r2.A00.Byx(InterstitialTrigger.Action.A3y, "3545");
                r2.A00.Byx(InterstitialTrigger.Action.A3T, "3545");
                r2.A00.Byx(InterstitialTrigger.Action.A3o, "3545");
                r2.A00.Byx(InterstitialTrigger.Action.A3U, "5579");
                r2.A00.Byx(InterstitialTrigger.Action.A3u, "5411");
                r2.A00.Byx(InterstitialTrigger.Action.A3w, "5411");
                r2.A00.Byx(InterstitialTrigger.Action.A7x, "7149");
                r2.A00.Byx(InterstitialTrigger.Action.A6H, "6383");
                r2.A00.Byx(InterstitialTrigger.Action.A6K, "7246");
                r2.A00.Byx(InterstitialTrigger.Action.A6U, "6935");
                r2.A00.Byx(InterstitialTrigger.Action.A6V, "7126");
                r2.A00.Byx(InterstitialTrigger.Action.A6I, "7058");
                r2.A00.Byx(InterstitialTrigger.Action.A6S, "6367");
                r2.A00.Byx(InterstitialTrigger.Action.A6M, "6042");
                r2.A00.Byx(InterstitialTrigger.Action.A6T, "6570");
                r2.A00.Byx(InterstitialTrigger.Action.A6O, "6437");
                r2.A00.Byx(InterstitialTrigger.Action.A6Q, "7279");
                r2.A00.Byx(InterstitialTrigger.Action.A6P, "6144");
                r2.A00.Byx(InterstitialTrigger.Action.A6J, "6306");
                r2.A00.Byx(InterstitialTrigger.Action.A6R, "7146");
                r2.A00.Byx(InterstitialTrigger.Action.A7y, "6513");
                r2.A00.Byx(InterstitialTrigger.Action.A7w, "6756");
                r2.A00.Byx(InterstitialTrigger.Action.A3p, "7165");
                r2.A00.Byx(InterstitialTrigger.Action.A3q, "7198");
                r2.A00.Byx(InterstitialTrigger.Action.A3r, "7199");
                r2.A00.Byx(InterstitialTrigger.Action.A3s, "7190");
                r2.A00.Byx(InterstitialTrigger.Action.A7j, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A2M, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A7c, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A7h, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A7l, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A5V, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A5W, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A5p, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A5m, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A5n, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A5o, "1820");
                r2.A00.Byx(InterstitialTrigger.Action.A63, "1820");
            }
            AbK = r2.A00.AbK(action);
        }
        return AbK;
    }

    public abstract C32821mO A04(String str);
}
