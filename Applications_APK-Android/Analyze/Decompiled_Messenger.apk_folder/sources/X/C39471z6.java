package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.1z6  reason: invalid class name and case insensitive filesystem */
public final class C39471z6 extends AnimatorListenerAdapter {
    public final /* synthetic */ C28411DuT A00;

    public C39471z6(C28411DuT duT) {
        this.A00 = duT;
    }

    public void onAnimationEnd(Animator animator) {
        C28411DuT duT = this.A00;
        duT.A02.BgR(duT.A00);
    }
}
