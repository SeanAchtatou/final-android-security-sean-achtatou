package X;

import android.content.Context;
import android.content.Intent;
import java.util.Map;

/* renamed from: X.1m9  reason: invalid class name and case insensitive filesystem */
public final class C32681m9 implements AnonymousClass06U, AnonymousClass1LY {
    private C02730Gc A00;
    private C02770Gh A01;
    private C02910Gx A02;
    private C22141Kb A03;
    private Boolean A04 = null;
    private final C06230bA A05;
    private final C04460Ut A06;
    private final AnonymousClass1YI A07;
    private volatile String A08 = "unknown";
    private volatile boolean A09 = true;
    private volatile boolean A0A = false;

    private synchronized boolean A02() {
        Boolean bool = this.A04;
        if (bool != null) {
            return bool.booleanValue();
        }
        Boolean valueOf = Boolean.valueOf(this.A07.AbO(26, false));
        this.A04 = valueOf;
        if (valueOf.booleanValue()) {
            C02760Gg A012 = A01();
            C56072pD.A00().A04(A012);
            this.A00 = new C02730Gc(C56072pD.A00(), A01(), A012, A01());
            this.A01 = C56072pD.A01();
            this.A03 = new C22141Kb();
            this.A02 = new C51852hy(new C188338ol(this.A05).A00);
            C06600bl BMm = this.A06.BMm();
            BMm.A02("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP", this);
            BMm.A00().A00();
        }
        return this.A04.booleanValue();
    }

    public synchronized void BgD(String str, String str2, Map map) {
        if (A02()) {
            if (str == null) {
                if (this.A09) {
                    str = "cold_start";
                    if (str2 != null) {
                        this.A09 = false;
                    }
                } else if (this.A0A) {
                    str = "user_left_app";
                    this.A0A = false;
                } else {
                    str = "unknown";
                }
            }
            if (str2 != null) {
                this.A08 = str2;
            }
            C02760Gg r2 = (C02760Gg) this.A00.A01();
            C02760Gg r0 = (C02760Gg) this.A03.get(str);
            if (r0 == null) {
                C02760Gg A012 = A01();
                A012.A0A(r2);
                this.A03.put(str, A012);
            } else {
                r0.A0B(r2, r0);
            }
        }
    }

    public synchronized void Bl1(Context context, Intent intent, AnonymousClass06Y r9) {
        int A002 = AnonymousClass09Y.A00(-1048264407);
        if (!A02()) {
            AnonymousClass09Y.A01(986599800, A002);
        } else {
            this.A0A = true;
            BgD(this.A08, null, null);
            int A062 = this.A03.A06(-1);
            while (A062 >= 0) {
                this.A03.A07(A062);
                this.A03.A08(A062);
                this.A02.ALe(null, "mobile_power_stats_nav_attribution");
                C02910Gx r2 = this.A02;
                if (!r2.isSampled()) {
                    break;
                }
                r2.AMW("dimension", (String) this.A03.A07(A062));
                this.A01.C2x((C02760Gg) this.A03.A08(A062), this.A02);
                this.A02.BIw();
                A062 = this.A03.A06(A062);
            }
            AnonymousClass09Y.A01(-1821178087, A002);
        }
    }

    public static final C32681m9 A00(AnonymousClass1XY r1) {
        return new C32681m9(r1);
    }

    private static C02760Gg A01() {
        C02760Gg r3 = new C02760Gg();
        r3.A0C(AnonymousClass0FV.class, new AnonymousClass0FV());
        r3.A0C(C02520Fg.class, new C02520Fg());
        r3.A0C(C02500Fe.class, new C02500Fe(false));
        r3.A0C(C02570Fl.class, new C02570Fl());
        r3.A0C(AnonymousClass0FL.class, new AnonymousClass0FL());
        r3.A0C(AnonymousClass0FP.class, new AnonymousClass0FP());
        return r3;
    }

    private C32681m9(AnonymousClass1XY r2) {
        this.A05 = AnonymousClass1ZF.A02(r2);
        this.A06 = C04430Uq.A02(r2);
        this.A07 = AnonymousClass0WA.A00(r2);
    }
}
