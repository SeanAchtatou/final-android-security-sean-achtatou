package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1YZ  reason: invalid class name */
public final class AnonymousClass1YZ {
    private static volatile AnonymousClass1YZ A01;
    public final C25051Yd A00;

    public static final AnonymousClass1YZ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1YZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1YZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass1YZ(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
