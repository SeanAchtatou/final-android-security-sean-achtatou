package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.view.Choreographer;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.logger.Logger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.04q  reason: invalid class name */
public final class AnonymousClass04q extends C000900o {
    public static final int A0D = ProvidersRegistry.A00.A02("frames");
    public final Runnable A00 = new AnonymousClass05S(this);
    public final AtomicBoolean A01 = new AtomicBoolean(false);
    private final Context A02;
    private final Handler A03;
    private final AnonymousClass05U A04;
    private final Runnable A05 = new AnonymousClass05R(this);
    private final Runnable A06 = new AnonymousClass05Q(this);
    public volatile double A07 = 0.0d;
    public volatile int A08 = 0;
    public volatile long A09 = 0;
    public volatile long A0A = -1;
    private volatile long A0B;
    private volatile AnonymousClass05V A0C = null;

    public AnonymousClass04q(Context context) {
        super(null);
        this.A02 = context;
        this.A03 = new Handler(Looper.getMainLooper());
        this.A04 = new AnonymousClass05T(this);
    }

    private static void A00(int i, long j) {
        Logger.writeStandardEntry(0, 7, 52, 0, 0, i, 0, j);
    }

    public boolean A08() {
        return true;
    }

    static {
        TimeUnit.NANOSECONDS.convert(1, TimeUnit.MILLISECONDS);
    }

    public static void A01(AnonymousClass04q r14) {
        AnonymousClass05V r3 = r14.A0C;
        if (r3 != null) {
            A00(8126532, r14.A0A);
            A00(8126533, (long) r14.A08);
            A00(8126534, r14.A09);
            Logger.writeBytesEntry(0, 1, 57, Logger.writeBytesEntry(0, 1, 56, Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126535, 0, 0), "4_frame_drop_uncapped"), String.valueOf(r14.A07));
            r3.disable();
        }
    }

    public static void A02(AnonymousClass04q r5) {
        AnonymousClass05V r0;
        if (r5.A01.get() && r5.A0A != -1 && (r0 = r5.A0C) != null) {
            r0.enable();
        }
    }

    public static void A03(AnonymousClass04q r3) {
        if (r3.A0C == null) {
            r3.A0C = new C04250Te(Choreographer.getInstance(), r3.A04);
        }
    }

    public static void A04(String str) {
        Logger.writeBytesEntry(A0D, 1, 83, Logger.writeStandardEntry(A0D, 7, 22, 0, 0, 0, 0, 0), str);
        Logger.writeStandardEntry(A0D, 7, 23, 0, 0, 0, 0, 0);
    }

    public int getTracingProviders() {
        if (this.A01.get()) {
            return A0D;
        }
        return 0;
    }

    public void disable() {
        int A032 = C000700l.A03(365365400);
        AnonymousClass05V r2 = this.A0C;
        if (!this.A01.getAndSet(false)) {
            C000700l.A09(-536166528, A032);
            return;
        }
        if (r2 == null) {
            AnonymousClass00S.A04(this.A03, this.A05, 853907308);
        } else {
            A01(this);
        }
        C000700l.A09(793906493, A032);
    }

    public void enable() {
        int A032 = C000700l.A03(2007067407);
        if (this.A01.getAndSet(true)) {
            C000700l.A09(923728031, A032);
            return;
        }
        if (this.A0A == -1) {
            if (AnonymousClass05W.A01 == null) {
                AnonymousClass05W.A01 = new AnonymousClass05W();
            }
            this.A0A = AnonymousClass05W.A01.A00(this.A02);
        }
        if (this.A0C != null) {
            A02(this);
        } else {
            boolean z = false;
            if (Process.myTid() == Process.myPid()) {
                z = true;
            }
            if (z) {
                A03(this);
                A02(this);
            } else {
                AnonymousClass00S.A04(this.A03, this.A06, -478382277);
            }
        }
        C000700l.A09(1410611369, A032);
    }

    public int getSupportedProviders() {
        return A0D;
    }
}
