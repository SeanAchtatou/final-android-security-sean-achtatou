package X;

import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0Ct  reason: invalid class name */
public final class AnonymousClass0Ct {
    private static final AnonymousClass0BZ A03 = AnonymousClass0BZ.A00("delivery_helper");
    public AtomicLong A00 = new AtomicLong(300000);
    public final SharedPreferences A01;
    private final boolean A02;

    public synchronized long A01(String str) {
        long j;
        AnonymousClass0PW A002;
        j = -1;
        String str2 = null;
        try {
            str2 = this.A01.getString(str, null);
        } catch (ClassCastException e) {
            C010708t.A0U("NotificationDeliveryStoreSharedPreferences", e, "fail to read notifId %s", str);
        }
        if (!(str2 == null || (A002 = AnonymousClass0PW.A00(str2)) == null)) {
            j = A002.A00 - A002.A01;
        }
        A00(this, this.A01.edit().remove(str));
        return j;
    }

    public synchronized void A02(String str, Intent intent) {
        String str2 = str;
        if (!this.A01.contains(str)) {
            long currentTimeMillis = System.currentTimeMillis();
            String A012 = new AnonymousClass0PW(intent, str2, currentTimeMillis, currentTimeMillis).A01();
            if (!TextUtils.isEmpty(A012)) {
                A00(this, this.A01.edit().putString(str, A012));
            }
        }
    }

    public static void A00(AnonymousClass0Ct r2, SharedPreferences.Editor editor) {
        if (r2.A02) {
            AnonymousClass07A.A04(A03, new AnonymousClass0SF(editor), -1470250264);
        } else {
            AnonymousClass0B4.A00(editor);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0024, code lost:
        if (r4.A01 == false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0Ct(android.content.Context r6, java.lang.String r7, boolean r8) {
        /*
            r5 = this;
            r5.<init>()
            java.util.concurrent.atomic.AtomicLong r2 = new java.util.concurrent.atomic.AtomicLong
            r0 = 300000(0x493e0, double:1.482197E-318)
            r2.<init>(r0)
            r5.A00 = r2
            X.0B3 r4 = new X.0B3
            java.lang.String r0 = "rti.mqtt.fbns_notification_store_"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r7)
            r0 = 0
            r4.<init>(r1, r0)
            X.0B4 r3 = X.AnonymousClass0B4.A00
            java.lang.String r2 = r4.A00
            boolean r0 = X.AnonymousClass0B2.A00
            if (r0 == 0) goto L_0x0026
            boolean r1 = r4.A01
            r0 = 1
            if (r1 != 0) goto L_0x0027
        L_0x0026:
            r0 = 0
        L_0x0027:
            android.content.SharedPreferences r0 = r3.A01(r6, r2, r0)
            r5.A01 = r0
            r5.A02 = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Ct.<init>(android.content.Context, java.lang.String, boolean):void");
    }
}
