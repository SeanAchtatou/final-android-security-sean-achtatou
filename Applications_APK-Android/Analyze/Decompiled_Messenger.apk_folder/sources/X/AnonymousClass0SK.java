package X;

/* renamed from: X.0SK  reason: invalid class name */
public class AnonymousClass0SK extends C03550Om {
    private final C03550Om[] A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0SK(X.C03550Om... r7) {
        /*
            r6 = this;
            int r5 = r7.length
            r2 = 0
            r4 = 0
        L_0x0004:
            if (r4 >= r5) goto L_0x000e
            r0 = r7[r4]
            long r0 = r0.A00
            long r2 = r2 + r0
            int r4 = r4 + 1
            goto L_0x0004
        L_0x000e:
            android.view.animation.LinearInterpolator r0 = new android.view.animation.LinearInterpolator
            r0.<init>()
            r6.<init>(r0, r2)
            r6.A00 = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0SK.<init>(X.0Om[]):void");
    }

    public void A00(float f) {
        float f2 = f * ((float) this.A00);
        for (C03550Om r4 : this.A00) {
            float f3 = (float) r4.A00;
            if (f2 < f3) {
                r4.A00(r4.A01.getInterpolation(f2 / f3));
                return;
            }
            r4.A00(r4.A01.getInterpolation(1.0f));
            f2 -= (float) r4.A00;
        }
    }
}
