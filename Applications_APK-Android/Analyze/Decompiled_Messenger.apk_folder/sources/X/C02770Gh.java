package X;

/* renamed from: X.0Gh  reason: invalid class name and case insensitive filesystem */
public final class C02770Gh implements C02780Gi {
    public final AnonymousClass04b A00 = new AnonymousClass04b();

    /* renamed from: A00 */
    public void C2x(C02760Gg r5, C02910Gx r6) {
        int i = 0;
        while (true) {
            AnonymousClass04b r1 = this.A00;
            if (i < r1.size()) {
                Class cls = (Class) r1.A07(i);
                if (r5.A0E(cls)) {
                    ((C02780Gi) this.A00.get(cls)).C2x(r5.A09(cls), r6);
                }
                i++;
            } else {
                return;
            }
        }
    }
}
