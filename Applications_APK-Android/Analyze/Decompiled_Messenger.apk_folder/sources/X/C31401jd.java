package X;

import android.util.SparseArray;
import android.view.ViewOutlineProvider;
import com.facebook.common.dextricks.DalvikConstants;
import com.facebook.common.dextricks.DexStore;

/* renamed from: X.1jd  reason: invalid class name and case insensitive filesystem */
public final class C31401jd {
    public float A00 = 1.0f;
    public float A01 = 0.0f;
    public float A02 = 0.0f;
    public float A03 = 0.0f;
    public float A04 = 1.0f;
    public float A05;
    public int A06 = 0;
    public int A07 = 0;
    public int A08 = 0;
    public int A09 = 0;
    public int A0A;
    public int A0B = 0;
    public SparseArray A0C;
    public ViewOutlineProvider A0D;
    public AnonymousClass10N A0E;
    public AnonymousClass10N A0F;
    public AnonymousClass10N A0G;
    public AnonymousClass10N A0H;
    public AnonymousClass10N A0I;
    public AnonymousClass10N A0J;
    public AnonymousClass10N A0K;
    public AnonymousClass10N A0L;
    public AnonymousClass10N A0M;
    public AnonymousClass10N A0N;
    public AnonymousClass10N A0O;
    public AnonymousClass10N A0P;
    public AnonymousClass10N A0Q;
    public CharSequence A0R;
    public CharSequence A0S;
    public Object A0T;
    public String A0U;
    public boolean A0V = true;
    public boolean A0W;

    public void A00(C31401jd r5) {
        if ((this.A0A & 8) != 0) {
            AnonymousClass10N r1 = this.A0E;
            r5.A0A |= 8;
            r5.A0E = r1;
        }
        if ((this.A0A & 16) != 0) {
            AnonymousClass10N r12 = this.A0I;
            r5.A0A |= 16;
            r5.A0I = r12;
        }
        if ((this.A0A & DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP) != 0) {
            AnonymousClass10N r13 = this.A0G;
            r5.A0A |= DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP;
            r5.A0G = r13;
        }
        if ((this.A0A & 32) != 0) {
            AnonymousClass10N r14 = this.A0Q;
            r5.A0A |= 32;
            r5.A0Q = r14;
        }
        if ((this.A0A & DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED) != 0) {
            AnonymousClass10N r15 = this.A0H;
            r5.A0A |= DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED;
            r5.A0H = r15;
        }
        if ((this.A0A & 4194304) != 0) {
            String str = this.A0U;
            r5.A0A |= 4194304;
            r5.A0U = str;
        }
        if ((this.A0A & 16777216) != 0) {
            CharSequence charSequence = this.A0R;
            r5.A0A |= 16777216;
            r5.A0R = charSequence;
        }
        if ((this.A0A & 64) != 0) {
            AnonymousClass10N r16 = this.A0F;
            r5.A0A |= 64;
            r5.A0F = r16;
        }
        if ((this.A0A & 128) != 0) {
            AnonymousClass10N r17 = this.A0J;
            r5.A0A |= 128;
            r5.A0J = r17;
        }
        if ((this.A0A & DexStore.LOAD_RESULT_OATMEAL_QUICKENED) != 0) {
            AnonymousClass10N r18 = this.A0K;
            r5.A0A |= DexStore.LOAD_RESULT_OATMEAL_QUICKENED;
            r5.A0K = r18;
        }
        if ((this.A0A & 512) != 0) {
            AnonymousClass10N r19 = this.A0L;
            r5.A0A |= 512;
            r5.A0L = r19;
        }
        if ((this.A0A & 1024) != 0) {
            AnonymousClass10N r110 = this.A0M;
            r5.A0A |= 1024;
            r5.A0M = r110;
        }
        if ((this.A0A & 2048) != 0) {
            AnonymousClass10N r111 = this.A0N;
            r5.A0A |= 2048;
            r5.A0N = r111;
        }
        if ((this.A0A & DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED) != 0) {
            AnonymousClass10N r112 = this.A0O;
            r5.A0A |= DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED;
            r5.A0O = r112;
        }
        if ((this.A0A & DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED) != 0) {
            AnonymousClass10N r113 = this.A0P;
            r5.A0A |= DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED;
            r5.A0P = r113;
        }
        boolean z = true;
        if ((this.A0A & 1) != 0) {
            CharSequence charSequence2 = this.A0S;
            r5.A0A |= 1;
            r5.A0S = charSequence2;
        }
        if ((this.A0A & DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET) != 0) {
            float f = this.A05;
            r5.A0A |= DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET;
            r5.A05 = f;
        }
        if ((this.A0A & DexStore.LOAD_RESULT_PGO) != 0) {
            ViewOutlineProvider viewOutlineProvider = this.A0D;
            r5.A0A |= DexStore.LOAD_RESULT_PGO;
            r5.A0D = viewOutlineProvider;
        }
        if ((this.A0A & 65536) != 0) {
            boolean z2 = this.A0W;
            r5.A0A |= 65536;
            r5.A0W = z2;
        }
        if ((this.A0A & DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE) != 0) {
            boolean z3 = this.A0V;
            r5.A0A |= DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE;
            r5.A0V = z3;
        }
        Object obj = this.A0T;
        if (obj != null) {
            r5.A0A |= 2;
            r5.A0T = obj;
        }
        SparseArray sparseArray = this.A0C;
        if (sparseArray != null) {
            r5.A0A |= 4;
            r5.A0C = sparseArray;
        }
        int i = this.A09;
        if (i != 0) {
            boolean z4 = false;
            if (i == 1) {
                z4 = true;
            }
            if (z4) {
                r5.A09 = 1;
            } else {
                r5.A09 = 2;
            }
        }
        int i2 = this.A07;
        if (i2 != 0) {
            boolean z5 = false;
            if (i2 == 1) {
                z5 = true;
            }
            if (z5) {
                r5.A07 = 1;
            } else {
                r5.A07 = 2;
            }
        }
        int i3 = this.A08;
        if (i3 != 0) {
            boolean z6 = false;
            if (i3 == 1) {
                z6 = true;
            }
            if (z6) {
                r5.A08 = 1;
            } else {
                r5.A08 = 2;
            }
        }
        int i4 = this.A0B;
        if (i4 != 0) {
            boolean z7 = false;
            if (i4 == 1) {
                z7 = true;
            }
            if (z7) {
                r5.A0B = 1;
            } else {
                r5.A0B = 2;
            }
        }
        int i5 = this.A06;
        if (i5 != 0) {
            if (i5 != 1) {
                z = false;
            }
            if (z) {
                r5.A06 = 1;
            } else {
                r5.A06 = 2;
            }
        }
        if ((this.A0A & DexStore.LOAD_RESULT_WITH_VDEX_ODEX) != 0) {
            r5.A04 = this.A04;
            r5.A0A |= DexStore.LOAD_RESULT_WITH_VDEX_ODEX;
        }
        if ((this.A0A & 1048576) != 0) {
            r5.A00 = this.A00;
            r5.A0A |= 1048576;
        }
        if ((this.A0A & 2097152) != 0) {
            r5.A01 = this.A01;
            r5.A0A |= 2097152;
        }
        if ((this.A0A & 33554432) != 0) {
            r5.A02 = this.A02;
            r5.A0A |= 33554432;
        }
        if ((this.A0A & 67108864) != 0) {
            r5.A03 = this.A03;
            r5.A0A |= 67108864;
        }
    }

    public boolean A01() {
        if (this.A0E == null && this.A0I == null && this.A0Q == null && this.A0H == null) {
            return false;
        }
        return true;
    }

    public boolean A02() {
        if (this.A0J == null && this.A0K == null && this.A0L == null && this.A0M == null && this.A0N == null && this.A0F == null && this.A0O == null && this.A0P == null && this.A0U == null && this.A0R == null) {
            return false;
        }
        return true;
    }
}
