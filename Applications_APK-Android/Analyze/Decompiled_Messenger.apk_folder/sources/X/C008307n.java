package X;

import android.view.Display;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.07n  reason: invalid class name and case insensitive filesystem */
public final class C008307n {
    private static volatile C008307n A01;
    private final C26881cI A00;

    public static final C008307n A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C008307n.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C008307n(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public float A02() {
        float refreshRate;
        C26881cI r1 = this.A00;
        synchronized (r1) {
            Display display = r1.A04;
            AnonymousClass064.A00(display);
            refreshRate = display.getRefreshRate();
        }
        return refreshRate;
    }

    public int A03() {
        return this.A00.A07();
    }

    public int A04() {
        return this.A00.A09();
    }

    private C008307n(AnonymousClass1XY r2) {
        this.A00 = C26881cI.A00(r2);
    }

    public static final C008307n A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
