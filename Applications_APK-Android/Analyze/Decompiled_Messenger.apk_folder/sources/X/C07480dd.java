package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.facebook.acra.ACRA;
import com.facebook.acra.LogCatCollector;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0dd  reason: invalid class name and case insensitive filesystem */
public final class C07480dd {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void A00(ContentValues contentValues, AnonymousClass1Y7 r13, Object obj, boolean z) {
        int length;
        byte[] byteArray;
        String A05 = r13.A05();
        if (A05 != null) {
            contentValues.put("key", A05);
            if (obj instanceof String) {
                String str = (String) obj;
                if (!z || (length = str.length()) < 100000) {
                    contentValues.put("type", (Integer) 1);
                    contentValues.put("value", str);
                    return;
                }
                try {
                    C010708t.A0P("FbSharedPreferencesContract", "Large string value for pref key %s, will zip", A05);
                    if (length == 0) {
                        byteArray = null;
                    } else {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                        gZIPOutputStream.write(str.getBytes(LogCatCollector.UTF_8_ENCODING));
                        gZIPOutputStream.flush();
                        gZIPOutputStream.close();
                        byteArray = byteArrayOutputStream.toByteArray();
                    }
                    if (byteArray != null && ((long) byteArray.length) > 100000000) {
                        C010708t.A0Q("FbSharedPreferencesContract", "Preference value too large for key %s", A05);
                    }
                    contentValues.put("type", (Integer) 7);
                    contentValues.put("value", byteArray);
                } catch (IOException e) {
                    C010708t.A0Q("FbSharedPreferencesContract", "Preference value too large for key %s", A05, e);
                    contentValues.put("type", (Integer) 1);
                    contentValues.put("value", str);
                }
            } else if (obj instanceof Boolean) {
                contentValues.put("type", (Integer) 2);
                contentValues.put("value", Integer.valueOf(((Boolean) obj).booleanValue() ? 1 : 0));
            } else if (obj instanceof Integer) {
                contentValues.put("type", (Integer) 3);
                contentValues.put("value", (Integer) obj);
            } else if (obj instanceof Long) {
                contentValues.put("type", (Integer) 4);
                contentValues.put("value", (Long) obj);
            } else if (obj instanceof Float) {
                contentValues.put("type", (Integer) 5);
                contentValues.put("value", (Float) obj);
            } else if (obj instanceof Double) {
                contentValues.put("type", (Integer) 6);
                contentValues.put("value", (Double) obj);
            }
        }
    }

    public static void A01(Cursor cursor, Map map) {
        if (cursor != null) {
            int A01 = AnonymousClass0W5.A00.A01(cursor);
            int A012 = AnonymousClass0W5.A01.A01(cursor);
            int A013 = AnonymousClass0W5.A02.A01(cursor);
            while (cursor.moveToNext()) {
                String string = cursor.getString(A01);
                if (string != null) {
                    Object obj = null;
                    switch (cursor.getInt(A012)) {
                        case 1:
                            obj = cursor.getString(A013);
                            break;
                        case 2:
                            boolean z = false;
                            if (cursor.getInt(A013) != 0) {
                                z = true;
                            }
                            obj = Boolean.valueOf(z);
                            break;
                        case 3:
                            obj = Integer.valueOf(cursor.getInt(A013));
                            break;
                        case 4:
                            obj = Long.valueOf(cursor.getLong(A013));
                            break;
                        case 5:
                            obj = Float.valueOf(cursor.getFloat(A013));
                            break;
                        case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                            obj = Double.valueOf(cursor.getDouble(A013));
                            break;
                        case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                            try {
                                GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(cursor.getBlob(A013)));
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(gZIPInputStream, LogCatCollector.UTF_8_ENCODING));
                                StringBuilder sb = new StringBuilder();
                                while (true) {
                                    String readLine = bufferedReader.readLine();
                                    if (readLine == null) {
                                        gZIPInputStream.close();
                                        obj = sb.toString();
                                        break;
                                    } else {
                                        sb.append(readLine);
                                    }
                                }
                            } catch (IOException unused) {
                                break;
                            }
                    }
                    if (obj != null) {
                        map.put(new AnonymousClass1Y7(string), obj);
                    }
                }
            }
        }
    }
}
