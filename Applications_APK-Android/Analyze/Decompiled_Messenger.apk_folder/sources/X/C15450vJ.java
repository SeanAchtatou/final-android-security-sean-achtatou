package X;

import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0vJ  reason: invalid class name and case insensitive filesystem */
public final class C15450vJ extends AnonymousClass0UT {
    public C15440vI A00(int i) {
        QuickPerformanceLogger A03 = AnonymousClass0ZD.A03(this);
        C27031cX r4 = new C27031cX(this);
        AnonymousClass1T3 A00 = AnonymousClass1T3.A00(this);
        C32781mK A002 = C32781mK.A00(this);
        AnonymousClass0UQ A003 = AnonymousClass0UQ.A00(AnonymousClass1Y3.BI2, this);
        AnonymousClass15Q A01 = AnonymousClass15Q.A01(this);
        AnonymousClass1T4.A00(this);
        return new C15440vI(A03, r4, A00, A002, A003, A01, i, AnonymousClass0WA.A00(this));
    }

    public C15450vJ(AnonymousClass1XY r1) {
        super(r1);
    }
}
