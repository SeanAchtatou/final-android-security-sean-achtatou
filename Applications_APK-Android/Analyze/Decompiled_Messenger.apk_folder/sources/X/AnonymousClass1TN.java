package X;

/* renamed from: X.1TN  reason: invalid class name */
public final class AnonymousClass1TN implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.push.mqtt.service.MqttPushServiceManager$3";
    public final /* synthetic */ AnonymousClass1U0 A00;

    public AnonymousClass1TN(AnonymousClass1U0 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:14|(2:16|17)|18|19) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:4|5|(2:7|8)|9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0060, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0033 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0029 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[Catch:{ all -> 0x0066 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            X.1U0 r0 = r5.A00
            java.lang.String r1 = "doInit"
            X.AnonymousClass1U0.A05(r0, r1)
            X.1U0 r0 = r5.A00
            X.AnonymousClass1U0.A04(r0, r1)
            java.lang.String r2 = "MqttPushServiceManager"
            java.lang.String r1 = "%s.doInit.run"
            r0 = -1751241319(0xffffffff979e2d99, float:-1.0222021E-24)
            X.C005505z.A05(r1, r2, r0)
            X.1U0 r4 = r5.A00     // Catch:{ all -> 0x0066 }
            android.content.Context r0 = r4.A04     // Catch:{ all -> 0x0066 }
            X.C07620dr.A00(r0)     // Catch:{ all -> 0x0066 }
            r2 = 10000(0x2710, double:4.9407E-320)
            java.lang.Object r1 = X.AnonymousClass001.A0j     // Catch:{ all -> 0x0066 }
            monitor-enter(r1)     // Catch:{ all -> 0x0066 }
            boolean r0 = X.AnonymousClass001.A0h     // Catch:{ all -> 0x0063 }
            if (r0 == 0) goto L_0x0029
            r1.wait(r2)     // Catch:{ InterruptedException -> 0x0029 }
        L_0x0029:
            monitor-exit(r1)     // Catch:{ all -> 0x0063 }
            java.lang.Object r1 = X.AnonymousClass001.A0i     // Catch:{ all -> 0x0066 }
            monitor-enter(r1)     // Catch:{ all -> 0x0066 }
            r0 = 0
            if (r0 == 0) goto L_0x0033
            r1.wait(r2)     // Catch:{ InterruptedException -> 0x0033 }
        L_0x0033:
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            X.0Ut r0 = r4.A06     // Catch:{ all -> 0x0066 }
            X.0bl r2 = r0.BMm()     // Catch:{ all -> 0x0066 }
            X.0dt r1 = new X.0dt     // Catch:{ all -> 0x0066 }
            r1.<init>(r4)     // Catch:{ all -> 0x0066 }
            java.lang.String r0 = "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"
            r2.A02(r0, r1)     // Catch:{ all -> 0x0066 }
            android.os.Handler r0 = r4.A05     // Catch:{ all -> 0x0066 }
            r2.A01(r0)     // Catch:{ all -> 0x0066 }
            X.0c5 r0 = r2.A00()     // Catch:{ all -> 0x0066 }
            r0.A00()     // Catch:{ all -> 0x0066 }
            java.lang.String r0 = "setEnabled"
            X.AnonymousClass1U0.A04(r4, r0)     // Catch:{ all -> 0x0066 }
            r0 = 1
            X.AnonymousClass1U0.A07(r4, r0)     // Catch:{ all -> 0x0066 }
            r0 = 1048777040(0x3e831150, float:0.25599146)
            X.C005505z.A00(r0)
            return
        L_0x0060:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            goto L_0x0065
        L_0x0063:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0063 }
        L_0x0065:
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r1 = move-exception
            r0 = -583272119(0xffffffffdd3bf949, float:-8.465586E17)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TN.run():void");
    }
}
