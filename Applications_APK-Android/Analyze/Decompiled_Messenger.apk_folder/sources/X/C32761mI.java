package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1mI  reason: invalid class name and case insensitive filesystem */
public final class C32761mI implements AnonymousClass06B {
    private static volatile C32761mI A08;
    public long A00;
    public long A01;
    public AnonymousClass069 A02;
    public Integer A03 = AnonymousClass07B.A00;
    private AnonymousClass2ST A04;
    public final AnonymousClass06B A05;
    private final C04460Ut A06;
    private final FbSharedPreferences A07;

    public static final C32761mI A00(AnonymousClass1XY r8) {
        if (A08 == null) {
            synchronized (C32761mI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A08 = new C32761mI(applicationInjector, FbSharedPreferencesModule.A00(applicationInjector), AnonymousClass067.A02(), AnonymousClass067.A03(applicationInjector), new AnonymousClass2ST(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public long A01(long j) {
        if (2 - this.A03.intValue() != 0) {
            return j;
        }
        return j - this.A01;
    }

    public void A02(long j) {
        if (this.A01 != j) {
            this.A01 = j;
            if (Math.abs(j) > 60000) {
                this.A03 = AnonymousClass07B.A0C;
            } else {
                this.A03 = AnonymousClass07B.A01;
            }
            C30281hn edit = this.A07.edit();
            edit.BzA(C52292iq.A00, j);
            edit.commit();
            this.A06.C4y("com.facebook.orca.SKEW_CHANGED");
        }
    }

    public long now() {
        return A01(this.A05.now());
    }

    private C32761mI(AnonymousClass1XY r5, FbSharedPreferences fbSharedPreferences, AnonymousClass06B r7, AnonymousClass069 r8, AnonymousClass2ST r9) {
        this.A06 = C04430Uq.A02(r5);
        this.A07 = fbSharedPreferences;
        this.A05 = r7;
        this.A02 = r8;
        this.A04 = r9;
        this.A00 = r7.now() - r8.now();
        if (this.A04.A01()) {
            A02(0);
            this.A03 = AnonymousClass07B.A01;
            return;
        }
        FbSharedPreferences fbSharedPreferences2 = this.A07;
        AnonymousClass1Y7 r3 = C52292iq.A00;
        if (fbSharedPreferences2.BBh(r3)) {
            A02(this.A07.At2(r3, 0));
        }
    }
}
