package X;

import com.facebook.cipher.jni.DecryptHybrid;
import com.facebook.cipher.jni.EncryptHybrid;
import com.facebook.soloader.NativeLibrary;
import java.util.Arrays;

/* renamed from: X.18C  reason: invalid class name */
public final class AnonymousClass18C extends NativeLibrary {
    public AnonymousClass18C() {
        super(Arrays.asList("fb", "concealjni"));
    }

    public void A02() {
        Integer num = AnonymousClass07B.A01;
        byte[] bArr = new byte[AnonymousClass189.A03(num)];
        byte[] bArr2 = new byte[0];
        EncryptHybrid encryptHybrid = new EncryptHybrid(bArr, new byte[AnonymousClass189.A02(num)], bArr2);
        byte[] start = encryptHybrid.start();
        byte[] end = encryptHybrid.end();
        DecryptHybrid decryptHybrid = new DecryptHybrid(bArr, bArr2);
        decryptHybrid.start(start);
        if (!decryptHybrid.end(end)) {
            throw new UnsatisfiedLinkError("Native implementation loaded but failed test.");
        }
    }

    public void A04() {
        try {
            A01();
        } catch (Throwable th) {
            throw new C37921wZ(th);
        }
    }
}
