package X;

import android.os.SystemClock;

/* renamed from: X.0eU  reason: invalid class name and case insensitive filesystem */
public abstract class C07980eU {
    public boolean A00 = false;
    public final Object A01 = new Object();

    public void A01() {
        synchronized (this.A01) {
            this.A00 = false;
            this.A01.notifyAll();
        }
    }

    public void A02() {
        synchronized (this.A01) {
            this.A00 = true;
        }
    }

    public boolean A03(long j) {
        boolean z;
        synchronized (this.A01) {
            z = true;
            if (this.A00) {
                long uptimeMillis = SystemClock.uptimeMillis();
                this.A01.wait(j);
                if (SystemClock.uptimeMillis() - uptimeMillis >= j) {
                    z = false;
                }
            }
        }
        return z;
    }
}
