package X;

import com.facebook.proxygen.PersistentSSLCacheSettings;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1jr  reason: invalid class name and case insensitive filesystem */
public final class C31541jr implements C08210er {
    private static final Class A02 = C31541jr.class;
    private static volatile C31541jr A03;
    public PersistentSSLCacheSettings A00;
    private final C25051Yd A01;

    public String getName() {
        return "LigerCache";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    public void prepareDataForWriting() {
    }

    public static final C31541jr A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C31541jr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C31541jr(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x004e A[SYNTHETIC, Splitter:B:27:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0058 A[SYNTHETIC, Splitter:B:34:0x0058] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map getExtraFileFromWorkerThread(java.io.File r9) {
        /*
            r8 = this;
            com.facebook.proxygen.PersistentSSLCacheSettings r0 = r8.A00
            if (r0 == 0) goto L_0x0065
            java.lang.String r1 = r0.filename
            if (r1 == 0) goto L_0x0065
            com.google.common.collect.ImmutableMap$Builder r7 = com.google.common.collect.ImmutableMap.builder()
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x005c }
            r0.<init>(r1)     // Catch:{ IOException -> 0x005c }
            java.io.File r6 = new java.io.File     // Catch:{ IOException -> 0x005c }
            java.lang.String r5 = "liger_dns_cache.txt"
            r6.<init>(r9, r5)     // Catch:{ IOException -> 0x005c }
            r1 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ all -> 0x0054 }
            r4.<init>(r0)     // Catch:{ all -> 0x0054 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ all -> 0x004b }
            r3.<init>(r6)     // Catch:{ all -> 0x004b }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x0048 }
        L_0x0027:
            int r1 = r4.read(r2)     // Catch:{ all -> 0x0048 }
            if (r1 <= 0) goto L_0x0032
            r0 = 0
            r3.write(r2, r0, r1)     // Catch:{ all -> 0x0048 }
            goto L_0x0027
        L_0x0032:
            r3.close()     // Catch:{ all -> 0x0052 }
            r4.close()     // Catch:{ IOException -> 0x005c }
            android.net.Uri r0 = android.net.Uri.fromFile(r6)     // Catch:{ IOException -> 0x005c }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x005c }
            r7.put(r5, r0)     // Catch:{ IOException -> 0x005c }
            com.google.common.collect.ImmutableMap r0 = r7.build()
            return r0
        L_0x0048:
            r0 = move-exception
            r1 = r3
            goto L_0x004c
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0051:
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
            goto L_0x0056
        L_0x0054:
            r0 = move-exception
            r4 = r1
        L_0x0056:
            if (r4 == 0) goto L_0x005b
            r4.close()     // Catch:{ IOException -> 0x005c }
        L_0x005b:
            throw r0     // Catch:{ IOException -> 0x005c }
        L_0x005c:
            r2 = move-exception
            java.lang.Class r1 = X.C31541jr.A02
            java.lang.String r0 = "Exception saving liger trace"
            X.C010708t.A09(r1, r0, r2)
            throw r2
        L_0x0065:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31541jr.getExtraFileFromWorkerThread(java.io.File):java.util.Map");
    }

    public boolean shouldSendAsync() {
        return this.A01.Aeo(281651071811861L, false);
    }

    private C31541jr(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0WT.A00(r2);
    }
}
