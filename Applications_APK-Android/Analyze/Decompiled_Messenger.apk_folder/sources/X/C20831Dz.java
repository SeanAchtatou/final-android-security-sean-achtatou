package X;

import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.acra.ACRA;
import com.facebook.fbpay.api.FbPayTransactionDetailsViewModel;
import com.facebook.messaging.accountswitch.model.MessengerAccountInfo;
import com.facebook.messaging.business.informationidentify.model.PIIQuestion;
import com.facebook.messaging.montage.model.art.ArtCategoryItem;
import com.facebook.messaging.montage.model.art.ArtItem;
import com.facebook.messaging.montage.model.art.BaseItem;
import com.facebook.messaging.montage.model.art.EffectItem;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

/* renamed from: X.1Dz  reason: invalid class name and case insensitive filesystem */
public abstract class C20831Dz {
    private boolean A00 = false;
    public final AnonymousClass1A1 A01 = new AnonymousClass1A1();

    public boolean A0A(C33781o8 r2) {
        return this instanceof C24151Sm;
    }

    public void A0B(C33781o8 r7) {
        C23819BnV bnV;
        C33781o8 r0;
        String str;
        String str2;
        if (this instanceof C23701Qn) {
            C23701Qn r1 = (C23701Qn) this;
            C28650Dye dye = ((AnonymousClass2NH) ((C28697DzP) r7).A0H).A02;
            if (dye != null && (bnV = dye.A02) != null && !C28617Dy6.A00().A01.A00) {
                Handler handler = r1.A01;
                handler.sendMessage(handler.obtainMessage(0, bnV));
            }
        } else if (this instanceof C24151Sm) {
            C87304Eg r72 = (C87304Eg) r7;
            C24161Sn r12 = ((C24151Sm) this).A03;
            if ((r12 instanceof C20831Dz) && (r0 = r72.A00) != null) {
                ((C20831Dz) r12).A0B(r0);
            }
        } else if (this instanceof C23671Qk) {
            C23671Qk r2 = (C23671Qk) this;
            HashMap hashMap = r2.A05;
            if (hashMap != null) {
                if (r7 instanceof C187608nV) {
                    C187608nV r4 = (C187608nV) r7;
                    str = r4.A01.A02;
                    if (!(str == null || ((String) hashMap.get(str)) == null || (str2 = (String) r2.A05.get(str)) == null)) {
                        r4.A04.A04();
                        r4.A0I(str2);
                    }
                } else if (r7 instanceof C187748nk) {
                    C187748nk r13 = (C187748nk) r7;
                    str = r13.A00.A02;
                    if (!(str == null || ((HashMap) hashMap.get(str)) == null)) {
                        r13.A0H((HashMap) r2.A05.get(str));
                    }
                } else if (r7 instanceof C187668nb) {
                    C187668nb r14 = (C187668nb) r7;
                    str = r14.A01.A02;
                    if (!(str == null || ((HashMap) hashMap.get(str)) == null)) {
                        r14.A0H((HashMap) r2.A05.get(str));
                    }
                }
                r2.A05.remove(str);
            }
            if (!r2.A07) {
                return;
            }
            if (r7 instanceof C187608nV) {
                ((C187608nV) r7).A0J(true);
            } else if (r7 instanceof C187748nk) {
                ((C187748nk) r7).A0I(true);
            } else if (r7 instanceof C187668nb) {
                ((C187668nb) r7).A0I(true);
            }
        }
    }

    public void A0C(C33781o8 r7) {
        C23819BnV bnV;
        C33781o8 r0;
        C26265Cv0 cv0;
        Queue queue;
        if (this instanceof C23701Qn) {
            C23701Qn r1 = (C23701Qn) this;
            C28650Dye dye = ((AnonymousClass2NH) ((C28697DzP) r7).A0H).A02;
            if (dye != null && (bnV = dye.A03) != null && !C28617Dy6.A00().A01.A00) {
                Handler handler = r1.A01;
                handler.sendMessage(handler.obtainMessage(0, bnV));
            }
        } else if (this instanceof C24151Sm) {
            C87304Eg r72 = (C87304Eg) r7;
            C24161Sn r12 = ((C24151Sm) this).A03;
            if ((r12 instanceof C20831Dz) && (r0 = r72.A00) != null) {
                ((C20831Dz) r12).A0C(r0);
            }
        } else if (!(this instanceof C20751Dr)) {
            if (this instanceof C23671Qk) {
                C23671Qk r13 = (C23671Qk) this;
                if (r7 instanceof C187608nV) {
                    C187608nV r73 = (C187608nV) r7;
                    HashMap hashMap = r13.A06;
                    if (r73.A0J(false)) {
                        hashMap.put(r73.A01.A02, r73.A0G());
                    }
                } else if (r7 instanceof C187748nk) {
                    C187748nk r74 = (C187748nk) r7;
                    HashMap hashMap2 = r13.A06;
                    if (r74.A0I(false)) {
                        hashMap2.put(r74.A00.A02, r74.A0G());
                    }
                } else if (r7 instanceof C187668nb) {
                    C187668nb r75 = (C187668nb) r7;
                    HashMap hashMap3 = r13.A06;
                    if (r75.A0I(false)) {
                        hashMap3.put(r75.A01.A02, r75.A0G());
                    }
                }
            }
        } else if ((r7 instanceof C26257Cur) && (queue = (cv0 = ((C26257Cur) r7).A06).A0R) != null && queue.size() >= 10) {
            for (int size = cv0.A0R.size() >> 1; size > 0; size--) {
                ((AnonymousClass3DS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ANF, cv0.A0J.A00)).A05((String) cv0.A0R.poll());
            }
        }
    }

    public void A0D(C33781o8 r1) {
    }

    public abstract int ArU();

    public void BOc(RecyclerView recyclerView) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:358:0x07ae, code lost:
        if (X.C06850cB.A0B(r8) == false) goto L_0x07b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0075, code lost:
        if (r3.A00.A0G.isEmpty() != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0134, code lost:
        if (X.AnonymousClass07c.enableRenderInfoDebugging == false) goto L_0x0136;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:257:0x0543  */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0583  */
    /* JADX WARNING: Removed duplicated region for block: B:302:0x064d  */
    /* JADX WARNING: Removed duplicated region for block: B:734:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:735:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BPy(X.C33781o8 r14, int r15) {
        /*
            r13 = this;
            boolean r0 = r13 instanceof X.C23701Qn
            if (r0 != 0) goto L_0x11b3
            boolean r0 = r13 instanceof X.C24151Sm
            if (r0 != 0) goto L_0x0bd8
            boolean r0 = r13 instanceof X.C23611Qe
            if (r0 != 0) goto L_0x11a4
            boolean r0 = r13 instanceof X.C23781Qv
            if (r0 != 0) goto L_0x1180
            boolean r0 = r13 instanceof X.C23631Qg
            if (r0 != 0) goto L_0x0b98
            boolean r0 = r13 instanceof X.AnonymousClass1IC
            if (r0 != 0) goto L_0x0b30
            boolean r0 = r13 instanceof X.C37441vg
            if (r0 != 0) goto L_0x0919
            boolean r0 = r13 instanceof X.C37451vh
            if (r0 != 0) goto L_0x0f4a
            boolean r0 = r13 instanceof X.C37461vi
            if (r0 != 0) goto L_0x08cd
            boolean r0 = r13 instanceof X.C37471vj
            if (r0 != 0) goto L_0x0f35
            boolean r0 = r13 instanceof X.C23641Qh
            if (r0 != 0) goto L_0x06d5
            boolean r0 = r13 instanceof X.C21701Ij
            if (r0 != 0) goto L_0x059b
            boolean r0 = r13 instanceof X.AnonymousClass1TV
            if (r0 != 0) goto L_0x04e6
            boolean r0 = r13 instanceof X.C21881Jb
            if (r0 != 0) goto L_0x045a
            boolean r0 = r13 instanceof X.C33361nS
            if (r0 != 0) goto L_0x03ee
            boolean r0 = r13 instanceof X.C20751Dr
            if (r0 != 0) goto L_0x0e8c
            boolean r0 = r13 instanceof X.C23651Qi
            if (r0 != 0) goto L_0x0d72
            boolean r0 = r13 instanceof X.C23661Qj
            if (r0 != 0) goto L_0x0390
            boolean r0 = r13 instanceof X.C188815s
            if (r0 != 0) goto L_0x0317
            boolean r0 = r13 instanceof X.C23671Qk
            if (r0 != 0) goto L_0x0171
            boolean r0 = r13 instanceof X.C23681Ql
            if (r0 != 0) goto L_0x0c76
            boolean r0 = r13 instanceof X.C23791Qw
            if (r0 != 0) goto L_0x0c38
            boolean r0 = r13 instanceof X.C23801Qx
            if (r0 != 0) goto L_0x0bf6
            r3 = r13
            X.1A0 r3 = (X.AnonymousClass1A0) r3
            X.1Qz r14 = (X.AnonymousClass1Qz) r14
            X.1Ri r0 = r3.A00
            X.1r0 r0 = r0.A0A
            boolean r0 = X.C35301r0.A01(r0)
            r4 = 1
            if (r0 == 0) goto L_0x0077
            X.1Ri r0 = r3.A00
            java.lang.String r0 = r0.A0G
            boolean r0 = r0.isEmpty()
            r11 = 1
            if (r0 == 0) goto L_0x0078
        L_0x0077:
            r11 = 0
        L_0x0078:
            X.1Ri r2 = r3.A00
            r1 = r15
            boolean r0 = r2.A0n
            if (r0 == 0) goto L_0x0087
            java.util.List r0 = r2.A0h
            int r0 = r0.size()
            int r1 = r15 % r0
        L_0x0087:
            X.1Ri r0 = r3.A00
            java.util.List r0 = r0.A0h
            java.lang.Object r7 = r0.get(r1)
            X.1IK r7 = (X.AnonymousClass1IK) r7
            X.1Ih r2 = r7.A03()
            boolean r0 = r2.C2H()
            if (r0 == 0) goto L_0x0165
            android.view.View r1 = r14.A0H
            com.facebook.litho.LithoView r1 = (com.facebook.litho.LithoView) r1
            X.1Ri r0 = r3.A00
            java.util.List r0 = r0.A0H
            r1.A0d(r0)
            X.1Ri r0 = r3.A00
            int r10 = X.AnonymousClass1Ri.A01(r0, r7)
            X.1Ri r0 = r3.A00
            int r9 = X.AnonymousClass1Ri.A00(r0, r7)
            boolean r0 = r7.A0A(r10, r9)
            if (r0 != 0) goto L_0x00c4
            X.10L r6 = new X.10L
            r6.<init>()
            X.1Ri r0 = r3.A00
            X.0p4 r0 = r0.A0U
            r7.A07(r0, r10, r9, r6)
        L_0x00c4:
            X.1Ri r0 = r3.A00
            X.19N r0 = r0.A0X
            int r0 = r0.B1z()
            r12 = 0
            if (r0 != r4) goto L_0x00d0
            r12 = 1
        L_0x00d0:
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            r4 = -2
            r8 = 1073741824(0x40000000, float:2.0)
            if (r0 != r8) goto L_0x015f
            int r6 = android.view.View.MeasureSpec.getSize(r10)
        L_0x00dd:
            int r0 = android.view.View.MeasureSpec.getMode(r9)
            if (r0 != r8) goto L_0x015b
            int r4 = android.view.View.MeasureSpec.getSize(r9)
        L_0x00e7:
            X.1R3 r0 = new X.1R3
            r2.BFA()
            r0.<init>(r6, r4, r10, r9)
            r1.setLayoutParams(r0)
            com.facebook.litho.ComponentTree r0 = r7.A02()
            r1.A0b(r0)
            X.1Ih r0 = r7.A03()
            X.10N r0 = r0.B0i()
            if (r0 == 0) goto L_0x0112
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A0C
            int r0 = r0.get()
            if (r0 != 0) goto L_0x0112
            X.45A r0 = new X.45A
            r0.<init>(r3, r1)
            r1.A06 = r0
        L_0x0112:
            if (r11 == 0) goto L_0x0157
            X.1Ri r0 = r3.A00
            X.1r0 r7 = r0.A0A
            java.lang.String r8 = r0.A0G
            boolean[] r9 = r0.A0o
            boolean[] r10 = r0.A0p
            int r0 = r3.ArU()
            r11 = 0
            if (r15 != r0) goto L_0x0126
            r11 = 1
        L_0x0126:
            X.38h r6 = new X.38h
            r6.<init>(r7, r8, r9, r10, r11, r12)
            r1.A05 = r6
        L_0x012d:
            boolean r0 = X.AnonymousClass07c.isDebugModeEnabled
            if (r0 == 0) goto L_0x0136
            boolean r1 = X.AnonymousClass07c.enableRenderInfoDebugging
            r0 = 1
            if (r1 != 0) goto L_0x0137
        L_0x0136:
            r0 = 0
        L_0x0137:
            if (r0 == 0) goto L_0x0156
            android.view.View r3 = r14.A0H
            java.lang.String r0 = "SONAR_SECTIONS_DEBUG_INFO"
            java.lang.Object r2 = r2.Ajj(r0)
            java.util.Map r0 = X.C84023ye.A00
            if (r0 != 0) goto L_0x014c
            java.util.WeakHashMap r0 = new java.util.WeakHashMap
            r0.<init>()
            X.C84023ye.A00 = r0
        L_0x014c:
            java.util.Map r1 = X.C84023ye.A00
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r2)
            r1.put(r3, r0)
        L_0x0156:
            return
        L_0x0157:
            r0 = 0
            r1.A05 = r0
            goto L_0x012d
        L_0x015b:
            if (r12 != 0) goto L_0x00e7
            r4 = -1
            goto L_0x00e7
        L_0x015f:
            r6 = -2
            if (r12 == 0) goto L_0x00dd
            r6 = -1
            goto L_0x00dd
        L_0x0165:
            X.3ph r1 = r2.B96()
            r14.A00 = r1
            android.view.View r0 = r14.A0H
            r1.APu(r0)
            goto L_0x012d
        L_0x0171:
            r1 = r13
            X.1Qk r1 = (X.C23671Qk) r1
            boolean r0 = r1.A08
            if (r0 == 0) goto L_0x0244
            if (r15 != 0) goto L_0x0242
            X.6Kx r14 = (X.C133116Kx) r14
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r1.A00
            if (r3 == 0) goto L_0x0156
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 3386882(0x33ae02, float:4.746033E-39)
            r0 = -1288717908(0xffffffffb32fb9ac, float:-4.091423E-8)
            com.facebook.graphservice.tree.TreeJNI r4 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4
            if (r4 == 0) goto L_0x01ce
            r1 = 1782764648(0x6a42d468, float:5.8883667E25)
            r0 = 1135539834(0x43aef67a, float:349.9256)
            com.facebook.graphservice.tree.TreeJNI r0 = r4.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x023a
            java.lang.String r1 = r0.A41()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x023a
            com.facebook.drawee.fbpipeline.FbDraweeView r2 = r14.A01
            android.net.Uri r1 = android.net.Uri.parse(r1)
            com.facebook.common.callercontext.CallerContext r0 = X.C133116Kx.A06
            r2.A09(r1, r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = r14.A01
            r0 = 0
            r1.setVisibility(r0)
        L_0x01b9:
            java.lang.String r1 = r4.A3r()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x01ce
            com.facebook.widget.text.BetterTextView r0 = r14.A03
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r1 = r14.A03
            r0 = 0
            r1.setVisibility(r0)
        L_0x01ce:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 8745437(0x8571dd, float:1.2254967E-38)
            r0 = 500297606(0x1dd1ef86, float:5.55695E-21)
            com.facebook.graphservice.tree.TreeJNI r2 = r3.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            if (r2 == 0) goto L_0x0156
            java.lang.String r1 = r2.A3h()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x01f3
            com.facebook.widget.text.BetterTextView r0 = r14.A04
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r1 = r14.A04
            r0 = 0
            r1.setVisibility(r0)
        L_0x01f3:
            r0 = -970177027(0xffffffffc62c45fd, float:-11025.497)
            java.lang.String r5 = r2.A0P(r0)
            java.lang.String r1 = r2.A42()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 != 0) goto L_0x0156
            com.facebook.widget.text.BetterTextView r0 = r14.A05
            r0.setText(r5)
            com.facebook.widget.text.BetterTextView r0 = r14.A05
            r4 = 0
            r0.setVisibility(r4)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x0156
            android.text.SpannableStringBuilder r3 = new android.text.SpannableStringBuilder
            r3.<init>()
            r3.append(r5)
            X.6Kw r2 = new X.6Kw
            r2.<init>(r14, r1)
            int r1 = X.CoM.A00(r5)
            r0 = 34
            r3.setSpan(r2, r4, r1, r0)
            com.facebook.widget.text.BetterTextView r0 = r14.A05
            r0.setText(r3)
            com.facebook.widget.text.BetterTextView r1 = r14.A05
            android.text.method.MovementMethod r0 = android.text.method.LinkMovementMethod.getInstance()
            r1.setMovementMethod(r0)
            return
        L_0x023a:
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = r14.A01
            r0 = 4
            r1.setVisibility(r0)
            goto L_0x01b9
        L_0x0242:
            int r15 = r15 + -1
        L_0x0244:
            boolean r0 = r1.A09
            if (r0 == 0) goto L_0x027a
            com.google.common.collect.ImmutableList r0 = r1.A02
            int r0 = r0.size()
            if (r15 != r0) goto L_0x027a
            X.5jd r14 = (X.C118745jd) r14
            java.lang.String r2 = r1.A03
            java.lang.String r7 = r1.A04
            java.lang.String r6 = "Visit Their Privacy Policy"
            android.view.View r1 = r14.A0H
            r0 = 2131299780(0x7f090dc4, float:1.8217571E38)
            android.view.View r5 = r1.findViewById(r0)
            com.facebook.widget.text.BetterTextView r5 = (com.facebook.widget.text.BetterTextView) r5
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 != 0) goto L_0x0156
            r5.setText(r2)
            r4 = 0
            r5.setVisibility(r4)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r7)
            if (r0 == 0) goto L_0x0c91
            r5.append(r6)
            return
        L_0x027a:
            com.google.common.collect.ImmutableList r0 = r1.A02
            java.lang.Object r1 = r0.get(r15)
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r1 = (com.facebook.messaging.business.informationidentify.model.PIIQuestion) r1
            int r0 = r14.A01
            switch(r0) {
                case 0: goto L_0x028f;
                case 1: goto L_0x0cec;
                case 2: goto L_0x028f;
                case 3: goto L_0x028f;
                case 4: goto L_0x028f;
                case 5: goto L_0x028f;
                case 6: goto L_0x0cb2;
                default: goto L_0x0287;
            }
        L_0x0287:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unsupported result view type"
            r1.<init>(r0)
            throw r1
        L_0x028f:
            X.8nV r14 = (X.C187608nV) r14
            r14.A01 = r1
            java.lang.String r1 = r1.A07
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            r3 = 0
            r2 = 8
            if (r0 != 0) goto L_0x0311
            com.facebook.widget.text.BetterTextView r0 = r14.A06
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r0 = r14.A06
            r0.setVisibility(r3)
        L_0x02a8:
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r0 = r14.A01
            java.lang.String r1 = r0.A06
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x030b
            com.facebook.widget.text.BetterTextView r0 = r14.A05
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r0 = r14.A05
            r0.setVisibility(r3)
        L_0x02bc:
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r0 = r14.A01
            java.lang.String r1 = r0.A05
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x02cb
            com.facebook.widget.text.BetterEditTextView r0 = r14.A04
            r0.setHint(r1)
        L_0x02cb:
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r0 = r14.A01
            java.lang.String r1 = r0.A04
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x02e4
            boolean r0 = java.lang.Boolean.parseBoolean(r1)
            if (r0 == 0) goto L_0x02e4
            com.facebook.widget.text.BetterEditTextView r1 = r14.A04
            android.text.method.PasswordTransformationMethod r0 = android.text.method.PasswordTransformationMethod.getInstance()
            r1.setTransformationMethod(r0)
        L_0x02e4:
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r0 = r14.A01
            java.lang.Integer r0 = r0.A00
            if (r0 == 0) goto L_0x02f3
            com.facebook.widget.text.BetterEditTextView r1 = r14.A04
            java.lang.String r0 = X.C187598nT.A01(r0)
            r1.setHint(r0)
        L_0x02f3:
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r1 = r14.A01
            java.lang.Integer r0 = r1.A01
            if (r0 == 0) goto L_0x0156
            int r0 = r0.intValue()
            switch(r0) {
                case 2: goto L_0x0d38;
                case 3: goto L_0x0d26;
                case 4: goto L_0x0300;
                case 5: goto L_0x0d4e;
                case 6: goto L_0x0d43;
                default: goto L_0x0300;
            }
        L_0x0300:
            com.facebook.widget.text.BetterEditTextView r1 = r14.A04
            X.8ng r0 = new X.8ng
            r0.<init>(r14)
            r1.addTextChangedListener(r0)
            return
        L_0x030b:
            com.facebook.widget.text.BetterTextView r0 = r14.A05
            r0.setVisibility(r2)
            goto L_0x02bc
        L_0x0311:
            com.facebook.widget.text.BetterTextView r0 = r14.A06
            r0.setVisibility(r2)
            goto L_0x02a8
        L_0x0317:
            r2 = r13
            X.15s r2 = (X.C188815s) r2
            X.4EU r14 = (X.AnonymousClass4EU) r14
            android.view.View r7 = r14.A0H
            X.853 r7 = (X.AnonymousClass853) r7
            if (r7 == 0) goto L_0x0156
            com.google.common.collect.ImmutableList r0 = r2.A04
            if (r0 == 0) goto L_0x0156
            android.view.TextureView r4 = r7.A00
            android.widget.ImageView r3 = r7.A01
            android.widget.ProgressBar r6 = r7.A02
            r0 = 8
            r4.setVisibility(r0)
            r5 = 4
            r3.setVisibility(r5)
            int r1 = r2.A01
            r0 = 0
            if (r15 != r1) goto L_0x038c
            r6.setVisibility(r0)
        L_0x033d:
            com.google.common.collect.ImmutableList r0 = r2.A04
            int r0 = r0.size()
            int r11 = r15 % r0
            com.google.common.collect.ImmutableList r0 = r2.A04
            java.lang.Object r8 = r0.get(r11)
            com.facebook.messaging.montage.model.art.EffectItem r8 = (com.facebook.messaging.montage.model.art.EffectItem) r8
            long r0 = r8.A01()
            java.lang.String r9 = X.C188815s.A00(r2, r0)
            boolean r0 = r2.A08
            if (r0 == 0) goto L_0x036c
            X.DD8 r1 = r2.A0B
            android.widget.ImageView r0 = r7.A01
            r1.A0B(r11, r0)
        L_0x0360:
            X.DD9 r0 = new X.DD9
            r0.<init>(r2, r15)
            r7.setOnClickListener(r0)
            r7.setContentDescription(r9)
            return
        L_0x036c:
            android.widget.ImageView r0 = r7.A01
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            android.content.Context r0 = r2.A09
            int r0 = X.DDT.A00(r9, r0)
            r1.width = r0
            r3.setLayoutParams(r1)
            r4.setLayoutParams(r1)
            X.DD8 r6 = r2.A0B
            int r0 = r2.A00
            r10 = 0
            if (r15 != r0) goto L_0x0388
            r10 = 1
        L_0x0388:
            r6.A0A(r7, r8, r9, r10, r11)
            goto L_0x0360
        L_0x038c:
            r6.setVisibility(r5)
            goto L_0x033d
        L_0x0390:
            r3 = r13
            X.1Qj r3 = (X.C23661Qj) r3
            int r1 = r3.getItemViewType(r15)
            r4 = 1
            if (r1 == 0) goto L_0x03d9
            if (r1 == r4) goto L_0x03ba
            r0 = 2
            if (r1 != r0) goto L_0x0d6a
            r0 = r14
            X.4EP r0 = (X.AnonymousClass4EP) r0
            r1 = 0
            com.facebook.fbui.widget.glyph.GlyphButton r0 = r0.A00
            r0.A02(r1)
        L_0x03a8:
            X.0uQ r0 = r3.A05
            if (r0 == 0) goto L_0x0156
            android.view.View r1 = r14.A0H
            android.graphics.drawable.Drawable$ConstantState r0 = r0.getConstantState()
            android.graphics.drawable.Drawable r0 = r0.newDrawable()
            X.C37531vp.A02(r1, r0)
            return
        L_0x03ba:
            java.util.List r1 = r3.A09
            X.Exn r0 = r3.A03
            boolean r0 = r0.A00
            int r15 = r15 - r0
            java.lang.Object r2 = r1.get(r15)
            com.facebook.ui.emoji.model.Emoji r2 = (com.facebook.ui.emoji.model.Emoji) r2
            r0 = r14
            X.D2X r0 = (X.D2X) r0
            r0.A0H(r2)
            android.view.View r1 = r0.A0H
            com.facebook.ui.emoji.model.Emoji r0 = r3.A06
            boolean r0 = r2.equals(r0)
            r1.setSelected(r0)
            goto L_0x03a8
        L_0x03d9:
            r2 = r14
            X.4EP r2 = (X.AnonymousClass4EP) r2
            android.view.View r1 = r2.A0H
            com.facebook.ui.emoji.model.Emoji r0 = r3.A06
            if (r0 == 0) goto L_0x03e3
            r4 = 0
        L_0x03e3:
            r1.setSelected(r4)
            int r1 = r3.A00
            com.facebook.fbui.widget.glyph.GlyphButton r0 = r2.A00
            r0.A02(r1)
            goto L_0x03a8
        L_0x03ee:
            r5 = r13
            X.1nS r5 = (X.C33361nS) r5
            int r1 = r5.getItemViewType(r15)
            if (r1 == 0) goto L_0x0448
            r0 = 1
            if (r1 == r0) goto L_0x0eb9
            r0 = 2
            if (r1 == r0) goto L_0x0428
            r0 = 4
            r4 = 1061158912(0x3f400000, float:0.75)
            if (r1 == r0) goto L_0x0e94
            r0 = 5
            if (r1 != r0) goto L_0x0156
            android.view.View r2 = r14.A0H
            X.513 r2 = (X.AnonymousClass513) r2
            java.util.ArrayList r0 = r5.A07
            java.lang.Object r0 = r0.get(r15)
            X.D2A r0 = (X.D2A) r0
            com.facebook.messaging.montage.model.art.ArtItem r1 = r0.A01
            if (r1 == 0) goto L_0x0424
            boolean r0 = r1.A02()
            if (r0 == 0) goto L_0x0424
            X.D2v r14 = (X.C26583D2v) r14
            r14.A00 = r1
            X.1MT r0 = r5.A02
            r2.A0T(r1, r0)
        L_0x0424:
            r2.setScale(r4)
            return
        L_0x0428:
            android.view.View r2 = r14.A0H
            com.facebook.messaging.montage.composer.art.ArtItemView r2 = (com.facebook.messaging.montage.composer.art.ArtItemView) r2
            java.util.ArrayList r0 = r5.A07
            java.lang.Object r0 = r0.get(r15)
            X.D2A r0 = (X.D2A) r0
            com.facebook.messaging.montage.model.art.ArtItem r1 = r0.A01
            if (r1 == 0) goto L_0x0156
            boolean r0 = r1.A02()
            if (r0 == 0) goto L_0x0156
            X.D2v r14 = (X.C26583D2v) r14
            r14.A00 = r1
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r2.A0M(r1, r0)
            return
        L_0x0448:
            java.util.ArrayList r0 = r5.A07
            java.lang.Object r0 = r0.get(r15)
            X.D2A r0 = (X.D2A) r0
            com.facebook.ui.emoji.model.Emoji r0 = r0.A02
            if (r0 == 0) goto L_0x0156
            X.D2X r14 = (X.D2X) r14
            r14.A0H(r0)
            return
        L_0x045a:
            r4 = r13
            X.1Jb r4 = (X.C21881Jb) r4
            int r1 = r4.getItemViewType(r15)
            r0 = 1
            if (r1 != 0) goto L_0x04ac
            com.facebook.messaging.montage.model.art.ArtItem r0 = X.C21881Jb.A01(r4, r15)
            X.D3j r2 = r0.A01
            r0 = 0
            if (r2 == 0) goto L_0x046e
            r0 = 1
        L_0x046e:
            if (r0 == 0) goto L_0x0156
            android.net.Uri r2 = r2.A00
            if (r2 == 0) goto L_0x0156
            android.view.View r5 = r14.A0H
            com.facebook.drawee.fbpipeline.FbDraweeView r5 = (com.facebook.drawee.fbpipeline.FbDraweeView) r5
            android.content.res.Resources r1 = r4.A02
            r0 = 2131828490(0x7f111f0a, float:1.9289922E38)
            java.lang.String r3 = r1.getString(r0)
            X.1Pv r0 = X.C23521Pv.A00(r2)
            X.1Q0 r2 = r0.A02()
            X.2zZ r1 = r4.A03
            java.lang.Class<X.1Jb> r0 = X.C21881Jb.class
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A04(r0)
            r1.A0Q(r0)
            X.306 r0 = r5.A05()
            r1.A09(r0)
            r1.A0A(r2)
            r0 = 1
            r1.A05 = r0
            X.303 r0 = r1.A0F()
            r5.A08(r0)
            r5.setContentDescription(r3)
            return
        L_0x04ac:
            if (r1 != r0) goto L_0x04bc
            android.view.View r2 = r14.A0H
            com.facebook.messaging.montage.composer.art.ArtItemView r2 = (com.facebook.messaging.montage.composer.art.ArtItemView) r2
            com.facebook.messaging.montage.model.art.ArtItem r1 = X.C21881Jb.A01(r4, r15)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r2.A0M(r1, r0)
            return
        L_0x04bc:
            r0 = 2
            r3 = 1065353216(0x3f800000, float:1.0)
            if (r1 != r0) goto L_0x04d2
            android.view.View r2 = r14.A0H
            X.512 r2 = (X.AnonymousClass512) r2
            com.facebook.messaging.montage.model.art.ArtItem r1 = X.C21881Jb.A01(r4, r15)
            X.1MT r0 = r4.A04
            r2.A0T(r1, r0)
            r2.setScale(r3)
            return
        L_0x04d2:
            r0 = 3
            if (r1 != r0) goto L_0x0156
            android.view.View r2 = r14.A0H
            X.513 r2 = (X.AnonymousClass513) r2
            com.facebook.messaging.montage.model.art.ArtItem r1 = X.C21881Jb.A01(r4, r15)
            X.1MT r0 = r4.A04
            r2.A0T(r1, r0)
            r2.setScale(r3)
            return
        L_0x04e6:
            r5 = r13
            X.1TV r5 = (X.AnonymousClass1TV) r5
            java.lang.Integer r0 = r5.A02
            if (r0 == 0) goto L_0x04fd
            int r2 = X.DAI.A00(r0)
            int r0 = r2 % 180
            if (r0 == 0) goto L_0x04f7
            int r2 = r2 + -180
        L_0x04f7:
            android.view.View r1 = r14.A0H
            float r0 = (float) r2
            r1.setRotation(r0)
        L_0x04fd:
            android.view.View r3 = r14.A0H
            android.content.res.Resources r2 = r3.getResources()
            int r1 = r5.getItemViewType(r15)
            r0 = 1
            if (r1 != r0) goto L_0x0599
            com.google.common.collect.ImmutableList r0 = r5.A01
            com.google.common.base.Preconditions.checkNotNull(r0)
            com.google.common.collect.ImmutableList r0 = r5.A01
            java.lang.Object r0 = r0.get(r15)
            com.facebook.messaging.montage.model.art.BaseItem r0 = (com.facebook.messaging.montage.model.art.BaseItem) r0
            com.facebook.messaging.montage.model.art.EffectItem r0 = (com.facebook.messaging.montage.model.art.EffectItem) r0
            if (r0 == 0) goto L_0x0599
            X.7y8 r0 = r0.A07
            int r1 = r0.ordinal()
            r0 = 0
            if (r1 != r0) goto L_0x0599
            r0 = 2131828294(0x7f111e46, float:1.9289525E38)
            java.lang.String r0 = r2.getString(r0)
        L_0x052b:
            r3.setContentDescription(r0)
            com.google.common.collect.ImmutableList r0 = r5.A01
            com.google.common.base.Preconditions.checkNotNull(r0)
            com.google.common.collect.ImmutableList r0 = r5.A01
            java.lang.Object r4 = r0.get(r15)
            com.facebook.messaging.montage.model.art.BaseItem r4 = (com.facebook.messaging.montage.model.art.BaseItem) r4
            android.view.View r3 = r14.A0H
            X.D75 r3 = (X.D75) r3
            android.net.Uri r7 = r4.A00
            if (r7 != 0) goto L_0x0598
            android.net.Uri r7 = r4.A01
        L_0x0545:
            com.google.common.base.Preconditions.checkNotNull(r7)
            com.facebook.drawee.fbpipeline.FbDraweeView r6 = r3.A02
            int r2 = X.AnonymousClass1Y3.BCv
            X.0UN r1 = r3.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2zZ r1 = (X.C61832zZ) r1
            com.facebook.common.callercontext.CallerContext r0 = X.D75.A04
            r1.A0Q(r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = r3.A02
            X.306 r0 = r0.A05()
            r1.A09(r0)
            X.1Q0 r0 = X.AnonymousClass1Q0.A00(r7)
            r1.A0A(r0)
            X.303 r0 = r1.A0F()
            r6.A08(r0)
            r0 = 2131299236(0x7f090ba4, float:1.8216468E38)
            r3.setTag(r0, r4)
            r1 = 2131299237(0x7f090ba5, float:1.821647E38)
            com.google.common.collect.ImmutableList r0 = r5.A01
            r3.setTag(r1, r0)
            boolean r0 = r4 instanceof com.facebook.messaging.montage.model.art.EffectItem
            if (r0 == 0) goto L_0x0156
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Ag6
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.25L r0 = (X.AnonymousClass25L) r0
            com.facebook.messaging.montage.model.art.EffectItem r4 = (com.facebook.messaging.montage.model.art.EffectItem) r4
            X.982 r0 = r0.A01(r4)
            r3.A0M(r0)
            return
        L_0x0598:
            goto L_0x0545
        L_0x0599:
            r0 = 0
            goto L_0x052b
        L_0x059b:
            r6 = r13
            X.1Ij r6 = (X.C21701Ij) r6
            java.lang.Integer r0 = r6.A06
            if (r0 == 0) goto L_0x05b2
            int r2 = X.DAI.A00(r0)
            int r0 = r2 % 180
            if (r0 == 0) goto L_0x05ac
            int r2 = r2 + -180
        L_0x05ac:
            android.view.View r1 = r14.A0H
            float r0 = (float) r2
            r1.setRotation(r0)
        L_0x05b2:
            android.view.View r4 = r14.A0H
            android.content.res.Resources r3 = r4.getResources()
            int r2 = r6.getItemViewType(r15)
            r1 = 0
            if (r2 != 0) goto L_0x0698
            r0 = 2131828289(0x7f111e41, float:1.9289515E38)
            java.lang.String r1 = r3.getString(r0)
        L_0x05c6:
            r4.setContentDescription(r1)
            int r0 = r6.getItemViewType(r15)
            if (r0 == 0) goto L_0x0f20
            int r0 = r6.getItemViewType(r15)
            r7 = 3
            if (r0 == r7) goto L_0x0f20
            com.facebook.messaging.montage.model.art.BaseItem r5 = X.C21701Ij.A02(r6, r15)
            com.google.common.base.Preconditions.checkNotNull(r5)
            android.view.View r4 = r14.A0H
            X.D6x r4 = (X.C26679D6x) r4
            int r3 = r6.A00
            r0 = -1
            if (r3 == r0) goto L_0x0694
            java.lang.String r2 = ""
            java.lang.String r1 = "CircularArtItem"
            int r15 = r15 - r3
            java.lang.String r0 = java.lang.String.valueOf(r15)
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
        L_0x05f3:
            r4.setTag(r0)
            X.DAB r0 = r6.A03
            if (r0 == 0) goto L_0x0612
            X.Czy r3 = r6.A05
            int r2 = X.AnonymousClass1Y3.A1h
            com.facebook.messaging.montage.composer.art.circularpicker.CircularArtPickerView r0 = r0.A00
            X.0UN r1 = r0.A06
            r0 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.CxE r2 = (X.CxE) r2
            java.util.Map r1 = java.util.Collections.emptyMap()
            com.fasterxml.jackson.databind.node.ArrayNode r0 = r2.A02
            X.CxE.A01(r2, r5, r3, r1, r0)
        L_0x0612:
            boolean r0 = r5 instanceof com.facebook.messaging.montage.model.art.ArtCategoryItem
            if (r0 == 0) goto L_0x068c
            X.DAA r0 = r6.A04
            if (r0 == 0) goto L_0x068c
            com.facebook.messaging.montage.composer.art.circularpicker.CircularArtPickerView r2 = r0.A00
            com.facebook.messaging.montage.model.art.ArtCategoryItem r1 = r2.A0I
            if (r1 == 0) goto L_0x068c
            if (r1 != r5) goto L_0x068c
            if (r1 == 0) goto L_0x068a
            X.D6t r0 = r2.A09
            if (r0 == 0) goto L_0x068a
            com.facebook.messaging.montage.model.art.BaseItem r0 = r0.A09
        L_0x062a:
            if (r0 == 0) goto L_0x068c
            if (r1 == 0) goto L_0x0688
            X.D6t r0 = r2.A09
            if (r0 == 0) goto L_0x0688
            com.facebook.messaging.montage.model.art.BaseItem r1 = r0.A09
        L_0x0634:
            android.net.Uri r0 = r1.A00
            if (r0 == 0) goto L_0x0685
        L_0x0638:
            r4.A0M(r0)
            r0 = 2131299238(0x7f090ba6, float:1.8216472E38)
            r4.setTag(r0, r5)
            r1 = 2131299239(0x7f090ba7, float:1.8216474E38)
            X.Czy r0 = r6.A05
            r4.setTag(r1, r0)
            boolean r0 = r5 instanceof com.facebook.messaging.montage.model.art.EffectItem
            if (r0 == 0) goto L_0x0156
            r2 = 5
            int r1 = X.AnonymousClass1Y3.B7R
            X.0UN r0 = r6.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.Czz r0 = (X.C26513Czz) r0
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x0ef9
            r2 = 4
            int r1 = X.AnonymousClass1Y3.ASK
            X.0UN r0 = r6.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0VL r1 = (X.AnonymousClass0VL) r1
            X.D8f r0 = new X.D8f
            r0.<init>(r6, r5)
            com.google.common.util.concurrent.ListenableFuture r3 = r1.CIF(r0)
            X.D7S r2 = new X.D7S
            r2.<init>(r6, r4, r5)
            int r1 = X.AnonymousClass1Y3.ADs
            X.0UN r0 = r6.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            java.util.concurrent.ExecutorService r0 = (java.util.concurrent.ExecutorService) r0
            X.C05350Yp.A08(r3, r2, r0)
            return
        L_0x0685:
            android.net.Uri r0 = r1.A01
            goto L_0x0638
        L_0x0688:
            r1 = 0
            goto L_0x0634
        L_0x068a:
            r0 = 0
            goto L_0x062a
        L_0x068c:
            android.net.Uri r0 = r5.A00
            if (r0 != 0) goto L_0x0693
            android.net.Uri r0 = r5.A01
            goto L_0x0638
        L_0x0693:
            goto L_0x0638
        L_0x0694:
            java.lang.String r0 = ""
            goto L_0x05f3
        L_0x0698:
            r0 = 1
            if (r2 != r0) goto L_0x06a4
            r0 = 2131828287(0x7f111e3f, float:1.928951E38)
            java.lang.String r1 = r3.getString(r0)
            goto L_0x05c6
        L_0x06a4:
            r0 = 2
            if (r2 != r0) goto L_0x06c1
            com.facebook.messaging.montage.model.art.BaseItem r0 = X.C21701Ij.A02(r6, r15)
            com.facebook.messaging.montage.model.art.EffectItem r0 = (com.facebook.messaging.montage.model.art.EffectItem) r0
            if (r0 == 0) goto L_0x05c6
            X.7y8 r0 = r0.A07
            int r2 = r0.ordinal()
            r0 = 0
            if (r2 != r0) goto L_0x05c6
            r0 = 2131828294(0x7f111e46, float:1.9289525E38)
            java.lang.String r1 = r3.getString(r0)
            goto L_0x05c6
        L_0x06c1:
            r0 = 4
            if (r2 != r0) goto L_0x05c6
            com.facebook.messaging.montage.model.art.BaseItem r0 = X.C21701Ij.A02(r6, r15)
            com.facebook.messaging.montage.model.art.ArtCategoryItem r0 = (com.facebook.messaging.montage.model.art.ArtCategoryItem) r0
            if (r0 == 0) goto L_0x05c6
            r0 = 2131828288(0x7f111e40, float:1.9289513E38)
            java.lang.String r1 = r3.getString(r0)
            goto L_0x05c6
        L_0x06d5:
            r4 = r13
            X.1Qh r4 = (X.C23641Qh) r4
            int r1 = r14.A01
            if (r1 != 0) goto L_0x0702
            boolean r3 = r4.A0E
            if (r3 == 0) goto L_0x0f23
            X.4KN r14 = (X.AnonymousClass4KN) r14
            r2 = 8
            r1 = 0
            android.view.View r0 = r14.A01
            if (r3 == 0) goto L_0x06f9
            r0.setVisibility(r2)
            android.view.View r0 = r14.A00
            r0.setVisibility(r1)
        L_0x06f1:
            java.lang.String r1 = r4.A09
            android.widget.TextView r0 = r14.A02
            r0.setText(r1)
            return
        L_0x06f9:
            r0.setVisibility(r1)
            android.view.View r0 = r14.A00
            r0.setVisibility(r2)
            goto L_0x06f1
        L_0x0702:
            r0 = 1
            if (r1 != r0) goto L_0x0812
            java.util.List r0 = r4.A0K
            java.lang.Object r0 = r0.get(r15)
            X.4KG r0 = (X.AnonymousClass4KG) r0
            com.facebook.messaging.model.threads.ThreadParticipant r0 = r0.A00
            r6 = r0
            X.4KS r14 = (X.AnonymousClass4KS) r14
            if (r0 != 0) goto L_0x071c
            java.lang.String r1 = "Messenger Montage Seen sheet"
            java.lang.String r0 = "Attempting to bind null participant"
            X.C010708t.A0I(r1, r0)
            return
        L_0x071c:
            android.view.View r5 = r14.A0H
            com.facebook.messaging.montage.viewer.seensheet.MontageSeenByListItemView r5 = (com.facebook.messaging.montage.viewer.seensheet.MontageSeenByListItemView) r5
            X.1Qh r3 = r14.A00
            boolean r0 = r3.A0E
            r2 = 0
            if (r0 == 0) goto L_0x075b
            int r1 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r3.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0r6 r1 = (X.AnonymousClass0r6) r1
            com.facebook.user.model.UserKey r0 = r6.A00()
            boolean r1 = r1.A0Z(r0)
            X.1Qh r0 = r14.A00
            boolean r0 = r0.A0E
            r5.A06 = r6
            com.facebook.messaging.montage.viewer.seensheet.MontageSeenByListItemView.A00(r5, r6, r1)
            r2 = 8
            android.view.View r1 = r5.A01
            if (r0 == 0) goto L_0x0756
            r1.setVisibility(r2)
        L_0x074b:
            android.widget.TextView r0 = r5.A04
            r0.setVisibility(r2)
            android.widget.SeekBar r0 = r5.A02
            r0.setVisibility(r2)
            return
        L_0x0756:
            r0 = 0
            r1.setVisibility(r0)
            goto L_0x074b
        L_0x075b:
            int r1 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r3.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0r6 r1 = (X.AnonymousClass0r6) r1
            com.facebook.user.model.UserKey r0 = r6.A00()
            boolean r10 = r1.A0Z(r0)
            X.1Qh r0 = r14.A00
            java.util.Map r1 = r0.A0B
            com.facebook.user.model.UserKey r0 = r6.A00()
            java.lang.String r0 = r0.id
            java.lang.Object r4 = r1.get(r0)
            java.lang.String r4 = (java.lang.String) r4
            X.1Qh r0 = r14.A00
            java.util.Map r1 = r0.A0D
            com.facebook.user.model.UserKey r0 = r6.A00()
            java.lang.String r0 = r0.id
            java.lang.Object r3 = r1.get(r0)
            java.lang.Integer r3 = (java.lang.Integer) r3
            X.1Qh r0 = r14.A00
            java.util.Map r1 = r0.A0C
            com.facebook.user.model.UserKey r0 = r6.A00()
            java.lang.String r0 = r0.id
            java.lang.Object r8 = r1.get(r0)
            java.lang.String r8 = (java.lang.String) r8
            X.1Qh r0 = r14.A00
            X.0V0 r1 = r0.A06
            if (r1 != 0) goto L_0x0809
            r9 = 0
        L_0x07a4:
            r5.A06 = r6
            r7 = 1
            if (r10 == 0) goto L_0x07b0
            boolean r1 = X.C06850cB.A0B(r8)
            r0 = 1
            if (r1 != 0) goto L_0x07b1
        L_0x07b0:
            r0 = 0
        L_0x07b1:
            com.facebook.messaging.montage.viewer.seensheet.MontageSeenByListItemView.A00(r5, r6, r0)
            r6 = 8
            android.widget.TextView r0 = r5.A04
            if (r4 == 0) goto L_0x0805
            r0.setText(r4)
            android.widget.TextView r0 = r5.A04
            r0.setVisibility(r2)
        L_0x07c2:
            android.widget.SeekBar r1 = r5.A02
            if (r3 == 0) goto L_0x0801
            int r0 = r3.intValue()
            r1.setProgress(r0)
            android.widget.SeekBar r0 = r5.A02
            r0.setVisibility(r2)
            android.widget.SeekBar r0 = r5.A02
            r0.setEnabled(r2)
        L_0x07d7:
            com.facebook.messaging.montage.forked.viewer.footer.AnimatedReactionBar r4 = r5.A07
            r2 = 2
            int r1 = X.AnonymousClass1Y3.A0x
            X.0UN r0 = r5.A05
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.4eZ r3 = (X.C94464eZ) r3
            com.google.common.collect.ImmutableList r2 = X.C99674pS.A00(r8)
            com.google.common.collect.ImmutableList r1 = X.C99674pS.A01(r9)
            r0 = 0
            com.google.common.collect.ImmutableList r0 = r3.A01(r2, r1, r0)
            r4.A0T(r0, r7)
            android.view.View r0 = r5.A01
            r0.setVisibility(r6)
            android.view.View r0 = r5.A00
            if (r0 == 0) goto L_0x0156
            r0.setVisibility(r6)
            return
        L_0x0801:
            r1.setVisibility(r6)
            goto L_0x07d7
        L_0x0805:
            r0.setVisibility(r6)
            goto L_0x07c2
        L_0x0809:
            com.facebook.user.model.UserKey r0 = r6.A00()
            java.util.Collection r9 = r1.AbK(r0)
            goto L_0x07a4
        L_0x0812:
            r0 = 2
            if (r1 != r0) goto L_0x0832
            X.4KK r14 = (X.AnonymousClass4KK) r14
            int r4 = r4.A00
            com.facebook.widget.text.BetterTextView r3 = r14.A00
            android.content.res.Resources r2 = r3.getResources()
            r1 = 2131689622(0x7f0f0096, float:1.9008265E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r0 = r2.getQuantityString(r1, r4, r0)
            r3.setText(r0)
            return
        L_0x0832:
            r0 = 3
            if (r1 != r0) goto L_0x08c4
            X.4EX r14 = (X.AnonymousClass4EX) r14
            java.util.List r2 = r4.A0A
            com.facebook.widget.CustomLinearLayout r0 = r14.A01
            r0.removeAllViews()
            com.facebook.widget.CustomLinearLayout r1 = r14.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r14.A00
            int r0 = r0.B2D()
            X.C15980wI.A00(r1, r0)
            java.util.Iterator r7 = r2.iterator()
        L_0x084d:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0156
            java.lang.Object r6 = r7.next()
            com.facebook.messaging.model.messages.MontageFeedbackPollOption r6 = (com.facebook.messaging.model.messages.MontageFeedbackPollOption) r6
            X.421 r3 = new X.421
            com.facebook.widget.CustomLinearLayout r0 = r14.A01
            android.content.Context r4 = r0.getContext()
            r2 = 0
            r0 = 0
            r3.<init>(r4, r2, r0)
            X.1Qh r0 = r14.A02
            boolean r0 = r0.A0F
            com.facebook.widget.text.BetterTextView r1 = r3.A01
            if (r0 == 0) goto L_0x08c1
            int r0 = r6.A02
        L_0x0870:
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r1.setText(r0)
            com.facebook.widget.text.BetterTextView r5 = r3.A00
            android.content.res.Resources r4 = r3.getResources()
            r2 = 2131689616(0x7f0f0090, float:1.9008252E38)
            int r1 = r6.A03
            java.lang.String r0 = r6.A04
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r0 = r4.getQuantityString(r2, r1, r0)
            r5.setText(r0)
            r0 = 2131301389(0x7f09140d, float:1.8220835E38)
            android.view.View r2 = X.C012809p.A01(r3, r0)
            com.facebook.widget.text.BetterTextView r2 = (com.facebook.widget.text.BetterTextView) r2
            r0 = 2131299520(0x7f090cc0, float:1.8217044E38)
            android.view.View r1 = X.C012809p.A01(r3, r0)
            com.facebook.widget.text.BetterTextView r1 = (com.facebook.widget.text.BetterTextView) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r14.A00
            X.0y3 r0 = r0.AzL()
            int r0 = r0.AhV()
            r2.setTextColor(r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r14.A00
            X.0y3 r0 = r0.AzL()
            int r0 = r0.AhV()
            r1.setTextColor(r0)
            com.facebook.widget.CustomLinearLayout r0 = r14.A01
            r0.addView(r3)
            goto L_0x084d
        L_0x08c1:
            int r0 = r6.A03
            goto L_0x0870
        L_0x08c4:
            r0 = 4
            if (r1 != r0) goto L_0x0156
            X.6Ls r14 = (X.C133316Ls) r14
            r14.A0G()
            return
        L_0x08cd:
            r5 = r13
            X.1vi r5 = (X.C37461vi) r5
            X.4EO r14 = (X.AnonymousClass4EO) r14
            com.google.common.collect.ImmutableList r0 = r5.A00
            int r0 = r0.size()
            if (r15 == r0) goto L_0x0156
            X.2mv r0 = r5.A03
            java.util.TimeZone r1 = r0.A05
            java.util.Locale r0 = r5.A02
            java.util.Calendar r4 = java.util.Calendar.getInstance(r1, r0)
            com.google.common.collect.ImmutableList r0 = r5.A00
            java.lang.Object r0 = r0.get(r15)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            long r2 = (long) r0
            r0 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r0
            r4.setTimeInMillis(r2)
            X.5wQ r14 = (X.C126175wQ) r14
            android.view.View r2 = r14.A00
            com.facebook.fig.deprecated.button.FigButton r2 = (com.facebook.fig.deprecated.button.FigButton) r2
            java.text.SimpleDateFormat r1 = r5.A01
            java.util.Date r0 = r4.getTime()
            java.lang.String r0 = r1.format(r0)
            r2.setText(r0)
            com.google.common.collect.ImmutableList r0 = r5.A00
            java.lang.Object r0 = r0.get(r15)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            r14.A00 = r0
            return
        L_0x0919:
            r7 = r13
            X.1vg r7 = (X.C37441vg) r7
            X.Bou r14 = (X.C23885Bou) r14
            com.google.common.collect.ImmutableList r0 = r7.A03
            java.lang.Object r3 = r0.get(r15)
            X.Bk6 r3 = (X.C23666Bk6) r3
            int r1 = r3.A00
            switch(r1) {
                case 1: goto L_0x092c;
                case 2: goto L_0x0997;
                case 3: goto L_0x0997;
                case 4: goto L_0x0a2f;
                case 5: goto L_0x0aac;
                case 6: goto L_0x0aac;
                case 7: goto L_0x0b0a;
                case 8: goto L_0x0fce;
                case 9: goto L_0x0ffb;
                default: goto L_0x092b;
            }
        L_0x092b:
            return
        L_0x092c:
            X.Bot r14 = (X.C23884Bot) r14
            android.net.Uri r4 = r3.A01
            r2 = 0
            if (r4 == 0) goto L_0x093f
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = r14.A04
            com.facebook.common.callercontext.CallerContext r0 = X.C37441vg.A04
            r1.A09(r4, r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = r14.A04
            r0.setVisibility(r2)
        L_0x093f:
            java.lang.String r1 = r3.A09
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0956
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setVisibility(r2)
        L_0x0956:
            java.lang.String r1 = r3.A0D
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x096d
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A09
            r0.setVisibility(r2)
        L_0x096d:
            java.lang.String r1 = r3.A0C
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x097f
            com.facebook.resources.ui.FbTextView r0 = r14.A07
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A07
            r0.setVisibility(r2)
        L_0x097f:
            java.lang.String r1 = r3.A0B
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0156
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A0A
            r0.setVisibility(r2)
            return
        L_0x0997:
            X.Bot r14 = (X.C23884Bot) r14
            r2 = 0
            r0 = 2
            if (r1 != r0) goto L_0x0a26
            android.widget.ImageView r6 = r14.A00
            int r1 = X.AnonymousClass1Y3.AOz
            X.0UN r0 = r7.A01
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1MX r5 = (X.AnonymousClass1MX) r5
            r4 = 2131231086(0x7f08016e, float:1.8078243E38)
            android.content.Context r1 = r7.A00
            r0 = 2132082802(0x7f150072, float:1.9805728E38)
            int r0 = X.AnonymousClass01R.A00(r1, r0)
            android.graphics.drawable.Drawable r0 = r5.A04(r4, r0)
            r6.setImageDrawable(r0)
            android.widget.ImageView r4 = r14.A00
            android.content.Context r1 = r7.A00
            r0 = 2132214175(0x7f17019f, float:2.0072184E38)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass01R.A03(r1, r0)
            r4.setBackground(r0)
        L_0x09ca:
            android.widget.ImageView r0 = r14.A00
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r1 = 8
            r0.setVisibility(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A07
            r0.setVisibility(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A09
            r0.setVisibility(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A0A
            r0.setVisibility(r1)
            java.lang.String r1 = r3.A0D
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x09fc
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setVisibility(r2)
        L_0x09fc:
            java.lang.String r1 = r3.A0C
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0a13
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A09
            r0.setVisibility(r2)
        L_0x0a13:
            java.lang.String r1 = r3.A0B
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0156
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setVisibility(r2)
            return
        L_0x0a26:
            android.widget.ImageView r1 = r14.A00
            r0 = 2132214173(0x7f17019d, float:2.007218E38)
            r1.setImageResource(r0)
            goto L_0x09ca
        L_0x0a2f:
            X.Bot r14 = (X.C23884Bot) r14
            android.net.Uri r4 = r3.A01
            r2 = 0
            if (r4 == 0) goto L_0x0a42
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = r14.A04
            com.facebook.common.callercontext.CallerContext r0 = X.C37441vg.A04
            r1.A09(r4, r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = r14.A04
            r0.setVisibility(r2)
        L_0x0a42:
            java.lang.String r1 = r3.A09
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0a59
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setVisibility(r2)
        L_0x0a59:
            java.lang.String r1 = r3.A0D
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0a70
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A09
            r0.setVisibility(r2)
        L_0x0a70:
            java.lang.String r1 = r3.A0C
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0a87
            com.facebook.resources.ui.FbTextView r0 = r14.A07
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A07
            r0.setVisibility(r2)
            com.facebook.resources.ui.FbTextView r0 = r14.A0A
            r0.setVisibility(r2)
        L_0x0a87:
            java.lang.String r1 = r3.A0B
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0a99
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A08
            r0.setVisibility(r2)
        L_0x0a99:
            java.lang.String r1 = r3.A0E
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0156
            com.facebook.resources.ui.FbTextView r0 = r14.A09
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A09
            r0.setVisibility(r2)
            return
        L_0x0aac:
            X.Bot r14 = (X.C23884Bot) r14
            r4 = 0
            r0 = 5
            if (r1 != r0) goto L_0x0af3
            android.net.Uri r2 = r3.A01
            if (r2 == 0) goto L_0x0ac2
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = r14.A03
            com.facebook.common.callercontext.CallerContext r0 = X.C37441vg.A04
            r1.A09(r2, r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = r14.A03
            r0.setVisibility(r4)
        L_0x0ac2:
            com.facebook.resources.ui.FbTextView r0 = r14.A07
            r1 = 8
            r0.setVisibility(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A0A
            r0.setVisibility(r1)
            java.lang.String r1 = r3.A08
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0ae0
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setVisibility(r4)
        L_0x0ae0:
            java.lang.String r1 = r3.A07
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0156
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setVisibility(r4)
            return
        L_0x0af3:
            r0 = 6
            if (r1 != r0) goto L_0x0ac2
            android.widget.ImageView r2 = r14.A01
            android.content.Context r1 = r7.A00
            r0 = 2131230757(0x7f080025, float:1.8077576E38)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass01R.A03(r1, r0)
            r2.setImageDrawable(r0)
            android.widget.ImageView r0 = r14.A01
            r0.setVisibility(r4)
            goto L_0x0ac2
        L_0x0b0a:
            com.facebook.payments.paymentmethods.model.NewCreditCardOption r0 = r3.A02
            if (r0 == 0) goto L_0x0156
            X.Bor r14 = (X.C23882Bor) r14
            java.lang.String r1 = r0.mTitle
            com.facebook.resources.ui.FbTextView r0 = r14.A00
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A00
            r3 = 0
            r0.setVisibility(r3)
            com.facebook.resources.ui.FbTextView r2 = r14.A00
            android.content.Context r0 = r7.A00
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2132148233(0x7f160009, float:1.9938438E38)
            int r0 = r1.getDimensionPixelOffset(r0)
            r2.setPadding(r3, r0, r3, r3)
            return
        L_0x0b30:
            r6 = r13
            X.1IC r6 = (X.AnonymousClass1IC) r6
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r0 = r0.get(r15)
            com.facebook.fbpay.api.FbPayTransactionDetailsViewModel r0 = (com.facebook.fbpay.api.FbPayTransactionDetailsViewModel) r0
            int r0 = r0.B9K()
            switch(r0) {
                case 0: goto L_0x102d;
                case 1: goto L_0x1084;
                case 2: goto L_0x10a4;
                case 3: goto L_0x10cf;
                case 4: goto L_0x0b43;
                case 5: goto L_0x1122;
                case 6: goto L_0x1147;
                default: goto L_0x0b42;
            }
        L_0x0b42:
            return
        L_0x0b43:
            X.C7U r14 = (X.C7U) r14
            com.facebook.payments.transactionhub.views.FbPayButtonView r5 = r14.A00
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r1 = r0.get(r15)
            com.facebook.fbpay.api.FBPayTransactionDetailsButtonModel r1 = (com.facebook.fbpay.api.FBPayTransactionDetailsButtonModel) r1
            java.lang.String r4 = r1.A01
            com.facebook.widget.text.BetterButton r0 = r5.A03
            r0.setText(r4)
            com.google.common.collect.ImmutableList r6 = r1.A00
            if (r6 == 0) goto L_0x0b81
            boolean r0 = r6.isEmpty()
            if (r0 != 0) goto L_0x0b81
            android.content.Context r4 = r5.getContext()
            X.4fG r3 = new X.4fG
            int r2 = X.AnonymousClass1Y3.AP2
            X.0UN r1 = r5.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.BpH r1 = (X.C23904BpH) r1
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r5.A02
            r3.<init>(r4, r6, r1, r0)
            com.facebook.widget.text.BetterButton r1 = r5.A03
            X.40P r0 = new X.40P
            r0.<init>(r3)
            r1.setOnClickListener(r0)
            return
        L_0x0b81:
            android.content.Context r3 = r5.getContext()
            java.lang.String r2 = r1.A02
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x0156
            com.facebook.widget.text.BetterButton r1 = r5.A03
            X.C7R r0 = new X.C7R
            r0.<init>(r5, r4, r2, r3)
            r1.setOnClickListener(r0)
            return
        L_0x0b98:
            r2 = r13
            X.1Qg r2 = (X.C23631Qg) r2
            int r1 = r2.getItemViewType(r15)
            r0 = 1
            if (r1 != r0) goto L_0x0156
            X.Civ r14 = (X.C25603Civ) r14
            java.util.List r0 = r2.A01
            java.lang.Object r3 = r0.get(r15)
            X.Cjo r3 = (X.C25656Cjo) r3
            if (r3 == 0) goto L_0x0156
            X.5jX r1 = r3.A00
            r2 = 4
            if (r1 == 0) goto L_0x0bd2
            com.facebook.widget.tiles.ThreadTileView r0 = r14.A02
            r0.A03(r1)
        L_0x0bb8:
            java.lang.String r1 = r3.A01
            if (r1 == 0) goto L_0x0bcc
            com.facebook.widget.text.BetterTextView r0 = r14.A01
            r0.setText(r1)
        L_0x0bc1:
            android.view.View r1 = r14.A00
            X.CfQ r0 = new X.CfQ
            r0.<init>(r14, r3, r15)
            r1.setOnClickListener(r0)
            return
        L_0x0bcc:
            com.facebook.widget.text.BetterTextView r0 = r14.A01
            r0.setVisibility(r2)
            goto L_0x0bc1
        L_0x0bd2:
            com.facebook.widget.tiles.ThreadTileView r0 = r14.A02
            r0.setVisibility(r2)
            goto L_0x0bb8
        L_0x0bd8:
            r1 = r13
            X.1Sm r1 = (X.C24151Sm) r1
            X.4Eg r14 = (X.C87304Eg) r14
            java.util.List r0 = r1.A01
            int r0 = r0.size()
            int r15 = r15 - r0
            if (r15 < 0) goto L_0x0156
            X.1Sn r0 = r1.A03
            int r0 = r0.ArU()
            if (r15 >= r0) goto L_0x0156
            X.1Sn r1 = r1.A03
            X.1o8 r0 = r14.A00
            r1.BPy(r0, r15)
            return
        L_0x0bf6:
            r5 = r13
            X.1Qx r5 = (X.C23801Qx) r5
            X.4EQ r14 = (X.AnonymousClass4EQ) r14
            android.view.View r4 = r14.A0H
            com.facebook.litho.LithoView r4 = (com.facebook.litho.LithoView) r4
            android.view.ViewGroup$LayoutParams r3 = r4.getLayoutParams()
            X.6Pv r0 = r5.A01
            int r0 = r0.A05(r15)
            int r0 = android.view.View.MeasureSpec.getMode(r0)
            r2 = -2
            r1 = -1
            if (r0 != 0) goto L_0x0c12
            r1 = -2
        L_0x0c12:
            X.6Pv r0 = r5.A01
            int r0 = r0.A04(r15)
            int r0 = android.view.View.MeasureSpec.getMode(r0)
            if (r0 == 0) goto L_0x0c1f
            r2 = -1
        L_0x0c1f:
            if (r3 == 0) goto L_0x0c2f
            r3.width = r1
            r3.height = r2
        L_0x0c25:
            X.6Pv r0 = r5.A01
            com.facebook.litho.ComponentTree r0 = r0.A07(r15)
            r4.A0b(r0)
            return
        L_0x0c2f:
            X.1R7 r0 = new X.1R7
            r0.<init>(r1, r2)
            r4.setLayoutParams(r0)
            goto L_0x0c25
        L_0x0c38:
            r0 = r13
            X.1Qw r0 = (X.C23791Qw) r0
            X.4EN r14 = (X.AnonymousClass4EN) r14
            java.util.List r0 = r0.A01
            java.lang.Object r5 = r0.get(r15)
            com.facebook.account.recovery.common.model.AccountCandidateModel r5 = (com.facebook.account.recovery.common.model.AccountCandidateModel) r5
            com.facebook.user.tiles.UserTileView r4 = r14.A02
            com.facebook.user.profilepic.PicSquare r3 = new com.facebook.user.profilepic.PicSquare
            com.facebook.user.profilepic.PicSquareUrlWithSize r2 = new com.facebook.user.profilepic.PicSquareUrlWithSize
            int r1 = r4.getWidth()
            java.lang.String r0 = r5.profilePictureUri
            r2.<init>(r1, r0)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r2)
            r3.<init>(r0)
            X.1JY r0 = X.AnonymousClass1JY.A06(r3)
            r4.A03(r0)
            android.widget.TextView r1 = r14.A01
            java.lang.String r0 = r5.name
            r1.setText(r0)
            android.widget.TextView r1 = r14.A00
            java.lang.String r0 = r5.networkName
            r1.setText(r0)
            android.view.View r0 = r14.A0H
            r0.setTag(r5)
            return
        L_0x0c76:
            r1 = r13
            X.1Ql r1 = (X.C23681Ql) r1
            X.7G8 r14 = (X.AnonymousClass7G8) r14
            java.util.List r0 = r1.A01
            int r0 = r0.size()
            if (r15 >= r0) goto L_0x0c8f
            java.util.List r0 = r1.A01
            java.lang.Object r0 = r0.get(r15)
            com.facebook.messaging.accountswitch.model.MessengerAccountInfo r0 = (com.facebook.messaging.accountswitch.model.MessengerAccountInfo) r0
        L_0x0c8b:
            r14.A0G(r0)
            return
        L_0x0c8f:
            r0 = 0
            goto L_0x0c8b
        L_0x0c91:
            android.text.SpannableStringBuilder r3 = new android.text.SpannableStringBuilder
            r3.<init>()
            r3.append(r6)
            X.5jc r2 = new X.5jc
            r2.<init>(r14, r7)
            int r1 = X.CoM.A00(r6)
            r0 = 34
            r3.setSpan(r2, r4, r1, r0)
            r5.append(r3)
            android.text.method.MovementMethod r0 = android.text.method.LinkMovementMethod.getInstance()
            r5.setMovementMethod(r0)
            return
        L_0x0cb2:
            X.8nb r14 = (X.C187668nb) r14
            r14.A01 = r1
            java.lang.String r1 = r1.A07
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            r3 = 0
            r2 = 8
            if (r0 != 0) goto L_0x0ce0
            com.facebook.widget.text.BetterTextView r0 = r14.A0B
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r0 = r14.A0B
            r0.setVisibility(r3)
        L_0x0ccb:
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r0 = r14.A01
            java.lang.String r1 = r0.A06
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x0ce6
            com.facebook.widget.text.BetterTextView r0 = r14.A0A
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r0 = r14.A0A
            r0.setVisibility(r3)
            return
        L_0x0ce0:
            com.facebook.widget.text.BetterTextView r0 = r14.A0B
            r0.setVisibility(r2)
            goto L_0x0ccb
        L_0x0ce6:
            com.facebook.widget.text.BetterTextView r0 = r14.A0A
            r0.setVisibility(r2)
            return
        L_0x0cec:
            X.8nk r14 = (X.C187748nk) r14
            r14.A00 = r1
            java.lang.String r1 = r1.A07
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            r3 = 0
            r2 = 8
            if (r0 != 0) goto L_0x0d1a
            com.facebook.widget.text.BetterTextView r0 = r14.A08
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r0 = r14.A08
            r0.setVisibility(r3)
        L_0x0d05:
            com.facebook.messaging.business.informationidentify.model.PIIQuestion r0 = r14.A00
            java.lang.String r1 = r0.A06
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x0d20
            com.facebook.widget.text.BetterTextView r0 = r14.A07
            r0.setText(r1)
            com.facebook.widget.text.BetterTextView r0 = r14.A07
            r0.setVisibility(r3)
            return
        L_0x0d1a:
            com.facebook.widget.text.BetterTextView r0 = r14.A08
            r0.setVisibility(r2)
            goto L_0x0d05
        L_0x0d20:
            com.facebook.widget.text.BetterTextView r0 = r14.A07
            r0.setVisibility(r2)
            return
        L_0x0d26:
            java.lang.Integer r2 = r1.A00
            if (r2 != 0) goto L_0x0d37
            java.lang.Integer r2 = X.AnonymousClass07B.A0C
        L_0x0d2c:
            com.facebook.widget.text.BetterEditTextView r1 = r14.A04
            X.8nX r0 = new X.8nX
            r0.<init>(r14, r2)
            r1.addTextChangedListener(r0)
            return
        L_0x0d37:
            goto L_0x0d2c
        L_0x0d38:
            com.facebook.widget.text.BetterEditTextView r1 = r14.A04
            X.8nW r0 = new X.8nW
            r0.<init>(r14)
            r1.addTextChangedListener(r0)
            return
        L_0x0d43:
            com.facebook.widget.text.BetterEditTextView r1 = r14.A04
            X.8nZ r0 = new X.8nZ
            r0.<init>(r14)
            r1.addTextChangedListener(r0)
            return
        L_0x0d4e:
            java.lang.String r1 = r1.A03
            if (r1 == 0) goto L_0x0d67
            int r0 = java.lang.Integer.parseInt(r1)
            if (r0 <= 0) goto L_0x0d67
            int r2 = java.lang.Integer.parseInt(r1)
        L_0x0d5c:
            com.facebook.widget.text.BetterEditTextView r1 = r14.A04
            X.8nY r0 = new X.8nY
            r0.<init>(r14, r2)
            r1.addTextChangedListener(r0)
            return
        L_0x0d67:
            r2 = 30
            goto L_0x0d5c
        L_0x0d6a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "EmojilikePickerView onBindViewHolder with unknown view type"
            r1.<init>(r0)
            throw r1
        L_0x0d72:
            r5 = r13
            X.1Qi r5 = (X.C23651Qi) r5
            int r3 = r14.A01
            java.lang.String r6 = "folder holder not matched "
            r2 = 1
            if (r3 == r2) goto L_0x0e3d
            r0 = 2
            r4 = 0
            if (r3 == r0) goto L_0x0dfc
            r0 = 3
            if (r3 != r0) goto L_0x0df0
            boolean r0 = r14 instanceof X.C88564Kw
            if (r0 == 0) goto L_0x0e82
            X.4Kw r14 = (X.C88564Kw) r14
            java.util.List r0 = r5.A04
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0de8
            r1 = 0
        L_0x0d92:
            int r3 = X.AnonymousClass1Y3.ASj
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r3, r0)
            X.3g8 r0 = (X.C73473g8) r0
            boolean r0 = r0.A0D()
            if (r0 == 0) goto L_0x0da9
            java.util.List r0 = r5.A04
            boolean r1 = r0.isEmpty()
            r1 = r1 ^ r2
        L_0x0da9:
            java.util.List r0 = r5.A03
            int r15 = r15 - r1
            java.lang.Object r2 = r0.get(r15)
            X.CvP r2 = (X.C26285CvP) r2
            android.widget.ImageView r1 = r14.A05
            android.graphics.drawable.Drawable r0 = r2.A01
            r1.setImageDrawable(r0)
            android.widget.TextView r1 = r14.A06
            java.lang.String r0 = r2.A02
            r1.setText(r0)
            android.content.Intent r0 = r2.A00
            r14.A00 = r0
            android.view.View r1 = r14.A04
            java.lang.String r0 = r2.A02
            r1.setContentDescription(r0)
            android.view.View r1 = r14.A04
            X.4U0 r0 = new X.4U0
            r0.<init>(r14)
            r1.setOnClickListener(r0)
            X.C88564Kw.A00(r14)
            X.4U5 r0 = r5.A09
            r14.A01 = r0
            boolean r1 = r5.A05
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A02
            r14.A03 = r1
            r14.A02 = r0
            X.C88564Kw.A00(r14)
            return
        L_0x0de8:
            java.util.List r0 = r5.A04
            int r1 = r0.size()
            int r1 = r1 + r2
            goto L_0x0d92
        L_0x0df0:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unsupported view type "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r3)
            r1.<init>(r0)
            throw r1
        L_0x0dfc:
            boolean r0 = r14 instanceof X.AnonymousClass4ST
            if (r0 == 0) goto L_0x0e82
            X.4ST r14 = (X.AnonymousClass4ST) r14
            int r1 = X.AnonymousClass1Y3.ASj
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.3g8 r0 = (X.C73473g8) r0
            boolean r0 = r0.A0D()
            if (r0 == 0) goto L_0x0e19
            java.util.List r0 = r5.A03
            int r0 = r0.size()
            int r2 = r2 + r0
        L_0x0e19:
            java.util.List r0 = r5.A04
            int r15 = r15 - r2
            java.lang.Object r0 = r0.get(r15)
            com.facebook.messaging.media.folder.Folder r0 = (com.facebook.messaging.media.folder.Folder) r0
            r14.A00 = r0
            android.net.Uri r2 = r0.A02
            java.lang.String r1 = r0.A03
            int r0 = r0.A00
            X.AnonymousClass4ST.A01(r14, r2, r1, r0)
            X.4U4 r0 = r5.A0A
            r14.A01 = r0
            boolean r1 = r5.A05
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A02
            r14.A03 = r1
            r14.A02 = r0
            X.AnonymousClass4ST.A00(r14)
            return
        L_0x0e3d:
            boolean r0 = r14 instanceof X.AnonymousClass4ST
            if (r0 == 0) goto L_0x0e82
            X.4ST r14 = (X.AnonymousClass4ST) r14
            java.util.List r2 = r5.A04
            r0 = 0
            r14.A00 = r0
            r0 = 0
            java.lang.Object r0 = r2.get(r0)
            com.facebook.messaging.media.folder.Folder r0 = (com.facebook.messaging.media.folder.Folder) r0
            android.net.Uri r4 = r0.A02
            android.content.res.Resources r1 = r14.A04
            r0 = 2131827156(0x7f1119d4, float:1.9287217E38)
            java.lang.String r3 = r1.getString(r0)
            java.util.Iterator r2 = r2.iterator()
            r1 = 0
        L_0x0e5f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0e6f
            java.lang.Object r0 = r2.next()
            com.facebook.messaging.media.folder.Folder r0 = (com.facebook.messaging.media.folder.Folder) r0
            int r0 = r0.A00
            int r1 = r1 + r0
            goto L_0x0e5f
        L_0x0e6f:
            X.AnonymousClass4ST.A01(r14, r4, r3, r1)
            X.4U4 r0 = r5.A0A
            r14.A01 = r0
            boolean r1 = r5.A05
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r5.A02
            r14.A03 = r1
            r14.A02 = r0
            X.AnonymousClass4ST.A00(r14)
            return
        L_0x0e82:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = X.AnonymousClass08S.A09(r6, r3)
            r1.<init>(r0)
            throw r1
        L_0x0e8c:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unexpected bind."
            r1.<init>(r0)
            throw r1
        L_0x0e94:
            android.view.View r3 = r14.A0H
            X.512 r3 = (X.AnonymousClass512) r3
            java.util.ArrayList r0 = r5.A07
            java.lang.Object r0 = r0.get(r15)
            X.D2A r0 = (X.D2A) r0
            com.facebook.messaging.montage.model.art.ArtItem r2 = r0.A01
            if (r2 == 0) goto L_0x0eb5
            X.D0t r1 = r2.A00
            r0 = 0
            if (r1 == 0) goto L_0x0eaa
            r0 = 1
        L_0x0eaa:
            if (r0 == 0) goto L_0x0eb5
            X.D2v r14 = (X.C26583D2v) r14
            r14.A00 = r2
            X.1MT r0 = r5.A02
            r3.A0T(r2, r0)
        L_0x0eb5:
            r3.setScale(r4)
            return
        L_0x0eb9:
            java.util.ArrayList r0 = r5.A07
            java.lang.Object r0 = r0.get(r15)
            X.D2A r0 = (X.D2A) r0
            com.facebook.messaging.montage.model.art.ArtItem r0 = r0.A01
            r4 = 0
            if (r0 == 0) goto L_0x0ec8
            java.lang.String r4 = r0.A08
        L_0x0ec8:
            java.util.Map r0 = r5.A08
            java.lang.Object r3 = r0.get(r4)
            com.facebook.messaging.montage.model.art.ArtItem r3 = (com.facebook.messaging.montage.model.art.ArtItem) r3
            if (r3 == 0) goto L_0x0ee6
            X.D3j r2 = r3.A01
            if (r2 == 0) goto L_0x0ee6
            android.net.Uri r0 = r2.A00
            if (r0 == 0) goto L_0x0ee6
            com.google.common.collect.ImmutableList r0 = r2.A01
            if (r0 == 0) goto L_0x0ee6
            X.C33361nS.A03(r5, r2, r14)
            X.D2v r14 = (X.C26583D2v) r14
            r14.A00 = r3
            return
        L_0x0ee6:
            X.D76 r1 = r5.A04
            X.D2G r0 = new X.D2G
            r0.<init>(r5, r15, r14)
            r1.C6Z(r0)
            X.D6e r0 = new X.D6e
            r0.<init>(r4)
            r1.CHB(r0)
            return
        L_0x0ef9:
            r2 = 1
            int r1 = X.AnonymousClass1Y3.Ag6
            X.0UN r0 = r6.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.25L r0 = (X.AnonymousClass25L) r0
            com.facebook.messaging.montage.model.art.EffectItem r5 = (com.facebook.messaging.montage.model.art.EffectItem) r5
            X.982 r0 = r0.A01(r5)
            r4.A0N(r0)
            r2 = 0
            int r1 = X.AnonymousClass1Y3.ACE
            X.0UN r0 = r6.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.D6p r0 = (X.C26674D6p) r0
            boolean r0 = r0.A02(r5)
            r4.A0O(r0)
            return
        L_0x0f20:
            r6.A00 = r15
            return
        L_0x0f23:
            X.4KJ r14 = (X.AnonymousClass4KJ) r14
            java.util.List r0 = r4.A0L
            int r0 = r0.size()
            java.lang.String r1 = java.lang.String.valueOf(r0)
            android.widget.TextView r0 = r14.A00
            r0.setText(r1)
            return
        L_0x0f35:
            r0 = r13
            X.1vj r0 = (X.C37471vj) r0
            X.4EO r14 = (X.AnonymousClass4EO) r14
            X.4Eb r14 = (X.C87254Eb) r14
            java.util.List r0 = r0.A02
            java.lang.Object r0 = r0.get(r15)
            X.0p2 r0 = (X.C12300p2) r0
            java.lang.Object r0 = r0.A01
            r14.BPx(r0)
            return
        L_0x0f4a:
            r3 = r13
            X.1vh r3 = (X.C37451vh) r3
            X.529 r14 = (X.AnonymousClass529) r14
            int r2 = r3.getItemViewType(r15)
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            int r0 = r0.intValue()
            if (r2 != r0) goto L_0x0f8d
            android.view.View r2 = r14.A0H
            com.facebook.widget.text.BetterButton r2 = (com.facebook.widget.text.BetterButton) r2
            com.google.common.collect.ImmutableList r0 = r3.A02
            java.lang.Object r0 = r0.get(r15)
            X.CFq r0 = (X.C24729CFq) r0
            java.lang.String r0 = r0.A01
            r2.setText(r0)
            X.CNK r1 = new X.CNK
            android.content.Context r0 = r2.getContext()
            android.content.res.Resources r0 = r0.getResources()
            r1.<init>(r0)
            r2.setTransformationMethod(r1)
            android.view.View r1 = r14.A0H
            X.Bp1 r0 = new X.Bp1
            r0.<init>(r3, r15)
            r1.setOnClickListener(r0)
            X.C37451vh.A01(r3, r2, r15)
            X.C37451vh.A00(r3, r2, r15)
            return
        L_0x0f8d:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = r0.intValue()
            if (r2 != r0) goto L_0x0fc2
            android.view.View r2 = r14.A0H
            com.facebook.widget.text.BetterButton r2 = (com.facebook.widget.text.BetterButton) r2
            com.google.common.collect.ImmutableList r0 = r3.A02
            java.lang.Object r0 = r0.get(r15)
            X.CFq r0 = (X.C24729CFq) r0
            java.lang.String r0 = r0.A01
            r2.setText(r0)
            X.CNK r1 = new X.CNK
            android.content.Context r0 = r2.getContext()
            android.content.res.Resources r0 = r0.getResources()
            r1.<init>(r0)
            r2.setTransformationMethod(r1)
            android.view.View$OnClickListener r0 = r3.A00
            r2.setOnClickListener(r0)
            X.C37451vh.A01(r3, r2, r15)
            X.C37451vh.A00(r3, r2, r15)
            return
        L_0x0fc2:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unrecognized row type "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r2)
            r1.<init>(r0)
            throw r1
        L_0x0fce:
            X.Bor r14 = (X.C23882Bor) r14
            android.content.Context r0 = r7.A00
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131834267(0x7f11359b, float:1.930164E38)
            java.lang.String r1 = r1.getString(r0)
            com.facebook.resources.ui.FbTextView r0 = r14.A00
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A00
            r3 = 0
            r0.setVisibility(r3)
            com.facebook.resources.ui.FbTextView r2 = r14.A00
            android.content.Context r0 = r7.A00
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2132148238(0x7f16000e, float:1.9938448E38)
            int r0 = r1.getDimensionPixelOffset(r0)
            r2.setPadding(r3, r0, r3, r3)
            return
        L_0x0ffb:
            X.Bot r14 = (X.C23884Bot) r14
            android.widget.ImageView r0 = r14.A02
            r4 = 0
            r0.setVisibility(r4)
            java.lang.String r1 = r3.A08
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x1015
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r0 = r14.A05
            r0.setVisibility(r4)
        L_0x1015:
            com.facebook.resources.ui.FbTextView r2 = r14.A06
            android.content.Context r0 = r7.A00
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131834268(0x7f11359c, float:1.9301642E38)
            java.lang.String r0 = r1.getString(r0)
            r2.setText(r0)
            com.facebook.resources.ui.FbTextView r0 = r14.A06
            r0.setVisibility(r4)
            return
        L_0x102d:
            X.4fL r14 = (X.C94854fL) r14
            X.4fK r3 = r14.A00
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r4 = r0.get(r15)
            com.facebook.fbpay.api.FBPayTransactionDetailsHeaderViewModel r4 = (com.facebook.fbpay.api.FBPayTransactionDetailsHeaderViewModel) r4
            java.lang.String r1 = r4.A01
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            java.lang.String r2 = ""
            if (r0 != 0) goto L_0x107a
            com.facebook.widget.text.BetterTextView r0 = r3.A04
            r0.setText(r1)
        L_0x1048:
            java.lang.String r1 = r4.A02
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x1074
            com.facebook.resources.ui.FbTextView r0 = r3.A03
            r0.setText(r1)
        L_0x1055:
            java.lang.String r1 = r4.A00
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x106e
            com.facebook.resources.ui.FbTextView r0 = r3.A01
            r0.setText(r1)
        L_0x1062:
            java.lang.String r1 = r4.A03
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x1080
            r3.A0C(r1)
            return
        L_0x106e:
            com.facebook.resources.ui.FbTextView r0 = r3.A01
            r0.setText(r2)
            goto L_0x1062
        L_0x1074:
            com.facebook.resources.ui.FbTextView r0 = r3.A03
            r0.setText(r2)
            goto L_0x1055
        L_0x107a:
            com.facebook.widget.text.BetterTextView r0 = r3.A04
            r0.setText(r2)
            goto L_0x1048
        L_0x1080:
            r3.A0C(r2)
            return
        L_0x1084:
            X.4fV r14 = (X.C94954fV) r14
            X.4fU r4 = r14.A00
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r3 = r0.get(r15)
            com.facebook.fbpay.api.FBPayTransactionDetailsProfileViewModel r3 = (com.facebook.fbpay.api.FBPayTransactionDetailsProfileViewModel) r3
            java.lang.String r2 = r3.A01
            java.lang.String r0 = r3.A02
            r1 = 0
            r4.A0D(r2, r0, r1)
            java.lang.String r0 = r3.A00
            if (r0 == 0) goto L_0x10a0
            android.net.Uri r1 = android.net.Uri.parse(r0)
        L_0x10a0:
            r4.A0C(r1)
            return
        L_0x10a4:
            X.4fJ r14 = (X.C94834fJ) r14
            X.4fI r4 = r14.A00
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r3 = r0.get(r15)
            com.facebook.fbpay.api.FbPayTransactionDetailsTitleAndDualColumnMultilineSubtitleViewModel r3 = (com.facebook.fbpay.api.FbPayTransactionDetailsTitleAndDualColumnMultilineSubtitleViewModel) r3
            java.lang.String r1 = r3.A02
            com.facebook.resources.ui.FbTextView r0 = r4.A03
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r1 = r4.A03
            r0 = 0
            r1.setVisibility(r0)
            com.google.common.collect.ImmutableList r2 = r3.A00
            com.google.common.collect.ImmutableList r1 = r3.A01
            X.4EK r0 = r4.A02
            if (r2 == 0) goto L_0x10c7
            r0.A01 = r2
        L_0x10c7:
            if (r1 == 0) goto L_0x10cb
            r0.A00 = r1
        L_0x10cb:
            r0.A04()
            return
        L_0x10cf:
            X.4fS r14 = (X.C94924fS) r14
            X.4fT r4 = r14.A00
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r2 = r0.get(r15)
            com.facebook.fbpay.api.FbPayTransactionDetailsTitleAndMultilineSubtitleViewModel r2 = (com.facebook.fbpay.api.FbPayTransactionDetailsTitleAndMultilineSubtitleViewModel) r2
            java.lang.String r1 = r2.A01
            com.facebook.resources.ui.FbTextView r0 = r4.A02
            r0.setText(r1)
            com.google.common.collect.ImmutableList r1 = r2.A00
            if (r1 == 0) goto L_0x111a
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x111a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            X.1Xv r2 = r1.iterator()
        L_0x10f5:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x1110
            java.lang.Object r1 = r2.next()
            java.lang.String r1 = (java.lang.String) r1
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x110c
            java.lang.String r0 = "\n"
            r3.append(r0)
        L_0x110c:
            r3.append(r1)
            goto L_0x10f5
        L_0x1110:
            com.facebook.resources.ui.FbTextView r1 = r4.A01
            java.lang.String r0 = r3.toString()
            r1.setText(r0)
            return
        L_0x111a:
            com.facebook.resources.ui.FbTextView r1 = r4.A01
            java.lang.String r0 = ""
            r1.setText(r0)
            return
        L_0x1122:
            X.4fQ r14 = (X.C94904fQ) r14
            X.4fR r3 = r14.A00
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r2 = r0.get(r15)
            com.facebook.fbpay.api.FBPayTransactionDetailsItemsViewModel r2 = (com.facebook.fbpay.api.FBPayTransactionDetailsItemsViewModel) r2
            java.lang.String r1 = r2.A01
            com.facebook.resources.ui.FbTextView r0 = r3.A03
            r0.setText(r1)
            com.facebook.resources.ui.FbTextView r1 = r3.A03
            r0 = 0
            r1.setVisibility(r0)
            com.google.common.collect.ImmutableList r1 = r2.A00
            X.CL5 r0 = r3.A02
            if (r1 == 0) goto L_0x1143
            r0.A00 = r1
        L_0x1143:
            r0.A04()
            return
        L_0x1147:
            X.4fO r14 = (X.C94884fO) r14
            X.4fP r3 = r14.A00
            com.google.common.collect.ImmutableList r0 = r6.A02
            java.lang.Object r0 = r0.get(r15)
            com.facebook.fbpay.api.FbPayTransactionDetailsDisclosureViewModel r0 = (com.facebook.fbpay.api.FbPayTransactionDetailsDisclosureViewModel) r0
            X.4sE r2 = r0.A00     // Catch:{ 1wM -> 0x1160 }
            X.68A r1 = new X.68A     // Catch:{ 1wM -> 0x1160 }
            r1.<init>(r3)     // Catch:{ 1wM -> 0x1160 }
            com.facebook.widget.text.textwithentitiesview.TextWithEntitiesView r0 = r3.A00     // Catch:{ 1wM -> 0x1160 }
            r0.A02(r2, r1)     // Catch:{ 1wM -> 0x1160 }
            return
        L_0x1160:
            r4 = move-exception
            r2 = 0
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r6.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r3 = (X.AnonymousClass09P) r3
            java.lang.String r2 = "HubTransactionDetailsAdapter"
            r0 = 61
            java.lang.String r1 = X.C22298Ase.$const$string(r0)
            java.lang.String r0 = r4.getMessage()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r3.CGY(r2, r0)
            return
        L_0x1180:
            r0 = r13
            X.1Qv r0 = (X.C23781Qv) r0
            X.4EM r14 = (X.AnonymousClass4EM) r14
            X.C23781Qv.A00(r0)
            com.google.common.collect.ImmutableList r0 = r0.A01
            java.lang.Object r2 = r0.get(r15)
            X.1yb r2 = (X.C39161yb) r2
            android.widget.ImageView r1 = r14.A00
            android.graphics.drawable.Drawable r0 = r2.A01
            r1.setImageDrawable(r0)
            android.widget.TextView r1 = r14.A01
            java.lang.CharSequence r0 = r2.A02
            r1.setText(r0)
            android.view.View r0 = r14.A0H
            r0.setTag(r2)
            return
        L_0x11a4:
            r0 = r13
            X.1Qe r0 = (X.C23611Qe) r0
            X.4ER r14 = (X.AnonymousClass4ER) r14
            X.DbG r2 = r0.A06
            android.view.View r1 = r14.A0H
            androidx.recyclerview.widget.RecyclerView r0 = r0.A00
            r2.getView(r15, r1, r0)
            return
        L_0x11b3:
            r1 = r13
            X.1Qn r1 = (X.C23701Qn) r1
            X.DzP r14 = (X.C28697DzP) r14
            java.util.List r0 = java.util.Collections.EMPTY_LIST
            X.C23701Qn.A00(r1, r14, r15, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20831Dz.BPy(X.1o8, int):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v22, resolved type: X.4EP} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v23, resolved type: X.D2X} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v30, resolved type: X.4EP} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v31, resolved type: X.4EP} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v32, resolved type: X.4EP} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x049c, code lost:
        if (r14 == 4) goto L_0x049e;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C33781o8 BV1(android.view.ViewGroup r13, int r14) {
        /*
            r12 = this;
            boolean r0 = r12 instanceof X.C23701Qn
            if (r0 != 0) goto L_0x0810
            boolean r0 = r12 instanceof X.C24151Sm
            if (r0 != 0) goto L_0x07e0
            boolean r0 = r12 instanceof X.C23611Qe
            if (r0 != 0) goto L_0x07d1
            boolean r0 = r12 instanceof X.C23781Qv
            if (r0 != 0) goto L_0x07ab
            boolean r0 = r12 instanceof X.C23631Qg
            if (r0 != 0) goto L_0x0766
            boolean r0 = r12 instanceof X.AnonymousClass1IC
            if (r0 != 0) goto L_0x06e9
            boolean r0 = r12 instanceof X.C37441vg
            if (r0 != 0) goto L_0x0697
            boolean r0 = r12 instanceof X.C37451vh
            if (r0 != 0) goto L_0x05ee
            boolean r0 = r12 instanceof X.C37461vi
            if (r0 != 0) goto L_0x059b
            boolean r0 = r12 instanceof X.C37471vj
            if (r0 != 0) goto L_0x056f
            boolean r0 = r12 instanceof X.C23641Qh
            if (r0 != 0) goto L_0x04e4
            boolean r0 = r12 instanceof X.C21701Ij
            if (r0 != 0) goto L_0x0490
            boolean r0 = r12 instanceof X.AnonymousClass1TV
            if (r0 != 0) goto L_0x04c2
            boolean r0 = r12 instanceof X.C21881Jb
            if (r0 != 0) goto L_0x03dc
            boolean r0 = r12 instanceof X.C33361nS
            if (r0 != 0) goto L_0x02db
            boolean r0 = r12 instanceof X.C20751Dr
            if (r0 != 0) goto L_0x027e
            boolean r0 = r12 instanceof X.C23651Qi
            if (r0 != 0) goto L_0x0243
            boolean r0 = r12 instanceof X.C23661Qj
            if (r0 != 0) goto L_0x01c0
            boolean r0 = r12 instanceof X.C188815s
            if (r0 != 0) goto L_0x01b1
            boolean r0 = r12 instanceof X.C23671Qk
            if (r0 != 0) goto L_0x011f
            boolean r0 = r12 instanceof X.C23681Ql
            if (r0 != 0) goto L_0x00d5
            boolean r0 = r12 instanceof X.C23791Qw
            if (r0 != 0) goto L_0x00b9
            boolean r0 = r12 instanceof X.C23801Qx
            if (r0 != 0) goto L_0x00a9
            r2 = r12
            X.1A0 r2 = (X.AnonymousClass1A0) r2
            X.1Ri r0 = r2.A00
            X.1A2 r0 = r0.A0Z
            android.util.SparseArray r0 = r0.A03
            java.lang.Object r1 = r0.get(r14)
            X.1vk r1 = (X.C37481vk) r1
            if (r1 == 0) goto L_0x007e
            X.1Ri r0 = r2.A00
            X.0p4 r0 = r0.A0U
            android.content.Context r0 = r0.A09
            android.view.View r2 = r1.AWD(r0, r13)
            X.1Qz r1 = new X.1Qz
            r0 = 0
            r1.<init>(r2, r0)
            return r1
        L_0x007e:
            X.1Ri r1 = r2.A00
            X.17r r0 = r1.A0b
            if (r0 != 0) goto L_0x0093
            com.facebook.litho.LithoView r4 = new com.facebook.litho.LithoView
            X.0p4 r1 = r1.A0U
            r0 = 0
            r4.<init>(r1, r0)
        L_0x008c:
            X.1Qz r1 = new X.1Qz
            r0 = 1
            r1.<init>(r4, r0)
            return r1
        L_0x0093:
            X.1HQ r4 = new X.1HQ
            com.facebook.orca.threadlist.ThreadListFragment r0 = r0.A00
            X.1GA r3 = r0.A0Q
            int r2 = X.AnonymousClass1Y3.AvI
            X.0UN r1 = r0.A0O
            r0 = 13
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1mK r0 = (X.C32781mK) r0
            r4.<init>(r3, r0)
            goto L_0x008c
        L_0x00a9:
            r0 = r12
            X.1Qx r0 = (X.C23801Qx) r0
            X.4EQ r2 = new X.4EQ
            com.facebook.litho.LithoView r1 = new com.facebook.litho.LithoView
            android.content.Context r0 = r0.A00
            r1.<init>(r0)
            r2.<init>(r1)
            return r2
        L_0x00b9:
            r3 = r12
            X.1Qw r3 = (X.C23791Qw) r3
            android.content.Context r0 = r3.A02
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            r1 = 2132412060(0x7f1a069c, float:2.0473543E38)
            r0 = 0
            android.view.View r2 = r2.inflate(r1, r13, r0)
            X.4EN r1 = new X.4EN
            r1.<init>(r2)
            android.view.View$OnClickListener r0 = r3.A03
            r2.setOnClickListener(r0)
            return r1
        L_0x00d5:
            r8 = r12
            X.1Ql r8 = (X.C23681Ql) r8
            r2 = 0
            if (r14 == 0) goto L_0x0109
            r0 = 2
            android.view.LayoutInflater r1 = r8.A03
            if (r14 == r0) goto L_0x00fc
            r0 = 2132410396(0x7f1a001c, float:2.0470168E38)
            android.view.View r2 = r1.inflate(r0, r13, r2)
            X.7G3 r1 = new X.7G3
            android.content.Context r3 = r8.A02
            X.2oo r4 = r8.A07
            com.facebook.prefs.shared.FbSharedPreferences r5 = r8.A08
            X.1Y6 r6 = r8.A06
            X.2Wx r7 = r8.A05
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x00f6:
            android.view.View$OnClickListener r0 = r8.A04
            r2.setOnClickListener(r0)
            return r1
        L_0x00fc:
            r0 = 2132410411(0x7f1a002b, float:2.0470198E38)
            android.view.View r2 = r1.inflate(r0, r13, r2)
            X.7G7 r1 = new X.7G7
            r1.<init>(r2)
            goto L_0x00f6
        L_0x0109:
            android.view.LayoutInflater r1 = r8.A03
            r0 = 2132410396(0x7f1a001c, float:2.0470168E38)
            android.view.View r2 = r1.inflate(r0, r13, r2)
            X.7G0 r1 = new X.7G0
            android.content.Context r3 = r8.A02
            X.2Wx r5 = r8.A05
            X.1Y6 r6 = r8.A06
            r4 = r8
            r1.<init>(r2, r3, r4, r5, r6)
            goto L_0x00f6
        L_0x011f:
            r0 = r12
            X.1Qk r0 = (X.C23671Qk) r0
            X.3zm r3 = r0.A01
            r2 = 0
            switch(r14) {
                case 0: goto L_0x019c;
                case 1: goto L_0x0187;
                case 2: goto L_0x019c;
                case 3: goto L_0x019c;
                case 4: goto L_0x019c;
                case 5: goto L_0x019c;
                case 6: goto L_0x0172;
                case 7: goto L_0x0153;
                case 8: goto L_0x0134;
                default: goto L_0x0128;
            }
        L_0x0128:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            r0 = 25
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r1.<init>(r0)
            throw r1
        L_0x0134:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132411855(0x7f1a05cf, float:2.0473127E38)
            android.view.View r2 = r1.inflate(r0, r13, r2)
            int r1 = X.AnonymousClass1Y3.Aer
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.3yB r0 = (X.C83773yB) r0
            X.5jd r1 = new X.5jd
            r1.<init>(r0, r2)
            return r1
        L_0x0153:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132411849(0x7f1a05c9, float:2.0473115E38)
            android.view.View r2 = r1.inflate(r0, r13, r2)
            int r1 = X.AnonymousClass1Y3.A4p
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.3yA r0 = (X.C83763yA) r0
            X.6Kx r1 = new X.6Kx
            r1.<init>(r0, r2)
            return r1
        L_0x0172:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132411851(0x7f1a05cb, float:2.047312E38)
            android.view.View r0 = r1.inflate(r0, r13, r2)
            X.8nb r1 = new X.8nb
            r1.<init>(r0)
            return r1
        L_0x0187:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132411848(0x7f1a05c8, float:2.0473113E38)
            android.view.View r0 = r1.inflate(r0, r13, r2)
            X.8nk r1 = new X.8nk
            r1.<init>(r0)
            return r1
        L_0x019c:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132411854(0x7f1a05ce, float:2.0473125E38)
            android.view.View r0 = r1.inflate(r0, r13, r2)
            X.8nV r1 = new X.8nV
            r1.<init>(r0)
            return r1
        L_0x01b1:
            X.853 r1 = new X.853
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            X.4EU r0 = new X.4EU
            r0.<init>(r1)
            return r0
        L_0x01c0:
            r3 = r12
            X.1Qj r3 = (X.C23661Qj) r3
            if (r14 == 0) goto L_0x021b
            r0 = 1
            if (r14 == r0) goto L_0x020a
            r0 = 2
            if (r14 != r0) goto L_0x023b
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            r1 = 2132411298(0x7f1a03a2, float:2.0471997E38)
            r0 = 0
            android.view.View r1 = r2.inflate(r1, r13, r0)
            com.facebook.fbui.widget.glyph.GlyphButton r1 = (com.facebook.fbui.widget.glyph.GlyphButton) r1
            X.3vs r0 = new X.3vs
            r0.<init>()
            r1.setOnClickListener(r0)
            X.4EP r4 = new X.4EP
            r4.<init>(r1)
        L_0x01ea:
            androidx.recyclerview.widget.RecyclerView r13 = (androidx.recyclerview.widget.RecyclerView) r13
            X.19T r2 = r13.A0L
            android.view.View r0 = r4.A0H
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            X.1R7 r1 = (X.AnonymousClass1R7) r1
            if (r1 != 0) goto L_0x0201
            X.1R7 r1 = r2.A0w()
            android.view.View r0 = r4.A0H
            r0.setLayoutParams(r1)
        L_0x0201:
            int r0 = r3.A02
            r1.width = r0
            int r0 = r3.A01
            r1.height = r0
            return r4
        L_0x020a:
            X.2Qm r0 = r3.A08
            X.D2X r4 = r0.BV2(r13)
            android.view.View r1 = r4.A0H
            X.DBy r0 = new X.DBy
            r0.<init>(r3, r4)
            r1.setOnClickListener(r0)
            goto L_0x01ea
        L_0x021b:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            r1 = 2132411297(0x7f1a03a1, float:2.0471995E38)
            r0 = 0
            android.view.View r1 = r2.inflate(r1, r13, r0)
            com.facebook.fbui.widget.glyph.GlyphButton r1 = (com.facebook.fbui.widget.glyph.GlyphButton) r1
            X.DC0 r0 = new X.DC0
            r0.<init>(r3)
            r1.setOnClickListener(r0)
            X.4EP r4 = new X.4EP
            r4.<init>(r1)
            goto L_0x01ea
        L_0x023b:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "EmojilikePickerView onCreateViewHolder with unknown view type"
            r1.<init>(r0)
            throw r1
        L_0x0243:
            r5 = r12
            X.1Qi r5 = (X.C23651Qi) r5
            r3 = 0
            r2 = 3
            android.content.Context r0 = r5.A06
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            if (r14 != r2) goto L_0x025f
            r0 = 2132411133(0x7f1a02fd, float:2.0471663E38)
            android.view.View r2 = r1.inflate(r0, r13, r3)
            X.4Kw r0 = new X.4Kw
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r5.A02
            r0.<init>(r2, r1)
            return r0
        L_0x025f:
            r0 = 2132411134(0x7f1a02fe, float:2.0471665E38)
            android.view.View r4 = r1.inflate(r0, r13, r3)
            android.content.res.Resources r1 = r5.A07
            r0 = 2132148225(0x7f160001, float:1.9938422E38)
            int r0 = r1.getDimensionPixelSize(r0)
            X.3xR r3 = r5.A08
            X.36w r2 = new X.36w
            r2.<init>(r0, r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r5.A02
            X.4ST r0 = new X.4ST
            r0.<init>(r3, r4, r2, r1)
            return r0
        L_0x027e:
            r1 = r12
            X.1Dr r1 = (X.C20751Dr) r1
            X.28p[] r0 = X.C421828p.values()
            int r0 = r0.length
            int r0 = r0 + 1
            r3 = 0
            android.view.LayoutInflater r2 = r1.A05
            if (r14 != r0) goto L_0x029e
            r0 = 2132411308(0x7f1a03ac, float:2.0472018E38)
            android.view.View r3 = r2.inflate(r0, r13, r3)
            X.3xT r2 = r1.A07
            X.Cvb r0 = r1.A09
            X.CtB r4 = new X.CtB
            r4.<init>(r2, r3, r0)
            return r4
        L_0x029e:
            r0 = 2132411138(0x7f1a0302, float:2.0471673E38)
            android.view.View r8 = r2.inflate(r0, r13, r3)
            X.3xS r2 = r1.A06
            X.Cv6 r0 = r1.A01
            boolean r9 = r0.A05
            boolean r10 = r0.A06
            com.facebook.messaging.model.threadkey.ThreadKey r11 = r0.A02
            X.Cur r4 = new X.Cur
            X.BO8 r5 = new X.BO8
            r5.<init>(r2)
            X.BO9 r6 = new X.BO9
            r6.<init>(r2)
            X.BO7 r7 = new X.BO7
            r7.<init>(r2)
            X.C64623Cp.A00(r2)
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
            X.Cw3 r0 = r1.A08
            r4.A00 = r0
            X.Cv6 r0 = r1.A01
            boolean r2 = r0.A03
            X.Cvc r0 = r4.A05
            android.widget.ImageView r1 = r0.A00
            r0 = 8
            if (r2 == 0) goto L_0x02d7
            r0 = 0
        L_0x02d7:
            r1.setVisibility(r0)
            return r4
        L_0x02db:
            r4 = r12
            X.1nS r4 = (X.C33361nS) r4
            android.content.res.Resources r1 = r4.A01
            r0 = 2132148247(0x7f160017, float:1.9938467E38)
            int r3 = r1.getDimensionPixelOffset(r0)
            android.content.res.Resources r1 = r4.A01
            r0 = 2132148235(0x7f16000b, float:1.9938442E38)
            int r2 = r1.getDimensionPixelOffset(r0)
            int r1 = r13.getWidth()
            int r0 = r3 * 3
            int r1 = r1 - r0
            int r0 = r2 << 1
            int r1 = r1 - r0
            int r7 = r1 >> 2
            r1 = 1061158912(0x3f400000, float:0.75)
            r6 = -1
            switch(r14) {
                case 0: goto L_0x0304;
                case 1: goto L_0x0331;
                case 2: goto L_0x0366;
                case 3: goto L_0x0302;
                case 4: goto L_0x038e;
                case 5: goto L_0x03b5;
                default: goto L_0x0302;
            }
        L_0x0302:
            r3 = 0
            return r3
        L_0x0304:
            X.2Qm r0 = r4.A03
            X.D2X r3 = r0.BV2(r13)
            android.view.View r1 = r3.A0H
            X.1R7 r0 = new X.1R7
            r0.<init>(r6, r7)
            r1.setLayoutParams(r0)
            android.view.View r0 = r3.A0H
            r1 = 1068708659(0x3fb33333, float:1.4)
            r0.setScaleX(r1)
            android.view.View r0 = r3.A0H
            r0.setScaleY(r1)
            android.view.View r2 = r3.A0H
            X.D22 r1 = new X.D22
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r3)
            r1.<init>(r4, r0)
            r2.setOnClickListener(r1)
            return r3
        L_0x0331:
            com.facebook.drawee.fbpipeline.FbDraweeView r5 = new com.facebook.drawee.fbpipeline.FbDraweeView
            android.content.Context r0 = r13.getContext()
            r5.<init>(r0)
            android.content.res.Resources r1 = r4.A01
            r0 = 2131828490(0x7f111f0a, float:1.9289922E38)
            java.lang.String r2 = r1.getString(r0)
            X.1R7 r0 = new X.1R7
            r0.<init>(r6, r7)
            r5.setLayoutParams(r0)
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.CENTER_INSIDE
            r5.setScaleType(r0)
            X.D2v r3 = new X.D2v
            r3.<init>(r5)
            X.D2F r1 = new X.D2F
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r3)
            r1.<init>(r4, r0)
            r5.setOnClickListener(r1)
            r5.setContentDescription(r2)
            return r3
        L_0x0366:
            com.facebook.messaging.montage.composer.art.ArtItemView r2 = new com.facebook.messaging.montage.composer.art.ArtItemView
            android.content.Context r0 = r13.getContext()
            r2.<init>(r0)
            X.1R7 r0 = new X.1R7
            r0.<init>(r6, r7)
            r2.setLayoutParams(r0)
            r0 = 0
            r2.setBackgroundResource(r0)
            X.D2v r3 = new X.D2v
            r3.<init>(r2)
            X.D2F r1 = new X.D2F
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r3)
            r1.<init>(r4, r0)
            r2.setOnClickListener(r1)
            return r3
        L_0x038e:
            X.512 r2 = new X.512
            android.content.Context r0 = r13.getContext()
            r2.<init>(r0)
            r2.setScale(r1)
            X.1R7 r0 = new X.1R7
            r0.<init>(r6, r7)
            r2.setLayoutParams(r0)
            X.D2v r3 = new X.D2v
            r3.<init>(r2)
            X.D2F r1 = new X.D2F
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r3)
            r1.<init>(r4, r0)
            r2.setOnClickListener(r1)
            return r3
        L_0x03b5:
            X.513 r2 = new X.513
            android.content.Context r0 = r13.getContext()
            r2.<init>(r0)
            r2.setScale(r1)
            X.1R7 r0 = new X.1R7
            r0.<init>(r6, r7)
            r2.setLayoutParams(r0)
            X.D2v r3 = new X.D2v
            r3.<init>(r2)
            X.D2F r1 = new X.D2F
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r3)
            r1.<init>(r4, r0)
            r2.setOnClickListener(r1)
            return r3
        L_0x03dc:
            r2 = r12
            X.1Jb r2 = (X.C21881Jb) r2
            android.content.res.Resources r1 = r2.A02
            r0 = 2132148259(0x7f160023, float:1.993849E38)
            int r3 = r1.getDimensionPixelSize(r0)
            android.content.res.Resources r1 = r2.A02
            r0 = 2132148243(0x7f160013, float:1.9938458E38)
            int r1 = r1.getDimensionPixelOffset(r0)
            int r4 = r13.getWidth()
            r6 = 2
            int r0 = r3 << 1
            int r4 = r4 - r0
            int r0 = r1 << 1
            int r4 = r4 - r0
            r1 = 3
            int r4 = r4 / r1
            r3 = -1
            if (r14 == 0) goto L_0x0471
            r0 = 1
            if (r14 == r0) goto L_0x044e
            r5 = 1065353216(0x3f800000, float:1.0)
            if (r14 == r6) goto L_0x042c
            if (r14 != r1) goto L_0x04d8
            X.513 r1 = new X.513
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r1.setScale(r5)
            X.1R7 r0 = new X.1R7
            r0.<init>(r3, r4)
            r1.setLayoutParams(r0)
            X.4EO r3 = new X.4EO
            r3.<init>(r1)
            X.D23 r0 = new X.D23
            r0.<init>(r2, r3)
            r1.setOnClickListener(r0)
            return r3
        L_0x042c:
            X.512 r1 = new X.512
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r1.setScale(r5)
            X.1R7 r0 = new X.1R7
            r0.<init>(r3, r4)
            r1.setLayoutParams(r0)
            X.4EO r3 = new X.4EO
            r3.<init>(r1)
            X.D23 r0 = new X.D23
            r0.<init>(r2, r3)
            r1.setOnClickListener(r0)
            return r3
        L_0x044e:
            com.facebook.messaging.montage.composer.art.ArtItemView r1 = new com.facebook.messaging.montage.composer.art.ArtItemView
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            X.1R7 r0 = new X.1R7
            r0.<init>(r3, r4)
            r1.setLayoutParams(r0)
            X.4EO r3 = new X.4EO
            r3.<init>(r1)
            X.D23 r0 = new X.D23
            r0.<init>(r2, r3)
            r1.setOnClickListener(r0)
            r0 = 0
            r1.setBackgroundResource(r0)
            return r3
        L_0x0471:
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = new com.facebook.drawee.fbpipeline.FbDraweeView
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            X.1R7 r0 = new X.1R7
            r0.<init>(r3, r4)
            r1.setLayoutParams(r0)
            X.4EO r3 = new X.4EO
            r3.<init>(r1)
            X.D23 r0 = new X.D23
            r0.<init>(r2, r3)
            r1.setOnClickListener(r0)
            return r3
        L_0x0490:
            if (r14 == 0) goto L_0x04b6
            r0 = 1
            if (r14 == r0) goto L_0x049e
            r0 = 2
            if (r14 == r0) goto L_0x049e
            r0 = 3
            if (r14 == r0) goto L_0x04aa
            r0 = 4
            if (r14 != r0) goto L_0x04d8
        L_0x049e:
            X.D6x r1 = new X.D6x
            android.content.Context r3 = r13.getContext()
            r2 = 0
            r0 = 0
            r1.<init>(r3, r2, r0)
            goto L_0x04d2
        L_0x04aa:
            X.425 r1 = new X.425
            android.content.Context r3 = r13.getContext()
            r2 = 0
            r0 = 0
            r1.<init>(r3, r2, r0)
            goto L_0x04d2
        L_0x04b6:
            X.41N r1 = new X.41N
            android.content.Context r3 = r13.getContext()
            r2 = 0
            r0 = 0
            r1.<init>(r3, r2, r0)
            goto L_0x04d2
        L_0x04c2:
            if (r14 == 0) goto L_0x04c7
            r0 = 1
            if (r14 != r0) goto L_0x04d8
        L_0x04c7:
            X.D75 r1 = new X.D75
            android.content.Context r3 = r13.getContext()
            r2 = 0
            r0 = 0
            r1.<init>(r3, r2, r0)
        L_0x04d2:
            X.4EO r0 = new X.4EO
            r0.<init>(r1)
            return r0
        L_0x04d8:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unknown viewtype: "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r14)
            r1.<init>(r0)
            throw r1
        L_0x04e4:
            r3 = r12
            X.1Qh r3 = (X.C23641Qh) r3
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            r1 = 0
            if (r14 == 0) goto L_0x054f
            r0 = 1
            if (r14 == r0) goto L_0x0537
            r0 = 2
            if (r14 == r0) goto L_0x0528
            r0 = 3
            if (r14 == r0) goto L_0x0519
            r0 = 4
            if (r14 != r0) goto L_0x050d
            X.6Ls r4 = new X.6Ls
            r0 = 2132411361(0x7f1a03e1, float:2.0472125E38)
            android.view.View r1 = r2.inflate(r0, r13, r1)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r3.A04
            r4.<init>(r3, r1, r0)
            return r4
        L_0x050d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unknown view type: "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r14)
            r1.<init>(r0)
            throw r1
        L_0x0519:
            X.4EX r4 = new X.4EX
            r0 = 2132411359(0x7f1a03df, float:2.0472121E38)
            android.view.View r1 = r2.inflate(r0, r13, r1)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r3.A04
            r4.<init>(r3, r1, r0)
            return r4
        L_0x0528:
            X.4KK r4 = new X.4KK
            r0 = 2132411420(0x7f1a041c, float:2.0472245E38)
            android.view.View r1 = r2.inflate(r0, r13, r1)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r3.A04
            r4.<init>(r1, r0)
            return r4
        L_0x0537:
            X.4KS r4 = new X.4KS
            android.content.Context r0 = r3.A0I
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            r1 = 2132411358(0x7f1a03de, float:2.047212E38)
            r0 = 0
            android.view.View r1 = r2.inflate(r1, r13, r0)
            com.facebook.messaging.montage.viewer.seensheet.MontageSeenByListItemView r1 = (com.facebook.messaging.montage.viewer.seensheet.MontageSeenByListItemView) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r3.A04
            r4.<init>(r3, r1, r0)
            return r4
        L_0x054f:
            boolean r0 = r3.A0E
            if (r0 == 0) goto L_0x0560
            X.4KN r4 = new X.4KN
            r0 = 2132411363(0x7f1a03e3, float:2.047213E38)
            android.view.View r0 = r2.inflate(r0, r13, r1)
            r4.<init>(r3, r0)
            return r4
        L_0x0560:
            X.4KJ r4 = new X.4KJ
            r0 = 2132411357(0x7f1a03dd, float:2.0472117E38)
            android.view.View r1 = r2.inflate(r0, r13, r1)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r3.A04
            r4.<init>(r3, r1, r0)
            return r4
        L_0x056f:
            r4 = r12
            X.1vj r4 = (X.C37471vj) r4
            X.4Jb[] r0 = r4.A04
            r3 = r0[r14]
            android.content.Context r0 = r4.A05
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            int r1 = r3.layoutResId
            r0 = 0
            android.view.View r1 = r2.inflate(r1, r13, r0)
            X.4Jb r0 = X.C88204Jb.AVAILABILITY_DATE_TITLE
            if (r3 != r0) goto L_0x058f
            X.4Ea r0 = new X.4Ea
            com.facebook.widget.text.BetterTextView r1 = (com.facebook.widget.text.BetterTextView) r1
            r0.<init>(r1)
            return r0
        L_0x058f:
            X.4Jb r0 = X.C88204Jb.AVAILABILITY_TIME_SLOT
            if (r3 != r0) goto L_0x0599
            X.4ic r0 = new X.4ic
            r0.<init>(r4, r1)
            return r0
        L_0x0599:
            r0 = 0
            return r0
        L_0x059b:
            r4 = r12
            X.1vi r4 = (X.C37461vi) r4
            r2 = 0
            if (r14 != 0) goto L_0x05bc
            X.2mv r0 = r4.A03
            android.content.Context r0 = r0.A1j()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132412218(0x7f1a073a, float:2.0473863E38)
            android.view.View r2 = r1.inflate(r0, r13, r2)
            com.facebook.fig.deprecated.button.FigButton r2 = (com.facebook.fig.deprecated.button.FigButton) r2
            X.5wQ r1 = new X.5wQ
            X.2mv r0 = r4.A03
            r1.<init>(r0, r2)
            return r1
        L_0x05bc:
            r0 = 1
            if (r14 != r0) goto L_0x0794
            X.2mv r0 = r4.A03
            android.content.Context r0 = r0.A1j()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132412220(0x7f1a073c, float:2.0473867E38)
            android.view.View r3 = r1.inflate(r0, r13, r2)
            com.facebook.widget.text.BetterTextView r3 = (com.facebook.widget.text.BetterTextView) r3
            X.2mv r2 = r4.A03
            r1 = 2131823168(0x7f110a40, float:1.9279128E38)
            java.util.TimeZone r0 = r2.A05
            java.lang.String r0 = r0.getDisplayName()
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r0 = r2.A1B(r1, r0)
            r3.setText(r0)
            X.4EJ r1 = new X.4EJ
            r1.<init>(r3)
            return r1
        L_0x05ee:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            int r0 = r0.intValue()
            if (r14 == r0) goto L_0x060a
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = r0.intValue()
            if (r14 == r0) goto L_0x060a
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unrecognized row type "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r14)
            r1.<init>(r0)
            throw r1
        L_0x060a:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132411899(0x7f1a05fb, float:2.0473216E38)
            r4 = 0
            android.view.View r3 = r1.inflate(r0, r13, r4)
            com.facebook.widget.text.BetterButton r3 = (com.facebook.widget.text.BetterButton) r3
            android.content.Context r0 = r3.getContext()
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2132148508(0x7f16011c, float:1.9938996E38)
            int r0 = r1.getDimensionPixelSize(r0)
            r3.setMinWidth(r0)
            android.content.Context r0 = r3.getContext()
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2132148388(0x7f1600a4, float:1.9938753E38)
            int r0 = r1.getDimensionPixelSize(r0)
            r3.setMinHeight(r0)
            r0 = 2132214756(0x7f1703e4, float:2.0073363E38)
            r3.setBackgroundResource(r0)
            android.content.Context r0 = r3.getContext()
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2132148290(0x7f160042, float:1.9938554E38)
            int r0 = r1.getDimensionPixelSize(r0)
            float r0 = (float) r0
            r3.setTextSize(r0)
            android.content.Context r0 = r3.getContext()
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2132148247(0x7f160017, float:1.9938467E38)
            int r2 = r1.getDimensionPixelSize(r0)
            android.content.Context r0 = r3.getContext()
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2132148247(0x7f160017, float:1.9938467E38)
            int r0 = r1.getDimensionPixelSize(r0)
            r3.setPadding(r2, r4, r0, r4)
            android.content.Context r0 = r3.getContext()
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131820545(0x7f110001, float:1.9273808E38)
            java.lang.String r1 = r1.getString(r0)
            r0 = 1
            android.graphics.Typeface r0 = android.graphics.Typeface.create(r1, r0)
            r3.setTypeface(r0)
            X.529 r0 = new X.529
            r0.<init>(r3)
            return r0
        L_0x0697:
            r3 = r12
            X.1vg r3 = (X.C37441vg) r3
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            r1 = 0
            switch(r14) {
                case 0: goto L_0x06ae;
                case 1: goto L_0x06bb;
                case 2: goto L_0x06bb;
                case 3: goto L_0x06bb;
                case 4: goto L_0x06bb;
                case 5: goto L_0x06bb;
                case 6: goto L_0x06bb;
                case 7: goto L_0x06d2;
                case 8: goto L_0x06d2;
                case 9: goto L_0x06bb;
                default: goto L_0x06a6;
            }
        L_0x06a6:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            java.lang.String r0 = "TransactionRowItem type not supported !!"
            r1.<init>(r0)
            throw r1
        L_0x06ae:
            r0 = 2132410932(0x7f1a0234, float:2.0471255E38)
            android.view.View r0 = r2.inflate(r0, r13, r1)
            X.Bos r2 = new X.Bos
            r2.<init>(r0)
            return r2
        L_0x06bb:
            r0 = 2132412258(0x7f1a0762, float:2.0473945E38)
            android.view.View r0 = r2.inflate(r0, r13, r1)
            X.Bot r2 = new X.Bot
            r2.<init>(r0)
            android.view.View r1 = r2.A0H
            X.BnM r0 = new X.BnM
            r0.<init>(r3, r2)
            r1.setOnClickListener(r0)
            return r2
        L_0x06d2:
            r0 = 2132410921(0x7f1a0229, float:2.0471233E38)
            android.view.View r0 = r2.inflate(r0, r13, r1)
            X.Bor r2 = new X.Bor
            r2.<init>(r0)
            com.facebook.resources.ui.FbTextView r1 = r2.A00
            X.BnM r0 = new X.BnM
            r0.<init>(r3, r2)
            r1.setOnClickListener(r0)
            return r2
        L_0x06e9:
            r3 = r12
            X.1IC r3 = (X.AnonymousClass1IC) r3
            switch(r14) {
                case 0: goto L_0x0757;
                case 1: goto L_0x0748;
                case 2: goto L_0x0739;
                case 3: goto L_0x072a;
                case 4: goto L_0x0719;
                case 5: goto L_0x070a;
                case 6: goto L_0x06fb;
                default: goto L_0x06ef;
            }
        L_0x06ef:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            java.lang.String r0 = "FbPayTransactionDetailsViewModel type not supported !! ViewType = "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r14)
            r1.<init>(r0)
            throw r1
        L_0x06fb:
            X.4fO r2 = new X.4fO
            X.4fP r1 = new X.4fP
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r2.<init>(r1)
            return r2
        L_0x070a:
            X.4fQ r2 = new X.4fQ
            X.4fR r1 = new X.4fR
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r2.<init>(r1)
            return r2
        L_0x0719:
            X.C7U r2 = new X.C7U
            com.facebook.payments.transactionhub.views.FbPayButtonView r1 = new com.facebook.payments.transactionhub.views.FbPayButtonView
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            com.facebook.payments.logging.PaymentsLoggingSessionData r0 = r3.A01
            r2.<init>(r1, r0)
            return r2
        L_0x072a:
            X.4fS r2 = new X.4fS
            X.4fT r1 = new X.4fT
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r2.<init>(r1)
            return r2
        L_0x0739:
            X.4fJ r2 = new X.4fJ
            X.4fI r1 = new X.4fI
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r2.<init>(r1)
            return r2
        L_0x0748:
            X.4fV r2 = new X.4fV
            X.4fU r1 = new X.4fU
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r2.<init>(r1)
            return r2
        L_0x0757:
            X.4fL r2 = new X.4fL
            X.4fK r1 = new X.4fK
            android.content.Context r0 = r13.getContext()
            r1.<init>(r0)
            r2.<init>(r1)
            return r2
        L_0x0766:
            r3 = r12
            X.1Qg r3 = (X.C23631Qg) r3
            r0 = 1
            r2 = 0
            if (r14 == r0) goto L_0x0796
            r0 = 2
            if (r14 != r0) goto L_0x0794
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132410893(0x7f1a020d, float:2.0471176E38)
            android.view.View r2 = r1.inflate(r0, r13, r2)
            r0 = 2131298204(0x7f09079c, float:1.8214375E38)
            android.view.View r1 = r2.findViewById(r0)
            X.CjA r0 = new X.CjA
            r0.<init>(r3)
            r1.setOnClickListener(r0)
            X.4EW r1 = new X.4EW
            r1.<init>(r2)
            return r1
        L_0x0794:
            r1 = 0
            return r1
        L_0x0796:
            android.content.Context r0 = r13.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r0)
            r0 = 2132410888(0x7f1a0208, float:2.0471166E38)
            android.view.View r0 = r1.inflate(r0, r13, r2)
            X.Civ r1 = new X.Civ
            r1.<init>(r3, r0)
            return r1
        L_0x07ab:
            r3 = r12
            X.1Qv r3 = (X.C23781Qv) r3
            android.content.Context r1 = r3.A03
            java.lang.String r0 = "Adapter has not been initialized. Please call init()"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            android.content.Context r0 = r3.A03
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            r1 = 2132412089(0x7f1a06b9, float:2.0473602E38)
            r0 = 0
            android.view.View r1 = r2.inflate(r1, r13, r0)
            X.D9g r0 = new X.D9g
            r0.<init>(r3)
            r1.setOnClickListener(r0)
            X.4EM r0 = new X.4EM
            r0.<init>(r1)
            return r0
        L_0x07d1:
            r0 = r12
            X.1Qe r0 = (X.C23611Qe) r0
            X.4ER r1 = new X.4ER
            X.DbG r0 = r0.A06
            android.view.View r0 = r0.AWB(r14, r13)
            r1.<init>(r0)
            return r1
        L_0x07e0:
            r2 = r12
            X.1Sm r2 = (X.C24151Sm) r2
            if (r14 >= 0) goto L_0x0804
            int r0 = r14 % 2
            if (r0 != 0) goto L_0x07fc
            int r0 = -r14
            int r0 = r0 >> 1
            int r1 = r0 + -1
            java.util.List r0 = r2.A00
        L_0x07f0:
            java.lang.Object r0 = r0.get(r1)
            android.view.View r0 = (android.view.View) r0
            X.4Eg r1 = new X.4Eg
            r1.<init>(r0)
            return r1
        L_0x07fc:
            int r0 = r14 + 1
            int r0 = r0 >> 1
            int r1 = -r0
            java.util.List r0 = r2.A01
            goto L_0x07f0
        L_0x0804:
            X.4Eg r1 = new X.4Eg
            X.1Sn r0 = r2.A03
            X.1o8 r0 = r0.BV1(r13, r14)
            r1.<init>(r0)
            return r1
        L_0x0810:
            r1 = r12
            X.1Qn r1 = (X.C23701Qn) r1
            X.2NH r4 = new X.2NH
            android.content.Context r0 = r13.getContext()
            r4.<init>(r0)
            int r3 = r1.A00
            r2 = -1
            r1 = -2
            r0 = 1
            if (r3 != r0) goto L_0x0835
            X.2NP r0 = new X.2NP
            r0.<init>(r2, r1)
            r4.setLayoutParams(r0)
        L_0x082b:
            r0 = 2
            r4.setImportantForAccessibility(r0)
            X.DzP r0 = new X.DzP
            r0.<init>(r4)
            return r0
        L_0x0835:
            X.2NP r0 = new X.2NP
            r0.<init>(r1, r2)
            r4.setLayoutParams(r0)
            goto L_0x082b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20831Dz.BV1(android.view.ViewGroup, int):X.1o8");
    }

    public void BW8(RecyclerView recyclerView) {
        if (this instanceof C24151Sm) {
            ((C24151Sm) this).A03.BW8(recyclerView);
        }
    }

    public long getItemId(int i) {
        if (this instanceof C23701Qn) {
            return ((C28642DyW) ((C23701Qn) this).A04.get(i)).A00;
        }
        if (this instanceof C24151Sm) {
            C24151Sm r2 = (C24151Sm) this;
            int size = r2.A01.size();
            int ArU = r2.A03.ArU();
            if (i < size) {
                return (long) (-1 - (i << 1));
            }
            int i2 = i - size;
            return i2 >= ArU ? (long) ((((i - r2.A03.ArU()) - r2.A01.size()) + 1) * -2) : r2.A03.getItemId(i2);
        } else if (this instanceof C23611Qe) {
            return ((C23611Qe) this).A06.getItemId(i);
        } else {
            if (this instanceof C20751Dr) {
                C20751Dr r1 = (C20751Dr) this;
                if (r1.A04) {
                    if (i == 0) {
                        return 0;
                    }
                    i--;
                }
                return (long) ((MediaResource) r1.A02.get(i)).A0D.hashCode();
            } else if (this instanceof C23661Qj) {
                return (long) i;
            } else {
                if ((this instanceof C23801Qx) || !(this instanceof AnonymousClass1A0)) {
                    return -1;
                }
                return (long) ((AnonymousClass1IK) ((AnonymousClass1A0) this).A00.A0h.get(i)).A0B;
            }
        }
    }

    public int getItemViewType(int i) {
        C26431CyL cyL;
        C26511Czx czx;
        int i2;
        Integer num;
        if (this instanceof C24151Sm) {
            C24151Sm r2 = (C24151Sm) this;
            int size = r2.A01.size();
            int ArU = r2.A03.ArU();
            if (i < size) {
                return -1 - (i << 1);
            }
            int i3 = i - size;
            return i3 >= ArU ? (((i - r2.A03.ArU()) - r2.A01.size()) + 1) * -2 : r2.A03.getItemViewType(i3);
        } else if (this instanceof C23611Qe) {
            return ((C23611Qe) this).A06.getItemViewType(i);
        } else {
            if (this instanceof C23631Qg) {
                return 1;
            }
            if (this instanceof AnonymousClass1IC) {
                return ((FbPayTransactionDetailsViewModel) ((AnonymousClass1IC) this).A02.get(i)).B9K();
            }
            if (this instanceof C37441vg) {
                return ((C23666Bk6) ((C37441vg) this).A03.get(i)).A00;
            }
            if (this instanceof C37451vh) {
                return ((C24729CFq) ((C37451vh) this).A02.get(i)).A00.intValue();
            }
            if (this instanceof C37461vi) {
                ImmutableList immutableList = ((C37461vi) this).A00;
                return (immutableList == null || i != immutableList.size()) ? 0 : 1;
            } else if (this instanceof C37471vj) {
                return ((C88204Jb) ((C12300p2) ((C37471vj) this).A02.get(i)).A00).ordinal();
            } else {
                if (this instanceof C23641Qh) {
                    return ((AnonymousClass4KG) ((C23641Qh) this).A0K.get(i)).A01;
                }
                if (this instanceof C21701Ij) {
                    C21701Ij r22 = (C21701Ij) this;
                    if (r22.A05 == null || (cyL = r22.A02) == null) {
                        return 3;
                    }
                    if (!cyL.A07 && i % C21701Ij.A00(r22) == 0) {
                        return 0;
                    }
                    if (C21701Ij.A02(r22, i) instanceof EffectItem) {
                        return 2;
                    }
                    if (!(C21701Ij.A02(r22, i) instanceof ArtItem)) {
                        return C21701Ij.A02(r22, i) instanceof ArtCategoryItem ? 4 : 3;
                    }
                    return 1;
                } else if (this instanceof AnonymousClass1TV) {
                    AnonymousClass1TV r1 = (AnonymousClass1TV) this;
                    Preconditions.checkNotNull(r1.A01);
                    return ((BaseItem) r1.A01.get(i)) instanceof EffectItem ? 1 : 0;
                } else if (this instanceof C21881Jb) {
                    C21881Jb r12 = (C21881Jb) this;
                    ArtItem A012 = C21881Jb.A01(r12, i);
                    if (A012 == null) {
                        throw new IllegalStateException("getItemViewType should not be called without items set or on invalid position indices");
                    } else if (A012.A02()) {
                        return (((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r12.A05.A00)).Aem(282501482939943L) || (czx = A012.A02) == C26511Czx.A06 || czx == C26511Czx.A04 || czx == C26511Czx.A03) ? 3 : 1;
                    } else {
                        boolean z = false;
                        if (A012.A00 != null) {
                            z = true;
                        }
                        return z ? 2 : 0;
                    }
                } else if (this instanceof C33361nS) {
                    C33361nS r13 = (C33361nS) this;
                    if (r13.A07.get(i) != null) {
                        D2H d2h = ((D2A) r13.A07.get(i)).A00;
                        if (d2h == D2H.EMOJI) {
                            return 0;
                        }
                        if (d2h == D2H.MONTAGE_STICKER) {
                            return 1;
                        }
                        if (d2h == D2H.SMART_STICKER) {
                            return 2;
                        }
                        if (d2h == D2H.STICKER) {
                            return 3;
                        }
                        if (d2h != D2H.INTERACTIVE_STICKER) {
                            return d2h != D2H.SMART_STICKER_ICON ? -1 : 5;
                        }
                        return 4;
                    }
                } else if (!(this instanceof C20751Dr)) {
                    if (this instanceof C23651Qi) {
                        C23651Qi r5 = (C23651Qi) this;
                        int size2 = r5.A04.size();
                        i2 = 3;
                        if (size2 != 0) {
                            boolean z2 = false;
                            if (!((C73473g8) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ASj, r5.A00)).A0D() ? i > size2 : !(i != 1 || r5.A03.isEmpty())) {
                                z2 = true;
                            }
                            if (i == 0) {
                                return 1;
                            }
                            return z2 ? 3 : 2;
                        }
                    } else if (this instanceof C23661Qj) {
                        C23661Qj r23 = (C23661Qj) this;
                        if (i != 0 || !r23.A03.A00) {
                            return (!r23.A03.A01 || i != r23.ArU() - 1) ? 1 : 2;
                        }
                        return 0;
                    } else if (this instanceof C23671Qk) {
                        C23671Qk r24 = (C23671Qk) this;
                        if (r24.A08 && i - 1 == -1) {
                            return 7;
                        }
                        if (r24.A09 && i == r24.A02.size()) {
                            return 8;
                        }
                        Integer num2 = ((PIIQuestion) r24.A02.get(i)).A01;
                        if (num2 == null) {
                            return -1;
                        }
                        switch (num2.intValue()) {
                            case 0:
                                num = AnonymousClass07B.A01;
                                break;
                            case 1:
                                num = AnonymousClass07B.A00;
                                break;
                            case 2:
                                num = AnonymousClass07B.A0C;
                                break;
                            case 3:
                                num = AnonymousClass07B.A0i;
                                break;
                            case 4:
                                num = AnonymousClass07B.A0n;
                                break;
                            case 5:
                                num = AnonymousClass07B.A0N;
                                break;
                            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                                num = AnonymousClass07B.A0Y;
                                break;
                            default:
                                return -1;
                        }
                        return C23671Qk.A00(num);
                    } else if (this instanceof C23681Ql) {
                        C23681Ql r14 = (C23681Ql) this;
                        if (i == r14.A01.size()) {
                            return 2;
                        }
                        i2 = 0;
                        if (!(i == 0 ? Objects.equal((String) r14.A09.get(), ((MessengerAccountInfo) r14.A01.get(i)).A04) : false)) {
                            return 1;
                        }
                    } else if (!(this instanceof AnonymousClass1A0)) {
                        return 0;
                    } else {
                        AnonymousClass1A0 r25 = (AnonymousClass1A0) this;
                        AnonymousClass1Ri r0 = r25.A00;
                        List list = r0.A0h;
                        if (r0.A0n) {
                            i %= list.size();
                        }
                        C21681Ih A03 = ((AnonymousClass1IK) list.get(i)).A03();
                        return A03.C2H() ? r25.A00.A0Z.A02 : A03.B9K();
                    }
                    return i2;
                } else {
                    C20751Dr r15 = (C20751Dr) this;
                    if (r15.A04) {
                        if (i == 0) {
                            return C421828p.values().length + 1;
                        }
                        i--;
                    }
                    return ((MediaResource) r15.A02.get(i)).A0L.ordinal();
                }
            }
        }
    }

    public final void A04() {
        this.A01.A00();
    }

    public final void A05(int i) {
        this.A01.A04(i, 1, null);
    }

    public final void A06(int i) {
        this.A01.A02(i, 1);
    }

    public final void A07(int i) {
        this.A01.A03(i, 1);
    }

    public final void A08(int i, Object obj) {
        this.A01.A04(i, 1, obj);
    }

    public void A09(boolean z) {
        if (!this.A01.A05()) {
            this.A00 = z;
            return;
        }
        throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
    }

    public void C0O(C15730vo r2) {
        this.A01.registerObserver(r2);
    }

    public void CJm(C15730vo r2) {
        this.A01.unregisterObserver(r2);
    }

    public final boolean hasStableIds() {
        return this.A00;
    }

    public void A0E(C33781o8 r1, int i, List list) {
        BPy(r1, i);
    }
}
