package X;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1gC  reason: invalid class name and case insensitive filesystem */
public abstract class C29301gC {
    public final C09330h1 A00;
    public final CopyOnWriteArrayList A01 = new CopyOnWriteArrayList();

    public void A01(C13330rF r4) {
        Iterator it;
        synchronized (this) {
            this.A00.A04(r4);
            it = this.A01.iterator();
        }
        while (it.hasNext()) {
            ((C30430Ew5) it.next()).onAddEvent(r4);
        }
    }

    public C29301gC(int i) {
        this.A00 = new C09330h1(i);
    }
}
