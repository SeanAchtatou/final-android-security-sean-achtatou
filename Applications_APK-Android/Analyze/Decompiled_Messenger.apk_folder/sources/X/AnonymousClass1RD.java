package X;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;

/* renamed from: X.1RD  reason: invalid class name */
public final class AnonymousClass1RD extends ShapeDrawable implements AnonymousClass1M9 {
    public final /* synthetic */ C21901Jd A00;

    public Drawable Akn() {
        return this;
    }

    public void clear() {
        setShaderFactory(null);
        getPaint().setShader(null);
        setShape(getShape());
    }

    public AnonymousClass1RD(C21901Jd r3, int i) {
        this.A00 = r3;
        Paint paint = getPaint();
        paint.setColor(i);
        paint.setDither(true);
        paint.setFilterBitmap(true);
    }

    public void C5v() {
        setShape(new OvalShape());
    }

    public void C5z() {
        C7T(new RectShape());
    }

    public void C60(float f) {
        setShape(new RoundRectShape(new float[]{f, f, f, f, f, f, f, f}, null, null));
    }

    public void C6M(AnonymousClass1S6 r4) {
        setShaderFactory(new C33511nh(null, r4.A04()));
        setShape(getShape());
    }

    public void CBr(int i) {
        setIntrinsicWidth(this.A00.A00);
        setIntrinsicHeight(this.A00.A00);
    }

    public boolean BGo() {
        if (getShaderFactory() != null) {
            return true;
        }
        return false;
    }

    public void C7T(Shape shape) {
        setShape(shape);
    }
}
