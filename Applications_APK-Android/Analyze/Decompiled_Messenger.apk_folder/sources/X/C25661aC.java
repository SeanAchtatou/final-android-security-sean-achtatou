package X;

import com.facebook.acra.ACRA;
import com.google.common.base.Platform;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1aC  reason: invalid class name and case insensitive filesystem */
public enum C25661aC {
    FACEBOOK,
    INSTAGRAM,
    PAGE,
    EVENT,
    GROUP,
    PARENT_APPROVED_USER,
    MESSENGER_CALL_GUEST_USER,
    SMS_MESSAGING_PARTICIPANT,
    UNSET;

    public static C25661aC A00(String str) {
        if (Platform.stringIsNullOrEmpty(str)) {
            return UNSET;
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -1058326424:
                if (str.equals("SmsMessagingParticipant")) {
                    c = 1;
                    break;
                }
                break;
            case 2479791:
                if (str.equals("Page")) {
                    c = 3;
                    break;
                }
                break;
            case 2645995:
                if (str.equals("User")) {
                    c = 0;
                    break;
                }
                break;
            case 67338874:
                if (str.equals("Event")) {
                    c = 4;
                    break;
                }
                break;
            case 69076575:
                if (str.equals("Group")) {
                    c = 5;
                    break;
                }
                break;
            case 95659194:
                if (str.equals(AnonymousClass80H.$const$string(10))) {
                    c = 2;
                    break;
                }
                break;
            case 1492338898:
                if (str.equals("MessengerCallGuestUser")) {
                    c = 6;
                    break;
                }
                break;
            case 1923144317:
                if (str.equals(C99084oO.$const$string(116))) {
                    c = 7;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return FACEBOOK;
            case 1:
                return SMS_MESSAGING_PARTICIPANT;
            case 2:
                return PARENT_APPROVED_USER;
            case 3:
                return PAGE;
            case 4:
                return EVENT;
            case 5:
                return GROUP;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return MESSENGER_CALL_GUEST_USER;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return INSTAGRAM;
            default:
                C010708t.A0O("MessagingActorType", "Got an actor of an unsupported GraphQL type: %s", str);
                return UNSET;
        }
    }
}
