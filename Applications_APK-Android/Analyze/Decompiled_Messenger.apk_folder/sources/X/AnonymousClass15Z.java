package X;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.15Z  reason: invalid class name */
public final class AnonymousClass15Z<F, T> extends AbstractCollection<T> {
    public final Function A00;
    public final Collection A01;

    public void clear() {
        this.A01.clear();
    }

    public boolean isEmpty() {
        return this.A01.isEmpty();
    }

    public Iterator iterator() {
        Iterator it = this.A01.iterator();
        Function function = this.A00;
        Preconditions.checkNotNull(function);
        return new C26991cT(it, function);
    }

    public int size() {
        return this.A01.size();
    }

    public AnonymousClass15Z(Collection collection, Function function) {
        Preconditions.checkNotNull(collection);
        this.A01 = collection;
        Preconditions.checkNotNull(function);
        this.A00 = function;
    }
}
