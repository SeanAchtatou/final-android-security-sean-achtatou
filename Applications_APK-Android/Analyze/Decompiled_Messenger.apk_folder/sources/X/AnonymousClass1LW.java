package X;

import android.graphics.Bitmap;

/* renamed from: X.1LW  reason: invalid class name */
public final class AnonymousClass1LW implements C22761Ms {
    private C22761Ms A00;
    public final /* synthetic */ Bitmap.Config A01;
    public final /* synthetic */ AnonymousClass1O0 A02;

    public AnonymousClass1LW(AnonymousClass1O0 r1, Bitmap.Config config) {
        this.A02 = r1;
        this.A01 = config;
    }

    public AnonymousClass1S9 AWg(AnonymousClass1NY r4, int i, C33271nJ r6, C23541Px r7) {
        C22761Ms r0;
        if (this.A00 == null) {
            if (this.A02.A01.Aem(282063387427900L)) {
                r0 = new C27509Dds();
            } else {
                r0 = new AnonymousClass96F(this);
            }
            this.A00 = r0;
        }
        return this.A00.AWg(r4, i, r6, r7);
    }
}
