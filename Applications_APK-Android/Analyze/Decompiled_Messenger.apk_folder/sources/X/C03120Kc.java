package X;

import android.os.health.TimerStat;

/* renamed from: X.0Kc  reason: invalid class name and case insensitive filesystem */
public final class C03120Kc {
    public int A00;
    public long A01;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C03120Kc r7 = (C03120Kc) obj;
            return this.A00 == r7.A00 && this.A01 == r7.A01;
        }
        return false;
    }

    public int hashCode() {
        long j = this.A01;
        return (this.A00 * 31) + ((int) (j ^ (j >>> 32)));
    }

    public C03120Kc() {
    }

    public C03120Kc(int i, long j) {
        this.A00 = i;
        this.A01 = j;
    }

    public C03120Kc(C03120Kc r3) {
        this.A00 = r3.A00;
        this.A01 = r3.A01;
    }

    public C03120Kc(TimerStat timerStat) {
        this.A00 = timerStat.getCount();
        this.A01 = timerStat.getTime();
    }
}
