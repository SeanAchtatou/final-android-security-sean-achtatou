package X;

/* renamed from: X.0jU  reason: invalid class name and case insensitive filesystem */
public abstract class C10060jU {
    public abstract C10120ja forClassAnnotations(C10470kA r1, C10030jR r2, C26761by r3);

    public abstract C10120ja forCreation(C10490kF r1, C10030jR r2, C26761by r3);

    public abstract C10120ja forDeserialization(C10490kF r1, C10030jR r2, C26761by r3);

    public abstract C10120ja forDeserializationWithBuilder(C10490kF r1, C10030jR r2, C26761by r3);

    public abstract C10120ja forSerialization(C10450k8 r1, C10030jR r2, C26761by r3);
}
