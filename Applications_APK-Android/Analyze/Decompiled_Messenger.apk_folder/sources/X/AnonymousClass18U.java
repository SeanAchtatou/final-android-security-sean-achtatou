package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.18U  reason: invalid class name */
public final class AnonymousClass18U implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18U(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r7) {
        int A002 = AnonymousClass09Y.A00(-1321144512);
        C193617v r0 = this.A00.A03;
        if (r0 != null) {
            r0.BtS(true, "ads injected");
        }
        AnonymousClass09Y.A01(-1994044991, A002);
    }
}
