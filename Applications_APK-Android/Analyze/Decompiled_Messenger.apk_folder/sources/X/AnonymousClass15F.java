package X;

import android.os.Handler;
import android.os.HandlerThread;
import java.util.ArrayDeque;

/* renamed from: X.15F  reason: invalid class name */
public final class AnonymousClass15F implements AnonymousClass0V6 {
    public final /* synthetic */ AnonymousClass15D A00;

    public AnonymousClass15F(AnonymousClass15D r1) {
        this.A00 = r1;
    }

    public HandlerThread AUc(String str, int i, boolean z) {
        HandlerThread A02;
        HandlerThread handlerThread;
        AnonymousClass15D r5 = this.A00;
        synchronized (r5) {
            ArrayDeque arrayDeque = r5.A03;
            synchronized (r5) {
                A02 = r5.A02.A02("FastHandlerThreadFactory-idle", AnonymousClass0V7.NORMAL);
                A02.start();
            }
            arrayDeque.offer(A02);
            handlerThread = (HandlerThread) r5.A03.poll();
            AnonymousClass00S.A04(new Handler(handlerThread.getLooper()), new AnonymousClass15H(str, i, handlerThread), 1464575073);
        }
        return handlerThread;
    }
}
