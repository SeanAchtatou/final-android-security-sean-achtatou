package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import com.facebook.push.mqtt.ipc.IMqttPushService;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.1mN  reason: invalid class name and case insensitive filesystem */
public final class C32811mN implements ServiceConnection {
    public final CountDownLatch A00 = new CountDownLatch(1);
    public final /* synthetic */ C36991uR A01;

    public C32811mN(C36991uR r3) {
        this.A01 = r3;
    }

    public void A00(IBinder iBinder) {
        IMqttPushService proxy;
        C36991uR r2 = this.A01;
        if (iBinder == null) {
            proxy = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface(AnonymousClass80H.$const$string(56));
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IMqttPushService)) {
                proxy = new IMqttPushService.Stub.Proxy(iBinder);
            } else {
                proxy = (IMqttPushService) queryLocalInterface;
            }
        }
        synchronized (r2) {
            r2.A00 = proxy;
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        C36991uR r0 = this.A01;
        r0.A06.A01(new C34431pZ(r0.A05.now(), "ServiceConnected (MqttPushServiceClientManager)", new Object[0]));
        A00(iBinder);
        this.A00.countDown();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        C36991uR r0 = this.A01;
        r0.A06.A01(new C34431pZ(r0.A05.now(), "ServiceDisconnected (MqttPushServiceClientManager)", new Object[0]));
        C36991uR r3 = this.A01;
        synchronized (r3) {
            r3.A00 = null;
            synchronized (r3) {
                for (C57332ro BYW : r3.A0A) {
                    BYW.BYW();
                }
                r3.A0A.clear();
            }
        }
    }
}
