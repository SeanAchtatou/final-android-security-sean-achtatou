package X;

import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.crypto.keychain.KeyChain;
import com.facebook.crypto.module.LoggedInUserCrypto;
import com.facebook.user.model.User;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@UserScoped
/* renamed from: X.18F  reason: invalid class name */
public final class AnonymousClass18F implements C05460Za, KeyChain {
    private static C05540Zi A08;
    public static final AnonymousClass18G A09 = AnonymousClass18G.A00.A04();
    public static final String A0A = AnonymousClass18F.class.getName();
    public AnonymousClass0UN A00;
    public Map A01;
    public byte[] A02;
    public byte[] A03;
    public final AnonymousClass09P A04;
    public final C30491i8 A05;
    private final SecureRandom A06 = C30241hj.A00();
    @LoggedInUser
    private final C04310Tq A07;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        X.C010708t.A0L(X.AnonymousClass18F.A0A, "Failed to decrypt master key using logged in user entity", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1 = ((com.facebook.crypto.module.LoggedInUserCrypto) X.AnonymousClass1XX.A02(0, X.AnonymousClass1Y3.BQh, r4.A00)).A02(r5, X.C194418e.A00("UserMasterKey.0"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0038, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0 = r1.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        com.google.common.base.Preconditions.checkNotNull(r0);
        A04(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0044, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        X.C010708t.A0L(X.AnonymousClass18F.A0A, "Failed to encrypt master key", r2);
        r1 = new X.C37911wY("Re-encryption of master key failed", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0057, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        X.C010708t.A0L(X.AnonymousClass18F.A0A, "Failed to decrypt master key using zero-fbid user entity", r2);
        r1 = new X.C37911wY("Decryption failed", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0066, code lost:
        throw r1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized byte[] A06(byte[] r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r3 = 0
            X.18e r2 = A00(r4)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            int r1 = X.AnonymousClass1Y3.BQh     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            X.0UN r0 = r4.A00     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            com.facebook.crypto.module.LoggedInUserCrypto r0 = (com.facebook.crypto.module.LoggedInUserCrypto) r0     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            X.1ih r1 = r0.A02(r5, r2)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            monitor-enter(r1)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            byte[] r0 = r1.A00     // Catch:{ all -> 0x0019 }
            monitor-exit(r1)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            goto L_0x0042
        L_0x0019:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
            throw r0     // Catch:{ 1wJ | 1wZ | IOException -> 0x001c }
        L_0x001c:
            r2 = move-exception
            java.lang.String r1 = X.AnonymousClass18F.A0A     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = "Failed to decrypt master key using logged in user entity"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = "UserMasterKey.0"
            X.18e r2 = X.C194418e.A00(r0)     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            int r1 = X.AnonymousClass1Y3.BQh     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            X.0UN r0 = r4.A00     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            com.facebook.crypto.module.LoggedInUserCrypto r0 = (com.facebook.crypto.module.LoggedInUserCrypto) r0     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            X.1ih r1 = r0.A02(r5, r2)     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            monitor-enter(r1)     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            byte[] r0 = r1.A00     // Catch:{ all -> 0x0054 }
            monitor-exit(r1)     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ 1wY -> 0x0044 }
            r4.A04(r0)     // Catch:{ 1wY -> 0x0044 }
        L_0x0042:
            monitor-exit(r4)
            return r0
        L_0x0044:
            r2 = move-exception
            java.lang.String r1 = X.AnonymousClass18F.A0A     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = "Failed to encrypt master key"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ all -> 0x0067 }
            X.1wY r1 = new X.1wY     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = "Re-encryption of master key failed"
            r1.<init>(r0, r2)     // Catch:{ all -> 0x0067 }
            goto L_0x0066
        L_0x0054:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
            throw r0     // Catch:{ 1wJ | 1wZ | IOException -> 0x0057 }
        L_0x0057:
            r2 = move-exception
            java.lang.String r1 = X.AnonymousClass18F.A0A     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = "Failed to decrypt master key using zero-fbid user entity"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ all -> 0x0067 }
            X.1wY r1 = new X.1wY     // Catch:{ all -> 0x0067 }
            java.lang.String r0 = "Decryption failed"
            r1.<init>(r0, r2)     // Catch:{ all -> 0x0067 }
        L_0x0066:
            throw r1     // Catch:{ all -> 0x0067 }
        L_0x0067:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18F.A06(byte[]):byte[]");
    }

    public synchronized byte[] A07(byte[] bArr) {
        C37911wY r1;
        byte[] bArr2;
        byte[] bArr3 = this.A02;
        if (bArr3 != null) {
            try {
                C30821ih A022 = ((LoggedInUserCrypto) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQh, this.A00)).A02(bArr, C194418e.A00(AnonymousClass08S.A0J("UserMasterKey.", new String(bArr3))));
                synchronized (A022) {
                    bArr2 = A022.A00;
                }
                Arrays.fill(this.A02, (byte) 0);
                this.A02 = null;
            } catch (C37821wJ | C37921wZ | IOException e) {
                C010708t.A0L(A0A, "Failed to decrypt master key using logged in user entity", e);
                r1 = new C37911wY("Decryption failed", e);
            }
        } else {
            C010708t.A0I(A0A, "Failed to decrypt master key because of null account key");
            r1 = new C37911wY("Decryption failed because of null account key");
            throw r1;
        }
        return bArr2;
    }

    public void clearUserData() {
        synchronized (this) {
            byte[] bArr = this.A03;
            if (bArr != null) {
                Arrays.fill(bArr, (byte) 0);
                this.A03 = null;
            }
        }
    }

    public synchronized byte[] getCipherKey() {
        byte[] bArr;
        bArr = this.A03;
        if (bArr == null) {
            LoggedInUserCrypto loggedInUserCrypto = (LoggedInUserCrypto) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQh, this.A00);
            byte[] A042 = this.A05.A04(AnonymousClass197.A05);
            if (A042 != null) {
                try {
                    loggedInUserCrypto.A03(new AnonymousClass2DJ(this));
                } catch (C37821wJ e) {
                    C010708t.A0L(A0A, "Failed to config account key setter", e);
                }
                A03(this);
                A02(this);
                byte[] A072 = A07(A042);
                this.A03 = A072;
                A04(A072);
                bArr = this.A03;
            } else {
                byte[] A043 = this.A05.A04(AnonymousClass197.A04);
                if (A043 != null) {
                    A03(this);
                    byte[] A044 = this.A05.A04(AnonymousClass197.A01);
                    if (A044 != null) {
                        C194418e A002 = A00(this);
                        C194418e A003 = C194418e.A00("UserMasterKey.0");
                        if (!Arrays.equals(A044, A002.A00) && !Arrays.equals(A044, A003.A00)) {
                            AnonymousClass09P r6 = this.A04;
                            String str = A0A;
                            AnonymousClass18G r3 = A09;
                            r6.CGS(str, AnonymousClass08S.A0S("Entity for master-key mismatch. Stored: ", r3.A06(A044), ", expected: ", r3.A06(A002.A00)));
                        }
                    }
                    bArr = A06(A043);
                    this.A03 = bArr;
                } else {
                    byte[] A045 = this.A05.A04(AnonymousClass197.A03);
                    if (A045 != null) {
                        this.A03 = A045;
                    } else {
                        this.A03 = A05(this);
                        if (!loggedInUserCrypto.A04()) {
                            this.A05.A03(AnonymousClass197.A03, A045);
                        }
                    }
                    if (loggedInUserCrypto.A04()) {
                        A04(this.A03);
                    }
                    bArr = this.A03;
                }
            }
        }
        return bArr;
    }

    public byte[] getMacKey() {
        return new byte[0];
    }

    public static C194418e A00(AnonymousClass18F r2) {
        User user = (User) r2.A07.get();
        Preconditions.checkNotNull(user, "This must not be called when user is not logged in");
        return C194418e.A00(AnonymousClass08S.A0J("UserMasterKey.", user.A0j));
    }

    public static final AnonymousClass18F A01(AnonymousClass1XY r4) {
        AnonymousClass18F r0;
        synchronized (AnonymousClass18F.class) {
            C05540Zi A002 = C05540Zi.A00(A08);
            A08 = A002;
            try {
                if (A002.A03(r4)) {
                    A08.A00 = new AnonymousClass18F((AnonymousClass1XY) A08.A01());
                }
                C05540Zi r1 = A08;
                r0 = (AnonymousClass18F) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A08.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A02(AnonymousClass18F r3) {
        if (r3.A05.A04(AnonymousClass197.A01) != null && r3.A02 == null) {
            ((C07380dK) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BQ3, r3.A00)).A03("tincan_account_key_null");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0033 A[Catch:{ 1wJ | 1wZ | IOException -> 0x001f, all -> 0x0051 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.AnonymousClass18F r7) {
        /*
            java.lang.String r0 = "PROBE"
            X.18e r5 = X.C194418e.A00(r0)     // Catch:{ all -> 0x0051 }
            X.1i8 r1 = r7.A05     // Catch:{ all -> 0x0051 }
            X.19A r0 = X.AnonymousClass197.A02     // Catch:{ all -> 0x0051 }
            byte[] r2 = r1.A04(r0)     // Catch:{ all -> 0x0051 }
            r6 = 0
            if (r2 == 0) goto L_0x002e
            int r1 = X.AnonymousClass1Y3.BQh     // Catch:{ 1wJ | 1wZ | IOException -> 0x001f }
            X.0UN r0 = r7.A00     // Catch:{ 1wJ | 1wZ | IOException -> 0x001f }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001f }
            com.facebook.crypto.module.LoggedInUserCrypto r0 = (com.facebook.crypto.module.LoggedInUserCrypto) r0     // Catch:{ 1wJ | 1wZ | IOException -> 0x001f }
            r0.A02(r2, r5)     // Catch:{ 1wJ | 1wZ | IOException -> 0x001f }
            goto L_0x0030
        L_0x001f:
            r4 = move-exception
            X.09P r3 = r7.A04     // Catch:{ all -> 0x0051 }
            java.lang.String r2 = X.AnonymousClass18F.A0A     // Catch:{ all -> 0x0051 }
            java.lang.String r1 = "Error testing probe"
            X.4kP r0 = new X.4kP     // Catch:{ all -> 0x0051 }
            r0.<init>(r4)     // Catch:{ all -> 0x0051 }
            r3.softReport(r2, r1, r0)     // Catch:{ all -> 0x0051 }
        L_0x002e:
            r0 = 0
            goto L_0x0031
        L_0x0030:
            r0 = 1
        L_0x0031:
            if (r0 != 0) goto L_0x0060
            java.lang.String r1 = "PROBECONTENT"
            java.lang.String r0 = "UTF-8"
            byte[] r2 = r1.getBytes(r0)     // Catch:{ all -> 0x0051 }
            int r1 = X.AnonymousClass1Y3.BQh     // Catch:{ all -> 0x0051 }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x0051 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x0051 }
            com.facebook.crypto.module.LoggedInUserCrypto r0 = (com.facebook.crypto.module.LoggedInUserCrypto) r0     // Catch:{ all -> 0x0051 }
            byte[] r2 = r0.A06(r2, r5)     // Catch:{ all -> 0x0051 }
            X.1i8 r1 = r7.A05     // Catch:{ all -> 0x0051 }
            X.19A r0 = X.AnonymousClass197.A02     // Catch:{ all -> 0x0051 }
            r1.A03(r0, r2)     // Catch:{ all -> 0x0051 }
            return
        L_0x0051:
            r0 = move-exception
            X.09P r3 = r7.A04
            java.lang.String r2 = X.AnonymousClass18F.A0A
            X.4kQ r1 = new X.4kQ
            r1.<init>(r0)
            java.lang.String r0 = "UNKOWN error"
            r3.softReport(r2, r0, r1)
        L_0x0060:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18F.A03(X.18F):void");
    }

    public static byte[] A05(AnonymousClass18F r3) {
        byte[] bArr;
        synchronized (r3.A06) {
            bArr = new byte[AnonymousClass189.A03(AnonymousClass187.A04)];
            r3.A06.nextBytes(bArr);
        }
        return bArr;
    }

    public byte[] getNewIV() {
        byte[] bArr;
        synchronized (this.A06) {
            bArr = new byte[AnonymousClass189.A02(AnonymousClass187.A04)];
            this.A06.nextBytes(bArr);
        }
        return bArr;
    }

    private AnonymousClass18F(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A07 = AnonymousClass0WY.A01(r3);
        this.A05 = new C30491i8(AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, r3));
        this.A04 = C04750Wa.A01(r3);
        this.A01 = new HashMap();
    }

    private void A04(byte[] bArr) {
        try {
            C194418e A002 = A00(this);
            byte[] A062 = ((LoggedInUserCrypto) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQh, this.A00)).A06(bArr, A002);
            this.A05.A03(AnonymousClass197.A01, A002.A00);
            this.A05.A03(AnonymousClass197.A04, A062);
            this.A05.A01(AnonymousClass197.A03);
            this.A05.A01(AnonymousClass197.A05);
        } catch (C37821wJ | C37921wZ | IOException e) {
            C010708t.A0L(A0A, "Failed to encrypt master key for local storage", e);
            throw new C37911wY("Encryption failed", e);
        }
    }
}
