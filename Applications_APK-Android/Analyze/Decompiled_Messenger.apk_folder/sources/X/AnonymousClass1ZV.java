package X;

/* renamed from: X.1ZV  reason: invalid class name */
public final class AnonymousClass1ZV implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appjobs.scheduler.impl.AppJobsSchedulerTriggersQueue$AppJobsRunnable";
    public final C25261Yy A00;
    private final int A01;

    public void run() {
        this.A00.A03(this.A01);
    }

    public AnonymousClass1ZV(C25261Yy r1, int i) {
        this.A00 = r1;
        this.A01 = i;
    }
}
