package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Fm  reason: invalid class name and case insensitive filesystem */
public final class C21171Fm {
    public static final Class A04 = C21171Fm.class;
    private static volatile C21171Fm A05;
    public C33641nu A00;
    public boolean A01 = false;
    public final C05160Xw A02;
    public final List A03 = new ArrayList();

    public static final C21171Fm A00(AnonymousClass1XY r7) {
        if (A05 == null) {
            synchronized (C21171Fm.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A05 = new C21171Fm(applicationInjector, AnonymousClass0VM.A00(applicationInjector).A04(AnonymousClass0VS.URGENT, "MqttClientSingleThreadExecutorService"), AnonymousClass0UX.A0D(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A01(C21171Fm r4, List list, List list2) {
        r4.A03.addAll(list);
        Iterator it = list2.iterator();
        while (it.hasNext()) {
            SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
            if (!r4.A03.remove(subscribeTopic)) {
                C010708t.A0C(A04, "Unsubscribed from topic that was not subscribed: '%s'", subscribeTopic);
            }
        }
    }

    public String A02() {
        C33641nu r0 = this.A00;
        if (r0 == null) {
            return "not_initialized";
        }
        return r0.A00.A0D.A0A().name();
    }

    private C21171Fm(AnonymousClass1XY r4, C05160Xw r5, C05160Xw r6) {
        if (AnonymousClass0WA.A00(r4).AbO(300, false)) {
            this.A02 = r5;
        } else {
            this.A02 = r6;
        }
    }

    public void A03(Collection collection, Collection collection2) {
        this.A02.CIC(new C21261Fw(this, ImmutableList.copyOf(collection), ImmutableList.copyOf(collection2)));
    }
}
