package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import androidx.fragment.app.Fragment;
import com.facebook.base.activity.DelegatingFbFragmentFrameworkActivity;

/* renamed from: X.1lV  reason: invalid class name and case insensitive filesystem */
public final class C32281lV implements C13080qc {
    public final /* synthetic */ DelegatingFbFragmentFrameworkActivity A00;

    public C32281lV(DelegatingFbFragmentFrameworkActivity delegatingFbFragmentFrameworkActivity) {
        this.A00 = delegatingFbFragmentFrameworkActivity;
    }

    public void AMa(C13180qr r2) {
        C32281lV.super.AMa(r2);
    }

    public void AZu() {
        C32281lV.super.finish();
    }

    public void AZx(Activity activity) {
        C32281lV.super.finishFromChild(activity);
    }

    public Object AqZ(Class cls) {
        return C32281lV.super.A16(cls);
    }

    public MenuInflater Atv() {
        return C32281lV.super.getMenuInflater();
    }

    public Object Aze(Object obj) {
        return C32281lV.super.Aze(obj);
    }

    public Resources B1A() {
        return C32281lV.super.getResources();
    }

    public C13060qW B4m() {
        return C32281lV.super.B4m();
    }

    public View B8z(int i) {
        return C32281lV.super.A14(i);
    }

    public Window B9w() {
        return C32281lV.super.getWindow();
    }

    public boolean BB8(Throwable th) {
        return C32281lV.super.BB8(th);
    }

    public boolean BBy() {
        return C32281lV.super.hasWindowFocus();
    }

    public boolean BLI(boolean z) {
        return C32281lV.super.moveTaskToBack(z);
    }

    public void BNB(Bundle bundle) {
        C32281lV.super.A1B(bundle);
    }

    public void BNF(Intent intent) {
        C32281lV.super.A19(intent);
    }

    public void BNH(int i, int i2, Intent intent) {
        C32281lV.super.onActivityResult(i, i2, intent);
    }

    public void BOX(Fragment fragment) {
        C32281lV.super.A11(fragment);
    }

    public void BPZ(Bundle bundle) {
        C32281lV.super.A1C(bundle);
    }

    public boolean BUO(MenuItem menuItem) {
        return C32281lV.super.onContextItemSelected(menuItem);
    }

    public Dialog BUk(int i) {
        return C32281lV.super.onCreateDialog(i);
    }

    public boolean BUt(Menu menu) {
        return C32281lV.super.onCreateOptionsMenu(menu);
    }

    public boolean BhL(MenuItem menuItem) {
        return C32281lV.super.onOptionsItemSelected(menuItem);
    }

    public void BjD(Bundle bundle) {
        C32281lV.super.onPostCreate(bundle);
    }

    public void BjI() {
        C32281lV.super.onPostResume();
    }

    public void BjR(int i, Dialog dialog) {
        C32281lV.super.onPrepareDialog(i, dialog);
    }

    public boolean BjY(Menu menu) {
        return C32281lV.super.onPrepareOptionsMenu(menu);
    }

    public void BmS() {
        C32281lV.super.A10();
    }

    public void Bmx(Bundle bundle) {
        C32281lV.super.onSaveInstanceState(bundle);
    }

    public void Btu() {
        C32281lV.super.onUserInteraction();
    }

    public void C0V(C27231cr r2) {
        C32281lV.super.C0V(r2);
    }

    public void C1L(C13180qr r2) {
        C32281lV.super.C1L(r2);
    }

    public void C7B(int i) {
        C32281lV.super.setContentView(i);
    }

    public void C7C(View view) {
        C32281lV.super.setContentView(view);
    }

    public void C8U(Intent intent) {
        C32281lV.super.setIntent(intent);
    }

    public void CB0(Object obj, Object obj2) {
        C32281lV.super.CB0(obj, obj2);
    }

    public void CBQ(int i) {
        C32281lV.super.setRequestedOrientation(i);
    }

    public void CGn(Intent intent) {
        C32281lV.super.startActivity(intent);
    }

    public void CIW() {
        C32281lV.super.CIW();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return C32281lV.super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return C32281lV.super.dispatchTouchEvent(motionEvent);
    }

    public Intent getIntent() {
        return C32281lV.super.getIntent();
    }

    public void onActivityDestroy() {
        C32281lV.super.A17();
    }

    public void onAttachedToWindow() {
        C32281lV.super.onAttachedToWindow();
    }

    public void onBackPressed() {
        C32281lV.super.onBackPressed();
    }

    public void onConfigurationChanged(Configuration configuration) {
        C32281lV.super.onConfigurationChanged(configuration);
    }

    public void onContentChanged() {
        C32281lV.super.onContentChanged();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        C32281lV.super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public View onCreatePanelView(int i) {
        return C32281lV.super.onCreatePanelView(i);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return C32281lV.super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return C32281lV.super.onKeyUp(i, keyEvent);
    }

    public void onLowMemory() {
        C32281lV.super.onLowMemory();
    }

    public void onPause() {
        C32281lV.super.onPause();
    }

    public void onResume() {
        C32281lV.super.onResume();
    }

    public boolean onSearchRequested() {
        return C32281lV.super.onSearchRequested();
    }

    public void onStart() {
        C32281lV.super.onStart();
    }

    public void onStop() {
        C32281lV.super.onStop();
    }

    public void onTrimMemory(int i) {
        C32281lV.super.onTrimMemory(i);
    }

    public void onWindowFocusChanged(boolean z) {
        C32281lV.super.onWindowFocusChanged(z);
    }

    public void startActivityForResult(Intent intent, int i) {
        C32281lV.super.startActivityForResult(intent, i);
    }
}
