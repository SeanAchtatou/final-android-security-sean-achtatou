package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/* renamed from: X.021  reason: invalid class name */
public final class AnonymousClass021 implements Application.ActivityLifecycleCallbacks {
    private final AnonymousClass01P A00;

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        AnonymousClass01P.A09(this.A00, activity, AnonymousClass01Y.ACTIVITY_CREATED);
    }

    public void onActivityDestroyed(Activity activity) {
        AnonymousClass01P.A09(this.A00, activity, AnonymousClass01Y.ACTIVITY_DESTROYED);
    }

    public void onActivityPaused(Activity activity) {
        AnonymousClass01P.A09(this.A00, activity, AnonymousClass01Y.ACTIVITY_PAUSED);
    }

    public void onActivityResumed(Activity activity) {
        AnonymousClass01P.A09(this.A00, activity, AnonymousClass01Y.ACTIVITY_RESUMED);
    }

    public void onActivityStarted(Activity activity) {
        AnonymousClass01P.A09(this.A00, activity, AnonymousClass01Y.ACTIVITY_STARTED);
    }

    public void onActivityStopped(Activity activity) {
        AnonymousClass01P.A09(this.A00, activity, AnonymousClass01Y.ACTIVITY_STOPPED);
    }

    public AnonymousClass021(AnonymousClass01P r1) {
        this.A00 = r1;
    }
}
