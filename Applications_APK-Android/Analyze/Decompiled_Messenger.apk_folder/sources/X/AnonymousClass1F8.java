package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1F8  reason: invalid class name */
public final class AnonymousClass1F8 {
    private static volatile AnonymousClass1F8 A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass1F8 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1F8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1F8(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001d, code lost:
        if (r1.booleanValue() != false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(java.lang.String r4) {
        /*
            r3 = this;
            int r1 = X.AnonymousClass1Y3.BH4
            X.0UN r0 = r3.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            X.00z r2 = (X.C001500z) r2
            int r1 = X.AnonymousClass1Y3.BAo
            X.0UN r0 = r3.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            X.00z r0 = X.C001500z.A07
            if (r2 != r0) goto L_0x001f
            boolean r1 = r1.booleanValue()
            r0 = 1
            if (r1 == 0) goto L_0x0020
        L_0x001f:
            r0 = 0
        L_0x0020:
            if (r0 == 0) goto L_0x0024
            r0 = 1
            return r0
        L_0x0024:
            java.lang.String r0 = "bottom_sharesheet_suggestions"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x0042
            int r1 = X.AnonymousClass1Y3.BU6
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.4QY r0 = (X.AnonymousClass4QY) r0
            X.1Yd r2 = r0.A00
            r0 = 283003985594496(0x1016400080880, double:1.398225469183915E-309)
        L_0x003d:
            boolean r0 = r2.Aem(r0)
            return r0
        L_0x0042:
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r3.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 282385509778796(0x100d40000056c, double:1.395169792650705E-309)
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1F8.A01(java.lang.String):boolean");
    }

    private AnonymousClass1F8(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
