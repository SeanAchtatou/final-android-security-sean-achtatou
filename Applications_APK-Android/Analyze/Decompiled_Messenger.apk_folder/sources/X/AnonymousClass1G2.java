package X;

import android.os.Bundle;
import android.os.Handler;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchThreadListParams;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1G2  reason: invalid class name */
public final class AnonymousClass1G2 implements AnonymousClass1YQ, C05460Za, CallerContextable {
    private static volatile AnonymousClass1G2 A08 = null;
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.common.UnreadInboxItemsCalculator";
    public AnonymousClass0UN A00;
    public ThreadsCollection A01 = null;
    public ThreadsCollection A02 = null;
    public C13880sE A03 = null;
    private C12300p2 A04 = null;
    private final C11330mk A05;
    private final C04310Tq A06;
    public volatile boolean A07 = false;

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0060, code lost:
        if (((X.C14780u1) X.AnonymousClass1XX.A02(5, X.AnonymousClass1Y3.BLe, r12.A00)).A04(r8.A0S).A03() == false) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A02(X.AnonymousClass1G2 r12, com.facebook.messaging.model.threads.ThreadsCollection r13, com.facebook.messaging.model.threads.ThreadsCollection r14, java.lang.String r15) {
        /*
            monitor-enter(r12)
            if (r13 == 0) goto L_0x0017
            r2 = 11
            int r1 = X.AnonymousClass1Y3.BAo     // Catch:{ all -> 0x017e }
            X.0UN r0 = r12.A00     // Catch:{ all -> 0x017e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x017e }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x017e }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0019
            if (r14 != 0) goto L_0x0019
        L_0x0017:
            monitor-exit(r12)
            return
        L_0x0019:
            com.google.common.collect.ImmutableList r1 = r13.A00     // Catch:{ all -> 0x017e }
            if (r14 != 0) goto L_0x001f
            r0 = 0
            goto L_0x0021
        L_0x001f:
            com.google.common.collect.ImmutableList r0 = r14.A00     // Catch:{ all -> 0x017e }
        L_0x0021:
            com.google.common.collect.ImmutableList r6 = X.AnonymousClass170.A01(r1, r0)     // Catch:{ all -> 0x017e }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x017e }
            r2.<init>()     // Catch:{ all -> 0x017e }
            com.facebook.messaging.badging.logging.ThreadReadStats r7 = new com.facebook.messaging.badging.logging.ThreadReadStats     // Catch:{ all -> 0x017e }
            r7.<init>()     // Catch:{ all -> 0x017e }
            r1 = 20
            int r0 = r6.size()     // Catch:{ all -> 0x017e }
            int r5 = java.lang.Math.min(r1, r0)     // Catch:{ all -> 0x017e }
            r4 = 0
            r9 = 0
        L_0x003b:
            r3 = 1
            if (r4 >= r5) goto L_0x00be
            java.lang.Object r8 = r6.get(r4)     // Catch:{ all -> 0x017e }
            com.facebook.messaging.model.threads.ThreadSummary r8 = (com.facebook.messaging.model.threads.ThreadSummary) r8     // Catch:{ all -> 0x017e }
            boolean r0 = r8.A0B()     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0062
            r10 = 5
            int r1 = X.AnonymousClass1Y3.BLe     // Catch:{ all -> 0x017e }
            X.0UN r0 = r12.A00     // Catch:{ all -> 0x017e }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r10, r1, r0)     // Catch:{ all -> 0x017e }
            X.0u1 r1 = (X.C14780u1) r1     // Catch:{ all -> 0x017e }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r8.A0S     // Catch:{ all -> 0x017e }
            com.facebook.messaging.model.threads.NotificationSetting r0 = r1.A04(r0)     // Catch:{ all -> 0x017e }
            boolean r0 = r0.A03()     // Catch:{ all -> 0x017e }
            r10 = 1
            if (r0 != 0) goto L_0x0063
        L_0x0062:
            r10 = 0
        L_0x0063:
            X.6x7 r1 = new X.6x7     // Catch:{ all -> 0x017e }
            r0 = 0
            if (r10 == 0) goto L_0x0069
            r0 = 1
        L_0x0069:
            r1.<init>(r8, r0)     // Catch:{ all -> 0x017e }
            r2.add(r1)     // Catch:{ all -> 0x017e }
            if (r10 == 0) goto L_0x0097
            int r9 = r9 + 1
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r8.A0S     // Catch:{ all -> 0x017e }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r1)     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0081
            int r0 = r7.A03     // Catch:{ all -> 0x017e }
            int r0 = r0 + r3
            r7.A03 = r0     // Catch:{ all -> 0x017e }
            goto L_0x00ba
        L_0x0081:
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r1)     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x008d
            int r0 = r7.A05     // Catch:{ all -> 0x017e }
            int r0 = r0 + r3
            r7.A05 = r0     // Catch:{ all -> 0x017e }
            goto L_0x00ba
        L_0x008d:
            boolean r0 = r8.A17     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x00ba
            int r0 = r7.A01     // Catch:{ all -> 0x017e }
            int r0 = r0 + r3
            r7.A01 = r0     // Catch:{ all -> 0x017e }
            goto L_0x00ba
        L_0x0097:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r8.A0S     // Catch:{ all -> 0x017e }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r1)     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x00a5
            int r0 = r7.A02     // Catch:{ all -> 0x017e }
            int r0 = r0 + r3
            r7.A02 = r0     // Catch:{ all -> 0x017e }
            goto L_0x00ba
        L_0x00a5:
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r1)     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x00b1
            int r0 = r7.A04     // Catch:{ all -> 0x017e }
            int r0 = r0 + r3
            r7.A04 = r0     // Catch:{ all -> 0x017e }
            goto L_0x00ba
        L_0x00b1:
            boolean r0 = r8.A17     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x00ba
            int r0 = r7.A00     // Catch:{ all -> 0x017e }
            int r0 = r0 + r3
            r7.A00 = r0     // Catch:{ all -> 0x017e }
        L_0x00ba:
            int r4 = r4 + 1
            goto L_0x003b
        L_0x00be:
            X.0Tq r0 = r12.A06     // Catch:{ all -> 0x017e }
            java.lang.Object r5 = r0.get()     // Catch:{ all -> 0x017e }
            X.42Q r5 = (X.AnonymousClass42Q) r5     // Catch:{ all -> 0x017e }
            monitor-enter(r5)     // Catch:{ all -> 0x017e }
            r5.A00 = r9     // Catch:{ all -> 0x0178 }
            java.util.Map r0 = r5.A01     // Catch:{ all -> 0x0178 }
            r0.clear()     // Catch:{ all -> 0x0178 }
            java.util.Iterator r4 = r2.iterator()     // Catch:{ all -> 0x0178 }
        L_0x00d2:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x0178 }
            if (r0 == 0) goto L_0x00e6
            java.lang.Object r2 = r4.next()     // Catch:{ all -> 0x0178 }
            X.6x7 r2 = (X.C149846x7) r2     // Catch:{ all -> 0x0178 }
            java.util.Map r1 = r5.A01     // Catch:{ all -> 0x0178 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A01     // Catch:{ all -> 0x0178 }
            r1.put(r0, r2)     // Catch:{ all -> 0x0178 }
            goto L_0x00d2
        L_0x00e6:
            monitor-exit(r5)     // Catch:{ all -> 0x017e }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x017e }
            X.0UN r0 = r12.A00     // Catch:{ all -> 0x017e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x017e }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x017e }
            long r10 = r0.now()     // Catch:{ all -> 0x017e }
            boolean r0 = r12.A07     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0119
            X.0p2 r1 = r12.A04     // Catch:{ all -> 0x017e }
            if (r1 == 0) goto L_0x0119
            java.lang.Object r0 = r1.A00     // Catch:{ all -> 0x017e }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x017e }
            int r0 = r0.intValue()     // Catch:{ all -> 0x017e }
            if (r0 != r9) goto L_0x0119
            java.lang.Object r0 = r1.A01     // Catch:{ all -> 0x017e }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x017e }
            long r4 = r0.longValue()     // Catch:{ all -> 0x017e }
            r0 = 2000(0x7d0, double:9.88E-321)
            long r4 = r4 + r0
            int r0 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x0119
            goto L_0x0017
        L_0x0119:
            X.0p2 r2 = new X.0p2     // Catch:{ all -> 0x017e }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x017e }
            java.lang.Long r0 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x017e }
            r2.<init>(r1, r0)     // Catch:{ all -> 0x017e }
            r12.A04 = r2     // Catch:{ all -> 0x017e }
            X.0mk r1 = r12.A05     // Catch:{ all -> 0x017e }
            java.lang.String r0 = "Badging - UnreadInboxItemsCalculator - "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r9)     // Catch:{ all -> 0x017e }
            r1.BIo(r0)     // Catch:{ all -> 0x017e }
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x017e }
            X.0UN r0 = r12.A00     // Catch:{ all -> 0x017e }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x017e }
            X.16c r6 = (X.C189216c) r6     // Catch:{ all -> 0x017e }
            r8 = 0
            int r2 = X.AnonymousClass1Y3.BHq     // Catch:{ all -> 0x017e }
            X.0UN r1 = r6.A00     // Catch:{ all -> 0x017e }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r8, r2, r1)     // Catch:{ all -> 0x017e }
            X.4LX r5 = (X.AnonymousClass4LX) r5     // Catch:{ all -> 0x017e }
            monitor-enter(r5)     // Catch:{ all -> 0x017e }
            X.48y r3 = new X.48y     // Catch:{ all -> 0x017b }
            X.06B r0 = r5.A01     // Catch:{ all -> 0x017b }
            long r1 = r0.now()     // Catch:{ all -> 0x017b }
            r0 = 0
            r3.<init>(r9, r1, r0)     // Catch:{ all -> 0x017b }
            r5.A00 = r3     // Catch:{ all -> 0x017b }
            monitor-exit(r5)     // Catch:{ all -> 0x017e }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ all -> 0x017e }
            java.lang.String r0 = X.C06680bu.A0I     // Catch:{ all -> 0x017e }
            r1.<init>(r0)     // Catch:{ all -> 0x017e }
            java.lang.String r0 = "EXTRA_BADGE_COUNT"
            r1.putExtra(r0, r9)     // Catch:{ all -> 0x017e }
            java.lang.String r0 = "EXTRA_BADGE_COUNT_BUSINESS"
            r1.putExtra(r0, r8)     // Catch:{ all -> 0x017e }
            java.lang.String r0 = "EXTRA_BADGE_COUNT_STATS"
            r1.putExtra(r0, r7)     // Catch:{ all -> 0x017e }
            java.lang.String r0 = "badge_count_update_trigger"
            r1.putExtra(r0, r15)     // Catch:{ all -> 0x017e }
            X.C189216c.A04(r6, r1)     // Catch:{ all -> 0x017e }
            goto L_0x0017
        L_0x0178:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x017e }
            goto L_0x017d
        L_0x017b:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x017e }
        L_0x017d:
            throw r0     // Catch:{ all -> 0x017e }
        L_0x017e:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1G2.A02(X.1G2, com.facebook.messaging.model.threads.ThreadsCollection, com.facebook.messaging.model.threads.ThreadsCollection, java.lang.String):void");
    }

    public static synchronized void A03(AnonymousClass1G2 r5, String str) {
        synchronized (r5) {
            if (!r5.A07) {
                r5.A01 = null;
                r5.A02 = null;
                C05350Yp.A08(r5.A00(C10950l8.A05).CGe(), new C30168Eqn(r5, str), (ExecutorService) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BIG, r5.A00));
                if (((Boolean) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BAo, r5.A00)).booleanValue()) {
                    C05350Yp.A08(r5.A00(C10950l8.A0B).CGe(), new C30170Eqp(r5, str), (ExecutorService) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BIG, r5.A00));
                }
            }
        }
    }

    public synchronized void clearUserData() {
        this.A04 = null;
        this.A03 = null;
        this.A01 = null;
        this.A02 = null;
    }

    public String getSimpleName() {
        return ECX.$const$string(AnonymousClass1Y3.A0s);
    }

    private AnonymousClass0lL A00(C10950l8 r6) {
        C10680kg r3 = new C10680kg();
        r3.A02 = C09510hU.DO_NOT_CHECK_SERVER;
        r3.A04 = r6;
        r3.A06 = ((C10960l9) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BOk, this.A00)).A04();
        r3.A00 = 20;
        FetchThreadListParams fetchThreadListParams = new FetchThreadListParams(r3);
        Bundle bundle = new Bundle();
        bundle.putParcelable("fetchThreadListParams", fetchThreadListParams);
        return ((BlueServiceOperationFactory) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B1W, this.A00)).newInstance("fetch_thread_list", bundle, 0, CallerContext.A07(AnonymousClass1G2.class, "badge"));
    }

    public static final AnonymousClass1G2 A01(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass1G2.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass1G2(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    private void A04(String str) {
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AKb, this.A00)).BMm();
        BMm.A02(str, new C91524Yr(this));
        BMm.A01((Handler) AnonymousClass1XX.A02(10, AnonymousClass1Y3.AcC, this.A00));
        BMm.A00().A00();
    }

    public void A05(boolean z) {
        if (((C35321r2) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Avj, this.A00)).A00.Aem(282282430563604L)) {
            this.A07 = z;
        }
    }

    private AnonymousClass1G2(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(14, r4);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.BA5, r4);
        this.A05 = ((AnonymousClass1CU) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AuL, this.A00)).A01(AnonymousClass24B.$const$string(27));
    }

    public void init() {
        int A032 = C000700l.A03(474841903);
        C001500z r1 = (C001500z) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BH4, this.A00);
        if (r1 == C001500z.A03 || r1 == C001500z.A08) {
            C000700l.A09(-1702129222, A032);
            return;
        }
        A04(C06680bu.A0H);
        A04("com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP");
        A04("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP");
        A03(this, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP");
        C000700l.A09(1390179285, A032);
    }
}
