package X;

/* renamed from: X.189  reason: invalid class name */
public final class AnonymousClass189 {
    public static byte A00(Integer num) {
        return 1 - num.intValue() != 0 ? (byte) 1 : 2;
    }

    public static int A02(Integer num) {
        return 12;
    }

    public static int A03(Integer num) {
        return 1 - num.intValue() != 0 ? 16 : 32;
    }

    public static int A01(Integer num) {
        return 16;
    }
}
