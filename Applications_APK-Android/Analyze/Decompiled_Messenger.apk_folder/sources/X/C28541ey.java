package X;

import com.fasterxml.jackson.databind.JsonNode;

/* renamed from: X.1ey  reason: invalid class name and case insensitive filesystem */
public final class C28541ey {
    public AnonymousClass0UN A00;

    public static final C28541ey A00(AnonymousClass1XY r1) {
        return new C28541ey(r1);
    }

    public static String A01(JsonNode jsonNode, String str) {
        if (jsonNode == null || jsonNode.get(str) == null || jsonNode.get(str).getNodeType() == C11980oL.NULL) {
            return null;
        }
        return jsonNode.get(str).asText();
    }

    public C28541ey(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
