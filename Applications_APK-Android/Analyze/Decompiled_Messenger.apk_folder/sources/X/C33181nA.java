package X;

/* renamed from: X.1nA  reason: invalid class name and case insensitive filesystem */
public final class C33181nA {
    public static C48132Zq A00;
    public static C30384EvI A01;
    public static C124875uA A02;
    public static C51302gG A03;
    public static AnonymousClass5O1 A04;
    public static C30388EvO A05;
    public static AnonymousClass5E3 A06;
    public static C30389EvP A07;
    public static AnonymousClass2I1 A08;
    public static AnonymousClass216 A09;
    public static C30390EvQ A0A;
    public static C30391EvR A0B;
    public static C48122Zp A0C;
    private static C74263hX A0D;
    private static C88344Jx A0E;
    private static C73913gy A0F;
    private static C73923gz A0G;
    private static C74423ho A0H;
    private static AnonymousClass1TA A0I;

    public static void A00(AnonymousClass10N r1) {
        if (A0D == null) {
            A0D = new C74263hX();
        }
        r1.A00(A0D);
    }

    public static void A01(AnonymousClass10N r1) {
        if (A0E == null) {
            A0E = new C88344Jx();
        }
        r1.A00(A0E);
    }

    public static void A02(AnonymousClass10N r1) {
        if (A0F == null) {
            A0F = new C73913gy();
        }
        r1.A00(A0F);
    }

    public static void A03(AnonymousClass10N r1) {
        if (A0G == null) {
            A0G = new C73923gz();
        }
        r1.A00(A0G);
    }

    public static void A05(AnonymousClass10N r1, int i, float f) {
        if (A0H == null) {
            A0H = new C74423ho();
        }
        C74423ho r0 = A0H;
        r0.A01 = i;
        r0.A00 = f;
        r1.A00(r0);
    }

    public static void A04(AnonymousClass10N r2) {
        boolean A022 = C27041cY.A02();
        if (A022) {
            C27041cY.A01("EventDispatcherUtils.dispatchOnVisible");
        }
        if (A0I == null) {
            A0I = new AnonymousClass1TA();
        }
        r2.A00(A0I);
        if (A022) {
            C27041cY.A00();
        }
    }
}
