package X;

import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.16X  reason: invalid class name */
public final class AnonymousClass16X implements C19961Aj {
    public final /* synthetic */ C19941Ah A00;

    public AnonymousClass16X(C19941Ah r1) {
        this.A00 = r1;
    }

    public void add(Object obj) {
        ThreadSummary threadSummary = (ThreadSummary) obj;
        this.A00.A00.put(threadSummary.A0S, threadSummary);
    }

    public void clear() {
        this.A00.A00.clear();
    }

    public boolean isEmpty() {
        return this.A00.A00.isEmpty();
    }
}
