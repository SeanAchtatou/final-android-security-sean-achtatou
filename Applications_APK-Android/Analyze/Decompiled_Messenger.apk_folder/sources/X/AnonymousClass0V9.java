package X;

import android.os.HandlerThread;
import java.lang.ref.WeakReference;

/* renamed from: X.0V9  reason: invalid class name */
public final class AnonymousClass0V9 extends HandlerThread {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.FbHandlerThreadFactory$FbHandlerThread";
    private C53472kq A00;
    private boolean A01;
    private final boolean A02;
    public final /* synthetic */ AnonymousClass0V4 A03;

    public synchronized void start() {
        if (!this.A02 || !this.A01) {
            super.start();
            this.A01 = true;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0V9(AnonymousClass0V4 r3, String str, int i, boolean z) {
        super(str, i);
        this.A03 = r3;
        this.A02 = z;
        AnonymousClass0V4.A02.add(new WeakReference(this));
    }

    public void onLooperPrepared() {
        AnonymousClass05x.A00();
        C37041uW r1 = (C37041uW) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A05, this.A03.A00);
        if (r1 != null) {
            this.A00 = new C53472kq(r1);
            C09730iO.A00(getLooper()).A01(this.A00);
        }
        C37051uX r0 = (C37051uX) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BUR, this.A03.A00);
        if (r0 != null) {
            C09730iO.A00(getLooper()).A01(r0.createLogger());
        }
    }

    public void run() {
        String str;
        try {
            super.run();
            if (this.A02) {
                C179868Tg.A00("Combined handler exited", AnonymousClass08S.A0J("Handler: ", getName()), null);
            }
        } finally {
            C53472kq r1 = this.A00;
            if (r1 != null) {
                str = "exitLooper";
                C53472kq.A00(r1, str, 0, 0);
            }
        }
    }
}
