package X;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Process;
import android.util.Log;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/* renamed from: X.014  reason: invalid class name */
public final class AnonymousClass014 implements AnonymousClass015 {
    private static int A00 = 2;
    private static int A01 = 5;
    private static int A02 = 5;
    private static int A03 = 10;
    private static int A04 = 30;
    private static int A05 = 40;
    private static int A06 = 45000;
    private static int A07 = -1;
    private static int A08 = -1;
    public static boolean A09;
    private static int A0A;
    private static int A0B;
    private static long A0C;
    private static AnonymousClass019 A0D;
    /* access modifiers changed from: private */
    public static AnonymousClass019 A0E;
    private static AnonymousClass01C A0F;
    private static AnonymousClass01C A0G;

    private static void A0A(int i, int i2, Context context, String str) {
        int i3;
        int i4;
        Context context2 = context;
        String str2 = context.getApplicationInfo().dataDir;
        long currentTimeMillis = System.currentTimeMillis();
        A0E(context, currentTimeMillis, false);
        A0E(context, currentTimeMillis, true);
        File file = new File(str2, "app_was_disabled");
        if (file.exists()) {
            try {
                C03650Ox.A01(new C03650Ox(context), 0);
            } catch (PackageManager.NameNotFoundException e) {
                throw new RuntimeException(e);
            } catch (RuntimeException e2) {
                Log.e("CatchMeIfYouCan", "unable to reset crash loop", e2);
            }
            file.delete();
        }
        AnonymousClass01C r0 = A0G;
        if (r0 == null) {
            i3 = 0;
        } else {
            i3 = r0.A00;
        }
        AnonymousClass01C r02 = A0F;
        if (r02 == null) {
            i4 = 0;
        } else {
            i4 = r02.A00;
        }
        int i5 = i2;
        String str3 = str;
        if (i2 <= i4) {
            int i6 = i;
            if (i <= i3) {
                if (i2 <= 0) {
                    if (i <= 0) {
                        return;
                    }
                }
            }
            A0C(context, i6, i3, false, currentTimeMillis, str);
            return;
        }
        A0C(context2, i5, i4, true, currentTimeMillis, str3);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0018  */
    /* JADX WARNING: Removed duplicated region for block: B:9:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass013 A02(java.lang.String r2) {
        /*
            if (r2 == 0) goto L_0x0015
            java.lang.Class r0 = java.lang.Class.forName(r2)     // Catch:{ all -> 0x000d }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ all -> 0x000d }
            X.013 r0 = (X.AnonymousClass013) r0     // Catch:{ all -> 0x000d }
            goto L_0x0016
        L_0x000d:
            r2 = move-exception
            java.lang.String r1 = "CatchMeIfYouCan"
            java.lang.String r0 = "instantiating custom remedy class failed; continuing"
            android.util.Log.e(r1, r0, r2)
        L_0x0015:
            r0 = 0
        L_0x0016:
            if (r0 != 0) goto L_0x001d
            X.013 r0 = new X.013
            r0.<init>()
        L_0x001d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass014.A02(java.lang.String):X.013");
    }

    private static void A09() {
        if ((A0A & 2) != 0) {
            try {
                if (System.currentTimeMillis() - A0C > ((long) A06)) {
                    A0D.A01();
                } else {
                    A0E.A01();
                }
            } catch (Throwable unused) {
            }
        }
    }

    public static void A0B(Context context) {
        int i;
        int myPid = Process.myPid();
        int myUid = Process.myUid();
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (next.uid == myUid && (i = next.pid) != myPid) {
                Process.killProcess(i);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x002f */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0062  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A0E(android.content.Context r9, long r10, boolean r12) {
        /*
            java.lang.String r3 = "CatchMeIfYouCan"
            if (r12 == 0) goto L_0x0009
            r7 = 3600000(0x36ee80, double:1.7786363E-317)
        L_0x0007:
            r6 = 0
            goto L_0x000d
        L_0x0009:
            r7 = 86400000(0x5265c00, double:4.2687272E-316)
            goto L_0x0007
        L_0x000d:
            java.io.File r5 = X.AnonymousClass01C.A00(r9, r12)     // Catch:{ all -> 0x0030 }
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0030 }
            java.lang.String r0 = "r"
            r1.<init>(r5, r0)     // Catch:{ all -> 0x0030 }
            int r2 = r1.readInt()     // Catch:{ all -> 0x0029 }
            r1.close()     // Catch:{ all -> 0x0030 }
            X.01C r4 = new X.01C     // Catch:{ all -> 0x0030 }
            long r0 = r5.lastModified()     // Catch:{ all -> 0x0030 }
            r4.<init>(r0, r2)     // Catch:{ all -> 0x0030 }
            goto L_0x0044
        L_0x0029:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x002f }
        L_0x002f:
            throw r0     // Catch:{ all -> 0x0030 }
        L_0x0030:
            r1 = move-exception
            java.io.File r0 = X.AnonymousClass01C.A00(r9, r12)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0040
            java.lang.String r0 = "unable to read remedy log file"
            android.util.Log.w(r3, r0, r1)
        L_0x0040:
            X.AnonymousClass01C.A01(r9, r12)
            r4 = r6
        L_0x0044:
            if (r4 == 0) goto L_0x0054
            long r0 = r4.A01
            long r10 = r10 - r0
            r1 = 0
            int r0 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x005a
            java.lang.String r0 = "remedy is from the future!"
            android.util.Log.w(r3, r0)
        L_0x0054:
            r6 = r4
        L_0x0055:
            if (r12 == 0) goto L_0x0062
            X.AnonymousClass014.A0F = r6
            return
        L_0x005a:
            int r0 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r0 < 0) goto L_0x0054
            X.AnonymousClass01C.A01(r9, r12)
            goto L_0x0055
        L_0x0062:
            X.AnonymousClass014.A0G = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass014.A0E(android.content.Context, long, boolean):void");
    }

    private static void A0H(Throwable th) {
        if (A09) {
            Log.e("CatchMeIfYouCan", AnonymousClass08S.A0P("Uncaught exception in '", AnonymousClass00M.A00().A03(), "':"));
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            for (String e : stringWriter.toString().split("\n")) {
                Log.e("CatchMeIfYouCan", e);
            }
        }
    }

    public void handleUncaughtException(Thread thread, Throwable th, C02120Da r4) {
        if (!(th instanceof AnonymousClass0MZ)) {
            A09();
            try {
                A0H(th);
            } catch (Throwable unused) {
            }
            if ((A0A & 1) != 0) {
                A08();
            }
        }
    }

    public static int A00() {
        return A0B;
    }

    private static C03670Oz A03(Context context, int i, int i2, boolean z, String str) {
        AnonymousClass013 A022 = A02(str);
        if (z) {
            return A022.A04(context, i, i2);
        }
        return AnonymousClass013.A01(A022, context, i, i2);
    }

    private static File A04(Context context) {
        return new File(context.getApplicationInfo().dataDir, "crash_log");
    }

    private static File A05(Context context) {
        return new File(context.getApplicationInfo().dataDir, "insta_crash_log");
    }

    public static String A06(Context context) {
        return A04(context).getPath();
    }

    public static String A07(Context context) {
        return A05(context).getPath();
    }

    /*  JADX ERROR: ArrayIndexOutOfBoundsException in pass: SSATransform
        java.lang.ArrayIndexOutOfBoundsException: Index 1 out of bounds for length 1
        	at jadx.core.dex.visitors.ssa.LiveVarAnalysis.processLiveInfo(LiveVarAnalysis.java:96)
        	at jadx.core.dex.visitors.ssa.LiveVarAnalysis.runAnalysis(LiveVarAnalysis.java:37)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:50)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    private static void A08() {
        /*
            int r0 = android.os.Process.myPid()
            android.os.Process.killProcess(r0)
            r0 = 10
            java.lang.System.exit(r0)
        L_0x000c:
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass014.A08():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0042, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0046 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A0C(android.content.Context r7, int r8, int r9, boolean r10, long r11, java.lang.String r13) {
        /*
            X.0Oz r3 = A03(r7, r8, r9, r10, r13)
            boolean r0 = r3.A01
            java.lang.String r2 = "CatchMeIfYouCan"
            if (r0 == 0) goto L_0x004d
            X.01C r4 = new X.01C     // Catch:{ IOException -> 0x0047 }
            r4.<init>(r11, r8)     // Catch:{ IOException -> 0x0047 }
            java.io.File r6 = X.AnonymousClass01C.A00(r7, r10)     // Catch:{ IOException -> 0x0047 }
            java.io.RandomAccessFile r5 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0047 }
            java.lang.String r0 = "rw"
            r5.<init>(r6, r0)     // Catch:{ IOException -> 0x0047 }
            int r0 = r4.A00     // Catch:{ all -> 0x0040 }
            r5.writeInt(r0)     // Catch:{ all -> 0x0040 }
            long r0 = r5.getFilePointer()     // Catch:{ all -> 0x0040 }
            r5.setLength(r0)     // Catch:{ all -> 0x0040 }
            r5.close()     // Catch:{ IOException -> 0x0047 }
            long r0 = r4.A01     // Catch:{ IOException -> 0x0047 }
            boolean r0 = r6.setLastModified(r0)     // Catch:{ IOException -> 0x0047 }
            if (r0 != 0) goto L_0x0038
            java.lang.String r1 = "CrashLoopRemedyLog"
            java.lang.String r0 = "unable to set remedy log last modified timestamp"
            android.util.Log.w(r1, r0)     // Catch:{ IOException -> 0x0047 }
        L_0x0038:
            if (r10 == 0) goto L_0x003d
            X.AnonymousClass014.A0F = r4     // Catch:{ IOException -> 0x0047 }
            goto L_0x004d
        L_0x003d:
            X.AnonymousClass014.A0G = r4     // Catch:{ IOException -> 0x0047 }
            goto L_0x004d
        L_0x0040:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x0042:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x0046 }
        L_0x0046:
            throw r0     // Catch:{ IOException -> 0x0047 }
        L_0x0047:
            r1 = move-exception
            java.lang.String r0 = "error recording remedy log"
            android.util.Log.w(r2, r0, r1)
        L_0x004d:
            boolean r0 = r3.A00
            if (r0 == 0) goto L_0x005e
            A0B(r7)     // Catch:{ all -> 0x0055 }
            goto L_0x005b
        L_0x0055:
            r1 = move-exception
            java.lang.String r0 = "error killing sibling processes"
            android.util.Log.w(r2, r0, r1)
        L_0x005b:
            A08()
        L_0x005e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass014.A0C(android.content.Context, int, int, boolean, long, java.lang.String):void");
    }

    public static void A0D(Context context, int i, String str, AnonymousClass011 r16) {
        A09 = A0I(context);
        AnonymousClass016.A02(new AnonymousClass014(), -100000);
        A0A = i;
        AnonymousClass011 r1 = r16;
        if (r16 != null) {
            int i2 = r1.A01;
            if (i2 > 0) {
                A00 = i2;
            }
            int i3 = r1.A02;
            if (i3 > 0) {
                A02 = i3;
            }
            int i4 = r1.A03;
            if (i4 > 0) {
                A03 = i4;
            }
            int i5 = r1.A00;
            if (i5 > 0) {
                A06 = i5;
            }
        }
        if (AnonymousClass018.A01) {
            A01 = 3;
            A04 = 5;
            A05 = 7;
        }
        File A042 = A04(context);
        String str2 = context.getApplicationInfo().sourceDir;
        A0C = System.currentTimeMillis();
        long lastModified = new File(str2).lastModified();
        long j = A0C - lastModified;
        if (A042.exists() && A042.lastModified() < lastModified) {
            AnonymousClass01C.A01(context, false);
            if (!A042.delete()) {
                Log.e("CatchMeIfYouCan", "unable to delete stale crash log file: " + A042);
            }
        }
        File A052 = A05(context);
        if (A052.exists() && A052.lastModified() < lastModified) {
            AnonymousClass01C.A01(context, true);
            if (!A052.delete()) {
                Log.e("CatchMeIfYouCan", "could not delete insta crash log file: " + A052);
            }
        }
        A0D = new AnonymousClass019(context, A042, 40);
        A0E = new AnonymousClass019(context, A052, 40);
        if ((i & 2) != 0) {
            Executors.newScheduledThreadPool(1).schedule(new AnonymousClass01A(str, context), (long) A06, TimeUnit.MILLISECONDS);
            A0F(context, str, j);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00b2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b6, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A0F(android.content.Context r8, java.lang.String r9, long r10) {
        /*
            android.content.pm.ApplicationInfo r0 = r8.getApplicationInfo()
            java.lang.String r2 = r0.dataDir
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "crash_lock"
            r1.<init>(r2, r0)
            X.01B r4 = new X.01B
            r4.<init>(r1)
            X.019 r0 = X.AnonymousClass014.A0D     // Catch:{ all -> 0x00b0 }
            r2 = 14400(0x3840, float:2.0179E-41)
            int r1 = r0.A00(r2)     // Catch:{ all -> 0x00b0 }
            X.AnonymousClass014.A07 = r1     // Catch:{ all -> 0x00b0 }
            int r0 = X.AnonymousClass014.A05     // Catch:{ all -> 0x00b0 }
            r6 = 1
            if (r1 < r0) goto L_0x0023
            r5 = 3
            goto L_0x002f
        L_0x0023:
            int r0 = X.AnonymousClass014.A04     // Catch:{ all -> 0x00b0 }
            if (r1 < r0) goto L_0x0029
            r5 = 2
            goto L_0x002f
        L_0x0029:
            int r0 = X.AnonymousClass014.A01     // Catch:{ all -> 0x00b0 }
            r5 = 0
            if (r1 < r0) goto L_0x002f
            r5 = 1
        L_0x002f:
            X.019 r0 = X.AnonymousClass014.A0E     // Catch:{ all -> 0x00b0 }
            int r1 = r0.A00(r2)     // Catch:{ all -> 0x00b0 }
            X.AnonymousClass014.A08 = r1     // Catch:{ all -> 0x00b0 }
            int r0 = X.AnonymousClass014.A03     // Catch:{ all -> 0x00b0 }
            if (r1 < r0) goto L_0x003d
            r3 = 3
            goto L_0x004a
        L_0x003d:
            int r0 = X.AnonymousClass014.A02     // Catch:{ all -> 0x00b0 }
            if (r1 < r0) goto L_0x0042
            goto L_0x0049
        L_0x0042:
            int r0 = X.AnonymousClass014.A00     // Catch:{ all -> 0x00b0 }
            r3 = 0
            if (r1 < r0) goto L_0x004a
            r3 = 1
            goto L_0x004a
        L_0x0049:
            r3 = 2
        L_0x004a:
            if (r3 > r6) goto L_0x004e
            if (r5 <= r6) goto L_0x0093
        L_0x004e:
            java.lang.String r1 = "DANJIN"
            java.lang.String r0 = "Ditto.disableForCrashMitigation() runs"
            android.util.Log.w(r1, r0)     // Catch:{ all -> 0x00b0 }
            java.io.File r7 = new java.io.File     // Catch:{ IOException | RuntimeException -> 0x0093 }
            java.io.File r2 = new java.io.File     // Catch:{ IOException | RuntimeException -> 0x0093 }
            android.content.pm.ApplicationInfo r0 = r8.getApplicationInfo()     // Catch:{ IOException | RuntimeException -> 0x0093 }
            java.lang.String r1 = r0.dataDir     // Catch:{ IOException | RuntimeException -> 0x0093 }
            java.lang.String r0 = "ditto"
            r2.<init>(r1, r0)     // Catch:{ IOException | RuntimeException -> 0x0093 }
            boolean r0 = r2.exists()     // Catch:{ IOException | RuntimeException -> 0x0093 }
            if (r0 == 0) goto L_0x0073
            boolean r0 = r2.isDirectory()     // Catch:{ IOException | RuntimeException -> 0x0093 }
            if (r0 != 0) goto L_0x0073
            r2.delete()     // Catch:{ IOException | RuntimeException -> 0x0093 }
        L_0x0073:
            boolean r0 = r2.exists()     // Catch:{ IOException | RuntimeException -> 0x0093 }
            if (r0 != 0) goto L_0x007c
            r2.mkdir()     // Catch:{ IOException | RuntimeException -> 0x0093 }
        L_0x007c:
            boolean r0 = r2.isDirectory()     // Catch:{ IOException | RuntimeException -> 0x0093 }
            if (r0 == 0) goto L_0x008b
            java.lang.String r0 = X.AnonymousClass096.A00     // Catch:{ IOException | RuntimeException -> 0x0093 }
            r7.<init>(r2, r0)     // Catch:{ IOException | RuntimeException -> 0x0093 }
            r7.createNewFile()     // Catch:{ IOException | RuntimeException -> 0x0093 }
            goto L_0x0093
        L_0x008b:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ IOException | RuntimeException -> 0x0093 }
            java.lang.String r0 = "could not create ditto directory"
            r1.<init>(r0)     // Catch:{ IOException | RuntimeException -> 0x0093 }
            throw r1     // Catch:{ IOException | RuntimeException -> 0x0093 }
        L_0x0093:
            r1 = 86400000(0x5265c00, double:4.2687272E-316)
            int r0 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x009d
            if (r5 <= r6) goto L_0x009d
            r5 = 1
        L_0x009d:
            r1 = 3600000(0x36ee80, double:1.7786363E-317)
            int r0 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00a7
            if (r3 <= r6) goto L_0x00a7
            r3 = 1
        L_0x00a7:
            X.AnonymousClass014.A0B = r3     // Catch:{ all -> 0x00b0 }
            A0A(r5, r3, r8, r9)     // Catch:{ all -> 0x00b0 }
            r4.close()
            return
        L_0x00b0:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00b2 }
        L_0x00b2:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x00b6 }
        L_0x00b6:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass014.A0F(android.content.Context, java.lang.String, long):void");
    }

    private static boolean A0I(Context context) {
        if ((context.getApplicationInfo().flags & 2) != 0) {
            return true;
        }
        return false;
    }
}
