package X;

import com.facebook.messaging.rtc.interop.LatencyTestContentProvider;

/* renamed from: X.0TM  reason: invalid class name */
public final class AnonymousClass0TM implements AnonymousClass04e {
    public final /* synthetic */ LatencyTestContentProvider A00;

    public AnonymousClass0TM(LatencyTestContentProvider latencyTestContentProvider) {
        this.A00 = latencyTestContentProvider;
    }

    public void C2S(String str) {
        this.A00.A00.CGS("LatencyTestContentProvider", str);
    }

    public void C2T(String str, String str2, Throwable th) {
        this.A00.A00.softReport(str, str2, th);
    }
}
