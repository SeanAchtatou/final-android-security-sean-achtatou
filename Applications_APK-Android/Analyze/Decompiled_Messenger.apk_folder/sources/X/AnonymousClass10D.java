package X;

import android.app.Service;
import android.content.Context;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.interstitial.triggers.InterstitialTriggerContext;
import com.google.common.base.Preconditions;
import java.util.HashMap;

/* renamed from: X.10D  reason: invalid class name */
public final class AnonymousClass10D {
    public Context A00;
    public AnonymousClass0UN A01;
    public C32901mW A02;
    public final AnonymousClass0WP A03;
    public final C001500z A04;
    public final C32541lv A05;
    public final C04310Tq A06;

    public static InterstitialTrigger A00(AnonymousClass10D r3, InterstitialTrigger interstitialTrigger) {
        HashMap hashMap = new HashMap();
        boolean z = false;
        if (AnonymousClass065.A00(r3.A00, Service.class) != null) {
            z = true;
        }
        hashMap.put(C99084oO.$const$string(186), String.valueOf(z));
        return new InterstitialTrigger(interstitialTrigger, new InterstitialTriggerContext(hashMap));
    }

    public static final AnonymousClass10D A01(AnonymousClass1XY r1) {
        return new AnonymousClass10D(r1);
    }

    public void A02(InterstitialTrigger interstitialTrigger) {
        Preconditions.checkNotNull(this.A00, "Init must be called first");
        ((C16430x3) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AZ7, this.A01)).A02();
        C32821mO A0M = ((AnonymousClass1F1) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BE8, this.A01)).A0M(A00(this, interstitialTrigger));
        if ((A0M instanceof C29362EWq) || (A0M instanceof EWr)) {
            ((C16430x3) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AZ7, this.A01)).A05(AnonymousClass07B.A01, (C22281Ks) A0M, new AnonymousClass69F(this));
        } else {
            this.A05.A02();
        }
    }

    public AnonymousClass10D(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(6, r3);
        this.A03 = C25091Yh.A00(r3);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.AJK, r3);
        this.A04 = AnonymousClass0UU.A05(r3);
        this.A05 = new C32541lv(r3);
    }
}
