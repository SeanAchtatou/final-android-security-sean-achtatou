package X;

import androidx.recyclerview.widget.RecyclerView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1ux  reason: invalid class name and case insensitive filesystem */
public final class C37261ux implements Runnable {
    public static Comparator A04 = new C37271uy();
    public static final ThreadLocal A05 = new ThreadLocal();
    public static final String __redex_internal_original_name = "androidx.recyclerview.widget.GapWorker";
    public long A00;
    public long A01;
    public ArrayList A02 = new ArrayList();
    public ArrayList A03 = new ArrayList();

    private static C33781o8 A00(RecyclerView recyclerView, int i, long j) {
        boolean z;
        int Ah4 = recyclerView.A0H.A01.Ah4();
        int i2 = 0;
        while (true) {
            if (i2 >= Ah4) {
                z = false;
                break;
            }
            C33781o8 A052 = RecyclerView.A05(recyclerView.A0H.A01.Ah1(i2));
            if (A052.A04 == i && !A052.A0D()) {
                z = true;
                break;
            }
            i2++;
        }
        if (z) {
            return null;
        }
        C15740vp r4 = recyclerView.A0w;
        try {
            recyclerView.A0A++;
            C33781o8 A053 = r4.A05(i, false, j);
            if (A053 != null) {
                boolean z2 = true;
                if ((A053.A00 & 1) == 0) {
                    z2 = false;
                }
                if (!z2 || A053.A0D()) {
                    r4.A0B(A053, false);
                } else {
                    r4.A07(A053.A0H);
                }
            }
            return A053;
        } finally {
            recyclerView.A18(false);
        }
    }

    public void run() {
        int i;
        C73833gq r6;
        RecyclerView recyclerView;
        long j;
        WeakReference weakReference;
        RecyclerView recyclerView2;
        C73833gq r0;
        try {
            AnonymousClass06K.A01("RV Prefetch", -966120296);
            if (this.A02.isEmpty()) {
                this.A01 = 0;
                i = 264918373;
            } else {
                int size = this.A02.size();
                long j2 = 0;
                for (int i2 = 0; i2 < size; i2++) {
                    RecyclerView recyclerView3 = (RecyclerView) this.A02.get(i2);
                    if (recyclerView3.getWindowVisibility() == 0) {
                        j2 = Math.max(recyclerView3.getDrawingTime(), j2);
                    }
                }
                if (j2 == 0) {
                    this.A01 = 0;
                    i = 583900495;
                } else {
                    long nanos = TimeUnit.MILLISECONDS.toNanos(j2) + this.A00;
                    int size2 = this.A02.size();
                    int i3 = 0;
                    for (int i4 = 0; i4 < size2; i4++) {
                        RecyclerView recyclerView4 = (RecyclerView) this.A02.get(i4);
                        if (recyclerView4.getWindowVisibility() == 0) {
                            recyclerView4.A0I.A00(recyclerView4, false);
                            i3 += recyclerView4.A0I.A00;
                        }
                    }
                    this.A03.ensureCapacity(i3);
                    int i5 = 0;
                    for (int i6 = 0; i6 < size2; i6++) {
                        RecyclerView recyclerView5 = (RecyclerView) this.A02.get(i6);
                        if (recyclerView5.getWindowVisibility() == 0) {
                            C37121uj r8 = recyclerView5.A0I;
                            int abs = Math.abs(r8.A01) + Math.abs(r8.A02);
                            for (int i7 = 0; i7 < (r8.A00 << 1); i7 += 2) {
                                if (i5 >= this.A03.size()) {
                                    r0 = new C73833gq();
                                    this.A03.add(r0);
                                } else {
                                    r0 = (C73833gq) this.A03.get(i5);
                                }
                                int[] iArr = r8.A03;
                                int i8 = iArr[i7 + 1];
                                boolean z = false;
                                if (i8 <= abs) {
                                    z = true;
                                }
                                r0.A04 = z;
                                r0.A02 = abs;
                                r0.A00 = i8;
                                r0.A03 = recyclerView5;
                                r0.A01 = iArr[i7];
                                i5++;
                            }
                        }
                    }
                    Collections.sort(this.A03, A04);
                    int i9 = 0;
                    while (i9 < this.A03.size() && (recyclerView = (r6 = (C73833gq) this.A03.get(i9)).A03) != null) {
                        if (r6.A04) {
                            j = Long.MAX_VALUE;
                        } else {
                            j = nanos;
                        }
                        C33781o8 A002 = A00(recyclerView, r6.A01, j);
                        if (!(A002 == null || (weakReference = A002.A0C) == null)) {
                            boolean z2 = true;
                            if ((A002.A00 & 1) == 0) {
                                z2 = false;
                            }
                            if (z2 && !A002.A0D() && (recyclerView2 = (RecyclerView) weakReference.get()) != null) {
                                if (recyclerView2.A0T && recyclerView2.A0H.A01.Ah4() != 0) {
                                    recyclerView2.A0g();
                                }
                                C37121uj r82 = recyclerView2.A0I;
                                r82.A00(recyclerView2, true);
                                if (r82.A00 != 0) {
                                    AnonymousClass06K.A01("RV Nested Prefetch", -1882727927);
                                    C15930wD r10 = recyclerView2.A0y;
                                    C20831Dz r1 = recyclerView2.A0J;
                                    r10.A04 = 1;
                                    r10.A03 = r1.ArU();
                                    r10.A08 = false;
                                    r10.A0D = false;
                                    r10.A09 = false;
                                    for (int i10 = 0; i10 < (r82.A00 << 1); i10 += 2) {
                                        A00(recyclerView2, r82.A03[i10], nanos);
                                    }
                                    AnonymousClass06K.A00(-839132815);
                                }
                            }
                        }
                        r6.A04 = false;
                        r6.A02 = 0;
                        r6.A00 = 0;
                        r6.A03 = null;
                        r6.A01 = 0;
                        i9++;
                    }
                    this.A01 = 0;
                    AnonymousClass06K.A00(1577108253);
                    return;
                }
            }
            AnonymousClass06K.A00(i);
        } catch (Throwable th) {
            this.A01 = 0;
            throw th;
        } finally {
        }
    }

    public void A01(RecyclerView recyclerView, int i, int i2) {
        long j;
        if (recyclerView.isAttachedToWindow() && this.A01 == 0) {
            if (RecyclerView.A1C) {
                j = System.nanoTime();
            } else {
                j = 0;
            }
            this.A01 = j;
            recyclerView.post(this);
        }
        C37121uj r0 = recyclerView.A0I;
        r0.A01 = i;
        r0.A02 = i2;
    }
}
