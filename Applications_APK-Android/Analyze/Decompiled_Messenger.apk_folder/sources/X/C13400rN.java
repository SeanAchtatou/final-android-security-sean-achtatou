package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.collect.ImmutableMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0rN  reason: invalid class name and case insensitive filesystem */
public final class C13400rN {
    private static volatile C13400rN A04;
    public ThreadKey A00;
    public Integer A01;
    public String A02;
    public final C13410rO A03;

    public static final C13400rN A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C13400rN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C13400rN(C13410rO.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static void A01(C13400rN r4, String str) {
        C13410rO r3 = r4.A03;
        ImmutableMap.Builder builder = ImmutableMap.builder();
        Integer num = r4.A01;
        if (num != null) {
            builder.put("badge_count", Integer.toString(num.intValue()));
        }
        ThreadKey threadKey = r4.A00;
        if (threadKey != null) {
            builder.put("thread_key", threadKey.toString());
        }
        r4.A01 = null;
        r4.A00 = null;
        r3.A01.A09(r3.A00, str, builder.build());
        r3.A00 = str;
    }

    private C13400rN(C13410rO r2) {
        String A012 = C16040wO.INBOX.A01();
        this.A02 = A012;
        r2.A00 = A012;
        this.A03 = r2;
    }
}
