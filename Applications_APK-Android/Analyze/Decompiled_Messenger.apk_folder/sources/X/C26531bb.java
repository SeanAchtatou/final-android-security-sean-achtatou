package X;

import io.card.payment.BuildConfig;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1bb  reason: invalid class name and case insensitive filesystem */
public final class C26531bb extends C08190ep {
    private static volatile C26531bb A0V;
    public long A00;
    public long A01;
    public long A02;
    public AnonymousClass0UN A03;
    public C73103fV A04;
    public C73113fW A05;
    public C73113fW A06;
    public C73113fW A07;
    public C73113fW A08;
    public C73113fW A09;
    public C73113fW A0A;
    public C73113fW A0B;
    public C73113fW A0C;
    public C73113fW A0D;
    public C73113fW A0E;
    public C73113fW A0F;
    public C73113fW A0G;
    public C73113fW A0H;
    public C73113fW A0I;
    public C73093fU A0J;
    public C73093fU A0K;
    public C73093fU A0L;
    public AtomicInteger A0M;
    public AtomicLong A0N;
    public AtomicLong A0O;
    public boolean A0P = true;
    public int[] A0Q;
    private int[] A0R;
    public final boolean A0S;
    public final boolean A0T;
    private final boolean A0U;

    public static int[] A02(int[]... iArr) {
        int i = 0;
        for (int[] length : iArr) {
            i += length.length;
        }
        int[] iArr2 = new int[i];
        int i2 = 0;
        for (int[] iArr3 : iArr) {
            System.arraycopy(iArr3, 0, iArr2, i2, iArr3.length);
            i2 += iArr3.length;
        }
        return iArr2;
    }

    public static final C26531bb A00(AnonymousClass1XY r4) {
        if (A0V == null) {
            synchronized (C26531bb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0V, r4);
                if (A002 != null) {
                    try {
                        A0V = new C26531bb(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0V;
    }

    public static void A01(C26531bb r12, C04270Tg r13) {
        int i = r13.A02;
        if (r13.A0M == 2) {
            TimeUnit timeUnit = TimeUnit.NANOSECONDS;
            int millis = (int) timeUnit.toMillis(r13.A09);
            if (i == 24444929) {
                String A012 = r13.A01("touch_phase");
                if (A012 != null) {
                    boolean equals = A012.equals("touch_down");
                    boolean equals2 = A012.equals("touch_up");
                    if (equals) {
                        r12.A0D.A03(millis);
                    } else if (equals2) {
                        r12.A0E.A03(millis);
                    }
                    long millis2 = timeUnit.toMillis(r13.A0C);
                    long millis3 = timeUnit.toMillis(r13.A0B);
                    if (r12.A0T) {
                        long j = r12.A02;
                        if (j >= millis2 && j <= millis3) {
                            if (equals) {
                                r12.A09.A03(millis);
                            } else if (equals2) {
                                r12.A0A.A03(millis);
                            }
                        }
                    }
                    r12.A0P = true;
                    long j2 = r12.A01;
                    r12.A07.A03((int) (millis3 - j2));
                    r12.A01 = Math.max(millis3, j2);
                }
            } else if (i != 44826629) {
                switch (i) {
                    case 44826634:
                    case 44826635:
                    case 44826636:
                    case 44826637:
                        r12.A08.A03(millis);
                        return;
                    default:
                        r12.A0F.A03(millis);
                        return;
                }
            } else {
                r12.A0G.A03(millis);
                r12.A0H.A03(millis + r12.A0M.getAndSet(0));
            }
        } else if (i == 24444929) {
            r12.A0P = true;
        }
    }

    public AnonymousClass0Ti AsW() {
        if (!this.A0S) {
            return AnonymousClass0Ti.A04;
        }
        if (this.A0R == null) {
            int[] iArr = {44826629, 24444929};
            if (this.A0U) {
                iArr = A02(iArr, new int[]{44826634, 44826635, 44826637, 44826636});
            }
            if (this.A0T) {
                iArr = A02(iArr, this.A0Q);
            }
            AnonymousClass0HJ r4 = new AnonymousClass0HJ(r6);
            for (String parseInt : ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A03)).B4E(846215932281077L, BuildConfig.FLAVOR).split(",")) {
                try {
                    r4.A01(Integer.parseInt(parseInt));
                } catch (NumberFormatException e) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A03)).softReport("StallImpactTracker", "Parse error for QPL ids", e);
                }
            }
            for (int A012 : iArr) {
                r4.A01(A012);
            }
            this.A0R = r4.A02();
        }
        int[] iArr2 = {44826629, 44826626, 44826627, 44826625, 3997722};
        if (this.A0U) {
            iArr2 = A02(iArr2, new int[]{44826634});
        }
        return new AnonymousClass0Ti(this.A0R, iArr2, null);
    }

    private C26531bb(AnonymousClass1XY r6) {
        AnonymousClass0UN r1 = new AnonymousClass0UN(4, r6);
        this.A03 = r1;
        this.A0U = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r1)).Aeo(283265978927378L, false);
        this.A0T = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A03)).Aeo(283265978992915L, false);
        boolean Aeo = ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A03)).Aeo(283265978337547L, false);
        this.A0S = Aeo;
        if (Aeo) {
            int i = AnonymousClass1Y3.BBa;
            this.A0J = new C73093fU((AnonymousClass069) AnonymousClass1XX.A02(1, i, this.A03));
            this.A0K = new C73093fU((AnonymousClass069) AnonymousClass1XX.A02(1, i, this.A03));
            this.A0L = new C73093fU((AnonymousClass069) AnonymousClass1XX.A02(1, i, this.A03));
            this.A04 = new C73103fV();
            int[] iArr = C73123fX.A01;
            this.A0C = new C73113fW(iArr);
            this.A06 = new C73113fW(iArr);
            this.A0B = new C73113fW(iArr);
            this.A0I = new C73113fW(iArr);
            this.A0G = new C73113fW(iArr);
            this.A0H = new C73113fW(iArr);
            this.A0D = new C73113fW(iArr);
            this.A09 = new C73113fW(iArr);
            this.A0E = new C73113fW(iArr);
            this.A0A = new C73113fW(iArr);
            this.A07 = new C73113fW(iArr);
            this.A05 = new C73113fW(iArr);
            int[] iArr2 = C73123fX.A00;
            this.A0F = new C73113fW(iArr2);
            this.A08 = new C73113fW(iArr2);
            this.A0M = new AtomicInteger();
            this.A0O = new AtomicLong(((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, this.A03)).now());
            this.A0N = new AtomicLong();
            if (this.A0T) {
                this.A0Q = new int[]{25952257, 44826634, 44826637, 44826636, 44826635};
            }
        }
    }
}
