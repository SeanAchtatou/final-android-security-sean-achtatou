package X;

import android.os.Build;
import android.widget.TextView;

/* renamed from: X.09x  reason: invalid class name and case insensitive filesystem */
public final class C013609x {
    private static C22311Kv A00(TextView textView) {
        if (Build.VERSION.SDK_INT >= 17) {
            return A01(textView);
        }
        return A02(textView);
    }

    private static C22311Kv A01(TextView textView) {
        int textDirection = textView.getTextDirection();
        if (textDirection != 1) {
            if (textDirection == 2) {
                return C22181Kf.A00;
            }
            if (textDirection == 3) {
                return C22181Kf.A04;
            }
            if (textDirection == 4) {
                return C22181Kf.A05;
            }
            if (textDirection != 5) {
                return C22181Kf.A01;
            }
            return C22181Kf.A03;
        } else if (textView.getLayoutDirection() == 1) {
            return C22181Kf.A02;
        } else {
            return C22181Kf.A01;
        }
    }

    private static C22311Kv A02(TextView textView) {
        if (C15320v6.getLayoutDirection(textView) == 1) {
            return C22181Kf.A02;
        }
        return C22181Kf.A01;
    }

    public static boolean A03(TextView textView, CharSequence charSequence, int i) {
        return A00(textView).BGQ(charSequence, 0, i);
    }
}
