package X;

import android.content.Context;
import android.os.Build;
import android.util.SparseArray;

/* renamed from: X.0eh  reason: invalid class name and case insensitive filesystem */
public final class C08110eh {
    public String A00 = "UNKNOWN";
    public String A01 = C26281bC.A00().A00;
    public String A02 = Build.BRAND;
    public String A03 = Build.MODEL;
    public String A04 = String.valueOf(Build.VERSION.SDK_INT);
    public final SparseArray A05;

    public C08110eh(Context context, SparseArray sparseArray) {
        String str;
        this.A05 = sparseArray;
        try {
            str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception unused) {
            str = "UNKNOWN";
        }
        this.A00 = str;
    }
}
