package X;

import com.facebook.common.util.TriState;

/* renamed from: X.162  reason: invalid class name */
public final class AnonymousClass162 {
    public int A00 = -1;
    public TriState A01;
    public TriState A02;
    public TriState A03;
    public TriState A04;
    public TriState A05;
    public TriState A06;
    public AnonymousClass0u3 A07;

    public AnonymousClass162() {
        TriState triState = TriState.UNSET;
        this.A02 = triState;
        this.A04 = triState;
        this.A03 = triState;
        this.A01 = triState;
        this.A05 = triState;
        this.A06 = triState;
    }
}
