package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.1ch  reason: invalid class name and case insensitive filesystem */
public final class C27131ch implements AnonymousClass06U {
    public final /* synthetic */ C27101ce A00;

    public C27131ch(C27101ce r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(1008159431);
        this.A00.A04 = false;
        C27101ce.A01(this.A00);
        C49132bh r0 = this.A00.A00;
        if (r0 != null) {
            C198369Up r1 = r0.A00;
            if (!r1.A0W) {
                C198369Up.A07(r1);
            }
        }
        AnonymousClass09Y.A01(-795767997, A002);
    }
}
