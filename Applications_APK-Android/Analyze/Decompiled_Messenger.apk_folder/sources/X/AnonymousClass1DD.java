package X;

import com.facebook.payments.history.model.SimplePaymentTransactions;

/* renamed from: X.1DD  reason: invalid class name */
public final class AnonymousClass1DD extends C06020ai {
    public final /* synthetic */ Long A00;
    public final /* synthetic */ SimplePaymentTransactions A01;
    public final /* synthetic */ C54822n5 A02;
    public final /* synthetic */ C23694Bka A03;

    public AnonymousClass1DD(C54822n5 r1, C23694Bka bka, SimplePaymentTransactions simplePaymentTransactions, Long l) {
        this.A02 = r1;
        this.A03 = bka;
        this.A01 = simplePaymentTransactions;
        this.A00 = l;
    }
}
