package X;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;
import androidx.fragment.app.FragmentActivity;
import com.facebook.common.build.BuildConstants;
import com.facebook.inject.InjectorModule;
import java.util.Locale;

@InjectorModule
/* renamed from: X.0Ux  reason: invalid class name and case insensitive filesystem */
public final class C04490Ux extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static final Object A02 = new Object();
    private static final Object A03 = new Object();
    private static final Object A04 = new Object();
    private static volatile PackageInfo A05;
    private static volatile PackageManager A06;
    private static volatile Resources A07;
    private static volatile AnonymousClass0V2 A08;
    private static volatile C04480Uv A09;
    private static volatile C53432km A0A;
    private static volatile String A0B;

    public static final PackageInfo A0I(AnonymousClass1XY r8) {
        if (A05 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        PackageInfo packageInfo = A0J(applicationInjector).getPackageInfo(AnonymousClass1YA.A00(applicationInjector).getPackageName(), 0);
                        int i = packageInfo.versionCode;
                        if (i != BuildConstants.A00()) {
                            C010708t.A0K("AndroidModule", String.format(Locale.US, "Android PackageManager returned version code: %d, apk version code is: %d", Integer.valueOf(i), Integer.valueOf(BuildConstants.A00())));
                        }
                        A05 = packageInfo;
                        A002.A01();
                    } catch (PackageManager.NameNotFoundException e) {
                        throw new RuntimeException(e);
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final PackageManager A0K(AnonymousClass1XY r3) {
        if (A06 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r3);
                if (A002 != null) {
                    try {
                        A06 = AnonymousClass1YA.A00(r3.getApplicationInjector()).getApplicationContext().getPackageManager();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static final Resources A0M(AnonymousClass1XY r3) {
        if (A07 == null) {
            synchronized (A02) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r3);
                if (A002 != null) {
                    try {
                        A07 = AnonymousClass1YA.A00(r3.getApplicationInjector()).getApplicationContext().getResources();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static final MediaPlayer A0S() {
        return new MediaPlayer();
    }

    public static final Handler A0X() {
        return new Handler();
    }

    public static final AnonymousClass0V2 A0m(AnonymousClass1XY r3) {
        if (A08 == null) {
            synchronized (A03) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r3);
                if (A002 != null) {
                    try {
                        A08 = AnonymousClass0V2.A00(AnonymousClass1YA.A00(r3.getApplicationInjector()).getApplicationContext());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static final C04480Uv A0o(AnonymousClass1XY r3) {
        if (A09 == null) {
            synchronized (C04480Uv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r3);
                if (A002 != null) {
                    try {
                        A09 = C04480Uv.A00(AnonymousClass1YA.A00(r3.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static final C53432km A0p(AnonymousClass1XY r3) {
        if (A0A == null) {
            synchronized (C53432km.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0A = new C53432km();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static final Integer A0q() {
        return Integer.valueOf(Build.VERSION.SDK_INT);
    }

    public static final Integer A0r() {
        return Integer.valueOf(Build.VERSION.SDK_INT);
    }

    public static final String A0u(AnonymousClass1XY r3) {
        if (A0B == null) {
            synchronized (A04) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0B, r3);
                if (A002 != null) {
                    try {
                        A0B = AnonymousClass1YA.A00(r3.getApplicationInjector()).getPackageName();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0B;
    }

    public static final AccountManager A00(AnonymousClass1XY r1) {
        return (AccountManager) AnonymousClass1YA.A00(r1).getSystemService("account");
    }

    public static final AccountManager A01(AnonymousClass1XY r1) {
        return (AccountManager) AnonymousClass1YA.A00(r1).getSystemService("account");
    }

    public static final Activity A02(AnonymousClass1XY r1) {
        return (Activity) AnonymousClass065.A00(AnonymousClass1YA.A00(r1), Activity.class);
    }

    public static final Activity A03(AnonymousClass1XY r1) {
        return (Activity) AnonymousClass065.A00(AnonymousClass1YA.A00(r1), Activity.class);
    }

    public static final ActivityManager A04(AnonymousClass1XY r1) {
        return (ActivityManager) AnonymousClass1YA.A00(r1).getSystemService("activity");
    }

    public static final ActivityManager A05(AnonymousClass1XY r1) {
        return (ActivityManager) AnonymousClass1YA.A00(r1).getSystemService("activity");
    }

    public static final AlarmManager A06(AnonymousClass1XY r1) {
        return (AlarmManager) AnonymousClass1YA.A00(r1).getSystemService("alarm");
    }

    public static final KeyguardManager A07(AnonymousClass1XY r1) {
        return (KeyguardManager) AnonymousClass1YA.A00(r1).getSystemService("keyguard");
    }

    public static final KeyguardManager A08(AnonymousClass1XY r1) {
        return (KeyguardManager) AnonymousClass1YA.A00(r1).getSystemService("keyguard");
    }

    public static final NotificationManager A09(AnonymousClass1XY r1) {
        return (NotificationManager) AnonymousClass1YA.A00(r1).getSystemService("notification");
    }

    public static final NotificationManager A0A(AnonymousClass1XY r1) {
        return (NotificationManager) AnonymousClass1YA.A00(r1).getSystemService("notification");
    }

    public static final Service A0B(AnonymousClass1XY r1) {
        Context A002 = AnonymousClass1YA.A00(r1);
        if (A002 instanceof Service) {
            return (Service) A002;
        }
        return null;
    }

    public static final ClipboardManager A0C(AnonymousClass1XY r1) {
        return (ClipboardManager) AnonymousClass1YA.A00(r1).getSystemService("clipboard");
    }

    public static final ClipboardManager A0D(AnonymousClass1XY r1) {
        return (ClipboardManager) AnonymousClass1YA.A00(r1).getSystemService("clipboard");
    }

    public static final ContentResolver A0E(AnonymousClass1XY r0) {
        return AnonymousClass1YA.A00(r0).getContentResolver();
    }

    public static final ContentResolver A0F(AnonymousClass1XY r0) {
        return AnonymousClass1YA.A00(r0).getContentResolver();
    }

    public static final ApplicationInfo A0G(AnonymousClass1XY r0) {
        return AnonymousClass1YA.A00(r0).getApplicationInfo();
    }

    public static final ApplicationInfo A0H(AnonymousClass1XY r0) {
        return AnonymousClass1YA.A00(r0).getApplicationInfo();
    }

    public static final PackageManager A0J(AnonymousClass1XY r0) {
        return A0K(r0);
    }

    public static final Resources A0L(AnonymousClass1XY r0) {
        return A0M(r0);
    }

    public static final SensorManager A0N(AnonymousClass1XY r1) {
        return (SensorManager) AnonymousClass1YA.A00(r1).getSystemService("sensor");
    }

    public static final LocationManager A0O(AnonymousClass1XY r1) {
        return (LocationManager) AnonymousClass1YA.A00(r1).getSystemService("location");
    }

    public static final LocationManager A0P(AnonymousClass1XY r1) {
        return (LocationManager) AnonymousClass1YA.A00(r1).getSystemService("location");
    }

    public static final AudioManager A0Q(AnonymousClass1XY r1) {
        return (AudioManager) AnonymousClass1YA.A00(r1).getSystemService("audio");
    }

    public static final AudioManager A0R(AnonymousClass1XY r1) {
        return (AudioManager) AnonymousClass1YA.A00(r1).getSystemService("audio");
    }

    public static final ConnectivityManager A0T(AnonymousClass1XY r1) {
        try {
            return (ConnectivityManager) AnonymousClass1YA.A00(r1).getSystemService("connectivity");
        } catch (Exception unused) {
            return null;
        }
    }

    public static final NetworkInfo A0U(AnonymousClass1XY r1) {
        ConnectivityManager connectivityManager;
        try {
            connectivityManager = (ConnectivityManager) AnonymousClass1YA.A00(r1).getSystemService("connectivity");
        } catch (Exception unused) {
            connectivityManager = null;
        }
        if (connectivityManager == null) {
            return null;
        }
        try {
            return connectivityManager.getActiveNetworkInfo();
        } catch (Exception unused2) {
            return null;
        }
    }

    public static final WifiManager A0V(AnonymousClass1XY r1) {
        return (WifiManager) AnonymousClass1YA.A00(r1).getApplicationContext().getSystemService("wifi");
    }

    public static final WifiManager A0W(AnonymousClass1XY r1) {
        return (WifiManager) AnonymousClass1YA.A00(r1).getApplicationContext().getSystemService("wifi");
    }

    public static final PowerManager A0Y(AnonymousClass1XY r1) {
        return (PowerManager) AnonymousClass1YA.A00(r1).getSystemService("power");
    }

    public static final PowerManager A0Z(AnonymousClass1XY r1) {
        return (PowerManager) AnonymousClass1YA.A00(r1).getSystemService("power");
    }

    public static final Vibrator A0a(AnonymousClass1XY r1) {
        return (Vibrator) AnonymousClass1YA.A00(r1).getSystemService("vibrator");
    }

    public static final Vibrator A0b(AnonymousClass1XY r1) {
        return (Vibrator) AnonymousClass1YA.A00(r1).getSystemService("vibrator");
    }

    public static final TelephonyManager A0c(AnonymousClass1XY r1) {
        return (TelephonyManager) AnonymousClass1YA.A00(r1).getSystemService("phone");
    }

    public static final TelephonyManager A0d(AnonymousClass1XY r1) {
        return (TelephonyManager) AnonymousClass1YA.A00(r1).getSystemService("phone");
    }

    public static final LayoutInflater A0e(AnonymousClass1XY r1) {
        return (LayoutInflater) AnonymousClass1YA.A00(r1).getSystemService("layout_inflater");
    }

    public static final LayoutInflater A0f(AnonymousClass1XY r1) {
        return (LayoutInflater) AnonymousClass1YA.A00(r1).getSystemService("layout_inflater");
    }

    public static final WindowManager A0g(AnonymousClass1XY r1) {
        return (WindowManager) AnonymousClass1YA.A00(r1).getSystemService("window");
    }

    public static final WindowManager A0h(AnonymousClass1XY r1) {
        return (WindowManager) AnonymousClass1YA.A00(r1).getSystemService("window");
    }

    public static final AccessibilityManager A0i(AnonymousClass1XY r1) {
        return (AccessibilityManager) AnonymousClass1YA.A00(r1).getSystemService("accessibility");
    }

    public static final InputMethodManager A0j(AnonymousClass1XY r1) {
        return (InputMethodManager) AnonymousClass1YA.A00(r1).getSystemService("input_method");
    }

    public static final InputMethodManager A0k(AnonymousClass1XY r1) {
        return (InputMethodManager) AnonymousClass1YA.A00(r1).getSystemService("input_method");
    }

    public static final FragmentActivity A0l(AnonymousClass1XY r1) {
        return (FragmentActivity) AnonymousClass065.A00(AnonymousClass1YA.A00(r1), FragmentActivity.class);
    }

    public static final C04480Uv A0n(AnonymousClass1XY r0) {
        return A0o(r0);
    }

    public static final Runtime A0s() {
        return Runtime.getRuntime();
    }

    public static final String A0t(AnonymousClass1XY r0) {
        return A0u(r0);
    }
}
