package X;

import com.facebook.payments.paymentmethods.cardform.CardFormParams;
import com.facebook.payments.paymentmethods.model.FbPaymentCard;

/* renamed from: X.1D0  reason: invalid class name */
public final class AnonymousClass1D0 extends C06020ai {
    public final /* synthetic */ CardFormParams A00;
    public final /* synthetic */ C50742eX A01;
    public final /* synthetic */ FbPaymentCard A02;

    public AnonymousClass1D0(C50742eX r1, CardFormParams cardFormParams, FbPaymentCard fbPaymentCard) {
        this.A01 = r1;
        this.A00 = cardFormParams;
        this.A02 = fbPaymentCard;
    }
}
