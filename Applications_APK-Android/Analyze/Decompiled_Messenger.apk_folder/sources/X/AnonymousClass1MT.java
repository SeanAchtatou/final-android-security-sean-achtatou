package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/* renamed from: X.1MT  reason: invalid class name */
public final class AnonymousClass1MT {
    private AnonymousClass0UN A00;

    public static final AnonymousClass1MT A00(AnonymousClass1XY r1) {
        return new AnonymousClass1MT(r1);
    }

    public static final AnonymousClass1MT A01(AnonymousClass1XY r1) {
        return new AnonymousClass1MT(r1);
    }

    public int A02(C34891qL r4, Integer num) {
        return ((C22001Jn) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BIU, this.A00)).A02(r4, num);
    }

    public int A03(C29631gj r4, Integer num) {
        return ((AnonymousClass1MV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AeL, this.A00)).A02(r4, num);
    }

    public Drawable A04(C34891qL r5, Integer num) {
        int i = AnonymousClass1Y3.AEg;
        AnonymousClass0UN r3 = this.A00;
        return ((Resources) AnonymousClass1XX.A02(0, i, r3)).getDrawable(((C22001Jn) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BIU, r3)).A02(r5, num));
    }

    public Drawable A05(C34891qL r5, Integer num, int i) {
        int i2 = AnonymousClass1Y3.AOz;
        AnonymousClass0UN r3 = this.A00;
        return ((AnonymousClass1MX) AnonymousClass1XX.A02(1, i2, r3)).A04(((C22001Jn) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BIU, r3)).A02(r5, num), i);
    }

    public Drawable A06(C29631gj r5, Integer num, int i) {
        int i2 = AnonymousClass1Y3.AOz;
        AnonymousClass0UN r3 = this.A00;
        return ((AnonymousClass1MX) AnonymousClass1XX.A02(1, i2, r3)).A04(((AnonymousClass1MV) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AeL, r3)).A02(r5, num), i);
    }

    private AnonymousClass1MT(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
