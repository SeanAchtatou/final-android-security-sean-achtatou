package X;

import android.text.TextUtils;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1j6  reason: invalid class name and case insensitive filesystem */
public final class C31071j6 {
    public int A00 = 3;
    public File A01;
    public Map A02;
    public final String A03;

    public void A00(C30971iw r3) {
        if (r3 != null) {
            this.A02.put(r3.A02(), r3);
        }
    }

    public C31071j6(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.A03 = str;
            this.A02 = new HashMap();
            return;
        }
        throw new IllegalArgumentException("feature should always be set");
    }
}
