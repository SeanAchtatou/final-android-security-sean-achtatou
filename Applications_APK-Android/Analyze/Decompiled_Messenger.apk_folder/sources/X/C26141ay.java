package X;

import android.util.SparseArray;

/* renamed from: X.1ay  reason: invalid class name and case insensitive filesystem */
public abstract class C26141ay {
    public C26191b3 A00;
    public final SparseArray A01 = new SparseArray();
    public final C08080ee A02 = C08080ee.A01;
    public final C26161b0 A03 = C26161b0.A00();

    public void A07(int i, int i2) {
        C08040ea A04 = A04(this, i);
        C26501bY r1 = new C08310f2().A00;
        r1.A01 = i2;
        A04.A01(new C07210ct(3, i, r1));
    }

    public static C08040ea A04(C26141ay r3, int i) {
        C08040ea r2 = (C08040ea) r3.A01.get(i);
        if (r2 != null) {
            return r2;
        }
        C08040ea r22 = new C08040ea(i, r3.A00, r3.A03);
        r3.A01.put(i, r22);
        return r22;
    }

    public C26141ay() {
        if (C26191b3.A04 == null) {
            C26191b3.A04 = new C26191b3(null);
        }
        this.A00 = C26191b3.A04;
    }

    public static void A05(C26141ay r2, int i, int i2) {
        A04(r2, i2).A01(new C07210ct(i, i2, null));
    }

    public static void A06(C26141ay r2, int i, int i2, int i3) {
        C08040ea A04 = A04(r2, i2);
        C26501bY r1 = new C08310f2().A00;
        r1.A03 = i3;
        A04.A01(new C07210ct(i, i2, r1));
    }

    public void A08(int i, int i2) {
        C08040ea A04 = A04(this, i);
        C07210ct r0 = (C07210ct) A04.A03.get(5);
        if (r0 == null) {
            C26501bY r2 = new C08310f2().A00;
            r2.A03 = i2;
            A04.A01(new C07210ct(5, A04.A02, r2));
            return;
        }
        C26501bY r1 = r0.A05;
        if (r1 != null) {
            r1.A03 = i2 | r1.A03;
        }
    }
}
