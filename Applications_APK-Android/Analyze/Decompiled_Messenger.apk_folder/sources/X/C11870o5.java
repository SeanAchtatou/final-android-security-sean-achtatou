package X;

import java.lang.reflect.Type;

/* renamed from: X.0o5  reason: invalid class name and case insensitive filesystem */
public final class C11870o5 extends C11880o6 {
    public C10030jR modifyType(C10030jR r10, Type type, C28311eb r12, C10300js r13) {
        C10030jR r2 = r10;
        if (!AnonymousClass0V0.class.isAssignableFrom(r10._class)) {
            return r2;
        }
        C10030jR containedType = r10.containedType(0);
        C10030jR containedType2 = r10.containedType(1);
        if (containedType == null) {
            containedType = r13._constructType(Object.class, null);
        }
        if (containedType2 == null) {
            containedType2 = r13._constructType(Object.class, null);
        }
        return new C21991Jm(r10._class, containedType, containedType2, null, null, false);
    }
}
