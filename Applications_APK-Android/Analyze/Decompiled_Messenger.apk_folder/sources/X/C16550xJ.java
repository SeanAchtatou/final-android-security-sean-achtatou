package X;

import com.google.common.base.Objects;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0xJ  reason: invalid class name and case insensitive filesystem */
public final class C16550xJ extends Enum {
    private static final /* synthetic */ C16550xJ[] A00;
    public static final C16550xJ A01;
    public static final C16550xJ A02;
    public static final C16550xJ A03;
    public static final C16550xJ A04;
    public static final C16550xJ A05;
    public static final C16550xJ A06;
    public static final C16550xJ A07;
    public static final C16550xJ A08;
    public static final C16550xJ A09;
    public static final C16550xJ A0A;
    public static final C16550xJ A0B;
    public static final C16550xJ A0C;
    public static final C16550xJ A0D;
    public static final C16550xJ A0E;
    public static final C16550xJ A0F;
    public final String dbValue;

    static {
        C16550xJ r12 = new C16550xJ("ACCOUNT_LINK", 0, "ACCOUNT_LINK");
        A01 = r12;
        String $const$string = AnonymousClass24B.$const$string(441);
        C16550xJ r11 = new C16550xJ($const$string, 1, $const$string);
        A02 = r11;
        C16550xJ r10 = new C16550xJ("EXTENSIBLE_SHARE", 2, "EXTENSIBLE_SHARE");
        A03 = r10;
        C16550xJ r9 = new C16550xJ("FACEBOOK_REPORT_A_PROBLEM", 3, "FACEBOOK_REPORT_A_PROBLEM");
        A04 = r9;
        C16550xJ r0 = new C16550xJ("MANAGE_MESSAGES", 4, "MANAGE_MESSAGES");
        C16550xJ r13 = new C16550xJ("NAVIGATION", 5, "NAVIGATION");
        A05 = r13;
        C16550xJ r8 = new C16550xJ("OPEN_DIRECT_SEND_VIEW", 6, "OPEN_DIRECT_SEND_VIEW");
        A07 = r8;
        C16550xJ r7 = new C16550xJ("OPEN_NATIVE", 7, "OPEN_NATIVE");
        A09 = r7;
        C16550xJ r02 = new C16550xJ("OPEN_REACT_NATIVE_MINI_APP", 8, "OPEN_REACT_NATIVE_MINI_APP");
        C16550xJ r14 = new C16550xJ("OPEN_URL", 9, "OPEN_URL");
        A0C = r14;
        C16550xJ r03 = new C16550xJ("OPEN_CANCEL_RIDE_MUTATION", 10, "OPEN_CANCEL_RIDE_MUTATION");
        C16550xJ r15 = new C16550xJ("OPEN_BRANDED_CAMERA", 11, "OPEN_BRANDED_CAMERA");
        A06 = r15;
        C16550xJ r6 = new C16550xJ("PAYMENT", 12, "PAYMENT");
        A0D = r6;
        C16550xJ r5 = new C16550xJ("POSTBACK", 13, "POSTBACK");
        A0E = r5;
        C16550xJ r4 = new C16550xJ("SHARE", 14, "SHARE");
        A0F = r4;
        C16550xJ r21 = new C16550xJ("SUBSCRIPTION_PRESELECT", 15, "SUBSCRIPTION_PRESELECT");
        C16550xJ r3 = new C16550xJ("OPEN_THREAD", 16, "OPEN_THREAD");
        A0B = r3;
        C16550xJ r2 = new C16550xJ("OPEN_PAGE_ABOUT", 17, "OPEN_PAGE_ABOUT");
        A0A = r2;
        C16550xJ r212 = new C16550xJ("OPEN_MARKETPLACE_PROFILE_REPORT", 18, "OPEN_MARKETPLACE_PROFILE_REPORT");
        A08 = r212;
        C16550xJ r34 = r212;
        C16550xJ r213 = r13;
        C16550xJ r22 = r8;
        C16550xJ r23 = r7;
        C16550xJ r24 = r02;
        C16550xJ r16 = r12;
        C16550xJ r17 = r11;
        C16550xJ r18 = r10;
        C16550xJ r19 = r9;
        A00 = new C16550xJ[]{r16, r17, r18, r19, r0, r213, r22, r23, r24, r14, r03, r15, r6, r5, r4, r21, r3, r2, r34};
    }

    public static C16550xJ valueOf(String str) {
        return (C16550xJ) Enum.valueOf(C16550xJ.class, str);
    }

    public static C16550xJ[] values() {
        return (C16550xJ[]) A00.clone();
    }

    private C16550xJ(String str, int i, String str2) {
        this.dbValue = str2;
    }

    public static C16550xJ A00(String str) {
        for (C16550xJ r1 : values()) {
            if (Objects.equal(r1.dbValue, str)) {
                return r1;
            }
        }
        return null;
    }
}
