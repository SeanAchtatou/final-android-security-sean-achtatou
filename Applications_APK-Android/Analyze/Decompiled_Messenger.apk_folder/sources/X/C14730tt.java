package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.google.common.base.Optional;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tt  reason: invalid class name and case insensitive filesystem */
public final class C14730tt extends C28161eM {
    private static volatile C14730tt A03;
    public List A00 = new ArrayList();
    public final AnonymousClass0US A01;
    public final AnonymousClass13w A02;

    public static final C14730tt A00(AnonymousClass1XY r29) {
        if (A03 == null) {
            synchronized (C14730tt.class) {
                AnonymousClass1XY r3 = r29;
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r3.getApplicationInjector();
                        A03 = new C14730tt(applicationInjector, C04490Ux.A0L(applicationInjector), new AnonymousClass0Z9(applicationInjector, AnonymousClass1YA.A00(applicationInjector)), AnonymousClass1ET.A00(applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AmD, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AR0, applicationInjector), C04750Wa.A03(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.BCW, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.Aym, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AKb, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.A7S, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.A9x, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.B6q, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.A1s, applicationInjector), AnonymousClass0WT.A00(applicationInjector), C04750Wa.A03(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AJ5, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AQf, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.Az1, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.BBU, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.BPg, applicationInjector), AnonymousClass1ZD.A00(applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AcD, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AC0, applicationInjector), C25481Zu.A00(applicationInjector), new C28171eN(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static void A01(C14730tt r2, String str, long j) {
        r2.A0X("dialtone_upgrade_dialog", true);
        if (str == "free_messenger_gif_interstitial") {
            r2.A00.add(Long.valueOf(j));
            r2.A0f(false);
        }
    }

    public boolean A0g() {
        if (super.A0g()) {
            return true;
        }
        if (!((C05720aD) this.A0K.get()).A07("messenger_enable_dialtone_on_cold_start")) {
            return false;
        }
        Optional optional = this.A00;
        if (!optional.isPresent() || !((C14710tr) optional.get()).A05) {
            return false;
        }
        this.A07.A05();
        C11670nb r2 = new C11670nb("dialtone_explicitly_entered");
        r2.A0D("pigeon_reserved_keyword_module", "dialtone");
        r2.A0D("ref", "messenger_cold_start");
        r2.A0D("carrier_id", ((AnonymousClass155) this.A0L.get()).A0A(AnonymousClass157.NORMAL));
        ((DeprecatedAnalyticsLogger) this.A09.get()).A09(r2);
        return true;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C14730tt(X.AnonymousClass1XY r24, android.content.res.Resources r25, X.AnonymousClass0Z9 r26, X.AnonymousClass1ET r27, X.AnonymousClass0US r28, X.AnonymousClass0US r29, X.AnonymousClass0US r30, X.C04310Tq r31, X.AnonymousClass0US r32, X.AnonymousClass0US r33, X.AnonymousClass0US r34, X.AnonymousClass0US r35, X.AnonymousClass0US r36, X.AnonymousClass0US r37, X.C25051Yd r38, X.AnonymousClass0US r39, X.AnonymousClass0US r40, X.AnonymousClass0US r41, X.AnonymousClass0US r42, X.AnonymousClass0US r43, X.AnonymousClass0US r44, X.AnonymousClass1ZE r45, X.AnonymousClass0US r46, X.AnonymousClass0US r47, X.AnonymousClass0XN r48, X.C28171eN r49) {
        /*
            r23 = this;
            r0 = r23
            r16 = r41
            r15 = r40
            r14 = r39
            r13 = r38
            r12 = r36
            r11 = r35
            r10 = r34
            r3 = r27
            r18 = r45
            r4 = r28
            r19 = r46
            r5 = r29
            r20 = r47
            r6 = r30
            r22 = r49
            r8 = r32
            r21 = r48
            r7 = r31
            r17 = r42
            r1 = r25
            r2 = r26
            r9 = r33
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22)
            X.13w r0 = X.AnonymousClass13w.A00(r24)
            r1 = r23
            r1.A02 = r0
            r0 = r44
            r1.A01 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14730tt.<init>(X.1XY, android.content.res.Resources, X.0Z9, X.1ET, X.0US, X.0US, X.0US, X.0Tq, X.0US, X.0US, X.0US, X.0US, X.0US, X.0US, X.1Yd, X.0US, X.0US, X.0US, X.0US, X.0US, X.0US, X.1ZE, X.0US, X.0US, X.0XN, X.1eN):void");
    }
}
