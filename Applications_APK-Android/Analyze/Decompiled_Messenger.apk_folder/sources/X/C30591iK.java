package X;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.Rect;

/* renamed from: X.1iK  reason: invalid class name and case insensitive filesystem */
public interface C30591iK {
    AnonymousClass1PS decodeFromEncodedImageWithColorSpace(AnonymousClass1NY r1, Bitmap.Config config, Rect rect, ColorSpace colorSpace);

    AnonymousClass1PS decodeJPEGFromEncodedImage(AnonymousClass1NY r1, Bitmap.Config config, Rect rect, int i);

    AnonymousClass1PS decodeJPEGFromEncodedImageWithColorSpace(AnonymousClass1NY r1, Bitmap.Config config, Rect rect, int i, ColorSpace colorSpace);
}
