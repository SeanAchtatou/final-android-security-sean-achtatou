package X;

import com.facebook.graphql.enums.GraphQLLivingRoomEntrySource;
import com.facebook.messaging.cowatch.launcher.parameters.CoWatchLauncherParams;
import com.facebook.messaging.cowatch.launcher.parameters.VideoInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;

/* renamed from: X.0r0  reason: invalid class name */
public final class AnonymousClass0r0 {
    public GraphQLLivingRoomEntrySource A00;
    public VideoInfo A01;
    public ThreadKey A02;
    public String A03;
    public boolean A04;

    public AnonymousClass0r0() {
    }

    public AnonymousClass0r0(CoWatchLauncherParams coWatchLauncherParams) {
        C28931fb.A05(coWatchLauncherParams);
        if (coWatchLauncherParams instanceof CoWatchLauncherParams) {
            this.A00 = coWatchLauncherParams.A00;
            this.A03 = coWatchLauncherParams.A03;
            this.A04 = coWatchLauncherParams.A04;
            this.A02 = coWatchLauncherParams.A02;
        } else {
            this.A00 = coWatchLauncherParams.A00;
            this.A03 = coWatchLauncherParams.A03;
            this.A04 = coWatchLauncherParams.A04;
            ThreadKey threadKey = coWatchLauncherParams.A02;
            this.A02 = threadKey;
            C28931fb.A06(threadKey, "threadKey");
        }
        this.A01 = coWatchLauncherParams.A01;
    }
}
