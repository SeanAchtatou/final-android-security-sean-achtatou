package X;

import android.os.Handler;
import android.view.Choreographer;

/* renamed from: X.1h3  reason: invalid class name and case insensitive filesystem */
public abstract class C29831h3 {
    public Runnable A00;
    private Choreographer.FrameCallback A01;

    public void A06(long j) {
        int i = AnonymousClass1Y3.Amu;
        C29791gz r3 = ((C29821h2) this).A00;
        AnonymousClass00S.A03((Handler) AnonymousClass1XX.A02(0, i, r3.A00), r3.A02, 584698192);
    }

    public Choreographer.FrameCallback A05() {
        if (this.A01 == null) {
            this.A01 = new C80763t3(this);
        }
        return this.A01;
    }
}
