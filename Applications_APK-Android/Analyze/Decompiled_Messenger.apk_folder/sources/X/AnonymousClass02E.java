package X;

import android.content.Context;
import android.os.StrictMode;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.02E  reason: invalid class name */
public abstract class AnonymousClass02E extends C002801t {
    public String A00;
    public String[] A01;
    private final Map A02 = new HashMap();
    public final Context A03;

    public abstract AnonymousClass0EL A0D();

    public static Object A03(AnonymousClass02E r3, String str) {
        Object obj;
        synchronized (r3.A02) {
            obj = r3.A02.get(str);
            if (obj == null) {
                obj = new Object();
                r3.A02.put(str, obj);
            }
        }
        return obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(java.io.File r3, byte r4) {
        /*
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile
            java.lang.String r0 = "rw"
            r2.<init>(r3, r0)
            r0 = 0
            r2.seek(r0)     // Catch:{ all -> 0x0021 }
            r2.write(r4)     // Catch:{ all -> 0x0021 }
            long r0 = r2.getFilePointer()     // Catch:{ all -> 0x0021 }
            r2.setLength(r0)     // Catch:{ all -> 0x0021 }
            java.io.FileDescriptor r0 = r2.getFD()     // Catch:{ all -> 0x0021 }
            r0.sync()     // Catch:{ all -> 0x0021 }
            r2.close()
            return
        L_0x0021:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0027 }
        L_0x0027:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02E.A04(java.io.File, byte):void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:130|(2:132|133)|134|135) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:171|(2:173|174)|175|176) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:180|(2:182|183)|184|185) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:163|164|165|166|167) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004c, code lost:
        if (r2 != 1) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x02c9, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:134:0x024b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:166:0x02ad */
    /* JADX WARNING: Missing exception handler attribute for start block: B:175:0x02b6 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:184:0x02bf */
    /* JADX WARNING: Missing exception handler attribute for start block: B:198:0x02cd */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01bb A[Catch:{ IOException -> 0x0220, all -> 0x0225 }] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01d6 A[Catch:{ IOException -> 0x0220, all -> 0x0225 }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01ff A[SYNTHETIC, Splitter:B:110:0x01ff] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0204 A[SYNTHETIC, Splitter:B:113:0x0204] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0209 A[SYNTHETIC, Splitter:B:115:0x0209] */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x025c A[Catch:{ all -> 0x02ce }] */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x025f A[Catch:{ all -> 0x02ce }] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0262 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0266 A[SYNTHETIC, Splitter:B:151:0x0266] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x028e A[SYNTHETIC, Splitter:B:157:0x028e] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x0142 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:227:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00e0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00f1 A[Catch:{ all -> 0x024b }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:141:0x0254=Splitter:B:141:0x0254, B:184:0x02bf=Splitter:B:184:0x02bf} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(int r28) {
        /*
            r27 = this;
            r10 = r27
            java.io.File r3 = r10.A00
            boolean r0 = r3.mkdirs()
            if (r0 != 0) goto L_0x0024
            boolean r0 = r3.isDirectory()
            if (r0 != 0) goto L_0x0024
            java.io.IOException r2 = new java.io.IOException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "cannot mkdir: "
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0024:
            java.io.File r2 = new java.io.File
            java.io.File r1 = r10.A00
            java.lang.String r0 = "dso_lock"
            r2.<init>(r1, r0)
            X.01B r9 = new X.01B
            r9.<init>(r2)
            byte[] r20 = r10.A0E()     // Catch:{ all -> 0x02ce }
            java.io.File r8 = new java.io.File     // Catch:{ all -> 0x02ce }
            java.io.File r1 = r10.A00     // Catch:{ all -> 0x02ce }
            java.lang.String r0 = "dso_state"
            r8.<init>(r1, r0)     // Catch:{ all -> 0x02ce }
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ all -> 0x02ce }
            java.lang.String r5 = "rw"
            r1.<init>(r8, r5)     // Catch:{ all -> 0x02ce }
            r0 = 1
            r3 = 0
            byte r2 = r1.readByte()     // Catch:{ EOFException -> 0x004e, all -> 0x02c7 }
            if (r2 == r0) goto L_0x004f
        L_0x004e:
            r2 = 0
        L_0x004f:
            r1.close()     // Catch:{ all -> 0x02ce }
            java.io.File r19 = new java.io.File     // Catch:{ all -> 0x02ce }
            java.io.File r4 = r10.A00     // Catch:{ all -> 0x02ce }
            java.lang.String r1 = "dso_deps"
            r0 = r19
            r0.<init>(r4, r1)     // Catch:{ all -> 0x02ce }
            r7 = 0
            java.io.RandomAccessFile r6 = new java.io.RandomAccessFile     // Catch:{ all -> 0x02ce }
            r6.<init>(r0, r5)     // Catch:{ all -> 0x02ce }
            long r0 = r6.length()     // Catch:{ all -> 0x02c0 }
            int r4 = (int) r0     // Catch:{ all -> 0x02c0 }
            byte[] r1 = new byte[r4]     // Catch:{ all -> 0x02c0 }
            int r0 = r6.read(r1)     // Catch:{ all -> 0x02c0 }
            if (r0 == r4) goto L_0x0071
            r2 = 0
        L_0x0071:
            r0 = r20
            boolean r0 = java.util.Arrays.equals(r1, r0)     // Catch:{ all -> 0x02c0 }
            if (r0 != 0) goto L_0x007a
            r2 = 0
        L_0x007a:
            if (r2 == 0) goto L_0x0080
            r0 = r28 & 2
            if (r0 == 0) goto L_0x0257
        L_0x0080:
            A04(r8, r3)     // Catch:{ all -> 0x02c0 }
            X.0EL r18 = r10.A0D()     // Catch:{ all -> 0x02c0 }
            X.0ET r7 = r18.A00()     // Catch:{ all -> 0x02b7 }
            X.0EM r17 = r18.A01()     // Catch:{ all -> 0x02b7 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x02ae }
            java.io.File r1 = r10.A00     // Catch:{ all -> 0x02ae }
            java.lang.String r0 = "dso_manifest"
            r3.<init>(r1, r0)     // Catch:{ all -> 0x02ae }
            java.io.RandomAccessFile r16 = new java.io.RandomAccessFile     // Catch:{ all -> 0x02ae }
            r0 = r16
            r0.<init>(r3, r5)     // Catch:{ all -> 0x02ae }
            r12 = 0
            r0 = 1
            if (r2 != r0) goto L_0x00dd
            byte r1 = r16.readByte()     // Catch:{ Exception -> 0x00dd }
            if (r1 != r0) goto L_0x00d4
            int r5 = r16.readInt()     // Catch:{ Exception -> 0x00dd }
            if (r5 < 0) goto L_0x00cc
            X.0EV[] r4 = new X.AnonymousClass0EV[r5]     // Catch:{ Exception -> 0x00dd }
            r3 = 0
        L_0x00b2:
            if (r3 >= r5) goto L_0x00c6
            X.0EV r2 = new X.0EV     // Catch:{ Exception -> 0x00dd }
            java.lang.String r1 = r16.readUTF()     // Catch:{ Exception -> 0x00dd }
            java.lang.String r0 = r16.readUTF()     // Catch:{ Exception -> 0x00dd }
            r2.<init>(r1, r0)     // Catch:{ Exception -> 0x00dd }
            r4[r3] = r2     // Catch:{ Exception -> 0x00dd }
            int r3 = r3 + 1
            goto L_0x00b2
        L_0x00c6:
            X.0ET r0 = new X.0ET     // Catch:{ Exception -> 0x00dd }
            r0.<init>(r4)     // Catch:{ Exception -> 0x00dd }
            goto L_0x00dc
        L_0x00cc:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x00dd }
            java.lang.String r0 = "illegal number of shared libraries"
            r1.<init>(r0)     // Catch:{ Exception -> 0x00dd }
            goto L_0x00db
        L_0x00d4:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x00dd }
            java.lang.String r0 = "wrong dso manifest version"
            r1.<init>(r0)     // Catch:{ Exception -> 0x00dd }
        L_0x00db:
            throw r1     // Catch:{ Exception -> 0x00dd }
        L_0x00dc:
            r12 = r0
        L_0x00dd:
            r0 = 0
            if (r12 != 0) goto L_0x00e7
            X.0ET r12 = new X.0ET     // Catch:{ all -> 0x02a7 }
            X.0EV[] r0 = new X.AnonymousClass0EV[r0]     // Catch:{ all -> 0x02a7 }
            r12.<init>(r0)     // Catch:{ all -> 0x02a7 }
        L_0x00e7:
            X.0EV[] r5 = r7.A00     // Catch:{ all -> 0x02a7 }
            java.io.File r0 = r10.A00     // Catch:{ all -> 0x02a7 }
            java.lang.String[] r4 = r0.list()     // Catch:{ all -> 0x02a7 }
            if (r4 == 0) goto L_0x028e
            r3 = 0
        L_0x00f2:
            int r0 = r4.length     // Catch:{ all -> 0x02a7 }
            if (r3 >= r0) goto L_0x013b
            r2 = r4[r3]     // Catch:{ all -> 0x02a7 }
            java.lang.String r0 = "dso_state"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 != 0) goto L_0x0138
            java.lang.String r0 = "dso_lock"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 != 0) goto L_0x0138
            java.lang.String r0 = "dso_deps"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 != 0) goto L_0x0138
            java.lang.String r0 = "dso_manifest"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 != 0) goto L_0x0138
            r11 = 0
            r1 = 0
        L_0x0119:
            if (r11 != 0) goto L_0x012c
            int r0 = r5.length     // Catch:{ all -> 0x02a7 }
            if (r1 >= r0) goto L_0x012c
            r0 = r5[r1]     // Catch:{ all -> 0x02a7 }
            java.lang.String r0 = r0.A01     // Catch:{ all -> 0x02a7 }
            boolean r0 = r0.equals(r2)     // Catch:{ all -> 0x02a7 }
            if (r0 == 0) goto L_0x0129
            r11 = 1
        L_0x0129:
            int r1 = r1 + 1
            goto L_0x0119
        L_0x012c:
            if (r11 != 0) goto L_0x0138
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x02a7 }
            java.io.File r0 = r10.A00     // Catch:{ all -> 0x02a7 }
            r1.<init>(r0, r2)     // Catch:{ all -> 0x02a7 }
            X.C002401o.A00(r1)     // Catch:{ all -> 0x02a7 }
        L_0x0138:
            int r3 = r3 + 1
            goto L_0x00f2
        L_0x013b:
            r11 = 32768(0x8000, float:4.5918E-41)
            byte[] r0 = new byte[r11]     // Catch:{ all -> 0x02a7 }
            r26 = r0
        L_0x0142:
            boolean r0 = r17.A01()     // Catch:{ all -> 0x02a7 }
            if (r0 == 0) goto L_0x024c
            X.0Qq r5 = r17.A00()     // Catch:{ all -> 0x02a7 }
            r13 = 1
            r3 = 0
        L_0x014e:
            if (r13 == 0) goto L_0x0171
            X.0EV[] r1 = r12.A00     // Catch:{ all -> 0x0243 }
            int r0 = r1.length     // Catch:{ all -> 0x0243 }
            if (r3 >= r0) goto L_0x0171
            r4 = r1[r3]     // Catch:{ all -> 0x0243 }
            java.lang.String r1 = r4.A01     // Catch:{ all -> 0x0243 }
            X.0EV r2 = r5.A00     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = r2.A01     // Catch:{ all -> 0x0243 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0243 }
            if (r0 == 0) goto L_0x016e
            java.lang.String r1 = r4.A00     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = r2.A00     // Catch:{ all -> 0x0243 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0243 }
            if (r0 == 0) goto L_0x016e
            r13 = 0
        L_0x016e:
            int r3 = r3 + 1
            goto L_0x014e
        L_0x0171:
            if (r13 == 0) goto L_0x0202
            java.lang.String r13 = "rw"
            java.lang.String r1 = "fb-UnpackingSoSource"
            java.io.File r0 = r10.A00     // Catch:{ all -> 0x0243 }
            r4 = 1
            boolean r0 = r0.setWritable(r4, r4)     // Catch:{ all -> 0x0243 }
            if (r0 == 0) goto L_0x022a
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0243 }
            java.io.File r2 = r10.A00     // Catch:{ all -> 0x0243 }
            X.0EV r0 = r5.A00     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = r0.A01     // Catch:{ all -> 0x0243 }
            r3.<init>(r2, r0)     // Catch:{ all -> 0x0243 }
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0191 }
            r2.<init>(r3, r13)     // Catch:{ IOException -> 0x0191 }
            goto L_0x01b3
        L_0x0191:
            r2 = move-exception
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x0243 }
            r14.<init>()     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = "error overwriting "
            r14.append(r0)     // Catch:{ all -> 0x0243 }
            r14.append(r3)     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = " trying to delete and start over"
            r14.append(r0)     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = r14.toString()     // Catch:{ all -> 0x0243 }
            android.util.Log.w(r1, r0, r2)     // Catch:{ all -> 0x0243 }
            X.C002401o.A00(r3)     // Catch:{ all -> 0x0243 }
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0243 }
            r2.<init>(r3, r13)     // Catch:{ all -> 0x0243 }
        L_0x01b3:
            java.io.InputStream r0 = r5.A01     // Catch:{ IOException -> 0x0220 }
            int r0 = r0.available()     // Catch:{ IOException -> 0x0220 }
            if (r0 <= r4) goto L_0x01c9
            java.io.FileDescriptor r15 = r2.getFD()     // Catch:{ IOException -> 0x0220 }
            long r0 = (long) r0     // Catch:{ IOException -> 0x0220 }
            int r14 = android.os.Build.VERSION.SDK_INT     // Catch:{ IOException -> 0x0220 }
            r13 = 21
            if (r14 < r13) goto L_0x01c9
            com.facebook.soloader.SysUtil$LollipopSysdeps.fallocateIfSupported(r15, r0)     // Catch:{ IOException -> 0x0220 }
        L_0x01c9:
            java.io.InputStream r0 = r5.A01     // Catch:{ IOException -> 0x0220 }
            r25 = r0
            r14 = 2147483647(0x7fffffff, float:NaN)
            r22 = r26
            r15 = 0
            r13 = 0
        L_0x01d4:
            if (r13 >= r14) goto L_0x01f2
            int r0 = r14 - r13
            int r0 = java.lang.Math.min(r11, r0)     // Catch:{ IOException -> 0x0220 }
            r21 = r25
            r23 = r15
            r24 = r0
            int r1 = r21.read(r22, r23, r24)     // Catch:{ IOException -> 0x0220 }
            r0 = -1
            if (r1 == r0) goto L_0x01f2
            r21 = r2
            r24 = r1
            r21.write(r22, r23, r24)     // Catch:{ IOException -> 0x0220 }
            int r13 = r13 + r1
            goto L_0x01d4
        L_0x01f2:
            long r0 = r2.getFilePointer()     // Catch:{ IOException -> 0x0220 }
            r2.setLength(r0)     // Catch:{ IOException -> 0x0220 }
            boolean r0 = r3.setExecutable(r4, r15)     // Catch:{ IOException -> 0x0220 }
            if (r0 == 0) goto L_0x0209
            r2.close()     // Catch:{ all -> 0x0243 }
        L_0x0202:
            if (r5 == 0) goto L_0x0142
            r5.close()     // Catch:{ all -> 0x02a7 }
            goto L_0x0142
        L_0x0209:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ IOException -> 0x0220 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0220 }
            r1.<init>()     // Catch:{ IOException -> 0x0220 }
            java.lang.String r0 = "cannot make file executable: "
            r1.append(r0)     // Catch:{ IOException -> 0x0220 }
            r1.append(r3)     // Catch:{ IOException -> 0x0220 }
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x0220 }
            r4.<init>(r0)     // Catch:{ IOException -> 0x0220 }
            throw r4     // Catch:{ IOException -> 0x0220 }
        L_0x0220:
            r0 = move-exception
            X.C002401o.A00(r3)     // Catch:{ all -> 0x0225 }
            throw r0     // Catch:{ all -> 0x0225 }
        L_0x0225:
            r3 = move-exception
            r2.close()     // Catch:{ all -> 0x0243 }
            goto L_0x0242
        L_0x022a:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x0243 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0243 }
            r1.<init>()     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = "cannot make directory writable for us: "
            r1.append(r0)     // Catch:{ all -> 0x0243 }
            java.io.File r0 = r10.A00     // Catch:{ all -> 0x0243 }
            r1.append(r0)     // Catch:{ all -> 0x0243 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0243 }
            r3.<init>(r0)     // Catch:{ all -> 0x0243 }
        L_0x0242:
            throw r3     // Catch:{ all -> 0x0243 }
        L_0x0243:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0245 }
        L_0x0245:
            r0 = move-exception
            if (r5 == 0) goto L_0x024b
            r5.close()     // Catch:{ all -> 0x024b }
        L_0x024b:
            throw r0     // Catch:{ all -> 0x02a7 }
        L_0x024c:
            r16.close()     // Catch:{ all -> 0x02ae }
            if (r17 == 0) goto L_0x0254
            r17.close()     // Catch:{ all -> 0x02b7 }
        L_0x0254:
            r18.close()     // Catch:{ all -> 0x02c0 }
        L_0x0257:
            r6.close()     // Catch:{ all -> 0x02ce }
            if (r7 != 0) goto L_0x0266
            r0 = 0
        L_0x025d:
            if (r0 == 0) goto L_0x0260
            r9 = 0
        L_0x0260:
            if (r9 == 0) goto L_0x0265
            r9.close()
        L_0x0265:
            return
        L_0x0266:
            X.0EX r3 = new X.0EX     // Catch:{ all -> 0x02ce }
            r4 = r10
            r5 = r19
            r6 = r20
            r3.<init>(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x02ce }
            r0 = r28 & 1
            if (r0 == 0) goto L_0x0289
            java.lang.Thread r2 = new java.lang.Thread     // Catch:{ all -> 0x02ce }
            java.lang.String r1 = "SoSync:"
            java.io.File r0 = r10.A00     // Catch:{ all -> 0x02ce }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x02ce }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x02ce }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x02ce }
            r2.start()     // Catch:{ all -> 0x02ce }
            goto L_0x028c
        L_0x0289:
            r3.run()     // Catch:{ all -> 0x02ce }
        L_0x028c:
            r0 = 1
            goto L_0x025d
        L_0x028e:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x02a7 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x02a7 }
            r1.<init>()     // Catch:{ all -> 0x02a7 }
            java.lang.String r0 = "unable to list directory "
            r1.append(r0)     // Catch:{ all -> 0x02a7 }
            java.io.File r0 = r10.A00     // Catch:{ all -> 0x02a7 }
            r1.append(r0)     // Catch:{ all -> 0x02a7 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x02a7 }
            r2.<init>(r0)     // Catch:{ all -> 0x02a7 }
            throw r2     // Catch:{ all -> 0x02a7 }
        L_0x02a7:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x02a9 }
        L_0x02a9:
            r0 = move-exception
            r16.close()     // Catch:{ all -> 0x02ad }
        L_0x02ad:
            throw r0     // Catch:{ all -> 0x02ae }
        L_0x02ae:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x02b0 }
        L_0x02b0:
            r0 = move-exception
            if (r17 == 0) goto L_0x02b6
            r17.close()     // Catch:{ all -> 0x02b6 }
        L_0x02b6:
            throw r0     // Catch:{ all -> 0x02b7 }
        L_0x02b7:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x02b9 }
        L_0x02b9:
            r0 = move-exception
            if (r18 == 0) goto L_0x02bf
            r18.close()     // Catch:{ all -> 0x02bf }
        L_0x02bf:
            throw r0     // Catch:{ all -> 0x02c0 }
        L_0x02c0:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x02c2 }
        L_0x02c2:
            r0 = move-exception
            r6.close()     // Catch:{ all -> 0x02cd }
            goto L_0x02cd
        L_0x02c7:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x02c9 }
        L_0x02c9:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x02cd }
        L_0x02cd:
            throw r0     // Catch:{ all -> 0x02ce }
        L_0x02ce:
            r0 = move-exception
            r9.close()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02E.A0A(int):void");
    }

    public String[] A0B() {
        String[] strArr = this.A01;
        if (strArr == null) {
            return super.A0B();
        }
        return strArr;
    }

    public int A09(String str, int i, StrictMode.ThreadPolicy threadPolicy) {
        int A0C;
        synchronized (A03(this, str)) {
            A0C = A0C(str, i, this.A00, threadPolicy);
        }
        return A0C;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0037, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0038, code lost:
        if (r4 != null) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] A0E() {
        /*
            r6 = this;
            android.os.Parcel r5 = android.os.Parcel.obtain()
            X.0EL r4 = r6.A0D()
            X.0ET r0 = r4.A00()     // Catch:{ all -> 0x0035 }
            X.0EV[] r3 = r0.A00     // Catch:{ all -> 0x0035 }
            r0 = 1
            r5.writeByte(r0)     // Catch:{ all -> 0x0035 }
            int r2 = r3.length     // Catch:{ all -> 0x0035 }
            r5.writeInt(r2)     // Catch:{ all -> 0x0035 }
            r1 = 0
        L_0x0017:
            if (r1 >= r2) goto L_0x002a
            r0 = r3[r1]     // Catch:{ all -> 0x0035 }
            java.lang.String r0 = r0.A01     // Catch:{ all -> 0x0035 }
            r5.writeString(r0)     // Catch:{ all -> 0x0035 }
            r0 = r3[r1]     // Catch:{ all -> 0x0035 }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x0035 }
            r5.writeString(r0)     // Catch:{ all -> 0x0035 }
            int r1 = r1 + 1
            goto L_0x0017
        L_0x002a:
            r4.close()
            byte[] r0 = r5.marshall()
            r5.recycle()
            return r0
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            if (r4 == 0) goto L_0x003d
            r4.close()     // Catch:{ all -> 0x003d }
        L_0x003d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02E.A0E():byte[]");
    }

    public AnonymousClass02E(Context context, File file) {
        super(file, 1);
        this.A03 = context;
    }

    public AnonymousClass02E(Context context, String str) {
        super(new File(AnonymousClass08S.A0P(context.getApplicationInfo().dataDir, "/", str)), 1);
        this.A03 = context;
    }
}
