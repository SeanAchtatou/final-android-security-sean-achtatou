package X;

import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0bc  reason: invalid class name and case insensitive filesystem */
public final class C06510bc implements C25401Zm {
    public final AnonymousClass0US A00;
    public final C07670dw A01;
    public final Executor A02;
    public final AtomicReference A03 = new AtomicReference();
    private final AnonymousClass0US A04;
    private final AnonymousClass0US A05;
    private final Random A06;
    private volatile C46942Sn A07;
    private volatile C46972Ss A08;

    public long AuS(int i) {
        C07200cs r0;
        AnonymousClass0YP r02 = (AnonymousClass0YP) this.A03.get();
        if (r02 != null && (r0 = r02.A00) != null) {
            return (long) r0.A00.A03(i);
        }
        if (this.A08 == null) {
            this.A08 = (C46972Ss) this.A04.get();
        }
        return this.A08.A00(i);
    }

    public int B1q(int i) {
        AnonymousClass0YP r0 = (AnonymousClass0YP) this.A03.get();
        if (r0 != null) {
            return r0.A01.A03(i);
        }
        if (this.A07 == null) {
            this.A07 = (C46942Sn) this.A05.get();
            if (this.A07 == null) {
                return -1;
            }
        }
        return this.A07.A00(i);
    }

    public int BzU(int i) {
        Random random = this.A06;
        if (i <= 0) {
            return Integer.MAX_VALUE;
        }
        if (i == 1) {
            return 1;
        }
        if (random.nextInt(i) != 0) {
            return Integer.MAX_VALUE;
        }
        return i;
    }

    public C06510bc(Executor executor, AnonymousClass0US r4, AnonymousClass0US r5, AnonymousClass0US r6, C07670dw r7, Random random) {
        this.A02 = executor;
        this.A00 = r4;
        this.A04 = r5;
        this.A05 = r6;
        this.A01 = r7;
        this.A06 = random;
        AnonymousClass07A.A04(executor, new C06520bd(this), -716295554);
    }
}
