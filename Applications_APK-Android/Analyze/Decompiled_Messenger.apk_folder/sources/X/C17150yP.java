package X;

import android.view.View;
import java.util.ArrayList;

/* renamed from: X.0yP  reason: invalid class name and case insensitive filesystem */
public final class C17150yP implements C17160yQ {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ArrayList A01;

    public void Bss(C14030sU r1) {
    }

    public void Bsv(C14030sU r1) {
    }

    public void Bsw(C14030sU r1) {
    }

    public void Bsx(C14030sU r1) {
    }

    public C17150yP(View view, ArrayList arrayList) {
        this.A00 = view;
        this.A01 = arrayList;
    }

    public void Bst(C14030sU r5) {
        r5.A0F(this);
        this.A00.setVisibility(8);
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((View) this.A01.get(i)).setVisibility(0);
        }
    }
}
