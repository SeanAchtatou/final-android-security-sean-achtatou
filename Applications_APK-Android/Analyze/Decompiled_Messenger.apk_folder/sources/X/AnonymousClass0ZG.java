package X;

/* renamed from: X.0ZG  reason: invalid class name */
public final class AnonymousClass0ZG extends AnonymousClass0ZH {
    private final Object A00 = new Object();

    public Object ALa() {
        Object ALa;
        synchronized (this.A00) {
            ALa = super.ALa();
        }
        return ALa;
    }

    public boolean C0w(Object obj) {
        boolean C0w;
        synchronized (this.A00) {
            C0w = super.C0w(obj);
        }
        return C0w;
    }

    public AnonymousClass0ZG(int i) {
        super(i);
    }
}
