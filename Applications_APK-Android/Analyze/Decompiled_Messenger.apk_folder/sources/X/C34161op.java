package X;

import android.text.TextUtils;
import com.facebook.auth.viewercontext.ViewerContext;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1op  reason: invalid class name and case insensitive filesystem */
public abstract class C34161op implements C34181or, C34171oq, C34191os, C187514y {
    public String A01() {
        ViewerContext A08 = ((AnonymousClass0XN) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ArW, ((C34151oo) this).A00)).A08();
        String str = A08 != null ? A08.mUserId : null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;
    }

    public int BwJ() {
        return 100;
    }

    public void A02() {
        HashSet<String> hashSet = new HashSet<>();
        for (Map.Entry entry : B43().A01().entrySet()) {
            String str = (String) entry.getKey();
            JSONObject jSONObject = (JSONObject) entry.getValue();
            if (!TextUtils.isEmpty(str) && !jSONObject.optBoolean("keep_data_between_sessions", false)) {
                hashSet.add(str);
            }
        }
        for (String str2 : hashSet) {
            AWt(new File(str2));
            B43().A03(str2);
        }
    }

    public /* bridge */ /* synthetic */ void Bhy(C31071j6 r2, C30971iw r3, File file) {
        if (!(this instanceof C34151oo)) {
            A04(r2, (AnonymousClass13z) r3, file);
        } else {
            ((C34151oo) this).A04(r2, (AnonymousClass13z) r3, file);
        }
    }

    public void A03(AnonymousClass13z r5, File file) {
        try {
            String canonicalPath = file.getCanonicalPath();
            String A01 = A01();
            if (A01 != null) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("user_id", A01);
                jSONObject.put("keep_data_between_sessions", r5.A00);
                B43().A04(canonicalPath, jSONObject);
            }
        } catch (IOException | JSONException unused) {
        }
    }

    public void A04(C31071j6 r4, AnonymousClass13z r5, File file) {
        AnonymousClass07A.A04(Ady(), new C34201ot(this, r5, file), 1262448536);
    }

    public String BwW(C31071j6 r2, C30971iw r3) {
        String A01 = A01();
        if (A01 != null) {
            return A01;
        }
        throw new C200919dB(r2);
    }
}
