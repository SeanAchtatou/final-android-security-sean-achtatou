package X;

import android.os.Bundle;

/* renamed from: X.0lL  reason: invalid class name */
public interface AnonymousClass0lL {
    String AwN();

    Bundle Ax6();

    boolean BEO();

    AnonymousClass0lL C6o(boolean z);

    AnonymousClass0lL C81(boolean z);

    AnonymousClass0lL CA2(C55182nf r1);

    AnonymousClass0lL CA5(C156107Jr r1);

    C27211cp CGe();

    C27211cp CHL();

    C27211cp CHd();

    boolean isRunning();
}
