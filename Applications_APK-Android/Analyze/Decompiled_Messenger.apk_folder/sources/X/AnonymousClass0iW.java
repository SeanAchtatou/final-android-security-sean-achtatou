package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.0iW  reason: invalid class name */
public abstract class AnonymousClass0iW {
    public int A03(String str) {
        C09820ik r2 = (C09820ik) this;
        AnonymousClass1LS A0E = r2.A0E();
        if (A0E == null) {
            return 0;
        }
        if (A0E.getResource(r2.A0G()) == null) {
            return 1;
        }
        String A01 = r2.A0F().A01();
        return (A01 == null || !A01.equals(str)) ? 2 : 3;
    }

    public File A04() {
        File resource;
        C09820ik r3 = (C09820ik) this;
        AnonymousClass1LS A0E = r3.A0E();
        if (A0E == null || (resource = A0E.getResource(r3.A0G())) == null) {
            return null;
        }
        r3.A0F().A02(resource);
        return resource;
    }

    public File A05() {
        if (!(this instanceof AnonymousClass0iU)) {
            return null;
        }
        return C09820ik.A02((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, ((AnonymousClass0iU) this).A00), "emoji_fonts", "FacebookEmoji.ttf");
    }

    public File A06() {
        C09820ik r2 = (C09820ik) this;
        File A00 = r2.A0F().A00();
        if (A00 == null) {
            A00 = r2.A05();
        }
        if (A00 == null || !A00.isFile()) {
            return null;
        }
        return A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0061, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0085, code lost:
        if (r0 == false) goto L_0x0087;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0065 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File A07(java.io.InputStream r14, java.lang.String r15) {
        /*
            r13 = this;
            r7 = r13
            X.0ik r7 = (X.C09820ik) r7
            X.1LS r4 = r7.A0E()
            r12 = 0
            if (r4 == 0) goto L_0x009e
            java.lang.String r1 = r7.A0G()
            java.lang.String r0 = ".tmp"
            java.lang.String r3 = X.AnonymousClass08S.A0J(r1, r0)
            java.io.File r2 = r4.BDA(r3)
            if (r2 == 0) goto L_0x009e
            A01(r14, r2)     // Catch:{ IOException -> 0x0066 }
            java.lang.String r6 = r7.A0G()
            java.io.File r5 = r4.BDA(r6)
            if (r5 == 0) goto L_0x009b
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ IOException -> 0x007d }
            r8.<init>(r2)     // Catch:{ IOException -> 0x007d }
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r10 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0058, UnsupportedEncodingException -> 0x0051 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r9 = new byte[r0]     // Catch:{ NoSuchAlgorithmException -> 0x0058, UnsupportedEncodingException -> 0x0051 }
        L_0x0036:
            int r1 = r8.read(r9)     // Catch:{ NoSuchAlgorithmException -> 0x0058, UnsupportedEncodingException -> 0x0051 }
            if (r1 <= 0) goto L_0x0041
            r0 = 0
            r10.update(r9, r0, r1)     // Catch:{ NoSuchAlgorithmException -> 0x0058, UnsupportedEncodingException -> 0x0051 }
            goto L_0x0036
        L_0x0041:
            byte[] r0 = r10.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0058, UnsupportedEncodingException -> 0x0051 }
            java.lang.String r0 = X.C03380Nn.A02(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0058, UnsupportedEncodingException -> 0x0051 }
            boolean r0 = r0.equals(r15)     // Catch:{ all -> 0x005f }
            r8.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x007e
        L_0x0051:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x005f }
            r1.<init>(r0)     // Catch:{ all -> 0x005f }
            goto L_0x005e
        L_0x0058:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x005f }
            r1.<init>(r0)     // Catch:{ all -> 0x005f }
        L_0x005e:
            throw r1     // Catch:{ all -> 0x005f }
        L_0x005f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0061 }
        L_0x0061:
            r0 = move-exception
            r8.close()     // Catch:{ all -> 0x0065 }
        L_0x0065:
            throw r0     // Catch:{ IOException -> 0x007d }
        L_0x0066:
            r2 = move-exception
            r1 = r2
        L_0x0068:
            if (r1 == 0) goto L_0x0073
            boolean r0 = r1 instanceof com.facebook.tigon.TigonErrorException
            if (r0 != 0) goto L_0x0073
            java.lang.Throwable r1 = r1.getCause()
            goto L_0x0068
        L_0x0073:
            if (r1 != 0) goto L_0x009b
            java.lang.Class<X.0ik> r1 = X.C09820ik.class
            java.lang.String r0 = "Unable to copy stream to temp file"
            X.C010708t.A0A(r1, r0, r2)
            goto L_0x009b
        L_0x007d:
            r0 = 0
        L_0x007e:
            if (r0 == 0) goto L_0x0087
            boolean r0 = r2.renameTo(r5)
            r2 = 1
            if (r0 != 0) goto L_0x0088
        L_0x0087:
            r2 = 0
        L_0x0088:
            if (r2 == 0) goto L_0x009f
            r4.ATT(r6, r5)
            X.0iq r0 = r7.A0F()
            r0.A03(r15)
            r0.A02(r5)
        L_0x0097:
            if (r2 != 0) goto L_0x009a
            r5 = r12
        L_0x009a:
            r12 = r5
        L_0x009b:
            r4.remove(r3)
        L_0x009e:
            return r12
        L_0x009f:
            java.lang.Class<X.0ik> r1 = X.C09820ik.class
            java.lang.String r0 = "Unable to copy temp file to new location"
            X.C010708t.A07(r1, r0)
            r4.remove(r6)
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0iW.A07(java.io.InputStream, java.lang.String):java.io.File");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x008e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08() {
        /*
            r9 = this;
            r7 = r9
            X.0ik r7 = (X.C09820ik) r7
            java.lang.Class<X.0ik> r5 = X.C09820ik.class
            java.io.File r8 = r7.A05()
            if (r8 == 0) goto L_0x0032
            boolean r0 = r8.exists()
            if (r0 == 0) goto L_0x0032
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A01
            int r0 = r0.get()
            if (r0 != 0) goto L_0x0032
            java.io.File r0 = r7.A04()
            r1 = 2
            r2 = 0
            if (r0 == 0) goto L_0x0033
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0033
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A01
            boolean r0 = r0.compareAndSet(r2, r1)
            if (r0 == 0) goto L_0x0032
            r8.delete()
        L_0x0032:
            return
        L_0x0033:
            java.lang.Object r3 = r7.A00
            monitor-enter(r3)
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A01     // Catch:{ all -> 0x00ad }
            r1 = 1
            boolean r0 = r0.compareAndSet(r2, r1)     // Catch:{ all -> 0x00ad }
            if (r0 != 0) goto L_0x0060
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A01     // Catch:{ all -> 0x00ad }
            int r0 = r0.get()     // Catch:{ all -> 0x00ad }
            if (r0 == r1) goto L_0x005e
            java.lang.String r2 = "Conflicting migrations for %s! (state is %d)"
            java.lang.String r1 = r8.getAbsolutePath()     // Catch:{ all -> 0x00ad }
            java.util.concurrent.atomic.AtomicInteger r0 = r7.A01     // Catch:{ all -> 0x00ad }
            int r0 = r0.get()     // Catch:{ all -> 0x00ad }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00ad }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0}     // Catch:{ all -> 0x00ad }
            X.C010708t.A0D(r5, r2, r0)     // Catch:{ all -> 0x00ad }
        L_0x005e:
            monitor-exit(r3)     // Catch:{ all -> 0x00ad }
            goto L_0x00ac
        L_0x0060:
            java.lang.String r6 = r7.A0G()     // Catch:{ all -> 0x00ad }
            X.1LS r0 = r7.A0E()     // Catch:{ all -> 0x00ad }
            java.io.File r4 = r0.BDA(r6)     // Catch:{ all -> 0x00ad }
            if (r4 != 0) goto L_0x007c
            java.lang.String r1 = "Unable to get file in the cache for migration of %s!"
            java.lang.String r0 = r8.getAbsolutePath()     // Catch:{ all -> 0x00ad }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x00ad }
            X.C010708t.A0D(r5, r1, r0)     // Catch:{ all -> 0x00ad }
            goto L_0x005e
        L_0x007c:
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x009a, IOException -> 0x008f }
            r1.<init>(r8)     // Catch:{ FileNotFoundException -> 0x009a, IOException -> 0x008f }
            A01(r1, r4)     // Catch:{ all -> 0x0088 }
            r1.close()     // Catch:{ FileNotFoundException -> 0x009a, IOException -> 0x008f }
            goto L_0x00a4
        L_0x0088:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x008a }
        L_0x008a:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x008e }
        L_0x008e:
            throw r0     // Catch:{ FileNotFoundException -> 0x009a, IOException -> 0x008f }
        L_0x008f:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r8, r4}     // Catch:{ all -> 0x00ad }
            java.lang.String r0 = "Unable to copy file: %s %s"
            X.C010708t.A0G(r5, r2, r0, r1)     // Catch:{ all -> 0x00ad }
            goto L_0x00a4
        L_0x009a:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r8, r4}     // Catch:{ all -> 0x00ad }
            java.lang.String r0 = "Unable to find file: %s %s"
            X.C010708t.A0G(r5, r2, r0, r1)     // Catch:{ all -> 0x00ad }
        L_0x00a4:
            X.1LS r0 = r7.A0E()     // Catch:{ all -> 0x00ad }
            r0.ATT(r6, r4)     // Catch:{ all -> 0x00ad }
            goto L_0x005e
        L_0x00ac:
            return
        L_0x00ad:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00ad }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0iW.A08():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A09() {
        /*
            r14 = this;
            r8 = r14
            X.0ik r8 = (X.C09820ik) r8
            boolean r0 = r8 instanceof X.AnonymousClass0iU
            if (r0 != 0) goto L_0x001a
            r8.A0B()
            r0 = 10485760(0xa00000, float:1.469368E-38)
            r4 = 1
            X.09i r3 = X.C012109i.A01()
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            long r0 = (long) r0
            boolean r0 = r3.A09(r2, r0)
            r4 = r4 ^ r0
        L_0x0019:
            return r4
        L_0x001a:
            X.0iU r8 = (X.AnonymousClass0iU) r8
            int r1 = X.AnonymousClass1Y3.BCt
            X.0UN r0 = r8.A00
            r7 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r0)
            android.content.Context r1 = (android.content.Context) r1
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r1.getSystemService(r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            boolean r0 = X.C12880q9.A00(r0)
            if (r0 == 0) goto L_0x0108
            X.0ip r1 = r8.A01
            java.lang.String r0 = "download_blocked_time"
            X.1Y7 r6 = r1.A04(r0)
            X.0ip r0 = r8.A01
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r7, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r5 = (com.facebook.prefs.shared.FbSharedPreferences) r5
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r5.At2(r6, r1)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x005f
            X.1hn r0 = r5.edit()
            r0.BzA(r6, r1)
            r0.commit()
        L_0x005f:
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r1 = r1 - r3
            long r3 = r0.toDays(r1)
            r5 = 0
            r1 = 128(0x80, double:6.32E-322)
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x0072
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0074
        L_0x0072:
            r3 = 128(0x80, double:6.32E-322)
        L_0x0074:
            r1 = 3
            r6 = 2
            java.lang.String r5 = "_DAYS"
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c6
            java.lang.String r0 = "NETWORK_METERED_"
            java.lang.String r2 = X.AnonymousClass08S.A0H(r0, r3, r5)
        L_0x0083:
            int r1 = X.AnonymousClass1Y3.BRo
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.0ij r0 = (X.C09810ij) r0
            r0.A01(r2, r7)
            r0 = 0
        L_0x0091:
            r4 = 0
            if (r0 == 0) goto L_0x0019
            X.09i r3 = X.C012109i.A01()
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            r0 = 52428800(0x3200000, double:2.5903269E-316)
            boolean r0 = r3.A09(r2, r0)
            if (r0 == 0) goto L_0x013e
            long r2 = r3.A05(r2)
            r0 = 1048576(0x100000, double:5.180654E-318)
            long r2 = r2 / r0
            long r2 = java.lang.Long.highestOneBit(r2)
            java.lang.String r1 = "LOW_DISK_"
            java.lang.String r0 = "_MB"
            java.lang.String r3 = X.AnonymousClass08S.A0H(r1, r2, r0)
            int r2 = X.AnonymousClass1Y3.BRo
            X.0UN r1 = r8.A00
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0ij r1 = (X.C09810ij) r1
            r1.A01(r3, r7)
            return r4
        L_0x00c6:
            int r1 = X.AnonymousClass1Y3.B3e
            X.0UN r0 = r8.A00
            r2 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.common.connectionstatus.FbDataConnectionManager r0 = (com.facebook.common.connectionstatus.FbDataConnectionManager) r0
            X.1Vy r13 = r0.A06()
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.common.connectionstatus.FbDataConnectionManager r0 = (com.facebook.common.connectionstatus.FbDataConnectionManager) r0
            double r11 = r0.A03()
            int r0 = r13.ordinal()
            r9 = 4652007308841189376(0x408f400000000000, double:1000.0)
            switch(r0) {
                case 0: goto L_0x011a;
                case 1: goto L_0x011a;
                case 2: goto L_0x0113;
                case 3: goto L_0x010a;
                case 4: goto L_0x010a;
                case 5: goto L_0x00f8;
                default: goto L_0x00ed;
            }
        L_0x00ed:
            java.lang.Class<X.0iU> r2 = X.AnonymousClass0iU.class
            java.lang.Object[] r1 = new java.lang.Object[]{r13}
            java.lang.String r0 = "Unhandled network quality: %s"
            X.C010708t.A0D(r2, r0, r1)
        L_0x00f8:
            r1 = 7
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x011d
            r1 = 0
            int r0 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0108
            int r0 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r0 <= 0) goto L_0x011d
        L_0x0108:
            r0 = 1
            goto L_0x0091
        L_0x010a:
            r1 = 4636737291354636288(0x4059000000000000, double:100.0)
            int r0 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0108
            java.lang.String r9 = "GOOD"
            goto L_0x011f
        L_0x0113:
            int r0 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r0 >= 0) goto L_0x0108
            java.lang.String r9 = "OKAY"
            goto L_0x011f
        L_0x011a:
            java.lang.String r9 = "POOR"
            goto L_0x011f
        L_0x011d:
            java.lang.String r9 = "UNKNOWN"
        L_0x011f:
            long r2 = java.lang.Long.highestOneBit(r3)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "NETWORK_"
            r1.<init>(r0)
            r1.append(r9)
            r0 = 95
            r1.append(r0)
            r1.append(r2)
            r1.append(r5)
            java.lang.String r2 = r1.toString()
            goto L_0x0083
        L_0x013e:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0iW.A09():boolean");
    }

    public void A0A(int i) {
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:12|13|14|15|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0024 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(java.io.InputStream r3, java.io.File r4) {
        /*
            java.io.FileOutputStream r2 = new java.io.FileOutputStream
            r2.<init>(r4)
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0025 }
            r1.<init>(r2)     // Catch:{ all -> 0x0025 }
            X.C76163l4.A01(r3, r1)     // Catch:{ all -> 0x001e }
            r1.flush()     // Catch:{ all -> 0x001e }
            java.io.FileDescriptor r0 = r2.getFD()     // Catch:{ all -> 0x001e }
            r0.sync()     // Catch:{ all -> 0x001e }
            r1.close()     // Catch:{ all -> 0x0025 }
            r2.close()
            return
        L_0x001e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0020 }
        L_0x0020:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0024 }
        L_0x0024:
            throw r0     // Catch:{ all -> 0x0025 }
        L_0x0025:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x002b }
        L_0x002b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0iW.A01(java.io.InputStream, java.io.File):void");
    }
}
