package X;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/* renamed from: X.1cM  reason: invalid class name and case insensitive filesystem */
public final class C26921cM implements Iterator, Map.Entry {
    public int A00;
    public int A01;
    public boolean A02 = false;
    public final /* synthetic */ C04180St A03;

    public C26921cM(C04180St r2) {
        this.A03 = r2;
        this.A00 = r2.A02() - 1;
        this.A01 = -1;
    }

    public boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (!this.A02) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object A05 = this.A03.A05(this.A01, 0);
            if (key == A05 || (key != null && key.equals(A05))) {
                z = true;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
            Object value = entry.getValue();
            Object A052 = this.A03.A05(this.A01, 1);
            if (value == A052 || (value != null && value.equals(A052))) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (z2) {
                return true;
            }
            return false;
        }
    }

    public Object getKey() {
        if (this.A02) {
            return this.A03.A05(this.A01, 0);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public Object getValue() {
        if (this.A02) {
            return this.A03.A05(this.A01, 1);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public boolean hasNext() {
        if (this.A01 < this.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode;
        if (this.A02) {
            C04180St r4 = this.A03;
            int i = this.A01;
            int i2 = 0;
            Object A05 = r4.A05(i, 0);
            Object A052 = r4.A05(i, 1);
            if (A05 == null) {
                hashCode = 0;
            } else {
                hashCode = A05.hashCode();
            }
            if (A052 != null) {
                i2 = A052.hashCode();
            }
            return hashCode ^ i2;
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public void remove() {
        if (this.A02) {
            this.A03.A09(this.A01);
            this.A01--;
            this.A00--;
            this.A02 = false;
            return;
        }
        throw new IllegalStateException();
    }

    public Object setValue(Object obj) {
        if (this.A02) {
            return this.A03.A06(this.A01, obj);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public String toString() {
        return getKey() + "=" + getValue();
    }

    public Object next() {
        if (hasNext()) {
            this.A01++;
            this.A02 = true;
            return this;
        }
        throw new NoSuchElementException();
    }
}
