package X;

import com.facebook.common.util.TriState;
import com.facebook.messaging.model.messagemetadata.MessagePlatformPersona;
import com.facebook.messaging.typingattribution.TypingAttributionData;

/* renamed from: X.1H2  reason: invalid class name */
public final class AnonymousClass1H2 {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public TriState A04 = TriState.UNSET;
    public MessagePlatformPersona A05;
    public TypingAttributionData A06;
    public Integer A07 = AnonymousClass07B.A01;
    public boolean A08;
    public boolean A09;
}
