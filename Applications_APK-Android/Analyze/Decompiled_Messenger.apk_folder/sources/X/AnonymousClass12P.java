package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.12P  reason: invalid class name */
public final class AnonymousClass12P extends C15380vC {
    public final C15390vD A00;
    public final C15960wG A01 = new AnonymousClass12Q(this);
    private final FbSharedPreferences A02;
    private final AnonymousClass1Y7 A03;

    public static void A00(AnonymousClass12P r3) {
        int i = r3.A00.Ae7().A00;
        C30281hn edit = r3.A02.edit();
        edit.Bz6(r3.A03, i);
        edit.commit();
        r3.A03(r3.A00.Ae7());
    }

    public AnonymousClass12P(AnonymousClass1XY r4, C15390vD r5, String str) {
        this.A02 = FbSharedPreferencesModule.A00(r4);
        this.A00 = r5;
        this.A03 = new AnonymousClass1Y7(C05690aA.A0E, AnonymousClass08S.A0J("prev_count_", str));
    }
}
