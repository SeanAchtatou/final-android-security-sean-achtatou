package X;

import android.util.SparseIntArray;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;
import com.facebook.quicklog.PerformanceLoggingEvent;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/* renamed from: X.04s  reason: invalid class name */
public final class AnonymousClass04s extends C000900o implements AnonymousClass0Td {
    public static final int A05 = ProvidersRegistry.A00.A02("qpl");
    public static final AnonymousClass04s A06 = new AnonymousClass04s();
    public SparseIntArray A00 = new SparseIntArray(2);
    public QuickPerformanceLogger A01;
    private final ThreadLocal A02 = new ThreadLocal();
    public volatile int A03 = 9;
    public volatile SparseIntArray A04 = new SparseIntArray(2);

    public AnonymousClass04s() {
        super(null);
    }

    public boolean A08() {
        return true;
    }

    public void BeV(int i, int i2, C04270Tg r3) {
    }

    public void BfD(PerformanceLoggingEvent performanceLoggingEvent) {
    }

    public void Bkk(int i, int i2) {
    }

    private static long A00(C04270Tg r3) {
        return (((long) r3.A08) << 16) & 281474976645120L;
    }

    private void A02(C04270Tg r18, long j) {
        C04270Tg r6;
        ArrayList arrayList;
        String str;
        if (TraceEvents.isEnabled(A05) && (arrayList = (r6 = r18).A0a) != null) {
            StringBuilder sb = (StringBuilder) this.A02.get();
            if (sb == null) {
                sb = new StringBuilder();
                this.A02.set(sb);
            }
            int i = 0;
            sb.setLength(0);
            while (i < arrayList.size() && (str = (String) arrayList.get(i)) != null) {
                int length = sb.length();
                int length2 = str.length();
                if (length + length2 > 1024) {
                    break;
                }
                if (length2 > 0) {
                    if (i != 0) {
                        sb.append(",");
                    }
                    sb.append(str);
                }
                i++;
            }
            if (sb.length() > 0) {
                int i2 = A05;
                Logger.writeBytesEntry(i2, 1, 57, Logger.writeBytesEntry(i2, 1, 56, Logger.writeStandardEntry(i2, 3, 58, 0, r6.A07, r6.A02, 0, j), "tags"), sb.toString());
            }
        }
    }

    private int[] A03(int[] iArr) {
        int length;
        int i;
        SparseIntArray sparseIntArray = this.A04;
        int size = sparseIntArray.size();
        if (!(iArr == null && size == 0)) {
            if (sparseIntArray.get(-1) > 0) {
                return AnonymousClass0Td.A00;
            }
            int i2 = 0;
            if (iArr == null) {
                length = 0;
            } else {
                length = iArr.length;
            }
            int i3 = length + size;
            if (i3 != 0) {
                int[] iArr2 = new int[i3];
                if (iArr != null) {
                    int length2 = iArr.length;
                    int i4 = 0;
                    i = 0;
                    while (i4 < length2) {
                        iArr2[i] = iArr[i4];
                        i4++;
                        i++;
                    }
                } else {
                    i = 0;
                }
                while (i2 < size) {
                    iArr2[i] = sparseIntArray.keyAt(i2);
                    i2++;
                    i++;
                }
                return iArr2;
            }
        }
        return null;
    }

    public void A07(TraceContext traceContext, C004505k r9) {
        int[] iArr;
        TreeMap treeMap = traceContext.A06.A01;
        if (treeMap == null) {
            iArr = null;
        } else {
            iArr = (int[]) treeMap.get("provider.qpl.event_whitelist");
        }
        if (iArr == null) {
            iArr = new int[]{-1};
        }
        SparseIntArray clone = this.A04.clone();
        for (int i : iArr) {
            int i2 = clone.get(i);
            if (i2 == 1) {
                clone.delete(i);
            } else if (i2 > 1) {
                clone.put(i, i2 - 1);
            }
        }
        this.A04 = clone;
        QuickPerformanceLogger quickPerformanceLogger = this.A01;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.updateListenerMarkers();
        }
        int i3 = 9;
        int A002 = traceContext.A06.A00("provider.qpl.point_max_level", 9);
        SparseIntArray sparseIntArray = this.A00;
        int i4 = sparseIntArray.get(A002);
        if (i4 == 1) {
            sparseIntArray.delete(A002);
        } else if (i4 > 1) {
            sparseIntArray.put(A002, i4 - 1);
        }
        synchronized (this) {
            if (sparseIntArray.size() != 0) {
                i3 = sparseIntArray.keyAt(sparseIntArray.size() - 1);
            }
            this.A03 = i3;
            this.A00 = sparseIntArray;
        }
    }

    public boolean A09(int i) {
        SparseIntArray sparseIntArray = this.A04;
        if (sparseIntArray.get(-1) > 0 || sparseIntArray.get(i) > 0) {
            return true;
        }
        return false;
    }

    public AnonymousClass0Ti AsW() {
        int[] iArr;
        AnonymousClass054 r1 = AnonymousClass054.A07;
        if (r1 == null) {
            return AnonymousClass0Ti.A04;
        }
        AnonymousClass05Z r12 = (AnonymousClass05Z) r1.A06(C003404k.A01);
        if (r12 != null && (iArr = r12.A00) != null) {
            return new AnonymousClass0Ti(A03(iArr), r12.A00, null);
        }
        int[] A032 = A03(null);
        if (A032 == null) {
            return AnonymousClass0Ti.A04;
        }
        return AnonymousClass0Ti.A00(A032);
    }

    public void BeF(C04270Tg r27) {
        if (TraceEvents.isEnabled(A05)) {
            C04270Tg r4 = r27;
            if (A09(r4.A02)) {
                int i = A05;
                Logger.writeStandardEntry(i, 7, 51, 0, 0, r4.A02, 0, ((((long) r4.A00) << 60) & -1152921504606846976L) | ((((long) r4.A08) << 16) & 281474976645120L));
                Logger.writeBytesEntry(i, 1, 57, Logger.writeBytesEntry(i, 1, 56, Logger.writeStandardEntry(A05, 7, 59, 0, 0, r4.A02, 0, (((long) r4.A08) << 16) & 281474976645120L), "type"), r4.A0L);
                List A022 = r4.A02();
                int size = A022.size();
                int i2 = 0;
                while (i2 < size) {
                    int i3 = i2 + 1;
                    i2 = i3 + 1;
                    String str = (String) A022.get(i3);
                    int writeBytesEntry = Logger.writeBytesEntry(i, 1, 56, Logger.writeStandardEntry(i, 7, 59, 0, 0, r4.A02, 0, (((long) r4.A08) << 16) & 281474976645120L), (String) A022.get(i2));
                    if (str == null) {
                        str = "null";
                    }
                    Logger.writeBytesEntry(i, 1, 57, writeBytesEntry, str);
                }
            }
        }
    }

    public void BeL(C04270Tg r13, String str, String str2) {
        if (str != null && str2 != null && TraceEvents.isEnabled(A05) && A09(r13.A02)) {
            int length = str.length();
            int length2 = str2.length();
            if (length != 0 && length2 != 0 && length + length2 <= 256 && !str.equals("loom_id")) {
                long A012 = A01(r13);
                int i = r13.A07;
                int writeStandardEntry = Logger.writeStandardEntry(A05, 1, 59, r13.A0B, i, r13.A02, 0, 281474976710656L | A012);
                int i2 = A05;
                Logger.writeBytesEntry(i2, 1, 57, Logger.writeBytesEntry(i2, 1, 56, writeStandardEntry, str), str2);
            }
        }
    }

    public void BeM(C04270Tg r14) {
        if (TraceEvents.isEnabled(A05) && A09(r14.A02)) {
            long A002 = A00(r14);
            if (!r14.A0V) {
                A002 |= 281474976710656L;
            }
            int i = r14.A02;
            int i2 = r14.A01;
            Logger.writeStandardEntry(A05, 0, 48, r14.A0B, r14.A07, i, 0, A002);
            AnonymousClass054 r3 = AnonymousClass054.A07;
            if (r3 != null) {
                AnonymousClass054.A05(r3, C003404k.A01 | C003604m.A01, null, 0, (((long) i2) << 32) | ((long) i), 2);
            }
        }
    }

    public void BeQ(C04270Tg r17, String str, AnonymousClass0lr r19, long j, boolean z, int i) {
        int i2;
        long A002;
        int i3;
        int i4;
        long j2;
        if (TraceEvents.isEnabled(A05) && (i2 = i) <= this.A03) {
            C04270Tg r5 = r17;
            if (A09(r5.A02)) {
                int i5 = r5.A02;
                int i6 = r5.A07;
                int i7 = A05;
                if (z) {
                    j2 = r5.A0B;
                    A002 = A00(r5) | ((((long) i2) << 60) & -1152921504606846976L);
                    i3 = 1;
                    i4 = 50;
                } else {
                    A002 = A00(r5) | ((((long) i2) << 60) & -1152921504606846976L);
                    i3 = 3;
                    i4 = 50;
                    j2 = 0;
                }
                int writeStandardEntry = Logger.writeStandardEntry(i7, i3, i4, j2, i6, i5, 0, A002);
                Logger.writeBytesEntry(i7, 1, 83, writeStandardEntry, str);
                if (r19 != null) {
                    Logger.writeBytesEntry(i7, 1, 57, Logger.writeBytesEntry(i7, 1, 56, writeStandardEntry, "QPL::data"), r19.toString());
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0051, code lost:
        if (r1 == false) goto L_0x0053;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BeT(X.C04270Tg r11) {
        /*
            r10 = this;
            int r0 = X.AnonymousClass04s.A05
            boolean r0 = com.facebook.profilo.core.TraceEvents.isEnabled(r0)
            if (r0 == 0) goto L_0x0082
            int r0 = r11.A02
            boolean r0 = r10.A09(r0)
            if (r0 == 0) goto L_0x0082
            X.054 r4 = X.AnonymousClass054.A07
            if (r4 == 0) goto L_0x0082
            int r3 = r11.A02
            java.lang.String r1 = r4.A07(r3)
            if (r1 == 0) goto L_0x0021
            java.lang.String r0 = "loom_id"
            r11.A07(r0, r1)
        L_0x0021:
            long r8 = A00(r11)
            boolean r0 = r11.A0V
            if (r0 != 0) goto L_0x002c
            r0 = 281474976710656(0x1000000000000, double:1.390671161567E-309)
            long r8 = r8 | r0
        L_0x002c:
            int r0 = r11.A04
            r5 = 1
            int r1 = r5 << r0
            int r0 = r11.A03
            r1 = r1 & r0
            if (r1 > 0) goto L_0x0037
            r5 = 0
        L_0x0037:
            if (r5 != 0) goto L_0x0053
            java.util.concurrent.atomic.AtomicInteger r0 = r4.A02
            int r0 = r0.get()
            r2 = 0
            if (r0 != 0) goto L_0x006e
            r1 = 0
        L_0x0043:
            int r0 = X.C003604m.A01
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0053
            java.lang.String r0 = r4.A07(r3)
            r1 = 0
            if (r0 == 0) goto L_0x0050
            r1 = 1
        L_0x0050:
            r0 = 1
            if (r1 != 0) goto L_0x0054
        L_0x0053:
            r0 = 0
        L_0x0054:
            if (r5 != 0) goto L_0x0058
            if (r0 == 0) goto L_0x005b
        L_0x0058:
            r0 = 562949953421312(0x2000000000000, double:2.781342323134002E-309)
            long r8 = r8 | r0
        L_0x005b:
            long r3 = r11.A0C
            int r5 = r11.A07
            int r0 = X.AnonymousClass04s.A05
            r1 = 0
            r2 = 46
            int r6 = r11.A02
            r7 = 0
            com.facebook.profilo.logger.Logger.writeStandardEntry(r0, r1, r2, r3, r5, r6, r7, r8)
            r10.A02(r11, r8)
            return
        L_0x006e:
            r1 = 0
        L_0x006f:
            r0 = 2
            if (r2 >= r0) goto L_0x0043
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r4.A04
            java.lang.Object r0 = r0.get(r2)
            com.facebook.profilo.ipc.TraceContext r0 = (com.facebook.profilo.ipc.TraceContext) r0
            if (r0 == 0) goto L_0x007f
            int r0 = r0.A01
            r1 = r1 | r0
        L_0x007f:
            int r2 = r2 + 1
            goto L_0x006f
        L_0x0082:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04s.BeT(X.0Tg):void");
    }

    public void BeU(C04270Tg r13) {
        if (TraceEvents.isEnabled(A05) && A09(r13.A02)) {
            long A012 = A01(r13);
            if (!r13.A0V) {
                A012 |= 281474976710656L;
            }
            A02(r13, A012);
            int i = r13.A02;
            int i2 = r13.A01;
            Logger.writeStandardEntry(A05, 0, 47, r13.A0B, r13.A07, i, 0, A012);
            AnonymousClass054 r7 = AnonymousClass054.A07;
            if (r7 != null) {
                r7.A0B(C003404k.A01 | C003604m.A01, null, (((long) i2) << 32) | ((long) i));
            }
        }
    }

    public boolean Bkl(int i, int i2) {
        AnonymousClass054 r3 = AnonymousClass054.A07;
        if (r3 == null) {
            return false;
        }
        long j = ((long) i2) << 32;
        return r3.A0A(C003404k.A01, 0, null, j | ((long) i));
    }

    public int getTracingProviders() {
        return A05 & TraceEvents.sProviders;
    }

    public void onTraceStarted(TraceContext traceContext, C004505k r9) {
        int[] iArr;
        TreeMap treeMap = traceContext.A06.A01;
        if (treeMap == null) {
            iArr = null;
        } else {
            iArr = (int[]) treeMap.get("provider.qpl.event_whitelist");
        }
        if (iArr == null) {
            iArr = new int[]{-1};
        }
        SparseIntArray clone = this.A04.clone();
        for (int i : iArr) {
            clone.put(i, clone.get(i) + 1);
        }
        this.A04 = clone;
        QuickPerformanceLogger quickPerformanceLogger = this.A01;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.updateListenerMarkers();
        }
        int A002 = traceContext.A06.A00("provider.qpl.point_max_level", 9);
        SparseIntArray clone2 = this.A00.clone();
        clone2.put(A002, clone2.get(A002) + 1);
        synchronized (this) {
            this.A03 = clone2.keyAt(clone2.size() - 1);
            this.A00 = clone2;
        }
    }

    private static long A01(C04270Tg r4) {
        return A00(r4) | ((long) r4.A0M);
    }

    public void disable() {
        C000700l.A09(-561358212, C000700l.A03(-1883740770));
    }

    public void enable() {
        C000700l.A09(1942287159, C000700l.A03(800229478));
    }

    public int getSupportedProviders() {
        return A05;
    }

    public void BeR(C04270Tg r1) {
        BeT(r1);
    }

    public void CB4(QuickPerformanceLogger quickPerformanceLogger) {
        this.A01 = quickPerformanceLogger;
    }
}
