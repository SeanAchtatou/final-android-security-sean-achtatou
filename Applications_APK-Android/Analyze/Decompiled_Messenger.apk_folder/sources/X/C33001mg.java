package X;

import com.google.common.base.Predicate;

/* renamed from: X.1mg  reason: invalid class name and case insensitive filesystem */
public final class C33001mg implements Predicate {
    public final /* synthetic */ Comparable A00;

    public C33001mg(Comparable comparable) {
        this.A00 = comparable;
    }

    public boolean apply(Object obj) {
        return AnonymousClass0A3.A00((Comparable) obj, this.A00);
    }
}
