package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tf  reason: invalid class name and case insensitive filesystem */
public final class C14600tf {
    private static volatile C14600tf A01;
    public final C06880cE A00;

    public static final C14600tf A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C14600tf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C14600tf(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C14600tf(AnonymousClass1XY r2) {
        this.A00 = C06880cE.A00(r2);
    }
}
