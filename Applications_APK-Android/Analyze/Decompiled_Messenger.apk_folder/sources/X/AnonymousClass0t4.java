package X;

import android.app.Activity;
import com.facebook.acra.util.StatFsUtil;
import com.facebook.common.activitycleaner.ActivityStackManager;
import com.facebook.common.memory.manager.MemoryManager;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0t4  reason: invalid class name */
public final class AnonymousClass0t4 implements Runnable, C16490xB {
    private static volatile AnonymousClass0t4 A09 = null;
    public static final String __redex_internal_original_name = "com.facebook.common.activitycleaner.ActivityCleaner";
    public final int A00;
    public final ActivityStackManager A01;
    public final AnonymousClass06B A02;
    public final FbSharedPreferences A03;
    public final boolean A04;
    public final boolean A05;
    private final ArrayDeque A06 = new ArrayDeque();
    private final ExecutorService A07;
    private final boolean A08;

    public static boolean A04(Activity activity) {
        return (activity == null || activity.getComponentName() == null || !activity.getComponentName().getClassName().contains("ComposerActivity")) ? false : true;
    }

    public void run() {
        AnonymousClass0tT r1;
        boolean z = false;
        while (!z) {
            synchronized (this.A06) {
                r1 = (AnonymousClass0tT) this.A06.removeFirst();
                z = this.A06.isEmpty();
            }
            r1.A00();
        }
    }

    public static final AnonymousClass0t4 A00(AnonymousClass1XY r11) {
        if (A09 == null) {
            synchronized (AnonymousClass0t4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        AnonymousClass06B A022 = AnonymousClass067.A02();
                        C14020sT A003 = C09990jL.A00();
                        FbSharedPreferences A004 = FbSharedPreferencesModule.A00(applicationInjector);
                        ActivityStackManager A005 = ActivityStackManager.A00(applicationInjector);
                        MemoryManager A006 = MemoryManager.A00(applicationInjector);
                        C06920cI.A00(applicationInjector);
                        ExecutorService A0X = AnonymousClass0UX.A0X(applicationInjector);
                        C25051Yd A007 = AnonymousClass0WT.A00(applicationInjector);
                        AnonymousClass0UX.A0Z(applicationInjector);
                        AnonymousClass0WA.A00(applicationInjector);
                        A09 = new AnonymousClass0t4(A022, A003, 8, A004, A005, A006, A0X, A007);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x008d, code lost:
        if (r3.getCallingActivity() == null) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00a6, code lost:
        if (r1 == false) goto L_0x00a8;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:73:0x00c3=Splitter:B:73:0x00c3, B:66:0x00b7=Splitter:B:66:0x00b7} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.AnonymousClass0t4 r8, int r9, android.app.Activity r10) {
        /*
            java.lang.String r1 = "ActivityCleaner.cleanUpEligibleActivities"
            r0 = -1193847469(0xffffffffb8d75553, float:-1.0267892E-4)
            X.C005505z.A03(r1, r0)
            com.facebook.prefs.shared.FbSharedPreferences r0 = r8.A03     // Catch:{ all -> 0x00c4 }
            boolean r0 = r0.BFQ()     // Catch:{ all -> 0x00c4 }
            if (r0 != 0) goto L_0x0017
            r0 = -686187128(0xffffffffd7199d88, float:-1.68901871E14)
            X.C005505z.A00(r0)
            return
        L_0x0017:
            com.facebook.common.activitycleaner.ActivityStackManager r0 = r8.A01     // Catch:{ all -> 0x00c4 }
            java.util.LinkedList r1 = r0.A04     // Catch:{ all -> 0x00c4 }
            monitor-enter(r1)     // Catch:{ all -> 0x00c4 }
            java.util.LinkedList r0 = r0.A04     // Catch:{ all -> 0x00c1 }
            int r7 = r0.size()     // Catch:{ all -> 0x00c1 }
            monitor-exit(r1)     // Catch:{ all -> 0x00c1 }
            int r7 = r7 - r9
            java.util.ArrayList r4 = X.C04300To.A00()     // Catch:{ all -> 0x00c4 }
            com.facebook.common.activitycleaner.ActivityStackManager r0 = r8.A01     // Catch:{ all -> 0x00c4 }
            java.util.List r0 = r0.A02()     // Catch:{ all -> 0x00c4 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x00c4 }
        L_0x0032:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x00c4 }
            if (r0 == 0) goto L_0x0054
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x00c4 }
            X.0tp r0 = (X.C14690tp) r0     // Catch:{ all -> 0x00c4 }
            android.app.Activity r1 = r0.A00()     // Catch:{ all -> 0x00c4 }
            if (r1 == 0) goto L_0x0032
            if (r1 != 0) goto L_0x004a
            r0 = 0
        L_0x0047:
            if (r0 != 0) goto L_0x0056
            goto L_0x004d
        L_0x004a:
            boolean r0 = r1 instanceof X.C49242bt     // Catch:{ all -> 0x00c4 }
            goto L_0x0047
        L_0x004d:
            boolean r0 = A04(r1)     // Catch:{ all -> 0x00c4 }
            if (r0 == 0) goto L_0x0032
            goto L_0x0056
        L_0x0054:
            r0 = 0
            goto L_0x0057
        L_0x0056:
            r0 = 1
        L_0x0057:
            r6 = 0
            if (r0 != 0) goto L_0x005b
            r6 = 1
        L_0x005b:
            com.facebook.common.activitycleaner.ActivityStackManager r0 = r8.A01     // Catch:{ all -> 0x00c4 }
            java.util.List r0 = r0.A02()     // Catch:{ all -> 0x00c4 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x00c4 }
        L_0x0065:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x00c4 }
            if (r0 == 0) goto L_0x00b7
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x00c4 }
            X.0tp r0 = (X.C14690tp) r0     // Catch:{ all -> 0x00c4 }
            if (r7 <= 0) goto L_0x00b7
            if (r6 == 0) goto L_0x0077
            r6 = 0
            goto L_0x0065
        L_0x0077:
            android.app.Activity r3 = r0.A00()     // Catch:{ all -> 0x00c4 }
            if (r3 != 0) goto L_0x007e
            goto L_0x0081
        L_0x007e:
            boolean r0 = r3 instanceof X.C49242bt     // Catch:{ all -> 0x00c4 }
            goto L_0x0082
        L_0x0081:
            r0 = 0
        L_0x0082:
            if (r0 != 0) goto L_0x0065
            if (r3 == r10) goto L_0x0065
            if (r3 == 0) goto L_0x008f
            android.content.ComponentName r1 = r3.getCallingActivity()     // Catch:{ all -> 0x00c4 }
            r0 = 1
            if (r1 != 0) goto L_0x0090
        L_0x008f:
            r0 = 0
        L_0x0090:
            if (r0 != 0) goto L_0x0065
            com.facebook.common.activitycleaner.ActivityStackManager r0 = r8.A01     // Catch:{ all -> 0x00c4 }
            java.util.LinkedList r2 = r0.A04     // Catch:{ all -> 0x00c4 }
            monitor-enter(r2)     // Catch:{ all -> 0x00c4 }
            if (r3 == 0) goto L_0x00a8
            java.util.HashSet r1 = r0.A03     // Catch:{ all -> 0x00b4 }
            java.util.Map r0 = r0.A05     // Catch:{ all -> 0x00b4 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x00b4 }
            boolean r1 = r1.contains(r0)     // Catch:{ all -> 0x00b4 }
            r0 = 1
            if (r1 != 0) goto L_0x00a9
        L_0x00a8:
            r0 = 0
        L_0x00a9:
            monitor-exit(r2)     // Catch:{ all -> 0x00b4 }
            if (r0 != 0) goto L_0x0065
            if (r3 == 0) goto L_0x0065
            r4.add(r3)     // Catch:{ all -> 0x00c4 }
            int r7 = r7 + -1
            goto L_0x0065
        L_0x00b4:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00b4 }
            goto L_0x00c3
        L_0x00b7:
            A03(r8, r4)     // Catch:{ all -> 0x00c4 }
            r0 = 150502937(0x8f87e19, float:1.49556025E-33)
            X.C005505z.A00(r0)
            return
        L_0x00c1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00c1 }
        L_0x00c3:
            throw r0     // Catch:{ all -> 0x00c4 }
        L_0x00c4:
            r1 = move-exception
            r0 = -1456374557(0xffffffffa9317ce3, float:-3.9410217E-14)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0t4.A01(X.0t4, int, android.app.Activity):void");
    }

    public static void A02(AnonymousClass0t4 r3, Activity activity) {
        List A022 = r3.A01.A02();
        if (activity instanceof AnonymousClass16P) {
            int size = A022.size() - 1;
            while (size >= 0) {
                Activity A002 = ((C14690tp) A022.get(size)).A00();
                if (A002 == activity || !(A002 instanceof AnonymousClass16P)) {
                    size--;
                } else {
                    ((AnonymousClass16P) A002).clearViewPools();
                    return;
                }
            }
        }
    }

    public void trim(C133746Nn r4) {
        if (r4 == C133746Nn.OnAppBackgrounded) {
            return;
        }
        if ((this.A05 || r4 != C133746Nn.OnSystemLowMemoryWhileAppInForeground) && !this.A08) {
            AnonymousClass07A.A04(this.A07, new AnonymousClass9KN(this), -1473965320);
        }
    }

    private AnonymousClass0t4(AnonymousClass06B r6, C14020sT r7, int i, FbSharedPreferences fbSharedPreferences, ActivityStackManager activityStackManager, C14320t5 r11, ExecutorService executorService, C25051Yd r13) {
        this.A02 = r6;
        boolean z = r7.A01 / StatFsUtil.IN_MEGA_BYTE <= 48;
        this.A05 = z;
        this.A00 = z ? r13.AqL(563100277342399L, 6) : i;
        this.A04 = r13.Aem(284232346439665L);
        this.A08 = r13.Aem(284232346505202L);
        this.A03 = fbSharedPreferences;
        this.A01 = activityStackManager;
        r11.C0d(this);
        this.A01.A01 = new AnonymousClass0t6(this);
        this.A07 = executorService;
    }

    public static void A03(AnonymousClass0t4 r3, List list) {
        if (!list.isEmpty()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Activity activity = (Activity) it.next();
                r3.A01.A04(activity);
                activity.finish();
            }
        }
    }
}
