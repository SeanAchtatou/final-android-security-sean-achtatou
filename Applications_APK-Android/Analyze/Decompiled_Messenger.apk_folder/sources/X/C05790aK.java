package X;

import android.text.TextUtils;

/* renamed from: X.0aK  reason: invalid class name and case insensitive filesystem */
public final class C05790aK {
    public AnonymousClass0UN A00;

    public static final C05790aK A00(AnonymousClass1XY r1) {
        return new C05790aK(r1);
    }

    public C05790aK(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public static C05770aI A01(C05790aK r3, String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return ((AnonymousClass0XQ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AIq, r3.A00)).A00(AnonymousClass08S.A0J("logged_in_", str));
    }
}
