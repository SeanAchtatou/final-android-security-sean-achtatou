package X;

import android.content.Context;
import com.facebook.common.dextricks.ResProvider;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipFile;

/* renamed from: X.0Gb  reason: invalid class name and case insensitive filesystem */
public final class C02720Gb extends ResProvider {
    public final Context A00;
    public final String A01;
    public final ZipFile A02;

    public void close() {
        ZipFile zipFile = this.A02;
        if (zipFile != null) {
            try {
                zipFile.close();
            } catch (IOException e) {
                C010708t.A0W("VoltronResProvider", e, "failed to close module zip file for module %s", this.A01);
            }
        }
    }

    public InputStream open(String str) {
        String str2 = this.A01;
        String str3 = File.separator;
        String A0P = AnonymousClass08S.A0P(str2, str3, str);
        ZipFile zipFile = this.A02;
        if (zipFile != null) {
            String A0P2 = AnonymousClass08S.A0P("assets", str3, A0P);
            if (zipFile.getEntry(A0P2) != null) {
                ZipFile zipFile2 = this.A02;
                return zipFile2.getInputStream(zipFile2.getEntry(A0P2));
            }
        }
        return this.A00.getAssets().open(A0P);
    }

    public C02720Gb(Context context, String str) {
        this.A00 = context;
        this.A01 = str;
        this.A02 = null;
    }

    public C02720Gb(Context context, String str, File file) {
        this.A00 = context;
        this.A01 = str;
        this.A02 = new ZipFile(file);
    }
}
