package X;

import java.util.concurrent.Executor;

/* renamed from: X.1yy  reason: invalid class name and case insensitive filesystem */
public final class C39391yy implements AnonymousClass90X {
    public AnonymousClass90J A00;
    public final Object A01 = new Object();
    private final Executor A02;

    public C39391yy(Executor executor, AnonymousClass90J r3) {
        this.A02 = executor;
        this.A00 = r3;
    }

    public final void ALM(C51412ga r4) {
        AnonymousClass90J r0;
        if (!r4.A06()) {
            synchronized (this.A01) {
                r0 = this.A00;
            }
            if (r0 != null) {
                AnonymousClass07A.A04(this.A02, new C39411z0(this, r4), 1317868696);
            }
        }
    }
}
