package X;

import android.graphics.drawable.Animatable;
import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;

/* renamed from: X.1jK  reason: invalid class name and case insensitive filesystem */
public final class C31211jK extends C31221jL implements C31231jM, C31241jN, AnonymousClass1OQ {
    public Optional A00 = Optional.absent();
    private boolean A01 = false;
    public final long A02;
    public final AnonymousClass06B A03;
    public final C25051Yd A04;
    public final C31191jI A05;
    public final QuickPerformanceLogger A06;
    private final C31141jD A07;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean A01() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.A01     // Catch:{ all -> 0x0022 }
            r1 = 1
            if (r0 != 0) goto L_0x0020
            X.1jI r0 = r2.A05     // Catch:{ all -> 0x0022 }
            com.facebook.prefs.shared.FbSharedPreferences r0 = r0.A00     // Catch:{ all -> 0x0022 }
            boolean r0 = r0.BFQ()     // Catch:{ all -> 0x0022 }
            if (r0 != 0) goto L_0x0013
            r0 = 0
            monitor-exit(r2)
            return r0
        L_0x0013:
            X.1jI r0 = r2.A05     // Catch:{ all -> 0x0022 }
            com.google.common.base.Optional r0 = r0.A00()     // Catch:{ all -> 0x0022 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x0022 }
            r2.A00 = r0     // Catch:{ all -> 0x0022 }
            r2.A01 = r1     // Catch:{ all -> 0x0022 }
        L_0x0020:
            monitor-exit(r2)
            return r1
        L_0x0022:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31211jK.A01():boolean");
    }

    public void BYg(String str, Throwable th) {
    }

    public void BZ2(String str, Object obj, Animatable animatable) {
    }

    public void Bbb(String str, Throwable th) {
    }

    public void Bbc(String str, Object obj) {
    }

    public synchronized void BgO(AnonymousClass1Q0 r10, CallerContext callerContext, int i, boolean z, boolean z2) {
        long j;
        C55702oW r0;
        if (this.A04.Aem(282054797296687L) && A01()) {
            if (!this.A00.isPresent() && this.A07.BwV(r10, callerContext)) {
                C31191jI r4 = this.A05;
                Uri uri = r10.A02;
                long now = this.A03.now();
                String str = callerContext.A02;
                String A022 = AnonymousClass01P.A02();
                synchronized (r4) {
                    try {
                        Preconditions.checkState(r4.A00.BFQ());
                        C30281hn edit = r4.A00.edit();
                        edit.BzC(r4.A0C, uri.toString());
                        edit.Bz6(r4.A0B, 0);
                        edit.Bz6(r4.A01, i);
                        edit.BzA(r4.A04, now);
                        edit.putBoolean(r4.A0A, z);
                        edit.BzC(r4.A02, str);
                        edit.BzC(r4.A03, A022);
                        edit.commit();
                        r0 = (C55702oW) r4.A00().get();
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
                this.A00 = Optional.of(r0);
            }
            if (this.A04.Aem(282054797296687L)) {
                synchronized (this) {
                    try {
                        if (this.A00.isPresent()) {
                            long now2 = this.A03.now();
                            Optional optional = this.A00;
                            long j2 = now2 - ((C55702oW) optional.get()).A02;
                            if (j2 >= this.A02) {
                                C55702oW r42 = (C55702oW) optional.get();
                                C31191jI r5 = this.A05;
                                synchronized (r5) {
                                    Preconditions.checkState(r5.A00.BFQ());
                                    C30281hn edit2 = r5.A00.edit();
                                    edit2.C24(r5.A0D);
                                    edit2.commit();
                                }
                                this.A00 = Optional.absent();
                                C21878Ajh markEventBuilder = this.A06.markEventBuilder(46399489, "fetch_efficiency");
                                if (this.A04.Aem(282054797362224L)) {
                                    markEventBuilder.AOA(TraceFieldType.Uri, Math.abs(r42.A03.hashCode()));
                                }
                                markEventBuilder.AOB("tracking_duration", j2);
                                markEventBuilder.AOA("times_requested", r42.A01);
                                markEventBuilder.AOA("content_length", r42.A00);
                                markEventBuilder.AOB("fetch_time_ms", r42.A02);
                                markEventBuilder.AOD("is_prefetch", r42.A0B);
                                markEventBuilder.AOC("fetch_calling_class", r42.A05);
                                markEventBuilder.AOC("fetch_endpoint", r42.A06);
                                Optional optional2 = r42.A04;
                                if (optional2.isPresent()) {
                                    j = ((Long) optional2.get()).longValue();
                                } else {
                                    j = -1;
                                }
                                markEventBuilder.AOB("first_ui_time", j);
                                markEventBuilder.AOC("first_ui_calling_class", r42.A08);
                                markEventBuilder.AOC("first_ui_context_chain", r42.A09);
                                markEventBuilder.AOC("first_ui_endpoint", r42.A0A);
                                markEventBuilder.AOC("first_ui_callback_source", r42.A07);
                                markEventBuilder.C2Q();
                            }
                        }
                    } catch (Throwable th2) {
                        while (true) {
                            th = th2;
                            break;
                        }
                    }
                }
            }
        }
    }

    public void BlR(String str) {
    }

    public void BqL(String str, Object obj) {
    }

    private void A00(Uri uri, String str, Object obj, String str2) {
        C55702oW r0;
        Optional optional = this.A00;
        if (optional.isPresent() && ((C55702oW) optional.get()).A03.equals(uri)) {
            long longValue = ((Long) ((C55702oW) this.A00.get()).A04.or(-1L)).longValue();
            if (longValue > -1) {
                C31191jI r4 = this.A05;
                C55702oW r5 = (C55702oW) this.A00.get();
                Preconditions.checkState(r4.A00.BFQ());
                Preconditions.checkState(r5.A03.toString().equals(r4.A00.B4F(r4.A0C, null)));
                C30281hn edit = r4.A00.edit();
                edit.Bz6(r4.A0B, r5.A01 + 1);
                edit.commit();
                r0 = (C55702oW) r4.A00().get();
            } else {
                C31191jI r3 = this.A05;
                C55702oW r1 = (C55702oW) this.A00.get();
                if (longValue <= -1) {
                    longValue = this.A03.now();
                }
                String A022 = AnonymousClass01P.A02();
                synchronized (r3) {
                    Preconditions.checkState(r3.A00.BFQ());
                    Preconditions.checkState(r1.A03.toString().equals(r3.A00.B4F(r3.A0C, null)));
                    C30281hn edit2 = r3.A00.edit();
                    edit2.BzA(r3.A09, longValue);
                    edit2.BzC(r3.A06, str);
                    edit2.BzC(r3.A07, String.valueOf(obj));
                    edit2.BzC(r3.A08, A022);
                    edit2.Bz6(r3.A0B, 0);
                    edit2.BzC(r3.A05, str2);
                    edit2.commit();
                    r0 = (C55702oW) r3.A00().get();
                }
            }
            this.A00 = Optional.fromNullable(r0);
        }
    }

    public void Bb2(String str, Object obj, C105014zy r7) {
        C35741rk r2;
        AnonymousClass1S9 r6 = (AnonymousClass1S9) obj;
        if (this.A04.Aem(282054797296687L) && (r2 = r6.A00) != null) {
            Object obj2 = r2.A03;
            if (obj2 instanceof CallerContext) {
                CallerContext callerContext = (CallerContext) obj2;
                A00(r2.A02, callerContext.A02, String.valueOf(callerContext.A00), "on-image-drawn");
            }
        }
    }

    public C31211jK(C31141jD r5, C31191jI r6, AnonymousClass06B r7, QuickPerformanceLogger quickPerformanceLogger, C25051Yd r9) {
        this.A07 = r5;
        this.A05 = r6;
        this.A03 = r7;
        this.A06 = quickPerformanceLogger;
        this.A04 = r9;
        this.A02 = r9.At1(563529774203373L, 86400000);
    }

    public void Bm8(AnonymousClass1QK r13) {
        String str;
        String str2;
        super.Bm8(r13);
        AnonymousClass1Q0 r5 = r13.A09;
        CallerContext callerContext = (CallerContext) r13.A0A;
        String str3 = BuildConfig.FLAVOR;
        if (callerContext != null) {
            str = callerContext.A02;
        } else {
            str = str3;
        }
        if (str != null) {
            str3 = String.valueOf(callerContext.A00);
        }
        boolean A08 = r13.A08();
        String str4 = (String) r13.A05.get(1, BuildConfig.FLAVOR);
        if (this.A04.Aem(282054797886516L) && this.A07.BwV(r5, callerContext)) {
            C21878Ajh markEventBuilder = this.A06.markEventBuilder(46399489, "fetch_efficiency_simple_event");
            String valueOf = String.valueOf(r5.A02);
            String str5 = null;
            if (callerContext != null) {
                str2 = callerContext.A02;
            } else {
                str2 = null;
            }
            if (callerContext != null) {
                str5 = String.valueOf(callerContext.A00);
            }
            if (this.A04.Aem(282054797362224L)) {
                markEventBuilder.AOA(TraceFieldType.Uri, Math.abs(valueOf.hashCode()));
            }
            markEventBuilder.AOC("calling_class", str2);
            markEventBuilder.AOC("context_chain", str5);
            markEventBuilder.AOC("origin", str4);
            markEventBuilder.AOD("is_prefetch", A08);
            markEventBuilder.C2Q();
        }
        if (this.A04.Aem(282054797296687L) && !A08 && A01()) {
            A00(r5.A02, str, str3, "on-request-success");
        }
    }
}
