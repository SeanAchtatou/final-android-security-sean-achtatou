package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentMap;

@UserScoped
/* renamed from: X.1HB  reason: invalid class name */
public final class AnonymousClass1HB implements C05460Za {
    private static C05540Zi A04;
    public AnonymousClass0UN A00;
    public UserKey A01 = null;
    private final AnonymousClass0r6 A02;
    private final C14300t0 A03;

    public void clearUserData() {
        this.A01 = null;
    }

    public static final AnonymousClass1HB A00(AnonymousClass1XY r4) {
        AnonymousClass1HB r0;
        synchronized (AnonymousClass1HB.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new AnonymousClass1HB((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (AnonymousClass1HB) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public ImmutableList A01(ThreadSummary threadSummary) {
        User A032;
        if (this.A01 == null) {
            this.A01 = (UserKey) AnonymousClass1XX.A03(AnonymousClass1Y3.B7T, this.A00);
        }
        UserKey userKey = this.A01;
        if (userKey == null) {
            return RegularImmutableList.A02;
        }
        ArrayList arrayList = new ArrayList();
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
            if (!threadParticipant.A00().equals(userKey) && (A032 = this.A03.A03(threadParticipant.A00())) != null && this.A02.A0Z(A032.A0Q)) {
                arrayList.add(A032);
            }
        }
        Collections.sort(arrayList, new AnonymousClass1HU());
        return ImmutableList.copyOf((Collection) arrayList);
    }

    public boolean A02(ThreadSummary threadSummary) {
        C37291v0 r0;
        if (this.A02.A0Y()) {
            if (this.A01 == null) {
                this.A01 = (UserKey) AnonymousClass1XX.A03(AnonymousClass1Y3.B7T, this.A00);
            }
            UserKey userKey = this.A01;
            if (userKey != null) {
                ConcurrentMap concurrentMap = this.A02.A0N;
                C24971Xv it = threadSummary.A0m.iterator();
                while (it.hasNext()) {
                    UserKey A002 = ((ThreadParticipant) it.next()).A00();
                    if (!userKey.equals(A002) && (r0 = (C37291v0) concurrentMap.get(A002)) != null && r0.A0A) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private AnonymousClass1HB(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = C14890uJ.A00(r3);
        this.A03 = C14300t0.A00(r3);
    }
}
