package X;

import android.view.animation.Interpolator;

/* renamed from: X.0DY  reason: invalid class name */
public final class AnonymousClass0DY implements Interpolator {
    public float getInterpolation(float f) {
        return 1.0f - f;
    }
}
