package X;

import android.content.Context;
import android.preference.Preference;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1vS  reason: invalid class name */
public final class AnonymousClass1vS extends C17770zR {
    @Comparable(type = 3)
    public long A00;
    @Comparable(type = 13)
    public Preference A01;
    @Comparable(type = 13)
    public Preference A02;
    @Comparable(type = 13)
    public Preference A03;
    public AnonymousClass0UN A04;
    @Comparable(type = 13)
    public C200059bN A05;
    @Comparable(type = 13)
    public MigColorScheme A06;
    @Comparable(type = 3)
    public boolean A07;
    @Comparable(type = 3)
    public boolean A08;

    public AnonymousClass1vS(Context context) {
        super("M4DataSettingPreferenceLayout");
        this.A04 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }
}
