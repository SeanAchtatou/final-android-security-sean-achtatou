package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Oh  reason: invalid class name and case insensitive filesystem */
public final class C23121Oh {
    public Map A00;
    public boolean A01 = false;
    public final String A02;

    public String A00() {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Map.Entry entry : this.A00.entrySet()) {
            if (i > 0) {
                sb.append(" , ");
            }
            sb.append((String) entry.getKey());
            sb.append(": ");
            sb.append((String) entry.getValue());
            i++;
        }
        return sb.toString();
    }

    public C23121Oh(String str) {
        this.A02 = str;
        this.A00 = new HashMap();
    }
}
