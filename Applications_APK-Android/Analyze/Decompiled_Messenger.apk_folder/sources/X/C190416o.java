package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.16o  reason: invalid class name and case insensitive filesystem */
public final class C190416o extends AnonymousClass0W4 {
    private static final ImmutableList A00;
    private static final String A01;

    static {
        AnonymousClass0W6 r27 = C190516p.A0G;
        AnonymousClass0W6 r28 = C190516p.A08;
        AnonymousClass0W6 r29 = C190516p.A0C;
        A00 = ImmutableList.of(r27, r28, r29, C190516p.A0D, C190516p.A0T, C190516p.A0A, C190516p.A0Z, C190516p.A06, C190516p.A0F, C190516p.A0Y, C190516p.A05, C190516p.A0E, C190516p.A07, C190516p.A0O, C190516p.A0N, C190516p.A0V, C190516p.A00, C190516p.A0X, C190516p.A0P, C190516p.A0b, C190516p.A0a, C190516p.A0U, C190516p.A0J, C190516p.A09, C190516p.A03, C190516p.A04, C190516p.A0Q, C190516p.A0L, C190516p.A0W, C190516p.A0I, C190516p.A01, C190516p.A0S, C190516p.A0H, C190516p.A02, C190516p.A0M, C190516p.A0B, C190516p.A0R, C190516p.A0c, C190516p.A0K);
        A01 = AnonymousClass0W4.A05("contacts", "contact_index_by_fbid", ImmutableList.of(r29));
    }

    public C190416o() {
        super("contacts", A00);
    }

    public void A09(SQLiteDatabase sQLiteDatabase) {
        super.A09(sQLiteDatabase);
        String str = A01;
        C007406x.A00(1549497997);
        sQLiteDatabase.execSQL(str);
        C007406x.A00(1383916523);
    }
}
