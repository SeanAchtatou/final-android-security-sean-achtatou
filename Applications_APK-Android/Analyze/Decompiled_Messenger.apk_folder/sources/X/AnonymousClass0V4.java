package X;

import android.os.HandlerThread;
import java.util.concurrent.ConcurrentLinkedQueue;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0V4  reason: invalid class name */
public final class AnonymousClass0V4 {
    public static final ConcurrentLinkedQueue A02 = new ConcurrentLinkedQueue();
    private static volatile AnonymousClass0V4 A03;
    public AnonymousClass0UN A00;
    public final AnonymousClass0V6 A01 = new AnonymousClass0V5(this);

    public static final AnonymousClass0V4 A01(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0V4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0V4(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public HandlerThread A02(String str, AnonymousClass0V7 r6) {
        return ((AnonymousClass0V8) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ana, this.A00)).A01(str, r6.mAndroidThreadPriority, this.A01);
    }

    private AnonymousClass0V4(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public static final AnonymousClass0V4 A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
