package X;

import com.facebook.litho.LithoView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1ji  reason: invalid class name and case insensitive filesystem */
public final class C31451ji {
    public C31491jm A00;
    public C31491jm A01;
    public C31491jm A02;
    public LithoView A03;
    public Map A04;
    public final List A05 = new ArrayList();
    public final List A06 = new ArrayList();

    public static void A00(C31451ji r4) {
        for (Map.Entry value : r4.A04.entrySet()) {
            C33181nA.A05(((AnonymousClass117) value.getValue()).A08, 0, 0.0f);
        }
        r4.A04.clear();
    }

    public C31451ji(LithoView lithoView) {
        this.A03 = lithoView;
        this.A02 = new C31491jm(lithoView);
        this.A01 = new C31491jm(lithoView);
        this.A00 = new C31491jm(lithoView);
        this.A04 = new HashMap();
    }
}
