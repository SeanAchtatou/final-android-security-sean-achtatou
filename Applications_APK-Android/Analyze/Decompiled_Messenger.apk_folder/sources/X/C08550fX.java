package X;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0fX  reason: invalid class name and case insensitive filesystem */
public abstract class C08550fX implements C26121aw {
    public boolean A00 = false;
    public int A01 = AnonymousClass1Y3.BEZ;
    public Set A02 = null;
    public boolean A03 = false;
    public final List A04 = new ArrayList();
    public final ReadWriteLock A05 = new ReentrantReadWriteLock(true);

    public void A01(Runnable runnable) {
        AnonymousClass07A.A04(((C08540fW) this).A00, runnable, 1419335587);
    }

    public void A02() {
        boolean equals = "1".equals(AnonymousClass00I.A02("facebook.PerfSocketEnabled"));
        this.A03 = equals;
        if (equals) {
            int parseInt = Integer.parseInt(AnonymousClass00I.A02("facebook.PerfSocketNumEvents"));
            this.A02 = new HashSet();
            for (int i = 0; i < parseInt; i++) {
                this.A02.add(AnonymousClass00I.A02(AnonymousClass08S.A09("facebook.PerfSocketEvent", i)));
            }
            this.A01 = Integer.parseInt(AnonymousClass00I.A02("facebook.PerfSocketPort"));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0099, code lost:
        if (r5.A02 == -1) goto L_0x009b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void CMc(com.facebook.quicklog.PerformanceLoggingEvent r18) {
        /*
            r17 = this;
            r3 = r17
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05
            java.util.concurrent.locks.Lock r0 = r0.readLock()
            r0.lock()
            boolean r1 = r3.A00     // Catch:{ all -> 0x01b1 }
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05
            java.util.concurrent.locks.Lock r0 = r0.readLock()
            r0.unlock()
            if (r1 != 0) goto L_0x003e
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05
            java.util.concurrent.locks.Lock r0 = r0.writeLock()
            r0.lock()
            boolean r0 = r3.A00     // Catch:{ all -> 0x002c }
            if (r0 != 0) goto L_0x0035
            r3.A02()     // Catch:{ all -> 0x002c }
            r0 = 1
            r3.A00 = r0     // Catch:{ all -> 0x002c }
            goto L_0x0035
        L_0x002c:
            r1 = move-exception
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05
            java.util.concurrent.locks.Lock r0 = r0.writeLock()
            goto L_0x01b8
        L_0x0035:
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05
            java.util.concurrent.locks.Lock r0 = r0.writeLock()
            r0.unlock()
        L_0x003e:
            boolean r0 = r3.A03
            if (r0 == 0) goto L_0x01b0
            r2 = r18
            java.lang.String r9 = r2.A0M
            if (r9 == 0) goto L_0x0161
        L_0x0048:
            X.2Hd r7 = new X.2Hd
            int r8 = r2.A06
            short r0 = r2.A0R
            java.lang.String r10 = X.AnonymousClass0I1.A00(r0)
            long r11 = r2.A0C
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r0 = r2.A08
            long r0 = r4.toMillis(r0)
            int r13 = (int) r0
            X.1GD r1 = r2.A0K
            java.util.HashMap r14 = new java.util.HashMap
            r14.<init>()
            if (r1 == 0) goto L_0x006e
            X.45o r0 = new X.45o
            r0.<init>(r14)
            r1.A01(r0)
        L_0x006e:
            X.00T r5 = r2.A0F
            if (r5 == 0) goto L_0x015c
            boolean r0 = r5.A0E
            if (r0 == 0) goto L_0x015c
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            long r0 = r5.A06
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "ps_cpu_ms"
            r4.put(r0, r1)
            long r0 = r5.A07
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "ps_flt"
            r4.put(r0, r1)
            boolean r0 = r5.A0E
            if (r0 == 0) goto L_0x009b
            int r6 = r5.A02
            r1 = -1
            r0 = 1
            if (r6 != r1) goto L_0x009c
        L_0x009b:
            r0 = 0
        L_0x009c:
            if (r0 == 0) goto L_0x00b4
            long r0 = r5.A09
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "th_cpu_ms"
            r4.put(r0, r1)
            long r0 = r5.A0A
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "th_flt"
            r4.put(r0, r1)
        L_0x00b4:
            long r0 = r5.A03
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "allocstall"
            r4.put(r0, r1)
            long r0 = r5.A04
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "pages_in"
            r4.put(r0, r1)
            long r0 = r5.A05
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "pages_out"
            r4.put(r0, r1)
            long r0 = r5.A01()
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "avail_disk_spc_kb"
            r4.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r5.A0C
            int r0 = r0.A00
            long r0 = (long) r0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "class_load_attempts"
            r4.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r5.A0C
            int r0 = r0.A01
            long r0 = (long) r0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "class_loads_failed"
            r4.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r5.A0C
            int r0 = r0.A02
            long r0 = (long) r0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "dex_queries"
            r4.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r5.A0C
            int r0 = r0.A04
            long r0 = (long) r0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "locator_assists"
            r4.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r5.A0C
            int r0 = r0.A03
            long r0 = (long) r0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "wrong_dfa_guesses"
            r4.put(r0, r1)
        L_0x0128:
            r14.putAll(r4)
            java.util.ArrayList r15 = new java.util.ArrayList
            java.util.List r0 = r2.A01()
            r15.<init>(r0)
            java.util.ArrayList r1 = new java.util.ArrayList
            java.util.ArrayList r0 = r2.A0P
            r1.<init>(r0)
            r16 = r1
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16)
            X.0lt r4 = r2.A0J
            if (r4 == 0) goto L_0x0150
            java.lang.String r0 = r2.A0M
            if (r0 == 0) goto L_0x0155
        L_0x0148:
            X.4cE r1 = new X.4cE
            r1.<init>(r3, r2, r0)
            r4.A01(r1)
        L_0x0150:
            r4 = 0
            java.util.List r2 = r3.A04
            monitor-enter(r2)
            goto L_0x0169
        L_0x0155:
            int r0 = r2.A06
            java.lang.String r0 = X.C03020Ho.A00(r0)
            goto L_0x0148
        L_0x015c:
            java.util.Map r4 = java.util.Collections.emptyMap()
            goto L_0x0128
        L_0x0161:
            int r0 = r2.A06
            java.lang.String r9 = X.C03020Ho.A00(r0)
            goto L_0x0048
        L_0x0169:
            java.util.List r0 = r3.A04     // Catch:{ all -> 0x01ad }
            r0.add(r7)     // Catch:{ all -> 0x01ad }
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05     // Catch:{ all -> 0x01ad }
            java.util.concurrent.locks.Lock r0 = r0.readLock()     // Catch:{ all -> 0x01ad }
            r0.lock()     // Catch:{ all -> 0x01ad }
            java.util.Set r1 = r3.A02     // Catch:{ all -> 0x01a2 }
            if (r1 == 0) goto L_0x0192
            java.lang.String r0 = "*"
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x01a2 }
            if (r0 != 0) goto L_0x018d
            java.util.Set r1 = r3.A02     // Catch:{ all -> 0x01a2 }
            java.lang.String r0 = r7.A04     // Catch:{ all -> 0x01a2 }
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x01a2 }
            if (r0 == 0) goto L_0x0192
        L_0x018d:
            X.8WF r4 = new X.8WF     // Catch:{ all -> 0x01a2 }
            r4.<init>(r3)     // Catch:{ all -> 0x01a2 }
        L_0x0192:
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05     // Catch:{ all -> 0x01ad }
            java.util.concurrent.locks.Lock r0 = r0.readLock()     // Catch:{ all -> 0x01ad }
            r0.unlock()     // Catch:{ all -> 0x01ad }
            monitor-exit(r2)     // Catch:{ all -> 0x01ad }
            if (r4 == 0) goto L_0x01b0
            r3.A01(r4)
            return
        L_0x01a2:
            r1 = move-exception
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05     // Catch:{ all -> 0x01ad }
            java.util.concurrent.locks.Lock r0 = r0.readLock()     // Catch:{ all -> 0x01ad }
            r0.unlock()     // Catch:{ all -> 0x01ad }
            throw r1     // Catch:{ all -> 0x01ad }
        L_0x01ad:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x01ad }
            throw r0
        L_0x01b0:
            return
        L_0x01b1:
            r1 = move-exception
            java.util.concurrent.locks.ReadWriteLock r0 = r3.A05
            java.util.concurrent.locks.Lock r0 = r0.readLock()
        L_0x01b8:
            r0.unlock()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08550fX.CMc(com.facebook.quicklog.PerformanceLoggingEvent):void");
    }
}
