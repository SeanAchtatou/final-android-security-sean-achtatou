package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1vQ  reason: invalid class name */
public final class AnonymousClass1vQ extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public C99924px A02;
    @Comparable(type = 13)
    public MigColorScheme A03;
    @Comparable(type = 13)
    public String A04;
    @Comparable(type = 3)
    public boolean A05;
    @Comparable(type = 3)
    public boolean A06;

    public AnonymousClass1vQ(Context context) {
        super("M4MontagePreferenceLayout");
        this.A01 = new AnonymousClass0UN(4, AnonymousClass1XX.get(context));
    }
}
