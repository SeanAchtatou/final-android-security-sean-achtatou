package X;

import com.facebook.profilo.ipc.TraceContext;

/* renamed from: X.05l  reason: invalid class name and case insensitive filesystem */
public interface C004605l {
    void Bsc(TraceContext traceContext);

    void Bsd(TraceContext traceContext);

    void onTraceAbort(TraceContext traceContext);

    void onTraceStop(TraceContext traceContext);
}
