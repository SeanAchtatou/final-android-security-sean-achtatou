package X;

import com.facebook.messaging.notify.MessengerRoomInviteReminderNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.17I  reason: invalid class name */
public final class AnonymousClass17I extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$26";
    public final /* synthetic */ MessengerRoomInviteReminderNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass17I(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, MessengerRoomInviteReminderNotification messengerRoomInviteReminderNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = messengerRoomInviteReminderNotification;
    }
}
