package X;

/* renamed from: X.0Ck  reason: invalid class name and case insensitive filesystem */
public final class C01990Ck {
    public int A00;
    public boolean A01;
    public final int A02;
    public final AnonymousClass0CL A03;
    public final boolean A04;

    public C01990Ck(AnonymousClass0CL r2) {
        this.A03 = r2;
        this.A04 = false;
        this.A02 = 0;
        this.A01 = false;
        this.A00 = 0;
    }

    public C01990Ck(AnonymousClass0CL r2, int i) {
        this.A03 = r2;
        this.A04 = false;
        this.A02 = i;
        this.A01 = false;
        this.A00 = 0;
    }

    public C01990Ck(AnonymousClass0CL r1, boolean z, int i, boolean z2, int i2) {
        this.A03 = r1;
        this.A04 = z;
        this.A02 = i;
        this.A01 = z2;
        this.A00 = i2;
    }
}
