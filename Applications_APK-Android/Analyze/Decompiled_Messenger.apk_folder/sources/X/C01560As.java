package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0As  reason: invalid class name and case insensitive filesystem */
public final class C01560As extends BroadcastReceiver {
    public final /* synthetic */ C01530Ap A00;

    public C01560As(C01530Ap r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(613356678);
        if (!AnonymousClass0A2.A00(intent.getAction(), this.A00.A01)) {
            C000700l.A0D(intent, -33443813, A01);
            return;
        }
        C01530Ap.A02(this.A00);
        C000700l.A0D(intent, 594548573, A01);
    }
}
