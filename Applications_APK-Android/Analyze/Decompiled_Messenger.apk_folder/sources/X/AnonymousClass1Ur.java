package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1Ur  reason: invalid class name */
public abstract class AnonymousClass1Ur implements C06670bt, OmnistoreComponent, AnonymousClass062 {
    public static final Class A0A = AnonymousClass1Ur.class;
    public C04440Ur A00;
    public AnonymousClass09P A01;
    public C25051Yd A02;
    public Collection A03;
    public C24341Tf A04;
    public ExecutorService A05;
    public C04310Tq A06;
    private boolean A07 = false;
    public final ArrayList A08 = new ArrayList();
    public final HashMap A09 = new HashMap();

    public long A01() {
        return !(this instanceof AnonymousClass1Us) ? 285177238656441L : 285177238525367L;
    }

    public CollectionName A02(Omnistore omnistore) {
        if (!(this instanceof AnonymousClass1Us)) {
            AnonymousClass1Uq r2 = (AnonymousClass1Uq) this;
            CollectionName.Builder createCollectionNameBuilder = omnistore.createCollectionNameBuilder(r2.getCollectionLabel());
            createCollectionNameBuilder.addSegment((String) r2.A06.get());
            return createCollectionNameBuilder.build();
        }
        AnonymousClass1Us r22 = (AnonymousClass1Us) this;
        CollectionName.Builder createCollectionNameWithDomainBuilder = omnistore.createCollectionNameWithDomainBuilder(r22.getCollectionLabel(), "messenger_user_sq");
        createCollectionNameWithDomainBuilder.addSegment((String) r22.A06.get());
        return createCollectionNameWithDomainBuilder.build();
    }

    public String A03() {
        return !(this instanceof AnonymousClass1Us) ? "com.facebook.fb4a.ACTION_OMNISTORE_USER_PREFS_UPDATED" : C06680bu.A0j;
    }

    public void Boz(int i) {
    }

    public String getCollectionLabel() {
        return !(this instanceof AnonymousClass1Us) ? "facebook_universal_prefs_v2" : "messenger_universal_prefs";
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:21|(2:23|24)|25|26) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0060, code lost:
        if (r3 != null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0065 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onCollectionAvailable(com.facebook.omnistore.Collection r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r4.A03 = r5     // Catch:{ all -> 0x0072 }
            java.util.ArrayList r0 = r4.A08     // Catch:{ all -> 0x0072 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0072 }
        L_0x0009:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x001b
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0072 }
            X.2KF r1 = (X.AnonymousClass2KF) r1     // Catch:{ all -> 0x0072 }
            com.facebook.omnistore.Collection r0 = r4.A03     // Catch:{ all -> 0x0072 }
            r1.A00(r0)     // Catch:{ all -> 0x0072 }
            goto L_0x0009
        L_0x001b:
            java.util.ArrayList r0 = r4.A08     // Catch:{ all -> 0x0072 }
            r0.clear()     // Catch:{ all -> 0x0072 }
            java.util.HashMap r0 = r4.A09     // Catch:{ all -> 0x0072 }
            r0.size()     // Catch:{ all -> 0x0072 }
            java.util.HashMap r0 = r4.A09     // Catch:{ all -> 0x0072 }
            r0.clear()     // Catch:{ all -> 0x0072 }
            com.facebook.omnistore.Collection r3 = r4.A03     // Catch:{ all -> 0x0072 }
            if (r3 == 0) goto L_0x0037
            java.lang.String r2 = ""
            r1 = -1
            r0 = 1
            com.facebook.omnistore.Cursor r3 = r3.query(r2, r1, r0)     // Catch:{ all -> 0x0072 }
            goto L_0x0038
        L_0x0037:
            r3 = 0
        L_0x0038:
            if (r3 == 0) goto L_0x0066
            boolean r0 = r3.step()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0066
            java.nio.ByteBuffer r1 = r3.getBlob()     // Catch:{ all -> 0x005d }
            if (r1 != 0) goto L_0x004a
            r3.getPrimaryKey()     // Catch:{ all -> 0x005d }
            goto L_0x0038
        L_0x004a:
            int r0 = r1.remaining()     // Catch:{ all -> 0x005d }
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x005d }
            r1.get(r2)     // Catch:{ all -> 0x005d }
            java.util.HashMap r1 = r4.A09     // Catch:{ all -> 0x005d }
            java.lang.String r0 = r3.getPrimaryKey()     // Catch:{ all -> 0x005d }
            r1.put(r0, r2)     // Catch:{ all -> 0x005d }
            goto L_0x0038
        L_0x005d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005f }
        L_0x005f:
            r0 = move-exception
            if (r3 == 0) goto L_0x0065
            r3.close()     // Catch:{ all -> 0x0065 }
        L_0x0065:
            throw r0     // Catch:{ all -> 0x0072 }
        L_0x0066:
            if (r3 == 0) goto L_0x006b
            r3.close()     // Catch:{ all -> 0x0072 }
        L_0x006b:
            java.util.HashMap r0 = r4.A09     // Catch:{ all -> 0x0072 }
            r0.size()     // Catch:{ all -> 0x0072 }
            monitor-exit(r4)
            return
        L_0x0072:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ur.onCollectionAvailable(com.facebook.omnistore.Collection):void");
    }

    public synchronized void onCollectionInvalidated() {
        this.A03 = null;
        this.A08.clear();
        this.A09.clear();
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public int Ai5() {
        return AnonymousClass1Y3.A6u;
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    public void BTE(int i) {
        boolean Aer = this.A02.Aer(A01(), AnonymousClass0XE.A05);
        this.A07 = Aer;
        if (Aer) {
            this.A04.A02(this);
        }
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        CollectionName A022;
        if ((this.A02.Aem(A01()) || this.A07) && (A022 = A02(omnistore)) != null) {
            return C32171lK.A00(A022, null);
        }
        return C32171lK.A03;
    }

    public AnonymousClass1Ur(Context context) {
        AnonymousClass1XX r1 = AnonymousClass1XX.get(context);
        this.A00 = C04440Ur.A00(r1);
        this.A06 = AnonymousClass0XJ.A0L(r1);
        this.A01 = C04750Wa.A01(r1);
        this.A05 = AnonymousClass0UX.A0W(r1);
        this.A04 = C24341Tf.A00(r1);
        this.A02 = AnonymousClass0WT.A00(r1);
    }

    public void BVs(List list) {
        list.size();
        ArrayList arrayList = new ArrayList();
        synchronized (AnonymousClass1Ur.class) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Delta delta = (Delta) it.next();
                int type = delta.getType();
                if (type == 1) {
                    ByteBuffer blob = delta.getBlob();
                    if (blob == null) {
                        delta.getPrimaryKey();
                    } else {
                        byte[] bArr = new byte[blob.remaining()];
                        blob.get(bArr);
                        this.A09.put(delta.getPrimaryKey(), bArr);
                    }
                } else if (type == 2) {
                    this.A09.remove(delta.getPrimaryKey());
                }
                arrayList.add(delta.getPrimaryKey());
            }
        }
        Intent intent = new Intent(A03());
        intent.putExtra(AnonymousClass80H.$const$string(AnonymousClass1Y3.A59), arrayList);
        this.A00.C4x(intent);
    }
}
