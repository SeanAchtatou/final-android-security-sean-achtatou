package X;

import android.content.Context;

/* renamed from: X.0B1  reason: invalid class name */
public final class AnonymousClass0B1 extends C01620Ay {
    public String A00() {
        return "WorkConnectionConfigOverrides";
    }

    public String A01() {
        return "com.facebook.rti.mqtt.ACTION_WORK_SWITCH";
    }

    public void A04(String str, String str2) {
        AnonymousClass0DD AY8 = this.A05.AbP(AnonymousClass07B.A07).AY8();
        AY8.BzD("work_last_host", str);
        AY8.BzD("work_last_analytics_endpoint", str2);
        AY8.commit();
    }

    public boolean A05(String str) {
        if (str == null || str.endsWith(".facebook.com") || str.endsWith(".workplace.com")) {
            return true;
        }
        return false;
    }

    public AnonymousClass0B1(Context context, C008007h r5, Integer num, AnonymousClass09P r7, AnonymousClass0AW r8) {
        super(context, r5, num, r7, r8);
        AnonymousClass0B0 AbP = this.A05.AbP(AnonymousClass07B.A07);
        this.A07 = AbP.getString("work_last_host", null);
        this.A06 = AbP.getString("work_last_analytics_endpoint", null);
    }

    public void A02() {
    }
}
