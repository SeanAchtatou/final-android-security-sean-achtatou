package X;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;

/* renamed from: X.1L9  reason: invalid class name */
public final class AnonymousClass1L9 {
    public Object A00;

    public static AnonymousClass1L9 A00(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 24) {
            return new AnonymousClass1L9(PointerIcon.getSystemIcon(context, i));
        }
        return new AnonymousClass1L9(null);
    }

    private AnonymousClass1L9(Object obj) {
        this.A00 = obj;
    }
}
