package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.1jy  reason: invalid class name and case insensitive filesystem */
public final class C31611jy implements AnonymousClass0ZM {
    public final /* synthetic */ C31601jx A00;

    public C31611jy(C31601jx r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r3) {
        C31601jx.A01(this.A00);
    }
}
