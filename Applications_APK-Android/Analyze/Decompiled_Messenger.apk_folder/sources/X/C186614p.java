package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.facebook.messaging.onboarding.OnboardingActivity;

/* renamed from: X.14p  reason: invalid class name and case insensitive filesystem */
public final class C186614p implements C186714q {
    private final C31791kO A00;

    public boolean CE5() {
        return false;
    }

    public static final C186614p A00(AnonymousClass1XY r2) {
        return new C186614p(new C31791kO(r2));
    }

    public Integer AeZ() {
        return AnonymousClass07B.A0i;
    }

    public Intent AqX(Activity activity) {
        return new Intent(activity, OnboardingActivity.class);
    }

    public boolean BDw(Context context) {
        return this.A00.A02();
    }

    public boolean CEE(Activity activity) {
        if (!(activity instanceof C27891dv)) {
            if (!C138546dM.class.isAssignableFrom(activity.getClass())) {
                return true;
            }
        }
        return false;
    }

    private C186614p(C31791kO r1) {
        this.A00 = r1;
    }
}
