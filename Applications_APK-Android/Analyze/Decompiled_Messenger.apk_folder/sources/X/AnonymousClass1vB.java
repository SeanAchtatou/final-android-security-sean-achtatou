package X;

import android.content.Context;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.montage.forked.model.viewer.PollVoteResults;
import com.facebook.messaging.montage.forked.viewer.store.StoryFeedbackStore;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.Map;

/* renamed from: X.1vB  reason: invalid class name */
public final class AnonymousClass1vB extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public StoryCard A01;
    @Comparable(type = 13)
    public C88334Jw A02;
    @Comparable(type = 13)
    public C95604gh A03;
    @Comparable(type = 13)
    public AnonymousClass46A A04;
    public C04310Tq A05;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1vB(Context context) {
        super("RatingStickerComponent");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A00 = new AnonymousClass0UN(8, r2);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ayx, r2);
    }

    public static void A00(GSTModelShape1S0000000 gSTModelShape1S0000000, String str, StoryFeedbackStore storyFeedbackStore, C112205Vu r9, boolean z) {
        float f;
        int i;
        ImmutableList A3b = gSTModelShape1S0000000.A3b();
        Object obj = storyFeedbackStore.A03.get(str);
        Preconditions.checkNotNull(obj);
        ImmutableList immutableList = ((PollVoteResults) obj).A01;
        int A012 = C90304Sw.A01(immutableList);
        Map A052 = C90304Sw.A05(immutableList);
        int i2 = 0;
        int i3 = 0;
        while (i2 < A3b.size()) {
            Integer valueOf = Integer.valueOf(i2);
            if (A052.containsKey(valueOf)) {
                i = ((Integer) A052.get(valueOf)).intValue();
            } else {
                i = 0;
            }
            i2++;
            i3 += i * i2;
        }
        if (i3 == 0 || A012 == 0) {
            r9.A05(false);
        }
        double d = ((double) i3) / ((double) A012);
        if (d < 1.0d || d > 5.0d) {
            r9.A05(false);
            return;
        }
        double round = ((double) Math.round(d * 2.0d)) / 2.0d;
        if (r9.A01 != 3) {
            int i4 = (int) round;
            boolean z2 = false;
            if (i4 == 5) {
                f = 1.0f;
                C112205Vu.A04(r9, false, 5.0d);
            } else {
                if (((int) (2.0d * round)) % 2 == 1) {
                    z2 = true;
                }
                float[] fArr = r9.A0A;
                if (z2) {
                    f = fArr[i4];
                } else {
                    f = (fArr[i4 - 1] + fArr[i4]) / 2.0f;
                }
                C112205Vu.A04(r9, z2, round);
            }
            if (z) {
                float f2 = r9.A00;
                AnonymousClass3E6 r2 = r9.A09;
                r2.A04((double) f2);
                r2.A05((double) f);
                return;
            }
            r9.A00 = f;
            r9.invalidateSelf();
        }
    }
}
