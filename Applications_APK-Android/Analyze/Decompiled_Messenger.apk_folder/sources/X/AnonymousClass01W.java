package X;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.DeadObjectException;

/* renamed from: X.01W  reason: invalid class name */
public class AnonymousClass01W {
    private final ApplicationInfo A00;
    private final PackageManager A01;

    public static Drawable A00(AnonymousClass01W r3, String str) {
        try {
            return r3.A01.getApplicationIcon(str);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                return null;
            }
            throw e;
        }
    }

    public ApplicationInfo A03(String str, int i) {
        try {
            return this.A01.getApplicationInfo(str, i);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                return null;
            }
            throw e;
        }
    }

    public PackageInfo A05(String str, int i) {
        try {
            return this.A01.getPackageInfo(str, i);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                return null;
            }
            throw e;
        }
    }

    public static boolean A01(AnonymousClass01W r3, ApplicationInfo applicationInfo) {
        int i = r3.A00.uid;
        int i2 = applicationInfo.uid;
        if (i == i2 || r3.A01.checkSignatures(i, i2) == 0) {
            return true;
        }
        return false;
    }

    public PackageInfo A06(String str, int i) {
        if (str.startsWith("com.facebook.")) {
            return A04(str, i);
        }
        return A05(str, i);
    }

    public AnonymousClass01W(PackageManager packageManager, ApplicationInfo applicationInfo) {
        this.A01 = packageManager;
        this.A00 = applicationInfo;
    }

    public PackageInfo A04(String str, int i) {
        PackageInfo A05 = A05(str, i);
        if (A05 == null || !A01(this, A05.applicationInfo)) {
            return null;
        }
        return A05;
    }
}
