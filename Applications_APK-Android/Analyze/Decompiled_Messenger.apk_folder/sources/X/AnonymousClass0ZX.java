package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ZX  reason: invalid class name */
public final class AnonymousClass0ZX extends AnonymousClass0Wl {
    private static volatile AnonymousClass0ZX A01;
    private AnonymousClass0UN A00;

    public String getSimpleName() {
        return "INeedInitForGatekeepersListenerRegister";
    }

    public static final AnonymousClass0ZX A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0ZX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0ZX(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass0ZX(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public void init() {
        int A03 = C000700l.A03(1187644812);
        int i = AnonymousClass1Y3.AAA;
        AnonymousClass0UN r3 = this.A00;
        ((C25391Zl) AnonymousClass1XX.A02(0, i, r3)).A02((C05000Xg) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ApT, r3));
        C000700l.A09(-1297582375, A03);
    }
}
