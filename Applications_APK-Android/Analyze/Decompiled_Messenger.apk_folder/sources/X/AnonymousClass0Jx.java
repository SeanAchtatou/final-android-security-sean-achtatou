package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0Jx  reason: invalid class name */
public final class AnonymousClass0Jx implements FilenameFilter {
    public boolean accept(File file, String str) {
        return str.endsWith("_native");
    }
}
