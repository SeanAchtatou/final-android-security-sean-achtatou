package X;

import android.content.Context;

/* renamed from: X.1Oy  reason: invalid class name and case insensitive filesystem */
public final class C23291Oy implements C23111Og {
    public final /* synthetic */ Context A00;

    public C23291Oy(Context context) {
        this.A00 = context;
    }

    public Object get() {
        return this.A00.getFilesDir();
    }
}
