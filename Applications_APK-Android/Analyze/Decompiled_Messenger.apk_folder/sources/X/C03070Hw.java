package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.push.fbns.ipc.FbnsAIDLRequest;

/* renamed from: X.0Hw  reason: invalid class name and case insensitive filesystem */
public final class C03070Hw implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new FbnsAIDLRequest(parcel);
    }

    public Object[] newArray(int i) {
        return new FbnsAIDLRequest[i];
    }
}
