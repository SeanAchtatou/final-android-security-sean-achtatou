package X;

/* renamed from: X.11L  reason: invalid class name */
public final class AnonymousClass11L {
    public final byte[][] _byteBuffers = new byte[AnonymousClass11M.values().length][];
    public final char[][] _charBuffers = new char[AnonymousClass11P.values().length][];

    public final char[] allocCharBuffer(AnonymousClass11P r5, int i) {
        int i2 = r5.size;
        if (i2 > i) {
            i = i2;
        }
        int ordinal = r5.ordinal();
        char[][] cArr = this._charBuffers;
        char[] cArr2 = cArr[ordinal];
        if (cArr2 == null || cArr2.length < i) {
            return new char[i];
        }
        cArr[ordinal] = null;
        return cArr2;
    }

    public final byte[] allocByteBuffer(AnonymousClass11M r5) {
        int ordinal = r5.ordinal();
        byte[][] bArr = this._byteBuffers;
        byte[] bArr2 = bArr[ordinal];
        if (bArr2 == null) {
            return new byte[r5.size];
        }
        bArr[ordinal] = null;
        return bArr2;
    }
}
