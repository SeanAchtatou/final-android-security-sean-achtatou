package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threadkey.ThreadKey;

/* renamed from: X.1fE  reason: invalid class name and case insensitive filesystem */
public final class C28701fE implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadKey(C28711fF.valueOf(parcel.readString()), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong());
    }

    public Object[] newArray(int i) {
        return new ThreadKey[i];
    }
}
