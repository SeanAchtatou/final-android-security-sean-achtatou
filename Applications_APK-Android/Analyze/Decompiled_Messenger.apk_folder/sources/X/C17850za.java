package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0za  reason: invalid class name and case insensitive filesystem */
public abstract class C17850za extends C17760zQ {
    public final ArrayList A00 = new ArrayList();

    public C70253aL A04(List list) {
        return new C80303sD(((C21661If) this).A00, list);
    }

    private void A03(C17760zQ r4) {
        if (r4 instanceof C31441jh) {
            C31441jh r42 = (C31441jh) r4;
            r42.A00();
            ArrayList arrayList = r42.A05;
            if (arrayList.size() > 1) {
                this.A00.add(new C21661If(arrayList));
            } else {
                this.A00.add(arrayList.get(0));
            }
        } else if (r4 != null) {
            this.A00.add(r4);
        } else {
            throw new IllegalStateException("Null element is not allowed in transition set");
        }
    }

    public C17850za(List list) {
        for (int i = 0; i < list.size(); i++) {
            A03((C17760zQ) list.get(i));
        }
    }

    public C17850za(C17760zQ... r3) {
        for (C17760zQ A03 : r3) {
            A03(A03);
        }
    }
}
