package X;

import android.content.Intent;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

/* renamed from: X.20m  reason: invalid class name and case insensitive filesystem */
public final class C401520m {
    public final DeprecatedAnalyticsLogger A00;
    public final C07150ci A01;
    public final C67553Ph A02;
    private final C001500z A03;
    private final AnonymousClass7XJ A04;
    private final Executor A05;

    public static final C401520m A00(AnonymousClass1XY r1) {
        return new C401520m(r1);
    }

    private C401520m(AnonymousClass1XY r2) {
        this.A01 = C07150ci.A00(r2);
        this.A02 = C67553Ph.A01(r2);
        this.A04 = AnonymousClass7XJ.A03(r2);
        this.A05 = AnonymousClass0UX.A0U(r2);
        this.A00 = C06920cI.A00(r2);
        this.A03 = AnonymousClass0UU.A05(r2);
    }

    public static ListenableFuture A01(C401520m r7, Intent intent, String str, String str2) {
        NavigationTrigger A002;
        C07150ci.A01(intent);
        ThreadKey A022 = r7.A01.A02(intent);
        boolean equals = "direct_reply_intent".equals(str2);
        if (A022 == null) {
            C010708t.A0K("RemoteInputReplyIntentHandler", "Failed to handle direct reply due to null thread key.");
            C11670nb r2 = new C11670nb(C05360Yq.$const$string(374));
            r2.A0D("error", "empty thread key");
            r7.A00.A09(r2);
            return C05350Yp.A03(Boolean.FALSE);
        }
        if (equals) {
            r7.A02.A03(new C51092fR(2131824010));
        }
        Message A0K = r7.A04.A0K(A022, Long.toString(((C415526a) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BRT, r7.A01.A00)).A01()), str);
        C07150ci r22 = r7.A01;
        if (r7.A03 == C001500z.A08) {
            A002 = NavigationTrigger.A02("pages_manager_app:notification_direct_reply_text", null);
        } else {
            A002 = NavigationTrigger.A00(str2);
        }
        ListenableFuture A032 = r22.A03(A0K, A002, C53972lf.A0y);
        C05350Yp.A08(A032, new AnonymousClass4WI(r7, equals, A022), r7.A05);
        return A032;
    }
}
