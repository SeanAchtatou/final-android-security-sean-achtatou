package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.1fy  reason: invalid class name and case insensitive filesystem */
public final class C29161fy implements Iterator, Iterable {
    private final Object[] _array;
    private int _index = 0;

    public Iterator iterator() {
        return this;
    }

    public boolean hasNext() {
        if (this._index < this._array.length) {
            return true;
        }
        return false;
    }

    public Object next() {
        int i = this._index;
        Object[] objArr = this._array;
        if (i < objArr.length) {
            this._index = i + 1;
            return objArr[i];
        }
        throw new NoSuchElementException();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public C29161fy(Object[] objArr) {
        this._array = objArr;
    }
}
