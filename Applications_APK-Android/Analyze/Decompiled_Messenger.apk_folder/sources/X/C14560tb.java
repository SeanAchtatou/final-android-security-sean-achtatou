package X;

import com.facebook.zero.rewritenative.ZeroNativeRequestInterceptor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tb  reason: invalid class name and case insensitive filesystem */
public final class C14560tb implements C06670bt {
    private static volatile C14560tb A03;
    public ZeroNativeRequestInterceptor A00 = null;
    private AnonymousClass0UN A01;
    private C31871ka A02 = null;

    public static final C14560tb A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C14560tb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C14560tb(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public C31871ka A01() {
        if (this.A02 == null) {
            this.A02 = new C31871ka((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A01));
        }
        return this.A02;
    }

    public int Ai5() {
        return AnonymousClass1Y3.ACl;
    }

    public void BTE(int i) {
        ZeroNativeRequestInterceptor zeroNativeRequestInterceptor = this.A00;
        if (zeroNativeRequestInterceptor != null) {
            zeroNativeRequestInterceptor.gqlConfigUpdated(A01());
        }
    }

    private C14560tb(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(1, r3);
    }
}
