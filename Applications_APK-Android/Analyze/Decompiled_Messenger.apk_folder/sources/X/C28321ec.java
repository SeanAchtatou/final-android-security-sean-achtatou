package X;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* renamed from: X.1ec  reason: invalid class name and case insensitive filesystem */
public final class C28321ec {
    public final Type _actualType;
    public final ParameterizedType _genericType;
    public final Class _rawClass;
    public C28321ec _subType;
    public C28321ec _superType;

    public C28321ec deepCloneWithoutSubtype() {
        C28321ec deepCloneWithoutSubtype;
        C28321ec r0 = this._superType;
        if (r0 == null) {
            deepCloneWithoutSubtype = null;
        } else {
            deepCloneWithoutSubtype = r0.deepCloneWithoutSubtype();
        }
        C28321ec r02 = new C28321ec(this._actualType, this._rawClass, this._genericType, deepCloneWithoutSubtype, null);
        if (deepCloneWithoutSubtype != null) {
            deepCloneWithoutSubtype._subType = r02;
        }
        return r02;
    }

    public String toString() {
        ParameterizedType parameterizedType = this._genericType;
        if (parameterizedType != null) {
            return parameterizedType.toString();
        }
        return this._rawClass.getName();
    }

    public C28321ec(Type type) {
        this._actualType = type;
        if (type instanceof Class) {
            this._rawClass = (Class) type;
            this._genericType = null;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            this._genericType = parameterizedType;
            this._rawClass = (Class) parameterizedType.getRawType();
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0P("Type ", type.getClass().getName(), " can not be used to construct HierarchicType"));
        }
    }

    private C28321ec(Type type, Class cls, ParameterizedType parameterizedType, C28321ec r4, C28321ec r5) {
        this._actualType = type;
        this._rawClass = cls;
        this._genericType = parameterizedType;
        this._superType = r4;
        this._subType = r5;
    }
}
