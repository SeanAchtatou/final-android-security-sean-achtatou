package X;

import android.content.Context;
import android.content.Intent;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.0uW  reason: invalid class name and case insensitive filesystem */
public final class C14980uW implements C15080uh {
    public void Bl3(Collection collection, Context context, Intent intent) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C101624ss) it.next()).BjK(intent);
        }
    }
}
