package X;

import android.view.View;

/* renamed from: X.121  reason: invalid class name */
public final class AnonymousClass121 implements AnonymousClass1M8 {
    public String getName() {
        return "alpha";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (r1 == false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float Ab1(X.C17730zN r5) {
        /*
            r4 = this;
            X.1jd r3 = r5.A0A
            if (r3 == 0) goto L_0x0010
            int r2 = r3.A0A
            r0 = 1048576(0x100000, float:1.469368E-39)
            r2 = r2 & r0
            r1 = 0
            if (r2 == 0) goto L_0x000d
            r1 = 1
        L_0x000d:
            r0 = 1
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            if (r0 == 0) goto L_0x0018
            if (r3 == 0) goto L_0x0018
            float r0 = r3.A00
            return r0
        L_0x0018:
            r0 = 1065353216(0x3f800000, float:1.0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass121.Ab1(X.0zN):float");
    }

    public float Ab2(Object obj) {
        if (obj instanceof View) {
            return ((View) obj).getAlpha();
        }
        throw new UnsupportedOperationException("Tried to get alpha of unsupported mount content: " + obj);
    }

    public void C3Y(Object obj) {
        C5f(obj, 1.0f);
    }

    public void C5f(Object obj, float f) {
        if (obj instanceof View) {
            ((View) obj).setAlpha(f);
            return;
        }
        throw new UnsupportedOperationException("Setting alpha on unsupported mount content: " + obj);
    }
}
