package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0TN  reason: invalid class name */
public final class AnonymousClass0TN {
    public static final AnonymousClass0TO A00 = new AnonymousClass0TO("fbandroid_family_release", "JDi84d23vQJtX_ifWYs7Xlu4JLM", "gHUKrFoWceNwTt-LAmskjOi6tbjZz0g1PCk6sS5ZNZo");
    public static final AnonymousClass0TO A01 = new AnonymousClass0TO("fbandroid_debug", TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0z), "-sYXRdwJA3hvue3mKpYrOZ9zSPC7b4mbgzJmdZEDO5w");
    public static final AnonymousClass0TO A02 = new AnonymousClass0TO("fbandroid_inhouse_v1", "pLdFLi7Y9fGRBYynu_0msNMhS_w", "Ep3FNIDivxtVz27sIF_g9Z0rn4Hn7k-2fOy77iWgsIA");
    public static final AnonymousClass0TO A03 = new AnonymousClass0TO("fbandroid_inhouse_v2", "RkHFCTArxterQ7h9g2sQjVr4Ej0", "wDby32gn_uCqMVAmAc62_hOfNu_VSqMa5uyB5sNI4dk");
    public static final AnonymousClass0TO A04 = new AnonymousClass0TO("fbandroid_inhousev2_debug", "pGLr1wNX3vElqWNF4QYKaubskS4", "jtZAS_lKBE3WXzNRjrPIcqOuRU4Dh_MGihoNLNw3CKA");
    public static final AnonymousClass0TO A05 = new AnonymousClass0TO("fbandroid_release", "ijxLJi1yGs1JpL-X1SExmchvork", "4_nh4M-Z0OVqBVumXiQbM5n3zqUkMmsM3W7BMn7Q_cE");
    public static final AnonymousClass0TO A06 = new AnonymousClass0TO("fbandroid_samples", "aIIMIXiHjw__OMc2ophMdya2vHQ", "qadsRSPy8q2oOtZucRAyNlbBCyrbDQYWnD6ZESwR0vs");
    public static final AnonymousClass0TO A07 = new AnonymousClass0TO("fbandroid_samples", "BcapvdaWLq6ZfAglJbxXazMNBFU", "jW-jYWbZ4GQZ28iGykpgUFoIDlGPXHb2sIWpDljhlYw");
    public static final AnonymousClass0TO A08 = new AnonymousClass0TO("games_release", "nFGQkG3LpFTag1g8R3ppoA0_NjA", "cUlH0jQYllHE5u0OaOFfOj_2ZPBtYk1dZtKmV4QmWOY");
    public static final AnonymousClass0TO A09 = new AnonymousClass0TO("instagram_release", TurboLoader.Locator.$const$string(178), "Xz5Q9DVYPJrmJjAqcfc0AEQIen4sYK2s_CVCBamT4wU");
    public static final AnonymousClass0TO A0A = new AnonymousClass0TO("kototoro_release", "GoRS3a2OGuGeDTeVe6y0xFgcNbo", "wkocCWOvIvFGAvqCNK7NFD1RWuuKWGrar7D70A1eTlQ");
    public static final AnonymousClass0TO A0B = new AnonymousClass0TO("oculus_apps_release", "MxZgtt071YLz39PLrkVGckZooCE", "3C-DTjwZQjeLFPB2yCzRq1nqZWa_9HMLIEfpjvyfna8");
    public static final AnonymousClass0TO A0C = new AnonymousClass0TO("oculus_core_apps_release", "mKGAnzw-eRcOXfgTW1AOz6Od8b4", "AS-BZcGR7-j7DW1GBqpKusFNd9HZfVNAhgyfgQRaoVc");
    public static final AnonymousClass0TO A0D = new AnonymousClass0TO("oculus_prod_release", "Sr9mhPKOEwo6NysnYn803dZ3UiY", "ZSEpCPBvG4p97Oj7FYJ_pqcOLvWpiwfdZ4BcNkPF08M");
    public static final AnonymousClass0TO A0E = new AnonymousClass0TO("oculus_system_debug", "JxluOGuHXnat9wDn6oTkxu7jPfo", "yKLpvM9ZfC-23Ga-4pP8E_L8R-x3vGsrDVLBH1EZKrg");
    public static final AnonymousClass0TO A0F = new AnonymousClass0TO("oxygen_debug", "7XE60X540nq3JXIiFpcVSgM8diY", "Jm4bl26QMphvIVgzVUeQb6f37Ys3IKRmCw0LBgLJBzs");
    public static final AnonymousClass0TO A0G = new AnonymousClass0TO("oxygen_release", "e6fv6XFRr-tXEDJmsSANhagF19Y", "uSEJtZCVlVzKr17Lzw8VPslkCkZYwQFmetlrfkmaQJI");
    public static final AnonymousClass0TO A0H = new AnonymousClass0TO("portal_frameworks", "OO66WgNh5QhN2YJvbm7WsMFraIk", "ZiIZ7fdUdXcbGMNL6M656x4mTCC9kdSoSNBOifLrBAA");
    public static final AnonymousClass0TO A0I = new AnonymousClass0TO("portal_frameworks_debug", "qvsjgntAXl2NeUeJG7WcMOO67_U", "_7txYJOR7-3x7Od7kFQdFwsbKaGzch2f0hsTE8UtSZo");
    public static final AnonymousClass0TO A0J = new AnonymousClass0TO("aloha_prod", "5IMQjt1hTC_xqzPM0QypzwhjD9g", "T4Q-4dJKibgNvwschNsyH9YBK3Hwl5zTIkQlBgZu10E");
    public static final AnonymousClass0TO A0K = new AnonymousClass0TO("portal_1p_nonprivapps", "d9u8HZP0Sr8IRZcknkT90o1dA4I", "JnN9N-awG-f7YQ7ROKJoQilHd7EWI2fcuf7cVk8IwbE");
    public static final AnonymousClass0TO A0L = new AnonymousClass0TO("portal_1p_nonprivapps_debug", "aIIMIXiHjw__OMc2ophMdya2vHQ", "qadsRSPy8q2oOtZucRAyNlbBCyrbDQYWnD6ZESwR0vs");
    public static final AnonymousClass0TO A0M = new AnonymousClass0TO("portal_1p_privapps", "ayxL3GZmow1e3H96rZTKomVeN6k", "2IY_S9EZqWwT_9dcezZrynlWkcjV0xIOItAdj_LCCzc");
    public static final AnonymousClass0TO A0N = new AnonymousClass0TO("portal_1p_privapps_debug", "FRNYKa3Xwpy4PBUuvctQLZyChQw", "ztXcjgEmmxMKYWXyXR1OtAW6codwAh6kiOzYzpxMCM4");
    public static final AnonymousClass0TO A0O = new AnonymousClass0TO("portal_2p", "wcJiR08sDZHVuZ_UeU3M5Lfj1lU", "VKYCWsqDaYYL11gG_pE8Boqff1awXJofnT5IhJbw6uk");
    public static final AnonymousClass0TO A0P = new AnonymousClass0TO("portal_2p_debug", "RAVUNRBo-sqseoeNZBd_uekYbJM", "8HU_MHdtxR7HUwGeKgfv4kXzi2-XbN5whjV851JZJGU");
    public static final AnonymousClass0TO A0Q = new AnonymousClass0TO("portal_companionapps", "X64eihZAou6lkJdulh7SBnnTnDU", "2eC54WVokOBCMfraLI5w5AkPzV4OhG2rUnfhWHKBM0M");
    public static final AnonymousClass0TO A0R = new AnonymousClass0TO("portal_platform_aloha", "n0_Ssi2N8d9-5-hJV0WQ1rt5nlk", "0qONa7PbO-l9JLKlJjkkf_VDfDI7jJKEQCKJX1nzxus");
    public static final AnonymousClass0TO A0S;
    public static final AnonymousClass0TO A0T = new AnonymousClass0TO("portal_platform_catalina", "gZ2hI80bGDGB_XVfiJOSCktxXwI", "-HdCA-5jUGjnsyAYKdXRg4w3tVAHYZTU3EzUOWRVTX0");
    public static final AnonymousClass0TO A0U;
    public static final AnonymousClass0TO A0V = new AnonymousClass0TO("portal_platform_omni", "HyZQnz_A_xFUJNx9-uRxfLglhH0", "W1BzWTPVNZBtrA45RvTcbkVphwqyUdZwQEL7X-RqPpM");
    public static final AnonymousClass0TO A0W = new AnonymousClass0TO("portal_platform_omni_debug", "a_U1bAb7cZcQ1uOM0JdyiPdHWq8", "b2sRSFyeAdgq4NbTDsF6EuDfHreyS9x2Pp7oKe8QclI");
    public static final AnonymousClass0TO A0X = new AnonymousClass0TO("portal_whatsapp", "FKzVxsqJpYsJuFdQqTE7IEgYC08", "7oVvh3Fck1xX0J5u42DHceCqMyIqewU2TWaJlChTsZA");
    public static final AnonymousClass0TO A0Y = new AnonymousClass0TO("portal_whatsapp_debug", "AMOxgVfUuJgIn3TkLNUWQ2QWkeY", "y-7IuVPL3L_a06a23Nl8rcsi51i83grZLyXD-OMtQO0");
    public static final AnonymousClass0TO A0Z = new AnonymousClass0TO("study_release", "r9BCZbqOeyVExd0xAOMmz1UyHz4", "GVQYWvhotR_1RBv0Am3VA2bLMifS4uOCNDeaKSVc7CU");
    public static final AnonymousClass0TO A0a = new AnonymousClass0TO("viewpoints_release", "XuWTzVfchfTAN8XtLJfChtm7034", "XdkQEeiVeIyDkvBEAHtGJKKHfdsrOFf9te68UpyJVhA");
    public static final AnonymousClass0TO A0b = new AnonymousClass0TO("whatsapp_debug", "HfqsFpVx2hvmL2FpTQgY5bCSyHo", "HEIr5yy3eXd1ynm5SueBb-3LsOp9aEYClzuRNXxJzFc");
    public static final AnonymousClass0TO A0c = new AnonymousClass0TO("whatsapp_release", "OKD31QX-GP7GT780Psqq8xDb15k", "OYfQQ9EK769ahxCzZxQY_lfg4ZtlPJ34JVj-tf_OXUQ");
    public static final Map A0d = Collections.unmodifiableMap(new AnonymousClass1XE());
    public static final Set A0e = Collections.unmodifiableSet(new AnonymousClass1XB());
    public static final Set A0f = Collections.unmodifiableSet(new AnonymousClass1XC());
    public static final Set A0g = Collections.unmodifiableSet(new HashSet(Collections.singletonList(A01)));
    public static final Set A0h = Collections.unmodifiableSet(new HashSet(Arrays.asList(A00, A08, A0A)));
    public static final Set A0i = Collections.unmodifiableSet(new HashSet(Arrays.asList(A02, A03, A07)));
    public static final Set A0j = Collections.unmodifiableSet(new HashSet(Arrays.asList(A04, A06)));
    public static final Set A0k = Collections.unmodifiableSet(new HashSet(Collections.singletonList(A05)));
    public static final Set A0l = Collections.unmodifiableSet(new HashSet(Collections.singletonList(A09)));
    public static final Set A0m = Collections.unmodifiableSet(new HashSet(Arrays.asList(A01, A0E)));
    public static final Set A0n = Collections.unmodifiableSet(new HashSet(Arrays.asList(A0D, A0C, A0B)));
    public static final Set A0o = Collections.unmodifiableSet(new HashSet(Collections.singletonList(A0F)));
    public static final Set A0p = Collections.unmodifiableSet(new HashSet(Collections.singletonList(A0G)));
    public static final Set A0q = Collections.unmodifiableSet(new AnonymousClass0TP());
    public static final Set A0r = Collections.unmodifiableSet(new HashSet(Arrays.asList(A0S, A0U, A0W)));
    public static final Set A0s = Collections.unmodifiableSet(new HashSet(Arrays.asList(A0R, A0T, A0V)));
    public static final Set A0t = Collections.unmodifiableSet(new C08800fy());
    public static final Set A0u = Collections.unmodifiableSet(new HashSet(Collections.singletonList(A0b)));
    public static final Set A0v = Collections.unmodifiableSet(new HashSet(Collections.singletonList(A0c)));

    static {
        new AnonymousClass0TO("fbandroid_oculus_assistant", "eBLxbTAHR6nuEIur96W48dDc7Io", "rO1i0-x-Hhu8_RdBiQjfrXe1WnMMVVkuOoFfs8ri2eE");
        new AnonymousClass0TO("oculus_priv_apps_release", "eBLxbTAHR6nuEIur96W48dDc7Io", "rO1i0-x-Hhu8_RdBiQjfrXe1WnMMVVkuOoFfs8ri2eE");
        AnonymousClass0TO r3 = new AnonymousClass0TO("portal_platform_aloha_debug", "Aj-bcwbSjGsTOUNEkMaqXHyTQeY", "5MCO54QyiJ31mua72pgMV7lET8XxQmxVGsxMmN3dAkA");
        A0S = r3;
        A0U = r3;
        Collections.unmodifiableSet(new HashSet(Arrays.asList(A0J, A0X)));
        Collections.unmodifiableSet(new AnonymousClass1X9());
        Collections.unmodifiableSet(new AnonymousClass1XA());
        Collections.unmodifiableSet(new AnonymousClass1XD());
    }
}
