package X;

import android.os.Handler;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0gG  reason: invalid class name and case insensitive filesystem */
public final class C08960gG extends C08950gF implements Runnable, AnonymousClass09S, AnonymousClass0Y0, RunnableFuture {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.ListenableScheduledFutureImpl";
    public final AnonymousClass0XX A00;

    public boolean cancel(boolean z) {
        return super.cancel(false);
    }

    public void addListener(Runnable runnable, Executor executor) {
        this.A00.addListener(runnable, executor);
    }

    public int compareTo(Object obj) {
        throw new UnsupportedOperationException();
    }

    public long getDelay(TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public Object getInnerRunnable() {
        return this.A00;
    }

    public void run() {
        this.A00.run();
    }

    public C08960gG(Handler handler, Runnable runnable, Object obj) {
        super(handler);
        this.A00 = new AnonymousClass0XX(runnable, obj);
    }

    public C08960gG(Handler handler, Callable callable) {
        super(handler);
        this.A00 = new AnonymousClass0XX(callable);
    }
}
