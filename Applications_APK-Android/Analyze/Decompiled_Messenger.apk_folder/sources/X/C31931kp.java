package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1kp  reason: invalid class name and case insensitive filesystem */
public final class C31931kp extends AnonymousClass0UV {
    public static final AnonymousClass1Y9 A00(AnonymousClass1XY r0) {
        return AnonymousClass0Ud.A00(r0).A09;
    }

    public static final Boolean A01(AnonymousClass1XY r0) {
        return Boolean.valueOf(AnonymousClass0Ud.A00(r0).A0G());
    }
}
