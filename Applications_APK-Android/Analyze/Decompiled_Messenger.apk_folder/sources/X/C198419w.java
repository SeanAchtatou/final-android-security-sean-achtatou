package X;

import android.view.Choreographer;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.19w  reason: invalid class name and case insensitive filesystem */
public abstract class C198419w {
    public Choreographer.FrameCallback A00;
    public Runnable A01;
    public final AtomicReference A02 = new AtomicReference();

    public void A01(long j) {
        ((C198319v) this).A00.A0U();
    }

    public static void A00(C198419w r2, long j) {
        Object A012 = C27461dE.A01("ChoreographerCompat_doFrame", r2.A02.getAndSet(null));
        try {
            r2.A01(j);
        } finally {
            C27461dE.A02(A012);
        }
    }
}
