package X;

import android.content.Context;
import android.content.Intent;
import java.util.Iterator;

/* renamed from: X.1Zo  reason: invalid class name and case insensitive filesystem */
public final class C25421Zo extends C06870cD {
    public final /* synthetic */ C06790c5 A00;
    public final /* synthetic */ Class A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C25421Zo(C06790c5 r1, Iterator it, Class cls) {
        super(it);
        this.A00 = r1;
        this.A01 = cls;
    }

    public String A04() {
        return AnonymousClass08S.A0S(this.A01.getName(), " (making use of ", getClass().getName(), ")");
    }

    public boolean A07(Context context, Intent intent) {
        return this.A00.A02();
    }
}
