package X;

import com.facebook.messaging.montage.composer.art.loader.MontageTextStylesLoader;

/* renamed from: X.0qo  reason: invalid class name */
public final class AnonymousClass0qo implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messenger.neue.M4MainActivityDelegate$2";
    public final /* synthetic */ C13100qf A00;

    public AnonymousClass0qo(C13100qf r1) {
        this.A00 = r1;
    }

    public void run() {
        MontageTextStylesLoader montageTextStylesLoader = (MontageTextStylesLoader) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BNK, this.A00.A01);
        MontageTextStylesLoader.A02(montageTextStylesLoader, new C87834Gt(montageTextStylesLoader));
    }
}
