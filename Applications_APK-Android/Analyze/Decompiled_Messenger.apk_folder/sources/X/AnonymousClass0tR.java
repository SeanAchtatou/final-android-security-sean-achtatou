package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tR  reason: invalid class name */
public final class AnonymousClass0tR {
    private static volatile AnonymousClass0tR A07;
    public C35211qr A00;
    public boolean A01 = A01(this);
    public final C06790c5 A02;
    public final C14500tS A03;
    private final C04460Ut A04;
    private final C28191eP A05;
    private final C04310Tq A06;

    public static final AnonymousClass0tR A00(AnonymousClass1XY r8) {
        if (A07 == null) {
            synchronized (AnonymousClass0tR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A07 = new AnonymousClass0tR(C04430Uq.A02(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.A7j, applicationInjector), C28191eP.A00(applicationInjector), C14500tS.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static boolean A01(AnonymousClass0tR r1) {
        if (!((Boolean) r1.A06.get()).booleanValue() || !r1.A05.A0J().contains("messenger_hide_ads")) {
            return false;
        }
        return true;
    }

    private AnonymousClass0tR(C04460Ut r4, C04310Tq r5, C28191eP r6, C14500tS r7) {
        this.A04 = r4;
        this.A06 = r5;
        this.A05 = r6;
        this.A03 = r7;
        C06600bl BMm = this.A04.BMm();
        BMm.A02("com.facebook.zero.ZERO_RATING_STATE_CHANGED", new C14510tU(this));
        this.A02 = BMm.A00();
    }
}
