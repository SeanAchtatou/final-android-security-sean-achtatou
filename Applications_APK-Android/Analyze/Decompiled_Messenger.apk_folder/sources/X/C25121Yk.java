package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Yk  reason: invalid class name and case insensitive filesystem */
public final class C25121Yk {
    private static volatile C25121Yk A09;
    public AnonymousClass0UN A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    private Boolean A04;
    private Boolean A05;
    private Boolean A06;
    private Boolean A07;
    private Long A08;

    public static final C25121Yk A00(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (C25121Yk.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new C25121Yk(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public long A01() {
        if (this.A08 == null) {
            this.A08 = Long.valueOf(AnonymousClass00J.A01((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, this.A00)).A0i);
        }
        return this.A08.longValue();
    }

    public boolean A02() {
        if (this.A04 == null) {
            this.A04 = Boolean.valueOf(AnonymousClass00J.A01((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, this.A00)).A19);
        }
        return this.A04.booleanValue();
    }

    public boolean A03() {
        if (this.A05 == null) {
            this.A05 = Boolean.valueOf(AnonymousClass00J.A01((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, this.A00)).A2T);
        }
        return this.A05.booleanValue();
    }

    public boolean A04() {
        if (this.A06 == null) {
            this.A06 = Boolean.valueOf(AnonymousClass00J.A01((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, this.A00)).A2U);
        }
        return this.A06.booleanValue();
    }

    public boolean A05() {
        if (this.A07 == null) {
            this.A07 = Boolean.valueOf(AnonymousClass00J.A01((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, this.A00)).A2V);
        }
        return this.A07.booleanValue();
    }

    private C25121Yk(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
