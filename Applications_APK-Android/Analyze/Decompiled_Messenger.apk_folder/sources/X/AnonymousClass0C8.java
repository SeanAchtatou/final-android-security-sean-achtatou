package X;

import android.net.NetworkInfo;
import android.os.SystemClock;
import io.card.payment.BuildConfig;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0C8  reason: invalid class name */
public final class AnonymousClass0C8 {
    public static final EnumSet A0d = EnumSet.of(AnonymousClass0C9.ACKNOWLEDGED_DELIVERY, AnonymousClass0C9.PROCESSING_LASTACTIVE_PRESENCEINFO, AnonymousClass0C9.EXACT_KEEPALIVE, AnonymousClass0C9.DELTA_SENT_MESSAGE_ENABLED, AnonymousClass0C9.USE_THRIFT_FOR_INBOX, AnonymousClass0C9.USE_ENUM_TOPIC);
    public static final AtomicInteger A0e = new AtomicInteger(1);
    public List A00;
    private int A01;
    public final C01390Aa A02;
    public final C01690Bg A03;
    public final C01490Al A04;
    public final AnonymousClass0BA A05;
    public final AnonymousClass0B6 A06;
    public final C01440Ag A07;
    public final AnonymousClass0B7 A08;
    public final C01570At A09;
    public final AnonymousClass0AQ A0A;
    public final AnonymousClass0CB A0B = new AnonymousClass0CB(this);
    public final AnonymousClass0C7 A0C;
    public final AnonymousClass0AT A0D;
    public final AnonymousClass0C6 A0E;
    public final Map A0F = new HashMap();
    public final ExecutorService A0G;
    public final AtomicInteger A0H;
    public final AtomicReference A0I;
    public final boolean A0J;
    public final boolean A0K;
    public final boolean A0L;
    public final boolean A0M;
    private final C01690Bg A0N;
    private final AnonymousClass0CA A0O = new AnonymousClass0CA(this);
    private final Long A0P;
    public volatile long A0Q;
    public volatile long A0R = Long.MAX_VALUE;
    public volatile long A0S = Long.MAX_VALUE;
    public volatile long A0T = Long.MAX_VALUE;
    public volatile long A0U = Long.MAX_VALUE;
    public volatile long A0V = Long.MAX_VALUE;
    public volatile long A0W;
    public volatile NetworkInfo A0X;
    public volatile AnonymousClass0CC A0Y;
    public volatile AnonymousClass089 A0Z = AnonymousClass089.DISCONNECTED;
    public volatile String A0a;
    public volatile String A0b = "none";
    public volatile boolean A0c;

    public static synchronized void A04(AnonymousClass0C8 r3, AnonymousClass0CE r4, AnonymousClass0CG r5, Throwable th) {
        synchronized (r3) {
            if (r3.A0A()) {
                AnonymousClass07A.A02(r3.A0G, new AnonymousClass0CF(r3, r4, r5, th), -555131673);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (r2 == X.AnonymousClass089.CONNECT_SENT) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A05(long r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            long r5 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0026 }
        L_0x0005:
            X.089 r2 = r7.A0Z     // Catch:{ all -> 0x0026 }
            X.089 r0 = X.AnonymousClass089.CONNECTING     // Catch:{ all -> 0x0026 }
            if (r2 == r0) goto L_0x0010
            X.089 r1 = X.AnonymousClass089.CONNECT_SENT     // Catch:{ all -> 0x0026 }
            r0 = 0
            if (r2 != r1) goto L_0x0011
        L_0x0010:
            r0 = 1
        L_0x0011:
            if (r0 == 0) goto L_0x0024
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0026 }
            long r0 = r0 - r5
            long r3 = r8 - r0
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0024
            r7.wait(r3)     // Catch:{ all -> 0x0026 }
            goto L_0x0005
        L_0x0024:
            monitor-exit(r7)
            return
        L_0x0026:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0C8.A05(long):void");
    }

    public synchronized void A06(AnonymousClass0CE r3) {
        A04(this, r3, AnonymousClass0CG.A01, null);
    }

    public synchronized void A07(String str, byte[] bArr, Integer num, int i, AnonymousClass0CZ r18, long j, String str2, AnonymousClass0CU r22) {
        if (A0A()) {
            String str3 = str;
            byte[] bArr2 = bArr;
            Integer num2 = num;
            int i2 = i;
            AnonymousClass07A.A04(this.A0G, new AnonymousClass0CH(this, str3, bArr2, num2, i2, r18, j, str2, r22), 167031433);
        } else {
            throw new AnonymousClass0CI(AnonymousClass07B.A00);
        }
    }

    private static C36191sf A00(Integer num) {
        if (num != null) {
            switch (num.intValue()) {
                case 2:
                    return C36191sf.WIFI;
                case 3:
                    return C36191sf.G2;
                case 4:
                    return C36191sf.G3;
                case 5:
                    return C36191sf.G4;
            }
        }
        return C36191sf.DEFAULT;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x007d, code lost:
        if (r27 != null) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x007f, code lost:
        r0 = X.C01660Bc.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0081, code lost:
        r9 = r3.A0W;
        r22 = r3.A09.A02();
        r11 = r3.A0X;
        r6 = r3.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x008d, code lost:
        if (r6 != null) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x008f, code lost:
        r25 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0091, code lost:
        r12.A02(r13, r14, r15, r16, r17, r18, r0, r9, r22, r11, r25);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x009a, code lost:
        if (r2 == null) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x009c, code lost:
        r8.toString();
        r26.toString();
        r9 = X.AnonymousClass08S.A0P(r7, "@", r1);
        r2.A02.A0l = android.os.SystemClock.elapsedRealtime();
        r2.A02.A0q = r9;
        X.AnonymousClass00S.A04(r2.A02.A05, new X.AnonymousClass0CD(r2), -1948040727);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00c5, code lost:
        if (r8 == X.AnonymousClass0CE.A0D) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00c9, code lost:
        if (r8 != X.AnonymousClass0CE.A0O) goto L_0x00d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00cb, code lost:
        r8.toString();
        r2.A02("Mqtt Unknown Exception", r7, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00d3, code lost:
        r3.A0D.Bfm(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d8, code lost:
        r3.A0R = Long.MAX_VALUE;
        r3.A0V = Long.MAX_VALUE;
        r3.A0U = Long.MAX_VALUE;
        r3.A0T = Long.MAX_VALUE;
        r3.A0S = Long.MAX_VALUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00e7, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00e8, code lost:
        r25 = ((java.lang.Boolean) r6.get()).booleanValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00f3, code lost:
        r0 = new X.C01550Ar(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        r0 = X.AnonymousClass0C3.A05;
        r8 = r25;
        r4 = r8.name();
        ((X.C01850Bw) r3.A08.A07(X.C01850Bw.class)).A02(r0, r4);
        r3.A08.A00.A02.set(android.os.SystemClock.elapsedRealtime());
        ((java.util.concurrent.atomic.AtomicLong) ((X.AnonymousClass08I) r3.A08.A07(X.AnonymousClass08I.class)).A00(X.AnonymousClass08J.A0A)).addAndGet(android.os.SystemClock.elapsedRealtime() - r3.A0W);
        r12 = r3.A06;
        r13 = r3.A01(r3.A0R);
        r14 = r3.A01(r3.A0V);
        r15 = r3.A01(r3.A0U);
        r16 = r3.A01(r3.A0T);
        r7 = r8.toString();
        r17 = X.C01540Aq.A00(r7);
        r1 = r26.toString();
        r18 = X.C01540Aq.A00(r1);
        r5 = r27;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.AnonymousClass0C8 r24, X.AnonymousClass0CE r25, X.AnonymousClass0CG r26, java.lang.Throwable r27) {
        /*
            r3 = r24
            monitor-enter(r3)
            boolean r0 = r3.A0A()     // Catch:{ all -> 0x00f9 }
            if (r0 != 0) goto L_0x000b
            monitor-exit(r3)     // Catch:{ all -> 0x00f9 }
            return
        L_0x000b:
            X.0CC r2 = r3.A0Y     // Catch:{ all -> 0x00f9 }
            X.0C7 r0 = r3.A0C     // Catch:{ all -> 0x00f9 }
            r0.ASP()     // Catch:{ all -> 0x00f9 }
            monitor-exit(r3)     // Catch:{ all -> 0x00f9 }
            X.0B7 r1 = r3.A08
            java.lang.Class<X.0Bw> r0 = X.C01850Bw.class
            X.0By r1 = r1.A07(r0)
            X.0Bw r1 = (X.C01850Bw) r1
            X.0C3 r0 = X.AnonymousClass0C3.A05
            r8 = r25
            java.lang.String r4 = r8.name()
            r1.A02(r0, r4)
            X.0B7 r0 = r3.A08
            X.0B9 r0 = r0.A00
            java.util.concurrent.atomic.AtomicLong r5 = r0.A02
            long r0 = android.os.SystemClock.elapsedRealtime()
            r5.set(r0)
            X.0B7 r1 = r3.A08
            java.lang.Class<X.08I> r0 = X.AnonymousClass08I.class
            X.0By r1 = r1.A07(r0)
            X.08I r1 = (X.AnonymousClass08I) r1
            X.08J r0 = X.AnonymousClass08J.A0A
            java.lang.Object r7 = r1.A00(r0)
            java.util.concurrent.atomic.AtomicLong r7 = (java.util.concurrent.atomic.AtomicLong) r7
            long r0 = android.os.SystemClock.elapsedRealtime()
            long r5 = r3.A0W
            long r0 = r0 - r5
            r7.addAndGet(r0)
            X.0B6 r12 = r3.A06
            long r0 = r3.A0R
            X.0Aq r13 = r3.A01(r0)
            long r0 = r3.A0V
            X.0Aq r14 = r3.A01(r0)
            long r0 = r3.A0U
            X.0Aq r15 = r3.A01(r0)
            long r0 = r3.A0T
            X.0Aq r16 = r3.A01(r0)
            java.lang.String r7 = r8.toString()
            X.0Aq r17 = X.C01540Aq.A00(r7)
            java.lang.String r1 = r26.toString()
            X.0Aq r18 = X.C01540Aq.A00(r1)
            r5 = r27
            if (r27 != 0) goto L_0x00f3
            X.0Bc r0 = X.C01660Bc.A00
        L_0x0081:
            long r9 = r3.A0W
            X.0At r6 = r3.A09
            long r22 = r6.A02()
            android.net.NetworkInfo r11 = r3.A0X
            X.0Bg r6 = r3.A03
            if (r6 != 0) goto L_0x00e8
            r25 = 0
        L_0x0091:
            r24 = r11
            r19 = r0
            r20 = r9
            r12.A02(r13, r14, r15, r16, r17, r18, r19, r20, r22, r24, r25)
            if (r2 == 0) goto L_0x00d8
            r8.toString()
            java.lang.String r0 = "@"
            r26.toString()
            java.lang.String r9 = X.AnonymousClass08S.A0P(r7, r0, r1)
            X.0AH r6 = r2.A02
            long r0 = android.os.SystemClock.elapsedRealtime()
            r6.A0l = r0
            X.0AH r0 = r2.A02
            r0.A0q = r9
            X.0AH r0 = r2.A02
            android.os.Handler r6 = r0.A05
            X.0CD r1 = new X.0CD
            r1.<init>(r2)
            r0 = -1948040727(0xffffffff8be341e9, float:-8.753639E-32)
            X.AnonymousClass00S.A04(r6, r1, r0)
            X.0CE r0 = X.AnonymousClass0CE.A0D
            if (r8 == r0) goto L_0x00cb
            X.0CE r0 = X.AnonymousClass0CE.A0O
            if (r8 != r0) goto L_0x00d3
        L_0x00cb:
            r8.toString()
            java.lang.String r0 = "Mqtt Unknown Exception"
            r2.A02(r0, r7, r5)
        L_0x00d3:
            X.0AT r0 = r3.A0D
            r0.Bfm(r4)
        L_0x00d8:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r3.A0R = r0
            r3.A0V = r0
            r3.A0U = r0
            r3.A0T = r0
            r3.A0S = r0
            return
        L_0x00e8:
            java.lang.Object r6 = r6.get()
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r25 = r6.booleanValue()
            goto L_0x0091
        L_0x00f3:
            X.0Ar r0 = new X.0Ar
            r0.<init>(r5)
            goto L_0x0081
        L_0x00f9:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00f9 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0C8.A03(X.0C8, X.0CE, X.0CG, java.lang.Throwable):void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(41:2|3|21|11|12|(1:14)(1:20)|15|(33:17|(1:19)|22|(1:28)(1:26)|27|29|30|31|32|(2:35|33)|84|36|(1:38)|39|(1:41)|42|(1:44)|45|(1:47)|48|(1:50)(1:67)|51|(1:53)(1:66)|54|(1:56)(1:65)|57|(1:59)(1:64)|60|(1:62)|63|(2:69|71)(1:70)|72|73)|21|22|(1:24)|28|27|29|30|31|32|(1:33)|84|36|(0)|39|(0)|42|(0)|45|(0)|48|(0)(0)|51|(0)(0)|54|(0)(0)|57|(0)(0)|60|(0)|63|(0)(0)|72|73) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00db */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00eb A[LOOP:1: B:33:0x00e5->B:35:0x00eb, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x020f  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x021f  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0225  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0226  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A08(java.util.List r39, boolean r40) {
        /*
            r38 = this;
            r0 = r38
            monitor-enter(r38)
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0258 }
            r0.A0S = r1     // Catch:{ all -> 0x0258 }
            r0.A0Q = r1     // Catch:{ all -> 0x0258 }
            r8 = r39
            X.AnonymousClass0A1.A00(r8)     // Catch:{ all -> 0x0258 }
            r0.A00 = r8     // Catch:{ all -> 0x0258 }
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            java.util.concurrent.atomic.AtomicInteger r1 = r1.A0K     // Catch:{ all -> 0x0258 }
            int r36 = r1.get()     // Catch:{ all -> 0x0258 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x0258 }
            r7.<init>()     // Catch:{ all -> 0x0258 }
            java.util.Map r5 = r0.A0F     // Catch:{ all -> 0x0258 }
            monitor-enter(r5)     // Catch:{ all -> 0x0258 }
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0255 }
            java.util.List r1 = r1.A0I     // Catch:{ all -> 0x0255 }
            java.util.Iterator r4 = r1.iterator()     // Catch:{ all -> 0x0255 }
        L_0x002a:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x0255 }
            if (r1 == 0) goto L_0x0043
            java.lang.Object r3 = r4.next()     // Catch:{ all -> 0x0255 }
            com.facebook.rti.mqtt.protocol.messages.SubscribeTopic r3 = (com.facebook.rti.mqtt.protocol.messages.SubscribeTopic) r3     // Catch:{ all -> 0x0255 }
            java.lang.String r1 = r3.A01     // Catch:{ all -> 0x0255 }
            r7.add(r1)     // Catch:{ all -> 0x0255 }
            java.util.Map r2 = r0.A0F     // Catch:{ all -> 0x0255 }
            java.lang.String r1 = r3.A01     // Catch:{ all -> 0x0255 }
            r2.put(r1, r3)     // Catch:{ all -> 0x0255 }
            goto L_0x002a
        L_0x0043:
            monitor-exit(r5)     // Catch:{ all -> 0x0255 }
            boolean r1 = r0.A0M     // Catch:{ all -> 0x0258 }
            r3 = 0
            if (r1 == 0) goto L_0x0081
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            X.0Ai r1 = r1.A0C     // Catch:{ all -> 0x0258 }
            java.lang.String r5 = "["
            java.lang.String r6 = "FBAN"
            java.lang.String r4 = r1.A02     // Catch:{ all -> 0x0258 }
            java.lang.String r2 = "FBAV"
            X.0Aw r1 = r1.A01     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = r1.A01     // Catch:{ all -> 0x0258 }
            java.lang.Object[] r4 = new java.lang.Object[]{r6, r4, r2, r1}     // Catch:{ all -> 0x0258 }
            java.lang.String r2 = "%s/%s;%s/%s;"
            java.lang.String r2 = java.lang.String.format(r3, r2, r4)     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = "]"
            java.lang.String r15 = X.AnonymousClass08S.A0P(r5, r2, r1)     // Catch:{ all -> 0x0258 }
            r5 = r3
        L_0x006a:
            X.0Bg r1 = r0.A0N     // Catch:{ all -> 0x0258 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x0092
            java.lang.String r6 = r1.trim()     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = ""
            boolean r1 = r6.equals(r1)     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x0093
            goto L_0x0092
        L_0x0081:
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            X.0Ai r1 = r1.A0C     // Catch:{ all -> 0x0258 }
            java.lang.String r15 = r1.A01()     // Catch:{ all -> 0x0258 }
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            X.0C5 r1 = r1.A0E     // Catch:{ all -> 0x0258 }
            java.lang.Object r5 = r1.first     // Catch:{ all -> 0x0258 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0258 }
            goto L_0x006a
        L_0x0092:
            r6 = r3
        L_0x0093:
            X.0At r1 = r0.A09     // Catch:{ all -> 0x0258 }
            android.net.NetworkInfo r1 = r1.A04()     // Catch:{ all -> 0x0258 }
            r0.A0X = r1     // Catch:{ all -> 0x0258 }
            X.0At r1 = r0.A09     // Catch:{ all -> 0x0258 }
            android.net.NetworkInfo r2 = r1.A04()     // Catch:{ all -> 0x0258 }
            if (r2 == 0) goto L_0x00c4
            java.lang.String r1 = r2.getTypeName()     // Catch:{ all -> 0x0258 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0258 }
            if (r1 != 0) goto L_0x00c4
            java.lang.String r1 = r2.getTypeName()     // Catch:{ all -> 0x0258 }
        L_0x00b1:
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ all -> 0x0258 }
            r0.A0b = r1     // Catch:{ all -> 0x0258 }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0258 }
            r0.A0W = r1     // Catch:{ all -> 0x0258 }
            r1 = 0
            java.lang.Long r14 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0258 }
            goto L_0x00c7
        L_0x00c4:
            java.lang.String r1 = "none"
            goto L_0x00b1
        L_0x00c7:
            X.0C6 r1 = r0.A0E     // Catch:{ NumberFormatException -> 0x00db }
            X.0Bu r1 = r1.A0D     // Catch:{ NumberFormatException -> 0x00db }
            java.lang.Object r1 = r1.first     // Catch:{ NumberFormatException -> 0x00db }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ NumberFormatException -> 0x00db }
            r0.A0a = r1     // Catch:{ NumberFormatException -> 0x00db }
            java.lang.String r1 = r0.A0a     // Catch:{ NumberFormatException -> 0x00db }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ NumberFormatException -> 0x00db }
            java.lang.Long r14 = java.lang.Long.valueOf(r1)     // Catch:{ NumberFormatException -> 0x00db }
        L_0x00db:
            X.0CJ r13 = new X.0CJ     // Catch:{ all -> 0x0258 }
            java.util.EnumSet r1 = X.AnonymousClass0C8.A0d     // Catch:{ all -> 0x0258 }
            java.util.Iterator r4 = r1.iterator()     // Catch:{ all -> 0x0258 }
            r9 = 0
        L_0x00e5:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x00f8
            java.lang.Object r1 = r4.next()     // Catch:{ all -> 0x0258 }
            X.0C9 r1 = (X.AnonymousClass0C9) r1     // Catch:{ all -> 0x0258 }
            byte r2 = r1.mPosition     // Catch:{ all -> 0x0258 }
            r1 = 1
            int r1 = r1 << r2
            long r1 = (long) r1     // Catch:{ all -> 0x0258 }
            long r9 = r9 | r1
            goto L_0x00e5
        L_0x00f8:
            boolean r1 = r0.A0L     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x0104
            X.0C9 r1 = X.AnonymousClass0C9.USE_SEND_PINGRESP     // Catch:{ all -> 0x0258 }
            byte r2 = r1.mPosition     // Catch:{ all -> 0x0258 }
            r1 = 1
            int r1 = r1 << r2
            long r1 = (long) r1     // Catch:{ all -> 0x0258 }
            long r9 = r9 | r1
        L_0x0104:
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            boolean r1 = r1.A0N     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x0112
            X.0C9 r1 = X.AnonymousClass0C9.REQUIRE_REPLAY_PROTECTION     // Catch:{ all -> 0x0258 }
            byte r2 = r1.mPosition     // Catch:{ all -> 0x0258 }
            r1 = 1
            int r1 = r1 << r2
            long r1 = (long) r1     // Catch:{ all -> 0x0258 }
            long r9 = r9 | r1
        L_0x0112:
            boolean r1 = r0.A0c     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x011e
            X.0C9 r1 = X.AnonymousClass0C9.FBNS_EXPLICIT_DELIVERY_ACK     // Catch:{ all -> 0x0258 }
            byte r2 = r1.mPosition     // Catch:{ all -> 0x0258 }
            r1 = 1
            int r1 = r1 << r2
            long r1 = (long) r1     // Catch:{ all -> 0x0258 }
            long r9 = r9 | r1
        L_0x011e:
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            boolean r1 = r1.A0O     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x012c
            X.0C9 r1 = X.AnonymousClass0C9.IS_LARGE_PAYLOAD_SUPPORTED     // Catch:{ all -> 0x0258 }
            byte r2 = r1.mPosition     // Catch:{ all -> 0x0258 }
            r1 = 1
            int r1 = r1 << r2
            long r1 = (long) r1     // Catch:{ all -> 0x0258 }
            long r9 = r9 | r1
        L_0x012c:
            java.lang.Long r16 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x0258 }
            long r1 = r0.A0W     // Catch:{ all -> 0x0258 }
            java.lang.Long r17 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0258 }
            android.net.NetworkInfo r1 = r0.A0X     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x021f
            android.net.NetworkInfo r1 = r0.A0X     // Catch:{ all -> 0x0258 }
            int r1 = r1.getType()     // Catch:{ all -> 0x0258 }
            java.lang.Integer r18 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0258 }
        L_0x0144:
            android.net.NetworkInfo r1 = r0.A0X     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x021b
            android.net.NetworkInfo r1 = r0.A0X     // Catch:{ all -> 0x0258 }
            int r1 = r1.getSubtype()     // Catch:{ all -> 0x0258 }
            java.lang.Integer r19 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0258 }
        L_0x0152:
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            boolean r2 = r1.A0R     // Catch:{ all -> 0x0258 }
            java.lang.Boolean r20 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x0258 }
            r2 = 1
            java.lang.Boolean r21 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x0258 }
            X.0C5 r2 = r1.A0E     // Catch:{ all -> 0x0258 }
            java.lang.Object r2 = r2.second     // Catch:{ all -> 0x0258 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0258 }
            boolean r4 = r1.A0Q     // Catch:{ all -> 0x0258 }
            java.lang.Boolean r24 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x0258 }
            X.0Bg r1 = r1.A0B     // Catch:{ all -> 0x0258 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x0258 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ all -> 0x0258 }
            long r9 = r1.longValue()     // Catch:{ all -> 0x0258 }
            java.lang.Long r25 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x0258 }
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            int r12 = r1.A08     // Catch:{ all -> 0x0258 }
            X.0AQ r4 = r0.A0A     // Catch:{ all -> 0x0258 }
            java.lang.String r27 = r4.AhN()     // Catch:{ all -> 0x0258 }
            java.lang.String r11 = r1.A0F     // Catch:{ all -> 0x0258 }
            boolean r4 = r0.A0M     // Catch:{ all -> 0x0258 }
            if (r4 == 0) goto L_0x0218
            java.lang.String r10 = r1.A0G     // Catch:{ all -> 0x0258 }
        L_0x018d:
            X.0C7 r4 = r0.A0C     // Catch:{ all -> 0x0258 }
            byte r4 = r4.AhL()     // Catch:{ all -> 0x0258 }
            java.lang.Byte r32 = java.lang.Byte.valueOf(r4)     // Catch:{ all -> 0x0258 }
            java.util.Map r9 = r1.A0J     // Catch:{ all -> 0x0258 }
            java.lang.Long r4 = r0.A0P     // Catch:{ all -> 0x0258 }
            X.0At r1 = r0.A09     // Catch:{ all -> 0x0258 }
            java.lang.Integer r1 = r1.A05()     // Catch:{ all -> 0x0258 }
            X.1sf r35 = A00(r1)     // Catch:{ all -> 0x0258 }
            r22 = r5
            r23 = r2
            r26 = r12
            r28 = r11
            r29 = r7
            r30 = r10
            r31 = r6
            r33 = r9
            r34 = r4
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35)     // Catch:{ all -> 0x0258 }
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            X.0C5 r1 = r1.A0E     // Catch:{ all -> 0x0258 }
            java.lang.Object r1 = r1.first     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0258 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0258 }
            if (r1 == 0) goto L_0x020f
            java.util.UUID r1 = java.util.UUID.randomUUID()     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0258 }
        L_0x01d0:
            r4 = 20
            int r2 = r1.length()     // Catch:{ all -> 0x0258 }
            int r2 = java.lang.Math.min(r4, r2)     // Catch:{ all -> 0x0258 }
            r5 = 0
            java.lang.String r2 = r1.substring(r5, r2)     // Catch:{ all -> 0x0258 }
            X.0CK r6 = new X.0CK     // Catch:{ all -> 0x0258 }
            boolean r1 = r0.A0M     // Catch:{ all -> 0x0258 }
            if (r1 != 0) goto L_0x01ed
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            X.0Bu r1 = r1.A0D     // Catch:{ all -> 0x0258 }
            java.lang.Object r3 = r1.second     // Catch:{ all -> 0x0258 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0258 }
        L_0x01ed:
            r6.<init>(r2, r13, r3, r8)     // Catch:{ all -> 0x0258 }
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = r1.A01     // Catch:{ all -> 0x0258 }
            r6.A00 = r1     // Catch:{ all -> 0x0258 }
            X.089 r1 = X.AnonymousClass089.CONNECTING     // Catch:{ all -> 0x0258 }
            r0.A0Z = r1     // Catch:{ all -> 0x0258 }
            X.0B7 r1 = r0.A08     // Catch:{ all -> 0x0258 }
            X.0B9 r8 = r1.A00     // Catch:{ all -> 0x0258 }
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0258 }
            java.util.concurrent.atomic.AtomicLong r7 = r8.A03     // Catch:{ all -> 0x0258 }
            r1 = 0
            r7.compareAndSet(r1, r3)     // Catch:{ all -> 0x0258 }
            java.util.concurrent.atomic.AtomicLong r7 = r8.A00     // Catch:{ all -> 0x0258 }
            r7.compareAndSet(r1, r3)     // Catch:{ all -> 0x0258 }
            goto L_0x0223
        L_0x020f:
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            X.0C5 r1 = r1.A0E     // Catch:{ all -> 0x0258 }
            java.lang.Object r1 = r1.first     // Catch:{ all -> 0x0258 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0258 }
            goto L_0x01d0
        L_0x0218:
            r10 = r3
            goto L_0x018d
        L_0x021b:
            r19 = r3
            goto L_0x0152
        L_0x021f:
            r18 = r3
            goto L_0x0144
        L_0x0223:
            if (r40 == 0) goto L_0x0226
            goto L_0x022b
        L_0x0226:
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            int r7 = r1.A07     // Catch:{ all -> 0x0258 }
            goto L_0x022f
        L_0x022b:
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            int r7 = r1.A02     // Catch:{ all -> 0x0258 }
        L_0x022f:
            r0.A01 = r7     // Catch:{ all -> 0x0258 }
            X.0C7 r4 = r0.A0C     // Catch:{ all -> 0x0258 }
            X.0C6 r1 = r0.A0E     // Catch:{ all -> 0x0258 }
            java.lang.String r3 = r1.A00     // Catch:{ all -> 0x0258 }
            boolean r2 = r0.A0M     // Catch:{ all -> 0x0258 }
            boolean r1 = r1.A0T     // Catch:{ all -> 0x0258 }
            r37 = r1
            r31 = r4
            r32 = r3
            r33 = r7
            r34 = r2
            r35 = r6
            r31.AU1(r32, r33, r34, r35, r36, r37)     // Catch:{ all -> 0x0258 }
            X.0AT r1 = r0.A0D     // Catch:{ all -> 0x0258 }
            X.0C6 r0 = r0.A0E     // Catch:{ all -> 0x0258 }
            java.util.List r0 = r0.A0I     // Catch:{ all -> 0x0258 }
            r1.BsK(r0, r5)     // Catch:{ all -> 0x0258 }
            monitor-exit(r38)
            return
        L_0x0255:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0255 }
            throw r0     // Catch:{ all -> 0x0258 }
        L_0x0258:
            r0 = move-exception
            monitor-exit(r38)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0C8.A08(java.util.List, boolean):void");
    }

    public boolean A09() {
        if (this.A0Z == AnonymousClass089.CONNECTED) {
            return true;
        }
        return false;
    }

    public boolean A0A() {
        AnonymousClass089 r2 = this.A0Z;
        if (r2 == AnonymousClass089.CONNECTED || r2 == AnonymousClass089.CONNECTING || r2 == AnonymousClass089.CONNECT_SENT) {
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[MqttClient (");
        AnonymousClass0C6 r1 = this.A0E;
        sb.append(r1.A00);
        sb.append(":");
        sb.append(this.A01);
        if (r1.A0T) {
            sb.append(" +ssl");
        }
        sb.append(") ");
        sb.append(this.A0Z);
        sb.append("]");
        return sb.toString();
    }

    private C01540Aq A01(long j) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (j > elapsedRealtime) {
            return C01660Bc.A00;
        }
        return C01540Aq.A00(Long.valueOf(elapsedRealtime - j));
    }

    public static String A02(AnonymousClass0C8 r1, long j) {
        C01540Aq A012 = r1.A01(j);
        if (A012.A02()) {
            return new Date(System.currentTimeMillis() - ((Long) A012.A01()).longValue()).toString();
        }
        return "N/A";
    }

    public AnonymousClass0C8(C01570At r5, C01490Al r6, AnonymousClass0B6 r7, AnonymousClass0B7 r8, AnonymousClass0C6 r9, ExecutorService executorService, C01440Ag r11, C01390Aa r12, AnonymousClass0AT r13, AnonymousClass0AQ r14, AnonymousClass0BA r15, AnonymousClass0C7 r16, C01690Bg r17, AtomicReference atomicReference, C01690Bg r19, boolean z, boolean z2, boolean z3, Long l) {
        String str;
        boolean z4 = false;
        this.A0H = new AtomicInteger(0);
        this.A09 = r5;
        this.A04 = r6;
        this.A06 = r7;
        this.A08 = r8;
        this.A0E = r9;
        this.A0G = executorService;
        this.A07 = r11;
        this.A02 = r12;
        this.A0D = r13;
        r13.C3a();
        this.A0A = r14;
        this.A05 = r15;
        AnonymousClass0C7 r3 = r16;
        this.A0C = r3;
        this.A0N = r17;
        this.A0I = atomicReference;
        r3.C6f(this.A0B, this.A0O);
        String AiB = this.A0A.AiB();
        if (BuildConfig.FLAVOR.equals(this.A0A.AhN()) && (str = this.A0E.A0G) != null && AiB.equals(str)) {
            z4 = true;
        }
        this.A0M = z4;
        this.A03 = r19;
        this.A0L = z;
        this.A0K = z2;
        this.A0J = z3;
        this.A0P = l;
    }
}
