package X;

import android.content.Context;
import com.facebook.analytics2.logger.DefaultHandlerThreadFactory;
import com.facebook.analytics2.logger.HandlerThreadFactory;
import com.facebook.analytics2.logger.Uploader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/* renamed from: X.15C  reason: invalid class name */
public final class AnonymousClass15C {
    private static AnonymousClass15C A06;
    public final ArrayList A00 = new ArrayList();
    public final ArrayList A01 = new ArrayList();
    public final ArrayList A02 = new ArrayList();
    private final Context A03;
    private final ArrayList A04 = new ArrayList();
    private final ArrayList A05 = new ArrayList();

    public static synchronized Object A01(AnonymousClass15C r4, ArrayList arrayList, String str) {
        synchronized (r4) {
            try {
                Object A022 = r4.A02(arrayList, Class.forName(str));
                return A022;
            } catch (ClassNotFoundException e) {
                C010708t.A0U("ContextConstructorHelper", e, "Cannot find class: %s", str);
            } catch (IllegalAccessException e2) {
                C010708t.A0T("ContextConstructorHelper", e2, "IllegalAccessException");
            } catch (InstantiationException e3) {
                C010708t.A0T("ContextConstructorHelper", e3, "InstantiationException");
            } catch (NoSuchMethodException e4) {
                C010708t.A0T("ContextConstructorHelper", e4, "NoSuchMethodException");
            } catch (InvocationTargetException e5) {
                C010708t.A0T("ContextConstructorHelper", e5, "InvocationTargetException");
            }
        }
        return null;
    }

    private synchronized Object A02(ArrayList arrayList, Class cls) {
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Object obj = arrayList.get(i);
            if (obj.getClass().equals(cls)) {
                return obj;
            }
        }
        Object newInstance = cls.getConstructor(Context.class).newInstance(this.A03);
        arrayList.add(newInstance);
        return newInstance;
    }

    public static synchronized AnonymousClass15C A00(Context context) {
        AnonymousClass15C r0;
        synchronized (AnonymousClass15C.class) {
            if (A06 == null) {
                A06 = new AnonymousClass15C(context.getApplicationContext());
            }
            r0 = A06;
        }
        return r0;
    }

    public HandlerThreadFactory A03(String str) {
        HandlerThreadFactory handlerThreadFactory = (HandlerThreadFactory) A01(this, this.A04, str);
        if (handlerThreadFactory != null) {
            return handlerThreadFactory;
        }
        DefaultHandlerThreadFactory defaultHandlerThreadFactory = new DefaultHandlerThreadFactory(this.A03);
        C010708t.A0K("ContextConstructorHelper", "Unable to create instance for HandlerThreadFactory");
        return defaultHandlerThreadFactory;
    }

    public Uploader A04(String str) {
        Object obj;
        ArrayList arrayList = this.A05;
        synchronized (this) {
            try {
                obj = A02(arrayList, Class.forName(str));
            } catch (ClassNotFoundException e) {
                C010708t.A0U("ContextConstructorHelper", e, "Cannot find class: %s", str);
                obj = null;
            }
        }
        return (Uploader) obj;
    }

    private AnonymousClass15C(Context context) {
        this.A03 = context;
    }
}
