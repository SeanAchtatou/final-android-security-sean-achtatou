package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1OY  reason: invalid class name */
public final class AnonymousClass1OY {
    private final C23091Oe A00;
    private final AnonymousClass1N6 A01;
    private final C23051Oa A02;
    private final QuickPerformanceLogger A03;

    private C30781id A00(C23121Oh r18) {
        C37861wT r3;
        List list;
        C23121Oh r12 = r18;
        try {
            String str = r12.A02;
            if (str != null) {
                C23131Oi r32 = new C23131Oi();
                C23131Oi.A00(new JSONObject(str), r32);
                C23131Oi r1 = r32;
                String str2 = r32.A03;
                if (str2 == null || r32.A02 == null) {
                    r3 = new C37861wT("Can't identify config");
                } else if (str2.equals("single-context-buckets-table")) {
                    String str3 = r12.A02;
                    C30147EqR eqR = new C30147EqR();
                    JSONObject jSONObject = new JSONObject(str3);
                    C23131Oi.A00(jSONObject, eqR);
                    eqR.A00 = C211779zh.A00(jSONObject.optJSONObject("context"));
                    eqR.A01 = jSONObject.optString("output", null);
                    if (jSONObject.isNull("table")) {
                        list = null;
                    } else {
                        JSONArray jSONArray = jSONObject.getJSONArray("table");
                        int length = jSONArray.length();
                        C30155EqZ[] eqZArr = new C30155EqZ[length];
                        for (int i = 0; i < length; i++) {
                            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                            C30155EqZ eqZ = new C30155EqZ();
                            eqZ.A00 = jSONObject2.optString("bucket", null);
                            eqZ.A01 = jSONObject2.optString("value", null);
                            eqZArr[i] = eqZ;
                        }
                        list = Arrays.asList(eqZArr);
                    }
                    eqR.A03 = list;
                    eqR.A02 = jSONObject.optString("default", null);
                    return new C30144EqO(r12, eqR, this.A00, this.A01, this.A02);
                } else if (str2.equals("multi-output-resolved")) {
                    JSONObject jSONObject3 = new JSONObject(r12.A02);
                    C23141Oj r13 = new C23141Oj();
                    C23131Oi.A00(jSONObject3, r13);
                    r13.A00 = A03(jSONObject3, "monitors");
                    r13.A01 = A04(jSONObject3, "outputs");
                    r13.A02 = A05(jSONObject3, "values");
                    return new C23171Om(r12, r13, this.A00, this.A01, this.A02);
                } else if (str2.equals("table")) {
                    return new C30144EqO(r12, A02(r12.A02), this.A00, this.A01, this.A02);
                } else {
                    if (str2.equals("dense")) {
                        return new C30145EqP(r12, A01(r12.A02), this.A00, this.A01, this.A02);
                    }
                    r3 = new C37861wT("Unknown config type", str2);
                }
                throw r3;
            }
            throw new C37861wT("Config not found", r12.A00());
        } catch (JSONException e) {
            throw new IOException(e);
        } catch (JSONException e2) {
            throw new IOException(e2);
        } catch (JSONException e3) {
            throw new IOException(e3);
        } catch (IOException unused) {
            return new C23221Or(r12, null, "Can't read config", this.A02);
        } catch (C37861wT e4) {
            return new C23221Or(r12, null, e4.getMessage(), this.A02);
        } catch (IndexOutOfBoundsException e5) {
            return new C23221Or(r12, null, e5.getMessage(), this.A02);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r1 = A00(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0083, code lost:
        r0 = r11.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0086, code lost:
        if (r0 == null) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r1 = A00(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008c, code lost:
        r0 = r11.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008f, code lost:
        if (r0 == null) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0091, code lost:
        r0.markerEnd(37945345, 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0094, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0095, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0096, code lost:
        r0 = r11.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0098, code lost:
        if (r0 != null) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009a, code lost:
        r0.markerEnd(37945345, 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009d, code lost:
        throw r1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:27:0x0080, B:31:0x0089] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0080 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0089 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C30781id A06(X.C23121Oh r12) {
        /*
            r11 = this;
            r3 = 4
            r2 = 37945345(0x2430001, float:1.4326339E-37)
            r6 = r12
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            if (r0 == 0) goto L_0x000c
            r0.markerStart(r2)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
        L_0x000c:
            r5 = 0
            java.lang.String r1 = r12.A02     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            if (r1 == 0) goto L_0x0048
            java.lang.String r0 = "\"version\":"
            boolean r0 = r1.contains(r0)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            if (r0 == 0) goto L_0x0048
            java.lang.String r0 = "\"cctype\":\"dense\""
            boolean r0 = r1.contains(r0)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            if (r0 == 0) goto L_0x0031
            X.EqT r7 = A01(r1)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.EqP r5 = new X.EqP     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.1Oe r8 = r11.A00     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.1N6 r9 = r11.A01     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.1Oa r10 = r11.A02     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            r5.<init>(r6, r7, r8, r9, r10)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            goto L_0x0048
        L_0x0031:
            java.lang.String r0 = "\"cctype\":\"table\""
            boolean r0 = r1.contains(r0)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            if (r0 == 0) goto L_0x0048
            X.EqS r7 = A02(r1)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.EqO r5 = new X.EqO     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.1Oe r8 = r11.A00     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.1N6 r9 = r11.A01     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            X.1Oa r10 = r11.A02     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            r5.<init>(r6, r7, r8, r9, r10)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
        L_0x0048:
            if (r5 != 0) goto L_0x004e
            X.1id r5 = r11.A00(r12)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
        L_0x004e:
            com.facebook.quicklog.QuickPerformanceLogger r4 = r11.A03     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            if (r4 == 0) goto L_0x0078
            java.lang.String r1 = "name"
            java.lang.String r0 = r5.getName()     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            r4.markerAnnotate(r2, r1, r0)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            boolean r0 = r5 instanceof X.C23221Or     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            if (r0 == 0) goto L_0x0072
            com.facebook.quicklog.QuickPerformanceLogger r4 = r11.A03     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            java.lang.String r1 = "error"
            r0 = r5
            X.1Or r0 = (X.C23221Or) r0     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            java.lang.String r0 = r0.A00     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            r4.markerAnnotate(r2, r1, r0)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            com.facebook.quicklog.QuickPerformanceLogger r1 = r11.A03     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            r0 = 3
            r1.markerEnd(r2, r0)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            goto L_0x0078
        L_0x0072:
            com.facebook.quicklog.QuickPerformanceLogger r1 = r11.A03     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
            r0 = 2
            r1.markerEnd(r2, r0)     // Catch:{ IOException -> 0x0089, 1wT -> 0x0080 }
        L_0x0078:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03
            if (r0 == 0) goto L_0x007f
            r0.markerEnd(r2, r3)
        L_0x007f:
            return r5
        L_0x0080:
            X.1id r1 = r11.A00(r12)     // Catch:{ all -> 0x0095 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03
            if (r0 == 0) goto L_0x0094
            goto L_0x0091
        L_0x0089:
            X.1id r1 = r11.A00(r12)     // Catch:{ all -> 0x0095 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03
            if (r0 == 0) goto L_0x0094
        L_0x0091:
            r0.markerEnd(r2, r3)
        L_0x0094:
            return r1
        L_0x0095:
            r1 = move-exception
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A03
            if (r0 == 0) goto L_0x009d
            r0.markerEnd(r2, r3)
        L_0x009d:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A06(X.1Oh):X.1id");
    }

    public static C30149EqT A01(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            C30149EqT eqT = new C30149EqT();
            C23131Oi.A00(jSONObject, eqT);
            eqT.A00 = A03(jSONObject, "contexts");
            eqT.A01 = A03(jSONObject, "monitors");
            eqT.A02 = A04(jSONObject, "outputs");
            eqT.A03 = C44502Il.A00(jSONObject, "vector");
            eqT.A04 = C44502Il.A00(jSONObject, "vectorDefaults");
            return eqT;
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    public static C30148EqS A02(String str) {
        List list;
        try {
            JSONObject jSONObject = new JSONObject(str);
            C30148EqS eqS = new C30148EqS();
            C23131Oi.A00(jSONObject, eqS);
            eqS.A00 = A03(jSONObject, "contexts");
            eqS.A02 = A03(jSONObject, "monitors");
            eqS.A03 = A04(jSONObject, "outputs");
            if (jSONObject.isNull("table")) {
                list = null;
            } else {
                JSONArray jSONArray = jSONObject.getJSONArray("table");
                int length = jSONArray.length();
                C30153EqX[] eqXArr = new C30153EqX[length];
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    C30153EqX eqX = new C30153EqX();
                    eqX.A00 = jSONObject2.optString("bucket", null);
                    eqX.A01 = A05(jSONObject2, "values");
                    eqXArr[i] = eqX;
                }
                list = Arrays.asList(eqXArr);
            }
            eqS.A04 = list;
            eqS.A01 = A05(jSONObject, "defaults");
            return eqS;
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    public AnonymousClass1OY(C23091Oe r1, AnonymousClass1N6 r2, C23051Oa r3, QuickPerformanceLogger quickPerformanceLogger) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = quickPerformanceLogger;
    }

    public static List A03(JSONObject jSONObject, String str) {
        if (jSONObject.isNull(str)) {
            return null;
        }
        JSONArray jSONArray = jSONObject.getJSONArray(str);
        int length = jSONArray.length();
        C211779zh[] r2 = new C211779zh[length];
        for (int i = 0; i < length; i++) {
            r2[i] = C211779zh.A00(jSONArray.getJSONObject(i));
        }
        return Arrays.asList(r2);
    }

    public static List A04(JSONObject jSONObject, String str) {
        if (jSONObject.isNull(str)) {
            return null;
        }
        JSONArray jSONArray = jSONObject.getJSONArray(str);
        int length = jSONArray.length();
        C23151Ok[] r5 = new C23151Ok[length];
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            C23151Ok r2 = new C23151Ok();
            r2.A00 = jSONObject2.optString("name", null);
            r2.A01 = jSONObject2.optString("type", null);
            if (!jSONObject2.isNull("range")) {
                C211789zi.A00(jSONObject2);
            }
            r5[i] = r2;
        }
        return Arrays.asList(r5);
    }

    public static List A05(JSONObject jSONObject, String str) {
        if (jSONObject.isNull(str)) {
            return null;
        }
        JSONArray jSONArray = jSONObject.getJSONArray(str);
        int length = jSONArray.length();
        C23161Ol[] r5 = new C23161Ol[length];
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            C23161Ol r2 = new C23161Ol();
            r2.A00 = jSONObject2.optString("name", null);
            r2.A01 = jSONObject2.optString("value", null);
            r5[i] = r2;
        }
        return Arrays.asList(r5);
    }
}
