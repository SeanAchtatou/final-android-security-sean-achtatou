package X;

import android.database.Cursor;

/* renamed from: X.0po  reason: invalid class name and case insensitive filesystem */
public final class C12690po {
    public static final String[] A09 = {"latest_montage_preview_message_id", "latest_montage_preview_message_type", "latest_montage_preview_attachments", "latest_montage_preview_text", "latest_montage_preview_sticker_id", "latest_montage_message_timestamp_ms", "latest_montage_preview_pending_send_attachment", "latest_montage_message_sender"};
    private AnonymousClass0UN A00;
    private final int A01;
    private final int A02;
    private final int A03;
    private final int A04;
    private final int A05;
    private final int A06;
    private final int A07;
    private final int A08;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: com.facebook.messaging.model.threads.MontageThreadPreview} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.facebook.ui.media.attachments.model.MediaResource} */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.C17920zh r13, X.C13680rr r14) {
        /*
            r12 = this;
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r13.A0W
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            android.database.Cursor r4 = r14.A00
            int r0 = r12.A03
            java.lang.String r7 = r4.getString(r0)
            int r0 = r12.A05
            int r0 = r4.getInt(r0)
            X.1V7 r0 = X.AnonymousClass1V7.A00(r0)
            X.2Br r8 = com.facebook.messaging.model.threads.MontageThreadPreview.A00(r0)
            r1 = 0
            if (r7 == 0) goto L_0x0096
            if (r8 == 0) goto L_0x0096
            int r0 = r12.A04
            long r9 = r4.getLong(r0)
            r3 = 2
            int r2 = X.AnonymousClass1Y3.AOI
            X.0UN r0 = r12.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.0jC r2 = (X.AnonymousClass0jC) r2
            int r0 = r12.A01
            java.lang.String r0 = r4.getString(r0)
            com.facebook.messaging.model.messages.ParticipantInfo r11 = r2.A03(r0)
            X.2VF r6 = new X.2VF
            r6.<init>(r7, r8, r9, r11)
            int r0 = r12.A07
            java.lang.String r0 = r4.getString(r0)
            r6.A06 = r0
            int r0 = r12.A08
            java.lang.String r0 = r4.getString(r0)
            r6.A07 = r0
            int r2 = X.AnonymousClass1Y3.A0X
            X.0UN r0 = r12.A00
            r5 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r0)
            X.0oH r2 = (X.C11940oH) r2
            int r0 = r12.A02
            java.lang.String r0 = r4.getString(r0)
            com.google.common.collect.ImmutableList r2 = r2.A03(r0, r7)
            boolean r0 = r2.isEmpty()
            if (r0 == 0) goto L_0x0099
            r0 = r1
        L_0x006c:
            r6.A01 = r0
            r3 = 1
            int r2 = X.AnonymousClass1Y3.ACe
            X.0UN r0 = r12.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.0oF r2 = (X.C11920oF) r2
            int r0 = r12.A06
            java.lang.String r0 = r4.getString(r0)
            java.util.List r2 = r2.A04(r0)
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x008f
            java.lang.Object r1 = r2.get(r5)
            com.facebook.ui.media.attachments.model.MediaResource r1 = (com.facebook.ui.media.attachments.model.MediaResource) r1
        L_0x008f:
            r6.A04 = r1
            com.facebook.messaging.model.threads.MontageThreadPreview r1 = new com.facebook.messaging.model.threads.MontageThreadPreview
            r1.<init>(r6)
        L_0x0096:
            r13.A0W = r1
            return
        L_0x0099:
            java.lang.Object r0 = r2.get(r5)
            com.facebook.messaging.model.attachment.Attachment r0 = (com.facebook.messaging.model.attachment.Attachment) r0
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12690po.A00(X.0zh, X.0rr):void");
    }

    public C12690po(AnonymousClass1XY r3, Cursor cursor) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A03 = cursor.getColumnIndexOrThrow("latest_montage_preview_message_id");
        this.A05 = cursor.getColumnIndexOrThrow("latest_montage_preview_message_type");
        this.A08 = cursor.getColumnIndexOrThrow("latest_montage_preview_text");
        this.A07 = cursor.getColumnIndexOrThrow("latest_montage_preview_sticker_id");
        this.A02 = cursor.getColumnIndexOrThrow("latest_montage_preview_attachments");
        this.A04 = cursor.getColumnIndexOrThrow("latest_montage_message_timestamp_ms");
        this.A06 = cursor.getColumnIndexOrThrow("latest_montage_preview_pending_send_attachment");
        this.A01 = cursor.getColumnIndexOrThrow("latest_montage_message_sender");
    }
}
