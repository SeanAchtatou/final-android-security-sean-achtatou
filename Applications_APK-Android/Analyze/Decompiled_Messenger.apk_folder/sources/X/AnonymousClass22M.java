package X;

import android.preference.Preference;

/* renamed from: X.22M  reason: invalid class name */
public final class AnonymousClass22M implements Preference.OnPreferenceClickListener {
    public final /* synthetic */ Preference A00;
    public final /* synthetic */ C197379Qa A01;

    public AnonymousClass22M(C197379Qa r1, Preference preference) {
        this.A01 = r1;
        this.A00 = preference;
    }

    public boolean onPreferenceClick(Preference preference) {
        int i = AnonymousClass1Y3.Ao8;
        AnonymousClass0UN r2 = this.A01.A02;
        ((AnonymousClass3P8) AnonymousClass1XX.A02(17, i, r2)).A00(((AnonymousClass3BB) AnonymousClass1XX.A02(15, AnonymousClass1Y3.APY, r2)).A01() + 1, true, 2131833115, new AnonymousClass9NR(this)).A07();
        return true;
    }
}
