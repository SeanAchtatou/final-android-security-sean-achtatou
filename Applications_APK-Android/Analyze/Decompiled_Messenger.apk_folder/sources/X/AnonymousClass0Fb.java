package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/* renamed from: X.0Fb  reason: invalid class name */
public final class AnonymousClass0Fb extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0Fa A00;

    public AnonymousClass0Fb(AnonymousClass0Fa r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(621189799);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        synchronized (this.A00) {
            try {
                String action = intent.getAction();
                char c = 65535;
                int hashCode = action.hashCode();
                if (hashCode != -1886648615) {
                    if (hashCode == 1019184907 && action.equals("android.intent.action.ACTION_POWER_CONNECTED")) {
                        c = 0;
                    }
                } else if (action.equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                    c = 1;
                }
                if (c == 0) {
                    AnonymousClass0Fa r9 = this.A00;
                    if (!r9.A03) {
                        r9.A00 += elapsedRealtime - r9.A02;
                    } else {
                        r9.A01 += elapsedRealtime - r9.A02;
                        r9.A05("CONNECTED", elapsedRealtime);
                    }
                    this.A00.A03 = true;
                } else if (c == 1) {
                    AnonymousClass0Fa r8 = this.A00;
                    if (r8.A03) {
                        r8.A01 += elapsedRealtime - r8.A02;
                    } else {
                        r8.A00 += elapsedRealtime - r8.A02;
                        r8.A05("DISCONNECTED", elapsedRealtime);
                    }
                    this.A00.A03 = false;
                }
                this.A00.A02 = elapsedRealtime;
            } catch (Throwable th) {
                while (true) {
                    C000700l.A0D(intent, -1037311408, A01);
                    throw th;
                }
            }
        }
        C000700l.A0D(intent, -1142889552, A01);
    }
}
