package X;

import sun.misc.Unsafe;

/* renamed from: X.0dT  reason: invalid class name and case insensitive filesystem */
public final class C07430dT extends C05490Zd {
    public static final long A00;
    public static final long A01;
    public static final long A02;
    public static final long A03;
    public static final long A04;
    public static final Unsafe A05;

    public void A00(C07500df r4, C07500df r5) {
        A05.putObject(r4, A03, r5);
    }

    public void A01(C07500df r4, Thread thread) {
        A05.putObject(r4, A04, thread);
    }

    public boolean A02(C07920eO r7, C07520dh r8, C07520dh r9) {
        return A05.compareAndSwapObject(r7, A00, r8, r9);
    }

    public boolean A03(C07920eO r7, C07500df r8, C07500df r9) {
        return A05.compareAndSwapObject(r7, A02, r8, r9);
    }

    public boolean A04(C07920eO r7, Object obj, Object obj2) {
        return A05.compareAndSwapObject(r7, A01, obj, obj2);
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:2:0x0005 */
    static {
        /*
            sun.misc.Unsafe r3 = sun.misc.Unsafe.getUnsafe()     // Catch:{ SecurityException -> 0x0005 }
            goto L_0x0010
        L_0x0005:
            X.0Zb r0 = new X.0Zb     // Catch:{ PrivilegedActionException -> 0x005d }
            r0.<init>()     // Catch:{ PrivilegedActionException -> 0x005d }
            java.lang.Object r3 = java.security.AccessController.doPrivileged(r0)     // Catch:{ PrivilegedActionException -> 0x005d }
            sun.misc.Unsafe r3 = (sun.misc.Unsafe) r3     // Catch:{ PrivilegedActionException -> 0x005d }
        L_0x0010:
            java.lang.Class<X.0eO> r2 = X.C07920eO.class
            java.lang.String r0 = "waiters"
            java.lang.reflect.Field r0 = r2.getDeclaredField(r0)     // Catch:{ Exception -> 0x0053 }
            long r0 = r3.objectFieldOffset(r0)     // Catch:{ Exception -> 0x0053 }
            X.C07430dT.A02 = r0     // Catch:{ Exception -> 0x0053 }
            java.lang.String r0 = "listeners"
            java.lang.reflect.Field r0 = r2.getDeclaredField(r0)     // Catch:{ Exception -> 0x0053 }
            long r0 = r3.objectFieldOffset(r0)     // Catch:{ Exception -> 0x0053 }
            X.C07430dT.A00 = r0     // Catch:{ Exception -> 0x0053 }
            java.lang.String r0 = "value"
            java.lang.reflect.Field r0 = r2.getDeclaredField(r0)     // Catch:{ Exception -> 0x0053 }
            long r0 = r3.objectFieldOffset(r0)     // Catch:{ Exception -> 0x0053 }
            X.C07430dT.A01 = r0     // Catch:{ Exception -> 0x0053 }
            java.lang.Class<X.0df> r2 = X.C07500df.class
            java.lang.String r0 = "thread"
            java.lang.reflect.Field r0 = r2.getDeclaredField(r0)     // Catch:{ Exception -> 0x0053 }
            long r0 = r3.objectFieldOffset(r0)     // Catch:{ Exception -> 0x0053 }
            X.C07430dT.A04 = r0     // Catch:{ Exception -> 0x0053 }
            java.lang.String r0 = "next"
            java.lang.reflect.Field r0 = r2.getDeclaredField(r0)     // Catch:{ Exception -> 0x0053 }
            long r0 = r3.objectFieldOffset(r0)     // Catch:{ Exception -> 0x0053 }
            X.C07430dT.A03 = r0     // Catch:{ Exception -> 0x0053 }
            X.C07430dT.A05 = r3     // Catch:{ Exception -> 0x0053 }
            return
        L_0x0053:
            r1 = move-exception
            com.google.common.base.Throwables.throwIfUnchecked(r1)
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        L_0x005d:
            r0 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.Throwable r1 = r0.getCause()
            java.lang.String r0 = "Could not initialize intrinsics"
            r2.<init>(r0, r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07430dT.<clinit>():void");
    }
}
