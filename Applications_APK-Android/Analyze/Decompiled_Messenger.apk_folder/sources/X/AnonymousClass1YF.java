package X;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.locks.LockSupport;

/* renamed from: X.1YF  reason: invalid class name */
public final class AnonymousClass1YF {
    public final C04560Ve A00;
    private final ArrayDeque A01 = new ArrayDeque();

    public synchronized void A02() {
        LockSupport.unpark((Thread) this.A01.pollLast());
    }

    public synchronized void A03() {
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            LockSupport.unpark((Thread) it.next());
        }
        this.A01.clear();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r8 = !r9.A01.remove(r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.AnonymousClass1YF r9, boolean r10, long r11, long r13) {
        /*
            X.0Ve r0 = r9.A00
            java.util.concurrent.locks.ReentrantLock r0 = r0.A05
            int r3 = r0.getHoldCount()
            X.0Ve r0 = r9.A00
            int r0 = r0.A00
            r8 = 0
            r7 = 0
            if (r0 <= 0) goto L_0x0011
            r7 = 1
        L_0x0011:
            r0 = 0
            if (r3 <= 0) goto L_0x0015
            r0 = 1
        L_0x0015:
            com.google.common.base.Preconditions.checkState(r0)
            java.lang.Thread r2 = java.lang.Thread.currentThread()
            monitor-enter(r9)
            java.util.ArrayDeque r0 = r9.A01     // Catch:{ all -> 0x00be }
            r0.add(r2)     // Catch:{ all -> 0x00be }
            monitor-exit(r9)     // Catch:{ all -> 0x00be }
            if (r7 == 0) goto L_0x002c
            X.0Ve r0 = r9.A00     // Catch:{ all -> 0x0079 }
            java.util.concurrent.atomic.AtomicInteger r0 = r0.A03     // Catch:{ all -> 0x0079 }
            r0.incrementAndGet()     // Catch:{ all -> 0x0079 }
        L_0x002c:
            r1 = 0
        L_0x002d:
            if (r1 >= r3) goto L_0x0037
            X.0Ve r0 = r9.A00     // Catch:{ all -> 0x0079 }
            r0.A02()     // Catch:{ all -> 0x0079 }
            int r1 = r1 + 1
            goto L_0x002d
        L_0x0037:
            r6 = 0
        L_0x0038:
            if (r6 != 0) goto L_0x008c
            monitor-enter(r9)     // Catch:{ all -> 0x0079 }
            if (r10 == 0) goto L_0x0044
            r4 = 0
            int r0 = (r11 > r4 ? 1 : (r11 == r4 ? 0 : -1))
            if (r0 > 0) goto L_0x0044
            goto L_0x006c
        L_0x0044:
            java.util.ArrayDeque r0 = r9.A01     // Catch:{ all -> 0x0076 }
            boolean r0 = r0.contains(r2)     // Catch:{ all -> 0x0076 }
            monitor-exit(r9)
            if (r0 != 0) goto L_0x004e
            goto L_0x008b
        L_0x004e:
            if (r10 == 0) goto L_0x0061
            java.util.concurrent.locks.LockSupport.parkNanos(r9, r11)     // Catch:{ all -> 0x0079 }
            boolean r0 = java.lang.Thread.interrupted()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x005a
            r6 = 1
        L_0x005a:
            long r0 = java.lang.System.nanoTime()     // Catch:{ all -> 0x0079 }
            long r11 = r13 - r0
            goto L_0x0038
        L_0x0061:
            java.util.concurrent.locks.LockSupport.park(r9)     // Catch:{ all -> 0x0079 }
            boolean r0 = java.lang.Thread.interrupted()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0038
            r6 = 1
            goto L_0x0038
        L_0x006c:
            java.util.ArrayDeque r0 = r9.A01     // Catch:{ all -> 0x0076 }
            boolean r0 = r0.remove(r2)     // Catch:{ all -> 0x0076 }
            r8 = r0 ^ 1
            monitor-exit(r9)     // Catch:{ all -> 0x0076 }
            goto L_0x008c
        L_0x0076:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0076 }
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r2 = move-exception
            r1 = 0
        L_0x007b:
            if (r1 >= r3) goto L_0x00c1
            X.0Ve r0 = r9.A00
            if (r7 == 0) goto L_0x0087
            r0.A03()
        L_0x0084:
            int r1 = r1 + 1
            goto L_0x007b
        L_0x0087:
            r0.A00()
            goto L_0x0084
        L_0x008b:
            r8 = 1
        L_0x008c:
            r1 = 0
        L_0x008d:
            if (r1 >= r3) goto L_0x009d
            X.0Ve r0 = r9.A00
            if (r7 == 0) goto L_0x0099
            r0.A03()
        L_0x0096:
            int r1 = r1 + 1
            goto L_0x008d
        L_0x0099:
            r0.A00()
            goto L_0x0096
        L_0x009d:
            if (r7 == 0) goto L_0x00a6
            X.0Ve r0 = r9.A00
            java.util.concurrent.atomic.AtomicInteger r0 = r0.A03
            r0.decrementAndGet()
        L_0x00a6:
            if (r6 == 0) goto L_0x00bd
            monitor-enter(r9)
            java.util.ArrayDeque r0 = r9.A01     // Catch:{ all -> 0x00ba }
            r0.remove(r2)     // Catch:{ all -> 0x00ba }
            if (r8 == 0) goto L_0x00b3
            r9.A02()     // Catch:{ all -> 0x00ba }
        L_0x00b3:
            monitor-exit(r9)     // Catch:{ all -> 0x00ba }
            java.lang.InterruptedException r0 = new java.lang.InterruptedException
            r0.<init>()
            throw r0
        L_0x00ba:
            r2 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x00ba }
            goto L_0x00ca
        L_0x00bd:
            return
        L_0x00be:
            r2 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x00be }
            goto L_0x00ca
        L_0x00c1:
            if (r7 == 0) goto L_0x00ca
            X.0Ve r0 = r9.A00
            java.util.concurrent.atomic.AtomicInteger r0 = r0.A03
            r0.decrementAndGet()
        L_0x00ca:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YF.A00(X.1YF, boolean, long, long):void");
    }

    public AnonymousClass1YF(C04560Ve r2) {
        this.A00 = r2;
    }

    public long A01(long j) {
        long nanoTime = System.nanoTime() + j;
        A00(this, true, j, nanoTime);
        long nanoTime2 = nanoTime - System.nanoTime();
        if (nanoTime2 > j) {
            return Long.MIN_VALUE;
        }
        return nanoTime2;
    }
}
