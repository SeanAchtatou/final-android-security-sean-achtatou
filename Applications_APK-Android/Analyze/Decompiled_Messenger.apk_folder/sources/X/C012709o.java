package X;

import android.os.Build;

/* renamed from: X.09o  reason: invalid class name and case insensitive filesystem */
public final class C012709o {
    public static boolean A00(int i) {
        if (Build.VERSION.SDK_INT >= i) {
            return true;
        }
        return false;
    }
}
