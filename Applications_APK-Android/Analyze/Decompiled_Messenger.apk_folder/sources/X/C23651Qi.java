package X;

import android.content.Context;
import android.content.res.Resources;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.List;

/* renamed from: X.1Qi  reason: invalid class name and case insensitive filesystem */
public final class C23651Qi extends C20831Dz {
    public AnonymousClass0UN A00;
    public AnonymousClass4U6 A01;
    public MigColorScheme A02;
    public List A03;
    public List A04;
    public boolean A05;
    public final Context A06;
    public final Resources A07;
    public final C83323xR A08;
    public final AnonymousClass4U5 A09 = new AnonymousClass4U5(this);
    public final AnonymousClass4U4 A0A = new AnonymousClass4U4(this);

    public int ArU() {
        int size;
        if (this.A04.isEmpty()) {
            size = 0;
        } else {
            size = this.A04.size() + 1;
        }
        return size + this.A03.size();
    }

    public C23651Qi(AnonymousClass1XY r3, Context context, MigColorScheme migColorScheme) {
        ImmutableList immutableList = RegularImmutableList.A02;
        this.A03 = immutableList;
        this.A04 = immutableList;
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A08 = new C83323xR(r3);
        this.A07 = C04490Ux.A0L(r3);
        this.A06 = context;
        this.A02 = migColorScheme;
    }
}
