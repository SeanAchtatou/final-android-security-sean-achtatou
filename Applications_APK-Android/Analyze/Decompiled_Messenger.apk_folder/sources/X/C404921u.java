package X;

import android.content.Context;
import android.view.View;
import com.facebook.payments.paymentmethods.model.PaymentMethod;

/* renamed from: X.21u  reason: invalid class name and case insensitive filesystem */
public final class C404921u implements View.OnClickListener {
    public final /* synthetic */ C24099BtG A00;

    public C404921u(C24099BtG btG) {
        this.A00 = btG;
    }

    public void onClick(View view) {
        C24194BvE bvE;
        String id;
        int A05 = C000700l.A05(-346242906);
        C24107BtP btP = this.A00.A05;
        if (btP != null) {
            AnonymousClass3IJ r6 = btP.A00.A0O;
            BHX A03 = B6X.A03(C22298Ase.$const$string(27));
            if (btP.A00.A04.A03 == C24270Bwa.A05) {
                bvE = C24194BvE.A0B;
            } else {
                bvE = C24194BvE.A0F;
            }
            A03.A01(bvE);
            A03.A00.A0D(C22298Ase.$const$string(202), "change_payment_method");
            A03.A02(C24195BvF.A06);
            A03.A00.A0D("sender_user_id", btP.A00.A04.A0F);
            A03.A06(btP.A00.A05.A0A);
            A03.A05(btP.A00.A05.A06);
            A03.A00(btP.A00.A05.A00());
            boolean z = false;
            if (btP.A00.A05.A04 != null) {
                z = true;
            }
            A03.A0B(z);
            r6.A05(A03);
            if (btP.A00.A0M.Aem(282879432918988L)) {
                C24098BtF btF = btP.A00;
                C23817BnR bnR = btF.A0N;
                Context context = btF.A00;
                PaymentMethod paymentMethod = btF.A07;
                if (paymentMethod == null) {
                    id = null;
                } else {
                    id = paymentMethod.getId();
                }
                C417626w.A01(C23817BnR.A00(bnR, context, id, null, false, null), 50, btP.A00.A01);
            } else {
                C24098BtF.A07(btP.A00, false);
            }
        }
        C000700l.A0B(-430318176, A05);
    }
}
