package X;

import android.os.ConditionVariable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Z9  reason: invalid class name */
public final class AnonymousClass1Z9 implements AnonymousClass0Y8 {
    public int A00 = -1;
    private int A01 = 0;
    public final AnonymousClass0US[] A02;
    private final ConditionVariable A03 = new ConditionVariable();
    private final C04840Wj A04;
    private final C001500z A05;
    private final AnonymousClass1YI A06;
    private final AtomicInteger A07 = new AtomicInteger(0);
    private final C04860Wo[] A08;
    private final C001500z[] A09;
    private final Integer[] A0A;
    private final boolean[] A0B;
    private final boolean[] A0C;

    private synchronized boolean A00(Integer num) {
        boolean z;
        Integer[] numArr = this.A0A;
        int intValue = num.intValue();
        Integer num2 = numArr[intValue];
        z = true;
        if (num2 != null && this.A0C[intValue] == this.A06.AbO(num2.intValue(), this.A0B[intValue])) {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r1 == null) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean A01(java.lang.Integer r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            X.00z[] r1 = r2.A09     // Catch:{ all -> 0x0013 }
            int r0 = r3.intValue()     // Catch:{ all -> 0x0013 }
            r1 = r1[r0]     // Catch:{ all -> 0x0013 }
            X.00z r0 = r2.A05     // Catch:{ all -> 0x0013 }
            if (r1 == r0) goto L_0x0010
            r0 = 0
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 1
        L_0x0011:
            monitor-exit(r2)
            return r0
        L_0x0013:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Z9.A01(java.lang.Integer):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002c, code lost:
        if (A01(java.lang.Integer.valueOf(r5.A00)) == false) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.AnonymousClass0US A02() {
        /*
            r5 = this;
            monitor-enter(r5)
            r4 = r5
            monitor-enter(r4)     // Catch:{ all -> 0x004a }
            X.0US[] r0 = r5.A02     // Catch:{ all -> 0x0047 }
            int r3 = r0.length     // Catch:{ all -> 0x0047 }
            int r0 = r5.A00     // Catch:{ all -> 0x0047 }
            if (r0 < r3) goto L_0x000b
            goto L_0x0036
        L_0x000b:
            int r0 = r5.A00     // Catch:{ all -> 0x0047 }
            int r0 = r0 + 1
            r5.A00 = r0     // Catch:{ all -> 0x0047 }
            if (r0 >= r3) goto L_0x0036
            r2 = r5
            monitor-enter(r2)     // Catch:{ all -> 0x0047 }
            int r0 = r5.A00     // Catch:{ all -> 0x0033 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0033 }
            boolean r0 = r5.A00(r0)     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x002e
            int r0 = r5.A00     // Catch:{ all -> 0x0033 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0033 }
            boolean r1 = r5.A01(r0)     // Catch:{ all -> 0x0033 }
            r0 = 0
            if (r1 != 0) goto L_0x002f
        L_0x002e:
            r0 = 1
        L_0x002f:
            monitor-exit(r2)     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0036
            goto L_0x000b
        L_0x0033:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0047 }
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x0036:
            monitor-exit(r4)     // Catch:{ all -> 0x004a }
            int r3 = r5.A00     // Catch:{ all -> 0x004a }
            X.0US[] r2 = r5.A02     // Catch:{ all -> 0x004a }
            int r0 = r2.length     // Catch:{ all -> 0x004a }
            r1 = 0
            if (r3 < r0) goto L_0x0041
            monitor-exit(r5)
            return r1
        L_0x0041:
            r0 = r2[r3]     // Catch:{ all -> 0x004a }
            r2[r3] = r1     // Catch:{ all -> 0x004a }
            monitor-exit(r5)
            return r0
        L_0x0047:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x004a }
            throw r0     // Catch:{ all -> 0x004a }
        L_0x004a:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Z9.A02():X.0US");
    }

    public void A03(AnonymousClass0VL r12, C04800Wf r13, Executor executor) {
        boolean[] zArr = new boolean[this.A02.length];
        for (int i = 0; i < this.A02.length; i++) {
            Integer valueOf = Integer.valueOf(i);
            if (A00(valueOf) && A01(valueOf)) {
                zArr[i] = true;
                this.A01++;
            }
        }
        for (int i2 = 0; i2 < zArr.length; i2++) {
            if (zArr[i2]) {
                AnonymousClass0WW[] r6 = null;
                C04860Wo r0 = this.A08[i2];
                if (r0 != null) {
                    r6 = r0.A00;
                }
                this.A04.A03(r12, r6, this, Integer.valueOf(i2), r13, executor);
            }
        }
    }

    public void BVt(Object obj) {
        AnonymousClass0US[] r0 = this.A02;
        int intValue = ((Integer) obj).intValue();
        AnonymousClass1Z5 r2 = (AnonymousClass1Z5) r0[intValue].get();
        this.A02[intValue] = null;
        if (r2 != null) {
            r2.init();
        }
        this.A04.A02(r2.AkC());
        if (this.A07.incrementAndGet() == this.A01) {
            this.A03.open();
        }
    }

    public AnonymousClass1Z9(AnonymousClass0US[] r3, C001500z[] r4, C001500z r5, Integer[] numArr, boolean[] zArr, boolean[] zArr2, C04860Wo[] r9, AnonymousClass1YI r10, C04840Wj r11) {
        this.A02 = r3;
        this.A09 = r4;
        this.A05 = r5;
        this.A0A = numArr;
        this.A0B = zArr;
        this.A0C = zArr2;
        this.A08 = r9;
        this.A04 = r11;
        int length = r3.length;
        if (length == numArr.length && length == zArr.length && length == zArr2.length && length == r4.length && length == r9.length) {
            this.A06 = r10;
            return;
        }
        throw new RuntimeException("length of arrays does not match up!");
    }
}
