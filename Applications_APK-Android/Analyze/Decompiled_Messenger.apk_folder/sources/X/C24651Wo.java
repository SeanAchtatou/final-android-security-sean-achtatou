package X;

/* renamed from: X.1Wo  reason: invalid class name and case insensitive filesystem */
public final class C24651Wo implements AnonymousClass1WW {
    private static final Object A02 = new Object();
    private volatile AnonymousClass1WW A00;
    private volatile Object A01 = A02;

    public Object get() {
        Object obj;
        Object obj2 = this.A01;
        Object obj3 = A02;
        if (obj2 != obj3) {
            return obj2;
        }
        synchronized (this) {
            obj = this.A01;
            if (obj == obj3) {
                obj = this.A00.get();
                this.A01 = obj;
                this.A00 = null;
            }
        }
        return obj;
    }

    public C24651Wo(AnonymousClass1WW r2) {
        this.A00 = r2;
    }
}
