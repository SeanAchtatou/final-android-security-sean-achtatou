package X;

import android.net.Uri;

/* renamed from: X.1oX  reason: invalid class name and case insensitive filesystem */
public class C34031oX implements C23601Qd {
    public final String A00;

    public boolean AUG(Uri uri) {
        return this.A00.contains(uri.toString());
    }

    public String B7w() {
        return this.A00;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C34031oX) {
            return this.A00.equals(((C34031oX) obj).A00);
        }
        return false;
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        return this.A00;
    }

    public C34031oX(String str) {
        C05520Zg.A02(str);
        this.A00 = str;
    }
}
