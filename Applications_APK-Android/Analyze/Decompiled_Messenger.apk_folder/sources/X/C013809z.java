package X;

/* renamed from: X.09z  reason: invalid class name and case insensitive filesystem */
public final class C013809z implements AnonymousClass084 {
    public boolean A00;
    public boolean A01;
    public boolean A02;

    public String toString() {
        return "[ " + getClass().getSimpleName() + " dontSuspendDuringScroll: " + this.A00 + " ignoreNativeAllocs: " + this.A02 + " ignoreBackgroundGcs: " + this.A01 + " ]";
    }
}
