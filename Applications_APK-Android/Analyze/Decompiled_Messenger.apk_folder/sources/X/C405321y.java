package X;

import android.content.Intent;
import com.facebook.ui.media.attachments.model.MediaResource;

/* renamed from: X.21y  reason: invalid class name and case insensitive filesystem */
public final class C405321y {
    public static Intent A00(MediaResource mediaResource, double d) {
        Intent intent = new Intent("com.facebook.orca.media.upload.MEDIA_UPLOAD_PROGRESS");
        intent.putExtra("resource", mediaResource);
        intent.putExtra("p", d);
        return intent;
    }
}
