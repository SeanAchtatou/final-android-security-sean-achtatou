package X;

import java.util.List;

/* renamed from: X.0A9  reason: invalid class name */
public final class AnonymousClass0A9 {
    public static final List A00 = new AnonymousClass0AA();
    public static final List A01 = new C01680Bf();

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0043 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0006 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(android.content.Context r6) {
        /*
            java.util.List r0 = X.AnonymousClass0A9.A01
            java.util.Iterator r5 = r0.iterator()
        L_0x0006:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0064
            java.lang.Object r4 = r5.next()
            java.lang.String r4 = (java.lang.String) r4
            X.07y r0 = X.C009207y.A01
            boolean r0 = X.AnonymousClass0AB.A01(r6, r4, r0)
            if (r0 == 0) goto L_0x0006
            X.07y r1 = X.C009207y.A01
            r0 = 4160(0x1040, float:5.83E-42)
            X.0AC r2 = X.AnonymousClass0AB.A00(r6, r4, r0, r1)
            java.lang.Integer r1 = r2.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            if (r1 != r0) goto L_0x0040
            android.content.pm.PackageInfo r0 = r2.A01
            if (r0 == 0) goto L_0x0040
            android.content.pm.PackageInfo r0 = r2.A01
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            if (r0 == 0) goto L_0x0040
            android.content.pm.PackageInfo r2 = r2.A01
            android.content.pm.ApplicationInfo r0 = r2.applicationInfo
            int r1 = r0.flags
            r0 = r1 & 1
            if (r0 != 0) goto L_0x0044
            r0 = r1 & 128(0x80, float:1.794E-43)
            if (r0 != 0) goto L_0x0044
        L_0x0040:
            r0 = 0
        L_0x0041:
            if (r0 == 0) goto L_0x0006
            return r4
        L_0x0044:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 < r0) goto L_0x0062
            java.lang.String[] r3 = r2.requestedPermissions
            if (r3 == 0) goto L_0x0040
            int[] r2 = r2.requestedPermissionsFlags
            if (r2 == 0) goto L_0x0040
            r1 = 0
        L_0x0053:
            int r0 = r3.length
            if (r1 >= r0) goto L_0x0062
            int r0 = r2.length
            if (r1 >= r0) goto L_0x0062
            r0 = r2[r1]
            r0 = r0 & 2
            if (r0 == 0) goto L_0x0040
            int r1 = r1 + 1
            goto L_0x0053
        L_0x0062:
            r0 = 1
            goto L_0x0041
        L_0x0064:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0A9.A00(android.content.Context):java.lang.String");
    }

    public static boolean A01(String str) {
        if ("com.facebook.services.dev".equals(str) || "com.facebook.services".equals(str)) {
            return true;
        }
        return false;
    }
}
