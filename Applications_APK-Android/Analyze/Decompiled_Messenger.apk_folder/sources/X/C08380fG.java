package X;

import android.os.Build;
import android.os.Looper;
import com.facebook.quicklog.PerformanceLoggingEvent;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fG  reason: invalid class name and case insensitive filesystem */
public final class C08380fG extends AnonymousClass0f8 {
    public static final Thread A02 = Looper.getMainLooper().getThread();
    private static volatile C08380fG A03;
    public AnonymousClass0UN A00;
    public final Runtime A01 = Runtime.getRuntime();

    public String Azg() {
        return "memory_stats";
    }

    public static final C08380fG A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C08380fG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C08380fG(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        C12800pz r5;
        Future future = (Future) obj;
        Future future2 = (Future) obj2;
        if (future2 != null && performanceLoggingEvent.A0F == null) {
            if (future != null) {
                try {
                    r5 = (C12800pz) future.get();
                } catch (InterruptedException | ExecutionException e) {
                    ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00)).softReport("qpl.provider.memory_stats", "failed to decorate event", e);
                    return;
                }
            } else {
                r5 = null;
            }
            C12800pz r6 = (C12800pz) future2.get();
            performanceLoggingEvent.A06("avail_mem", r6.A02.availMem);
            performanceLoggingEvent.A06("low_mem", r6.A02.threshold);
            if (Build.VERSION.SDK_INT >= 16) {
                performanceLoggingEvent.A06("total_mem", r6.A02.totalMem);
            }
            if (r5 != null) {
                performanceLoggingEvent.A06("avail_mem_delta", r5.A02.availMem - r6.A02.availMem);
                performanceLoggingEvent.A06("java_heap_used", r6.A00 - r5.A00);
                performanceLoggingEvent.A06("native_heap_used", r6.A01 - r5.A01);
                performanceLoggingEvent.A06("java_heap_at_start", r5.A00);
                performanceLoggingEvent.A06("native_heap_at_start", r5.A01);
            }
        }
    }

    public long Azh() {
        return C08350fD.A0A;
    }

    public Class B3O() {
        return Future.class;
    }

    public /* bridge */ /* synthetic */ Object CGO() {
        C12790py r3 = new C12790py(this);
        if (Thread.currentThread() == A02) {
            return AnonymousClass07A.A03((ExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Aoc, this.A00), r3, -1424664133);
        }
        FutureTask futureTask = new FutureTask(r3);
        futureTask.run();
        return futureTask;
    }

    private C08380fG(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return true;
    }
}
