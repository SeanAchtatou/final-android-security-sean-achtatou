package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1dP  reason: invalid class name and case insensitive filesystem */
public final class C27571dP {
    public static boolean A02;
    private static volatile C27571dP A03;
    public AnonymousClass0UN A00;
    public boolean A01 = false;

    public static final C27571dP A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C27571dP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C27571dP(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C27571dP(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
