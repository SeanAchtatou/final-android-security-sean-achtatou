package X;

import android.os.Handler;
import android.os.Message;

/* renamed from: X.0YU  reason: invalid class name */
public final class AnonymousClass0YU implements Handler.Callback {
    public final /* synthetic */ AnonymousClass0YT A00;

    public AnonymousClass0YU(AnonymousClass0YT r1) {
        this.A00 = r1;
    }

    public boolean handleMessage(Message message) {
        AnonymousClass0YT.A01(this.A00);
        return true;
    }
}
