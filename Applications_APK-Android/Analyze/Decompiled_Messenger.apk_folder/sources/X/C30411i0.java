package X;

import com.facebook.messaging.notify.MultipleAccountsNewMessagesNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1i0  reason: invalid class name and case insensitive filesystem */
public final class C30411i0 extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$20";
    public final /* synthetic */ MultipleAccountsNewMessagesNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C30411i0(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, MultipleAccountsNewMessagesNotification multipleAccountsNewMessagesNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = multipleAccountsNewMessagesNotification;
    }
}
