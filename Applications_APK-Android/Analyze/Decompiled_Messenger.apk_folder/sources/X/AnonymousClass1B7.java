package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.common.util.ParcelablePair;
import com.facebook.content.SecureContextHelper;
import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.business.inboxads.common.InboxAdsItem;
import com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem;
import com.facebook.messaging.dialog.MenuDialogFragment;
import com.facebook.messaging.dialog.MenuDialogParams;
import com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem;
import com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit;
import com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper;
import com.facebook.messaging.inbox2.horizontaltiles.HorizontalTileInboxItem;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem;
import com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem;
import com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem;
import com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem;
import com.facebook.messaging.montage.model.MontageInboxNuxItem;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.facebook.workchat.inbox.pinnedthreads.editorder.WorkchatPinnedThreadsEditOrderActivity;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1B7  reason: invalid class name */
public final class AnonymousClass1B7 implements AnonymousClass1BC {
    private static final Class A0K = AnonymousClass1B7.class;
    public AnonymousClass0UN A00;
    public C139896fh A01;
    private C20961Em A02;
    private C102894vH A03;
    private AnonymousClass33E A04;
    public final Context A05;
    public final C13060qW A06;
    public final C32871mT A07;
    public final AnonymousClass17O A08;
    public final C15000uY A09;
    public final AnonymousClass1B2 A0A;
    public final AnonymousClass1B6 A0B;
    public final Map A0C;
    private final AnonymousClass146 A0D;
    private final AnonymousClass17S A0E;
    private final AnonymousClass1B8 A0F;
    private final FbSharedPreferences A0G;
    private final C14300t0 A0H;
    private final C191617a A0I;
    private final boolean A0J;

    public void BRY(DiscoverTabAttachmentUnit discoverTabAttachmentUnit) {
    }

    public void BWQ(DiscoverTabAttachmentItem discoverTabAttachmentItem) {
    }

    public void BcG(InboxUnitItem inboxUnitItem) {
    }

    public void Bdl(DiscoverLocationUpsellItem discoverLocationUpsellItem) {
    }

    public void Bdp(DiscoverLocationUpsellItem discoverLocationUpsellItem) {
    }

    private AnonymousClass33E A00() {
        if (this.A04 == null) {
            this.A04 = new AnonymousClass33E((C53492ks) AnonymousClass1XX.A03(AnonymousClass1Y3.A6L, this.A00), this.A05);
        }
        return this.A04;
    }

    private void A01(C66903Me r8) {
        boolean A072 = ((C05720aD) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Az1, this.A00)).A07(AnonymousClass24B.$const$string(6));
        String $const$string = C99084oO.$const$string(5);
        if (!A072) {
            ((C414325l) AnonymousClass1XX.A02(11, AnonymousClass1Y3.A9x, this.A00)).A06($const$string, this.A05.getString(2131825034), this.A05.getString(2131825033), r8);
            ((C414325l) AnonymousClass1XX.A02(11, AnonymousClass1Y3.A9x, this.A00)).A0B($const$string, this.A06, null);
        } else if (((C05720aD) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Az1, this.A00)).A08($const$string)) {
            this.A0I.A04(this.A05, $const$string, r8, null);
        } else {
            r8.BTM(null);
        }
    }

    private void A02(InboxUnitItem inboxUnitItem) {
        GraphQLMessengerInboxUnitType A0Q = inboxUnitItem.A02.A0Q();
        if (A0Q == null) {
            C010708t.A06(A0K, "null GraphQLMessengerInboxUnitType");
            return;
        }
        switch (A0Q.ordinal()) {
            case 1:
                this.A0B.BN9();
                break;
            case AnonymousClass1Y3.A03 /*12*/:
                this.A0B.Bf5();
                break;
            case AnonymousClass1Y3.A0B /*31*/:
                ((SecureContextHelper) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7S, this.A00)).startFacebookActivity(new Intent().setAction(C60982y9.A03).setData(Uri.parse(C52652jT.A0J)).putExtra(C99084oO.$const$string(40), C143186lf.A04), this.A05);
                break;
        }
        this.A07.A04(inboxUnitItem, "SEE_ALL", this.A0C);
    }

    public static void A03(AnonymousClass1B7 r3, ThreadSummary threadSummary, InboxUnitItem inboxUnitItem) {
        r3.A0B.BhB(threadSummary, inboxUnitItem.A07().A00, C14570tc.A02(inboxUnitItem, null), C13220qv.A0N);
    }

    private boolean A04(InboxUnitItem inboxUnitItem) {
        String A3y;
        C133186Lf r2 = new C133186Lf();
        GSTModelShape1S0000000 A0S = inboxUnitItem.A02.A0S();
        if (A0S != null && (A3y = A0S.A3y()) != null) {
            r2.A02 = A3y;
        } else if (inboxUnitItem.A0A() != null) {
            r2.A02 = inboxUnitItem.A0A();
        } else {
            r2.A00 = 2131833821;
        }
        r2.A01 = new ParcelablePair(null, inboxUnitItem);
        C135676Vz.A04(r2, inboxUnitItem);
        if (r2.A03.isEmpty()) {
            return false;
        }
        MenuDialogFragment A002 = MenuDialogFragment.A00(new MenuDialogParams(r2));
        A002.A25(this.A06, "inbox2_dialog");
        A002.A00 = new C138926e3(this);
        return true;
    }

    public void BC7() {
        C139896fh r0 = this.A01;
        if (r0 != null) {
            r0.A02.dismiss();
        }
    }

    public void BNS(InboxAdsItem inboxAdsItem, C72423eG r5, AnonymousClass5SI r6) {
        ((AnonymousClass5S4) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BD8, this.A00)).A01(inboxAdsItem, this.A06, r5, r6);
    }

    public void BNT(InboxAdsItem inboxAdsItem) {
        if (this.A0E == null || !((AnonymousClass1G4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BGY, this.A00)).A04.Aem(282428460500357L)) {
            this.A08.A0E(inboxAdsItem);
            return;
        }
        this.A0B.Bc3(inboxAdsItem);
        AnonymousClass1G9 r2 = (AnonymousClass1G9) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BDj, this.A00);
        r2.A00 = r2.A01.now();
        AnonymousClass17S r4 = this.A0E;
        synchronized (r4) {
            boolean z = false;
            if (r4.A00 != null) {
                z = true;
            }
            if (z) {
                r4.ARp();
                r4.A01 = null;
            }
            synchronized (r4) {
                r4.A02 = null;
            }
        }
        C21581Hw r7 = (C21581Hw) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BKZ, r4.A00);
        C21899AkD A002 = InboxUnitGraphQLQueryExecutorHelper.A00(inboxAdsItem.A02, inboxAdsItem.A01);
        ((C05160Xw) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AJD, r7.A00)).CIC(new C29718Egu(r7, inboxAdsItem.A02, A002.A04));
        ((C11170mD) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASp, r7.A00)).A06(A002, C137796c1.A02);
    }

    public void BNW(InboxAdsItem inboxAdsItem) {
        AnonymousClass5SM r3 = (AnonymousClass5SM) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Aop, this.A00);
        r3.A01 = new C111565Sz(this, inboxAdsItem);
        r3.A03(inboxAdsItem.A00, this.A06, this.A05);
    }

    public void BQe(Context context, InboxUnitThreadItem inboxUnitThreadItem, C23761Qt r7) {
        this.A07.A04(inboxUnitThreadItem, AnonymousClass08S.A0P(r7.A02, ":", r7.A03), this.A0C);
        r7.A00.BQf(context, inboxUnitThreadItem.A01);
    }

    public void BVr(InboxUnitThreadItem inboxUnitThreadItem) {
        C32871mT r3 = this.A07;
        C20961Em r2 = this.A02;
        if (r3 != null) {
            r3.A04(inboxUnitThreadItem, "inboxRight:Delete", null);
        }
        r2.A02(inboxUnitThreadItem.A01);
    }

    public void BWP(DiscoverTabAttachmentItem discoverTabAttachmentItem) {
        this.A0B.BWP(discoverTabAttachmentItem);
    }

    public void BXM(ImmutableList immutableList) {
        if (((C30479ExJ) AnonymousClass1XX.A02(15, AnonymousClass1Y3.Ack, this.A00)) != null) {
            Context context = this.A05;
            Intent intent = new Intent(context, WorkchatPinnedThreadsEditOrderActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("pinned_threads", new ArrayList(immutableList));
            intent.putExtras(bundle);
            C417626w.A06(intent, context);
        }
    }

    public void BZY(C64663Ct r2) {
        this.A0B.BZY(r2);
    }

    public void Bai(InboxUnitItem inboxUnitItem) {
        C27161ck r3 = inboxUnitItem.A02;
        C149226w7 r2 = new C149226w7();
        Bundle bundle = new Bundle();
        AnonymousClass324.A09(bundle, "node", r3);
        r2.A1P(bundle);
        r2.A00 = new C149726wv(this);
        r2.A25(this.A06, "inbox2_huc_dialog");
    }

    public void Bak(HorizontalTileInboxItem horizontalTileInboxItem) {
        ThreadKey A032;
        this.A07.A04(horizontalTileInboxItem, BuildConfig.FLAVOR, this.A0C);
        if (horizontalTileInboxItem.A01 != null) {
            this.A08.A0F(horizontalTileInboxItem);
        }
        if (horizontalTileInboxItem.A01.B6D() == C21381Gs.A0T) {
            A032 = ((AnonymousClass2F9) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A1J, this.A00)).A03(horizontalTileInboxItem.A00.A0j);
        } else {
            A032 = this.A09.A03(horizontalTileInboxItem.A00.A0Q);
        }
        this.A0B.BhA(A032, C14570tc.A02(horizontalTileInboxItem, null), C13220qv.A0A);
    }

    public void Bbw(InboxUnitItem inboxUnitItem) {
        ThreadKey threadKey;
        this.A0D.A0D(inboxUnitItem.A09());
        if (inboxUnitItem instanceof InboxUnitThreadItem) {
            InboxUnitThreadItem inboxUnitThreadItem = (InboxUnitThreadItem) inboxUnitItem;
            ThreadSummary threadSummary = inboxUnitThreadItem.A01;
            ThreadKey threadKey2 = threadSummary.A0S;
            if (AnonymousClass1Gw.A00(threadKey2)) {
                C11670nb A012 = C21451Ha.A01("sms_takeover_business_row");
                A012.A0D("action", "click_sms_business_row");
                C21451Ha.A05((C21451Ha) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AFw, this.A00), A012);
                this.A0B.Bh6(C149266wB.A03, null);
                return;
            }
            if (ThreadKey.A0E(threadKey2)) {
                boolean A092 = threadSummary.A09();
                boolean z = this.A0J;
                C11670nb A013 = C21451Ha.A01("sms_takeover_enter_sms_thread");
                A013.A0E("is_group_thread", A092);
                A013.A0E("in_chat_head", z);
                C21451Ha.A05((C21451Ha) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AFw, this.A00), A013);
            }
            if (((C139926fk) AnonymousClass1XX.A02(13, AnonymousClass1Y3.A4e, this.A00)).A02(threadSummary)) {
                C139896fh r3 = new C139896fh((C83633xw) AnonymousClass1XX.A03(AnonymousClass1Y3.A0s, this.A00), this.A05, threadSummary, this.A06, new AnonymousClass6LM(this, threadSummary, inboxUnitThreadItem));
                this.A01 = r3;
                r3.A00();
            } else {
                A03(this, threadSummary, inboxUnitItem);
            }
        } else if (inboxUnitItem instanceof InboxMoreThreadsItem) {
            if (((InboxMoreThreadsItem) inboxUnitItem).A00 == C192517k.LOAD_MORE) {
                this.A0B.Bd9();
            }
        } else if (inboxUnitItem instanceof InboxUnitConversationStarterItem) {
            InboxUnitConversationStarterItem inboxUnitConversationStarterItem = (InboxUnitConversationStarterItem) inboxUnitItem;
            if (inboxUnitConversationStarterItem.A00.Are() != null) {
                String A3m = inboxUnitConversationStarterItem.A00.Are().A3m();
                Preconditions.checkNotNull(A3m);
                threadKey = this.A09.A01(Long.parseLong(A3m));
            } else {
                threadKey = inboxUnitConversationStarterItem.A01.A0S;
                Preconditions.checkNotNull(threadKey);
            }
            this.A0B.BhA(threadKey, C14570tc.A02(inboxUnitConversationStarterItem, null), C13220qv.A07);
        } else if (inboxUnitItem instanceof MessageRequestsBannerInboxItem) {
            this.A0B.Bf5();
        } else if (inboxUnitItem instanceof InboxAdsItem) {
            ((AnonymousClass5S4) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BD8, this.A00)).A01((InboxAdsItem) inboxUnitItem, this.A06, C72423eG.A03, AnonymousClass5SI.A0H);
        } else if (inboxUnitItem instanceof InboxUnitSeeAllItem) {
            A02(inboxUnitItem);
        } else if ((inboxUnitItem instanceof InboxUnitSectionHeaderItem) && inboxUnitItem.A02.getBooleanValue(-579238035)) {
            Bng(inboxUnitItem);
        }
        if (inboxUnitItem.A0E()) {
            this.A07.A04(inboxUnitItem, BuildConfig.FLAVOR, this.A0C);
        }
        if (inboxUnitItem.A01 != null) {
            this.A08.A0F(inboxUnitItem);
        }
    }

    public boolean Bc6(InboxUnitItem inboxUnitItem) {
        C13060qW r3 = this.A06;
        if (C16000wK.A01(r3)) {
            if (inboxUnitItem instanceof InboxUnitThreadItem) {
                this.A0A.A05((InboxUnitThreadItem) inboxUnitItem, r3, new C138916e2(this));
                return true;
            } else if (inboxUnitItem.A02() != 0) {
                return A04(inboxUnitItem);
            }
        }
        return false;
    }

    public void BcY(InboxUnitThreadItem inboxUnitThreadItem) {
        this.A07.A04(inboxUnitThreadItem, "inboxLeft", this.A0C);
    }

    public void BfN(InboxMontageItem inboxMontageItem) {
        this.A07.A04(inboxMontageItem, BuildConfig.FLAVOR, this.A0C);
        if (inboxMontageItem.A01 != null) {
            this.A08.A0F(inboxMontageItem);
        }
        this.A0B.BhA(this.A09.A03(inboxMontageItem.A02.A01.A0K.A01), C14570tc.A02(inboxMontageItem, C115105dO.A00(inboxMontageItem)), C13220qv.A0D);
    }

    public void BfO(InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem) {
        ThreadKey threadKey;
        this.A07.A04(inboxUnitMontageActiveNowItem, BuildConfig.FLAVOR, this.A0C);
        if (inboxUnitMontageActiveNowItem.A01 != null) {
            this.A08.A0F(inboxUnitMontageActiveNowItem);
        }
        ThreadSummary A0I2 = inboxUnitMontageActiveNowItem.A0I();
        if (A0I2 != null) {
            threadKey = A0I2.A0S;
        } else {
            threadKey = null;
        }
        if (threadKey == null) {
            Preconditions.checkNotNull(inboxUnitMontageActiveNowItem.A0K());
            if (inboxUnitMontageActiveNowItem.A03.B6D() == C21381Gs.A0T) {
                threadKey = ((AnonymousClass2F9) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A1J, this.A00)).A03(inboxUnitMontageActiveNowItem.A0K().id);
            } else {
                threadKey = this.A09.A03(inboxUnitMontageActiveNowItem.A0K());
            }
        }
        if (AnonymousClass10R.A01((AnonymousClass10R) AnonymousClass1XX.A02(18, AnonymousClass1Y3.BO4, this.A00), true)) {
            int i = AnonymousClass1Y3.AoY;
            ((C74163hN) AnonymousClass1XX.A02(17, i, this.A00)).A05();
            C74163hN r4 = (C74163hN) AnonymousClass1XX.A02(17, i, this.A00);
            C74163hN.A01(r4, threadKey.A0I(), ((AnonymousClass06B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgK, r4.A00)).now(), 0, AnonymousClass2RN.A01);
        }
        AnonymousClass1B6 r5 = this.A0B;
        String str = inboxUnitMontageActiveNowItem.A00.A03;
        String A0U = inboxUnitMontageActiveNowItem.A02.A0U();
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        objectNode.put("r", str);
        objectNode.put("u", A0U);
        r5.BhA(threadKey, C14570tc.A02(inboxUnitMontageActiveNowItem, objectNode.toString()), C13220qv.A02);
    }

    public boolean BfP(InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem) {
        C405522a r5 = (C405522a) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AbY, this.A00);
        C13060qW r4 = this.A06;
        Context context = this.A05;
        User A0J2 = inboxUnitMontageActiveNowItem.A0J();
        if (A0J2 == null) {
            return false;
        }
        AnonymousClass59F r6 = (AnonymousClass59F) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BBE, r5.A00);
        String str = A0J2.A0j;
        C185414b r0 = r6.A01;
        C08900gA r8 = AnonymousClass59F.A02;
        r0.CH1(r8);
        C185414b r7 = r6.A01;
        AnonymousClass26K A002 = AnonymousClass26K.A00();
        A002.A04(C99084oO.$const$string(88), str);
        r7.AOO(r8, "open_long_tap_menu", null, A002);
        boolean A012 = ((AnonymousClass240) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AUO, r5.A00)).A01();
        if (!A012) {
            return false;
        }
        if (r5.A01.Aem(282432754419087L)) {
            AnonymousClass404 r62 = (AnonymousClass404) AnonymousClass1XX.A03(AnonymousClass1Y3.Ar0, r5.A00);
            AnonymousClass53H A022 = r62.A02(context);
            if (A012) {
                A022.add(2131828606).A02 = new AnonymousClass59G(r5, context, inboxUnitMontageActiveNowItem);
            }
            A022.A03 = false;
            r62.A01(context, A022).show();
            return true;
        }
        C133186Lf r63 = new C133186Lf();
        r63.A02 = BuildConfig.FLAVOR;
        r63.A04 = true;
        if (A012) {
            C1054852u r1 = new C1054852u();
            r1.A02 = 1;
            r1.A03 = 2131828606;
            r63.A00(r1.A00());
        }
        MenuDialogFragment A003 = MenuDialogFragment.A00(new MenuDialogParams(r63));
        A003.A00 = new AnonymousClass59E(r5, A0J2, context, inboxUnitMontageActiveNowItem);
        A003.A25(r4, "tag");
        return true;
    }

    public void BfU(InboxMontageItem inboxMontageItem) {
        A01(new C115085dM(this, inboxMontageItem));
    }

    public void BfZ(InboxMontageItem inboxMontageItem) {
        if (inboxMontageItem != null) {
            MontageInboxNuxItem montageInboxNuxItem = inboxMontageItem.A03;
            if (montageInboxNuxItem != null) {
                this.A0B.Bfa(montageInboxNuxItem.A00);
            }
            this.A07.A04(inboxMontageItem, "montage_nux", this.A0C);
        }
    }

    public void Bfb(InboxMontageItem inboxMontageItem) {
        MontageInboxNuxItem montageInboxNuxItem;
        if (inboxMontageItem != null && (montageInboxNuxItem = inboxMontageItem.A03) != null) {
            this.A0B.Bfc(montageInboxNuxItem.A00);
        }
    }

    public void Bff(long j) {
        A01(new C115115dP(this, j));
    }

    public void Bfh(InboxUnitThreadItem inboxUnitThreadItem) {
        C32871mT r2 = this.A07;
        if (r2 != null) {
            r2.A04(inboxUnitThreadItem, "inboxRight:More", null);
        }
        this.A0A.A05(inboxUnitThreadItem, this.A06, new C138916e2(this));
    }

    public void Bg1(InboxUnitThreadItem inboxUnitThreadItem) {
        AnonymousClass5C0.A01(inboxUnitThreadItem, this.A07, this.A06);
    }

    public void Bh1(InboxUnitThreadItem inboxUnitThreadItem) {
        if (this.A03 == null) {
            this.A03 = new C102894vH(this.A0F, this.A05, this.A06);
        }
        AnonymousClass5C0.A02(inboxUnitThreadItem, this.A07, this.A03);
    }

    public void BhR(UserKey userKey, long j, InboxMontageItem inboxMontageItem) {
        this.A0B.BhP(userKey, j, inboxMontageItem);
    }

    public void Bii(InboxUnitThreadItem inboxUnitThreadItem) {
        new AnonymousClass2IC((C46492Qg) AnonymousClass1XX.A03(AnonymousClass1Y3.AOB, this.A00), this.A05);
    }

    public void Bmk(InboxUnitThreadItem inboxUnitThreadItem) {
        this.A07.A04(inboxUnitThreadItem, "inboxRight", this.A0C);
    }

    public void BnL() {
        this.A0B.Bh7();
    }

    public void BpT(InboxUnitThreadItem inboxUnitThreadItem) {
        AnonymousClass5C0.A00(this.A05, inboxUnitThreadItem, this.A07, (C33331nP) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BH8, this.A00), this.A0H, A00());
    }

    public void Bpd(ImmutableList immutableList, long j, InboxUnitItem inboxUnitItem, C114765cl r12) {
        A01(new C115095dN(this, immutableList, j, inboxUnitItem, r12));
    }

    public void Bpn(InboxUnitThreadItem inboxUnitThreadItem) {
        String str;
        String str2;
        Context context = this.A05;
        C32871mT r4 = this.A07;
        C33331nP r3 = (C33331nP) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BH8, this.A00);
        C14300t0 r2 = this.A0H;
        AnonymousClass33E A002 = A00();
        if (r4 != null) {
            r4.A04(inboxUnitThreadItem, "inboxLeft:RTC", null);
        }
        ThreadSummary threadSummary = inboxUnitThreadItem.A01;
        User A032 = r2.A03(ThreadKey.A07(threadSummary.A0S));
        boolean A003 = C22541Lt.A00(context);
        if (C33331nP.A03(r3, threadSummary, true)) {
            ThreadKey threadKey = threadSummary.A0S;
            if (A003) {
                str2 = "multiway_join_chat_head_inbox_swipe_menu_video_button";
            } else {
                str2 = "multiway_join_inbox_swipe_menu_video_button";
            }
            A002.A03(threadKey, threadSummary, true, str2, null);
        } else if (r3.A06(threadSummary, A032)) {
            ThreadKey threadKey2 = threadSummary.A0S;
            if (A003) {
                str = "chat_head_inbox_swipe_menu_video_button";
            } else {
                str = "inbox_swipe_menu_video_button";
            }
            A002.A02(threadKey2, threadSummary, A032, str, null, A003);
        }
    }

    public void Bt6() {
        C30281hn edit = this.A0G.edit();
        edit.putBoolean(C34341pQ.A00, true);
        edit.commit();
        ((C39281yn) AnonymousClass1XX.A03(AnonymousClass1Y3.BGm, this.A00)).A01(true);
    }

    public void BtB(InboxUnitThreadItem inboxUnitThreadItem) {
        new AnonymousClass2IC((C46492Qg) AnonymousClass1XX.A03(AnonymousClass1Y3.AOB, this.A00), this.A05);
    }

    public void BtG(InboxUnitThreadItem inboxUnitThreadItem) {
        C32871mT r3 = this.A07;
        C14780u1 r2 = (C14780u1) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BLe, this.A00);
        if (r3 != null) {
            r3.A04(inboxUnitThreadItem, "inboxRight:Unmute", null);
        }
        r2.A08(inboxUnitThreadItem.A01.A0S);
    }

    public AnonymousClass1B7(AnonymousClass1XY r4, Context context, C13060qW r6, AnonymousClass17O r7, AnonymousClass17S r8, C32871mT r9, AnonymousClass1B2 r10, C20961Em r11, AnonymousClass1B6 r12) {
        String str;
        this.A00 = new AnonymousClass0UN(19, r4);
        this.A0G = FbSharedPreferencesModule.A00(r4);
        this.A0D = AnonymousClass146.A00(r4);
        this.A09 = C14990uX.A00(r4);
        this.A0H = C14300t0.A00(r4);
        this.A0I = C191617a.A00(r4);
        this.A0F = new AnonymousClass1B8(r4);
        this.A05 = context;
        boolean A002 = C22541Lt.A00(context);
        this.A0J = A002;
        HashMap hashMap = new HashMap();
        if (A002) {
            str = "1";
        } else {
            str = "0";
        }
        hashMap.put("ch", str);
        this.A0C = hashMap;
        this.A06 = r6;
        this.A08 = r7;
        this.A0E = r8;
        this.A07 = r9;
        this.A0A = r10;
        this.A0B = r12;
        this.A02 = r11;
    }

    public boolean Bal(HorizontalTileInboxItem horizontalTileInboxItem) {
        return A04(horizontalTileInboxItem);
    }

    public void Bng(InboxUnitItem inboxUnitItem) {
        A02(inboxUnitItem);
    }
}
