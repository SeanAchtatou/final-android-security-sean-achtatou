package X;

import com.facebook.messaging.integrity.frx.model.AdditionalAction;
import io.card.payment.BuildConfig;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.20q  reason: invalid class name and case insensitive filesystem */
public final class C401920q {
    public C34891qL A00;
    public Integer A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public Set A07;
    public boolean A08;

    public C401920q() {
        this.A07 = new HashSet();
        this.A03 = BuildConfig.FLAVOR;
        this.A05 = BuildConfig.FLAVOR;
    }

    public C401920q(AdditionalAction additionalAction) {
        this.A07 = new HashSet();
        C28931fb.A05(additionalAction);
        if (additionalAction instanceof AdditionalAction) {
            this.A01 = additionalAction.A01;
            this.A02 = additionalAction.A02;
            this.A03 = additionalAction.A03;
            this.A08 = additionalAction.A08;
            this.A00 = additionalAction.A00;
            this.A04 = additionalAction.A04;
            this.A05 = additionalAction.A05;
            this.A06 = additionalAction.A06;
            this.A07 = new HashSet(additionalAction.A07);
            return;
        }
        Integer A012 = additionalAction.A01();
        this.A01 = A012;
        C28931fb.A06(A012, "actionType");
        this.A07.add("actionType");
        this.A02 = additionalAction.A02;
        String str = additionalAction.A03;
        this.A03 = str;
        C28931fb.A06(str, C99084oO.$const$string(AnonymousClass1Y3.A3c));
        this.A08 = additionalAction.A08;
        C34891qL A002 = additionalAction.A00();
        this.A00 = A002;
        String $const$string = C99084oO.$const$string(561);
        C28931fb.A06(A002, $const$string);
        this.A07.add($const$string);
        this.A04 = additionalAction.A04;
        String str2 = additionalAction.A05;
        this.A05 = str2;
        C28931fb.A06(str2, "title");
        this.A06 = additionalAction.A06;
    }
}
