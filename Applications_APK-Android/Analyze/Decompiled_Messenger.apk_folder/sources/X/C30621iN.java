package X;

import android.database.sqlite.SQLiteDatabase;
import com.facebook.proxygen.TraceFieldType;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1iN  reason: invalid class name and case insensitive filesystem */
public final class C30621iN extends AnonymousClass0W4 {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6(AnonymousClass24B.$const$string(306), "INTEGER");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("expired", "INTEGER");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6(TraceFieldType.MsgId, "TEXT");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("retries_left", "INTEGER");

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public C30621iN() {
        super("retry_status", ImmutableList.of(A02, A00, A03, A01));
    }
}
