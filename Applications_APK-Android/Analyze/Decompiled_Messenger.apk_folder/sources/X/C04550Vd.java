package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.stringformat.StringFormatUtil;
import com.google.common.base.Preconditions;
import java.util.AbstractQueue;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Vd  reason: invalid class name and case insensitive filesystem */
public final class C04550Vd extends AbstractQueue<Runnable> implements BlockingQueue<Runnable> {
    public int A00;
    public int A01;
    public C04590Vi A02;
    private int A03;
    private C07820eD A04;
    public final AnonymousClass1YF A05;
    public final C04560Ve A06;
    public final AnonymousClass0VY A07;
    public final AnonymousClass0VV A08;
    public final ThreadLocal A09 = new ThreadLocal();
    public final Queue A0A = new ArrayDeque();
    public final boolean A0B;
    private final AnonymousClass1YF A0C;
    private final boolean A0D;
    public volatile Executor A0E;

    public Object take() {
        C07820eD A012 = A01(false, 0);
        Preconditions.checkNotNull(A012);
        return A012;
    }

    private C07820eD A00() {
        C07820eD r1;
        while (true) {
            r1 = (C07820eD) this.A0A.poll();
            if (r1 == null) {
                r1 = this.A07.A0H(AnonymousClass0Y1.A00(), AnonymousClass07B.A01);
            }
            if (r1 == null) {
                return null;
            }
            if (!(r1 instanceof C07690e0) || !((C07690e0) r1).isCancelled()) {
                return r1;
            }
            A02(this, r1);
        }
        return r1;
    }

    private C07820eD A01(boolean z, long j) {
        int i;
        IllegalStateException th;
        long j2;
        long j3 = j;
        String $const$string = TurboLoader.Locator.$const$string(44);
        this.A06.A03();
        int holdCount = this.A06.A05.getHoldCount();
        try {
            Preconditions.checkState(this.A06.A05.isHeldByCurrentThread(), "Lock not held before end. holdcount=%s", holdCount);
            C07820eD r2 = (C07820eD) this.A09.get();
            if (r2 != null) {
                A02(this, r2);
            }
            i = 0;
            while (this.A06.A05.isHeldByCurrentThread()) {
                try {
                    i++;
                    C07820eD A002 = A00();
                    if (A002 != null) {
                        try {
                            this.A06.A02();
                            return A002;
                        } catch (IllegalMonitorStateException e) {
                            IllegalMonitorStateException illegalMonitorStateException = new IllegalMonitorStateException(StringFormatUtil.formatStrLocaleSafe("%s returnValue=%s loopcount=%d holdcount=%d error=%s", e.getMessage(), "nonnull", Integer.valueOf(i), Integer.valueOf(holdCount), $const$string));
                            illegalMonitorStateException.initCause(e);
                            throw illegalMonitorStateException;
                        }
                    } else if (!z || j3 > 0) {
                        AnonymousClass0VY r22 = this.A07;
                        long A003 = AnonymousClass0Y1.A00();
                        C07690e0 r23 = (C07690e0) r22.A05.peek();
                        if (r23 == null) {
                            j2 = Long.MAX_VALUE;
                        } else {
                            j2 = r23.A00() - A003;
                        }
                        if (j2 > 0) {
                            this.A00++;
                            if (z) {
                                try {
                                    long min = Math.min(j2, j3);
                                    j3 -= min - this.A05.A01(min);
                                } catch (Throwable th2) {
                                    th = th2;
                                    int i2 = this.A00 - 1;
                                    this.A00 = i2;
                                    boolean z2 = false;
                                    if (i2 >= 0) {
                                        z2 = true;
                                    }
                                    Preconditions.checkState(z2);
                                    int i3 = this.A01;
                                    if (i3 > 0) {
                                        this.A01 = i3 - 1;
                                    }
                                }
                            } else if (j2 == Long.MAX_VALUE) {
                                AnonymousClass1YF.A00(this.A05, false, 0, 0);
                            } else {
                                this.A05.A01(j2);
                            }
                            int i4 = this.A00 - 1;
                            this.A00 = i4;
                            boolean z3 = false;
                            if (i4 >= 0) {
                                z3 = true;
                            }
                            Preconditions.checkState(z3);
                            int i5 = this.A01;
                            if (i5 > 0) {
                                this.A01 = i5 - 1;
                            }
                        }
                    } else {
                        try {
                            this.A06.A02();
                            return null;
                        } catch (IllegalMonitorStateException e2) {
                            IllegalMonitorStateException illegalMonitorStateException2 = new IllegalMonitorStateException(StringFormatUtil.formatStrLocaleSafe("%s returnValue=%s loopcount=%d holdcount=%d error=%s", e2.getMessage(), "null", Integer.valueOf(i), Integer.valueOf(holdCount), $const$string));
                            illegalMonitorStateException2.initCause(e2);
                            throw illegalMonitorStateException2;
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    try {
                        throw e;
                    } catch (Throwable th3) {
                        th = th3;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    e = null;
                    try {
                        this.A06.A02();
                        throw th;
                    } catch (IllegalMonitorStateException e4) {
                        if (e != null) {
                            AnonymousClass8NQ.A00(e4, e);
                        }
                        String message = e4.getMessage();
                        Integer valueOf = Integer.valueOf(i);
                        Integer valueOf2 = Integer.valueOf(holdCount);
                        if (e != null) {
                            $const$string = e.getMessage();
                        }
                        IllegalMonitorStateException illegalMonitorStateException3 = new IllegalMonitorStateException(StringFormatUtil.formatStrLocaleSafe("%s returnValue=%s loopcount=%d holdcount=%d error=%s", message, "null", valueOf, valueOf2, $const$string));
                        illegalMonitorStateException3.initCause(e4);
                        throw illegalMonitorStateException3;
                    }
                }
            }
            th = new IllegalStateException(Preconditions.format("Lock not held. loopCount=%s holdcount=%s", Integer.valueOf(i), Integer.valueOf(holdCount)));
            throw th;
        } catch (Exception e5) {
            e = e5;
            i = 0;
            throw e;
        } catch (Throwable th5) {
            th = th5;
            e = null;
            i = 0;
            this.A06.A02();
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r1 == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
        if (r1.A05.size() != 0) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(X.C04550Vd r2, X.C07820eD r3) {
        /*
            java.lang.ThreadLocal r1 = r2.A09
            r0 = 0
            r1.set(r0)
            X.0Vk r0 = r3.AZE()
            r0.A0B(r3)
            r0.A07()
            X.0VY r1 = r2.A07
            X.0Vc r0 = r1.A02
            int r0 = r0.A00
            if (r0 != 0) goto L_0x0025
            int r0 = r1.A00
            if (r0 != 0) goto L_0x0025
            java.util.PriorityQueue r0 = r1.A05
            int r1 = r0.size()
            r0 = 1
            if (r1 == 0) goto L_0x0026
        L_0x0025:
            r0 = 0
        L_0x0026:
            if (r0 == 0) goto L_0x0037
            X.0Ve r0 = r2.A06
            java.util.concurrent.ConcurrentLinkedQueue r0 = r0.A02
            java.lang.Object r0 = r0.peek()
            r1 = 0
            if (r0 != 0) goto L_0x0034
            r1 = 1
        L_0x0034:
            r0 = 1
            if (r1 != 0) goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            if (r0 == 0) goto L_0x004c
            X.1YF r0 = r2.A0C
            r0.A03()
            X.0Ve r0 = r2.A06
            java.util.concurrent.locks.ReentrantLock r0 = r0.A05
            boolean r1 = r0.isHeldByCurrentThread()
            java.lang.String r0 = "Lock not held"
            com.google.common.base.Preconditions.checkState(r1, r0)
        L_0x004c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04550Vd.A02(X.0Vd, X.0eD):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r5.getDelay(java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.C04550Vd r8, X.C07820eD r9) {
        /*
            int r0 = r8.A03
            r6 = 1
            if (r0 >= r6) goto L_0x0051
            boolean r0 = r9 instanceof X.C07690e0
            if (r0 == 0) goto L_0x0018
            r5 = r9
            X.0e0 r5 = (X.C07690e0) r5
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r3 = r5.getDelay(r0)
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0019
        L_0x0018:
            r5 = 0
        L_0x0019:
            if (r5 == 0) goto L_0x0052
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r0 = r5.getDelay(r0)
            r8.A05(r5, r0)
            X.0Vk r0 = r5.AZE()
            r0.A06()
            X.0Vk r0 = r5.AZE()
            r0.A08(r5)
            X.0VY r0 = r8.A07
            java.util.PriorityQueue r0 = r0.A05
            r0.offer(r5)
            X.0VY r0 = r8.A07
            X.0Vc r0 = r0.A02
            int r2 = r0.A00
            int r1 = r0.A01
            r0 = 0
            if (r2 >= r1) goto L_0x0045
            r0 = 1
        L_0x0045:
            if (r0 == 0) goto L_0x004c
            X.0Vi r0 = r8.A02
            r0.prestartCoreThread()
        L_0x004c:
            X.1YF r0 = r8.A05
            r0.A02()
        L_0x0051:
            return
        L_0x0052:
            r0 = 0
            r8.A05(r9, r0)
            X.0Vk r0 = r9.AZE()
            r0.A06()
            X.0Vk r0 = r9.AZE()
            r0.A08(r9)
            long r2 = X.AnonymousClass0Y1.A00()
            boolean r0 = r8.A0B
            if (r0 == 0) goto L_0x00b7
            X.0VY r0 = r8.A07
            r0.A0I(r9)
        L_0x0072:
            X.0VY r1 = r8.A07
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            X.0eD r5 = r1.A0H(r2, r0)
            if (r5 == 0) goto L_0x0051
            X.0VY r7 = r8.A07
            X.0Vc r0 = r7.A02
            int r4 = r0.A00
            int r1 = r0.A01
            r0 = 0
            if (r4 >= r1) goto L_0x0088
            r0 = 1
        L_0x0088:
            if (r0 == 0) goto L_0x0051
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            X.0eD r1 = r7.A0H(r2, r0)
            r4 = 0
            r0 = 0
            if (r1 != r5) goto L_0x0095
            r0 = 1
        L_0x0095:
            com.google.common.base.Preconditions.checkState(r0)
            int r1 = r8.A00
            int r0 = r8.A01
            if (r1 <= r0) goto L_0x009f
            r4 = 1
        L_0x009f:
            if (r4 == 0) goto L_0x00b3
            int r0 = r0 + r6
            r8.A01 = r0
            java.util.Queue r0 = r8.A0A
            boolean r0 = r0.offer(r5)
            com.google.common.base.Preconditions.checkState(r0)
            X.1YF r0 = r8.A05
            r0.A02()
            goto L_0x0072
        L_0x00b3:
            r8.A04(r5)
            goto L_0x0072
        L_0x00b7:
            int r1 = r8.A00
            int r0 = r8.A01
            r7 = 0
            if (r1 <= r0) goto L_0x00bf
            r7 = 1
        L_0x00bf:
            if (r7 != 0) goto L_0x00ec
            X.0VY r1 = r8.A07
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            X.0eD r4 = r1.A0H(r2, r0)
            X.0Vk r1 = r9.AZE()
            boolean r0 = r1 instanceof X.C04610Vk
            com.google.common.base.Preconditions.checkState(r0)
            boolean r0 = r1.A0F(r9)
            if (r0 == 0) goto L_0x00e0
            if (r4 == 0) goto L_0x00e7
            int r0 = X.C04530Vb.A00(r9, r4)
            if (r0 <= 0) goto L_0x00e7
        L_0x00e0:
            r0 = 0
        L_0x00e1:
            if (r0 == 0) goto L_0x00ec
            r8.A04(r9)
            return
        L_0x00e7:
            r1.A09(r9)     // Catch:{ IllegalStateException -> 0x010d }
            r0 = 1
            goto L_0x00e1
        L_0x00ec:
            X.0VY r0 = r8.A07
            r0.A0I(r9)
            if (r7 == 0) goto L_0x004c
            int r0 = r8.A01
            int r0 = r0 + r6
            r8.A01 = r0
            X.0VY r1 = r8.A07
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            X.0eD r1 = r1.A0H(r2, r0)
            if (r1 == 0) goto L_0x004c
            java.util.Queue r0 = r8.A0A
            boolean r0 = r0.offer(r1)
            com.google.common.base.Preconditions.checkState(r0)
            goto L_0x004c
        L_0x010d:
            r2 = move-exception
            java.lang.String r1 = r2.getMessage()
            if (r1 != 0) goto L_0x0116
            java.lang.String r1 = "<null message>"
        L_0x0116:
            java.lang.String r0 = "%s task=%s head=%s"
            java.lang.String r1 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1, r9, r4)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1, r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04550Vd.A03(X.0Vd, X.0eD):void");
    }

    /* JADX INFO: finally extract failed */
    private void A04(C07820eD r3) {
        boolean z = false;
        if (this.A04 == null) {
            z = true;
        }
        Preconditions.checkState(z);
        try {
            this.A04 = r3;
            this.A02.execute(r3);
            this.A04 = null;
        } catch (Throwable th) {
            this.A04 = null;
            throw th;
        }
    }

    private void A05(C07820eD r11, long j) {
        if (this.A0D) {
            AnonymousClass0VY r1 = this.A07;
            int i = r1.A02.A00;
            int size = r1.A04.size();
            AnonymousClass0VY r0 = this.A07;
            r11.C6z(new C52952jx(j, i, size, r0.A00, r0.A05.size(), this.A02.getPoolSize()));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: A06 */
    public boolean offer(Runnable runnable) {
        this.A06.A00();
        try {
            Preconditions.checkNotNull(this.A04);
            boolean z = false;
            if (this.A04 == runnable) {
                z = true;
            }
            Preconditions.checkNotNull(Boolean.valueOf(z));
            return false;
        } finally {
            this.A06.A02();
        }
    }

    public void A07() {
        boolean z;
        this.A06.A00();
        try {
            C07820eD r0 = (C07820eD) this.A09.get();
            if (r0 == null) {
                z = false;
            } else {
                A02(this, r0);
                z = true;
            }
            if (z) {
                this.A05.A02();
            }
        } finally {
            this.A06.A02();
        }
    }

    public Iterator iterator() {
        throw new UnsupportedOperationException("iterator not implamented");
    }

    public Object peek() {
        this.A06.A00();
        try {
            return this.A07.A0H(AnonymousClass0Y1.A00(), AnonymousClass07B.A00);
        } finally {
            this.A06.A02();
        }
    }

    public void put(Object obj) {
        offer((Runnable) obj);
    }

    public int remainingCapacity() {
        throw new UnsupportedOperationException("remainingCapacity not implamented");
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("remove not implamented");
    }

    public int size() {
        int i;
        this.A06.A00();
        try {
            if (this.A03 >= 2) {
                i = 0;
            } else {
                AnonymousClass0VY r0 = this.A07;
                int size = r0.A00 + r0.A05.size();
                int i2 = 0;
                if (this.A06.A02.peek() == null) {
                    i2 = 1;
                }
                i = size + (i2 ^ 1);
            }
            return i;
        } finally {
            this.A06.A02();
        }
    }

    public C04550Vd(AnonymousClass0VY r3, AnonymousClass0VX r4) {
        boolean z = false;
        this.A03 = 0;
        this.A07 = r3;
        this.A0D = r4.A07 != null ? true : z;
        this.A08 = r4.A06;
        this.A0B = r4.A0A;
        C04560Ve r1 = new C04560Ve(this);
        this.A06 = r1;
        this.A05 = r1.A01;
        this.A0C = new AnonymousClass1YF(r1);
    }

    public int drainTo(Collection collection) {
        this.A06.A00();
        try {
            int i = this.A03;
            boolean z = true;
            if (i < 1) {
                z = false;
            }
            Preconditions.checkState(z);
            this.A03 = Math.max(i, 2);
            return 0;
        } finally {
            this.A06.A02();
        }
    }

    public int drainTo(Collection collection, int i) {
        throw new UnsupportedOperationException("drainTo not implamented");
    }

    public boolean offer(Object obj, long j, TimeUnit timeUnit) {
        return offer((Runnable) obj);
    }

    public Object poll() {
        this.A06.A00();
        try {
            C07820eD r0 = (C07820eD) this.A09.get();
            if (r0 != null) {
                A02(this, r0);
            }
            return A00();
        } finally {
            this.A06.A02();
        }
    }

    public Object poll(long j, TimeUnit timeUnit) {
        return A01(true, timeUnit.toNanos(j));
    }
}
