package X;

import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.1OV  reason: invalid class name */
public final class AnonymousClass1OV {
    public C23051Oa A00;
    private AnonymousClass1OW A01;
    private final QuickPerformanceLogger A02;

    private C23211Oq A00(C30781id r7, AnonymousClass1P6 r8) {
        try {
            QuickPerformanceLogger quickPerformanceLogger = this.A02;
            if (quickPerformanceLogger != null) {
                quickPerformanceLogger.markerStart(37945346);
            }
            C23211Oq C3k = r7.C3k(r8);
            QuickPerformanceLogger quickPerformanceLogger2 = this.A02;
            if (quickPerformanceLogger2 != null) {
                if (r7.getName() != null) {
                    quickPerformanceLogger2.markerAnnotate(37945346, "name", r7.getName());
                }
                QuickPerformanceLogger quickPerformanceLogger3 = this.A02;
                short s = 3;
                if (C3k.A03()) {
                    s = 2;
                }
                quickPerformanceLogger3.markerEnd(37945346, s);
            }
            QuickPerformanceLogger quickPerformanceLogger4 = this.A02;
            if (quickPerformanceLogger4 != null) {
                quickPerformanceLogger4.markerEnd(37945346, 4);
            }
            return C3k;
        } catch (C37861wT e) {
            this.A00.C08(r7, e.getMessage(), r7.Acu());
            C23211Oq r1 = new C23211Oq(r7, null);
            QuickPerformanceLogger quickPerformanceLogger5 = this.A02;
            if (quickPerformanceLogger5 != null) {
                quickPerformanceLogger5.markerEnd(37945346, 4);
            }
            return r1;
        } catch (Throwable th) {
            QuickPerformanceLogger quickPerformanceLogger6 = this.A02;
            if (quickPerformanceLogger6 != null) {
                quickPerformanceLogger6.markerEnd(37945346, 4);
            }
            throw th;
        }
    }

    public C23211Oq A01(long j) {
        return A00(this.A01.A00(j), null);
    }

    public C23211Oq A02(long j, AnonymousClass1P6 r4) {
        return A00(this.A01.A00(j), r4);
    }

    public AnonymousClass1OV(AnonymousClass1OW r1, C23051Oa r2, QuickPerformanceLogger quickPerformanceLogger) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = quickPerformanceLogger;
    }
}
