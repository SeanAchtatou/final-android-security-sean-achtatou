package X;

/* renamed from: X.1d0  reason: invalid class name and case insensitive filesystem */
public final class C27321d0 {
    public static final C27321d0 A00 = new C27321d0();

    public void A00() {
        if (AnonymousClass08Z.A05(4)) {
            AnonymousClass00C.A00(4, 1973692568);
        }
        C010708t.A0X(2);
    }

    public void A01() {
        if (AnonymousClass08Z.A05(4)) {
            AnonymousClass00C.A00(4, -1796659989);
        }
        if (C010708t.A0X(2)) {
            C010708t.A01.BFj(2);
        }
    }

    public void A02() {
        if (AnonymousClass08Z.A05(4)) {
            AnonymousClass00C.A00(4, -1577137399);
        }
        C010708t.A0X(2);
    }

    public void A03() {
        if (AnonymousClass08Z.A05(4)) {
            AnonymousClass00C.A00(4, 1527315541);
        }
        C010708t.A0X(2);
    }

    public void A04(String str) {
        if (AnonymousClass08Z.A05(4)) {
            C02320Ea A002 = AnonymousClass0EY.A00(4, AnonymousClass0EY.A00, "KILL_SWITCH");
            A002.A02("kill_switch_name", str);
            A002.A03();
        }
        C010708t.A0X(2);
    }

    public void A05(String str, String str2) {
        if (AnonymousClass08Z.A05(4)) {
            C02320Ea A002 = AnonymousClass0EY.A00(4, AnonymousClass0EY.A00, "IMPLEMENTATION:INTERFACE_METHOD_CALL");
            A002.A02("interface_name", str);
            A002.A02("interface_method_name", str2);
            A002.A03();
        }
        C010708t.A0X(2);
    }

    public void A06(String str, String str2) {
        if (AnonymousClass08Z.A05(4)) {
            C02320Ea A002 = AnonymousClass0EY.A00(4, AnonymousClass0EY.A00, "IMPLEMENTATIONS:IS_NEEDED");
            A002.A02("interface_name", str2);
            A002.A02("implementation_name", str);
            A002.A03();
        }
        C010708t.A0X(2);
    }

    public void A07(String str, String str2, String str3) {
        if (AnonymousClass08Z.A05(4)) {
            C02320Ea A002 = AnonymousClass0EY.A00(4, AnonymousClass0EY.A00, "IMPLEMENTATIONS:METHOD_CALL");
            A002.A02("interface_name", str2);
            A002.A02("interface_method_name", str3);
            A002.A02("implementation_name", str);
            A002.A03();
        }
        if (C010708t.A0X(2)) {
            C010708t.A01.BFj(2);
        }
    }

    private C27321d0() {
    }
}
