package X;

/* renamed from: X.05S  reason: invalid class name */
public final class AnonymousClass05S implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.profilo.provider.frames.FramesProvider$3";
    public final /* synthetic */ AnonymousClass04q A00;

    public AnonymousClass05S(AnonymousClass04q r1) {
        this.A00 = r1;
    }

    public void run() {
        long j = this.A00.A0A;
        if (this.A00.A01.get() && j != -1) {
            AnonymousClass04q.A04("ScrollPerf.FrameEnded");
        }
    }
}
