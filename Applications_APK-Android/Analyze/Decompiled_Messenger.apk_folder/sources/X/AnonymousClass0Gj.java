package X;

/* renamed from: X.0Gj  reason: invalid class name */
public final class AnonymousClass0Gj implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        AnonymousClass0FL r62 = (AnonymousClass0FL) r6;
        int i = r62.bleScanCount;
        if (i != 0) {
            r7.AMU("ble_scan_count", i);
        }
        long j = r62.bleScanDurationMs;
        if (j != 0) {
            r7.AMV("ble_scan_duration_ms", j);
        }
        int i2 = r62.bleOpportunisticScanCount;
        if (i2 != 0) {
            r7.AMU("ble_opportunistic_scan_count", i2);
        }
        if (r62.bleScanDurationMs != 0) {
            r7.AMV("ble_opportunistic_scan_duration_ms", r62.bleOpportunisticScanDurationMs);
        }
    }
}
