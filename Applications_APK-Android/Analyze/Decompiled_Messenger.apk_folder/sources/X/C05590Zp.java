package X;

/* renamed from: X.0Zp  reason: invalid class name and case insensitive filesystem */
public final class C05590Zp implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.prefs.shared.FbSharedPreferencesImpl$1";
    public final /* synthetic */ AnonymousClass0VD A00;

    public C05590Zp(AnonymousClass0VD r1) {
        this.A00 = r1;
    }

    public void run() {
        while (true) {
            Runnable runnable = (Runnable) this.A00.A02.poll();
            if (runnable != null) {
                runnable.run();
            } else {
                return;
            }
        }
    }
}
