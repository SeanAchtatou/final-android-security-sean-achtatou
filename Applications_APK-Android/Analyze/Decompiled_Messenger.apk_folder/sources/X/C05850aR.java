package X;

import android.app.Application;
import android.content.Context;
import com.facebook.graphservice.asset.GraphServiceAsset;
import com.facebook.graphservice.factory.GraphQLServiceFactory;
import com.facebook.graphservice.interfaces.TreeJsonSerializer;
import com.facebook.graphservice.interfaces.TreeSerializer;
import java.io.File;
import java.io.IOException;

/* renamed from: X.0aR  reason: invalid class name and case insensitive filesystem */
public final class C05850aR {
    public static volatile GraphQLServiceFactory A00;
    public static volatile TreeJsonSerializer A01;
    private static volatile GraphServiceAsset A02;
    private static volatile GraphQLServiceFactory A03;
    private static volatile GraphQLServiceFactory A04;
    private static volatile TreeSerializer A05;

    public static GraphServiceAsset A00(Context context) {
        if (A02 == null) {
            synchronized (GraphServiceAsset.class) {
                if (A02 == null) {
                    try {
                        File file = new File(context.getDir("graphservice", 0), "graph_metadata.bin");
                        AnonymousClass07O r4 = new AnonymousClass07O();
                        r4.A02 = "GraphServiceUnpacker";
                        r4.A00 = context;
                        r4.A01 = file.getParentFile();
                        r4.A04.add(new AnonymousClass07T("graph_metadata.bin.checksum", "uncompressed_graph_metadata.bin.checksum"));
                        r4.A04.add(new AnonymousClass07U("graph_metadata.bin.xzs", "graph_metadata.bin"));
                        r4.A00().A04();
                        A02 = new GraphServiceAsset(file.getCanonicalPath());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return A02;
    }

    public static GraphQLServiceFactory A01() {
        if (A03 == null) {
            if (A02 == null) {
                Application A002 = AnonymousClass00e.A00();
                A002.getAssets();
                A00(A002.getApplicationContext());
            }
            synchronized (GraphQLServiceFactory.class) {
                if (A03 == null) {
                    A03 = new GraphQLServiceFactory(A02);
                }
            }
        }
        return A03;
    }

    public static GraphQLServiceFactory A02() {
        if (A04 == null) {
            A04 = A01();
        }
        return A04;
    }

    public static TreeSerializer A03() {
        if (A05 == null) {
            GraphQLServiceFactory A012 = A01();
            synchronized (TreeSerializer.class) {
                if (A05 == null) {
                    A05 = A012.newTreeSerializer();
                }
            }
        }
        return A05;
    }
}
