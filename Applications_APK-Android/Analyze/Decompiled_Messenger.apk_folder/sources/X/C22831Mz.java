package X;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Build;

/* renamed from: X.1Mz  reason: invalid class name and case insensitive filesystem */
public abstract class C22831Mz {
    private static void A02(int i, int i2) {
        boolean z = true;
        boolean z2 = false;
        if (i > 0) {
            z2 = true;
        }
        C05520Zg.A06(z2, "width must be > 0");
        if (i2 <= 0) {
            z = false;
        }
        C05520Zg.A06(z, "height must be > 0");
    }

    public AnonymousClass1PS A06(int i, int i2, Bitmap.Config config) {
        AnonymousClass1NY r4;
        if (!(this instanceof AnonymousClass1Ns)) {
            C37421ve r42 = (C37421ve) this;
            int i3 = i * i2;
            Bitmap bitmap = (Bitmap) r42.A01.get(i3 * AnonymousClass1SJ.A00(config));
            boolean z = false;
            if (bitmap.getAllocationByteCount() >= i3 * AnonymousClass1SJ.A00(config)) {
                z = true;
            }
            C05520Zg.A04(z);
            bitmap.reconfigure(i, i2, config);
            return r42.A00.A00(bitmap, r42.A01);
        }
        AnonymousClass1Ns r5 = (AnonymousClass1Ns) this;
        if (r5.A00) {
            return r5.A02.A00(Bitmap.createBitmap(i, i2, config), C196549Mk.A00());
        }
        AnonymousClass1PS A00 = r5.A01.A00((short) i, (short) i2);
        try {
            r4 = new AnonymousClass1NY(A00);
            r4.A07 = AnonymousClass1SI.A06;
            AnonymousClass1PS decodeJPEGFromEncodedImage = r5.A03.decodeJPEGFromEncodedImage(r4, config, null, ((AnonymousClass1SS) A00.A0A()).size());
            if (!((Bitmap) decodeJPEGFromEncodedImage.A0A()).isMutable()) {
                AnonymousClass1PS.A05(decodeJPEGFromEncodedImage);
                r5.A00 = true;
                AnonymousClass02w.A0C("HoneycombBitmapFactory", "Immutable bitmap returned by decoder");
                decodeJPEGFromEncodedImage = r5.A02.A00(Bitmap.createBitmap(i, i2, config), C196549Mk.A00());
            } else {
                ((Bitmap) decodeJPEGFromEncodedImage.A0A()).setHasAlpha(true);
                ((Bitmap) decodeJPEGFromEncodedImage.A0A()).eraseColor(0);
            }
            AnonymousClass1NY.A04(r4);
            A00.close();
            return decodeJPEGFromEncodedImage;
        } catch (Throwable th) {
            A00.close();
            throw th;
        }
    }

    public AnonymousClass1PS A07(Bitmap bitmap) {
        return A01(this, bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), null, false, null);
    }

    public AnonymousClass1PS A08(Bitmap bitmap, int i, int i2, int i3, int i4) {
        return A01(this, bitmap, i, i2, i3, i4, null, false, null);
    }

    public AnonymousClass1PS A09(Bitmap bitmap, int i, int i2, boolean z) {
        A02(i, i2);
        Matrix matrix = new Matrix();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        matrix.setScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
        return A01(this, bitmap, 0, 0, width, height, matrix, z, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0095, code lost:
        if (r13.hasAlpha() != false) goto L_0x0097;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1PS A01(X.C22831Mz r12, android.graphics.Bitmap r13, int r14, int r15, int r16, int r17, android.graphics.Matrix r18, boolean r19, java.lang.Object r20) {
        /*
            java.lang.String r0 = "Source bitmap cannot be null"
            X.C05520Zg.A03(r13, r0)
            r2 = 1
            r1 = 0
            if (r14 < 0) goto L_0x000a
            r1 = 1
        L_0x000a:
            java.lang.String r0 = "x must be >= 0"
            X.C05520Zg.A06(r1, r0)
            if (r15 >= 0) goto L_0x0012
            r2 = 0
        L_0x0012:
            java.lang.String r0 = "y must be >= 0"
            X.C05520Zg.A06(r2, r0)
            r2 = r17
            r3 = r16
            A02(r3, r2)
            int r6 = r14 + r16
            int r5 = r13.getWidth()
            r4 = 1
            r1 = 0
            if (r6 > r5) goto L_0x0029
            r1 = 1
        L_0x0029:
            java.lang.String r0 = "x + width must be <= bitmap.width()"
            X.C05520Zg.A06(r1, r0)
            int r1 = r15 + r17
            int r0 = r13.getHeight()
            if (r1 <= r0) goto L_0x0037
            r4 = 0
        L_0x0037:
            java.lang.String r0 = "y + height must be <= bitmap.height()"
            X.C05520Zg.A06(r4, r0)
            android.graphics.Rect r5 = new android.graphics.Rect
            r5.<init>(r14, r15, r6, r1)
            android.graphics.RectF r6 = new android.graphics.RectF
            float r4 = (float) r3
            float r1 = (float) r2
            r0 = 0
            r6.<init>(r0, r0, r4, r1)
            android.graphics.Bitmap$Config r11 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap$Config r0 = r13.getConfig()
            if (r0 == 0) goto L_0x0061
            int[] r1 = X.C55982p4.A00
            int r0 = r0.ordinal()
            r1 = r1[r0]
            r0 = 1
            if (r1 == r0) goto L_0x00f0
            r0 = 2
            if (r1 != r0) goto L_0x0061
            android.graphics.Bitmap$Config r11 = android.graphics.Bitmap.Config.ALPHA_8
        L_0x0061:
            r4 = 0
            r7 = r18
            if (r18 == 0) goto L_0x00d2
            boolean r0 = r7.isIdentity()
            if (r0 != 0) goto L_0x00d2
            boolean r10 = r7.rectStaysRect()
            r3 = 1
            r10 = r10 ^ r3
            android.graphics.RectF r8 = new android.graphics.RectF
            r8.<init>()
            r7.mapRect(r8, r6)
            float r0 = r8.width()
            int r9 = java.lang.Math.round(r0)
            float r0 = r8.height()
            int r2 = java.lang.Math.round(r0)
            if (r10 == 0) goto L_0x008e
            android.graphics.Bitmap$Config r11 = android.graphics.Bitmap.Config.ARGB_8888
        L_0x008e:
            if (r10 != 0) goto L_0x0097
            boolean r1 = r13.hasAlpha()
            r0 = 0
            if (r1 == 0) goto L_0x0098
        L_0x0097:
            r0 = 1
        L_0x0098:
            X.1PS r9 = r12.A00(r9, r2, r11, r0)
            java.lang.Object r0 = r9.A0A()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            A03(r13, r0)
            android.graphics.Canvas r2 = new android.graphics.Canvas
            java.lang.Object r0 = r9.A0A()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            r2.<init>(r0)
            float r0 = r8.left
            float r1 = -r0
            float r0 = r8.top
            float r0 = -r0
            r2.translate(r1, r0)
            r2.concat(r7)
            android.graphics.Paint r0 = new android.graphics.Paint
            r0.<init>()
            r1 = r19
            r0.setFilterBitmap(r1)
            if (r10 == 0) goto L_0x00cb
            r0.setAntiAlias(r3)
        L_0x00cb:
            r2.drawBitmap(r13, r5, r6, r0)
            r2.setBitmap(r4)
            return r9
        L_0x00d2:
            boolean r0 = r13.hasAlpha()
            X.1PS r9 = r12.A00(r3, r2, r11, r0)
            java.lang.Object r0 = r9.A0A()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            A03(r13, r0)
            android.graphics.Canvas r2 = new android.graphics.Canvas
            java.lang.Object r0 = r9.A0A()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            r2.<init>(r0)
            r0 = r4
            goto L_0x00cb
        L_0x00f0:
            android.graphics.Bitmap$Config r11 = android.graphics.Bitmap.Config.RGB_565
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22831Mz.A01(X.1Mz, android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean, java.lang.Object):X.1PS");
    }

    public AnonymousClass1PS A04(int i, int i2) {
        return A05(i, i2, Bitmap.Config.ARGB_8888);
    }

    private AnonymousClass1PS A00(int i, int i2, Bitmap.Config config, boolean z) {
        A02(i, i2);
        AnonymousClass1PS A06 = A06(i, i2, config);
        Bitmap bitmap = (Bitmap) A06.A0A();
        if (Build.VERSION.SDK_INT >= 12) {
            bitmap.setHasAlpha(z);
        }
        if (config == Bitmap.Config.ARGB_8888 && !z) {
            bitmap.eraseColor((int) C15320v6.MEASURED_STATE_MASK);
        }
        return A06;
    }

    private static void A03(Bitmap bitmap, Bitmap bitmap2) {
        bitmap2.setDensity(bitmap.getDensity());
        int i = Build.VERSION.SDK_INT;
        if (i >= 12) {
            bitmap2.setHasAlpha(bitmap.hasAlpha());
        }
        if (i >= 19) {
            bitmap2.setPremultiplied(bitmap.isPremultiplied());
        }
    }

    public AnonymousClass1PS A05(int i, int i2, Bitmap.Config config) {
        return A06(i, i2, config);
    }
}
