package X;

import io.card.payment.BuildConfig;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.00I  reason: invalid class name */
public final class AnonymousClass00I {
    public static final Method A00;
    private static final Method A01;
    private static final Method A02;
    public static volatile boolean A03 = true;

    public static Object A01(Method method, Object... objArr) {
        if (method != null) {
            try {
                return method.invoke(null, objArr);
            } catch (IllegalAccessException unused) {
                A03 = false;
                return null;
            } catch (InvocationTargetException e) {
                Throwable targetException = e.getTargetException();
                if (targetException instanceof RuntimeException) {
                    throw ((RuntimeException) targetException);
                } else if (targetException instanceof Error) {
                    throw ((Error) targetException);
                }
            }
        }
        return null;
    }

    public static long A00(String str, long j) {
        Long l;
        if (!A03 || (l = (Long) A01(A02, str, Long.valueOf(j))) == null) {
            return j;
        }
        return l.longValue();
    }

    public static String A02(String str) {
        String str2;
        if (!A03 || (str2 = (String) A01(A01, str)) == null) {
            return BuildConfig.FLAVOR;
        }
        return str2;
    }

    static {
        C000400i A002 = C000400i.A00();
        if (A002 != null) {
            A00 = A002.A00;
            A01 = A002.A01;
            A02 = A002.A02;
        }
    }
}
