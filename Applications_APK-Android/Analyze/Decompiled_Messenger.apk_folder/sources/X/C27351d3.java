package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1d3  reason: invalid class name and case insensitive filesystem */
public final class C27351d3 {
    public static Boolean A00;

    public static boolean A00(C27321d0 r1) {
        if (A00 == null) {
            r1.A04(BuildConfig.FLAVOR);
            try {
                A00 = true;
                r1.A03();
            } catch (Exception e) {
                throw e;
            } catch (Throwable th) {
                r1.A03();
                throw th;
            }
        }
        return A00.booleanValue();
    }
}
