package X;

/* renamed from: X.1Oq  reason: invalid class name and case insensitive filesystem */
public final class C23211Oq {
    public final C30781id A00;
    private final C23191Oo[] A01;

    public boolean A03() {
        if (this.A01 != null) {
            return true;
        }
        return false;
    }

    public C23211Oq(C30781id r1, C23191Oo[] r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static C23191Oo A00(C23211Oq r3, String str) {
        if (!r3.A03()) {
            return null;
        }
        int Ax8 = r3.A00.Ax8(str);
        if (Ax8 < 0 && str.equals("legacy_zero")) {
            Ax8 = 0;
        }
        if (Ax8 >= 0) {
            return r3.A01[Ax8];
        }
        r3.A00.C2s(str, "Requested param not found");
        return null;
    }

    public double A01(String str, double d) {
        C23191Oo A002 = A00(this, str);
        if (A002 == null) {
            return d;
        }
        try {
            return A002.A01();
        } catch (C37861wT e) {
            this.A00.C2s(str, e.getMessage());
            return d;
        }
    }

    public long A02(String str, long j) {
        C23191Oo A002 = A00(this, str);
        if (A002 == null) {
            return j;
        }
        try {
            return A002.A02();
        } catch (C37861wT e) {
            this.A00.C2s(str, e.getMessage());
            return j;
        }
    }

    public boolean A04(String str, boolean z) {
        C23191Oo A002 = A00(this, str);
        if (A002 == null) {
            return z;
        }
        try {
            if (A002.A00.equals("BOOL")) {
                return A002.A01;
            }
            throw new C37861wT("Invalid value type");
        } catch (C37861wT e) {
            this.A00.C2s(str, e.getMessage());
            return z;
        }
    }
}
