package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Yp  reason: invalid class name and case insensitive filesystem */
public final class C25171Yp {
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    private static volatile C25171Yp A03;
    public final FbSharedPreferences A00;

    static {
        AnonymousClass1Y7 r1 = C04350Ue.A03;
        A01 = (AnonymousClass1Y7) r1.A09("sso_stored_in_account_manager");
        A02 = (AnonymousClass1Y7) r1.A09("sso_version_stored_in_account_manager");
        r1.A09("username_stored_in_account_manager");
    }

    public static final C25171Yp A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C25171Yp.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r4.getApplicationInjector();
                        AnonymousClass0UU.A05(applicationInjector);
                        A03 = new C25171Yp(applicationInjector);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C25171Yp(AnonymousClass1XY r2) {
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }
}
