package X;

import android.os.Bundle;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchThreadListParams;
import com.facebook.messaging.service.model.FetchThreadListResult;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0pK  reason: invalid class name and case insensitive filesystem */
public final class C12430pK implements C11090lq {
    public final /* synthetic */ C10950l8 A00;
    public final /* synthetic */ FetchThreadListParams A01;

    public C12430pK(FetchThreadListParams fetchThreadListParams, C10950l8 r2) {
        this.A01 = fetchThreadListParams;
        this.A00 = r2;
    }

    public OperationResult BL3(Map map, ImmutableSet immutableSet) {
        Bundle bundle;
        String string;
        if (!map.isEmpty() || immutableSet.isEmpty()) {
            ArrayList arrayList = new ArrayList(map.size());
            ArrayList arrayList2 = new ArrayList(map.size());
            HashMap hashMap = new HashMap();
            long j = Long.MIN_VALUE;
            for (OperationResult A07 : map.values()) {
                FetchThreadListResult fetchThreadListResult = (FetchThreadListResult) A07.A07();
                arrayList.add(fetchThreadListResult.A06);
                arrayList2.add(fetchThreadListResult.A02);
                C11070lo.A06(hashMap, fetchThreadListResult.A09);
                j = Math.max(j, fetchThreadListResult.A00);
            }
            if (this.A01.A04 != C10700ki.SMS && !map.containsKey(C11120lv.FACEBOOK)) {
                arrayList.add(new ThreadsCollection(RegularImmutableList.A02, false));
            }
            AnonymousClass161 r3 = new AnonymousClass161();
            r3.A02 = DataFetchDisposition.A00(arrayList2);
            r3.A04 = this.A00;
            r3.A06 = AnonymousClass170.A00(arrayList);
            r3.A09 = ImmutableList.copyOf(hashMap.values());
            r3.A00 = j;
            OperationResult A04 = OperationResult.A04(new FetchThreadListResult(r3));
            OperationResult operationResult = (OperationResult) map.get(C11120lv.FACEBOOK);
            if (!(operationResult == null || (bundle = operationResult.resultDataBundle) == null || (string = bundle.getString("source")) == null)) {
                A04.resultDataBundle.putString("source", string);
            }
            return A04;
        }
        throw new C97214kN(immutableSet);
    }
}
