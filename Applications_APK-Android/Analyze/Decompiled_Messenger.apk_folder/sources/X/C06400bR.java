package X;

import android.content.Context;
import android.os.HandlerThread;

/* renamed from: X.0bR  reason: invalid class name and case insensitive filesystem */
public final class C06400bR {
    public AnonymousClass15B A00;
    public AnonymousClass15B A01;
    public AnonymousClass966 A02;
    private C29671gn A03;
    public final Context A04;
    public final C1936795w A05;
    public final C07250d5 A06;
    public final C25761aM A07;
    public final C07290dB A08;
    public final C07290dB A09;
    public final C06320bJ A0A;
    public final C06840cA A0B;
    public final C07300dC A0C;
    public final C07300dC A0D;
    public final C07330dF A0E;
    public final C06390bQ A0F;
    public final Class A0G;
    public final Class A0H;
    public final Class A0I;
    public final Class A0J;
    public final Class A0K;
    private final C1936795w A0L;
    private final C06320bJ A0M;
    private final C06260bD A0N;

    public static synchronized C29671gn A01(C06400bR r3) {
        C29671gn r0;
        synchronized (r3) {
            if (r3.A03 == null) {
                r3.A03 = new C29671gn(r3.A0N, r3.A04);
            }
            r0 = r3.A03;
        }
        return r0;
    }

    public static HandlerThread A00(C06400bR r2, String str, int i) {
        return AnonymousClass15C.A00(r2.A04).A03(r2.A0H.getName()).AUr(str, i);
    }

    public synchronized AnonymousClass15B A02() {
        if (this.A00 == null) {
            AnonymousClass15A r5 = new AnonymousClass15A(this.A0K, this.A0G, this.A0I, this.A0H, this.A0J, AnonymousClass07B.A01, "ads");
            HandlerThread A002 = A00(this, "Analytics-HighPri-Proc", 0);
            Integer num = AnonymousClass07B.A00;
            C07290dB r3 = this.A08;
            Context context = this.A04;
            C06320bJ r10 = this.A0M;
            AnonymousClass15B r16 = new AnonymousClass15B(A002, num, r3, new AnonymousClass15I(context, 2131298545, "high", r10, new C06410bS(this.A07, this.A0L), this.A0F, r5, this.A0E, this.A0H, this.A0C), this, this.A06, r10);
            this.A00 = r16;
            C29671gn A012 = A01(this);
            AnonymousClass15K r2 = r16.A04;
            AnonymousClass15K.A03(r2);
            r2.sendMessage(r2.obtainMessage(2, A012));
        }
        return this.A00;
    }

    public synchronized AnonymousClass15B A03() {
        if (this.A01 == null) {
            AnonymousClass15A r5 = new AnonymousClass15A(this.A0K, this.A0G, this.A0I, this.A0H, this.A0J, AnonymousClass07B.A00, "regular");
            HandlerThread A002 = A00(this, "Analytics-NormalPri-Proc", 10);
            Integer num = AnonymousClass07B.A01;
            C07290dB r3 = this.A09;
            Context context = this.A04;
            C06320bJ r10 = this.A0A;
            AnonymousClass15B r16 = new AnonymousClass15B(A002, num, r3, new AnonymousClass15I(context, 2131298547, "normal", r10, new C06410bS(this.A07, this.A05), this.A0F, r5, this.A0E, this.A0H, this.A0D), this, this.A06, r10);
            this.A01 = r16;
            C29671gn A012 = A01(this);
            AnonymousClass15K r2 = r16.A04;
            AnonymousClass15K.A03(r2);
            r2.sendMessage(r2.obtainMessage(2, A012));
        }
        return this.A01;
    }

    public C06400bR(Context context, Class cls, C07290dB r4, C07290dB r5, Class cls2, Class cls3, Class cls4, C25761aM r9, C1936795w r10, C1936795w r11, C06260bD r12, C06390bQ r13, C07330dF r14, C07300dC r15, C07300dC r16, C06320bJ r17, C06320bJ r18, C06840cA r19, C07250d5 r20, Class cls5) {
        this.A04 = context;
        this.A0K = cls;
        this.A09 = r4;
        this.A08 = r5;
        this.A0G = cls2;
        this.A0I = cls3;
        this.A0H = cls4;
        this.A07 = r9;
        this.A0L = r10;
        this.A05 = r11;
        this.A0N = r12;
        this.A0F = r13;
        this.A0E = r14;
        this.A0D = r15;
        this.A0C = r16;
        this.A0A = r17;
        this.A0M = r18;
        this.A0B = r19;
        this.A06 = r20;
        this.A0J = cls5;
    }
}
