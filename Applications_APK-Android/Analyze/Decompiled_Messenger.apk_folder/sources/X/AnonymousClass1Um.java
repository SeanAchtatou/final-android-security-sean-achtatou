package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Um  reason: invalid class name */
public final class AnonymousClass1Um {
    private static volatile AnonymousClass1Um A00;

    public static final AnonymousClass1Um A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass1Um.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass1Um();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
