package X;

import android.content.Context;
import com.facebook.common.callercontext.CallerContext;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1vg  reason: invalid class name and case insensitive filesystem */
public final class C37441vg extends C20831Dz {
    public static final CallerContext A04 = CallerContext.A09("HubLandingSectionAdapter");
    public Context A00;
    public AnonymousClass0UN A01;
    public C23845Bo1 A02;
    public ImmutableList A03;

    public int ArU() {
        ImmutableList immutableList = this.A03;
        if (immutableList == null) {
            return 0;
        }
        return immutableList.size();
    }

    public C37441vg(Context context) {
        this.A00 = context;
        this.A01 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
