package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0Kn  reason: invalid class name and case insensitive filesystem */
public final class C03220Kn implements FilenameFilter {
    public boolean accept(File file, String str) {
        return str.startsWith("metrics_");
    }
}
