package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.129  reason: invalid class name */
public final class AnonymousClass129 implements C05700aB {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) A0Y.A09("has_clicked_add_to_montage_editor_button"));
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F = ((AnonymousClass1Y7) A0Y.A09("montage_num_times_send_tooltip_shown"));
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y7 A0H;
    public static final AnonymousClass1Y7 A0I;
    public static final AnonymousClass1Y7 A0J;
    public static final AnonymousClass1Y7 A0K;
    public static final AnonymousClass1Y7 A0L;
    public static final AnonymousClass1Y7 A0M;
    public static final AnonymousClass1Y8 A0N;
    public static final AnonymousClass1Y8 A0O;
    public static final AnonymousClass1Y8 A0P;
    public static final AnonymousClass1Y8 A0Q = A0Z.A09("has_performed_my_montage_fbid_fetch");
    public static final AnonymousClass1Y8 A0R;
    public static final AnonymousClass1Y8 A0S;
    public static final AnonymousClass1Y8 A0T;
    public static final AnonymousClass1Y8 A0U = A0Z.A09("montage_forwarded_message_ids");
    public static final AnonymousClass1Y8 A0V;
    public static final AnonymousClass1Y8 A0W;
    public static final AnonymousClass1Y8 A0X = A0Z.A09("montage_num_times_pre_select_day_tooltip_shown");
    private static final AnonymousClass1Y7 A0Y = ((AnonymousClass1Y7) C05690aA.A0m.A09("montage/"));
    private static final AnonymousClass1Y8 A0Z;

    static {
        AnonymousClass1Y8 A0D2 = C05690aA.A1i.A09("montage/");
        A0Z = A0D2;
        A0W = A0D2.A09("montage_hidden_user_last_server_update_time");
        AnonymousClass1Y8 r1 = A0Z;
        A0V = r1.A09("montage_hidden_user_ids");
        A0R = r1.A09("montage_converted_message_ids");
        A0Y.A09("montage_direct_contextual_tombstones");
        AnonymousClass1Y7 r12 = A0Y;
        A03 = (AnonymousClass1Y7) r12.A09("has_viewed_montage_stories_merge_nux");
        r12.A09("messenger_home_camera_capture_tooltip/");
        AnonymousClass1Y8 r13 = A0Z;
        A0P = r13.A09("entry_point_nux_tooltip/");
        A0N = r13.A09("add_to_montage_message_upsell_card_as_nux/");
        A0O = r13.A09("add_to_montage_message_upsell_click_count");
        r13.A09("effect_picker_badge_count");
        AnonymousClass1Y7 r14 = A0Y;
        A0C = (AnonymousClass1Y7) r14.A09("montage_num_add_to_montage_editor_posts");
        A0D = (AnonymousClass1Y7) r14.A09("montage_num_times_add_to_montage_editor_tooltip_shown");
        AnonymousClass1Y8 r15 = A0Z;
        A0T = r15.A09("montage_featured_art_order_token");
        A0S = r15.A09("montage_featured_art_content_hash");
        AnonymousClass1Y7 r16 = A0Y;
        A00 = (AnonymousClass1Y7) r16.A09("montage_cached_msqrd_compression_capabilies_key");
        A01 = (AnonymousClass1Y7) r16.A09("montage_cached_msqrd_compression_capabilies_value");
        A07 = (AnonymousClass1Y7) r16.A09("montage_effects_last_clicked_time_map");
        r16.A09("montage_tooltip_last_seen_time");
        A0J = (AnonymousClass1Y7) r16.A09("montage_public_audience_mode_enabled");
        r16.A09("montage_omnistore_optimistic_thread_read/");
        A0I = (AnonymousClass1Y7) r16.A09("montage_promotion_new_story_resource_list");
        A0H = (AnonymousClass1Y7) r16.A09("montage_promotion_new_story_fetch_timestamp");
        r16.A09("montage_num_add_to_montage_only_posts");
        A0G = (AnonymousClass1Y7) r16.A09("montage_nux_hidden_state");
        A05 = (AnonymousClass1Y7) r16.A09("montage_canvas_font_last_used");
        A04 = (AnonymousClass1Y7) r16.A09("montage_canvas_color_last_used");
        A0L = (AnonymousClass1Y7) r16.A09("montage_text_font_last_used");
        A09 = (AnonymousClass1Y7) r16.A09("montage_last_used_surface_pref_key");
        A0E = (AnonymousClass1Y7) r16.A09("montage_num_times_m4_inbox_add_to_montage_tooltip_shown");
        A0A = (AnonymousClass1Y7) r16.A09("montage_m4_inbox_add_to_montage_tooltip_last_seen_time");
        A0B = (AnonymousClass1Y7) r16.A09("montage_m4_inbox_montage_last_viewed_time");
        r16.A09("montage_m4_contacts_tab_last_viewed");
        A0M = (AnonymousClass1Y7) r16.A09("montage_viewer_session_id_creation_time");
        A08 = (AnonymousClass1Y7) r16.A09("montage_inbox_reaction_last_seen_time");
        A06 = (AnonymousClass1Y7) r16.A09("montage_did_play_introductory_lwr_anim");
        A0K = (AnonymousClass1Y7) r16.A09("montage_self_story_latest_seen_by_id");
    }

    public static final AnonymousClass129 A00() {
        return new AnonymousClass129();
    }

    public ImmutableSet AzO() {
        return ImmutableSet.A09(A0W, A0V, A0R, A0U, A0P, A0O, A0N, A0Q, A0X, A0T, A0S);
    }
}
