package X;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: X.1w6  reason: invalid class name and case insensitive filesystem */
public class C37701w6 extends C37571vt {
    private static final long serialVersionUID = 1;
    public LinkedList _path;

    public static C37701w6 from(C28271eX r2, String str) {
        C64443Bu tokenLocation;
        if (r2 == null) {
            tokenLocation = null;
        } else {
            tokenLocation = r2.getTokenLocation();
        }
        return new C37701w6(str, tokenLocation);
    }

    public static C37701w6 fromUnexpectedIOE(IOException iOException) {
        return new C37701w6(AnonymousClass08S.A0S("Unexpected IOException (of type ", iOException.getClass().getName(), "): ", iOException.getMessage()), null, iOException);
    }

    public static C37701w6 wrapWithPath(Throwable th, C181018ap r4) {
        C37701w6 r3;
        if (th instanceof C37701w6) {
            r3 = (C37701w6) th;
        } else {
            String message = th.getMessage();
            if (message == null || message.length() == 0) {
                message = AnonymousClass08S.A0P("(was ", th.getClass().getName(), ")");
            }
            r3 = new C37701w6(message, null, th);
        }
        r3.prependPath(r4);
        return r3;
    }

    public void prependPath(C181018ap r3) {
        if (this._path == null) {
            this._path = new LinkedList();
        }
        if (this._path.size() < 1000) {
            this._path.addFirst(r3);
        }
    }

    private String _buildMessage() {
        StringBuilder sb;
        String message = super.getMessage();
        LinkedList linkedList = this._path;
        if (linkedList == null) {
            return message;
        }
        if (message == null) {
            sb = new StringBuilder();
        } else {
            sb = new StringBuilder(message);
        }
        sb.append(" (through reference chain: ");
        if (linkedList != null) {
            Iterator it = linkedList.iterator();
            while (it.hasNext()) {
                sb.append(((C181018ap) it.next()).toString());
                if (it.hasNext()) {
                    sb.append("->");
                }
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public String getLocalizedMessage() {
        return _buildMessage();
    }

    public String getMessage() {
        return _buildMessage();
    }

    public String toString() {
        return AnonymousClass08S.A0P(getClass().getName(), ": ", getMessage());
    }

    public C37701w6(String str) {
        super(str);
    }

    public C37701w6(String str, C64443Bu r3) {
        super(str, r3, null);
    }

    public C37701w6(String str, C64443Bu r2, Throwable th) {
        super(str, r2, th);
    }

    public C37701w6(String str, Throwable th) {
        super(str, null, th);
    }
}
