package X;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Ba  reason: invalid class name and case insensitive filesystem */
public final class C01640Ba implements C01730Bk {
    public final /* synthetic */ AnonymousClass0AD A00;

    public C01640Ba(AnonymousClass0AD r1) {
        this.A00 = r1;
    }

    public Map B5A() {
        String valueOf;
        String valueOf2;
        AnonymousClass0AD r6 = this.A00;
        HashMap hashMap = new HashMap();
        hashMap.put("is_mqtt_direct", "false");
        long j = r6.A09.A01;
        if (j > 0) {
            valueOf = new Date(j).toString();
        } else {
            valueOf = String.valueOf(j);
        }
        hashMap.put("last_connection_time", valueOf);
        long j2 = r6.A09.A03;
        if (j2 > 0) {
            valueOf2 = new Date(j2).toString();
        } else {
            valueOf2 = String.valueOf(j2);
        }
        hashMap.put("last_network_changed_time", valueOf2);
        hashMap.put("subscribed_topics", r6.A09.A0C().toString());
        hashMap.put("mqtt_health_stats", AnonymousClass0AD.A00(r6));
        return hashMap;
    }
}
