package X;

import com.facebook.messaging.model.messages.ParticipantInfo;
import java.util.Comparator;

/* renamed from: X.1fP  reason: invalid class name and case insensitive filesystem */
public final class C28811fP implements Comparator {
    public int compare(Object obj, Object obj2) {
        ParticipantInfo participantInfo = (ParticipantInfo) obj;
        ParticipantInfo participantInfo2 = (ParticipantInfo) obj2;
        C181810n r2 = ParticipantInfo.A07;
        int compare = r2.compare(participantInfo.A03, participantInfo2.A03);
        if (compare != 0) {
            return compare;
        }
        return r2.compare(participantInfo.A01.id, participantInfo2.A01.id);
    }
}
