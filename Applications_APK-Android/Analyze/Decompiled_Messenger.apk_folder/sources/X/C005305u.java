package X;

/* renamed from: X.05u  reason: invalid class name and case insensitive filesystem */
public final class C005305u extends ThreadLocal {
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x002e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object initialValue() {
        /*
            r5 = this;
            android.os.StrictMode$ThreadPolicy r4 = android.os.StrictMode.allowThreadDiskReads()
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x002f }
            java.lang.String r0 = "/dev/urandom"
            r3.<init>(r0)     // Catch:{ IOException -> 0x002f }
            r0 = 8
            java.nio.ByteBuffer r1 = java.nio.ByteBuffer.allocate(r0)     // Catch:{ all -> 0x0028 }
            byte[] r0 = r1.array()     // Catch:{ all -> 0x0028 }
            r3.read(r0)     // Catch:{ all -> 0x0028 }
            java.util.Random r2 = new java.util.Random     // Catch:{ all -> 0x0028 }
            long r0 = r1.getLong()     // Catch:{ all -> 0x0028 }
            r2.<init>(r0)     // Catch:{ all -> 0x0028 }
            r3.close()     // Catch:{ IOException -> 0x002f }
            android.os.StrictMode.setThreadPolicy(r4)
            return r2
        L_0x0028:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x002e }
        L_0x002e:
            throw r0     // Catch:{ IOException -> 0x002f }
        L_0x002f:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0038 }
            java.lang.String r0 = "Cannot read from /dev/urandom"
            r1.<init>(r0, r2)     // Catch:{ all -> 0x0038 }
            throw r1     // Catch:{ all -> 0x0038 }
        L_0x0038:
            r0 = move-exception
            android.os.StrictMode.setThreadPolicy(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C005305u.initialValue():java.lang.Object");
    }
}
