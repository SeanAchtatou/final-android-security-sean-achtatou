package X;

/* renamed from: X.0N6  reason: invalid class name */
public final class AnonymousClass0N6 {
    public static double[] A00 = {10.0d, 100.0d, 10000.0d, 1.0E8d, 1.0E16d, 1.0E32d, 1.0E64d, 1.0E128d, 1.0E256d};
    public static final char[] A01 = {' ', 9, 10, 11, 12, 13};

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0066, code lost:
        if (r12 > '9') goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00a5, code lost:
        if (r12 > 'Z') goto L_0x00a7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long A00(byte[] r17, int r18, int r19) {
        /*
            r9 = r18
            r10 = r17
            int r8 = r10.length
            r5 = 0
            if (r9 < r8) goto L_0x000a
        L_0x0009:
            return r5
        L_0x000a:
            byte r11 = r17[r9]
            r4 = 1
            int r9 = r9 + r4
            if (r9 >= r8) goto L_0x001e
            char r3 = (char) r11
            r2 = 0
        L_0x0012:
            char[] r1 = X.AnonymousClass0N6.A01
            int r0 = r1.length
            if (r2 >= r0) goto L_0x00c4
            char r0 = r1[r2]
            if (r0 != r3) goto L_0x00c0
            r0 = 1
        L_0x001c:
            if (r0 != 0) goto L_0x000a
        L_0x001e:
            r0 = 45
            if (r11 != r0) goto L_0x00b4
            byte r11 = r17[r9]
            int r9 = r9 + 1
            r18 = 1
        L_0x0028:
            r7 = 16
            r2 = 48
            r3 = r19
            if (r19 == 0) goto L_0x0032
            if (r3 != r7) goto L_0x00b2
        L_0x0032:
            if (r11 != r2) goto L_0x00b2
            if (r9 >= r8) goto L_0x00b2
            byte r1 = r17[r9]
            r0 = 120(0x78, float:1.68E-43)
            if (r1 == r0) goto L_0x0040
            r0 = 88
            if (r1 != r0) goto L_0x00b2
        L_0x0040:
            byte r11 = r17[r4]
            int r9 = r9 + 2
        L_0x0044:
            if (r9 >= r8) goto L_0x0009
            if (r7 != 0) goto L_0x004e
            r7 = 10
            if (r11 != r2) goto L_0x004e
            r7 = 8
        L_0x004e:
            long r4 = (long) r7
            r16 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            long r0 = r16 % r4
            int r6 = (int) r0
            long r14 = r16 / r4
            r2 = 0
            r13 = 0
        L_0x005c:
            if (r9 > r8) goto L_0x00c7
            char r12 = (char) r11
            r0 = 48
            if (r0 > r12) goto L_0x0068
            r1 = 57
            r0 = 1
            if (r12 <= r1) goto L_0x0069
        L_0x0068:
            r0 = 0
        L_0x0069:
            if (r0 == 0) goto L_0x008b
            int r11 = r11 + -48
        L_0x006d:
            byte r1 = (byte) r11
            if (r1 >= r7) goto L_0x00c7
            if (r13 < 0) goto L_0x007c
            int r0 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1))
            if (r0 > 0) goto L_0x007c
            int r0 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1))
            if (r0 != 0) goto L_0x0086
            if (r1 <= r6) goto L_0x0086
        L_0x007c:
            r13 = -1
        L_0x007d:
            if (r9 >= r8) goto L_0x0084
            byte r11 = r10[r9]
        L_0x0081:
            int r9 = r9 + 1
            goto L_0x005c
        L_0x0084:
            r11 = 0
            goto L_0x0081
        L_0x0086:
            long r2 = r2 * r4
            long r0 = (long) r1
            long r2 = r2 + r0
            r13 = 1
            goto L_0x007d
        L_0x008b:
            r0 = 97
            if (r0 > r12) goto L_0x0093
            r0 = 122(0x7a, float:1.71E-43)
            if (r12 <= r0) goto L_0x009b
        L_0x0093:
            r0 = 65
            if (r0 > r12) goto L_0x00b0
            r0 = 90
            if (r12 > r0) goto L_0x00b0
        L_0x009b:
            r0 = 1
        L_0x009c:
            if (r0 == 0) goto L_0x00c7
            r0 = 65
            if (r0 > r12) goto L_0x00a7
            r0 = 90
            r1 = 1
            if (r12 <= r0) goto L_0x00a8
        L_0x00a7:
            r1 = 0
        L_0x00a8:
            r0 = 87
            if (r1 == 0) goto L_0x00ae
            r0 = 55
        L_0x00ae:
            int r11 = r11 - r0
            goto L_0x006d
        L_0x00b0:
            r0 = 0
            goto L_0x009c
        L_0x00b2:
            r7 = r3
            goto L_0x0044
        L_0x00b4:
            r0 = 43
            if (r11 != r0) goto L_0x00bc
            byte r11 = r17[r9]
            int r9 = r9 + 1
        L_0x00bc:
            r18 = 0
            goto L_0x0028
        L_0x00c0:
            int r2 = r2 + 1
            goto L_0x0012
        L_0x00c4:
            r0 = 0
            goto L_0x001c
        L_0x00c7:
            if (r13 >= 0) goto L_0x00ce
            if (r18 == 0) goto L_0x00cd
            r16 = -9223372036854775808
        L_0x00cd:
            return r16
        L_0x00ce:
            if (r18 == 0) goto L_0x00d2
            long r2 = -r2
            return r2
        L_0x00d2:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0N6.A00(byte[], int, int):long");
    }
}
