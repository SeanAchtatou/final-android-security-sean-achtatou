package X;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.1m5  reason: invalid class name and case insensitive filesystem */
public final class C32641m5 implements C17040yE {
    private int A00 = 0;
    private boolean A01 = false;
    private boolean A02 = false;
    private boolean A03 = false;
    public final Set A04 = new CopyOnWriteArraySet();

    public void A00(Runnable runnable) {
        int i;
        int i2 = this.A00;
        if (i2 == 0) {
            this.A03 = false;
            this.A01 = false;
            this.A02 = false;
        }
        this.A00 = i2 + 1;
        try {
            runnable.run();
            if (i != 0) {
                return;
            }
        } finally {
            i = this.A00 - 1;
            this.A00 = i;
            if (i == 0) {
                if (this.A02) {
                    this.A02 = false;
                    Bhu();
                }
                if (this.A01) {
                    this.A01 = false;
                    Bht();
                }
                if (this.A03) {
                    this.A03 = false;
                    Bhv();
                }
            }
        }
    }

    public void Bht() {
        if (this.A00 > 0) {
            this.A01 = true;
            return;
        }
        for (C17040yE Bht : this.A04) {
            Bht.Bht();
        }
    }

    public void Bhu() {
        if (this.A00 > 0) {
            this.A02 = true;
            return;
        }
        for (C17040yE Bhu : this.A04) {
            Bhu.Bhu();
        }
    }

    public void Bhv() {
        if (this.A00 > 0) {
            this.A03 = true;
            return;
        }
        for (C17040yE Bhv : this.A04) {
            Bhv.Bhv();
        }
    }
}
