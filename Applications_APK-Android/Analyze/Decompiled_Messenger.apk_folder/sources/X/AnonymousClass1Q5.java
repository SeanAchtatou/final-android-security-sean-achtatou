package X;

/* renamed from: X.1Q5  reason: invalid class name */
public final class AnonymousClass1Q5 implements AnonymousClass1Q3 {
    public final C30911iq A00;
    public final C30911iq A01;
    public final C22601Mc A02;
    public final AnonymousClass1Q3 A03;

    public void ByS(C23581Qb r8, AnonymousClass1QK r9) {
        C33261nI r2 = r8;
        AnonymousClass1QK r3 = r9;
        if (r9.A08.mValue >= C23531Pw.DISK_CACHE.mValue) {
            r8.Bgg(null, 1);
            return;
        }
        if (r9.A09.A0E) {
            r2 = new C33261nI(r2, r3, this.A00, this.A01, this.A02);
        }
        this.A03.ByS(r2, r9);
    }

    public AnonymousClass1Q5(C30911iq r1, C30911iq r2, C22601Mc r3, AnonymousClass1Q3 r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }
}
