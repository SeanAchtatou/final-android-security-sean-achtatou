package X;

/* renamed from: X.1x5  reason: invalid class name and case insensitive filesystem */
public final class C38221x5 implements Runnable {
    public static final String __redex_internal_original_name = "com.google.android.exoplayer2.source.MediaSourceEventListener$EventDispatcher$4";
    public final /* synthetic */ C21439AEg A00;
    public final /* synthetic */ C21422ADo A01;
    public final /* synthetic */ C38031wm A02;
    public final /* synthetic */ C38211x4 A03;
    public final /* synthetic */ Object A04;

    public C38221x5(C21439AEg aEg, C38211x4 r2, C21422ADo aDo, C38031wm r4, Object obj) {
        this.A00 = aEg;
        this.A03 = r2;
        this.A01 = aDo;
        this.A02 = r4;
        this.A04 = obj;
    }

    public void run() {
        C38211x4 r1 = this.A03;
        C21439AEg aEg = this.A00;
        r1.Bcs(aEg.A00, aEg.A01, this.A01, this.A02, this.A04);
    }
}
