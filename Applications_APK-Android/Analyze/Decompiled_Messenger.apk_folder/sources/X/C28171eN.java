package X;

import com.facebook.common.perftest.base.PerfTestConfigBase;
import java.util.Map;

/* renamed from: X.1eN  reason: invalid class name and case insensitive filesystem */
public final class C28171eN {
    public AnonymousClass0UN A00;
    public final C28181eO A01;

    public int A02(AnonymousClass9M6 r11, String str, boolean z, Map map) {
        String str2;
        String str3 = str;
        int A002 = A00(this, r11, str);
        if (A002 != -1 && z && !this.A01.A00(r11).A01()) {
            AnonymousClass4SR r4 = (AnonymousClass4SR) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AyN, this.A00);
            String str4 = r11.name;
            if (A002 < 0 || A002 >= r11.groupCount) {
                str2 = "not_in_experiment";
            } else {
                str2 = r11.groupNames[A002];
            }
            AnonymousClass4SR.A01(r4, str4, str3, str2, map, false);
        }
        return A002;
    }

    public static final C28171eN A01(AnonymousClass1XY r1) {
        return new C28171eN(r1);
    }

    public C28171eN(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = new C28181eO(r3);
    }

    public static int A00(C28171eN r2, AnonymousClass9M6 r3, String str) {
        if (PerfTestConfigBase.A00() || C006006f.A01()) {
            return -1;
        }
        AnonymousClass9M8 A002 = r2.A01.A00(r3);
        if (!AnonymousClass9M8.A00(A002) || C06850cB.A0B(str)) {
            return -1;
        }
        int AqN = A002.A05.AqN(C93864dV.A00(A002.A07, str), -1);
        if (AqN < 0) {
            AqN = AnonymousClass559.A00(str, A002.A07);
            C30281hn edit = A002.A05.edit();
            edit.Bz6(C93864dV.A00(A002.A07, str), AqN);
            edit.commit();
        }
        int i = A002.A01;
        int i2 = A002.A00;
        int floor = (int) Math.floor((double) (AqN / i));
        if (floor < i2) {
            return floor;
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A03(X.AnonymousClass9M6 r8, boolean r9) {
        /*
            r7 = this;
            boolean r0 = com.facebook.common.perftest.base.PerfTestConfigBase.A00()
            if (r0 != 0) goto L_0x00b1
            boolean r0 = X.C006006f.A01()
            if (r0 != 0) goto L_0x00b1
            X.1eO r0 = r7.A01
            X.9M8 r3 = r0.A00(r8)
            boolean r0 = r3.A01()
            if (r0 == 0) goto L_0x0075
            int r0 = r3.A00
            int r1 = r0 + -1
        L_0x001c:
            r0 = -1
            if (r1 == r0) goto L_0x0071
            if (r9 == 0) goto L_0x0071
            X.1eO r0 = r7.A01
            X.9M8 r0 = r0.A00(r8)
            boolean r0 = r0.A01()
            if (r0 != 0) goto L_0x0071
            r3 = 0
            int r2 = X.AnonymousClass1Y3.AyN
            X.0UN r0 = r7.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.4SR r6 = (X.AnonymousClass4SR) r6
            java.lang.String r4 = r8.name
            if (r1 < 0) goto L_0x0072
            int r0 = r8.groupCount
            if (r1 >= r0) goto L_0x0072
            java.lang.String[] r0 = r8.groupNames
            r5 = r0[r1]
        L_0x0044:
            r3 = 1
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r0 = r6.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.1ZE r3 = (X.AnonymousClass1ZE) r3
            X.0Xc r2 = X.C04970Xc.A02
            java.lang.String r0 = "initial_app_launch_experiment_exposure"
            X.0bW r3 = r3.A02(r0, r2)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 289(0x121, float:4.05E-43)
            r2.<init>(r3, r0)
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = "exp_name"
            r2.A0D(r0, r4)
            java.lang.String r0 = "exp_group"
            r2.A0D(r0, r5)
            r2.A06()
        L_0x0071:
            return r1
        L_0x0072:
            java.lang.String r5 = "not_in_experiment"
            goto L_0x0044
        L_0x0075:
            boolean r0 = X.AnonymousClass9M8.A00(r3)
            r2 = -1
            if (r0 == 0) goto L_0x00b1
            com.facebook.prefs.shared.FbSharedPreferences r1 = r3.A05
            X.1Y7 r0 = r3.A06
            int r2 = r1.AqN(r0, r2)
            if (r2 >= 0) goto L_0x00a0
            X.1ac r0 = r3.A03
            java.lang.String r1 = r0.B7Z()
            java.lang.String r0 = r3.A07
            int r2 = X.AnonymousClass559.A00(r1, r0)
            com.facebook.prefs.shared.FbSharedPreferences r0 = r3.A05
            X.1hn r1 = r0.edit()
            X.1Y7 r0 = r3.A06
            r1.Bz6(r0, r2)
            r1.commit()
        L_0x00a0:
            int r0 = r3.A01
            int r3 = r3.A00
            int r2 = r2 / r0
            double r0 = (double) r2
            double r1 = java.lang.Math.floor(r0)
            int r0 = (int) r1
            r1 = -1
            if (r0 >= r3) goto L_0x001c
            r1 = r0
            goto L_0x001c
        L_0x00b1:
            r1 = -1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28171eN.A03(X.9M6, boolean):int");
    }
}
