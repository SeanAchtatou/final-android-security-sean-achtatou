package X;

import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.15b  reason: invalid class name and case insensitive filesystem */
public final class C187815b {
    public final /* synthetic */ ThreadListFragment A00;

    public C187815b(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public void A00() {
        C196118y r0 = this.A00.A0w;
        if (r0 != null) {
            r0.A02.A0C(false);
        }
        ((AnonymousClass1CV) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AdF, this.A00.A0O)).A0E(true, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0091, code lost:
        if (r2 == X.AnonymousClass0u3.A04) goto L_0x0093;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.C32971md r9, X.C13890sF r10) {
        /*
            r8 = this;
            com.facebook.orca.threadlist.ThreadListFragment r3 = r8.A00
            com.facebook.orca.threadlist.ThreadListFragment.A0D(r3)
            X.1r8 r2 = r9.A02
            X.1r8 r0 = X.C35381r8.MORE_THREADS
            r1 = 0
            if (r2 != r0) goto L_0x0011
            com.facebook.widget.recyclerview.BetterRecyclerView r0 = r3.A1F
            r0.A0q(r1, r1)
        L_0x0011:
            r3.A1U = r1
            com.facebook.fbservice.results.DataFetchDisposition r2 = r10.A01
            X.1r8 r0 = r9.A02
            java.lang.String r4 = r0.toString()
            X.144 r1 = r3.A0B
            android.content.Context r0 = r3.A1j()
            java.lang.String r5 = r1.A01(r0)
            boolean r0 = r2.A08
            if (r0 != 0) goto L_0x0084
            r1 = 1
        L_0x002a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            android.content.Context r0 = r3.A1j()
            boolean r0 = r0 instanceof X.C11510nI
            if (r0 == 0) goto L_0x0053
            int r0 = r2.length()
            if (r0 <= 0) goto L_0x0042
            java.lang.String r0 = "_"
            r2.append(r0)
        L_0x0042:
            android.content.Context r0 = r3.A1j()
            X.0nI r0 = (X.C11510nI) r0
            java.lang.String r0 = r0.Act()
            if (r0 != 0) goto L_0x0050
            java.lang.String r0 = "unknown"
        L_0x0050:
            r2.append(r0)
        L_0x0053:
            java.lang.String r7 = r2.toString()
            X.0nb r2 = new X.0nb
            java.lang.String r0 = "content"
            r2.<init>(r0)
            java.lang.String r0 = "flags"
            r2.A09(r0, r1)
            r0 = -1
            r2.A02 = r0
            if (r7 == 0) goto L_0x006e
            java.lang.String r0 = "pigeon_reserved_keyword_module"
            r2.A0D(r0, r7)
        L_0x006e:
            if (r5 == 0) goto L_0x0075
            java.lang.String r0 = "pigeon_reserved_keyword_uuid"
            r2.A0D(r0, r5)
        L_0x0075:
            r0 = 191(0xbf, float:2.68E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.A0D(r0, r4)
            com.facebook.analytics.DeprecatedAnalyticsLogger r0 = r3.A0A
            r0.A09(r2)
            return
        L_0x0084:
            X.0u3 r2 = r2.A07
            X.0u3 r0 = X.AnonymousClass0u3.IN_MEMORY_CACHE
            if (r2 == r0) goto L_0x0093
            X.0u3 r0 = X.AnonymousClass0u3.LOCAL_DISK_CACHE
            if (r2 == r0) goto L_0x0093
            X.0u3 r1 = X.AnonymousClass0u3.LOCAL_UNSPECIFIED_CACHE
            r0 = 0
            if (r2 != r1) goto L_0x0094
        L_0x0093:
            r0 = 1
        L_0x0094:
            r1 = 3
            if (r0 == 0) goto L_0x002a
            r1 = 2
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C187815b.A01(X.1md, X.0sF):void");
    }
}
