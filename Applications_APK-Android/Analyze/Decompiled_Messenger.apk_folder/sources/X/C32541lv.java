package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

/* renamed from: X.1lv  reason: invalid class name and case insensitive filesystem */
public final class C32541lv {
    public C16440x4 A00;
    public C15750vt A01;
    private ListenableFuture A02;
    public final AnonymousClass09P A03;
    private final AnonymousClass1YI A04;
    private final C32731mF A05;
    private final Executor A06;

    public static final C32541lv A00(AnonymousClass1XY r1) {
        return new C32541lv(r1);
    }

    public static void A01(C32541lv r2) {
        C15750vt r0 = r2.A01;
        if (r0 != null) {
            r0.A02().setVisibility(8);
        }
        C16440x4 r02 = r2.A00;
        if (r02 != null) {
            r02.A00.A02.A00.A07.setTranslationY(0.0f);
        }
    }

    public void A02() {
        if (this.A04.AbO(AnonymousClass1Y3.A4z, false)) {
            if (C50302dk.A02(this.A02)) {
                this.A02.cancel(true);
            }
            ListenableFuture A012 = this.A05.A01(AnonymousClass24B.$const$string(15), "INBOX", null);
            this.A02 = A012;
            C05350Yp.A08(A012, new C1302568d(this), this.A06);
        }
    }

    public C32541lv(AnonymousClass1XY r2) {
        this.A05 = new C32731mF(r2);
        this.A06 = AnonymousClass0UX.A0U(r2);
        this.A04 = AnonymousClass0WA.A00(r2);
        this.A03 = C04750Wa.A01(r2);
    }
}
