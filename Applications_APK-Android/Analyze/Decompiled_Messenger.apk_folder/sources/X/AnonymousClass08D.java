package X;

import com.facebook.common.smartgc.art.ArtSmartGc;
import com.facebook.common.smartgc.dalvik.DalvikSmartGc;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.08D  reason: invalid class name */
public final class AnonymousClass08D {
    public static AnonymousClass0A0 A00;
    public static final C013809z[] A01 = new C013809z[7];
    public static final C013709y[] A02 = new C013709y[7];
    private static final AtomicBoolean A03 = new AtomicBoolean(false);

    static {
        AnonymousClass0A0 dalvikSmartGc;
        if (!DalvikSmartGc.CAN_RUN_ON_THIS_PLATFORM) {
            dalvikSmartGc = null;
        } else {
            dalvikSmartGc = new DalvikSmartGc();
        }
        if (dalvikSmartGc == null) {
            if (!ArtSmartGc.CAN_RUN_ON_THIS_PLATFORM) {
                dalvikSmartGc = null;
            } else {
                dalvikSmartGc = new ArtSmartGc();
            }
        }
        if (dalvikSmartGc == null) {
            dalvikSmartGc = C02920Gz.A00;
        }
        A00 = dalvikSmartGc;
    }

    private static AnonymousClass084 A00(int i) {
        boolean z = false;
        if (A00 != C02920Gz.A00) {
            z = true;
        }
        if (!z) {
            return null;
        }
        if (C011108x.A00) {
            return A01[i];
        }
        return A02[i];
    }

    private static boolean A03(int i) {
        boolean z = false;
        if (A00 != C02920Gz.A00) {
            z = true;
        }
        if (!z) {
            return false;
        }
        if (i < 0 || i >= 7) {
            throw new IllegalArgumentException(AnonymousClass08S.A0A("GcSection ", i, " is not valid!"));
        } else if (A00(i) != null) {
            return true;
        } else {
            return false;
        }
    }

    public static void A01(int i) {
        if (A03(i) && !A03.getAndSet(true)) {
            AnonymousClass084 A002 = A00(i);
            if (A002 != null) {
                A002.toString();
            }
            A00.badTimeToDoGc(A002);
        }
    }

    public static void A02(int i) {
        if (A03(i) && A03.getAndSet(false)) {
            A00.notAsBadTimeToDoGc();
        }
    }
}
