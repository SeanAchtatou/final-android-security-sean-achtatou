package X;

import android.net.TrafficStats;
import com.facebook.device.resourcemonitor.DataUsageBytes;
import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ex  reason: invalid class name and case insensitive filesystem */
public final class C08260ex extends AnonymousClass0f8 {
    private static volatile C08260ex A00;

    public String Azg() {
        return "data_usage";
    }

    public static final C08260ex A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C08260ex.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new C08260ex(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        DataUsageBytes dataUsageBytes = (DataUsageBytes) obj;
        DataUsageBytes dataUsageBytes2 = (DataUsageBytes) obj2;
        if (dataUsageBytes != null && dataUsageBytes2 != null) {
            DataUsageBytes dataUsageBytes3 = new DataUsageBytes(dataUsageBytes2.A00 - dataUsageBytes.A00, dataUsageBytes2.A01 - dataUsageBytes.A01);
            performanceLoggingEvent.A06("bytes_received", dataUsageBytes3.A00);
            performanceLoggingEvent.A06("bytes_transmitted", dataUsageBytes3.A01);
        }
    }

    public long Azh() {
        return C08350fD.A02;
    }

    public Class B3O() {
        return DataUsageBytes.class;
    }

    public Object CGO() {
        long j;
        long j2;
        try {
            j = TrafficStats.getTotalRxBytes();
        } catch (RuntimeException e) {
            AnonymousClass0f9.A02(e);
            j = 0;
        }
        try {
            j2 = TrafficStats.getTotalTxBytes();
        } catch (RuntimeException e2) {
            AnonymousClass0f9.A02(e2);
            j2 = 0;
        }
        return new DataUsageBytes(j, j2);
    }

    private C08260ex(AnonymousClass1XY r1) {
        AnonymousClass0f9.A00(r1);
    }

    public boolean BEZ(C08360fE r2) {
        return true;
    }
}
