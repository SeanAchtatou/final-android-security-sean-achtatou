package X;

import android.net.Uri;
import com.google.common.base.Optional;
import java.util.Set;

/* renamed from: X.0aq  reason: invalid class name and case insensitive filesystem */
public final class C06100aq {
    public final Uri A00;
    public final String A01;
    public final Set A02 = C25011Xz.A03();
    private final C06110ar A03;

    public static Optional A00(C06100aq r1, Uri uri) {
        try {
            C55562oH A022 = r1.A03.A02(uri.toString());
            if (A022 != null && "THREAD".equals(A022.A01)) {
                return Optional.of(A022.A00.getString("thread_id"));
            }
        } catch (C37681w4 unused) {
        }
        return Optional.absent();
    }

    public C06100aq(Uri uri, String str) {
        this.A00 = uri;
        C06110ar r2 = new C06110ar();
        this.A03 = r2;
        r2.A03(AnonymousClass08S.A0J(str, "/{thread_id}"), "THREAD");
        this.A01 = Uri.parse(str).getPath();
    }
}
