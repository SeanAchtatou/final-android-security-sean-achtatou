package X;

import com.facebook.gk.sessionless.GkSessionlessModule;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1eO  reason: invalid class name and case insensitive filesystem */
public final class C28181eO extends AnonymousClass0UT {
    public AnonymousClass9M8 A00(AnonymousClass9M6 r14) {
        return new AnonymousClass9M8(AnonymousClass067.A02(), FbSharedPreferencesModule.A00(this), C25901aa.A00(this), GkSessionlessModule.A00(this), r14.name, r14.groupSize, r14.groupCount, r14.startDate, r14.endDate, r14.mConditionalFilter, r14.killswitch, r14.launchswitch);
    }

    public C28181eO(AnonymousClass1XY r1) {
        super(r1);
    }
}
