package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.Iterator;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1fG  reason: invalid class name and case insensitive filesystem */
public final class C28721fG {
    public static final String[] A01 = {"thread_key", "user_key", "phone", "sms_participant_fbid", "type", "is_admin", "admin_type", "last_read_receipt_time", "last_read_receipt_watermark_time", "last_delivered_receipt_time", "last_delivered_receipt_id", "request_timestamp_ms", "can_viewer_message", "inviter_user_key", "request_source"};
    private static volatile C28721fG A02;
    public final C04310Tq A00;

    public static final C28721fG A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C28721fG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C28721fG(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static String A01(ImmutableList immutableList) {
        String[] strArr = A01;
        StringBuilder sb = new StringBuilder(100);
        sb.append("thread_key");
        sb.append(" IN (");
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            sb.append('\'');
            sb.append(((ThreadKey) it.next()).A0J());
            sb.append("',");
        }
        if (!immutableList.isEmpty()) {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append(')');
        return AnonymousClass08S.A0T("((", SQLiteQueryBuilder.buildQueryString(false, "thread_participants", strArr, sb.toString(), null, null, null, null), ") NATURAL LEFT JOIN ", "thread_users", " )");
    }

    public static void A03(SQLiteDatabase sQLiteDatabase, ThreadKey threadKey, Collection collection, Integer num) {
        ContentValues contentValues = new ContentValues();
        C007406x.A01(sQLiteDatabase, -1365934489);
        try {
            sQLiteDatabase.delete("thread_participants", "thread_key=? AND type=?", new String[]{threadKey.A0J(), C181710m.A01(num)});
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                A02(contentValues, (ThreadParticipant) it.next(), threadKey, num);
                C007406x.A00(-688578539);
                sQLiteDatabase.replaceOrThrow("thread_participants", null, contentValues);
                C007406x.A00(1390324830);
                contentValues.clear();
            }
            sQLiteDatabase.setTransactionSuccessful();
        } finally {
            C007406x.A02(sQLiteDatabase, -625478761);
        }
    }

    private C28721fG(AnonymousClass1XY r2) {
        this.A00 = C25771aN.A02(r2);
    }

    public static void A02(ContentValues contentValues, ThreadParticipant threadParticipant, ThreadKey threadKey, Integer num) {
        contentValues.put("thread_key", threadKey.A0J());
        contentValues.put("user_key", threadParticipant.A00().A06());
        contentValues.put("phone", threadParticipant.A04.A04);
        contentValues.put("sms_participant_fbid", threadParticipant.A04.A05);
        contentValues.put("last_read_receipt_time", Long.valueOf(threadParticipant.A02));
        contentValues.put("last_read_receipt_watermark_time", Long.valueOf(threadParticipant.A03));
        contentValues.put("last_delivered_receipt_time", Long.valueOf(threadParticipant.A01));
        contentValues.put("last_delivered_receipt_id", threadParticipant.A07);
        contentValues.put("type", C181710m.A01(num));
        contentValues.put("is_admin", Boolean.valueOf(threadParticipant.A09));
        contentValues.put("admin_type", Integer.valueOf(threadParticipant.A05.dbValue));
        contentValues.put("can_viewer_message", Boolean.valueOf(threadParticipant.A08));
        UserKey userKey = threadParticipant.A06;
        if (userKey != null) {
            contentValues.put("inviter_user_key", userKey.A06());
            contentValues.put("request_source", Integer.valueOf(threadParticipant.A00));
        }
    }
}
