package X;

import android.text.TextUtils;
import com.facebook.flexiblesampling.SamplingResult;
import com.fasterxml.jackson.databind.JsonNode;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0Xj  reason: invalid class name and case insensitive filesystem */
public final class C05030Xj {
    public final C06500bb A00;
    public final C05060Xm A01;
    public final C05770aI A02;
    public final Map A03 = new HashMap();
    private final C05050Xl A04;
    private final AnonymousClass0US A05;

    public static synchronized void A01(C05030Xj r6) {
        synchronized (r6) {
            if (!(!TextUtils.isEmpty(r6.A01.A02("__fs_policy_config_checksum__", BuildConfig.FLAVOR)))) {
                C05050Xl r5 = r6.A04;
                if (r5.A00.BFQ()) {
                    String B4F = r5.A00.B4F(C08720fq.A0G, BuildConfig.FLAVOR);
                    String B4F2 = r5.A00.B4F(C08720fq.A0F, BuildConfig.FLAVOR);
                    String B4F3 = r5.A00.B4F(C08720fq.A0H, null);
                    if (!TextUtils.isEmpty(B4F) && !TextUtils.isEmpty(B4F2)) {
                        r6.A04(B4F, B4F2, B4F3);
                        C30281hn edit = r5.A00.edit();
                        edit.C1B(C08720fq.A0G);
                        edit.C1B(C08720fq.A0F);
                        edit.C1B(C08720fq.A0H);
                        edit.commit();
                    }
                }
                TextUtils.isEmpty(r6.A01.A02("__fs_policy_config_checksum__", BuildConfig.FLAVOR));
            }
        }
    }

    private static Set A00(JsonNode jsonNode) {
        HashSet hashSet = new HashSet(jsonNode.size());
        Iterator it = jsonNode.iterator();
        while (it.hasNext()) {
            hashSet.add(((JsonNode) it.next()).asText());
        }
        return hashSet;
    }

    public SamplingResult A03(String str, String str2, String str3, boolean z) {
        AnonymousClass0YK r3 = new AnonymousClass0YK();
        A01(this);
        int A012 = this.A01.A01(str, str2, str3, -2);
        boolean z2 = false;
        if (A012 == -1) {
            z2 = true;
        }
        if (z2) {
            r3.A01 = false;
        } else {
            boolean z3 = false;
            if (A012 == -2) {
                z3 = true;
            }
            if (z3) {
                r3.A01 = true;
                r3.A02 = false;
                A012 = 1;
            } else {
                r3.A01 = true;
                r3.A02 = true;
            }
        }
        r3.A00 = A012;
        boolean z4 = false;
        if (A012 == -1) {
            z4 = true;
        }
        if (z4) {
            int i = AnonymousClass1Y3.A1c;
            if (z) {
                i = 1;
            }
            r3.A00 = i;
        }
        return new SamplingResult(r3);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:92|93|94|95|96) */
    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        X.C010708t.A0M("QPLConfig", "failed to save qpl config", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01f0, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01f5, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:95:0x01f4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(java.lang.String r18, java.lang.String r19, java.lang.String r20) {
        /*
            r17 = this;
            r8 = r17
            X.0aI r0 = r8.A02
            X.16O r4 = r0.A06()
            X.AnonymousClass16O.A04(r4)
            r0 = 1
            r4.A01 = r0
            r3 = r19
            X.0US r0 = r8.A05     // Catch:{ IOException -> 0x0231 }
            java.lang.Object r0 = r0.get()     // Catch:{ IOException -> 0x0231 }
            X.0jJ r0 = (X.AnonymousClass0jJ) r0     // Catch:{ IOException -> 0x0231 }
            com.fasterxml.jackson.databind.JsonNode r1 = r0.readTree(r3)     // Catch:{ IOException -> 0x0231 }
            boolean r0 = r1.isArray()     // Catch:{ IOException -> 0x0231 }
            java.lang.String r7 = "__fs_policy_blacklisted_events__"
            r6 = r20
            if (r0 == 0) goto L_0x0036
            java.util.Set r0 = A00(r1)     // Catch:{ IOException -> 0x0231 }
            r4.A0C(r7, r0)     // Catch:{ IOException -> 0x0231 }
        L_0x002d:
            java.lang.String r0 = "__fs_policy_config_checksum__"
            r1 = r18
            r4.A0B(r0, r1)     // Catch:{ IOException -> 0x0231 }
            goto L_0x0217
        L_0x0036:
            java.util.Iterator r16 = r1.fields()     // Catch:{ IOException -> 0x0231 }
        L_0x003a:
            boolean r0 = r16.hasNext()     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x002d
            java.lang.Object r2 = r16.next()     // Catch:{ IOException -> 0x0231 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ IOException -> 0x0231 }
            java.lang.Object r1 = r2.getKey()     // Catch:{ IOException -> 0x0231 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x0231 }
            java.lang.String r0 = "blacklist"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x006a
            java.lang.Object r1 = r2.getValue()     // Catch:{ IOException -> 0x0231 }
            com.fasterxml.jackson.databind.JsonNode r1 = (com.fasterxml.jackson.databind.JsonNode) r1     // Catch:{ IOException -> 0x0231 }
            if (r1 == 0) goto L_0x003a
            boolean r0 = r1.isArray()     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x003a
            java.util.Set r0 = A00(r1)     // Catch:{ IOException -> 0x0231 }
            r4.A0C(r7, r0)     // Catch:{ IOException -> 0x0231 }
            goto L_0x003a
        L_0x006a:
            java.lang.Object r5 = r2.getKey()     // Catch:{ IOException -> 0x0231 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x0231 }
            java.lang.Object r9 = r2.getValue()     // Catch:{ IOException -> 0x0231 }
            com.fasterxml.jackson.databind.JsonNode r9 = (com.fasterxml.jackson.databind.JsonNode) r9     // Catch:{ IOException -> 0x0231 }
            java.util.Map r0 = r8.A03     // Catch:{ IOException -> 0x0231 }
            java.lang.Object r10 = r0.get(r5)     // Catch:{ IOException -> 0x0231 }
            X.0ZE r10 = (X.AnonymousClass0ZE) r10     // Catch:{ IOException -> 0x0231 }
            if (r10 == 0) goto L_0x0203
            boolean r0 = r9.isObject()     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x01fe
            java.lang.String r1 = "version"
            boolean r0 = r9.has(r1)     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x01fe
            com.fasterxml.jackson.databind.JsonNode r0 = r9.get(r1)     // Catch:{ IOException -> 0x0231 }
            java.lang.String r1 = r0.asText()     // Catch:{ IOException -> 0x0231 }
            java.lang.String r0 = "v2"
            boolean r0 = r0.equals(r1)     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x01fe
            java.lang.String r2 = "QPLConfigHandler"
            java.lang.String r0 = "checksum"
            com.fasterxml.jackson.databind.JsonNode r0 = r9.get(r0)     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x00f0
            java.lang.String r12 = r0.asText()     // Catch:{ IOException -> 0x0231 }
        L_0x00ac:
            X.C000300h.A01(r12)     // Catch:{ IOException -> 0x0231 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ IOException -> 0x0231 }
            java.lang.String r0 = "sampling"
            com.fasterxml.jackson.databind.JsonNode r0 = r9.get(r0)     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x00ee
            java.lang.String r11 = r0.asText()     // Catch:{ IOException -> 0x0231 }
        L_0x00bd:
            X.C000300h.A01(r11)     // Catch:{ IOException -> 0x0231 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ IOException -> 0x0231 }
            java.lang.String r0 = "metadata"
            com.fasterxml.jackson.databind.JsonNode r0 = r9.get(r0)     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x00ec
            java.lang.String r14 = r0.asText()     // Catch:{ IOException -> 0x0231 }
        L_0x00ce:
            X.C000300h.A01(r14)     // Catch:{ IOException -> 0x0231 }
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ IOException -> 0x0231 }
            X.0bc r0 = r10.A02     // Catch:{ IOException -> 0x0231 }
            java.util.concurrent.atomic.AtomicReference r0 = r0.A03     // Catch:{ IOException -> 0x0231 }
            java.lang.Object r0 = r0.get()     // Catch:{ IOException -> 0x0231 }
            X.0YP r0 = (X.AnonymousClass0YP) r0     // Catch:{ IOException -> 0x0231 }
            if (r0 != 0) goto L_0x00e9
            java.lang.String r0 = ""
        L_0x00e1:
            boolean r0 = r0.equals(r12)     // Catch:{ IOException -> 0x0231 }
            r15 = 1
            if (r0 != 0) goto L_0x01ff
            goto L_0x00f2
        L_0x00e9:
            java.lang.String r0 = r0.A02     // Catch:{ IOException -> 0x0231 }
            goto L_0x00e1
        L_0x00ec:
            r14 = 0
            goto L_0x00ce
        L_0x00ee:
            r11 = 0
            goto L_0x00bd
        L_0x00f0:
            r12 = 0
            goto L_0x00ac
        L_0x00f2:
            X.0US r0 = r10.A01     // Catch:{ IOException | NumberFormatException -> 0x0101 }
            java.lang.Object r1 = r0.get()     // Catch:{ IOException | NumberFormatException -> 0x0101 }
            X.0jJ r1 = (X.AnonymousClass0jJ) r1     // Catch:{ IOException | NumberFormatException -> 0x0101 }
            X.09P r0 = r10.A00     // Catch:{ IOException | NumberFormatException -> 0x0101 }
            X.1Zr r11 = X.AnonymousClass0ZE.A00(r11, r1, r0)     // Catch:{ IOException | NumberFormatException -> 0x0101 }
            goto L_0x010f
        L_0x0101:
            r1 = move-exception
            java.lang.String r0 = "failed to read sampling config"
            X.C010708t.A0L(r2, r0, r1)     // Catch:{ IOException -> 0x0231 }
            X.1Zr r11 = new X.1Zr     // Catch:{ IOException -> 0x0231 }
            r1 = 0
            r0 = 0
            r11.<init>(r0, r1, r1)     // Catch:{ IOException -> 0x0231 }
            r15 = 0
        L_0x010f:
            X.0US r0 = r10.A01     // Catch:{ IOException | NumberFormatException -> 0x0123 }
            java.lang.Object r13 = r0.get()     // Catch:{ IOException | NumberFormatException -> 0x0123 }
            X.0jJ r13 = (X.AnonymousClass0jJ) r13     // Catch:{ IOException | NumberFormatException -> 0x0123 }
            X.09P r0 = r10.A00     // Catch:{ IOException | NumberFormatException -> 0x0123 }
            X.0cs r1 = new X.0cs     // Catch:{ IOException | NumberFormatException -> 0x0123 }
            X.1Zr r0 = X.AnonymousClass0ZE.A00(r14, r13, r0)     // Catch:{ IOException | NumberFormatException -> 0x0123 }
            r1.<init>(r0)     // Catch:{ IOException | NumberFormatException -> 0x0123 }
            goto L_0x012b
        L_0x0123:
            r1 = move-exception
            java.lang.String r0 = "failed to read metadata config"
            X.C010708t.A0L(r2, r0, r1)     // Catch:{ IOException -> 0x0231 }
            r1 = 0
            r15 = 0
        L_0x012b:
            X.0bc r2 = r10.A02     // Catch:{ IOException -> 0x0231 }
            X.0YP r10 = new X.0YP     // Catch:{ IOException -> 0x0231 }
            r10.<init>(r6, r11, r1, r12)     // Catch:{ IOException -> 0x0231 }
            X.0dw r0 = r2.A01     // Catch:{ IOException -> 0x0231 }
            X.0US r0 = r0.A00     // Catch:{ IOException -> 0x0231 }
            java.lang.Object r1 = r0.get()     // Catch:{ IOException -> 0x0231 }
            X.0XN r1 = (X.AnonymousClass0XN) r1     // Catch:{ IOException -> 0x0231 }
            r0 = 0
            if (r1 == 0) goto L_0x0147
            com.facebook.auth.viewercontext.ViewerContext r1 = r1.A08()     // Catch:{ IOException -> 0x0231 }
            if (r1 == 0) goto L_0x0147
            java.lang.String r0 = r1.mUserId     // Catch:{ IOException -> 0x0231 }
        L_0x0147:
            java.lang.String r1 = r10.A03     // Catch:{ IOException -> 0x0231 }
            if (r0 != 0) goto L_0x014c
            goto L_0x0151
        L_0x014c:
            boolean r0 = r0.equals(r1)     // Catch:{ IOException -> 0x0231 }
            goto L_0x0155
        L_0x0151:
            r0 = 0
            if (r1 != 0) goto L_0x0155
            r0 = 1
        L_0x0155:
            if (r0 == 0) goto L_0x015c
            java.util.concurrent.atomic.AtomicReference r0 = r2.A03     // Catch:{ IOException -> 0x0231 }
            r0.set(r10)     // Catch:{ IOException -> 0x0231 }
        L_0x015c:
            X.0US r0 = r2.A00     // Catch:{ IOException -> 0x0231 }
            java.lang.Object r11 = r0.get()     // Catch:{ IOException -> 0x0231 }
            X.0YL r11 = (X.AnonymousClass0YL) r11     // Catch:{ IOException -> 0x0231 }
            java.lang.String r2 = r10.A03     // Catch:{ IOException -> 0x0231 }
            if (r2 != 0) goto L_0x0169
            goto L_0x017e
        L_0x0169:
            X.0cj r0 = X.C07810eC.A00     // Catch:{ IOException -> 0x0231 }
            java.nio.charset.Charset r1 = com.google.common.base.Charsets.UTF_8     // Catch:{ IOException -> 0x0231 }
            X.0co r0 = r0.A01()     // Catch:{ IOException -> 0x0231 }
            X.0co r0 = r0.A05(r2, r1)     // Catch:{ IOException -> 0x0231 }
            X.0cq r0 = r0.A09()     // Catch:{ IOException -> 0x0231 }
            java.lang.String r12 = r0.toString()     // Catch:{ IOException -> 0x0231 }
            goto L_0x017f
        L_0x017e:
            r12 = 0
        L_0x017f:
            android.content.Context r2 = r11.A00     // Catch:{ IOException -> 0x01f5 }
            java.lang.String r1 = "qpl"
            r0 = 0
            java.io.File r11 = r2.getDir(r1, r0)     // Catch:{ IOException -> 0x01f5 }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x01f5 }
            java.lang.String r0 = "qpl_sampling_config_v2.tmp"
            r1.<init>(r11, r0)     // Catch:{ IOException -> 0x01f5 }
            boolean r0 = r11.exists()     // Catch:{ IOException -> 0x01f5 }
            if (r0 != 0) goto L_0x01a3
            boolean r0 = r11.mkdirs()     // Catch:{ IOException -> 0x01f5 }
            if (r0 != 0) goto L_0x01a3
            java.io.IOException r1 = new java.io.IOException     // Catch:{ IOException -> 0x01f5 }
            java.lang.String r0 = "Failed to create storage dir"
            r1.<init>(r0)     // Catch:{ IOException -> 0x01f5 }
            throw r1     // Catch:{ IOException -> 0x01f5 }
        L_0x01a3:
            X.8WI r2 = new X.8WI     // Catch:{ IOException -> 0x01f5 }
            r2.<init>(r1, r12, r1, r11)     // Catch:{ IOException -> 0x01f5 }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x01f5 }
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x01f5 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x01f5 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x01f5 }
            r0 = 1
            r1.writeInt(r0)     // Catch:{ all -> 0x01ee }
            java.lang.String r0 = r10.A02     // Catch:{ all -> 0x01ee }
            r1.writeUTF(r0)     // Catch:{ all -> 0x01ee }
            X.1Zr r2 = r10.A01     // Catch:{ all -> 0x01ee }
            int r0 = r2.A00     // Catch:{ all -> 0x01ee }
            r1.writeInt(r0)     // Catch:{ all -> 0x01ee }
            android.util.SparseIntArray r0 = r2.A02     // Catch:{ all -> 0x01ee }
            X.C25451Zr.A02(r1, r0)     // Catch:{ all -> 0x01ee }
            android.util.SparseIntArray r0 = r2.A01     // Catch:{ all -> 0x01ee }
            X.C25451Zr.A02(r1, r0)     // Catch:{ all -> 0x01ee }
            X.0cs r2 = r10.A00     // Catch:{ all -> 0x01ee }
            if (r2 != 0) goto L_0x01d5
            r0 = 0
            r1.writeByte(r0)     // Catch:{ all -> 0x01ee }
            goto L_0x01ea
        L_0x01d5:
            r0 = 1
            r1.writeByte(r0)     // Catch:{ all -> 0x01ee }
            X.1Zr r2 = r2.A00     // Catch:{ all -> 0x01ee }
            int r0 = r2.A00     // Catch:{ all -> 0x01ee }
            r1.writeInt(r0)     // Catch:{ all -> 0x01ee }
            android.util.SparseIntArray r0 = r2.A02     // Catch:{ all -> 0x01ee }
            X.C25451Zr.A02(r1, r0)     // Catch:{ all -> 0x01ee }
            android.util.SparseIntArray r0 = r2.A01     // Catch:{ all -> 0x01ee }
            X.C25451Zr.A02(r1, r0)     // Catch:{ all -> 0x01ee }
        L_0x01ea:
            r1.close()     // Catch:{ IOException -> 0x01f5 }
            goto L_0x01ff
        L_0x01ee:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x01f0 }
        L_0x01f0:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x01f4 }
        L_0x01f4:
            throw r0     // Catch:{ IOException -> 0x01f5 }
        L_0x01f5:
            r2 = move-exception
            java.lang.String r1 = "QPLConfig"
            java.lang.String r0 = "failed to save qpl config"
            X.C010708t.A0M(r1, r0, r2)     // Catch:{ IOException -> 0x0231 }
            goto L_0x01ff
        L_0x01fe:
            r15 = 0
        L_0x01ff:
            if (r15 == 0) goto L_0x0203
            goto L_0x003a
        L_0x0203:
            boolean r0 = r9.isObject()     // Catch:{ IOException -> 0x0231 }
            if (r0 == 0) goto L_0x020e
            r8.A02(r5, r4, r9)     // Catch:{ IOException -> 0x0231 }
            goto L_0x003a
        L_0x020e:
            int r0 = r9.asInt()     // Catch:{ IOException -> 0x0231 }
            r4.A09(r5, r0)     // Catch:{ IOException -> 0x0231 }
            goto L_0x003a
        L_0x0217:
            if (r20 == 0) goto L_0x021a
            goto L_0x0224
        L_0x021a:
            X.1Y7 r0 = X.C08720fq.A0H     // Catch:{ IOException -> 0x0231 }
            java.lang.String r0 = r0.A05()     // Catch:{ IOException -> 0x0231 }
            r4.A08(r0)     // Catch:{ IOException -> 0x0231 }
            goto L_0x022d
        L_0x0224:
            X.1Y7 r0 = X.C08720fq.A0H     // Catch:{ IOException -> 0x0231 }
            java.lang.String r0 = r0.A05()     // Catch:{ IOException -> 0x0231 }
            r4.A0B(r0, r6)     // Catch:{ IOException -> 0x0231 }
        L_0x022d:
            r4.A06()     // Catch:{ IOException -> 0x0231 }
            return
        L_0x0231:
            r1 = move-exception
            java.lang.String r0 = "AnalyticsLoggingPolicy"
            X.C010708t.A0T(r0, r1, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05030Xj.A04(java.lang.String, java.lang.String, java.lang.String):void");
    }

    public C05030Xj(AnonymousClass0US r8, C05770aI r9, C05060Xm r10, C05050Xl r11, Set set) {
        this.A05 = r8;
        this.A02 = r9;
        this.A01 = r10;
        this.A04 = r11;
        this.A00 = new C06500bb(r9, r10);
        if (set != null) {
            Iterator it = set.iterator();
            while (it.hasNext()) {
                AnonymousClass0ZE r5 = (AnonymousClass0ZE) it.next();
                String[] strArr = {"perf"};
                for (int i = 0; i < 1; i++) {
                    this.A03.put(strArr[i], r5);
                }
            }
        }
    }

    private void A02(String str, AnonymousClass16O r7, JsonNode jsonNode) {
        Iterator fields = jsonNode.fields();
        while (fields.hasNext()) {
            Map.Entry entry = (Map.Entry) fields.next();
            if (((String) entry.getKey()).equals("*")) {
                r7.A09(str, ((JsonNode) entry.getValue()).asInt());
            } else {
                String A0P = AnonymousClass08S.A0P(str, ":", (String) entry.getKey());
                JsonNode jsonNode2 = (JsonNode) entry.getValue();
                if (jsonNode2.isObject()) {
                    A02(A0P, r7, jsonNode2);
                } else {
                    r7.A09(A0P, jsonNode2.asInt());
                }
            }
        }
    }
}
