package X;

import com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000;

/* renamed from: X.1yn  reason: invalid class name and case insensitive filesystem */
public final class C39281yn {
    private final C11170mD A00;
    private final String A01;

    public static final C39281yn A00(AnonymousClass1XY r1) {
        return new C39281yn(r1);
    }

    public void A01(boolean z) {
        C82483w3 r3 = new C82483w3();
        GQLCallInputCInputShape1S0000000 gQLCallInputCInputShape1S0000000 = new GQLCallInputCInputShape1S0000000(AnonymousClass1Y3.A1P);
        gQLCallInputCInputShape1S0000000.A06("active_now_on", Boolean.valueOf(z));
        gQLCallInputCInputShape1S0000000.A0B(this.A01);
        r3.A04("data", gQLCallInputCInputShape1S0000000);
        this.A00.A05(C26931cN.A01(r3));
    }

    private C39281yn(AnonymousClass1XY r2) {
        this.A00 = C11170mD.A00(r2);
        this.A01 = AnonymousClass0XJ.A0F(r2);
    }
}
