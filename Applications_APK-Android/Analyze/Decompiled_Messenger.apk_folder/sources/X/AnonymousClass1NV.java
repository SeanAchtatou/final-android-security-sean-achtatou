package X;

/* renamed from: X.1NV  reason: invalid class name */
public final class AnonymousClass1NV {
    public static int A00 = AnonymousClass1Y3.A38;
    public static final int A01;
    public static volatile AnonymousClass1NW A02;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    static {
        int i;
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (((long) min) > 16777216) {
            i = (min >> 2) * 3;
        } else {
            i = min >> 1;
        }
        A01 = i;
    }
}
