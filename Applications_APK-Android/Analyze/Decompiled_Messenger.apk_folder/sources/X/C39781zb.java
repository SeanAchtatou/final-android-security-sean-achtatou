package X;

/* renamed from: X.1zb  reason: invalid class name and case insensitive filesystem */
public final class C39781zb implements C35411rB {
    private AnonymousClass0UN A00;

    public static final C39781zb A00(AnonymousClass1XY r1) {
        return new C39781zb(r1);
    }

    public double Amf(String str, String... strArr) {
        return (double) ((AnonymousClass2RK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6N, this.A00)).A01(str, AnonymousClass2RN.A01);
    }

    private C39781zb(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public C74973it Ame() {
        return C74973it.A01;
    }
}
