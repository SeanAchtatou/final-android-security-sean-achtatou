package X;

/* renamed from: X.1db  reason: invalid class name and case insensitive filesystem */
public interface C27691db extends C27671dZ {
    void onCreate(C11410n6 r1);

    void onDestroy(C11410n6 r1);

    void onPause(C11410n6 r1);

    void onResume(C11410n6 r1);

    void onStart(C11410n6 r1);

    void onStop(C11410n6 r1);
}
