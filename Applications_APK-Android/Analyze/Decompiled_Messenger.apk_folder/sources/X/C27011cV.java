package X;

import android.content.Context;
import com.facebook.compactdisk.current.CompactDiskManager;
import com.facebook.compactdisk.current.DiskCacheConfig;
import com.facebook.graphservice.GraphQLConsistencyJNI;
import com.facebook.graphservice.GraphQLServiceDecorator;
import com.facebook.graphservice.GraphQLServiceJNI;
import com.facebook.graphservice.asset.GraphServiceAsset;
import com.facebook.graphservice.factory.GraphQLServiceFactory;
import com.facebook.graphservice.interfaces.GraphQLService;
import com.facebook.graphservice.interfaces.TreeJsonSerializer;
import com.facebook.graphservice.interfaces.TreeSerializer;
import com.facebook.graphservice.live.GraphQLLiveConfig;
import com.facebook.inject.InjectorModule;
import com.facebook.livefeed.LiveFeedClientQEStore;
import com.facebook.livefeed.service.common.LiveFeedServiceFactory;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.reactivesocket.flipper.common.FlipperLiveDataProviderFactory;
import com.facebook.reactivesocket.livequery.common.LiveQueryServiceFactory;
import com.facebook.stash.core.Stash;
import com.facebook.tigon.nativeservice.authed.NativeAuthedTigonServiceHolder;
import com.google.common.base.Preconditions;
import java.util.concurrent.ScheduledExecutorService;

@InjectorModule
/* renamed from: X.1cV  reason: invalid class name and case insensitive filesystem */
public final class C27011cV extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static C05540Zi A01;
    private static C05540Zi A02;
    private static C05540Zi A03;
    private static final Object A04 = new Object();
    private static final Object A05 = new Object();
    private static volatile GraphServiceAsset A06;
    private static volatile GraphQLServiceFactory A07;
    private static volatile TreeJsonSerializer A08;
    private static volatile TreeSerializer A09;
    private static volatile ScheduledExecutorService A0A;

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a1, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        X.C010708t.A0T("graph_store_cache_initialize", r7, "Can't create graph_store");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r14 = new com.facebook.graphservice.GraphQLConsistencyJNI(r15, null, r6, r18, 0, 16);
        r4.markerEnd(17432582, 3);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.facebook.graphservice.GraphQLConsistencyJNI A00(X.AnonymousClass1XY r20) {
        /*
            java.lang.Class<com.facebook.graphservice.GraphQLConsistencyJNI> r13 = com.facebook.graphservice.GraphQLConsistencyJNI.class
            monitor-enter(r13)
            X.0Zi r0 = X.C27011cV.A00     // Catch:{ all -> 0x00db }
            X.0Zi r0 = X.C05540Zi.A00(r0)     // Catch:{ all -> 0x00db }
            X.C27011cV.A00 = r0     // Catch:{ all -> 0x00db }
            r1 = r20
            boolean r0 = r0.A03(r1)     // Catch:{ all -> 0x00d4 }
            if (r0 == 0) goto L_0x00c9
            X.0Zi r0 = X.C27011cV.A00     // Catch:{ all -> 0x00d4 }
            X.1Xi r2 = r0.A01()     // Catch:{ all -> 0x00d4 }
            X.1XY r2 = (X.AnonymousClass1XY) r2     // Catch:{ all -> 0x00d4 }
            X.0Zi r5 = X.C27011cV.A00     // Catch:{ all -> 0x00d4 }
            com.facebook.graphservice.asset.GraphServiceAsset r15 = A02(r2)     // Catch:{ all -> 0x00d4 }
            java.util.concurrent.ScheduledExecutorService r10 = X.AnonymousClass0UX.A0d(r2)     // Catch:{ all -> 0x00d4 }
            java.util.concurrent.ScheduledExecutorService r18 = X.AnonymousClass0UX.A0f(r2)     // Catch:{ all -> 0x00d4 }
            X.1p6 r7 = new X.1p6     // Catch:{ all -> 0x00d4 }
            r7.<init>(r2)     // Catch:{ all -> 0x00d4 }
            com.facebook.quicklog.QuickPerformanceLogger r4 = X.AnonymousClass0ZD.A03(r2)     // Catch:{ all -> 0x00d4 }
            X.15S r1 = X.AnonymousClass15S.A00(r2)     // Catch:{ all -> 0x00d4 }
            int r0 = X.AnonymousClass1Y3.Av8     // Catch:{ all -> 0x00d4 }
            X.0VB r9 = X.AnonymousClass0VB.A00(r0, r2)     // Catch:{ all -> 0x00d4 }
            X.1Yd r8 = X.AnonymousClass0WT.A00(r2)     // Catch:{ all -> 0x00d4 }
            X.1iX r3 = X.C30721iX.A00(r2)     // Catch:{ all -> 0x00d4 }
            r2 = 17432582(0x10a0006, float:2.5346614E-38)
            r4.markerStart(r2)     // Catch:{ all -> 0x00d4 }
            r0 = 5000(0x1388, float:7.006E-42)
            X.1p7 r6 = new X.1p7     // Catch:{ all -> 0x00d4 }
            r6.<init>(r7, r10, r0)     // Catch:{ all -> 0x00d4 }
            java.lang.String r7 = X.C34141on.A01(r1)     // Catch:{ Exception -> 0x00a1 }
            r0 = 564350112826119(0x2014600010307, double:2.78826002974017E-309)
            long r11 = r8.At0(r0)     // Catch:{ Exception -> 0x00a1 }
            java.lang.Object r8 = r9.get()     // Catch:{ Exception -> 0x00a1 }
            com.facebook.storage.cleaner.PathSizeOverflowCleaner r8 = (com.facebook.storage.cleaner.PathSizeOverflowCleaner) r8     // Catch:{ Exception -> 0x00a1 }
            r0 = 20
            long r11 = r11 << r0
            r9 = 5242880(0x500000, double:2.590327E-317)
            monitor-enter(r8)     // Catch:{ Exception -> 0x00a1 }
            boolean r0 = r8.A03     // Catch:{ all -> 0x009e }
            if (r0 == 0) goto L_0x0083
            if (r7 == 0) goto L_0x0083
            java.util.HashMap r1 = r8.A01     // Catch:{ all -> 0x009e }
            java.lang.Long r0 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x009e }
            r1.put(r7, r0)     // Catch:{ all -> 0x009e }
            java.util.HashMap r1 = r8.A02     // Catch:{ all -> 0x009e }
            java.lang.Long r0 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x009e }
            r1.put(r7, r0)     // Catch:{ all -> 0x009e }
        L_0x0083:
            monitor-exit(r8)     // Catch:{ Exception -> 0x00a1 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00a1 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x00a1 }
            X.C34211ov.A00(r0)     // Catch:{ Exception -> 0x00a1 }
            com.facebook.graphservice.GraphQLConsistencyJNI r14 = new com.facebook.graphservice.GraphQLConsistencyJNI     // Catch:{ Exception -> 0x00a1 }
            r19 = 0
            r20 = 16
            r17 = r6
            r16 = r7
            r14.<init>(r15, r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x00a1 }
            r0 = 2
            r4.markerEnd(r2, r0)     // Catch:{ Exception -> 0x00a1 }
            goto L_0x00c4
        L_0x009e:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ Exception -> 0x00a1 }
            throw r0     // Catch:{ Exception -> 0x00a1 }
        L_0x00a1:
            r7 = move-exception
            java.lang.String r1 = "graph_store_cache_initialize"
            java.lang.String r0 = "Can't create graph_store"
            X.C010708t.A0T(r1, r7, r0)     // Catch:{ all -> 0x00d4 }
            com.facebook.graphservice.GraphQLConsistencyJNI r14 = new com.facebook.graphservice.GraphQLConsistencyJNI     // Catch:{ Exception -> 0x00bb }
            r16 = 0
            r19 = 0
            r20 = 16
            r17 = r6
            r14.<init>(r15, r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x00bb }
            r0 = 3
            r4.markerEnd(r2, r0)     // Catch:{ Exception -> 0x00bb }
            goto L_0x00c4
        L_0x00bb:
            r2 = move-exception
            java.lang.String r1 = "graph_service_initialize"
            java.lang.String r0 = "Failed to create fallback GraphQLConsistency"
            X.C010708t.A0T(r1, r2, r0)     // Catch:{ all -> 0x00d4 }
            throw r2     // Catch:{ all -> 0x00d4 }
        L_0x00c4:
            r3.C0U(r14)     // Catch:{ all -> 0x00d4 }
            r5.A00 = r14     // Catch:{ all -> 0x00d4 }
        L_0x00c9:
            X.0Zi r1 = X.C27011cV.A00     // Catch:{ all -> 0x00d4 }
            java.lang.Object r0 = r1.A00     // Catch:{ all -> 0x00d4 }
            com.facebook.graphservice.GraphQLConsistencyJNI r0 = (com.facebook.graphservice.GraphQLConsistencyJNI) r0     // Catch:{ all -> 0x00d4 }
            r1.A02()     // Catch:{ all -> 0x00db }
            monitor-exit(r13)     // Catch:{ all -> 0x00db }
            return r0
        L_0x00d4:
            r1 = move-exception
            X.0Zi r0 = X.C27011cV.A00     // Catch:{ all -> 0x00db }
            r0.A02()     // Catch:{ all -> 0x00db }
            throw r1     // Catch:{ all -> 0x00db }
        L_0x00db:
            r0 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x00db }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27011cV.A00(X.1XY):com.facebook.graphservice.GraphQLConsistencyJNI");
    }

    public static final GraphQLServiceJNI A01(AnonymousClass1XY r43) {
        GraphQLServiceJNI graphQLServiceJNI;
        GraphServiceAsset A022;
        ScheduledExecutorService A092;
        ScheduledExecutorService A0f;
        NativeAuthedTigonServiceHolder $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXFACTORY_METHOD;
        LiveQueryServiceFactory liveQueryServiceFactory;
        FlipperLiveDataProviderFactory flipperLiveDataProviderFactory;
        GraphQLConsistencyJNI A002;
        GraphQLLiveConfig graphQLLiveConfig;
        LiveFeedClientQEStore $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXFACTORY_METHOD;
        LiveFeedServiceFactory liveFeedServiceFactory;
        C12230or A003;
        QuickPerformanceLogger A032;
        String charSequence;
        int At0;
        int At02;
        boolean Aem;
        String B4C;
        boolean Aem2;
        GraphQLServiceJNI graphQLServiceJNI2;
        DiskCacheConfig diskCacheConfig;
        synchronized (GraphQLServiceJNI.class) {
            C05540Zi A004 = C05540Zi.A00(A01);
            A01 = A004;
            try {
                if (A004.A03(r43)) {
                    AnonymousClass1XY r0 = (AnonymousClass1XY) A01.A01();
                    C05540Zi r12 = A01;
                    A022 = A02(r0);
                    ScheduledExecutorService A0d = AnonymousClass0UX.A0d(r0);
                    A092 = A09(r0);
                    A0f = AnonymousClass0UX.A0f(r0);
                    $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXFACTORY_METHOD = NativeAuthedTigonServiceHolder.$ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXFACTORY_METHOD(r0);
                    liveQueryServiceFactory = new LiveQueryServiceFactory(r0);
                    flipperLiveDataProviderFactory = new FlipperLiveDataProviderFactory(r0);
                    A002 = A00(r0);
                    graphQLLiveConfig = new GraphQLLiveConfig(r0);
                    $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXFACTORY_METHOD = LiveFeedClientQEStore.$ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXFACTORY_METHOD(r0);
                    liveFeedServiceFactory = new LiveFeedServiceFactory(r0);
                    A003 = C12230or.A00(r0);
                    AnonymousClass0UQ A005 = AnonymousClass0UQ.A00(AnonymousClass1Y3.Ay7, r0);
                    Context A023 = AnonymousClass1YA.A02(r0);
                    C30541iE A072 = C30461i5.A07(r0);
                    C25051Yd A006 = AnonymousClass0WT.A00(r0);
                    A032 = AnonymousClass0ZD.A03(r0);
                    C34141on r3 = new C34141on(r0);
                    A032.markerStart(17432581);
                    charSequence = A023.getApplicationInfo().loadLabel(A023.getPackageManager()).toString();
                    At0 = (int) A006.At0(569637217700775L);
                    At02 = (int) A006.At0(569637217504166L);
                    Aem = A006.Aem(288162240929434L);
                    B4C = A006.B4C(851112194278167L);
                    Aem2 = A006.Aem(283880158531166L);
                    Stash A042 = r3.A04();
                    CompactDiskManager compactDiskManager = null;
                    if (A042 == null) {
                        compactDiskManager = (CompactDiskManager) A005.get();
                        diskCacheConfig = r3.A03(A023, A072);
                    } else {
                        diskCacheConfig = null;
                    }
                    graphQLServiceJNI2 = new GraphQLServiceJNI(A022, $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXFACTORY_METHOD, A092, A0f, liveQueryServiceFactory, flipperLiveDataProviderFactory, compactDiskManager, A042, diskCacheConfig, A0d, A002, graphQLLiveConfig, $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXFACTORY_METHOD, liveFeedServiceFactory, A003.BA9(), false, "localhost:8088", "Android", charSequence, At0, At02, Aem, B4C, false, Aem2);
                    A032.markerEnd(17432581, 2);
                    r12.A00 = graphQLServiceJNI2;
                }
            } catch (Exception e) {
                C010708t.A0T("graph_service_initialize", e, "Failed to create fallback GraphQLService");
                throw e;
            } catch (Exception e2) {
                C010708t.A0T("graph_service_cache_initialize", e2, "Can't create response cache");
                graphQLServiceJNI2 = new GraphQLServiceJNI(A022, $ul_$xXXcom_facebook_tigon_nativeservice_authed_NativeAuthedTigonServiceHolder$xXXFACTORY_METHOD, A092, A0f, liveQueryServiceFactory, flipperLiveDataProviderFactory, null, null, null, null, A002, graphQLLiveConfig, $ul_$xXXcom_facebook_livefeed_LiveFeedClientQEStore$xXXFACTORY_METHOD, liveFeedServiceFactory, A003.BA9(), false, "localhost:8088", "Android", charSequence, At0, At02, Aem, B4C, false, Aem2);
                A032.markerEnd(17432581, 3);
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
            C05540Zi r1 = A01;
            graphQLServiceJNI = (GraphQLServiceJNI) r1.A00;
            r1.A02();
        }
        return graphQLServiceJNI;
    }

    public static final GraphServiceAsset A02(AnonymousClass1XY r3) {
        if (A06 == null) {
            synchronized (GraphServiceAsset.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r3);
                if (A002 != null) {
                    try {
                        Context A022 = AnonymousClass1YA.A02(r3.getApplicationInjector());
                        A022.getAssets();
                        A06 = C05850aR.A00(A022);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static final GraphQLServiceFactory A03(AnonymousClass1XY r3) {
        if (A07 == null) {
            synchronized (GraphQLServiceFactory.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r3);
                if (A002 != null) {
                    try {
                        A02(r3.getApplicationInjector());
                        A07 = C05850aR.A01();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    /* JADX WARN: Type inference failed for: r0v12, types: [com.facebook.graphservice.GraphQLConsistencyDecorator] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.facebook.graphservice.interfaces.GraphQLConsistency A04(X.AnonymousClass1XY r5) {
        /*
            java.lang.Object r4 = X.C27011cV.A04
            monitor-enter(r4)
            X.0Zi r0 = X.C27011cV.A02     // Catch:{ all -> 0x0041 }
            X.0Zi r0 = X.C05540Zi.A00(r0)     // Catch:{ all -> 0x0041 }
            X.C27011cV.A02 = r0     // Catch:{ all -> 0x0041 }
            boolean r0 = r0.A03(r5)     // Catch:{ all -> 0x003a }
            if (r0 == 0) goto L_0x002f
            X.0Zi r0 = X.C27011cV.A02     // Catch:{ all -> 0x003a }
            X.1Xi r0 = r0.A01()     // Catch:{ all -> 0x003a }
            X.1XY r0 = (X.AnonymousClass1XY) r0     // Catch:{ all -> 0x003a }
            X.0Zi r3 = X.C27011cV.A02     // Catch:{ all -> 0x003a }
            com.facebook.graphservice.GraphQLConsistencyJNI r2 = A00(r0)     // Catch:{ all -> 0x003a }
            X.1XU r1 = X.C25231Yv.A00     // Catch:{ all -> 0x003a }
            r0 = 0
            if (r1 == 0) goto L_0x0025
            r0 = 1
        L_0x0025:
            if (r0 == 0) goto L_0x002d
            com.facebook.graphservice.GraphQLConsistencyDecorator r0 = new com.facebook.graphservice.GraphQLConsistencyDecorator     // Catch:{ all -> 0x003a }
            r0.<init>(r2)     // Catch:{ all -> 0x003a }
            r2 = r0
        L_0x002d:
            r3.A00 = r2     // Catch:{ all -> 0x003a }
        L_0x002f:
            X.0Zi r1 = X.C27011cV.A02     // Catch:{ all -> 0x003a }
            java.lang.Object r0 = r1.A00     // Catch:{ all -> 0x003a }
            com.facebook.graphservice.interfaces.GraphQLConsistency r0 = (com.facebook.graphservice.interfaces.GraphQLConsistency) r0     // Catch:{ all -> 0x003a }
            r1.A02()     // Catch:{ all -> 0x0041 }
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            return r0
        L_0x003a:
            r1 = move-exception
            X.0Zi r0 = X.C27011cV.A02     // Catch:{ all -> 0x0041 }
            r0.A02()     // Catch:{ all -> 0x0041 }
            throw r1     // Catch:{ all -> 0x0041 }
        L_0x0041:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27011cV.A04(X.1XY):com.facebook.graphservice.interfaces.GraphQLConsistency");
    }

    public static final GraphQLService A05(AnonymousClass1XY r5) {
        GraphQLService graphQLService;
        synchronized (GraphQLService.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r5)) {
                    C05540Zi r3 = A03;
                    GraphQLService A012 = A01((AnonymousClass1XY) A03.A01());
                    Preconditions.checkNotNull(A012, "Got null GraphQLServiceJNI in GraphQLService provider");
                    boolean z = false;
                    if (C25231Yv.A00 != null) {
                        z = true;
                    }
                    if (z) {
                        A012 = new GraphQLServiceDecorator(A012);
                    }
                    r3.A00 = A012;
                }
                C05540Zi r1 = A03;
                graphQLService = (GraphQLService) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return graphQLService;
    }

    public static final TreeJsonSerializer A07(AnonymousClass1XY r5) {
        if (A08 == null) {
            synchronized (TreeJsonSerializer.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r5);
                if (A002 != null) {
                    try {
                        A02(r5.getApplicationInjector());
                        if (C05850aR.A01 == null) {
                            GraphQLServiceFactory A012 = C05850aR.A01();
                            synchronized (TreeJsonSerializer.class) {
                                if (C05850aR.A01 == null) {
                                    C05850aR.A01 = A012.newTreeJsonSerializer();
                                }
                            }
                        }
                        A08 = C05850aR.A01;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static final TreeSerializer A08(AnonymousClass1XY r3) {
        if (A09 == null) {
            synchronized (TreeSerializer.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r3);
                if (A002 != null) {
                    try {
                        A02(r3.getApplicationInjector());
                        A09 = C05850aR.A03();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static final ScheduledExecutorService A09(AnonymousClass1XY r5) {
        if (A0A == null) {
            synchronized (A05) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r5);
                if (A002 != null) {
                    try {
                        A0A = AnonymousClass0VM.A00(r5.getApplicationInjector()).A03(AnonymousClass0VS.FOREGROUND, "GraphServiceParsing");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static final TreeJsonSerializer A06(AnonymousClass1XY r0) {
        return A07(r0);
    }
}
