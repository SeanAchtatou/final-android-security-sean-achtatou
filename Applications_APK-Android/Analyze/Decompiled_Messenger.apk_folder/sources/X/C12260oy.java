package X;

import android.content.Context;
import android.os.Bundle;

/* renamed from: X.0oy  reason: invalid class name and case insensitive filesystem */
public abstract class C12260oy implements Cloneable {
    public C131486Ds A00;
    private boolean A01;
    public final String A02;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.util.Map} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.util.Map} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.util.Map} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: java.util.Map} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.util.Map} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v8, resolved type: java.util.Map} */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x000e, code lost:
        if (r2.A01 == false) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0010, code lost:
        if (r1 == null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0012, code lost:
        r1 = r1.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        r2.A01 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001a, code lost:
        if (r1 == null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001c, code lost:
        r1 = r1.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0020, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0021, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0022, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x002c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000b, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A01(java.lang.String r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.A01     // Catch:{ all -> 0x002d }
            r1 = 0
            if (r0 != 0) goto L_0x0025
            X.6Ds r0 = r2.A00     // Catch:{ all -> 0x002d }
            if (r0 == 0) goto L_0x0025
            monitor-exit(r2)     // Catch:{ all -> 0x002d }
            monitor-enter(r2)
            boolean r0 = r2.A01     // Catch:{ all -> 0x0022 }
            if (r0 == 0) goto L_0x0017
            if (r1 == 0) goto L_0x0020
            java.lang.Object r1 = r1.get(r3)     // Catch:{ all -> 0x0022 }
            goto L_0x0020
        L_0x0017:
            r0 = 1
            r2.A01 = r0     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x0020
            java.lang.Object r1 = r1.get(r3)     // Catch:{ all -> 0x0022 }
        L_0x0020:
            monitor-exit(r2)     // Catch:{ all -> 0x0022 }
            return r1
        L_0x0022:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0022 }
            goto L_0x002f
        L_0x0025:
            if (r1 == 0) goto L_0x002b
            java.lang.Object r1 = r1.get(r3)     // Catch:{ all -> 0x002d }
        L_0x002b:
            monitor-exit(r2)     // Catch:{ all -> 0x002d }
            return r1
        L_0x002d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x002d }
        L_0x002f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12260oy.A01(java.lang.String):java.lang.Object");
    }

    public abstract C73173fc A02(Context context);

    public abstract C12260oy A03(C131486Ds r1, Bundle bundle);

    public C12260oy(String str) {
        this.A02 = str;
    }
}
