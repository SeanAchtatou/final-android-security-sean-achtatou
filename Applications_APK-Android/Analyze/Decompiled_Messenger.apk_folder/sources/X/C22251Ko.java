package X;

import com.facebook.common.callercontext.CallerContextable;

/* renamed from: X.1Ko  reason: invalid class name and case insensitive filesystem */
public final class C22251Ko implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippetParser";
    public AnonymousClass0UN A00;

    public static final C22251Ko A00(AnonymousClass1XY r1) {
        return new C22251Ko(r1);
    }

    public C22251Ko(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
