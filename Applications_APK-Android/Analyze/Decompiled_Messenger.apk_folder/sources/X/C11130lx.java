package X;

import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.service.model.FetchMessageAfterTimestampParams;
import com.facebook.messaging.service.model.FetchMessageResult;
import com.facebook.messaging.service.model.FetchThreadParams;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0lx  reason: invalid class name and case insensitive filesystem */
public final class C11130lx extends C11150lz {
    public AnonymousClass0UN A00;
    public final AnonymousClass0US A01;
    public final AnonymousClass0m8 A02;
    public final AnonymousClass0m6 A03;
    public final C11230mQ A04;
    @LoggedInUser
    public final C04310Tq A05;
    public final C04310Tq A06;

    public static OperationResult A00(C11130lx r12, C11060ln r13, C27311cz r14) {
        MessagesCollection messagesCollection;
        FetchThreadParams fetchThreadParams = (FetchThreadParams) AnonymousClass0ls.A00(r13, "fetchThreadParams");
        boolean A012 = A01(r12);
        try {
            OperationResult BAz = r14.BAz(r13);
            FetchThreadResult fetchThreadResult = (FetchThreadResult) BAz.A08();
            DataFetchDisposition dataFetchDisposition = fetchThreadResult.A02;
            if (dataFetchDisposition != null && dataFetchDisposition.A08) {
                ThreadSummary threadSummary = fetchThreadResult.A05;
                boolean z = false;
                if (threadSummary != null) {
                    z = true;
                }
                ThreadSummary threadSummary2 = null;
                if (z) {
                    threadSummary2 = r12.A03.A0R(threadSummary.A0S);
                }
                if (z && threadSummary2 != null) {
                    long j = threadSummary.A09;
                    if (j != -1 && j < threadSummary2.A09) {
                    }
                }
                if (threadSummary != null) {
                    ThreadKey threadKey = threadSummary.A0S;
                    if (ThreadKey.A0E(threadKey) && !AnonymousClass1Gw.A00(threadKey) && (messagesCollection = fetchThreadResult.A03) != null && messagesCollection.A08()) {
                        ImmutableList immutableList = fetchThreadResult.A07;
                        if (immutableList != null) {
                            ((C14300t0) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AxE, r12.A00)).A06(immutableList);
                        }
                    }
                }
                ((C14800u9) r12.A01.get()).A03(fetchThreadParams.A01, fetchThreadResult);
            }
            return BAz;
        } finally {
            if (A012) {
                ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r12.A00)).A03();
            }
        }
    }

    public static boolean A01(C11130lx r3) {
        if (r3.A02 != AnonymousClass0m8.FACEBOOK) {
            return false;
        }
        ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r3.A00)).A02();
        return true;
    }

    public OperationResult A0z(C11060ln r9, C27311cz r10) {
        MessagesCollection A0P;
        FetchMessageAfterTimestampParams fetchMessageAfterTimestampParams = (FetchMessageAfterTimestampParams) r9.A00.getParcelable("fetchMessageAfterTimestampParams");
        ThreadKey threadKey = fetchMessageAfterTimestampParams.A01;
        long j = fetchMessageAfterTimestampParams.A00;
        C11230mQ r0 = this.A04;
        if (threadKey == null) {
            A0P = null;
        } else {
            A0P = r0.A01.A0P(threadKey);
        }
        if (A0P != null && A0P.A04() >= 2 && j < A0P.A07(0).A03 && j >= A0P.A07(A0P.A04() - 1).A03) {
            for (int i = 1; i < A0P.A04(); i++) {
                if (A0P.A07(i).A03 == j) {
                    return OperationResult.A04(new FetchMessageResult(AnonymousClass102.FROM_CACHE_UP_TO_DATE, A0P.A07(i - 1), 0));
                }
            }
            C000300h.A00();
        }
        return super.A0z(r9, r10);
    }

    public C11130lx(AnonymousClass1XY r3, String str, AnonymousClass0m6 r5, C11230mQ r6, AnonymousClass0US r7) {
        super(str);
        this.A00 = new AnonymousClass0UN(13, r3);
        this.A05 = AnonymousClass0WY.A01(r3);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.AXk, r3);
        this.A03 = r5;
        this.A02 = r5.A09;
        this.A04 = r6;
        this.A01 = r7;
    }
}
