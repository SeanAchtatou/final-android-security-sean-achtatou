package X;

/* renamed from: X.13c  reason: invalid class name */
public final class AnonymousClass13c implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.util.AnalyticsConnectionUtils$1";
    public final /* synthetic */ AnonymousClass13b A00;

    public AnonymousClass13c(AnonymousClass13b r1) {
        this.A00 = r1;
    }

    public void run() {
        C005505z.A03("AnalyticsConnectionUtils#readCurrentTrafficStats", 785690806);
        try {
            AnonymousClass13b.A02(this.A00);
        } finally {
            C005505z.A00(685980304);
        }
    }
}
