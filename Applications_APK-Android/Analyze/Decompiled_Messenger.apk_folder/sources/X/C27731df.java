package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import androidx.fragment.app.Fragment;

/* renamed from: X.1df  reason: invalid class name and case insensitive filesystem */
public final class C27731df implements LayoutInflater.Factory2 {
    private final C13060qW A00;

    public C27731df(C13060qW r1) {
        this.A00 = r1;
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z;
        int i;
        String str2 = str;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        if (C29031fl.class.getName().equals(str2)) {
            return new C29031fl(context2, attributeSet2, this.A00);
        }
        Fragment fragment = null;
        if ("fragment".equals(str2)) {
            String attributeValue = attributeSet2.getAttributeValue(null, "class");
            TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet2, AnonymousClass3D3.A00);
            if (attributeValue == null) {
                attributeValue = obtainStyledAttributes.getString(0);
            }
            int resourceId = obtainStyledAttributes.getResourceId(1, -1);
            String string = obtainStyledAttributes.getString(2);
            obtainStyledAttributes.recycle();
            if (attributeValue != null) {
                ClassLoader classLoader = context2.getClassLoader();
                try {
                    Class<?> cls = (Class) C27801dm.A00.get(attributeValue);
                    if (cls == null) {
                        cls = Class.forName(attributeValue, false, classLoader);
                        C27801dm.A00.put(attributeValue, cls);
                    }
                    z = Fragment.class.isAssignableFrom(cls);
                } catch (ClassNotFoundException unused) {
                    z = false;
                }
                if (z) {
                    if (view != null) {
                        i = view.getId();
                    } else {
                        i = 0;
                    }
                    if (i == -1 && resourceId == -1 && string == null) {
                        throw new IllegalArgumentException(AnonymousClass08S.A0P(attributeSet2.getPositionDescription(), ": Must specify unique android:id, android:tag, or have a parent with an id for ", attributeValue));
                    }
                    if (resourceId != -1) {
                        fragment = this.A00.A0O(resourceId);
                    }
                    if (fragment == null && string != null) {
                        fragment = this.A00.A0Q(string);
                    }
                    if (fragment == null && i != -1) {
                        fragment = this.A00.A0O(i);
                    }
                    C13060qW.A0I(2);
                    if (fragment == null) {
                        fragment = this.A00.A0S().A01(context2.getClassLoader(), attributeValue);
                        fragment.A0Z = true;
                        int i2 = i;
                        if (resourceId != 0) {
                            i2 = resourceId;
                        }
                        fragment.A0E = i2;
                        fragment.A0D = i;
                        fragment.A0T = string;
                        fragment.A0d = true;
                        C13060qW r0 = this.A00;
                        fragment.A0P = r0;
                        C13020qR r02 = r0.A06;
                        fragment.A0N = r02;
                        fragment.A1K(r02.A01, attributeSet2, fragment.A01);
                        this.A00.A0h(fragment);
                        C13060qW r1 = this.A00;
                        r1.A0s(fragment, r1.A01);
                    } else if (!fragment.A0d) {
                        fragment.A0d = true;
                        C13020qR r03 = this.A00.A06;
                        fragment.A0N = r03;
                        fragment.A1K(r03.A01, attributeSet2, fragment.A01);
                    } else {
                        throw new IllegalArgumentException(AnonymousClass08S.A0W(attributeSet2.getPositionDescription(), ": Duplicate id 0x", Integer.toHexString(resourceId), ", tag ", string, ", or parent id 0x", Integer.toHexString(i), " with another fragment for ", attributeValue));
                    }
                    C13060qW r5 = this.A00;
                    int i3 = r5.A01;
                    if (i3 >= 1 || !fragment.A0Z) {
                        r5.A0s(fragment, i3);
                    } else {
                        r5.A0s(fragment, 1);
                    }
                    View view2 = fragment.A0I;
                    if (view2 != null) {
                        if (resourceId != 0) {
                            view2.setId(resourceId);
                        }
                        if (fragment.A0I.getTag() == null) {
                            fragment.A0I.setTag(string);
                        }
                        return fragment.A0I;
                    }
                    throw new IllegalStateException(AnonymousClass08S.A0P("Fragment ", attributeValue, " did not create a view."));
                }
            }
        }
        return null;
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }
}
