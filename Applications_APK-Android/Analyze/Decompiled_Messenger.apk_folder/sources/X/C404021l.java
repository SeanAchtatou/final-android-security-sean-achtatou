package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.21l  reason: invalid class name and case insensitive filesystem */
public final class C404021l extends C403921k {
    private static C04470Uu A01;
    private AnonymousClass0UN A00;

    public static final C404021l A00(AnonymousClass1XY r4) {
        C404021l r0;
        synchronized (C404021l.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C404021l((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C404021l) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A01(android.content.Intent r7, android.content.Context r8) {
        /*
            r6 = this;
            int r1 = X.AnonymousClass1Y3.Akd
            X.0UN r0 = r6.A00
            r4 = 1
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.4QA r5 = (X.AnonymousClass4QA) r5
            if (r7 == 0) goto L_0x0038
            if (r8 == 0) goto L_0x0038
            int r1 = X.AnonymousClass1Y3.A5j
            X.0UN r0 = r5.A00
            r2 = 4
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0aD r1 = (X.C05720aD) r1
            java.lang.String r0 = "url_interstitial"
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0047
            int r1 = X.AnonymousClass1Y3.A5j
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0aD r1 = (X.C05720aD) r1
            r0 = 499(0x1f3, float:6.99E-43)
            java.lang.String r0 = X.C05360Yq.$const$string(r0)
            boolean r0 = r1.A07(r0)
            if (r0 != 0) goto L_0x0047
        L_0x0038:
            r0 = 0
        L_0x0039:
            r2 = 0
            if (r0 == 0) goto L_0x0046
            int r1 = X.AnonymousClass1Y3.Aqf
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.En2 r0 = (X.C29989En2) r0
        L_0x0046:
            return r2
        L_0x0047:
            int r1 = X.AnonymousClass1Y3.A5j
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0aD r1 = (X.C05720aD) r1
            java.lang.String r0 = "photo_dialtone"
            boolean r0 = r1.A07(r0)
            if (r0 != 0) goto L_0x0038
            if (r7 != 0) goto L_0x00b6
            r1 = 0
        L_0x005c:
            boolean r0 = X.C06850cB.A0A(r1)
            if (r0 == 0) goto L_0x0082
            java.lang.String r0 = r7.getDataString()
            boolean r0 = X.C06850cB.A0A(r0)
            if (r0 != 0) goto L_0x0082
            r2 = 3
            int r1 = X.AnonymousClass1Y3.BEA
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.8s7 r1 = (X.AnonymousClass8s7) r1
            java.lang.String r0 = r7.getDataString()
            android.content.Intent r1 = r1.A02(r8, r0)
            if (r1 != 0) goto L_0x00af
            r1 = 0
        L_0x0082:
            boolean r0 = X.C06850cB.A0B(r1)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0038
            java.lang.String r3 = "tracking_codes"
            java.lang.String r0 = r7.getStringExtra(r3)
            r2 = 0
            if (r0 == 0) goto L_0x00f1
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 845026226274407(0x3008c000b0067, double:4.17498428237056E-309)
            java.lang.String r1 = r2.B4C(r0)
            java.lang.String r0 = "disable"
            boolean r0 = X.C06850cB.A0C(r1, r0)
            if (r0 == 0) goto L_0x00f1
            goto L_0x00bd
        L_0x00af:
            java.lang.String r0 = "extra_instant_articles_id"
            java.lang.String r1 = r1.getStringExtra(r0)
            goto L_0x0082
        L_0x00b6:
            java.lang.String r0 = "extra_instant_articles_id"
            java.lang.String r1 = r7.getStringExtra(r0)
            goto L_0x005c
        L_0x00bd:
            int r2 = X.AnonymousClass1Y3.Axm     // Catch:{ IOException -> 0x00d2 }
            X.0UN r1 = r5.A00     // Catch:{ IOException -> 0x00d2 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ IOException -> 0x00d2 }
            X.0jE r1 = (X.AnonymousClass0jE) r1     // Catch:{ IOException -> 0x00d2 }
            java.lang.String r0 = r7.getStringExtra(r3)     // Catch:{ IOException -> 0x00d2 }
            com.fasterxml.jackson.databind.JsonNode r0 = r1.readTree(r0)     // Catch:{ IOException -> 0x00d2 }
            if (r0 == 0) goto L_0x00f1
            goto L_0x00f3
        L_0x00d2:
            r3 = move-exception
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "InstantArticlesIntentChecker"
            java.lang.String r0 = "Failed fetching tracking codes."
            X.06G r0 = X.AnonymousClass06F.A02(r1, r0)
            r0.A03 = r3
            X.06F r0 = r0.A00()
            r2.CGQ(r0)
            r0 = 0
            goto L_0x012a
        L_0x00f1:
            r0 = 0
            goto L_0x012a
        L_0x00f3:
            boolean r0 = X.AnonymousClass8F2.A01(r0)
            if (r0 == 0) goto L_0x00f1
            r3 = 0
            if (r7 == 0) goto L_0x0120
            android.net.Uri r0 = r7.getData()
            if (r0 == 0) goto L_0x0120
            android.net.Uri r1 = r7.getData()
            r0 = 52
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            java.lang.String r2 = r1.getQueryParameter(r0)
            if (r2 == 0) goto L_0x012e
            int r1 = X.CoM.A00(r2)
        L_0x0116:
            if (r2 == 0) goto L_0x0120
            r0 = 2
            if (r1 <= r0) goto L_0x0120
            int r1 = r1 - r4
            java.lang.String r3 = r2.substring(r4, r1)
        L_0x0120:
            if (r3 == 0) goto L_0x0129
            android.net.Uri r0 = android.net.Uri.parse(r3)
            r7.setData(r0)
        L_0x0129:
            r0 = 1
        L_0x012a:
            r0 = r0 ^ 1
            goto L_0x0039
        L_0x012e:
            r1 = 0
            goto L_0x0116
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C404021l.A01(android.content.Intent, android.content.Context):boolean");
    }

    private C404021l(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public boolean A09(Intent intent, int i, Activity activity) {
        return A01(intent, activity);
    }

    public boolean A0A(Intent intent, int i, Fragment fragment) {
        return A01(intent, fragment.A1j());
    }

    public boolean A0B(Intent intent, Context context) {
        return A01(intent, context);
    }
}
