package X;

import android.content.Intent;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.common.util.JSONUtil;
import com.facebook.http.protocol.ApiErrorResult;
import com.facebook.proxygen.TraceFieldType;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Preconditions;
import java.io.IOException;
import javax.inject.Singleton;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;

@Singleton
/* renamed from: X.200  reason: invalid class name */
public final class AnonymousClass200 {
    private static volatile AnonymousClass200 A04;
    public AnonymousClass0UN A00;
    public C04310Tq A01;
    private C04460Ut A02;
    public final AnonymousClass0jJ A03;

    public static final AnonymousClass200 A00(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (AnonymousClass200.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        AnonymousClass0jJ A042 = C05040Xk.A04();
                        C04460Ut A022 = C04430Uq.A02(applicationInjector);
                        C04310Tq A012 = AnonymousClass0WY.A01(applicationInjector);
                        AnonymousClass0WT.A00(applicationInjector);
                        A04 = new AnonymousClass200(applicationInjector, A042, A022, A012);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private JsonNode A01(JsonNode jsonNode) {
        String A0N = JSONUtil.A0N(jsonNode.path("error_data"));
        if (A0N != null) {
            try {
                return this.A03.readTree(A0N);
            } catch (IOException unused) {
            }
        }
        return null;
    }

    public static void A02(AnonymousClass200 r4, C48372aK r5) {
        int i = r5.A01;
        if (i >= 300) {
            if (r5.A00 == null) {
                r5.A00 = r5.A00();
            }
            String str = r5.A00;
            if (i >= 400) {
                r4.A06(str);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(r5.A02);
            if (str != null) {
                sb.append(10);
                sb.append(str);
            }
            throw new HttpResponseException(i, sb.toString());
        }
    }

    public static void A03(AnonymousClass200 r14, JsonNode jsonNode) {
        int i;
        int i2;
        String str;
        String str2;
        String str3;
        String str4;
        C30367Eut eut;
        int i3;
        JsonNode path;
        if (jsonNode == null) {
            return;
        }
        if (jsonNode.isObject()) {
            boolean z = false;
            Throwable th = null;
            if (jsonNode.has(TraceFieldType.ErrorCode)) {
                JSONUtil.A04(jsonNode.path(TraceFieldType.ErrorCode));
                if (jsonNode.has("error_subcode")) {
                    JSONUtil.A04(jsonNode.path("error_subcode"));
                }
                JsonNode A012 = r14.A01(jsonNode);
                if (A012 == null || !A012.has("error_message")) {
                    path = jsonNode.path("error_msg");
                } else {
                    path = A012.get("error_message");
                }
                AnonymousClass7VF A002 = ApiErrorResult.A00(JSONUtil.A04(jsonNode.path(TraceFieldType.ErrorCode)), JSONUtil.A0N(path));
                A002.A03 = JSONUtil.A0N(jsonNode.path("error_data"));
                A002.A09 = jsonNode.toString();
                AnonymousClass7VC r0 = AnonymousClass7VC.API_EC_DOMAIN;
                if (r0 != null) {
                    A002.A02 = r0;
                    th = new C37741wB(A002.A00());
                }
                throw new NullPointerException("errorDomain cannot be null");
            } else if (jsonNode.has("error") && jsonNode.path("error").isInt()) {
                AnonymousClass7VF A003 = ApiErrorResult.A00(JSONUtil.A04(jsonNode.path("error")), JSONUtil.A0N(jsonNode.path(TraceFieldType.Error)));
                A003.A09 = jsonNode.toString();
                AnonymousClass7VC r02 = AnonymousClass7VC.API_EC_DOMAIN;
                if (r02 != null) {
                    A003.A02 = r02;
                    th = new C37741wB(A003.A00());
                }
                throw new NullPointerException("errorDomain cannot be null");
            } else if (jsonNode.has("error") && jsonNode.path("error").isObject()) {
                JsonNode path2 = jsonNode.path("error");
                if (path2.has("code") && path2.has("description")) {
                    int A042 = JSONUtil.A04(path2.path("code"));
                    AnonymousClass7VF A004 = ApiErrorResult.A00(A042, JSONUtil.A0N(path2.path("description")));
                    A004.A09 = jsonNode.toString();
                    AnonymousClass7VC r03 = AnonymousClass7VC.GRAPHQL_KERROR_DOMAIN;
                    if (r03 != null) {
                        A004.A02 = r03;
                        ApiErrorResult A005 = A004.A00();
                        if (A042 != 102) {
                            if (A042 == 190) {
                                if (path2.has("error_subcode")) {
                                    i3 = JSONUtil.A04(path2.path("error_subcode"));
                                } else {
                                    i3 = 0;
                                }
                                if (i3 == 490) {
                                    r14.A04(r14.A01(path2));
                                } else if (i3 == 491) {
                                    C30367Eut eut2 = (C30367Eut) AnonymousClass1XX.A03(AnonymousClass1Y3.BCn, r14.A00);
                                    if (eut2 != null) {
                                        eut2.handleNeedReauthNow();
                                    }
                                }
                            } else if (A042 == 1675007) {
                                th = new C71343cE(A005);
                            } else if (A042 == 1675013) {
                                th = new C71353cF(A005);
                            }
                            th = new C37741wB(A005);
                        }
                        th = new C198849Xh(A005);
                    }
                    throw new NullPointerException("errorDomain cannot be null");
                } else if (path2.has("message")) {
                    if (path2.has("code")) {
                        i = JSONUtil.A04(path2.path("code"));
                    } else {
                        i = 0;
                    }
                    if (path2.has("error_subcode")) {
                        i2 = JSONUtil.A04(path2.path("error_subcode"));
                    } else {
                        i2 = 0;
                    }
                    if (path2.has("error_user_title")) {
                        str = JSONUtil.A0N(path2.path("error_user_title"));
                    } else {
                        str = null;
                    }
                    if (path2.has("error_user_msg")) {
                        str2 = JSONUtil.A0N(path2.path("error_user_msg"));
                    } else {
                        str2 = null;
                    }
                    if (path2.has("fbtrace_id")) {
                        str3 = JSONUtil.A0N(path2.path("fbtrace_id"));
                    } else {
                        str3 = null;
                    }
                    if (path2.has("www_request_id")) {
                        str4 = JSONUtil.A0N(path2.path("www_request_id"));
                    } else {
                        str4 = null;
                    }
                    if (i == 190) {
                        C04310Tq r04 = r14.A01;
                        if (!(r04 == null || r04.get() == null)) {
                            r14.A01.get();
                        }
                        if (i2 == 490) {
                            r14.A04(r14.A01(path2));
                            z = true;
                        } else if (i2 == 491 && (eut = (C30367Eut) AnonymousClass1XX.A03(AnonymousClass1Y3.BCn, r14.A00)) != null) {
                            eut.handleNeedReauthNow();
                        }
                        if (AnonymousClass9BT.A00() || !z) {
                            r14.A05();
                        }
                    }
                    if (!z) {
                        AnonymousClass7VF A006 = ApiErrorResult.A00(i, JSONUtil.A0N(path2.path("message")));
                        A006.A01 = i2;
                        A006.A03 = path2.path("error_data").toString();
                        A006.A06 = str;
                        A006.A05 = str2;
                        A006.A08 = str3;
                        A006.A07 = str4;
                        A006.A09 = jsonNode.toString();
                        AnonymousClass7VC r05 = AnonymousClass7VC.API_EC_DOMAIN;
                        if (r05 != null) {
                            A006.A02 = r05;
                            th = new C37741wB(A006.A00());
                        }
                        throw new NullPointerException("errorDomain cannot be null");
                    }
                }
            }
            if (th != null) {
                throw th;
            }
        } else if (!jsonNode.isArray()) {
            throw new C37701w6("Response was neither an array or a dictionary");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
        if (com.facebook.common.util.JSONUtil.A0S(r11.path(r4)) == false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A04(com.fasterxml.jackson.databind.JsonNode r11) {
        /*
            r10 = this;
            java.lang.String r7 = ""
            r0 = 1273(0x4f9, float:1.784E-42)
            java.lang.String r6 = X.C05360Yq.$const$string(r0)
            if (r11 != 0) goto L_0x005f
            r8 = r7
        L_0x000b:
            r0 = 1272(0x4f8, float:1.782E-42)
            java.lang.String r5 = X.C05360Yq.$const$string(r0)
            if (r11 == 0) goto L_0x001b
            com.fasterxml.jackson.databind.JsonNode r0 = r11.path(r5)
            java.lang.String r7 = com.facebook.common.util.JSONUtil.A0N(r0)
        L_0x001b:
            r0 = 2290(0x8f2, float:3.209E-42)
            java.lang.String r4 = X.C05360Yq.$const$string(r0)
            r9 = 0
            r2 = 1
            if (r11 == 0) goto L_0x0030
            com.fasterxml.jackson.databind.JsonNode r0 = r11.path(r4)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r3 = 1
            if (r0 != 0) goto L_0x0031
        L_0x0030:
            r3 = 0
        L_0x0031:
            int r1 = X.AnonymousClass1Y3.A0m
            X.0UN r0 = r10.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.4Kg r0 = (X.C88414Kg) r0
            r0.A01(r8, r2, r2)
            X.0Ut r2 = r10.A02
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            r0 = 1294(0x50e, float:1.813E-42)
            java.lang.String r0 = X.C05360Yq.$const$string(r0)
            android.content.Intent r0 = r1.setAction(r0)
            android.content.Intent r0 = r0.putExtra(r6, r8)
            android.content.Intent r0 = r0.putExtra(r5, r7)
            android.content.Intent r0 = r0.putExtra(r4, r3)
            r2.C4x(r0)
            return
        L_0x005f:
            com.fasterxml.jackson.databind.JsonNode r0 = r11.path(r6)
            java.lang.String r8 = com.facebook.common.util.JSONUtil.A0N(r0)
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass200.A04(com.fasterxml.jackson.databind.JsonNode):void");
    }

    public void A05() {
        int i = AnonymousClass1Y3.AXA;
        if (((AnonymousClass0XN) AnonymousClass1XX.A02(1, i, this.A00)).A08() != null && ((AnonymousClass0XN) AnonymousClass1XX.A02(1, i, this.A00)).A08().mUserId != null) {
            this.A02.C4x(new Intent().putExtra("arg_uid", ((AnonymousClass0XN) AnonymousClass1XX.A02(1, i, this.A00)).A08().mUserId).setAction("com.facebook.http.protocol.AUTH_TOKEN_EXCEPTION"));
        }
    }

    private AnonymousClass200(AnonymousClass1XY r3, AnonymousClass0jJ r4, C04460Ut r5, @LoggedInUser C04310Tq r6) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A03 = r4;
        this.A02 = r5;
        this.A01 = r6;
    }

    public void A06(String str) {
        if (!C06850cB.A0B(str)) {
            try {
                A03(this, this.A03.readTree(str));
            } catch (C37741wB e) {
                throw e;
            } catch (C37571vt | IOException unused) {
            }
        }
    }

    public void A07(HttpResponse httpResponse) {
        Preconditions.checkNotNull(httpResponse);
        StatusLine statusLine = httpResponse.getStatusLine();
        A02(this, new C48362aJ(statusLine.getStatusCode(), statusLine.getReasonPhrase(), httpResponse.getEntity()));
    }
}
