package X;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;

/* renamed from: X.0yr  reason: invalid class name and case insensitive filesystem */
public class C17410yr implements AnonymousClass165 {
    public void BNK(Fragment fragment, int i, int i2, Intent intent) {
    }

    public void BNh(Fragment fragment, Bundle bundle) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r2 = ((AnonymousClass1JD) this).A00;
            r2.A01 = fragment.A0b;
            r2.A04 = fragment.A0i;
            boolean z = false;
            if (fragment.A0I != null) {
                z = true;
            }
            r2.A00 = z;
        }
    }

    public void BNp(Fragment fragment, boolean z) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r0 = ((AnonymousClass1JD) this).A00;
            r0.A01 = z;
            C13990sQ.A00(r0);
        }
    }

    public void BNx(Fragment fragment, View view, Bundle bundle) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r1 = ((AnonymousClass1JD) this).A00;
            r1.A00 = true;
            C13990sQ.A00(r1);
        }
    }

    public void BNy(Fragment fragment) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r1 = ((AnonymousClass1JD) this).A00;
            r1.A00 = false;
            C13990sQ.A00(r1);
        } else if (this instanceof C188015d) {
            ((C188015d) this).A00.A07();
        }
    }

    public void BOY(Fragment fragment, Fragment fragment2) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r1 = ((AnonymousClass1JD) this).A00;
            if (fragment2 instanceof C13250qz) {
                ((C13250qz) fragment2).CAE(r1.A05);
            }
        }
    }

    public void BP9(C138056cU r1) {
    }

    public void BPa(Fragment fragment, Bundle bundle) {
    }

    public void BPf(Bundle bundle) {
    }

    public void BTL(Fragment fragment, Configuration configuration) {
    }

    public void BVy(Fragment fragment) {
        if (this instanceof C188015d) {
            ((C188015d) this).A00.A06();
        }
    }

    public void Bi0(Fragment fragment) {
        if (this instanceof C188015d) {
            ((C188015d) this).A00.A08();
        }
    }

    public void BmO(Fragment fragment) {
        boolean z = this instanceof C188015d;
    }

    public void Bmy(Fragment fragment, Bundle bundle) {
    }

    public void BoL(Fragment fragment, boolean z) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r0 = ((AnonymousClass1JD) this).A00;
            r0.A04 = z;
            C13990sQ.A00(r0);
        }
    }

    public void BpQ(Fragment fragment) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r1 = ((AnonymousClass1JD) this).A00;
            r1.A03 = true;
            C13990sQ.A00(r1);
        }
    }

    public void Bq5(Fragment fragment) {
        if (this instanceof AnonymousClass1JD) {
            C13990sQ r1 = ((AnonymousClass1JD) this).A00;
            r1.A03 = false;
            C13990sQ.A00(r1);
        }
    }
}
