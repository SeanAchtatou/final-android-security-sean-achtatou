package X;

import android.content.Context;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1b6  reason: invalid class name and case insensitive filesystem */
public final class C26221b6 extends C26231b7 implements AnonymousClass0ZM {
    private static volatile C26221b6 A01;
    private final C05760aH A00;

    public static final C26221b6 A00(AnonymousClass1XY r7) {
        if (A01 == null) {
            synchronized (C26221b6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A01 = new C26221b6(AnonymousClass1YA.A00(applicationInjector), AnonymousClass0ZS.A00(applicationInjector), AnonymousClass1ZC.A01(applicationInjector), FbSharedPreferencesModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A04() {
        String locale = this.A01.A06().toString();
        if (!C06850cB.A0B(locale)) {
            this.A00.A0A(C007006q.A01(locale));
            A03(locale);
        }
    }

    private C26221b6(Context context, AnonymousClass0ZS r3, C05760aH r4, FbSharedPreferences fbSharedPreferences) {
        super(context, r3);
        this.A00 = r4;
        fbSharedPreferences.C0f(C26241b8.A00, this);
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r2) {
        A04();
    }
}
