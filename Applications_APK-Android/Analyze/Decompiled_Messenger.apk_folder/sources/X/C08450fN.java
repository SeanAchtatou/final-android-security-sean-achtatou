package X;

import android.os.Looper;
import android.util.SparseIntArray;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.stall.contframes.ContiguousFramesTracker;
import com.google.common.collect.ImmutableList;
import java.nio.IntBuffer;
import java.util.Collection;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fN  reason: invalid class name and case insensitive filesystem */
public final class C08450fN implements C08470fP {
    public static final Thread A0A = Looper.getMainLooper().getThread();
    private static volatile C08450fN A0B;
    public AnonymousClass0UN A00;
    public Boolean A01;
    public boolean A02;
    private C09020gN A03;
    private boolean A04;
    private boolean A05;
    private boolean A06;
    public final SparseIntArray A07 = new SparseIntArray();
    public final C08430fL A08;
    private final C08440fM A09;

    private C09020gN A00() {
        if (this.A03 == null) {
            int i = AnonymousClass1Y3.AyI;
            AnonymousClass0UN r2 = this.A00;
            C27031cX r3 = (C27031cX) AnonymousClass1XX.A02(0, i, r2);
            if (this.A01 == null) {
                this.A01 = Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r2)).Aem(283265978272010L));
            }
            C09020gN A002 = r3.A00(Boolean.valueOf(this.A01.booleanValue()));
            this.A03 = A002;
            A002.A01 = this;
        }
        return this.A03;
    }

    public static final C08450fN A01(AnonymousClass1XY r4) {
        if (A0B == null) {
            synchronized (C08450fN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0B, r4);
                if (A002 != null) {
                    try {
                        A0B = new C08450fN(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0B;
    }

    public static void A02(C08450fN r4, int i) {
        if (r4.A07.size() == 0) {
            if (r4.A04) {
                C179938Ui r3 = (C179938Ui) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B8r, r4.A00);
                r3.A02 = true;
                if (!r3.A03) {
                    ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, r3.A00)).BxR(new C179958Uk(r3));
                }
            }
            if (r4.A05) {
                ContiguousFramesTracker contiguousFramesTracker = (ContiguousFramesTracker) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Axe, r4.A00);
                if (!contiguousFramesTracker.mHookSetup) {
                    ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, contiguousFramesTracker.$ul_mInjectionContext)).BxR(new C23326Bcc(contiguousFramesTracker));
                }
            }
            if (!r4.A02) {
                r4.A00().A02();
                r4.A02 = true;
            }
        }
        r4.A07.put(i, i);
    }

    public static void A03(C08450fN r4, int i) {
        r4.A07.delete(i);
        if (r4.A07.size() == 0) {
            if (r4.A02) {
                r4.A00().A01();
                r4.A02 = false;
            }
            if (r4.A04) {
                ((C179938Ui) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B8r, r4.A00)).A02 = false;
            }
            if (r4.A05) {
                ContiguousFramesTracker contiguousFramesTracker = (ContiguousFramesTracker) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Axe, r4.A00);
                ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, contiguousFramesTracker.$ul_mInjectionContext)).BxR(new AnonymousClass2O1(contiguousFramesTracker));
            }
            r4.A09.A00();
        }
    }

    public BJ4 A04() {
        BJ3 bj3;
        C08440fM r2 = this.A09;
        C08460fO r0 = r2.A02;
        C08460fO r6 = new C08460fO(r0.A02, r0.A00, r0.A01);
        C08460fO r02 = r2.A01;
        C08460fO r7 = new C08460fO(r02.A02, r02.A00, r02.A01);
        C08460fO r03 = r2.A00;
        BJ4 bj4 = new BJ4(r6, r7, new C08460fO(r03.A02, r03.A00, r03.A01), ImmutableList.copyOf((Collection) r2.A03.A01), ImmutableList.copyOf((Collection) r2.A03.A00), r2.A04.now());
        if (this.A06) {
            C26531bb r4 = (C26531bb) AnonymousClass1XX.A02(3, AnonymousClass1Y3.APX, this.A00);
            if (!r4.A0S) {
                bj3 = null;
            } else {
                bj3 = new BJ3();
                bj3.A0G = r4.A0C.A05();
                bj3.A07 = r4.A06.A05();
                bj3.A0F = r4.A0B.A05();
                bj3.A0M = r4.A0I.A05();
                bj3.A0K = r4.A0G.A05();
                bj3.A0L = r4.A0H.A05();
                bj3.A0H = r4.A0D.A05();
                bj3.A0D = r4.A09.A05();
                bj3.A0I = r4.A0E.A05();
                bj3.A0E = r4.A0A.A05();
                bj3.A0A = r4.A07.A05();
                bj3.A06 = r4.A05.A05();
                bj3.A0J = r4.A0F.A05();
                bj3.A0B = r4.A08.A05();
                bj3.A02 = r4.A0J.A02();
                bj3.A04 = r4.A0K.A02();
                bj3.A05 = r4.A0L.A02();
                bj3.A00 = 10000;
                int i = AnonymousClass1Y3.Axe;
                bj3.A09 = ((ContiguousFramesTracker) AnonymousClass1XX.A02(3, i, r4.A03)).mContiguousFrameBuckets.A05();
                bj3.A08 = ((ContiguousFramesTracker) AnonymousClass1XX.A02(3, i, r4.A03)).mFirstFrameBuckets.A05();
                bj3.A0C = ((ContiguousFramesTracker) AnonymousClass1XX.A02(3, i, r4.A03)).mFrameBuckets.A05();
                ContiguousFramesTracker contiguousFramesTracker = (ContiguousFramesTracker) AnonymousClass1XX.A02(3, i, r4.A03);
                BJ5 A022 = contiguousFramesTracker.mCUTracker.A02();
                A022.A01 += contiguousFramesTracker.mTotalStateDurationWhilePendingMs;
                bj3.A03 = A022;
            }
            bj4.A00 = bj3;
        }
        return bj4;
    }

    public void onFrameRendered(int i) {
        ContiguousFramesTracker contiguousFramesTracker;
        IntBuffer intBuffer;
        if (this.A02) {
            C08440fM r1 = this.A09;
            r1.A02.A00(i);
            r1.A01.A00(i);
            r1.A00.A00(i);
            C08480fQ r4 = r1.A03;
            long now = r1.A04.now();
            if (i >= 200 && r4.A01.size() < 50) {
                r4.A01.add(Long.valueOf(now));
                r4.A00.add(Integer.valueOf(i));
            }
            if (this.A06) {
                C26531bb r9 = (C26531bb) AnonymousClass1XX.A02(3, AnonymousClass1Y3.APX, this.A00);
                if (r9.A0S) {
                    long now2 = ((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, r9.A03)).now();
                    r9.A0O.set(now2);
                    C73113fW r6 = r9.A0C;
                    int i2 = 0;
                    while (true) {
                        int[] iArr = r6.A02;
                        if (i2 >= iArr.length || i < iArr[i2]) {
                            r6.A04(i2, i);
                            boolean z = false;
                        } else {
                            i2++;
                        }
                    }
                    r6.A04(i2, i);
                    boolean z2 = false;
                    if (C73103fV.A00(r9.A04).A01.length == 0) {
                        z2 = true;
                    }
                    if (z2) {
                        r9.A0L.A04(false);
                    } else {
                        r9.A0L.A04(true);
                    }
                    if (C73093fU.A01(r9.A0L).A05) {
                        r9.A0I.A04(i2, i);
                    }
                    if (r9.A0K.A05()) {
                        r9.A0B.A04(i2, i);
                    }
                    if (r9.A0J.A05()) {
                        r9.A06.A04(i2, i);
                        long j = r9.A00;
                        if (j == 0) {
                            r9.A00 = now2;
                        } else if (now2 - j > 2000) {
                            r9.A00 = 0;
                            r9.A0J.A04(false);
                        }
                    }
                    long j2 = r9.A0N.get();
                    if (j2 != 0) {
                        if (now2 - j2 < LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT) {
                            r9.A05.A04(i2, i);
                        } else {
                            r9.A0N.set(0);
                        }
                    }
                }
            }
            if (this.A05 && (intBuffer = (contiguousFramesTracker = (ContiguousFramesTracker) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Axe, this.A00)).mSharedBuffer) != null) {
                boolean z3 = false;
                if (intBuffer.get(0) != 0) {
                    z3 = true;
                }
                if (z3) {
                    contiguousFramesTracker.mSharedBuffer.put(0, 0);
                    C73113fW r42 = contiguousFramesTracker.mFrameBuckets;
                    int i3 = 0;
                    while (true) {
                        int[] iArr2 = r42.A02;
                        if (i3 >= iArr2.length || i < iArr2[i3]) {
                            r42.A04(i3, i);
                        } else {
                            i3++;
                        }
                    }
                    r42.A04(i3, i);
                    if (contiguousFramesTracker.mContiguousUpdates == 0) {
                        long currentMonotonicTimestamp = ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, contiguousFramesTracker.$ul_mInjectionContext)).currentMonotonicTimestamp();
                        contiguousFramesTracker.mFirstFrameDurationMs = i;
                        contiguousFramesTracker.mContiguousUpdateStartQplTime = currentMonotonicTimestamp - ((long) i);
                        C73113fW r43 = contiguousFramesTracker.mPendingBuckets;
                        int i4 = 0;
                        while (true) {
                            int[] iArr3 = r43.A00;
                            if (i4 >= iArr3.length) {
                                break;
                            }
                            iArr3[i4] = 0;
                            i4++;
                        }
                        contiguousFramesTracker.mStateDurationWhilePendingMs = 0;
                    }
                    int i5 = contiguousFramesTracker.mContiguousUpdates + 1;
                    contiguousFramesTracker.mContiguousUpdates = i5;
                    if (!contiguousFramesTracker.mInContiguousUpdate) {
                        C73113fW r92 = contiguousFramesTracker.mPendingBuckets;
                        r92.A04(i3, i);
                        int i6 = contiguousFramesTracker.mStateDurationWhilePendingMs + i;
                        contiguousFramesTracker.mStateDurationWhilePendingMs = i6;
                        if (i5 >= 5) {
                            contiguousFramesTracker.mInContiguousUpdate = true;
                            C73113fW r7 = contiguousFramesTracker.mContiguousFrameBuckets;
                            int i7 = 0;
                            while (true) {
                                int[] iArr4 = r7.A00;
                                if (i7 < iArr4.length) {
                                    iArr4[i7] = iArr4[i7] + r92.A00[i7];
                                    i7++;
                                } else {
                                    contiguousFramesTracker.mTotalStateDurationWhilePendingMs += i6;
                                    contiguousFramesTracker.mFirstFrameBuckets.A03(contiguousFramesTracker.mFirstFrameDurationMs);
                                    int i8 = AnonymousClass1Y3.BBd;
                                    AnonymousClass0UN r12 = contiguousFramesTracker.$ul_mInjectionContext;
                                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, i8, r12)).markerStart(44826638, 0, "surface", ((AnonymousClass14J) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Asq, r12)).A02(), contiguousFramesTracker.mContiguousUpdateStartQplTime);
                                    contiguousFramesTracker.mCUTracker.A04(true);
                                    return;
                                }
                            }
                        }
                    } else {
                        contiguousFramesTracker.mContiguousFrameBuckets.A04(i3, i);
                    }
                } else if (contiguousFramesTracker.mContiguousUpdates != 0) {
                    ContiguousFramesTracker.endContiguousUpdate(contiguousFramesTracker);
                }
            }
        }
    }

    private C08450fN(AnonymousClass1XY r6) {
        this.A00 = new AnonymousClass0UN(7, r6);
        this.A08 = new C08430fL(this);
        int i = AnonymousClass1Y3.BBa;
        AnonymousClass0UN r1 = this.A00;
        this.A09 = new C08440fM((AnonymousClass069) AnonymousClass1XX.A02(2, i, r1));
        this.A06 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r1)).Aeo(283265978337547L, false);
        this.A04 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aeo(283265978403084L, false);
        this.A05 = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A00)).Aeo(283265978468621L, false);
    }
}
