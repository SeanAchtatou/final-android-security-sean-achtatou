package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.facebook.selfupdate2.SelfUpdateActivity;

/* renamed from: X.0YY  reason: invalid class name */
public final class AnonymousClass0YY implements C186714q {
    private AnonymousClass0UN A00;
    private final C187014t A01;

    public boolean CE5() {
        return false;
    }

    public static final AnonymousClass0YY A00(AnonymousClass1XY r1) {
        return new AnonymousClass0YY(r1);
    }

    public Integer AeZ() {
        return AnonymousClass07B.A01;
    }

    public Intent AqX(Activity activity) {
        return ((C187114u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abn, this.A00)).A06(activity, false, false);
    }

    public boolean BDw(Context context) {
        C187014t r2 = this.A01;
        if (!((C187114u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abn, r2.A00)).A07(false) && r2.A01.A01() == 3) {
            return false;
        }
        C187114u r22 = (C187114u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abn, r2.A00);
        boolean z = false;
        if (C187114u.A03(r22, false) && (C187114u.A05(r22, false) || C187114u.A04(r22, false))) {
            z = true;
        }
        if (z) {
            return true;
        }
        return false;
    }

    public boolean CEE(Activity activity) {
        if (!(activity instanceof SelfUpdateActivity)) {
            return true;
        }
        return false;
    }

    private AnonymousClass0YY(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = new C187014t(r3);
    }
}
