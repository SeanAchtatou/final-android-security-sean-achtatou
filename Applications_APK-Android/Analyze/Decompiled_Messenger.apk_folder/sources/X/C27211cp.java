package X;

import android.os.RemoteException;
import com.facebook.fbservice.service.IBlueService;
import com.facebook.http.interfaces.RequestPriority;
import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1cp  reason: invalid class name and case insensitive filesystem */
public class C27211cp extends C07920eO implements ListenableFuture {
    public void A01(RequestPriority requestPriority) {
        if (this instanceof AnonymousClass0lN) {
            AnonymousClass0lN r1 = (AnonymousClass0lN) this;
            if (!r1.isDone()) {
                C27171cl r0 = r1.A00;
                try {
                    IBlueService iBlueService = r0.A07;
                    String str = r0.A09;
                    if (iBlueService != null && str != null) {
                        iBlueService.AS8(str, requestPriority);
                    }
                } catch (RemoteException e) {
                    C010708t.A0R("DefaultBlueServiceOperation", e, "Cannot changePriority because of a RemoteException.");
                }
            }
        }
    }

    public boolean A02() {
        return false;
    }
}
