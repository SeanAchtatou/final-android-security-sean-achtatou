package X;

import java.util.concurrent.ExecutorService;

/* renamed from: X.17A  reason: invalid class name */
public final class AnonymousClass17A extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$38";
    public final /* synthetic */ C190716r A00;
    public final /* synthetic */ String A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass17A(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, String str2) {
        super(executorService, r3, str);
        this.A00 = r1;
        this.A01 = str2;
    }
}
