package X;

import io.card.payment.BuildConfig;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0jV  reason: invalid class name and case insensitive filesystem */
public final class C10070jV extends C10080jW {
    public static final C10090jX[] NO_ANNOTATION_MAPS = new C10090jX[0];
    public final C10140jc _annotationIntrospector;
    public final Class _class;
    public C10090jX _classAnnotations;
    public List _constructors;
    public List _creatorMethods;
    public boolean _creatorsResolved = false;
    public C29151fx _defaultConstructor;
    public List _fields;
    public C183812t _memberMethods;
    public final C26761by _mixInResolver;
    public final Class _primaryMixIn;
    public final List _superTypes;

    private C10090jX[] _collectRelevantAnnotations(Annotation[][] annotationArr) {
        int length = annotationArr.length;
        C10090jX[] r3 = new C10090jX[length];
        for (int i = 0; i < length; i++) {
            Annotation[] annotationArr2 = annotationArr[i];
            C10090jX r0 = new C10090jX();
            _addAnnotationsIfNotPresent(r0, annotationArr2);
            r3[i] = r0;
        }
        return r3;
    }

    private void _addClassMixIns(C10090jX r3, Class cls, Class cls2) {
        if (cls2 != null) {
            _addAnnotationsIfNotPresent(r3, cls2.getDeclaredAnnotations());
            ArrayList<Class> arrayList = new ArrayList<>(8);
            C29081fq._addSuperTypes(cls2, cls, arrayList, false);
            for (Class declaredAnnotations : arrayList) {
                _addAnnotationsIfNotPresent(r3, declaredAnnotations.getDeclaredAnnotations());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jV._addMixOvers(java.lang.reflect.Method, X.1fw, boolean):void
     arg types: [java.lang.reflect.Method, X.1fw, int]
     candidates:
      X.0jV._addMixOvers(java.lang.reflect.Constructor, X.1fx, boolean):void
      X.0jV._addMixOvers(java.lang.reflect.Method, X.1fw, boolean):void */
    private void _addMemberMethods(Class cls, C183812t r12, Class cls2, C183812t r14) {
        C29141fw r8;
        C29141fw r0;
        if (cls2 != null) {
            _addMethodMixIns(cls, r12, cls2, r14);
        }
        if (cls != null) {
            for (Method method : cls.getDeclaredMethods()) {
                if (_isIncludableMemberMethod(method)) {
                    LinkedHashMap linkedHashMap = r12._methods;
                    if (linkedHashMap == null) {
                        r8 = null;
                    } else {
                        r8 = (C29141fw) linkedHashMap.get(new C184212z(method.getName(), method.getParameterTypes()));
                    }
                    if (r8 == null) {
                        C29141fw _constructMethod = _constructMethod(method);
                        r12.add(_constructMethod);
                        LinkedHashMap linkedHashMap2 = r14._methods;
                        if (linkedHashMap2 != null) {
                            r0 = (C29141fw) linkedHashMap2.remove(new C184212z(method.getName(), method.getParameterTypes()));
                        } else {
                            r0 = null;
                        }
                        if (r0 != null) {
                            _addMixOvers(r0._method, _constructMethod, false);
                        }
                    } else {
                        _addAnnotationsIfNotPresent(r8, method.getDeclaredAnnotations());
                        if (r8.getDeclaringClass().isInterface() && !method.getDeclaringClass().isInterface()) {
                            r12.add(new C29141fw(method, r8._annotations, r8._paramAnnotations));
                        }
                    }
                }
            }
        }
    }

    private void _addMethodMixIns(Class cls, C183812t r11, Class cls2, C183812t r13) {
        C29141fw r2;
        ArrayList<Class> arrayList = new ArrayList<>();
        arrayList.add(cls2);
        C29081fq._addSuperTypes(cls2, cls, arrayList, false);
        for (Class declaredMethods : arrayList) {
            for (Method method : declaredMethods.getDeclaredMethods()) {
                if (_isIncludableMemberMethod(method)) {
                    LinkedHashMap linkedHashMap = r11._methods;
                    if (linkedHashMap == null) {
                        r2 = null;
                    } else {
                        r2 = (C29141fw) linkedHashMap.get(new C184212z(method.getName(), method.getParameterTypes()));
                    }
                    if (r2 != null) {
                        _addAnnotationsIfNotPresent(r2, method.getDeclaredAnnotations());
                    } else {
                        r13.add(_constructMethod(method));
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        if (r1 == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void _addOrOverrideAnnotations(X.C183512m r7, java.lang.annotation.Annotation[] r8) {
        /*
            r6 = this;
            if (r8 == 0) goto L_0x0048
            r5 = 0
            int r4 = r8.length
            r3 = 0
        L_0x0005:
            if (r3 >= r4) goto L_0x0032
            r2 = r8[r3]
            X.0jc r0 = r6._annotationIntrospector
            if (r0 == 0) goto L_0x0014
            boolean r1 = r0.isAnnotationBundle(r2)
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 == 0) goto L_0x002c
            if (r5 != 0) goto L_0x001e
            java.util.LinkedList r5 = new java.util.LinkedList
            r5.<init>()
        L_0x001e:
            java.lang.Class r0 = r2.annotationType()
            java.lang.annotation.Annotation[] r0 = r0.getDeclaredAnnotations()
            r5.add(r0)
        L_0x0029:
            int r3 = r3 + 1
            goto L_0x0005
        L_0x002c:
            X.0jX r0 = r7._annotations
            r0.add(r2)
            goto L_0x0029
        L_0x0032:
            if (r5 == 0) goto L_0x0048
            java.util.Iterator r1 = r5.iterator()
        L_0x0038:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0048
            java.lang.Object r0 = r1.next()
            java.lang.annotation.Annotation[] r0 = (java.lang.annotation.Annotation[]) r0
            r6._addOrOverrideAnnotations(r7, r0)
            goto L_0x0038
        L_0x0048:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10070jV._addOrOverrideAnnotations(X.12m, java.lang.annotation.Annotation[]):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        if (r4 == r0) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0046, code lost:
        r3 = new java.lang.annotation.Annotation[r0][];
        java.lang.System.arraycopy(r7, 0, r3, r1, r6);
        r5 = _collectRelevantAnnotations(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008c, code lost:
        if (r4 == r0) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.C29151fx _constructConstructor(java.lang.reflect.Constructor r9, boolean r10) {
        /*
            r8 = this;
            X.0jc r0 = r8._annotationIntrospector
            if (r0 != 0) goto L_0x0027
            X.1fx r5 = new X.1fx
            X.0jX r4 = new X.0jX
            r4.<init>()
            java.lang.Class[] r0 = r9.getParameterTypes()
            int r3 = r0.length
            if (r3 != 0) goto L_0x0018
            X.0jX[] r2 = X.C10070jV.NO_ANNOTATION_MAPS
        L_0x0014:
            r5.<init>(r9, r4, r2)
            return r5
        L_0x0018:
            X.0jX[] r2 = new X.C10090jX[r3]
            r1 = 0
        L_0x001b:
            if (r1 >= r3) goto L_0x0014
            X.0jX r0 = new X.0jX
            r0.<init>()
            r2[r1] = r0
            int r1 = r1 + 1
            goto L_0x001b
        L_0x0027:
            r5 = 0
            if (r10 != 0) goto L_0x0095
            java.lang.annotation.Annotation[][] r7 = r9.getParameterAnnotations()
            java.lang.Class[] r0 = r9.getParameterTypes()
            int r4 = r0.length
            int r6 = r7.length
            if (r4 == r6) goto L_0x0091
            java.lang.Class r3 = r9.getDeclaringClass()
            boolean r0 = r3.isEnum()
            r2 = 0
            if (r0 == 0) goto L_0x0083
            r1 = 2
            int r0 = r6 + r1
            if (r4 != r0) goto L_0x0083
        L_0x0046:
            java.lang.annotation.Annotation[][] r3 = new java.lang.annotation.Annotation[r0][]
            java.lang.System.arraycopy(r7, r2, r3, r1, r6)
            X.0jX[] r5 = r8._collectRelevantAnnotations(r3)
        L_0x004f:
            if (r5 != 0) goto L_0x0095
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Internal error: constructor for "
            r1.<init>(r0)
            java.lang.Class r0 = r9.getDeclaringClass()
            java.lang.String r0 = r0.getName()
            r1.append(r0)
            java.lang.String r0 = " has mismatch: "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = " parameters; "
            r1.append(r0)
            int r0 = r3.length
            r1.append(r0)
            java.lang.String r0 = " sets of annotations"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0083:
            boolean r0 = r3.isMemberClass()
            if (r0 == 0) goto L_0x008f
            r1 = 1
            int r0 = r6 + r1
            if (r4 != r0) goto L_0x008f
            goto L_0x0046
        L_0x008f:
            r3 = r7
            goto L_0x004f
        L_0x0091:
            X.0jX[] r5 = r8._collectRelevantAnnotations(r7)
        L_0x0095:
            X.1fx r3 = new X.1fx
            java.lang.annotation.Annotation[] r2 = r9.getDeclaredAnnotations()
            X.0jX r0 = new X.0jX
            r0.<init>()
            r8._addAnnotationsIfNotPresent(r0, r2)
            r3.<init>(r9, r0, r5)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10070jV._constructConstructor(java.lang.reflect.Constructor, boolean):X.1fx");
    }

    private C29141fw _constructMethod(Method method) {
        if (this._annotationIntrospector == null) {
            return new C29141fw(method, new C10090jX(), null);
        }
        Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
        C10090jX r0 = new C10090jX();
        _addAnnotationsIfNotPresent(r0, declaredAnnotations);
        return new C29141fw(method, r0, null);
    }

    public static C10070jV construct(Class cls, C10140jc r8, C26761by r9) {
        ArrayList arrayList = new ArrayList(8);
        C29081fq._addSuperTypes(cls, null, arrayList, false);
        return new C10070jV(cls, arrayList, r8, r9, null);
    }

    public static C10070jV constructWithoutSuperTypes(Class cls, C10140jc r7, C26761by r8) {
        return new C10070jV(cls, Collections.emptyList(), r7, r8, null);
    }

    public static void resolveClassAnnotations(C10070jV r4) {
        C10090jX r2 = new C10090jX();
        r4._classAnnotations = r2;
        if (r4._annotationIntrospector != null) {
            Class cls = r4._primaryMixIn;
            if (cls != null) {
                r4._addClassMixIns(r2, r4._class, cls);
            }
            r4._addAnnotationsIfNotPresent(r4._classAnnotations, r4._class.getDeclaredAnnotations());
            for (Class cls2 : r4._superTypes) {
                C10090jX r1 = r4._classAnnotations;
                C26761by r0 = r4._mixInResolver;
                if (r0 != null) {
                    r4._addClassMixIns(r1, cls2, r0.findMixInClassFor(cls2));
                }
                r4._addAnnotationsIfNotPresent(r4._classAnnotations, cls2.getDeclaredAnnotations());
            }
            C10090jX r22 = r4._classAnnotations;
            Class<Object> cls3 = Object.class;
            C26761by r02 = r4._mixInResolver;
            if (r02 != null) {
                r4._addClassMixIns(r22, cls3, r02.findMixInClassFor(cls3));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jV._addMixOvers(java.lang.reflect.Constructor, X.1fx, boolean):void
     arg types: [java.lang.reflect.Constructor<?>, X.1fx, int]
     candidates:
      X.0jV._addMixOvers(java.lang.reflect.Method, X.1fw, boolean):void
      X.0jV._addMixOvers(java.lang.reflect.Constructor, X.1fx, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jV._addMixOvers(java.lang.reflect.Method, X.1fw, boolean):void
     arg types: [java.lang.reflect.Method, X.1fw, int]
     candidates:
      X.0jV._addMixOvers(java.lang.reflect.Constructor, X.1fx, boolean):void
      X.0jV._addMixOvers(java.lang.reflect.Method, X.1fw, boolean):void */
    public static void resolveCreators(C10070jV r14) {
        C29141fw r4;
        C10090jX[] r2;
        int size;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        for (Constructor<?> constructor : r14._class.getDeclaredConstructors()) {
            if (constructor.getParameterTypes().length == 0) {
                r14._defaultConstructor = r14._constructConstructor(constructor, true);
            } else {
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(Math.max(10, r5));
                }
                arrayList2.add(r14._constructConstructor(constructor, false));
            }
        }
        if (arrayList2 == null) {
            r14._constructors = Collections.emptyList();
        } else {
            r14._constructors = arrayList2;
        }
        if (r14._primaryMixIn != null && (r14._defaultConstructor != null || !r14._constructors.isEmpty())) {
            Class cls = r14._primaryMixIn;
            List list = r14._constructors;
            if (list == null) {
                size = 0;
            } else {
                size = list.size();
            }
            C184212z[] r5 = null;
            for (Constructor<?> constructor2 : cls.getDeclaredConstructors()) {
                if (constructor2.getParameterTypes().length != 0) {
                    if (r5 == null) {
                        r5 = new C184212z[size];
                        for (int i = 0; i < size; i++) {
                            r5[i] = new C184212z(BuildConfig.FLAVOR, ((C29151fx) r14._constructors.get(i))._constructor.getParameterTypes());
                        }
                    }
                    C184212z r12 = new C184212z(BuildConfig.FLAVOR, constructor2.getParameterTypes());
                    int i2 = 0;
                    while (true) {
                        if (i2 < size) {
                            if (r12.equals(r5[i2])) {
                                r14._addMixOvers((Constructor) constructor2, (C29151fx) r14._constructors.get(i2), true);
                                break;
                            }
                            i2++;
                        } else {
                            break;
                        }
                    }
                } else {
                    C29151fx r0 = r14._defaultConstructor;
                    if (r0 != null) {
                        r14._addMixOvers((Constructor) constructor2, r0, false);
                    }
                }
            }
        }
        C10140jc r1 = r14._annotationIntrospector;
        if (r1 != null) {
            C29151fx r02 = r14._defaultConstructor;
            if (r02 != null && r1.hasIgnoreMarker(r02)) {
                r14._defaultConstructor = null;
            }
            List list2 = r14._constructors;
            if (list2 != null) {
                int size2 = list2.size();
                while (true) {
                    size2--;
                    if (size2 < 0) {
                        break;
                    } else if (r14._annotationIntrospector.hasIgnoreMarker((C183512m) r14._constructors.get(size2))) {
                        r14._constructors.remove(size2);
                    }
                }
            }
        }
        for (Method method : r14._class.getDeclaredMethods()) {
            if (Modifier.isStatic(method.getModifiers())) {
                if (arrayList == null) {
                    arrayList = new ArrayList(8);
                }
                if (r14._annotationIntrospector == null) {
                    C10090jX r11 = new C10090jX();
                    int length = method.getParameterTypes().length;
                    if (length == 0) {
                        r2 = NO_ANNOTATION_MAPS;
                    } else {
                        r2 = new C10090jX[length];
                        for (int i3 = 0; i3 < length; i3++) {
                            r2[i3] = new C10090jX();
                        }
                    }
                    r4 = new C29141fw(method, r11, r2);
                } else {
                    Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
                    C10090jX r13 = new C10090jX();
                    r14._addAnnotationsIfNotPresent(r13, declaredAnnotations);
                    r4 = new C29141fw(method, r13, r14._collectRelevantAnnotations(method.getParameterAnnotations()));
                }
                arrayList.add(r4);
            }
        }
        if (arrayList == null) {
            r14._creatorMethods = Collections.emptyList();
        } else {
            r14._creatorMethods = arrayList;
            Class cls2 = r14._primaryMixIn;
            if (cls2 != null) {
                int size3 = arrayList.size();
                C184212z[] r52 = null;
                for (Method method2 : cls2.getDeclaredMethods()) {
                    if (Modifier.isStatic(method2.getModifiers()) && method2.getParameterTypes().length != 0) {
                        if (r52 == null) {
                            r52 = new C184212z[size3];
                            for (int i4 = 0; i4 < size3; i4++) {
                                Method method3 = ((C29141fw) r14._creatorMethods.get(i4))._method;
                                r52[i4] = new C184212z(method3.getName(), method3.getParameterTypes());
                            }
                        }
                        C184212z r9 = new C184212z(method2.getName(), method2.getParameterTypes());
                        int i5 = 0;
                        while (true) {
                            if (i5 < size3) {
                                if (r9.equals(r52[i5])) {
                                    r14._addMixOvers(method2, (C29141fw) r14._creatorMethods.get(i5), true);
                                    break;
                                }
                                i5++;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (r14._annotationIntrospector != null) {
                int size4 = r14._creatorMethods.size();
                while (true) {
                    size4--;
                    if (size4 < 0) {
                        break;
                    } else if (r14._annotationIntrospector.hasIgnoreMarker((C183512m) r14._creatorMethods.get(size4))) {
                        r14._creatorMethods.remove(size4);
                    }
                }
            }
        }
        r14._creatorsResolved = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jV._addMixOvers(java.lang.reflect.Method, X.1fw, boolean):void
     arg types: [java.lang.reflect.Method, X.1fw, int]
     candidates:
      X.0jV._addMixOvers(java.lang.reflect.Constructor, X.1fx, boolean):void
      X.0jV._addMixOvers(java.lang.reflect.Method, X.1fw, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        if (r1 == 0) goto L_0x0057;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void resolveMemberMethods(X.C10070jV r5) {
        /*
            X.12t r2 = new X.12t
            r2.<init>()
            r5._memberMethods = r2
            X.12t r3 = new X.12t
            r3.<init>()
            java.lang.Class r1 = r5._class
            java.lang.Class r0 = r5._primaryMixIn
            r5._addMemberMethods(r1, r2, r0, r3)
            java.util.List r0 = r5._superTypes
            java.util.Iterator r4 = r0.iterator()
        L_0x0019:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0035
            java.lang.Object r2 = r4.next()
            java.lang.Class r2 = (java.lang.Class) r2
            X.1by r0 = r5._mixInResolver
            if (r0 != 0) goto L_0x0030
            r1 = 0
        L_0x002a:
            X.12t r0 = r5._memberMethods
            r5._addMemberMethods(r2, r0, r1, r3)
            goto L_0x0019
        L_0x0030:
            java.lang.Class r1 = r0.findMixInClassFor(r2)
            goto L_0x002a
        L_0x0035:
            X.1by r1 = r5._mixInResolver
            if (r1 == 0) goto L_0x0048
            java.lang.Class<java.lang.Object> r0 = java.lang.Object.class
            java.lang.Class r2 = r1.findMixInClassFor(r0)
            if (r2 == 0) goto L_0x0048
            java.lang.Class r1 = r5._class
            X.12t r0 = r5._memberMethods
            r5._addMethodMixIns(r1, r0, r2, r3)
        L_0x0048:
            X.0jc r0 = r5._annotationIntrospector
            if (r0 == 0) goto L_0x0094
            java.util.LinkedHashMap r0 = r3._methods
            if (r0 == 0) goto L_0x0057
            int r1 = r0.size()
            r0 = 0
            if (r1 != 0) goto L_0x0058
        L_0x0057:
            r0 = 1
        L_0x0058:
            if (r0 != 0) goto L_0x0094
            java.util.Iterator r4 = r3.iterator()
        L_0x005e:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0094
            java.lang.Object r3 = r4.next()
            X.1fw r3 = (X.C29141fw) r3
            java.lang.Class<java.lang.Object> r2 = java.lang.Object.class
            java.lang.String r1 = r3.getName()     // Catch:{ Exception -> 0x005e }
            java.lang.Class[] r0 = r3._paramClasses     // Catch:{ Exception -> 0x005e }
            if (r0 != 0) goto L_0x007c
            java.lang.reflect.Method r0 = r3._method     // Catch:{ Exception -> 0x005e }
            java.lang.Class[] r0 = r0.getParameterTypes()     // Catch:{ Exception -> 0x005e }
            r3._paramClasses = r0     // Catch:{ Exception -> 0x005e }
        L_0x007c:
            java.lang.Class[] r0 = r3._paramClasses     // Catch:{ Exception -> 0x005e }
            java.lang.reflect.Method r0 = r2.getDeclaredMethod(r1, r0)     // Catch:{ Exception -> 0x005e }
            if (r0 == 0) goto L_0x005e
            X.1fw r2 = r5._constructMethod(r0)     // Catch:{ Exception -> 0x005e }
            java.lang.reflect.Method r1 = r3._method     // Catch:{ Exception -> 0x005e }
            r0 = 0
            r5._addMixOvers(r1, r2, r0)     // Catch:{ Exception -> 0x005e }
            X.12t r0 = r5._memberMethods     // Catch:{ Exception -> 0x005e }
            r0.add(r2)     // Catch:{ Exception -> 0x005e }
            goto L_0x005e
        L_0x0094:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10070jV.resolveMemberMethods(X.0jV):void");
    }

    public Iterable fields() {
        if (this._fields == null) {
            Map _findFields = _findFields(this._class, null);
            if (_findFields == null || _findFields.size() == 0) {
                this._fields = Collections.emptyList();
            } else {
                ArrayList arrayList = new ArrayList(_findFields.size());
                this._fields = arrayList;
                arrayList.addAll(_findFields.values());
            }
        }
        return this._fields;
    }

    public /* bridge */ /* synthetic */ AnnotatedElement getAnnotated() {
        return this._class;
    }

    public Annotation getAnnotation(Class cls) {
        if (this._classAnnotations == null) {
            resolveClassAnnotations(this);
        }
        HashMap hashMap = this._classAnnotations._annotations;
        if (hashMap == null) {
            return null;
        }
        return (Annotation) hashMap.get(cls);
    }

    public Type getGenericType() {
        return this._class;
    }

    public String getName() {
        return this._class.getName();
    }

    public Class getRawType() {
        return this._class;
    }

    public String toString() {
        return AnonymousClass08S.A0P("[AnnotedClass ", this._class.getName(), "]");
    }

    private C10070jV(Class cls, List list, C10140jc r4, C26761by r5, C10090jX r6) {
        Class findMixInClassFor;
        this._class = cls;
        this._superTypes = list;
        this._annotationIntrospector = r4;
        this._mixInResolver = r5;
        if (r5 == null) {
            findMixInClassFor = null;
        } else {
            findMixInClassFor = r5.findMixInClassFor(cls);
        }
        this._primaryMixIn = findMixInClassFor;
        this._classAnnotations = r6;
    }

    private Map _findFields(Class cls, Map map) {
        Class findMixInClassFor;
        C29091fr r1;
        C29091fr r3;
        Class superclass = cls.getSuperclass();
        if (superclass != null) {
            map = _findFields(superclass, map);
            for (Field field : cls.getDeclaredFields()) {
                if (_isIncludableField(field)) {
                    if (map == null) {
                        map = new LinkedHashMap();
                    }
                    String name = field.getName();
                    if (this._annotationIntrospector == null) {
                        r3 = new C29091fr(field, new C10090jX());
                    } else {
                        Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
                        C10090jX r0 = new C10090jX();
                        _addAnnotationsIfNotPresent(r0, declaredAnnotations);
                        r3 = new C29091fr(field, r0);
                    }
                    map.put(name, r3);
                }
            }
            C26761by r02 = this._mixInResolver;
            if (!(r02 == null || (findMixInClassFor = r02.findMixInClassFor(cls)) == null)) {
                ArrayList<Class> arrayList = new ArrayList<>();
                arrayList.add(findMixInClassFor);
                C29081fq._addSuperTypes(findMixInClassFor, superclass, arrayList, false);
                for (Class declaredFields : arrayList) {
                    for (Field field2 : declaredFields.getDeclaredFields()) {
                        if (_isIncludableField(field2) && (r1 = (C29091fr) map.get(field2.getName())) != null) {
                            _addOrOverrideAnnotations(r1, field2.getDeclaredAnnotations());
                        }
                    }
                }
            }
        }
        return map;
    }

    private static boolean _isIncludableField(Field field) {
        if (!field.isSynthetic()) {
            int modifiers = field.getModifiers();
            if (Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers)) {
                return false;
            }
            return true;
        }
        return false;
    }

    private static boolean _isIncludableMemberMethod(Method method) {
        if (Modifier.isStatic(method.getModifiers()) || method.isSynthetic() || method.isBridge() || method.getParameterTypes().length > 2) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        if (r1 == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void _addAnnotationsIfNotPresent(X.C10090jX r7, java.lang.annotation.Annotation[] r8) {
        /*
            r6 = this;
            if (r8 == 0) goto L_0x0046
            r5 = 0
            int r4 = r8.length
            r3 = 0
        L_0x0005:
            if (r3 >= r4) goto L_0x0030
            r2 = r8[r3]
            X.0jc r0 = r6._annotationIntrospector
            if (r0 == 0) goto L_0x0014
            boolean r1 = r0.isAnnotationBundle(r2)
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 == 0) goto L_0x002c
            if (r5 != 0) goto L_0x001e
            java.util.LinkedList r5 = new java.util.LinkedList
            r5.<init>()
        L_0x001e:
            java.lang.Class r0 = r2.annotationType()
            java.lang.annotation.Annotation[] r0 = r0.getDeclaredAnnotations()
            r5.add(r0)
        L_0x0029:
            int r3 = r3 + 1
            goto L_0x0005
        L_0x002c:
            r7.addIfNotPresent(r2)
            goto L_0x0029
        L_0x0030:
            if (r5 == 0) goto L_0x0046
            java.util.Iterator r1 = r5.iterator()
        L_0x0036:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0046
            java.lang.Object r0 = r1.next()
            java.lang.annotation.Annotation[] r0 = (java.lang.annotation.Annotation[]) r0
            r6._addAnnotationsIfNotPresent(r7, r0)
            goto L_0x0036
        L_0x0046:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10070jV._addAnnotationsIfNotPresent(X.0jX, java.lang.annotation.Annotation[]):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        if (r1 == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void _addAnnotationsIfNotPresent(X.C183512m r7, java.lang.annotation.Annotation[] r8) {
        /*
            r6 = this;
            if (r8 == 0) goto L_0x0048
            r5 = 0
            int r4 = r8.length
            r3 = 0
        L_0x0005:
            if (r3 >= r4) goto L_0x0032
            r2 = r8[r3]
            X.0jc r0 = r6._annotationIntrospector
            if (r0 == 0) goto L_0x0014
            boolean r1 = r0.isAnnotationBundle(r2)
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 == 0) goto L_0x002c
            if (r5 != 0) goto L_0x001e
            java.util.LinkedList r5 = new java.util.LinkedList
            r5.<init>()
        L_0x001e:
            java.lang.Class r0 = r2.annotationType()
            java.lang.annotation.Annotation[] r0 = r0.getDeclaredAnnotations()
            r5.add(r0)
        L_0x0029:
            int r3 = r3 + 1
            goto L_0x0005
        L_0x002c:
            X.0jX r0 = r7._annotations
            r0.addIfNotPresent(r2)
            goto L_0x0029
        L_0x0032:
            if (r5 == 0) goto L_0x0048
            java.util.Iterator r1 = r5.iterator()
        L_0x0038:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0048
            java.lang.Object r0 = r1.next()
            java.lang.annotation.Annotation[] r0 = (java.lang.annotation.Annotation[]) r0
            r6._addAnnotationsIfNotPresent(r7, r0)
            goto L_0x0038
        L_0x0048:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10070jV._addAnnotationsIfNotPresent(X.12m, java.lang.annotation.Annotation[]):void");
    }

    private void _addMixOvers(Constructor constructor, C29151fx r11, boolean z) {
        _addOrOverrideAnnotations(r11, constructor.getDeclaredAnnotations());
        if (z) {
            Annotation[][] parameterAnnotations = constructor.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i = 0; i < length; i++) {
                for (Annotation annotation : parameterAnnotations[i]) {
                    C10090jX[] r1 = r11._paramAnnotations;
                    C10090jX r0 = r1[i];
                    if (r0 == null) {
                        r0 = new C10090jX();
                        r1[i] = r0;
                    }
                    r0.add(annotation);
                }
            }
        }
    }

    private void _addMixOvers(Method method, C29141fw r11, boolean z) {
        _addOrOverrideAnnotations(r11, method.getDeclaredAnnotations());
        if (z) {
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i = 0; i < length; i++) {
                for (Annotation annotation : parameterAnnotations[i]) {
                    C10090jX[] r1 = r11._paramAnnotations;
                    C10090jX r0 = r1[i];
                    if (r0 == null) {
                        r0 = new C10090jX();
                        r1[i] = r0;
                    }
                    r0.add(annotation);
                }
            }
        }
    }
}
