package X;

import android.os.Build;
import android.system.ErrnoException;
import android.system.Int64Ref;
import android.system.Os;
import android.system.StructStat;
import android.util.Log;
import android.util.MutableLong;
import com.facebook.common.dextricks.DexStore;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import libcore.io.Libcore;

/* renamed from: X.0Me  reason: invalid class name */
public final class AnonymousClass0Me {
    public static boolean A00 = true;
    private static final boolean A01;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        if (r3 == 3) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0038, code lost:
        r7 = true;
     */
    static {
        /*
            r7 = 0
            java.lang.String r0 = "os.version"
            java.lang.String r1 = java.lang.System.getProperty(r0)     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            if (r1 == 0) goto L_0x0039
            boolean r0 = r1.isEmpty()     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            if (r0 != 0) goto L_0x0039
            java.lang.String r0 = "\\."
            java.lang.String[] r6 = r1.split(r0)     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            r5 = 3
            r2 = 2
            r1 = 6
            r0 = 33
            int[] r4 = new int[]{r2, r1, r0}     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            int r0 = r6.length     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            int r3 = java.lang.Math.min(r0, r5)     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            r2 = 0
            goto L_0x0029
        L_0x0025:
            if (r1 > r0) goto L_0x0038
            int r2 = r2 + 1
        L_0x0029:
            if (r2 >= r3) goto L_0x0036
            r0 = r6[r2]     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            int r1 = java.lang.Integer.parseInt(r0)     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            r0 = r4[r2]     // Catch:{ IllegalArgumentException | NullPointerException | SecurityException -> 0x0039 }
            if (r1 < r0) goto L_0x0039
            goto L_0x0025
        L_0x0036:
            if (r3 != r5) goto L_0x0039
        L_0x0038:
            r7 = 1
        L_0x0039:
            X.AnonymousClass0Me.A01 = r7
            r0 = 1
            X.AnonymousClass0Me.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Me.<clinit>():void");
    }

    public static int A0A(InputStream inputStream, byte[] bArr, int i) {
        int min = Math.min(bArr.length, i);
        int i2 = 0;
        int i3 = 0;
        while (i2 < min) {
            i3 = inputStream.read(bArr, i2, min - i2);
            if (i3 < 0) {
                break;
            }
            i2 += i3;
        }
        if (i3 == -1 && i2 == 0) {
            return -1;
        }
        return i2;
    }

    public static int A00(FileDescriptor fileDescriptor) {
        StructStat fstat = Libcore.os.fstat(fileDescriptor);
        if (fstat == null) {
            return -1;
        }
        return (int) fstat.st_size;
    }

    public static int A04(FileDescriptor fileDescriptor, FileDescriptor fileDescriptor2, AnonymousClass0Md r8, int i) {
        MutableLong mutableLong = new MutableLong(r8.A00);
        int sendfile = (int) Libcore.os.sendfile(fileDescriptor, fileDescriptor2, mutableLong, (long) i);
        if (sendfile >= 0) {
            r8.A00 = mutableLong.value;
        }
        return sendfile;
    }

    public static int A05(FileDescriptor fileDescriptor, FileDescriptor fileDescriptor2, AnonymousClass0Md r5, int i) {
        int sendfile = (int) Os.sendfile(fileDescriptor, fileDescriptor2, new Int64Ref(r5.A00), (long) i);
        if (sendfile >= 0) {
            r5.A00 = (long) sendfile;
        }
        return sendfile;
    }

    private static int A06(FileDescriptor fileDescriptor, FileDescriptor fileDescriptor2, AnonymousClass0Md r7, int i) {
        try {
            int i2 = Build.VERSION.SDK_INT;
            FileDescriptor fileDescriptor3 = fileDescriptor;
            FileDescriptor fileDescriptor4 = fileDescriptor2;
            if (i2 >= 28) {
                return A05(fileDescriptor, fileDescriptor2, r7, i);
            }
            if (i2 >= 21) {
                return A04(fileDescriptor, fileDescriptor2, r7, i);
            }
            libcore.util.MutableLong mutableLong = new libcore.util.MutableLong(r7.A00);
            int sendfile = (int) Libcore.os.sendfile(fileDescriptor3, fileDescriptor4, mutableLong, (long) i);
            if (sendfile >= 0) {
                r7.A00 = mutableLong.value;
            }
            return sendfile;
        } catch (IllegalAccessError | NoClassDefFoundError | NoSuchFieldError | NoSuchMethodError e) {
            Log.w("CopyUtils", "Failed to call send file for copy utils", e);
            A00 = false;
            return -1;
        } catch (Exception e2) {
            throw new IOException(e2);
        }
    }

    public static int A09(FileOutputStream fileOutputStream, FileInputStream fileInputStream, int i) {
        int i2;
        int i3;
        if (!A01 || !A00) {
            i2 = -1;
        } else {
            FileDescriptor fd = fileOutputStream.getFD();
            FileDescriptor fd2 = fileInputStream.getFD();
            try {
                int i4 = Build.VERSION.SDK_INT;
                if (i4 >= 28) {
                    i3 = A01(fd2);
                } else if (i4 >= 21) {
                    i3 = A00(fd2);
                } else {
                    StructStat fstat = Libcore.os.fstat(fd2);
                    if (fstat == null) {
                        i3 = -1;
                    } else {
                        i3 = (int) fstat.st_size;
                    }
                }
            } catch (IllegalAccessError | NoClassDefFoundError | NoSuchFieldError | NoSuchMethodError e) {
                Log.w("CopyUtils", "Failed to call fstat st.size for copy utils", e);
                A00 = false;
                i3 = -1;
            } catch (ErrnoException e2) {
                throw new IOException(e2);
            }
            if (i3 >= 0) {
                int min = Math.min(i3, i);
                AnonymousClass0Md r3 = new AnonymousClass0Md(0);
                i2 = 0;
                int i5 = 0;
                while (i2 < min) {
                    int A06 = A06(fd, fd2, r3, min - i2);
                    if (A06 >= 0) {
                        i2 += A06;
                        int i6 = i5 + 1;
                        if (i5 <= 50) {
                            i5 = i6;
                        } else {
                            throw new IOException(String.format("Tried %d times to send file. Progress %d / %d sent: %d", Integer.valueOf(i6), Integer.valueOf(i2), Integer.valueOf(min), Integer.valueOf(A06)));
                        }
                    } else {
                        throw new IOException(String.format("Failed to send file. Ret: %d", Integer.valueOf(A06)));
                    }
                }
            } else {
                throw new IOException(String.format("fstat st_size failed with value %d", Integer.valueOf(i3)));
            }
        }
        if (i2 > 0) {
            return i2;
        }
        return A0B(fileOutputStream, fileInputStream, i);
    }

    public static int A01(FileDescriptor fileDescriptor) {
        StructStat fstat = Os.fstat(fileDescriptor);
        if (fstat == null) {
            return -1;
        }
        return (int) fstat.st_size;
    }

    public static int A0B(OutputStream outputStream, InputStream inputStream, int i) {
        byte[] bArr = new byte[DexStore.LOAD_RESULT_PGO];
        int i2 = 0;
        while (i2 < i) {
            int A0A = A0A(inputStream, bArr, i - i2);
            if (A0A == -1) {
                break;
            }
            outputStream.write(bArr, 0, A0A);
            i2 += A0A;
        }
        return i2;
    }
}
