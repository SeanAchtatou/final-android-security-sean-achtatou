package X;

import java.util.concurrent.atomic.AtomicLong;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.08J  reason: invalid class name */
public final class AnonymousClass08J extends Enum implements AnonymousClass0C4 {
    private static final /* synthetic */ AnonymousClass08J[] A00;
    public static final AnonymousClass08J A01;
    public static final AnonymousClass08J A02;
    public static final AnonymousClass08J A03;
    public static final AnonymousClass08J A04;
    public static final AnonymousClass08J A05;
    public static final AnonymousClass08J A06;
    public static final AnonymousClass08J A07;
    public static final AnonymousClass08J A08;
    public static final AnonymousClass08J A09;
    public static final AnonymousClass08J A0A;
    public static final AnonymousClass08J A0B;
    public static final AnonymousClass08J A0C;
    public static final AnonymousClass08J A0D;
    public static final AnonymousClass08J A0E;
    private final String mJsonKey;
    private final Class mType;

    static {
        Class<AtomicLong> cls = AtomicLong.class;
        AnonymousClass08J r15 = new AnonymousClass08J("MqttDurationMs", 0, "m", cls);
        A09 = r15;
        AnonymousClass08J r13 = new AnonymousClass08J("MqttTotalDurationMs", 1, "mt", cls);
        A0A = r13;
        AnonymousClass08J r12 = new AnonymousClass08J("NetworkDurationMs", 2, "n", cls);
        A0B = r12;
        AnonymousClass08J r11 = new AnonymousClass08J("NetworkTotalDurationMs", 3, "nt", cls);
        A0C = r11;
        AnonymousClass08J r10 = new AnonymousClass08J("ServiceDurationMs", 4, "s", cls);
        A0E = r10;
        AnonymousClass08J r9 = new AnonymousClass08J("MessageSendAttempt", 5, "sa", cls);
        A07 = r9;
        AnonymousClass08J r8 = new AnonymousClass08J("MessageSendSuccess", 6, "ss", cls);
        A08 = r8;
        AnonymousClass08J r7 = new AnonymousClass08J("ForegroundPing", 7, "fp", cls);
        A06 = r7;
        AnonymousClass08J r6 = new AnonymousClass08J("BackgroundPing", 8, "bp", cls);
        A01 = r6;
        AnonymousClass08J r5 = new AnonymousClass08J("PublishReceived", 9, "pr", cls);
        A0D = r5;
        AnonymousClass08J r4 = new AnonymousClass08J("FbnsNotificationReceived", 10, "fnr", cls);
        A05 = r4;
        AnonymousClass08J r3 = new AnonymousClass08J("FbnsLiteNotificationReceived", 11, "flnr", cls);
        A03 = r3;
        AnonymousClass08J r17 = new AnonymousClass08J("FbnsNotificationDeliveryRetried", 12, "fdr", cls);
        A04 = r17;
        AnonymousClass08J r18 = new AnonymousClass08J("FbnsLiteNotificationDeliveryRetried", 13, "fldr", cls);
        A02 = r18;
        AnonymousClass08J r26 = r17;
        AnonymousClass08J r27 = r18;
        AnonymousClass08J r20 = r8;
        AnonymousClass08J r21 = r7;
        AnonymousClass08J r182 = r10;
        AnonymousClass08J r19 = r9;
        AnonymousClass08J r16 = r12;
        AnonymousClass08J r172 = r11;
        AnonymousClass08J r14 = r15;
        AnonymousClass08J r152 = r13;
        A00 = new AnonymousClass08J[]{r14, r152, r16, r172, r182, r19, r20, r21, r6, r5, r4, r3, r26, r27};
    }

    public static AnonymousClass08J valueOf(String str) {
        return (AnonymousClass08J) Enum.valueOf(AnonymousClass08J.class, str);
    }

    public static AnonymousClass08J[] values() {
        return (AnonymousClass08J[]) A00.clone();
    }

    private AnonymousClass08J(String str, int i, String str2, Class cls) {
        this.mJsonKey = str2;
        this.mType = cls;
    }

    public String Arl() {
        return this.mJsonKey;
    }

    public Class B8J() {
        return this.mType;
    }
}
