package X;

import com.facebook.common.callercontext.CallerContextable;

/* renamed from: X.0ih  reason: invalid class name and case insensitive filesystem */
public final class C09790ih extends C09800ii implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.fbui.remote.downloader.FetchFileExecutorImpl";
    public AnonymousClass0UN A00;

    public C09790ih(AnonymousClass1XY r3, AnonymousClass0iW r4) {
        super(r4);
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
