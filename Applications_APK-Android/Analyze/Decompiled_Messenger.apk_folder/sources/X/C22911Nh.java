package X;

import java.lang.ref.SoftReference;
import java.util.LinkedList;
import java.util.Queue;

/* renamed from: X.1Nh  reason: invalid class name and case insensitive filesystem */
public class C22911Nh {
    public int A00;
    public final int A01;
    public final int A02;
    public final Queue A03;
    public final boolean A04;

    public Object A00() {
        Object obj;
        if (!(this instanceof AnonymousClass1S4)) {
            return this.A03.poll();
        }
        AnonymousClass1S4 r4 = (AnonymousClass1S4) this;
        A26 a26 = (A26) r4.A03.poll();
        SoftReference softReference = a26.A00;
        if (softReference == null) {
            obj = null;
        } else {
            obj = softReference.get();
        }
        SoftReference softReference2 = a26.A00;
        if (softReference2 != null) {
            softReference2.clear();
            a26.A00 = null;
        }
        SoftReference softReference3 = a26.A01;
        if (softReference3 != null) {
            softReference3.clear();
            a26.A01 = null;
        }
        SoftReference softReference4 = a26.A02;
        if (softReference4 != null) {
            softReference4.clear();
            a26.A02 = null;
        }
        r4.A00.add(a26);
        return obj;
    }

    public void A01(Object obj) {
        if (!(this instanceof AnonymousClass1S4)) {
            this.A03.add(obj);
            return;
        }
        AnonymousClass1S4 r2 = (AnonymousClass1S4) this;
        A26 a26 = (A26) r2.A00.poll();
        if (a26 == null) {
            a26 = new A26();
        }
        a26.A00 = new SoftReference(obj);
        a26.A01 = new SoftReference(obj);
        a26.A02 = new SoftReference(obj);
        r2.A03.add(a26);
    }

    public C22911Nh(int i, int i2, int i3, boolean z) {
        boolean z2 = true;
        C05520Zg.A05(i > 0);
        C05520Zg.A05(i2 >= 0);
        C05520Zg.A05(i3 < 0 ? false : z2);
        this.A01 = i;
        this.A02 = i2;
        this.A03 = new LinkedList();
        this.A00 = i3;
        this.A04 = z;
    }
}
