package X;

import android.graphics.Bitmap;

/* renamed from: X.1iV  reason: invalid class name and case insensitive filesystem */
public final class C30701iV implements C22761Ms {
    public final /* synthetic */ Bitmap.Config A00;
    public final /* synthetic */ AnonymousClass1O0 A01;

    public C30701iV(AnonymousClass1O0 r1, Bitmap.Config config) {
        this.A01 = r1;
        this.A00 = config;
    }

    public AnonymousClass1S9 AWg(AnonymousClass1NY r3, int i, C33271nJ r5, C23541Px r6) {
        return this.A01.A00.A04(r3, r6, this.A00);
    }
}
