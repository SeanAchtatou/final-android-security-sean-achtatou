package X;

import com.facebook.common.util.TriState;

/* renamed from: X.1yF  reason: invalid class name and case insensitive filesystem */
public final class C38941yF {
    public TriState A00;
    private TriState A01;

    public static final C38941yF A00() {
        return new C38941yF();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        if (r1.asBoolean() == false) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01() {
        /*
            r3 = this;
            com.facebook.common.util.TriState r1 = r3.A00
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r1 != r0) goto L_0x001d
            java.lang.String r1 = "true"
            java.lang.String r0 = "fb.running_sapienz"
            java.lang.String r0 = X.AnonymousClass00I.A02(r0)     // Catch:{ all -> 0x0019 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0019 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)     // Catch:{ all -> 0x0019 }
            r3.A00 = r0     // Catch:{ all -> 0x0019 }
            goto L_0x001d
        L_0x0019:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO
            r3.A00 = r0
        L_0x001d:
            com.facebook.common.util.TriState r1 = r3.A00
            boolean r0 = r1.isSet()
            if (r0 == 0) goto L_0x002c
            boolean r1 = r1.asBoolean()
            r0 = 1
            if (r1 != 0) goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            r2 = 0
            if (r0 == 0) goto L_0x005c
            com.facebook.common.util.TriState r1 = r3.A01
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r1 != r0) goto L_0x004d
            java.lang.String r1 = "true"
            java.lang.String r0 = "fb.running_leak_detection"
            java.lang.String r0 = X.AnonymousClass00I.A02(r0)     // Catch:{ all -> 0x0049 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0049 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)     // Catch:{ all -> 0x0049 }
            r3.A01 = r0     // Catch:{ all -> 0x0049 }
            goto L_0x004d
        L_0x0049:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO
            r3.A01 = r0
        L_0x004d:
            com.facebook.common.util.TriState r1 = r3.A01
            boolean r0 = r1.isSet()
            if (r0 == 0) goto L_0x005c
            boolean r0 = r1.asBoolean()
            if (r0 == 0) goto L_0x005c
            r2 = 1
        L_0x005c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38941yF.A01():boolean");
    }

    public C38941yF() {
        TriState triState = TriState.UNSET;
        this.A00 = triState;
        this.A01 = triState;
    }
}
