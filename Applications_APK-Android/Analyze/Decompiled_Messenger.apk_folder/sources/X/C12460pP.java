package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0pP  reason: invalid class name and case insensitive filesystem */
public final class C12460pP {
    private static volatile C12460pP A03;
    public AnonymousClass0UN A00;
    public Set A01;
    public final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A05.A09("runtime_permissions/")).A09("permission_requested"));

    public boolean A08(String str) {
        try {
            if (!str.equalsIgnoreCase("android.permission.SYSTEM_ALERT_WINDOW") || Build.VERSION.SDK_INT < 23) {
                return (str.equalsIgnoreCase(TurboLoader.Locator.$const$string(1)) && Build.VERSION.SDK_INT < 19) || ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A00)).checkCallingOrSelfPermission(str) == 0;
            }
            return A05();
        } catch (RuntimeException unused) {
            return false;
        }
    }

    public boolean A09(String[] strArr) {
        for (String A08 : strArr) {
            if (!A08(A08)) {
                return false;
            }
        }
        return true;
    }

    public static Intent A00(C12460pP r5) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.parse(AnonymousClass08S.A0J("package:", ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r5.A00)).getPackageName())));
        return intent;
    }

    public static final C12460pP A02(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C12460pP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C12460pP(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public Intent A03(boolean z) {
        Intent intent = new Intent(AnonymousClass80H.$const$string(46));
        intent.setData(Uri.parse(AnonymousClass08S.A0J("package:", ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A00)).getPackageName())));
        if (z) {
            intent.addFlags(268435456);
        }
        return intent;
    }

    public boolean A05() {
        if (Build.VERSION.SDK_INT < 23) {
            return A08("android.permission.SYSTEM_ALERT_WINDOW");
        }
        return Settings.canDrawOverlays((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A00));
    }

    public boolean A07(Activity activity, String str) {
        boolean contains;
        if (this.A01.isEmpty()) {
            contains = true;
        } else {
            contains = this.A01.contains(str);
        }
        if (!contains || A08(str) || activity.shouldShowRequestPermissionRationale(str)) {
            return false;
        }
        return true;
    }

    public String[] A0A(Activity activity, String[] strArr) {
        boolean contains;
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (this.A01.isEmpty()) {
                contains = true;
            } else {
                contains = this.A01.contains(str);
            }
            boolean z = true;
            if (contains && (A08(str) || !activity.shouldShowRequestPermissionRationale(str))) {
                z = false;
            }
            if (z) {
                arrayList.add(str);
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    private C12460pP(AnonymousClass1XY r7) {
        String str;
        String str2;
        AnonymousClass0UN r1 = new AnonymousClass0UN(4, r7);
        this.A00 = r1;
        HashSet hashSet = new HashSet();
        try {
            PackageManager packageManager = ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r1)).getPackageManager();
            List<PermissionGroupInfo> allPermissionGroups = packageManager.getAllPermissionGroups(0);
            if (allPermissionGroups != null) {
                for (PermissionGroupInfo permissionGroupInfo : allPermissionGroups) {
                    for (PermissionInfo permissionInfo : packageManager.queryPermissionsByGroup(permissionGroupInfo.name, 0)) {
                        hashSet.add(permissionInfo.name);
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e = e;
            str = "RuntimePermissionsUtil";
            str2 = "Error getting all permissions: ";
            C010708t.A0M(str, str2, e);
            hashSet = new HashSet();
            this.A01 = hashSet;
        } catch (RuntimeException e2) {
            e = e2;
            str = "RuntimePermissionsUtil";
            str2 = "getAllPermissions caught Exception";
            C010708t.A0M(str, str2, e);
            hashSet = new HashSet();
            this.A01 = hashSet;
        }
        this.A01 = hashSet;
    }

    public static final C12460pP A01(AnonymousClass1XY r0) {
        return A02(r0);
    }

    public void A04() {
        Intent A002 = A00(this);
        A002.addFlags(268435456);
        int i = AnonymousClass1Y3.AT5;
        AnonymousClass0UN r3 = this.A00;
        ((C12480pR) AnonymousClass1XX.A02(2, i, r3)).A06.A0B(A002, (Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, r3));
    }

    public boolean A06(Activity activity, String str) {
        if (!A07(activity, str) || (!((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, this.A00)).Aep((AnonymousClass1Y7) this.A02.A09(str), false))) {
            return false;
        }
        return true;
    }
}
