package X;

/* renamed from: X.1uM  reason: invalid class name and case insensitive filesystem */
public final class C36941uM {
    public final C05920aY A00;
    public final C05920aY A01;
    private final C36961uO A02 = new C32361ld();
    private final C36961uO A03 = new C36951uN(this);

    public static final C36941uM A00(AnonymousClass1XY r3) {
        return new C36941uM(AnonymousClass0XJ.A02(r3), C10580kT.A01(r3));
    }

    public C36961uO A01(C26941cO r3) {
        if (r3 != null && r3.B9R() != null) {
            this.A00.Byt(r3.B9R());
            this.A01.Byt(r3.B9R());
        } else if (this.A01.Awn() == null) {
            return this.A02;
        } else {
            this.A00.Byt(this.A01.Awn());
            C05920aY r1 = this.A01;
            r1.Byt(r1.Awn());
        }
        return this.A03;
    }

    public C36941uM(C05920aY r2, C05920aY r3) {
        this.A00 = r2;
        this.A01 = r3;
    }
}
