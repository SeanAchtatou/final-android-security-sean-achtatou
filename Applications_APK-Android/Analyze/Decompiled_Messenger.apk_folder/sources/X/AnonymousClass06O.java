package X;

import android.text.TextUtils;
import java.util.HashSet;

/* renamed from: X.06O  reason: invalid class name */
public final class AnonymousClass06O implements AnonymousClass0Z1 {
    public String Amj() {
        return "LOADED_APPLICATION_MODULES";
    }

    public static final AnonymousClass06O A00() {
        return new AnonymousClass06O();
    }

    public String getCustomData(Throwable th) {
        HashSet hashSet;
        String join;
        String sb;
        AnonymousClass0Eq A00 = AnonymousClass0Eq.A00();
        synchronized (A00) {
            synchronized (A00) {
                hashSet = new HashSet();
                for (int i = 0; i < 10; i++) {
                    if (AnonymousClass0Eq.A02(A00, i, 1)) {
                        hashSet.add(AnonymousClass0GL.A01(i));
                    }
                }
            }
            join = TextUtils.join(", ", hashSet);
        }
        AnonymousClass0Eq A002 = AnonymousClass0Eq.A00();
        synchronized (A002) {
            StringBuilder sb2 = new StringBuilder();
            for (int i2 = 0; i2 < 10; i2++) {
                if (AnonymousClass0Eq.A02(A002, i2, 2)) {
                    if (sb2.length() > 0) {
                        sb2.append(", ");
                    }
                    sb2.append(AnonymousClass0GL.A01(i2));
                }
            }
            sb = sb2.toString();
        }
        return AnonymousClass08S.A0S("Loaded modules=", join, ", Initialized modules=", sb);
    }
}
