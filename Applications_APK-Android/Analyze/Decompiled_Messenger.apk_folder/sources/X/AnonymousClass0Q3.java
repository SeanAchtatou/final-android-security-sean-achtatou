package X;

import java.io.FileNotFoundException;

/* renamed from: X.0Q3  reason: invalid class name */
public final class AnonymousClass0Q3 extends FileNotFoundException {
    public AnonymousClass0Q3(String str, String str2) {
        super(AnonymousClass08S.A0S("Module: ", str, " not found in path: ", str2));
    }
}
