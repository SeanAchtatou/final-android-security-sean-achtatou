package X;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0pd  reason: invalid class name and case insensitive filesystem */
public class C12580pd extends C12590pe implements C12950qG {
    public static final String __redex_internal_original_name = "androidx.fragment.app.CustomDialogFragment";

    public int A27(C16290wo r3, String str, boolean z) {
        int A02;
        this.A06 = false;
        this.A07 = true;
        r3.A0C(this, str);
        this.A08 = false;
        if (z) {
            A02 = r3.A03();
        } else {
            A02 = r3.A02();
        }
        this.A00 = A02;
        return A02;
    }

    public void A28(Bundle bundle) {
    }

    public void A29(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, View view) {
    }

    public void A2A(boolean z, boolean z2) {
    }

    public LayoutInflater A13(Bundle bundle) {
        if (this.A0A) {
            return super.A13(bundle);
        }
        C13020qR r0 = this.A0N;
        if (r0 != null) {
            LayoutInflater cloneInContext = r0.A02().cloneInContext(this.A0N.A01);
            A17();
            AnonymousClass14W.A00(cloneInContext, this.A0O.A0O);
            return cloneInContext;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }

    public void A1T(boolean z) {
        if (this.A0a != z && this.A0e) {
            super.A1T(z);
        }
    }

    public final void A1W(boolean z) {
        C13060qW r0;
        boolean z2 = this.A0i;
        if (!z2 && z && this.A0F < 3 && (r0 = this.A0P) != null) {
            r0.A0o(this);
        }
        super.A1W(z);
        A2A(z, z2);
    }

    public void A1C() {
        super.A1C();
    }

    public void A1D() {
        super.A1D();
    }

    public void A1E() {
        super.A1E();
    }

    public void A1F() {
        super.A1F();
    }

    public void A1G() {
        super.A1G();
    }

    public void A1H() {
        super.A1H();
    }

    public void A1M(Bundle bundle) {
        super.A1M(bundle);
    }

    public void A1N(Bundle bundle) {
        A28(bundle);
        super.A1N(bundle);
    }

    public void A1Q(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        try {
            super.A1Q(layoutInflater, viewGroup, bundle);
        } finally {
            A29(layoutInflater, viewGroup, bundle, this.A0I);
        }
    }
}
