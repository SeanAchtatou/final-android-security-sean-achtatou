package X;

/* renamed from: X.0bk  reason: invalid class name and case insensitive filesystem */
public final class C06590bk extends AnonymousClass0Wl implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.startup.Inbox2Prefetcher";
    private AnonymousClass0UN A00;

    public String getSimpleName() {
        return "Inbox2Prefetcher";
    }

    public static final C06590bk A00(AnonymousClass1XY r1) {
        return new C06590bk(r1);
    }

    public void run() {
        ((AnonymousClass0WE) AnonymousClass1XX.A03(AnonymousClass1Y3.BEI, this.A00)).A03();
        if (!((AnonymousClass1YI) AnonymousClass1XX.A03(AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A6M, false)) {
            ((C05530Zh) AnonymousClass1XX.A03(AnonymousClass1Y3.AFN, this.A00)).A03(new C09500hT(C09510hU.DO_NOT_CHECK_SERVER, C25611a7.TOP, "REGULAR"));
        }
    }

    private C06590bk(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public void init() {
        int A03 = C000700l.A03(-1569284425);
        if (((C001500z) AnonymousClass1XX.A03(AnonymousClass1Y3.BH4, this.A00)) == C001500z.A07) {
            new Thread(this).start();
        }
        C000700l.A09(-658698071, A03);
    }
}
