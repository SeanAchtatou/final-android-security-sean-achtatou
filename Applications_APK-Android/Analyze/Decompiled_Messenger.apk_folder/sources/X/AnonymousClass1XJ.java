package X;

import android.os.Handler;

/* renamed from: X.1XJ  reason: invalid class name */
public final class AnonymousClass1XJ implements C11410n6 {
    public static final AnonymousClass1XJ A08 = new AnonymousClass1XJ();
    public int A00 = 0;
    public int A01 = 0;
    public Handler A02;
    public AnonymousClass1XP A03 = new AnonymousClass1XO(this);
    public Runnable A04 = new AnonymousClass1XN(this);
    public boolean A05 = true;
    public boolean A06 = true;
    public final C12990qL A07 = new C12990qL(this);

    public AnonymousClass0qM AsK() {
        return this.A07;
    }

    private AnonymousClass1XJ() {
    }
}
