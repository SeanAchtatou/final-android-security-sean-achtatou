package X;

import com.facebook.proxygen.SamplePolicy;
import com.facebook.proxygen.TraceEventHandler;
import org.apache.http.protocol.HttpContext;

/* renamed from: X.1W6  reason: invalid class name */
public final class AnonymousClass1W6 implements AnonymousClass1W7 {
    public final /* synthetic */ TraceEventHandler A00;

    public AnonymousClass1W6(TraceEventHandler traceEventHandler) {
        this.A00 = traceEventHandler;
    }

    public TraceEventHandler AbE(String str, HttpContext httpContext, AnonymousClass2ZJ r4, SamplePolicy samplePolicy) {
        return this.A00;
    }
}
