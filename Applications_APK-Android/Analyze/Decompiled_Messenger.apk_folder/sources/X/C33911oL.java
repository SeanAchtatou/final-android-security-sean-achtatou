package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1oL  reason: invalid class name and case insensitive filesystem */
public final class C33911oL extends Enum {
    private static final /* synthetic */ C33911oL[] A00;
    public static final C33911oL A01;
    public static final C33911oL A02;
    public static final C33911oL A03;
    public static final C33911oL A04;
    public static final C33911oL A05;
    public static final C33911oL A06;
    public static final C33911oL A07;
    public static final C33911oL A08;

    static {
        C33911oL r2 = new C33911oL("UNUSED", 0);
        C33911oL r3 = new C33911oL("VOIP", 1);
        A07 = r3;
        C33911oL r4 = new C33911oL("BACKGROUND_LOCATION", 2);
        C33911oL r5 = new C33911oL("VOIP_WEB", 3);
        A08 = r5;
        C33911oL r6 = new C33911oL("MQTT_AGGRESSIVELY_NOTIFY", 4);
        A05 = r6;
        C33911oL r7 = new C33911oL("VIDEO", 5);
        C33911oL r8 = new C33911oL("ONE_ON_ONE_OVER_MULTIWAY", 6);
        A06 = r8;
        C33911oL r9 = new C33911oL("SHARED_SECRET", 7);
        C33911oL r10 = new C33911oL("USER_AND_DEVICE_AUTH", 8);
        C33911oL r11 = new C33911oL("ALOHA_ENABLED", 9);
        A04 = r11;
        C33911oL r12 = new C33911oL("ALOHA_PRIVATE_MODE", 10);
        C33911oL r13 = new C33911oL("ACTIVE_ON_WEB", 11);
        A03 = r13;
        C33911oL r14 = new C33911oL("ACTIVE_ON_MESSENGER_APP", 12);
        A02 = r14;
        C33911oL r15 = new C33911oL("ACTIVE_ON_FACEBOOK_APP", 13);
        A01 = r15;
        A00 = new C33911oL[]{r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15};
    }

    public static C33911oL valueOf(String str) {
        return (C33911oL) Enum.valueOf(C33911oL.class, str);
    }

    public static C33911oL[] values() {
        return (C33911oL[]) A00.clone();
    }

    private C33911oL(String str, int i) {
    }
}
