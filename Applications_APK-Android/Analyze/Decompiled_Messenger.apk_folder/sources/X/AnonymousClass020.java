package X;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* renamed from: X.020  reason: invalid class name */
public final class AnonymousClass020 extends WeakReference {
    public AnonymousClass020 A00 = this;
    public AnonymousClass020 A01 = this;
    public AnonymousClass020 A02 = this;
    public AnonymousClass020 A03 = this;

    public void A00() {
        AnonymousClass020 r0;
        AnonymousClass020 r02;
        AnonymousClass020 r1 = this.A01;
        if (!(r1 == null || (r02 = this.A02) == null)) {
            r1.A02 = r02;
            this.A02.A01 = r1;
            this.A02 = this;
            this.A01 = this;
        }
        AnonymousClass020 r12 = this.A03;
        if (r12 != null && (r0 = this.A00) != null) {
            r12.A00 = r0;
            this.A00.A03 = r12;
            this.A00 = this;
            this.A03 = this;
        }
    }

    public AnonymousClass020(Object obj) {
        super(obj);
    }

    public AnonymousClass020(Object obj, ReferenceQueue referenceQueue) {
        super(obj, referenceQueue);
    }
}
