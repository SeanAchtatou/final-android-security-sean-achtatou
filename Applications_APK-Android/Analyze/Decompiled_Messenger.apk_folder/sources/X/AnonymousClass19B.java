package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.19B  reason: invalid class name */
public final class AnonymousClass19B extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static final Object A01 = new Object();

    public static final AnonymousClass19G A00(AnonymousClass1XY r4) {
        AnonymousClass19G r0;
        synchronized (A01) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r4)) {
                    A00.A00 = new C14460tN((AnonymousClass1XY) A00.A01()).A00("MESSENGER_INBOX2");
                }
                C05540Zi r1 = A00;
                r0 = (AnonymousClass19G) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }
}
