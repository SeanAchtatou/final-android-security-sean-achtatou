package X;

import com.facebook.omnistore.Collection;
import java.util.concurrent.ScheduledExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Uj  reason: invalid class name */
public final class AnonymousClass1Uj {
    private static volatile AnonymousClass1Uj A07;
    public long A00;
    public Collection A01;
    public final AnonymousClass069 A02;
    public final AnonymousClass1F1 A03;
    public final AnonymousClass1F6 A04;
    public final Object A05 = new Object();
    public final ScheduledExecutorService A06;

    public static final AnonymousClass1Uj A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (AnonymousClass1Uj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new AnonymousClass1Uj(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    private AnonymousClass1Uj(AnonymousClass1XY r3) {
        new AnonymousClass0UN(1, r3);
        this.A03 = AnonymousClass1F1.A02(r3);
        this.A04 = AnonymousClass1F6.A00(r3);
        this.A06 = AnonymousClass0UX.A0f(r3);
        this.A02 = AnonymousClass067.A03(r3);
    }
}
