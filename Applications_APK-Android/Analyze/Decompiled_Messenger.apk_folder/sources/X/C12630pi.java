package X;

import com.facebook.common.util.JSONUtil;
import com.facebook.user.model.WorkUserInfo;
import com.facebook.user.profilepic.PicSquare;
import com.facebook.user.profilepic.PicSquareUrlWithSize;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.io.IOException;
import java.util.Iterator;

/* renamed from: X.0pi  reason: invalid class name and case insensitive filesystem */
public final class C12630pi {
    public final AnonymousClass06B A00;
    public final AnonymousClass0jJ A01;

    public static final C12630pi A01() {
        return new C12630pi(AnonymousClass067.A02(), C05040Xk.A04());
    }

    public static final C12630pi A02(AnonymousClass1XY r2) {
        return new C12630pi(AnonymousClass067.A02(), C05040Xk.A04());
    }

    public static JsonNode A04(ImmutableList immutableList) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        objectNode.put("commerce_faq_enabled", immutableList.contains(C37891wW.COMMERCE_FAQ_ENABLED));
        objectNode.put("in_messenger_shopping_enabled", immutableList.contains(C37891wW.IN_MESSENGER_SHOPPING_ENABLED));
        objectNode.put("commerce_nux_enabled", immutableList.contains(C37891wW.COMMERCE_NUX_ENABLED));
        objectNode.put("structured_menu_enabled", immutableList.contains(C37891wW.STRUCTURED_MENU_ENABLED));
        objectNode.put("user_control_topic_manage_enabled", immutableList.contains(C37891wW.USER_CONTROL_TOPIC_MANAGE_ENABLED));
        objectNode.put("null_state_cta_button_always_enabled", immutableList.contains(C37891wW.NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED));
        objectNode.put("composer_input_disabled", immutableList.contains(C37891wW.COMPOSER_INPUT_DISABLED));
        return objectNode;
    }

    public static ImmutableList A05(String str) {
        if (str == null || str.equals("[]")) {
            return RegularImmutableList.A02;
        }
        try {
            return (ImmutableList) AnonymousClass0jE.getInstance().readValue(str, new C1052751r());
        } catch (IOException e) {
            throw new RuntimeException("Unexpected serialization exception", e);
        }
    }

    public static String A06(WorkUserInfo workUserInfo) {
        if (workUserInfo == null) {
            return null;
        }
        try {
            return AnonymousClass0jE.getInstance().writeValueAsString(workUserInfo);
        } catch (IOException e) {
            throw new RuntimeException("Unexpected serialization exception", e);
        }
    }

    public JsonNode A08(PicSquare picSquare) {
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        C24971Xv it = picSquare.mPicSquareUrlsWithSizes.iterator();
        while (it.hasNext()) {
            PicSquareUrlWithSize picSquareUrlWithSize = (PicSquareUrlWithSize) it.next();
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("url", picSquareUrlWithSize.url);
            objectNode.put("size", picSquareUrlWithSize.size);
            arrayNode.add(objectNode);
        }
        return arrayNode;
    }

    private C12630pi(AnonymousClass06B r1, AnonymousClass0jJ r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static WorkUserInfo A00(String str) {
        if (C06850cB.A0B(str)) {
            return null;
        }
        try {
            return (WorkUserInfo) AnonymousClass0jE.getInstance().readValue(str, new C1052651q());
        } catch (IOException e) {
            throw new RuntimeException("Unexpected deserialization exception", e);
        }
    }

    public static PicSquare A03(JsonNode jsonNode) {
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = jsonNode.iterator();
        while (it.hasNext()) {
            JsonNode jsonNode2 = (JsonNode) it.next();
            builder.add((Object) new PicSquareUrlWithSize(JSONUtil.A04(jsonNode2.get("size")), JSONUtil.A0N(jsonNode2.get("url"))));
        }
        return new PicSquare(builder.build());
    }

    public static String A07(ImmutableList immutableList) {
        try {
            return AnonymousClass0jE.getInstance().writeValueAsString(immutableList);
        } catch (IOException e) {
            throw new RuntimeException("Unexpected serialization exception", e);
        }
    }
}
