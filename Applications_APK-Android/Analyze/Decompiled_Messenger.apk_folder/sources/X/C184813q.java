package X;

import android.content.Context;
import androidx.fragment.app.Fragment;
import com.facebook.messaging.threadlist.ui.extendedfab.ExtendedFloatingActionButton;
import com.facebook.orca.threadlist.ThreadListFragment;
import com.facebook.widget.recyclerview.BetterRecyclerView;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.13q  reason: invalid class name and case insensitive filesystem */
public final class C184813q implements AnonymousClass13r {
    private static volatile C184813q A01;
    public AnonymousClass0UN A00;

    public void BST(boolean z) {
    }

    public static final C184813q A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C184813q.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C184813q(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public /* bridge */ /* synthetic */ void AP7(Context context, Fragment fragment, C33691nz r7) {
        ThreadListFragment threadListFragment = (ThreadListFragment) fragment;
        threadListFragment.A1B = new C186414n((C186314m) AnonymousClass1XX.A03(AnonymousClass1Y3.B1F, this.A00), context, threadListFragment.A0P, r7);
        threadListFragment.A19 = new C187715a(r7, context);
        C187915c r3 = new C187915c(r7, context);
        C27814DjR djR = threadListFragment.A0p;
        if (djR != null) {
            C138266cq r1 = new C138266cq(r3, (C13410rO) AnonymousClass1XX.A03(AnonymousClass1Y3.Ajr, djR.A02));
            djR.A00 = r1;
            ExtendedFloatingActionButton extendedFloatingActionButton = djR.A03;
            if (extendedFloatingActionButton != null) {
                extendedFloatingActionButton.setOnClickListener(r1);
            }
        }
    }

    public Fragment AV6() {
        return new ThreadListFragment();
    }

    public C15390vD Ae6() {
        return (C15390vD) AnonymousClass1XX.A03(AnonymousClass1Y3.BR0, this.A00);
    }

    public Class AiR() {
        return ThreadListFragment.class;
    }

    public AnonymousClass10Z B5D(Context context) {
        return new AnonymousClass10Z(AnonymousClass01R.A03(context, ((AnonymousClass1MT) AnonymousClass1XX.A03(AnonymousClass1Y3.AJa, this.A00)).A03(C29631gj.A1L, AnonymousClass07B.A0N)), 20, -1, Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public ImmutableList B6e() {
        C34851qH r4 = (C34851qH) AnonymousClass1XX.A03(AnonymousClass1Y3.AN8, this.A00);
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C34861qI r5 = (C34861qI) AnonymousClass1XX.A03(AnonymousClass1Y3.AzU, r4.A00);
        if (!((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r5.A00)).Aem(282406984615287L)) {
            C34871qJ r7 = (C34871qJ) AnonymousClass1XX.A03(AnonymousClass1Y3.AQQ, r4.A00);
            if (((C34881qK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BUW, r4.A00)).A01(26) && ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r7.A00)).Aem(287397736619437L)) {
                builder.add((Object) new AnonymousClass3AK((C53542kx) AnonymousClass1XX.A03(AnonymousClass1Y3.AOw, r4.A00), "inbox"));
            }
            if (((C34881qK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BUW, r4.A00)).A01(27)) {
                builder.add((Object) ((C193817x) AnonymousClass1XX.A03(AnonymousClass1Y3.Agt, r4.A00)));
            }
            if (((C34881qK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BUW, r4.A00)).A01(28) && !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r5.A00)).Aem(282406984680824L)) {
                builder.add((Object) ((C15550vT) AnonymousClass1XX.A03(AnonymousClass1Y3.BM0, r4.A00)));
            }
        }
        return builder.build();
    }

    public boolean BGe(Fragment fragment) {
        BetterRecyclerView betterRecyclerView = ((ThreadListFragment) fragment).A1F;
        if (betterRecyclerView == null || !betterRecyclerView.canScrollVertically(-1)) {
            return true;
        }
        return false;
    }

    public void C4d(Fragment fragment) {
        ThreadListFragment threadListFragment = (ThreadListFragment) fragment;
        int AZq = threadListFragment.A0T.AZq();
        int AZs = ((threadListFragment.A0T.AZs() - AZq) + 1) << 1;
        if (AZq > AZs) {
            threadListFragment.A1F.A0j(AZs);
        }
        threadListFragment.A1F.A0m(0);
    }

    public void CBh(Fragment fragment, C33761o6 r3) {
        ((ThreadListFragment) fragment).A1A = new C35201qq(r3);
    }

    private C184813q(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public String B5E(Context context) {
        return context.getString(2131825808);
    }

    public String B6U(Context context) {
        return context.getString(2131825809);
    }
}
