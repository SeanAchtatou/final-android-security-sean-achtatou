package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import java.util.BitSet;

/* renamed from: X.1GZ  reason: invalid class name */
public final class AnonymousClass1GZ extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A01;
    @Comparable(type = 13)
    public C15400vE A02;
    @Comparable(type = 13)
    public C15350v9 A03;

    public AnonymousClass1GZ() {
        super("OrbitBadgeOverlay");
    }

    public C17770zR A0O(AnonymousClass0p4 r12) {
        String[] strArr;
        BitSet bitSet;
        C107875Dz r4;
        MigColorScheme migColorScheme;
        int i;
        String str;
        C17770zR A16;
        C17770zR A162;
        C17770zR A163;
        String[] strArr2;
        BitSet bitSet2;
        AnonymousClass5Tk r3;
        MigColorScheme migColorScheme2;
        int i2;
        C17770zR A164;
        C17770zR A165;
        C17770zR A166;
        C15400vE r32 = this.A02;
        C17770zR r2 = this.A01;
        C15350v9 r1 = this.A03;
        int i3 = this.A00;
        boolean z = false;
        if (r32.A00 <= 0) {
            z = true;
        }
        if (z) {
            return r2;
        }
        switch (r32.A01.ordinal()) {
            case 0:
                strArr = new String[]{"badgeColor", "badgeText", "borderColor", "content", "size", "textColor"};
                bitSet = new BitSet(6);
                r4 = new C107875Dz();
                C17770zR r7 = r12.A04;
                if (r7 != null) {
                    r4.A07 = r7.A06;
                }
                bitSet.clear();
                if (r2 == null) {
                    A16 = null;
                } else {
                    A16 = r2.A16();
                }
                r4.A04 = A16;
                bitSet.set(3);
                str = C75183jN.A00(r12.A09, r32.A00);
                r4.A05 = str;
                bitSet.set(1);
                migColorScheme = r1.A00;
                r4.A00 = migColorScheme.B0I();
                bitSet.set(0);
                i = -1;
                r4.A03 = i;
                bitSet.set(5);
                r4.A01 = AnonymousClass1KA.A00(migColorScheme.B9n());
                bitSet.set(2);
                r4.A02 = i3;
                bitSet.set(4);
                r4.A14().CNK(i3);
                r4.A14().BC2(i3);
                AnonymousClass11F.A0C(6, bitSet, strArr);
                return r4;
            case 1:
                strArr = new String[]{"badgeColor", "badgeText", "borderColor", "content", "size", "textColor"};
                bitSet = new BitSet(6);
                r4 = new C107875Dz();
                C17770zR r72 = r12.A04;
                if (r72 != null) {
                    r4.A07 = r72.A06;
                }
                bitSet.clear();
                if (r2 == null) {
                    A162 = null;
                } else {
                    A162 = r2.A16();
                }
                r4.A04 = A162;
                bitSet.set(3);
                str = C75183jN.A01(r12.A09, r32.A00);
                r4.A05 = str;
                bitSet.set(1);
                migColorScheme = r1.A00;
                r4.A00 = migColorScheme.B0I();
                bitSet.set(0);
                i = -1;
                r4.A03 = i;
                bitSet.set(5);
                r4.A01 = AnonymousClass1KA.A00(migColorScheme.B9n());
                bitSet.set(2);
                r4.A02 = i3;
                bitSet.set(4);
                r4.A14().CNK(i3);
                r4.A14().BC2(i3);
                AnonymousClass11F.A0C(6, bitSet, strArr);
                return r4;
            case 2:
                strArr = new String[]{"badgeColor", "badgeText", "borderColor", "content", "size", "textColor"};
                bitSet = new BitSet(6);
                r4 = new C107875Dz();
                C17770zR r73 = r12.A04;
                if (r73 != null) {
                    r4.A07 = r73.A06;
                }
                bitSet.clear();
                if (r2 == null) {
                    A163 = null;
                } else {
                    A163 = r2.A16();
                }
                r4.A04 = A163;
                bitSet.set(3);
                r4.A05 = C75183jN.A00(r12.A09, r32.A00);
                bitSet.set(1);
                migColorScheme = r1.A00;
                r4.A00 = migColorScheme.Abs();
                bitSet.set(0);
                i = migColorScheme.AoD();
                r4.A03 = i;
                bitSet.set(5);
                r4.A01 = AnonymousClass1KA.A00(migColorScheme.B9n());
                bitSet.set(2);
                r4.A02 = i3;
                bitSet.set(4);
                r4.A14().CNK(i3);
                r4.A14().BC2(i3);
                AnonymousClass11F.A0C(6, bitSet, strArr);
                return r4;
            case 3:
                strArr2 = new String[]{"badgeColor", "borderColor", "content", "size"};
                bitSet2 = new BitSet(4);
                r3 = new AnonymousClass5Tk();
                C17770zR r6 = r12.A04;
                if (r6 != null) {
                    r3.A07 = r6.A06;
                }
                bitSet2.clear();
                if (r2 == null) {
                    A164 = null;
                } else {
                    A164 = r2.A16();
                }
                r3.A03 = A164;
                bitSet2.set(2);
                migColorScheme2 = r1.A00;
                i2 = migColorScheme2.B0I();
                r3.A00 = i2;
                bitSet2.set(0);
                r3.A01 = AnonymousClass1KA.A00(migColorScheme2.B9n());
                bitSet2.set(1);
                r3.A02 = i3;
                bitSet2.set(3);
                r3.A14().CNK(i3);
                r3.A14().BC2(i3);
                AnonymousClass11F.A0C(4, bitSet2, strArr2);
                return r3;
            case 4:
                strArr2 = new String[]{"badgeColor", "borderColor", "content", "size"};
                bitSet2 = new BitSet(4);
                r3 = new AnonymousClass5Tk();
                C17770zR r62 = r12.A04;
                if (r62 != null) {
                    r3.A07 = r62.A06;
                }
                bitSet2.clear();
                if (r2 == null) {
                    A165 = null;
                } else {
                    A165 = r2.A16();
                }
                r3.A03 = A165;
                bitSet2.set(2);
                migColorScheme2 = r1.A00;
                i2 = migColorScheme2.AoD();
                r3.A00 = i2;
                bitSet2.set(0);
                r3.A01 = AnonymousClass1KA.A00(migColorScheme2.B9n());
                bitSet2.set(1);
                r3.A02 = i3;
                bitSet2.set(3);
                r3.A14().CNK(i3);
                r3.A14().BC2(i3);
                AnonymousClass11F.A0C(4, bitSet2, strArr2);
                return r3;
            case 5:
                String[] strArr3 = {"borderColor", "content", "size"};
                BitSet bitSet3 = new BitSet(3);
                C107645Cz r33 = new C107645Cz();
                C17770zR r63 = r12.A04;
                if (r63 != null) {
                    r33.A07 = r63.A06;
                }
                bitSet3.clear();
                if (r2 == null) {
                    A166 = null;
                } else {
                    A166 = r2.A16();
                }
                r33.A02 = A166;
                bitSet3.set(1);
                r33.A00 = AnonymousClass1KA.A00(r1.A00.B9n());
                bitSet3.set(0);
                r33.A01 = i3;
                bitSet3.set(2);
                r33.A14().CNK(i3);
                r33.A14().BC2(i3);
                AnonymousClass11F.A0C(3, bitSet3, strArr3);
                return r33;
            default:
                throw new IllegalStateException();
        }
    }

    public C17770zR A16() {
        C17770zR r0;
        AnonymousClass1GZ r1 = (AnonymousClass1GZ) super.A16();
        C17770zR r02 = r1.A01;
        if (r02 != null) {
            r0 = r02.A16();
        } else {
            r0 = null;
        }
        r1.A01 = r0;
        return r1;
    }
}
