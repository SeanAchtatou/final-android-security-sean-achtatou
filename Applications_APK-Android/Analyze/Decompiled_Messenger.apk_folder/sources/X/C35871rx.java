package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1rx  reason: invalid class name and case insensitive filesystem */
public final class C35871rx {
    private static volatile C35871rx A01;
    public AnonymousClass0UN A00;

    public static final C35871rx A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C35871rx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C35871rx(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A01(String str) {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerPoint(30474250, str);
    }

    public void A02(boolean z, int i) {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(30474250, "is_from_cache", z);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(30474250, "fallback_build_number", i);
    }

    private C35871rx(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
