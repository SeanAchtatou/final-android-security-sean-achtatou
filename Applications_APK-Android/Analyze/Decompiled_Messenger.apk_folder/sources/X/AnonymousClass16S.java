package X;

import android.os.SystemClock;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.EnumSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.16S  reason: invalid class name */
public final class AnonymousClass16S {
    public long A00 = -1;
    public AnonymousClass0UN A01;
    public C189016a A02;
    public C34801qC A03;
    public AnonymousClass1G3 A04;
    public ImmutableList A05;
    public ImmutableList A06;
    public ScheduledFuture A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    private C20021Ap A0B;
    private AnonymousClass12A A0C;
    private ImmutableList A0D = RegularImmutableList.A02;
    private boolean A0E;
    private final AnonymousClass0ZM A0F = new AnonymousClass16U(this);

    public static final AnonymousClass16S A00(AnonymousClass1XY r2) {
        AnonymousClass16T A022 = AnonymousClass16T.A02(r2);
        C14970uR.A00(r2);
        return new AnonymousClass16S(r2, A022);
    }

    public static void A01(AnonymousClass16S r5) {
        if ((((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, r5.A01)).A0Y() || r5.A09) && r5.A08 && r5.A09) {
            r5.A04(true);
            if (r5.A0E && r5.A07 == null) {
                r5.A07 = ((ScheduledExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BKX, r5.A01)).schedule(new AnonymousClass185(r5), 1, TimeUnit.MINUTES);
                return;
            }
            return;
        }
        r5.A04(false);
        ((C20921Ei) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BMw, r5.A01)).A05();
    }

    public static void A02(AnonymousClass16S r7) {
        AnonymousClass1G3 r1;
        if (((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, r7.A01)).A0Y()) {
            long j = r7.A00;
            if (j == -1 || SystemClock.elapsedRealtime() - j <= 180000) {
                return;
            }
        }
        r7.A00 = -1;
        r7.A03 = C34801qC.A03;
        if (r7.A08 && (r1 = r7.A04) != null) {
            r1.A00(false);
        }
    }

    public static void A03(AnonymousClass16S r4) {
        if (!r4.A0A) {
            int i = AnonymousClass1Y3.Auh;
            AnonymousClass0UN r2 = r4.A01;
            if (((AnonymousClass0r6) AnonymousClass1XX.A02(1, i, r2)).A04 == C13430rQ.TP_FULL_LIST_RECEIVED || ((AnonymousClass1F8) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADw, r2)).A01("unknown")) {
                C20921Ei r3 = (C20921Ei) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BMw, r4.A01);
                if (C20921Ei.A03(r3)) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r3.A00)).markerPoint(5505198, "active_now_load_start");
                }
                r4.A02.A05();
            }
        }
    }

    private void A04(boolean z) {
        AnonymousClass1G3 r0;
        if (this.A0E != z) {
            this.A0E = z;
            if (z) {
                C189016a r1 = this.A02;
                r1.C6Z(this.A0B);
                r1.A05 = true;
                ((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, this.A01)).A0S(this.A0C);
                ((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, this.A01)).A0W(this);
                A03(this);
                return;
            }
            this.A02.ARp();
            this.A0A = false;
            C189016a r02 = this.A02;
            r02.C6Z(null);
            r02.A05 = false;
            ((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, this.A01)).A0T(this.A0C);
            ((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, this.A01)).A0X(this);
            ScheduledFuture scheduledFuture = this.A07;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(false);
            }
            this.A07 = null;
            if (this.A08 && (r0 = this.A04) != null) {
                r0.A00(false);
            }
        }
    }

    public C15820w2 A05() {
        return new C15820w2(((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, this.A01)).A0Y(), this.A03, this.A06, this.A05);
    }

    public void A06(boolean z) {
        this.A08 = z;
        if (z) {
            ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A01)).C0f(C34341pQ.A00, this.A0F);
        } else {
            ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A01)).CJr(C34341pQ.A00, this.A0F);
        }
        A02(this);
        A01(this);
    }

    public AnonymousClass16S(AnonymousClass1XY r3, AnonymousClass16T r4) {
        this.A01 = new AnonymousClass0UN(9, r3);
        this.A0C = new AnonymousClass16V(this);
        this.A02 = AnonymousClass16T.A00(r4, EnumSet.of(AnonymousClass16Y.A07), -1);
        this.A0B = new C189916j(this);
    }
}
