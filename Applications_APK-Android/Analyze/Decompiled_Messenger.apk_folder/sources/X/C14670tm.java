package X;

import java.util.concurrent.Executor;

/* renamed from: X.0tm  reason: invalid class name and case insensitive filesystem */
public final class C14670tm {
    public C14670tm A00;
    public final Runnable A01;
    public final Executor A02;

    public C14670tm(Runnable runnable, Executor executor) {
        this.A01 = runnable;
        this.A02 = executor;
    }
}
