package X;

/* renamed from: X.1eZ  reason: invalid class name and case insensitive filesystem */
public final class C28291eZ extends C28301ea {
    public C28291eZ _child = null;
    public int _columnNr;
    public String _currentName;
    public int _lineNr;
    public final C28291eZ _parent;

    public C28291eZ createChildArrayContext(int i, int i2) {
        C28291eZ r2 = this._child;
        if (r2 == null) {
            C28291eZ r0 = new C28291eZ(this, 1, i, i2);
            this._child = r0;
            return r0;
        }
        r2._type = 1;
        r2._index = -1;
        r2._lineNr = i;
        r2._columnNr = i2;
        r2._currentName = null;
        return r2;
    }

    public C28291eZ createChildObjectContext(int i, int i2) {
        C28291eZ r2 = this._child;
        if (r2 == null) {
            C28291eZ r0 = new C28291eZ(this, 2, i, i2);
            this._child = r0;
            return r0;
        }
        r2._type = 2;
        r2._index = -1;
        r2._lineNr = i;
        r2._columnNr = i2;
        r2._currentName = null;
        return r2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        int i = this._type;
        if (i == 0) {
            sb.append("/");
        } else if (i == 1) {
            sb.append('[');
            int i2 = this._index;
            if (i2 < 0) {
                i2 = 0;
            }
            sb.append(i2);
            sb.append(']');
        } else if (i == 2) {
            sb.append('{');
            String str = this._currentName;
            if (str != null) {
                sb.append('\"');
                AnonymousClass11z.appendQuoted(sb, str);
                sb.append('\"');
            } else {
                sb.append('?');
            }
            sb.append('}');
        }
        return sb.toString();
    }

    public C28291eZ(C28291eZ r2, int i, int i2, int i3) {
        this._type = i;
        this._parent = r2;
        this._lineNr = i2;
        this._columnNr = i3;
        this._index = -1;
    }
}
