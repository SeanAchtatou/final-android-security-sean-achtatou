package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: X.0Xp  reason: invalid class name and case insensitive filesystem */
public class C05090Xp extends FutureTask implements ListenableFuture {
    private C14670tm A00;
    private C14670tm A01;
    private boolean A02;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addListener(java.lang.Runnable r4, java.util.concurrent.Executor r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.A02     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x0022
            X.0tm r2 = new X.0tm     // Catch:{ all -> 0x0033 }
            r2.<init>(r4, r5)     // Catch:{ all -> 0x0033 }
            X.0tm r0 = r3.A00     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x001c
            X.0tm r1 = r3.A01     // Catch:{ all -> 0x0033 }
            r0 = 0
            if (r1 != 0) goto L_0x0014
            r0 = 1
        L_0x0014:
            com.google.common.base.Preconditions.checkState(r0)     // Catch:{ all -> 0x0033 }
            r3.A00 = r2     // Catch:{ all -> 0x0033 }
            r3.A01 = r2     // Catch:{ all -> 0x0033 }
            goto L_0x0020
        L_0x001c:
            r0.A00 = r2     // Catch:{ all -> 0x0033 }
            r3.A00 = r2     // Catch:{ all -> 0x0033 }
        L_0x0020:
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            return
        L_0x0022:
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            r0 = -1769962474(0xffffffff96808416, float:-2.0762873E-25)
            X.AnonymousClass07A.A04(r5, r4, r0)     // Catch:{ RuntimeException -> 0x002a }
            goto L_0x0032
        L_0x002a:
            r2 = move-exception
            java.lang.String r1 = "Combined executeListener"
            r0 = 0
            X.C179868Tg.A00(r1, r0, r2)
            return
        L_0x0032:
            return
        L_0x0033:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05090Xp.addListener(java.lang.Runnable, java.util.concurrent.Executor):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0025, code lost:
        if (r3 == null) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        X.AnonymousClass07A.A04(r3.A02, r3.A01, -1769962474);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0032, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0033, code lost:
        X.C179868Tg.A00("Combined executeListener", null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void done() {
        /*
            r4 = this;
            super.done()
            monitor-enter(r4)
            boolean r3 = r4.A02     // Catch:{ all -> 0x003d }
            r2 = 0
            r1 = 1
            r0 = 0
            if (r3 != 0) goto L_0x000c
            r0 = 1
        L_0x000c:
            com.google.common.base.Preconditions.checkState(r0)     // Catch:{ all -> 0x003d }
            r4.A02 = r1     // Catch:{ all -> 0x003d }
            X.0tm r3 = r4.A01     // Catch:{ all -> 0x003d }
            if (r3 != 0) goto L_0x001f
            X.0tm r0 = r4.A00     // Catch:{ all -> 0x003d }
            if (r0 != 0) goto L_0x001a
            r2 = 1
        L_0x001a:
            com.google.common.base.Preconditions.checkState(r2)     // Catch:{ all -> 0x003d }
            monitor-exit(r4)     // Catch:{ all -> 0x003d }
            return
        L_0x001f:
            r0 = 0
            r4.A01 = r0     // Catch:{ all -> 0x003d }
            r4.A00 = r0     // Catch:{ all -> 0x003d }
            monitor-exit(r4)     // Catch:{ all -> 0x003d }
        L_0x0025:
            if (r3 == 0) goto L_0x003c
            java.lang.Runnable r2 = r3.A01
            java.util.concurrent.Executor r1 = r3.A02
            r0 = -1769962474(0xffffffff96808416, float:-2.0762873E-25)
            X.AnonymousClass07A.A04(r1, r2, r0)     // Catch:{ RuntimeException -> 0x0032 }
            goto L_0x0039
        L_0x0032:
            r2 = move-exception
            java.lang.String r1 = "Combined executeListener"
            r0 = 0
            X.C179868Tg.A00(r1, r0, r2)
        L_0x0039:
            X.0tm r3 = r3.A00
            goto L_0x0025
        L_0x003c:
            return
        L_0x003d:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05090Xp.done():void");
    }

    public C05090Xp(Callable callable) {
        super(callable);
    }
}
