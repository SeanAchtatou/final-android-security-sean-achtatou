package X;

/* renamed from: X.0ry  reason: invalid class name and case insensitive filesystem */
public final class C13730ry {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07 = ((AnonymousClass1Y7) C04350Ue.A05.A09("messages/"));
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A = ((AnonymousClass1Y7) A09.A09("primary_chat_heads_enabled"));

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("messages/")).A09(AnonymousClass80H.$const$string(71));
        A09 = r1;
        A08 = (AnonymousClass1Y7) r1.A09("chat_heads_enabled");
        AnonymousClass1Y7 r12 = (AnonymousClass1Y7) A07.A09("notifications/chat_heads");
        A02 = r12;
        A00 = (AnonymousClass1Y7) r12.A09("/dock_x_percentage");
        AnonymousClass1Y7 r13 = A02;
        A01 = (AnonymousClass1Y7) r13.A09("/dock_y_percentage");
        A06 = (AnonymousClass1Y7) r13.A09("/has_chat_head_settings_been_reported");
        A03 = (AnonymousClass1Y7) r13.A09("/should_present_accessibility_hint");
        A04 = (AnonymousClass1Y7) r13.A09("/debug_shading_enabled");
        A05 = (AnonymousClass1Y7) ((AnonymousClass1Y7) r13.A09("dive_head/")).A09("shortcut_notif_enabled");
    }
}
