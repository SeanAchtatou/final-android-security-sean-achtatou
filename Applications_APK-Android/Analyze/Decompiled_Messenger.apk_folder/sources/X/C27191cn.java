package X;

import com.google.common.collect.ImmutableList;
import java.util.Locale;

/* renamed from: X.1cn  reason: invalid class name and case insensitive filesystem */
public final class C27191cn {
    public static ImmutableList A00(ImmutableList immutableList, Enum enumR) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            builder.add((Object) A01((String) it.next(), enumR));
        }
        return builder.build();
    }

    public static Enum A01(String str, Enum enumR) {
        if (str == null) {
            return enumR;
        }
        try {
            return Enum.valueOf(enumR.getClass(), str.toUpperCase(Locale.US));
        } catch (IllegalArgumentException unused) {
            return enumR;
        }
    }
}
