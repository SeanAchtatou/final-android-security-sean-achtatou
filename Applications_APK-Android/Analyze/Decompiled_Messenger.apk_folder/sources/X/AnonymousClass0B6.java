package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.proxygen.TraceFieldType;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0B6  reason: invalid class name */
public final class AnonymousClass0B6 {
    private static final AtomicLong A0B = new AtomicLong(0);
    public final AnonymousClass07g A00;
    public final C01590Av A01;
    public final C01570At A02;
    public final String A03;
    private final long A04 = A0B.incrementAndGet();
    private final long A05 = ((long) Process.myPid());
    private final long A06 = SystemClock.elapsedRealtime();
    private final Context A07;
    private final C01400Ab A08;
    private final String A09;
    private final String A0A;

    public static void A00(AnonymousClass0B6 r5, Map map, NetworkInfo networkInfo) {
        if (networkInfo != null) {
            String state = networkInfo.getState().toString();
            String typeName = networkInfo.getTypeName();
            if (typeName == null) {
                typeName = BuildConfig.FLAVOR;
            }
            String subtypeName = networkInfo.getSubtypeName();
            if (subtypeName == null) {
                subtypeName = BuildConfig.FLAVOR;
            }
            String extraInfo = networkInfo.getExtraInfo();
            if (extraInfo == null) {
                extraInfo = BuildConfig.FLAVOR;
            }
            map.put("network_state", state);
            map.put("network_type", typeName);
            map.put("network_subtype", subtypeName);
            map.put("network_extra_info", extraInfo);
        } else {
            map.put("network_info", "null");
        }
        map.put("is_in_idle_mode", Boolean.toString(r5.A02.A06()));
    }

    public void A01(long j, int i, String str, C01540Aq r11, long j2, long j3, NetworkInfo networkInfo) {
        Map A002 = C01740Bl.A00("timespan_ms", String.valueOf(j), TraceFieldType.Port, String.valueOf(i), "he_state", str);
        if (r11.A02()) {
            String th = ((Throwable) r11.A01()).toString();
            if (((Throwable) r11.A01()).getCause() != null) {
                th = AnonymousClass08S.A0P(th, " Caused by: ", ((Throwable) r11.A01()).getCause().toString());
            }
            A002.put("error_message", th);
        }
        A002.put("mqtt_session_id", Long.toString(j2));
        A002.put("network_session_id", Long.toString(j3));
        A00(this, A002, networkInfo);
        A07("mqtt_socket_connect", A002);
    }

    public void A03(String str, int i, int i2, long j, int i3, int i4, long j2) {
        A07("mqtt_publish_debug", C01740Bl.A00("result", OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, "operation", str, TraceFieldType.QoS, Integer.toString(i), TraceFieldType.MsgId, Integer.toString(i2), "original_ops_id", Integer.toString(i3), "timespan_ms", Long.toString(j), "retry_cnt", Integer.toString(i4), "mqtt_session_id", Long.toString(j2)));
    }

    public void A04(String str, long j) {
        Map A002 = C01740Bl.A00("operation", str, "timespan_ms", Long.toString(j));
        A00(this, A002, this.A02.A04());
        A07("mqtt_publish_arrive_processing_latency", A002);
    }

    public void A05(String str, String str2, int i, int i2, int i3, Throwable th, int i4, long j) {
        Map A002 = C01740Bl.A00("result", str, "operation", str2, TraceFieldType.QoS, Integer.toString(i), TraceFieldType.MsgId, Integer.toString(i2), "original_ops_id", Integer.toString(i3), "retry_cnt", Integer.toString(i4), "mqtt_session_id", Long.toString(j));
        if (th != null) {
            A002.put("error_message", th.toString());
        }
        A07("mqtt_publish_debug", A002);
    }

    public void A07(String str, Map map) {
        map.put("service_name", this.A0A);
        map.put("service_session_id", Long.toString(this.A06));
        map.put("process_id", Long.toString(this.A05));
        map.put("logger_object_id", Long.toString(this.A04));
        if (!map.containsKey("network_session_id")) {
            map.put("network_session_id", Long.toString(this.A02.A02()));
        }
        String str2 = this.A03;
        if (str2 != null) {
            str = AnonymousClass08S.A0P(str, "_", str2);
        }
        C01750Bm r2 = new C01750Bm(str, this.A09);
        r2.A01(map);
        this.A08.C2i(r2);
    }

    public AnonymousClass0B6(Context context, String str, String str2, C01570At r6, C01590Av r7, C01400Ab r8, AnonymousClass07g r9) {
        this.A07 = context;
        this.A0A = str;
        this.A02 = r6;
        this.A01 = r7;
        this.A09 = context.getPackageName();
        this.A03 = str2;
        this.A08 = r8;
        this.A00 = r9;
    }

    public void A02(C01540Aq r8, C01540Aq r9, C01540Aq r10, C01540Aq r11, C01540Aq r12, C01540Aq r13, C01540Aq r14, long j, long j2, NetworkInfo networkInfo, boolean z) {
        C01540Aq r2;
        HashMap hashMap = new HashMap();
        Context context = this.A07;
        int i = Build.VERSION.SDK_INT;
        boolean z2 = true;
        ContentResolver contentResolver = context.getContentResolver();
        if (i >= 17 ? Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) == 0 : Settings.System.getInt(contentResolver, "airplane_mode_on", 0) == 0) {
            z2 = false;
        }
        hashMap.put("is_airplane_mode_on", String.valueOf(z2));
        C01590Av r1 = this.A01;
        try {
            Intent registerReceiver = r1.A00.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver == null) {
                r2 = C01660Bc.A00;
            } else {
                int intExtra = registerReceiver.getIntExtra("status", -1);
                boolean z3 = true;
                boolean z4 = false;
                if (intExtra == 2) {
                    z4 = true;
                }
                if (intExtra != 5) {
                    z3 = false;
                }
                int intExtra2 = registerReceiver.getIntExtra("level", -1);
                int intExtra3 = registerReceiver.getIntExtra("scale", -1);
                if (intExtra2 == -1 || intExtra3 == -1) {
                    r2 = C01540Aq.A00(new AnonymousClass0SW(z4, z3, C01660Bc.A00));
                } else {
                    r2 = C01540Aq.A00(new AnonymousClass0SW(z4, z3, C01540Aq.A00(Integer.valueOf((int) ((((float) intExtra2) / ((float) intExtra3)) * 100.0f)))));
                }
            }
        } catch (IllegalArgumentException | SecurityException unused) {
            r2 = C01660Bc.A00;
        }
        if (r2.A02()) {
            if (((AnonymousClass0SW) r2.A01()).A01 || ((AnonymousClass0SW) r2.A01()).A02) {
                hashMap.put("bat", "crg");
            } else if (((AnonymousClass0SW) r2.A01()).A00.A02()) {
                hashMap.put("bat", String.valueOf(((AnonymousClass0SW) r2.A01()).A00.A01()));
            }
        }
        if (r8.A02()) {
            hashMap.put("connected_duration_ms", ((Long) r8.A01()).toString());
        }
        if (r9.A02()) {
            hashMap.put("last_ping_ms_ago", ((Long) r9.A01()).toString());
        }
        if (r10.A02()) {
            hashMap.put("last_sent_ms_ago", ((Long) r10.A01()).toString());
        }
        if (r11.A02()) {
            hashMap.put("last_received_ms_ago", ((Long) r11.A01()).toString());
        }
        if (r12.A02()) {
            hashMap.put("reason", r12.A01());
        }
        if (r13.A02()) {
            hashMap.put("operation", r13.A01());
        }
        if (r14.A02()) {
            hashMap.put("exception", ((Throwable) r14.A01()).getClass().getSimpleName());
            hashMap.put("error_message", ((Throwable) r14.A01()).getMessage());
        }
        hashMap.put("fs", String.valueOf(z));
        hashMap.put("mqtt_session_id", Long.toString(j));
        hashMap.put("network_session_id", Long.toString(j2));
        A00(this, hashMap, networkInfo);
        A07("mqtt_disconnection_on_failure", hashMap);
        if (this.A00 != null) {
            HashMap hashMap2 = new HashMap();
            if (r12.A02()) {
                hashMap2.put("reason", r12.A01());
            }
            if (r13.A02()) {
                hashMap2.put("operation", r13.A01());
            }
            if (r14.A02()) {
                hashMap2.put("exception", ((Throwable) r14.A01()).getClass().getSimpleName());
            }
            A00(this, hashMap2, this.A02.A04());
            this.A00.BIq("mqtt_disconnection_on_failure", hashMap2);
        }
    }

    public void A06(String str, String str2, String str3, C01540Aq r7, C01540Aq r8, boolean z, int i, long j, NetworkInfo networkInfo) {
        Map A002 = C01740Bl.A00("act", str, "running", String.valueOf(z));
        A002.put("process_id", Long.toString(this.A05));
        A002.put("thread_id", Long.toString(Thread.currentThread().getId()));
        if (str2 != null) {
            A002.put("mqtt_persistence_string", str2);
        }
        A002.put("network_session_id", Long.toString(j));
        A00(this, A002, networkInfo);
        if (i >= 0) {
            A002.put("fflg", String.valueOf(i));
        }
        if (!TextUtils.isEmpty(str3)) {
            A002.put("calr", str3);
        }
        if (r7.A02()) {
            A002.put("flg", String.valueOf(r7.A01()));
        }
        if (r8.A02()) {
            A002.put("sta_id", String.valueOf(r8.A01()));
        }
        A07("mqtt_service_state", A002);
    }
}
