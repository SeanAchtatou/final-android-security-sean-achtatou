package X;

import android.os.Bundle;

/* renamed from: X.1yq  reason: invalid class name and case insensitive filesystem */
public final class C39311yq implements AnonymousClass24N {
    public final /* synthetic */ C29556EdE A00;

    public C39311yq(C29556EdE edE) {
        this.A00 = edE;
    }

    public void BYh(Throwable th) {
        Bundle bundle = new Bundle();
        bundle.putString("failure_reason", "close_session_fail");
        bundle.putString(ECX.$const$string(36), th.getMessage());
        C29556EdE edE = this.A00;
        C29564EdM edM = edE.A0Q;
        C29556EdE.A01(edE, bundle);
        for (C29567EdP BSi : edM.A01) {
            BSi.BSi(bundle);
        }
    }

    public void Bqg(Object obj, Bundle bundle) {
        C207819qt r6 = (C207819qt) obj;
        if (r6 != null) {
            C200709cY r3 = this.A00.A0R;
            long currentTimeMillis = System.currentTimeMillis();
            String str = (String) r3.A01.get();
            if (str != null) {
                AnonymousClass16O A06 = r3.A00.A06();
                A06.A0A(AnonymousClass08S.A0J(str, "last_upload_success_ts"), currentTimeMillis);
                A06.A06();
            }
            Bundle bundle2 = new Bundle();
            bundle2.putBoolean(ECX.$const$string(38), this.A00.A0L);
            bundle2.putLong(ECX.$const$string(AnonymousClass1Y3.A1C), this.A00.A0R.A00(0));
            bundle2.putLong("time_spent", System.currentTimeMillis() - this.A00.A0B);
            bundle2.putInt(ECX.$const$string(AnonymousClass1Y3.A1E), this.A00.A0N.A05);
            bundle2.putString(ECX.$const$string(AnonymousClass1Y3.A10), this.A00.A0G);
            bundle2.putBoolean("in_sync", Boolean.valueOf(r6.A00.getBooleanValue(1939092853)).booleanValue());
            for (C29567EdP BSj : this.A00.A0Q.A01) {
                BSj.BSj(bundle2);
            }
        }
    }
}
