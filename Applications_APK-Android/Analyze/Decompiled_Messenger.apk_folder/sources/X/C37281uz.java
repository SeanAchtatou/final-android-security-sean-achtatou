package X;

import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/* renamed from: X.1uz  reason: invalid class name and case insensitive filesystem */
public final class C37281uz {
    public final InterstitialTrigger A00;
    public final String A01;
    public final Throwable A02;
    public final Map A03 = AnonymousClass0TG.A04();
    public final SortedSet A04 = new TreeSet();
    public volatile boolean A05 = false;

    private synchronized void A00(C71363cG r4, int i) {
        Preconditions.checkNotNull(r4);
        String str = r4.A02;
        C70693b8 r1 = new C70693b8(i, r4);
        this.A03.put(str, r1);
        this.A04.add(r1);
    }

    public static synchronized void A01(C37281uz r2, String str) {
        synchronized (r2) {
            C70693b8 r1 = (C70693b8) r2.A03.remove(str);
            if (r1 != null) {
                r2.A04.remove(r1);
            }
        }
    }

    public synchronized void A02(C71363cG r3, int i) {
        Preconditions.checkNotNull(r3);
        if (((C70693b8) this.A03.get(r3.A02)) != null) {
            A03(r3, i);
        } else {
            A00(r3, i);
        }
    }

    public synchronized void A03(C71363cG r3, int i) {
        Preconditions.checkNotNull(r3);
        C70693b8 r1 = (C70693b8) this.A03.get(r3.A02);
        if (!(r1 == null || r1.A00 == i)) {
            this.A04.remove(r1);
            A00(r3, i);
        }
    }

    public synchronized String toString() {
        MoreObjects.ToStringHelper stringHelper;
        stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("KnowinglyFullyRestored", this.A05);
        stringHelper.add("Trigger", this.A00);
        stringHelper.add("RankedInterstitials", this.A04);
        return stringHelper.toString();
    }

    public C37281uz(InterstitialTrigger interstitialTrigger, String str) {
        Preconditions.checkNotNull(interstitialTrigger);
        this.A00 = interstitialTrigger;
        this.A01 = str;
        this.A02 = new Throwable(AnonymousClass08S.A0J("Added Reason: ", str));
    }
}
