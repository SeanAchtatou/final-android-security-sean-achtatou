package X;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

/* renamed from: X.1LA  reason: invalid class name */
public final class AnonymousClass1LA {
    public static void A01(View view, int i, int i2, int i3, int i4, boolean z) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        if (!(!z && view.getMeasuredHeight() == i6 && view.getMeasuredWidth() == i5)) {
            view.measure(View.MeasureSpec.makeMeasureSpec(i5, 1073741824), View.MeasureSpec.makeMeasureSpec(i6, 1073741824));
        }
        if (z || view.getLeft() != i || view.getTop() != i2 || view.getRight() != i3 || view.getBottom() != i4) {
            view.layout(i, i2, i3, i4);
        }
    }

    public static void A00(Drawable drawable, int i, int i2) {
        Drawable drawable2;
        Rect bounds = drawable.getBounds();
        int i3 = bounds.left;
        int i4 = bounds.top;
        drawable.setBounds(i3, i4, i3 + i, i4 + i2);
        if ((drawable instanceof AnonymousClass1MR) && (drawable2 = ((AnonymousClass1MR) drawable).A00) != null) {
            drawable2.setBounds(0, 0, i, i2);
        }
    }
}
