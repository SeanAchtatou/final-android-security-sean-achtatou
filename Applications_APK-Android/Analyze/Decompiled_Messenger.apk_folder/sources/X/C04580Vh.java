package X;

import java.util.concurrent.ThreadFactory;

/* renamed from: X.0Vh  reason: invalid class name and case insensitive filesystem */
public final class C04580Vh implements ThreadFactory {
    public final C04550Vd A00;
    private final ThreadFactory A01;

    public Thread newThread(Runnable runnable) {
        return this.A01.newThread(new AnonymousClass0Y9(this, runnable));
    }

    public C04580Vh(ThreadFactory threadFactory, C04550Vd r2) {
        this.A01 = threadFactory;
        this.A00 = r2;
    }
}
