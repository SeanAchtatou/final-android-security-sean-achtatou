package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;

/* renamed from: X.08F  reason: invalid class name */
public final class AnonymousClass08F implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new SubscribeTopic(parcel);
    }

    public Object[] newArray(int i) {
        return new SubscribeTopic[i];
    }
}
