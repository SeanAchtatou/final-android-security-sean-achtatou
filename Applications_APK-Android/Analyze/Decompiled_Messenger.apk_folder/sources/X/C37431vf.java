package X;

import android.graphics.drawable.Animatable;
import com.facebook.common.time.RealtimeSinceBootClock;

/* renamed from: X.1vf  reason: invalid class name and case insensitive filesystem */
public abstract class C37431vf implements C31231jM {
    public AnonymousClass069 A00 = RealtimeSinceBootClock.A00;
    private boolean A01;
    public final AnonymousClass54H A02;
    private final C104954zs A03;

    public void Bbb(String str, Throwable th) {
    }

    public void BlR(String str) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x010e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.C37431vf r9) {
        /*
            boolean r0 = r9.A01
            if (r0 != 0) goto L_0x0119
            X.54H r8 = r9.A02
            boolean r0 = r8.A01()
            if (r0 == 0) goto L_0x0119
            X.4zs r5 = r9.A03
            boolean r0 = r5.A04
            if (r0 == 0) goto L_0x0116
            java.util.Random r0 = r5.A03
            int r0 = r0.nextInt()
            long r3 = (long) r0
            long r0 = r5.A00
            long r3 = r3 % r0
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0116
            X.4zv r2 = r5.A02
            com.facebook.analytics.DeprecatedAnalyticsLogger r1 = r2.A00
            r4 = 0
            java.lang.String r0 = "fresco_percent_photos_rendered"
            X.1La r7 = r1.A04(r0, r4)
            boolean r0 = r7.A0B()
            if (r0 == 0) goto L_0x0116
            int r3 = r8.A00()
            r0 = 1
            r1 = 0
            if (r3 != r0) goto L_0x003c
            r1 = 1
        L_0x003c:
            java.lang.String r0 = "rendered"
            r7.A02(r0, r1)
            r0 = 2
            r1 = 0
            if (r3 != r0) goto L_0x0046
            r1 = 1
        L_0x0046:
            java.lang.String r0 = "not_rendered"
            r7.A02(r0, r1)
            r0 = 3
            if (r3 != r0) goto L_0x004f
            r4 = 1
        L_0x004f:
            java.lang.String r0 = "ignored"
            r7.A02(r0, r4)
            com.facebook.common.connectionstatus.FbDataConnectionManager r0 = r2.A01
            X.1Vy r1 = r0.A06()
            java.lang.String r0 = "conn_qual"
            r7.A05(r0, r1)
            X.0h3 r0 = r2.A02
            java.lang.String r1 = r0.A0K()
            java.lang.String r0 = "network_type"
            r7.A06(r0, r1)
            X.0h3 r0 = r2.A02
            java.lang.String r1 = r0.A0J()
            java.lang.String r0 = "network_subtype"
            r7.A06(r0, r1)
            android.util.Pair r2 = r8.A07
            if (r2 == 0) goto L_0x0087
            java.lang.Object r1 = r2.first
            java.lang.String r0 = "bitmap_size_width"
            r7.A05(r0, r1)
            java.lang.Object r1 = r2.second
            java.lang.String r0 = "bitmap_size_height"
            r7.A05(r0, r1)
        L_0x0087:
            android.util.Pair r2 = r8.A08
            if (r2 == 0) goto L_0x0099
            java.lang.Object r1 = r2.first
            java.lang.String r0 = "target_size_width"
            r7.A05(r0, r1)
            java.lang.Object r1 = r2.second
            java.lang.String r0 = "target_size_height"
            r7.A05(r0, r1)
        L_0x0099:
            java.lang.Boolean r1 = r8.A09
            if (r1 == 0) goto L_0x00a2
            java.lang.String r0 = "bitmap_cache_hit"
            r7.A05(r0, r1)
        L_0x00a2:
            java.lang.Boolean r1 = r8.A0B
            if (r1 == 0) goto L_0x00ab
            java.lang.String r0 = "encoded_cache_hit"
            r7.A05(r0, r1)
        L_0x00ab:
            java.lang.Boolean r1 = r8.A0A
            if (r1 == 0) goto L_0x00b4
            java.lang.String r0 = "disk_cache_hit"
            r7.A05(r0, r1)
        L_0x00b4:
            java.lang.String r1 = r8.A0D
            r0 = 36
            java.lang.String r0 = X.ECX.$const$string(r0)
            r7.A06(r0, r1)
            android.net.Uri r0 = r8.A06
            if (r0 == 0) goto L_0x00cc
            java.lang.String r1 = r0.getScheme()
            java.lang.String r0 = "uri_scheme"
            r7.A06(r0, r1)
        L_0x00cc:
            java.lang.Object r2 = r8.A0C
            boolean r0 = r2 instanceof com.facebook.common.callercontext.CallerContext
            if (r0 == 0) goto L_0x00f6
            com.facebook.common.callercontext.CallerContext r2 = (com.facebook.common.callercontext.CallerContext) r2
            java.lang.String r1 = r2.A0F()
            java.lang.String r0 = "analytics_tag"
            r7.A06(r0, r1)
            java.lang.String r1 = r2.A02
            java.lang.String r0 = "calling_class"
            r7.A06(r0, r1)
            java.lang.String r1 = r2.A0G()
            java.lang.String r0 = "feature_tag"
            r7.A06(r0, r1)
            java.lang.String r1 = r2.A0H()
            java.lang.String r0 = "module_tag"
            r7.A06(r0, r1)
        L_0x00f6:
            long r5 = r8.A00
            r3 = -1
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x011a
            long r1 = r8.A01
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x011a
            long r1 = r1 - r5
        L_0x0105:
            java.lang.String r0 = "in_viewport_during_ms"
            r7.A03(r0, r1)
            java.lang.String r1 = r8.A0E
            if (r1 == 0) goto L_0x0113
            java.lang.String r0 = "image_fbid"
            r7.A06(r0, r1)
        L_0x0113:
            r7.A0A()
        L_0x0116:
            r0 = 1
            r9.A01 = r0
        L_0x0119:
            return
        L_0x011a:
            r1 = -1
            goto L_0x0105
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37431vf.A00(X.1vf):void");
    }

    public void BYg(String str, Throwable th) {
        AnonymousClass54H r7 = this.A02;
        long now = this.A00.now();
        if (r7.A02 == -1) {
            r7.A02 = now;
        }
        if (th != null) {
            this.A02.A0D = th.getMessage();
        }
        A00(this);
    }

    public void BZ2(String str, Object obj, Animatable animatable) {
        AnonymousClass54H r7 = this.A02;
        long now = this.A00.now();
        if (r7.A05 == -1) {
            r7.A05 = now;
        }
        A00(this);
    }

    public void Bbc(String str, Object obj) {
        AnonymousClass54H r7 = this.A02;
        long now = this.A00.now();
        if (r7.A03 == -1) {
            r7.A03 = now;
        }
        A00(this);
    }

    public void BqL(String str, Object obj) {
        AnonymousClass54H r7 = this.A02;
        long now = this.A00.now();
        if (r7.A04 == -1) {
            r7.A04 = now;
        }
        this.A02.A0C = obj;
    }

    public C37431vf(C104954zs r5, long j) {
        boolean z = false;
        this.A01 = false;
        C05520Zg.A02(r5);
        C05520Zg.A04(j >= 0 ? true : z);
        this.A03 = r5;
        this.A02 = new AnonymousClass54H(j);
    }
}
