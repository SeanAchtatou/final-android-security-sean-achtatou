package X;

import android.content.Intent;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.zero.common.ZeroToken;
import com.facebook.zero.common.ZeroUrlRewriteRule;
import com.facebook.zero.common.util.CarrierAndSimMccMnc;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1eP  reason: invalid class name and case insensitive filesystem */
public final class C28191eP implements C14250su, C28201eQ, C05700aB, CallerContextable {
    public static final Class A0F = C28191eP.class;
    private static volatile C28191eP A0G = null;
    public static final String __redex_internal_original_name = "com.facebook.zero.token.FbZeroTokenManager";
    public int A00 = 0;
    public int A01 = 3;
    public AnonymousClass0UN A02;
    public C28221eS A03 = C28221eS.UNKNOWN;
    private boolean A04 = true;
    public final C28211eR A05;
    public final C04310Tq A06;
    public final C04310Tq A07;
    public final C04310Tq A08;
    private final C04310Tq A09;
    private final C04310Tq A0A;
    private final C04310Tq A0B;
    public volatile ImmutableList A0C;
    public volatile ImmutableList A0D;
    public volatile ImmutableSet A0E;

    private synchronized ImmutableList A01() {
        if (A0E()) {
            AnonymousClass155 r1 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
            return AnonymousClass155.A04(r1, AnonymousClass155.A05(r1, AnonymousClass155.A02(r1)));
        }
        if (this.A0D == null) {
            A05();
        }
        return this.A0D;
    }

    private synchronized void A03() {
        AnonymousClass155 r1 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
        this.A0E = AnonymousClass1JB.A00(r1.A0E(AnonymousClass155.A02(r1)));
    }

    private synchronized void A04() {
        this.A0C = null;
        AnonymousClass155 r1 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
        if (r1.A00.BBh(AnonymousClass155.A00(AnonymousClass155.A02(r1)).A09("backup_rewrite_rules"))) {
            if (A0E()) {
                AnonymousClass155 r12 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
                this.A0C = AnonymousClass155.A04(r12, r12.A07());
            } else {
                try {
                    String A072 = ((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02)).A07();
                    if (!C06850cB.A0B(A072)) {
                        this.A0C = ImmutableList.copyOf((Collection) C47882Yl.A00(A072));
                    }
                } catch (IOException e) {
                    C010708t.A0E(A0F, e, "Error deserializing backup rewrite rules: %s", e.getMessage());
                }
            }
        }
        return;
    }

    private synchronized void A05() {
        this.A0D = RegularImmutableList.A02;
        if (A0E()) {
            AnonymousClass155 r1 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
            this.A0D = AnonymousClass155.A04(r1, AnonymousClass155.A05(r1, AnonymousClass155.A02(r1)));
            A06(CallerContext.A07(C28191eP.class, "set_rules"));
        } else {
            try {
                AnonymousClass155 r12 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
                String A052 = AnonymousClass155.A05(r12, AnonymousClass155.A02(r12));
                if (!C06850cB.A0B(A052)) {
                    this.A0D = ImmutableList.copyOf((Collection) C47882Yl.A00(A052));
                    A06(CallerContext.A07(C28191eP.class, "set_rules"));
                }
            } catch (IOException e) {
                C010708t.A0E(A0F, e, "Error deserializing rewrite rules: %s", e.getMessage());
            }
        }
        return;
    }

    public void onBeforeDialtoneStateChanged(boolean z) {
    }

    public static final C28191eP A00(AnonymousClass1XY r6) {
        if (A0G == null) {
            synchronized (C28191eP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0G, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0G = new C28191eP(applicationInjector, C28211eR.A00(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.Ay6, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0G;
    }

    public static String A02(ImmutableList immutableList) {
        JSONArray jSONArray = new JSONArray();
        try {
            int size = immutableList.size();
            for (int i = 0; i < size; i++) {
                ZeroUrlRewriteRule zeroUrlRewriteRule = (ZeroUrlRewriteRule) immutableList.get(i);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("matcher", zeroUrlRewriteRule.A01);
                jSONObject.put("replacer", zeroUrlRewriteRule.A02);
                jSONArray.put(jSONObject);
            }
            return jSONArray.toString();
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    private void A06(CallerContext callerContext) {
        C22361La A042 = ((C47842Yh) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B7G, this.A02)).A00.A04("zero_rewrite_rules_applied", false);
        if (A042.A0B()) {
            A042.A06("caller_context", callerContext.toString());
            C47842Yh.A01(A042, null);
            A042.A0A();
        }
        ImmutableList A012 = A01();
        for (C31851kY Bmi : (Set) AnonymousClass1XX.A02(12, AnonymousClass1Y3.AY1, this.A02)) {
            Bmi.Bmi(A012);
        }
    }

    public static void A07(C28191eP r3) {
        ((C04460Ut) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AKb, r3.A02)).C4y("com.facebook.zero.ZERO_RATING_STATE_CHANGED");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0069, code lost:
        if (r1 == false) goto L_0x006b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void A08(X.C28191eP r12, X.AnonymousClass157 r13, java.lang.String r14) {
        /*
            int r2 = X.AnonymousClass1Y3.AHR
            X.0UN r1 = r12.A02
            r0 = 6
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2Yi r0 = (X.C47852Yi) r0
            java.lang.String r1 = r0.A03()
            java.lang.String r0 = "none"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x017f
            boolean r0 = r12.A0D()
            if (r0 != 0) goto L_0x0023
            java.lang.String r0 = "disabled"
            A09(r12, r0)
            return
        L_0x0023:
            r2 = 14
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r12.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 286714836884574(0x104c400011c5e, double:1.416559510576447E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x015d
            r2 = 9
            int r1 = X.AnonymousClass1Y3.ALf
            X.0UN r0 = r12.A02
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.56q r3 = (X.C1062756q) r3
            X.1YI r1 = r3.A00
            r0 = 582(0x246, float:8.16E-43)
            com.facebook.common.util.TriState r1 = r1.Ab9(r0)
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            if (r1 != r0) goto L_0x017f
            X.1eR r0 = r3.A03
            X.157 r1 = X.AnonymousClass157.DIALTONE
            java.util.Map r0 = r0.A01
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x006b
            X.1eR r0 = r3.A03
            X.157 r1 = X.AnonymousClass157.NORMAL
            java.util.Map r0 = r0.A01
            boolean r1 = r0.containsKey(r1)
            r0 = 1
            if (r1 != 0) goto L_0x006c
        L_0x006b:
            r0 = 0
        L_0x006c:
            if (r0 != 0) goto L_0x017f
            X.155 r1 = r3.A04
            X.157 r0 = X.AnonymousClass157.NORMAL
            r1.A0K(r0)
            X.155 r1 = r3.A04
            X.157 r0 = X.AnonymousClass157.DIALTONE
            r1.A0K(r0)
            X.56p r5 = new X.56p
            r5.<init>(r3)
            X.0US r0 = r3.A01
            java.lang.Object r6 = r0.get()
            X.2a4 r6 = (X.C48262a4) r6
            com.facebook.zero.protocol.params.FetchZeroDualTokenRequestParams r7 = new com.facebook.zero.protocol.params.FetchZeroDualTokenRequestParams
            X.0US r0 = r3.A02
            java.lang.Object r0 = r0.get()
            X.2Yi r0 = (X.C47852Yi) r0
            com.facebook.zero.common.util.CarrierAndSimMccMnc r8 = r0.A02()
            X.0US r0 = r3.A02
            java.lang.Object r0 = r0.get()
            X.2Yi r0 = (X.C47852Yi) r0
            java.lang.String r9 = r0.A03()
            X.155 r1 = r3.A04
            X.157 r0 = X.AnonymousClass157.DIALTONE
            com.facebook.prefs.shared.FbSharedPreferences r2 = r1.A00
            X.1Y8 r1 = X.AnonymousClass155.A00(r0)
            java.lang.String r0 = "backup_rewrite_rules"
            X.1Y8 r0 = r1.A09(r0)
            boolean r0 = r2.BBh(r0)
            r10 = r0 ^ 1
            X.155 r1 = r3.A04
            X.157 r0 = X.AnonymousClass157.NORMAL
            com.facebook.prefs.shared.FbSharedPreferences r2 = r1.A00
            X.1Y8 r1 = X.AnonymousClass155.A00(r0)
            java.lang.String r0 = "backup_rewrite_rules"
            X.1Y8 r0 = r1.A09(r0)
            boolean r0 = r2.BBh(r0)
            r11 = r0 ^ 1
            X.155 r1 = r3.A04
            X.157 r0 = X.AnonymousClass157.DIALTONE
            java.lang.String r12 = r1.A0D(r0)
            X.155 r1 = r3.A04
            X.157 r0 = X.AnonymousClass157.NORMAL
            java.lang.String r13 = r1.A0D(r0)
            r7.<init>(r8, r9, r10, r11, r12, r13, r14)
            com.facebook.graphql.query.GQSQStringShape1S0000000_I1 r2 = new com.facebook.graphql.query.GQSQStringShape1S0000000_I1
            r0 = 53
            r2.<init>(r0)
            java.lang.String r1 = r7.A00
            java.lang.String r0 = "hash_dialtone_token"
            r2.A09(r0, r1)
            java.lang.String r1 = r7.A01
            java.lang.String r0 = "hash_normal_token"
            r2.A09(r0, r1)
            boolean r0 = r7.A03
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "needs_backup_rules_for_dialtone_token"
            r2.A05(r0, r1)
            boolean r0 = r7.A04
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "needs_backup_rules_for_normal_token"
            r2.A05(r0, r1)
            java.lang.String r1 = r7.A02
            r0 = 1343(0x53f, float:1.882E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r2.A09(r0, r1)
            com.facebook.graphql.query.GraphQlQueryParamSet r2 = r2.A00
            com.facebook.graphql.query.GQSQStringShape1S0000000_I1 r1 = new com.facebook.graphql.query.GQSQStringShape1S0000000_I1
            r0 = 53
            r1.<init>(r0)
            X.1cN r4 = X.C26931cN.A00(r1)
            com.google.common.base.Preconditions.checkNotNull(r2)
            X.0l0 r0 = r4.A0B
            r0.A03(r2)
            int r2 = X.AnonymousClass1Y3.ASp
            X.0UN r1 = r6.A00
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0mD r0 = (X.C11170mD) r0
            X.0l3 r4 = r0.A04(r4)
            int r2 = X.AnonymousClass1Y3.BKH
            X.0UN r1 = r6.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            java.util.concurrent.ExecutorService r0 = (java.util.concurrent.ExecutorService) r0
            X.C05350Yp.A08(r4, r5, r0)
            X.1eR r0 = r3.A03
            X.157 r1 = X.AnonymousClass157.DIALTONE
            java.util.Map r0 = r0.A01
            r0.put(r1, r4)
            X.1eR r0 = r3.A03
            X.157 r1 = X.AnonymousClass157.NORMAL
            java.util.Map r0 = r0.A01
            r0.put(r1, r4)
            return
        L_0x015d:
            X.0Tq r0 = r12.A09
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x017a
            X.1eR r1 = r12.A05
            X.157 r0 = X.AnonymousClass157.DIALTONE
            r1.A06(r0, r14)
            X.1eR r1 = r12.A05
            X.157 r0 = X.AnonymousClass157.NORMAL
            r1.A06(r0, r14)
            return
        L_0x017a:
            X.1eR r0 = r12.A05
            r0.A06(r13, r14)
        L_0x017f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28191eP.A08(X.1eP, X.157, java.lang.String):void");
    }

    public static void A09(C28191eP r7, String str) {
        C30281hn edit = ((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, r7.A02)).A00.edit();
        edit.BzC(AnonymousClass155.A00((AnonymousClass157) r7.A06.get()).A09("current_zero_rating_status"), str);
        edit.commit();
        A07(r7);
        AnonymousClass157 r2 = (AnonymousClass157) r7.A06.get();
        int i = AnonymousClass1Y3.AC0;
        ((AnonymousClass155) AnonymousClass1XX.A02(8, i, r7.A02)).A0G(r2, "unknown");
        String A0H = ((AnonymousClass155) AnonymousClass1XX.A02(8, i, r7.A02)).A0H(r2, "unknown");
        String A0I = ((AnonymousClass155) AnonymousClass1XX.A02(8, i, r7.A02)).A0I(r2, "unavailable");
        if (((AnonymousClass0XN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ArW, r7.A02)).A0I() && !r7.A0F()) {
            if ((!r7.A0D() || !A0H.equals("unknown")) && !A0I.equals("unavailable")) {
                r7.A0F();
                Intent intent = new Intent("com.facebook.zero.ZERO_RATING_STATE_UNREGISTERED_REASON");
                intent.putExtra("unregistered_reason", A0I);
                ((C04460Ut) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AKb, r7.A02)).C4x(intent);
            }
        }
    }

    private void A0B(String str, String str2) {
        Intent intent;
        C185213x r3 = (C185213x) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AfJ, this.A02);
        if (!((Boolean) r3.A03.get()).booleanValue()) {
            AnonymousClass1EU r0 = r3.A00;
            if (r0 != null) {
                AnonymousClass13w.A01(r0.A00, C10930l6.A02).optString("host_name_v6", "mqtt-mini.facebook.com");
                intent = new Intent("com.facebook.rti.mqtt.ACTION_ZR_SWITCH");
            } else {
                return;
            }
        } else if (r3.A00 != null) {
            intent = new Intent("com.facebook.rti.mqtt.ACTION_ZR_SWITCH");
            if (!C06850cB.A0B(str)) {
                intent.putExtra("extra_mqtt_endpoint", str);
            }
            if (!C06850cB.A0B(str2)) {
                intent.putExtra("extra_fbns_endpoint", str2);
            }
        } else {
            return;
        }
        r3.A02.A03(intent, r3.A01.getPackageName());
    }

    private boolean A0C() {
        return ((FbSharedPreferences) AnonymousClass1XX.A02(11, AnonymousClass1Y3.B6q, this.A02)).Aep(C05730aE.A0h, true);
    }

    private boolean A0D() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, this.A02)).AbO(582, false);
    }

    private boolean A0E() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, this.A02)).AbO(AnonymousClass1Y3.A6s, false);
    }

    private boolean A0F() {
        if (!((C47852Yi) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AHR, this.A02)).A03().equals("wifi") || ((FbSharedPreferences) AnonymousClass1XX.A02(11, AnonymousClass1Y3.B6q, this.A02)).Aep(C05730aE.A04, false)) {
            return false;
        }
        return true;
    }

    public static boolean A0G(C28191eP r3) {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, r3.A02)).AbO(859, false);
    }

    public C28221eS A0H() {
        if (!A0J().contains("paid_balance_detection")) {
            return C28221eS.DISABLED;
        }
        return this.A03;
    }

    public ImmutableList A0I() {
        if (((Boolean) this.A0A.get()).booleanValue() || ((Boolean) this.A0B.get()).booleanValue()) {
            if (this.A04) {
                this.A04 = false;
                A0O("force backup rules");
            }
            if (A0C() || A0H() == C28221eS.UNKNOWN) {
                if (this.A0C == null) {
                    A04();
                }
                if (((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, this.A02)).Ab9(582) != TriState.UNSET && this.A0C != null) {
                    return this.A0C;
                }
                if (!((Boolean) this.A08.get()).booleanValue()) {
                    return C31851kY.A01;
                }
                return C31851kY.A00;
            }
        }
        return A01();
    }

    public Map A0K() {
        AnonymousClass157 r6 = (AnonymousClass157) this.A06.get();
        int i = AnonymousClass1Y3.AC0;
        String A0H = ((AnonymousClass155) AnonymousClass1XX.A02(8, i, this.A02)).A0H(r6, "unknown");
        String A0G2 = ((AnonymousClass155) AnonymousClass1XX.A02(8, i, this.A02)).A0G(r6, "unknown");
        String A0I = ((AnonymousClass155) AnonymousClass1XX.A02(8, i, this.A02)).A0I(r6, "unavailable");
        String A092 = ((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02)).A09(r6);
        HashMap hashMap = new HashMap();
        hashMap.put("campaign_id", A092);
        hashMap.put("zero_rating_status", A0H);
        hashMap.put("registration_status", A0G2);
        hashMap.put("unregistered_reason", A0I);
        hashMap.put("zero_unknown_state", Boolean.valueOf(A0C()));
        if (A0J().contains("paid_balance_detection")) {
            hashMap.put("paid_balance_state", A0H());
        }
        return hashMap;
    }

    public void A0M(ZeroToken zeroToken, AnonymousClass157 r5) {
        C28211eR r2 = this.A05;
        zeroToken.toString();
        C28211eR.A01(r2, "fetch_success", r5, "debug");
        r2.A01.remove(r5);
        r2.A04(zeroToken, r5);
    }

    public void A0N(C28201eQ r3) {
        C28211eR r1 = this.A05;
        synchronized (r1) {
            r1.A02.remove(r3);
        }
    }

    public void A0O(String str) {
        C22361La A042 = ((C47842Yh) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B7G, this.A02)).A00.A04("zero_enter_unknown_state", false);
        if (A042.A0B()) {
            A042.A06("reason", str);
            A042.A0A();
        }
        A0A(this, true);
        A0L();
    }

    public void A0P(String str) {
        A08(this, (AnonymousClass157) this.A06.get(), str);
    }

    public ImmutableSet AzO() {
        this.A05.A03();
        boolean z = true;
        if (((C28171eN) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BLh, this.A02)).A03(AnonymousClass9M6.A08, true) != 1) {
            z = false;
        }
        if (z) {
            ((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02)).A0J();
            this.A0E = null;
        }
        HashSet hashSet = new HashSet();
        hashSet.add(AnonymousClass155.A00(AnonymousClass157.NORMAL));
        hashSet.add(AnonymousClass155.A00(AnonymousClass157.DIALTONE));
        return ImmutableSet.A0A(hashSet);
    }

    public void onAfterDialtoneStateChanged(boolean z) {
        this.A05.A03();
        A05();
        A03();
        A04();
        AnonymousClass157 r5 = (AnonymousClass157) this.A06.get();
        A0B(((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02)).A00.B4F(AnonymousClass155.A00(r5).A09("mqtt_host"), BuildConfig.FLAVOR), ((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02)).A00.B4F(AnonymousClass155.A00(r5).A09("fbns_host"), BuildConfig.FLAVOR));
        A07(this);
        A0L();
        if (z) {
            AnonymousClass157 r4 = AnonymousClass157.NORMAL;
            if (!ZeroToken.A00(((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02)).A09(r4))) {
                A08(this, r4, "prefetch");
            }
        }
    }

    private C28191eP(AnonymousClass1XY r5, C28211eR r6, C04310Tq r7) {
        this.A02 = new AnonymousClass0UN(15, r5);
        this.A09 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ard, r5);
        this.A08 = AnonymousClass0VG.A00(AnonymousClass1Y3.AJY, r5);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.A7j, r5);
        this.A0A = AnonymousClass0VG.A00(AnonymousClass1Y3.AjP, r5);
        this.A0B = AnonymousClass0VG.A00(AnonymousClass1Y3.B6Y, r5);
        ((AnonymousClass12c) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BCZ, this.A02)).A00.add(new AnonymousClass12d(this));
        this.A05 = r6;
        r6.A05(this);
        this.A06 = new AnonymousClass12e(r7);
    }

    public static void A0A(C28191eP r4, boolean z) {
        if (A0G(r4)) {
            r4.A00 = 0;
            r4.A01 = 3;
        }
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(11, AnonymousClass1Y3.B6q, r4.A02)).edit();
        edit.putBoolean(C05730aE.A0h, z);
        edit.commit();
        if (!z) {
            ((AnonymousClass2YR) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BPW, r4.A02)).A00 = false;
        } else if (A0G(r4) && !r4.A0F()) {
            ((AnonymousClass2YR) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BPW, r4.A02)).A00 = true;
        }
        if (!z) {
            r4.A04 = false;
        }
        r4.A06(CallerContext.A07(C28191eP.class, "update_unk_state"));
    }

    public ImmutableSet A0J() {
        if (A0E()) {
            AnonymousClass155 r4 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
            String A0E2 = r4.A0E(AnonymousClass155.A02(r4));
            if (r4.A01.containsKey(A0E2)) {
                return (ImmutableSet) r4.A01.get(A0E2);
            }
            ImmutableSet A002 = AnonymousClass1JB.A00(A0E2);
            if (r4.A01.size() < 20) {
                r4.A01.put(A0E2, A002);
                return A002;
            }
            C010708t.A06(AnonymousClass155.A07, "Too many cached deserialized features.");
            return A002;
        }
        if (this.A0E == null) {
            A03();
        }
        return this.A0E;
    }

    public void A0L() {
        if (!A0D()) {
            A09(this, "disabled");
            return;
        }
        String A032 = ((C47852Yi) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AHR, this.A02)).A03();
        if (A032.equals("none")) {
            return;
        }
        if (A0F()) {
            A09(this, "disabled");
            ((C04460Ut) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AKb, this.A02)).C4y("com.facebook.zero.ZERO_RATING_DISABLED_ON_WIFI");
            return;
        }
        String B4F = ((FbSharedPreferences) AnonymousClass1XX.A02(11, AnonymousClass1Y3.B6q, this.A02)).B4F(C05730aE.A0J, String.valueOf((int) ((Math.random() * 9000.0d) + 1000.0d)).replaceAll(".(?!$)", "$0:"));
        CarrierAndSimMccMnc carrierAndSimMccMnc = new CarrierAndSimMccMnc(B4F);
        if (carrierAndSimMccMnc.equals(CarrierAndSimMccMnc.A02)) {
            C22361La A042 = ((C47842Yh) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B7G, this.A02)).A00.A04("zero_invalid_mcc_mnc", false);
            if (A042.A0B()) {
                A042.A06("mccmnc", B4F);
                C47842Yh.A01(A042, null);
                A042.A0A();
            }
        }
        CarrierAndSimMccMnc A022 = ((C47852Yi) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AHR, this.A02)).A02();
        AnonymousClass1Y8 r4 = C05730aE.A0U;
        String B4F2 = ((FbSharedPreferences) AnonymousClass1XX.A02(11, AnonymousClass1Y3.B6q, this.A02)).B4F(r4, "none");
        AnonymousClass155 r1 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
        long At2 = r1.A00.At2(AnonymousClass155.A00(AnonymousClass155.A02(r1)).A09("last_time_checked"), 0);
        AnonymousClass155 r12 = (AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02);
        boolean z = false;
        if (((AnonymousClass06B) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AgK, this.A02)).now() - At2 < ((long) r12.A00.AqN(AnonymousClass155.A00(AnonymousClass155.A02(r12)).A09("ttl"), 3600)) * 1000) {
            z = true;
        }
        if (!z) {
            A0P("ttl_expired");
        } else if (!((Boolean) this.A0A.get()).booleanValue() || !A0C()) {
            if (!A022.equals(carrierAndSimMccMnc) || !A032.equals(B4F2)) {
                C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(11, AnonymousClass1Y3.B6q, this.A02)).edit();
                AnonymousClass1Y8 r2 = C05730aE.A0J;
                CarrierAndSimMccMnc.MccMncPair mccMncPair = A022.A00;
                CarrierAndSimMccMnc.MccMncPair mccMncPair2 = A022.A01;
                edit.BzC(r2, AnonymousClass08S.A0V(mccMncPair.A00, ":", mccMncPair.A01, ":", mccMncPair2.A00, ":", mccMncPair2.A01));
                edit.BzC(r4, A032);
                edit.commit();
                A0P("mccmnc_changed");
                return;
            }
            A09(this, "enabled");
        } else if (!A0G(this)) {
            A08(this, (AnonymousClass157) this.A06.get(), "unknown_state");
        }
    }

    public void BYv(Throwable th, AnonymousClass157 r6) {
        th.getMessage();
        if (r6 == this.A06.get()) {
            A09(this, "unknown");
        } else {
            C30281hn edit = ((AnonymousClass155) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AC0, this.A02)).A00.edit();
            edit.BzC(AnonymousClass155.A00(r6).A09("current_zero_rating_status"), "unknown");
            edit.commit();
        }
        C22361La A042 = ((C47842Yh) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B7G, this.A02)).A00.A04("zero_token_fetch_failed", false);
        if (A042.A0B()) {
            A042.A06("pigeon_reserved_keyword_module", "zero_module");
            A042.A05(AnonymousClass24B.$const$string(AnonymousClass1Y3.A1o), th);
            C47842Yh.A01(A042, null);
            A042.A0A();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x01b1 A[SYNTHETIC, Splitter:B:40:0x01b1] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01d8  */
    /* JADX WARNING: Removed duplicated region for block: B:64:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BYw(com.facebook.zero.common.ZeroToken r12, X.AnonymousClass157 r13) {
        /*
            r11 = this;
            r12.toString()
            com.facebook.zero.common.ZeroToken r0 = com.facebook.zero.common.ZeroToken.A0J
            boolean r0 = r12.equals(r0)
            java.lang.String r5 = "enabled"
            r4 = 0
            if (r0 == 0) goto L_0x0015
            A0A(r11, r4)
            A09(r11, r5)
            return
        L_0x0015:
            int r2 = X.AnonymousClass1Y3.AC0
            X.0UN r1 = r11.A02
            r0 = 8
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.155 r3 = (X.AnonymousClass155) r3
            java.lang.String r6 = ""
            com.google.common.collect.ImmutableSet r0 = r12.A06     // Catch:{ IOException -> 0x0074 }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ IOException -> 0x0074 }
            java.lang.String r2 = X.C24351Tg.A01(r0)     // Catch:{ IOException -> 0x0074 }
            com.google.common.collect.ImmutableList r0 = r12.A04     // Catch:{ IOException -> 0x0071 }
            java.lang.String r10 = A02(r0)     // Catch:{ IOException -> 0x0071 }
            com.google.common.collect.ImmutableMap r9 = r12.A05     // Catch:{ IOException -> 0x006e }
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ IOException -> 0x006e }
            r8.<init>()     // Catch:{ IOException -> 0x006e }
            com.google.common.collect.ImmutableSet r0 = r9.keySet()     // Catch:{ JSONException -> 0x0067 }
            X.1Xv r7 = r0.iterator()     // Catch:{ JSONException -> 0x0067 }
        L_0x0042:
            boolean r0 = r7.hasNext()     // Catch:{ JSONException -> 0x0067 }
            if (r0 == 0) goto L_0x0056
            java.lang.Object r1 = r7.next()     // Catch:{ JSONException -> 0x0067 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ JSONException -> 0x0067 }
            java.lang.Object r0 = r9.get(r1)     // Catch:{ JSONException -> 0x0067 }
            r8.put(r1, r0)     // Catch:{ JSONException -> 0x0067 }
            goto L_0x0042
        L_0x0056:
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x006e }
            com.facebook.zero.common.ZeroTrafficEnforcementConfig r0 = r12.A02     // Catch:{ IOException -> 0x007a }
            java.lang.String r7 = X.C101764t7.A01(r0)     // Catch:{ IOException -> 0x007a }
            java.lang.String r9 = X.C03380Nn.A00(r10)     // Catch:{ IOException -> 0x0065 }
            goto L_0x0084
        L_0x0065:
            r9 = move-exception
            goto L_0x007c
        L_0x0067:
            r1 = move-exception
            java.io.IOException r0 = new java.io.IOException     // Catch:{ IOException -> 0x006e }
            r0.<init>(r1)     // Catch:{ IOException -> 0x006e }
            throw r0     // Catch:{ IOException -> 0x006e }
        L_0x006e:
            r9 = move-exception
            r8 = r6
            goto L_0x0078
        L_0x0071:
            r9 = move-exception
            r10 = r6
            goto L_0x0077
        L_0x0074:
            r9 = move-exception
            r2 = r6
            r10 = r6
        L_0x0077:
            r8 = r6
        L_0x0078:
            r7 = r6
            goto L_0x007c
        L_0x007a:
            r9 = move-exception
            r7 = r6
        L_0x007c:
            java.lang.Class r1 = X.AnonymousClass155.A07
            java.lang.String r0 = "Error serializing enabled ui features, rewrite rules, and pool pricing map, and zero traffic enforcement config."
            X.C010708t.A08(r1, r0, r9)
            r9 = r6
        L_0x0084:
            com.facebook.prefs.shared.FbSharedPreferences r0 = r3.A00
            X.1hn r3 = r0.edit()
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "campaign"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A07
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "current_zero_rating_status"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0G
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "reg_status"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0F
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            r0 = 17
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0A
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "carrier_id"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A08
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "carrier_logo_url"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A09
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "ttl"
            X.1Y8 r1 = r1.A09(r0)
            int r0 = r12.A01
            r3.Bz6(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "enabled_ui_features"
            X.1Y8 r0 = r1.A09(r0)
            r3.BzC(r0, r2)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "rewrite_rules"
            X.1Y8 r0 = r1.A09(r0)
            r3.BzC(r0, r10)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            r0 = 216(0xd8, float:3.03E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            X.1Y8 r0 = r1.A09(r0)
            r3.BzC(r0, r9)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "unregistered_reason"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0I
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "token_hash"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0H
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            r0 = 1344(0x540, float:1.883E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            X.1Y8 r1 = r1.A09(r0)
            int r0 = r12.A00
            r3.Bz6(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "fast_hash"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0C
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "pool_pricing_map"
            X.1Y8 r0 = r1.A09(r0)
            r3.BzC(r0, r8)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "mqtt_host"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0E
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "fbns_host"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0D
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "eligibility_hash"
            X.1Y8 r1 = r1.A09(r0)
            java.lang.String r0 = r12.A0B
            r3.BzC(r1, r0)
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "zero_traffic_enforcement_config"
            X.1Y8 r0 = r1.A09(r0)
            r3.BzC(r0, r7)
            com.google.common.collect.ImmutableList r0 = r12.A03
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x01cd
            com.google.common.collect.ImmutableList r0 = r12.A03     // Catch:{ IOException -> 0x01b8 }
            java.lang.String r6 = A02(r0)     // Catch:{ IOException -> 0x01b8 }
            goto L_0x01c0
        L_0x01b8:
            r2 = move-exception
            java.lang.Class r1 = X.AnonymousClass155.A07
            java.lang.String r0 = "Error serializing backup rewrite rules."
            X.C010708t.A08(r1, r0, r2)
        L_0x01c0:
            X.1Y8 r1 = X.AnonymousClass155.A00(r13)
            java.lang.String r0 = "backup_rewrite_rules"
            X.1Y8 r0 = r1.A09(r0)
            r3.BzC(r0, r6)
        L_0x01cd:
            r3.commit()
            X.0Tq r0 = r11.A06
            java.lang.Object r0 = r0.get()
            if (r13 != r0) goto L_0x023a
            com.google.common.collect.ImmutableSet r0 = r12.A06
            r11.A0E = r0
            r11.A0J()
            com.google.common.collect.ImmutableList r0 = r12.A04
            r11.A0D = r0
            java.lang.Class<X.1eP> r1 = X.C28191eP.class
            java.lang.String r0 = "set_rules"
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A07(r1, r0)
            r11.A06(r0)
            java.lang.String r1 = r12.A0E
            java.lang.String r0 = r12.A0D
            r11.A0B(r1, r0)
            com.google.common.collect.ImmutableList r0 = r12.A03
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0234
            com.google.common.collect.ImmutableList r0 = r12.A03
            r11.A0C = r0
            java.lang.Class<X.1eP> r1 = X.C28191eP.class
            java.lang.String r0 = "set_backup_rules"
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A07(r1, r0)
            r11.A06(r0)
            r3 = r11
            monitor-enter(r3)
            boolean r0 = r11.A0E()     // Catch:{ all -> 0x0230 }
            if (r0 == 0) goto L_0x0228
            r2 = 8
            int r1 = X.AnonymousClass1Y3.AC0     // Catch:{ all -> 0x0230 }
            X.0UN r0 = r11.A02     // Catch:{ all -> 0x0230 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0230 }
            X.155 r1 = (X.AnonymousClass155) r1     // Catch:{ all -> 0x0230 }
            java.lang.String r0 = r1.A07()     // Catch:{ all -> 0x0230 }
            X.AnonymousClass155.A04(r1, r0)     // Catch:{ all -> 0x0230 }
            goto L_0x0233
        L_0x0228:
            com.google.common.collect.ImmutableList r0 = r11.A0C     // Catch:{ all -> 0x0230 }
            if (r0 != 0) goto L_0x0233
            r11.A04()     // Catch:{ all -> 0x0230 }
            goto L_0x0233
        L_0x0230:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0233:
            monitor-exit(r3)
        L_0x0234:
            A0A(r11, r4)
            A09(r11, r5)
        L_0x023a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28191eP.BYw(com.facebook.zero.common.ZeroToken, X.157):void");
    }
}
