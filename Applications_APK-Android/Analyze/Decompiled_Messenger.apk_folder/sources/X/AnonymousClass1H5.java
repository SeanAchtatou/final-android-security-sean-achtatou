package X;

import java.util.Set;

/* renamed from: X.1H5  reason: invalid class name */
public abstract class AnonymousClass1H5<E> extends C21221Fs<E> implements Set<E> {
    public Set A06() {
        return ((C21211Fr) this).A00.A00;
    }

    public boolean equals(Object obj) {
        if (obj == this || A06().equals(obj)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return A06().hashCode();
    }
}
