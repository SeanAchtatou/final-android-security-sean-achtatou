package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.ThreadConnectivityData;

/* renamed from: X.1g1  reason: invalid class name and case insensitive filesystem */
public final class C29191g1 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadConnectivityData(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadConnectivityData[i];
    }
}
