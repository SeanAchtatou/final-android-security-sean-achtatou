package X;

import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.1F0  reason: invalid class name */
public final class AnonymousClass1F0 extends C21061Ew implements C31011j0 {
    public int A00;
    public long A01;
    public AnonymousClass0lr A02;
    public String A03;
    public final QuickPerformanceLogger A04;
    public final C09150gc A05;
    public volatile int A06;
    public volatile int A07;
    public volatile int A08;

    private AnonymousClass0lr A00() {
        if (this.A02 == null) {
            this.A02 = new AnonymousClass0lr();
        }
        return this.A02;
    }

    public C31011j0 ANX(String str, String[] strArr) {
        if (strArr == null) {
            return this;
        }
        A00().A00(str, AnonymousClass36y.A03(strArr), 3);
        return this;
    }

    public C31011j0 Bx9(long j) {
        if (this.A00 != 1 || j == -1) {
            this.A01 = j;
            return this;
        }
        throw new IllegalStateException("You can't collect metadata with custom timestamp, as point appeared in the past but metadata is to be collected in the present");
    }

    public C21061Ew BxA() {
        if (this.A03 != null) {
            AnonymousClass0lr r1 = this.A02;
            if (r1 != null) {
                r1.A03 = true;
            }
            this.A04.markerPoint(this.A08, this.A06, this.A07, this.A03, this.A02, this.A01, this.A00);
            this.A03 = null;
            this.A02 = null;
            this.A01 = -1;
            this.A00 = 0;
            return this;
        }
        throw new IllegalStateException("You should not use PointEditor after point was completed");
    }

    public C31011j0 BxB(boolean z) {
        if (!z) {
            this.A00 = 0;
            return this;
        } else if (this.A01 == -1) {
            this.A00 = 1;
            return this;
        } else {
            throw new IllegalStateException("You can't collect metadata with custom timestamp, as point appeared in the past but metadata is to be collected in the present");
        }
    }

    public AnonymousClass1F0(QuickPerformanceLogger quickPerformanceLogger, C09150gc r2) {
        this.A04 = quickPerformanceLogger;
        this.A05 = r2;
    }

    public C31011j0 ANQ(String str, double d) {
        A00().A00(str, String.valueOf(d), 5);
        return this;
    }

    public C31011j0 ANR(String str, int i) {
        A00().A00(str, String.valueOf(i), 2);
        return this;
    }

    public C31011j0 ANS(String str, long j) {
        A00().A00(str, String.valueOf(j), 2);
        return this;
    }

    public C31011j0 ANT(String str, String str2) {
        A00().A00(str, str2, 1);
        return this;
    }

    public C31011j0 ANU(String str, boolean z) {
        A00().A00(str, String.valueOf(z), 7);
        return this;
    }

    public C31011j0 ANV(String str, double[] dArr) {
        A00().A00(str, AnonymousClass36y.A00(dArr), 6);
        return this;
    }

    public C31011j0 ANW(String str, int[] iArr) {
        A00().A00(str, AnonymousClass36y.A01(iArr), 4);
        return this;
    }

    public C31011j0 ANY(String str, boolean[] zArr) {
        A00().A00(str, AnonymousClass36y.A04(zArr), 8);
        return this;
    }
}
