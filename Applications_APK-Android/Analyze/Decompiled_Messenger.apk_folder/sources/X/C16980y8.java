package X;

import java.util.List;

/* renamed from: X.0y8  reason: invalid class name and case insensitive filesystem */
public final class C16980y8 extends C17770zR {
    public C14940uO A00;
    public C14940uO A01;
    public C14950uP A02;
    public AnonymousClass6I0 A03;
    public List A04;
    public boolean A05;

    public static C37941wd A02(AnonymousClass0p4 r3, String str) {
        C37941wd r1 = new C37941wd();
        r1.A3B(r3, 0, 0, new C16980y8(str));
        return r1;
    }

    public boolean A1K() {
        return true;
    }

    public static C37941wd A00(AnonymousClass0p4 r4) {
        C37941wd r1 = new C37941wd();
        r1.A3B(r4, 0, 0, new C16980y8("Row"));
        return r1;
    }

    public static C37941wd A01(AnonymousClass0p4 r3, int i, int i2) {
        C37941wd r1 = new C37941wd();
        r1.A3B(r3, i, i2, new C16980y8("Row"));
        return r1;
    }

    private C16980y8(String str) {
        super(str);
    }
}
