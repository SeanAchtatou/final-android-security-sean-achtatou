package X;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Rb  reason: invalid class name and case insensitive filesystem */
public final class C23821Rb extends AnonymousClass1QN {
    public int A00;
    public int A01;
    public Throwable A02;
    public AtomicInteger A03;
    private ArrayList A04;
    public final /* synthetic */ AnonymousClass5SY A05;

    public static synchronized AnonymousClass1QO A00(C23821Rb r1) {
        AnonymousClass1QO A022;
        synchronized (r1) {
            A022 = A02(r1, r1.A00);
        }
        return A022;
    }

    public static synchronized AnonymousClass1QO A01(C23821Rb r2, int i) {
        AnonymousClass1QO r0;
        synchronized (r2) {
            ArrayList arrayList = r2.A04;
            r0 = null;
            if (arrayList != null && i < arrayList.size()) {
                r0 = (AnonymousClass1QO) r2.A04.set(i, null);
            }
        }
        return r0;
    }

    public static synchronized AnonymousClass1QO A02(C23821Rb r1, int i) {
        AnonymousClass1QO r0;
        synchronized (r1) {
            ArrayList arrayList = r1.A04;
            if (arrayList == null || i >= arrayList.size()) {
                r0 = null;
            } else {
                r0 = (AnonymousClass1QO) r1.A04.get(i);
            }
        }
        return r0;
    }

    public static void A04(C23821Rb r2, int i, AnonymousClass1QO r4) {
        AnonymousClass1QO r0;
        Throwable th;
        synchronized (r2) {
            if (r4 == A00(r2)) {
                r0 = null;
            } else if (r4 == A02(r2, i)) {
                r0 = A01(r2, i);
            } else {
                r0 = r4;
            }
        }
        if (r0 != null) {
            r0.AT6();
        }
        if (i == 0) {
            r2.A02 = r4.AmS();
        }
        if (r2.A03.incrementAndGet() == r2.A01 && (th = r2.A02) != null) {
            r2.A09(th);
        }
    }

    public synchronized Object B1I() {
        Object obj;
        if (this.A05.A01) {
            A03(this);
        }
        AnonymousClass1QO A002 = A00(this);
        if (A002 != null) {
            obj = A002.B1I();
        } else {
            obj = null;
        }
        return obj;
    }

    public C23821Rb(AnonymousClass5SY r2) {
        this.A05 = r2;
        if (!r2.A01) {
            A03(this);
        }
    }

    public static void A03(C23821Rb r5) {
        if (r5.A03 == null) {
            synchronized (r5) {
                if (r5.A03 == null) {
                    r5.A03 = new AtomicInteger(0);
                    int size = r5.A05.A00.size();
                    r5.A01 = size;
                    r5.A00 = size;
                    r5.A04 = new ArrayList(size);
                    for (int i = 0; i < size; i++) {
                        AnonymousClass1QO r2 = (AnonymousClass1QO) ((C23111Og) r5.A05.A00.get(i)).get();
                        r5.A04.add(r2);
                        r2.CIK(new C29725Eh1(r5, i), C50292dj.A00);
                        if (r2.BBn()) {
                            break;
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        if (r2 >= r1.size()) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        r0 = (X.AnonymousClass1QO) r1.get(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0027, code lost:
        if (r0 == null) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0029, code lost:
        r0.AT6();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002c, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AT6() {
        /*
            r3 = this;
            X.5SY r0 = r3.A05
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x0009
            A03(r3)
        L_0x0009:
            monitor-enter(r3)
            boolean r0 = super.AT6()     // Catch:{ all -> 0x0031 }
            r2 = 0
            if (r0 != 0) goto L_0x0013
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            return r2
        L_0x0013:
            java.util.ArrayList r1 = r3.A04     // Catch:{ all -> 0x0031 }
            r0 = 0
            r3.A04 = r0     // Catch:{ all -> 0x0031 }
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x002f
        L_0x001b:
            int r0 = r1.size()
            if (r2 >= r0) goto L_0x002f
            java.lang.Object r0 = r1.get(r2)
            X.1QO r0 = (X.AnonymousClass1QO) r0
            if (r0 == 0) goto L_0x002c
            r0.AT6()
        L_0x002c:
            int r2 = r2 + 1
            goto L_0x001b
        L_0x002f:
            r0 = 1
            return r0
        L_0x0031:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C23821Rb.AT6():boolean");
    }
}
