package X;

import java.util.Random;

/* renamed from: X.1pU  reason: invalid class name and case insensitive filesystem */
public final class C34381pU {
    public final C25051Yd A00;
    public final Random A01 = AnonymousClass0W9.A01();
    public final C04310Tq A02;

    public static final C34381pU A00(AnonymousClass1XY r1) {
        return new C34381pU(r1);
    }

    public C34381pU(AnonymousClass1XY r2) {
        this.A02 = C04750Wa.A05(r2);
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
