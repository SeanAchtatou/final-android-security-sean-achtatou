package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.provider.perfevents.PerfEventsSession;

/* renamed from: X.05H  reason: invalid class name */
public final class AnonymousClass05H extends C000900o {
    public static final int A02 = ProvidersRegistry.A00.A02("faults");
    private PerfEventsSession A00 = null;
    private boolean A01;

    public AnonymousClass05H() {
        super("profilo_perfevents");
    }

    public int getTracingProviders() {
        TraceContext traceContext;
        if (!this.A01 || (traceContext = this.A00) == null) {
            return 0;
        }
        return traceContext.A02 & getSupportedProviders();
    }

    public void disable() {
        int A03 = C000700l.A03(2042172352);
        this.A01 = false;
        PerfEventsSession perfEventsSession = this.A00;
        if (perfEventsSession != null) {
            perfEventsSession.stop();
            synchronized (perfEventsSession) {
                long j = perfEventsSession.mNativeHandle;
                if (j != 0) {
                    PerfEventsSession.nativeDetach(j);
                    perfEventsSession.mNativeHandle = 0;
                }
            }
        }
        this.A00 = null;
        C000700l.A09(524046475, A03);
    }

    public void enable() {
        IllegalStateException illegalStateException;
        boolean z;
        int A03 = C000700l.A03(-1337747942);
        PerfEventsSession perfEventsSession = this.A00;
        if (perfEventsSession == null) {
            perfEventsSession = new PerfEventsSession();
            this.A00 = perfEventsSession;
        }
        int i = this.A00.A02;
        synchronized (perfEventsSession) {
            if (perfEventsSession.mNativeHandle == 0) {
                z = false;
                boolean z2 = false;
                if ((i & A02) != 0) {
                    z2 = true;
                }
                if (z2) {
                    perfEventsSession.mNativeHandle = PerfEventsSession.nativeAttach(z2, 1, 3, 0.5f);
                }
                if (perfEventsSession.mNativeHandle != 0) {
                    z = true;
                }
            } else {
                illegalStateException = new IllegalStateException("Already attached");
            }
            throw illegalStateException;
        }
        if (z) {
            this.A01 = true;
            synchronized (perfEventsSession) {
                if (perfEventsSession.mThread == null) {
                    Thread thread = new Thread(perfEventsSession.mSessionRunnable, "Prflo:PerfEvt");
                    thread.start();
                    perfEventsSession.mThread = thread;
                } else {
                    illegalStateException = new IllegalStateException("Thread is already running");
                    throw illegalStateException;
                }
            }
        }
        C000700l.A09(-1251311378, A03);
    }

    public int getSupportedProviders() {
        return A02;
    }
}
