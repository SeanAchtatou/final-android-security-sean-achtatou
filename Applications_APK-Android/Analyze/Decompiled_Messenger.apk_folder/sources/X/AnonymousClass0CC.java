package X;

/* renamed from: X.0CC  reason: invalid class name */
public final class AnonymousClass0CC {
    public final AnonymousClass0C8 A00;
    public final boolean A01;
    public final /* synthetic */ AnonymousClass0AH A02;

    public AnonymousClass0CC(AnonymousClass0AH r1, AnonymousClass0C8 r2, boolean z) {
        this.A02 = r1;
        this.A00 = r2;
        this.A01 = z;
    }

    public static void A00(AnonymousClass0CC r4) {
        if (r4.A02.A0o == r4.A00) {
            C010708t.A0J("FbnsConnectionManager", "Preemptive connection succeeded, switch to new connection");
            AnonymousClass0AH r3 = r4.A02;
            r3.A0L(r3.A0n, AnonymousClass0CE.A0B, AnonymousClass0FG.PREEMPTIVE_RECONNECT_SUCCESS);
            AnonymousClass0AH.A02(r4.A02);
        }
    }

    public void A01(String str, int i) {
        AnonymousClass00S.A04(this.A02.A05, new C01980Cj(this, str, i), 1625279192);
    }

    public void A02(String str, String str2, Throwable th) {
        AnonymousClass00S.A04(this.A02.A05, new AnonymousClass0SI(this, str, str2, th), -197035015);
    }
}
