package X;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;

/* renamed from: X.07Z  reason: invalid class name */
public final class AnonymousClass07Z implements Comparator, SortedMap {
    public static SortedMap A00 = new AnonymousClass07Z();

    public void clear() {
    }

    public Comparator comparator() {
        return this;
    }

    public int compare(Object obj, Object obj2) {
        return 0;
    }

    public boolean containsKey(Object obj) {
        return false;
    }

    public boolean containsValue(Object obj) {
        return false;
    }

    public Object get(Object obj) {
        return null;
    }

    public boolean isEmpty() {
        return true;
    }

    public int size() {
        return 0;
    }

    public Object firstKey() {
        throw new NoSuchElementException();
    }

    public Object lastKey() {
        throw new NoSuchElementException();
    }

    public Object put(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    public void putAll(Map map) {
        throw new UnsupportedOperationException();
    }

    public Object remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public Set entrySet() {
        return Collections.emptySet();
    }

    public Set keySet() {
        return Collections.emptySet();
    }

    public Collection values() {
        return Collections.emptySet();
    }

    public SortedMap headMap(Object obj) {
        return A00;
    }

    public SortedMap tailMap(Object obj) {
        return A00;
    }

    public SortedMap subMap(Object obj, Object obj2) {
        return A00;
    }
}
