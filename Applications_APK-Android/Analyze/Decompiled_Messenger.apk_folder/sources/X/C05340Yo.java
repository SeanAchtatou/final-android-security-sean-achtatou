package X;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

/* renamed from: X.0Yo  reason: invalid class name and case insensitive filesystem */
public final class C05340Yo extends PhantomReference {
    public final C05190Xz A00;

    public C05340Yo(Object obj, C05190Xz r2, ReferenceQueue referenceQueue) {
        super(obj, referenceQueue);
        this.A00 = r2;
    }
}
