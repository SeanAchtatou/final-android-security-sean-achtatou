package X;

import com.facebook.funnellogger.FunnelLoggerImpl;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.14R  reason: invalid class name */
public final class AnonymousClass14R extends AnonymousClass0UV {
    public static final C185414b A00(AnonymousClass1XY r0) {
        return FunnelLoggerImpl.A01(r0);
    }
}
