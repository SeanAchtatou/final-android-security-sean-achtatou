package X;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: X.1rm  reason: invalid class name and case insensitive filesystem */
public abstract class C35761rm {
    public int A00(byte[] bArr, int i, int i2) {
        InputStream inputStream = ((C36311sr) this).A00;
        if (inputStream != null) {
            try {
                int read = inputStream.read(bArr, i, i2);
                if (read >= 0) {
                    return read;
                }
                throw new C22147ApX(4);
            } catch (IOException e) {
                throw new C22147ApX(0, e);
            }
        } else {
            throw new C22147ApX(1, "Cannot read from null inputStream");
        }
    }

    public void A01(byte[] bArr, int i, int i2) {
        int i3 = 0;
        while (i3 < i2) {
            int A00 = A00(bArr, i + i3, i2 - i3);
            if (A00 > 0) {
                i3 += A00;
            } else {
                throw new C22147ApX(AnonymousClass08S.A0C("Cannot read. Remote side has closed. Tried to read ", i2, " bytes, but only got ", i3, " bytes."));
            }
        }
    }

    public void A02(byte[] bArr, int i, int i2) {
        OutputStream outputStream = ((C36311sr) this).A01;
        if (outputStream != null) {
            try {
                outputStream.write(bArr, i, i2);
            } catch (IOException e) {
                throw new C22147ApX(0, e);
            }
        } else {
            throw new C22147ApX(1, "Cannot write to null outputStream");
        }
    }
}
