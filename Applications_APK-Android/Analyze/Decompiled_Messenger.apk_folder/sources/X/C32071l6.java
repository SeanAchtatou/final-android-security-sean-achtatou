package X;

import android.net.Uri;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1l6  reason: invalid class name and case insensitive filesystem */
public final class C32071l6 extends C17770zR {
    public static final C22111Jy A04 = C22111Jy.A04;
    public static final MigColorScheme A05 = C17190yT.A00();
    @Comparable(type = 3)
    public int A00 = 0;
    @Comparable(type = 13)
    public Uri A01;
    @Comparable(type = 13)
    public C22111Jy A02 = A04;
    @Comparable(type = 13)
    public AnonymousClass1J0 A03;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r3) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(8);
        ComponentBuilderCBuilderShape0_0S0300000.A08(componentBuilderCBuilderShape0_0S0300000, r3, 0, 0, new C32071l6());
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public C32071l6() {
        super("M4MigUriProfile");
    }
}
