package X;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1Rf  reason: invalid class name and case insensitive filesystem */
public final class C23861Rf implements Callable {
    public final /* synthetic */ C23601Qd A00;
    public final /* synthetic */ C30911iq A01;
    public final /* synthetic */ Object A02;
    public final /* synthetic */ AtomicBoolean A03;

    public C23861Rf(C30911iq r1, Object obj, AtomicBoolean atomicBoolean, C23601Qd r4) {
        this.A01 = r1;
        this.A02 = obj;
        this.A03 = atomicBoolean;
        this.A00 = r4;
    }

    public Object call() {
        AnonymousClass1SS r0;
        AnonymousClass1PS A012;
        InputStream openStream;
        Object A002 = C27491dH.A00(this.A02, null);
        try {
            if (!this.A03.get()) {
                AnonymousClass1NY A003 = this.A01.A03.A00(this.A00);
                if (A003 != null) {
                    C23601Qd r2 = this.A00;
                    r2.B7w();
                    this.A01.A02.BpN(r2);
                } else {
                    C23601Qd r1 = this.A00;
                    r1.B7w();
                    this.A01.A02.BpO(r1);
                    try {
                        C30911iq r7 = this.A01;
                        C23601Qd r6 = this.A00;
                        try {
                            r6.B7w();
                            C24011Rv B17 = r7.A00.B17(r6);
                            if (B17 == null) {
                                r6.B7w();
                                r7.A02.BWT(r6);
                                r0 = null;
                            } else {
                                r6.B7w();
                                r7.A02.BWS(r6);
                                openStream = B17.openStream();
                                r0 = r7.A04.A00(openStream, (int) B17.size());
                                openStream.close();
                                r6.B7w();
                            }
                            if (r0 == null) {
                                C27491dH.A03(A002);
                                return null;
                            }
                            A012 = AnonymousClass1PS.A01(r0);
                            A003 = new AnonymousClass1NY(A012);
                            AnonymousClass1PS.A05(A012);
                        } catch (IOException e) {
                            AnonymousClass02w.A09(C30911iq.A07, e, "Exception reading from cache for %s", r6.B7w());
                            r7.A02.BWR(r6);
                            throw e;
                        } catch (Throwable th) {
                            openStream.close();
                            throw th;
                        }
                    } catch (Exception unused) {
                        C27491dH.A03(A002);
                        return null;
                    } catch (Throwable th2) {
                        AnonymousClass1PS.A05(A012);
                        throw th2;
                    }
                }
                if (!Thread.interrupted()) {
                    return A003;
                }
                A003.close();
                throw new InterruptedException();
            }
            throw new CancellationException();
        } finally {
            C27491dH.A03(A002);
        }
    }
}
