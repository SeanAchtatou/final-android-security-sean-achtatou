package X;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.0CV  reason: invalid class name */
public class AnonymousClass0CV implements AnonymousClass0CW {
    public Throwable A00 = null;
    public final int A01;
    public final long A02;
    public final AnonymousClass0C8 A03;
    public final AnonymousClass0CL A04;
    public final String A05;
    public volatile C01950Cg A06;
    public volatile AnonymousClass0PF A07;

    public void A01() {
        synchronized (this) {
            this.A00 = new TimeoutException();
        }
        if (this.A07 != null) {
            this.A07.A01(this.A01);
        }
        if (this.A06 != null) {
            this.A06.cancel(false);
        }
    }

    public void A03(Throwable th) {
        synchronized (this) {
            this.A00 = th;
        }
        if (this.A07 != null) {
            this.A07.A00(this.A01);
        }
        if (this.A06 != null) {
            this.A06.cancel(false);
        }
    }

    public void A00() {
        if (this.A07 != null) {
            this.A07.A02(this.A01);
        }
        if (this.A06 != null) {
            this.A06.cancel(false);
        }
    }

    public void CMt(long j) {
        if (this.A06 != null) {
            try {
                this.A06.get(j, TimeUnit.MILLISECONDS);
            } catch (CancellationException unused) {
            }
            synchronized (this) {
                Throwable th = this.A00;
                if (th != null) {
                    throw new ExecutionException(th);
                }
            }
        }
    }

    public String toString() {
        return "MqttOperation{mResponseType=" + this.A04 + ", mOperationId=" + this.A01 + ", mCreationTime=" + this.A02 + '}';
    }

    public AnonymousClass0CV(AnonymousClass0C8 r2, String str, AnonymousClass0CL r4, int i, long j) {
        this.A03 = r2;
        this.A05 = str;
        this.A04 = r4;
        this.A01 = i;
        this.A02 = j;
    }

    public void A02(C01950Cg r3) {
        AnonymousClass0A1.A00(r3);
        boolean z = false;
        if (this.A06 == null) {
            z = true;
        }
        AnonymousClass0A1.A02(z);
        this.A06 = r3;
    }

    public int AwM() {
        return this.A01;
    }
}
