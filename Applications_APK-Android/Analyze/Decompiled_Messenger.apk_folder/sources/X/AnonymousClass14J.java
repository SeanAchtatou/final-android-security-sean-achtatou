package X;

import java.util.Stack;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.14J  reason: invalid class name */
public final class AnonymousClass14J {
    private static volatile AnonymousClass14J A03;
    public AnonymousClass0UN A00;
    public Stack A01;
    public final Object A02 = new Object();

    public static final AnonymousClass14J A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass14J.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass14J(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public AnonymousClass15L A01() {
        synchronized (this.A02) {
            if (this.A01.isEmpty()) {
                return null;
            }
            AnonymousClass15L r0 = (AnonymousClass15L) this.A01.peek();
            return r0;
        }
    }

    public String A02() {
        String str;
        AnonymousClass15L A012 = A01();
        return (A012 == null || (str = A012.A01) == null) ? "unknown" : str;
    }

    public Stack A03() {
        Stack stack;
        synchronized (this.A02) {
            stack = (Stack) this.A01.clone();
        }
        return stack;
    }

    private AnonymousClass14J(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = new Stack();
    }
}
