package X;

import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1Tk  reason: invalid class name and case insensitive filesystem */
public final class C24381Tk extends AnonymousClass0UV {
    public static final Long A01() {
        return Long.valueOf((long) StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS);
    }

    public static final C29281gA A00(AnonymousClass1XY r0) {
        return AnonymousClass1U2.A00(r0);
    }
}
