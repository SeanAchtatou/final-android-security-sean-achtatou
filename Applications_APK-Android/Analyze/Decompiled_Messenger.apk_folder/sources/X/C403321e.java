package X;

import android.view.MenuItem;
import com.google.common.collect.ImmutableList;

/* renamed from: X.21e  reason: invalid class name and case insensitive filesystem */
public final class C403321e implements CM2 {
    public final /* synthetic */ ImmutableList A00;

    public C403321e(ImmutableList immutableList) {
        this.A00 = immutableList;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        C24971Xv it = this.A00.iterator();
        while (it.hasNext()) {
            C403521g r2 = (C403521g) it.next();
            if (r2.A02 == menuItem.getItemId()) {
                return r2.A04.Bc1();
            }
        }
        return false;
    }
}
