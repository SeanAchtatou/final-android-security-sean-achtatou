package X;

import android.view.View;

/* renamed from: X.19f  reason: invalid class name and case insensitive filesystem */
public final class C196719f {
    public C196819g A00 = new C196819g();
    public final AnonymousClass19Z A01;

    public View A00(int i, int i2, int i3, int i4) {
        int AxG = this.A01.AxG();
        int AxE = this.A01.AxE();
        int i5 = -1;
        if (i2 > i) {
            i5 = 1;
        }
        View view = null;
        while (i != i2) {
            View Ah1 = this.A01.Ah1(i);
            int Ah8 = this.A01.Ah8(Ah1);
            int Ah5 = this.A01.Ah5(Ah1);
            C196819g r1 = this.A00;
            r1.A04 = AxG;
            r1.A03 = AxE;
            r1.A02 = Ah8;
            r1.A01 = Ah5;
            if (i3 != 0) {
                r1.A00 = 0;
                r1.A00 = 0 | i3;
                if (r1.A00()) {
                    return Ah1;
                }
            }
            if (i4 != 0) {
                r1.A00 = 0;
                r1.A00 = 0 | i4;
                if (r1.A00()) {
                    view = Ah1;
                }
            }
            i += i5;
        }
        return view;
    }

    public boolean A01(View view, int i) {
        C196819g r4 = this.A00;
        int AxG = this.A01.AxG();
        int AxE = this.A01.AxE();
        int Ah8 = this.A01.Ah8(view);
        int Ah5 = this.A01.Ah5(view);
        r4.A04 = AxG;
        r4.A03 = AxE;
        r4.A02 = Ah8;
        r4.A01 = Ah5;
        if (i == 0) {
            return false;
        }
        C196819g r1 = this.A00;
        r1.A00 = 0;
        r1.A00 = i | 0;
        return r1.A00();
    }

    public C196719f(AnonymousClass19Z r2) {
        this.A01 = r2;
    }
}
