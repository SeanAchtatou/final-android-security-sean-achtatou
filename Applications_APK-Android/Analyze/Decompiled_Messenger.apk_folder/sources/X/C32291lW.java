package X;

import android.content.Context;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1lW  reason: invalid class name and case insensitive filesystem */
public interface C32291lW {
    void BvP(Context context);

    void BvS(Context context, NavigationTrigger navigationTrigger, MontageComposerFragmentParams montageComposerFragmentParams);

    void BvV(Context context, ImmutableList immutableList);

    void Bvd(Context context, ThreadKey threadKey, C149266wB r3);

    void Bvn(Context context, C13060qW r2, String str);

    void Bvo(Context context);

    void Bvw(Context context);

    void Bw0(Context context);
}
