package X;

import com.facebook.payments.settings.PaymentSettingsPickerRunTimeData;

/* renamed from: X.1Cu  reason: invalid class name and case insensitive filesystem */
public final class C20521Cu extends C06020ai {
    public final /* synthetic */ C23694Bka A00;
    public final /* synthetic */ PaymentSettingsPickerRunTimeData A01;
    public final /* synthetic */ C55112nY A02;
    public final /* synthetic */ C55102nX A03;

    public C20521Cu(C55112nY r1, C55102nX r2, C23694Bka bka, PaymentSettingsPickerRunTimeData paymentSettingsPickerRunTimeData) {
        this.A02 = r1;
        this.A03 = r2;
        this.A00 = bka;
        this.A01 = paymentSettingsPickerRunTimeData;
    }
}
