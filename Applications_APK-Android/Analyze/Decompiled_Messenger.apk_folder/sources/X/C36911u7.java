package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.messages.Publicity;

/* renamed from: X.1u7  reason: invalid class name and case insensitive filesystem */
public final class C36911u7 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new Publicity(parcel);
    }

    public Object[] newArray(int i) {
        return new Publicity[i];
    }
}
