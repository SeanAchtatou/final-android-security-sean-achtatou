package X;

/* renamed from: X.0iE  reason: invalid class name and case insensitive filesystem */
public enum C09640iE {
    LANGPACK("langpack", "langpack"),
    FBT("fbt", "fbt");
    
    public final String mServerValue;
    public final String mValue;

    private C09640iE(String str, String str2) {
        this.mValue = str;
        this.mServerValue = str2;
    }
}
