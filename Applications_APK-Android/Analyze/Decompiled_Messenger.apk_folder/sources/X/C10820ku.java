package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.fbtrace.FbTraceNode;
import com.google.common.base.Objects;

/* renamed from: X.0ku  reason: invalid class name and case insensitive filesystem */
public final class C10820ku implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        String readString3 = parcel.readString();
        if (Objects.equal(readString, "invalid_id")) {
            return FbTraceNode.A03;
        }
        return new FbTraceNode(readString, readString2, readString3);
    }

    public Object[] newArray(int i) {
        return new FbTraceNode[i];
    }
}
