package X;

import android.os.HandlerThread;
import android.os.Process;

/* renamed from: X.15H  reason: invalid class name */
public final class AnonymousClass15H implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.FbFastHandlerThreadFactory$2";
    public final /* synthetic */ int A00;
    public final /* synthetic */ HandlerThread A01;
    public final /* synthetic */ String A02;

    public AnonymousClass15H(String str, int i, HandlerThread handlerThread) {
        this.A02 = str;
        this.A00 = i;
        this.A01 = handlerThread;
    }

    public void run() {
        Thread.currentThread().setName(this.A02);
        if (this.A00 >= 1) {
            Process.setThreadPriority(this.A01.getThreadId(), this.A00);
        }
    }
}
