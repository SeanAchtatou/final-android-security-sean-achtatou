package X;

import android.content.Context;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1YH  reason: invalid class name */
public class AnonymousClass1YH implements Supplier, AnonymousClass062 {
    public static boolean A05;
    public static final Class A06 = AnonymousClass1YH.class;
    public SQLiteDatabase A00;
    public final Context A01;
    public final String A02;
    private final C04740Vz A03;
    private final ImmutableList A04;

    public int A04() {
        if (this instanceof AnonymousClass1uY) {
            return 204800;
        }
        if ((this instanceof C37061uZ) || (this instanceof AnonymousClass1ua)) {
            return 10240;
        }
        if (this instanceof AnonymousClass1ub) {
            return 25600;
        }
        if (this instanceof C05330Yn) {
            return 102400;
        }
        boolean z = this instanceof C37071uc;
        return 51200;
    }

    public long A05() {
        return !(this instanceof AnonymousClass1ub) ? -1 : 5242880;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:18|19|20|21|22|23|74|24) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x003d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.database.sqlite.SQLiteDatabase A06() {
        /*
            r7 = this;
            monitor-enter(r7)
            X.0Vz r0 = r7.A03     // Catch:{ all -> 0x00d5 }
            r0.ASG()     // Catch:{ all -> 0x00d5 }
            r6 = r7
            monitor-enter(r6)     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r0 = r7.A00     // Catch:{ all -> 0x00cf }
            if (r0 == 0) goto L_0x0014
            boolean r0 = r0.isOpen()     // Catch:{ all -> 0x00cf }
            if (r0 == 0) goto L_0x0014
            goto L_0x009b
        L_0x0014:
            r5 = 0
            r7.A00 = r5     // Catch:{ all -> 0x00cf }
            java.lang.String r2 = "ensureDatabase(%s)"
            java.lang.String r1 = r7.A02     // Catch:{ all -> 0x00cf }
            r0 = 1483097034(0x586643ca, float:1.0127153E15)
            X.C005505z.A05(r2, r1, r0)     // Catch:{ all -> 0x00cf }
            r4 = 0
        L_0x0022:
            r0 = 10
            if (r4 > r0) goto L_0x0047
            r0 = 5
            if (r4 <= r0) goto L_0x002c
            r7.A0B(r5)     // Catch:{ SQLiteException -> 0x0036 }
        L_0x002c:
            android.database.sqlite.SQLiteDatabase r0 = r7.A02()     // Catch:{ SQLiteException -> 0x0036 }
            r7.A00 = r0     // Catch:{ SQLiteException -> 0x0036 }
            r7.A0A(r5)     // Catch:{ SQLiteException -> 0x0036 }
            goto L_0x0047
        L_0x0036:
            r5 = move-exception
            r0 = 30
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x003d }
            goto L_0x0044
        L_0x003d:
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0076 }
            r0.interrupt()     // Catch:{ all -> 0x0076 }
        L_0x0044:
            int r4 = r4 + 1
            goto L_0x0022
        L_0x0047:
            if (r4 <= 0) goto L_0x007b
            X.42t r2 = new X.42t     // Catch:{ all -> 0x0076 }
            android.content.Context r0 = r7.A01     // Catch:{ all -> 0x0076 }
            r2.<init>(r0)     // Catch:{ all -> 0x0076 }
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0076 }
            X.0UN r0 = r2.A00     // Catch:{ all -> 0x0076 }
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x0076 }
            X.09P r3 = (X.AnonymousClass09P) r3     // Catch:{ all -> 0x0076 }
            boolean r0 = r5 instanceof X.Am1     // Catch:{ all -> 0x0076 }
            java.lang.String r2 = " attempts for "
            java.lang.String r1 = "AbstractDatabaseSupplier_RETRIES"
            if (r0 == 0) goto L_0x006c
            java.lang.String r0 = r7.A02     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = X.AnonymousClass08S.A03(r4, r2, r0)     // Catch:{ all -> 0x0076 }
            r3.softReport(r1, r0, r5)     // Catch:{ all -> 0x0076 }
            goto L_0x007b
        L_0x006c:
            java.lang.String r0 = r7.A02     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = X.AnonymousClass08S.A03(r4, r2, r0)     // Catch:{ all -> 0x0076 }
            r3.CGZ(r1, r0, r5)     // Catch:{ all -> 0x0076 }
            goto L_0x007b
        L_0x0076:
            r5 = move-exception
            r0 = 1363895060(0x514b6314, float:5.4596289E10)
            goto L_0x00cb
        L_0x007b:
            r0 = -2122442569(0xffffffff817e18b7, float:-4.6670164E-38)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x00cf }
            android.database.sqlite.SQLiteDatabase r4 = r7.A00     // Catch:{ all -> 0x00cf }
            if (r4 == 0) goto L_0x00ce
            java.lang.Class<X.0cR> r3 = X.C07000cR.class
            monitor-enter(r3)     // Catch:{ all -> 0x00cf }
            java.util.Map r2 = X.C07000cR.A00     // Catch:{ all -> 0x00c8 }
            int r0 = r4.hashCode()     // Catch:{ all -> 0x00c8 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00c8 }
            X.0cS r0 = new X.0cS     // Catch:{ all -> 0x00c8 }
            r0.<init>(r4)     // Catch:{ all -> 0x00c8 }
            r2.put(r1, r0)     // Catch:{ all -> 0x00c8 }
            monitor-exit(r3)     // Catch:{ all -> 0x00cf }
        L_0x009b:
            monitor-exit(r6)     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r0 = r7.A00     // Catch:{ all -> 0x00d5 }
            java.lang.Class<X.0cR> r3 = X.C07000cR.class
            monitor-enter(r3)     // Catch:{ all -> 0x00d5 }
            java.util.Map r1 = X.C07000cR.A00     // Catch:{ all -> 0x00d2 }
            int r0 = r0.hashCode()     // Catch:{ all -> 0x00d2 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00d2 }
            java.lang.Object r2 = r1.get(r0)     // Catch:{ all -> 0x00d2 }
            X.0cS r2 = (X.C07010cS) r2     // Catch:{ all -> 0x00d2 }
            if (r2 == 0) goto L_0x00bc
            com.facebook.common.time.AwakeTimeSinceBootClock r0 = com.facebook.common.time.AwakeTimeSinceBootClock.INSTANCE     // Catch:{ all -> 0x00d2 }
            long r0 = r0.now()     // Catch:{ all -> 0x00d2 }
            r2.A00 = r0     // Catch:{ all -> 0x00d2 }
            goto L_0x00c3
        L_0x00bc:
            java.lang.Class r1 = X.AnonymousClass1YH.A06     // Catch:{ all -> 0x00d2 }
            java.lang.String r0 = "Database was not found"
            X.C010708t.A05(r1, r0)     // Catch:{ all -> 0x00d2 }
        L_0x00c3:
            monitor-exit(r3)     // Catch:{ all -> 0x00d5 }
            android.database.sqlite.SQLiteDatabase r0 = r7.A00     // Catch:{ all -> 0x00d5 }
            monitor-exit(r7)
            return r0
        L_0x00c8:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00cf }
            throw r0     // Catch:{ all -> 0x00cf }
        L_0x00cb:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x00cf }
        L_0x00ce:
            throw r5     // Catch:{ all -> 0x00cf }
        L_0x00cf:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00d5 }
            goto L_0x00d4
        L_0x00d2:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00d5 }
        L_0x00d4:
            throw r0     // Catch:{ all -> 0x00d5 }
        L_0x00d5:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YH.A06():android.database.sqlite.SQLiteDatabase");
    }

    public synchronized void A09() {
        synchronized (this) {
            synchronized (this) {
                SQLiteDatabase sQLiteDatabase = this.A00;
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                    SQLiteDatabase sQLiteDatabase2 = this.A00;
                    synchronized (C07000cR.class) {
                        C07000cR.A00.remove(Integer.valueOf(sQLiteDatabase2.hashCode()));
                    }
                    this.A00 = null;
                }
            }
            this.A01.deleteDatabase(this.A02);
        }
    }

    public void A0A(SQLiteException sQLiteException) {
        boolean z = this instanceof C25771aN;
    }

    private SQLiteDatabase A02() {
        try {
            SQLiteDatabase writableDatabase = new AnonymousClass0Z5(this.A01, this.A02, this.A04, A04(), new DefaultDatabaseErrorHandler()).getWritableDatabase();
            if (A05() != -1) {
                writableDatabase.setMaximumSize(A05());
            }
            return writableDatabase;
        } catch (StackOverflowError unused) {
            C010708t.A0C(A06, "Database %s corrupt and repair overflowed; deleting", this.A02);
            A09();
            return A02();
        }
    }

    public static void A03() {
        synchronized (C07000cR.class) {
            Iterator it = C07000cR.A00.entrySet().iterator();
            while (it.hasNext()) {
                C07010cS r1 = (C07010cS) ((Map.Entry) it.next()).getValue();
                SQLiteDatabase sQLiteDatabase = (SQLiteDatabase) r1.A01.get();
                if (sQLiteDatabase != null) {
                    if (AwakeTimeSinceBootClock.INSTANCE.now() > r1.A00 + 60000) {
                        sQLiteDatabase.close();
                    }
                }
                it.remove();
            }
        }
        SQLiteDatabase.releaseMemory();
    }

    public void A07() {
        if (this instanceof C09260gp) {
            C09260gp r1 = (C09260gp) this;
            if (A05) {
                r1.A09();
                return;
            }
            SQLiteDatabase A012 = C09260gp.A01(r1);
            r1.A08();
            r1.A00.A01(A012.getPath());
        } else if (A05) {
            A09();
        } else {
            A06();
            A08();
        }
    }

    public void A08() {
        C24971Xv it = this.A04.iterator();
        while (it.hasNext()) {
            ((AnonymousClass0W2) it.next()).A05(this.A00);
        }
    }

    public void A0B(SQLiteException sQLiteException) {
        this.A01.deleteDatabase(this.A02);
    }

    public /* bridge */ /* synthetic */ Object get() {
        if (!(this instanceof C09260gp)) {
            return A06();
        }
        return ((C09260gp) this).A06();
    }

    public AnonymousClass1YH(Context context, C04740Vz r2, ImmutableList immutableList, String str) {
        this.A01 = context;
        this.A03 = r2;
        this.A04 = immutableList;
        this.A02 = str;
    }
}
