package X;

import android.os.Build;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.1T6  reason: invalid class name */
public final class AnonymousClass1T6 {
    private static boolean A00;

    public static synchronized void A00() {
        synchronized (AnonymousClass1T6.class) {
            if (!A00) {
                if (Build.VERSION.SDK_INT <= 16) {
                    try {
                        AnonymousClass01u.A01(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1D));
                    } catch (UnsatisfiedLinkError unused) {
                    }
                }
                AnonymousClass01u.A01("native-imagetranscoder");
                A00 = true;
            }
        }
    }
}
