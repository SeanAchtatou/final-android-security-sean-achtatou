package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/* renamed from: X.06X  reason: invalid class name */
public abstract class AnonymousClass06X extends BroadcastReceiver implements AnonymousClass06Y {
    public C006606m A00 = C006606m.A00;
    public String A01;
    private C28141eK A02;

    public abstract AnonymousClass06U A02(Context context, String str);

    public Object A03(AnonymousClass06U r1) {
        return r1;
    }

    public String A04() {
        return "SecureBroadcastReceiver";
    }

    public abstract boolean A08(String str);

    public static void A01(Object obj) {
        if (obj == null) {
            throw new NullPointerException("Object is null!");
        }
    }

    public boolean A07(Context context, Intent intent) {
        C28141eK r2 = this.A02;
        if (r2 == null || r2.A0C(intent, context, null) != null) {
            return true;
        }
        return false;
    }

    private static C24481Tu A00() {
        return C06560bh.A00;
    }

    public void A05(Context context, String str) {
        String A04 = A04();
        Log.e(A04, AnonymousClass08S.A0S("Rejected the intent for the receiver because it was not registered: ", str, ":", A04));
    }

    public final void onReceive(Context context, Intent intent) {
        boolean A002;
        int A012 = C000700l.A01(-1544703797);
        if (this.A01 == null) {
            this.A01 = String.format("%s/%s", context.getPackageName(), getClass().getName());
        }
        String action = intent.getAction();
        AnonymousClass06U A022 = A02(context, action);
        if (A022 != null) {
            if (!C006506l.A01().A02(context, A03(A022), intent)) {
                A00().A00(this.A01, null, "deny", intent);
                C000700l.A0D(intent, -1553093352, A012);
                return;
            }
            synchronized (this) {
                A002 = this.A00.A00(context, this, intent, null);
            }
            if (!A002 || !A07(context, intent)) {
                A00().A00(this.A01, null, "deny", intent);
                C000700l.A0D(intent, -975594931, A012);
                return;
            }
            A00().A00(this.A01, null, "allow", intent);
            A022.Bl1(context, intent, this);
        } else if (!A08(action)) {
            A00().A00(this.A01, null, "deny", intent);
            A05(context, action);
        }
        C000700l.A0D(intent, 1800194351, A012);
    }

    public void A06(C28141eK r1) {
        this.A02 = r1;
    }
}
