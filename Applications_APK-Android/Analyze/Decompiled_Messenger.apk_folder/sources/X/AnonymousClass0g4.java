package X;

import com.facebook.acra.ActionId;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0g4  reason: invalid class name */
public final class AnonymousClass0g4 {
    private static volatile AnonymousClass0g4 A03;
    public List A00 = new ArrayList();
    private AnonymousClass0UN A01;
    private final ReentrantLock A02 = new ReentrantLock();

    public static final AnonymousClass0g4 A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0g4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0g4(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private void A01(AnonymousClass1TL r8) {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A01)).markerStart(5505146, r8.hashCode(), "TaskName", r8.A07);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A01)).markerAnnotate(5505146, r8.hashCode(), "TaskType", r8.A05);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A01)).markerAnnotate(5505146, r8.hashCode(), "InitialActiveCriticalPaths", ((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, this.A01)).A07());
        String str = r8.A07;
        int i = 0;
        for (AnonymousClass1TL r1 : this.A00) {
            if (r1.A07.equals(str)) {
                i = r1.A02 + 1;
                r1.A02 = i;
            }
        }
        r8.A02 = i;
        if (i > 0) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A01)).markerAnnotate(5505146, r8.hashCode(), "ExistingTaskCountInQueue", Integer.toString(r8.A02));
        }
        this.A00.add(r8);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A01)).markerAnnotate(5505146, r8.hashCode(), "CurrentQueueSize", Integer.toString(this.A00.size()));
        A02(this, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public static void A02(AnonymousClass0g4 r10, boolean z) {
        r10.A02.lock();
        try {
            Iterator it = r10.A00.iterator();
            while (it.hasNext()) {
                AnonymousClass1TL r4 = (AnonymousClass1TL) it.next();
                if (((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, r10.A01)).A0B(r4.A00) && (!r4.A01 || !((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, r10.A01)).A09())) {
                    if (z) {
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r10.A01)).markerTag(5505146, r4.hashCode(), "WasTimeoutTriggered");
                    }
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r10.A01)).markerAnnotate(5505146, r4.hashCode(), "FinalActiveCriticalPaths", ((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, r10.A01)).A07());
                    if (r4.A01) {
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r10.A01)).markerAnnotate(5505146, r4.hashCode(), "LoggingOnly", "true");
                    }
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r10.A01)).markerEnd(5505146, r4.hashCode(), (short) 2);
                    if (!r4.A01) {
                        r4.A04.CIC(r4.A03);
                    }
                    it.remove();
                }
            }
        } finally {
            r10.A02.unlock();
        }
    }

    private static boolean A03(AnonymousClass1TL r4, AnonymousClass1TL r5) {
        String str = r4.A06;
        if (str == null && r5.A06 == null && r4.A07.equals(r5.A07)) {
            return true;
        }
        if (str == null || !str.equals(r5.A06) || !r4.A07.equals(r5.A07)) {
            return false;
        }
        return true;
    }

    public ListenableFuture A04(AnonymousClass1TL r7, String str) {
        ListenableFuture listenableFuture;
        this.A02.lock();
        try {
            if (!((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, this.A01)).A0B(r7.A00)) {
                if (!str.equals("KeepExisting")) {
                    if (str.equals("ReplaceExisting")) {
                        for (AnonymousClass1TL r5 : this.A00) {
                            if (A03(r5, r7)) {
                                this.A00.remove(r5);
                                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, this.A01)).markerEnd(5505146, r5.hashCode(), (short) ActionId.ABORTED);
                                A01(r7);
                                r5.A03.cancel(false);
                                listenableFuture = r7.A03;
                                break;
                            }
                        }
                    }
                } else {
                    for (AnonymousClass1TL A032 : this.A00) {
                        if (A03(A032, r7)) {
                            listenableFuture = C05350Yp.A03(null);
                            break;
                        }
                    }
                }
                A01(r7);
            } else {
                if (((C08790fx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsC, this.A01)).A09()) {
                    r7.A01 = true;
                    A01(r7);
                }
                r7.A04.CIC(r7.A03);
            }
            listenableFuture = r7.A03;
            return listenableFuture;
        } finally {
            this.A02.unlock();
        }
    }

    private AnonymousClass0g4(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(2, r3);
    }
}
