package X;

/* renamed from: X.1Qf  reason: invalid class name and case insensitive filesystem */
public final class C23621Qf extends C23711Qo {
    public static final String __redex_internal_original_name = "com.facebook.imagepipeline.producers.ThreadHandoffProducer$1";
    public final /* synthetic */ AnonymousClass1QK A00;
    public final /* synthetic */ C23581Qb A01;
    public final /* synthetic */ AnonymousClass1MN A02;
    public final /* synthetic */ AnonymousClass0mS A03;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C23621Qf(AnonymousClass0mS r1, C23581Qb r2, AnonymousClass1MN r3, AnonymousClass1QK r4, String str, AnonymousClass1MN r6, AnonymousClass1QK r7, C23581Qb r8) {
        super(r2, r3, r4, str);
        this.A03 = r1;
        this.A02 = r6;
        this.A00 = r7;
        this.A01 = r8;
    }

    public void A05(Object obj) {
        this.A02.Bk6(this.A00, "BackgroundThreadHandoffProducer", null);
        this.A03.A00.ByS(this.A01, this.A00);
    }
}
