package X;

/* renamed from: X.1K3  reason: invalid class name */
public final class AnonymousClass1K3 {
    public static int A00(float f) {
        double d;
        if (f > 0.0f) {
            d = ((double) f) + 0.5d;
        } else {
            d = ((double) f) - 0.5d;
        }
        return (int) d;
    }
}
