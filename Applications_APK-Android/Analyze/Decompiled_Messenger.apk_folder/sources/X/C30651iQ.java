package X;

import android.os.Build;

/* renamed from: X.1iQ  reason: invalid class name and case insensitive filesystem */
public final class C30651iQ {
    public static C22831Mz A00(AnonymousClass1N0 r3, C30591iK r4, C22971No r5) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            return new C37421ve(r3.A02(), r5);
        }
        if (i >= 11) {
            return new AnonymousClass1Ns(new AnonymousClass1Nt(r3.A05(0)), r4, r5);
        }
        return new C196539Mj();
    }
}
