package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.0rR  reason: invalid class name and case insensitive filesystem */
public final class C13440rR implements AnonymousClass06U {
    public final /* synthetic */ C13100qf A00;

    public C13440rR(C13100qf r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r10) {
        String str;
        int A002 = AnonymousClass09Y.A00(2008825972);
        if (!((Boolean) this.A00.A0C.get()).booleanValue()) {
            C13100qf r3 = this.A00;
            C17050yF r6 = (C17050yF) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8I, r3.A01);
            r6.A01.add(r6.A00.CIG("maybeStartInterstitial", new C401220j(r3), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, AnonymousClass07B.A00));
        }
        AnonymousClass7GX r62 = (AnonymousClass7GX) AnonymousClass1XX.A03(AnonymousClass1Y3.B70, this.A00.A01);
        r62.A01.CH3(C08870g7.A04);
        AnonymousClass26K A003 = AnonymousClass26K.A00();
        if (((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r62.A00)).Aep(C34341pQ.A00, true)) {
            str = "availability_on";
        } else {
            str = "availability_off";
        }
        A003.A04("current_availability", str);
        r62.A01.AOO(C08870g7.A04, "enter_app", A003.toString(), A003);
        AnonymousClass09Y.A01(-1108269165, A002);
    }
}
