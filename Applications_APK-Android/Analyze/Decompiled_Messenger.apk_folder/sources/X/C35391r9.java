package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1r9  reason: invalid class name and case insensitive filesystem */
public final class C35391r9 {
    public final C25051Yd A00;

    public static final C35391r9 A00(AnonymousClass1XY r1) {
        return new C35391r9(r1);
    }

    public boolean A01() {
        return this.A00.Aem(282325382792517L);
    }

    private C35391r9(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }

    public boolean A02() {
        if (!A01() || this.A00.B4E(845275336671356L, BuildConfig.FLAVOR).isEmpty()) {
            return false;
        }
        return true;
    }
}
