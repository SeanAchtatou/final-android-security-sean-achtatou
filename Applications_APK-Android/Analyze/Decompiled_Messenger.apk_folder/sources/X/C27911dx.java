package X;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1dx  reason: invalid class name and case insensitive filesystem */
public final class C27911dx {
    public final HashMap A00 = new HashMap();

    public final void A00() {
        for (C27941e0 r4 : this.A00.values()) {
            Map map = r4.A00;
            if (map != null) {
                synchronized (map) {
                    for (Object next : r4.A00.values()) {
                        if (next instanceof Closeable) {
                            try {
                                ((Closeable) next).close();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }
            }
            r4.A02();
        }
        this.A00.clear();
    }
}
