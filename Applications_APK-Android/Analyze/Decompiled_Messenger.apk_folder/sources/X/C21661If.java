package X;

import java.util.List;

/* renamed from: X.1If  reason: invalid class name and case insensitive filesystem */
public final class C21661If extends C17850za {
    public final int A00;

    public C21661If(int i, C17760zQ... r2) {
        super(r2);
        this.A00 = i;
    }

    public C21661If(List list) {
        super(list);
        this.A00 = 0;
    }

    public C21661If(C17760zQ... r2) {
        super(r2);
        this.A00 = 0;
    }
}
