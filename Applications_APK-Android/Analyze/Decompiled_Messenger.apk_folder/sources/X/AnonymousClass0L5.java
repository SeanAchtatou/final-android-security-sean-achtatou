package X;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0L5  reason: invalid class name */
public final class AnonymousClass0L5<E> extends AbstractSet<E> implements Serializable {
    private static final long serialVersionUID = 2454657854757543876L;
    private transient Set A00;
    private Map m;

    public boolean add(Object obj) {
        if (this.m.put(obj, Boolean.TRUE) == null) {
            return true;
        }
        return false;
    }

    public void clear() {
        this.m.clear();
    }

    public boolean contains(Object obj) {
        return this.A00.contains(obj);
    }

    public boolean containsAll(Collection collection) {
        return this.A00.containsAll(collection);
    }

    public boolean equals(Object obj) {
        return this.A00.equals(obj);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public boolean isEmpty() {
        return this.m.isEmpty();
    }

    public Iterator iterator() {
        return this.A00.iterator();
    }

    public boolean remove(Object obj) {
        if (this.m.remove(obj) != null) {
            return true;
        }
        return false;
    }

    public boolean retainAll(Collection collection) {
        return this.A00.retainAll(collection);
    }

    public int size() {
        return this.m.size();
    }

    public String toString() {
        return this.A00.toString();
    }

    public AnonymousClass0L5(Map map) {
        this.m = map;
        this.A00 = map.keySet();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.A00 = this.m.keySet();
    }

    public Object[] toArray() {
        return this.A00.toArray();
    }

    public Object[] toArray(Object[] objArr) {
        return this.A00.toArray(objArr);
    }
}
