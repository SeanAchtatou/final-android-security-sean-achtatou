package X;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/* renamed from: X.0PR  reason: invalid class name */
public final class AnonymousClass0PR extends AnonymousClass0BE {
    public final AnonymousClass0AW A00;
    public final AnonymousClass0BF A01;
    public final ExecutorService A02;
    private final List A03;

    public synchronized Future A00(String str) {
        List list;
        list = this.A03;
        try {
        } catch (AnonymousClass0SV e) {
            return AnonymousClass07A.A03(this.A02, new AnonymousClass0SC(e), -1649037840);
        }
        return (Future) ((AnonymousClass0DW) list.get(0)).BDF(new AnonymousClass0PE(str, list, 0));
    }

    public AnonymousClass0PR(ExecutorService executorService, AnonymousClass0BF r5, AnonymousClass0AW r6) {
        super(executorService, r6);
        this.A02 = executorService;
        this.A01 = r5;
        this.A00 = r6;
        ArrayList arrayList = new ArrayList(new ArrayList(C02440Eu.A00));
        this.A03 = arrayList;
        arrayList.add(new AnonymousClass0PS(this));
    }
}
