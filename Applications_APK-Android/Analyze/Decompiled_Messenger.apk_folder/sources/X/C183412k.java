package X;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/* renamed from: X.12k  reason: invalid class name and case insensitive filesystem */
public final class C183412k {
    public final C10140jc _annotationIntrospector;
    public LinkedList _anyGetters;
    public LinkedList _anySetters;
    public final C10070jV _classDef;
    public final C10470kA _config;
    public LinkedList _creatorProperties;
    public final boolean _forSerialization;
    public HashSet _ignoredPropertyNames;
    public LinkedHashMap _injectables;
    public LinkedList _jsonValueGetters;
    public final String _mutatorPrefix;
    public final LinkedHashMap _properties = new LinkedHashMap();
    public final C10030jR _type;
    public final C10160je _visibilityChecker;

    private void _doAddInjectable(Object obj, C183512m r8) {
        if (obj != null) {
            if (this._injectables == null) {
                this._injectables = new LinkedHashMap();
            }
            if (((C183512m) this._injectables.put(obj, r8)) != null) {
                throw new IllegalArgumentException(AnonymousClass08S.A0T("Duplicate injectable value with id '", String.valueOf(obj), "' (of type ", obj.getClass().getName(), ")"));
            }
        }
    }

    private C29101fs _property(String str) {
        C29101fs r2 = (C29101fs) this._properties.get(str);
        if (r2 != null) {
            return r2;
        }
        C29101fs r22 = new C29101fs(str, this._annotationIntrospector, this._forSerialization);
        this._properties.put(str, r22);
        return r22;
    }

    public static void reportProblem(C183412k r3, String str) {
        throw new IllegalArgumentException("Problem with definition of " + r3._classDef + ": " + str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:201:0x031b, code lost:
        if (r1 != false) goto L_0x031d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x034e, code lost:
        if (r1 != false) goto L_0x0350;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005a, code lost:
        if (r1.hasIgnoreMarker(r6) == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:323:0x04f4, code lost:
        if (r3.hasField() != false) goto L_0x04f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:342:0x0552, code lost:
        if (r3.hasGetter() != false) goto L_0x0554;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0014, code lost:
        if (r15._config.isEnabled(X.C26771bz.ALLOW_FINAL_FIELDS_AS_MUTATORS) != false) goto L_0x0016;
     */
    /* JADX WARNING: Removed duplicated region for block: B:327:0x050a  */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x051a  */
    /* JADX WARNING: Removed duplicated region for block: B:332:0x0522  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C183412k collect() {
        /*
            r15 = this;
            java.util.LinkedHashMap r0 = r15._properties
            r0.clear()
            X.0jc r1 = r15._annotationIntrospector
            boolean r0 = r15._forSerialization
            if (r0 != 0) goto L_0x0016
            X.0kA r2 = r15._config
            X.1bz r0 = X.C26771bz.ALLOW_FINAL_FIELDS_AS_MUTATORS
            boolean r0 = r2.isEnabled(r0)
            r4 = 1
            if (r0 == 0) goto L_0x0017
        L_0x0016:
            r4 = 0
        L_0x0017:
            X.0jV r0 = r15._classDef
            java.lang.Iterable r0 = r0.fields()
            java.util.Iterator r3 = r0.iterator()
        L_0x0021:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0088
            java.lang.Object r6 = r3.next()
            X.1fr r6 = (X.C29091fr) r6
            java.lang.String r2 = r6.getName()
            r8 = 0
            if (r1 == 0) goto L_0x003e
            boolean r0 = r15._forSerialization
            if (r0 == 0) goto L_0x007e
            X.4Hw r0 = r1.findNameForSerialization(r6)
            if (r0 != 0) goto L_0x0085
        L_0x003e:
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0047
            r8 = r2
        L_0x0047:
            r9 = 0
            if (r8 == 0) goto L_0x004b
            r9 = 1
        L_0x004b:
            if (r9 != 0) goto L_0x0053
            X.0je r0 = r15._visibilityChecker
            boolean r9 = r0.isFieldVisible(r6)
        L_0x0053:
            if (r1 == 0) goto L_0x005c
            boolean r0 = r1.hasIgnoreMarker(r6)
            r10 = 1
            if (r0 != 0) goto L_0x005d
        L_0x005c:
            r10 = 0
        L_0x005d:
            if (r4 == 0) goto L_0x0070
            if (r8 != 0) goto L_0x0070
            if (r10 != 0) goto L_0x0070
            java.lang.reflect.Field r0 = r6._field
            int r0 = r0.getModifiers()
            boolean r0 = java.lang.reflect.Modifier.isFinal(r0)
            if (r0 == 0) goto L_0x0070
            goto L_0x0021
        L_0x0070:
            X.1fs r0 = r15._property(r2)
            X.1fv r5 = new X.1fv
            X.1fv r7 = r0._fields
            r5.<init>(r6, r7, r8, r9, r10)
            r0._fields = r5
            goto L_0x0021
        L_0x007e:
            X.4Hw r0 = r1.findNameForDeserialization(r6)
            if (r0 != 0) goto L_0x0085
            goto L_0x003e
        L_0x0085:
            java.lang.String r8 = r0._simpleName
            goto L_0x003e
        L_0x0088:
            X.0jc r1 = r15._annotationIntrospector
            X.0jV r2 = r15._classDef
            X.12t r0 = r2._memberMethods
            if (r0 != 0) goto L_0x0093
            X.C10070jV.resolveMemberMethods(r2)
        L_0x0093:
            X.12t r0 = r2._memberMethods
            java.util.Iterator r5 = r0.iterator()
        L_0x0099:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x01cd
            java.lang.Object r7 = r5.next()
            X.1fw r7 = (X.C29141fw) r7
            int r2 = r7.getParameterCount()
            if (r2 != 0) goto L_0x013e
            if (r1 == 0) goto L_0x00db
            boolean r0 = r1.hasAnyGetterAnnotation(r7)
            if (r0 == 0) goto L_0x00c4
            java.util.LinkedList r0 = r15._anyGetters
            if (r0 != 0) goto L_0x00be
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
            r15._anyGetters = r0
        L_0x00be:
            java.util.LinkedList r0 = r15._anyGetters
            r0.add(r7)
            goto L_0x0099
        L_0x00c4:
            boolean r0 = r1.hasAsValueAnnotation(r7)
            if (r0 == 0) goto L_0x00db
            java.util.LinkedList r0 = r15._jsonValueGetters
            if (r0 != 0) goto L_0x00d5
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
            r15._jsonValueGetters = r0
        L_0x00d5:
            java.util.LinkedList r0 = r15._jsonValueGetters
            r0.add(r7)
            goto L_0x0099
        L_0x00db:
            r9 = 0
            if (r1 != 0) goto L_0x0139
            r0 = r9
        L_0x00df:
            if (r0 == 0) goto L_0x00e3
            java.lang.String r9 = r0._simpleName
        L_0x00e3:
            if (r9 != 0) goto L_0x011c
            java.lang.String r0 = r7.getName()
            java.lang.String r2 = X.AnonymousClass134.okNameForRegularGetter(r7, r0)
            if (r2 != 0) goto L_0x0115
            java.lang.String r0 = r7.getName()
            java.lang.String r2 = X.AnonymousClass134.okNameForIsGetter(r7, r0)
            if (r2 == 0) goto L_0x0099
            X.0je r0 = r15._visibilityChecker
            boolean r10 = r0.isIsGetterVisible(r7)
        L_0x00ff:
            if (r1 != 0) goto L_0x0110
            r11 = 0
        L_0x0102:
            X.1fs r0 = r15._property(r2)
            X.1fv r6 = new X.1fv
            X.1fv r8 = r0._getters
            r6.<init>(r7, r8, r9, r10, r11)
            r0._getters = r6
            goto L_0x0099
        L_0x0110:
            boolean r11 = r1.hasIgnoreMarker(r7)
            goto L_0x0102
        L_0x0115:
            X.0je r0 = r15._visibilityChecker
            boolean r10 = r0.isGetterVisible(r7)
            goto L_0x00ff
        L_0x011c:
            java.lang.String r0 = r7.getName()
            java.lang.String r2 = X.AnonymousClass134.okNameForIsGetter(r7, r0)
            if (r2 != 0) goto L_0x012a
            java.lang.String r2 = X.AnonymousClass134.okNameForRegularGetter(r7, r0)
        L_0x012a:
            if (r2 != 0) goto L_0x0130
            java.lang.String r2 = r7.getName()
        L_0x0130:
            int r0 = r9.length()
            if (r0 != 0) goto L_0x0137
            r9 = r2
        L_0x0137:
            r10 = 1
            goto L_0x00ff
        L_0x0139:
            X.4Hw r0 = r1.findNameForSerialization(r7)
            goto L_0x00df
        L_0x013e:
            r0 = 1
            if (r2 != r0) goto L_0x01b0
            r9 = 0
            if (r1 != 0) goto L_0x01ab
            r0 = r9
        L_0x0145:
            if (r0 == 0) goto L_0x0149
            java.lang.String r9 = r0._simpleName
        L_0x0149:
            java.lang.String r4 = r15._mutatorPrefix
            if (r9 != 0) goto L_0x0184
            java.lang.String r2 = r7.getName()
            boolean r0 = r2.startsWith(r4)
            if (r0 == 0) goto L_0x0182
            int r0 = r4.length()
            java.lang.String r0 = r2.substring(r0)
            java.lang.String r2 = X.AnonymousClass134.manglePropertyName(r0)
        L_0x0163:
            if (r2 == 0) goto L_0x0099
            X.0je r0 = r15._visibilityChecker
            boolean r10 = r0.isSetterVisible(r7)
        L_0x016b:
            if (r1 != 0) goto L_0x017d
            r11 = 0
        L_0x016e:
            X.1fs r0 = r15._property(r2)
            X.1fv r6 = new X.1fv
            X.1fv r8 = r0._setters
            r6.<init>(r7, r8, r9, r10, r11)
            r0._setters = r6
            goto L_0x0099
        L_0x017d:
            boolean r11 = r1.hasIgnoreMarker(r7)
            goto L_0x016e
        L_0x0182:
            r2 = 0
            goto L_0x0163
        L_0x0184:
            java.lang.String r2 = r7.getName()
            boolean r0 = r2.startsWith(r4)
            if (r0 == 0) goto L_0x01a9
            int r0 = r4.length()
            java.lang.String r0 = r2.substring(r0)
            java.lang.String r2 = X.AnonymousClass134.manglePropertyName(r0)
        L_0x019a:
            if (r2 != 0) goto L_0x01a0
            java.lang.String r2 = r7.getName()
        L_0x01a0:
            int r0 = r9.length()
            if (r0 != 0) goto L_0x01a7
            r9 = r2
        L_0x01a7:
            r10 = 1
            goto L_0x016b
        L_0x01a9:
            r2 = 0
            goto L_0x019a
        L_0x01ab:
            X.4Hw r0 = r1.findNameForDeserialization(r7)
            goto L_0x0145
        L_0x01b0:
            r0 = 2
            if (r2 != r0) goto L_0x0099
            if (r1 == 0) goto L_0x0099
            boolean r0 = r1.hasAnySetterAnnotation(r7)
            if (r0 == 0) goto L_0x0099
            java.util.LinkedList r0 = r15._anySetters
            if (r0 != 0) goto L_0x01c6
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
            r15._anySetters = r0
        L_0x01c6:
            java.util.LinkedList r0 = r15._anySetters
            r0.add(r7)
            goto L_0x0099
        L_0x01cd:
            X.0jc r1 = r15._annotationIntrospector
            if (r1 == 0) goto L_0x027d
            X.0jV r3 = r15._classDef
            boolean r0 = r3._creatorsResolved
            if (r0 != 0) goto L_0x01da
            X.C10070jV.resolveCreators(r3)
        L_0x01da:
            java.util.List r0 = r3._constructors
            java.util.Iterator r7 = r0.iterator()
        L_0x01e0:
            boolean r0 = r7.hasNext()
            r8 = 0
            if (r0 == 0) goto L_0x0229
            java.lang.Object r6 = r7.next()
            X.1fx r6 = (X.C29151fx) r6
            java.util.LinkedList r0 = r15._creatorProperties
            if (r0 != 0) goto L_0x01f8
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
            r15._creatorProperties = r0
        L_0x01f8:
            java.lang.reflect.Constructor r0 = r6._constructor
            java.lang.Class[] r0 = r0.getParameterTypes()
            int r5 = r0.length
            r4 = 0
        L_0x0200:
            if (r4 >= r5) goto L_0x01e0
            X.137 r10 = r6.getParameter(r4)
            X.4Hw r0 = r1.findNameForDeserialization(r10)
            if (r0 != 0) goto L_0x0226
            r12 = r8
        L_0x020d:
            if (r12 == 0) goto L_0x0223
            X.1fs r3 = r15._property(r12)
            X.1fv r9 = new X.1fv
            X.1fv r11 = r3._ctorParameters
            r13 = 1
            r14 = 0
            r9.<init>(r10, r11, r12, r13, r14)
            r3._ctorParameters = r9
            java.util.LinkedList r0 = r15._creatorProperties
            r0.add(r3)
        L_0x0223:
            int r4 = r4 + 1
            goto L_0x0200
        L_0x0226:
            java.lang.String r12 = r0._simpleName
            goto L_0x020d
        L_0x0229:
            X.0jV r3 = r15._classDef
            boolean r0 = r3._creatorsResolved
            if (r0 != 0) goto L_0x0232
            X.C10070jV.resolveCreators(r3)
        L_0x0232:
            java.util.List r0 = r3._creatorMethods
            java.util.Iterator r7 = r0.iterator()
        L_0x0238:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x027d
            java.lang.Object r6 = r7.next()
            X.1fw r6 = (X.C29141fw) r6
            java.util.LinkedList r0 = r15._creatorProperties
            if (r0 != 0) goto L_0x024f
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
            r15._creatorProperties = r0
        L_0x024f:
            int r5 = r6.getParameterCount()
            r4 = 0
        L_0x0254:
            if (r4 >= r5) goto L_0x0238
            X.137 r10 = r6.getParameter(r4)
            X.4Hw r0 = r1.findNameForDeserialization(r10)
            if (r0 != 0) goto L_0x027a
            r12 = r8
        L_0x0261:
            if (r12 == 0) goto L_0x0277
            X.1fs r3 = r15._property(r12)
            X.1fv r9 = new X.1fv
            X.1fv r11 = r3._ctorParameters
            r13 = 1
            r14 = 0
            r9.<init>(r10, r11, r12, r13, r14)
            r3._ctorParameters = r9
            java.util.LinkedList r0 = r15._creatorProperties
            r0.add(r3)
        L_0x0277:
            int r4 = r4 + 1
            goto L_0x0254
        L_0x027a:
            java.lang.String r12 = r0._simpleName
            goto L_0x0261
        L_0x027d:
            X.0jc r4 = r15._annotationIntrospector
            if (r4 == 0) goto L_0x02c9
            X.0jV r0 = r15._classDef
            java.lang.Iterable r0 = r0.fields()
            java.util.Iterator r2 = r0.iterator()
        L_0x028b:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x029f
            java.lang.Object r1 = r2.next()
            X.1fr r1 = (X.C29091fr) r1
            java.lang.Object r0 = r4.findInjectableValueId(r1)
            r15._doAddInjectable(r0, r1)
            goto L_0x028b
        L_0x029f:
            X.0jV r1 = r15._classDef
            X.12t r0 = r1._memberMethods
            if (r0 != 0) goto L_0x02a8
            X.C10070jV.resolveMemberMethods(r1)
        L_0x02a8:
            X.12t r0 = r1._memberMethods
            java.util.Iterator r3 = r0.iterator()
        L_0x02ae:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x02c9
            java.lang.Object r2 = r3.next()
            X.1fw r2 = (X.C29141fw) r2
            int r1 = r2.getParameterCount()
            r0 = 1
            if (r1 != r0) goto L_0x02ae
            java.lang.Object r0 = r4.findInjectableValueId(r2)
            r15._doAddInjectable(r0, r2)
            goto L_0x02ae
        L_0x02c9:
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r5 = r0.iterator()
            X.0kA r1 = r15._config
            X.1bz r0 = X.C26771bz.INFER_PROPERTY_MUTATORS
            boolean r0 = r1.isEnabled(r0)
            r4 = r0 ^ 1
        L_0x02dd:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0418
            java.lang.Object r0 = r5.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getValue()
            X.1fs r2 = (X.C29101fs) r2
            X.1fv r3 = r2._fields
            r1 = r3
        L_0x02f2:
            if (r1 == 0) goto L_0x0415
            boolean r0 = r1.isVisible
            if (r0 == 0) goto L_0x0411
            r0 = 1
        L_0x02f9:
            if (r0 != 0) goto L_0x031d
            X.1fv r1 = r2._getters
        L_0x02fd:
            if (r1 == 0) goto L_0x040e
            boolean r0 = r1.isVisible
            if (r0 == 0) goto L_0x040a
            r0 = 1
        L_0x0304:
            if (r0 != 0) goto L_0x031d
            X.1fv r1 = r2._setters
        L_0x0308:
            if (r1 == 0) goto L_0x0407
            boolean r0 = r1.isVisible
            if (r0 == 0) goto L_0x0403
            r0 = 1
        L_0x030f:
            if (r0 != 0) goto L_0x031d
            X.1fv r1 = r2._ctorParameters
        L_0x0313:
            if (r1 == 0) goto L_0x0400
            boolean r0 = r1.isVisible
            if (r0 == 0) goto L_0x03fc
            r1 = 1
        L_0x031a:
            r0 = 0
            if (r1 == 0) goto L_0x031e
        L_0x031d:
            r0 = 1
        L_0x031e:
            if (r0 != 0) goto L_0x0324
            r5.remove()
            goto L_0x02dd
        L_0x0324:
            r1 = r3
        L_0x0325:
            if (r1 == 0) goto L_0x0388
            boolean r0 = r1.isMarkedIgnored
            if (r0 == 0) goto L_0x0385
            r0 = 1
        L_0x032c:
            if (r0 != 0) goto L_0x0350
            X.1fv r1 = r2._getters
        L_0x0330:
            if (r1 == 0) goto L_0x0383
            boolean r0 = r1.isMarkedIgnored
            if (r0 == 0) goto L_0x0380
            r0 = 1
        L_0x0337:
            if (r0 != 0) goto L_0x0350
            X.1fv r1 = r2._setters
        L_0x033b:
            if (r1 == 0) goto L_0x037e
            boolean r0 = r1.isMarkedIgnored
            if (r0 == 0) goto L_0x037b
            r0 = 1
        L_0x0342:
            if (r0 != 0) goto L_0x0350
            X.1fv r1 = r2._ctorParameters
        L_0x0346:
            if (r1 == 0) goto L_0x0379
            boolean r0 = r1.isMarkedIgnored
            if (r0 == 0) goto L_0x0376
            r1 = 1
        L_0x034d:
            r0 = 0
            if (r1 == 0) goto L_0x0351
        L_0x0350:
            r0 = 1
        L_0x0351:
            if (r0 == 0) goto L_0x03d6
            boolean r0 = r2.isExplicitlyIncluded()
            if (r0 != 0) goto L_0x038a
            r5.remove()
            java.lang.String r1 = r2.getName()
            boolean r0 = r15._forSerialization
            if (r0 != 0) goto L_0x02dd
            java.util.HashSet r0 = r15._ignoredPropertyNames
            if (r0 != 0) goto L_0x036f
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r15._ignoredPropertyNames = r0
        L_0x036f:
            java.util.HashSet r0 = r15._ignoredPropertyNames
            r0.add(r1)
            goto L_0x02dd
        L_0x0376:
            X.1fv r1 = r1.next
            goto L_0x0346
        L_0x0379:
            r1 = 0
            goto L_0x034d
        L_0x037b:
            X.1fv r1 = r1.next
            goto L_0x033b
        L_0x037e:
            r0 = 0
            goto L_0x0342
        L_0x0380:
            X.1fv r1 = r1.next
            goto L_0x0330
        L_0x0383:
            r0 = 0
            goto L_0x0337
        L_0x0385:
            X.1fv r1 = r1.next
            goto L_0x0325
        L_0x0388:
            r0 = 0
            goto L_0x032c
        L_0x038a:
            if (r3 == 0) goto L_0x0390
            X.1fv r3 = r3.withoutIgnored()
        L_0x0390:
            r2._fields = r3
            X.1fv r0 = r2._getters
            if (r0 == 0) goto L_0x039a
            X.1fv r0 = r0.withoutIgnored()
        L_0x039a:
            r2._getters = r0
            X.1fv r0 = r2._setters
            if (r0 == 0) goto L_0x03a4
            X.1fv r0 = r0.withoutIgnored()
        L_0x03a4:
            r2._setters = r0
            X.1fv r0 = r2._ctorParameters
            if (r0 == 0) goto L_0x03ae
            X.1fv r0 = r0.withoutIgnored()
        L_0x03ae:
            r2._ctorParameters = r0
            boolean r0 = r15._forSerialization
            if (r0 != 0) goto L_0x03d6
            X.12m r1 = r2.getMutator()
            r0 = 0
            if (r1 == 0) goto L_0x03bc
            r0 = 1
        L_0x03bc:
            if (r0 != 0) goto L_0x03d6
            java.lang.String r1 = r2.getName()
            boolean r0 = r15._forSerialization
            if (r0 != 0) goto L_0x03d6
            java.util.HashSet r0 = r15._ignoredPropertyNames
            if (r0 != 0) goto L_0x03d1
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r15._ignoredPropertyNames = r0
        L_0x03d1:
            java.util.HashSet r0 = r15._ignoredPropertyNames
            r0.add(r1)
        L_0x03d6:
            X.1fv r0 = r2._getters
            X.1fv r1 = X.C29101fs._removeNonVisible(r0)
            r2._getters = r1
            X.1fv r0 = r2._ctorParameters
            X.1fv r0 = X.C29101fs._removeNonVisible(r0)
            r2._ctorParameters = r0
            if (r4 != 0) goto L_0x03ea
            if (r1 != 0) goto L_0x02dd
        L_0x03ea:
            X.1fv r0 = r2._fields
            X.1fv r0 = X.C29101fs._removeNonVisible(r0)
            r2._fields = r0
            X.1fv r0 = r2._setters
            X.1fv r0 = X.C29101fs._removeNonVisible(r0)
            r2._setters = r0
            goto L_0x02dd
        L_0x03fc:
            X.1fv r1 = r1.next
            goto L_0x0313
        L_0x0400:
            r1 = 0
            goto L_0x031a
        L_0x0403:
            X.1fv r1 = r1.next
            goto L_0x0308
        L_0x0407:
            r0 = 0
            goto L_0x030f
        L_0x040a:
            X.1fv r1 = r1.next
            goto L_0x02fd
        L_0x040e:
            r0 = 0
            goto L_0x0304
        L_0x0411:
            X.1fv r1 = r1.next
            goto L_0x02f2
        L_0x0415:
            r0 = 0
            goto L_0x02f9
        L_0x0418:
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r5 = r0.iterator()
            r4 = 0
        L_0x0423:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0467
            java.lang.Object r0 = r5.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r3 = r0.getValue()
            X.1fs r3 = (X.C29101fs) r3
            X.1fv r0 = r3._fields
            r2 = 0
            X.1fv r1 = X.C29101fs.findRenamed(r3, r0, r2)
            X.1fv r0 = r3._getters
            X.1fv r1 = X.C29101fs.findRenamed(r3, r0, r1)
            X.1fv r0 = r3._setters
            X.1fv r1 = X.C29101fs.findRenamed(r3, r0, r1)
            X.1fv r0 = r3._ctorParameters
            X.1fv r0 = X.C29101fs.findRenamed(r3, r0, r1)
            if (r0 == 0) goto L_0x0452
            java.lang.String r2 = r0.explicitName
        L_0x0452:
            if (r2 == 0) goto L_0x0423
            if (r4 != 0) goto L_0x045b
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
        L_0x045b:
            X.1fs r0 = new X.1fs
            r0.<init>(r3, r2)
            r4.add(r0)
            r5.remove()
            goto L_0x0423
        L_0x0467:
            if (r4 == 0) goto L_0x04b4
            java.util.Iterator r4 = r4.iterator()
        L_0x046d:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x04b4
            java.lang.Object r3 = r4.next()
            X.1fs r3 = (X.C29101fs) r3
            java.lang.String r1 = r3.getName()
            java.util.LinkedHashMap r0 = r15._properties
            java.lang.Object r0 = r0.get(r1)
            X.1fs r0 = (X.C29101fs) r0
            if (r0 != 0) goto L_0x04b0
            java.util.LinkedHashMap r0 = r15._properties
            r0.put(r1, r3)
        L_0x048c:
            java.util.LinkedList r0 = r15._creatorProperties
            if (r0 == 0) goto L_0x046d
            r2 = 0
        L_0x0491:
            java.util.LinkedList r0 = r15._creatorProperties
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x046d
            java.util.LinkedList r0 = r15._creatorProperties
            java.lang.Object r0 = r0.get(r2)
            X.1fs r0 = (X.C29101fs) r0
            java.lang.String r1 = r0._internalName
            java.lang.String r0 = r3._internalName
            if (r1 != r0) goto L_0x04ad
            java.util.LinkedList r0 = r15._creatorProperties
            r0.set(r2, r3)
            goto L_0x046d
        L_0x04ad:
            int r2 = r2 + 1
            goto L_0x0491
        L_0x04b0:
            r0.addAll(r3)
            goto L_0x048c
        L_0x04b4:
            X.0jc r1 = r15._annotationIntrospector
            if (r1 != 0) goto L_0x0583
            r4 = 0
        L_0x04b9:
            if (r4 != 0) goto L_0x055f
            X.0kA r0 = r15._config
            X.0jr r0 = r0._base
            X.BEG r4 = r0._propertyNamingStrategy
        L_0x04c1:
            if (r4 == 0) goto L_0x058b
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Collection r1 = r0.values()
            java.util.LinkedHashMap r0 = r15._properties
            int r0 = r0.size()
            X.1fs[] r0 = new X.C29101fs[r0]
            java.lang.Object[] r7 = r1.toArray(r0)
            X.1fs[] r7 = (X.C29101fs[]) r7
            java.util.LinkedHashMap r0 = r15._properties
            r0.clear()
            int r6 = r7.length
            r5 = 0
        L_0x04de:
            if (r5 >= r6) goto L_0x058b
            r3 = r7[r5]
            java.lang.String r2 = r3.getName()
            boolean r0 = r15._forSerialization
            if (r0 == 0) goto L_0x0526
            boolean r0 = r3.hasGetter()
            if (r0 != 0) goto L_0x0554
            boolean r0 = r3.hasField()
            if (r0 == 0) goto L_0x0500
        L_0x04f6:
            X.0kA r1 = r15._config
            X.1fr r0 = r3.getField()
            java.lang.String r2 = r4.nameForField(r1, r0, r2)
        L_0x0500:
            java.lang.String r0 = r3.getName()
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x0510
            X.1fs r0 = new X.1fs
            r0.<init>(r3, r2)
            r3 = r0
        L_0x0510:
            java.util.LinkedHashMap r0 = r15._properties
            java.lang.Object r0 = r0.get(r2)
            X.1fs r0 = (X.C29101fs) r0
            if (r0 != 0) goto L_0x0522
            java.util.LinkedHashMap r0 = r15._properties
            r0.put(r2, r3)
        L_0x051f:
            int r5 = r5 + 1
            goto L_0x04de
        L_0x0522:
            r0.addAll(r3)
            goto L_0x051f
        L_0x0526:
            boolean r0 = r3.hasSetter()
            if (r0 == 0) goto L_0x0537
            X.0kA r1 = r15._config
            X.1fw r0 = r3.getSetter()
            java.lang.String r2 = r4.nameForSetterMethod(r1, r0, r2)
            goto L_0x0500
        L_0x0537:
            boolean r0 = r3.hasConstructorParameter()
            if (r0 == 0) goto L_0x0548
            X.0kA r1 = r15._config
            X.137 r0 = r3.getConstructorParameter()
            java.lang.String r2 = r4.nameForConstructorParameter(r1, r0, r2)
            goto L_0x0500
        L_0x0548:
            boolean r0 = r3.hasField()
            if (r0 != 0) goto L_0x04f6
            boolean r0 = r3.hasGetter()
            if (r0 == 0) goto L_0x0500
        L_0x0554:
            X.0kA r1 = r15._config
            X.1fw r0 = r3.getGetter()
            java.lang.String r2 = r4.nameForGetterMethod(r1, r0, r2)
            goto L_0x0500
        L_0x055f:
            boolean r0 = r4 instanceof X.BEG
            if (r0 == 0) goto L_0x0567
            X.BEG r4 = (X.BEG) r4
            goto L_0x04c1
        L_0x0567:
            boolean r0 = r4 instanceof java.lang.Class
            if (r0 == 0) goto L_0x07a2
            java.lang.Class r4 = (java.lang.Class) r4
            java.lang.Class<X.BEG> r0 = X.BEG.class
            boolean r0 = r0.isAssignableFrom(r4)
            if (r0 == 0) goto L_0x0790
            X.0kA r0 = r15._config
            boolean r0 = r0.canOverrideAccessModifiers()
            java.lang.Object r4 = X.C29081fq.createInstance(r4, r0)
            X.BEG r4 = (X.BEG) r4
            goto L_0x04c1
        L_0x0583:
            X.0jV r0 = r15._classDef
            java.lang.Object r4 = r1.findNamingStrategy(r0)
            goto L_0x04b9
        L_0x058b:
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x0595:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x05ca
            java.lang.Object r1 = r2.next()
            X.1fs r1 = (X.C29101fs) r1
            X.1fv r0 = r1._fields
            if (r0 == 0) goto L_0x05a9
            X.1fv r0 = r0.trimByVisibility()
        L_0x05a9:
            r1._fields = r0
            X.1fv r0 = r1._getters
            if (r0 == 0) goto L_0x05b3
            X.1fv r0 = r0.trimByVisibility()
        L_0x05b3:
            r1._getters = r0
            X.1fv r0 = r1._setters
            if (r0 == 0) goto L_0x05bd
            X.1fv r0 = r0.trimByVisibility()
        L_0x05bd:
            r1._setters = r0
            X.1fv r0 = r1._ctorParameters
            if (r0 == 0) goto L_0x05c7
            X.1fv r0 = r0.trimByVisibility()
        L_0x05c7:
            r1._ctorParameters = r0
            goto L_0x0595
        L_0x05ca:
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Collection r0 = r0.values()
            java.util.Iterator r6 = r0.iterator()
        L_0x05d4:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0693
            java.lang.Object r4 = r6.next()
            X.1fs r4 = (X.C29101fs) r4
            boolean r0 = r15._forSerialization
            r3 = 0
            if (r0 == 0) goto L_0x061a
            X.1fv r5 = r4._getters
            if (r5 == 0) goto L_0x060d
            X.1fv r2 = r4._fields
            X.1fv r1 = r4._ctorParameters
            X.1fv r0 = r4._setters
            X.1fv[] r0 = new X.C29131fv[]{r5, r2, r1, r0}
            X.0jX r5 = X.C29101fs._mergeAnnotations(r4, r3, r0)
            X.1fv r3 = r4._getters
            java.lang.Object r0 = r3.value
            X.1fw r0 = (X.C29141fw) r0
            X.1fw r2 = new X.1fw
            java.lang.reflect.Method r1 = r0._method
            X.0jX[] r0 = r0._paramAnnotations
            r2.<init>(r1, r5, r0)
            X.1fv r0 = r3.withValue(r2)
            r4._getters = r0
            goto L_0x05d4
        L_0x060d:
            X.1fv r2 = r4._fields
            if (r2 == 0) goto L_0x05d4
            X.1fv r1 = r4._ctorParameters
            X.1fv r0 = r4._setters
            X.1fv[] r0 = new X.C29131fv[]{r2, r1, r0}
            goto L_0x067a
        L_0x061a:
            X.1fv r5 = r4._ctorParameters
            if (r5 == 0) goto L_0x0649
            X.1fv r2 = r4._setters
            X.1fv r1 = r4._fields
            X.1fv r0 = r4._getters
            X.1fv[] r0 = new X.C29131fv[]{r5, r2, r1, r0}
            X.0jX r5 = X.C29101fs._mergeAnnotations(r4, r3, r0)
            X.1fv r3 = r4._ctorParameters
            java.lang.Object r0 = r3.value
            X.137 r0 = (X.AnonymousClass137) r0
            X.0jX r1 = r0._annotations
            if (r5 == r1) goto L_0x0642
            X.12y r2 = r0._owner
            int r1 = r0._index
            X.0jX[] r0 = r2._paramAnnotations
            r0[r1] = r5
            X.137 r0 = r2.getParameter(r1)
        L_0x0642:
            X.1fv r0 = r3.withValue(r0)
            r4._ctorParameters = r0
            goto L_0x05d4
        L_0x0649:
            X.1fv r2 = r4._setters
            if (r2 == 0) goto L_0x0670
            X.1fv r1 = r4._fields
            X.1fv r0 = r4._getters
            X.1fv[] r0 = new X.C29131fv[]{r2, r1, r0}
            X.0jX r5 = X.C29101fs._mergeAnnotations(r4, r3, r0)
            X.1fv r3 = r4._setters
            java.lang.Object r0 = r3.value
            X.1fw r0 = (X.C29141fw) r0
            X.1fw r2 = new X.1fw
            java.lang.reflect.Method r1 = r0._method
            X.0jX[] r0 = r0._paramAnnotations
            r2.<init>(r1, r5, r0)
            X.1fv r0 = r3.withValue(r2)
            r4._setters = r0
            goto L_0x05d4
        L_0x0670:
            X.1fv r1 = r4._fields
            if (r1 == 0) goto L_0x05d4
            X.1fv r0 = r4._getters
            X.1fv[] r0 = new X.C29131fv[]{r1, r0}
        L_0x067a:
            X.0jX r2 = X.C29101fs._mergeAnnotations(r4, r3, r0)
            X.1fv r3 = r4._fields
            java.lang.Object r0 = r3.value
            X.1fr r0 = (X.C29091fr) r0
            X.1fr r1 = new X.1fr
            java.lang.reflect.Field r0 = r0._field
            r1.<init>(r0, r2)
            X.1fv r0 = r3.withValue(r1)
            r4._fields = r0
            goto L_0x05d4
        L_0x0693:
            X.0kA r1 = r15._config
            X.1bz r0 = X.C26771bz.USE_WRAPPER_NAME_AS_PROPERTY_NAME
            boolean r0 = r1.isEnabled(r0)
            if (r0 == 0) goto L_0x06c5
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x06a7:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x06c5
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getValue()
            X.1fs r1 = (X.C29101fs) r1
            boolean r0 = r1._forSerialization
            if (r0 == 0) goto L_0x06c1
            r1.getAccessor()
            goto L_0x06a7
        L_0x06c1:
            r1.getMutator()
            goto L_0x06a7
        L_0x06c5:
            X.0jc r2 = r15._annotationIntrospector
            r9 = 0
            if (r2 != 0) goto L_0x06eb
            r0 = r9
        L_0x06cb:
            if (r0 != 0) goto L_0x06e6
            X.0kA r1 = r15._config
            X.1bz r0 = X.C26771bz.SORT_PROPERTIES_ALPHABETICALLY
            boolean r1 = r1.isEnabled(r0)
        L_0x06d5:
            if (r2 == 0) goto L_0x06dd
            X.0jV r0 = r15._classDef
            java.lang.String[] r9 = r2.findSerializationPropertyOrder(r0)
        L_0x06dd:
            if (r1 != 0) goto L_0x06f2
            java.util.LinkedList r0 = r15._creatorProperties
            if (r0 != 0) goto L_0x06f2
            if (r9 != 0) goto L_0x06f2
            return r15
        L_0x06e6:
            boolean r1 = r0.booleanValue()
            goto L_0x06d5
        L_0x06eb:
            X.0jV r0 = r15._classDef
            java.lang.Boolean r0 = r2.findSerializationSortAlphabetically(r0)
            goto L_0x06cb
        L_0x06f2:
            java.util.LinkedHashMap r0 = r15._properties
            int r4 = r0.size()
            if (r1 == 0) goto L_0x071d
            java.util.TreeMap r5 = new java.util.TreeMap
            r5.<init>()
        L_0x06ff:
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x0709:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0725
            java.lang.Object r1 = r2.next()
            X.1fs r1 = (X.C29101fs) r1
            java.lang.String r0 = r1.getName()
            r5.put(r0, r1)
            goto L_0x0709
        L_0x071d:
            java.util.LinkedHashMap r5 = new java.util.LinkedHashMap
            int r0 = r4 + r4
            r5.<init>(r0)
            goto L_0x06ff
        L_0x0725:
            java.util.LinkedHashMap r3 = new java.util.LinkedHashMap
            int r4 = r4 + r4
            r3.<init>(r4)
            if (r9 == 0) goto L_0x0766
            int r8 = r9.length
            r7 = 0
        L_0x072f:
            if (r7 >= r8) goto L_0x0766
            r6 = r9[r7]
            java.lang.Object r4 = r5.get(r6)
            X.1fs r4 = (X.C29101fs) r4
            if (r4 != 0) goto L_0x075e
            java.util.LinkedHashMap r0 = r15._properties
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x0745:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x075e
            java.lang.Object r1 = r2.next()
            X.1fs r1 = (X.C29101fs) r1
            java.lang.String r0 = r1._internalName
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0745
            java.lang.String r6 = r1.getName()
            r4 = r1
        L_0x075e:
            if (r4 == 0) goto L_0x0763
            r3.put(r6, r4)
        L_0x0763:
            int r7 = r7 + 1
            goto L_0x072f
        L_0x0766:
            java.util.LinkedList r0 = r15._creatorProperties
            if (r0 == 0) goto L_0x0782
            java.util.Iterator r2 = r0.iterator()
        L_0x076e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0782
            java.lang.Object r1 = r2.next()
            X.1fs r1 = (X.C29101fs) r1
            java.lang.String r0 = r1.getName()
            r3.put(r0, r1)
            goto L_0x076e
        L_0x0782:
            r3.putAll(r5)
            java.util.LinkedHashMap r0 = r15._properties
            r0.clear()
            java.util.LinkedHashMap r0 = r15._properties
            r0.putAll(r3)
            return r15
        L_0x0790:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = "AnnotationIntrospector returned Class "
            java.lang.String r1 = r4.getName()
            java.lang.String r0 = "; expected Class<PropertyNamingStrategy>"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        L_0x07a2:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = "AnnotationIntrospector returned PropertyNamingStrategy definition of type "
            java.lang.Class r0 = r4.getClass()
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = "; expected type PropertyNamingStrategy or Class<PropertyNamingStrategy> instead"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C183412k.collect():X.12k");
    }

    public C29141fw getJsonValueMethod() {
        LinkedList linkedList = this._jsonValueGetters;
        if (linkedList == null) {
            return null;
        }
        if (linkedList.size() > 1) {
            reportProblem(this, "Multiple value properties defined (" + this._jsonValueGetters.get(0) + " vs " + this._jsonValueGetters.get(1) + ")");
        }
        return (C29141fw) this._jsonValueGetters.get(0);
    }

    public C183412k(C10470kA r3, boolean z, C10030jR r5, C10070jV r6, String str) {
        C10140jc r1 = null;
        this._creatorProperties = null;
        this._anyGetters = null;
        this._anySetters = null;
        this._jsonValueGetters = null;
        this._config = r3;
        this._forSerialization = z;
        this._type = r5;
        this._classDef = r6;
        this._mutatorPrefix = str == null ? "set" : str;
        r1 = r3.isEnabled(C26771bz.USE_ANNOTATIONS) ? r3.getAnnotationIntrospector() : r1;
        this._annotationIntrospector = r1;
        if (r1 == null) {
            this._visibilityChecker = this._config.getDefaultVisibilityChecker();
        } else {
            this._visibilityChecker = r1.findAutoDetectVisibility(r6, this._config.getDefaultVisibilityChecker());
        }
    }
}
