package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.1so  reason: invalid class name and case insensitive filesystem */
public final class C36281so {
    private final C36301sq A00;

    public byte[] A00(C36221si r4) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        r4.CNX(this.A00.Azf(new C36311sr(byteArrayOutputStream)));
        return byteArrayOutputStream.toByteArray();
    }

    public C36281so() {
        this(new Aw0());
    }

    public C36281so(C36301sq r1) {
        this.A00 = r1;
    }
}
