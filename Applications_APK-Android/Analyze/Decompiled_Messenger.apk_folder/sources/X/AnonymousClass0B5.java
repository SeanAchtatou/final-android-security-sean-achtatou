package X;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: X.0B5  reason: invalid class name */
public final class AnonymousClass0B5 extends AnonymousClass0B4 {
    public SharedPreferences A01(Context context, String str, boolean z) {
        int i = 0;
        if (z) {
            i = 4;
        }
        return context.getSharedPreferences(str, i);
    }
}
