package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import java.nio.charset.Charset;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ti  reason: invalid class name and case insensitive filesystem */
public final class C36781ti extends C36471tB {
    public static final Class A03 = C36781ti.class;
    private static volatile C36781ti A04;
    private final AnonymousClass06A A00;
    private final AnonymousClass18N A01;
    private final C04310Tq A02;

    private C36781ti(AnonymousClass18N r2, AnonymousClass06A r3, C04310Tq r4, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        super(202, deprecatedAnalyticsLogger);
        this.A01 = r2;
        this.A00 = r3;
        this.A02 = r4;
    }

    public static final C36781ti A00(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (C36781ti.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A04 = new C36781ti(AnonymousClass18N.A00(applicationInjector), AnonymousClass067.A0A(applicationInjector), C10580kT.A04(applicationInjector), C06920cI.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void A0D(C22338Aw9 aw9) {
        Class cls;
        String str;
        if (!A0C()) {
            cls = A03;
            str = "Stored procedure sender not available";
        } else if (this.A01.A03()) {
            cls = A03;
            str = "Invalid device id";
        } else {
            C22363Awi awi = new C22363Awi(aw9);
            String A032 = C36471tB.A03(C22037AnD.A01(aw9));
            C22036AnC anC = C22036AnC.A08;
            C22335Aw3 aw3 = new C22335Aw3();
            aw3.setField_ = 22;
            aw3.value_ = awi;
            A0B(C22030An2.A01(C22039AnF.A01(null, new C22338Aw9(Long.valueOf(Long.parseLong((String) this.A02.get())), this.A01.A02()), this.A00.now() * 1000, anC, aw3, A032.getBytes(Charset.defaultCharset()), null)));
            return;
        }
        C010708t.A05(cls, str);
    }
}
