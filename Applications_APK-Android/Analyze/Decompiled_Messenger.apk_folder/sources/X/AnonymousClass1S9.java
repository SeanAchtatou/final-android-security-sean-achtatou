package X;

import java.io.Closeable;

/* renamed from: X.1S9  reason: invalid class name */
public abstract class AnonymousClass1S9 implements Closeable, AnonymousClass1SH {
    public C35741rk A00;

    public int A01() {
        int sizeInBytes;
        if (this instanceof C33281nK) {
            return ((C33281nK) this).A00.A01();
        }
        if (this instanceof AnonymousClass1S5) {
            return AnonymousClass1SJ.A01(((AnonymousClass1S5) this).A04);
        }
        C33291nL r1 = (C33291nL) this;
        synchronized (r1) {
            sizeInBytes = r1.A02() ? 0 : r1.A00.A03.getSizeInBytes();
        }
        return sizeInBytes;
    }

    public boolean A02() {
        boolean z;
        boolean z2;
        if (this instanceof C33281nK) {
            return ((C33281nK) this).A00.A02();
        }
        if (!(this instanceof AnonymousClass1S5)) {
            C33291nL r2 = (C33291nL) this;
            synchronized (r2) {
                z2 = false;
                if (r2.A00 == null) {
                    z2 = true;
                }
            }
            return z2;
        }
        AnonymousClass1S5 r22 = (AnonymousClass1S5) this;
        synchronized (r22) {
            z = false;
            if (r22.A00 == null) {
                z = true;
            }
        }
        return z;
    }

    public boolean A03() {
        if (this instanceof C33281nK) {
            return ((C33281nK) this).A00.A03();
        }
        if (!(this instanceof C33291nL)) {
            return false;
        }
        return ((C33291nL) this).A01;
    }

    public void close() {
        AnonymousClass1PS r1;
        if (this instanceof C33281nK) {
            ((C33281nK) this).A00.close();
        } else if (!(this instanceof AnonymousClass1S5)) {
            C33291nL r3 = (C33291nL) this;
            synchronized (r3) {
                C80973tY r2 = r3.A00;
                if (r2 != null) {
                    r3.A00 = null;
                    synchronized (r2) {
                        AnonymousClass1PS.A05(r2.A00);
                        r2.A00 = null;
                        AnonymousClass1PS.A06(r2.A02);
                        r2.A02 = null;
                    }
                }
            }
        } else {
            AnonymousClass1S5 r22 = (AnonymousClass1S5) this;
            synchronized (r22) {
                r1 = r22.A00;
                r22.A00 = null;
                r22.A04 = null;
            }
            if (r1 != null) {
                r1.close();
            }
        }
    }

    public C33271nJ Azo() {
        if (this instanceof C33281nK) {
            return ((C33281nK) this).A00.Azo();
        }
        if (!(this instanceof AnonymousClass1S5)) {
            return C33271nJ.A03;
        }
        return ((AnonymousClass1S5) this).A03;
    }

    public void finalize() {
        int A03 = C000700l.A03(1138332922);
        if (A02()) {
            C000700l.A09(2084914746, A03);
            return;
        }
        AnonymousClass02w.A0E("CloseableImage", "finalize: %s %x still open.", getClass().getSimpleName(), Integer.valueOf(System.identityHashCode(this)));
        try {
            close();
        } finally {
            super.finalize();
            C000700l.A09(-901561839, A03);
        }
    }
}
