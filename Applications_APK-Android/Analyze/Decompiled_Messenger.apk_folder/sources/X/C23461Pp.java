package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Pp  reason: invalid class name and case insensitive filesystem */
public final class C23461Pp implements C23411Pk {
    private static volatile C23461Pp A02;
    public final QuickPerformanceLogger A00;
    private final C25051Yd A01;

    private void A01(String str, String str2, short s) {
        int hashCode;
        int i = 0;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        int i2 = hashCode * 31;
        if (str2 != null) {
            i = str2.hashCode();
        }
        this.A00.markerEnd(33357825, i2 + i, s);
    }

    public void Bk1(String str, String str2, String str3) {
    }

    public void Bls(String str) {
    }

    public void Bm2(AnonymousClass1Q0 r1, String str, Throwable th, boolean z) {
    }

    public void Bm7(AnonymousClass1Q0 r1, Object obj, String str, boolean z) {
    }

    public void Bm9(AnonymousClass1Q0 r1, String str, boolean z) {
    }

    public void Bt9(String str, String str2, boolean z) {
    }

    public boolean C3R(String str) {
        return false;
    }

    public static final C23461Pp A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C23461Pp.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C23461Pp(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private boolean A02(String str) {
        if (!this.A01.Aem(282063387755584L)) {
            return false;
        }
        if ("DecodeProducer".equals(str) || AnonymousClass24B.$const$string(AnonymousClass1Y3.A5P).equals(str)) {
            return true;
        }
        return false;
    }

    private C23461Pp(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0ZD.A03(r2);
        this.A01 = AnonymousClass0WT.A00(r2);
    }

    public void Bk3(String str, String str2, Map map) {
        if (A02(str2)) {
            A01(str, str2, 4);
        }
    }

    public void Bk5(String str, String str2, Throwable th, Map map) {
        if (A02(str2)) {
            A01(str, str2, 3);
        }
    }

    public void Bk7(String str, String str2, Map map) {
        if (A02(str2)) {
            A01(str, str2, 2);
        }
    }

    public void Bk9(String str, String str2) {
        int hashCode;
        if (A02(str2)) {
            int i = 0;
            if (str == null) {
                hashCode = 0;
            } else {
                hashCode = str.hashCode();
            }
            int i2 = hashCode * 31;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i3 = i2 + i;
            this.A00.markerStart(33357825, i3);
            this.A00.markerAnnotate(33357825, i3, "producer-name", str2);
        }
    }
}
