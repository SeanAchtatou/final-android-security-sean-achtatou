package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.0sh  reason: invalid class name and case insensitive filesystem */
public final class C14150sh {
    public final C16750xi A00;
    public final FbSharedPreferences A01;

    public static final C14150sh A00(AnonymousClass1XY r1) {
        return new C14150sh(r1);
    }

    public C14150sh(AnonymousClass1XY r3) {
        this.A01 = FbSharedPreferencesModule.A00(r3);
        C15090ui.A00(r3);
        this.A00 = new C16750xi(r3, AnonymousClass1YA.A02(r3));
    }
}
