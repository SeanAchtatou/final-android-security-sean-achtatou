package X;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0e8  reason: invalid class name and case insensitive filesystem */
public final class C07770e8 implements AnonymousClass06U {
    public final /* synthetic */ C09340h3 A00;

    public C07770e8(C09340h3 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0020, code lost:
        if (r6 != r10.A00.A0J.A00) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.C07770e8 r10, android.content.Intent r11, X.AnonymousClass06Y r12) {
        /*
            r0 = 85
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            android.os.Parcelable r7 = r11.getParcelableExtra(r0)
            android.net.NetworkInfo r7 = (android.net.NetworkInfo) r7
            java.lang.String r1 = "inetCondition"
            r0 = -1
            int r6 = r11.getIntExtra(r1, r0)
            X.0h3 r0 = r10.A00
            X.8U2 r0 = r0.A0J
            if (r0 == 0) goto L_0x0022
            X.0h3 r0 = r10.A00
            X.8U2 r0 = r0.A0J
            int r0 = r0.A00
            r9 = 0
            if (r6 == r0) goto L_0x0023
        L_0x0022:
            r9 = 1
        L_0x0023:
            boolean r5 = r12.isInitialStickyBroadcast()
            X.0h3 r4 = r10.A00
            r2 = 0
            if (r6 != 0) goto L_0x009e
            r4.A0H = r2
        L_0x002f:
            X.0h3 r8 = r10.A00
            int r1 = X.AnonymousClass1Y3.BLN
            X.0UN r0 = r8.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            X.8U2 r4 = new X.8U2
            r4.<init>(r7, r6, r0)
            X.8U2 r3 = r8.A0J
            r2 = 0
            if (r3 == 0) goto L_0x005c
            X.8U3 r1 = r4.A01
            X.8U3 r0 = r3.A01
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 == 0) goto L_0x005c
            int r1 = r4.A00
            int r0 = r3.A00
            if (r1 != r0) goto L_0x005c
            boolean r1 = r4.A02
            boolean r0 = r3.A02
            if (r1 != r0) goto L_0x005c
            r2 = 1
        L_0x005c:
            if (r2 != 0) goto L_0x0065
            r8.A0J = r4
            X.0h1 r0 = r8.A0A
            r0.A04(r4)
        L_0x0065:
            if (r9 == 0) goto L_0x0084
            X.0h3 r4 = r10.A00
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r0 = "com.facebook.common.hardware.ACTION_INET_CONDITION_CHANGED"
            r1.<init>(r0)
            java.lang.String r0 = "INET_CONDITION"
            android.content.Intent r3 = r1.putExtra(r0, r6)
            int r2 = X.AnonymousClass1Y3.AKb
            X.0UN r1 = r4.A03
            r0 = 6
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0Ut r0 = (X.C04460Ut) r0
            r0.C4x(r3)
        L_0x0084:
            r2 = 11
            int r1 = X.AnonymousClass1Y3.BKH
            X.0h3 r0 = r10.A00
            X.0UN r0 = r0.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.util.concurrent.ExecutorService r2 = (java.util.concurrent.ExecutorService) r2
            X.4qU r1 = new X.4qU
            r1.<init>(r10, r5, r11)
            r0 = -1021511139(0xffffffffc31cfa1d, float:-156.977)
            X.AnonymousClass07A.A04(r2, r1, r0)
            return
        L_0x009e:
            long r0 = r4.A0H
            int r8 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r8 != 0) goto L_0x002f
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r4.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            long r0 = r0.now()
            r4.A0H = r0
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07770e8.A00(X.0e8, android.content.Intent, X.06Y):void");
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r9) {
        int A002 = AnonymousClass09Y.A00(-631160512);
        int i = AnonymousClass1Y3.AyQ;
        int i2 = AnonymousClass1Y3.APr;
        AnonymousClass0UN r2 = ((C83923yT) AnonymousClass1XX.A02(13, i, this.A00.A03)).A00;
        if (!((AnonymousClass1Y6) AnonymousClass1XX.A02(1, i2, r2)).BHM() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r2)).Aem(284464273952933L)) {
            A00(this, intent, r9);
        } else {
            AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AYk, ((C83923yT) AnonymousClass1XX.A02(13, i, this.A00.A03)).A00), new AnonymousClass4UG(this, intent, r9), 1970755722);
        }
        AnonymousClass09Y.A01(1286214030, A002);
    }
}
