package X;

import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

/* renamed from: X.1MI  reason: invalid class name */
public final class AnonymousClass1MI extends C23441Pn implements AnonymousClass1OQ {
    public Optional A00 = Optional.absent();
    private boolean A01 = false;
    public final AnonymousClass06B A02;
    public final AnonymousClass1QT A03;
    private final C31141jD A04;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A00(X.AnonymousClass1MI r2) {
        /*
            monitor-enter(r2)
            boolean r0 = r2.A01     // Catch:{ all -> 0x0022 }
            r1 = 1
            if (r0 != 0) goto L_0x0020
            X.1QT r0 = r2.A03     // Catch:{ all -> 0x0022 }
            com.facebook.prefs.shared.FbSharedPreferences r0 = r0.A00     // Catch:{ all -> 0x0022 }
            boolean r0 = r0.BFQ()     // Catch:{ all -> 0x0022 }
            if (r0 != 0) goto L_0x0013
            r0 = 0
            monitor-exit(r2)
            return r0
        L_0x0013:
            X.1QT r0 = r2.A03     // Catch:{ all -> 0x0022 }
            com.google.common.base.Optional r0 = r0.A00()     // Catch:{ all -> 0x0022 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x0022 }
            r2.A00 = r0     // Catch:{ all -> 0x0022 }
            r2.A01 = r1     // Catch:{ all -> 0x0022 }
        L_0x0020:
            monitor-exit(r2)
            return r1
        L_0x0022:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MI.A00(X.1MI):boolean");
    }

    public synchronized void BgO(AnonymousClass1Q0 r11, CallerContext callerContext, int i, boolean z, boolean z2) {
        C33651nv r0;
        if (A00(this) && !this.A00.isPresent() && this.A04.BwV(r11, callerContext)) {
            AnonymousClass1QT r3 = this.A03;
            Uri uri = r11.A02;
            long now = this.A02.now();
            String str = callerContext.A02;
            String A0F = callerContext.A0F();
            String A0G = callerContext.A0G();
            synchronized (r3) {
                Preconditions.checkState(r3.A00.BFQ());
                C30281hn edit = r3.A00.edit();
                edit.BzC(r3.A09, uri.toString());
                edit.Bz6(r3.A01, i);
                edit.BzA(r3.A05, now);
                edit.putBoolean(r3.A08, z);
                edit.putBoolean(r3.A07, z2);
                edit.BzC(r3.A03, str);
                edit.BzC(r3.A02, A0F);
                edit.BzC(r3.A04, A0G);
                edit.commit();
                r0 = (C33651nv) r3.A00().get();
            }
            this.A00 = Optional.of(r0);
        }
    }

    public AnonymousClass1MI(C31141jD r2, AnonymousClass1QT r3, AnonymousClass06B r4) {
        this.A04 = r2;
        this.A03 = r3;
        this.A02 = r4;
    }
}
