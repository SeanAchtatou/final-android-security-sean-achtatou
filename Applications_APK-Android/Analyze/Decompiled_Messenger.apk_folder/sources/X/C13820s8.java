package X;

import android.content.Context;
import android.view.ContextThemeWrapper;

/* renamed from: X.0s8  reason: invalid class name and case insensitive filesystem */
public final class C13820s8 implements C13830s9 {
    private static volatile C13820s8 A00;

    public static C13820s8 A00() {
        if (A00 == null) {
            synchronized (C13820s8.class) {
                if (A00 == null) {
                    A00 = new C13820s8();
                }
            }
        }
        return A00;
    }

    public Context CNV(Context context) {
        return new ContextThemeWrapper(context, 2132476608);
    }
}
