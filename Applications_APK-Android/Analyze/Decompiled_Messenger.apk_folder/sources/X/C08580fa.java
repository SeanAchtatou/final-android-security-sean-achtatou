package X;

import android.os.Build;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.quicklog.PerformanceLoggingEvent;

/* renamed from: X.0fa  reason: invalid class name and case insensitive filesystem */
public final class C08580fa implements C08560fY {
    public String AWk() {
        return "perf_stats";
    }

    public static final C08580fa A00() {
        return new C08580fa();
    }

    public long BL4() {
        return C08350fD.A01 | C08350fD.A0A | C08350fD.A07;
    }

    public void BjZ(PerformanceLoggingEvent performanceLoggingEvent) {
        AnonymousClass00T r5 = performanceLoggingEvent.A0F;
        if (r5 != null && r5.A0E) {
            performanceLoggingEvent.A03("cpu_stats");
            performanceLoggingEvent.A04("start_pri", r5.A00);
            performanceLoggingEvent.A04("stop_pri", r5.A01);
            performanceLoggingEvent.A06("ps_cpu_ms", r5.A06);
            long j = r5.A09;
            if (j != -1) {
                performanceLoggingEvent.A06("th_cpu_ms", j);
            }
            performanceLoggingEvent.A0B(TurboLoader.Locator.$const$string(38), r5.A0D);
        }
        AnonymousClass00T r3 = performanceLoggingEvent.A0F;
        if (r3 != null && r3.A0E) {
            performanceLoggingEvent.A03("io_stats");
            performanceLoggingEvent.A06("ps_flt", r3.A07);
            long j2 = r3.A0A;
            if (j2 != -1) {
                performanceLoggingEvent.A06("th_flt", j2);
            }
            performanceLoggingEvent.A04("class_load_attempts", r3.A0C.A00);
            performanceLoggingEvent.A04("dex_queries", r3.A0C.A02);
            performanceLoggingEvent.A04("class_loads_failed", r3.A0C.A01);
            performanceLoggingEvent.A04("locator_assists", r3.A0C.A04);
            performanceLoggingEvent.A04("wrong_dfa_guesses", r3.A0C.A03);
            performanceLoggingEvent.A04("class_hashmap_generate_successes", r3.A0C.A08);
            performanceLoggingEvent.A04("class_hashmap_generate_failures", r3.A0C.A07);
            performanceLoggingEvent.A04("class_hashmap_load_successes", r3.A0C.A06);
            performanceLoggingEvent.A04("class_hashmap_load_failures", r3.A0C.A05);
            long j3 = r3.A03;
            if (j3 != -1) {
                performanceLoggingEvent.A06("allocstall", j3);
            }
            long j4 = r3.A04;
            if (j4 != -1) {
                performanceLoggingEvent.A06("pages_in", j4);
            }
            long j5 = r3.A05;
            if (j5 != -1) {
                performanceLoggingEvent.A06("pages_out", j5);
            }
            performanceLoggingEvent.A06("ps_min_flt", r3.A08);
            performanceLoggingEvent.A06("avail_disk_spc_kb", r3.A01());
        }
        AnonymousClass00T r0 = performanceLoggingEvent.A0F;
        if (r0 != null && r0.A0E && 0 != 0) {
            performanceLoggingEvent.A03("memory_stats");
            performanceLoggingEvent.A06("avail_mem", null.availMem);
            performanceLoggingEvent.A06("low_mem", null.threshold);
            if (Build.VERSION.SDK_INT >= 16) {
                performanceLoggingEvent.A06("total_mem", null.totalMem);
            }
        }
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A05;
    }
}
