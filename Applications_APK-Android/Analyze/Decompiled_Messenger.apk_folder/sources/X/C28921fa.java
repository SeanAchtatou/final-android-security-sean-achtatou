package X;

import android.net.Uri;
import com.facebook.graphql.enums.GraphQLMessengerThreadViewMode;
import com.facebook.messaging.customthreads.model.ThreadThemeInfo;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1fa  reason: invalid class name and case insensitive filesystem */
public interface C28921fa {
    public static final ThreadThemeInfo A00 = ThreadThemeInfo.A00().A00();

    String AbY();

    int AmT();

    ImmutableList Ao6();

    Uri Art();

    Uri Aru();

    ImmutableList B01();

    Uri B3J();

    long B5o();

    GraphQLMessengerThreadViewMode B61();

    boolean BGP();
}
