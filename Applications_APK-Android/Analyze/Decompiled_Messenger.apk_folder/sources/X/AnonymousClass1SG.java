package X;

import android.util.Base64;
import com.facebook.acra.LogCatCollector;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1SG  reason: invalid class name */
public final class AnonymousClass1SG {
    public static String A00(C23601Qd r1) {
        try {
            if (!(r1 instanceof C24081Sd)) {
                return A01(r1);
            }
            List list = null;
            return A01((C23601Qd) list.get(0));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static List A02(C23601Qd r3) {
        try {
            if (r3 instanceof C24081Sd) {
                List list = null;
                ArrayList arrayList = new ArrayList(list.size());
                for (int i = 0; i < list.size(); i++) {
                    arrayList.add(A01((C23601Qd) list.get(i)));
                }
                return arrayList;
            }
            ArrayList arrayList2 = new ArrayList(1);
            arrayList2.add(A01(r3));
            return arrayList2;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static String A01(C23601Qd r3) {
        byte[] bytes = r3.B7w().getBytes(LogCatCollector.UTF_8_ENCODING);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bytes, 0, bytes.length);
            return Base64.encodeToString(instance.digest(), 11);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
