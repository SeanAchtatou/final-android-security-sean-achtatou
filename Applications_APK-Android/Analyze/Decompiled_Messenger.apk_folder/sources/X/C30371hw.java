package X;

/* renamed from: X.1hw  reason: invalid class name and case insensitive filesystem */
public final class C30371hw {
    public final C30401hz A00;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (r4.equals(X.C22298Ase.$const$string(24)) == false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C30371hw(java.lang.String r4, android.content.Context r5) {
        /*
            r3 = this;
            r3.<init>()
            int r1 = r4.hashCode()
            r0 = 906422384(0x3606e870, float:2.0102852E-6)
            if (r1 != r0) goto L_0x0019
            r0 = 24
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            boolean r0 = r4.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r1 = -1
        L_0x001a:
            if (r1 != 0) goto L_0x0024
            X.1hz r0 = new X.1hz
            r0.<init>(r5)
            r3.A00 = r0
            return
        L_0x0024:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r1 = "Invalid registry name \""
            java.lang.String r0 = "\"!"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r4, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30371hw.<init>(java.lang.String, android.content.Context):void");
    }
}
