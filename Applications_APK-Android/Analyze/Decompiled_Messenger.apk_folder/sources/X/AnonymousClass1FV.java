package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FV  reason: invalid class name */
public final class AnonymousClass1FV implements AnonymousClass1FI {
    private static volatile AnonymousClass1FV A03;
    private AnonymousClass0UN A00;
    private final C04310Tq A01;
    private final C04310Tq A02;

    public static final AnonymousClass1FV A00(AnonymousClass1XY r7) {
        if (A03 == null) {
            synchronized (AnonymousClass1FV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        AnonymousClass0VG A003 = AnonymousClass0VG.A00(AnonymousClass1Y3.BKW, applicationInjector);
                        AnonymousClass0VG A004 = AnonymousClass0VG.A00(AnonymousClass1Y3.AUq, applicationInjector);
                        new AnonymousClass1FX(applicationInjector);
                        A03 = new AnonymousClass1FV(applicationInjector, A003, A004);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public ImmutableMap get() {
        HashMap hashMap = new HashMap();
        if (((Boolean) this.A01.get()).booleanValue()) {
            hashMap.put(new SubscribeTopic("/t_ms", 0), AnonymousClass1FP.ALWAYS);
        } else {
            hashMap.put(new SubscribeTopic(TurboLoader.Locator.$const$string(91), 0), AnonymousClass1FP.ALWAYS);
            hashMap.put(new SubscribeTopic(TurboLoader.Locator.$const$string(92), 0), AnonymousClass1FP.ALWAYS);
            hashMap.put(new SubscribeTopic("/delete_messages_notification", 0), AnonymousClass1FP.ALWAYS);
        }
        if (((Boolean) this.A02.get()).booleanValue()) {
            hashMap.put(new SubscribeTopic("/t_ps", 0), AnonymousClass1FP.ALWAYS);
        }
        hashMap.put(new SubscribeTopic("/t_tn", 0), AnonymousClass1FP.APP_USE);
        if (((AnonymousClass1FZ) AnonymousClass1XX.A03(AnonymousClass1Y3.BTP, this.A00)).A02()) {
            hashMap.put(new SubscribeTopic(TurboLoader.Locator.$const$string(99), 0), AnonymousClass1FP.APP_USE);
        }
        if (((C25051Yd) AnonymousClass1XX.A03(AnonymousClass1Y3.AOJ, this.A00)).Aem(284992554931533L)) {
            hashMap.put(new SubscribeTopic("/t_tp", 0), AnonymousClass1FP.APP_USE);
        }
        hashMap.put(new SubscribeTopic(TurboLoader.Locator.$const$string(43), 0), AnonymousClass1FP.ALWAYS);
        hashMap.put(new SubscribeTopic(TurboLoader.Locator.$const$string(100), 0), AnonymousClass1FP.ALWAYS);
        hashMap.put(new SubscribeTopic(TurboLoader.Locator.$const$string(42), 0), AnonymousClass1FP.ALWAYS);
        hashMap.put(new SubscribeTopic(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0i), 0), AnonymousClass1FP.ALWAYS);
        hashMap.put(new SubscribeTopic("/t_dr_response", 0), AnonymousClass1FP.ALWAYS);
        return ImmutableMap.copyOf(hashMap);
    }

    private AnonymousClass1FV(AnonymousClass1XY r3, C04310Tq r4, C04310Tq r5) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = r4;
        this.A02 = r5;
    }
}
