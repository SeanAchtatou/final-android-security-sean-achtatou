package X;

import android.content.Context;
import com.facebook.http.interfaces.RequestPriority;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.util.Arrays;
import java.util.Locale;

/* renamed from: X.0if  reason: invalid class name and case insensitive filesystem */
public final class C09770if {
    public C35871rx A00;
    public String A01;
    public final Context A02;
    public final C09400hF A03;
    public final C09640iE A04;
    public final Integer A05;
    public final String A06;
    public final Locale A07;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C09770if r5 = (C09770if) obj;
            if (!Objects.equal(this.A02, r5.A02) || this.A05 != r5.A05 || !Objects.equal(this.A03, r5.A03) || !Objects.equal(this.A07, r5.A07) || this.A04 != r5.A04 || !Objects.equal(this.A06, r5.A06) || !Objects.equal(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public RequestPriority A00() {
        if (this.A05.intValue() != 1) {
            return RequestPriority.A04;
        }
        return RequestPriority.A03;
    }

    public int hashCode() {
        String str;
        Context context = this.A02;
        if (1 - this.A05.intValue() != 0) {
            str = "NORMAL";
        } else {
            str = "UPDATE";
        }
        return Arrays.hashCode(new Object[]{context, str, this.A03, this.A07, this.A04, this.A06, this.A01});
    }

    public C09770if(Integer num, Context context, Locale locale, C09400hF r4, C09640iE r5, String str, String str2) {
        this.A02 = context;
        this.A05 = num;
        this.A07 = locale;
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = str;
        this.A01 = str2;
    }

    public String toString() {
        String str;
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        if (1 - this.A05.intValue() != 0) {
            str = "NORMAL";
        } else {
            str = "UPDATE";
        }
        stringHelper.add("mType", str);
        stringHelper.add("mAppVersionInfo", this.A03);
        stringHelper.add("mLocale", this.A07);
        stringHelper.add("mLanguageFileFormat", this.A04);
        stringHelper.add("mStringResourcesHash", this.A06);
        stringHelper.add("mLangpackContentChecksum", this.A01);
        return stringHelper.toString();
    }
}
