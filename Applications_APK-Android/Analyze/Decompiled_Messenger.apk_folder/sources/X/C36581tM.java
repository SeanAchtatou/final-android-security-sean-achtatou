package X;

import android.os.Bundle;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.fbtrace.FbTraceNode;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.notify.type.NewMessageNotification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@UserScoped
/* renamed from: X.1tM  reason: invalid class name and case insensitive filesystem */
public final class C36581tM implements C05460Za {
    private static C05540Zi A07;
    public final C189216c A00;
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();
    public final Map A03 = new HashMap();
    public final Map A04 = new HashMap();
    public final Map A05 = new HashMap();
    public final Set A06 = new HashSet();

    public static final C36581tM A01(AnonymousClass1XY r4) {
        C36581tM r0;
        synchronized (C36581tM.class) {
            C05540Zi A002 = C05540Zi.A00(A07);
            A07 = A002;
            try {
                if (A002.A03(r4)) {
                    A07.A00 = new C36581tM(C189216c.A02((AnonymousClass1XY) A07.A01()));
                }
                C05540Zi r1 = A07;
                r0 = (C36581tM) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A07.A02();
                throw th;
            }
        }
        return r0;
    }

    public static List A02(C36581tM r2, ThreadKey threadKey) {
        if (threadKey == null) {
            C010708t.A0K("DeltaUiChangesCache", "Thread key is null");
            return new ArrayList();
        }
        List list = (List) r2.A03.get(threadKey);
        if (list != null) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        r2.A03.put(threadKey, arrayList);
        return arrayList;
    }

    public void A03() {
        this.A01.clear();
        this.A03.clear();
        this.A06.clear();
        this.A04.clear();
        this.A02.clear();
        this.A05.clear();
    }

    public void A04(Message message, long j) {
        Map map = this.A05;
        ThreadKey threadKey = message.A0U;
        Bundle A002 = C189216c.A00(C61242yZ.A05, threadKey, -1, FbTraceNode.A03, j);
        A002.putString("offline_threading_id", message.A0w);
        map.put(threadKey, A002);
    }

    private C36581tM(C189216c r2) {
        this.A00 = r2;
    }

    public static final C36581tM A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public void A06(ThreadKey threadKey, NewMessageNotification newMessageNotification) {
        A02(this, threadKey).add(newMessageNotification);
        this.A04.remove(threadKey);
        this.A02.remove(threadKey);
    }

    public void A05(ThreadKey threadKey) {
        A02(this, threadKey);
    }

    public void clearUserData() {
        A03();
    }
}
