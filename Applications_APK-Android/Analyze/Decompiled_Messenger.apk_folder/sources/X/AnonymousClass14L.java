package X;

/* renamed from: X.14L  reason: invalid class name */
public final class AnonymousClass14L {
    public static String A01(Object... objArr) {
        StringBuilder sb = new StringBuilder();
        sb.append(objArr[0]);
        for (int i = 1; i < objArr.length; i++) {
            sb.append(",");
            sb.append(objArr[i]);
        }
        return sb.toString();
    }

    public static String A00(String str, String str2) {
        StringBuilder sb = new StringBuilder(str.length() + str2.length() + 1);
        sb.append(str);
        sb.append(",");
        sb.append(str2);
        return sb.toString();
    }
}
