package X;

import java.util.List;

/* renamed from: X.0S1  reason: invalid class name */
public final class AnonymousClass0S1 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.protocol.MqttClient$5";
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass0C8 A01;
    public final /* synthetic */ List A02;

    public AnonymousClass0S1(AnonymousClass0C8 r1, List list, int i) {
        this.A01 = r1;
        this.A02 = list;
        this.A00 = i;
    }

    public void run() {
        AnonymousClass0C8 r3 = this.A01;
        List list = this.A02;
        int i = this.A00;
        try {
            r3.A05((long) (r3.A0E.A03 * AnonymousClass1Y3.A87));
            if (r3.A09()) {
                r3.A0C.C5U(C02430Et.A00(list), i);
                r3.A0D.BsL(list, i);
                AnonymousClass0CC r1 = r3.A0Y;
                if (r1 != null) {
                    r1.A01(AnonymousClass0CL.A0C.name(), i);
                }
            }
        } catch (Throwable th) {
            AnonymousClass0C8.A03(r3, AnonymousClass0CE.A01(th), AnonymousClass0CG.A08, th);
        }
    }
}
