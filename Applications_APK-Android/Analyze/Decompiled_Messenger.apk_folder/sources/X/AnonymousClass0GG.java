package X;

import android.os.SystemClock;

/* renamed from: X.0GG  reason: invalid class name */
public final class AnonymousClass0GG extends C007907e {
    private final AnonymousClass0GH A00 = new AnonymousClass0GH();
    private final AnonymousClass0GH A01 = new AnonymousClass0GH();

    public synchronized void A05(Integer num, String str) {
        String str2;
        Integer num2 = num;
        if (this.A01.appWakeups.containsKey(str)) {
            if (num != null) {
                str2 = AnonymousClass0KW.A00(num);
            } else {
                str2 = "null";
            }
            AnonymousClass0KZ.A00("AppWakeupMetricsCollector", AnonymousClass08S.A0T("Wakeup started again without ending for ", str, " (", str2, ")"), null);
        } else {
            this.A01.appWakeups.put(str, new AnonymousClass0GI(num2, 1, SystemClock.elapsedRealtime()));
        }
    }

    public synchronized void A06(String str) {
        if (this.A01.appWakeups.containsKey(str)) {
            AnonymousClass0GI r2 = (AnonymousClass0GI) this.A01.appWakeups.get(str);
            r2.A01 = SystemClock.elapsedRealtime() - r2.A01;
            if (!this.A00.appWakeups.containsKey(str)) {
                AnonymousClass04b r1 = this.A00.appWakeups;
                AnonymousClass0GI r0 = new AnonymousClass0GI();
                r0.A00(r2);
                r1.put(str, r0);
            } else {
                ((AnonymousClass0GI) this.A00.appWakeups.get(str)).A01(r2, (AnonymousClass0GI) this.A00.appWakeups.get(str));
            }
            this.A01.appWakeups.remove(str);
        }
    }

    public AnonymousClass0FM A03() {
        return new AnonymousClass0GH();
    }

    public boolean A04(AnonymousClass0FM r6) {
        AnonymousClass0GH r62 = (AnonymousClass0GH) r6;
        synchronized (this) {
            C02740Gd.A00(r62, "Null value passed to getSnapshot!");
            r62.appWakeups.clear();
            int i = 0;
            while (true) {
                AnonymousClass04b r3 = this.A00.appWakeups;
                if (i < r3.size()) {
                    AnonymousClass0GI r2 = new AnonymousClass0GI();
                    r2.A00((AnonymousClass0GI) r3.A09(i));
                    r62.appWakeups.put(r3.A07(i), r2);
                    i++;
                }
            }
        }
        return true;
    }
}
