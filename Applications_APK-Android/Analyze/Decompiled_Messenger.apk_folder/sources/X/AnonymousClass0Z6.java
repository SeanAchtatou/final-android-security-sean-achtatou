package X;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.0Z6  reason: invalid class name */
public final class AnonymousClass0Z6 {
    public static final String[] A03 = {"version"};
    public final int A00;
    public final Context A01;
    public final ImmutableList A02;

    public static int A00(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor rawQuery = sQLiteDatabase.rawQuery(AnonymousClass08S.A0J("PRAGMA ", str), null);
        try {
            rawQuery.moveToNext();
            int i = rawQuery.getInt(0);
            rawQuery.close();
            return i;
        } catch (Throwable th) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            throw th;
        }
    }

    public static void A01(SQLiteDatabase sQLiteDatabase, String str, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", str);
        contentValues.put("version", Integer.valueOf(i));
        C007406x.A00(-196916162);
        sQLiteDatabase.replaceOrThrow("_shared_version", null, contentValues);
        C007406x.A00(-374338645);
    }

    public static void A02(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        Cursor rawQuery = sQLiteDatabase.rawQuery(AnonymousClass08S.A0S("PRAGMA ", str, "=", str2), null);
        try {
            rawQuery.moveToNext();
            rawQuery.close();
        } catch (Throwable th) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            throw th;
        }
    }

    public AnonymousClass0Z6(List list, int i, Context context) {
        this.A02 = ImmutableList.copyOf((Collection) list);
        this.A00 = i;
        this.A01 = context.getApplicationContext();
    }
}
