package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0CG  reason: invalid class name */
public final class AnonymousClass0CG extends Enum {
    private static final /* synthetic */ AnonymousClass0CG[] A00;
    public static final AnonymousClass0CG A01;
    public static final AnonymousClass0CG A02;
    public static final AnonymousClass0CG A03;
    public static final AnonymousClass0CG A04;
    public static final AnonymousClass0CG A05;
    public static final AnonymousClass0CG A06;
    public static final AnonymousClass0CG A07;
    public static final AnonymousClass0CG A08;

    static {
        AnonymousClass0CG r2 = new AnonymousClass0CG("DISCONNECT", 0);
        A01 = r2;
        AnonymousClass0CG r3 = new AnonymousClass0CG("NETWORK_THREAD_LOOP", 1);
        A02 = r3;
        AnonymousClass0CG r4 = new AnonymousClass0CG("PUBLISH", 2);
        A06 = r4;
        AnonymousClass0CG r5 = new AnonymousClass0CG("PUBACK", 3);
        A05 = r5;
        AnonymousClass0CG r6 = new AnonymousClass0CG("PING", 4);
        A03 = r6;
        AnonymousClass0CG r7 = new AnonymousClass0CG("PINGRESP", 5);
        A04 = r7;
        AnonymousClass0CG r8 = new AnonymousClass0CG("SUBSCRIBE", 6);
        A07 = r8;
        AnonymousClass0CG r9 = new AnonymousClass0CG("UNSUBSCRIBE", 7);
        A08 = r9;
        A00 = new AnonymousClass0CG[]{r2, r3, r4, r5, r6, r7, r8, r9, new AnonymousClass0CG("TIMEOUT", 8)};
    }

    public static AnonymousClass0CG valueOf(String str) {
        return (AnonymousClass0CG) Enum.valueOf(AnonymousClass0CG.class, str);
    }

    public static AnonymousClass0CG[] values() {
        return (AnonymousClass0CG[]) A00.clone();
    }

    private AnonymousClass0CG(String str, int i) {
    }
}
