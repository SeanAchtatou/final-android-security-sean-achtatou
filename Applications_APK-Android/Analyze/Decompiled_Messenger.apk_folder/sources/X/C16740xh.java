package X;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0xh  reason: invalid class name and case insensitive filesystem */
public final class C16740xh {
    public C16780xl A00;
    public final Map A01 = new HashMap();
    private final ScheduledExecutorService A02;

    public static final C16740xh A00(AnonymousClass1XY r2) {
        return new C16740xh(AnonymousClass0UX.A0e(r2));
    }

    public void A01() {
        for (Future future : this.A01.values()) {
            if (!future.isDone()) {
                future.cancel(true);
            }
        }
        this.A01.clear();
    }

    public void A02(String str, long j) {
        Future future = (Future) this.A01.remove(str);
        if (future != null && !future.isDone()) {
            future.cancel(true);
        }
        this.A01.put(str, this.A02.schedule(new D36(this, str), j, TimeUnit.MILLISECONDS));
    }

    public C16740xh(ScheduledExecutorService scheduledExecutorService) {
        this.A02 = scheduledExecutorService;
    }
}
