package X;

import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem;

/* renamed from: X.1BI  reason: invalid class name */
public interface AnonymousClass1BI {
    void BfN(InboxMontageItem inboxMontageItem);

    void BfO(InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem);

    boolean BfP(InboxUnitMontageActiveNowItem inboxUnitMontageActiveNowItem);
}
