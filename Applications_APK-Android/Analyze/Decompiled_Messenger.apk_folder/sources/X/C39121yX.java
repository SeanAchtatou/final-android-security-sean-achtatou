package X;

import io.card.payment.BuildConfig;
import java.io.Serializable;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;

/* renamed from: X.1yX  reason: invalid class name and case insensitive filesystem */
public final class C39121yX implements Comparable, C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("h265KeyFrameInterval", (byte) 8, 2);
    private static final C36241sk A01 = new C36241sk("h265KeyFrameSize", (byte) 8, 3);
    private static final C36241sk A02 = new C36241sk("requireSpsPpsForKeyframe", (byte) 2, 4);
    private static final C36241sk A03 = new C36241sk("useH265AndroidZeroCopyDecoder", (byte) 2, 1);
    private static final C36231sj A04 = new C36231sj("H265Config");
    public BitSet __isset_bit_vector = new BitSet(4);
    public int h265KeyFrameInterval = AnonymousClass1Y3.A7F;
    public int h265KeyFrameSize = -1;
    public boolean requireSpsPpsForKeyframe = false;
    public boolean useH265AndroidZeroCopyDecoder = false;

    public boolean equals(Object obj) {
        C39121yX r3;
        if (obj == null || !(obj instanceof C39121yX) || (r3 = (C39121yX) obj) == null) {
            return false;
        }
        if (this == r3) {
            return true;
        }
        if (!B36.A0I(this.useH265AndroidZeroCopyDecoder, r3.useH265AndroidZeroCopyDecoder) || !B36.A0A(this.h265KeyFrameInterval, r3.h265KeyFrameInterval) || !B36.A0A(this.h265KeyFrameSize, r3.h265KeyFrameSize) || !B36.A0I(this.requireSpsPpsForKeyframe, r3.requireSpsPpsForKeyframe)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(1, new AnonymousClass2EQ("useH265AndroidZeroCopyDecoder", (byte) 3, new C22604B3c((byte) 2)));
        hashMap.put(2, new AnonymousClass2EQ("h265KeyFrameInterval", (byte) 3, new C22604B3c((byte) 8)));
        hashMap.put(3, new AnonymousClass2EQ("h265KeyFrameSize", (byte) 3, new C22604B3c((byte) 8)));
        hashMap.put(4, new AnonymousClass2EQ("requireSpsPpsForKeyframe", (byte) 3, new C22604B3c((byte) 2)));
        AnonymousClass2EQ.A00(C39121yX.class, Collections.unmodifiableMap(hashMap));
    }

    public String CJ9(int i, boolean z) {
        String str;
        String str2;
        String str3 = BuildConfig.FLAVOR;
        if (z) {
            str = B36.A05(i);
        } else {
            str = str3;
        }
        if (z) {
            str2 = "\n";
        } else {
            str2 = str3;
        }
        if (z) {
            str3 = " ";
        }
        StringBuilder sb = new StringBuilder("H265Config");
        sb.append(str3);
        sb.append("(");
        sb.append(str2);
        sb.append(str);
        sb.append("useH265AndroidZeroCopyDecoder");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        int i2 = i + 1;
        sb.append(B36.A07(Boolean.valueOf(this.useH265AndroidZeroCopyDecoder), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("h265KeyFrameInterval");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Integer.valueOf(this.h265KeyFrameInterval), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("h265KeyFrameSize");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Integer.valueOf(this.h265KeyFrameSize), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("requireSpsPpsForKeyframe");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Boolean.valueOf(this.requireSpsPpsForKeyframe), i2, z));
        sb.append(AnonymousClass08S.A0J(str2, B36.A08(str)));
        sb.append(")");
        return sb.toString();
    }

    public void CNX(C35781ro r2) {
        r2.A0i(A04);
        r2.A0e(A03);
        r2.A0l(this.useH265AndroidZeroCopyDecoder);
        r2.A0S();
        r2.A0e(A00);
        r2.A0c(this.h265KeyFrameInterval);
        r2.A0S();
        r2.A0e(A01);
        r2.A0c(this.h265KeyFrameSize);
        r2.A0S();
        r2.A0e(A02);
        r2.A0l(this.requireSpsPpsForKeyframe);
        r2.A0S();
        r2.A0T();
        r2.A0X();
    }

    public int compareTo(Object obj) {
        int compareTo;
        C39121yX r5 = (C39121yX) obj;
        if (r5 == null) {
            throw new NullPointerException();
        } else if (r5 == this || ((compareTo = Boolean.valueOf(this.__isset_bit_vector.get(0)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(0)))) == 0 && (compareTo = B36.A04(this.useH265AndroidZeroCopyDecoder, r5.useH265AndroidZeroCopyDecoder)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(1)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(1)))) == 0 && (compareTo = B36.A00(this.h265KeyFrameInterval, r5.h265KeyFrameInterval)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(2)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(2)))) == 0 && (compareTo = B36.A00(this.h265KeyFrameSize, r5.h265KeyFrameSize)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(3)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(3)))) == 0 && (compareTo = B36.A04(this.requireSpsPpsForKeyframe, r5.requireSpsPpsForKeyframe)) == 0)) {
            return 0;
        } else {
            return compareTo;
        }
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{Boolean.valueOf(this.useH265AndroidZeroCopyDecoder), Integer.valueOf(this.h265KeyFrameInterval), Integer.valueOf(this.h265KeyFrameSize), Boolean.valueOf(this.requireSpsPpsForKeyframe)});
    }
}
