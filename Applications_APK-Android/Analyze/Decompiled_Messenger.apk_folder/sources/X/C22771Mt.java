package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Mt  reason: invalid class name and case insensitive filesystem */
public final class C22771Mt extends C22781Mu {
    private static final Bitmap.Config A03 = Bitmap.Config.ARGB_8888;
    private static volatile C22771Mt A04;
    public Method A00;
    private AnonymousClass0UN A01;
    private final AnonymousClass1YI A02;

    public AnonymousClass1S5 A03(AnonymousClass1NY r7, int i, C33271nJ r9, C23541Px r10) {
        if (0 != 0 || ((long) r7.A08()) <= 100000 || r7.A03 != 1 || !r7.A0D(i) || !this.A02.AbO(AnonymousClass1Y3.A0r, false)) {
            return A00(r7, i, r9, r10);
        }
        r7.A03 = 2;
        AnonymousClass1S5 A002 = A00(r7, i, r9, r10);
        r7.A03 = 1;
        return A002;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C22771Mt(X.AnonymousClass1XY r4, X.C22791Mv r5, X.C30591iK r6, X.AnonymousClass1O1 r7) {
        /*
            r3 = this;
            android.graphics.Bitmap$Config r0 = X.C22771Mt.A03
            X.1Ms r2 = r5.Anz(r0)
            X.1Ms r1 = r5.B9o(r0)
            if (r7 == 0) goto L_0x002d
            java.util.Map r0 = r7.A01
        L_0x000e:
            r3.<init>(r2, r1, r6, r0)
            X.0UN r1 = new X.0UN
            r0 = 1
            r1.<init>(r0, r4)
            r3.A01 = r1
            X.1YI r0 = X.AnonymousClass0WA.A00(r4)
            r3.A02 = r0
            if (r7 == 0) goto L_0x002c
            X.1OG r1 = X.AnonymousClass1OG.A03()
            java.util.List r0 = r7.A00
            r1.A01 = r0
            X.AnonymousClass1OG.A04(r1)
        L_0x002c:
            return
        L_0x002d:
            r0 = 0
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22771Mt.<init>(X.1XY, X.1Mv, X.1iK, X.1O1):void");
    }

    private AnonymousClass1S5 A00(AnonymousClass1NY r7, int i, C33271nJ r9, C23541Px r10) {
        BitmapFactory.Options options;
        if (!((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A01)).Aem(282063389066320L)) {
            return super.A03(r7, i, r9, r10);
        }
        int i2 = r7.A03;
        if (i2 != 1) {
            options = new BitmapFactory.Options();
            options.inPurgeable = true;
            options.inInputShareable = false;
            options.inSampleSize = i2;
        } else {
            options = null;
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(r7.A09(), null, options);
        if (decodeStream == null) {
            return null;
        }
        try {
            if (this.A00 == null) {
                this.A00 = Bitmap.class.getMethod("createAshmemBitmap", null);
            }
            AnonymousClass1PS A022 = AnonymousClass1PS.A02((Bitmap) this.A00.invoke(decodeStream, new Object[0]), C196549Mk.A00());
            AnonymousClass8W1 r3 = r10.A04;
            if (r3 != null) {
                Bitmap bitmap = (Bitmap) A022.A0A();
                if (Build.VERSION.SDK_INT >= 12) {
                    bitmap.setHasAlpha(true);
                }
                r3.A00(bitmap);
            }
            AnonymousClass1NY.A05(r7);
            int i3 = r7.A02;
            AnonymousClass1NY.A05(r7);
            return new AnonymousClass1S5(A022, r9, i3, r7.A00);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException unused) {
            return super.A03(r7, i, r9, r10);
        }
    }

    public static final C22771Mt A01(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (C22771Mt.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A04 = new C22771Mt(applicationInjector, AnonymousClass1MY.A0A(applicationInjector), AnonymousClass1MY.A0Z(applicationInjector), AnonymousClass1MY.A0O(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }
}
