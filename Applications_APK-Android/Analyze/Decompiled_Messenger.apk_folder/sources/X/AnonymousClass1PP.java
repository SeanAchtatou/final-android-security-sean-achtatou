package X;

import android.os.Build;

/* renamed from: X.1PP  reason: invalid class name */
public final class AnonymousClass1PP implements C30831ii {
    public double B7D(C133746Nn r6) {
        switch (r6.ordinal()) {
            case 0:
                if (Build.VERSION.SDK_INT >= 21) {
                    return C133746Nn.OnCloseToDalvikHeapLimit.mSuggestedTrimRatio;
                }
                return 0.0d;
            case 1:
            case 2:
            case 3:
            case 4:
                return 1.0d;
            default:
                AnonymousClass02w.A0F("BitmapMemoryCacheTrimStrategy", "unknown trim type: %s", r6);
                return 0.0d;
        }
    }
}
