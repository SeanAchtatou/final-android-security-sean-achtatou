package X;

import android.content.Context;
import android.content.DialogInterface;

/* renamed from: X.0rX  reason: invalid class name and case insensitive filesystem */
public class C13500rX extends C13370rK {
    public DialogInterface.OnClickListener A00;
    public DialogInterface.OnClickListener A01;
    public DialogInterface.OnClickListener A02;

    public C13370rK A00(int i, DialogInterface.OnClickListener onClickListener) {
        this.A00 = onClickListener;
        super.A00(i, onClickListener);
        return this;
    }

    public C13370rK A01(int i, DialogInterface.OnClickListener onClickListener) {
        this.A01 = onClickListener;
        super.A01(i, onClickListener);
        return this;
    }

    public C13370rK A02(int i, DialogInterface.OnClickListener onClickListener) {
        this.A02 = onClickListener;
        super.A02(i, onClickListener);
        return this;
    }

    public C13370rK A03(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.A00 = onClickListener;
        super.A03(charSequence, onClickListener);
        return this;
    }

    public C13370rK A04(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.A01 = onClickListener;
        super.A04(charSequence, onClickListener);
        return this;
    }

    public C13370rK A05(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.A02 = onClickListener;
        super.A05(charSequence, onClickListener);
        return this;
    }

    public AnonymousClass3At A06() {
        AnonymousClass3At A06 = super.A06();
        C123045qx.A01(A06);
        return A06;
    }

    public C13500rX(Context context) {
        super(new AnonymousClass6K9(context));
    }

    public C13500rX(Context context, int i) {
        super(new AnonymousClass6K9(context), i);
    }
}
