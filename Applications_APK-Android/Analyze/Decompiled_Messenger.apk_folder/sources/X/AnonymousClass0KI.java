package X;

import android.os.health.HealthStats;
import android.os.health.PackageHealthStats;
import android.os.health.PidHealthStats;
import android.os.health.ProcessHealthStats;
import android.os.health.ServiceHealthStats;
import android.os.health.TimerStat;
import android.os.health.UidHealthStats;
import android.util.Log;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0KI  reason: invalid class name */
public final class AnonymousClass0KI extends AnonymousClass0FM {
    public static final SparseArray A00 = new SparseArray();
    public String dataType;
    public final SparseArray measurement = new SparseArray();
    public final SparseArray measurements = new SparseArray();
    public final SparseArray stats = new SparseArray();
    public final SparseArray timer = new SparseArray();
    public final SparseArray timers = new SparseArray();

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1.equals(r5.dataType) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0058
            r2 = 0
            if (r5 == 0) goto L_0x001e
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x001e
            X.0KI r5 = (X.AnonymousClass0KI) r5
            java.lang.String r1 = r4.dataType
            if (r1 == 0) goto L_0x001f
            java.lang.String r0 = r5.dataType
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0024
        L_0x001e:
            return r2
        L_0x001f:
            java.lang.String r0 = r5.dataType
            if (r0 == 0) goto L_0x0024
            return r2
        L_0x0024:
            android.util.SparseArray r1 = r4.measurement
            android.util.SparseArray r0 = r5.measurement
            boolean r0 = X.C02740Gd.A01(r1, r0)
            if (r0 == 0) goto L_0x0057
            android.util.SparseArray r1 = r4.measurements
            android.util.SparseArray r0 = r5.measurements
            boolean r0 = X.C02740Gd.A01(r1, r0)
            if (r0 == 0) goto L_0x0057
            android.util.SparseArray r1 = r4.timer
            android.util.SparseArray r0 = r5.timer
            boolean r0 = X.C02740Gd.A01(r1, r0)
            if (r0 == 0) goto L_0x0057
            android.util.SparseArray r1 = r4.timers
            android.util.SparseArray r0 = r5.timers
            boolean r0 = X.C02740Gd.A01(r1, r0)
            if (r0 == 0) goto L_0x0057
            android.util.SparseArray r1 = r4.stats
            android.util.SparseArray r0 = r5.stats
            boolean r0 = X.C02740Gd.A01(r1, r0)
            if (r0 == 0) goto L_0x0057
            return r3
        L_0x0057:
            r3 = 0
        L_0x0058:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0KI.equals(java.lang.Object):boolean");
    }

    /* access modifiers changed from: private */
    /* renamed from: A00 */
    public AnonymousClass0KI A07(AnonymousClass0KI r6, AnonymousClass0KI r7) {
        boolean equals;
        if (r7 == null) {
            r7 = new AnonymousClass0KI();
        }
        r7.dataType = this.dataType;
        if (r6 == null || ((Long) this.measurement.get(10001, 0L)).longValue() - ((Long) r6.measurement.get(10001, 0L)).longValue() < 0) {
            r7.A05(this);
            return r7;
        }
        String str = r6.dataType;
        String str2 = this.dataType;
        if (str == null) {
            equals = false;
            if (str2 == null) {
                equals = true;
            }
        } else {
            equals = str.equals(str2);
        }
        if (equals) {
            A04(-1, this.measurement, r6.measurement, r7.measurement);
            A04(-1, this.measurements, r6.measurements, r7.measurements);
            A04(-1, this.timer, r6.timer, r7.timer);
            A04(-1, this.timers, r6.timers, r7.timers);
            A04(-1, this.stats, r6.stats, r7.stats);
            return r7;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0S("Attempting to subtract different types of HealthStatMetrics: ", str2, " and ", str));
    }

    /* access modifiers changed from: private */
    /* renamed from: A01 */
    public AnonymousClass0KI A08(AnonymousClass0KI r6, AnonymousClass0KI r7) {
        boolean equals;
        if (r7 == null) {
            r7 = new AnonymousClass0KI();
        }
        r7.dataType = this.dataType;
        if (r6 == null) {
            r7.A05(this);
            return r7;
        }
        String str = r6.dataType;
        String str2 = this.dataType;
        if (str == null) {
            equals = false;
            if (str2 == null) {
                equals = true;
            }
        } else {
            equals = str.equals(str2);
        }
        if (equals) {
            A04(1, this.measurement, r6.measurement, r7.measurement);
            A04(1, this.measurements, r6.measurements, r7.measurements);
            A04(1, this.timer, r6.timer, r7.timer);
            A04(1, this.timers, r6.timers, r7.timers);
            A04(1, this.stats, r6.stats, r7.stats);
            return r7;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0S("Attempting to add different types of HealthStatMetrics: ", str2, " and ", str));
    }

    private static Object A02(int i, Object obj, Object obj2) {
        int i2;
        Object obj3;
        Object A02;
        long longValue;
        if (obj instanceof Long) {
            long longValue2 = ((Long) obj).longValue();
            if (obj2 == null) {
                longValue = 0;
            } else {
                longValue = ((long) i) * ((Long) obj2).longValue();
            }
            return Long.valueOf(longValue2 + longValue);
        } else if (obj instanceof C03120Kc) {
            C03120Kc r9 = (C03120Kc) obj;
            C03120Kc r7 = (C03120Kc) obj2;
            if (obj2 == null) {
                return new C03120Kc(r9);
            }
            C03120Kc r6 = new C03120Kc();
            r6.A00 = r9.A00 + (r7.A00 * i);
            r6.A01 = r9.A01 + (((long) i) * r7.A01);
            return r6;
        } else if (obj instanceof AnonymousClass0KI) {
            AnonymousClass0KI r92 = (AnonymousClass0KI) obj;
            AnonymousClass0KI r10 = (AnonymousClass0KI) obj2;
            if (i == 1) {
                return r92.A08(r10, null);
            }
            return r92.A07(r10, null);
        } else if (obj instanceof AnonymousClass04a) {
            AnonymousClass04a r93 = (AnonymousClass04a) obj;
            AnonymousClass04a r102 = (AnonymousClass04a) obj2;
            int size = r93.size();
            AnonymousClass04a r5 = new AnonymousClass04a();
            for (int i3 = 0; i3 < size; i3++) {
                Object A07 = r93.A07(i3);
                if (r102 == null) {
                    obj3 = null;
                } else {
                    obj3 = r102.get(A07);
                }
                if (obj3 == null) {
                    A02 = r93.A09(i3);
                } else {
                    A02 = A02(i, r93.A09(i3), obj3);
                }
                r5.put(A07, A02);
            }
            if (i == 1) {
                if (r102 == null) {
                    i2 = 0;
                } else {
                    i2 = r102.size();
                }
                for (int i4 = 0; i4 < i2; i4++) {
                    Object A072 = r102.A07(i4);
                    if (r93.get(A072) == null) {
                        r5.put(A072, r102.A09(i4));
                    }
                }
            }
            return r5;
        } else {
            throw new IllegalArgumentException("Handling unsupported values");
        }
    }

    private static String A03(int i) {
        if (A00.size() == 0) {
            try {
                Class[] clsArr = {UidHealthStats.class, PidHealthStats.class, ProcessHealthStats.class, PackageHealthStats.class, ServiceHealthStats.class};
                Class<?> cls = Class.forName("android.os.health.HealthKeys$Constant");
                for (int i2 = 0; i2 < 5; i2++) {
                    for (Field field : clsArr[i2].getFields()) {
                        if (field.isAnnotationPresent(cls)) {
                            A00.put(field.getInt(null), field.getName());
                        }
                    }
                }
            } catch (IllegalAccessException e) {
                AnonymousClass0KZ.A00("HealthStatsMetrics", "Unable to read constant names", e);
                A00.put(-1, "Unable to read");
                return (String) A00.get(i, String.valueOf(i));
            } catch (ClassNotFoundException e2) {
                AnonymousClass0KZ.A00("HealthStatsMetrics", "Unable to find constant annotation", e2);
                A00.put(-1, "Unable to read");
                return (String) A00.get(i, String.valueOf(i));
            }
        }
        return (String) A00.get(i, String.valueOf(i));
    }

    private void A05(AnonymousClass0KI r8) {
        this.dataType = r8.dataType;
        this.measurement.clear();
        for (int i = 0; i < r8.measurement.size(); i++) {
            this.measurement.append(r8.measurement.keyAt(i), r8.measurement.valueAt(i));
        }
        this.timer.clear();
        for (int i2 = 0; i2 < r8.timer.size(); i2++) {
            this.timer.append(r8.timer.keyAt(i2), new C03120Kc((C03120Kc) r8.timer.valueAt(i2)));
        }
        this.measurements.clear();
        for (int i3 = 0; i3 < r8.measurements.size(); i3++) {
            AnonymousClass04a r2 = new AnonymousClass04a();
            r2.putAll((Map) r8.measurements.valueAt(i3));
            this.measurements.append(r8.measurements.keyAt(i3), r2);
        }
        this.timers.clear();
        for (int i4 = 0; i4 < r8.timers.size(); i4++) {
            AnonymousClass04a r5 = (AnonymousClass04a) r8.timers.valueAt(i4);
            AnonymousClass04a r4 = new AnonymousClass04a();
            for (int i5 = 0; i5 < r5.size(); i5++) {
                r4.put(r5.A07(i5), new C03120Kc((C03120Kc) r5.A09(i5)));
            }
            this.timers.append(r8.timers.keyAt(i4), r4);
        }
        this.stats.clear();
        for (int i6 = 0; i6 < r8.stats.size(); i6++) {
            AnonymousClass04a r52 = (AnonymousClass04a) r8.stats.valueAt(i6);
            AnonymousClass04a r42 = new AnonymousClass04a();
            for (int i7 = 0; i7 < r52.size(); i7++) {
                r42.put(r52.A07(i7), new AnonymousClass0KI((AnonymousClass0KI) r52.A09(i7)));
            }
            this.stats.append(r8.stats.keyAt(i6), r42);
        }
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A05((AnonymousClass0KI) r1);
        return this;
    }

    public JSONObject A09() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("type", this.dataType);
        JSONObject jSONObject2 = new JSONObject();
        int size = this.measurement.size();
        for (int i = 0; i < size; i++) {
            long longValue = ((Long) this.measurement.valueAt(i)).longValue();
            if (longValue != 0) {
                jSONObject2.put(A03(this.measurement.keyAt(i)), longValue);
            }
        }
        if (jSONObject2.length() > 0) {
            jSONObject.put("measurement", jSONObject2);
        }
        JSONObject jSONObject3 = new JSONObject();
        int size2 = this.timer.size();
        for (int i2 = 0; i2 < size2; i2++) {
            C03120Kc r9 = (C03120Kc) this.timer.valueAt(i2);
            if (r9.A00 != 0 || r9.A01 != 0) {
                String A03 = A03(this.timer.keyAt(i2));
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("count", r9.A00);
                jSONObject4.put("time_ms", r9.A01);
                jSONObject3.put(A03, jSONObject4);
            }
        }
        if (jSONObject3.length() > 0) {
            jSONObject.put("timer", jSONObject3);
        }
        JSONObject jSONObject5 = new JSONObject();
        int size3 = this.measurements.size();
        for (int i3 = 0; i3 < size3; i3++) {
            AnonymousClass04a r12 = (AnonymousClass04a) this.measurements.valueAt(i3);
            JSONObject jSONObject6 = new JSONObject();
            int size4 = r12.size();
            for (int i4 = 0; i4 < size4; i4++) {
                long longValue2 = ((Long) r12.A09(i4)).longValue();
                if (longValue2 != 0) {
                    jSONObject6.put((String) r12.A07(i4), longValue2);
                }
            }
            if (jSONObject6.length() > 0) {
                jSONObject5.put(A03(this.measurements.keyAt(i3)), jSONObject6);
            }
        }
        if (jSONObject5.length() > 0) {
            jSONObject.put("measurements", jSONObject5);
        }
        JSONObject jSONObject7 = new JSONObject();
        int size5 = this.timers.size();
        for (int i5 = 0; i5 < size5; i5++) {
            JSONObject jSONObject8 = new JSONObject();
            AnonymousClass04a r10 = (AnonymousClass04a) this.timers.valueAt(i5);
            int size6 = r10.size();
            for (int i6 = 0; i6 < size6; i6++) {
                C03120Kc r2 = (C03120Kc) r10.A09(i6);
                if (r2.A00 != 0 || r2.A01 != 0) {
                    JSONObject jSONObject9 = new JSONObject();
                    jSONObject9.put("count", r2.A00);
                    jSONObject9.put("time_ms", r2.A01);
                    jSONObject8.put((String) r10.A07(i6), jSONObject9);
                }
            }
            if (jSONObject8.length() > 0) {
                jSONObject7.put(A03(this.timers.keyAt(i5)), jSONObject8);
            }
        }
        if (jSONObject7.length() > 0) {
            jSONObject.put("timers", jSONObject7);
        }
        JSONObject jSONObject10 = new JSONObject();
        int size7 = this.stats.size();
        for (int i7 = 0; i7 < size7; i7++) {
            JSONObject jSONObject11 = new JSONObject();
            AnonymousClass04a r92 = (AnonymousClass04a) this.stats.valueAt(i7);
            int size8 = r92.size();
            for (int i8 = 0; i8 < size8; i8++) {
                JSONObject A09 = ((AnonymousClass0KI) r92.A09(i8)).A09();
                if (A09.length() > 0) {
                    jSONObject11.put((String) r92.A07(i8), A09);
                }
            }
            if (jSONObject11.length() > 0) {
                jSONObject10.put(A03(this.stats.keyAt(i7)), jSONObject11);
            }
        }
        if (jSONObject10.length() > 0) {
            jSONObject.put("stats", jSONObject10);
        }
        return jSONObject;
    }

    public int hashCode() {
        int i;
        String str = this.dataType;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        return (((((((((i * 31) + this.measurement.hashCode()) * 31) + this.timer.hashCode()) * 31) + this.measurements.hashCode()) * 31) + this.timers.hashCode()) * 31) + this.stats.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HealthStatsMetrics {\n");
        try {
            sb.append(A09().toString(2));
        } catch (JSONException e) {
            sb.append("<error>");
            Log.e("HealthStatsMetrics", "Unable to convert to string", e);
        }
        sb.append("\n}");
        return sb.toString();
    }

    private static void A04(int i, SparseArray sparseArray, SparseArray sparseArray2, SparseArray sparseArray3) {
        sparseArray3.clear();
        for (int i2 = 0; i2 < sparseArray.size(); i2++) {
            int keyAt = sparseArray.keyAt(i2);
            sparseArray3.put(keyAt, A02(i, sparseArray.valueAt(i2), sparseArray2.get(keyAt)));
        }
        if (i == 1) {
            for (int i3 = 0; i3 < sparseArray2.size(); i3++) {
                int keyAt2 = sparseArray2.keyAt(i3);
                if (sparseArray.get(keyAt2) == null) {
                    sparseArray3.put(keyAt2, sparseArray2.valueAt(i3));
                }
            }
        }
    }

    public void A0A(HealthStats healthStats) {
        this.dataType = healthStats.getDataType();
        this.measurement.clear();
        for (int i = 0; i < healthStats.getMeasurementKeyCount(); i++) {
            int measurementKeyAt = healthStats.getMeasurementKeyAt(i);
            this.measurement.put(measurementKeyAt, Long.valueOf(healthStats.getMeasurement(measurementKeyAt)));
        }
        this.measurements.clear();
        for (int i2 = 0; i2 < healthStats.getMeasurementsKeyCount(); i2++) {
            int measurementsKeyAt = healthStats.getMeasurementsKeyAt(i2);
            AnonymousClass04a r3 = new AnonymousClass04a();
            for (Map.Entry next : healthStats.getMeasurements(measurementsKeyAt).entrySet()) {
                r3.put(next.getKey(), next.getValue());
            }
            this.measurements.put(measurementsKeyAt, r3);
        }
        this.timer.clear();
        for (int i3 = 0; i3 < healthStats.getTimerKeyCount(); i3++) {
            int timerKeyAt = healthStats.getTimerKeyAt(i3);
            this.timer.put(timerKeyAt, new C03120Kc(healthStats.getTimerCount(timerKeyAt), healthStats.getTimerTime(timerKeyAt)));
        }
        this.timers.clear();
        for (int i4 = 0; i4 < healthStats.getTimersKeyCount(); i4++) {
            int timersKeyAt = healthStats.getTimersKeyAt(i4);
            AnonymousClass04a r4 = new AnonymousClass04a();
            for (Map.Entry next2 : healthStats.getTimers(timersKeyAt).entrySet()) {
                r4.put(next2.getKey(), new C03120Kc((TimerStat) next2.getValue()));
            }
            this.timers.put(timersKeyAt, r4);
        }
        this.stats.clear();
        for (int i5 = 0; i5 < healthStats.getStatsKeyCount(); i5++) {
            int statsKeyAt = healthStats.getStatsKeyAt(i5);
            AnonymousClass04a r42 = new AnonymousClass04a();
            for (Map.Entry next3 : healthStats.getStats(statsKeyAt).entrySet()) {
                r42.put(next3.getKey(), new AnonymousClass0KI((HealthStats) next3.getValue()));
            }
            this.stats.put(statsKeyAt, r42);
        }
    }

    public AnonymousClass0KI() {
    }

    private AnonymousClass0KI(AnonymousClass0KI r2) {
        A05(r2);
    }

    private AnonymousClass0KI(HealthStats healthStats) {
        A0A(healthStats);
    }
}
