package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1QV  reason: invalid class name */
public final class AnonymousClass1QV extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public DC3 A01;
    @Comparable(type = 13)
    public C26790DBo A02;
    @Comparable(type = 13)
    public MigColorScheme A03;
    @Comparable(type = 13)
    public Boolean A04;
    @Comparable(type = 13)
    public Boolean A05;
    @Comparable(type = 13)
    public String A06;
    @Comparable(type = 3)
    public boolean A07;

    public AnonymousClass1QV(Context context) {
        super("M4PhotosAndMediaPreferenceLayout");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
