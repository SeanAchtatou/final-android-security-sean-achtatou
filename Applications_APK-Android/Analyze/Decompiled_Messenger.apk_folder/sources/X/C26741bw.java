package X;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

/* renamed from: X.1bw  reason: invalid class name and case insensitive filesystem */
public final class C26741bw extends C10430k6 implements Serializable {
    private static final long serialVersionUID = 1;
    public final LinkedHashSet _registeredSubtypes;

    private void _collectAndResolve(C10070jV r11, AnonymousClass8ZW r12, C10470kA r13, C10140jc r14, HashMap hashMap) {
        AnonymousClass8ZW r6;
        String findTypeName;
        boolean z = false;
        if (r12._name != null) {
            z = true;
        }
        C10140jc r8 = r14;
        if (!z && (findTypeName = r14.findTypeName(r11)) != null) {
            r12 = new AnonymousClass8ZW(r12._class, findTypeName);
        }
        HashMap hashMap2 = hashMap;
        if (hashMap.containsKey(r12)) {
            boolean z2 = false;
            if (r12._name != null) {
                z2 = true;
            }
            if (z2) {
                boolean z3 = false;
                if (((AnonymousClass8ZW) hashMap.get(r12))._name != null) {
                    z3 = true;
                }
                if (!z3) {
                    hashMap.put(r12, r12);
                    return;
                }
                return;
            }
            return;
        }
        hashMap.put(r12, r12);
        List<AnonymousClass8ZW> findSubtypes = r14.findSubtypes(r11);
        if (findSubtypes != null && !findSubtypes.isEmpty()) {
            for (AnonymousClass8ZW r2 : findSubtypes) {
                C10470kA r7 = r13;
                C10070jV constructWithoutSuperTypes = C10070jV.constructWithoutSuperTypes(r2._class, r14, r13);
                boolean z4 = false;
                if (r2._name != null) {
                    z4 = true;
                }
                if (!z4) {
                    r6 = new AnonymousClass8ZW(r2._class, r14.findTypeName(constructWithoutSuperTypes));
                } else {
                    r6 = r2;
                }
                _collectAndResolve(constructWithoutSuperTypes, r6, r7, r8, hashMap2);
            }
        }
    }

    public Collection collectAndResolveSubtypes(C10070jV r16, C10470kA r17, C10140jc r18) {
        HashMap hashMap = new HashMap();
        LinkedHashSet linkedHashSet = this._registeredSubtypes;
        C10470kA r6 = r17;
        C10140jc r7 = r18;
        C10070jV r10 = r16;
        if (linkedHashSet != null) {
            Class rawType = r10.getRawType();
            Iterator it = linkedHashSet.iterator();
            while (it.hasNext()) {
                AnonymousClass8ZW r5 = (AnonymousClass8ZW) it.next();
                if (rawType.isAssignableFrom(r5._class)) {
                    _collectAndResolve(C10070jV.constructWithoutSuperTypes(r5._class, r7, r6), r5, r6, r7, hashMap);
                }
            }
        }
        _collectAndResolve(r10, new AnonymousClass8ZW(r10.getRawType(), null), r6, r7, hashMap);
        return new ArrayList(hashMap.values());
    }

    public Collection collectAndResolveSubtypes(C183512m r10, C10470kA r11, C10140jc r12, C10030jR r13) {
        Class cls;
        if (r13 == null) {
            cls = r10.getRawType();
        } else {
            cls = r13._class;
        }
        HashMap hashMap = new HashMap();
        LinkedHashSet linkedHashSet = this._registeredSubtypes;
        C10470kA r6 = r11;
        C10140jc r7 = r12;
        if (linkedHashSet != null) {
            Iterator it = linkedHashSet.iterator();
            while (it.hasNext()) {
                AnonymousClass8ZW r5 = (AnonymousClass8ZW) it.next();
                if (cls.isAssignableFrom(r5._class)) {
                    _collectAndResolve(C10070jV.constructWithoutSuperTypes(r5._class, r12, r11), r5, r6, r7, hashMap);
                }
            }
        }
        List<AnonymousClass8ZW> findSubtypes = r12.findSubtypes(r10);
        if (findSubtypes != null) {
            for (AnonymousClass8ZW r52 : findSubtypes) {
                _collectAndResolve(C10070jV.constructWithoutSuperTypes(r52._class, r12, r11), r52, r6, r7, hashMap);
            }
        }
        _collectAndResolve(C10070jV.constructWithoutSuperTypes(cls, r12, r11), new AnonymousClass8ZW(cls, null), r6, r7, hashMap);
        return new ArrayList(hashMap.values());
    }
}
