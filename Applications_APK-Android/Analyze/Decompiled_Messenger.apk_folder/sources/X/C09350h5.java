package X;

import android.text.TextUtils;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.0h5  reason: invalid class name and case insensitive filesystem */
public final class C09350h5 extends AnonymousClass0Wl {
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public AnonymousClass0UN A00;
    public final AnonymousClass0Ud A01;
    public final AnonymousClass09P A02;
    public final FbSharedPreferences A03;
    private final C09360hB A04;
    private final C25921ac A05;
    private final C04310Tq A06;

    public String getSimpleName() {
        return "ErrorReporterSecondaryInit";
    }

    static {
        AnonymousClass1Y7 r1 = C04350Ue.A06;
        A07 = (AnonymousClass1Y7) r1.A09("app_version_name_current");
        A08 = (AnonymousClass1Y7) r1.A09(ErrorReportingConstants.PREV_APP_VERSION_STR);
    }

    public static final C09350h5 A00(AnonymousClass1XY r1) {
        return new C09350h5(r1);
    }

    private C09350h5(AnonymousClass1XY r5) {
        this.A00 = new AnonymousClass0UN(2, r5);
        this.A02 = C04750Wa.A01(r5);
        this.A06 = AnonymousClass0XJ.A0K(r5);
        this.A05 = C25901aa.A00(r5);
        this.A03 = FbSharedPreferencesModule.A00(r5);
        this.A04 = new C09360hB(new C09370hC(C04490Ux.A0J(r5), C04490Ux.A0t(r5)));
        this.A01 = AnonymousClass0Ud.A00(r5);
    }

    public void init() {
        int A032 = C000700l.A03(858718338);
        String str = (String) this.A06.get();
        if (!TextUtils.isEmpty(str)) {
            this.A02.Bz1(str);
        }
        String B7Z = this.A05.B7Z();
        if (B7Z != null) {
            this.A02.Bz2(TurboLoader.Locator.$const$string(165), B7Z);
        }
        this.A02.Bz9(ErrorReportingConstants.INSTALLED_FACEBOOK_APKS, this.A04);
        this.A02.Bz9("last_launcher_intent_ms", new AnonymousClass190(this));
        this.A02.Bz9("last_user_interaction_ms", new C09430hM(this));
        this.A02.Bz9("analytics_session_id", new C09440hN(this));
        String A022 = ((C09400hF) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BMV, this.A00)).A02();
        if (A022 == null) {
            A022 = "unknown";
        }
        if (this.A03.BBh(A07)) {
            String B4F = this.A03.B4F(A07, "DUMMY");
            if (B4F.equals(A022)) {
                this.A02.Bz2(ErrorReportingConstants.PREV_APP_VERSION_STR, this.A03.B4F(A08, "unknown"));
            } else {
                this.A02.Bz2(ErrorReportingConstants.PREV_APP_VERSION_STR, B4F);
                C30281hn edit = this.A03.edit();
                edit.BzC(A08, B4F);
                edit.BzC(A07, A022);
                edit.commit();
            }
        } else {
            C30281hn edit2 = this.A03.edit();
            edit2.BzC(A08, "unknown");
            edit2.BzC(A07, A022);
            edit2.commit();
            this.A02.Bz2(ErrorReportingConstants.PREV_APP_VERSION_STR, "unknown");
        }
        C000700l.A09(-516908055, A032);
    }
}
