package X;

import com.facebook.webpsupport.WebpBitmapFactoryImpl;
import java.io.UnsupportedEncodingException;

/* renamed from: X.1Q4  reason: invalid class name */
public final class AnonymousClass1Q4 {
    public static WebpBitmapFactoryImpl A00 = null;
    private static boolean A01 = false;
    public static final boolean A02;
    public static final boolean A03;
    public static final byte[] A04 = A03("VP8L");
    public static final byte[] A05 = A03("VP8X");
    public static final byte[] A06 = A03("VP8 ");
    private static final byte[] A07 = A03("WEBP");
    private static final byte[] A08 = A03("RIFF");

    public static boolean A02(byte[] bArr, int i, byte[] bArr2) {
        if (!(bArr2 == null || bArr == null)) {
            int length = bArr2.length;
            if (length + i <= bArr.length) {
                int i2 = 0;
                while (i2 < length) {
                    if (bArr[i2 + i] == bArr2[i2]) {
                        i2++;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        if (r1.outWidth == 1) goto L_0x005a;
     */
    static {
        /*
            int r2 = android.os.Build.VERSION.SDK_INT
            r4 = 0
            r1 = 17
            r0 = 0
            if (r2 > r1) goto L_0x0009
            r0 = 1
        L_0x0009:
            X.AnonymousClass1Q4.A03 = r0
            if (r2 < r1) goto L_0x0029
            r3 = 1
            if (r2 != r1) goto L_0x005a
            java.lang.String r0 = "UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA=="
            byte[] r2 = android.util.Base64.decode(r0, r4)
            android.graphics.BitmapFactory$Options r1 = new android.graphics.BitmapFactory$Options
            r1.<init>()
            r1.inJustDecodeBounds = r3
            int r0 = r2.length
            android.graphics.BitmapFactory.decodeByteArray(r2, r4, r0, r1)
            int r0 = r1.outHeight
            if (r0 != r3) goto L_0x0029
            int r0 = r1.outWidth
            if (r0 == r3) goto L_0x005a
        L_0x0029:
            r0 = 0
        L_0x002a:
            X.AnonymousClass1Q4.A02 = r0
            r0 = 0
            X.AnonymousClass1Q4.A00 = r0
            X.AnonymousClass1Q4.A01 = r4
            java.lang.String r0 = "RIFF"
            byte[] r0 = A03(r0)
            X.AnonymousClass1Q4.A08 = r0
            java.lang.String r0 = "WEBP"
            byte[] r0 = A03(r0)
            X.AnonymousClass1Q4.A07 = r0
            java.lang.String r0 = "VP8 "
            byte[] r0 = A03(r0)
            X.AnonymousClass1Q4.A06 = r0
            java.lang.String r0 = "VP8L"
            byte[] r0 = A03(r0)
            X.AnonymousClass1Q4.A04 = r0
            java.lang.String r0 = "VP8X"
            byte[] r0 = A03(r0)
            X.AnonymousClass1Q4.A05 = r0
            return
        L_0x005a:
            r0 = 1
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Q4.<clinit>():void");
    }

    public static WebpBitmapFactoryImpl A00() {
        if (A01) {
            return A00;
        }
        WebpBitmapFactoryImpl webpBitmapFactoryImpl = null;
        try {
            webpBitmapFactoryImpl = (WebpBitmapFactoryImpl) Class.forName("com.facebook.webpsupport.WebpBitmapFactoryImpl").newInstance();
        } catch (Throwable unused) {
        }
        A01 = true;
        return webpBitmapFactoryImpl;
    }

    public static boolean A01(byte[] bArr, int i, int i2) {
        if (i2 < 20 || !A02(bArr, i, A08) || !A02(bArr, i + 8, A07)) {
            return false;
        }
        return true;
    }

    private static byte[] A03(String str) {
        try {
            return str.getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("ASCII not found!", e);
        }
    }
}
