package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.facebook.voltron.metadata.VoltronModuleMetadataHelper;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0Er  reason: invalid class name and case insensitive filesystem */
public final class C02420Er {
    private static final int[] APP_MODULE_INDICES = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
    private static final int[] APP_MODULE_RANGES = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

    public static String A03(String str) {
        String str2;
        if (str == null) {
            throw new IllegalArgumentException("Class name is null");
        } else if (!str.isEmpty()) {
            if (str.startsWith("X.")) {
                str2 = "X";
            } else {
                str2 = null;
                if (Character.isLowerCase(str.codePointAt(0))) {
                    int indexOf = str.indexOf(46);
                    while (true) {
                        if (indexOf <= 0) {
                            break;
                        }
                        int i = indexOf + 1;
                        if (!Character.isLowerCase(str.codePointAt(i))) {
                            str2 = str.substring(0, indexOf);
                            break;
                        }
                        indexOf = str.indexOf(46, i);
                    }
                }
            }
            if ("X".equals(str2)) {
                int moduleRangeIndexForRedexClassName = VoltronModuleMetadataHelper.getModuleRangeIndexForRedexClassName(str, APP_MODULE_RANGES);
                if (moduleRangeIndexForRedexClassName == -1) {
                    return null;
                }
                return A02(APP_MODULE_INDICES[moduleRangeIndexForRedexClassName]);
            } else if (Math.abs(str.hashCode() % 1) == 0) {
                String A04 = A04(str);
                if (A04 != null) {
                    return A04;
                }
                return A05(str2);
            } else {
                throw new IllegalStateException("Unreachable state");
            }
        } else {
            throw new IllegalArgumentException("Class name is empty");
        }
    }

    private static String A05(String str) {
        if (str == null) {
            return null;
        }
        int abs = Math.abs(str.hashCode() % 2);
        if (abs == 0) {
            return A06(str);
        }
        if (abs == 1) {
            return A07(str);
        }
        throw new IllegalStateException("Unreachable state");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0034, code lost:
        if (r13.equals("msuggestionsops") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003f, code lost:
        if (r13.equals("slam") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004a, code lost:
        if (r13.equals("openh264") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (r13.equals("mapbox") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005e, code lost:
        if (r13.equals("instantgamesads") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0068, code lost:
        if (r13.equals("cardio") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0072, code lost:
        if (r13.equals("caffe2deeptext") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007c, code lost:
        if (r13.equals("effects") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0020, code lost:
        if (r13.equals("internalprefs") == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        if (r13.equals("arservicesoptional") == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.String r13) {
        /*
            int r0 = r13.hashCode()
            r12 = 9
            r11 = 8
            r10 = 7
            r9 = 6
            r8 = 5
            r7 = 4
            r6 = 3
            r5 = 2
            r4 = 1
            r3 = 0
            r2 = -1
            switch(r0) {
                case -1833928446: goto L_0x0075;
                case -1741830236: goto L_0x006b;
                case -1367604170: goto L_0x0061;
                case -1317439696: goto L_0x0057;
                case -1081373969: goto L_0x004d;
                case -504104366: goto L_0x0042;
                case 3532869: goto L_0x0037;
                case 1061772048: goto L_0x002d;
                case 1736157167: goto L_0x0023;
                case 1842317843: goto L_0x0019;
                default: goto L_0x0014;
            }
        L_0x0014:
            r1 = -1
        L_0x0015:
            switch(r1) {
                case 0: goto L_0x0088;
                case 1: goto L_0x0087;
                case 2: goto L_0x0086;
                case 3: goto L_0x0085;
                case 4: goto L_0x0084;
                case 5: goto L_0x0083;
                case 6: goto L_0x0082;
                case 7: goto L_0x0081;
                case 8: goto L_0x0080;
                case 9: goto L_0x007f;
                default: goto L_0x0018;
            }
        L_0x0018:
            return r2
        L_0x0019:
            java.lang.String r0 = "internalprefs"
            boolean r0 = r13.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x0023:
            java.lang.String r0 = "arservicesoptional"
            boolean r0 = r13.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x002d:
            java.lang.String r0 = "msuggestionsops"
            boolean r0 = r13.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x0037:
            java.lang.String r0 = "slam"
            boolean r0 = r13.equals(r0)
            r1 = 9
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x0042:
            java.lang.String r0 = "openh264"
            boolean r0 = r13.equals(r0)
            r1 = 8
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x004d:
            java.lang.String r0 = "mapbox"
            boolean r0 = r13.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x0057:
            java.lang.String r0 = "instantgamesads"
            boolean r0 = r13.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x0061:
            java.lang.String r0 = "cardio"
            boolean r0 = r13.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x006b:
            java.lang.String r0 = "caffe2deeptext"
            boolean r0 = r13.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x0075:
            java.lang.String r0 = "effects"
            boolean r0 = r13.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x0015
            goto L_0x0014
        L_0x007f:
            return r12
        L_0x0080:
            return r11
        L_0x0081:
            return r10
        L_0x0082:
            return r9
        L_0x0083:
            return r8
        L_0x0084:
            return r7
        L_0x0085:
            return r6
        L_0x0086:
            return r5
        L_0x0087:
            return r4
        L_0x0088:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02420Er.A00(java.lang.String):int");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0034, code lost:
        if (r2.equals("cardio") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        if (r2.equals("instantgamesads") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0048, code lost:
        if (r2.equals("mapbox") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0053, code lost:
        if (r2.equals("openh264") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005e, code lost:
        if (r2.equals("slam") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0068, code lost:
        if (r2.equals("msuggestionsops") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
        if (r2.equals("arservicesoptional") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007c, code lost:
        if (r2.equals("internalprefs") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        if (r2.equals("effects") == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002a, code lost:
        if (r2.equals("caffe2deeptext") == false) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Integer A01(java.lang.String r2) {
        /*
            int r0 = r2.hashCode()
            switch(r0) {
                case -1833928446: goto L_0x0019;
                case -1741830236: goto L_0x0023;
                case -1367604170: goto L_0x002d;
                case -1317439696: goto L_0x0037;
                case -1081373969: goto L_0x0041;
                case -504104366: goto L_0x004b;
                case 3532869: goto L_0x0056;
                case 1061772048: goto L_0x0061;
                case 1736157167: goto L_0x006b;
                case 1842317843: goto L_0x0075;
                default: goto L_0x0007;
            }
        L_0x0007:
            r1 = -1
        L_0x0008:
            switch(r1) {
                case 0: goto L_0x007f;
                case 1: goto L_0x0082;
                case 2: goto L_0x0082;
                case 3: goto L_0x0085;
                case 4: goto L_0x0085;
                case 5: goto L_0x0082;
                case 6: goto L_0x0082;
                case 7: goto L_0x0082;
                case 8: goto L_0x0085;
                case 9: goto L_0x0082;
                default: goto L_0x000b;
            }
        L_0x000b:
            java.lang.String r0 = "Unexpected module name: "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r2)
            java.lang.String r0 = "VoltronModuleMetadata"
            X.C010708t.A0K(r0, r1)
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            return r0
        L_0x0019:
            java.lang.String r0 = "effects"
            boolean r0 = r2.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0023:
            java.lang.String r0 = "caffe2deeptext"
            boolean r0 = r2.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x002d:
            java.lang.String r0 = "cardio"
            boolean r0 = r2.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0037:
            java.lang.String r0 = "instantgamesads"
            boolean r0 = r2.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0041:
            java.lang.String r0 = "mapbox"
            boolean r0 = r2.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x004b:
            java.lang.String r0 = "openh264"
            boolean r0 = r2.equals(r0)
            r1 = 8
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0056:
            java.lang.String r0 = "slam"
            boolean r0 = r2.equals(r0)
            r1 = 9
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0061:
            java.lang.String r0 = "msuggestionsops"
            boolean r0 = r2.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x006b:
            java.lang.String r0 = "arservicesoptional"
            boolean r0 = r2.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0075:
            java.lang.String r0 = "internalprefs"
            boolean r0 = r2.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x007f:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            return r0
        L_0x0082:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            return r0
        L_0x0085:
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02420Er.A01(java.lang.String):java.lang.Integer");
    }

    public static String A02(int i) {
        switch (i) {
            case 0:
                return "arservicesoptional";
            case 1:
                return "caffe2deeptext";
            case 2:
                return "cardio";
            case 3:
                return "effects";
            case 4:
                return "instantgamesads";
            case 5:
                return "internalprefs";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "mapbox";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "msuggestionsops";
            case 8:
                return "openh264";
            case Process.SIGKILL /*9*/:
                return "slam";
            default:
                C010708t.A0K("VoltronModuleMetadata", AnonymousClass08S.A09("Unexpected module index: ", i));
                return null;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (r5.equals("com.facebook.http.prefs.HttpPreferencesFactory") == false) goto L_0x000a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0030, code lost:
        if (r5.equals("com.facebook.analytics.structuredlogger.events.InternalSettingsOpened") == false) goto L_0x000a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        if (r5.equals("com.facebook.analytics.structuredlogger.events.InternalSettingsOpened$Factory") == false) goto L_0x000a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r5.equals("com.facebook.analytics.structuredlogger.events.InternalSettingsOpenedImpl") == false) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String A04(java.lang.String r5) {
        /*
            int r0 = r5.hashCode()
            r4 = 3
            r3 = 2
            r2 = 1
            switch(r0) {
                case -1408227148: goto L_0x0015;
                case -413295677: goto L_0x001f;
                case 230730868: goto L_0x0029;
                case 891621562: goto L_0x0033;
                default: goto L_0x000a;
            }
        L_0x000a:
            r1 = -1
        L_0x000b:
            if (r1 == 0) goto L_0x003d
            if (r1 == r2) goto L_0x003d
            if (r1 == r3) goto L_0x003d
            if (r1 == r4) goto L_0x003d
            r0 = 0
            return r0
        L_0x0015:
            java.lang.String r0 = "com.facebook.analytics.structuredlogger.events.InternalSettingsOpenedImpl"
            boolean r0 = r5.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x000b
            goto L_0x000a
        L_0x001f:
            java.lang.String r0 = "com.facebook.http.prefs.HttpPreferencesFactory"
            boolean r0 = r5.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x000b
            goto L_0x000a
        L_0x0029:
            java.lang.String r0 = "com.facebook.analytics.structuredlogger.events.InternalSettingsOpened"
            boolean r0 = r5.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x000b
            goto L_0x000a
        L_0x0033:
            java.lang.String r0 = "com.facebook.analytics.structuredlogger.events.InternalSettingsOpened$Factory"
            boolean r0 = r5.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x000b
            goto L_0x000a
        L_0x003d:
            java.lang.String r0 = "internalprefs"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02420Er.A04(java.lang.String):java.lang.String");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0239, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0245, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0251, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x025c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0267, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0273, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x027f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x028b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0297, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02a3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02af, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02bb, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02c7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02d3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02df, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02eb, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02f7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0303, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x030f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x031b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0327, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0333, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x033f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x034b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0357, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0363, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x036f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x037b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0387, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0393, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x039f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x03ab, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x03b7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03c3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03cf, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03db, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03e7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03f3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x03ff, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x040b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x0417, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0422, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0061, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0077, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0082, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008d, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0099, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a5, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b1, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00bd, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c9, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d5, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00e1, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ed, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f9, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0105, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0110, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x011c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0128, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0134, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x014c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0158, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0164, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0170, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x017c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0188, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0194, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01a0, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01ab, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01b7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01c3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01cf, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01db, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01e7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01f3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0020, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01ff, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x020a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0216, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0222, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x022d, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String A06(java.lang.String r1) {
        /*
            int r0 = r1.hashCode()
            switch(r0) {
                case -2036287542: goto L_0x000d;
                case -2032956884: goto L_0x0018;
                case -1981196920: goto L_0x0023;
                case -1949136376: goto L_0x002e;
                case -1850706498: goto L_0x0038;
                case -1842104928: goto L_0x0043;
                case -1791611172: goto L_0x004e;
                case -1780617188: goto L_0x0059;
                case -1740701722: goto L_0x0064;
                case -1724835988: goto L_0x006f;
                case -1719412836: goto L_0x007a;
                case -1691702792: goto L_0x0085;
                case -1690908854: goto L_0x0091;
                case -1676935988: goto L_0x009d;
                case -1603460824: goto L_0x00a9;
                case -1504281732: goto L_0x00b5;
                case -1452273644: goto L_0x00c1;
                case -1428512038: goto L_0x00cd;
                case -1340089978: goto L_0x00d9;
                case -1209293018: goto L_0x00e5;
                case -1009128268: goto L_0x00f1;
                case -952830860: goto L_0x00fd;
                case -933470354: goto L_0x0109;
                case -932057170: goto L_0x0114;
                case -921792342: goto L_0x0120;
                case -883390492: goto L_0x012c;
                case -845676658: goto L_0x0138;
                case -751106432: goto L_0x0144;
                case -625876774: goto L_0x0150;
                case -605316998: goto L_0x015c;
                case -534416306: goto L_0x0168;
                case -497494522: goto L_0x0174;
                case -485972600: goto L_0x0180;
                case -481746542: goto L_0x018c;
                case -413848806: goto L_0x0198;
                case -360322526: goto L_0x01a4;
                case -274290368: goto L_0x01af;
                case -221142106: goto L_0x01bb;
                case -184351768: goto L_0x01c7;
                case -131659694: goto L_0x01d3;
                case -73819734: goto L_0x01df;
                case 38462434: goto L_0x01eb;
                case 59977478: goto L_0x01f7;
                case 70557176: goto L_0x0203;
                case 106485324: goto L_0x020e;
                case 139770122: goto L_0x021a;
                case 196521900: goto L_0x0226;
                case 308890436: goto L_0x0231;
                case 353396332: goto L_0x023d;
                case 375869830: goto L_0x0249;
                case 383091170: goto L_0x0255;
                case 436821446: goto L_0x0260;
                case 458789554: goto L_0x026b;
                case 480602824: goto L_0x0277;
                case 489066202: goto L_0x0283;
                case 556282474: goto L_0x028f;
                case 573519370: goto L_0x029b;
                case 586139606: goto L_0x02a7;
                case 668754930: goto L_0x02b3;
                case 676152958: goto L_0x02bf;
                case 738678584: goto L_0x02cb;
                case 753148260: goto L_0x02d7;
                case 893754518: goto L_0x02e3;
                case 933313084: goto L_0x02ef;
                case 987865228: goto L_0x02fb;
                case 1052338222: goto L_0x0307;
                case 1122444326: goto L_0x0313;
                case 1130273214: goto L_0x031f;
                case 1203460186: goto L_0x032b;
                case 1250165200: goto L_0x0337;
                case 1258774236: goto L_0x0343;
                case 1348677736: goto L_0x034f;
                case 1394802792: goto L_0x035b;
                case 1408141870: goto L_0x0367;
                case 1425663524: goto L_0x0373;
                case 1456397522: goto L_0x037f;
                case 1541488000: goto L_0x038b;
                case 1562010396: goto L_0x0397;
                case 1591767338: goto L_0x03a3;
                case 1604327652: goto L_0x03af;
                case 1652370904: goto L_0x03bb;
                case 1705912224: goto L_0x03c7;
                case 1719550696: goto L_0x03d3;
                case 1777796116: goto L_0x03df;
                case 1785714900: goto L_0x03eb;
                case 1914989306: goto L_0x03f7;
                case 1960359020: goto L_0x0403;
                case 2061921654: goto L_0x040f;
                case 2082222854: goto L_0x041b;
                default: goto L_0x0007;
            }
        L_0x0007:
            r1 = -1
        L_0x0008:
            switch(r1) {
                case 0: goto L_0x0426;
                case 1: goto L_0x0426;
                case 2: goto L_0x0429;
                case 3: goto L_0x0429;
                case 4: goto L_0x0429;
                case 5: goto L_0x0429;
                case 6: goto L_0x0429;
                case 7: goto L_0x0429;
                case 8: goto L_0x0429;
                case 9: goto L_0x0429;
                case 10: goto L_0x0429;
                case 11: goto L_0x0429;
                case 12: goto L_0x0429;
                case 13: goto L_0x0429;
                case 14: goto L_0x0429;
                case 15: goto L_0x0429;
                case 16: goto L_0x0429;
                case 17: goto L_0x0429;
                case 18: goto L_0x0429;
                case 19: goto L_0x0429;
                case 20: goto L_0x0429;
                case 21: goto L_0x0429;
                case 22: goto L_0x0429;
                case 23: goto L_0x0429;
                case 24: goto L_0x0429;
                case 25: goto L_0x0429;
                case 26: goto L_0x0429;
                case 27: goto L_0x0429;
                case 28: goto L_0x0429;
                case 29: goto L_0x0429;
                case 30: goto L_0x0429;
                case 31: goto L_0x0429;
                case 32: goto L_0x0429;
                case 33: goto L_0x0429;
                case 34: goto L_0x0429;
                case 35: goto L_0x0429;
                case 36: goto L_0x0429;
                case 37: goto L_0x0429;
                case 38: goto L_0x0429;
                case 39: goto L_0x0429;
                case 40: goto L_0x0429;
                case 41: goto L_0x0429;
                case 42: goto L_0x0429;
                case 43: goto L_0x0429;
                case 44: goto L_0x0429;
                case 45: goto L_0x0429;
                case 46: goto L_0x0429;
                case 47: goto L_0x0429;
                case 48: goto L_0x0429;
                case 49: goto L_0x0429;
                case 50: goto L_0x0429;
                case 51: goto L_0x0429;
                case 52: goto L_0x0429;
                case 53: goto L_0x0429;
                case 54: goto L_0x0429;
                case 55: goto L_0x0429;
                case 56: goto L_0x0429;
                case 57: goto L_0x0429;
                case 58: goto L_0x0429;
                case 59: goto L_0x0429;
                case 60: goto L_0x0429;
                case 61: goto L_0x0429;
                case 62: goto L_0x0429;
                case 63: goto L_0x0429;
                case 64: goto L_0x042c;
                case 65: goto L_0x042c;
                case 66: goto L_0x042c;
                case 67: goto L_0x042c;
                case 68: goto L_0x042c;
                case 69: goto L_0x042c;
                case 70: goto L_0x042c;
                case 71: goto L_0x042c;
                case 72: goto L_0x042c;
                case 73: goto L_0x042c;
                case 74: goto L_0x042c;
                case 75: goto L_0x042c;
                case 76: goto L_0x042c;
                case 77: goto L_0x042c;
                case 78: goto L_0x042c;
                case 79: goto L_0x042c;
                case 80: goto L_0x042c;
                case 81: goto L_0x042c;
                case 82: goto L_0x042c;
                case 83: goto L_0x042c;
                case 84: goto L_0x042f;
                case 85: goto L_0x042f;
                case 86: goto L_0x042f;
                case 87: goto L_0x042f;
                case 88: goto L_0x042f;
                default: goto L_0x000b;
            }
        L_0x000b:
            r0 = 0
            return r0
        L_0x000d:
            java.lang.String r0 = "com.mapbox.geojson.utils"
            boolean r0 = r1.equals(r0)
            r1 = 88
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0018:
            java.lang.String r0 = "com.facebook.ads.internal.view.component"
            boolean r0 = r1.equals(r0)
            r1 = 49
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0023:
            java.lang.String r0 = "com.facebook.surveyplatformdev.graphql"
            boolean r0 = r1.equals(r0)
            r1 = 81
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x002e:
            java.lang.String r0 = "com.facebook.ads.cache.internal.file"
            boolean r0 = r1.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0038:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.audiencenetworkimpl.util"
            boolean r0 = r1.equals(r0)
            r1 = 21
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0043:
            java.lang.String r0 = "com.facebook.ads.internal.bridge.gms"
            boolean r0 = r1.equals(r0)
            r1 = 32
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x004e:
            java.lang.String r0 = "com.facebook.ads.internal.view.video.plugins"
            boolean r0 = r1.equals(r0)
            r1 = 59
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0059:
            java.lang.String r0 = "com.facebook.ads.internal.adcontrollers"
            boolean r0 = r1.equals(r0)
            r1 = 12
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0064:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.library.nativesignals"
            boolean r0 = r1.equals(r0)
            r1 = 27
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x006f:
            java.lang.String r0 = "com.facebook.selfupdate2.internalsettings"
            boolean r0 = r1.equals(r0)
            r1 = 79
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x007a:
            java.lang.String r0 = "com.facebook.ads.internal.server.constants"
            boolean r0 = r1.equals(r0)
            r1 = 41
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0085:
            java.lang.String r0 = "com.facebook.ads.internal.bench"
            boolean r0 = r1.equals(r0)
            r1 = 19
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0091:
            java.lang.String r0 = "com.facebook.ads.internal.cache"
            boolean r0 = r1.equals(r0)
            r1 = 33
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x009d:
            java.lang.String r0 = "com.facebook.ads.internal.redex"
            boolean r0 = r1.equals(r0)
            r1 = 40
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00a9:
            java.lang.String r0 = "com.facebook.zero.internal"
            boolean r0 = r1.equals(r0)
            r1 = 82
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00b5:
            java.lang.String r0 = "com.facebook.surveyplatform.surveyplatformremixnt"
            boolean r0 = r1.equals(r0)
            r1 = 80
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00c1:
            java.lang.String r0 = "com.facebook.mig.favicon.uri"
            boolean r0 = r1.equals(r0)
            r1 = 75
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00cd:
            java.lang.String r0 = "com.facebook.mig.xma.template"
            boolean r0 = r1.equals(r0)
            r1 = 78
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00d9:
            java.lang.String r0 = "com.facebook.ads.internal.adapters.datamodels"
            boolean r0 = r1.equals(r0)
            r1 = 10
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00e5:
            java.lang.String r0 = "com.facebook.account.logout"
            boolean r0 = r1.equals(r0)
            r1 = 64
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00f1:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.controller"
            boolean r0 = r1.equals(r0)
            r1 = 23
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00fd:
            java.lang.String r0 = "com.facebook.ads.internal.adapters.util"
            boolean r0 = r1.equals(r0)
            r1 = 11
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0109:
            java.lang.String r0 = "com.facebook.ads.internal.action"
            boolean r0 = r1.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0114:
            java.lang.String r0 = "com.facebook.ads.internal.view.component.interstitial"
            boolean r0 = r1.equals(r0)
            r1 = 50
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0120:
            java.lang.String r0 = "com.facebook.ads.internal.apiimp"
            boolean r0 = r1.equals(r0)
            r1 = 18
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x012c:
            java.lang.String r0 = "com.facebook.applifecycle"
            boolean r0 = r1.equals(r0)
            r1 = 66
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0138:
            java.lang.String r0 = "com.facebook.ads.internal.device"
            boolean r0 = r1.equals(r0)
            r1 = 35
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0144:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.library"
            boolean r0 = r1.equals(r0)
            r1 = 26
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0150:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.model.signal_value"
            boolean r0 = r1.equals(r0)
            r1 = 29
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x015c:
            java.lang.String r0 = "com.facebook.ads.internal.util.reporting"
            boolean r0 = r1.equals(r0)
            r1 = 44
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0168:
            java.lang.String r0 = "com.facebook.messaging.internalprefs"
            boolean r0 = r1.equals(r0)
            r1 = 71
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0174:
            java.lang.String r0 = "com.facebook.ads.internal.eventstorage.record"
            boolean r0 = r1.equals(r0)
            r1 = 37
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0180:
            java.lang.String r0 = "com.mapbox.geojson.shifter"
            boolean r0 = r1.equals(r0)
            r1 = 87
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x018c:
            java.lang.String r0 = "com.mapbox.geojson.gson"
            boolean r0 = r1.equals(r0)
            r1 = 86
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0198:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.util"
            boolean r0 = r1.equals(r0)
            r1 = 31
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01a4:
            java.lang.String r0 = "com.facebook.ads.cache.internal.network"
            boolean r0 = r1.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01af:
            java.lang.String r0 = "com.facebook.ads.internal.dispatcher.record"
            boolean r0 = r1.equals(r0)
            r1 = 36
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01bb:
            java.lang.String r0 = "com.facebook.contactlogscommon.service"
            boolean r0 = r1.equals(r0)
            r1 = 67
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01c7:
            java.lang.String r0 = "com.mapbox.geojson.constants"
            boolean r0 = r1.equals(r0)
            r1 = 84
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01d3:
            java.lang.String r0 = "com.facebook.mig.navigation"
            boolean r0 = r1.equals(r0)
            r1 = 76
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01df:
            java.lang.String r0 = "com.facebook.messaging.search.listcreator.examples.contactpicker"
            boolean r0 = r1.equals(r0)
            r1 = 73
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01eb:
            java.lang.String r0 = "com.facebook.ads.internal.api"
            boolean r0 = r1.equals(r0)
            r1 = 17
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01f7:
            java.lang.String r0 = "com.facebook.ads.internal.threadcheck"
            boolean r0 = r1.equals(r0)
            r1 = 43
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0203:
            java.lang.String r0 = "com.facebook.payments.cardio.downloadablecardio.ui"
            boolean r0 = r1.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x020e:
            java.lang.String r0 = "com.facebook.interstitial.debug"
            boolean r0 = r1.equals(r0)
            r1 = 70
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x021a:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.biometric.library"
            boolean r0 = r1.equals(r0)
            r1 = 24
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0226:
            java.lang.String r0 = "com.facebook.ads.cache.utils"
            boolean r0 = r1.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0231:
            java.lang.String r0 = "com.facebook.ads.internal.adquality"
            boolean r0 = r1.equals(r0)
            r1 = 13
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x023d:
            java.lang.String r0 = "com.facebook.dialtone.prefs.internal"
            boolean r0 = r1.equals(r0)
            r1 = 69
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0249:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.util"
            boolean r0 = r1.equals(r0)
            r1 = 15
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0255:
            java.lang.String r0 = "com.facebook.ads.cache.internal"
            boolean r0 = r1.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0260:
            java.lang.String r0 = "com.facebook.payments.cardio.realcardio"
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x026b:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.audiencenetworkimpl"
            boolean r0 = r1.equals(r0)
            r1 = 20
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0277:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.widget"
            boolean r0 = r1.equals(r0)
            r1 = 16
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0283:
            java.lang.String r0 = "com.facebook.ads.internal.view.adreporting"
            boolean r0 = r1.equals(r0)
            r1 = 47
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x028f:
            java.lang.String r0 = "com.facebook.zero.internal.prefs"
            boolean r0 = r1.equals(r0)
            r1 = 83
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x029b:
            java.lang.String r0 = "com.facebook.messaging.games.quicksilver.ads"
            boolean r0 = r1.equals(r0)
            r1 = 63
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02a7:
            java.lang.String r0 = "com.facebook.ads.internal.util.risky"
            boolean r0 = r1.equals(r0)
            r1 = 45
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02b3:
            java.lang.String r0 = "com.facebook.ads.network.signals"
            boolean r0 = r1.equals(r0)
            r1 = 61
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02bf:
            java.lang.String r0 = "com.facebook.ads.internal.shield.signals"
            boolean r0 = r1.equals(r0)
            r1 = 42
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02cb:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.biometric.model"
            boolean r0 = r1.equals(r0)
            r1 = 25
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02d7:
            java.lang.String r0 = "com.facebook.mig.playground.activity"
            boolean r0 = r1.equals(r0)
            r1 = 77
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02e3:
            java.lang.String r0 = "com.facebook.mig.favicon"
            boolean r0 = r1.equals(r0)
            r1 = 74
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02ef:
            java.lang.String r0 = "com.facebook.ads.internal.adapters"
            boolean r0 = r1.equals(r0)
            r1 = 8
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02fb:
            java.lang.String r0 = "com.facebook.ads.network.executor"
            boolean r0 = r1.equals(r0)
            r1 = 60
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0307:
            java.lang.String r0 = "com.facebook.device_id.debug"
            boolean r0 = r1.equals(r0)
            r1 = 68
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0313:
            java.lang.String r0 = "com.facebook.ads.internal.view.adbehavior"
            boolean r0 = r1.equals(r0)
            r1 = 46
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x031f:
            java.lang.String r0 = "com.facebook.messaging.internalprefs.nonwork"
            boolean r0 = r1.equals(r0)
            r1 = 72
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x032b:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.config"
            boolean r0 = r1.equals(r0)
            r1 = 22
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0337:
            java.lang.String r0 = "com.facebook.ads.internal.protocol"
            boolean r0 = r1.equals(r0)
            r1 = 39
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0343:
            java.lang.String r0 = "com.facebook.ads.internal.view.rewardedvideo"
            boolean r0 = r1.equals(r0)
            r1 = 56
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x034f:
            java.lang.String r0 = "com.facebook.ads.internal.view.templates"
            boolean r0 = r1.equals(r0)
            r1 = 57
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x035b:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.os"
            boolean r0 = r1.equals(r0)
            r1 = 14
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0367:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.model"
            boolean r0 = r1.equals(r0)
            r1 = 28
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0373:
            java.lang.String r0 = "com.facebook.ads.internal.view.hscroll"
            boolean r0 = r1.equals(r0)
            r1 = 54
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x037f:
            java.lang.String r0 = "com.facebook.ads.internal.adapters.carousel"
            boolean r0 = r1.equals(r0)
            r1 = 9
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x038b:
            java.lang.String r0 = "com.facebook.ads.internal.featureconfig"
            boolean r0 = r1.equals(r0)
            r1 = 38
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0397:
            java.lang.String r0 = "com.facebook.ads.internal.view.common"
            boolean r0 = r1.equals(r0)
            r1 = 48
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03a3:
            java.lang.String r0 = "com.facebook.ads.internal.view.video"
            boolean r0 = r1.equals(r0)
            r1 = 58
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03af:
            java.lang.String r0 = "com.facebook.ads.internal.debuglogging"
            boolean r0 = r1.equals(r0)
            r1 = 34
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03bb:
            java.lang.String r0 = "com.facebook.ads.internal.view.dynamiclayout"
            boolean r0 = r1.equals(r0)
            r1 = 52
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03c7:
            java.lang.String r0 = "com.facebook.ads.internal.view.component.interstitial.carousel"
            boolean r0 = r1.equals(r0)
            r1 = 51
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03d3:
            java.lang.String r0 = "com.mapbox.geojson.exception"
            boolean r0 = r1.equals(r0)
            r1 = 85
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03df:
            java.lang.String r0 = "com.facebook.ads.sync"
            boolean r0 = r1.equals(r0)
            r1 = 62
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03eb:
            java.lang.String r0 = "com.facebook.ads.internal.view.playables"
            boolean r0 = r1.equals(r0)
            r1 = 55
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03f7:
            java.lang.String r0 = "com.facebook.account.relogin"
            boolean r0 = r1.equals(r0)
            r1 = 65
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0403:
            java.lang.String r0 = "com.facebook.ads.internal.view.fullscreen"
            boolean r0 = r1.equals(r0)
            r1 = 53
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x040f:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.periodic"
            boolean r0 = r1.equals(r0)
            r1 = 30
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x041b:
            java.lang.String r0 = "com.facebook.ads.cache.signals"
            boolean r0 = r1.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0426:
            java.lang.String r0 = "cardio"
            return r0
        L_0x0429:
            java.lang.String r0 = "instantgamesads"
            return r0
        L_0x042c:
            java.lang.String r0 = "internalprefs"
            return r0
        L_0x042f:
            java.lang.String r0 = "mapbox"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02420Er.A06(java.lang.String):java.lang.String");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0239, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0245, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0251, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x025d, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0269, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0275, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0281, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x028d, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0299, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02a5, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02b1, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02bd, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02c9, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02d5, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02e0, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02ec, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02f8, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0304, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0310, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x031c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0327, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0333, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x033f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x034b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0357, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0363, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x036f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x037a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0386, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0392, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x039e, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x03aa, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x03b6, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03c2, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03ce, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03da, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03e6, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03f2, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0061, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0077, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0082, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008d, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0099, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a4, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b0, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00bc, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c8, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d4, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00e0, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ec, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f8, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0104, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0110, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x011c, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0128, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0133, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x013f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x014b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0157, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0163, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x016f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x017b, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0187, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0193, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x019f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01ab, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01b7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01c3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01cf, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01db, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01e7, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01f3, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01fe, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x020a, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0216, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0222, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x022e, code lost:
        if (r0 == false) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String A07(java.lang.String r1) {
        /*
            int r0 = r1.hashCode()
            switch(r0) {
                case -2135724975: goto L_0x000d;
                case -2103682411: goto L_0x0018;
                case -2041007763: goto L_0x0022;
                case -2024729957: goto L_0x002d;
                case -2000833195: goto L_0x0038;
                case -1992580159: goto L_0x0043;
                case -1965370403: goto L_0x004e;
                case -1960291365: goto L_0x0059;
                case -1958625695: goto L_0x0064;
                case -1956275897: goto L_0x006f;
                case -1912120175: goto L_0x007a;
                case -1663692505: goto L_0x0085;
                case -1653027493: goto L_0x0091;
                case -1310775569: goto L_0x009d;
                case -1245175103: goto L_0x00a8;
                case -1218810227: goto L_0x00b4;
                case -1200206245: goto L_0x00c0;
                case -1114469537: goto L_0x00cc;
                case -1060799947: goto L_0x00d8;
                case -1054442633: goto L_0x00e4;
                case -1015203603: goto L_0x00f0;
                case -981624017: goto L_0x00fc;
                case -918138275: goto L_0x0108;
                case -881847365: goto L_0x0114;
                case -864014017: goto L_0x0120;
                case -813327009: goto L_0x012c;
                case -801848751: goto L_0x0137;
                case -737557871: goto L_0x0143;
                case -704597673: goto L_0x014f;
                case -698393365: goto L_0x015b;
                case -621007013: goto L_0x0167;
                case -617110055: goto L_0x0173;
                case -584430345: goto L_0x017f;
                case -552424449: goto L_0x018b;
                case -528497807: goto L_0x0197;
                case -497243633: goto L_0x01a3;
                case -454145875: goto L_0x01af;
                case -416345989: goto L_0x01bb;
                case -415046603: goto L_0x01c7;
                case -413859679: goto L_0x01d3;
                case -266173261: goto L_0x01df;
                case -224511729: goto L_0x01eb;
                case -187524139: goto L_0x01f7;
                case -140844887: goto L_0x0202;
                case -131031991: goto L_0x020e;
                case -118163421: goto L_0x021a;
                case -106029319: goto L_0x0226;
                case 19297285: goto L_0x0232;
                case 38465447: goto L_0x023d;
                case 54513241: goto L_0x0249;
                case 54877243: goto L_0x0255;
                case 85254915: goto L_0x0261;
                case 231915667: goto L_0x026d;
                case 280203009: goto L_0x0279;
                case 325608625: goto L_0x0285;
                case 375888937: goto L_0x0291;
                case 378214423: goto L_0x029d;
                case 555260963: goto L_0x02a9;
                case 562669711: goto L_0x02b5;
                case 566385439: goto L_0x02c1;
                case 577929639: goto L_0x02cd;
                case 583470293: goto L_0x02d9;
                case 619263901: goto L_0x02e4;
                case 665517467: goto L_0x02f0;
                case 666364825: goto L_0x02fc;
                case 681503687: goto L_0x0308;
                case 699533899: goto L_0x0314;
                case 788498429: goto L_0x0320;
                case 925750749: goto L_0x032b;
                case 932368141: goto L_0x0337;
                case 1061876953: goto L_0x0343;
                case 1124218635: goto L_0x034f;
                case 1192954333: goto L_0x035b;
                case 1256359679: goto L_0x0367;
                case 1277412263: goto L_0x0373;
                case 1306004257: goto L_0x037e;
                case 1330044453: goto L_0x038a;
                case 1367662739: goto L_0x0396;
                case 1414066533: goto L_0x03a2;
                case 1610135773: goto L_0x03ae;
                case 1624978549: goto L_0x03ba;
                case 1738346345: goto L_0x03c6;
                case 1779265463: goto L_0x03d2;
                case 2076682051: goto L_0x03de;
                case 2110698055: goto L_0x03ea;
                default: goto L_0x0007;
            }
        L_0x0007:
            r1 = -1
        L_0x0008:
            switch(r1) {
                case 0: goto L_0x03f6;
                case 1: goto L_0x03f6;
                case 2: goto L_0x03f9;
                case 3: goto L_0x03fc;
                case 4: goto L_0x03fc;
                case 5: goto L_0x03fc;
                case 6: goto L_0x03fc;
                case 7: goto L_0x03fc;
                case 8: goto L_0x03fc;
                case 9: goto L_0x03fc;
                case 10: goto L_0x03fc;
                case 11: goto L_0x03fc;
                case 12: goto L_0x03fc;
                case 13: goto L_0x03fc;
                case 14: goto L_0x03fc;
                case 15: goto L_0x03fc;
                case 16: goto L_0x03fc;
                case 17: goto L_0x03fc;
                case 18: goto L_0x03fc;
                case 19: goto L_0x03fc;
                case 20: goto L_0x03fc;
                case 21: goto L_0x03fc;
                case 22: goto L_0x03fc;
                case 23: goto L_0x03fc;
                case 24: goto L_0x03fc;
                case 25: goto L_0x03fc;
                case 26: goto L_0x03fc;
                case 27: goto L_0x03fc;
                case 28: goto L_0x03fc;
                case 29: goto L_0x03fc;
                case 30: goto L_0x03fc;
                case 31: goto L_0x03fc;
                case 32: goto L_0x03fc;
                case 33: goto L_0x03fc;
                case 34: goto L_0x03fc;
                case 35: goto L_0x03fc;
                case 36: goto L_0x03fc;
                case 37: goto L_0x03fc;
                case 38: goto L_0x03fc;
                case 39: goto L_0x03fc;
                case 40: goto L_0x03fc;
                case 41: goto L_0x03fc;
                case 42: goto L_0x03fc;
                case 43: goto L_0x03fc;
                case 44: goto L_0x03fc;
                case 45: goto L_0x03fc;
                case 46: goto L_0x03fc;
                case 47: goto L_0x03fc;
                case 48: goto L_0x03fc;
                case 49: goto L_0x03fc;
                case 50: goto L_0x03fc;
                case 51: goto L_0x03fc;
                case 52: goto L_0x03fc;
                case 53: goto L_0x03fc;
                case 54: goto L_0x03fc;
                case 55: goto L_0x03fc;
                case 56: goto L_0x03fc;
                case 57: goto L_0x03fc;
                case 58: goto L_0x03fc;
                case 59: goto L_0x03fc;
                case 60: goto L_0x03fc;
                case 61: goto L_0x03fc;
                case 62: goto L_0x03fc;
                case 63: goto L_0x03fc;
                case 64: goto L_0x03fc;
                case 65: goto L_0x03fc;
                case 66: goto L_0x03fc;
                case 67: goto L_0x03ff;
                case 68: goto L_0x03ff;
                case 69: goto L_0x03ff;
                case 70: goto L_0x03ff;
                case 71: goto L_0x03ff;
                case 72: goto L_0x03ff;
                case 73: goto L_0x03ff;
                case 74: goto L_0x03ff;
                case 75: goto L_0x03ff;
                case 76: goto L_0x03ff;
                case 77: goto L_0x03ff;
                case 78: goto L_0x03ff;
                case 79: goto L_0x03ff;
                case 80: goto L_0x03ff;
                case 81: goto L_0x03ff;
                case 82: goto L_0x03ff;
                case 83: goto L_0x0402;
                case 84: goto L_0x0402;
                default: goto L_0x000b;
            }
        L_0x000b:
            r0 = 0
            return r0
        L_0x000d:
            java.lang.String r0 = "com.facebook.ads.internal.assetpreload"
            boolean r0 = r1.equals(r0)
            r1 = 18
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0018:
            java.lang.String r0 = "com.facebook.ads.internal.adapters.interstitials"
            boolean r0 = r1.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0022:
            java.lang.String r0 = "com.facebook.ads.internal.fbvalidation"
            boolean r0 = r1.equals(r0)
            r1 = 38
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x002d:
            java.lang.String r0 = "com.facebook.messaging.internalprefs.presence"
            boolean r0 = r1.equals(r0)
            r1 = 70
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0038:
            java.lang.String r0 = "com.facebook.ads.internal.dynamicloading"
            boolean r0 = r1.equals(r0)
            r1 = 34
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0043:
            java.lang.String r0 = "com.facebook.ads.internal.view.video.plugins.animations"
            boolean r0 = r1.equals(r0)
            r1 = 59
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x004e:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.interval"
            boolean r0 = r1.equals(r0)
            r1 = 22
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0059:
            java.lang.String r0 = "com.facebook.ads.internal.environment"
            boolean r0 = r1.equals(r0)
            r1 = 35
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0064:
            java.lang.String r0 = "com.facebook.messaging.settings.playground"
            boolean r0 = r1.equals(r0)
            r1 = 73
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x006f:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.logging"
            boolean r0 = r1.equals(r0)
            r1 = 24
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x007a:
            java.lang.String r0 = "com.facebook.ads.internal.view.interstitial.carousel"
            boolean r0 = r1.equals(r0)
            r1 = 55
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0085:
            java.lang.String r0 = "com.facebook.ads.internal.logging"
            boolean r0 = r1.equals(r0)
            r1 = 39
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0091:
            java.lang.String r0 = "com.facebook.base.activity.activitylike.fragment"
            boolean r0 = r1.equals(r0)
            r1 = 67
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x009d:
            java.lang.String r0 = "com.facebook.ads.funnel"
            boolean r0 = r1.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00a8:
            java.lang.String r0 = "com.facebook.ads.network.api"
            boolean r0 = r1.equals(r0)
            r1 = 62
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00b4:
            java.lang.String r0 = "com.facebook.surveyplatformdev"
            boolean r0 = r1.equals(r0)
            r1 = 81
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00c0:
            java.lang.String r0 = "com.facebook.ads.internal.context.deps"
            boolean r0 = r1.equals(r0)
            r1 = 31
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00cc:
            java.lang.String r0 = "com.facebook.mobileboost.apps.debug"
            boolean r0 = r1.equals(r0)
            r1 = 76
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00d8:
            java.lang.String r0 = "com.facebook.messaging.search.listcreator.examples.listitem"
            boolean r0 = r1.equals(r0)
            r1 = 72
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00e4:
            java.lang.String r0 = "com.facebook.ads.internal.context"
            boolean r0 = r1.equals(r0)
            r1 = 30
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00f0:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.biometric.controller"
            boolean r0 = r1.equals(r0)
            r1 = 25
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x00fc:
            java.lang.String r0 = "com.facebook.ads.internal.view.video.common"
            boolean r0 = r1.equals(r0)
            r1 = 57
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0108:
            java.lang.String r0 = "com.facebook.ads.internal.view.video.events"
            boolean r0 = r1.equals(r0)
            r1 = 58
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0114:
            java.lang.String r0 = "com.facebook.ads.internal.util.process"
            boolean r0 = r1.equals(r0)
            r1 = 50
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0120:
            java.lang.String r0 = "com.facebook.ads.internal.view.overlay"
            boolean r0 = r1.equals(r0)
            r1 = 56
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x012c:
            java.lang.String r0 = "com.facebook.cameracore.mediapipeline.dataproviders.recognitiontracking.implementation"
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0137:
            java.lang.String r0 = "com.facebook.ads.internal.events"
            boolean r0 = r1.equals(r0)
            r1 = 36
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0143:
            java.lang.String r0 = "com.facebook.ads.internal.bridge.fbsdk"
            boolean r0 = r1.equals(r0)
            r1 = 29
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x014f:
            java.lang.String r0 = "com.facebook.voltron.prefs"
            boolean r0 = r1.equals(r0)
            r1 = 82
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x015b:
            java.lang.String r0 = "com.facebook.ads.internal.util.concurrent"
            boolean r0 = r1.equals(r0)
            r1 = 48
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0167:
            java.lang.String r0 = "com.facebook.ads.internal.settings"
            boolean r0 = r1.equals(r0)
            r1 = 44
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0173:
            java.lang.String r0 = "com.facebook.ads.internal.eventstorage"
            boolean r0 = r1.equals(r0)
            r1 = 37
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x017f:
            java.lang.String r0 = "com.facebook.ads.internal.mirror"
            boolean r0 = r1.equals(r0)
            r1 = 40
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x018b:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.view.animation"
            boolean r0 = r1.equals(r0)
            r1 = 16
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0197:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.interval.buffer"
            boolean r0 = r1.equals(r0)
            r1 = 23
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01a3:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.graphics"
            boolean r0 = r1.equals(r0)
            r1 = 13
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01af:
            java.lang.String r0 = "com.facebook.ads.internal.viewability"
            boolean r0 = r1.equals(r0)
            r1 = 61
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01bb:
            java.lang.String r0 = "com.facebook.ads.internal.server"
            boolean r0 = r1.equals(r0)
            r1 = 43
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01c7:
            java.lang.String r0 = "com.facebook.ads.internal.adreportingconfig"
            boolean r0 = r1.equals(r0)
            r1 = 10
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01d3:
            java.lang.String r0 = "com.facebook.ads.internal.shield"
            boolean r0 = r1.equals(r0)
            r1 = 45
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01df:
            java.lang.String r0 = "com.facebook.ads.internal.database"
            boolean r0 = r1.equals(r0)
            r1 = 32
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01eb:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.handler"
            boolean r0 = r1.equals(r0)
            r1 = 28
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x01f7:
            java.lang.String r0 = "com.facebook.ads.cache.api"
            boolean r0 = r1.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0202:
            java.lang.String r0 = "com.facebook.mig.playground.ui"
            boolean r0 = r1.equals(r0)
            r1 = 74
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x020e:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.view.accessibility"
            boolean r0 = r1.equals(r0)
            r1 = 15
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x021a:
            java.lang.String r0 = "com.facebook.ads.internal.sdkdebugger"
            boolean r0 = r1.equals(r0)
            r1 = 41
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0226:
            java.lang.String r0 = "com.facebook.ads.network.security"
            boolean r0 = r1.equals(r0)
            r1 = 65
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0232:
            java.lang.String r0 = "com.facebook.messaging.particles.downloadableparticles"
            boolean r0 = r1.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x023d:
            java.lang.String r0 = "com.facebook.ads.internal.dto"
            boolean r0 = r1.equals(r0)
            r1 = 33
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0249:
            java.lang.String r0 = "com.facebook.ads.network.impl"
            boolean r0 = r1.equals(r0)
            r1 = 64
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0255:
            java.lang.String r0 = "com.facebook.ads.network.util"
            boolean r0 = r1.equals(r0)
            r1 = 66
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0261:
            java.lang.String r0 = "com.facebook.mobileconfig.ui"
            boolean r0 = r1.equals(r0)
            r1 = 78
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x026d:
            java.lang.String r0 = "com.facebook.messaging.search.listcreator.examples.activity"
            boolean r0 = r1.equals(r0)
            r1 = 71
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0279:
            java.lang.String r0 = "com.facebook.mig.playground.ui.ntxma"
            boolean r0 = r1.equals(r0)
            r1 = 75
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0285:
            java.lang.String r0 = "com.facebook.mobileconfig.util"
            boolean r0 = r1.equals(r0)
            r1 = 79
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0291:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.view"
            boolean r0 = r1.equals(r0)
            r1 = 14
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x029d:
            java.lang.String r0 = "com.facebook.ads.internal.view.browser"
            boolean r0 = r1.equals(r0)
            r1 = 53
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02a9:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.audiencenetworkimpl.logging"
            boolean r0 = r1.equals(r0)
            r1 = 21
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02b5:
            java.lang.String r0 = "com.facebook.ads.internal.assetpreloaddb"
            boolean r0 = r1.equals(r0)
            r1 = 19
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02c1:
            java.lang.String r0 = "com.facebook.ads.internal.util.common"
            boolean r0 = r1.equals(r0)
            r1 = 47
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02cd:
            java.lang.String r0 = "com.facebook.ads.internal.util.image"
            boolean r0 = r1.equals(r0)
            r1 = 49
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02d9:
            java.lang.String r0 = "com.facebook.ads"
            boolean r0 = r1.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02e4:
            java.lang.String r0 = "com.facebook.ads.internal.view.interstitial"
            boolean r0 = r1.equals(r0)
            r1 = 54
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02f0:
            java.lang.String r0 = "com.facebook.ads.internal.sdkevents"
            boolean r0 = r1.equals(r0)
            r1 = 42
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x02fc:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.biometric.handler"
            boolean r0 = r1.equals(r0)
            r1 = 26
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0308:
            java.lang.String r0 = "com.mapbox.geojson"
            boolean r0 = r1.equals(r0)
            r1 = 84
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0314:
            java.lang.String r0 = "com.facebook.mobileconfig.troubleshooting"
            boolean r0 = r1.equals(r0)
            r1 = 77
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0320:
            java.lang.String r0 = "com.facebook.cameracore.mediapipeline.dataproviders.movingtargettracking.implementation"
            boolean r0 = r1.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x032b:
            java.lang.String r0 = "com.facebook.common.internalprefhelpers"
            boolean r0 = r1.equals(r0)
            r1 = 68
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0337:
            java.lang.String r0 = "com.facebook.payments.p2p.debug"
            boolean r0 = r1.equals(r0)
            r1 = 80
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0343:
            java.lang.String r0 = "com.google.gson.typeadapters"
            boolean r0 = r1.equals(r0)
            r1 = 83
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x034f:
            java.lang.String r0 = "com.facebook.ads.internal.view.video.support"
            boolean r0 = r1.equals(r0)
            r1 = 60
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x035b:
            java.lang.String r0 = "com.facebook.ads.internal.view"
            boolean r0 = r1.equals(r0)
            r1 = 52
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0367:
            java.lang.String r0 = "com.facebook.fig.components.sectionheader"
            boolean r0 = r1.equals(r0)
            r1 = 69
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0373:
            java.lang.String r0 = "com.facebook.ads.cache.config"
            boolean r0 = r1.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x037e:
            java.lang.String r0 = "com.facebook.ads.internal.androidx"
            boolean r0 = r1.equals(r0)
            r1 = 11
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x038a:
            java.lang.String r0 = "com.facebook.ads.internal.util.rageshake"
            boolean r0 = r1.equals(r0)
            r1 = 51
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x0396:
            java.lang.String r0 = "com.facebook.ads.internal.addelegates"
            boolean r0 = r1.equals(r0)
            r1 = 8
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03a2:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v7.widget"
            boolean r0 = r1.equals(r0)
            r1 = 17
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03ae:
            java.lang.String r0 = "com.facebook.ads.network.configuration"
            boolean r0 = r1.equals(r0)
            r1 = 63
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03ba:
            java.lang.String r0 = "com.facebook.ads.internal.androidx.support.v4.content"
            boolean r0 = r1.equals(r0)
            r1 = 12
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03c6:
            java.lang.String r0 = "com.facebook.ads.internal.addelegates.messaging"
            boolean r0 = r1.equals(r0)
            r1 = 9
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03d2:
            java.lang.String r0 = "com.facebook.ads.internal.botdetection.signals.controller"
            boolean r0 = r1.equals(r0)
            r1 = 27
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03de:
            java.lang.String r0 = "com.facebook.ads.internal.util.activity"
            boolean r0 = r1.equals(r0)
            r1 = 46
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03ea:
            java.lang.String r0 = "com.facebook.ads.internal.attribution"
            boolean r0 = r1.equals(r0)
            r1 = 20
            if (r0 != 0) goto L_0x0008
            goto L_0x0007
        L_0x03f6:
            java.lang.String r0 = "arservicesoptional"
            return r0
        L_0x03f9:
            java.lang.String r0 = "effects"
            return r0
        L_0x03fc:
            java.lang.String r0 = "instantgamesads"
            return r0
        L_0x03ff:
            java.lang.String r0 = "internalprefs"
            return r0
        L_0x0402:
            java.lang.String r0 = "mapbox"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02420Er.A07(java.lang.String):java.lang.String");
    }
}
