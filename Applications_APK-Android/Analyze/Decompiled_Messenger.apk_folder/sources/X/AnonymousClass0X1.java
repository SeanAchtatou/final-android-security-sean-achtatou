package X;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

/* renamed from: X.0X1  reason: invalid class name */
public final class AnonymousClass0X1 implements AnonymousClass0X0 {
    public Object BzW(File file) {
        DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
        boolean z = false;
        try {
            String readUTF = dataInputStream.readUTF();
            if (!"GK_NAMES".equals(readUTF)) {
                C010708t.A0O("NamesFileSerializer", "Cannot read gatekeepers, invalid signature: %s", readUTF);
            } else {
                int readInt = dataInputStream.readInt();
                if (readInt != 1) {
                    C010708t.A0Q("NamesFileSerializer", "Cannot read gatekeepers, invalid version: %s", Integer.valueOf(readInt));
                } else {
                    String readUTF2 = dataInputStream.readUTF();
                    int readInt2 = dataInputStream.readInt();
                    ArrayList arrayList = new ArrayList(readInt2);
                    for (int i = 0; i < readInt2; i++) {
                        arrayList.add(dataInputStream.readUTF());
                    }
                    try {
                        C83853yK r0 = new C83853yK(readUTF2, arrayList);
                        AnonymousClass0ZA.A00(dataInputStream, false);
                        return r0;
                    } catch (Throwable th) {
                        th = th;
                        z = true;
                        AnonymousClass0ZA.A00(dataInputStream, !z);
                        throw th;
                    }
                }
            }
            AnonymousClass0ZA.A00(dataInputStream, true);
            return null;
        } catch (Throwable th2) {
            th = th2;
            AnonymousClass0ZA.A00(dataInputStream, !z);
            throw th;
        }
    }

    public void CNY(File file, Object obj) {
        C83853yK r8 = (C83853yK) obj;
        DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
        boolean z = true;
        try {
            dataOutputStream.writeUTF("GK_NAMES");
            dataOutputStream.writeInt(z ? 1 : 0);
            dataOutputStream.writeUTF(r8.A00);
            int size = r8.A01.size();
            dataOutputStream.writeInt(size);
            z = false;
            for (int i = 0; i < size; i++) {
                dataOutputStream.writeUTF((String) r8.A01.get(i));
            }
        } finally {
            AnonymousClass0ZA.A00(dataOutputStream, z);
        }
    }
}
