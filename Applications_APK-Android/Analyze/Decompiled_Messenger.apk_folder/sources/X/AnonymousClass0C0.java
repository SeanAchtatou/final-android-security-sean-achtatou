package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.0C0  reason: invalid class name */
public abstract class AnonymousClass0C0 implements C01870By {
    private int A00 = ((int) (System.currentTimeMillis() / 86400000));
    private long A01 = SystemClock.elapsedRealtime();
    private SharedPreferences A02;
    public final String A03;
    private final Context A04;
    private final String A05;
    private final HashMap A06 = new HashMap();
    private final boolean A07;

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0024, code lost:
        if (r4.A01 == false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void A01() {
        /*
            r6 = this;
            monitor-enter(r6)
            android.content.SharedPreferences r0 = r6.A02     // Catch:{ all -> 0x002f }
            if (r0 != 0) goto L_0x002d
            android.content.Context r5 = r6.A04     // Catch:{ all -> 0x002f }
            X.0B3 r4 = new X.0B3     // Catch:{ all -> 0x002f }
            java.lang.String r3 = "rti.mqtt.counter."
            java.lang.String r2 = r6.A05     // Catch:{ all -> 0x002f }
            java.lang.String r1 = "."
            java.lang.String r0 = r6.A03     // Catch:{ all -> 0x002f }
            java.lang.String r1 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)     // Catch:{ all -> 0x002f }
            r0 = 0
            r4.<init>(r1, r0)     // Catch:{ all -> 0x002f }
            X.0B4 r3 = X.AnonymousClass0B4.A00     // Catch:{ all -> 0x002f }
            java.lang.String r2 = r4.A00     // Catch:{ all -> 0x002f }
            boolean r0 = X.AnonymousClass0B2.A00     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0026
            boolean r1 = r4.A01     // Catch:{ all -> 0x002f }
            r0 = 1
            if (r1 != 0) goto L_0x0027
        L_0x0026:
            r0 = 0
        L_0x0027:
            android.content.SharedPreferences r0 = r3.A01(r5, r2, r0)     // Catch:{ all -> 0x002f }
            r6.A02 = r0     // Catch:{ all -> 0x002f }
        L_0x002d:
            monitor-exit(r6)
            return
        L_0x002f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0C0.A01():void");
    }

    private void A00() {
        HashMap hashMap;
        if (this.A07) {
            synchronized (this.A06) {
                hashMap = new HashMap(this.A06);
                this.A06.clear();
            }
            if (!hashMap.isEmpty()) {
                A01();
                SharedPreferences.Editor edit = this.A02.edit();
                for (Map.Entry entry : hashMap.entrySet()) {
                    edit.putLong((String) entry.getKey(), this.A02.getLong((String) entry.getKey(), 0) + ((Long) entry.getValue()).longValue());
                }
                AnonymousClass0B4.A00(edit);
                this.A01 = SystemClock.elapsedRealtime();
            }
        }
    }

    public AnonymousClass0C0(Context context, String str, String str2, boolean z) {
        this.A04 = context;
        this.A05 = str;
        this.A03 = str2;
        this.A07 = z;
    }

    public JSONObject A02(boolean z) {
        int i;
        int indexOf;
        A01();
        JSONObject jSONObject = new JSONObject();
        int currentTimeMillis = (int) (System.currentTimeMillis() / 86400000);
        Map<String, ?> all = this.A02.getAll();
        SharedPreferences.Editor edit = this.A02.edit();
        for (Map.Entry next : all.entrySet()) {
            String str = (String) next.getKey();
            if (str == null || (indexOf = str.indexOf(".")) <= 0) {
                i = 0;
            } else {
                try {
                    i = Integer.parseInt(str.substring(0, indexOf));
                } catch (NumberFormatException unused) {
                    i = 0;
                }
            }
            if (i <= currentTimeMillis && i + 3 >= currentTimeMillis) {
                if (z) {
                    jSONObject.putOpt((String) next.getKey(), (Long) next.getValue());
                } else if (i != currentTimeMillis) {
                    jSONObject.putOpt((String) next.getKey(), (Long) next.getValue());
                }
            }
            edit.remove((String) next.getKey());
        }
        AnonymousClass0B4.A00(edit);
        if (jSONObject.length() == 0) {
            return null;
        }
        if (!z) {
            return jSONObject;
        }
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("period_ms", System.currentTimeMillis() % 86400000);
        jSONObject2.put("data", jSONObject);
        return jSONObject2;
    }

    public void A03(long j, String... strArr) {
        int currentTimeMillis = (int) (System.currentTimeMillis() / 86400000);
        if (this.A00 != currentTimeMillis) {
            this.A00 = currentTimeMillis;
            A00();
        }
        String valueOf = String.valueOf(currentTimeMillis);
        StringBuilder sb = new StringBuilder();
        sb.append(valueOf);
        for (String append : strArr) {
            sb.append(".");
            sb.append(append);
        }
        String sb2 = sb.toString();
        synchronized (this.A06) {
            Long l = (Long) this.A06.get(sb2);
            if (l == null) {
                l = 0L;
            }
            this.A06.put(sb2, Long.valueOf(l.longValue() + j));
        }
        if (SystemClock.elapsedRealtime() - this.A01 > 3600000) {
            A00();
        }
    }
}
