package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0mi  reason: invalid class name and case insensitive filesystem */
public final class C11320mi implements C11340ml {
    public C28421em Ajf() {
        return new C28421em("threads.db", "9ff464f5f34d69692fd58bfc5ce406c0f46a6d74-93ce81ce28eee8c1093d8c7c354d289e961e0a3d-1bcd6e9eb62fa38b51ac5dcdbe02508f74a80905-2a0a66b83a11b654b2e307f03f5f692f79ebba56-74258ec0806a0f06ce08da84db4bea147aef32c6-14a171f66f88b11a7ee982fcb26aaf7f0e0a9acb-527149c29b604fc7b1c30388b5a704753320f147-efc9b2c77e1a69ac9c25cdf0366374674c09e316-4549b13a33bacd9d5b68d6524b322bef39abf61e-b784809f5ba6efe7074768912c7aa9f1ca33e9dd-e4dcdfa8c294779f7d8ba2d3d1df563fc3d853e4-eb2facd304027b174a690bd03089f5e5a3834b72-c282eb5c1f138b982184c8490afae07908ee3120-7fb935bec2f57c04195fcf07f13bc8c5a3bfc70e-bfca8c2dd6016bb448482485c59db360678022f4-80b77938b727e4a963cb365fc7e5a757aacc5141-3601d7152a6b75e6c333dac94e62b13fec752bc5-9f86e768b309462c3e7b0cacd44256bf656e3953-59db128999f5c4193df5f5ca9cef6f031093e417-94fdc63fdcd0d2b9658eb7204f22911099194246-");
    }

    public C22890BJj[] B5G() {
        C22890BJj bJj = new C22890BJj("properties", "9ff464f5f34d69692fd58bfc5ce406c0f46a6d74", "7c030d06bb4a41bb517bae9295fa5a1dd57fa756");
        C22890BJj bJj2 = new C22890BJj("folder_counts", "93ce81ce28eee8c1093d8c7c354d289e961e0a3d", "f3d5b0bb6a1d51d8a7708809700c666f69f183b2");
        C22890BJj bJj3 = new C22890BJj("folders", "1bcd6e9eb62fa38b51ac5dcdbe02508f74a80905", "88e57dcb0812588c094013ab1caa870be9514c1f");
        C22890BJj bJj4 = new C22890BJj("threads", "2a0a66b83a11b654b2e307f03f5f692f79ebba56", "68166c2a84c5a152b76d2572a4187cd6ef8571d6");
        C22890BJj bJj5 = new C22890BJj("threads_metadata", "74258ec0806a0f06ce08da84db4bea147aef32c6", "440ae4be74dafb6d828e72b9074c15d420d5609b");
        C22890BJj bJj6 = new C22890BJj("messages", "14a171f66f88b11a7ee982fcb26aaf7f0e0a9acb", "11998b4cd52c4b66850f750873e2ec29af462ac5");
        C22890BJj bJj7 = new C22890BJj("thread_users", "527149c29b604fc7b1c30388b5a704753320f147", "e0c21f2ab76dde66b4e37392562514cc8259bcdc");
        C22890BJj bJj8 = new C22890BJj("group_conversations", "efc9b2c77e1a69ac9c25cdf0366374674c09e316", "440ae4be74dafb6d828e72b9074c15d420d5609b");
        C22890BJj bJj9 = new C22890BJj("ranked_threads", "4549b13a33bacd9d5b68d6524b322bef39abf61e", "440ae4be74dafb6d828e72b9074c15d420d5609b");
        C22890BJj bJj10 = new C22890BJj("thread_participants", "b784809f5ba6efe7074768912c7aa9f1ca33e9dd", "4f0fe1a85c734778e2a6070e1fb17fb89034cccd");
        C22890BJj bJj11 = new C22890BJj("event_reminders", "e4dcdfa8c294779f7d8ba2d3d1df563fc3d853e4", "52a27fb2b39f868a99d615801b85c8973c115807");
        C22890BJj bJj12 = new C22890BJj(AnonymousClass24B.$const$string(76), "eb2facd304027b174a690bd03089f5e5a3834b72", "61ede168a8a3be1258ecaaa150cf1b6325ae02e2");
        return new C22890BJj[]{bJj, bJj2, bJj3, bJj4, bJj5, bJj6, bJj7, bJj8, bJj9, bJj10, bJj11, bJj12, new C22890BJj("montage_message_reactions", "c282eb5c1f138b982184c8490afae07908ee3120", "1d06c0376644e63afc78d1d90cd596a98f1335b4"), new C22890BJj("fb_events", "7fb935bec2f57c04195fcf07f13bc8c5a3bfc70e", "76ced4e5877abf6cda68c259f1a0bd9721917639"), new C22890BJj("fb_event_members", "bfca8c2dd6016bb448482485c59db360678022f4", "220f7181caf4181f137c2b0fe2a770f22a5d6a7f"), new C22890BJj("montage_directs", "80b77938b727e4a963cb365fc7e5a757aacc5141", "81e3aeaf1bb3257362feb698c79f218cfd5d6015"), new C22890BJj("montage_message_poll", "3601d7152a6b75e6c333dac94e62b13fec752bc5", "ce9dff78c3c9288f245e34094cd3057237922f98"), new C22890BJj("montage_message_poll_options", "9f86e768b309462c3e7b0cacd44256bf656e3953", "abed35df76b8d1f1350e076f2e4d833f2ca775f7"), new C22890BJj("montage_message_interactive_overlays", "59db128999f5c4193df5f5ca9cef6f031093e417", "abed35df76b8d1f1350e076f2e4d833f2ca775f7"), new C22890BJj("thread_themes", "94fdc63fdcd0d2b9658eb7204f22911099194246", "99c1c6aabba590bfcc0f2a136626083cf23b3d65")};
    }

    public BJR[] Ahc(int i) {
        switch (i) {
            case 0:
                return BJF.A00;
            case 1:
                return BJK.A00;
            case 2:
                return BJM.A00;
            case 3:
                return C30285EtO.A00;
            case 4:
                return BJZ.A00;
            case 5:
                return C30281EtK.A00;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return C30283EtM.A00;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return C22883BJc.A00;
            case 8:
                return BJN.A00;
            case Process.SIGKILL:
                return C22894BJn.A00;
            case AnonymousClass1Y3.A01 /*10*/:
                return C22892BJl.A00;
            case AnonymousClass1Y3.A02 /*11*/:
                return BJT.A00;
            case AnonymousClass1Y3.A03 /*12*/:
                return BJS.A00;
            case 13:
                return C22897BJq.A00;
            case 14:
                return BJa.A00;
            case 15:
                return BJD.A00;
            case 16:
                return C22888BJh.A00;
            case 17:
                return C22886BJf.A00;
            case Process.SIGCONT:
                return C22889BJi.A00;
            case Process.SIGSTOP:
                return BJG.A00;
            default:
                throw new IllegalArgumentException(AnonymousClass08S.A09("tableIndex=", i));
        }
    }

    public BJH[] Apr(int i) {
        switch (i) {
            case 0:
                return BJF.A01;
            case 1:
                return BJK.A01;
            case 2:
                return BJM.A01;
            case 3:
                return C30285EtO.A01;
            case 4:
                return BJZ.A01;
            case 5:
                return C30281EtK.A01;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return C30283EtM.A01;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return C22883BJc.A01;
            case 8:
                return BJN.A01;
            case Process.SIGKILL:
                return C22894BJn.A01;
            case AnonymousClass1Y3.A01 /*10*/:
                return C22892BJl.A01;
            case AnonymousClass1Y3.A02 /*11*/:
                return BJT.A01;
            case AnonymousClass1Y3.A03 /*12*/:
                return BJS.A01;
            case 13:
                return C22897BJq.A01;
            case 14:
                return BJa.A01;
            case 15:
                return BJD.A01;
            case 16:
                return C22888BJh.A01;
            case 17:
                return C22886BJf.A01;
            case Process.SIGCONT:
                return C22889BJi.A01;
            case Process.SIGSTOP:
                return BJG.A01;
            default:
                throw new IllegalArgumentException(AnonymousClass08S.A09("tableIndex=", i));
        }
    }
}
