package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.SparseIntArray;
import com.facebook.common.dextricks.DexStore;
import com.facebook.common.memory.manager.MemoryManager;
import com.facebook.inject.InjectorModule;
import java.util.ArrayList;

@InjectorModule
/* renamed from: X.1MY  reason: invalid class name */
public final class AnonymousClass1MY extends AnonymousClass0UV {
    public static final String A00 = "ImagePipelineFactory";
    private static final Object A01 = new Object();
    private static final Object A02 = new Object();
    private static final Object A03 = new Object();
    private static final Object A04 = new Object();
    private static final Object A05 = new Object();
    private static final Object A06 = new Object();
    private static final Object A07 = new Object();
    private static final Object A08 = new Object();
    private static final Object A09 = new Object();
    private static final Object A0A = new Object();
    private static final Object A0B = new Object();
    private static final Object A0C = new Object();
    private static final Object A0D = new Object();
    private static final Object A0E = new Object();
    private static final Object A0F = new Object();
    private static final Object A0G = new Object();
    private static final Object A0H = new Object();
    private static final Object A0I = new Object();
    private static final Object A0J = new Object();
    private static final Object A0K = new Object();
    private static final Object A0L = new Object();
    private static volatile AnonymousClass1OL A0M;
    private static volatile AnonymousClass1OL A0N;
    private static volatile C23251Ou A0O;
    private static volatile C23251Ou A0P;
    private static volatile C23361Pf A0Q;
    private static volatile C23361Pf A0R;
    private static volatile C22661Mi A0S;
    private static volatile C22661Mi A0T;
    private static volatile C23111Og A0U;
    private static volatile C23431Pm A0V;
    private static volatile C22791Mv A0W;
    private static volatile C22801Mw A0X;
    private static volatile C22811Mx A0Y;
    private static volatile C22831Mz A0Z;
    private static volatile C30881in A0a;
    private static volatile C22971No A0b;
    private static volatile AnonymousClass1PF A0c;
    private static volatile AnonymousClass1MZ A0d;
    private static volatile C22581Ma A0e;
    private static volatile C30641iP A0f;
    private static volatile C22611Md A0g;
    private static volatile C22761Ms A0h;
    private static volatile AnonymousClass1O1 A0i;
    private static volatile C23301Oz A0j;
    private static volatile AnonymousClass552 A0k;
    private static volatile AnonymousClass1N3 A0l;
    private static volatile AnonymousClass1N3 A0m;
    private static volatile AnonymousClass1N3 A0n;
    private static volatile AnonymousClass1N1 A0o;
    private static volatile AnonymousClass1O6 A0p;
    private static volatile C23451Po A0q;
    private static volatile AnonymousClass1N0 A0r;
    private static volatile AnonymousClass1N7 A0s;
    private static volatile C30591iK A0t;
    private static volatile AnonymousClass0VL A0u;
    private static volatile AnonymousClass0VL A0v;
    private static volatile AnonymousClass0VL A0w;
    private static volatile Boolean A0x;
    private static volatile Boolean A0y;
    private static volatile String A0z;

    public static final AnonymousClass1OL A00(AnonymousClass1XY r6) {
        if (A0M == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0M, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass1OK A003 = AnonymousClass1OK.A00(applicationInjector);
                        AnonymousClass0X5 r1 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A0S);
                        String A0g2 = A0g(applicationInjector);
                        ArrayList arrayList = new ArrayList();
                        arrayList.addAll(r1);
                        arrayList.add(new AnonymousClass1OR(A003.A01(A0g2)));
                        A0M = new AnonymousClass1OT(arrayList);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0M;
    }

    public static final AnonymousClass1OL A01(AnonymousClass1XY r5) {
        if (A0N == null) {
            synchronized (A02) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0N, r5);
                if (A002 != null) {
                    try {
                        A0N = new AnonymousClass1OR(AnonymousClass1OK.A00(r5.getApplicationInjector()).A01("profile_thumbnail_image_file"));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0N;
    }

    public static final C23251Ou A02(AnonymousClass1XY r15) {
        if (A0O == null) {
            synchronized (A03) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0O, r15);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r15.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnonymousClass1ML A004 = AnonymousClass1ML.A00(applicationInjector);
                        C30721iX A005 = C30721iX.A00(applicationInjector);
                        AnonymousClass1OL A006 = A00(applicationInjector);
                        AnonymousClass1OV A052 = AnonymousClass1OU.A05(applicationInjector);
                        C23101Of r6 = new C23101Of(A003);
                        C23211Oq A012 = A052.A01(844429225099264L);
                        long A022 = A012.A02("cache_size_limit", 62914560);
                        long A023 = A012.A02("cache_size_limit_low_space", 15728640);
                        long A024 = A012.A02("cache_size_limit_min_space", 2097152);
                        C23231Os r11 = new C23231Os(A003);
                        r11.A00 = 1;
                        r11.A09 = "image";
                        r11.A08 = r6;
                        r11.A03 = A024;
                        r11.A02 = A023;
                        r11.A01 = A022;
                        r11.A04 = A004;
                        r11.A05 = A006;
                        r11.A07 = A005;
                        r11.A06 = new C23241Ot();
                        A0O = r11.A00();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0O;
    }

    public static final C23251Ou A03(AnonymousClass1XY r8) {
        if (A0P == null) {
            synchronized (A04) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0P, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnonymousClass1ML A004 = AnonymousClass1ML.A00(applicationInjector);
                        C30721iX A005 = C30721iX.A00(applicationInjector);
                        AnonymousClass1OL A012 = A01(applicationInjector);
                        C23231Os r2 = new C23231Os(A003);
                        r2.A00 = 1;
                        r2.A09 = "image";
                        r2.A08 = new C23291Oy(A003);
                        r2.A03 = 262144;
                        r2.A02 = 4194304;
                        r2.A01 = 4194304;
                        r2.A04 = A004;
                        r2.A05 = A012;
                        r2.A07 = A005;
                        A0P = r2.A00();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0P;
    }

    public static final C23361Pf A04(AnonymousClass1XY r5) {
        if (A0Q == null) {
            synchronized (A05) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Q, r5);
                if (A002 != null) {
                    try {
                        C22581Ma A0K2 = A0K(r5.getApplicationInjector());
                        if (A0K2.A00 == null) {
                            AnonymousClass1PL r0 = A0K2.A0H;
                            A0K2.A00 = r0.A0E.Ab7(r0.A03);
                        }
                        A0Q = A0K2.A00;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Q;
    }

    public static final C23361Pf A05(AnonymousClass1XY r3) {
        if (A0R == null) {
            synchronized (A06) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0R, r3);
                if (A002 != null) {
                    try {
                        A0R = A0K(r3.getApplicationInjector()).A06();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0R;
    }

    public static final C22661Mi A06(AnonymousClass1XY r6) {
        if (A0S == null) {
            synchronized (A07) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0S, r6);
                if (A002 != null) {
                    try {
                        A0S = new C22651Mh("ImageCache", 2, AnonymousClass0VM.A00(r6.getApplicationInjector()).A02(Integer.MAX_VALUE, AnonymousClass0VS.FOREGROUND, "ImageCache"));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0S;
    }

    public static final C22661Mi A07(AnonymousClass1XY r7) {
        if (A0T == null) {
            synchronized (A08) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0T, r7);
                if (A002 != null) {
                    try {
                        A0T = new C22651Mh("ThumbnailProducer", Runtime.getRuntime().availableProcessors(), AnonymousClass0VM.A00(r7.getApplicationInjector()).A02(Integer.MAX_VALUE, AnonymousClass0VS.FOREGROUND, "ThumbnailProducer"));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0T;
    }

    public static final C23111Og A08(AnonymousClass1XY r7) {
        if (A0U == null) {
            synchronized (A09) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0U, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        C25051Yd A003 = AnonymousClass0WT.A00(applicationInjector);
                        C04490Ux.A04(applicationInjector);
                        AnonymousClass0WT.A00(applicationInjector);
                        C23111Og r3 = new AnonymousClass1PA();
                        C23111Og r2 = new C30811ig(AnonymousClass0WT.A00(applicationInjector));
                        if (A003.Aem(285434936891056L)) {
                            r3 = r2;
                        }
                        A0U = r3;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0U;
    }

    public static final C23431Pm A09(AnonymousClass1XY r3) {
        if (A0V == null) {
            synchronized (C23431Pm.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0V, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0V = new C23431Pm();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0V;
    }

    public static final C22791Mv A0A(AnonymousClass1XY r6) {
        if (A0W == null) {
            synchronized (C22791Mv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0W, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0W = new AnonymousClass1O0(AnonymousClass0WT.A00(applicationInjector), new C23021Nx(A0B(applicationInjector), A0D(applicationInjector)));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0W;
    }

    public static final C22801Mw A0B(AnonymousClass1XY r4) {
        if (A0X == null) {
            synchronized (C22801Mw.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0X, r4);
                if (A002 != null) {
                    try {
                        A0X = new C22821My(A0C(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0X;
    }

    public static final C22811Mx A0C(AnonymousClass1XY r3) {
        if (A0Y == null) {
            synchronized (C22811Mx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Y, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0Y = new C22811Mx();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Y;
    }

    public static final C22831Mz A0E(AnonymousClass1XY r5) {
        if (A0Z == null) {
            synchronized (C22831Mz.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0Z, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0Z = C30651iQ.A00(A0X(applicationInjector), A0Z(applicationInjector), A0G(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0Z;
    }

    public static final C30881in A0F(AnonymousClass1XY r3) {
        if (A0a == null) {
            synchronized (A0A) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0a, r3);
                if (A002 != null) {
                    try {
                        A0a = A0K(r3.getApplicationInjector()).A09();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0a;
    }

    public static final C22971No A0G(AnonymousClass1XY r4) {
        if (A0b == null) {
            synchronized (C22971No.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0b, r4);
                if (A002 != null) {
                    try {
                        A0b = new C22971No(A0L(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0b;
    }

    public static final AnonymousClass1PF A0H(AnonymousClass1XY r4) {
        if (A0c == null) {
            synchronized (AnonymousClass1PF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0c, r4);
                if (A002 != null) {
                    try {
                        A0c = new C30791ie(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0c;
    }

    public static final AnonymousClass1MZ A0J(AnonymousClass1XY r3) {
        if (A0d == null) {
            synchronized (AnonymousClass1MZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0d, r3);
                if (A002 != null) {
                    try {
                        A0d = A0K(r3.getApplicationInjector()).A0A();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0d;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00d4, code lost:
        if (r30.Aeo(282063387296826L, false) != false) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x02a9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r34.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02ad, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C22581Ma A0K(X.AnonymousClass1XY r39) {
        /*
            X.1Ma r0 = X.AnonymousClass1MY.A0e
            if (r0 != 0) goto L_0x02b6
            java.lang.Class<X.1Ma> r35 = X.C22581Ma.class
            monitor-enter(r35)
            X.1Ma r0 = X.AnonymousClass1MY.A0e     // Catch:{ all -> 0x02b3 }
            r1 = r39
            X.0WD r34 = X.AnonymousClass0WD.A00(r0, r1)     // Catch:{ all -> 0x02b3 }
            if (r34 == 0) goto L_0x02b1
            X.1XY r2 = r1.getApplicationInjector()     // Catch:{ all -> 0x02a9 }
            X.1Mb r33 = X.C22591Mb.A01(r2)     // Catch:{ all -> 0x02a9 }
            android.content.Context r32 = X.AnonymousClass1YA.A00(r2)     // Catch:{ all -> 0x02a9 }
            X.1YI r31 = X.AnonymousClass0WA.A00(r2)     // Catch:{ all -> 0x02a9 }
            X.1Yd r30 = X.AnonymousClass0WT.A00(r2)     // Catch:{ all -> 0x02a9 }
            X.1Mf r29 = X.C22631Mf.A00(r2)     // Catch:{ all -> 0x02a9 }
            X.1Ml r28 = X.C22691Ml.A00(r2)     // Catch:{ all -> 0x02a9 }
            X.1Ms r27 = A0N(r2)     // Catch:{ all -> 0x02a9 }
            int r0 = X.AnonymousClass1Y3.Ao2     // Catch:{ all -> 0x02a9 }
            X.0VG r3 = X.AnonymousClass0VG.A00(r0, r2)     // Catch:{ all -> 0x02a9 }
            X.1Ou r26 = A02(r2)     // Catch:{ all -> 0x02a9 }
            int r0 = X.AnonymousClass1Y3.BMu     // Catch:{ all -> 0x02a9 }
            X.0VG r25 = X.AnonymousClass0VG.A00(r0, r2)     // Catch:{ all -> 0x02a9 }
            com.facebook.common.memory.manager.MemoryManager r24 = com.facebook.common.memory.manager.MemoryManager.A00(r2)     // Catch:{ all -> 0x02a9 }
            X.1Ov r23 = X.C23261Ov.A02(r2)     // Catch:{ all -> 0x02a9 }
            X.1Mz r22 = A0D(r2)     // Catch:{ all -> 0x02a9 }
            X.1N0 r21 = A0X(r2)     // Catch:{ all -> 0x02a9 }
            X.1Ou r13 = A03(r2)     // Catch:{ all -> 0x02a9 }
            X.1Oz r12 = A0P(r2)     // Catch:{ all -> 0x02a9 }
            X.0X5 r11 = new X.0X5     // Catch:{ all -> 0x02a9 }
            int[] r0 = X.AnonymousClass0X6.A18     // Catch:{ all -> 0x02a9 }
            r11.<init>(r2, r0)     // Catch:{ all -> 0x02a9 }
            X.0X5 r10 = new X.0X5     // Catch:{ all -> 0x02a9 }
            int[] r0 = X.AnonymousClass0X6.A19     // Catch:{ all -> 0x02a9 }
            r10.<init>(r2, r0)     // Catch:{ all -> 0x02a9 }
            X.09P r20 = X.C04750Wa.A01(r2)     // Catch:{ all -> 0x02a9 }
            X.1PF r9 = A0H(r2)     // Catch:{ all -> 0x02a9 }
            java.lang.Boolean r19 = A0d(r2)     // Catch:{ all -> 0x02a9 }
            X.1P3 r8 = new X.1P3     // Catch:{ all -> 0x02a9 }
            android.app.ActivityManager r1 = X.C04490Ux.A04(r2)     // Catch:{ all -> 0x02a9 }
            X.1Yd r0 = X.AnonymousClass0WT.A00(r2)     // Catch:{ all -> 0x02a9 }
            r8.<init>(r2, r1, r0)     // Catch:{ all -> 0x02a9 }
            X.1Og r7 = A08(r2)     // Catch:{ all -> 0x02a9 }
            X.1N8 r6 = new X.1N8     // Catch:{ all -> 0x02a9 }
            r6.<init>(r2)     // Catch:{ all -> 0x02a9 }
            X.1PC r5 = new X.1PC     // Catch:{ all -> 0x02a9 }
            r5.<init>(r2)     // Catch:{ all -> 0x02a9 }
            X.1iP r18 = A0L(r2)     // Catch:{ all -> 0x02a9 }
            X.1MM r0 = new X.1MM     // Catch:{ all -> 0x02a9 }
            r0.<init>()     // Catch:{ all -> 0x02a9 }
            X.AnonymousClass1NB.A00 = r0     // Catch:{ all -> 0x02a9 }
            java.lang.Class<X.1Ma> r2 = X.C22581Ma.class
            monitor-enter(r2)     // Catch:{ all -> 0x02a9 }
            X.1Ma r1 = X.C22581Ma.A0J     // Catch:{ all -> 0x02a6 }
            r0 = 0
            if (r1 == 0) goto L_0x00a2
            r0 = 1
        L_0x00a2:
            monitor-exit(r2)     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x00b2
            java.lang.String r1 = X.AnonymousClass1MY.A00     // Catch:{ all -> 0x02a9 }
            java.lang.String r0 = "Attempting to initialize ImagePipelineFactory but it has already been initialized, most likely via Fresco.initialize(). Fresco.initialize() should not be used in applications that also inject the ImagePipeline via DI."
            X.AnonymousClass02w.A0C(r1, r0)     // Catch:{ all -> 0x02a9 }
            X.1Ma r2 = X.C22581Ma.A04()     // Catch:{ all -> 0x02a9 }
            goto L_0x02a3
        L_0x00b2:
            X.1PE r14 = new X.1PE     // Catch:{ all -> 0x02a9 }
            r14.<init>(r3)     // Catch:{ all -> 0x02a9 }
            r1 = 61
            r4 = 0
            r0 = r31
            boolean r0 = r0.AbO(r1, r4)     // Catch:{ all -> 0x02a9 }
            r17 = 1
            if (r0 != 0) goto L_0x00d6
            r0 = 282063387296826(0x100890001043a, double:1.39357829612976E-309)
            r36 = r30
            r37 = r0
            r39 = r4
            boolean r0 = r36.Aeo(r37, r39)     // Catch:{ all -> 0x02a9 }
            r15 = 0
            if (r0 == 0) goto L_0x00d7
        L_0x00d6:
            r15 = 1
        L_0x00d7:
            r0 = 282063387952195(0x10089000b0443, double:1.393578299367713E-309)
            r36 = r30
            r37 = r0
            r39 = r4
            boolean r16 = r36.Aeo(r37, r39)     // Catch:{ all -> 0x02a9 }
            X.1PG r3 = new X.1PG     // Catch:{ all -> 0x02a9 }
            r1 = r30
            r3.<init>(r1)     // Catch:{ all -> 0x02a9 }
            X.1PH r2 = new X.1PH     // Catch:{ all -> 0x02a9 }
            r2.<init>(r1)     // Catch:{ all -> 0x02a9 }
            X.1PI r1 = new X.1PI     // Catch:{ all -> 0x02a9 }
            r36 = r1
            r37 = r32
            r36.<init>(r37)     // Catch:{ all -> 0x02a9 }
            r0 = r33
            r1.A08 = r0     // Catch:{ all -> 0x02a9 }
            r1.A0K = r15     // Catch:{ all -> 0x02a9 }
            r0 = r29
            r1.A0B = r0     // Catch:{ all -> 0x02a9 }
            r0 = r28
            r1.A09 = r0     // Catch:{ all -> 0x02a9 }
            r0 = r27
            r1.A0E = r0     // Catch:{ all -> 0x02a9 }
            r1.A05 = r14     // Catch:{ all -> 0x02a9 }
            r0 = r26
            r1.A00 = r0     // Catch:{ all -> 0x02a9 }
            r0 = r24
            r1.A06 = r0     // Catch:{ all -> 0x02a9 }
            r0 = r23
            r1.A0H = r0     // Catch:{ all -> 0x02a9 }
            r0 = r22
            r1.A07 = r0     // Catch:{ all -> 0x02a9 }
            r0 = r21
            r1.A0G = r0     // Catch:{ all -> 0x02a9 }
            r1.A0F = r12     // Catch:{ all -> 0x02a9 }
            r1.A0J = r11     // Catch:{ all -> 0x02a9 }
            r1.A0I = r10     // Catch:{ all -> 0x02a9 }
            java.lang.Object r0 = r25.get()     // Catch:{ all -> 0x02a9 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x02a9 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x02a9 }
            r1.A0L = r0     // Catch:{ all -> 0x02a9 }
            r1.A01 = r13     // Catch:{ all -> 0x02a9 }
            X.C05520Zg.A02(r8)     // Catch:{ all -> 0x02a9 }
            r1.A03 = r8     // Catch:{ all -> 0x02a9 }
            X.C05520Zg.A02(r7)     // Catch:{ all -> 0x02a9 }
            r1.A04 = r7     // Catch:{ all -> 0x02a9 }
            r1.A0C = r9     // Catch:{ all -> 0x02a9 }
            r1.A02 = r5     // Catch:{ all -> 0x02a9 }
            X.1PJ r5 = r1.A0N     // Catch:{ all -> 0x02a9 }
            r0 = 282063387821121(0x1008900090441, double:1.39357829872012E-309)
            r7 = r30
            boolean r0 = r7.Aeo(r0, r4)     // Catch:{ all -> 0x02a9 }
            r5.A0J = r0     // Catch:{ all -> 0x02a9 }
            X.1PI r0 = r5.A0L     // Catch:{ all -> 0x02a9 }
            X.1PJ r10 = r0.A0N     // Catch:{ all -> 0x02a9 }
            com.facebook.prefs.shared.FbSharedPreferences r5 = r6.A00     // Catch:{ all -> 0x02a9 }
            X.1Y7 r1 = r6.A0D     // Catch:{ all -> 0x02a9 }
            boolean r9 = r5.Aep(r1, r4)     // Catch:{ all -> 0x02a9 }
            com.facebook.prefs.shared.FbSharedPreferences r5 = r6.A00     // Catch:{ all -> 0x02a9 }
            X.1Y7 r1 = r6.A0A     // Catch:{ all -> 0x02a9 }
            int r8 = r5.AqN(r1, r4)     // Catch:{ all -> 0x02a9 }
            com.facebook.prefs.shared.FbSharedPreferences r5 = r6.A00     // Catch:{ all -> 0x02a9 }
            X.1Y7 r1 = r6.A0C     // Catch:{ all -> 0x02a9 }
            r0 = 2147483647(0x7fffffff, float:NaN)
            int r7 = r5.AqN(r1, r0)     // Catch:{ all -> 0x02a9 }
            com.facebook.prefs.shared.FbSharedPreferences r5 = r6.A00     // Catch:{ all -> 0x02a9 }
            X.1Y7 r1 = r6.A0B     // Catch:{ all -> 0x02a9 }
            boolean r0 = r5.Aep(r1, r4)     // Catch:{ all -> 0x02a9 }
            r10.A0I = r9     // Catch:{ all -> 0x02a9 }
            r10.A02 = r8     // Catch:{ all -> 0x02a9 }
            r10.A01 = r7     // Catch:{ all -> 0x02a9 }
            r10.A0A = r0     // Catch:{ all -> 0x02a9 }
            X.1PI r0 = r10.A0L     // Catch:{ all -> 0x02a9 }
            X.1PJ r1 = r0.A0N     // Catch:{ all -> 0x02a9 }
            boolean r0 = r19.booleanValue()     // Catch:{ all -> 0x02a9 }
            r1.A0H = r0     // Catch:{ all -> 0x02a9 }
            X.1PI r0 = r1.A0L     // Catch:{ all -> 0x02a9 }
            X.1PJ r0 = r0.A0N     // Catch:{ all -> 0x02a9 }
            r0.A05 = r3     // Catch:{ all -> 0x02a9 }
            X.1PI r1 = r0.A0L     // Catch:{ all -> 0x02a9 }
            r0 = r18
            r1.A0D = r0     // Catch:{ all -> 0x02a9 }
            X.1PJ r3 = r1.A0N     // Catch:{ all -> 0x02a9 }
            r0 = 563538364858864(0x20089000e01f0, double:2.784249461903147E-309)
            r7 = r30
            long r7 = r7.At0(r0)     // Catch:{ all -> 0x02a9 }
            int r0 = (int) r7     // Catch:{ all -> 0x02a9 }
            r3.A00 = r0     // Catch:{ all -> 0x02a9 }
            X.1PI r0 = r3.A0L     // Catch:{ all -> 0x02a9 }
            X.1PJ r0 = r0.A0N     // Catch:{ all -> 0x02a9 }
            r0.A06 = r2     // Catch:{ all -> 0x02a9 }
            X.1PI r0 = r0.A0L     // Catch:{ all -> 0x02a9 }
            X.1PJ r2 = r0.A0N     // Catch:{ all -> 0x02a9 }
            r0 = 282063389131857(0x10089001d0451, double:1.39357830519602E-309)
            r7 = r30
            boolean r0 = r7.Aem(r0)     // Catch:{ all -> 0x02a9 }
            r2.A0E = r0     // Catch:{ all -> 0x02a9 }
            X.1PI r3 = r2.A0L     // Catch:{ all -> 0x02a9 }
            r0 = 283308927748404(0x101ab00000934, double:1.399732083606063E-309)
            boolean r0 = r7.Aem(r0)     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x01d6
            X.1PJ r1 = r3.A0N     // Catch:{ all -> 0x02a9 }
            X.3c0 r0 = new X.3c0     // Catch:{ all -> 0x02a9 }
            r0.<init>()     // Catch:{ all -> 0x02a9 }
            r1.A07 = r0     // Catch:{ all -> 0x02a9 }
        L_0x01d6:
            X.1PJ r2 = r3.A0N     // Catch:{ all -> 0x02a9 }
            r0 = 563538364989937(0x20089001001f1, double:2.784249462550733E-309)
            long r0 = r7.At0(r0)     // Catch:{ all -> 0x02a9 }
            r2.A04 = r0     // Catch:{ all -> 0x02a9 }
            X.1PJ r2 = r3.A0N     // Catch:{ all -> 0x02a9 }
            r0 = 282063388345415(0x1008900110447, double:1.39357830131048E-309)
            boolean r0 = r7.Aem(r0)     // Catch:{ all -> 0x02a9 }
            r2.A0G = r0     // Catch:{ all -> 0x02a9 }
            X.1PJ r2 = r3.A0N     // Catch:{ all -> 0x02a9 }
            r0 = 282063388935246(0x10089001a044e, double:1.39357830422463E-309)
            boolean r0 = r7.Aem(r0)     // Catch:{ all -> 0x02a9 }
            r2.A0C = r0     // Catch:{ all -> 0x02a9 }
            r1 = 61
            r0 = r31
            boolean r0 = r0.AbO(r1, r4)     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x0223
            android.content.res.Resources r0 = android.content.res.Resources.getSystem()     // Catch:{ all -> 0x02a9 }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ all -> 0x02a9 }
            int r2 = r0.widthPixels     // Catch:{ all -> 0x02a9 }
            android.content.res.Resources r0 = android.content.res.Resources.getSystem()     // Catch:{ all -> 0x02a9 }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ all -> 0x02a9 }
            int r0 = r0.heightPixels     // Catch:{ all -> 0x02a9 }
            X.1PJ r1 = r3.A0N     // Catch:{ all -> 0x02a9 }
            int r0 = java.lang.Math.max(r2, r0)     // Catch:{ all -> 0x02a9 }
            r1.A03 = r0     // Catch:{ all -> 0x02a9 }
        L_0x0223:
            X.1PJ r5 = r3.A0N     // Catch:{ all -> 0x02a9 }
            r0 = r16
            r5.A0K = r0     // Catch:{ all -> 0x02a9 }
            if (r16 == 0) goto L_0x023f
            boolean r0 = X.AnonymousClass1Q4.A03     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x023f
            com.facebook.webpsupport.WebpBitmapFactoryImpl r1 = new com.facebook.webpsupport.WebpBitmapFactoryImpl     // Catch:{ all -> 0x02a9 }
            r1.<init>()     // Catch:{ all -> 0x02a9 }
            r5.A09 = r1     // Catch:{ all -> 0x02a9 }
            X.3yd r1 = new X.3yd     // Catch:{ all -> 0x02a9 }
            r0 = r20
            r1.<init>(r0)     // Catch:{ all -> 0x02a9 }
            r5.A08 = r1     // Catch:{ all -> 0x02a9 }
        L_0x023f:
            com.facebook.prefs.shared.FbSharedPreferences r2 = r6.A00     // Catch:{ all -> 0x02a9 }
            X.1Y7 r1 = r6.A0F     // Catch:{ all -> 0x02a9 }
            boolean r0 = r2.Aep(r1, r4)     // Catch:{ all -> 0x02a9 }
            r5.A0F = r0     // Catch:{ all -> 0x02a9 }
            r1 = 37
            r0 = r31
            boolean r1 = r0.AbO(r1, r4)     // Catch:{ all -> 0x02a9 }
            X.1PJ r0 = r3.A0N     // Catch:{ all -> 0x02a9 }
            r0.A0B = r1     // Catch:{ all -> 0x02a9 }
            r0 = 282063387231289(0x1008900000439, double:1.393578295805964E-309)
            r5 = r7
            boolean r0 = r5.Aem(r0)     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x0266
            X.1PM r1 = X.AnonymousClass1PL.A0Q     // Catch:{ all -> 0x02a9 }
            r0 = 1
            r1.A00 = r0     // Catch:{ all -> 0x02a9 }
        L_0x0266:
            r1 = 27
            r0 = r31
            boolean r0 = r0.AbO(r1, r4)     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x0277
            X.6OM r0 = new X.6OM     // Catch:{ all -> 0x02a9 }
            r0.<init>()     // Catch:{ all -> 0x02a9 }
            r3.A0A = r0     // Catch:{ all -> 0x02a9 }
        L_0x0277:
            X.1PJ r2 = r3.A0N     // Catch:{ all -> 0x02a9 }
            r0 = 285434937349809(0x1039a000c16b1, double:1.41023596667389E-309)
            r4 = r5
            boolean r0 = r4.Aem(r0)     // Catch:{ all -> 0x02a9 }
            r0 = r0 ^ r17
            r2.A0D = r0     // Catch:{ all -> 0x02a9 }
            X.1PL r0 = new X.1PL     // Catch:{ all -> 0x02a9 }
            r0.<init>(r3)     // Catch:{ all -> 0x02a9 }
            X.C22581Ma.A05(r0)     // Catch:{ all -> 0x02a9 }
            X.1Ma r2 = X.C22581Ma.A04()     // Catch:{ all -> 0x02a9 }
            r0 = r21
            boolean r0 = r0 instanceof X.C30561iH     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x02a3
            r0 = r21
            X.1iH r0 = (X.C30561iH) r0     // Catch:{ all -> 0x02a9 }
            X.1PZ r1 = r2.A08()     // Catch:{ all -> 0x02a9 }
            r0.A00 = r1     // Catch:{ all -> 0x02a9 }
        L_0x02a3:
            X.AnonymousClass1MY.A0e = r2     // Catch:{ all -> 0x02a9 }
            goto L_0x02ae
        L_0x02a6:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x02a9 }
            throw r0     // Catch:{ all -> 0x02a9 }
        L_0x02a9:
            r0 = move-exception
            r34.A01()     // Catch:{ all -> 0x02b3 }
            throw r0     // Catch:{ all -> 0x02b3 }
        L_0x02ae:
            r34.A01()     // Catch:{ all -> 0x02b3 }
        L_0x02b1:
            monitor-exit(r35)     // Catch:{ all -> 0x02b3 }
            goto L_0x02b6
        L_0x02b3:
            r0 = move-exception
            monitor-exit(r35)     // Catch:{ all -> 0x02b3 }
            throw r0
        L_0x02b6:
            X.1Ma r0 = X.AnonymousClass1MY.A0e
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MY.A0K(X.1XY):X.1Ma");
    }

    public static final C30641iP A0L(AnonymousClass1XY r3) {
        if (A0f == null) {
            synchronized (C30641iP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0f, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0f = new C22961Nn();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0f;
    }

    public static final C22611Md A0M(AnonymousClass1XY r3) {
        if (A0g == null) {
            synchronized (C22611Md.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0g, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0g = new C22621Me();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0g;
    }

    public static final C22761Ms A0N(AnonymousClass1XY r3) {
        if (A0h == null) {
            synchronized (C22761Ms.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0h, r3);
                if (A002 != null) {
                    try {
                        A0h = C22771Mt.A01(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0h;
    }

    public static final AnonymousClass1O1 A0O(AnonymousClass1XY r7) {
        if (A0i == null) {
            synchronized (AnonymousClass1O1.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0i, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass0X5<AnonymousClass1O7> r3 = new AnonymousClass0X5<>(r7.getApplicationInjector(), AnonymousClass0X6.A16);
                        AnonymousClass1O2 r4 = new AnonymousClass1O2();
                        r4.A00(C30671iS.A00, new C30681iT(), new C30691iU());
                        if (!r3.isEmpty()) {
                            for (AnonymousClass1O7 r0 : r3) {
                                r4.A00(r0.ApV(), r0.AnW(), r0.ApT());
                            }
                        }
                        A0i = new AnonymousClass1O1(r4);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0i;
    }

    public static final C23301Oz A0P(AnonymousClass1XY r5) {
        C23301Oz r1;
        if (A0j == null) {
            synchronized (C23301Oz.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0j, r5);
                if (A002 != null) {
                    try {
                        C25051Yd A003 = AnonymousClass0WT.A00(r5.getApplicationInjector());
                        if (!A003.Aem(2306125072601318462L)) {
                            r1 = new C191758xQ();
                        } else {
                            r1 = new AnonymousClass1P2(new AnonymousClass1P0(A003));
                        }
                        A0j = r1;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0j;
    }

    public static final AnonymousClass552 A0Q(AnonymousClass1XY r8) {
        AnonymousClass552 r0;
        if (A0k == null) {
            synchronized (A0B) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0k, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        C25051Yd A003 = AnonymousClass0WT.A00(applicationInjector);
                        C22591Mb A012 = C22591Mb.A01(applicationInjector);
                        AnonymousClass09P A013 = C04750Wa.A01(applicationInjector);
                        C06920cI.A00(applicationInjector);
                        AnonymousClass06A A0A2 = AnonymousClass067.A0A(applicationInjector);
                        if (A003.Aem(282063387690047L)) {
                            r0 = new AnonymousClass552(A012, A013, A0A2);
                        } else {
                            r0 = null;
                        }
                        A0k = r0;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0k;
    }

    public static final AnonymousClass1N3 A0R(AnonymousClass1XY r7) {
        if (A0l == null) {
            synchronized (A0C) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0l, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A0l = new AnonymousClass1N5(A0U(applicationInjector), new AnonymousClass1N2(C22741Mq.A01(applicationInjector), AnonymousClass067.A02(), "bitmap_pool_stats_counters"));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0l;
    }

    public static final AnonymousClass1N3 A0S(AnonymousClass1XY r7) {
        if (A0m == null) {
            synchronized (A0D) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0m, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A0m = new AnonymousClass1N5(A0U(applicationInjector), new AnonymousClass1N2(C22741Mq.A01(applicationInjector), AnonymousClass067.A02(), "native_memory_chunk_pool_stats_counters"));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0m;
    }

    public static final AnonymousClass1N3 A0T(AnonymousClass1XY r7) {
        if (A0n == null) {
            synchronized (A0E) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0n, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A0n = new AnonymousClass1N5(A0U(applicationInjector), new AnonymousClass1N2(C22741Mq.A01(applicationInjector), AnonymousClass067.A02(), "common_byte_array_pool_stats_counters"));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0n;
    }

    public static final AnonymousClass1N1 A0U(AnonymousClass1XY r5) {
        if (A0o == null) {
            synchronized (AnonymousClass1N1.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0o, r5);
                if (A002 != null) {
                    try {
                        A0o = new AnonymousClass1N1(AnonymousClass0Xd.A00(r5.getApplicationInjector()), AnonymousClass067.A02());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0o;
    }

    public static final AnonymousClass1O6 A0V(AnonymousClass1XY r4) {
        if (A0p == null) {
            synchronized (AnonymousClass1O6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0p, r4);
                if (A002 != null) {
                    try {
                        A0p = new AnonymousClass1O6(new AnonymousClass1O8(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0p;
    }

    public static final C23451Po A0W(AnonymousClass1XY r3) {
        if (A0q == null) {
            synchronized (C23451Po.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0q, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0q = new C23451Po();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0q;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public static final AnonymousClass1N0 A0X(AnonymousClass1XY r14) {
        AnonymousClass1N0 r2;
        if (A0r == null) {
            synchronized (AnonymousClass1N0.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0r, r14);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r14.getApplicationInjector();
                        AnonymousClass1N3 A0R2 = A0R(applicationInjector);
                        AnonymousClass1N3 A0T2 = A0T(applicationInjector);
                        MemoryManager A003 = MemoryManager.A00(applicationInjector);
                        AnonymousClass1N3 A0S2 = A0S(applicationInjector);
                        AnonymousClass1N7 A0Y2 = A0Y(applicationInjector);
                        AnonymousClass1N8 r10 = new AnonymousClass1N8(applicationInjector);
                        C25051Yd A004 = AnonymousClass0WT.A00(applicationInjector);
                        Resources A0L2 = C04490Ux.A0L(applicationInjector);
                        C30551iG r9 = new C30551iG();
                        C05520Zg.A02(A0R2);
                        r9.A05 = A0R2;
                        C05520Zg.A02(A0T2);
                        r9.A07 = A0T2;
                        r9.A02 = A003;
                        C05520Zg.A02(A0S2);
                        r9.A06 = A0S2;
                        r9.A04 = A0Y2;
                        long min = Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
                        double d = (double) A0L2.getDisplayMetrics().widthPixels;
                        String B4F = r10.A00.B4F(r10.A04, "legacy");
                        r9.A08 = B4F;
                        double d2 = (double) min;
                        r9.A01 = (int) (r10.A00.Akk(r10.A02, 0.5d) * d2);
                        r9.A00 = (int) (r10.A00.Akk(r10.A01, 0.5d) * d * d * 4.0d);
                        r9.A09 = r10.A00.Aep(r10.A03, false);
                        if (A004.Aem(282063388017732L)) {
                            boolean Aem = A004.Aem(282063388083269L);
                            float min2 = (float) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
                            Bitmap.Config config = Bitmap.Config.ARGB_8888;
                            AnonymousClass1SJ.A00(config);
                            AnonymousClass1SJ.A00(config);
                            SparseIntArray sparseIntArray = new SparseIntArray();
                            sparseIntArray.put(DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED * AnonymousClass1SJ.A00(config), 128);
                            sparseIntArray.put(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET * AnonymousClass1SJ.A00(config), 64);
                            sparseIntArray.put(65536 * AnonymousClass1SJ.A00(config), 32);
                            sparseIntArray.put(230400 * AnonymousClass1SJ.A00(config), 32);
                            sparseIntArray.put(240000 * AnonymousClass1SJ.A00(config), 16);
                            sparseIntArray.put(345600 * AnonymousClass1SJ.A00(config), 16);
                            sparseIntArray.put(640000 * AnonymousClass1SJ.A00(config), 16);
                            sparseIntArray.put(1166400 * AnonymousClass1SJ.A00(config), 4);
                            sparseIntArray.put(2073600 * AnonymousClass1SJ.A00(config), 4);
                            AnonymousClass1N7 r1 = new AnonymousClass1N7((int) (0.25f * min2), (int) (min2 * 0.5f), sparseIntArray, -1);
                            r1.A00 = false;
                            C05520Zg.A02(r1);
                            r9.A03 = r1;
                            AnonymousClass1NA r0 = new AnonymousClass1NA(r9);
                            r2 = new C411524i(r0, r0, Aem, sparseIntArray);
                        } else {
                            boolean Aep = r10.A00.Aep(r10.A0E, true);
                            if ("legacy".equals(B4F) && !Aep) {
                                int Akk = (int) (r10.A00.Akk(r10.A09, 0.0d) * d2);
                                int Akk2 = (int) (d2 * r10.A00.Akk(r10.A06, 0.5d));
                                r10.A00.Akk(r10.A08, 0.0d);
                                r10.A00.Akk(r10.A07, 0.0d);
                                AnonymousClass1N7 r3 = new AnonymousClass1N7(Akk, Akk2, null, -1);
                                r3.A00 = r10.A00.Aep(r10.A05, false);
                                C05520Zg.A02(r3);
                                r9.A03 = r3;
                            }
                            r2 = new C30561iH(new AnonymousClass1NA(r9), new C30581iJ(A004));
                        }
                        A0r = r2;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0r;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x006a A[Catch:{ all -> 0x0079 }, LOOP:0: B:18:0x0068->B:19:0x006a, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.AnonymousClass1N7 A0Y(X.AnonymousClass1XY r7) {
        /*
            X.1N7 r0 = X.AnonymousClass1MY.A0s
            if (r0 != 0) goto L_0x0086
            java.lang.Object r6 = X.AnonymousClass1MY.A0F
            monitor-enter(r6)
            X.1N7 r0 = X.AnonymousClass1MY.A0s     // Catch:{ all -> 0x0083 }
            X.0WD r5 = X.AnonymousClass0WD.A00(r0, r7)     // Catch:{ all -> 0x0083 }
            if (r5 == 0) goto L_0x0081
            X.1XY r0 = r7.getApplicationInjector()     // Catch:{ all -> 0x0079 }
            android.view.WindowManager r2 = X.C04490Ux.A0g(r0)     // Catch:{ all -> 0x0079 }
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ all -> 0x0079 }
            int r0 = r0.availableProcessors()     // Catch:{ all -> 0x0079 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0079 }
            int r4 = r0.intValue()     // Catch:{ all -> 0x0079 }
            android.view.Display r1 = r2.getDefaultDisplay()     // Catch:{ all -> 0x0079 }
            if (r1 != 0) goto L_0x0041
            java.lang.Class r0 = r2.getClass()     // Catch:{ all -> 0x0079 }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0079 }
            java.lang.Object[] r2 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x0079 }
            java.lang.String r1 = "FbFlexByteArrayPoolParams"
            java.lang.String r0 = "Window manager passed down to Fresco has no display attached! Object of class %s"
            X.C010708t.A0Q(r1, r0, r2)     // Catch:{ all -> 0x0079 }
            goto L_0x005d
        L_0x0041:
            android.graphics.Point r0 = new android.graphics.Point     // Catch:{ all -> 0x0079 }
            r0.<init>()     // Catch:{ all -> 0x0079 }
            r1.getSize(r0)     // Catch:{ all -> 0x0079 }
            int r1 = r0.x     // Catch:{ all -> 0x0079 }
            int r0 = r0.y     // Catch:{ all -> 0x0079 }
            int r1 = r1 * r0
            r0 = 518400(0x7e900, float:7.26433E-40)
            if (r1 < r0) goto L_0x005d
            r0 = 1024000(0xfa000, float:1.43493E-39)
            r2 = 262144(0x40000, float:3.67342E-40)
            if (r1 >= r0) goto L_0x005f
            r2 = 131072(0x20000, float:1.83671E-40)
            goto L_0x005f
        L_0x005d:
            r2 = 65536(0x10000, float:9.18355E-41)
        L_0x005f:
            r1 = 4194304(0x400000, float:5.877472E-39)
            r0 = 4194304(0x400000, float:5.877472E-39)
            android.util.SparseIntArray r3 = new android.util.SparseIntArray     // Catch:{ all -> 0x0079 }
            r3.<init>()     // Catch:{ all -> 0x0079 }
        L_0x0068:
            if (r2 > r1) goto L_0x0070
            r3.put(r2, r4)     // Catch:{ all -> 0x0079 }
            int r2 = r2 << 1
            goto L_0x0068
        L_0x0070:
            X.1N7 r2 = new X.1N7     // Catch:{ all -> 0x0079 }
            int r1 = r1 * r4
            r2.<init>(r0, r1, r3, r4)     // Catch:{ all -> 0x0079 }
            X.AnonymousClass1MY.A0s = r2     // Catch:{ all -> 0x0079 }
            goto L_0x007e
        L_0x0079:
            r0 = move-exception
            r5.A01()     // Catch:{ all -> 0x0083 }
            throw r0     // Catch:{ all -> 0x0083 }
        L_0x007e:
            r5.A01()     // Catch:{ all -> 0x0083 }
        L_0x0081:
            monitor-exit(r6)     // Catch:{ all -> 0x0083 }
            goto L_0x0086
        L_0x0083:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0083 }
            throw r0
        L_0x0086:
            X.1N7 r0 = X.AnonymousClass1MY.A0s
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MY.A0Y(X.1XY):X.1N7");
    }

    public static final C30591iK A0Z(AnonymousClass1XY r6) {
        if (A0t == null) {
            synchronized (C30591iK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0t, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass1N0 A0X2 = A0X(applicationInjector);
                        AnonymousClass1N8 r0 = new AnonymousClass1N8(applicationInjector);
                        A0t = AnonymousClass1NU.A00(A0X2, r0.A00.Aep(r0.A0F, false));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0t;
    }

    public static final AnonymousClass0VL A0a(AnonymousClass1XY r6) {
        if (A0u == null) {
            synchronized (A0G) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0u, r6);
                if (A002 != null) {
                    try {
                        A0u = AnonymousClass0VM.A00(r6.getApplicationInjector()).A02(Integer.valueOf(Runtime.getRuntime().availableProcessors()).intValue(), AnonymousClass0VS.FOREGROUND, "ImageDecode");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0u;
    }

    public static final AnonymousClass0VL A0b(AnonymousClass1XY r6) {
        if (A0v == null) {
            synchronized (A0H) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0v, r6);
                if (A002 != null) {
                    try {
                        A0v = AnonymousClass0VM.A00(r6.getApplicationInjector()).A02(1, AnonymousClass0VS.FOREGROUND, "ImageOffUiThread");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0v;
    }

    public static final AnonymousClass0VL A0c(AnonymousClass1XY r6) {
        if (A0w == null) {
            synchronized (A0I) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0w, r6);
                if (A002 != null) {
                    try {
                        A0w = AnonymousClass0VM.A00(r6.getApplicationInjector()).A02(3, AnonymousClass0VS.FOREGROUND, "ImageTransform");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0w;
    }

    public static final Boolean A0d(AnonymousClass1XY r3) {
        if (A0x == null) {
            synchronized (A0J) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0x, r3);
                if (A002 != null) {
                    try {
                        AnonymousClass0WT.A00(r3.getApplicationInjector());
                        A0x = false;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0x;
    }

    public static final Boolean A0e(AnonymousClass1XY r5) {
        if (A0y == null) {
            synchronized (A0K) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0y, r5);
                if (A002 != null) {
                    try {
                        A0y = Boolean.valueOf(AnonymousClass0WA.A00(r5.getApplicationInjector()).AbO(61, false));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0y;
    }

    public static final String A0g(AnonymousClass1XY r3) {
        if (A0z == null) {
            synchronized (A0L) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0z, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A0z = "image_file";
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0z;
    }

    public static final C22831Mz A0D(AnonymousClass1XY r0) {
        return A0E(r0);
    }

    public static final AnonymousClass1MZ A0I(AnonymousClass1XY r0) {
        return A0J(r0);
    }

    public static final Integer A0f() {
        return Integer.valueOf(Runtime.getRuntime().availableProcessors());
    }
}
