package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.15T  reason: invalid class name */
public abstract class AnonymousClass15T {
    public final String A00;

    public Map A01() {
        AnonymousClass09P r2;
        String str;
        AnonymousClass150 r5 = (AnonymousClass150) this;
        Set<AnonymousClass1Y7> Arq = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r5.A00)).Arq(r5.A01);
        HashMap hashMap = new HashMap();
        for (AnonymousClass1Y7 A002 : Arq) {
            JSONObject A003 = AnonymousClass150.A00(r5, A002);
            if (A003.length() <= 0) {
                r2 = (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r5.A00);
                str = "Found an invalid JSON for plugin=";
            } else {
                String optString = A003.optString("path", BuildConfig.FLAVOR);
                if (optString.isEmpty()) {
                    r2 = (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r5.A00);
                    str = "Found an JSON without path param for plugin=";
                } else {
                    hashMap.put(optString, A003);
                }
            }
            r2.CGS("FBCaskMetadataStore", AnonymousClass08S.A0J(str, r5.A00));
        }
        return hashMap;
    }

    public JSONObject A02(String str) {
        AnonymousClass150 r2 = (AnonymousClass150) this;
        return AnonymousClass150.A00(r2, (AnonymousClass1Y7) r2.A01.A09(String.valueOf(str.hashCode())));
    }

    public void A03(String str) {
        AnonymousClass150 r4 = (AnonymousClass150) this;
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r4.A00)).edit();
        edit.C24((AnonymousClass1Y7) r4.A01.A09(String.valueOf(str.hashCode())));
        edit.commit();
    }

    public void A04(String str, JSONObject jSONObject) {
        AnonymousClass150 r4 = (AnonymousClass150) this;
        try {
            jSONObject.put("path", str);
        } catch (JSONException unused) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r4.A00)).CGS("FBCaskMetadataStore", AnonymousClass08S.A0S("Failed to add path for plugin=", r4.A00, "; path=", str));
        }
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r4.A00)).edit();
        edit.BzC((AnonymousClass1Y7) r4.A01.A09(String.valueOf(str.hashCode())), jSONObject.toString());
        edit.commit();
    }

    public AnonymousClass15T(String str) {
        this.A00 = str;
    }
}
