package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;

/* renamed from: X.1FD  reason: invalid class name */
public enum AnonymousClass1FD implements CallerContextable {
    AUTOMATIC_REFRESH,
    REFRESH_AFTER_RESUME,
    EXPLICIT_USER_REFRESH,
    RECEIPTS_REFRESH,
    INBOX_FILTER_CHANGE;
    
    public static final String __redex_internal_original_name = "com.facebook.orca.threadlist.common.RefreshType";

    public C32971md A00(C10700ki r4, String str) {
        String str2;
        String str3;
        if (this instanceof AnonymousClass1ED) {
            return C32971md.A00(false, true, true, r4, CallerContext.A0C(str, "messages", "thread_list_refresh_filter"));
        }
        if (this instanceof C21291Fz) {
            return C32971md.A00(false, false, true, r4, CallerContext.A0C(str, "messages", "thread_list_refresh_receipts"));
        }
        if (this instanceof C21281Fy) {
            return C32971md.A00(true, true, false, r4, CallerContext.A0C(str, "messages", "thread_list_refresh_user"));
        }
        if (!(this instanceof AnonymousClass1FH)) {
            str2 = "messages";
            str3 = "thread_list_refresh_auto";
        } else {
            str2 = "messages";
            str3 = "thread_list_refresh_after_resume";
        }
        return C32971md.A00(false, false, false, r4, CallerContext.A0C(str, str2, str3));
    }
}
