package X;

import com.facebook.graphservice.tree.TreeJNI;

/* renamed from: X.1cL  reason: invalid class name and case insensitive filesystem */
public class C26911cL extends TreeJNI {
    public static final int A01 = -2073950043;
    public static final Object A02 = new Object();
    public final Object[] A00;

    public final int getFieldCacheIndex(int i) {
        Object[] objArr = this.A00;
        if (objArr == null || i != A01) {
            return super.getFieldCacheIndex(i);
        }
        return objArr.length - 1;
    }

    public String getTypeName() {
        int fieldCacheIndex;
        if (this.A00 == null || (fieldCacheIndex = getFieldCacheIndex(A01)) < 0) {
            return super.getTypeName();
        }
        Object obj = this.A00[fieldCacheIndex];
        if (obj == null) {
            obj = super.getTypeName();
            this.A00[fieldCacheIndex] = obj;
        }
        if (obj != A02) {
            return (String) obj;
        }
        return null;
    }

    public C26911cL(int i, int[] iArr) {
        super(i, iArr);
        Object[] objArr;
        int[] iArr2 = this.mSetFields;
        if (iArr2 == null) {
            objArr = null;
        } else {
            objArr = new Object[(iArr2.length + 1)];
        }
        this.A00 = objArr;
    }
}
