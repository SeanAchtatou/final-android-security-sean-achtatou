package X;

import com.fasterxml.jackson.databind.JsonDeserializer;

/* renamed from: X.1c6  reason: invalid class name */
public abstract class AnonymousClass1c6 {
    public static final AnonymousClass1c7[] NO_DESERIALIZERS = new AnonymousClass1c7[0];

    public abstract JsonDeserializer createArrayDeserializer(C26791c3 r1, BMA bma, C10120ja r3);

    public abstract JsonDeserializer createBeanDeserializer(C26791c3 r1, C10030jR r2, C10120ja r3);

    public abstract JsonDeserializer createBuilderBasedDeserializer(C26791c3 r1, C10030jR r2, C10120ja r3, Class cls);

    public abstract JsonDeserializer createCollectionDeserializer(C26791c3 r1, C28331ed r2, C10120ja r3);

    public abstract JsonDeserializer createCollectionLikeDeserializer(C26791c3 r1, AnonymousClass1CA r2, C10120ja r3);

    public abstract JsonDeserializer createEnumDeserializer(C26791c3 r1, C10030jR r2, C10120ja r3);

    public abstract C422628y createKeyDeserializer(C26791c3 r1, C10030jR r2);

    public abstract JsonDeserializer createMapDeserializer(C26791c3 r1, C180610a r2, C10120ja r3);

    public abstract JsonDeserializer createMapLikeDeserializer(C26791c3 r1, C21991Jm r2, C10120ja r3);

    public abstract JsonDeserializer createTreeDeserializer(C10490kF r1, C10030jR r2, C10120ja r3);

    public abstract C64433Bp findTypeDeserializer(C10490kF r1, C10030jR r2);

    public abstract C10030jR mapAbstractType(C10490kF r1, C10030jR r2);

    public abstract AnonymousClass1c6 withAdditionalDeserializers(AnonymousClass1c7 r1);
}
