package X;

/* renamed from: X.1Hc  reason: invalid class name and case insensitive filesystem */
public enum C21471Hc {
    NORMAL("n"),
    VIDEO("v"),
    PHOTO("p"),
    LIGHT_MEDIA("l");
    
    public final String serializedValue;

    private C21471Hc(String str) {
        this.serializedValue = str;
    }
}
