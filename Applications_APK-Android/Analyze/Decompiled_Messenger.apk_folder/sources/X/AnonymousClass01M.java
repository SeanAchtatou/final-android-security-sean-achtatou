package X;

/* renamed from: X.01M  reason: invalid class name */
public final class AnonymousClass01M {
    private static final int[] A03 = {AnonymousClass1Y3.A2G, AnonymousClass1Y3.AaT, AnonymousClass1Y3.B7e};
    public final long A00;
    public final long A01;
    public final long A02;

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r2 == null) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00() {
        /*
            r3 = 0
            r2 = 0
            java.lang.String r1 = "/proc/zoneinfo"
            int r0 = android.system.OsConstants.O_RDONLY     // Catch:{ ErrnoException | NullPointerException -> 0x001c, all -> 0x000f }
            java.io.FileDescriptor r2 = android.system.Os.open(r1, r0, r3)     // Catch:{ ErrnoException | NullPointerException -> 0x001c, all -> 0x000f }
            boolean r3 = r2.valid()     // Catch:{ ErrnoException | NullPointerException -> 0x001c, all -> 0x000f }
            goto L_0x001e
        L_0x000f:
            r1 = move-exception
            if (r2 == 0) goto L_0x001b
            boolean r0 = r2.valid()
            if (r0 == 0) goto L_0x001b
            android.system.Os.close(r2)     // Catch:{ ErrnoException -> 0x001b }
        L_0x001b:
            throw r1
        L_0x001c:
            if (r2 == 0) goto L_0x0027
        L_0x001e:
            boolean r0 = r2.valid()
            if (r0 == 0) goto L_0x0027
            android.system.Os.close(r2)     // Catch:{ ErrnoException -> 0x0027 }
        L_0x0027:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01M.A00():boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0054 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass01M() {
        /*
            r24 = this;
            r7 = r24
            java.lang.String r5 = "/proc/zoneinfo"
            r24.<init>()
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0014
            boolean r0 = A00()
            if (r0 != 0) goto L_0x0014
            return
        L_0x0014:
            r9 = 0
            r8 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0039 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0039 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0039 }
            r3.<init>()     // Catch:{ IOException -> 0x0039 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r0]     // Catch:{ IOException -> 0x0039 }
            r2 = 0
        L_0x0025:
            int r2 = r4.read(r1)     // Catch:{ IOException -> 0x003c }
            r0 = -1
            if (r2 != r0) goto L_0x0035
            byte[] r9 = r3.toByteArray()     // Catch:{ IOException -> 0x003c }
            int r2 = r9.length     // Catch:{ IOException -> 0x003c }
            r4.close()     // Catch:{ IOException -> 0x003c }
            goto L_0x0044
        L_0x0035:
            r3.write(r1, r8, r2)     // Catch:{ IOException -> 0x003c }
            goto L_0x0025
        L_0x0039:
            r1 = move-exception
            r2 = 0
            goto L_0x003d
        L_0x003c:
            r1 = move-exception
        L_0x003d:
            java.lang.String r0 = "MemProcWatermarkReader"
            android.util.Log.e(r0, r5, r1)
            r6 = r2
            goto L_0x0048
        L_0x0044:
            r6 = 0
            if (r2 > 0) goto L_0x00a5
        L_0x0047:
            r6 = r2
        L_0x0048:
            r15 = 1
            r4 = 0
            r2 = 0
            r13 = 0
            r1 = 1
            r18 = 0
        L_0x0052:
            if (r9 == 0) goto L_0x00ae
            if (r1 >= r6) goto L_0x00ae
            r10 = r18
        L_0x0058:
            if (r10 >= r6) goto L_0x0065
            byte r1 = r9[r10]
            r0 = 10
            if (r1 == r0) goto L_0x0065
            if (r1 == 0) goto L_0x0065
            int r10 = r10 + 1
            goto L_0x0058
        L_0x0065:
            int r1 = r10 + 1
            int[] r11 = X.AnonymousClass01M.A03
            int r0 = r11.length
            java.lang.String[] r10 = new java.lang.String[r0]
            long[] r0 = new long[r0]
            r23 = 0
            X.00X r16 = X.AnonymousClass00V.A00
            if (r16 == 0) goto L_0x0081
            r19 = r1
            r20 = r11
            r21 = r10
            r22 = r0
            r17 = r9
            r16.BwU(r17, r18, r19, r20, r21, r22, r23)
        L_0x0081:
            r10 = r10[r8]
            r11 = r0[r15]
            java.lang.String r0 = "min"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x0091
            long r4 = r4 + r11
        L_0x008e:
            r18 = r1
            goto L_0x0052
        L_0x0091:
            java.lang.String r0 = "low"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x009b
            long r2 = r2 + r11
            goto L_0x008e
        L_0x009b:
            java.lang.String r0 = "high"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x008e
            long r13 = r13 + r11
            goto L_0x008e
        L_0x00a5:
            if (r6 >= r2) goto L_0x0047
            byte r0 = r9[r6]     // Catch:{ IOException -> 0x003c }
            if (r0 == 0) goto L_0x0048
            int r6 = r6 + 1
            goto L_0x00a5
        L_0x00ae:
            r0 = 4096(0x1000, double:2.0237E-320)
            long r4 = r4 * r0
            r7.A02 = r4
            long r2 = r2 * r0
            r7.A01 = r2
            long r0 = r0 * r13
            r7.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01M.<init>():void");
    }
}
