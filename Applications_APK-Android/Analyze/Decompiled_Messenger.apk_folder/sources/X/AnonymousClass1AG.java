package X;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* renamed from: X.1AG  reason: invalid class name */
public final class AnonymousClass1AG implements C15590vX {
    public static final boolean A07 = AnonymousClass07c.isDebugModeEnabled;
    private int A00 = -1;
    private int A01 = -1;
    private int A02 = Integer.MAX_VALUE;
    public final SparseArray A03 = new SparseArray();
    public final AnonymousClass1AE A04;
    public final String A05;
    private final C15590vX A06;

    private void A01(int i, List list) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.A04.A02(this.A05, i + i2, (C21681Ih) list.get(i2), Thread.currentThread().getName());
        }
    }

    private void A02(int i, List list) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.A04.A03(this.A05, i + i2, (C21681Ih) list.get(i2), Thread.currentThread().getName());
        }
    }

    private static List A00(int i, int i2, SparseArray sparseArray) {
        ArrayList arrayList = new ArrayList(i2);
        int i3 = i;
        while (i3 < i + i2) {
            C21681Ih r0 = (C21681Ih) sparseArray.get(i3);
            if (r0 != null) {
                arrayList.add(r0);
                i3++;
            } else {
                throw new IllegalStateException(String.format(Locale.US, "Index %d does not have a corresponding renderInfo", Integer.valueOf(i3)));
            }
        }
        return arrayList;
    }

    public void A03() {
        int i = this.A02;
        if (i != Integer.MAX_VALUE) {
            if (i == 1) {
                List A002 = A00(this.A01, this.A00, this.A03);
                int i2 = this.A00;
                if (i2 > 1) {
                    this.A06.BDC(this.A01, i2, A002);
                    if (A07) {
                        A01(this.A01, A002);
                    }
                } else {
                    C15590vX r2 = this.A06;
                    int i3 = this.A01;
                    r2.BD9(i3, (C21681Ih) this.A03.get(i3));
                    if (A07) {
                        AnonymousClass1AE r4 = this.A04;
                        String str = this.A05;
                        int i4 = this.A01;
                        r4.A02(str, i4, (C21681Ih) this.A03.get(i4), Thread.currentThread().getName());
                    }
                }
            } else if (i == 2) {
                List A003 = A00(this.A01, this.A00, this.A03);
                int i5 = this.A00;
                if (i5 > 1) {
                    this.A06.CKm(this.A01, i5, A003);
                    if (A07) {
                        A02(this.A01, A003);
                    }
                } else {
                    C15590vX r22 = this.A06;
                    int i6 = this.A01;
                    r22.CK6(i6, (C21681Ih) this.A03.get(i6));
                    if (A07) {
                        AnonymousClass1AE r42 = this.A04;
                        String str2 = this.A05;
                        int i7 = this.A01;
                        r42.A03(str2, i7, (C21681Ih) this.A03.get(i7), Thread.currentThread().getName());
                    }
                }
            } else if (i == 3) {
                int i8 = this.A00;
                if (i8 > 1) {
                    this.A06.AWu(this.A01, i8);
                    if (A07) {
                        int i9 = this.A01;
                        int i10 = this.A00;
                        for (int i11 = 0; i11 < i10; i11++) {
                            this.A04.A04(this.A05, i9 + i11, Thread.currentThread().getName());
                        }
                    }
                } else {
                    this.A06.AWp(this.A01);
                    if (A07) {
                        this.A04.A04(this.A05, this.A01, Thread.currentThread().getName());
                    }
                }
            }
            this.A02 = Integer.MAX_VALUE;
            this.A03.clear();
        }
    }

    public void AS5(AnonymousClass20H r2) {
        this.A06.AS5(r2);
    }

    public void AWp(int i) {
        int i2;
        if (this.A02 != 3 || (i2 = this.A01) < i || i2 > i + 1) {
            A03();
            this.A01 = i;
            this.A00 = 1;
            this.A02 = 3;
            return;
        }
        this.A00++;
        this.A01 = i;
    }

    public void BD9(int i, C21681Ih r6) {
        int i2;
        int i3;
        int i4;
        if (this.A02 != 1 || i < (i2 = this.A01) || i > (i4 = i2 + (i3 = this.A00)) || i < i4) {
            A03();
            this.A01 = i;
            this.A00 = 1;
            this.A02 = 1;
        } else {
            this.A00 = i3 + 1;
            this.A01 = Math.min(i, i2);
        }
        this.A03.put(i, r6);
    }

    public void BM3(boolean z, C20841Ea r3) {
        this.A06.BM3(z, r3);
    }

    public void C3C(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        this.A06.C3C(i, i2);
        if (A07 && this.A03.size() != 0) {
            AnonymousClass1AE r1 = this.A04;
            String str = this.A05;
            C21681Ih r7 = (C21681Ih) this.A03.get(i);
            String name = Thread.currentThread().getName();
            for (AnonymousClass1AE r0 : r1.A00) {
                for (AnonymousClass1AE A002 : r0.A00) {
                    A002.A00(str, i3, i4, r7, name);
                }
            }
        }
    }

    public boolean CIZ() {
        return this.A06.CIZ();
    }

    public void CK6(int i, C21681Ih r6) {
        int i2;
        int i3;
        int i4;
        if (this.A02 != 2 || i > (i3 = (i2 = this.A01) + this.A00) || (i4 = i + 1) < i2) {
            A03();
            this.A01 = i;
            this.A00 = 1;
            this.A02 = 2;
        } else {
            int min = Math.min(i, i2);
            this.A01 = min;
            this.A00 = Math.max(i3, i4) - min;
        }
        this.A03.put(i, r6);
    }

    public AnonymousClass1AG(C15590vX r2, AnonymousClass1AE r3, String str) {
        this.A06 = r2;
        this.A04 = r3;
        this.A05 = str;
    }

    public void AWu(int i, int i2) {
        A03();
        this.A06.AWu(i, i2);
    }

    public void BDC(int i, int i2, List list) {
        A03();
        this.A06.BDC(i, i2, list);
        if (A07) {
            A01(i, list);
        }
    }

    public void BLF(int i, int i2) {
        A03();
        this.A06.BLF(i, i2);
        if (A07) {
            AnonymousClass1AE r1 = this.A04;
            String str = this.A05;
            String name = Thread.currentThread().getName();
            for (AnonymousClass1AE r0 : r1.A00) {
                for (AnonymousClass1AE A012 : r0.A00) {
                    A012.A01(str, i, i2, name);
                }
            }
        }
    }

    public void CKm(int i, int i2, List list) {
        A03();
        this.A06.CKm(i, i2, list);
        if (A07) {
            A02(i, list);
        }
    }
}
