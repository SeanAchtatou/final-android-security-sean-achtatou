package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem;

/* renamed from: X.1I5  reason: invalid class name */
public final class AnonymousClass1I5 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxUnitMontageAndActiveNowItem(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxUnitMontageAndActiveNowItem[i];
    }
}
