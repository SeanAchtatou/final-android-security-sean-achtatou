package X;

import android.content.Context;
import java.util.UUID;

/* renamed from: X.1gn  reason: invalid class name and case insensitive filesystem */
public final class C29671gn implements C06430bU {
    public AnonymousClass962 A00;
    public C06400bR A01;
    public AnonymousClass23I A02;
    public boolean A03;
    private String A04;
    public final C06260bD A05;
    private final Context A06;

    private synchronized void A00() {
        if (!this.A03) {
            throw new IllegalStateException("SessionDelegate should have called bootstrapIfNeeded");
        }
    }

    public synchronized AnonymousClass962 A03(C06400bR r4) {
        AnonymousClass23I r0;
        synchronized (this) {
            if (!this.A03) {
                this.A01 = r4;
                C06260bD r1 = this.A05;
                synchronized (r1) {
                    if (!r1.A01) {
                        r1.A00 = r1.A01();
                        r1.A01 = true;
                    }
                    r0 = r1.A00;
                }
                this.A02 = r0;
                this.A05.A02.registerObserver(this);
                A02(true);
                this.A03 = true;
            }
        }
        return this.A00;
    }

    private void A02(boolean z) {
        if (z || this.A04 == null) {
            this.A04 = UUID.randomUUID().toString();
        }
        this.A00 = new AnonymousClass962(this.A02, this.A04);
    }

    public C29671gn(C06260bD r1, Context context) {
        this.A05 = r1;
        this.A06 = context;
    }

    private void A01(boolean z) {
        AnonymousClass15B r0;
        AnonymousClass15B r02;
        AnonymousClass966 r03;
        A02(z);
        synchronized (this) {
            C06400bR r1 = this.A01;
            synchronized (r1) {
                r0 = r1.A00;
            }
            AnonymousClass962 r2 = this.A00;
            if (r0 != null) {
                AnonymousClass15K r12 = r0.A04;
                AnonymousClass15K.A03(r12);
                r12.sendMessage(r12.obtainMessage(3, r2));
            }
            C06400bR r13 = this.A01;
            synchronized (r13) {
                r02 = r13.A01;
            }
            AnonymousClass962 r22 = this.A00;
            if (r02 != null) {
                AnonymousClass15K r14 = r02.A04;
                AnonymousClass15K.A03(r14);
                r14.sendMessage(r14.obtainMessage(3, r22));
            }
            C06400bR r15 = this.A01;
            synchronized (r15) {
                r03 = r15.A02;
            }
            AnonymousClass962 r23 = this.A00;
            if (r03 != null) {
                AnonymousClass965 r16 = r03.A08;
                AnonymousClass965.A05(r16);
                r16.sendMessage(r16.obtainMessage(3, r23));
            }
        }
    }

    public void CLP(AnonymousClass23I r2) {
        A00();
        this.A02 = r2;
        A01(true);
    }

    public void CLQ() {
        String str;
        String str2;
        A00();
        AnonymousClass23I r3 = this.A02;
        AnonymousClass15K r2 = this.A01.A02().A04;
        if (r3 != null) {
            str = r3.A02;
        } else {
            str = null;
        }
        AnonymousClass15K.A03(r2);
        r2.sendMessage(r2.obtainMessage(4, str));
        AnonymousClass15K r22 = this.A01.A03().A04;
        if (r3 != null) {
            str2 = r3.A02;
        } else {
            str2 = null;
        }
        AnonymousClass15K.A03(r22);
        r22.sendMessage(r22.obtainMessage(4, str2));
        this.A02 = null;
        A01(true);
    }

    public void onBackground() {
        A00();
        boolean z = false;
        if (AnonymousClass08X.A00(this.A06, "removeBgSessionId", 0) != 1) {
            z = true;
        }
        A01(z);
    }

    public void onForeground() {
        A00();
        A01(true);
    }
}
