package X;

import com.facebook.messenger.app.MessengerApplicationImpl;

/* renamed from: X.1bx  reason: invalid class name and case insensitive filesystem */
public final class C26751bx implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messenger.app.MessengerApplicationImpl$4";
    public final /* synthetic */ MessengerApplicationImpl A00;

    public C26751bx(MessengerApplicationImpl messengerApplicationImpl) {
        this.A00 = messengerApplicationImpl;
    }

    public void run() {
        AnonymousClass08X.A04(this.A00.A02, "android_messenger_enable_lean_crash_reporting", ((AnonymousClass1YI) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AcD, this.A00.A04)).Ab9(AnonymousClass1Y3.A1Y).getDbValue());
    }
}
