package X;

import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;
import java.util.zip.DataFormatException;
import javax.net.ssl.SSLException;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0CE  reason: invalid class name */
public final class AnonymousClass0CE extends Enum {
    private static final /* synthetic */ AnonymousClass0CE[] A00;
    public static final AnonymousClass0CE A01;
    public static final AnonymousClass0CE A02;
    public static final AnonymousClass0CE A03;
    public static final AnonymousClass0CE A04;
    public static final AnonymousClass0CE A05;
    public static final AnonymousClass0CE A06;
    public static final AnonymousClass0CE A07;
    public static final AnonymousClass0CE A08;
    public static final AnonymousClass0CE A09;
    public static final AnonymousClass0CE A0A;
    public static final AnonymousClass0CE A0B;
    public static final AnonymousClass0CE A0C;
    public static final AnonymousClass0CE A0D;
    public static final AnonymousClass0CE A0E;
    public static final AnonymousClass0CE A0F;
    public static final AnonymousClass0CE A0G;
    public static final AnonymousClass0CE A0H;
    public static final AnonymousClass0CE A0I;
    public static final AnonymousClass0CE A0J;
    public static final AnonymousClass0CE A0K;
    public static final AnonymousClass0CE A0L;
    public static final AnonymousClass0CE A0M;
    public static final AnonymousClass0CE A0N;
    public static final AnonymousClass0CE A0O;
    public static final AnonymousClass0CE A0P;
    public static final AnonymousClass0CE A0Q;
    public static final AnonymousClass0CE A0R;
    public static final AnonymousClass0CE A0S;

    static {
        AnonymousClass0CE r0 = new AnonymousClass0CE("SERVICE_DESTROY", 0);
        A0K = r0;
        AnonymousClass0CE r02 = new AnonymousClass0CE("SERVICE_STOP", 1);
        A0L = r02;
        AnonymousClass0CE r03 = new AnonymousClass0CE("KICK_SHOULD_NOT_CONNECT", 2);
        A07 = r03;
        AnonymousClass0CE r04 = new AnonymousClass0CE("KICK_CONFIG_CHANGED", 3);
        A06 = r04;
        AnonymousClass0CE r05 = new AnonymousClass0CE("KEEPALIVE_SHOULD_NOT_CONNECT", 4);
        A05 = r05;
        AnonymousClass0CE r06 = new AnonymousClass0CE("EXPIRE_CONNECTION", 5);
        A04 = r06;
        AnonymousClass0CE r07 = new AnonymousClass0CE("OPERATION_TIMEOUT", 6);
        A09 = r07;
        AnonymousClass0CE r08 = new AnonymousClass0CE("PING_UNRECEIVED", 7);
        A0A = r08;
        AnonymousClass0CE r09 = new AnonymousClass0CE("READ_TIMEOUT", 8);
        A0I = r09;
        AnonymousClass0CE r010 = new AnonymousClass0CE("READ_EOF", 9);
        A0C = r010;
        AnonymousClass0CE r011 = new AnonymousClass0CE("READ_SOCKET", 10);
        A0G = r011;
        AnonymousClass0CE r13 = new AnonymousClass0CE("READ_SSL", 11);
        A0H = r13;
        AnonymousClass0CE r012 = new AnonymousClass0CE("READ_IO", 12);
        A0F = r012;
        AnonymousClass0CE r013 = new AnonymousClass0CE("READ_FORMAT", 13);
        A0E = r013;
        AnonymousClass0CE r15 = new AnonymousClass0CE("READ_FAILURE_UNCLASSIFIED", 14);
        A0D = r15;
        AnonymousClass0CE r14 = new AnonymousClass0CE("WRITE_TIMEOUT", 15);
        A0S = r14;
        AnonymousClass0CE r12 = new AnonymousClass0CE("WRITE_EOF", 16);
        A0N = r12;
        AnonymousClass0CE r11 = new AnonymousClass0CE("WRITE_SOCKET", 17);
        A0Q = r11;
        AnonymousClass0CE r10 = new AnonymousClass0CE("WRITE_SSL", 18);
        A0R = r10;
        AnonymousClass0CE r9 = new AnonymousClass0CE("WRITE_IO", 19);
        A0P = r9;
        AnonymousClass0CE r8 = new AnonymousClass0CE("WRITE_FAILURE_UNCLASSIFIED", 20);
        A0O = r8;
        AnonymousClass0CE r7 = new AnonymousClass0CE("UNKNOWN_RUNTIME", 21);
        A0M = r7;
        AnonymousClass0CE r3 = new AnonymousClass0CE("SEND_FAILURE", 22);
        A0J = r3;
        AnonymousClass0CE r2 = new AnonymousClass0CE("DISCONNECT_FROM_SERVER", 23);
        A03 = r2;
        AnonymousClass0CE r4 = new AnonymousClass0CE("SERIALIZER_FAILURE", 24);
        AnonymousClass0CE r1 = new AnonymousClass0CE("PREEMPTIVE_RECONNECT_SUCCESS", 25);
        A0B = r1;
        AnonymousClass0CE r014 = new AnonymousClass0CE("ABORTED_PREEMPTIVE_RECONNECT", 26);
        A01 = r014;
        AnonymousClass0CE r6 = new AnonymousClass0CE("AUTH_CREDENTIALS_CHANGE", 27);
        A02 = r6;
        AnonymousClass0CE r31 = new AnonymousClass0CE("NETWORK_LOST", 28);
        A08 = r31;
        AnonymousClass0CE[] r42 = new AnonymousClass0CE[29];
        System.arraycopy(new AnonymousClass0CE[]{r0, r02, r03, r04, r05, r06, r07, r08, r09, r010, r011, r13, r012, r013, r15, r14, r12, r11, r10, r9, r8, r7, r3, r2, r4, r1, r014}, 0, r42, 0, 27);
        System.arraycopy(new AnonymousClass0CE[]{r6, r31}, 0, r42, 27, 2);
        A00 = r42;
    }

    public static AnonymousClass0CE A00(Throwable th) {
        if ((th instanceof TimeoutException) || (th instanceof SocketTimeoutException)) {
            return A0I;
        }
        if (th instanceof EOFException) {
            return A0C;
        }
        if (th instanceof SocketException) {
            return A0G;
        }
        if (th instanceof SSLException) {
            return A0H;
        }
        if (th instanceof IOException) {
            return A0F;
        }
        if (th instanceof DataFormatException) {
            return A0E;
        }
        return A0D;
    }

    public static AnonymousClass0CE A01(Throwable th) {
        if ((th instanceof TimeoutException) || (th instanceof SocketTimeoutException)) {
            return A0S;
        }
        if (th instanceof EOFException) {
            return A0N;
        }
        if (th instanceof SocketException) {
            return A0Q;
        }
        if (th instanceof SSLException) {
            return A0R;
        }
        if (th instanceof IOException) {
            return A0P;
        }
        return A0O;
    }

    public static AnonymousClass0CE valueOf(String str) {
        return (AnonymousClass0CE) Enum.valueOf(AnonymousClass0CE.class, str);
    }

    public static AnonymousClass0CE[] values() {
        return (AnonymousClass0CE[]) A00.clone();
    }

    private AnonymousClass0CE(String str, int i) {
    }
}
