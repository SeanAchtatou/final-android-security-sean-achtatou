package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import java.util.List;

/* renamed from: X.0wQ  reason: invalid class name and case insensitive filesystem */
public final class C16060wQ extends C16070wR {
    public static final MigColorScheme A0A = C17190yT.A00();
    @Comparable(type = 13)
    public C04480Uv A00;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public C15340v8 A02;
    @Comparable(type = 13)
    public C15460vK A03;
    @Comparable(type = 13)
    public MigColorScheme A04 = A0A;
    @Comparable(type = 13)
    public C191917d A05;
    @Comparable(type = 13)
    public C16450x5 A06;
    @Comparable(type = 13)
    public C15560vU A07;
    @Comparable(type = 13)
    public Integer A08;
    @Comparable(type = 5)
    public List A09;

    public C16060wQ(Context context) {
        super("InboxUnitItemsSection");
        this.A01 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:29:0x00b7 */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v91, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v92, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v93, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v94, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v97, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v113, resolved type: X.3e4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v114, resolved type: X.3eF} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v115, resolved type: X.3eH} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v116, resolved type: X.3eJ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v118, resolved type: X.1GJ} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v119, resolved type: X.3e2} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v120, resolved type: X.3e0} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v121, resolved type: X.3eD} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v122, resolved type: X.3e5} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v123, resolved type: X.3eI} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v124, resolved type: X.3eA} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v125, resolved type: X.3Cv} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v127, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v128, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v129, resolved type: X.3eO} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v130, resolved type: X.3eO} */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        if (r2 != null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:362:0x0b4b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:363:0x0b4c, code lost:
        r8.A09.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:364:0x0b51, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:368:0x0b7f, code lost:
        if (r2.A07().ArZ() != r5.A00.A07().ArZ()) goto L_0x0b81;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0028, code lost:
        if (r3.A00.A0F(r2.A00) == false) goto L_0x0b81;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object AXa(X.AnonymousClass10N r17, java.lang.Object r18) {
        /*
            r16 = this;
            r2 = r18
            r1 = r17
            int r0 = r1.A01
            switch(r0) {
                case -680621655: goto L_0x000b;
                case 239257522: goto L_0x002c;
                case 1171108835: goto L_0x0b52;
                case 2120202712: goto L_0x0b87;
                default: goto L_0x0009;
            }
        L_0x0009:
            r0 = 0
            return r0
        L_0x000b:
            X.1Kn r2 = (X.AnonymousClass1Kn) r2
            java.lang.Object r3 = r2.A01
            X.1Hr r3 = (X.C21531Hr) r3
            java.lang.Object r2 = r2.A00
            X.1Hr r2 = (X.C21531Hr) r2
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r3.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r2.A01
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0b81
            com.facebook.messaging.inbox2.items.InboxUnitItem r1 = r3.A00
            com.facebook.messaging.inbox2.items.InboxUnitItem r0 = r2.A00
            boolean r0 = r1.A0F(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0b82
            goto L_0x0b81
        L_0x002c:
            X.1IX r2 = (X.AnonymousClass1IX) r2
            X.0zV r1 = r1.A00
            java.lang.Object r0 = r2.A01
            X.1Hr r0 = (X.C21531Hr) r0
            X.0wQ r1 = (X.C16060wQ) r1
            X.0vU r3 = r1.A07
            X.0x5 r2 = r1.A06
            X.0v8 r5 = r1.A02
            com.facebook.messaging.inbox2.items.InboxUnitItem r1 = r0.A00
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r0.A01
            r6 = r5
            if (r0 != 0) goto L_0x0047
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = X.C17190yT.A00()
        L_0x0047:
            X.1IY r4 = r1.A06()
            int r4 = r4.ordinal()
            switch(r4) {
                case 0: goto L_0x07b4;
                case 1: goto L_0x0574;
                case 2: goto L_0x0331;
                case 3: goto L_0x036a;
                case 4: goto L_0x04f7;
                case 5: goto L_0x04a7;
                case 6: goto L_0x07b4;
                case 7: goto L_0x0052;
                case 8: goto L_0x07b4;
                case 9: goto L_0x03e5;
                case 10: goto L_0x0052;
                case 11: goto L_0x07b4;
                case 12: goto L_0x0778;
                case 13: goto L_0x06f3;
                case 14: goto L_0x0052;
                case 15: goto L_0x0736;
                case 16: goto L_0x06ac;
                case 17: goto L_0x02e5;
                case 18: goto L_0x066a;
                case 19: goto L_0x0282;
                case 20: goto L_0x0052;
                case 21: goto L_0x05e3;
                case 22: goto L_0x0623;
                case 23: goto L_0x03a5;
                case 24: goto L_0x0319;
                case 25: goto L_0x0052;
                case 26: goto L_0x0052;
                case 27: goto L_0x0052;
                case 28: goto L_0x0534;
                case 29: goto L_0x0052;
                case 30: goto L_0x0459;
                case 31: goto L_0x0052;
                case 32: goto L_0x0422;
                case 33: goto L_0x0052;
                case 34: goto L_0x02af;
                case 35: goto L_0x03f3;
                case 36: goto L_0x07b4;
                case 37: goto L_0x0052;
                case 38: goto L_0x0052;
                case 39: goto L_0x0201;
                case 40: goto L_0x0052;
                case 41: goto L_0x01be;
                case 42: goto L_0x018a;
                default: goto L_0x0052;
            }
        L_0x0052:
            r4 = 0
        L_0x0053:
            r9 = 0
            if (r4 != 0) goto L_0x0059
            r6 = 0
            if (r2 == 0) goto L_0x005a
        L_0x0059:
            r6 = 1
        L_0x005a:
            X.1IY r5 = r1.A06()
            java.lang.String r0 = "No binding found for type %s"
            com.google.common.base.Preconditions.checkArgument(r6, r0, r5)
            if (r4 != 0) goto L_0x00a8
            X.0p4 r8 = r3.A03
            r7 = 4
            java.lang.String r6 = "inboxViewBinderNoFallback"
            java.lang.String r5 = "listener"
            java.lang.String r4 = "lithoViewCompatCreators"
            java.lang.String r0 = "model"
            java.lang.String[] r6 = new java.lang.String[]{r6, r5, r4, r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r7)
            X.3eQ r4 = new X.3eQ
            r4.<init>()
            X.0zR r0 = r8.A04
            if (r0 == 0) goto L_0x0086
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x0086:
            r5.clear()
            X.1Be r0 = r3.A0A
            r4.A03 = r0
            r5.set(r9)
            r4.A02 = r2
            r0 = 2
            r5.set(r0)
            r4.A00 = r1
            r0 = 3
            r5.set(r0)
            X.1BC r0 = r3.A05
            r4.A01 = r0
            r0 = 1
            r5.set(r0)
            X.AnonymousClass11F.A0C(r7, r5, r6)
            r9 = 1
        L_0x00a8:
            r1.A06()
            if (r9 != 0) goto L_0x00f6
            X.1IY r5 = r1.A06()
            X.1IY r2 = X.AnonymousClass1IY.A0W
            r0 = 0
            if (r5 == r2) goto L_0x00b7
            r0 = 1
        L_0x00b7:
            if (r0 == 0) goto L_0x00f6
            X.0p4 r8 = r3.A03
            r7 = 3
            java.lang.String r5 = "content"
            java.lang.String r2 = "item"
            java.lang.String r0 = "listener"
            java.lang.String[] r6 = new java.lang.String[]{r5, r2, r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r7)
            X.1Ia r2 = new X.1Ia
            r2.<init>()
            X.0zR r0 = r8.A04
            if (r0 == 0) goto L_0x00d8
            java.lang.String r0 = r0.A06
            r2.A07 = r0
        L_0x00d8:
            r5.clear()
            if (r4 != 0) goto L_0x0184
            r0 = 0
        L_0x00de:
            r2.A00 = r0
            r0 = 0
            r5.set(r0)
            r2.A01 = r1
            r0 = 1
            r5.set(r0)
            X.1BC r0 = r3.A05
            r2.A02 = r0
            r0 = 2
            r5.set(r0)
            X.AnonymousClass11F.A0C(r7, r5, r6)
            r4 = r2
        L_0x00f6:
            r1.A06()
            X.1At r2 = r3.A07
            if (r2 == 0) goto L_0x016e
            boolean r0 = r1 instanceof com.facebook.messaging.inbox2.items.InboxUnitThreadItem
            if (r0 != 0) goto L_0x0105
            boolean r0 = r1 instanceof com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem
            if (r0 == 0) goto L_0x010a
        L_0x0105:
            java.util.Map r0 = r2.A01
            r0.put(r4, r1)
        L_0x010a:
            X.1AQ r0 = r2.A00
            int r2 = X.AnonymousClass1Y3.AdF
            com.facebook.orca.threadlist.ThreadListFragment r0 = r0.A00
            X.0UN r1 = r0.A0O
            r0 = 16
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1CV r5 = (X.AnonymousClass1CV) r5
            int r2 = r5.A03
            r1 = -1
            r0 = 0
            if (r2 == r1) goto L_0x0121
            r0 = 1
        L_0x0121:
            if (r0 == 0) goto L_0x016e
            X.1Ri r1 = r5.A08
            java.lang.String r0 = "Unread pill not initialized"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            X.1Ri r0 = r5.A08
            int r1 = r0.ArU()
            int r0 = r5.A02
            if (r1 > r0) goto L_0x013a
            boolean r0 = X.AnonymousClass1CV.A0B(r5)
            if (r0 == 0) goto L_0x016e
        L_0x013a:
            int r1 = X.AnonymousClass1Y3.BFn
            X.0UN r0 = r5.A07
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.108 r1 = (X.AnonymousClass108) r1
            X.3eR r0 = new X.3eR
            r0.<init>(r5)
            r1.A02(r0)
            java.lang.String r0 = "UnreadPill"
            r1.A02 = r0
            java.lang.String r0 = "ForUiThread"
            r1.A03(r0)
            java.lang.String r0 = "ResumeScrolling"
            r1.A04 = r0
            X.1TL r3 = r1.A01()
            r2 = 11
            int r1 = X.AnonymousClass1Y3.ASI
            X.0UN r0 = r5.A07
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0g4 r1 = (X.AnonymousClass0g4) r1
            java.lang.String r0 = "KeepExisting"
            r1.A04(r3, r0)
        L_0x016e:
            X.1my r2 = X.C21621Ib.A00()
            r2.A00 = r4
            r0 = r9 ^ 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "THREAD_SAFE"
            r2.A01(r0, r1)
            X.1Ib r0 = r2.A03()
            return r0
        L_0x0184:
            X.0zR r0 = r4.A16()
            goto L_0x00de
        L_0x018a:
            r0 = r1
            com.facebook.workchat.inbox.pinnedthreads.header.InboxUnitPinnedThreadsHeaderItem r0 = (com.facebook.workchat.inbox.pinnedthreads.header.InboxUnitPinnedThreadsHeaderItem) r0
            com.google.common.collect.ImmutableList r8 = r0.A00
            X.0p4 r7 = r3.A03
            r4 = 1
            java.lang.String r0 = "actionListener"
            java.lang.String[] r6 = new java.lang.String[]{r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r4)
            X.3eO r4 = new X.3eO
            r4.<init>()
            X.0zR r0 = r7.A04
            if (r0 == 0) goto L_0x01aa
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x01aa:
            r5.clear()
            X.3eP r0 = new X.3eP
            r0.<init>(r3, r8)
            r4.A00 = r0
            r0 = 0
            r5.set(r0)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r5, r6)
            goto L_0x0053
        L_0x01be:
            X.0p4 r0 = r3.A03
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            X.0p4 r0 = r3.A03
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r4 = X.C21641Id.A00(r0)
            r0 = 251658240(0xf000000, float:6.3108872E-30)
            r4.A3F(r0)
            r0 = 1073741824(0x40000000, float:2.0)
            r4.A1p(r0)
            r0 = 1120403456(0x42c80000, float:100.0)
            r4.A20(r0)
            X.1Id r0 = r4.A34()
            r5.A3A(r0)
            X.0uO r0 = X.C14940uO.CENTER
            r5.A3D(r0)
            X.10G r4 = X.AnonymousClass10G.A09
            X.1JQ r0 = X.AnonymousClass1JQ.XXSMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r5.A2X(r4, r0)
            X.10G r4 = X.AnonymousClass10G.A04
            X.1JQ r0 = X.AnonymousClass1JQ.XLARGE
            int r0 = r0.B3A()
            float r0 = (float) r0
            r5.A2Z(r4, r0)
            X.11D r4 = r5.A00
            goto L_0x0053
        L_0x0201:
            r7 = r1
            com.facebook.messaging.inboxfolder.InboxUnitFolderItem r7 = (com.facebook.messaging.inboxfolder.InboxUnitFolderItem) r7
            if (r5 == 0) goto L_0x0209
            r5.A03(r7)
        L_0x0209:
            int r5 = X.AnonymousClass1Y3.Ajc
            X.0UN r4 = r3.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r5, r4)
            X.2Ry r4 = (X.C46822Ry) r4
            X.3Cu r5 = new X.3Cu
            r5.<init>(r4)
            X.0p4 r6 = r3.A03
            r4 = 6
            java.lang.String r9 = "clickListener"
            java.lang.String r10 = "colorScheme"
            r8 = 1098(0x44a, float:1.539E-42)
            java.lang.String r11 = X.AnonymousClass24B.$const$string(r8)
            java.lang.String r12 = "item"
            java.lang.String r13 = "migIconName"
            java.lang.String r14 = "title"
            java.lang.String[] r9 = new java.lang.String[]{r9, r10, r11, r12, r13, r14}
            java.util.BitSet r8 = new java.util.BitSet
            r8.<init>(r4)
            X.3Cv r4 = new X.3Cv
            r4.<init>()
            X.0zR r6 = r6.A04
            if (r6 == 0) goto L_0x0241
            java.lang.String r6 = r6.A06
            r4.A07 = r6
        L_0x0241:
            r8.clear()
            r4.A01 = r7
            r6 = 3
            r8.set(r6)
            X.3Cw r6 = new X.3Cw
            r6.<init>(r3)
            r4.A03 = r6
            r6 = 0
            r8.set(r6)
            X.0p4 r7 = r3.A03
            r6 = 2131833825(0x7f1133e1, float:1.9300743E38)
            java.lang.String r6 = r7.A0C(r6)
            r4.A06 = r6
            r6 = 5
            r8.set(r6)
            r4.A04 = r0
            r0 = 1
            r8.set(r0)
            X.1gj r0 = X.C29631gj.A1K
            r4.A00 = r0
            r0 = 4
            r8.set(r0)
            X.1Bc r0 = r3.A04
            r4.A02 = r0
            r0 = 2
            r8.set(r0)
            r4.A05 = r5
            r0 = 6
            X.AnonymousClass11F.A0C(r0, r8, r9)
            goto L_0x0053
        L_0x0282:
            r8 = r1
            com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem r8 = (com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem) r8
            X.0p4 r7 = r3.A03
            r4 = 1
            java.lang.String r0 = "item"
            java.lang.String[] r6 = new java.lang.String[]{r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r4)
            X.3eA r4 = new X.3eA
            r4.<init>()
            X.0zR r0 = r7.A04
            if (r0 == 0) goto L_0x02a0
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x02a0:
            r5.clear()
            r4.A00 = r8
            r0 = 0
            r5.set(r0)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r5, r6)
            goto L_0x0053
        L_0x02af:
            r9 = r1
            com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem r9 = (com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem) r9
            X.0p4 r8 = r3.A03
            r7 = 2
            java.lang.String r4 = "item"
            java.lang.String r0 = "listener"
            java.lang.String[] r6 = new java.lang.String[]{r4, r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r7)
            X.3eI r4 = new X.3eI
            r4.<init>()
            X.0zR r0 = r8.A04
            if (r0 == 0) goto L_0x02cf
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x02cf:
            r5.clear()
            r4.A01 = r9
            r0 = 0
            r5.set(r0)
            X.1BC r0 = r3.A05
            r4.A00 = r0
            r0 = 1
            r5.set(r0)
            X.AnonymousClass11F.A0C(r7, r5, r6)
            goto L_0x0053
        L_0x02e5:
            r9 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r9 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r9
            X.0p4 r11 = r3.A03
            r10 = 2
            java.lang.String r5 = "item"
            java.lang.String r4 = "listener"
            java.lang.String[] r8 = new java.lang.String[]{r5, r4}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r10)
            X.3e5 r4 = new X.3e5
            android.content.Context r5 = r11.A09
            r4.<init>(r5)
            X.0zR r5 = r11.A04
            if (r5 == 0) goto L_0x0307
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x0307:
            r7.clear()
            r4.A02 = r9
            r5 = 0
            r7.set(r5)
            r4.A04 = r0
            r4.A03 = r6
            X.AnonymousClass11F.A0C(r10, r7, r8)
            goto L_0x0053
        L_0x0319:
            r5 = r1
            com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem r5 = (com.facebook.messaging.inbox2.morefooter.InboxUnitSeeAllItem) r5
            X.0p4 r0 = r3.A03
            X.3eD r4 = new X.3eD
            r4.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x032b
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x032b:
            int r0 = r5.A00
            r4.A00 = r0
            goto L_0x0053
        L_0x0331:
            r8 = r1
            com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem r8 = (com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem) r8
            X.0p4 r9 = r3.A03
            r7 = 2
            java.lang.String r4 = "item"
            java.lang.String r0 = "menuClickListener"
            java.lang.String[] r6 = new java.lang.String[]{r4, r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r7)
            X.3e0 r4 = new X.3e0
            r4.<init>()
            X.0zR r0 = r9.A04
            if (r0 == 0) goto L_0x0351
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x0351:
            r5.clear()
            r4.A01 = r8
            r0 = 0
            r5.set(r0)
            X.3e1 r0 = new X.3e1
            r0.<init>(r3, r8)
            r4.A00 = r0
            r0 = 1
            r5.set(r0)
            X.AnonymousClass11F.A0C(r7, r5, r6)
            goto L_0x0053
        L_0x036a:
            r8 = r1
            com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem r8 = (com.facebook.messaging.inbox2.sectionheader.NonInboxServiceSectionHeaderItem) r8
            X.0p4 r10 = r3.A03
            r9 = 2
            java.lang.String r5 = "clickListener"
            java.lang.String r4 = "item"
            java.lang.String[] r7 = new java.lang.String[]{r5, r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r9)
            X.3e2 r4 = new X.3e2
            r4.<init>()
            X.0zR r5 = r10.A04
            if (r5 == 0) goto L_0x038a
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x038a:
            r6.clear()
            r4.A01 = r8
            r5 = 1
            r6.set(r5)
            X.3e3 r5 = new X.3e3
            r5.<init>(r3, r8)
            r4.A00 = r5
            r5 = 0
            r6.set(r5)
            r4.A02 = r0
            X.AnonymousClass11F.A0C(r9, r6, r7)
            goto L_0x0053
        L_0x03a5:
            r8 = r1
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r8 = (com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem) r8
            X.0p4 r10 = r3.A03
            r9 = 3
            java.lang.String r6 = "colorScheme"
            java.lang.String r5 = "isM4ThreadList"
            java.lang.String r4 = "moreThreadsItem"
            java.lang.String[] r7 = new java.lang.String[]{r6, r5, r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r9)
            X.1GJ r4 = new X.1GJ
            r4.<init>()
            X.0zR r5 = r10.A04
            if (r5 == 0) goto L_0x03c7
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x03c7:
            r6.clear()
            r4.A01 = r8
            r5 = 2
            r6.set(r5)
            r4.A02 = r0
            r0 = 0
            r6.set(r0)
            r0 = 1
            r4.A03 = r0
            r6.set(r0)
            X.1BP r0 = r3.A09
            r4.A00 = r0
            X.AnonymousClass11F.A0C(r9, r6, r7)
            goto L_0x0053
        L_0x03e5:
            X.0p4 r0 = r3.A03
            X.1wd r4 = X.C16980y8.A00(r0)
            r0 = 1
            r4.A27(r0)
            X.0y8 r4 = r4.A00
            goto L_0x0053
        L_0x03f3:
            r8 = r1
            com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem r8 = (com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem) r8
            X.0p4 r7 = r3.A03
            r4 = 1
            java.lang.String r0 = "item"
            java.lang.String[] r6 = new java.lang.String[]{r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r4)
            X.3eJ r4 = new X.3eJ
            android.content.Context r0 = r7.A09
            r4.<init>(r0)
            X.0zR r0 = r7.A04
            if (r0 == 0) goto L_0x0413
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x0413:
            r5.clear()
            r4.A01 = r8
            r0 = 0
            r5.set(r0)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r5, r6)
            goto L_0x0053
        L_0x0422:
            r8 = r1
            com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem r8 = (com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem) r8
            X.0p4 r9 = r3.A03
            r5 = 1
            r4 = 34
            java.lang.String r4 = X.ECX.$const$string(r4)
            java.lang.String[] r7 = new java.lang.String[]{r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r5)
            X.3eH r4 = new X.3eH
            android.content.Context r5 = r9.A09
            r4.<init>(r5)
            X.0zR r5 = r9.A04
            if (r5 == 0) goto L_0x0446
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x0446:
            r6.clear()
            java.lang.String r5 = r8.A01
            r4.A02 = r5
            r5 = 0
            r6.set(r5)
            r4.A01 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r6, r7)
            goto L_0x0053
        L_0x0459:
            X.0p4 r11 = r3.A03
            r10 = r1
            com.facebook.messaging.business.inboxads.common.InboxAdsItem r10 = (com.facebook.messaging.business.inboxads.common.InboxAdsItem) r10
            X.1BC r9 = r3.A05
            X.1BY r8 = r3.A06
            r12 = 4
            java.lang.String r7 = "colorScheme"
            java.lang.String r6 = "item"
            java.lang.String r5 = "listener"
            java.lang.String r4 = "tapContext"
            java.lang.String[] r7 = new java.lang.String[]{r7, r6, r5, r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r12)
            X.3eF r4 = new X.3eF
            android.content.Context r5 = r11.A09
            r4.<init>(r5)
            X.0zR r5 = r11.A04
            if (r5 == 0) goto L_0x0483
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x0483:
            r6.clear()
            r4.A01 = r10
            r5 = 1
            r6.set(r5)
            r4.A06 = r0
            r0 = 0
            r6.set(r0)
            r4.A02 = r9
            r0 = 2
            r6.set(r0)
            X.3eG r0 = X.C72423eG.A03
            r4.A03 = r0
            r0 = 3
            r6.set(r0)
            r4.A05 = r8
            X.AnonymousClass11F.A0C(r12, r6, r7)
            goto L_0x0053
        L_0x04a7:
            r9 = r1
            com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem r9 = (com.facebook.messaging.inbox2.messagerequests.MessageRequestsBannerInboxItem) r9
            X.0p4 r12 = r3.A03
            r8 = 3
            java.lang.String r6 = "colorScheme"
            java.lang.String r5 = "snippet"
            java.lang.String r4 = "title"
            java.lang.String[] r7 = new java.lang.String[]{r6, r5, r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r8)
            X.3e4 r4 = new X.3e4
            android.content.Context r5 = r12.A09
            r4.<init>(r5)
            r11 = 0
            r10 = 2132476608(0x7f1b02c0, float:2.0604462E38)
            X.0zR r5 = r12.A04
            if (r5 == 0) goto L_0x04cf
            java.lang.String r8 = r5.A06
            r4.A07 = r8
        L_0x04cf:
            X.11G r5 = r4.A14()
            r5.A0B(r11, r10)
            r4.A0d(r12, r11, r10)
            r6.clear()
            java.lang.String r5 = r9.A01
            r4.A03 = r5
            r5 = 2
            r6.set(r5)
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r5 = r9.A00
            r4.A01 = r5
            r5 = 1
            r6.set(r5)
            r4.A02 = r0
            r6.set(r11)
            r0 = 3
            X.AnonymousClass11F.A0C(r0, r6, r7)
            goto L_0x0053
        L_0x04f7:
            r6 = r1
            com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem r6 = (com.facebook.messaging.inbox2.messagerequests.MessageRequestsHeaderInboxItem) r6
            X.0p4 r4 = r3.A03
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r5 = X.C17030yD.A00(r4)
            int r4 = r6.A00
            r5.A3P(r4)
            r5.A3c(r0)
            r0 = 3
            r5.A3J(r0)
            X.10J r0 = X.AnonymousClass10J.A0Q
            r5.A3f(r0)
            X.0uO r0 = X.C14940uO.CENTER
            r5.A2V(r0)
            X.10G r4 = X.AnonymousClass10G.A04
            X.1JQ r0 = X.AnonymousClass1JQ.XLARGE
            int r0 = r0.B3A()
            float r0 = (float) r0
            r5.A2X(r4, r0)
            X.10G r4 = X.AnonymousClass10G.A09
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r5.A2X(r4, r0)
            X.0yD r4 = r5.A39()
            goto L_0x0053
        L_0x0534:
            com.google.common.base.Preconditions.checkNotNull(r5)
            r9 = r1
            com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem r9 = (com.facebook.messaging.inbox2.horizontaltiles.HorizontalTilesUnitInboxItem) r9
            com.google.common.collect.ImmutableList r0 = r9.A00
            r5.A04(r0)
            X.0p4 r10 = r3.A03
            r8 = 2
            java.lang.String r4 = "data"
            java.lang.String r0 = "impressionTracker"
            java.lang.String[] r7 = new java.lang.String[]{r4, r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r8)
            X.3eE r4 = new X.3eE
            r4.<init>()
            X.0zR r0 = r10.A04
            if (r0 == 0) goto L_0x055c
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x055c:
            r5.clear()
            r4.A02 = r9
            r0 = 0
            r5.set(r0)
            r4.A00 = r6
            r0 = 1
            r5.set(r0)
            X.1BC r0 = r3.A05
            r4.A01 = r0
            X.AnonymousClass11F.A0C(r8, r5, r7)
            goto L_0x0053
        L_0x0574:
            r6 = r1
            com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem r6 = (com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem) r6
            X.0xv r0 = r6.A00
            boolean r4 = r0.B33()
            r0 = 0
            if (r4 == 0) goto L_0x05a0
            X.0xv r4 = r6.A00
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r7 = r4.Are()
            if (r7 == 0) goto L_0x05a0
            r5 = 0
            int r4 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r3.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.0r6 r5 = (X.AnonymousClass0r6) r5
            java.lang.String r0 = r7.A3m()
            com.facebook.user.model.UserKey r4 = com.facebook.user.model.UserKey.A01(r0)
            r0 = -1
            X.18m r0 = r5.A0L(r4, r0)
        L_0x05a0:
            X.0p4 r10 = r3.A03
            r9 = 2
            java.lang.String r5 = "conversationStarter"
            java.lang.String r4 = "threadTileDataFactory"
            java.lang.String[] r8 = new java.lang.String[]{r5, r4}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r9)
            X.3dz r4 = new X.3dz
            android.content.Context r5 = r10.A09
            r4.<init>(r5)
            X.0zR r5 = r10.A04
            if (r5 == 0) goto L_0x05bf
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x05bf:
            r7.clear()
            X.0xv r5 = r6.A00
            r4.A01 = r5
            r5 = 0
            r7.set(r5)
            com.facebook.messaging.model.threads.ThreadSummary r5 = r6.A01
            r4.A02 = r5
            X.0Tq r5 = r3.A0B
            java.lang.Object r5 = r5.get()
            X.0ty r5 = (X.C14760ty) r5
            r4.A03 = r5
            r5 = 1
            r7.set(r5)
            r4.A04 = r0
            X.AnonymousClass11F.A0C(r9, r7, r8)
            goto L_0x0053
        L_0x05e3:
            r8 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r8 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r8
            X.0p4 r9 = r3.A03
            r10 = 3
            java.lang.String r6 = "colorScheme"
            java.lang.String r5 = "item"
            java.lang.String r4 = "listener"
            java.lang.String[] r7 = new java.lang.String[]{r6, r5, r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r10)
            X.3eB r4 = new X.3eB
            android.content.Context r5 = r9.A09
            r4.<init>(r5)
            X.0zR r5 = r9.A04
            if (r5 == 0) goto L_0x0607
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x0607:
            r6.clear()
            r4.A02 = r8
            r5 = 1
            r6.set(r5)
            r4.A03 = r0
            r0 = 0
            r6.set(r0)
            X.1BC r0 = r3.A05
            r4.A01 = r0
            r0 = 2
            r6.set(r0)
            X.AnonymousClass11F.A0C(r10, r6, r7)
            goto L_0x0053
        L_0x0623:
            r9 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r9 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit) r9
            com.google.common.collect.ImmutableList r4 = r9.A01
            r5.A04(r4)
            X.0p4 r10 = r3.A03
            r11 = 3
            java.lang.String r7 = "impressionTracker"
            java.lang.String r5 = "listener"
            java.lang.String r4 = "unit"
            java.lang.String[] r8 = new java.lang.String[]{r7, r5, r4}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r11)
            X.3eC r4 = new X.3eC
            android.content.Context r5 = r10.A09
            r4.<init>(r5)
            X.0zR r5 = r10.A04
            if (r5 == 0) goto L_0x064c
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x064c:
            r7.clear()
            r4.A02 = r9
            r5 = 2
            r7.set(r5)
            r4.A03 = r6
            r5 = 0
            r7.set(r5)
            X.1BC r5 = r3.A05
            r4.A01 = r5
            r5 = 1
            r7.set(r5)
            r4.A04 = r0
            X.AnonymousClass11F.A0C(r11, r7, r8)
            goto L_0x0053
        L_0x066a:
            r8 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r8 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r8
            X.0p4 r9 = r3.A03
            r10 = 3
            java.lang.String r6 = "isCardStyle"
            java.lang.String r5 = "item"
            java.lang.String r4 = "listener"
            java.lang.String[] r7 = new java.lang.String[]{r6, r5, r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r10)
            X.3e9 r4 = new X.3e9
            android.content.Context r5 = r9.A09
            r4.<init>(r5)
            X.0zR r5 = r9.A04
            if (r5 == 0) goto L_0x068e
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x068e:
            r6.clear()
            r4.A02 = r8
            r5 = 1
            r6.set(r5)
            X.1BC r5 = r3.A05
            r4.A01 = r5
            r5 = 2
            r6.set(r5)
            r5 = 0
            r4.A05 = r5
            r6.set(r5)
            r4.A03 = r0
            X.AnonymousClass11F.A0C(r10, r6, r7)
            goto L_0x0053
        L_0x06ac:
            r9 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r9 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit) r9
            com.google.common.collect.ImmutableList r4 = r9.A01
            r5.A04(r4)
            X.0p4 r10 = r3.A03
            r11 = 3
            java.lang.String r7 = "impressionTracker"
            java.lang.String r5 = "listener"
            java.lang.String r4 = "unit"
            java.lang.String[] r8 = new java.lang.String[]{r7, r5, r4}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r11)
            X.3e8 r4 = new X.3e8
            android.content.Context r5 = r10.A09
            r4.<init>(r5)
            X.0zR r5 = r10.A04
            if (r5 == 0) goto L_0x06d5
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x06d5:
            r7.clear()
            r4.A01 = r9
            r5 = 2
            r7.set(r5)
            r4.A02 = r6
            r5 = 0
            r7.set(r5)
            X.1BC r5 = r3.A05
            r4.A00 = r5
            r5 = 1
            r7.set(r5)
            r4.A03 = r0
            X.AnonymousClass11F.A0C(r11, r7, r8)
            goto L_0x0053
        L_0x06f3:
            r8 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r8 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit) r8
            com.google.common.collect.ImmutableList r0 = r8.A01
            r5.A04(r0)
            X.0p4 r10 = r3.A03
            r9 = 3
            java.lang.String r5 = "impressionTracker"
            java.lang.String r4 = "item"
            java.lang.String r0 = "listener"
            java.lang.String[] r7 = new java.lang.String[]{r5, r4, r0}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r9)
            X.3e6 r4 = new X.3e6
            r4.<init>()
            X.0zR r0 = r10.A04
            if (r0 == 0) goto L_0x071a
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x071a:
            r5.clear()
            r4.A01 = r8
            r0 = 1
            r5.set(r0)
            r4.A02 = r6
            r0 = 0
            r5.set(r0)
            X.1BC r0 = r3.A05
            r4.A00 = r0
            r0 = 2
            r5.set(r0)
            X.AnonymousClass11F.A0C(r9, r5, r7)
            goto L_0x0053
        L_0x0736:
            r9 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r9 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r9
            X.1BY r8 = r3.A06
            X.0p4 r10 = r3.A03
            r11 = 3
            java.lang.String r6 = "item"
            java.lang.String r5 = "listener"
            java.lang.String r4 = "swipeableListItemComponentCoordinator"
            java.lang.String[] r7 = new java.lang.String[]{r6, r5, r4}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r11)
            X.3e7 r4 = new X.3e7
            r4.<init>()
            X.0zR r5 = r10.A04
            if (r5 == 0) goto L_0x075a
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x075a:
            r6.clear()
            r4.A01 = r9
            r5 = 0
            r6.set(r5)
            X.1BC r5 = r3.A05
            r4.A00 = r5
            r5 = 1
            r6.set(r5)
            r4.A02 = r8
            r5 = 2
            r6.set(r5)
            r4.A03 = r0
            X.AnonymousClass11F.A0C(r11, r6, r7)
            goto L_0x0053
        L_0x0778:
            r9 = r1
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem r9 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentItem) r9
            X.0p4 r10 = r3.A03
            r11 = 2
            java.lang.String r5 = "item"
            java.lang.String r4 = "listener"
            java.lang.String[] r8 = new java.lang.String[]{r5, r4}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r11)
            X.3e5 r4 = new X.3e5
            android.content.Context r5 = r10.A09
            r4.<init>(r5)
            X.0zR r5 = r10.A04
            if (r5 == 0) goto L_0x079a
            java.lang.String r5 = r5.A06
            r4.A07 = r5
        L_0x079a:
            r7.clear()
            r4.A02 = r9
            r5 = 0
            r7.set(r5)
            X.1BC r5 = r3.A05
            r4.A01 = r5
            r5 = 1
            r7.set(r5)
            r4.A04 = r0
            r4.A03 = r6
            X.AnonymousClass11F.A0C(r11, r7, r8)
            goto L_0x0053
        L_0x07b4:
            X.1qy r4 = r3.A01
            com.google.common.base.Preconditions.checkNotNull(r4)
            X.1qy r5 = r3.A01
            X.1IY r4 = r1.A06()
            java.lang.String r12 = r4.toString()
            r9 = r1
            X.1qw r4 = r5.A00
            X.1qx r8 = r4.A00
            java.util.concurrent.atomic.AtomicInteger r4 = X.C27271cv.A02
            r4.getAndIncrement()
            X.1d0 r4 = r8.A09
            java.lang.String r10 = "createComponent"
            java.lang.String r7 = "com.facebook.messaging.threadlist.plugins.interfaces.inboxviewbinder.InboxViewBinderInterfaceSpec"
            r4.A05(r7, r10)
            r4 = 0
            monitor-enter(r8)     // Catch:{ all -> 0x0b4b }
            boolean r5 = r8.A05     // Catch:{ all -> 0x0b48 }
            if (r5 != 0) goto L_0x07ed
            android.content.Context r5 = r8.A06     // Catch:{ all -> 0x0b48 }
            if (r5 == 0) goto L_0x0ae7
            X.1XX r5 = X.AnonymousClass1XX.get(r5)     // Catch:{ all -> 0x0b48 }
            X.0Tq r5 = X.AnonymousClass0XJ.A0I(r5)     // Catch:{ all -> 0x0b48 }
            r8.A04 = r5     // Catch:{ all -> 0x0b48 }
            r5 = 1
            r8.A05 = r5     // Catch:{ all -> 0x0b48 }
        L_0x07ed:
            monitor-exit(r8)     // Catch:{ all -> 0x0b4b }
            java.lang.String r5 = "ACTIVE_NOW_PRESENCE_DISABLED_UPSELL"
            boolean r5 = r12.equals(r5)     // Catch:{ all -> 0x0b4b }
            if (r5 == 0) goto L_0x08a3
            java.lang.Object r5 = r8.A00     // Catch:{ all -> 0x0b4b }
            r14 = 1
            if (r5 != 0) goto L_0x083a
            java.util.concurrent.atomic.AtomicInteger r5 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r5.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r13 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r11 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.activenow.ActiveNowPresenceDisabledUpsellComponent"
            r13.A06(r11, r7)     // Catch:{ all -> 0x0b4b }
            X.3eK r11 = r8.A07     // Catch:{ Exception -> 0x0aef }
            if (r11 != 0) goto L_0x080c
            goto L_0x0813
        L_0x080c:
            java.lang.String r5 = "com.facebook.messaging.threadlist.plugins.inboxviewbinder.InboxViewBinderKillSwitch"
            java.lang.Boolean r5 = r11.isNeeded(r5)     // Catch:{ Exception -> 0x0aef }
            goto L_0x0814
        L_0x0813:
            r5 = 0
        L_0x0814:
            if (r5 == 0) goto L_0x081b
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0aef }
            goto L_0x082a
        L_0x081b:
            java.lang.Boolean r5 = X.C35491rJ.A00     // Catch:{ Exception -> 0x0aef }
            if (r5 == 0) goto L_0x0824
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0aef }
            goto L_0x082a
        L_0x0824:
            X.1d0 r5 = r8.A09     // Catch:{ Exception -> 0x0aef }
            boolean r5 = X.C35491rJ.A00(r5)     // Catch:{ Exception -> 0x0aef }
        L_0x082a:
            if (r5 == 0) goto L_0x0831
            java.lang.Object r5 = X.C27271cv.A00     // Catch:{ Exception -> 0x0aef }
            r8.A00 = r5     // Catch:{ Exception -> 0x0aef }
            goto L_0x0835
        L_0x0831:
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ Exception -> 0x0aef }
            r8.A00 = r5     // Catch:{ Exception -> 0x0aef }
        L_0x0835:
            X.1d0 r5 = r8.A09     // Catch:{ all -> 0x0b4b }
            r5.A02()     // Catch:{ all -> 0x0b4b }
        L_0x083a:
            java.lang.Object r11 = r8.A00     // Catch:{ all -> 0x0b4b }
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ all -> 0x0b4b }
            if (r11 != r5) goto L_0x0841
            r14 = 0
        L_0x0841:
            if (r14 == 0) goto L_0x08a3
            java.util.concurrent.atomic.AtomicInteger r0 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r0.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r4 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r0 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.activenow.ActiveNowPresenceDisabledUpsellComponent"
            r4.A07(r0, r7, r10)     // Catch:{ all -> 0x0b4b }
            X.0p4 r13 = r8.A08     // Catch:{ Exception -> 0x0af7 }
            X.1BC r11 = r8.A0A     // Catch:{ Exception -> 0x0af7 }
            X.0Tq r12 = r8.A04     // Catch:{ Exception -> 0x0af7 }
            r5 = 2
            java.lang.String r4 = "listener"
            r0 = 517(0x205, float:7.24E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ Exception -> 0x0af7 }
            java.lang.String[] r9 = new java.lang.String[]{r4, r0}     // Catch:{ Exception -> 0x0af7 }
            java.util.BitSet r7 = new java.util.BitSet     // Catch:{ Exception -> 0x0af7 }
            r7.<init>(r5)     // Catch:{ Exception -> 0x0af7 }
            X.3eM r4 = new X.3eM     // Catch:{ Exception -> 0x0af7 }
            android.content.Context r0 = r13.A09     // Catch:{ Exception -> 0x0af7 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0af7 }
            r10 = 0
            r6 = 2132476608(0x7f1b02c0, float:2.0604462E38)
            X.0zR r0 = r13.A04     // Catch:{ Exception -> 0x0af7 }
            if (r0 == 0) goto L_0x087a
            java.lang.String r5 = r0.A06     // Catch:{ Exception -> 0x0af7 }
            r4.A07 = r5     // Catch:{ Exception -> 0x0af7 }
        L_0x087a:
            X.11G r0 = r4.A14()     // Catch:{ Exception -> 0x0af7 }
            r0.A0B(r10, r6)     // Catch:{ Exception -> 0x0af7 }
            r4.A0d(r13, r10, r6)     // Catch:{ Exception -> 0x0af7 }
            r7.clear()     // Catch:{ Exception -> 0x0af7 }
            java.lang.Object r0 = r12.get()     // Catch:{ Exception -> 0x0af7 }
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0     // Catch:{ Exception -> 0x0af7 }
            r4.A02 = r0     // Catch:{ Exception -> 0x0af7 }
            r0 = 1
            r7.set(r0)     // Catch:{ Exception -> 0x0af7 }
            r4.A01 = r11     // Catch:{ Exception -> 0x0af7 }
            r7.set(r10)     // Catch:{ Exception -> 0x0af7 }
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r7, r9)     // Catch:{ Exception -> 0x0af7 }
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
            goto L_0x0ad9
        L_0x08a3:
            java.lang.String r5 = "BYMM_PAGE"
            boolean r5 = r12.equals(r5)     // Catch:{ all -> 0x0b4b }
            if (r5 == 0) goto L_0x0952
            java.lang.Object r5 = r8.A01     // Catch:{ all -> 0x0b4b }
            r14 = 1
            if (r5 != 0) goto L_0x08ef
            java.util.concurrent.atomic.AtomicInteger r5 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r5.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r13 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r11 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.bymmpage.BYMMPageComponent"
            r13.A06(r11, r7)     // Catch:{ all -> 0x0b4b }
            X.3eK r11 = r8.A07     // Catch:{ Exception -> 0x0b00 }
            if (r11 != 0) goto L_0x08c1
            goto L_0x08c8
        L_0x08c1:
            java.lang.String r5 = "com.facebook.messaging.threadlist.plugins.inboxviewbinder.InboxViewBinderKillSwitch"
            java.lang.Boolean r5 = r11.isNeeded(r5)     // Catch:{ Exception -> 0x0b00 }
            goto L_0x08c9
        L_0x08c8:
            r5 = 0
        L_0x08c9:
            if (r5 == 0) goto L_0x08d0
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0b00 }
            goto L_0x08df
        L_0x08d0:
            java.lang.Boolean r5 = X.C35491rJ.A00     // Catch:{ Exception -> 0x0b00 }
            if (r5 == 0) goto L_0x08d9
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0b00 }
            goto L_0x08df
        L_0x08d9:
            X.1d0 r5 = r8.A09     // Catch:{ Exception -> 0x0b00 }
            boolean r5 = X.C35491rJ.A00(r5)     // Catch:{ Exception -> 0x0b00 }
        L_0x08df:
            if (r5 == 0) goto L_0x08e6
            java.lang.Object r5 = X.C27271cv.A00     // Catch:{ Exception -> 0x0b00 }
            r8.A01 = r5     // Catch:{ Exception -> 0x0b00 }
            goto L_0x08ea
        L_0x08e6:
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ Exception -> 0x0b00 }
            r8.A01 = r5     // Catch:{ Exception -> 0x0b00 }
        L_0x08ea:
            X.1d0 r5 = r8.A09     // Catch:{ all -> 0x0b4b }
            r5.A02()     // Catch:{ all -> 0x0b4b }
        L_0x08ef:
            java.lang.Object r11 = r8.A01     // Catch:{ all -> 0x0b4b }
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ all -> 0x0b4b }
            if (r11 != r5) goto L_0x08f6
            r14 = 0
        L_0x08f6:
            if (r14 == 0) goto L_0x0952
            java.util.concurrent.atomic.AtomicInteger r4 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r4.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r5 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r4 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.bymmpage.BYMMPageComponent"
            r5.A07(r4, r7, r10)     // Catch:{ all -> 0x0b4b }
            X.0p4 r12 = r8.A08     // Catch:{ Exception -> 0x0b0d }
            X.1BC r11 = r8.A0A     // Catch:{ Exception -> 0x0b0d }
            com.google.common.base.Preconditions.checkNotNull(r6)     // Catch:{ Exception -> 0x0b0d }
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r9 = (com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit) r9     // Catch:{ Exception -> 0x0b0d }
            com.google.common.collect.ImmutableList r4 = r9.A01     // Catch:{ Exception -> 0x0b0d }
            r6.A04(r4)     // Catch:{ Exception -> 0x0b0d }
            r13 = 3
            java.lang.String r7 = "impressionTracker"
            java.lang.String r5 = "listener"
            java.lang.String r4 = "unit"
            java.lang.String[] r10 = new java.lang.String[]{r7, r5, r4}     // Catch:{ Exception -> 0x0b0d }
            java.util.BitSet r7 = new java.util.BitSet     // Catch:{ Exception -> 0x0b0d }
            r7.<init>(r13)     // Catch:{ Exception -> 0x0b0d }
            X.3eL r4 = new X.3eL     // Catch:{ Exception -> 0x0b0d }
            android.content.Context r5 = r12.A09     // Catch:{ Exception -> 0x0b0d }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0b0d }
            X.0zR r5 = r12.A04     // Catch:{ Exception -> 0x0b0d }
            if (r5 == 0) goto L_0x0931
            java.lang.String r5 = r5.A06     // Catch:{ Exception -> 0x0b0d }
            r4.A07 = r5     // Catch:{ Exception -> 0x0b0d }
        L_0x0931:
            r7.clear()     // Catch:{ Exception -> 0x0b0d }
            r4.A01 = r9     // Catch:{ Exception -> 0x0b0d }
            r5 = 2
            r7.set(r5)     // Catch:{ Exception -> 0x0b0d }
            r4.A02 = r6     // Catch:{ Exception -> 0x0b0d }
            r5 = 0
            r7.set(r5)     // Catch:{ Exception -> 0x0b0d }
            r4.A00 = r11     // Catch:{ Exception -> 0x0b0d }
            r5 = 1
            r7.set(r5)     // Catch:{ Exception -> 0x0b0d }
            r4.A03 = r0     // Catch:{ Exception -> 0x0b0d }
            X.AnonymousClass11F.A0C(r13, r7, r10)     // Catch:{ Exception -> 0x0b0d }
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
            goto L_0x0ad9
        L_0x0952:
            java.lang.String r5 = "MONTAGE_AND_ACTIVE_NOW"
            boolean r5 = r12.equals(r5)     // Catch:{ all -> 0x0b4b }
            if (r5 == 0) goto L_0x0a14
            java.lang.Object r5 = r8.A02     // Catch:{ all -> 0x0b4b }
            r14 = 1
            if (r5 != 0) goto L_0x099e
            java.util.concurrent.atomic.AtomicInteger r5 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r5.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r13 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r11 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.montageactivenow.MontageAndActiveNowComponentPlugin"
            r13.A06(r11, r7)     // Catch:{ all -> 0x0b4b }
            X.3eK r11 = r8.A07     // Catch:{ Exception -> 0x0b16 }
            if (r11 != 0) goto L_0x0970
            goto L_0x0977
        L_0x0970:
            java.lang.String r5 = "com.facebook.messaging.threadlist.plugins.inboxviewbinder.InboxViewBinderKillSwitch"
            java.lang.Boolean r5 = r11.isNeeded(r5)     // Catch:{ Exception -> 0x0b16 }
            goto L_0x0978
        L_0x0977:
            r5 = 0
        L_0x0978:
            if (r5 == 0) goto L_0x097f
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0b16 }
            goto L_0x098e
        L_0x097f:
            java.lang.Boolean r5 = X.C35491rJ.A00     // Catch:{ Exception -> 0x0b16 }
            if (r5 == 0) goto L_0x0988
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0b16 }
            goto L_0x098e
        L_0x0988:
            X.1d0 r5 = r8.A09     // Catch:{ Exception -> 0x0b16 }
            boolean r5 = X.C35491rJ.A00(r5)     // Catch:{ Exception -> 0x0b16 }
        L_0x098e:
            if (r5 == 0) goto L_0x0995
            java.lang.Object r5 = X.C27271cv.A00     // Catch:{ Exception -> 0x0b16 }
            r8.A02 = r5     // Catch:{ Exception -> 0x0b16 }
            goto L_0x0999
        L_0x0995:
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ Exception -> 0x0b16 }
            r8.A02 = r5     // Catch:{ Exception -> 0x0b16 }
        L_0x0999:
            X.1d0 r5 = r8.A09     // Catch:{ all -> 0x0b4b }
            r5.A02()     // Catch:{ all -> 0x0b4b }
        L_0x099e:
            java.lang.Object r11 = r8.A02     // Catch:{ all -> 0x0b4b }
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ all -> 0x0b4b }
            if (r11 != r5) goto L_0x09a5
            r14 = 0
        L_0x09a5:
            if (r14 == 0) goto L_0x0a14
            java.util.concurrent.atomic.AtomicInteger r4 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r4.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r5 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r4 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.montageactivenow.MontageAndActiveNowComponentPlugin"
            r5.A07(r4, r7, r10)     // Catch:{ all -> 0x0b4b }
            X.0p4 r12 = r8.A08     // Catch:{ Exception -> 0x0b23 }
            X.1BC r11 = r8.A0A     // Catch:{ Exception -> 0x0b23 }
            com.google.common.base.Preconditions.checkNotNull(r6)     // Catch:{ Exception -> 0x0b23 }
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem r9 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem) r9     // Catch:{ Exception -> 0x0b23 }
            com.google.common.collect.ImmutableList r4 = r9.A03     // Catch:{ Exception -> 0x0b23 }
            r6.A04(r4)     // Catch:{ Exception -> 0x0b23 }
            com.google.common.collect.ImmutableList r4 = r9.A01     // Catch:{ Exception -> 0x0b23 }
            r6.A04(r4)     // Catch:{ Exception -> 0x0b23 }
            r13 = 5
            java.lang.String r14 = "activeNowListener"
            java.lang.String r10 = "impressionTracker"
            java.lang.String r7 = "item"
            java.lang.String r5 = "migColorScheme"
            java.lang.String r4 = "montageHeaderListener"
            java.lang.String[] r10 = new java.lang.String[]{r14, r10, r7, r5, r4}     // Catch:{ Exception -> 0x0b23 }
            java.util.BitSet r7 = new java.util.BitSet     // Catch:{ Exception -> 0x0b23 }
            r7.<init>(r13)     // Catch:{ Exception -> 0x0b23 }
            X.1kt r4 = new X.1kt     // Catch:{ Exception -> 0x0b23 }
            android.content.Context r5 = r12.A09     // Catch:{ Exception -> 0x0b23 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0b23 }
            X.0zR r5 = r12.A04     // Catch:{ Exception -> 0x0b23 }
            if (r5 == 0) goto L_0x09e9
            java.lang.String r5 = r5.A06     // Catch:{ Exception -> 0x0b23 }
            r4.A07 = r5     // Catch:{ Exception -> 0x0b23 }
        L_0x09e9:
            r7.clear()     // Catch:{ Exception -> 0x0b23 }
            r4.A04 = r9     // Catch:{ Exception -> 0x0b23 }
            r5 = 2
            r7.set(r5)     // Catch:{ Exception -> 0x0b23 }
            r4.A03 = r6     // Catch:{ Exception -> 0x0b23 }
            r5 = 1
            r7.set(r5)     // Catch:{ Exception -> 0x0b23 }
            r4.A07 = r11     // Catch:{ Exception -> 0x0b23 }
            r5 = 4
            r7.set(r5)     // Catch:{ Exception -> 0x0b23 }
            r4.A05 = r11     // Catch:{ Exception -> 0x0b23 }
            r5 = 0
            r7.set(r5)     // Catch:{ Exception -> 0x0b23 }
            r4.A08 = r0     // Catch:{ Exception -> 0x0b23 }
            r0 = 3
            r7.set(r0)     // Catch:{ Exception -> 0x0b23 }
            X.AnonymousClass11F.A0C(r13, r7, r10)     // Catch:{ Exception -> 0x0b23 }
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
            goto L_0x0ad9
        L_0x0a14:
            java.lang.String r5 = "THREAD"
            boolean r5 = r12.equals(r5)     // Catch:{ all -> 0x0b4b }
            if (r5 == 0) goto L_0x0ae0
            java.lang.Object r5 = r8.A03     // Catch:{ all -> 0x0b4b }
            r12 = 1
            if (r5 != 0) goto L_0x0a60
            java.util.concurrent.atomic.AtomicInteger r5 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r5.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r11 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r6 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.threaditem.ThreadItemComponentPlugin"
            r11.A06(r6, r7)     // Catch:{ all -> 0x0b4b }
            X.3eK r6 = r8.A07     // Catch:{ Exception -> 0x0b2c }
            if (r6 != 0) goto L_0x0a32
            goto L_0x0a39
        L_0x0a32:
            java.lang.String r5 = "com.facebook.messaging.threadlist.plugins.inboxviewbinder.InboxViewBinderKillSwitch"
            java.lang.Boolean r5 = r6.isNeeded(r5)     // Catch:{ Exception -> 0x0b2c }
            goto L_0x0a3a
        L_0x0a39:
            r5 = 0
        L_0x0a3a:
            if (r5 == 0) goto L_0x0a41
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0b2c }
            goto L_0x0a50
        L_0x0a41:
            java.lang.Boolean r5 = X.C35491rJ.A00     // Catch:{ Exception -> 0x0b2c }
            if (r5 == 0) goto L_0x0a4a
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0b2c }
            goto L_0x0a50
        L_0x0a4a:
            X.1d0 r5 = r8.A09     // Catch:{ Exception -> 0x0b2c }
            boolean r5 = X.C35491rJ.A00(r5)     // Catch:{ Exception -> 0x0b2c }
        L_0x0a50:
            if (r5 == 0) goto L_0x0a57
            java.lang.Object r5 = X.C27271cv.A00     // Catch:{ Exception -> 0x0b2c }
            r8.A03 = r5     // Catch:{ Exception -> 0x0b2c }
            goto L_0x0a5b
        L_0x0a57:
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ Exception -> 0x0b2c }
            r8.A03 = r5     // Catch:{ Exception -> 0x0b2c }
        L_0x0a5b:
            X.1d0 r5 = r8.A09     // Catch:{ all -> 0x0b4b }
            r5.A02()     // Catch:{ all -> 0x0b4b }
        L_0x0a60:
            java.lang.Object r6 = r8.A03     // Catch:{ all -> 0x0b4b }
            java.lang.Object r5 = X.C27271cv.A01     // Catch:{ all -> 0x0b4b }
            if (r6 != r5) goto L_0x0a67
            r12 = 0
        L_0x0a67:
            if (r12 == 0) goto L_0x0ae0
            java.util.concurrent.atomic.AtomicInteger r4 = X.C27271cv.A02     // Catch:{ all -> 0x0b4b }
            r4.getAndIncrement()     // Catch:{ all -> 0x0b4b }
            X.1d0 r5 = r8.A09     // Catch:{ all -> 0x0b4b }
            java.lang.String r4 = "com.facebook.messaging.threadlist.plugins.implementations.inboxviewbinder.threaditem.ThreadItemComponentPlugin"
            r5.A07(r4, r7, r10)     // Catch:{ all -> 0x0b4b }
            X.0p4 r7 = r8.A08     // Catch:{ Exception -> 0x0b39 }
            X.1BC r6 = r8.A0A     // Catch:{ Exception -> 0x0b39 }
            X.1BY r5 = r8.A0B     // Catch:{ Exception -> 0x0b39 }
            com.google.common.base.Preconditions.checkNotNull(r5)     // Catch:{ Exception -> 0x0b39 }
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r9 = (com.facebook.messaging.inbox2.items.InboxUnitThreadItem) r9     // Catch:{ Exception -> 0x0b39 }
            r4 = 6
            java.lang.String r10 = "callToActionListener"
            java.lang.String r11 = "colorScheme"
            java.lang.String r12 = "item"
            java.lang.String r13 = "itemContentClickListener"
            java.lang.String r14 = "montageListener"
            java.lang.String r15 = "threadItemSwipeActionHandler"
            java.lang.String[] r11 = new java.lang.String[]{r10, r11, r12, r13, r14, r15}     // Catch:{ Exception -> 0x0b39 }
            java.util.BitSet r10 = new java.util.BitSet     // Catch:{ Exception -> 0x0b39 }
            r10.<init>(r4)     // Catch:{ Exception -> 0x0b39 }
            X.1IZ r4 = new X.1IZ     // Catch:{ Exception -> 0x0b39 }
            android.content.Context r12 = r7.A09     // Catch:{ Exception -> 0x0b39 }
            r4.<init>(r12)     // Catch:{ Exception -> 0x0b39 }
            X.0zR r7 = r7.A04     // Catch:{ Exception -> 0x0b39 }
            if (r7 == 0) goto L_0x0aa5
            java.lang.String r7 = r7.A06     // Catch:{ Exception -> 0x0b39 }
            r4.A07 = r7     // Catch:{ Exception -> 0x0b39 }
        L_0x0aa5:
            r10.clear()     // Catch:{ Exception -> 0x0b39 }
            r4.A01 = r9     // Catch:{ Exception -> 0x0b39 }
            r7 = 2
            r10.set(r7)     // Catch:{ Exception -> 0x0b39 }
            r4.A08 = r0     // Catch:{ Exception -> 0x0b39 }
            r0 = 1
            r10.set(r0)     // Catch:{ Exception -> 0x0b39 }
            r4.A04 = r6     // Catch:{ Exception -> 0x0b39 }
            r0 = 3
            r10.set(r0)     // Catch:{ Exception -> 0x0b39 }
            r4.A02 = r6     // Catch:{ Exception -> 0x0b39 }
            r0 = 0
            r10.set(r0)     // Catch:{ Exception -> 0x0b39 }
            r4.A03 = r6     // Catch:{ Exception -> 0x0b39 }
            r0 = 4
            r10.set(r0)     // Catch:{ Exception -> 0x0b39 }
            r4.A05 = r6     // Catch:{ Exception -> 0x0b39 }
            r0 = 5
            r10.set(r0)     // Catch:{ Exception -> 0x0b39 }
            r4.A07 = r5     // Catch:{ Exception -> 0x0b39 }
            r4.A06 = r6     // Catch:{ Exception -> 0x0b39 }
            r0 = 6
            X.AnonymousClass11F.A0C(r0, r10, r11)     // Catch:{ Exception -> 0x0b39 }
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
        L_0x0ad9:
            X.1d0 r0 = r8.A09
            r0.A01()
            goto L_0x0053
        L_0x0ae0:
            X.1d0 r0 = r8.A09
            r0.A01()
            goto L_0x0053
        L_0x0ae7:
            java.lang.NullPointerException r1 = new java.lang.NullPointerException     // Catch:{ all -> 0x0b48 }
            java.lang.String r0 = "If you use @PluginInjectProp, you must put a non-null context in InboxViewBinderInterface.create() as the second parameter"
            r1.<init>(r0)     // Catch:{ all -> 0x0b48 }
            throw r1     // Catch:{ all -> 0x0b48 }
        L_0x0aef:
            r1 = move-exception
            java.lang.Object r0 = X.C27271cv.A01     // Catch:{ all -> 0x0af5 }
            r8.A00 = r0     // Catch:{ all -> 0x0af5 }
            throw r1     // Catch:{ all -> 0x0af5 }
        L_0x0af5:
            r1 = move-exception
            goto L_0x0b42
        L_0x0af7:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0af9 }
        L_0x0af9:
            r1 = move-exception
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
            goto L_0x0b47
        L_0x0b00:
            r1 = move-exception
            java.lang.Object r0 = X.C27271cv.A01     // Catch:{ all -> 0x0b06 }
            r8.A01 = r0     // Catch:{ all -> 0x0b06 }
            throw r1     // Catch:{ all -> 0x0b06 }
        L_0x0b06:
            r1 = move-exception
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A02()     // Catch:{ all -> 0x0b4b }
            goto L_0x0b47
        L_0x0b0d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0b0f }
        L_0x0b0f:
            r1 = move-exception
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
            goto L_0x0b47
        L_0x0b16:
            r1 = move-exception
            java.lang.Object r0 = X.C27271cv.A01     // Catch:{ all -> 0x0b1c }
            r8.A02 = r0     // Catch:{ all -> 0x0b1c }
            throw r1     // Catch:{ all -> 0x0b1c }
        L_0x0b1c:
            r1 = move-exception
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A02()     // Catch:{ all -> 0x0b4b }
            goto L_0x0b47
        L_0x0b23:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0b25 }
        L_0x0b25:
            r1 = move-exception
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
            goto L_0x0b47
        L_0x0b2c:
            r1 = move-exception
            java.lang.Object r0 = X.C27271cv.A01     // Catch:{ all -> 0x0b32 }
            r8.A03 = r0     // Catch:{ all -> 0x0b32 }
            throw r1     // Catch:{ all -> 0x0b32 }
        L_0x0b32:
            r1 = move-exception
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A02()     // Catch:{ all -> 0x0b4b }
            goto L_0x0b47
        L_0x0b39:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0b3b }
        L_0x0b3b:
            r1 = move-exception
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A00()     // Catch:{ all -> 0x0b4b }
            goto L_0x0b47
        L_0x0b42:
            X.1d0 r0 = r8.A09     // Catch:{ all -> 0x0b4b }
            r0.A02()     // Catch:{ all -> 0x0b4b }
        L_0x0b47:
            throw r1     // Catch:{ all -> 0x0b4b }
        L_0x0b48:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0b4b }
            throw r0     // Catch:{ all -> 0x0b4b }
        L_0x0b4b:
            r1 = move-exception
            X.1d0 r0 = r8.A09
            r0.A01()
            throw r1
        L_0x0b52:
            X.1Jg r2 = (X.C21931Jg) r2
            java.lang.Object r0 = r2.A01
            X.1Hr r0 = (X.C21531Hr) r0
            java.lang.Object r5 = r2.A00
            X.1Hr r5 = (X.C21531Hr) r5
            com.facebook.messaging.inbox2.items.InboxUnitItem r2 = r0.A00
            X.1IY r1 = r2.A06()
            com.facebook.messaging.inbox2.items.InboxUnitItem r0 = r5.A00
            X.1IY r0 = r0.A06()
            if (r1 != r0) goto L_0x0b81
            com.facebook.messaging.inbox2.items.InboxTrackableItem r0 = r2.A07()
            long r3 = r0.ArZ()
            com.facebook.messaging.inbox2.items.InboxUnitItem r0 = r5.A00
            com.facebook.messaging.inbox2.items.InboxTrackableItem r0 = r0.A07()
            long r1 = r0.ArZ()
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r1 = 1
            if (r0 == 0) goto L_0x0b82
        L_0x0b81:
            r1 = 0
        L_0x0b82:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            return r0
        L_0x0b87:
            X.1Jg r2 = (X.C21931Jg) r2
            java.lang.Object r1 = r2.A01
            X.1Hr r1 = (X.C21531Hr) r1
            java.lang.Object r0 = r2.A00
            X.1Hr r0 = (X.C21531Hr) r0
            com.facebook.messaging.inbox2.items.InboxUnitItem r1 = r1.A00
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r1 = (com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem) r1
            com.facebook.messaging.inbox2.items.InboxUnitItem r0 = r0.A00
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r0 = (com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem) r0
            java.lang.Integer r2 = r1.A0I()
            java.lang.Integer r1 = r0.A0I()
            r0 = 0
            if (r2 != r1) goto L_0x0ba5
            r0 = 1
        L_0x0ba5:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16060wQ.AXa(X.10N, java.lang.Object):java.lang.Object");
    }
}
