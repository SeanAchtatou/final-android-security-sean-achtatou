package X;

/* renamed from: X.0GI  reason: invalid class name */
public final class AnonymousClass0GI {
    public long A00;
    public long A01;
    public Integer A02;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass0GI r8 = (AnonymousClass0GI) obj;
            return this.A00 == r8.A00 && this.A01 == r8.A01 && this.A02 == r8.A02;
        }
        return false;
    }

    public void A00(AnonymousClass0GI r3) {
        this.A02 = r3.A02;
        this.A00 = r3.A00;
        this.A01 = r3.A01;
    }

    public void A01(AnonymousClass0GI r5, AnonymousClass0GI r6) {
        if (r5 == null) {
            r6.A00(this);
            return;
        }
        if (r5.A02 != this.A02) {
            AnonymousClass0KZ.A00("AppWakeupMetrics", AnonymousClass08S.A0S("Sum only allowed for similar wakeups: ", toString(), ", ", r5.toString()), null);
        }
        r6.A02 = this.A02;
        r6.A00 = this.A00 + r5.A00;
        r6.A01 = this.A01 + r5.A01;
    }

    public int hashCode() {
        int i;
        Integer num = this.A02;
        if (num != null) {
            i = AnonymousClass0KW.A00(num).hashCode() + num.intValue();
        } else {
            i = 0;
        }
        long j = this.A00;
        long j2 = this.A01;
        return (((i * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("{reason=");
        Integer num = this.A02;
        if (num != null) {
            str = AnonymousClass0KW.A00(num);
        } else {
            str = "null";
        }
        sb.append(str);
        sb.append(", count=");
        sb.append(this.A00);
        sb.append(", wakeupTimeMs=");
        sb.append(this.A01);
        sb.append("}");
        return sb.toString();
    }

    public AnonymousClass0GI() {
        this.A00 = 0;
        this.A01 = 0;
    }

    public AnonymousClass0GI(Integer num, long j, long j2) {
        this.A02 = num;
        this.A00 = j;
        this.A01 = j2;
    }
}
