package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import io.card.payment.BuildConfig;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0iw  reason: invalid class name and case insensitive filesystem */
public final class C09940iw {
    private static final Set A01 = new HashSet();
    public final FbSharedPreferences A00;

    public static final C09940iw A00(AnonymousClass1XY r1) {
        return new C09940iw(r1);
    }

    public boolean A01() {
        boolean z;
        boolean z2;
        boolean z3 = false;
        if (this.A00.BFQ()) {
            FbSharedPreferences fbSharedPreferences = this.A00;
            AnonymousClass1Y7 r1 = C25951af.A0A;
            z = fbSharedPreferences.Aep(r1, false);
            if (!this.A00.BFQ()) {
                z2 = false;
            } else {
                String[] split = this.A00.B4F((AnonymousClass1Y7) r1.A09("_ttl"), BuildConfig.FLAVOR).split(":");
                if (split.length == 2) {
                    try {
                        long parseLong = Long.parseLong(split[0]);
                        long parseLong2 = Long.parseLong(split[1]);
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis < parseLong || currentTimeMillis - parseLong >= parseLong2) {
                            z2 = true;
                        }
                    } catch (NumberFormatException unused) {
                    }
                }
                z2 = false;
            }
            if (z2) {
                C30281hn edit = this.A00.edit();
                edit.putBoolean(C25951af.A0A, false);
                edit.commit();
            }
        } else {
            z = false;
            z2 = false;
        }
        if (z && !z2) {
            z3 = true;
        }
        Set set = A01;
        AnonymousClass1Y7 r0 = C25951af.A0A;
        if (z3) {
            set.add(r0);
        } else if (set.contains(r0)) {
            return true;
        }
        return z3;
    }

    public boolean A02() {
        boolean z = false;
        if (this.A00.BFQ()) {
            z = this.A00.Aep(C25951af.A0C, false);
        }
        Set set = A01;
        AnonymousClass1Y7 r0 = C25951af.A0C;
        if (z) {
            set.add(r0);
        } else if (set.contains(r0)) {
            return true;
        }
        return z;
    }

    public C09940iw(AnonymousClass1XY r2) {
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }
}
