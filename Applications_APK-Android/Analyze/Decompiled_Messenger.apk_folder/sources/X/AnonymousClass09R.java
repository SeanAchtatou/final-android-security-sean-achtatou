package X;

/* renamed from: X.09R  reason: invalid class name */
public final class AnonymousClass09R {
    public final StringBuilder A00;

    public static char A00(char c) {
        if (c == 0 || c == 13 || c == ';' || c == '|' || c == 9 || c == 10) {
            return ' ';
        }
        return c;
    }

    public void A01(int i) {
        StringBuilder sb = this.A00;
        sb.append('|');
        sb.append(i);
    }

    public void A03(String str) {
        this.A00.append('|');
        A02(str);
    }

    public void A04(String[] strArr, int i) {
        StringBuilder sb = this.A00;
        sb.append('|');
        for (int i2 = 1; i2 < i; i2 += 2) {
            String str = strArr[i2 - 1];
            String str2 = strArr[i2];
            sb.append(str);
            sb.append('=');
            sb.append(str2);
            if (i2 < i - 1) {
                sb.append(';');
            }
        }
    }

    public String toString() {
        return this.A00.toString();
    }

    public AnonymousClass09R(char c) {
        StringBuilder sb = new StringBuilder(1024);
        this.A00 = sb;
        sb.append(c);
    }

    public void A02(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            this.A00.append(A00(str.charAt(i)));
        }
    }
}
