package X;

import android.content.Context;
import android.content.res.Resources;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1ZC  reason: invalid class name */
public final class AnonymousClass1ZC extends AnonymousClass0UV {
    private static volatile C14650tk A00;

    public static final C14650tk A02(AnonymousClass1XY r5) {
        if (A00 == null) {
            synchronized (C14650tk.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A00 = new C14650tk(AnonymousClass1ZD.A00(applicationInjector), AnonymousClass0ZS.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final Resources A00(AnonymousClass1XY r1) {
        Context A002 = AnonymousClass1YA.A00(r1);
        if (!(A002 instanceof AnonymousClass005)) {
            return A002.getResources();
        }
        return ((AnonymousClass005) A002).AeF();
    }

    public static final C05760aH A01(AnonymousClass1XY r0) {
        return AnonymousClass0YX.A04(r0);
    }
}
