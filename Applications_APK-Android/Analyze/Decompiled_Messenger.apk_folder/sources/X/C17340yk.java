package X;

import android.content.Intent;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.fbservice.service.ServiceException;

/* renamed from: X.0yk  reason: invalid class name and case insensitive filesystem */
public final class C17340yk extends C28791fN {
    public static final String __redex_internal_original_name = "com.facebook.fbservice.ops.DefaultBlueServiceOperation$5";
    public final /* synthetic */ C27171cl A00;
    public final /* synthetic */ OperationResult A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C17340yk(C27171cl r1, String str, String str2, OperationResult operationResult) {
        super(str, str2);
        this.A00 = r1;
        this.A01 = operationResult;
    }

    public void run() {
        Throwable serviceException;
        boolean z;
        if (!this.A00.BEO()) {
            C27171cl r3 = this.A00;
            OperationResult operationResult = this.A01;
            C156107Jr r0 = r3.A05;
            if (r0 != null) {
                r0.CI0();
            }
            if (operationResult.success) {
                r3.A0L.A03(operationResult);
            } else {
                if (r3.A0D != 1 || (serviceException = operationResult.errorThrowable) == null) {
                    serviceException = new ServiceException(operationResult);
                }
                C11480nE r02 = (C11480nE) AnonymousClass065.A00(r3.A0E, C11480nE.class);
                if (r02 != null) {
                    z = r02.BB8(serviceException);
                } else {
                    r3.A0N.get();
                    if (AnonymousClass7NS.A01(serviceException)) {
                        r3.A0G.C4x(new Intent(BlueServiceOperationFactory.BLUESERVICE_NO_AUTH));
                    }
                    z = false;
                }
                if (!z) {
                    r3.A0L.setException(serviceException);
                }
            }
            r3.A01.A03();
        }
    }
}
