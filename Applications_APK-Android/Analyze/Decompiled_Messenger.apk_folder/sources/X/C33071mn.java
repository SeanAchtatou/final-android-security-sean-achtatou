package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.facebook.acra.ACRA;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1mn  reason: invalid class name and case insensitive filesystem */
public final class C33071mn {
    public static final C29631gj A01 = C29631gj.A21;
    public static final C29631gj A02 = C29631gj.A22;
    public static final C29631gj A03 = C29631gj.A24;
    public static final C29631gj A04 = C29631gj.A25;
    public static final Integer A05 = AnonymousClass07B.A00;
    public AnonymousClass0UN A00;

    public static final C33071mn A00(AnonymousClass1XY r1) {
        return new C33071mn(r1);
    }

    public C33071mn(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public Drawable A01(Context context, C69273Wk r6) {
        C29631gj r3;
        switch (r6.ordinal()) {
            case 0:
                r3 = A03;
                break;
            case 1:
                r3 = A04;
                break;
            case 2:
                r3 = A01;
                break;
            case 3:
            case 4:
            case 5:
                r3 = C29631gj.A1B;
                break;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 8:
                r3 = A02;
                break;
            default:
                throw new IllegalArgumentException(AnonymousClass08S.A0P("No drawable binding defined for ", r6.name(), "! Make sure you specify a drawable in M4DeliveryStateDrawableBinder."));
        }
        if (r3 == C29631gj.A1B) {
            return null;
        }
        return AnonymousClass01R.A03(context, ((AnonymousClass1MT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJa, this.A00)).A03(r3, A05));
    }
}
