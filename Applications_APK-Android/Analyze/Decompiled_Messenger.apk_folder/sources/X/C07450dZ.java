package X;

/* renamed from: X.0dZ  reason: invalid class name and case insensitive filesystem */
public class C07450dZ extends C10880l0 {
    public final String A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C07450dZ(java.lang.Class r13, int r14, long r15, boolean r17, int r18, java.lang.String r19, java.lang.String r20, java.lang.String r21, long r22) {
        /*
            r12 = this;
            r0 = r12
            r6 = 0
            r8 = r19
            r7 = r18
            r5 = r17
            r2 = r14
            r10 = r22
            r3 = r15
            r9 = r20
            r1 = r13
            r0.<init>(r1, r2, r3, r5, r6, r7, r8, r9, r10)
            r0 = r21
            r12.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07450dZ.<init>(java.lang.Class, int, long, boolean, int, java.lang.String, java.lang.String, java.lang.String, long):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C07450dZ(java.lang.Class r13, int r14, long r15, boolean r17, boolean r18, int r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, long r23) {
        /*
            r12 = this;
            r0 = r12
            r7 = r19
            r6 = r18
            r5 = r17
            r3 = r15
            r10 = r23
            r9 = r21
            r2 = r14
            r8 = r20
            r1 = r13
            r0.<init>(r1, r2, r3, r5, r6, r7, r8, r9, r10)
            r0 = r22
            r12.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07450dZ.<init>(java.lang.Class, int, long, boolean, boolean, int, java.lang.String, java.lang.String, java.lang.String, long):void");
    }
}
