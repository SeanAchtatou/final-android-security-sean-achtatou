package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0kJ  reason: invalid class name and case insensitive filesystem */
public final class C10510kJ implements C10520kK {
    private static volatile C10510kJ A01;
    private AnonymousClass0UN A00;

    public AnonymousClass10B Aio() {
        return null;
    }

    public static final C10510kJ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C10510kJ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C10510kJ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public C09240gn Aik() {
        return (C09240gn) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6u, this.A00);
    }

    public C08850g3 Aim() {
        return (C08850g3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6I, this.A00);
    }

    private C10510kJ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
