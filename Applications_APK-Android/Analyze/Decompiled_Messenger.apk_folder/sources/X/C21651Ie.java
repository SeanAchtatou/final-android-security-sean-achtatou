package X;

import android.view.View;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

/* renamed from: X.1Ie  reason: invalid class name and case insensitive filesystem */
public final class C21651Ie {
    public static void A02(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, AnonymousClass1I9 r2) {
        if (r2 != null) {
            accessibilityNodeInfoCompat.A0J(r2.mValue);
            if (r2.equals(AnonymousClass1I9.A0E)) {
                accessibilityNodeInfoCompat.A0S(true);
            }
        }
    }

    public static AnonymousClass1I9 A00(View view) {
        AnonymousClass8NI A05;
        AccessibilityNodeInfoCompat A01 = AccessibilityNodeInfoCompat.A01();
        C15320v6.onInitializeAccessibilityNodeInfo(view, A01);
        AnonymousClass1I9 A00 = AnonymousClass1I9.A00((String) A01.A02.getClassName());
        if (A00.equals(AnonymousClass1I9.A0E) || A00.equals(AnonymousClass1I9.A0D)) {
            A00 = A01.A02.isClickable() ? AnonymousClass1I9.A0E : AnonymousClass1I9.A0D;
        } else if (A00.equals(AnonymousClass1I9.A0H) && (A05 = A01.A05()) != null) {
            A00 = (A05.A02() <= 1 || A05.A01() <= 1) ? AnonymousClass1I9.A0G : AnonymousClass1I9.A0A;
        }
        A01.A08();
        return A00;
    }

    public static void A01(View view, AnonymousClass1I9 r2) {
        if (!C15320v6.hasAccessibilityDelegate(view) && r2 != null) {
            C15320v6.setAccessibilityDelegate(view, new AnonymousClass1KC(r2));
        }
    }
}
