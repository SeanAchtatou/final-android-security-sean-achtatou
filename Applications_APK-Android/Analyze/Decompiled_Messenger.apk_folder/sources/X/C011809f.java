package X;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.profilo.ipc.IProfiloMultiProcessTraceListener;
import com.facebook.profilo.ipc.IProfiloMultiProcessTraceService;
import com.facebook.profilo.multiprocess.ProfiloIPCParcelable;

/* renamed from: X.09f  reason: invalid class name and case insensitive filesystem */
public final class C011809f extends AnonymousClass07D {
    public IProfiloMultiProcessTraceService A00;

    public ProfiloIPCParcelable A00() {
        return null;
    }

    public String A01() {
        return "com.facebook.profilo.NON_MAIN_PROCESS_STARTED_V3";
    }

    public String A02() {
        return "com.facebook.profilo.MAIN_PROCESS_STARTED_V3";
    }

    public C011809f(IProfiloMultiProcessTraceService iProfiloMultiProcessTraceService) {
        this.A00 = iProfiloMultiProcessTraceService;
    }

    public void onReceive(Context context, Intent intent) {
        IBinder iBinder;
        int A01 = C000700l.A01(1071571908);
        if (!intent.getAction().equals(A01())) {
            C000700l.A0D(intent, -752075508, A01);
        } else if (intent.getIntExtra("pid", -1) == -1) {
            C000700l.A0D(intent, 1008543834, A01);
        } else {
            ProfiloIPCParcelable profiloIPCParcelable = (ProfiloIPCParcelable) intent.getParcelableExtra("parcel");
            if (profiloIPCParcelable == null || (iBinder = profiloIPCParcelable.A00) == null) {
                C000700l.A0D(intent, 1124539031, A01);
                return;
            }
            try {
                IProfiloMultiProcessTraceListener.Stub.A00(iBinder).Bl2(this.A00);
            } catch (RemoteException e) {
                Log.e("ProfiloMainBroadcastReceiver", "Failed to call method onReceive on listener.  Listener's process is dead", e);
            } catch (Exception e2) {
                Log.e("ProfiloMainBroadcastReceiver", "Failed to retrieve listener from parcel", e2);
            }
            C000700l.A0D(intent, -1756626895, A01);
        }
    }
}
