package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1qf  reason: invalid class name and case insensitive filesystem */
public final class C35091qf {
    public static C35141qk A01;
    private static volatile C35091qf A02;
    public AnonymousClass0UN A00;

    public static C188608pF A00(C35091qf r4) {
        return new C188608pF(new C1746683w((C06230bA) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BTX, r4.A00)).A00);
    }

    public static final C35091qf A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C35091qf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C35091qf(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C35091qf(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
