package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadRtcCallInfoData;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.1Qq  reason: invalid class name and case insensitive filesystem */
public final class C23731Qq implements C23751Qs {
    public final /* synthetic */ AnonymousClass1TY A00;

    public C23731Qq(AnonymousClass1TY r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void BQf(Context context, ThreadSummary threadSummary) {
        String $const$string;
        String str;
        AnonymousClass1TY r3 = this.A00;
        ThreadSummary threadSummary2 = threadSummary;
        ThreadRtcCallInfoData threadRtcCallInfoData = threadSummary.A0h;
        boolean z = false;
        if (threadRtcCallInfoData.A00 == "VIDEO_GROUP_CALL") {
            z = true;
        }
        Context context2 = context;
        if (threadRtcCallInfoData != null) {
            ThreadKey threadKey = threadSummary.A0S;
            if (threadKey.A0L()) {
                C163127gW r32 = (C163127gW) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ApQ, r3.A00);
                if (z) {
                    str = "multiway_join_inbox_cta_video";
                } else {
                    str = "multiway_join_inbox_cta";
                }
                r32.A02(threadKey, threadSummary2, null, z, str, null, context2);
                return;
            }
        }
        Uri parse = Uri.parse(StringFormatUtil.formatStrLocaleSafe(C52652jT.A0B, Long.valueOf(threadSummary.A0S.A01)));
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(parse);
        intent.setFlags(335544320);
        intent.putExtra(AnonymousClass24B.$const$string(124), true);
        AnonymousClass15w A0G = C02200Dj.A00().A0G();
        if (z) {
            $const$string = AnonymousClass80H.$const$string(301);
        } else {
            $const$string = AnonymousClass80H.$const$string(299);
        }
        A0G.A00 = $const$string;
        A0G.A0B(intent, context);
    }
}
