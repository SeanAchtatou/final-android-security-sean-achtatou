package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1ZI  reason: invalid class name */
public final class AnonymousClass1ZI extends AnonymousClass0UV {
    private static volatile AnonymousClass1ZJ A00;

    public static final AnonymousClass1ZJ A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass1ZJ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass1ZJ();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
