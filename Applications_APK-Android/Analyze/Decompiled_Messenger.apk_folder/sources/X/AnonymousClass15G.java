package X;

import com.facebook.common.iopri.IoPriority;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.15G  reason: invalid class name */
public final class AnonymousClass15G implements AnonymousClass1YQ {
    private static volatile AnonymousClass15G A0F;
    public int A00;
    public int A01;
    public int A02 = Integer.MIN_VALUE;
    public int A03 = Integer.MIN_VALUE;
    public C73263fn A04;
    public C73263fn A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    private boolean A0B;
    private boolean A0C;
    private boolean A0D;
    private final C25051Yd A0E;

    public String getSimpleName() {
        return "IoPriorityController";
    }

    public static final AnonymousClass15G A00(AnonymousClass1XY r4) {
        if (A0F == null) {
            synchronized (AnonymousClass15G.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0F, r4);
                if (A002 != null) {
                    try {
                        A0F = new AnonymousClass15G(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0F;
    }

    public static void A01(AnonymousClass15G r3) {
        boolean z;
        if (!r3.A0B) {
            A03(r3);
            int i = r3.A02;
            if (r3.A07 && !r3.A06 && i != Integer.MIN_VALUE && (z = IoPriority.sLibLoaded)) {
                if (z) {
                    IoPriority.nativeGetIoPriority(i);
                }
                IoPriority.setIoPriority(i, r3.A04, r3.A00);
                r3.A06 = true;
            }
        }
    }

    public static void A02(AnonymousClass15G r3) {
        boolean z;
        if (!r3.A0B) {
            A03(r3);
            int i = r3.A03;
            if (r3.A09 && !r3.A08 && i != Integer.MIN_VALUE && (z = IoPriority.sLibLoaded)) {
                if (z) {
                    IoPriority.nativeGetIoPriority(i);
                }
                IoPriority.setIoPriority(i, r3.A05, r3.A01);
                r3.A08 = true;
            }
        }
    }

    public static void A03(AnonymousClass15G r4) {
        if (!r4.A0A && IoPriority.sLibLoaded) {
            r4.A0E.Aeo(285473591989953L, false);
            C73263fn.A00(r4.A0E.AqL(566948568631083L, 0));
            r4.A0E.AqL(566948568762156L, 0);
            C73263fn.A00(r4.A0E.AqL(566948568827693L, 0));
            r4.A07 = r4.A0E.Aeo(285473591334587L, false);
            r4.A04 = C73263fn.A00(r4.A0E.AqL(566948567975719L, 0));
            r4.A00 = r4.A0E.AqL(566948568106792L, 0);
            r4.A09 = r4.A0E.Aeo(285473591531196L, false);
            r4.A05 = C73263fn.A00(r4.A0E.AqL(566948568172329L, 0));
            r4.A01 = r4.A0E.AqL(566948568303402L, 0);
            r4.A0B = r4.A0E.Aeo(285473591727806L, false);
            r4.A0E.Aeo(285473591662269L, false);
            r4.A0C = r4.A0E.Aeo(285473591793343L, false);
            r4.A0D = r4.A0E.Aeo(285473591858880L, false);
            if (r4.A0C) {
                try {
                    Class cls = IoPriority.IO_PRI_LOADER_CLS;
                    if (cls != null) {
                        cls.getDeclaredMethod("enableArtGcHack", new Class[0]).invoke(null, new Object[0]);
                    }
                } catch (Exception unused) {
                }
            }
            if (r4.A0D) {
                try {
                    Class cls2 = IoPriority.IO_PRI_LOADER_CLS;
                    if (cls2 != null) {
                        cls2.getDeclaredMethod("enableDalvikGcHack", new Class[0]).invoke(null, new Object[0]);
                    }
                } catch (Exception unused2) {
                }
            }
            r4.A0A = true;
        }
    }

    private AnonymousClass15G(AnonymousClass1XY r2) {
        this.A0E = AnonymousClass0WT.A00(r2);
    }

    public void init() {
        int A032 = C000700l.A03(1417296494);
        A03(this);
        A01(this);
        A02(this);
        C000700l.A09(-2119934916, A032);
    }
}
