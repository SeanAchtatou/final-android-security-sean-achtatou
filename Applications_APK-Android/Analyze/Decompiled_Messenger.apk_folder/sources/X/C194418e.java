package X;

import com.facebook.acra.LogCatCollector;
import java.nio.charset.Charset;

/* renamed from: X.18e  reason: invalid class name and case insensitive filesystem */
public final class C194418e {
    private static final Charset A01 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    public final byte[] A00;

    static {
        Charset.forName("UTF-16");
    }

    public static C194418e A00(String str) {
        return new C194418e(str.getBytes(A01));
    }

    private C194418e(byte[] bArr) {
        this.A00 = bArr;
    }
}
