package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.view.Window;
import com.facebook.rtc.services.RtcVideoChatHeadService;
import java.lang.ref.WeakReference;

/* renamed from: X.22X  reason: invalid class name */
public final class AnonymousClass22X implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ C159867az A00;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public AnonymousClass22X(C159867az r1) {
        this.A00 = r1;
    }

    private static boolean A00(Activity activity) {
        if (!(activity instanceof C11580nP) || ((C11580nP) activity).Agn() != AnonymousClass07B.A0N) {
            return true;
        }
        return false;
    }

    public void onActivityStopped(Activity activity) {
        WeakReference weakReference;
        C159867az r2 = this.A00;
        r2.A0Q = false;
        if (activity instanceof C168427pq) {
            r2.A0U = false;
        }
        if (A00(activity) && (weakReference = r2.A0K) != null && weakReference.get() == activity.getWindow()) {
            this.A00.A0K = null;
        }
    }

    public void onActivityPaused(Activity activity) {
        RtcVideoChatHeadService rtcVideoChatHeadService;
        Window window;
        C160067bJ r0;
        if (A00(activity) && (rtcVideoChatHeadService = this.A00.A0E) != null) {
            Window window2 = activity.getWindow();
            C162207f0 r1 = rtcVideoChatHeadService.A0X;
            if (r1 != null && !r1.A02 && window2 != null) {
                WeakReference weakReference = r1.A01;
                if (weakReference == null) {
                    window = null;
                } else {
                    window = (Window) weakReference.get();
                }
                if (window == window2) {
                    r1.A04 = false;
                    if (r1.A03 && (r0 = r1.A00) != null) {
                        RtcVideoChatHeadService.A0A(r0.A00);
                    }
                }
            }
        }
    }

    public void onActivityResumed(Activity activity) {
        RtcVideoChatHeadService rtcVideoChatHeadService;
        if (A00(activity)) {
            C159867az r4 = this.A00;
            if (r4.A0o.Aem(286852275772683L)) {
                ((C161497dm) AnonymousClass1XX.A02(53, AnonymousClass1Y3.A6E, r4.A08)).A00 = C162577fb.GRID;
            } else {
                ((C161497dm) AnonymousClass1XX.A02(53, AnonymousClass1Y3.A6E, r4.A08)).A00 = C162577fb.NORMAL;
            }
            if (r4.A1U() && (rtcVideoChatHeadService = r4.A0E) != null) {
                rtcVideoChatHeadService.A0o(((C161497dm) AnonymousClass1XX.A02(53, AnonymousClass1Y3.A6E, r4.A08)).A00);
            }
            RtcVideoChatHeadService rtcVideoChatHeadService2 = this.A00.A0E;
            if (rtcVideoChatHeadService2 != null) {
                rtcVideoChatHeadService2.A0n(activity.getWindow());
            }
        }
    }

    public void onActivityStarted(Activity activity) {
        if (A00(activity)) {
            Window window = activity.getWindow();
            this.A00.A0K = new WeakReference(window);
            C159867az r2 = this.A00;
            r2.A0Q = true;
            if (activity instanceof C168427pq) {
                r2.A0U = true;
            }
        }
    }
}
