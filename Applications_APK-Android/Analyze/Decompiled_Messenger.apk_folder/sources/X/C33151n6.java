package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1n6  reason: invalid class name and case insensitive filesystem */
public final class C33151n6 implements C23831Rc {
    private static C05540Zi A01;
    private final C14900uK A00;

    public static final C33151n6 A00(AnonymousClass1XY r4) {
        C33151n6 r0;
        synchronized (C33151n6.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C33151n6((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C33151n6) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001b, code lost:
        if (r5.A0o.isEmpty() != false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x004e, code lost:
        if (X.C421828p.VIDEO.equals(((com.facebook.ui.media.attachments.model.MediaResource) ((com.facebook.messaging.model.messages.Message) r5.A0o.get(0)).A0b.get(0)).A0L) == false) goto L_0x0050;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C49252bu B3Q(com.facebook.messaging.model.threads.ThreadSummary r5) {
        /*
            r4 = this;
            X.0uK r0 = r4.A00
            int r2 = X.AnonymousClass1Y3.BRG
            X.0UN r1 = r0.A00
            r0 = 10
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0yl r0 = (X.C17350yl) r0
            boolean r0 = r0.A08()
            if (r0 == 0) goto L_0x001d
            com.google.common.collect.ImmutableList r0 = r5.A0o
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x001e
        L_0x001d:
            r0 = 0
        L_0x001e:
            if (r0 == 0) goto L_0x0059
            X.2bu r3 = new X.2bu
            com.google.common.collect.ImmutableList r0 = r5.A0o
            r2 = 0
            java.lang.Object r0 = r0.get(r2)
            com.facebook.messaging.model.messages.Message r0 = (com.facebook.messaging.model.messages.Message) r0
            com.google.common.collect.ImmutableList r0 = r0.A0b
            boolean r0 = X.C013509w.A01(r0)
            if (r0 == 0) goto L_0x0050
            X.28p r1 = X.C421828p.VIDEO
            com.google.common.collect.ImmutableList r0 = r5.A0o
            java.lang.Object r0 = r0.get(r2)
            com.facebook.messaging.model.messages.Message r0 = (com.facebook.messaging.model.messages.Message) r0
            com.google.common.collect.ImmutableList r0 = r0.A0b
            java.lang.Object r0 = r0.get(r2)
            com.facebook.ui.media.attachments.model.MediaResource r0 = (com.facebook.ui.media.attachments.model.MediaResource) r0
            X.28p r0 = r0.A0L
            boolean r0 = r1.equals(r0)
            r1 = 2131833227(0x7f11318b, float:1.929953E38)
            if (r0 != 0) goto L_0x0053
        L_0x0050:
            r1 = 2131833226(0x7f11318a, float:1.9299528E38)
        L_0x0053:
            X.1JZ r0 = X.AnonymousClass1JZ.A02
            r3.<init>(r1, r0)
            return r3
        L_0x0059:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33151n6.B3Q(com.facebook.messaging.model.threads.ThreadSummary):X.2bu");
    }

    private C33151n6(AnonymousClass1XY r2) {
        this.A00 = C14900uK.A00(r2);
    }
}
