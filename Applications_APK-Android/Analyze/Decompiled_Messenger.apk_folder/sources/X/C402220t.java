package X;

import java.lang.reflect.Array;
import java.util.ArrayList;

/* renamed from: X.20t  reason: invalid class name and case insensitive filesystem */
public final class C402220t extends AnonymousClass397 {
    public static final C637638p A02 = new C640339s();
    private final AnonymousClass397 A00;
    private final Class A01;

    public void write(AnonymousClass3DQ r5, Object obj) {
        if (obj == null) {
            r5.A0A();
            return;
        }
        r5.A06();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.A00.write(r5, Array.get(obj, i));
        }
        r5.A08();
    }

    public C402220t(C637238l r2, AnonymousClass397 r3, Class cls) {
        this.A00 = new AnonymousClass3A7(r2, r3, cls);
        this.A01 = cls;
    }

    public Object read(C640839x r6) {
        if (r6.A0F() == AnonymousClass07B.A0p) {
            r6.A0O();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        r6.A0K();
        while (r6.A0Q()) {
            arrayList.add(this.A00.read(r6));
        }
        r6.A0M();
        int size = arrayList.size();
        Object newInstance = Array.newInstance(this.A01, size);
        for (int i = 0; i < size; i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }
}
