package X;

import com.google.common.base.Preconditions;

/* renamed from: X.1Xy  reason: invalid class name and case insensitive filesystem */
public final class C25001Xy {
    public static void A00(int i, String str) {
        if (i < 0) {
            throw new IllegalArgumentException(AnonymousClass08S.A0L(str, " cannot be negative but was: ", i));
        }
    }

    public static void A01(int i, String str) {
        if (i <= 0) {
            throw new IllegalArgumentException(AnonymousClass08S.A0L(str, " must be positive but was: ", i));
        }
    }

    public static void A02(long j, String str) {
        if (j < 0) {
            throw new IllegalArgumentException(AnonymousClass08S.A0O(str, " cannot be negative but was: ", j));
        }
    }

    public static void A03(Object obj, Object obj2) {
        if (obj == null) {
            throw new NullPointerException("null key in entry: null=" + obj2);
        } else if (obj2 == null) {
            throw new NullPointerException("null value in entry: " + obj + "=null");
        }
    }

    public static void A04(boolean z) {
        Preconditions.checkState(z, "no calls to next() since the last call to remove()");
    }
}
