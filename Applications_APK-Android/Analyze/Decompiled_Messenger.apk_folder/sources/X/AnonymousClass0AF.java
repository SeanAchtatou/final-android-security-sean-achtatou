package X;

import android.os.SystemClock;

/* renamed from: X.0AF  reason: invalid class name */
public final class AnonymousClass0AF implements AnonymousClass0AG {
    public final /* synthetic */ AnonymousClass0AD A00;

    public AnonymousClass0AF(AnonymousClass0AD r1) {
        this.A00 = r1;
    }

    public void AYn(String str, String str2, Throwable th) {
        this.A00.A0X(str, str2, th);
    }

    public void BTa() {
        AnonymousClass0AD.A02(this.A00);
    }

    public void BTc() {
        AnonymousClass0AD r2 = this.A00;
        r2.A00 = SystemClock.elapsedRealtime();
        AnonymousClass0AD.A02(r2);
    }

    public void BTg(C01540Aq r3) {
        AnonymousClass0AD r1 = this.A00;
        if (r3.A02()) {
            r1.A0T((AnonymousClass0SB) r3.A01());
        }
        AnonymousClass0AD.A02(r1);
    }

    public void BV6() {
        this.A00.A0N();
    }

    public void Bf4(C02020Cn r2) {
        this.A00.A0V(r2);
    }

    public void BkT(String str, byte[] bArr, int i, long j, AnonymousClass0CU r13) {
        this.A00.A0Y(str, bArr, i, j, r13);
    }

    public void C2f(String str, long j, boolean z) {
        this.A00.A0W(str, j, z);
    }

    public boolean CDh() {
        return this.A00.A0Z();
    }

    public void onConnectSent() {
        AnonymousClass0AD.A02(this.A00);
    }

    public void BSZ(Throwable th) {
    }

    public void BfA(String str, int i) {
    }
}
