package X;

import android.net.Uri;

/* renamed from: X.1rk  reason: invalid class name and case insensitive filesystem */
public final class C35741rk {
    public final int A00;
    public final int A01;
    public final Uri A02;
    public final Object A03;

    public C35741rk(Uri uri, Object obj, int i, int i2) {
        this.A02 = uri;
        this.A03 = obj;
        this.A01 = i;
        this.A00 = i2;
    }
}
