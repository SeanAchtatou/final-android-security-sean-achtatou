package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ab  reason: invalid class name and case insensitive filesystem */
public final class C25911ab implements C25921ac {
    private static volatile C25911ab A03;
    public final AnonymousClass06B A00;
    public final FbSharedPreferences A01;
    private volatile C09420hL A02;

    private synchronized C09420hL A00() {
        C09420hL r4;
        if (this.A02 == null) {
            String B4F = this.A01.B4F(C09380hD.A00, null);
            long At2 = this.A01.At2(C09380hD.A01, Long.MAX_VALUE);
            if (B4F == null || At2 == Long.MAX_VALUE) {
                r4 = new C09420hL(C188215g.A00().toString(), this.A00.now());
                C30281hn edit = this.A01.edit();
                edit.BzA(C09380hD.A01, r4.A00);
                edit.BzC(C09380hD.A00, r4.A01);
                edit.commit();
            } else {
                r4 = new C09420hL(B4F, At2);
            }
            this.A02 = r4;
        }
        return this.A02;
    }

    public synchronized void CCU(C09420hL r6) {
        this.A02 = r6;
        C09420hL r4 = this.A02;
        C30281hn edit = this.A01.edit();
        edit.BzA(C09380hD.A01, r4.A00);
        edit.BzC(C09380hD.A00, r4.A01);
        edit.commit();
    }

    public static final C25911ab A01(AnonymousClass1XY r5) {
        if (A03 == null) {
            synchronized (C25911ab.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r5);
                if (A002 != null) {
                    try {
                        A03 = new C25911ab(FbSharedPreferencesModule.A00(r5.getApplicationInjector()), AnonymousClass067.A02());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public boolean BFQ() {
        return this.A01.BFQ();
    }

    private C25911ab(FbSharedPreferences fbSharedPreferences, AnonymousClass06B r2) {
        this.A01 = fbSharedPreferences;
        this.A00 = r2;
    }

    public C09420hL B6J() {
        return A00();
    }

    public String B7Z() {
        C09420hL A002 = A00();
        if (A002 == null) {
            return null;
        }
        return A002.A01;
    }
}
