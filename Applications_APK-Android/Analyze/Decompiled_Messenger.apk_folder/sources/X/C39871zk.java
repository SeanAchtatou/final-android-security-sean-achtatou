package X;

import io.card.payment.BuildConfig;
import java.io.Serializable;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;

/* renamed from: X.1zk  reason: invalid class name and case insensitive filesystem */
public final class C39871zk implements Comparable, C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("decoderPollingIntervalMs", (byte) 8, 5);
    private static final C36241sk A01 = new C36241sk("decoderPollingLowerLatency", (byte) 2, 4);
    private static final C36241sk A02 = new C36241sk("decoderRePollingIntervalMs", (byte) 8, 6);
    private static final C36241sk A03 = new C36241sk("encoderPollingIntervalMs", (byte) 8, 2);
    private static final C36241sk A04 = new C36241sk("encoderPollingLowerLatency", (byte) 2, 1);
    private static final C36241sk A05 = new C36241sk("encoderRePollingIntervalMs", (byte) 8, 3);
    private static final C36241sk A06 = new C36241sk("restartHwDecoderOnSwFallbackFailure", (byte) 2, 7);
    private static final C36231sj A07 = new C36231sj("HardwareCodecConfig");
    public BitSet __isset_bit_vector = new BitSet(7);
    public int decoderPollingIntervalMs = -1;
    public boolean decoderPollingLowerLatency = false;
    public int decoderRePollingIntervalMs = -1;
    public int encoderPollingIntervalMs = -1;
    public boolean encoderPollingLowerLatency = false;
    public int encoderRePollingIntervalMs = -1;
    public boolean restartHwDecoderOnSwFallbackFailure = false;

    public boolean equals(Object obj) {
        C39871zk r3;
        if (obj == null || !(obj instanceof C39871zk) || (r3 = (C39871zk) obj) == null) {
            return false;
        }
        if (this == r3) {
            return true;
        }
        if (!B36.A0I(this.encoderPollingLowerLatency, r3.encoderPollingLowerLatency) || !B36.A0A(this.encoderPollingIntervalMs, r3.encoderPollingIntervalMs) || !B36.A0A(this.encoderRePollingIntervalMs, r3.encoderRePollingIntervalMs) || !B36.A0I(this.decoderPollingLowerLatency, r3.decoderPollingLowerLatency) || !B36.A0A(this.decoderPollingIntervalMs, r3.decoderPollingIntervalMs) || !B36.A0A(this.decoderRePollingIntervalMs, r3.decoderRePollingIntervalMs) || !B36.A0I(this.restartHwDecoderOnSwFallbackFailure, r3.restartHwDecoderOnSwFallbackFailure)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(1, new AnonymousClass2EQ("encoderPollingLowerLatency", (byte) 3, new C22604B3c((byte) 2)));
        hashMap.put(2, new AnonymousClass2EQ("encoderPollingIntervalMs", (byte) 3, new C22604B3c((byte) 8)));
        hashMap.put(3, new AnonymousClass2EQ("encoderRePollingIntervalMs", (byte) 3, new C22604B3c((byte) 8)));
        hashMap.put(4, new AnonymousClass2EQ("decoderPollingLowerLatency", (byte) 3, new C22604B3c((byte) 2)));
        hashMap.put(5, new AnonymousClass2EQ("decoderPollingIntervalMs", (byte) 3, new C22604B3c((byte) 8)));
        hashMap.put(6, new AnonymousClass2EQ("decoderRePollingIntervalMs", (byte) 3, new C22604B3c((byte) 8)));
        hashMap.put(7, new AnonymousClass2EQ("restartHwDecoderOnSwFallbackFailure", (byte) 3, new C22604B3c((byte) 2)));
        AnonymousClass2EQ.A00(C39871zk.class, Collections.unmodifiableMap(hashMap));
    }

    public String CJ9(int i, boolean z) {
        String str;
        String str2;
        String str3 = BuildConfig.FLAVOR;
        if (z) {
            str = B36.A05(i);
        } else {
            str = str3;
        }
        if (z) {
            str2 = "\n";
        } else {
            str2 = str3;
        }
        if (z) {
            str3 = " ";
        }
        StringBuilder sb = new StringBuilder("HardwareCodecConfig");
        sb.append(str3);
        sb.append("(");
        sb.append(str2);
        sb.append(str);
        sb.append("encoderPollingLowerLatency");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        int i2 = i + 1;
        sb.append(B36.A07(Boolean.valueOf(this.encoderPollingLowerLatency), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("encoderPollingIntervalMs");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Integer.valueOf(this.encoderPollingIntervalMs), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("encoderRePollingIntervalMs");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Integer.valueOf(this.encoderRePollingIntervalMs), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("decoderPollingLowerLatency");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Boolean.valueOf(this.decoderPollingLowerLatency), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("decoderPollingIntervalMs");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Integer.valueOf(this.decoderPollingIntervalMs), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("decoderRePollingIntervalMs");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Integer.valueOf(this.decoderRePollingIntervalMs), i2, z));
        sb.append(AnonymousClass08S.A0J(",", str2));
        sb.append(str);
        sb.append("restartHwDecoderOnSwFallbackFailure");
        sb.append(str3);
        sb.append(":");
        sb.append(str3);
        sb.append(B36.A07(Boolean.valueOf(this.restartHwDecoderOnSwFallbackFailure), i2, z));
        sb.append(AnonymousClass08S.A0J(str2, B36.A08(str)));
        sb.append(")");
        return sb.toString();
    }

    public void CNX(C35781ro r2) {
        r2.A0i(A07);
        r2.A0e(A04);
        r2.A0l(this.encoderPollingLowerLatency);
        r2.A0S();
        r2.A0e(A03);
        r2.A0c(this.encoderPollingIntervalMs);
        r2.A0S();
        r2.A0e(A05);
        r2.A0c(this.encoderRePollingIntervalMs);
        r2.A0S();
        r2.A0e(A01);
        r2.A0l(this.decoderPollingLowerLatency);
        r2.A0S();
        r2.A0e(A00);
        r2.A0c(this.decoderPollingIntervalMs);
        r2.A0S();
        r2.A0e(A02);
        r2.A0c(this.decoderRePollingIntervalMs);
        r2.A0S();
        r2.A0e(A06);
        r2.A0l(this.restartHwDecoderOnSwFallbackFailure);
        r2.A0S();
        r2.A0T();
        r2.A0X();
    }

    public int compareTo(Object obj) {
        int compareTo;
        C39871zk r5 = (C39871zk) obj;
        if (r5 == null) {
            throw new NullPointerException();
        } else if (r5 == this || ((compareTo = Boolean.valueOf(this.__isset_bit_vector.get(0)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(0)))) == 0 && (compareTo = B36.A04(this.encoderPollingLowerLatency, r5.encoderPollingLowerLatency)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(1)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(1)))) == 0 && (compareTo = B36.A00(this.encoderPollingIntervalMs, r5.encoderPollingIntervalMs)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(2)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(2)))) == 0 && (compareTo = B36.A00(this.encoderRePollingIntervalMs, r5.encoderRePollingIntervalMs)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(3)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(3)))) == 0 && (compareTo = B36.A04(this.decoderPollingLowerLatency, r5.decoderPollingLowerLatency)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(4)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(4)))) == 0 && (compareTo = B36.A00(this.decoderPollingIntervalMs, r5.decoderPollingIntervalMs)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(5)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(5)))) == 0 && (compareTo = B36.A00(this.decoderRePollingIntervalMs, r5.decoderRePollingIntervalMs)) == 0 && (compareTo = Boolean.valueOf(this.__isset_bit_vector.get(6)).compareTo(Boolean.valueOf(r5.__isset_bit_vector.get(6)))) == 0 && (compareTo = B36.A04(this.restartHwDecoderOnSwFallbackFailure, r5.restartHwDecoderOnSwFallbackFailure)) == 0)) {
            return 0;
        } else {
            return compareTo;
        }
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{Boolean.valueOf(this.encoderPollingLowerLatency), Integer.valueOf(this.encoderPollingIntervalMs), Integer.valueOf(this.encoderRePollingIntervalMs), Boolean.valueOf(this.decoderPollingLowerLatency), Integer.valueOf(this.decoderPollingIntervalMs), Integer.valueOf(this.decoderRePollingIntervalMs), Boolean.valueOf(this.restartHwDecoderOnSwFallbackFailure)});
    }
}
