package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0lX  reason: invalid class name */
public final class AnonymousClass0lX extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static C05540Zi A01;
    private static C05540Zi A02;
    private static C05540Zi A03;
    private static C05540Zi A04;
    private static final Object A05 = new Object();
    private static final Object A06 = new Object();
    private static final Object A07 = new Object();
    private static final Object A08 = new Object();
    private static final Object A09 = new Object();

    public static final C27311cz A01(AnonymousClass1XY r10) {
        C27311cz r0;
        synchronized (A05) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r10)) {
                    AnonymousClass1XY r8 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C27301cy(AnonymousClass0lw.A07(r8), new C27301cy(C12370pE.A01(r8), new C27301cy(new C12380pF(r8, AnonymousClass0VB.A00(AnonymousClass1Y3.AKh, r8), AnonymousClass0VG.A00(AnonymousClass1Y3.Apk, r8), AnonymousClass0VG.A00(AnonymousClass1Y3.BBF, r8)), new C27331d1())));
                }
                C05540Zi r1 = A00;
                r0 = (C27311cz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C27311cz A02(AnonymousClass1XY r5) {
        C27311cz r0;
        synchronized (A06) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r5)) {
                    A01.A00 = new C27301cy(new C11070lo((AnonymousClass1XY) A01.A01()), new C27331d1());
                }
                C05540Zi r1 = A01;
                r0 = (C27311cz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C27311cz A03(AnonymousClass1XY r5) {
        C27311cz r0;
        synchronized (A07) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r5)) {
                    A02.A00 = new C27301cy(new C11070lo((AnonymousClass1XY) A02.A01()), new C27331d1());
                }
                C05540Zi r1 = A02;
                r0 = (C27311cz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C27311cz A04(AnonymousClass1XY r5) {
        C27311cz r0;
        synchronized (A08) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r5)) {
                    A03.A00 = new C27301cy(new C11070lo((AnonymousClass1XY) A03.A01()), new C27331d1());
                }
                C05540Zi r1 = A03;
                r0 = (C27311cz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C27311cz A05(AnonymousClass1XY r9) {
        C27311cz r0;
        synchronized (A09) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r9)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A04.A01();
                    A04.A00 = new C27301cy(new C27561dO(r02), new C27301cy(AnonymousClass0lw.A08(r02), new C27301cy(new C12440pM(r02), new C27331d1())));
                }
                C05540Zi r1 = A04;
                r0 = (C27311cz) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final AnonymousClass0V7 A00(AnonymousClass1XY r2) {
        C001500z A052 = AnonymousClass0UU.A05(r2);
        C25051Yd A002 = AnonymousClass0WT.A00(r2);
        if (A052 != C001500z.A07) {
            return AnonymousClass0V7.FOREGROUND;
        }
        if (A002.Aem(285035504604516L)) {
            return AnonymousClass0V7.REALTIME_DO_NOT_USE;
        }
        return AnonymousClass0V7.URGENT;
    }
}
