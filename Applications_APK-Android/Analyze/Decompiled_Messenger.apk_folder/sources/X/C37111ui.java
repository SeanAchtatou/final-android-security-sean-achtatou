package X;

import android.transition.Transition;

/* renamed from: X.1ui  reason: invalid class name and case insensitive filesystem */
public final class C37111ui extends C16340wt {
    public static boolean A00(Transition transition) {
        if (!C16340wt.A03(transition.getTargetIds()) || !C16340wt.A03(transition.getTargetNames()) || !C16340wt.A03(transition.getTargetTypes())) {
            return true;
        }
        return false;
    }
}
