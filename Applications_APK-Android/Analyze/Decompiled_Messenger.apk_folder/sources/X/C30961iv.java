package X;

import org.json.JSONObject;

/* renamed from: X.1iv  reason: invalid class name and case insensitive filesystem */
public final class C30961iv extends C30971iw {
    public final long A00;
    public final long A01;
    public final long A02;
    public final boolean A03;

    public static C30961iv A00(long j) {
        return new C30961iv(j, j, j, false);
    }

    public static C30961iv A01(JSONObject jSONObject) {
        C30951iu r7 = new C30951iu();
        long optLong = jSONObject.optLong("max_size", -1);
        String $const$string = ECX.$const$string(155);
        long optLong2 = jSONObject.optLong($const$string, -1);
        if (optLong <= 0 || optLong2 == -1) {
            return null;
        }
        r7.A00 = optLong;
        r7.A01 = optLong2;
        r7.A02 = jSONObject.optLong($const$string, optLong2);
        r7.A03 = jSONObject.optBoolean(ECX.$const$string(AnonymousClass1Y3.A16), false);
        return r7.A00();
    }

    public C30961iv(long j, long j2, long j3, boolean z) {
        this.A00 = j;
        this.A01 = j2;
        this.A02 = j3;
        this.A03 = z;
    }
}
