package X;

import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.07W  reason: invalid class name */
public final class AnonymousClass07W {
    public static AnonymousClass0lL A00(BlueServiceOperationFactory blueServiceOperationFactory, String str, Bundle bundle, int i) {
        Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 21, 0, 0, i, 0, 0);
        return blueServiceOperationFactory.newInstance(str, bundle);
    }

    public static AnonymousClass0lL A01(BlueServiceOperationFactory blueServiceOperationFactory, String str, Bundle bundle, CallerContext callerContext, int i) {
        Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 21, 0, 0, i, 0, 0);
        return blueServiceOperationFactory.newInstance(str, bundle, callerContext);
    }
}
