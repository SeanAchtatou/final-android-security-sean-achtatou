package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0mo  reason: invalid class name and case insensitive filesystem */
public final class C11370mo implements C11380mp {
    private final AnonymousClass1ZE A00;
    private final QuickPerformanceLogger A01;
    private final Set A02 = new HashSet();
    private final Set A03 = new HashSet();

    public void BNj() {
    }

    public void BVc() {
    }

    public void BVd() {
    }

    public static final C11370mo A00(AnonymousClass1XY r1) {
        return new C11370mo(r1);
    }

    public void BNi(Set set) {
        this.A02.addAll(set);
    }

    public void BNk() {
        boolean z = !this.A02.isEmpty();
        this.A01.markerTag(5505206, AnonymousClass08S.A0J("did_drop_data:", Boolean.toString(z)));
        this.A01.markerEnd(5505206, 2);
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(this.A00.A01("threads_db_auto_upgraded"), 571);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A08("did_drop_data", Boolean.valueOf(z));
            uSLEBaseShape0S0000000.A00.A0E("drop_reasons", this.A02);
            uSLEBaseShape0S0000000.A00.A0E("upgrade_errors", this.A03);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public void BVV(String str, boolean z) {
        this.A01.markerTag(5505207, AnonymousClass08S.A0J("data_migrator:", str));
        this.A01.markerTag(5505207, AnonymousClass08S.A0J("data_migrated:", Boolean.toString(z)));
        this.A01.markerEnd(5505207, 2);
    }

    public void BVW(String str) {
        this.A01.markerStart(5505207);
    }

    public void BVb() {
        this.A01.markerStart(5505206);
        this.A02.clear();
        this.A03.clear();
    }

    public void BVe(String str, Exception exc) {
        Set set = this.A03;
        set.add(str + ":" + exc);
    }

    public C11370mo(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1ZD.A00(r2);
        this.A01 = AnonymousClass0ZD.A03(r2);
    }
}
