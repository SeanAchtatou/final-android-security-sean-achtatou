package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.inject.Singleton;
import org.apache.http.HttpHost;

@Singleton
/* renamed from: X.1jx  reason: invalid class name and case insensitive filesystem */
public final class C31601jx {
    public static final AnonymousClass1Y7 A06;
    private static final Set A07;
    private static volatile C31601jx A08;
    public AnonymousClass0UN A00;
    public HttpHost A01 = null;
    public boolean A02 = false;
    public boolean A03 = true;
    public final FbSharedPreferences A04;
    public final CopyOnWriteArrayList A05;

    static {
        AnonymousClass1Y7 r4 = (AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("react_native/")).A09("developer_mode_enabled");
        A06 = r4;
        A07 = ImmutableSet.A08(C25951af.A0B, C25951af.A0D, C25951af.A0J, C25951af.A0I, r4);
    }

    public static final C31601jx A00(AnonymousClass1XY r5) {
        if (A08 == null) {
            synchronized (C31601jx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A08 = new C31601jx(applicationInjector, FbSharedPreferencesModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004a, code lost:
        if ("facebook.com".equals(r6.A04.B4F(r1, "facebook.com")) != false) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C31601jx r6) {
        /*
            com.facebook.prefs.shared.FbSharedPreferences r0 = r6.A04
            boolean r0 = r0.BFQ()
            r3 = 1
            if (r0 == 0) goto L_0x001f
            com.facebook.prefs.shared.FbSharedPreferences r1 = r6.A04
            X.1Y7 r0 = X.C25951af.A0B
            boolean r0 = r1.Aep(r0, r3)
            if (r0 != 0) goto L_0x001f
            com.facebook.prefs.shared.FbSharedPreferences r2 = r6.A04
            X.1Y7 r1 = X.C31601jx.A06
            r0 = 0
            boolean r0 = r2.Aep(r1, r0)
            if (r0 != 0) goto L_0x001f
            r3 = 0
        L_0x001f:
            boolean r0 = r6.A03
            if (r3 == r0) goto L_0x00f1
            r6.A03 = r3
            r5 = 1
        L_0x0026:
            com.facebook.prefs.shared.FbSharedPreferences r0 = r6.A04
            boolean r0 = r0.BFQ()
            r3 = 0
            if (r0 == 0) goto L_0x0099
            java.lang.String r2 = "facebook.com"
            com.facebook.prefs.shared.FbSharedPreferences r0 = r6.A04
            X.1Y7 r1 = X.C25951af.A0J
            java.lang.String r0 = r0.B4F(r1, r2)
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x004c
            com.facebook.prefs.shared.FbSharedPreferences r0 = r6.A04
            java.lang.String r0 = r0.B4F(r1, r2)
            boolean r1 = r2.equals(r0)
            r0 = 1
            if (r1 == 0) goto L_0x004d
        L_0x004c:
            r0 = 0
        L_0x004d:
            if (r0 != 0) goto L_0x00ef
            com.facebook.prefs.shared.FbSharedPreferences r2 = r6.A04
            X.1Y7 r1 = X.C25951af.A0I
            java.lang.String r0 = ""
            java.lang.String r0 = r2.B4F(r1, r0)
            boolean r0 = X.C06850cB.A0B(r0)
            if (r0 == 0) goto L_0x0098
            int r1 = X.AnonymousClass1Y3.BCt
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            android.content.Context r0 = (android.content.Context) r0
            android.content.SharedPreferences r2 = android.preference.PreferenceManager.getDefaultSharedPreferences(r0)
            java.lang.String r1 = "sandbox_subdomain"
            java.lang.String r0 = ""
            java.lang.String r0 = r2.getString(r1, r0)
            boolean r0 = X.C06850cB.A0B(r0)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x0098
            com.facebook.prefs.shared.FbSharedPreferences r2 = r6.A04
            X.1Y7 r1 = X.C31601jx.A06
            boolean r0 = r2.Aep(r1, r3)
            if (r0 != 0) goto L_0x0098
            com.facebook.prefs.shared.FbSharedPreferences r2 = r6.A04
            X.1Y7 r1 = X.C31621jz.A00
            r0 = 0
            java.lang.String r0 = r2.B4F(r1, r0)
            boolean r0 = X.C06850cB.A0B(r0)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0099
        L_0x0098:
            r3 = 1
        L_0x0099:
            boolean r0 = r6.A02
            if (r3 == r0) goto L_0x00a0
            r6.A02 = r3
            r5 = 1
        L_0x00a0:
            com.facebook.prefs.shared.FbSharedPreferences r1 = r6.A04
            X.1Y7 r0 = X.C25951af.A0D
            r4 = 0
            java.lang.String r3 = r1.B4F(r0, r4)
            boolean r0 = X.C06850cB.A0B(r3)
            if (r0 != 0) goto L_0x00cc
            r0 = 58
            int r2 = r3.indexOf(r0)
            r0 = -1
            if (r2 == r0) goto L_0x00cc
            int r0 = r2 + 1
            java.lang.String r0 = r3.substring(r0)
            int r1 = java.lang.Integer.parseInt(r0)
            org.apache.http.HttpHost r4 = new org.apache.http.HttpHost
            r0 = 0
            java.lang.String r0 = r3.substring(r0, r2)
            r4.<init>(r0, r1)
        L_0x00cc:
            org.apache.http.HttpHost r0 = r6.A01
            boolean r0 = com.google.common.base.Objects.equal(r0, r4)
            if (r0 != 0) goto L_0x00d7
            r6.A01 = r4
            r5 = 1
        L_0x00d7:
            if (r5 == 0) goto L_0x00f4
            java.util.concurrent.CopyOnWriteArrayList r0 = r6.A05
            java.util.Iterator r1 = r0.iterator()
        L_0x00df:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00f4
            java.lang.Object r0 = r1.next()
            X.EWX r0 = (X.EWX) r0
            r0.A00()
            goto L_0x00df
        L_0x00ef:
            r3 = r0
            goto L_0x0099
        L_0x00f1:
            r5 = 0
            goto L_0x0026
        L_0x00f4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31601jx.A01(X.1jx):void");
    }

    private C31601jx(AnonymousClass1XY r4, FbSharedPreferences fbSharedPreferences) {
        this.A00 = new AnonymousClass0UN(1, r4);
        this.A04 = fbSharedPreferences;
        this.A05 = new CopyOnWriteArrayList();
        this.A04.C0h(A07, new C31611jy(this));
        if (this.A04.BFQ()) {
            A01(this);
        } else {
            this.A04.C0e(new C21425ADr(this));
        }
    }
}
