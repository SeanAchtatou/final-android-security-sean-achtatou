package X;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists$TransformingRandomAccessList;
import com.google.common.collect.Lists$TransformingSequentialList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.0To  reason: invalid class name and case insensitive filesystem */
public final class C04300To {
    public static ArrayList A00() {
        return new ArrayList();
    }

    public static ArrayList A01(int i) {
        C25001Xy.A00(i, "initialArraySize");
        return new ArrayList(i);
    }

    public static ArrayList A02(int i) {
        C25001Xy.A00(i, "arraySize");
        return new ArrayList(C06950cM.A01(((long) i) + 5 + ((long) (i / 10))));
    }

    public static List A05(List list) {
        if (list instanceof ImmutableList) {
            return ((ImmutableList) list).reverse();
        }
        if (list instanceof C68253Sc) {
            return ((C68253Sc) list).A00;
        }
        if (list instanceof RandomAccess) {
            return new C68263Sd(list);
        }
        return new C68253Sc(list);
    }

    public static List A07(List list, Function function) {
        if (list instanceof RandomAccess) {
            return new Lists$TransformingRandomAccessList(list, function);
        }
        return new Lists$TransformingSequentialList(list, function);
    }

    public static ArrayList A03(Iterable iterable) {
        Preconditions.checkNotNull(iterable);
        if (iterable instanceof Collection) {
            return new ArrayList((Collection) iterable);
        }
        Iterator it = iterable.iterator();
        ArrayList A00 = A00();
        C24931Xr.A09(A00, it);
        return A00;
    }

    public static ArrayList A04(Object... objArr) {
        Preconditions.checkNotNull(objArr);
        int length = objArr.length;
        C25001Xy.A00(length, "arraySize");
        ArrayList arrayList = new ArrayList(C06950cM.A01(((long) length) + 5 + ((long) (length / 10))));
        Collections.addAll(arrayList, objArr);
        return arrayList;
    }

    public static List A06(List list, int i) {
        Preconditions.checkNotNull(list);
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        if (list instanceof RandomAccess) {
            return new C22710B8z(list, i);
        }
        return new B90(list, i);
    }
}
