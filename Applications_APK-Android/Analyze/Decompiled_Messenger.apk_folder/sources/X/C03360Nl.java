package X;

/* renamed from: X.0Nl  reason: invalid class name and case insensitive filesystem */
public final class C03360Nl extends AnonymousClass0EL {
    public final AnonymousClass0Nk[] A00;

    /* JADX WARNING: Can't wrap try/catch for region: R(5:37|38|39|40|41) */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a9, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c0, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x00c4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C03360Nl(X.C002601r r17, X.AnonymousClass02E r18) {
        /*
            r16 = this;
            r8 = r16
            r16.<init>()
            r0 = r17
            android.content.Context r0 = r0.A03
            java.io.File r14 = new java.io.File
            java.lang.String r2 = "/data/local/tmp/exopackage/"
            java.lang.String r1 = r0.getPackageName()
            java.lang.String r0 = "/native-libs/"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r14.<init>(r0)
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.LinkedHashSet r12 = new java.util.LinkedHashSet
            r12.<init>()
            java.lang.String[] r13 = X.C002401o.A02()
            int r11 = r13.length
            r1 = 0
            r10 = 0
        L_0x002b:
            if (r10 >= r11) goto L_0x00cc
            r2 = r13[r10]
            java.io.File r7 = new java.io.File
            r7.<init>(r14, r2)
            boolean r0 = r7.isDirectory()
            if (r0 == 0) goto L_0x00ac
            r12.add(r2)
            java.io.File r2 = new java.io.File
            java.lang.String r0 = "metadata.txt"
            r2.<init>(r7, r0)
            boolean r0 = r2.isFile()
            if (r0 == 0) goto L_0x00ac
            java.io.FileReader r6 = new java.io.FileReader
            r6.<init>(r2)
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ all -> 0x00c5 }
            r5.<init>(r6)     // Catch:{ all -> 0x00c5 }
        L_0x0054:
            java.lang.String r4 = r5.readLine()     // Catch:{ all -> 0x00be }
            if (r4 == 0) goto L_0x00a6
            int r0 = r4.length()     // Catch:{ all -> 0x00be }
            if (r0 == 0) goto L_0x0054
            r0 = 32
            int r2 = r4.indexOf(r0)     // Catch:{ all -> 0x00be }
            r0 = -1
            if (r2 == r0) goto L_0x00b0
            java.lang.String r1 = r4.substring(r1, r2)     // Catch:{ all -> 0x00be }
            java.lang.String r0 = ".so"
            java.lang.String r3 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x00be }
            int r15 = r9.size()     // Catch:{ all -> 0x00be }
            r1 = 0
        L_0x0078:
            if (r1 >= r15) goto L_0x008e
            java.lang.Object r0 = r9.get(r1)     // Catch:{ all -> 0x00be }
            X.0Nk r0 = (X.AnonymousClass0Nk) r0     // Catch:{ all -> 0x00be }
            java.lang.String r0 = r0.A01     // Catch:{ all -> 0x00be }
            boolean r0 = r0.equals(r3)     // Catch:{ all -> 0x00be }
            if (r0 == 0) goto L_0x0089
            goto L_0x008c
        L_0x0089:
            int r1 = r1 + 1
            goto L_0x0078
        L_0x008c:
            r0 = 1
            goto L_0x008f
        L_0x008e:
            r0 = 0
        L_0x008f:
            if (r0 != 0) goto L_0x00a4
            int r0 = r2 + 1
            java.lang.String r2 = r4.substring(r0)     // Catch:{ all -> 0x00be }
            X.0Nk r1 = new X.0Nk     // Catch:{ all -> 0x00be }
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x00be }
            r0.<init>(r7, r2)     // Catch:{ all -> 0x00be }
            r1.<init>(r3, r2, r0)     // Catch:{ all -> 0x00be }
            r9.add(r1)     // Catch:{ all -> 0x00be }
        L_0x00a4:
            r1 = 0
            goto L_0x0054
        L_0x00a6:
            r5.close()     // Catch:{ all -> 0x00c5 }
            r6.close()
        L_0x00ac:
            int r10 = r10 + 1
            goto L_0x002b
        L_0x00b0:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x00be }
            java.lang.String r1 = "illegal line in exopackage metadata: ["
            java.lang.String r0 = "]"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r4, r0)     // Catch:{ all -> 0x00be }
            r2.<init>(r0)     // Catch:{ all -> 0x00be }
            throw r2     // Catch:{ all -> 0x00be }
        L_0x00be:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00c0 }
        L_0x00c0:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x00c4 }
        L_0x00c4:
            throw r0     // Catch:{ all -> 0x00c5 }
        L_0x00c5:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00c7 }
        L_0x00c7:
            r0 = move-exception
            r6.close()     // Catch:{ all -> 0x00cb }
        L_0x00cb:
            throw r0
        L_0x00cc:
            int r0 = r12.size()
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.Object[] r0 = r12.toArray(r0)
            java.lang.String[] r0 = (java.lang.String[]) r0
            r1 = r18
            r1.A01 = r0
            int r0 = r9.size()
            X.0Nk[] r0 = new X.AnonymousClass0Nk[r0]
            java.lang.Object[] r0 = r9.toArray(r0)
            X.0Nk[] r0 = (X.AnonymousClass0Nk[]) r0
            r8.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03360Nl.<init>(X.01r, X.02E):void");
    }

    public AnonymousClass0ET A00() {
        return new AnonymousClass0ET(this.A00);
    }

    public AnonymousClass0EM A01() {
        return new C03370Nm(this);
    }
}
