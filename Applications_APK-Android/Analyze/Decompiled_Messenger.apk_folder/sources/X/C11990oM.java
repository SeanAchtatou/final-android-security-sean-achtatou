package X;

import android.net.Uri;
import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.business.attachments.generic.model.LogoImage;
import com.facebook.messaging.business.attachments.generic.model.PlatformGenericAttachmentItem;
import com.facebook.messaging.business.commerce.model.retail.CommerceData;
import com.facebook.messaging.business.commerce.model.retail.RetailAddress;
import com.facebook.messaging.business.commerce.model.retail.RetailCarrier;
import com.facebook.messaging.business.commerce.model.retail.Shipment;
import com.facebook.messaging.business.commerce.model.retail.ShipmentTrackingEvent;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0oM  reason: invalid class name and case insensitive filesystem */
public final class C11990oM {
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r5.isObject() == false) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0037, code lost:
        if (r1.size() != 0) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01c7, code lost:
        if (r2 == X.AnonymousClass07B.A02) goto L_0x01c9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.business.commerce.model.retail.CommerceData A09(com.fasterxml.jackson.databind.JsonNode r5) {
        /*
            r4 = this;
            r3 = 0
            if (r5 == 0) goto L_0x0039
            boolean r0 = r5.isObject()
            if (r0 == 0) goto L_0x0039
            int r0 = r5.size()
            if (r0 == 0) goto L_0x0039
            java.lang.String r2 = "fb_object_contents"
            boolean r0 = r5.has(r2)
            if (r0 != 0) goto L_0x0021
            java.util.Iterator r0 = r5.iterator()
            java.lang.Object r5 = r0.next()
            com.fasterxml.jackson.databind.JsonNode r5 = (com.fasterxml.jackson.databind.JsonNode) r5
        L_0x0021:
            if (r5 == 0) goto L_0x002a
            boolean r1 = r5.isObject()
            r0 = 1
            if (r1 != 0) goto L_0x002b
        L_0x002a:
            r0 = 0
        L_0x002b:
            if (r0 == 0) goto L_0x014a
            com.fasterxml.jackson.databind.JsonNode r1 = r5.get(r2)
        L_0x0031:
            if (r1 == 0) goto L_0x0039
            int r0 = r1.size()
            if (r0 != 0) goto L_0x003a
        L_0x0039:
            r1 = r3
        L_0x003a:
            if (r1 == 0) goto L_0x01d3
            java.lang.String r0 = "messenger_commerce_bubble_type"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            if (r0 != 0) goto L_0x0140
            java.lang.Integer r2 = X.AnonymousClass07B.A00
        L_0x0046:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r2 != r0) goto L_0x014d
            X.COJ r2 = new X.COJ
            r2.<init>()
            java.lang.String r0 = "receipt_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0B = r0
            java.lang.String r0 = "order_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0E = r0
            java.lang.String r0 = "shipping_method"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0H = r0
            java.lang.String r0 = "payment_method"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0F = r0
            java.lang.String r0 = "order_url"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r3 = com.facebook.common.util.JSONUtil.A0N(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)
            if (r0 != 0) goto L_0x013d
            android.net.Uri r0 = android.net.Uri.parse(r3)
        L_0x0093:
            r2.A02 = r0
            java.lang.String r0 = "cancellation_url"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r3 = com.facebook.common.util.JSONUtil.A0N(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)
            if (r0 != 0) goto L_0x013a
            android.net.Uri r0 = android.net.Uri.parse(r3)
        L_0x00a9:
            r2.A01 = r0
            java.lang.String r0 = "structured_address"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            com.facebook.messaging.business.commerce.model.retail.RetailAddress r0 = A04(r0)
            r2.A04 = r0
            java.lang.String r0 = "status"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0I = r0
            java.lang.String r0 = "total_cost"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A09 = r0
            java.lang.String r0 = "total_tax"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0A = r0
            java.lang.String r0 = "shipping_cost"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A07 = r0
            java.lang.String r0 = "subtotal"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A08 = r0
            java.lang.String r0 = "order_time"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0D = r0
            java.lang.String r0 = "partner_logo"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            com.facebook.messaging.business.attachments.generic.model.LogoImage r0 = A00(r0)
            r2.A03 = r0
            java.lang.String r0 = "items"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.util.List r0 = A08(r0)
            r2.A0J = r0
            java.lang.String r0 = "recipient_name"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0G = r0
            java.lang.String r0 = "account_holder_name"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A06 = r0
            com.facebook.messaging.business.commerce.model.retail.CommerceData r1 = new com.facebook.messaging.business.commerce.model.retail.CommerceData
            com.facebook.messaging.business.commerce.model.retail.Receipt r0 = new com.facebook.messaging.business.commerce.model.retail.Receipt
            r0.<init>(r2)
            r1.<init>(r0)
            return r1
        L_0x013a:
            r0 = 0
            goto L_0x00a9
        L_0x013d:
            r0 = 0
            goto L_0x0093
        L_0x0140:
            int r0 = com.facebook.common.util.JSONUtil.A04(r0)
            java.lang.Integer r2 = X.C22327AtE.A01(r0)
            goto L_0x0046
        L_0x014a:
            r1 = r3
            goto L_0x0031
        L_0x014d:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            if (r2 != r0) goto L_0x01a9
            X.CP5 r3 = new X.CP5
            r3.<init>()
            java.lang.String r0 = "cancellation_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r3.A02 = r0
            java.lang.String r0 = "items"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.util.List r0 = A08(r0)
            r3.A03 = r0
            X.COJ r2 = new X.COJ
            r2.<init>()
            java.lang.String r0 = "receipt_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0B = r0
            java.lang.String r0 = "order_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r2.A0E = r0
            java.lang.String r0 = "partner_logo"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            com.facebook.messaging.business.attachments.generic.model.LogoImage r0 = A00(r0)
            r2.A03 = r0
            com.facebook.messaging.business.commerce.model.retail.Receipt r0 = new com.facebook.messaging.business.commerce.model.retail.Receipt
            r0.<init>(r2)
            r3.A01 = r0
            com.facebook.messaging.business.commerce.model.retail.CommerceData r1 = new com.facebook.messaging.business.commerce.model.retail.CommerceData
            com.facebook.messaging.business.commerce.model.retail.ReceiptCancellation r0 = new com.facebook.messaging.business.commerce.model.retail.ReceiptCancellation
            r0.<init>(r3)
            r1.<init>(r0)
            return r1
        L_0x01a9:
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            if (r2 == r0) goto L_0x01ce
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            if (r2 == r0) goto L_0x01c9
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            if (r2 == r0) goto L_0x01c9
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            if (r2 == r0) goto L_0x01c9
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            if (r2 == r0) goto L_0x01c9
            java.lang.Integer r0 = X.AnonymousClass07B.A0p
            if (r2 == r0) goto L_0x01c9
            java.lang.Integer r0 = X.AnonymousClass07B.A0q
            if (r2 == r0) goto L_0x01ce
            java.lang.Integer r0 = X.AnonymousClass07B.A02
            if (r2 != r0) goto L_0x01d3
        L_0x01c9:
            com.facebook.messaging.business.commerce.model.retail.CommerceData r0 = A02(r2, r1)
            return r0
        L_0x01ce:
            com.facebook.messaging.business.commerce.model.retail.CommerceData r0 = A01(r1)
            return r0
        L_0x01d3:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11990oM.A09(com.fasterxml.jackson.databind.JsonNode):com.facebook.messaging.business.commerce.model.retail.CommerceData");
    }

    private static LogoImage A00(JsonNode jsonNode) {
        Uri uri;
        if (jsonNode == null) {
            return null;
        }
        CPI cpi = new CPI();
        String A0N = JSONUtil.A0N(jsonNode.get("url"));
        if (!Platform.stringIsNullOrEmpty(A0N)) {
            uri = Uri.parse(A0N);
        } else {
            uri = null;
        }
        cpi.A02 = uri;
        cpi.A01 = JSONUtil.A04(jsonNode.get("width"));
        cpi.A00 = JSONUtil.A04(jsonNode.get("height"));
        return new LogoImage(cpi);
    }

    private static CommerceData A01(JsonNode jsonNode) {
        Uri uri;
        Uri uri2;
        long j;
        String A0N = JSONUtil.A0N(jsonNode.get("carrier"));
        LogoImage A00 = A00(jsonNode.get("carrier_logo"));
        String A0N2 = JSONUtil.A0N(jsonNode.get("carrier_tracking_url"));
        CP4 cp4 = new CP4();
        cp4.A03 = A0N;
        cp4.A01 = A00;
        if (!Platform.stringIsNullOrEmpty(A0N2)) {
            uri = Uri.parse(A0N2);
        } else {
            uri = null;
        }
        cp4.A00 = uri;
        RetailCarrier retailCarrier = new RetailCarrier(cp4);
        C24915COn cOn = new C24915COn();
        cOn.A0B = JSONUtil.A0N(jsonNode.get("shipment_id"));
        cOn.A0C = JSONUtil.A0N(jsonNode.get("receipt_id"));
        cOn.A0E = JSONUtil.A0N(jsonNode.get("tracking_number"));
        cOn.A07 = retailCarrier;
        if (!Platform.stringIsNullOrEmpty(A0N2)) {
            uri2 = Uri.parse(A0N2);
        } else {
            uri2 = null;
        }
        cOn.A03 = uri2;
        cOn.A02 = Long.parseLong(JSONUtil.A0N(jsonNode.get("ship_date"))) * 1000;
        cOn.A0A = JSONUtil.A0N(jsonNode.get("display_ship_date"));
        cOn.A06 = A04(jsonNode.get("origin"));
        cOn.A05 = A04(jsonNode.get("destination"));
        String A0N3 = JSONUtil.A0N(jsonNode.get("estimated_delivery_time"));
        long j2 = 0;
        if (!Platform.stringIsNullOrEmpty(A0N3)) {
            j = Long.parseLong(A0N3) * 1000;
        } else {
            j = 0;
        }
        cOn.A01 = j;
        cOn.A09 = JSONUtil.A0N(jsonNode.get("estimated_delivery_display_time"));
        String A0N4 = JSONUtil.A0N(jsonNode.get("delayed_delivery_time"));
        if (!Platform.stringIsNullOrEmpty(A0N4)) {
            j2 = Long.parseLong(A0N4) * 1000;
        }
        cOn.A00 = j2;
        cOn.A08 = JSONUtil.A0N(jsonNode.get("delayed_delivery_display_time"));
        cOn.A0D = JSONUtil.A0N(jsonNode.get("service_type"));
        cOn.A04 = A00;
        cOn.A0F = A08(jsonNode.get("items"));
        return new CommerceData(new Shipment(cOn));
    }

    private static CommerceData A02(Integer num, JsonNode jsonNode) {
        Uri uri;
        C24922COx cOx = new C24922COx();
        cOx.A06 = JSONUtil.A0N(jsonNode.get("id"));
        cOx.A03 = num;
        cOx.A00 = Long.parseLong(JSONUtil.A0N(jsonNode.get("timestamp"))) * 1000;
        cOx.A05 = JSONUtil.A0N(jsonNode.get("display_time"));
        cOx.A01 = A04(jsonNode.get("tracking_event_location"));
        if (jsonNode.get("shipment_id") != null) {
            C24915COn cOn = new C24915COn();
            CP4 cp4 = new CP4();
            cp4.A03 = JSONUtil.A0N(jsonNode.get("carrier"));
            cp4.A01 = A00(jsonNode.get("carrier_logo"));
            String A0N = JSONUtil.A0N(jsonNode.get("carrier_tracking_url"));
            if (!Platform.stringIsNullOrEmpty(A0N)) {
                uri = Uri.parse(A0N);
            } else {
                uri = null;
            }
            cp4.A00 = uri;
            cOn.A07 = new RetailCarrier(cp4);
            cOn.A0B = JSONUtil.A0N(jsonNode.get("shipment_id"));
            cOn.A0E = JSONUtil.A0N(jsonNode.get("tracking_number"));
            cOn.A0D = JSONUtil.A0N(jsonNode.get("service_type"));
            cOn.A0F = A08(jsonNode.get("items"));
            cOx.A02 = new Shipment(cOn);
        }
        return new CommerceData(new ShipmentTrackingEvent(cOx));
    }

    public static final C11990oM A03() {
        return new C11990oM();
    }

    private static RetailAddress A04(JsonNode jsonNode) {
        if (jsonNode == null) {
            return null;
        }
        C24921COw cOw = new C24921COw();
        cOw.A06 = JSONUtil.A0N(jsonNode.get("street_1"));
        cOw.A07 = JSONUtil.A0N(jsonNode.get("street_2"));
        cOw.A02 = JSONUtil.A0N(jsonNode.get("city"));
        cOw.A05 = JSONUtil.A0N(jsonNode.get("state"));
        cOw.A04 = JSONUtil.A0N(jsonNode.get("postal_code"));
        cOw.A03 = JSONUtil.A0N(jsonNode.get("country"));
        cOw.A08 = JSONUtil.A0N(jsonNode.get("timezone"));
        cOw.A00 = JSONUtil.A00(jsonNode.get("latitude"));
        cOw.A01 = JSONUtil.A00(jsonNode.get("longitude"));
        return new RetailAddress(cOw);
    }

    public static ObjectNode A05(LogoImage logoImage) {
        String str;
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        if (logoImage != null) {
            Uri uri = logoImage.A02;
            if (uri != null) {
                str = uri.toString();
            } else {
                str = BuildConfig.FLAVOR;
            }
            objectNode.put("url", str);
            objectNode.put("width", logoImage.A01);
            objectNode.put("height", logoImage.A00);
        }
        return objectNode;
    }

    public static ObjectNode A06(RetailAddress retailAddress) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        if (retailAddress != null) {
            objectNode.put("street_1", retailAddress.A06);
            objectNode.put("street_2", retailAddress.A07);
            objectNode.put("city", retailAddress.A02);
            objectNode.put("state", retailAddress.A05);
            objectNode.put("postal_code", retailAddress.A04);
            objectNode.put("country", retailAddress.A03);
            objectNode.put("timezone", retailAddress.A08);
            objectNode.put("latitude", Double.toString(retailAddress.A00));
            objectNode.put("longitude", Double.toString(retailAddress.A01));
        }
        return objectNode;
    }

    public static ObjectNode A07(List list) {
        String str;
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        if (list != null && !list.isEmpty()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                PlatformGenericAttachmentItem platformGenericAttachmentItem = (PlatformGenericAttachmentItem) it.next();
                ObjectNode objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                objectNode2.put("location", platformGenericAttachmentItem.A0A);
                objectNode2.put("title", platformGenericAttachmentItem.A0F);
                objectNode2.put("desc", platformGenericAttachmentItem.A07);
                objectNode2.put("price", platformGenericAttachmentItem.A09);
                objectNode2.put("quantity", Integer.toString(platformGenericAttachmentItem.A01));
                Uri uri = platformGenericAttachmentItem.A03;
                if (uri != null) {
                    str = uri.toString();
                } else {
                    str = BuildConfig.FLAVOR;
                }
                objectNode2.put("thumb_url", str);
                ObjectNode objectNode3 = new ObjectNode(JsonNodeFactory.instance);
                objectNode3.put("metaline_1", platformGenericAttachmentItem.A0B);
                objectNode3.put("metaline_2", platformGenericAttachmentItem.A0C);
                objectNode3.put("metaline_3", platformGenericAttachmentItem.A0D);
                objectNode2.put("metalines", objectNode3);
                objectNode.put(platformGenericAttachmentItem.A0A, objectNode2);
            }
        }
        return objectNode;
    }

    private static List A08(JsonNode jsonNode) {
        Uri uri;
        if (jsonNode == null || jsonNode.size() == 0) {
            return Collections.emptyList();
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = jsonNode.iterator();
        while (it.hasNext()) {
            JsonNode jsonNode2 = (JsonNode) it.next();
            C97654li r3 = new C97654li();
            r3.A09 = JSONUtil.A0N(jsonNode2.get("location"));
            r3.A0E = JSONUtil.A0N(jsonNode2.get("title"));
            r3.A06 = JSONUtil.A0N(jsonNode2.get("desc"));
            r3.A08 = JSONUtil.A0N(jsonNode2.get("price"));
            r3.A01 = JSONUtil.A04(jsonNode2.get("quantity"));
            String A0N = JSONUtil.A0N(jsonNode2.get("thumb_url"));
            if (!Platform.stringIsNullOrEmpty(A0N)) {
                uri = Uri.parse(A0N);
            } else {
                uri = null;
            }
            r3.A03 = uri;
            JsonNode jsonNode3 = jsonNode2.get("metalines");
            if (jsonNode3 != null) {
                r3.A0A = JSONUtil.A0N(jsonNode3.get("metaline_1"));
                r3.A0B = JSONUtil.A0N(jsonNode3.get("metaline_2"));
                r3.A0C = JSONUtil.A0N(jsonNode3.get("metaline_3"));
            }
            builder.add((Object) new PlatformGenericAttachmentItem(r3));
        }
        return builder.build();
    }
}
