package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import io.card.payment.BuildConfig;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;

/* renamed from: X.0AH  reason: invalid class name */
public class AnonymousClass0AH {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public Context A04;
    public Handler A05;
    public AnonymousClass07g A06;
    public C01490Al A07;
    public C01420Ad A08;
    public AnonymousClass0B6 A09;
    public AnonymousClass0B7 A0A;
    public C008007h A0B;
    public C01570At A0C;
    public C01470Aj A0D;
    public AnonymousClass0AQ A0E;
    public AnonymousClass0BO A0F;
    public AnonymousClass0AV A0G;
    public AnonymousClass0BW A0H;
    public AnonymousClass0AG A0I;
    public AnonymousClass0BI A0J;
    public AnonymousClass0AU A0K;
    public AnonymousClass0BN A0L;
    public AnonymousClass0B1 A0M;
    public C01610Ax A0N;
    public AnonymousClass0AY A0O;
    public AnonymousClass0BL A0P;
    public String A0Q;
    public Map A0R = new HashMap();
    public Executor A0S;
    public AtomicBoolean A0T = new AtomicBoolean(false);
    public AtomicInteger A0U;
    public boolean A0V = false;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z = false;
    private long A0a;
    private BroadcastReceiver A0b;
    private BroadcastReceiver A0c;
    private Method A0d;
    public long A0e;
    public final Runnable A0f = new AnonymousClass0AN(this);
    public final Runnable A0g = new AnonymousClass0AO(this);
    public final Runnable A0h = new AnonymousClass0AM(this);
    public final AtomicLong A0i = new AtomicLong();
    private final AnonymousClass0AL A0j = new AnonymousClass0AK(this);
    private final Object A0k = new Object();
    public volatile long A0l;
    public volatile AnonymousClass07i A0m;
    public volatile AnonymousClass0C8 A0n;
    public volatile AnonymousClass0C8 A0o;
    public volatile AnonymousClass0AT A0p;
    public volatile String A0q;
    public volatile boolean A0r;
    public volatile long A0s;

    public void A0L(AnonymousClass0C8 r5, AnonymousClass0CE r6, AnonymousClass0FG r7) {
        synchronized (this) {
            if (this.A0n == r5) {
                this.A0n = null;
            }
        }
        boolean z = false;
        if (r5 != null) {
            if (r5.A0Z == AnonymousClass089.DISCONNECTED) {
                z = true;
            }
            r5.A0Y = null;
            r5.A06(r6);
            this.A02 = System.currentTimeMillis();
        }
        if (!z) {
            A05(this, r7, C01660Bc.A00);
        }
    }

    public Long A0V(Boolean bool) {
        return null;
    }

    public void A0X(Boolean bool) {
    }

    public void A0Y(Boolean bool) {
    }

    public void A0Z(List list) {
    }

    public void A0a(List list) {
    }

    public void A0b(List list, List list2) {
    }

    public void A0c(List list, List list2, Boolean bool) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r5.A0A() == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0d(java.util.List r11) {
        /*
            r10 = this;
            r3 = 0
            if (r11 == 0) goto L_0x005f
            boolean r0 = r11.isEmpty()
            if (r0 != 0) goto L_0x005f
            X.0C8 r5 = r10.A0n
            if (r5 == 0) goto L_0x0014
            boolean r1 = r5.A0A()
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 == 0) goto L_0x005f
            java.util.concurrent.atomic.AtomicInteger r0 = X.AnonymousClass0C8.A0e     // Catch:{ 0CI -> 0x0050 }
            int r8 = r0.getAndIncrement()     // Catch:{ 0CI -> 0x0050 }
            r0 = 65535(0xffff, float:9.1834E-41)
            r8 = r8 & r0
            X.0BN r4 = r10.A0L     // Catch:{ 0CI -> 0x0050 }
            java.lang.String r6 = "callSub"
            X.0CL r7 = X.AnonymousClass0CL.A09     // Catch:{ 0CI -> 0x0050 }
            int r9 = r10.A07()     // Catch:{ 0CI -> 0x0050 }
            r4.A01(r5, r6, r7, r8, r9)     // Catch:{ 0CI -> 0x0050 }
            monitor-enter(r5)     // Catch:{ 0CI -> 0x0050 }
            boolean r0 = r5.A0A()     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x0045
            java.util.concurrent.ExecutorService r2 = r5.A0G     // Catch:{ all -> 0x004d }
            X.0S2 r1 = new X.0S2     // Catch:{ all -> 0x004d }
            r1.<init>(r5, r11, r8)     // Catch:{ all -> 0x004d }
            r0 = 1495634195(0x59259113, float:2.91268012E15)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ all -> 0x004d }
            monitor-exit(r5)     // Catch:{ 0CI -> 0x0050 }
            r0 = 1
            return r0
        L_0x0045:
            X.0CI r1 = new X.0CI     // Catch:{ all -> 0x004d }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x004d }
            r1.<init>(r0)     // Catch:{ all -> 0x004d }
            throw r1     // Catch:{ all -> 0x004d }
        L_0x004d:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ 0CI -> 0x0050 }
            throw r0     // Catch:{ 0CI -> 0x0050 }
        L_0x0050:
            r2 = move-exception
            java.lang.String r1 = "FbnsConnectionManager"
            java.lang.String r0 = "exception/subscribe"
            X.C010708t.A0S(r1, r2, r0)
            X.0CE r1 = X.AnonymousClass0CE.A0J
            X.0FG r0 = X.AnonymousClass0FG.CONNECTION_LOST
            r10.A0L(r5, r1, r0)
        L_0x005f:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A0d(java.util.List):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r5.A0A() == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0e(java.util.List r11) {
        /*
            r10 = this;
            r3 = 0
            if (r11 == 0) goto L_0x005f
            boolean r0 = r11.isEmpty()
            if (r0 != 0) goto L_0x005f
            X.0C8 r5 = r10.A0n
            if (r5 == 0) goto L_0x0014
            boolean r1 = r5.A0A()
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 == 0) goto L_0x005f
            java.util.concurrent.atomic.AtomicInteger r0 = X.AnonymousClass0C8.A0e     // Catch:{ 0CI -> 0x0050 }
            int r8 = r0.getAndIncrement()     // Catch:{ 0CI -> 0x0050 }
            r0 = 65535(0xffff, float:9.1834E-41)
            r8 = r8 & r0
            X.0BN r4 = r10.A0L     // Catch:{ 0CI -> 0x0050 }
            java.lang.String r6 = "callUnSub"
            X.0CL r7 = X.AnonymousClass0CL.A0B     // Catch:{ 0CI -> 0x0050 }
            int r9 = r10.A07()     // Catch:{ 0CI -> 0x0050 }
            r4.A01(r5, r6, r7, r8, r9)     // Catch:{ 0CI -> 0x0050 }
            monitor-enter(r5)     // Catch:{ 0CI -> 0x0050 }
            boolean r0 = r5.A0A()     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x0045
            java.util.concurrent.ExecutorService r2 = r5.A0G     // Catch:{ all -> 0x004d }
            X.0S1 r1 = new X.0S1     // Catch:{ all -> 0x004d }
            r1.<init>(r5, r11, r8)     // Catch:{ all -> 0x004d }
            r0 = 167544367(0x9fc862f, float:6.079298E-33)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ all -> 0x004d }
            monitor-exit(r5)     // Catch:{ 0CI -> 0x0050 }
            r0 = 1
            return r0
        L_0x0045:
            X.0CI r1 = new X.0CI     // Catch:{ all -> 0x004d }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x004d }
            r1.<init>(r0)     // Catch:{ all -> 0x004d }
            throw r1     // Catch:{ all -> 0x004d }
        L_0x004d:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ 0CI -> 0x0050 }
            throw r0     // Catch:{ 0CI -> 0x0050 }
        L_0x0050:
            r2 = move-exception
            java.lang.String r1 = "FbnsConnectionManager"
            java.lang.String r0 = "exception/unsubscribe"
            X.C010708t.A0S(r1, r2, r0)
            X.0CE r1 = X.AnonymousClass0CE.A0J
            X.0FG r0 = X.AnonymousClass0FG.CONNECTION_LOST
            r10.A0L(r5, r1, r0)
        L_0x005f:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A0e(java.util.List):boolean");
    }

    private Pair A00(List list, List list2) {
        ArrayList arrayList;
        ArrayList arrayList2;
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
                if (!this.A0R.containsKey(subscribeTopic.A01)) {
                    this.A0R.put(subscribeTopic.A01, subscribeTopic);
                }
            }
        }
        if (list2 != null) {
            Iterator it2 = list2.iterator();
            while (it2.hasNext()) {
                SubscribeTopic subscribeTopic2 = (SubscribeTopic) it2.next();
                if (this.A0R.containsKey(subscribeTopic2.A01)) {
                    this.A0R.remove(subscribeTopic2.A01);
                }
            }
        }
        AnonymousClass0C8 r7 = this.A0n;
        if (r7 == null) {
            return null;
        }
        Map map = this.A0R;
        synchronized (r7.A0F) {
            arrayList = null;
            for (SubscribeTopic subscribeTopic3 : map.values()) {
                if (!r7.A0F.containsKey(subscribeTopic3.A01)) {
                    r7.A0F.put(subscribeTopic3.A01, subscribeTopic3);
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(subscribeTopic3);
                }
            }
            Iterator it3 = r7.A0F.entrySet().iterator();
            arrayList2 = null;
            while (it3.hasNext()) {
                Map.Entry entry = (Map.Entry) it3.next();
                if (!map.containsKey(entry.getKey())) {
                    if (arrayList2 == null) {
                        arrayList2 = new ArrayList();
                    }
                    arrayList2.add(entry.getValue());
                    it3.remove();
                }
            }
            boolean z = false;
            if (map.size() == r7.A0F.size()) {
                z = true;
            }
            AnonymousClass0A1.A02(z);
        }
        if (arrayList == null && arrayList2 == null) {
            return null;
        }
        return new Pair(arrayList, arrayList2);
    }

    public static void A01(AnonymousClass0AH r3) {
        AnonymousClass0C8 r2 = r3.A0o;
        if (r2 != null) {
            r3.A0o = null;
            r3.A00 = 0;
            r2.A0Y = null;
            r2.A06(AnonymousClass0CE.A01);
        }
    }

    public static void A02(AnonymousClass0AH r4) {
        AnonymousClass0C8 A0B2;
        AnonymousClass0C8 r2;
        ((AtomicLong) ((C01850Bw) r4.A0A.A07(C01850Bw.class)).A00(AnonymousClass0C3.A02)).incrementAndGet();
        r4.A0i.set(SystemClock.elapsedRealtime());
        Thread.currentThread().setPriority(r4.A0B.A03().A0F);
        r4.A0H();
        if (r4.A0o != null) {
            C010708t.A0P("FbnsConnectionManager", "Using preemptive client op %d", Integer.valueOf(r4.A00));
            A0B2 = r4.A0o;
            r4.A0o = null;
            r4.A00 = 0;
        } else {
            A0B2 = r4.A0B();
        }
        synchronized (r4) {
            r2 = r4.A0n;
            r4.A0n = A0B2;
        }
        if (r2 != null) {
            C010708t.A0J("FbnsConnectionManager", "connecting new client without disconnecting old one");
            r4.A0L(r2, AnonymousClass0CE.A04, AnonymousClass0FG.DISCONNECTED);
        }
        r4.A01 = System.currentTimeMillis();
        r4.A0I.BTa();
    }

    public static void A04(AnonymousClass0AH r4, C04140Sd r5, boolean z) {
        String str;
        if (r5 != null) {
            try {
                str = C04140Sd.A00(r5, r5.A00).toString();
            } catch (JSONException unused) {
                str = BuildConfig.FLAVOR;
            }
            if (!TextUtils.isEmpty(str)) {
                try {
                    Integer num = AnonymousClass07B.A01;
                    if (z) {
                        num = AnonymousClass07B.A00;
                    }
                    r4.A08("/mqtt_health_stats", AnonymousClass0ED.A00(str), num, null);
                } catch (AnonymousClass0CI unused2) {
                }
            }
        }
    }

    public static void A05(AnonymousClass0AH r18, AnonymousClass0FG r19, C01540Aq r20) {
        String str;
        AnonymousClass0BL r1;
        int i;
        long j;
        AnonymousClass0AH r2 = r18;
        AnonymousClass0C8 r0 = r2.A0n;
        if (r0 != null) {
            str = r0.A0C.AiA();
        } else {
            str = BuildConfig.FLAVOR;
        }
        AnonymousClass0BN r6 = r2.A0L;
        StringBuilder sb = new StringBuilder("Connection lost ");
        AnonymousClass0FG r7 = r19;
        sb.append(r7);
        sb.append(", ");
        sb.append(str);
        AnonymousClass0CI r5 = new AnonymousClass0CI(sb.toString(), null);
        ArrayList<AnonymousClass0CV> arrayList = new ArrayList<>();
        synchronized (r6.A04) {
            arrayList.addAll(r6.A04.values());
            r6.A04.clear();
        }
        arrayList.size();
        for (AnonymousClass0CV r8 : arrayList) {
            r8.A03(r5);
            int AwM = r8.AwM();
            boolean z = r8 instanceof C02070Cs;
            if (z) {
                AwM = ((C02070Cs) r8).A00;
            }
            if (z) {
                i = ((C02070Cs) r8).A01;
            } else {
                i = 0;
            }
            AnonymousClass0C8 r02 = r8.A03;
            if (r02 == null) {
                j = 0;
            } else {
                j = r02.A0W;
            }
            r6.A00.A05("abort", r8.A05, AnonymousClass08G.A00(AnonymousClass07B.A01), r8.A01, AwM, r5, i, j);
        }
        r5.getMessage();
        switch (r7.ordinal()) {
            case 1:
                r2.A0A.A0E = C01770Bo.A03;
                long j2 = r2.A0e;
                if (j2 <= 0 || (SystemClock.elapsedRealtime() - j2) / 1000 >= ((long) r2.A0B.A03().A0L)) {
                    AnonymousClass0BL r12 = r2.A0P;
                    synchronized (r12) {
                        r12.A07 = false;
                    }
                    r1 = r2.A0P;
                    synchronized (r1) {
                        AnonymousClass0BL.A00(r1);
                    }
                } else {
                    r1 = r2.A0P;
                    synchronized (r1) {
                        r1.A07 = true;
                    }
                }
                break;
            case 0:
                r2.A0P.A05();
                break;
        }
        C01540Aq r3 = r20;
        if (!r3.A02() || r3.A01() != AnonymousClass0SB.A0E) {
            r2.A0V = false;
        } else {
            r2.A0V = true;
        }
        r2.A0I.BTg(r3);
    }

    public int A07() {
        return this.A0B.A03().A0I;
    }

    public long A09() {
        AnonymousClass0C8 r1 = this.A0n;
        if (r1 == null || !r1.A09()) {
            return 0;
        }
        return SystemClock.elapsedRealtime() - r1.A0W;
    }

    public AnonymousClass089 A0A() {
        AnonymousClass0C8 r0 = this.A0n;
        if (r0 == null) {
            return AnonymousClass089.DISCONNECTED;
        }
        return r0.A0Z;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r25v2, resolved type: X.0C7} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r25v3, resolved type: X.0PK} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r25v7, resolved type: X.0PK} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: X.0PK} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0C8 A0B() {
        /*
            r47 = this;
            r6 = r47
            java.util.Map r2 = r6.A0R
            monitor-enter(r2)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x01ae }
            java.util.Map r0 = r6.A0R     // Catch:{ all -> 0x01ae }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x01ae }
            r1.<init>(r0)     // Catch:{ all -> 0x01ae }
            X.0AY r0 = r6.A0O     // Catch:{ all -> 0x01ae }
            java.util.List r29 = r0.AiC(r1)     // Catch:{ all -> 0x01ae }
            monitor-exit(r2)     // Catch:{ all -> 0x01ae }
            X.0BI r5 = r6.A0J
            X.0AX r0 = r5.A03
            X.07h r0 = r0.A0D
            X.07i r2 = r0.A03()
            X.0Ac r0 = r5.A04
            X.0AU r0 = r0.A0M
            r0.BIb()
            java.lang.String r3 = r0.getDeviceId()
            X.0Ac r0 = r5.A04
            X.0AU r0 = r0.A0M
            java.lang.String r1 = r0.AkN()
            boolean r0 = r5.A08
            if (r0 != 0) goto L_0x0054
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 != 0) goto L_0x0044
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 == 0) goto L_0x0054
        L_0x0044:
            X.0Ac r0 = r5.A04
            X.0AQ r0 = r0.A0I
            r0.clear()
            X.0Ac r0 = r5.A04
            X.0AU r1 = r0.A0M
            X.0C5 r0 = X.AnonymousClass0C5.A01
            r1.CKB(r0)
        L_0x0054:
            X.0C6 r4 = new X.0C6
            java.lang.String r0 = r2.A0R
            r46 = r0
            java.lang.String r0 = r2.A0S
            r45 = r0
            int r0 = r2.A0K
            r44 = r0
            int r0 = r2.A07
            r43 = r0
            boolean r0 = r2.A0V
            r42 = r0
            X.0Ac r0 = r5.A04
            X.0AQ r0 = r0.A0I
            X.0Bu r16 = r0.Arn()
            X.0Ac r0 = r5.A04
            X.0AU r0 = r0.A0M
            java.lang.String r17 = r0.getDeviceId()
            X.0Ac r0 = r5.A04
            X.0AU r0 = r0.A0M
            java.lang.String r18 = r0.AkN()
            X.0Ac r1 = r5.A04
            X.0AU r0 = r1.A0M
            java.lang.String r19 = r0.AdF()
            X.0AX r3 = r5.A03
            X.0Ai r0 = r3.A08
            r41 = r0
            java.util.concurrent.atomic.AtomicInteger r0 = r3.A0T
            r21 = r0
            int r0 = r2.A0H
            r22 = r0
            int r0 = r2.A0N
            r20 = r0
            int r15 = r2.A0B
            boolean r3 = r2.A0U
            boolean r0 = r1.A0g
            if (r3 == 0) goto L_0x01aa
            r25 = 1
            if (r0 == 0) goto L_0x00aa
            r25 = 2
        L_0x00aa:
            int r14 = r2.A0E
            int r13 = r2.A0G
            X.0Bg r12 = r1.A0F
            X.0Bg r0 = r1.A0C
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r30 = r0.booleanValue()
            X.0Ac r0 = r5.A04
            X.0Bg r0 = r0.A0E
            if (r0 == 0) goto L_0x01a6
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r31 = r0.booleanValue()
        L_0x00cc:
            X.0Ac r0 = r5.A04
            boolean r11 = r0.A0e
            java.util.Map r10 = r0.A0V
            boolean r9 = r0.A0b
            java.lang.String r8 = r0.A0U
            boolean r7 = r0.A0X
            boolean r3 = r0.A0Y
            int r2 = r0.A00
            boolean r1 = r0.A0Z
            boolean r0 = r0.A0a
            r23 = r20
            r24 = r15
            r26 = r14
            r27 = r13
            r28 = r12
            r32 = r11
            r33 = r10
            r34 = r9
            r35 = r8
            r36 = r7
            r37 = r3
            r38 = r2
            r39 = r1
            r40 = r0
            r10 = r4
            r11 = r46
            r12 = r45
            r13 = r44
            r14 = r43
            r15 = r42
            r20 = r41
            r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40)
            X.0Ac r1 = r5.A04
            X.0AZ r0 = r1.A0N
            X.0BD r7 = r5.A02
            java.util.concurrent.ScheduledExecutorService r3 = r5.A07
            X.0BE r2 = r5.A05
            X.0AY r1 = r1.A0P
            X.0C7 r25 = r0.A00(r4)
            if (r25 != 0) goto L_0x012d
            X.0PK r25 = new X.0PK
            X.0B6 r0 = r0.A01
            r8 = r25
            r9 = r7
            r10 = r0
            r11 = r4
            r12 = r3
            r13 = r2
            r14 = r1
            r8.<init>(r9, r10, r11, r12, r13, r14)
        L_0x012d:
            X.0AX r0 = r5.A03
            X.0B7 r1 = r0.A0C
            X.0Ac r0 = r5.A04
            X.0AZ r0 = r0.A0N
            java.lang.String r0 = r0.A03
            r1.A0F = r0
            X.0C8 r13 = new X.0C8
            X.0AX r1 = r5.A03
            X.0At r0 = r1.A0H
            r34 = r0
            X.0Al r0 = r5.A01
            r33 = r0
            X.0B6 r0 = r1.A0A
            r16 = r0
            X.0B7 r0 = r1.A0C
            r17 = r0
            java.util.concurrent.ExecutorService r15 = r5.A06
            X.0Ag r12 = r1.A0B
            X.0Ac r0 = r5.A04
            X.0Aa r14 = r0.A09
            X.0AT r11 = r0.A0O
            X.0AQ r10 = r0.A0I
            X.0BA r9 = r1.A09
            X.0Bg r8 = r0.A0G
            java.util.concurrent.atomic.AtomicReference r7 = r0.A0W
            X.0Bg r5 = r0.A0E
            boolean r3 = r0.A0i
            boolean r2 = r0.A0h
            boolean r1 = r0.A0f
            java.lang.Long r0 = r0.A0R
            r18 = r4
            r19 = r15
            r20 = r12
            r21 = r14
            r22 = r11
            r23 = r10
            r24 = r9
            r26 = r8
            r27 = r7
            r28 = r5
            r29 = r3
            r30 = r2
            r31 = r1
            r32 = r0
            r15 = r33
            r14 = r34
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)
            X.0CC r1 = new X.0CC
            boolean r0 = r6.A0W
            r1.<init>(r6, r13, r0)
            r13.A0Y = r1
            r0 = 0
            r13.A0c = r0
            java.util.List r2 = r6.A0W(r13)
            boolean r1 = r6.A0Z
            r0 = r1 ^ 1
            r6.A0Z = r0
            r13.A08(r2, r1)
            return r13
        L_0x01a6:
            r31 = 0
            goto L_0x00cc
        L_0x01aa:
            r25 = 0
            goto L_0x00aa
        L_0x01ae:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x01ae }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A0B():X.0C8");
    }

    public Set A0C() {
        HashSet hashSet;
        synchronized (this.A0R) {
            hashSet = new HashSet(this.A0R.keySet());
        }
        return hashSet;
    }

    public void A0D() {
        if (!this.A0I.CDh()) {
            A0K(AnonymousClass0CE.A07);
            return;
        }
        AnonymousClass0C8 r2 = this.A0n;
        if (r2 == null) {
            A02(this);
        } else if (!r2.A0A()) {
            A0L(r2, AnonymousClass0CE.A04, AnonymousClass0FG.DISCONNECTED);
            A02(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        if (r2.A0T.get() == false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0E() {
        /*
            r2 = this;
            X.0BW r0 = r2.A0H
            r0.A00()
            boolean r0 = r2.A0X
            if (r0 == 0) goto L_0x0012
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.A0T
            boolean r1 = r0.get()
            r0 = 1
            if (r1 != 0) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            if (r0 == 0) goto L_0x001b
            X.0BW r0 = r2.A0H
            r0.A01()
            return
        L_0x001b:
            X.0BO r0 = r2.A0F
            r0.A04()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A0E():void");
    }

    public void A0F() {
        C01800Br r3 = new C01800Br(this);
        this.A0c = r3;
        this.A04.registerReceiver(r3, new IntentFilter("android.os.action.POWER_SAVE_MODE_CHANGED"), null, this.A05);
        C01810Bs r32 = new C01810Bs(this);
        this.A0b = r32;
        this.A04.registerReceiver(r32, new IntentFilter("com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED"), null, this.A05);
        C01570At r2 = this.A0C;
        AnonymousClass0AL r1 = this.A0j;
        synchronized (r2) {
            r2.A07.add(r1);
        }
        this.A0N.A02();
    }

    public void A0G() {
        this.A0F.A03();
        this.A0H.A00();
        C01470Aj r3 = this.A0D;
        boolean equals = r3.A03.getLooper().equals(Looper.myLooper());
        C01700Bh.A00(equals, "ScreenStateListener unregistration should be called on MqttThread. Current Looper:" + Looper.myLooper());
        try {
            r3.A02.unregisterReceiver(r3.A01);
        } catch (IllegalArgumentException unused) {
        }
        r3.A05.set(null);
        C01570At r2 = this.A0C;
        AnonymousClass0AL r1 = this.A0j;
        synchronized (r2) {
            r2.A07.remove(r1);
        }
        BroadcastReceiver broadcastReceiver = this.A0c;
        if (broadcastReceiver != null) {
            try {
                this.A04.unregisterReceiver(broadcastReceiver);
            } catch (IllegalArgumentException e) {
                C010708t.A0S("FbnsConnectionManager", e, "Failed to unregister broadcast receiver");
            }
            this.A0c = null;
        }
        BroadcastReceiver broadcastReceiver2 = this.A0b;
        if (broadcastReceiver2 != null) {
            try {
                this.A04.unregisterReceiver(broadcastReceiver2);
            } catch (IllegalArgumentException e2) {
                C010708t.A0S("FbnsConnectionManager", e2, "Failed to unregister broadcast receiver");
            }
            this.A0b = null;
        }
        this.A0N.A03();
        this.A0M.A03();
    }

    public void A0H() {
        int Adx;
        boolean z = this.A0T.get();
        AnonymousClass0AV r0 = this.A0G;
        if (z) {
            Adx = r0.AnM();
        } else {
            Adx = r0.Adx();
        }
        if (this.A0U.getAndSet(Adx) != Adx) {
            A0E();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0046, code lost:
        if (r2.A09() == false) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (r5.A0T.get() == false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0J(X.C01770Bo r6) {
        /*
            r5 = this;
            boolean r0 = r5.A0X
            if (r0 == 0) goto L_0x000d
            java.util.concurrent.atomic.AtomicBoolean r0 = r5.A0T
            boolean r1 = r0.get()
            r0 = 1
            if (r1 != 0) goto L_0x000e
        L_0x000d:
            r0 = 0
        L_0x000e:
            if (r0 == 0) goto L_0x001e
            X.0BW r1 = r5.A0H
            monitor-enter(r1)
            boolean r0 = r1.A00     // Catch:{ all -> 0x001b }
            if (r0 != 0) goto L_0x002e
            r1.A01()     // Catch:{ all -> 0x001b }
            goto L_0x002e
        L_0x001b:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x001e:
            X.0BO r1 = r5.A0F
            monitor-enter(r1)
            boolean r0 = r1.A03     // Catch:{ all -> 0x0076 }
            if (r0 != 0) goto L_0x0029
            r1.A04()     // Catch:{ all -> 0x0076 }
            goto L_0x002c
        L_0x0029:
            android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0076 }
        L_0x002c:
            monitor-exit(r1)
            goto L_0x002f
        L_0x002e:
            monitor-exit(r1)
        L_0x002f:
            X.0AG r0 = r5.A0I
            boolean r0 = r0.CDh()
            if (r0 != 0) goto L_0x003d
            X.0CE r0 = X.AnonymousClass0CE.A07
            r5.A0K(r0)
            return
        L_0x003d:
            X.0C8 r2 = r5.A0n
            if (r2 == 0) goto L_0x0048
            boolean r1 = r2.A09()
            r0 = 1
            if (r1 != 0) goto L_0x0049
        L_0x0048:
            r0 = 0
        L_0x0049:
            if (r0 == 0) goto L_0x006f
            X.0AQ r0 = r5.A0E
            X.0Bu r0 = r0.Arn()
            java.lang.Object r1 = r0.first
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r0 = r2.A0a
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0066
            X.0CE r0 = X.AnonymousClass0CE.A02
            r5.A0K(r0)
        L_0x0062:
            r5.A0I(r6)
            return
        L_0x0066:
            long r3 = r5.A03
            long r1 = r5.A0e
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0075
            goto L_0x0062
        L_0x006f:
            boolean r0 = r5.A0R()
            if (r0 == 0) goto L_0x0062
        L_0x0075:
            return
        L_0x0076:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A0J(X.0Bo):void");
    }

    public void A0K(AnonymousClass0CE r3) {
        AnonymousClass0BL r1 = this.A0P;
        synchronized (r1) {
            AnonymousClass0BL.A00(r1);
        }
        A01(this);
        A0L(this.A0n, r3, AnonymousClass0FG.BY_REQUEST);
    }

    public void A0M(PrintWriter printWriter) {
        String str;
        printWriter.println("[ FbnsConnectionManager ]");
        printWriter.println("keepAliveIntervalSeconds=" + this.A0U);
        NetworkInfo A032 = this.A0C.A03();
        if (A032 != null) {
            str = A032.toString();
        } else {
            str = "null";
        }
        printWriter.println(AnonymousClass08S.A0J("networkInfo=", str));
        if (this.A0q != null) {
            printWriter.println(AnonymousClass08S.A0J("lastConnectLostTime=", new Date((System.currentTimeMillis() + this.A0l) - SystemClock.elapsedRealtime()).toString()));
            printWriter.println(AnonymousClass08S.A0J("lastConnectLostReason=", this.A0q));
        }
        AnonymousClass0C8 r3 = this.A0n;
        if (r3 != null) {
            synchronized (r3) {
                printWriter.println("[ MqttClient ]");
                printWriter.println("state=" + r3.A0Z);
                printWriter.println(AnonymousClass08S.A0J("lastMessageSent=", AnonymousClass0C8.A02(r3, r3.A0U)));
                printWriter.println(AnonymousClass08S.A0J("lastMessageReceived=", AnonymousClass0C8.A02(r3, r3.A0T)));
                printWriter.println(AnonymousClass08S.A0J("connectionEstablished=", AnonymousClass0C8.A02(r3, r3.A0R)));
                printWriter.println(AnonymousClass08S.A0J("lastPing=", AnonymousClass0C8.A02(r3, r3.A0V)));
                printWriter.println(AnonymousClass08S.A0J("peer=", r3.A0C.B0g()));
            }
            return;
        }
        printWriter.println("mMqttClient=null");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0060, code lost:
        if (r8.A09() == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007e, code lost:
        if (r8.A09() == false) goto L_0x0080;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0N(java.lang.String r14) {
        /*
            r13 = this;
            java.lang.String r2 = "FbnsConnectionManager"
            X.0C8 r8 = r13.A0n
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ 0CI -> 0x00b2 }
            long r0 = r13.A0s     // Catch:{ 0CI -> 0x00b2 }
            long r6 = r3 - r0
            r0 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r0
            X.07h r0 = r13.A0B     // Catch:{ 0CI -> 0x00b2 }
            X.07i r0 = r0.A03()     // Catch:{ 0CI -> 0x00b2 }
            int r0 = r0.A0M     // Catch:{ 0CI -> 0x00b2 }
            long r0 = (long) r0     // Catch:{ 0CI -> 0x00b2 }
            int r5 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r5 < 0) goto L_0x00bf
            r13.A0s = r3     // Catch:{ 0CI -> 0x00b2 }
            X.0CN r0 = X.AnonymousClass0CN.A02     // Catch:{ 0CI -> 0x00b2 }
            r0.A01 = r14     // Catch:{ 0CI -> 0x00b2 }
            boolean r0 = r13.A0Y     // Catch:{ 0CI -> 0x00b2 }
            if (r0 == 0) goto L_0x0077
            java.util.concurrent.atomic.AtomicBoolean r0 = r13.A0T     // Catch:{ 0CI -> 0x00b2 }
            boolean r0 = r0.get()     // Catch:{ 0CI -> 0x00b2 }
            if (r0 == 0) goto L_0x0044
            X.0B7 r1 = r13.A0A     // Catch:{ 0CI -> 0x00b2 }
            java.lang.Class<X.08I> r0 = X.AnonymousClass08I.class
            X.0By r1 = r1.A07(r0)     // Catch:{ 0CI -> 0x00b2 }
            X.08I r1 = (X.AnonymousClass08I) r1     // Catch:{ 0CI -> 0x00b2 }
            X.08J r0 = X.AnonymousClass08J.A06     // Catch:{ 0CI -> 0x00b2 }
            java.lang.Object r0 = r1.A00(r0)     // Catch:{ 0CI -> 0x00b2 }
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0     // Catch:{ 0CI -> 0x00b2 }
            r0.incrementAndGet()     // Catch:{ 0CI -> 0x00b2 }
            goto L_0x0059
        L_0x0044:
            X.0B7 r1 = r13.A0A     // Catch:{ 0CI -> 0x00b2 }
            java.lang.Class<X.08I> r0 = X.AnonymousClass08I.class
            X.0By r1 = r1.A07(r0)     // Catch:{ 0CI -> 0x00b2 }
            X.08I r1 = (X.AnonymousClass08I) r1     // Catch:{ 0CI -> 0x00b2 }
            X.08J r0 = X.AnonymousClass08J.A01     // Catch:{ 0CI -> 0x00b2 }
            java.lang.Object r0 = r1.A00(r0)     // Catch:{ 0CI -> 0x00b2 }
            java.util.concurrent.atomic.AtomicLong r0 = (java.util.concurrent.atomic.AtomicLong) r0     // Catch:{ 0CI -> 0x00b2 }
            r0.incrementAndGet()     // Catch:{ 0CI -> 0x00b2 }
        L_0x0059:
            if (r8 == 0) goto L_0x0062
            boolean r1 = r8.A09()     // Catch:{ 0CI -> 0x00b2 }
            r0 = 1
            if (r1 != 0) goto L_0x0063
        L_0x0062:
            r0 = 0
        L_0x0063:
            if (r0 == 0) goto L_0x00bf
            long r0 = r8.A0W     // Catch:{ 0CI -> 0x00b2 }
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ 0CI -> 0x00b2 }
            long r3 = r3 - r0
            X.0B7 r0 = r13.A0A     // Catch:{ 0CI -> 0x00b2 }
            X.0Sd r1 = r0.A05(r3)     // Catch:{ 0CI -> 0x00b2 }
            r0 = 0
            A04(r13, r1, r0)     // Catch:{ 0CI -> 0x00b2 }
            return
        L_0x0077:
            if (r8 == 0) goto L_0x0080
            boolean r1 = r8.A09()     // Catch:{ 0CI -> 0x00b2 }
            r0 = 1
            if (r1 != 0) goto L_0x0081
        L_0x0080:
            r0 = 0
        L_0x0081:
            if (r0 == 0) goto L_0x00bf
            X.0BN r7 = r13.A0L     // Catch:{ 0CI -> 0x00b2 }
            X.0CL r10 = X.AnonymousClass0CL.A06     // Catch:{ 0CI -> 0x00b2 }
            r11 = -1
            int r12 = r13.A07()     // Catch:{ 0CI -> 0x00b2 }
            java.lang.String r9 = "callPing"
            r7.A01(r8, r9, r10, r11, r12)     // Catch:{ 0CI -> 0x00b2 }
            monitor-enter(r8)     // Catch:{ 0CI -> 0x00b2 }
            boolean r0 = r8.A09()     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x00a7
            java.util.concurrent.ExecutorService r3 = r8.A0G     // Catch:{ all -> 0x00af }
            X.0Dd r1 = new X.0Dd     // Catch:{ all -> 0x00af }
            r1.<init>(r8)     // Catch:{ all -> 0x00af }
            r0 = 1398772463(0x535f92ef, float:9.6024284E11)
            X.AnonymousClass07A.A04(r3, r1, r0)     // Catch:{ all -> 0x00af }
            monitor-exit(r8)     // Catch:{ 0CI -> 0x00b2 }
            return
        L_0x00a7:
            X.0CI r1 = new X.0CI     // Catch:{ all -> 0x00af }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x00af }
            r1.<init>(r0)     // Catch:{ all -> 0x00af }
            throw r1     // Catch:{ all -> 0x00af }
        L_0x00af:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ 0CI -> 0x00b2 }
            throw r0     // Catch:{ 0CI -> 0x00b2 }
        L_0x00b2:
            r1 = move-exception
            java.lang.String r0 = "exception/send_keepalive"
            X.C010708t.A0R(r2, r1, r0)
            X.0CE r1 = X.AnonymousClass0CE.A0J
            X.0FG r0 = X.AnonymousClass0FG.CONNECTION_LOST
            r13.A0L(r8, r1, r0)
        L_0x00bf:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A0N(java.lang.String):void");
    }

    public void A0O(List list, List list2) {
        synchronized (this.A0R) {
            Pair A002 = A00(list, list2);
            if (A002 != null) {
                AnonymousClass07A.A04(this.A0S, new AnonymousClass0SM(this, A002), -720075784);
            }
        }
    }

    public boolean A0Q() {
        AnonymousClass0C8 r0 = this.A0n;
        if (r0 == null || !r0.A09()) {
            return false;
        }
        return true;
    }

    public boolean A0R() {
        AnonymousClass0C8 r0 = this.A0n;
        if (r0 == null || !r0.A0A()) {
            return false;
        }
        return true;
    }

    public boolean A0S(long j) {
        try {
            AnonymousClass0C8 r0 = this.A0n;
            if (r0 == null || !r0.A0A()) {
                A0H();
                this.A0A.A0E = C01770Bo.A05;
                Future A032 = this.A0P.A03();
                if (A032 != null) {
                    A032.get(j, TimeUnit.MILLISECONDS);
                }
            }
            AnonymousClass0C8 r1 = this.A0n;
            if (r1 == null) {
                return false;
            }
            if (r1.A09()) {
                return true;
            }
            r1.A05(j);
            return r1.A09();
        } catch (InterruptedException unused) {
            C010708t.A0J("FbnsConnectionManager", "exception/connect_interrupted");
            Thread.currentThread().interrupt();
            return false;
        } catch (ExecutionException e) {
            C010708t.A0S("FbnsConnectionManager", e, "exception/execution_exception");
            return false;
        } catch (CancellationException | TimeoutException unused2) {
            return false;
        }
    }

    public boolean A0T(String str, byte[] bArr, long j, AnonymousClass0CZ r15, long j2, String str2) {
        String str3 = str;
        AnonymousClass0CZ r4 = r15;
        C01540Aq A0U2 = A0U(str3, bArr, AnonymousClass07B.A01, r4, A07(), j2, str2, null);
        if (!A0U2.A02()) {
            return false;
        }
        try {
            A0U2.A01();
            ((AnonymousClass0CW) A0U2.A01()).CMt(j);
            return true;
        } catch (ExecutionException | TimeoutException e) {
            throw e;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0049, code lost:
        if (r1 == false) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0124, code lost:
        if (r0.isConnected() == false) goto L_0x0126;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.AnonymousClass0AH r7, android.content.Intent r8) {
        /*
            java.lang.String r1 = r8.getAction()
            java.lang.String r0 = "com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED"
            boolean r0 = X.AnonymousClass0A2.A00(r1, r0)
            java.lang.String r4 = "FbnsConnectionManager"
            if (r0 == 0) goto L_0x007a
            X.07h r0 = r7.A0B
            r0.A04()
            X.07h r0 = r7.A0B
            X.07i r3 = r0.A03()
            X.07i r4 = r7.A0m
            java.lang.String r1 = r3.A0R
            java.lang.String r0 = r4.A0R
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x004b
            int r1 = r3.A0K
            int r0 = r4.A0K
            if (r1 != r0) goto L_0x004b
            int r1 = r3.A07
            int r0 = r4.A07
            if (r1 != r0) goto L_0x004b
            boolean r1 = r3.A0V
            boolean r0 = r4.A0V
            if (r1 != r0) goto L_0x004b
            java.lang.String r2 = r3.A0S
            java.lang.String r1 = r4.A0S
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 == 0) goto L_0x0067
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 == 0) goto L_0x0067
            r1 = 1
        L_0x0048:
            r0 = 0
            if (r1 != 0) goto L_0x004c
        L_0x004b:
            r0 = 1
        L_0x004c:
            r7.A0m = r3
            if (r0 == 0) goto L_0x005b
            X.0CE r0 = X.AnonymousClass0CE.A06
            r7.A0K(r0)
            X.0Bo r0 = X.C01770Bo.A02
            r7.A0J(r0)
        L_0x005a:
            return
        L_0x005b:
            boolean r0 = r7.A0R()
            if (r0 != 0) goto L_0x005a
            X.0Bo r0 = X.C01770Bo.A02
            r7.A0J(r0)
            return
        L_0x0067:
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x0078
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0078
            boolean r1 = r2.equals(r1)
            goto L_0x0048
        L_0x0078:
            r1 = 0
            goto L_0x0048
        L_0x007a:
            java.lang.String r0 = "android.os.action.POWER_SAVE_MODE_CHANGED"
            boolean r0 = X.AnonymousClass0A2.A00(r1, r0)
            if (r0 == 0) goto L_0x00cd
            java.lang.reflect.Method r0 = r7.A0d     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            r3 = 0
            if (r0 != 0) goto L_0x0093
            java.lang.Class<android.os.PowerManager> r2 = android.os.PowerManager.class
            java.lang.String r1 = "isPowerSaveMode"
            java.lang.Class[] r0 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.reflect.Method r0 = r2.getDeclaredMethod(r1, r0)     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            r7.A0d = r0     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
        L_0x0093:
            X.0Ad r2 = r7.A08     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.String r1 = "power"
            java.lang.Class<android.os.PowerManager> r0 = android.os.PowerManager.class
            X.0Aq r1 = r2.A00(r1, r0)     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            boolean r0 = r1.A02()     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            if (r0 == 0) goto L_0x005a
            java.lang.reflect.Method r2 = r7.A0d     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.Object r1 = r1.A01()     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.Object r0 = r2.invoke(r1, r0)     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.String r1 = r0.toString()     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            X.0B6 r3 = r7.A09     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.String r0 = "pow"
            java.lang.String[] r0 = new java.lang.String[]{r0, r1}     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.util.Map r2 = X.C01740Bl.A00(r0)     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            java.lang.String r1 = "mqtt_device_state"
            r3.A07(r1, r2)     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            X.07g r0 = r3.A00     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            if (r0 == 0) goto L_0x005a
            r0.BIq(r1, r2)     // Catch:{ NoSuchMethodException -> 0x0185, IllegalAccessException -> 0x017e, InvocationTargetException -> 0x0177 }
            goto L_0x0176
        L_0x00cd:
            java.lang.String r0 = "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"
            boolean r0 = X.AnonymousClass0A2.A00(r1, r0)
            if (r0 == 0) goto L_0x005a
            X.0B6 r6 = r7.A09
            X.0At r0 = r7.A0C
            long r3 = r0.A02()
            X.0At r0 = r7.A0C
            android.net.NetworkInfo r2 = r0.A04()
            r0 = 0
            java.lang.String[] r0 = new java.lang.String[r0]
            java.util.Map r5 = X.C01740Bl.A00(r0)
            java.lang.String r1 = java.lang.Long.toString(r3)
            java.lang.String r0 = "network_session_id"
            r5.put(r0, r1)
            X.AnonymousClass0B6.A00(r6, r5, r2)
            X.0At r1 = r6.A02
            monitor-enter(r1)
            long r3 = r1.A01     // Catch:{ all -> 0x019c }
            monitor-exit(r1)
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x010b
            java.lang.String r1 = java.lang.String.valueOf(r3)
            java.lang.String r0 = "dc_ms_ago"
            r5.put(r0, r1)
        L_0x010b:
            java.lang.String r1 = "mqtt_network_changed"
            r6.A07(r1, r5)
            X.07g r0 = r6.A00
            if (r0 == 0) goto L_0x0117
            r0.BIq(r1, r5)
        L_0x0117:
            X.0At r0 = r7.A0C
            android.net.NetworkInfo r0 = r0.A03()
            if (r0 == 0) goto L_0x0126
            boolean r0 = r0.isConnected()
            r5 = 1
            if (r0 != 0) goto L_0x0127
        L_0x0126:
            r5 = 0
        L_0x0127:
            X.0At r0 = r7.A0C
            android.net.NetworkInfo r3 = r0.A04()
            android.net.NetworkInfo$State r2 = android.net.NetworkInfo.State.DISCONNECTED
            if (r3 == 0) goto L_0x0173
            int r1 = r3.getType()
            int r0 = r3.getSubtype()
            android.net.NetworkInfo$State r2 = r3.getState()
            r3.getTypeName()
            r3.getSubtypeName()
            r3.getState()
        L_0x0146:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0, r2}
            int r0 = java.util.Arrays.hashCode(r0)
            long r3 = (long) r0
            long r1 = r7.A0a
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0163
            boolean r0 = r7.A0R()
            if (r0 != 0) goto L_0x005a
        L_0x0163:
            r7.A0a = r3
            long r0 = android.os.SystemClock.elapsedRealtime()
            r7.A03 = r0
            if (r5 == 0) goto L_0x018c
            X.0Bo r0 = X.C01770Bo.A04
            r7.A0J(r0)
            return
        L_0x0173:
            r1 = 0
            r0 = 0
            goto L_0x0146
        L_0x0176:
            return
        L_0x0177:
            r1 = move-exception
            java.lang.String r0 = "exception/InvocationTargetException"
            X.C010708t.A0S(r4, r1, r0)
            return
        L_0x017e:
            r1 = move-exception
            java.lang.String r0 = "exception/IllegalAccessException"
            X.C010708t.A0S(r4, r1, r0)
            return
        L_0x0185:
            r1 = move-exception
            java.lang.String r0 = "exception/NoSuchMethodException"
            X.C010708t.A0S(r4, r1, r0)
            return
        L_0x018c:
            X.0BO r0 = r7.A0F
            r0.A03()
            X.0BW r0 = r7.A0H
            r0.A00()
            X.0CE r0 = X.AnonymousClass0CE.A08
            r7.A0K(r0)
            return
        L_0x019c:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A03(X.0AH, android.content.Intent):void");
    }

    public int A08(String str, byte[] bArr, Integer num, AnonymousClass0CZ r14) {
        String str2 = str;
        Integer num2 = num;
        C01540Aq A0U2 = A0U(str2, bArr, num2, r14, A07(), 0, null, null);
        if (!A0U2.A02()) {
            return -1;
        }
        return ((AnonymousClass0CW) A0U2.A01()).AwM();
    }

    public void A0I(C01770Bo r2) {
        A0H();
        this.A0A.A0E = r2;
        this.A0P.A04();
    }

    public void A0P(boolean z, List list, List list2) {
        Boolean bool;
        int Adx;
        A0b(list, list2);
        synchronized (this.A0k) {
            AtomicBoolean atomicBoolean = this.A0T;
            boolean z2 = false;
            if (!z) {
                z2 = true;
            }
            boolean compareAndSet = atomicBoolean.compareAndSet(z2, z);
            if (compareAndSet) {
                A0H();
            }
            Integer num = null;
            if (compareAndSet) {
                bool = Boolean.valueOf(z);
            } else {
                bool = null;
            }
            if (compareAndSet) {
                if (this.A0T.get()) {
                    Adx = this.A0G.AnM();
                } else {
                    Adx = this.A0G.Adx();
                }
                num = Integer.valueOf(Adx);
            }
            C010708t.A01.BFj(2);
            synchronized (this.A0R) {
                Pair A002 = A00(list, list2);
                if (bool != null || num != null || A002 != null) {
                    AnonymousClass07A.A04(this.A0S, new AnonymousClass085(this, A002, bool, num), -115999708);
                }
            }
        }
    }

    public List A0W(AnonymousClass0C8 r2) {
        return Collections.emptyList();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (r3 == X.AnonymousClass089.A03) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C01540Aq A0U(java.lang.String r29, byte[] r30, java.lang.Integer r31, X.AnonymousClass0CZ r32, int r33, long r34, java.lang.String r36, X.AnonymousClass0PF r37) {
        /*
            r28 = this;
            r5 = r28
            r7 = r31
            int r2 = X.AnonymousClass08G.A00(r7)
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            int r1 = X.AnonymousClass08G.A00(r0)
            r0 = 0
            if (r2 >= r1) goto L_0x0012
            r0 = 1
        L_0x0012:
            X.AnonymousClass0A1.A01(r0)
            X.0C8 r0 = r5.A0n
            r15 = r29
            if (r0 == 0) goto L_0x0154
            boolean r1 = r0.A0A()
            if (r1 == 0) goto L_0x0154
            X.0Al r1 = r5.A07
            X.0CU r27 = r1.A00()
            java.util.concurrent.atomic.AtomicInteger r1 = X.AnonymousClass0C8.A0e     // Catch:{ 0CI -> 0x0141 }
            int r17 = r1.getAndIncrement()     // Catch:{ 0CI -> 0x0141 }
            r1 = 65535(0xffff, float:9.1834E-41)
            r17 = r17 & r1
            X.089 r3 = r0.A0Z     // Catch:{ 0CI -> 0x0141 }
            X.089 r1 = X.AnonymousClass089.CONNECTING     // Catch:{ 0CI -> 0x0141 }
            if (r3 == r1) goto L_0x003d
            X.089 r2 = X.AnonymousClass089.CONNECT_SENT     // Catch:{ 0CI -> 0x0141 }
            r1 = 0
            if (r3 != r2) goto L_0x003e
        L_0x003d:
            r1 = 1
        L_0x003e:
            if (r1 == 0) goto L_0x006e
            long r1 = r0.A0W     // Catch:{ 0CI -> 0x0141 }
            r13 = 0
            int r3 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r3 <= 0) goto L_0x0049
            goto L_0x004c
        L_0x0049:
            r11 = 0
            goto L_0x0053
        L_0x004c:
            long r11 = android.os.SystemClock.elapsedRealtime()     // Catch:{ 0CI -> 0x0141 }
            long r1 = r0.A0W     // Catch:{ 0CI -> 0x0141 }
            long r11 = r11 - r1
        L_0x0053:
            X.07h r1 = r5.A0B     // Catch:{ 0CI -> 0x0141 }
            X.07i r1 = r1.A03()     // Catch:{ 0CI -> 0x0141 }
            int r1 = r1.A0H     // Catch:{ 0CI -> 0x0141 }
            long r3 = (long) r1     // Catch:{ 0CI -> 0x0141 }
            r9 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r9
            long r1 = r3 - r11
            int r8 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r8 >= 0) goto L_0x0068
            r1 = 0
            goto L_0x0070
        L_0x0068:
            int r8 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r8 <= 0) goto L_0x0070
            r1 = r3
            goto L_0x0070
        L_0x006e:
            r3 = 0
            goto L_0x0072
        L_0x0070:
            long r1 = r1 / r9
            int r3 = (int) r1     // Catch:{ 0CI -> 0x0141 }
        L_0x0072:
            int r6 = r33 + r3
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ 0CI -> 0x0141 }
            if (r7 != r1) goto L_0x00eb
            r4 = r37
            if (r37 != 0) goto L_0x008a
            X.0BN r1 = r5.A0L     // Catch:{ 0CI -> 0x0141 }
            X.0CL r16 = X.AnonymousClass0CL.A07     // Catch:{ 0CI -> 0x0141 }
            r13 = r1
            r14 = r0
            r18 = r6
            X.0CV r1 = r13.A01(r14, r15, r16, r17, r18)     // Catch:{ 0CI -> 0x0141 }
            goto L_0x011a
        L_0x008a:
            X.0BN r3 = r5.A0L     // Catch:{ 0CI -> 0x0141 }
            X.0CL r11 = X.AnonymousClass0CL.A07     // Catch:{ 0CI -> 0x0141 }
            X.AnonymousClass0A1.A00(r0)     // Catch:{ 0CI -> 0x0141 }
            X.0CV r1 = new X.0CV     // Catch:{ 0CI -> 0x0141 }
            long r13 = android.os.SystemClock.elapsedRealtime()     // Catch:{ 0CI -> 0x0141 }
            r10 = r15
            r12 = r17
            r8 = r1
            r9 = r0
            r8.<init>(r9, r10, r11, r12, r13)     // Catch:{ 0CI -> 0x0141 }
            java.util.Map r10 = r3.A04     // Catch:{ 0CI -> 0x0141 }
            monitor-enter(r10)     // Catch:{ 0CI -> 0x0141 }
            java.util.Map r8 = r3.A04     // Catch:{ all -> 0x00e8 }
            int r2 = r1.A01     // Catch:{ all -> 0x00e8 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x00e8 }
            java.lang.Object r9 = r8.put(r2, r1)     // Catch:{ all -> 0x00e8 }
            X.0CV r9 = (X.AnonymousClass0CV) r9     // Catch:{ all -> 0x00e8 }
            monitor-exit(r10)     // Catch:{ all -> 0x00e8 }
            if (r9 == 0) goto L_0x00cd
            r9.A01()     // Catch:{ 0CI -> 0x0141 }
            int r2 = r9.A01     // Catch:{ 0CI -> 0x0141 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r2)     // Catch:{ 0CI -> 0x0141 }
            X.0CL r2 = r9.A04     // Catch:{ 0CI -> 0x0141 }
            java.lang.String r2 = r2.name()     // Catch:{ 0CI -> 0x0141 }
            java.lang.Object[] r9 = new java.lang.Object[]{r8, r2}     // Catch:{ 0CI -> 0x0141 }
            java.lang.String r8 = "MqttOperationManager"
            java.lang.String r2 = "operation/add/duplicate; id=%d, name=%s"
            X.C010708t.A0O(r8, r2, r9)     // Catch:{ 0CI -> 0x0141 }
        L_0x00cd:
            X.0Ap r9 = r3.A03     // Catch:{ 0CI -> 0x0141 }
            X.0SH r8 = new X.0SH     // Catch:{ 0CI -> 0x0141 }
            r8.<init>(r3, r1)     // Catch:{ 0CI -> 0x0141 }
            long r2 = (long) r6     // Catch:{ 0CI -> 0x0141 }
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ 0CI -> 0x0141 }
            X.0Cf r2 = r9.schedule(r8, r2, r6)     // Catch:{ 0CI -> 0x0141 }
            r1.A02(r2)     // Catch:{ 0CI -> 0x0141 }
            X.AnonymousClass0A1.A00(r4)     // Catch:{ 0CI -> 0x0141 }
            X.0PF r3 = r1.A07     // Catch:{ 0CI -> 0x0141 }
            r2 = 0
            if (r3 != 0) goto L_0x0115
            r2 = 1
            goto L_0x0115
        L_0x00e8:
            r1 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00e8 }
            throw r1     // Catch:{ 0CI -> 0x0141 }
        L_0x00eb:
            X.0CV r1 = new X.0CV     // Catch:{ 0CI -> 0x0141 }
            X.0CL r11 = X.AnonymousClass0CL.A07     // Catch:{ 0CI -> 0x0141 }
            long r13 = android.os.SystemClock.elapsedRealtime()     // Catch:{ 0CI -> 0x0141 }
            r8 = r1
            r9 = r0
            r10 = r15
            r12 = r17
            r8.<init>(r9, r10, r11, r12, r13)     // Catch:{ 0CI -> 0x0141 }
            r1.A00()     // Catch:{ 0CI -> 0x0141 }
            X.0B6 r4 = r5.A09     // Catch:{ 0CI -> 0x0141 }
            int r16 = X.AnonymousClass08G.A00(r7)     // Catch:{ 0CI -> 0x0141 }
            r18 = 0
            int r20 = r1.AwM()     // Catch:{ 0CI -> 0x0141 }
            r21 = 0
            long r2 = r0.A0W     // Catch:{ 0CI -> 0x0141 }
            r14 = r4
            r22 = r2
            r14.A03(r15, r16, r17, r18, r20, r21, r22)     // Catch:{ 0CI -> 0x0141 }
            goto L_0x011a
        L_0x0115:
            X.AnonymousClass0A1.A02(r2)     // Catch:{ 0CI -> 0x0141 }
            r1.A07 = r4     // Catch:{ 0CI -> 0x0141 }
        L_0x011a:
            r24 = r34
            r23 = r32
            r20 = r30
            r26 = r36
            r18 = r0
            r19 = r15
            r21 = r7
            r22 = r17
            r18.A07(r19, r20, r21, r22, r23, r24, r26, r27)     // Catch:{ 0CI -> 0x0141 }
            java.lang.String r2 = "/mqtt_health_stats"
            boolean r2 = r2.equals(r15)     // Catch:{ 0CI -> 0x0141 }
            if (r2 != 0) goto L_0x013c
            java.lang.Integer r2 = X.AnonymousClass07B.A01     // Catch:{ 0CI -> 0x0141 }
            if (r7 != r2) goto L_0x013c
            r28.A0E()     // Catch:{ 0CI -> 0x0141 }
        L_0x013c:
            X.0Aq r0 = X.C01540Aq.A00(r1)     // Catch:{ 0CI -> 0x0141 }
            return r0
        L_0x0141:
            r3 = move-exception
            java.lang.String r2 = "FbnsConnectionManager"
            java.lang.String r1 = "exception/publish"
            X.C010708t.A0S(r2, r3, r1)
            X.0CE r2 = X.AnonymousClass0CE.A0J
            X.0FG r1 = X.AnonymousClass0FG.CONNECTION_LOST
            r5.A0L(r0, r2, r1)
            r27.A00()
            throw r3
        L_0x0154:
            if (r0 != 0) goto L_0x0170
            r0 = 0
        L_0x0158:
            X.0B6 r13 = r5.A09
            int r16 = X.AnonymousClass08G.A00(r7)
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            java.lang.String r14 = "not_connected"
            r21 = r0
            r13.A05(r14, r15, r16, r17, r18, r19, r20, r21)
            X.0Bc r0 = X.C01660Bc.A00
            return r0
        L_0x0170:
            long r0 = r0.A0W
            goto L_0x0158
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AH.A0U(java.lang.String, byte[], java.lang.Integer, X.0CZ, int, long, java.lang.String, X.0PF):X.0Aq");
    }
}
