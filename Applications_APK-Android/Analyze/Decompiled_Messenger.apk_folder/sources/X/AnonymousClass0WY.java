package X;

import com.facebook.inject.InjectorModule;
import com.facebook.user.model.User;

@InjectorModule
/* renamed from: X.0WY  reason: invalid class name */
public final class AnonymousClass0WY extends AnonymousClass0UV {
    public static final C04310Tq A01(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AoG, r1);
    }

    public static final C04310Tq A02(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.Awq, r1);
    }

    public static final User A00(AnonymousClass1XY r0) {
        return C25481Zu.A00(r0).A09();
    }
}
