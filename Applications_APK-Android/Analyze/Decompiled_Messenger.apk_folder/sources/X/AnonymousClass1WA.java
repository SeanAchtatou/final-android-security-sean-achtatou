package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.proxygen.LigerSamplePolicy;

/* renamed from: X.1WA  reason: invalid class name */
public final class AnonymousClass1WA implements C04310Tq {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ FbSharedPreferences A03;

    public AnonymousClass1WA(int i, int i2, int i3, FbSharedPreferences fbSharedPreferences) {
        this.A02 = i;
        this.A00 = i2;
        this.A01 = i3;
        this.A03 = fbSharedPreferences;
    }

    public Object get() {
        return new LigerSamplePolicy(this.A02, this.A00, this.A01, this.A03.Aep(C25951af.A0G, false));
    }
}
