package X;

import android.util.SparseArray;
import java.util.HashMap;

/* renamed from: X.1pH  reason: invalid class name and case insensitive filesystem */
public final class C34291pH extends HashMap<String, Integer> {
    public C34291pH() {
        SparseArray sparseArray = AnonymousClass0AI.A00;
        put((String) sparseArray.get(72), 2000);
        put((String) sparseArray.get(62), 2000);
        put((String) sparseArray.get(98), Integer.valueOf((int) AnonymousClass1Y3.AX2));
    }
}
