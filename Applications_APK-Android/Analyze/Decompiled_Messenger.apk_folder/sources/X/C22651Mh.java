package X;

import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Mh  reason: invalid class name and case insensitive filesystem */
public final class C22651Mh extends AbstractExecutorService implements C22661Mi {
    public final C04650Vo A00;
    private final Executor A01;
    private final ExecutorService A02 = new C22671Mj(this, AnonymousClass07B.A0C);
    private final ExecutorService A03 = new C22671Mj(this, AnonymousClass07B.A00);
    private final ExecutorService A04 = new C22671Mj(this, AnonymousClass07B.A01);

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void execute(Runnable runnable) {
        C04650Vo r1 = this.A00;
        if (r1 != null) {
            AnonymousClass07A.A04(r1, runnable, 1412387429);
        } else {
            AnonymousClass07A.A04(this.A01, runnable, -886739937);
        }
    }

    public void shutdown() {
        throw new UnsupportedOperationException();
    }

    public List shutdownNow() {
        throw new UnsupportedOperationException();
    }

    public C22651Mh(String str, int i, Executor executor) {
        Executor r1;
        PriorityBlockingQueue priorityBlockingQueue = new PriorityBlockingQueue(11, new C22681Mk());
        if (executor instanceof C04640Vn) {
            AnonymousClass064.A03(i <= priorityBlockingQueue.remainingCapacity());
            C04640Vn r6 = (C04640Vn) executor;
            r1 = r6.A03.A02(r6, i, str, null);
        } else {
            r1 = new C21191Fp(str, i, executor, priorityBlockingQueue);
        }
        if (r1 instanceof C04650Vo) {
            this.A00 = (C04650Vo) r1;
        } else {
            this.A01 = r1;
        }
    }

    public ExecutorService AaS(Integer num) {
        switch (num.intValue()) {
            case 0:
                return this.A03;
            case 1:
            default:
                return this.A04;
            case 2:
                return this.A02;
        }
    }
}
