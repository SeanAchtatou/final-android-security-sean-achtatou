package X;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.recyclerview.widget.LinearLayoutManager$SavedState;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState;
import java.util.ArrayList;

/* renamed from: X.19T  reason: invalid class name */
public abstract class AnonymousClass19T {
    public boolean A00 = false;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public C16220wh A06;
    public C621830i A07;
    public RecyclerView A08;
    public C196719f A09;
    public C196719f A0A;
    public boolean A0B = false;
    public boolean A0C = true;
    public boolean A0D = true;
    public boolean A0E;
    public boolean A0F = false;

    public static int A0I(int i, int i2, int i3, int i4, boolean z) {
        int i5 = 0;
        int max = Math.max(0, i - i3);
        if (z) {
            if (i4 < 0) {
                if (i4 == -1) {
                    if (i2 != Integer.MIN_VALUE && (i2 == 0 || i2 != 1073741824)) {
                        i2 = 0;
                        max = 0;
                    }
                }
                max = 0;
                return View.MeasureSpec.makeMeasureSpec(max, i5);
            }
            max = i4;
            i5 = 1073741824;
            return View.MeasureSpec.makeMeasureSpec(max, i5);
        }
        if (i4 < 0) {
            if (i4 != -1) {
                if (i4 == -2) {
                    if (i2 == Integer.MIN_VALUE || i2 == 1073741824) {
                        i5 = Integer.MIN_VALUE;
                    }
                    return View.MeasureSpec.makeMeasureSpec(max, i5);
                }
                max = 0;
                return View.MeasureSpec.makeMeasureSpec(max, i5);
            }
        }
        max = i4;
        i5 = 1073741824;
        return View.MeasureSpec.makeMeasureSpec(max, i5);
        i5 = i2;
        return View.MeasureSpec.makeMeasureSpec(max, i5);
    }

    public int A0m(C15930wD r2) {
        if (this instanceof C20901Eg) {
            return C20901Eg.A08((C20901Eg) this, r2);
        }
        if (!(this instanceof AnonymousClass19S)) {
            return 0;
        }
        return AnonymousClass19S.A03((AnonymousClass19S) this, r2);
    }

    public int A0n(C15930wD r2) {
        if (this instanceof C20901Eg) {
            return C20901Eg.A09((C20901Eg) this, r2);
        }
        if (!(this instanceof AnonymousClass19S)) {
            return 0;
        }
        return AnonymousClass19S.A04((AnonymousClass19S) this, r2);
    }

    public int A0o(C15930wD r2) {
        if (this instanceof C20901Eg) {
            return C20901Eg.A0A((C20901Eg) this, r2);
        }
        if (!(this instanceof AnonymousClass19S)) {
            return 0;
        }
        return AnonymousClass19S.A05((AnonymousClass19S) this, r2);
    }

    public int A0p(C15930wD r2) {
        if (this instanceof C20901Eg) {
            return C20901Eg.A08((C20901Eg) this, r2);
        }
        if (!(this instanceof AnonymousClass19S)) {
            return 0;
        }
        return AnonymousClass19S.A03((AnonymousClass19S) this, r2);
    }

    public int A0q(C15930wD r2) {
        if (this instanceof C20901Eg) {
            return C20901Eg.A09((C20901Eg) this, r2);
        }
        if (!(this instanceof AnonymousClass19S)) {
            return 0;
        }
        return AnonymousClass19S.A04((AnonymousClass19S) this, r2);
    }

    public int A0r(C15930wD r2) {
        if (this instanceof C20901Eg) {
            return C20901Eg.A0A((C20901Eg) this, r2);
        }
        if (!(this instanceof AnonymousClass19S)) {
            return 0;
        }
        return AnonymousClass19S.A05((AnonymousClass19S) this, r2);
    }

    public Parcelable A0s() {
        int A062;
        int A063;
        int[] iArr;
        if (this instanceof C20901Eg) {
            C20901Eg r5 = (C20901Eg) this;
            StaggeredGridLayoutManager$SavedState staggeredGridLayoutManager$SavedState = r5.A09;
            if (staggeredGridLayoutManager$SavedState != null) {
                return new StaggeredGridLayoutManager$SavedState(staggeredGridLayoutManager$SavedState);
            }
            StaggeredGridLayoutManager$SavedState staggeredGridLayoutManager$SavedState2 = new StaggeredGridLayoutManager$SavedState();
            staggeredGridLayoutManager$SavedState2.A07 = r5.A0D;
            staggeredGridLayoutManager$SavedState2.A05 = r5.A0B;
            staggeredGridLayoutManager$SavedState2.A06 = r5.A0C;
            C22750BAu bAu = r5.A08;
            if (bAu == null || (iArr = bAu.A01) == null) {
                staggeredGridLayoutManager$SavedState2.A01 = 0;
            } else {
                staggeredGridLayoutManager$SavedState2.A08 = iArr;
                staggeredGridLayoutManager$SavedState2.A01 = iArr.length;
                staggeredGridLayoutManager$SavedState2.A04 = bAu.A00;
            }
            if (r5.A0c() > 0) {
                staggeredGridLayoutManager$SavedState2.A00 = r5.A0B ? C20901Eg.A07(r5) : C20901Eg.A06(r5);
                View A0C2 = r5.A0E ? C20901Eg.A0C(r5, true) : C20901Eg.A0D(r5, true);
                staggeredGridLayoutManager$SavedState2.A03 = A0C2 == null ? -1 : A0L(A0C2);
                int i = r5.A05;
                staggeredGridLayoutManager$SavedState2.A02 = i;
                staggeredGridLayoutManager$SavedState2.A09 = new int[i];
                for (int i2 = 0; i2 < r5.A05; i2++) {
                    if (r5.A0B) {
                        A062 = r5.A0G[i2].A05(Integer.MIN_VALUE);
                        if (A062 != Integer.MIN_VALUE) {
                            A063 = r5.A06.A02();
                        } else {
                            staggeredGridLayoutManager$SavedState2.A09[i2] = A062;
                        }
                    } else {
                        A062 = r5.A0G[i2].A06(Integer.MIN_VALUE);
                        if (A062 != Integer.MIN_VALUE) {
                            A063 = r5.A06.A06();
                        } else {
                            staggeredGridLayoutManager$SavedState2.A09[i2] = A062;
                        }
                    }
                    A062 -= A063;
                    staggeredGridLayoutManager$SavedState2.A09[i2] = A062;
                }
                return staggeredGridLayoutManager$SavedState2;
            }
            staggeredGridLayoutManager$SavedState2.A00 = -1;
            staggeredGridLayoutManager$SavedState2.A03 = -1;
            staggeredGridLayoutManager$SavedState2.A02 = 0;
            return staggeredGridLayoutManager$SavedState2;
        } else if (!(this instanceof AnonymousClass19S)) {
            return null;
        } else {
            AnonymousClass19S r4 = (AnonymousClass19S) this;
            LinearLayoutManager$SavedState linearLayoutManager$SavedState = r4.A05;
            if (linearLayoutManager$SavedState != null) {
                return new LinearLayoutManager$SavedState(linearLayoutManager$SavedState);
            }
            LinearLayoutManager$SavedState linearLayoutManager$SavedState2 = new LinearLayoutManager$SavedState();
            if (r4.A0c() > 0) {
                r4.A1y();
                boolean z = r4.mLastStackFromEnd;
                boolean z2 = r4.A08;
                boolean z3 = z ^ z2;
                linearLayoutManager$SavedState2.A02 = z3;
                if (z3) {
                    View A0u = r4.A0u(z2 ? 0 : r4.A0c() - 1);
                    linearLayoutManager$SavedState2.A00 = r4.A06.A02() - r4.A06.A08(A0u);
                    linearLayoutManager$SavedState2.A01 = A0L(A0u);
                    return linearLayoutManager$SavedState2;
                }
                View A0u2 = r4.A0u(z2 ? r4.A0c() - 1 : 0);
                linearLayoutManager$SavedState2.A01 = A0L(A0u2);
                linearLayoutManager$SavedState2.A00 = r4.A06.A0B(A0u2) - r4.A06.A06();
                return linearLayoutManager$SavedState2;
            }
            linearLayoutManager$SavedState2.A01 = -1;
            return linearLayoutManager$SavedState2;
        }
    }

    public AnonymousClass1R7 A0w() {
        if (this instanceof C20901Eg) {
            return ((C20901Eg) this).A01 == 0 ? new C25157Cat(-2, -1) : new C25157Cat(-1, -2);
        }
        AnonymousClass19S r1 = (AnonymousClass19S) this;
        return !(r1 instanceof AnonymousClass1AW) ? !(r1 instanceof AnonymousClass19R) ? !(r1 instanceof C24131Sk) ? new AnonymousClass1R7(-2, -2) : ((C24131Sk) r1).A01 == 0 ? new C25158Cau(-2, -1) : new C25158Cau(-1, -2) : ((AnonymousClass19R) r1).A01 == 1 ? new AnonymousClass1R7(-1, -2) : new AnonymousClass1R7(-2, -1) : new AnonymousClass1R7(-1, -2);
    }

    public void A0z(int i) {
        if (this instanceof C20901Eg) {
            C20901Eg r0 = (C20901Eg) this;
            if (i == 0) {
                r0.A1t();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0094, code lost:
        if (r2 >= r10.A00()) goto L_0x0096;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A12(int r8, int r9, X.C15930wD r10, X.C37131uk r11) {
        /*
            r7 = this;
            boolean r0 = r7 instanceof X.C20901Eg
            if (r0 != 0) goto L_0x002d
            boolean r0 = r7 instanceof X.AnonymousClass19S
            if (r0 == 0) goto L_0x002c
            r3 = r7
            X.19S r3 = (X.AnonymousClass19S) r3
            int r0 = r3.A01
            if (r0 == 0) goto L_0x0010
            r8 = r9
        L_0x0010:
            int r0 = r3.A0c()
            if (r0 == 0) goto L_0x002c
            if (r8 == 0) goto L_0x002c
            r3.A1y()
            r2 = 1
            r1 = -1
            if (r8 <= 0) goto L_0x0020
            r1 = 1
        L_0x0020:
            int r0 = java.lang.Math.abs(r8)
            X.AnonymousClass19S.A0E(r3, r1, r0, r2, r10)
            X.1AA r0 = r3.A04
            r3.A22(r10, r0, r11)
        L_0x002c:
            return
        L_0x002d:
            r5 = r7
            X.1Eg r5 = (X.C20901Eg) r5
            int r0 = r5.A01
            if (r0 == 0) goto L_0x0035
            r8 = r9
        L_0x0035:
            int r0 = r5.A0c()
            if (r0 == 0) goto L_0x002c
            if (r8 == 0) goto L_0x002c
            X.C20901Eg.A0Z(r5, r8, r10)
            int[] r0 = r5.A0F
            if (r0 == 0) goto L_0x0049
            int r1 = r0.length
            int r0 = r5.A05
            if (r1 >= r0) goto L_0x004f
        L_0x0049:
            int r0 = r5.A05
            int[] r0 = new int[r0]
            r5.A0F = r0
        L_0x004f:
            r4 = 0
            r2 = 0
            r3 = 0
        L_0x0052:
            int r0 = r5.A05
            if (r2 >= r0) goto L_0x0082
            X.46r r6 = r5.A0I
            int r1 = r6.A06
            r0 = -1
            if (r1 != r0) goto L_0x0073
            int r1 = r6.A08
            X.Cas[] r0 = r5.A0G
            r0 = r0[r2]
            int r0 = r0.A06(r1)
        L_0x0067:
            int r1 = r1 - r0
            if (r1 < 0) goto L_0x0070
            int[] r0 = r5.A0F
            r0[r3] = r1
            int r3 = r3 + 1
        L_0x0070:
            int r2 = r2 + 1
            goto L_0x0052
        L_0x0073:
            X.Cas[] r0 = r5.A0G
            r1 = r0[r2]
            int r0 = r6.A05
            int r1 = r1.A05(r0)
            X.46r r0 = r5.A0I
            int r0 = r0.A05
            goto L_0x0067
        L_0x0082:
            int[] r0 = r5.A0F
            java.util.Arrays.sort(r0, r4, r3)
        L_0x0087:
            if (r4 >= r3) goto L_0x002c
            X.46r r0 = r5.A0I
            int r2 = r0.A04
            if (r2 < 0) goto L_0x0096
            int r1 = r10.A00()
            r0 = 1
            if (r2 < r1) goto L_0x0097
        L_0x0096:
            r0 = 0
        L_0x0097:
            if (r0 == 0) goto L_0x002c
            int[] r0 = r5.A0F
            r0 = r0[r4]
            r11.ANZ(r2, r0)
            X.46r r2 = r5.A0I
            int r1 = r2.A04
            int r0 = r2.A06
            int r1 = r1 + r0
            r2.A04 = r1
            int r4 = r4 + 1
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19T.A12(int, int, X.0wD, X.1uk):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0019  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A13(int r7, X.C37131uk r8) {
        /*
            r6 = this;
            boolean r0 = r6 instanceof X.AnonymousClass19S
            if (r0 == 0) goto L_0x0039
            r5 = r6
            X.19S r5 = (X.AnonymousClass19S) r5
            androidx.recyclerview.widget.LinearLayoutManager$SavedState r1 = r5.A05
            r4 = -1
            r3 = 0
            if (r1 == 0) goto L_0x002a
            int r2 = r1.A01
            r0 = 0
            if (r2 < 0) goto L_0x0013
            r0 = 1
        L_0x0013:
            if (r0 == 0) goto L_0x002a
            boolean r0 = r1.A02
        L_0x0017:
            if (r0 != 0) goto L_0x001a
            r4 = 1
        L_0x001a:
            r1 = 0
        L_0x001b:
            int r0 = r5.A00
            if (r1 >= r0) goto L_0x0039
            if (r2 < 0) goto L_0x0039
            if (r2 >= r7) goto L_0x0039
            r8.ANZ(r2, r3)
            int r2 = r2 + r4
            int r1 = r1 + 1
            goto L_0x001b
        L_0x002a:
            X.AnonymousClass19S.A0D(r5)
            boolean r0 = r5.A08
            int r2 = r5.A02
            if (r2 != r4) goto L_0x0017
            r2 = 0
            if (r0 == 0) goto L_0x0017
            int r2 = r7 + -1
            goto L_0x0017
        L_0x0039:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19T.A13(int, X.1uk):void");
    }

    public void A15(Parcelable parcelable) {
        if (this instanceof C20901Eg) {
            C20901Eg r1 = (C20901Eg) this;
            if (parcelable instanceof StaggeredGridLayoutManager$SavedState) {
                r1.A09 = (StaggeredGridLayoutManager$SavedState) parcelable;
                r1.A0y();
            }
        } else if (this instanceof AnonymousClass19S) {
            AnonymousClass19S r12 = (AnonymousClass19S) this;
            if (parcelable instanceof LinearLayoutManager$SavedState) {
                r12.A05 = (LinearLayoutManager$SavedState) parcelable;
                r12.A0y();
            }
        }
    }

    public void A16(View view) {
        A18(view, -1);
    }

    public void A18(View view, int i) {
        A0O(this, view, i, false);
    }

    public void A1L(RecyclerView recyclerView) {
        if (this instanceof C20901Eg) {
            C20901Eg r1 = (C20901Eg) this;
            r1.A08.A03();
            r1.A0y();
        } else if (this instanceof C24131Sk) {
            ((C24131Sk) this).A02.A00.clear();
        }
    }

    public void A1M(RecyclerView recyclerView, int i, int i2) {
        if (this instanceof C20901Eg) {
            C20901Eg.A0Y((C20901Eg) this, i, i2, 1);
        } else if (this instanceof C24131Sk) {
            ((C24131Sk) this).A02.A00.clear();
        }
    }

    public void A1N(RecyclerView recyclerView, int i, int i2) {
        if (this instanceof C20901Eg) {
            C20901Eg.A0Y((C20901Eg) this, i, i2, 2);
        } else if (this instanceof C24131Sk) {
            ((C24131Sk) this).A02.A00.clear();
        }
    }

    public void A1O(RecyclerView recyclerView, int i, int i2, int i3) {
        if (this instanceof C20901Eg) {
            C20901Eg.A0Y((C20901Eg) this, i, i2, 8);
        } else if (this instanceof C24131Sk) {
            ((C24131Sk) this).A02.A00.clear();
        }
    }

    public boolean A1S() {
        int i;
        if (this instanceof C20901Eg) {
            i = ((C20901Eg) this).A01;
        } else if (!(this instanceof AnonymousClass19S)) {
            return false;
        } else {
            i = ((AnonymousClass19S) this).A01;
        }
        return i == 0;
    }

    public boolean A1T() {
        int i;
        if (this instanceof C20901Eg) {
            i = ((C20901Eg) this).A01;
        } else if (!(this instanceof AnonymousClass19S)) {
            return false;
        } else {
            i = ((AnonymousClass19S) this).A01;
        }
        return i == 1;
    }

    public boolean A1W() {
        boolean z;
        if (!(this instanceof AnonymousClass19S)) {
            return false;
        }
        AnonymousClass19S r4 = (AnonymousClass19S) this;
        if (!(r4.A02 == 1073741824 || r4.A05 == 1073741824)) {
            int A0c = r4.A0c();
            int i = 0;
            while (true) {
                if (i >= A0c) {
                    z = false;
                    break;
                }
                ViewGroup.LayoutParams layoutParams = r4.A0u(i).getLayoutParams();
                if (layoutParams.width < 0 && layoutParams.height < 0) {
                    z = true;
                    break;
                }
                i++;
            }
            return z;
        }
    }

    public boolean A1Y(AnonymousClass1R7 r2) {
        return !(this instanceof C20901Eg) ? !(this instanceof C24131Sk) ? r2 != null : r2 instanceof C25158Cau : r2 instanceof C25157Cat;
    }

    public boolean A1Z(RecyclerView recyclerView, View view, Rect rect, boolean z) {
        return A1a(recyclerView, view, rect, z, false);
    }

    public int A1b(int i, C15740vp r3, C15930wD r4) {
        return 0;
    }

    public int A1c(int i, C15740vp r3, C15930wD r4) {
        return 0;
    }

    public View A1f(View view, int i, C15740vp r4, C15930wD r5) {
        return null;
    }

    public void A1j(int i) {
    }

    public void A1o(C15930wD r1) {
    }

    public boolean A1s() {
        return false;
    }

    public int A0c() {
        C16220wh r0 = this.A06;
        if (r0 != null) {
            return r0.A02();
        }
        return 0;
    }

    public int A0d() {
        C20831Dz r0;
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            r0 = recyclerView.A0J;
        } else {
            r0 = null;
        }
        if (r0 != null) {
            return r0.ArU();
        }
        return 0;
    }

    public int A0e() {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            return recyclerView.getPaddingBottom();
        }
        return 0;
    }

    public int A0f() {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            return recyclerView.getPaddingLeft();
        }
        return 0;
    }

    public int A0g() {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            return recyclerView.getPaddingRight();
        }
        return 0;
    }

    public int A0h() {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            return recyclerView.getPaddingTop();
        }
        return 0;
    }

    public View A0u(int i) {
        C16220wh r0 = this.A06;
        if (r0 != null) {
            return r0.A03(i);
        }
        return null;
    }

    public View A0v(View view) {
        View A0a;
        RecyclerView recyclerView = this.A08;
        if (recyclerView == null || (A0a = recyclerView.A0a(view)) == null || this.A06.A02.contains(A0a)) {
            return null;
        }
        return A0a;
    }

    public AnonymousClass1R7 A0x(Context context, AttributeSet attributeSet) {
        if (this instanceof C20901Eg) {
            return new C25157Cat(context, attributeSet);
        }
        if (!(this instanceof C24131Sk)) {
            return new AnonymousClass1R7(context, attributeSet);
        }
        return new C25158Cau(context, attributeSet);
    }

    public void A0y() {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            recyclerView.requestLayout();
        }
    }

    public void A17(View view) {
        C16220wh r2 = this.A06;
        int BCT = r2.A01.BCT(view);
        if (BCT >= 0) {
            if (r2.A00.A07(BCT)) {
                C16220wh.A01(r2, view);
            }
            r2.A01.C27(BCT);
        }
    }

    public void A1A(View view, Rect rect) {
        RecyclerView recyclerView = this.A08;
        if (recyclerView == null) {
            rect.set(0, 0, 0, 0);
        } else {
            rect.set(recyclerView.A0Y(view));
        }
    }

    public void A1D(View view, boolean z, Rect rect) {
        Matrix matrix;
        if (z) {
            Rect rect2 = ((AnonymousClass1R7) view.getLayoutParams()).A02;
            rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, view.getHeight() + rect2.bottom);
        } else {
            rect.set(0, 0, view.getWidth(), view.getHeight());
        }
        if (!(this.A08 == null || (matrix = view.getMatrix()) == null || matrix.isIdentity())) {
            RectF rectF = this.A08.A0p;
            rectF.set(rect);
            matrix.mapRect(rectF);
            rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
        }
        rect.offset(view.getLeft(), view.getTop());
    }

    public void A1G(C15740vp r7) {
        int size = r7.A04.size();
        for (int i = size - 1; i >= 0; i--) {
            View view = ((C33781o8) r7.A04.get(i)).A0H;
            C33781o8 A052 = RecyclerView.A05(view);
            if (!A052.A0F()) {
                A052.A0A(false);
                if (A052.A0B()) {
                    this.A08.removeDetachedView(view, false);
                }
                C20221Bj r0 = this.A08.A0K;
                if (r0 != null) {
                    r0.A0A(A052);
                }
                A052.A0A(true);
                C33781o8 A053 = RecyclerView.A05(view);
                A053.A08 = null;
                A053.A0F = false;
                A053.A00 &= -33;
                r7.A09(A053);
            }
        }
        r7.A04.clear();
        ArrayList arrayList = r7.A03;
        if (arrayList != null) {
            arrayList.clear();
        }
        if (size > 0) {
            this.A08.invalidate();
        }
    }

    public void A1H(C15740vp r2, C15930wD r3, int i, int i2) {
        this.A08.A0o(i, i2);
    }

    public void A1I(C621830i r7) {
        C621830i r1 = this.A07;
        if (!(r1 == null || r7 == r1 || !r1.A05)) {
            r1.A01();
        }
        this.A07 = r7;
        RecyclerView recyclerView = this.A08;
        C15920wC r12 = recyclerView.mViewFlinger;
        r12.A06.removeCallbacks(r12);
        r12.A03.abortAnimation();
        if (r7.A06) {
            String simpleName = r7.getClass().getSimpleName();
            Log.w("RecyclerView", AnonymousClass08S.A0T("An instance of ", simpleName, " was started more than once. Each instance of", simpleName, " is intended to only be used once. You should create a new instance for each use."));
        }
        r7.A03 = recyclerView;
        r7.A02 = this;
        int i = r7.A00;
        if (i != -1) {
            recyclerView.A0y.A06 = i;
            r7.A05 = true;
            r7.A04 = true;
            r7.A01 = recyclerView.A0L.A0t(i);
            r7.A02();
            r7.A03.mViewFlinger.A01();
            r7.A06 = true;
            return;
        }
        throw new IllegalArgumentException("Invalid target position");
    }

    public void A1K(RecyclerView recyclerView) {
        if (recyclerView == null) {
            this.A08 = null;
            this.A06 = null;
            this.A04 = 0;
            this.A01 = 0;
        } else {
            this.A08 = recyclerView;
            this.A06 = recyclerView.A0H;
            this.A04 = recyclerView.getWidth();
            this.A01 = recyclerView.getHeight();
        }
        this.A05 = 1073741824;
        this.A02 = 1073741824;
    }

    public void A1P(RecyclerView recyclerView, int i, int i2, Object obj) {
        if (this instanceof C20901Eg) {
            C20901Eg.A0Y((C20901Eg) this, i, i2, 4);
        } else if (this instanceof C24131Sk) {
            ((C24131Sk) this).A02.A00.clear();
        }
    }

    public void A1Q(boolean z) {
        if (!(this instanceof AnonymousClass1AX)) {
            this.A00 = z;
        } else {
            ((AnonymousClass1AX) this).A00 = z;
        }
    }

    public final void A1R(boolean z) {
        if (z != this.A0C) {
            this.A0C = z;
            this.A03 = 0;
            RecyclerView recyclerView = this.A08;
            if (recyclerView != null) {
                recyclerView.A0w.A06();
            }
        }
    }

    public boolean A1U() {
        RecyclerView recyclerView = this.A08;
        if (recyclerView == null || !recyclerView.A02) {
            return false;
        }
        return true;
    }

    public boolean A1V() {
        if (this instanceof C20901Eg) {
            return true;
        }
        if (!(this instanceof AnonymousClass19S)) {
            return this.A00;
        }
        AnonymousClass19S r1 = (AnonymousClass19S) this;
        if (!(r1 instanceof AnonymousClass1AX)) {
            return true;
        }
        return ((AnonymousClass1AX) r1).A00;
    }

    public int A1d(C15740vp r4, C15930wD r5) {
        C20831Dz r1;
        RecyclerView recyclerView = this.A08;
        if (recyclerView == null || (r1 = recyclerView.A0J) == null || !A1S()) {
            return 1;
        }
        return r1.ArU();
    }

    public int A1e(C15740vp r4, C15930wD r5) {
        C20831Dz r1;
        RecyclerView recyclerView = this.A08;
        if (recyclerView == null || (r1 = recyclerView.A0J) == null || !A1T()) {
            return 1;
        }
        return r1.ArU();
    }

    public AnonymousClass1R7 A1g(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof AnonymousClass1R7) {
            return new AnonymousClass1R7((AnonymousClass1R7) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new AnonymousClass1R7((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new AnonymousClass1R7(layoutParams);
    }

    public void A1h(int i) {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            int A022 = recyclerView.A0H.A02();
            for (int i2 = 0; i2 < A022; i2++) {
                recyclerView.A0H.A03(i2).offsetLeftAndRight(i);
            }
        }
    }

    public void A1i(int i) {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            recyclerView.A0i(i);
        }
    }

    public void A1l(AccessibilityEvent accessibilityEvent) {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null && accessibilityEvent != null) {
            boolean z = true;
            if (!recyclerView.canScrollVertically(1) && !this.A08.canScrollVertically(-1) && !this.A08.canScrollHorizontally(-1) && !this.A08.canScrollHorizontally(1)) {
                z = false;
            }
            accessibilityEvent.setScrollable(z);
            C20831Dz r0 = this.A08.A0J;
            if (r0 != null) {
                accessibilityEvent.setItemCount(r0.ArU());
            }
        }
    }

    public void A1m(C15740vp r3, C15930wD r4) {
        Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
    }

    public void A1q(RecyclerView recyclerView, C15930wD r4, int i) {
        Log.e("RecyclerView", "You must override smoothScrollToPosition to support smooth scrolling");
    }

    public void A1r(String str) {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            recyclerView.A17(str);
        }
    }

    public AnonymousClass19T() {
        AnonymousClass19Y r2 = new AnonymousClass19Y(this);
        C196319b r1 = new C196319b(this);
        this.A09 = new C196719f(r2);
        this.A0A = new C196719f(r1);
    }

    public static int A0H(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        if (mode == Integer.MIN_VALUE) {
            return Math.min(size, Math.max(i2, i3));
        }
        if (mode != 1073741824) {
            return Math.max(i2, i3);
        }
        return size;
    }

    public static int A0J(View view) {
        Rect rect = ((AnonymousClass1R7) view.getLayoutParams()).A02;
        return view.getMeasuredHeight() + rect.top + rect.bottom;
    }

    public static int A0K(View view) {
        Rect rect = ((AnonymousClass1R7) view.getLayoutParams()).A02;
        return view.getMeasuredWidth() + rect.left + rect.right;
    }

    public static int A0L(View view) {
        return ((AnonymousClass1R7) view.getLayoutParams()).mViewHolder.A05();
    }

    private void A0M(int i) {
        C16220wh r3;
        int A002;
        View Ah1;
        if (A0u(i) != null && (Ah1 = r3.A01.Ah1((A002 = C16220wh.A00((r3 = this.A06), i)))) != null) {
            if (r3.A00.A07(A002)) {
                C16220wh.A01(r3, Ah1);
            }
            r3.A01.C27(A002);
        }
    }

    public static void A0N(View view, int i, int i2, int i3, int i4) {
        AnonymousClass1R7 r2 = (AnonymousClass1R7) view.getLayoutParams();
        Rect rect = r2.A02;
        view.layout(i + rect.left + r2.leftMargin, i2 + rect.top + r2.topMargin, (i3 - rect.right) - r2.rightMargin, (i4 - rect.bottom) - r2.bottomMargin);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0O(X.AnonymousClass19T r7, android.view.View r8, int r9, boolean r10) {
        /*
            X.1o8 r3 = androidx.recyclerview.widget.RecyclerView.A05(r8)
            if (r10 != 0) goto L_0x00e0
            boolean r0 = r3.A0E()
            if (r0 != 0) goto L_0x00e0
            androidx.recyclerview.widget.RecyclerView r0 = r7.A08
            X.0wA r0 = r0.A10
            r0.A02(r3)
        L_0x0013:
            android.view.ViewGroup$LayoutParams r5 = r8.getLayoutParams()
            X.1R7 r5 = (X.AnonymousClass1R7) r5
            int r2 = r3.A00
            r1 = r2 & 32
            r0 = 0
            if (r1 == 0) goto L_0x0021
            r0 = 1
        L_0x0021:
            r4 = 0
            if (r0 != 0) goto L_0x00c6
            X.0vp r1 = r3.A08
            r0 = 0
            if (r1 == 0) goto L_0x002a
            r0 = 1
        L_0x002a:
            if (r0 != 0) goto L_0x00c6
            android.view.ViewParent r1 = r8.getParent()
            androidx.recyclerview.widget.RecyclerView r0 = r7.A08
            if (r1 != r0) goto L_0x00ab
            X.0wh r1 = r7.A06
            X.0wj r0 = r1.A01
            int r2 = r0.BCT(r8)
            r6 = -1
            if (r2 == r6) goto L_0x004d
            X.0wk r1 = r1.A00
            boolean r0 = r1.A06(r2)
            if (r0 != 0) goto L_0x004d
            int r0 = r1.A01(r2)
            int r2 = r2 - r0
            r6 = r2
        L_0x004d:
            r1 = -1
            if (r9 != r1) goto L_0x0056
            X.0wh r0 = r7.A06
            int r9 = r0.A02()
        L_0x0056:
            if (r6 == r1) goto L_0x00fb
            if (r6 == r9) goto L_0x0097
            androidx.recyclerview.widget.RecyclerView r0 = r7.A08
            X.19T r8 = r0.A0L
            android.view.View r7 = r8.A0u(r6)
            if (r7 == 0) goto L_0x00e9
            r8.A0u(r6)
            X.0wh r2 = r8.A06
            int r1 = X.C16220wh.A00(r2, r6)
            X.0wk r0 = r2.A00
            r0.A07(r1)
            X.0wj r0 = r2.A01
            r0.AXE(r1)
            android.view.ViewGroup$LayoutParams r6 = r7.getLayoutParams()
            X.1R7 r6 = (X.AnonymousClass1R7) r6
            X.1o8 r2 = androidx.recyclerview.widget.RecyclerView.A05(r7)
            boolean r0 = r2.A0E()
            if (r0 == 0) goto L_0x00a3
            androidx.recyclerview.widget.RecyclerView r0 = r8.A08
            X.0wA r0 = r0.A10
            r0.A01(r2)
        L_0x008e:
            X.0wh r1 = r8.A06
            boolean r0 = r2.A0E()
            r1.A04(r7, r9, r6, r0)
        L_0x0097:
            boolean r0 = r5.A00
            if (r0 == 0) goto L_0x00a2
            android.view.View r0 = r3.A0H
            r0.invalidate()
            r5.A00 = r4
        L_0x00a2:
            return
        L_0x00a3:
            androidx.recyclerview.widget.RecyclerView r0 = r8.A08
            X.0wA r0 = r0.A10
            r0.A02(r2)
            goto L_0x008e
        L_0x00ab:
            X.0wh r0 = r7.A06
            r0.A05(r8, r9, r4)
            r0 = 1
            r5.A01 = r0
            X.30i r2 = r7.A07
            if (r2 == 0) goto L_0x0097
            boolean r0 = r2.A05
            if (r0 == 0) goto L_0x0097
            int r1 = androidx.recyclerview.widget.RecyclerView.A03(r8)
            int r0 = r2.A00
            if (r1 != r0) goto L_0x0097
            r2.A01 = r8
            goto L_0x0097
        L_0x00c6:
            X.0vp r1 = r3.A08
            r0 = 0
            if (r1 == 0) goto L_0x00cc
            r0 = 1
        L_0x00cc:
            if (r0 == 0) goto L_0x00db
            r1.A0A(r3)
        L_0x00d1:
            X.0wh r1 = r7.A06
            android.view.ViewGroup$LayoutParams r0 = r8.getLayoutParams()
            r1.A04(r8, r9, r0, r4)
            goto L_0x0097
        L_0x00db:
            r0 = r2 & -33
            r3.A00 = r0
            goto L_0x00d1
        L_0x00e0:
            androidx.recyclerview.widget.RecyclerView r0 = r7.A08
            X.0wA r0 = r0.A10
            r0.A01(r3)
            goto L_0x0013
        L_0x00e9:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Cannot move a child from non-existing index:"
            androidx.recyclerview.widget.RecyclerView r0 = r8.A08
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = X.AnonymousClass08S.A0A(r1, r6, r0)
            r2.<init>(r0)
            throw r2
        L_0x00fb:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = "Added View has RecyclerView as parent but view is not a real child. Unfiltered index:"
            androidx.recyclerview.widget.RecyclerView r0 = r7.A08
            int r1 = r0.indexOfChild(r8)
            androidx.recyclerview.widget.RecyclerView r0 = r7.A08
            java.lang.String r0 = r0.A0d()
            java.lang.String r0 = X.AnonymousClass08S.A0A(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19T.A0O(X.19T, android.view.View, int, boolean):void");
    }

    public static boolean A0P(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (i3 <= 0 || i == i3) {
            if (mode != Integer.MIN_VALUE) {
                if (mode == 0) {
                    return true;
                }
                if (mode == 1073741824 && size == i) {
                    return true;
                }
                return false;
            } else if (size >= i) {
                return true;
            }
        }
        return false;
    }

    public int A0i(View view) {
        return view.getBottom() + ((AnonymousClass1R7) view.getLayoutParams()).A02.bottom;
    }

    public int A0j(View view) {
        return view.getLeft() - ((AnonymousClass1R7) view.getLayoutParams()).A02.left;
    }

    public int A0k(View view) {
        return view.getRight() + ((AnonymousClass1R7) view.getLayoutParams()).A02.right;
    }

    public int A0l(View view) {
        return view.getTop() - ((AnonymousClass1R7) view.getLayoutParams()).A02.top;
    }

    public View A0t(int i) {
        int A0c = A0c();
        for (int i2 = 0; i2 < A0c; i2++) {
            View A0u = A0u(i2);
            C33781o8 A052 = RecyclerView.A05(A0u);
            if (A052 != null && A052.A05() == i && !A052.A0F() && (this.A08.A0y.A08 || !A052.A0E())) {
                return A0u;
            }
        }
        return null;
    }

    public void A10(int i, int i2) {
        this.A04 = View.MeasureSpec.getSize(i);
        int mode = View.MeasureSpec.getMode(i);
        this.A05 = mode;
        if (mode == 0 && !RecyclerView.A18) {
            this.A04 = 0;
        }
        this.A01 = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i2);
        this.A02 = mode2;
        if (mode2 == 0 && !RecyclerView.A18) {
            this.A01 = 0;
        }
    }

    public void A11(int i, int i2) {
        int A0c = A0c();
        if (A0c == 0) {
            this.A08.A0o(i, i2);
            return;
        }
        int i3 = Integer.MAX_VALUE;
        int i4 = Integer.MAX_VALUE;
        int i5 = Integer.MIN_VALUE;
        int i6 = Integer.MIN_VALUE;
        for (int i7 = 0; i7 < A0c; i7++) {
            View A0u = A0u(i7);
            Rect rect = this.A08.A0t;
            RecyclerView.A0F(A0u, rect);
            int i8 = rect.left;
            if (i8 < i3) {
                i3 = i8;
            }
            int i9 = rect.right;
            if (i9 > i5) {
                i5 = i9;
            }
            int i10 = rect.top;
            if (i10 < i4) {
                i4 = i10;
            }
            int i11 = rect.bottom;
            if (i11 > i6) {
                i6 = i11;
            }
        }
        this.A08.A0t.set(i3, i4, i5, i6);
        A1k(this.A08.A0t, i, i2);
    }

    public void A14(int i, C15740vp r3) {
        View A0u = A0u(i);
        A0M(i);
        r3.A07(A0u);
    }

    public void A19(View view, int i, int i2) {
        AnonymousClass1R7 r6 = (AnonymousClass1R7) view.getLayoutParams();
        Rect A0Y = this.A08.A0Y(view);
        int i3 = i + A0Y.left + A0Y.right;
        int i4 = i2 + A0Y.top + A0Y.bottom;
        int A0I = A0I(this.A04, this.A05, A0f() + A0g() + r6.leftMargin + r6.rightMargin + i3, r6.width, A1S());
        int A0I2 = A0I(this.A01, this.A02, A0h() + A0e() + r6.topMargin + r6.bottomMargin + i4, r6.height, A1T());
        if (A1X(view, A0I, A0I2, r6)) {
            view.measure(A0I, A0I2);
        }
    }

    public void A1B(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        C33781o8 A052 = RecyclerView.A05(view);
        if (A052 != null && !A052.A0E()) {
            C16220wh r0 = this.A06;
            if (!r0.A02.contains(A052.A0H)) {
                RecyclerView recyclerView = this.A08;
                A1n(recyclerView.A0w, recyclerView.A0y, view, accessibilityNodeInfoCompat);
            }
        }
    }

    public void A1C(View view, C15740vp r2) {
        A17(view);
        r2.A07(view);
    }

    public void A1E(C15740vp r7) {
        for (int A0c = A0c() - 1; A0c >= 0; A0c--) {
            View A0u = A0u(A0c);
            C33781o8 A052 = RecyclerView.A05(A0u);
            if (!A052.A0F()) {
                if (!A052.A0D() || A052.A0E() || this.A08.A0J.hasStableIds()) {
                    A0u(A0c);
                    C16220wh r2 = this.A06;
                    int A002 = C16220wh.A00(r2, A0c);
                    r2.A00.A07(A002);
                    r2.A01.AXE(A002);
                    r7.A08(A0u);
                    this.A08.A10.A02(A052);
                } else {
                    A0M(A0c);
                    r7.A09(A052);
                }
            }
        }
    }

    public void A1F(C15740vp r3) {
        for (int A0c = A0c() - 1; A0c >= 0; A0c--) {
            if (!RecyclerView.A05(A0u(A0c)).A0F()) {
                A14(A0c, r3);
            }
        }
    }

    public void A1J(RecyclerView recyclerView) {
        A10(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 1073741824));
    }

    public boolean A1X(View view, int i, int i2, AnonymousClass1R7 r6) {
        if (view.isLayoutRequested() || !this.A0D || !A0P(view.getWidth(), i, r6.width) || !A0P(view.getHeight(), i2, r6.height)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00a0, code lost:
        if (r0 <= r7) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a3, code lost:
        if (r1 != false) goto L_0x00a5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A1a(androidx.recyclerview.widget.RecyclerView r12, android.view.View r13, android.graphics.Rect r14, boolean r15, boolean r16) {
        /*
            r11 = this;
            int r3 = r11.A0f()
            int r4 = r11.A0h()
            int r2 = r11.A04
            int r0 = r11.A0g()
            int r2 = r2 - r0
            int r1 = r11.A01
            int r0 = r11.A0e()
            int r1 = r1 - r0
            int r9 = r13.getLeft()
            int r0 = r14.left
            int r9 = r9 + r0
            int r0 = r13.getScrollX()
            int r9 = r9 - r0
            int r8 = r13.getTop()
            int r0 = r14.top
            int r8 = r8 + r0
            int r0 = r13.getScrollY()
            int r8 = r8 - r0
            int r7 = r14.width()
            int r7 = r7 + r9
            int r0 = r14.height()
            int r0 = r0 + r8
            int r9 = r9 - r3
            r6 = 0
            int r3 = java.lang.Math.min(r6, r9)
            int r8 = r8 - r4
            int r5 = java.lang.Math.min(r6, r8)
            int r7 = r7 - r2
            int r2 = java.lang.Math.max(r6, r7)
            int r0 = r0 - r1
            int r1 = java.lang.Math.max(r6, r0)
            androidx.recyclerview.widget.RecyclerView r0 = r11.A08
            int r0 = X.C15320v6.getLayoutDirection(r0)
            r4 = 1
            if (r0 != r4) goto L_0x00af
            if (r2 != 0) goto L_0x005c
            int r2 = java.lang.Math.max(r3, r7)
        L_0x005c:
            if (r5 != 0) goto L_0x0062
            int r5 = java.lang.Math.min(r8, r1)
        L_0x0062:
            int[] r0 = new int[]{r2, r5}
            r3 = r0[r6]
            if (r16 == 0) goto L_0x00a5
            android.view.View r10 = r12.getFocusedChild()
            if (r10 == 0) goto L_0x00a2
            int r8 = r11.A0f()
            int r7 = r11.A0h()
            int r9 = r11.A04
            int r0 = r11.A0g()
            int r9 = r9 - r0
            int r2 = r11.A01
            int r0 = r11.A0e()
            int r2 = r2 - r0
            androidx.recyclerview.widget.RecyclerView r0 = r11.A08
            android.graphics.Rect r1 = r0.A0t
            androidx.recyclerview.widget.RecyclerView.A0F(r10, r1)
            int r0 = r1.left
            int r0 = r0 - r3
            if (r0 >= r9) goto L_0x00a2
            int r0 = r1.right
            int r0 = r0 - r3
            if (r0 <= r8) goto L_0x00a2
            int r0 = r1.top
            int r0 = r0 - r5
            if (r0 >= r2) goto L_0x00a2
            int r0 = r1.bottom
            int r0 = r0 - r5
            r1 = 1
            if (r0 > r7) goto L_0x00a3
        L_0x00a2:
            r1 = 0
        L_0x00a3:
            if (r1 == 0) goto L_0x00bb
        L_0x00a5:
            if (r3 != 0) goto L_0x00a9
            if (r5 == 0) goto L_0x00bb
        L_0x00a9:
            if (r15 == 0) goto L_0x00b7
            r12.scrollBy(r3, r5)
            return r4
        L_0x00af:
            if (r3 != 0) goto L_0x00b5
            int r3 = java.lang.Math.min(r9, r2)
        L_0x00b5:
            r2 = r3
            goto L_0x005c
        L_0x00b7:
            r12.A0q(r3, r5)
            return r4
        L_0x00bb:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19T.A1a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean");
    }

    public void A1k(Rect rect, int i, int i2) {
        int width = rect.width() + A0f() + A0g();
        int height = rect.height() + A0h() + A0e();
        this.A08.setMeasuredDimension(A0H(i, width, C15320v6.getMinimumWidth(this.A08)), A0H(i2, height, C15320v6.getMinimumHeight(this.A08)));
    }

    public void A1n(C15740vp r8, C15930wD r9, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        int i;
        int i2;
        if (A1T()) {
            i = A0L(view);
        } else {
            i = 0;
        }
        if (A1S()) {
            i2 = A0L(view);
        } else {
            i2 = 0;
        }
        accessibilityNodeInfoCompat.A0Q(AnonymousClass4CJ.A00(i, 1, i2, 1, false, false));
    }

    public void A1p(RecyclerView recyclerView, C15740vp r2) {
    }
}
