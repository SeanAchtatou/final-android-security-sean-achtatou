package X;

import com.facebook.common.connectionstatus.FbDataConnectionManager;
import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fZ  reason: invalid class name and case insensitive filesystem */
public final class C08570fZ implements C08560fY {
    private static volatile C08570fZ A01;
    public AnonymousClass0UN A00;

    public String AWk() {
        return "network_stats";
    }

    public static final C08570fZ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C08570fZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C08570fZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public long BL4() {
        return C08350fD.A0C;
    }

    public void BjZ(PerformanceLoggingEvent performanceLoggingEvent) {
        performanceLoggingEvent.A0B("connqual", ((FbDataConnectionManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B3e, this.A00)).A08().toString());
        performanceLoggingEvent.A0B("network_type", ((C09340h3) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AeN, this.A00)).A0K());
        performanceLoggingEvent.A0B("network_subtype", ((C09340h3) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AeN, this.A00)).A0J());
    }

    private C08570fZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A09;
    }
}
