package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0UU  reason: invalid class name */
public final class AnonymousClass0UU extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static volatile C001300x A02;
    private static volatile Boolean A03;
    private static volatile Boolean A04;

    public static final C001300x A03(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C001300x.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        r4.getApplicationInjector();
                        C001300x r0 = C001300x.A07;
                        if (r0 != null) {
                            A02 = r0;
                            A002.A01();
                        } else {
                            throw new IllegalStateException("Application did not provide its own FbAppType");
                        }
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final Boolean A07(AnonymousClass1XY r3) {
        if (A03 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A03 = false;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final Boolean A09(AnonymousClass1XY r3) {
        if (A04 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A04 = false;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final AnonymousClass010 A00(AnonymousClass1XY r0) {
        return A02(r0).A00;
    }

    public static final AnonymousClass010 A01(AnonymousClass1XY r0) {
        return A02(r0).A00;
    }

    public static final C001300x A02(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public static final C001400y A04(AnonymousClass1XY r0) {
        return A02(r0).A01;
    }

    public static final C001500z A05(AnonymousClass1XY r0) {
        return A02(r0).A02;
    }

    public static final C001500z A06(AnonymousClass1XY r0) {
        return A02(r0).A02;
    }

    public static final Boolean A08(AnonymousClass1XY r0) {
        return A09(r0);
    }
}
