package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1tZ  reason: invalid class name and case insensitive filesystem */
public final class C36691tZ {
    public int A00 = 500;
    public int A01 = 72;
    public String A02 = "DefaultWaterfallLogger";
    public String A03 = "default_waterfall_logger";
    public String A04 = BuildConfig.FLAVOR;
    public boolean A05 = true;

    public C36691tZ() {
    }

    public C36691tZ(boolean z, String str, String str2, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A05 = z;
        this.A02 = str;
        this.A04 = str2;
    }
}
