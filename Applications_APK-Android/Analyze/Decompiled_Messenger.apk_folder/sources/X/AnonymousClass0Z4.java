package X;

import com.facebook.acra.ErrorReporter;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.callercontext.CallerContext;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;
import org.apache.http.client.ResponseHandler;

@Singleton
/* renamed from: X.0Z4  reason: invalid class name */
public final class AnonymousClass0Z4 {
    private static volatile AnonymousClass0Z4 A06;
    public AnonymousClass0UN A00;
    public Exception A01;
    public boolean A02 = false;
    public final AnonymousClass1LZ A03;
    public volatile AnonymousClass2YN A04;
    private volatile boolean A05;

    public void enterLameDuckMode() {
        this.A05 = true;
    }

    public void exitLameDuckMode() {
        this.A05 = false;
    }

    public static AnonymousClass2YN A00(AnonymousClass0Z4 r3) {
        Throwable th;
        AnonymousClass2YN r0;
        if (r3.A04 != null) {
            return r3.A04;
        }
        synchronized (r3) {
            while (r3.A04 == null && r3.A01 == null) {
                if (!r3.A02) {
                    r3.A02 = true;
                    AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BKH, r3.A00), new AnonymousClass2Y8(r3), -1938639378);
                }
                try {
                    r3.wait();
                } catch (InterruptedException e) {
                    th = new RuntimeException(e);
                }
            }
            Exception exc = r3.A01;
            if (exc == null) {
                r0 = r3.A04;
            } else {
                th = new IllegalStateException(exc);
                throw th;
            }
        }
        return r0;
    }

    public static final AnonymousClass0Z4 A02(AnonymousClass1XY r5) {
        if (A06 == null) {
            synchronized (AnonymousClass0Z4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A06 = new AnonymousClass0Z4(applicationInjector, C26251b9.A05(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static void A03(AnonymousClass0Z4 r3, String str) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Ap7, r3.A00)).A01("tigon_init"), AnonymousClass1Y3.A4m);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("status", str);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public C48142Zr A04(AnonymousClass2Y3 r10) {
        boolean z;
        int i;
        boolean z2;
        ResponseHandler r4;
        int i2;
        ListenableFuture listenableFuture;
        C005505z.A03("FbHttpRequestProcessor - executeAsync", 1499402577);
        try {
            AnonymousClass2Y6 r42 = (AnonymousClass2Y6) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BP8, this.A00);
            ResponseHandler responseHandler = r10.A0G;
            String str = r10.A0B;
            boolean z3 = false;
            AnonymousClass2Y7 r2 = (AnonymousClass2Y7) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AfR, r42.A01);
            if (C001500z.A07.equals(r2.A00)) {
                z = r2.A02.Aep(C26211b5.A06, false);
            } else {
                z = r2.A02.Aep(C25951af.A07, false);
            }
            if (!z) {
                r4 = null;
            } else {
                if (C001500z.A07.equals(r2.A00)) {
                    i = r2.A01.AqL(563705867862585L, 3000);
                } else {
                    i = 10000;
                }
                String lowerCase = str.toLowerCase(Locale.US);
                if (lowerCase.contains("video")) {
                    i = 10000;
                    if (C001500z.A07.equals(r2.A00)) {
                        i = 3000;
                    }
                } else if (lowerCase.contains("image")) {
                    if (C001500z.A07.equals(r2.A00)) {
                        i = r2.A01.AqL(563705867993659L, 3000);
                    } else {
                        i = ErrorReporter.MAX_ANR_TRACES_TIME_DELTA_MS;
                    }
                }
                if (C001500z.A07.equals(r2.A00)) {
                    z2 = r2.A01.Aeo(282230891349218L, false);
                } else {
                    z2 = false;
                }
                if (z2) {
                    i = r42.A02.nextInt(i);
                }
                C001500z r0 = C001500z.A07;
                C001500z r8 = r2.A00;
                boolean equals = r0.equals(r8);
                boolean z4 = true;
                if (equals) {
                    z4 = false;
                }
                if (z4) {
                    int i3 = r42.A00;
                    r42.A00 = i3 - 1;
                    if (i3 < 0) {
                        C001500z.A07.equals(r8);
                        if (equals) {
                            i2 = r2.A01.AqL(563705868124732L, 15);
                        } else {
                            i2 = 10;
                        }
                        r42.A00 = r42.A02.nextInt(i2);
                        z3 = true;
                    }
                }
                r4 = new C182208cw(responseHandler, i, z3);
            }
            if (r4 != null) {
                AnonymousClass2Y1 r3 = new AnonymousClass2Y1();
                r3.A05 = r10.A04;
                r3.A06 = r10.A05;
                r3.A0A = r10.A09;
                r3.A09 = r10.A08;
                r3.A0C = r10.A0B;
                r3.A0I = r10.A0H;
                r3.A01 = r10.A00;
                r3.A0E = r10.A0D;
                r3.A0G = r10.A0F;
                r3.A0H = r10.A0G;
                r3.A02 = r10.A01;
                r3.A03 = r10.A02;
                r3.A0J = r10.A0I;
                r3.A08 = r10.A06;
                r3.A04 = r10.A03;
                r3.A0D = r10.A0C;
                Map map = r10.A0E;
                if (map != null) {
                    for (Map.Entry entry : map.entrySet()) {
                        r3.A01((String) entry.getKey(), (String) entry.getValue());
                    }
                }
                Optional optional = r10.A07;
                if (optional.isPresent()) {
                    List list = (List) optional.get();
                    Preconditions.checkNotNull(list);
                    r3.A0F = list;
                }
                r3.A0H = r4;
                r10 = r3.A00();
            }
            CallerContext callerContext = r10.A04;
            if (!this.A05 || (callerContext != null && "MAGIC_LOGOUT_TAG".equals(callerContext.A0G()))) {
                listenableFuture = A00(this).AZ8(r10);
            } else {
                listenableFuture = C05350Yp.A04(new IOException("In lame duck mode"));
            }
            return new C48142Zr(r10, listenableFuture, this);
        } finally {
            C005505z.A00(-1570653658);
        }
    }

    public Object A05(AnonymousClass2Y3 r4) {
        C005505z.A03("FbHttpRequestProcessor - request", 655249000);
        try {
            AnonymousClass1Y6 r0 = (AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, this.A00);
            if (r0 != null) {
                r0.AOx();
            }
            Object A002 = C07360dI.A00(A04(r4).A00());
            C005505z.A00(-19180564);
            return A002;
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            Preconditions.checkNotNull(cause);
            Throwables.propagateIfInstanceOf(cause, IOException.class);
            throw Throwables.propagate(cause);
        } catch (CancellationException e2) {
            throw new C181238bJ(e2);
        } catch (Throwable th) {
            C005505z.A00(682246602);
            throw th;
        }
    }

    private AnonymousClass0Z4(AnonymousClass1XY r3, AnonymousClass1LZ r4) {
        this.A00 = new AnonymousClass0UN(8, r3);
        this.A03 = r4;
    }

    public static final AnonymousClass0Z4 A01(AnonymousClass1XY r0) {
        return A02(r0);
    }
}
