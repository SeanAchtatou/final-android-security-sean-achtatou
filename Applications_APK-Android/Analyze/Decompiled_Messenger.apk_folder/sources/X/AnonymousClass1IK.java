package X;

import com.facebook.litho.ComponentTree;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1IK  reason: invalid class name */
public final class AnonymousClass1IK {
    private static final AtomicInteger A0M = new AtomicInteger(1);
    public int A00;
    public ComponentTree A01;
    public C31551js A02;
    public AnonymousClass1JC A03;
    public C198219u A04;
    public C21681Ih A05;
    public boolean A06 = false;
    public boolean A07 = true;
    public boolean A08;
    private int A09 = -1;
    private int A0A = -1;
    public final int A0B;
    public final AtomicInteger A0C = new AtomicInteger(0);
    private final C31551js A0D;
    private final AnonymousClass6QY A0E;
    private final boolean A0F;
    private final boolean A0G;
    private final boolean A0H;
    private final boolean A0I;
    private final boolean A0J;
    private final boolean A0K;
    private final boolean A0L;

    public synchronized int A01() {
        return this.A00;
    }

    public synchronized ComponentTree A02() {
        return this.A01;
    }

    public synchronized C21681Ih A03() {
        return this.A05;
    }

    public synchronized void A04() {
        AnonymousClass1JC r0;
        ComponentTree componentTree = this.A01;
        if (componentTree != null) {
            synchronized (componentTree) {
                r0 = new AnonymousClass1JC(componentTree.A0E);
            }
            this.A03 = r0;
        }
        ComponentTree componentTree2 = this.A01;
        if (componentTree2 != null) {
            this.A06 = componentTree2.A0p;
        }
        A05();
    }

    public synchronized void A05() {
        ComponentTree componentTree = this.A01;
        if (componentTree != null) {
            componentTree.A0M();
            this.A01 = null;
        }
        this.A08 = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0028, code lost:
        if (r15 == null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        r2.A0V(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        if (r3 == null) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        com.facebook.litho.ComponentTree.A09(r2, r3, r4, r5, true, null, 1, null, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        monitor-enter(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        if (r11.A01 != r2) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        if (r3 != r11.A05.Ahm()) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0043, code lost:
        r11.A08 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0045, code lost:
        monitor-exit(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0046, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0047, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0051, code lost:
        throw new java.lang.IllegalArgumentException("Root component can't be null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0054, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.AnonymousClass0p4 r12, int r13, int r14, X.C35561rQ r15) {
        /*
            r11 = this;
            monitor-enter(r11)
            X.1Ih r0 = r11.A05     // Catch:{ all -> 0x0052 }
            boolean r0 = r0.C2K()     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r11)     // Catch:{ all -> 0x0052 }
            return
        L_0x000b:
            r4 = r13
            r11.A0A = r13     // Catch:{ all -> 0x0052 }
            r5 = r14
            r11.A09 = r14     // Catch:{ all -> 0x0052 }
            r11.A00(r12)     // Catch:{ all -> 0x0052 }
            com.facebook.litho.ComponentTree r2 = r11.A01     // Catch:{ all -> 0x0052 }
            X.1Ih r0 = r11.A05     // Catch:{ all -> 0x0052 }
            X.0zR r3 = r0.Ahm()     // Catch:{ all -> 0x0052 }
            X.1Ih r1 = r11.A05     // Catch:{ all -> 0x0052 }
            boolean r0 = r1 instanceof X.C21741In     // Catch:{ all -> 0x0052 }
            r10 = 0
            if (r0 == 0) goto L_0x0027
            X.1In r1 = (X.C21741In) r1     // Catch:{ all -> 0x0052 }
            X.1KE r10 = r1.A00     // Catch:{ all -> 0x0052 }
        L_0x0027:
            monitor-exit(r11)     // Catch:{ all -> 0x0052 }
            if (r15 == 0) goto L_0x002d
            r2.A0V(r15)
        L_0x002d:
            if (r3 == 0) goto L_0x004a
            r7 = 0
            r6 = 1
            r8 = 1
            r9 = r7
            com.facebook.litho.ComponentTree.A09(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            monitor-enter(r11)
            com.facebook.litho.ComponentTree r0 = r11.A01     // Catch:{ all -> 0x0047 }
            if (r0 != r2) goto L_0x0045
            X.1Ih r0 = r11.A05     // Catch:{ all -> 0x0047 }
            X.0zR r0 = r0.Ahm()     // Catch:{ all -> 0x0047 }
            if (r3 != r0) goto L_0x0045
            r11.A08 = r6     // Catch:{ all -> 0x0047 }
        L_0x0045:
            monitor-exit(r11)     // Catch:{ all -> 0x0047 }
            return
        L_0x0047:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0047 }
            goto L_0x0054
        L_0x004a:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Root component can't be null"
            r1.<init>(r0)
            throw r1
        L_0x0052:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0052 }
        L_0x0054:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IK.A06(X.0p4, int, int, X.1rQ):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0028, code lost:
        if (r3 == null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        com.facebook.litho.ComponentTree.A09(r2, r3, r4, r5, false, r15, 0, null, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        monitor-enter(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r2 != r11.A01) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
        if (r3 != r11.A05.Ahm()) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003e, code lost:
        r11.A08 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        if (r15 == null) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0043, code lost:
        r11.A00 = r15.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0047, code lost:
        monitor-exit(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0048, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0049, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0053, code lost:
        throw new java.lang.IllegalArgumentException("Root component can't be null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0056, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.AnonymousClass0p4 r12, int r13, int r14, X.AnonymousClass10L r15) {
        /*
            r11 = this;
            monitor-enter(r11)
            X.1Ih r0 = r11.A05     // Catch:{ all -> 0x0054 }
            boolean r0 = r0.C2K()     // Catch:{ all -> 0x0054 }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r11)     // Catch:{ all -> 0x0054 }
            return
        L_0x000b:
            r4 = r13
            r11.A0A = r13     // Catch:{ all -> 0x0054 }
            r5 = r14
            r11.A09 = r14     // Catch:{ all -> 0x0054 }
            r11.A00(r12)     // Catch:{ all -> 0x0054 }
            com.facebook.litho.ComponentTree r2 = r11.A01     // Catch:{ all -> 0x0054 }
            X.1Ih r0 = r11.A05     // Catch:{ all -> 0x0054 }
            X.0zR r3 = r0.Ahm()     // Catch:{ all -> 0x0054 }
            X.1Ih r1 = r11.A05     // Catch:{ all -> 0x0054 }
            boolean r0 = r1 instanceof X.C21741In     // Catch:{ all -> 0x0054 }
            r10 = 0
            if (r0 == 0) goto L_0x0027
            X.1In r1 = (X.C21741In) r1     // Catch:{ all -> 0x0054 }
            X.1KE r10 = r1.A00     // Catch:{ all -> 0x0054 }
        L_0x0027:
            monitor-exit(r11)     // Catch:{ all -> 0x0054 }
            if (r3 == 0) goto L_0x004c
            r9 = 0
            r6 = 0
            r8 = 0
            r7 = r15
            com.facebook.litho.ComponentTree.A09(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            monitor-enter(r11)
            com.facebook.litho.ComponentTree r0 = r11.A01     // Catch:{ all -> 0x0049 }
            if (r2 != r0) goto L_0x0047
            X.1Ih r0 = r11.A05     // Catch:{ all -> 0x0049 }
            X.0zR r0 = r0.Ahm()     // Catch:{ all -> 0x0049 }
            if (r3 != r0) goto L_0x0047
            r0 = 1
            r11.A08 = r0     // Catch:{ all -> 0x0049 }
            if (r15 == 0) goto L_0x0047
            int r0 = r15.A00     // Catch:{ all -> 0x0049 }
            r11.A00 = r0     // Catch:{ all -> 0x0049 }
        L_0x0047:
            monitor-exit(r11)     // Catch:{ all -> 0x0049 }
            return
        L_0x0049:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0049 }
            goto L_0x0056
        L_0x004c:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Root component can't be null"
            r1.<init>(r0)
            throw r1
        L_0x0054:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0054 }
        L_0x0056:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IK.A07(X.0p4, int, int, X.10L):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        if (com.facebook.litho.ComponentTree.A0F(r3.A08, r2, r1) != false) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0029, code lost:
        if (r0 != false) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A08() {
        /*
            r4 = this;
            monitor-enter(r4)
            X.1Ih r0 = r4.A05     // Catch:{ all -> 0x0030 }
            boolean r0 = r0.C2K()     // Catch:{ all -> 0x0030 }
            if (r0 != 0) goto L_0x002b
            com.facebook.litho.ComponentTree r3 = r4.A01     // Catch:{ all -> 0x0030 }
            if (r3 == 0) goto L_0x002e
            int r2 = r4.A0A     // Catch:{ all -> 0x0030 }
            int r1 = r4.A09     // Catch:{ all -> 0x0030 }
            monitor-enter(r3)     // Catch:{ all -> 0x0030 }
            X.15v r0 = r3.A09     // Catch:{ all -> 0x0026 }
            boolean r0 = com.facebook.litho.ComponentTree.A0F(r0, r2, r1)     // Catch:{ all -> 0x0026 }
            if (r0 != 0) goto L_0x0023
            X.15v r0 = r3.A08     // Catch:{ all -> 0x0026 }
            boolean r1 = com.facebook.litho.ComponentTree.A0F(r0, r2, r1)     // Catch:{ all -> 0x0026 }
            r0 = 0
            if (r1 == 0) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            monitor-exit(r3)     // Catch:{ all -> 0x0030 }
            goto L_0x0029
        L_0x0026:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0030 }
            throw r0     // Catch:{ all -> 0x0030 }
        L_0x0029:
            if (r0 == 0) goto L_0x002e
        L_0x002b:
            r0 = 1
        L_0x002c:
            monitor-exit(r4)
            return r0
        L_0x002e:
            r0 = 0
            goto L_0x002c
        L_0x0030:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IK.A08():boolean");
    }

    public synchronized boolean A09() {
        return this.A08;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        if (r2.A09 != r4) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0A(int r3, int r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.A09()     // Catch:{ all -> 0x0013 }
            if (r0 == 0) goto L_0x0010
            int r0 = r2.A0A     // Catch:{ all -> 0x0013 }
            if (r0 != r3) goto L_0x0010
            int r1 = r2.A09     // Catch:{ all -> 0x0013 }
            r0 = 1
            if (r1 == r4) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            monitor-exit(r2)
            return r0
        L_0x0013:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IK.A0A(int, int):boolean");
    }

    private void A00(AnonymousClass0p4 r5) {
        C27375DbZ dbZ;
        if (this.A01 == null) {
            AnonymousClass11J A022 = ComponentTree.A02(r5, this.A05.Ahm());
            Object AjM = this.A05.AjM("is_reconciliation_enabled");
            Object AjM2 = this.A05.AjM("layout_diffing_enabled");
            if (AjM != null) {
                A022.A0C = ((Boolean) AjM).booleanValue();
            } else {
                A022.A0C = this.A0J;
            }
            if (AjM2 != null) {
                A022.A0B = ((Boolean) AjM2).booleanValue();
            } else {
                A022.A0B = this.A0I;
            }
            A022.A03 = this.A02;
            A022.A05 = this.A03;
            A022.A04 = this.A0D;
            A022.A0D = this.A0K;
            AnonymousClass6QY r0 = this.A0E;
            if (r0 == null) {
                dbZ = null;
            } else {
                dbZ = new C27375DbZ(r0.A00, this);
            }
            A022.A01 = dbZ;
            A022.A08 = this.A06;
            A022.A09 = this.A0G;
            A022.A07 = this.A0F;
            A022.A0E = this.A0L;
            A022.A0A = this.A0H;
            C21681Ih r02 = this.A05;
            C637038i Aht = r02.Aht();
            String Asm = r02.Asm();
            A022.A02 = Aht;
            A022.A06 = Asm;
            ComponentTree A002 = A022.A00();
            this.A01 = A002;
            C198219u r03 = this.A04;
            if (r03 != null) {
                A002.A0o = r03;
            }
        }
    }

    public AnonymousClass1IK(AnonymousClass1IJ r3) {
        this.A05 = r3.A03;
        this.A02 = r3.A00;
        this.A0D = r3.A01;
        this.A0K = false;
        this.A0E = r3.A02;
        this.A0L = r3.A09;
        this.A0F = r3.A04;
        this.A0B = A0M.getAndIncrement();
        this.A0G = r3.A05;
        this.A0J = r3.A08;
        this.A0I = r3.A07;
        this.A0H = r3.A06;
    }
}
