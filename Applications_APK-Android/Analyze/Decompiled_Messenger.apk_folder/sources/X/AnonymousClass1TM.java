package X;

import android.net.ConnectivityManager;
import android.os.Build;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1TM  reason: invalid class name */
public final class AnonymousClass1TM {
    private static volatile AnonymousClass1TM A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass1TM A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1TM.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1TM(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public String A01() {
        if (Build.VERSION.SDK_INT < 24) {
            return "unknown";
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) AnonymousClass1XX.A03(AnonymousClass1Y3.BLN, this.A00);
            if (connectivityManager != null && !connectivityManager.isActiveNetworkMetered()) {
                return "unmetered";
            }
            int restrictBackgroundStatus = connectivityManager.getRestrictBackgroundStatus();
            if (restrictBackgroundStatus == 1) {
                return "disabled";
            }
            if (restrictBackgroundStatus == 2) {
                return "whitelisted";
            }
            if (restrictBackgroundStatus == 3) {
                return "enabled";
            }
            return "unknown";
        } catch (Exception unused) {
        }
    }

    private AnonymousClass1TM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
