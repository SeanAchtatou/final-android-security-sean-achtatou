package X;

import android.util.SparseIntArray;
import com.facebook.common.dextricks.DexStore;

/* renamed from: X.1NA  reason: invalid class name */
public final class AnonymousClass1NA {
    public final int A00;
    public final int A01;
    public final C14320t5 A02;
    public final AnonymousClass1N7 A03;
    public final AnonymousClass1N7 A04;
    public final AnonymousClass1N7 A05;
    public final AnonymousClass1N7 A06;
    public final AnonymousClass1N4 A07;
    public final AnonymousClass1N4 A08;
    public final AnonymousClass1N4 A09;
    public final String A0A;
    public final boolean A0B;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public AnonymousClass1NA(C30551iG r8) {
        int i;
        int i2;
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("PoolConfig()");
        }
        AnonymousClass1N7 r0 = r8.A03;
        this.A03 = r0 == null ? C30571iI.A00() : r0;
        AnonymousClass1N4 r02 = r8.A05;
        this.A07 = r02 == null ? C22901Ng.A00() : r02;
        AnonymousClass1N7 r5 = r8.A04;
        if (r5 == null) {
            int i3 = C190068sY.A00;
            int i4 = i3 * 4194304;
            SparseIntArray sparseIntArray = new SparseIntArray();
            for (int i5 = DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP; i5 <= 4194304; i5 <<= 1) {
                sparseIntArray.put(i5, i3);
            }
            r5 = new AnonymousClass1N7(4194304, i4, sparseIntArray, i3);
        }
        this.A04 = r5;
        C14320t5 r03 = r8.A02;
        this.A02 = r03 == null ? BTD.A00() : r03;
        SparseIntArray sparseIntArray2 = new SparseIntArray();
        sparseIntArray2.put(1024, 5);
        sparseIntArray2.put(2048, 5);
        sparseIntArray2.put(DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED, 5);
        sparseIntArray2.put(DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED, 5);
        sparseIntArray2.put(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET, 5);
        sparseIntArray2.put(DexStore.LOAD_RESULT_PGO, 5);
        sparseIntArray2.put(65536, 5);
        sparseIntArray2.put(DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP, 5);
        sparseIntArray2.put(DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED, 2);
        sparseIntArray2.put(DexStore.LOAD_RESULT_WITH_VDEX_ODEX, 2);
        sparseIntArray2.put(1048576, 2);
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min < 16777216) {
            i = 3145728;
        } else {
            i = 12582912;
            if (min < 33554432) {
                i = 6291456;
            }
        }
        int min2 = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min2 < 16777216) {
            i2 = min2 >> 1;
        } else {
            i2 = (min2 >> 2) * 3;
        }
        this.A05 = new AnonymousClass1N7(i, i2, sparseIntArray2, -1);
        AnonymousClass1N4 r04 = r8.A06;
        this.A08 = r04 == null ? C22901Ng.A00() : r04;
        SparseIntArray sparseIntArray3 = new SparseIntArray();
        sparseIntArray3.put(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET, 5);
        this.A06 = new AnonymousClass1N7(81920, 1048576, sparseIntArray3, -1);
        AnonymousClass1N4 r05 = r8.A07;
        this.A09 = r05 == null ? C22901Ng.A00() : r05;
        String str = r8.A08;
        this.A0A = str == null ? "legacy" : str;
        this.A01 = r8.A01;
        int i6 = r8.A00;
        this.A00 = i6 > 0 ? i6 : 4194304;
        this.A0B = r8.A09;
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
    }
}
