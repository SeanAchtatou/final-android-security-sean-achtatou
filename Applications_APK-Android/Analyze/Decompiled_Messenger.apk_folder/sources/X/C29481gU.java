package X;

/* renamed from: X.1gU  reason: invalid class name and case insensitive filesystem */
public enum C29481gU {
    ACCENT,
    PRIMARY_GLYPH,
    INVERSE_PRIMARY_GLYPH,
    SECONDARY_GLYPH,
    TERTIARY_GLYPH,
    DISABLED_GLYPH,
    WASH,
    SECONDARY_WASH;

    /* access modifiers changed from: public */
    static {
        values();
    }
}
