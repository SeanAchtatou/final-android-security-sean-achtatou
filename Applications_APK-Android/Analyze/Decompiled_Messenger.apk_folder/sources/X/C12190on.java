package X;

import com.facebook.graphservice.tree.TreeJNI;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0on  reason: invalid class name and case insensitive filesystem */
public abstract class C12190on extends C26911cL implements C12200oo {
    private final Object A0G(int i) {
        int fieldCacheIndex;
        if (this.A00 == null || (fieldCacheIndex = getFieldCacheIndex(i)) < 0) {
            return null;
        }
        return this.A00[fieldCacheIndex];
    }

    private final void A0H(int i, Object obj) {
        int fieldCacheIndex;
        if (this.A00 != null && (fieldCacheIndex = getFieldCacheIndex(i)) >= 0) {
            Object[] objArr = this.A00;
            if (obj == null) {
                obj = C26911cL.A02;
            }
            objArr[fieldCacheIndex] = obj;
        }
    }

    public C12190on(int i, int[] iArr) {
        super(i, iArr);
    }

    public AnonymousClass7NQ A0I(String str, Class cls, int i) {
        int hashCode = str.hashCode();
        Object A0G = A0G(hashCode);
        if (A0G == null) {
            A0G = getPaginableTreeList(str, cls, i);
            A0H(hashCode, A0G);
        }
        return (AnonymousClass7NQ) A0G;
    }

    public final TreeJNI A0J(int i, Class cls, int i2) {
        Object A0G = A0G(i);
        if (A0G == null) {
            A0G = getTree(i, cls, i2);
            A0H(i, A0G);
        }
        if (A0G != C26911cL.A02) {
            return (TreeJNI) A0G;
        }
        return null;
    }

    public final ImmutableList A0K(int i) {
        Object A0G = A0G(i);
        if (A0G == null) {
            A0G = getIntList(i);
            A0H(i, A0G);
        }
        return (ImmutableList) A0G;
    }

    public final ImmutableList A0L(int i) {
        Object A0G = A0G(i);
        if (A0G == null) {
            A0G = getStringList(i);
            A0H(i, A0G);
        }
        return (ImmutableList) A0G;
    }

    public final ImmutableList A0M(int i, Class cls, int i2) {
        Object A0G = A0G(i);
        if (A0G == null) {
            A0G = getTreeList(i, cls, i2);
            A0H(i, A0G);
        }
        return (ImmutableList) A0G;
    }

    public final ImmutableList A0N(int i, Enum enumR) {
        Object A0G = A0G(i);
        if (A0G == null) {
            A0G = C27191cn.A00(getStringList(i), enumR);
            A0H(i, A0G);
        }
        if (A0G instanceof ImmutableList) {
            ImmutableList immutableList = (ImmutableList) A0G;
            if (!immutableList.isEmpty() && (immutableList.get(0) instanceof String)) {
                A0G = C27191cn.A00(immutableList, enumR);
                A0H(i, A0G);
            }
        }
        return (ImmutableList) A0G;
    }

    public final Enum A0O(int i, Enum enumR) {
        Object A0G = A0G(i);
        if (A0G == null) {
            A0G = C27191cn.A01(getString(i), enumR);
            A0H(i, A0G);
        }
        if (A0G instanceof String) {
            A0G = C27191cn.A01((String) A0G, enumR);
            A0H(i, A0G);
        }
        return (Enum) A0G;
    }

    public final String A0P(int i) {
        Object A0G = A0G(i);
        if (A0G == null) {
            A0G = getString(i);
            A0H(i, A0G);
        }
        if (A0G != C26911cL.A02) {
            return (String) A0G;
        }
        return null;
    }
}
