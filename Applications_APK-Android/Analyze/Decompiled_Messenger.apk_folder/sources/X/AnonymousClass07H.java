package X;

import com.facebook.common.util.TriState;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.07H  reason: invalid class name */
public final class AnonymousClass07H extends C08190ep {
    private AnonymousClass0UN A00;
    private final AnonymousClass04s A01 = AnonymousClass04s.A06;

    public static final AnonymousClass07H A00(AnonymousClass1XY r1) {
        return new AnonymousClass07H(r1);
    }

    public AnonymousClass0Ti AsW() {
        return this.A01.AsW();
    }

    public void BeF(C04270Tg r2) {
        this.A01.BeF(r2);
    }

    public void BeL(C04270Tg r2, String str, String str2) {
        this.A01.BeL(r2, str, str2);
    }

    public void BeM(C04270Tg r2) {
        this.A01.BeM(r2);
    }

    public void BeQ(C04270Tg r9, String str, AnonymousClass0lr r11, long j, boolean z, int i) {
        this.A01.BeQ(r9, str, r11, j, z, i);
    }

    public void BeR(C04270Tg r2) {
        this.A01.BeR(r2);
    }

    public void BeT(C04270Tg r2) {
        this.A01.BeT(r2);
    }

    public void BeU(C04270Tg r4) {
        if (!r4.A0N || ((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, this.A00)).A0C() != TriState.YES) {
            this.A01.BeU(r4);
        } else {
            BeM(r4);
        }
    }

    public boolean Bkl(int i, int i2) {
        return this.A01.Bkl(i, i2);
    }

    public void CB4(QuickPerformanceLogger quickPerformanceLogger) {
        this.A01.CB4(quickPerformanceLogger);
    }

    private AnonymousClass07H(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public void BeV(int i, int i2, C04270Tg r3) {
    }
}
