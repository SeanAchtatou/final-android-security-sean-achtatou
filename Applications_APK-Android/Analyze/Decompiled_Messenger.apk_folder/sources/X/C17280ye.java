package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.service.model.FetchThreadListResult;

/* renamed from: X.0ye  reason: invalid class name and case insensitive filesystem */
public final class C17280ye implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new FetchThreadListResult(parcel);
    }

    public Object[] newArray(int i) {
        return new FetchThreadListResult[i];
    }
}
