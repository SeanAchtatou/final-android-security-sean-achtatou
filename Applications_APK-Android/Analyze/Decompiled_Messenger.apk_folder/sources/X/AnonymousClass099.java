package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.099  reason: invalid class name */
public final class AnonymousClass099 extends ByteArrayOutputStream {
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        if (com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited == false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void flush() {
        /*
            r3 = this;
            int r1 = r3.count
            byte[] r2 = r3.buf
            int r0 = r2.length
            if (r1 != r0) goto L_0x0016
            boolean r0 = com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited
            if (r0 != 0) goto L_0x0012
        L_0x000b:
            java.lang.String r1 = "AppStateLoggerNative"
            java.lang.String r0 = "AppStateLoggerNative.initializeNativeCrashReporting not called.  setBreakpadStreamData will most likely crash."
            X.C010708t.A0J(r1, r0)
        L_0x0012:
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.setBreakpadStreamDataNative(r2)
            return
        L_0x0016:
            byte[] r2 = r3.toByteArray()
            boolean r0 = com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited
            if (r0 != 0) goto L_0x0012
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass099.flush():void");
    }
}
