package X;

import com.facebook.appupdate.ReleaseInfo;

/* renamed from: X.14u  reason: invalid class name and case insensitive filesystem */
public final class C187114u {
    private static final Class A02 = C187114u.class;
    public AnonymousClass0UN A00;
    private final C25051Yd A01;

    private AnonymousClass9FT A00() {
        for (AnonymousClass9FT r2 : ((AnonymousClass9F1) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BJq, this.A00)).A02()) {
            C195319Ft r1 = r2.A05().operationState;
            if (r1 != C195319Ft.STATE_NOT_STARTED && r1 != C195319Ft.STATE_DISCARDED) {
                return r2;
            }
        }
        return null;
    }

    public static final C187114u A01(AnonymousClass1XY r1) {
        return new C187114u(r1);
    }

    private static void A02(boolean z, String str) {
        if (z) {
            C010708t.A05(A02, str);
            throw new RuntimeException(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0037, code lost:
        if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A03(X.C187114u r4, boolean r5) {
        /*
            int r2 = X.AnonymousClass1Y3.AF5
            X.0UN r1 = r4.A00
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.9D1 r0 = (X.AnonymousClass9D1) r0
            boolean r0 = r0.A02()
            r3 = 0
            if (r0 != 0) goto L_0x0018
            java.lang.String r0 = "Failed eligibility check, skipping"
        L_0x0014:
            A02(r5, r0)
            return r3
        L_0x0018:
            int r1 = X.AnonymousClass1Y3.BJq
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.9F1 r0 = (X.AnonymousClass9F1) r0
            boolean r0 = r0.A06()
            if (r0 != 0) goto L_0x003f
            boolean r0 = r4.A07(r5)
            if (r0 == 0) goto L_0x0039
            android.os.Looper r2 = android.os.Looper.myLooper()
            android.os.Looper r1 = android.os.Looper.getMainLooper()
            r0 = 1
            if (r2 != r1) goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            if (r0 != 0) goto L_0x003f
            java.lang.String r0 = "AppUpdate init not done, skipping"
            goto L_0x0014
        L_0x003f:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C187114u.A03(X.14u, boolean):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00cf, code lost:
        if (r8 > (r3 + ((java.lang.Long) r1.get(r6 - 1)).longValue())) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e9, code lost:
        if (r1 == false) goto L_0x00eb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A05(X.C187114u r12, boolean r13) {
        /*
            int r2 = X.AnonymousClass1Y3.ArE
            X.0UN r1 = r12.A00
            r0 = 4
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4oA r6 = (X.C98974oA) r6
            com.facebook.prefs.shared.FbSharedPreferences r3 = r6.A02
            X.1Y7 r2 = X.C31821kR.A04
            r0 = 0
            long r4 = r3.At2(r2, r0)
            X.06B r0 = r6.A00
            long r2 = r0.now()
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x0021
            r0 = 1
        L_0x0021:
            if (r0 == 0) goto L_0x004c
            r6.A07()
            r0 = 1
        L_0x0027:
            if (r0 == 0) goto L_0x004e
            r2 = 0
            int r1 = X.AnonymousClass1Y3.BJq
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9F1 r0 = (X.AnonymousClass9F1) r0
            java.util.List r0 = r0.A02()
            java.util.Iterator r1 = r0.iterator()
        L_0x003c:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004e
            java.lang.Object r0 = r1.next()
            X.9FT r0 = (X.AnonymousClass9FT) r0
            r0.A06()
            goto L_0x003c
        L_0x004c:
            r0 = 0
            goto L_0x0027
        L_0x004e:
            X.9FT r0 = r12.A00()
            r11 = 0
            if (r0 != 0) goto L_0x017a
            boolean r10 = r12.A07(r13)
            r2 = 4
            int r1 = X.AnonymousClass1Y3.ArE
            X.0UN r0 = r12.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.4oA r7 = (X.C98974oA) r7
            X.06B r0 = r7.A00
            long r8 = r0.now()
            com.facebook.prefs.shared.FbSharedPreferences r3 = r7.A02
            X.1Y7 r0 = X.C31821kR.A01
            r1 = 0
            long r3 = r3.At2(r0, r1)
            r5 = 0
            int r0 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r0 <= 0) goto L_0x0098
            r5 = 0
        L_0x007a:
            if (r5 != 0) goto L_0x0084
            if (r10 != 0) goto L_0x0084
            java.lang.String r0 = "Download prompt not permitted, skipping"
        L_0x0080:
            A02(r13, r0)
            return r11
        L_0x0084:
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AF5
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9D1 r0 = (X.AnonymousClass9D1) r0
            boolean r0 = r0.A02()
            if (r0 != 0) goto L_0x00d3
            java.lang.String r0 = "Failed eligibility check, skipping"
            goto L_0x0080
        L_0x0098:
            com.facebook.prefs.shared.FbSharedPreferences r3 = r7.A02
            X.1Y7 r0 = X.C31821kR.A02
            int r6 = r3.AqN(r0, r11)
            com.facebook.prefs.shared.FbSharedPreferences r3 = r7.A02
            X.1Y7 r0 = X.C31821kR.A03
            long r3 = r3.At2(r0, r1)
            com.facebook.prefs.shared.FbSharedPreferences r1 = r7.A02
            X.1Y7 r0 = X.C31821kR.A00
            boolean r0 = r1.Aep(r0, r11)
            r2 = 1
            if (r6 == 0) goto L_0x00d1
            if (r0 != 0) goto L_0x00d1
            X.4o9 r0 = r7.A03
            com.google.common.collect.ImmutableList r1 = r0.A02()
            int r0 = r1.size()
            if (r6 >= r0) goto L_0x007a
            int r6 = r6 - r2
            java.lang.Object r0 = r1.get(r6)
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            long r3 = r3 + r0
            int r0 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x007a
        L_0x00d1:
            r5 = 1
            goto L_0x007a
        L_0x00d3:
            int r1 = X.AnonymousClass1Y3.AQR
            X.0UN r0 = r12.A00
            r2 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14v r1 = (X.C187214v) r1
            com.facebook.appupdate.ReleaseInfo r0 = X.C187214v.A00(r1, r13)
            if (r0 == 0) goto L_0x00eb
            boolean r1 = X.C187214v.A03(r1, r0, r13)
            r0 = 1
            if (r1 != 0) goto L_0x00ec
        L_0x00eb:
            r0 = 0
        L_0x00ec:
            r4 = 5
            if (r0 != 0) goto L_0x0105
            java.lang.String r0 = "No valid ReleaseInfo, skipping"
            A02(r13, r0)
            int r1 = X.AnonymousClass1Y3.B4X
            X.0UN r0 = r12.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.3f6 r2 = (X.C72873f6) r2
            r1 = 0
            java.lang.String r0 = "selfupdate2_no_release_info_found"
            r2.A09(r0, r1)
            return r11
        L_0x0105:
            int r1 = X.AnonymousClass1Y3.AQR
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14v r0 = (X.C187214v) r0
            boolean r0 = r0.A04(r13)
            if (r0 != 0) goto L_0x0179
            java.lang.String r0 = "Insufficient storage for update, skipping"
            A02(r13, r0)
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            int r1 = X.AnonymousClass1Y3.AfZ
            X.0UN r0 = r12.A00
            r5 = 6
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.9Da r0 = (X.C194819Da) r0
            long r1 = r0.A01()
            r0 = 406(0x196, float:5.69E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            X.C52052iO.A02(r3, r0, r1)
            int r1 = X.AnonymousClass1Y3.AfZ
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.9Da r0 = (X.C194819Da) r0
            long r1 = r0.A02()
            r0 = 407(0x197, float:5.7E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            X.C52052iO.A02(r3, r0, r1)
            int r1 = X.AnonymousClass1Y3.B4X
            X.0UN r0 = r12.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.3f6 r1 = (X.C72873f6) r1
            java.lang.String r0 = "selfupdate2_insufficient_disk_space"
            r1.A09(r0, r3)
            X.1Yd r2 = r12.A01
            r0 = 284593126904230(0x102d6003c11a6, double:1.40607687045918E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x017a
            r2 = 3
            int r1 = X.AnonymousClass1Y3.BS1
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9DX r0 = (X.AnonymousClass9DX) r0
            r0.A05()
            return r11
        L_0x0179:
            return r2
        L_0x017a:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C187114u.A05(X.14u, boolean):boolean");
    }

    public boolean A07(boolean z) {
        C187214v r0;
        ReleaseInfo A002;
        if (!this.A01.Aem(284593125134739L) && (A002 = C187214v.A00((r0 = (C187214v) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AQR, this.A00)), z)) != null && C187214v.A03(r0, A002, z)) {
            return A002.isHardNag;
        }
        return false;
    }

    private C187114u(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(7, r3);
        this.A01 = AnonymousClass0WT.A00(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0071, code lost:
        if (r6.A00.now() > (r8 + ((java.lang.Long) r1.get(r4 - 1)).longValue())) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a2, code lost:
        if (r3 != false) goto L_0x00a4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(X.C187114u r13, boolean r14) {
        /*
            X.9FT r0 = r13.A00()
            r5 = 0
            if (r0 == 0) goto L_0x001b
            X.9FN r4 = r0.A05()
            boolean r12 = r13.A07(r14)
            X.9Ft r0 = r4.operationState
            int r0 = r0.ordinal()
            r3 = 4
            r2 = 2
            r11 = 1
            switch(r0) {
                case 6: goto L_0x001c;
                case 7: goto L_0x00ae;
                default: goto L_0x001b;
            }
        L_0x001b:
            return r5
        L_0x001c:
            int r1 = X.AnonymousClass1Y3.AF5
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9D1 r0 = (X.AnonymousClass9D1) r0
            boolean r0 = r0.A02()
            if (r0 != 0) goto L_0x0032
            java.lang.String r0 = "Install prompt not permitted. Not eligible for Self Update, skipping"
        L_0x002e:
            A02(r14, r0)
            return r5
        L_0x0032:
            int r1 = X.AnonymousClass1Y3.ArE
            X.0UN r0 = r13.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.4oA r6 = (X.C98974oA) r6
            com.facebook.prefs.shared.FbSharedPreferences r1 = r6.A02
            X.1Y7 r0 = X.C31821kR.A0G
            int r4 = r1.AqN(r0, r5)
            com.facebook.prefs.shared.FbSharedPreferences r3 = r6.A02
            X.1Y7 r2 = X.C31821kR.A0H
            r0 = 0
            long r8 = r3.At2(r2, r0)
            r7 = 1
            if (r4 == 0) goto L_0x0073
            X.4o9 r0 = r6.A03
            com.google.common.collect.ImmutableList r1 = r0.A03()
            int r0 = r1.size()
            if (r4 >= r0) goto L_0x00ac
            int r4 = r4 - r11
            java.lang.Object r0 = r1.get(r4)
            java.lang.Long r0 = (java.lang.Long) r0
            long r3 = r0.longValue()
            X.06B r0 = r6.A00
            long r1 = r0.now()
            long r8 = r8 + r3
            int r0 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r0 <= 0) goto L_0x00ac
        L_0x0073:
            if (r7 != 0) goto L_0x00a4
            com.facebook.prefs.shared.FbSharedPreferences r3 = r6.A02
            X.1Y7 r2 = X.C31821kR.A0R
            r0 = 0
            long r9 = r3.At2(r2, r0)
            X.06B r0 = r6.A00
            long r7 = r0.now()
            r0 = 3600000(0x36ee80, double:1.7786363E-317)
            long r9 = r9 + r0
            r3 = 0
            int r0 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r0 > 0) goto L_0x00a1
            com.facebook.prefs.shared.FbSharedPreferences r1 = r6.A02
            X.1Y7 r0 = X.C31821kR.A0Q
            com.facebook.common.util.TriState r2 = r1.Aeq(r0)
            boolean r1 = X.C98974oA.A04(r6)
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO
            if (r2 != r0) goto L_0x00a1
            if (r1 == 0) goto L_0x00a1
            r3 = 1
        L_0x00a1:
            r0 = 0
            if (r3 == 0) goto L_0x00a5
        L_0x00a4:
            r0 = 1
        L_0x00a5:
            if (r0 != 0) goto L_0x00d6
            if (r12 != 0) goto L_0x00d6
            java.lang.String r0 = "Install prompt not permitted, skipping"
            goto L_0x002e
        L_0x00ac:
            r7 = 0
            goto L_0x0073
        L_0x00ae:
            int r1 = X.AnonymousClass1Y3.ArE
            X.0UN r0 = r13.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.4oA r1 = (X.C98974oA) r1
            X.06B r0 = r1.A00
            long r6 = r0.now()
            com.facebook.prefs.shared.FbSharedPreferences r3 = r1.A02
            X.1Y7 r2 = X.C31821kR.A01
            r0 = 0
            long r2 = r3.At2(r2, r0)
            int r1 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x00ce
            r0 = 1
        L_0x00ce:
            if (r0 != 0) goto L_0x00d2
            if (r12 == 0) goto L_0x00d7
        L_0x00d2:
            boolean r0 = r4.isBackgroundMode
            if (r0 != 0) goto L_0x00d7
        L_0x00d6:
            return r11
        L_0x00d7:
            java.lang.String r0 = "Failure screen not permitted, skipping"
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C187114u.A04(X.14u, boolean):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.Intent A06(android.content.Context r5, boolean r6, boolean r7) {
        /*
            r4 = this;
            boolean r0 = A03(r4, r6)
            r3 = 0
            if (r0 == 0) goto L_0x006f
            boolean r0 = r4.A07(r6)
            if (r0 == 0) goto L_0x0018
            android.os.Looper r2 = android.os.Looper.myLooper()
            android.os.Looper r1 = android.os.Looper.getMainLooper()
            r0 = 1
            if (r2 != r1) goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            if (r0 == 0) goto L_0x0029
            r2 = 0
            int r1 = X.AnonymousClass1Y3.BJq
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9F1 r0 = (X.AnonymousClass9F1) r0
            r0.A03()
        L_0x0029:
            boolean r0 = A05(r4, r6)
            if (r0 == 0) goto L_0x004c
            android.content.Intent r2 = new android.content.Intent
            java.lang.Class<com.facebook.selfupdate2.SelfUpdateActivity> r0 = com.facebook.selfupdate2.SelfUpdateActivity.class
            r2.<init>(r5, r0)
            r1 = 1
            r0 = 635(0x27b, float:8.9E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r2.putExtra(r0, r1)
            if (r7 == 0) goto L_0x004b
            r0 = 606(0x25e, float:8.49E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r2.putExtra(r0, r1)
        L_0x004b:
            return r2
        L_0x004c:
            boolean r0 = A04(r4, r6)
            if (r0 == 0) goto L_0x006f
            X.9FT r0 = r4.A00()
            if (r0 == 0) goto L_0x006f
            X.9FN r0 = r0.A05()
            java.lang.String r2 = r0.operationUuid
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.facebook.selfupdate2.SelfUpdateActivity> r0 = com.facebook.selfupdate2.SelfUpdateActivity.class
            r1.<init>(r5, r0)
            r0 = 554(0x22a, float:7.76E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r1.putExtra(r0, r2)
            return r1
        L_0x006f:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C187114u.A06(android.content.Context, boolean, boolean):android.content.Intent");
    }
}
