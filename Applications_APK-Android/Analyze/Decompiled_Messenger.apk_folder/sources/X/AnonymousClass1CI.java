package X;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import java.util.Iterator;

/* renamed from: X.1CI  reason: invalid class name */
public final class AnonymousClass1CI extends AnonymousClass1CJ {
    public final /* synthetic */ Function A00;
    public final /* synthetic */ Iterable A01;

    public AnonymousClass1CI(Iterable iterable, Function function) {
        this.A01 = iterable;
        this.A00 = function;
    }

    public Iterator iterator() {
        Iterator it = this.A01.iterator();
        Function function = this.A00;
        Preconditions.checkNotNull(function);
        return new C26991cT(it, function);
    }
}
