package X;

import android.database.Cursor;
import com.facebook.omnistore.OmnistoreIOException;

/* renamed from: X.1lE  reason: invalid class name and case insensitive filesystem */
public final class C32141lE {
    public final Cursor A00;

    public Cursor A00() {
        if (!this.A00.isClosed()) {
            return this.A00;
        }
        throw new OmnistoreIOException("SQLite cursor is closed.");
    }

    public C32141lE(Cursor cursor) {
        this.A00 = cursor;
    }
}
