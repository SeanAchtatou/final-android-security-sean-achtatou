package X;

import com.google.common.base.MoreObjects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* renamed from: X.1Yn  reason: invalid class name and case insensitive filesystem */
public final class C25151Yn implements Runnable {
    public static final String __redex_internal_original_name = "com.google.common.util.concurrent.Futures$CallbackListener";
    public final C04810Wg A00;
    public final Future A01;

    public void run() {
        try {
            this.A00.Bqf(C05350Yp.A06(this.A01));
        } catch (ExecutionException e) {
            this.A00.BYh(e.getCause());
        } catch (Error | RuntimeException e2) {
            this.A00.BYh(e2);
        }
    }

    public C25151Yn(Future future, C04810Wg r2) {
        this.A01 = future;
        this.A00 = r2;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.addValue(this.A00);
        return stringHelper.toString();
    }
}
