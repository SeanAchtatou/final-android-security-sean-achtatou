package X;

/* renamed from: X.1Sy  reason: invalid class name and case insensitive filesystem */
public final class C24271Sy {
    public static float A00(double d, double d2, double d3) {
        boolean z = true;
        boolean z2 = false;
        if (d >= 0.0d) {
            z2 = true;
        }
        AnonymousClass064.A03(z2);
        boolean z3 = false;
        if (d2 >= 0.0d) {
            z3 = true;
        }
        AnonymousClass064.A03(z3);
        if (d3 < 0.0d) {
            z = false;
        }
        AnonymousClass064.A03(z);
        return (float) Math.abs(Math.toDegrees(Math.acos(((Math.pow(d2, 2.0d) + Math.pow(d3, 2.0d)) - Math.pow(d, 2.0d)) / ((d2 * 2.0d) * d3))));
    }
}
