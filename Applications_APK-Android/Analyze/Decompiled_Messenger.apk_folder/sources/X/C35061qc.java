package X;

import android.os.Process;

/* renamed from: X.1qc  reason: invalid class name and case insensitive filesystem */
public final class C35061qc implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.appboost.cpu.periodicboost.PeriodicThreadBooster$1";
    public final /* synthetic */ C34731q5 A00;

    public C35061qc(C34731q5 r1) {
        this.A00 = r1;
    }

    public void run() {
        C34731q5 r1 = this.A00;
        if (r1.A07 == null) {
            r1.A07 = Integer.valueOf(Process.getThreadPriority(Process.myPid()));
        }
        Process.setThreadPriority(this.A00.A03);
        C34731q5 r0 = this.A00;
        AnonymousClass00S.A05(r0.A0E, this, (long) r0.A04, -1985013763);
    }
}
