package X;

import java.io.File;

/* renamed from: X.0MU  reason: invalid class name */
public final class AnonymousClass0MU {
    public static final boolean A00 = new File("/data/local/tmp/save_oatmeal_sdcard").exists();
    public static final boolean A01 = new File("/data/local/tmp/use_dex2oat_quicken").exists();
    public static final boolean A02 = new File("/data/local/tmp/use_mixed_mode").exists();
    public static final boolean A03 = new File("/data/local/tmp/use_mixed_mode_pgo").exists();
    public static final boolean A04 = new File("/data/local/tmp/use_oatmeal").exists();

    static {
        new File("/data/local/tmp/should_zopt_speed_pcs").exists();
        new File("/data/local/tmp/quick_bg_zopt").exists();
        new File("/data/local/tmp/low_mem_zopt").exists();
    }
}
