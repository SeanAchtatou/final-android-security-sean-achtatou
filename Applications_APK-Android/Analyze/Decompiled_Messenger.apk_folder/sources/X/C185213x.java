package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.13x  reason: invalid class name and case insensitive filesystem */
public final class C185213x {
    private static volatile C185213x A04;
    public AnonymousClass1EU A00;
    public final Context A01;
    public final C012309k A02;
    public final C04310Tq A03;

    public static final C185213x A00(AnonymousClass1XY r5) {
        if (A04 == null) {
            synchronized (C185213x.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A04 = new C185213x(applicationInjector, AnonymousClass1YA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private C185213x(AnonymousClass1XY r3, Context context) {
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.A7j, r3);
        this.A01 = context;
        this.A02 = new C012309k(context, null);
    }
}
