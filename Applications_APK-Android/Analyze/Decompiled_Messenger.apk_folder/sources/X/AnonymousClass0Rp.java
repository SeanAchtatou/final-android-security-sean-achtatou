package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0Rp  reason: invalid class name */
public final class AnonymousClass0Rp {
    public final List A00;

    public String toString() {
        return TextUtils.join(",", this.A00.toArray());
    }

    public AnonymousClass0Rp(List list) {
        this.A00 = Collections.unmodifiableList(new ArrayList(list));
    }
}
