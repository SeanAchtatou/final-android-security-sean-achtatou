package X;

import android.content.Context;
import android.content.SharedPreferences;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0OO  reason: invalid class name */
public final class AnonymousClass0OO implements AnonymousClass07g {
    public int A00;
    public C01730Bk A01 = null;
    public ArrayList A02 = new ArrayList();
    public ScheduledFuture A03 = null;
    public final Context A04;
    public final SharedPreferences A05;
    public final ScheduledExecutorService A06;
    private final SimpleDateFormat A07;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0057, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x005b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.AnonymousClass0OO r8, boolean r9) {
        /*
            monitor-enter(r8)
            java.util.ArrayList r2 = r8.A02     // Catch:{ all -> 0x0097 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            r8.A02 = r0     // Catch:{ all -> 0x0097 }
            if (r9 == 0) goto L_0x0014
            java.util.concurrent.ScheduledFuture r1 = r8.A03     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x0014
            r0 = 0
            r1.cancel(r0)     // Catch:{ all -> 0x0097 }
        L_0x0014:
            r0 = 0
            r8.A03 = r0     // Catch:{ all -> 0x0097 }
            monitor-exit(r8)     // Catch:{ all -> 0x0097 }
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0096
            java.io.File r5 = new java.io.File
            android.content.Context r0 = r8.A04
            java.io.File r1 = r0.getCacheDir()
            java.lang.String r4 = "fbnslite_log"
            int r0 = r8.A00
            java.lang.String r0 = X.AnonymousClass08S.A09(r4, r0)
            r5.<init>(r1, r0)
            r7 = 1
            java.io.FileWriter r3 = new java.io.FileWriter     // Catch:{ IOException -> 0x005c }
            r3.<init>(r5, r7)     // Catch:{ IOException -> 0x005c }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0055 }
        L_0x003b:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x0051
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0055 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0055 }
            r0 = 10
            java.lang.String r0 = X.AnonymousClass08S.A06(r1, r0)     // Catch:{ all -> 0x0055 }
            r3.write(r0)     // Catch:{ all -> 0x0055 }
            goto L_0x003b
        L_0x0051:
            r3.close()     // Catch:{ IOException -> 0x005c }
            goto L_0x005c
        L_0x0055:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0057 }
        L_0x0057:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x005b }
        L_0x005b:
            throw r0     // Catch:{ IOException -> 0x005c }
        L_0x005c:
            long r5 = r5.length()
            r2 = 30000(0x7530, double:1.4822E-319)
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            r0 = 0
            if (r1 < 0) goto L_0x0068
            r0 = 1
        L_0x0068:
            if (r0 == 0) goto L_0x0096
            int r0 = r8.A00
            if (r0 == 0) goto L_0x006f
            r7 = 0
        L_0x006f:
            r8.A00 = r7
            android.content.SharedPreferences r0 = r8.A05
            android.content.SharedPreferences$Editor r2 = r0.edit()
            int r1 = r8.A00
            java.lang.String r0 = "CurrentFile"
            android.content.SharedPreferences$Editor r0 = r2.putInt(r0, r1)
            r0.commit()
            java.io.File r2 = new java.io.File
            android.content.Context r0 = r8.A04
            java.io.File r1 = r0.getCacheDir()
            int r0 = r8.A00
            java.lang.String r0 = X.AnonymousClass08S.A09(r4, r0)
            r2.<init>(r1, r0)
            r2.delete()
        L_0x0096:
            return
        L_0x0097:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0097 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0OO.A00(X.0OO, boolean):void");
    }

    public void BIp(String str, String str2) {
        BIo(AnonymousClass08S.A0S("[", str, "] ", str2));
    }

    public void BIq(String str, Map map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : map.entrySet()) {
            sb.append((String) entry.getKey());
            sb.append("=");
            sb.append((String) entry.getValue());
            sb.append("; ");
        }
        BIo(AnonymousClass08S.A0S("[", str, "] ", sb.toString()));
    }

    public AnonymousClass0OO(Context context) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd HH:mm:ss.SSS", Locale.US);
        this.A07 = simpleDateFormat;
        this.A04 = context;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        this.A05 = this.A04.getSharedPreferences("Fbnslite_Flytrap", 0);
        this.A06 = Executors.newSingleThreadScheduledExecutor();
        this.A00 = this.A05.getInt("CurrentFile", 0);
    }

    public void BIo(String str) {
        String A0P = AnonymousClass08S.A0P(this.A07.format(new Date(System.currentTimeMillis())), " ", str);
        synchronized (this) {
            if (A0P.length() > 500) {
                A0P = A0P.substring(0, 500);
            }
            this.A02.add(A0P);
            if (this.A03 == null) {
                this.A03 = this.A06.schedule(new C02160De(this), 60000, TimeUnit.MILLISECONDS);
            }
        }
    }

    public void CCF(C01730Bk r1) {
        this.A01 = r1;
    }
}
