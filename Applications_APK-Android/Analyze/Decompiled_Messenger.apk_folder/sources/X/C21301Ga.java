package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Ga  reason: invalid class name and case insensitive filesystem */
public final class C21301Ga extends C17770zR {
    @Comparable(type = 3)
    public int A00 = C15320v6.MEASURED_STATE_MASK;
    @Comparable(type = 3)
    public int A01;
    @Comparable(type = 3)
    public int A02 = 0;
    @Comparable(type = 13)
    public String A03 = "Line";

    public C21301Ga() {
        super("Line");
    }
}
