package X;

import android.os.SystemClock;
import android.util.Pair;
import com.facebook.common.stringformat.StringFormatUtil;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableEntry;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.RejectedExecutionException;

/* renamed from: X.0VZ  reason: invalid class name */
public abstract class AnonymousClass0VZ {
    public List A00;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0029, code lost:
        if (r5 > 0) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0014, code lost:
        if (r5 > 0) goto L_0x0016;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[LOOP:0: B:18:0x0037->B:20:0x003b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A04(java.util.List r8, com.google.common.base.Predicate r9, boolean r10) {
        /*
            r7 = this;
            boolean r0 = r7 instanceof X.AnonymousClass0VY
            if (r0 != 0) goto L_0x0059
            r6 = r7
            X.0Vk r6 = (X.C04610Vk) r6
            java.util.PriorityQueue r0 = r6.A02
            r2 = 0
            r1 = 1
            if (r0 == 0) goto L_0x001d
            int r5 = A00(r8, r0, r9, r10)
            int r5 = r5 + r2
            if (r10 != 0) goto L_0x001e
            if (r5 <= 0) goto L_0x001e
        L_0x0016:
            if (r5 != r1) goto L_0x0019
            r2 = 1
        L_0x0019:
            com.google.common.base.Preconditions.checkState(r2)
            return r5
        L_0x001d:
            r5 = 0
        L_0x001e:
            java.util.List r0 = r6.A01
            if (r0 == 0) goto L_0x002c
            int r0 = A00(r8, r0, r9, r10)
            int r5 = r5 + r0
            if (r10 != 0) goto L_0x002c
            if (r5 <= 0) goto L_0x002c
            goto L_0x0016
        L_0x002c:
            int r3 = r8.size()
            X.0VZ r0 = r6.A07
            int r4 = r0.A04(r8, r9, r10)
            r2 = r3
        L_0x0037:
            int r0 = r3 + r4
            if (r2 >= r0) goto L_0x0049
            X.0Vm r1 = r6.A09
            java.lang.Object r0 = r8.get(r2)
            X.0eD r0 = (X.C07820eD) r0
            r1.A01(r0)
            int r2 = r2 + 1
            goto L_0x0037
        L_0x0049:
            X.0Vc r3 = r6.A08
            int r2 = r3.A00
            r0 = 0
            if (r2 < r4) goto L_0x0051
            r0 = 1
        L_0x0051:
            com.google.common.base.Preconditions.checkState(r0)
            int r2 = r2 - r4
            r3.A00 = r2
            int r4 = r4 + r5
            return r4
        L_0x0059:
            r0 = r7
            X.0VY r0 = (X.AnonymousClass0VY) r0
            java.util.PriorityQueue r0 = r0.A04
            int r0 = A00(r8, r0, r9, r10)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VZ.A04(java.util.List, com.google.common.base.Predicate, boolean):int");
    }

    public C07820eD A05(Predicate predicate) {
        C07820eD r0;
        C07820eD r02;
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r2 = (C04610Vk) this;
            PriorityQueue priorityQueue = r2.A02;
            if (priorityQueue == null || (r0 = (C07820eD) AnonymousClass0j4.A05(priorityQueue, predicate, null)) == null) {
                List list = r2.A01;
                return (list == null || (r02 = (C07820eD) AnonymousClass0j4.A05(list, predicate, null)) == null) ? r2.A07.A05(predicate) : r02;
            }
        } else {
            AnonymousClass0VY r22 = (AnonymousClass0VY) this;
            r0 = (C07820eD) AnonymousClass0j4.A05(r22.A04, predicate, null);
            if (r0 == null) {
                return (C07820eD) AnonymousClass0j4.A05(r22.A05, predicate, null);
            }
        }
        return r0;
    }

    public void A06() {
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r3 = (C04610Vk) this;
            if (r3.A03) {
                r3.A03 = false;
                AnonymousClass0VZ r2 = r3.A07;
                List list = r2.A00;
                if (list == null) {
                    r2.A00 = new ArrayList();
                } else {
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        if (((WeakReference) it.next()).get() == null) {
                            it.remove();
                        }
                    }
                }
                r2.A00.add(new WeakReference(r3));
                r3.A07.A06();
            }
        }
    }

    public void A07() {
        C07820eD r1;
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r3 = (C04610Vk) this;
            if (r3.A02 != null) {
                while (true) {
                    C04540Vc r0 = r3.A08;
                    int i = r0.A00;
                    int i2 = r0.A01;
                    boolean z = false;
                    if (i < i2) {
                        z = true;
                    }
                    if (!z || (r1 = (C07820eD) r3.A02.poll()) == null) {
                        break;
                    }
                    r3.A08.A00();
                    r3.A09.A00(r1);
                    r3.A07.A0A(r1);
                }
            }
            r3.A07.A07();
        }
    }

    public void A08(C07820eD r10) {
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r5 = (C04610Vk) this;
            if (r5.A0C.compareTo((Enum) C04620Vl.SHUTTING_DOWN) < 0) {
                if (!r5.A05 && C04610Vk.A01(r5) + r5.A09.A00 >= r5.A06) {
                    r5.A05 = true;
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(r5);
                    r5.A0D(arrayList);
                    ArrayList arrayList2 = new ArrayList();
                    ArrayList arrayList3 = new ArrayList();
                    for (Map.Entry entry : r5.A0A.A03.entrySet()) {
                        ArrayList arrayList4 = arrayList3;
                        if (arrayList.contains(((C07820eD) entry.getKey()).AZE())) {
                            arrayList4 = arrayList2;
                        }
                        arrayList4.add(new ImmutableEntry(((C07820eD) entry.getKey()).C4G(), entry.getValue()));
                    }
                    Pair create = Pair.create(arrayList2, arrayList3);
                    String str = r5.A0B;
                    int i = r5.A08.A01;
                    List list = (List) create.first;
                    ArrayList arrayList5 = new ArrayList(r5.A09.A00 + C04610Vk.A01(r5));
                    PriorityQueue priorityQueue = r5.A09.A01;
                    Preconditions.checkNotNull(priorityQueue);
                    C04610Vk.A02(arrayList5, Collections.unmodifiableCollection(priorityQueue));
                    PriorityQueue priorityQueue2 = r5.A02;
                    if (priorityQueue2 != null) {
                        C04610Vk.A02(arrayList5, priorityQueue2);
                    }
                    List list2 = r5.A01;
                    if (list2 != null) {
                        C04610Vk.A02(arrayList5, list2);
                    }
                    C179868Tg.A00("Combined Thread Pool Full", null, AnonymousClass8YV.A01(str, i, list, arrayList5, (List) create.second));
                }
                r5.A07.A08(r10);
                return;
            }
            throw new RejectedExecutionException(StringFormatUtil.formatStrLocaleSafe("Task %s rejected because %s is no longer running (%s).", r10.C4H(), r10.AZE().A0B, r5.A0C));
        }
    }

    public void A09(C07820eD r4) {
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r1 = (C04610Vk) this;
            r1.A08.A00();
            r1.A07.A09(r4);
            return;
        }
        AnonymousClass0VY r12 = (AnonymousClass0VY) this;
        r12.A02.A00();
        r12.A03.put(r4, Long.valueOf(SystemClock.uptimeMillis()));
    }

    public void A0A(C07820eD r6) {
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r3 = (C04610Vk) this;
            boolean z = false;
            if (0 != 0) {
                if (r3.A01 == null) {
                    r3.A01 = new ArrayList();
                }
                r3.A01.add(r6);
                C010708t.A0X(4);
                return;
            }
            C04540Vc r4 = r3.A08;
            if (r4.A00 < r4.A01) {
                z = true;
            }
            if (z) {
                r4.A00();
                PriorityQueue priorityQueue = r3.A02;
                if (priorityQueue != null && !priorityQueue.isEmpty()) {
                    r3.A02.offer(r6);
                    r6 = (C07820eD) r3.A02.poll();
                }
                r3.A09.A00(r6);
                r3.A07.A0A(r6);
                return;
            }
            PriorityQueue priorityQueue2 = r3.A09.A01;
            if (priorityQueue2 != null) {
                C07820eD r1 = (C07820eD) priorityQueue2.peek();
                if (r1 != null && C04530Vb.A00(r6, r1) < 0) {
                    Preconditions.checkState(r3.A0G(r1));
                    r3.A09.A00(r6);
                    r3.A08.A00();
                    r3.A07.A0A(r6);
                    r6 = r1;
                }
                if (r3.A02 == null) {
                    r3.A02 = new PriorityQueue(16, C04530Vb.A00);
                }
                r3.A02.offer(r6);
                return;
            }
            throw new IllegalStateException("Peek should not be called");
        }
        ((AnonymousClass0VY) this).A04.offer(r6);
    }

    public void A0B(C07820eD r7) {
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r4 = (C04610Vk) this;
            C04540Vc r3 = r4.A08;
            int i = r3.A00;
            boolean z = false;
            if (i >= 1) {
                z = true;
            }
            Preconditions.checkState(z);
            r3.A00 = i - 1;
            r4.A07.A0B(r7);
            if (r4.A00 != null && C04610Vk.A03(r4)) {
                r4.A0C = C04620Vl.TERMINATED;
                r4.A00.A03();
                return;
            }
            return;
        }
        AnonymousClass0VY r5 = (AnonymousClass0VY) this;
        C04540Vc r42 = r5.A02;
        int i2 = r42.A00;
        boolean z2 = false;
        if (i2 >= 1) {
            z2 = true;
        }
        Preconditions.checkState(z2);
        r42.A00 = i2 - 1;
        r5.A03.remove(r7);
    }

    public void A0C(C07820eD r5) {
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r1 = (C04610Vk) this;
            r1.A09.A01(r5);
            r1.A07.A0C(r5);
            return;
        }
        AnonymousClass0VY r3 = (AnonymousClass0VY) this;
        int i = r3.A00;
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkState(z);
        r3.A00 = i - 1;
        r3.A02.A00();
        r3.A03.put(r5, Long.valueOf(SystemClock.uptimeMillis()));
    }

    public boolean A0E() {
        if (this instanceof AnonymousClass0VY) {
            return false;
        }
        C04610Vk r2 = (C04610Vk) this;
        if (r2.A0C.compareTo((Enum) C04620Vl.SHUTTING_DOWN) >= 0) {
            return true;
        }
        return r2.A07.A0E();
    }

    public boolean A0F(C07820eD r4) {
        if (!(this instanceof AnonymousClass0VY)) {
            C04610Vk r1 = (C04610Vk) this;
            if (!r1.A07.A0F(r4) || 0 != 0) {
                return false;
            }
            C04540Vc r0 = r1.A08;
            return r0.A00 < r0.A01;
        }
        C04540Vc r02 = ((AnonymousClass0VY) this).A02;
        return r02.A00 < r02.A01;
    }

    public boolean A0G(C07820eD r5) {
        if (this instanceof AnonymousClass0VY) {
            return ((AnonymousClass0VY) this).A04.remove(r5);
        }
        C04610Vk r1 = (C04610Vk) this;
        PriorityQueue priorityQueue = r1.A02;
        if (priorityQueue != null && priorityQueue.remove(r5)) {
            return true;
        }
        List list = r1.A01;
        if (list != null && list.remove(r5)) {
            return true;
        }
        if (!r1.A07.A0G(r5)) {
            return false;
        }
        r1.A09.A01(r5);
        C04540Vc r2 = r1.A08;
        int i = r2.A00;
        boolean z = false;
        if (i >= 1) {
            z = true;
        }
        Preconditions.checkState(z);
        r2.A00 = i - 1;
        return true;
    }

    public void A0D(List list) {
        List list2 = this.A00;
        if (list2 != null) {
            Iterator it = list2.iterator();
            while (it.hasNext()) {
                C04610Vk r0 = (C04610Vk) ((WeakReference) it.next()).get();
                if (r0 == null) {
                    it.remove();
                } else {
                    list.add(r0);
                    r0.A0D(list);
                }
            }
        }
    }

    public static int A00(List list, Collection collection, Predicate predicate, boolean z) {
        Iterator it = collection.iterator();
        int i = 0;
        while (it.hasNext()) {
            C07820eD r1 = (C07820eD) it.next();
            if (predicate.apply(r1)) {
                i++;
                list.add(r1);
                it.remove();
                if (!z) {
                    break;
                }
            }
        }
        return i;
    }
}
