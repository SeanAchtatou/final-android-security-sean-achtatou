package X;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.common.banner.BasicBannerNotificationView;

/* renamed from: X.0xa  reason: invalid class name and case insensitive filesystem */
public final class C16670xa extends C32501lr {
    public AnonymousClass0UN A00;
    private final Context A01;

    private C16670xa(AnonymousClass1XY r3, Context context) {
        super("SystemMuteWarningNotification");
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A01 = context;
    }

    public static final C16670xa A00(AnonymousClass1XY r2) {
        return new C16670xa(r2, AnonymousClass1YA.A00(r2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View B90(ViewGroup viewGroup) {
        BasicBannerNotificationView basicBannerNotificationView = (BasicBannerNotificationView) ((LayoutInflater) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AhI, this.A00)).inflate(2132410497, viewGroup, false);
        String string = this.A01.getString(2131833672);
        C46862Se r3 = new C46862Se();
        r3.A07 = string;
        r3.A03 = new ColorDrawable(AnonymousClass01R.A00(this.A01, 2132082843));
        basicBannerNotificationView.A0T(r3.A00());
        basicBannerNotificationView.setOnClickListener(new C46872Sf(this));
        return basicBannerNotificationView;
    }

    public void onResume() {
        if (((C15230uw) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ACc, this.A00)).A05() || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((AnonymousClass3AM) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AaY, this.A00)).A00)).Aem(282535833634454L)) {
            this.A00.A05(this);
        } else {
            this.A00.A06(this);
        }
    }
}
