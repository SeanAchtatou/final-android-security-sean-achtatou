package X;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: X.1TH  reason: invalid class name */
public final class AnonymousClass1TH {
    private static final C31381jb A04 = new AnonymousClass10V();
    private AnonymousClass0UN A00;
    public final C17350yl A01;
    private final AnonymousClass0VL A02;
    private final Executor A03;

    public static final AnonymousClass1TH A00(AnonymousClass1XY r1) {
        return new AnonymousClass1TH(r1);
    }

    public C34801qC A01(List list) {
        List list2 = list;
        if (this.A01.A0E()) {
            return ((C76243lC) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B2l, this.A00)).A01(new C76233lB(list, A04, ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A01.A00)).B4C(845451442192531L), true));
        }
        return ((C406822n) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A4R, this.A00)).A03(new C49522cQ(list2, A04, AnonymousClass230.A0E, C627032z.A01, true));
    }

    public AnonymousClass1TH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
        this.A03 = AnonymousClass0UX.A0U(r3);
        this.A02 = AnonymousClass0UX.A0Q(r3);
        this.A01 = C17350yl.A00(r3);
    }

    public void A02(List list, C05270Yh r7) {
        C34801qC A012;
        Preconditions.checkNotNull(list);
        Preconditions.checkNotNull(r7);
        if (!this.A01.A0E()) {
            C31381jb r4 = A04;
            AnonymousClass230 r1 = AnonymousClass230.A0E;
            Comparator comparator = C627032z.A01;
            AnonymousClass21D A013 = ((C406822n) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A4R, this.A00)).A00.A01(r1);
            if (A013 == null) {
                A012 = null;
            } else {
                A012 = C406822n.A01(A013, C49532cR.A01(list, r4), r4, comparator, C49532cR.A00(A013));
            }
            if (A012 != null) {
                list.size();
                r7.Bqf(C627032z.A01(A012));
                return;
            }
        }
        list.size();
        ListenableFuture CIF = this.A02.CIF(new AnonymousClass333(this, list));
        C05350Yp.A08(CIF, r7, this.A03);
        AnonymousClass1G0.A00(CIF, r7);
    }
}
