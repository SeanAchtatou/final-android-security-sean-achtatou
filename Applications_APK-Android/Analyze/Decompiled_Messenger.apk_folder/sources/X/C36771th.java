package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1th  reason: invalid class name and case insensitive filesystem */
public final class C36771th extends C36471tB {
    public static final Class A04 = C36771th.class;
    private static volatile C36771th A05;
    public final AnonymousClass06A A00;
    public final AnonymousClass18M A01;
    public final AnonymousClass18N A02;
    public final C04310Tq A03;

    private C36771th(AnonymousClass18N r2, AnonymousClass06A r3, C04310Tq r4, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, AnonymousClass18M r6) {
        super(12, deprecatedAnalyticsLogger);
        this.A02 = r2;
        this.A00 = r3;
        this.A03 = r4;
        this.A01 = r6;
    }

    public static final C36771th A00(AnonymousClass1XY r8) {
        if (A05 == null) {
            synchronized (C36771th.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        AnonymousClass18N A003 = AnonymousClass18N.A00(applicationInjector);
                        AnonymousClass06A A0A = AnonymousClass067.A0A(applicationInjector);
                        C04310Tq A042 = C10580kT.A04(applicationInjector);
                        C12420pJ.A00(applicationInjector);
                        A05 = new C36771th(A003, A0A, A042, C06920cI.A00(applicationInjector), AnonymousClass18M.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A01(C36771th r2) {
        C010708t.A05(A04, "Malformed StoredProcedureResponse");
        for (C22086AoB aoB : r2.A01) {
            synchronized (aoB) {
                C22086AoB.A07(aoB, AnonymousClass07B.A0N);
            }
        }
    }
}
