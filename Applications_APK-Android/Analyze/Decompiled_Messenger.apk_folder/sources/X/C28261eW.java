package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1eW  reason: invalid class name and case insensitive filesystem */
public abstract class C28261eW extends C28271eX {
    public C182811d _currToken;
    public C182811d _lastClearedToken;

    public static final String _getCharDesc(int i) {
        StringBuilder sb;
        char c = (char) i;
        if (Character.isISOControl(c)) {
            return AnonymousClass08S.A0A("(CTRL-CHAR, code ", i, ")");
        }
        if (i > 255) {
            sb = new StringBuilder();
            sb.append("'");
            sb.append(c);
            sb.append("' (code ");
            sb.append(i);
            sb.append(" / 0x");
            sb.append(Integer.toHexString(i));
        } else {
            sb = new StringBuilder();
            sb.append("'");
            sb.append(c);
            sb.append("' (code ");
            sb.append(i);
        }
        sb.append(")");
        return sb.toString();
    }

    public abstract void _handleEOF();

    public void _throwInvalidSpace(int i) {
        _reportError(AnonymousClass08S.A0P("Illegal character (", _getCharDesc((char) i), "): only regular white space (\\r, \\n, \\t) is allowed between tokens"));
    }

    public abstract void close();

    public abstract byte[] getBinaryValue(C10350jx r1);

    public abstract String getCurrentName();

    public abstract String getText();

    public abstract char[] getTextCharacters();

    public abstract int getTextLength();

    public abstract int getTextOffset();

    public abstract boolean hasTextCharacters();

    public abstract C182811d nextToken();

    public char _handleUnrecognizedCharacterEscape(char c) {
        if (isEnabled(C10390k1.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER) || (c == '\'' && isEnabled(C10390k1.ALLOW_SINGLE_QUOTES))) {
            return c;
        }
        _reportError(AnonymousClass08S.A0J("Unrecognized character escape ", _getCharDesc(c)));
        return c;
    }

    public void _reportInvalidEOF(String str) {
        _reportError(AnonymousClass08S.A0J("Unexpected end-of-input", str));
    }

    public void _reportUnexpectedChar(int i, String str) {
        String A0P = AnonymousClass08S.A0P("Unexpected character (", _getCharDesc(i), ")");
        if (str != null) {
            A0P = AnonymousClass08S.A0P(A0P, ": ", str);
        }
        _reportError(A0P);
    }

    public void _throwUnquotedSpace(int i, String str) {
        if (!isEnabled(C10390k1.ALLOW_UNQUOTED_CONTROL_CHARS) || i >= 32) {
            _reportError(AnonymousClass08S.A0S("Illegal unquoted character (", _getCharDesc((char) i), "): has to be escaped using backslash to be included in ", str));
        }
    }

    public void clearCurrentToken() {
        C182811d r0 = this._currToken;
        if (r0 != null) {
            this._lastClearedToken = r0;
            this._currToken = null;
        }
    }

    public C182811d getCurrentToken() {
        return this._currToken;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getValueAsBoolean(boolean r4) {
        /*
            r3 = this;
            X.11d r0 = r3._currToken
            if (r0 == 0) goto L_0x0011
            int[] r1 = X.C29601gg.$SwitchMap$com$fasterxml$jackson$core$JsonToken
            int r0 = r0.ordinal()
            r0 = r1[r0]
            r1 = 0
            r2 = 1
            switch(r0) {
                case 5: goto L_0x0032;
                case 6: goto L_0x0031;
                case 7: goto L_0x0039;
                case 8: goto L_0x0039;
                case 9: goto L_0x0012;
                case 10: goto L_0x0021;
                default: goto L_0x0011;
            }
        L_0x0011:
            return r4
        L_0x0012:
            java.lang.Object r1 = r3.getEmbeddedObject()
            boolean r0 = r1 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x0021
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r0 = r1.booleanValue()
            return r0
        L_0x0021:
            java.lang.String r0 = r3.getText()
            java.lang.String r1 = r0.trim()
            java.lang.String r0 = "true"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0011
        L_0x0031:
            return r2
        L_0x0032:
            int r0 = r3.getIntValue()
            if (r0 == 0) goto L_0x0039
            r1 = 1
        L_0x0039:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28261eW.getValueAsBoolean(boolean):boolean");
    }

    public double getValueAsDouble(double d) {
        C182811d r0 = this._currToken;
        if (r0 != null) {
            switch (C29601gg.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r0.ordinal()]) {
                case 5:
                case AnonymousClass1Y3.A02:
                    return getDoubleValue();
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    return 1.0d;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case 8:
                    return 0.0d;
                case Process.SIGKILL:
                    Object embeddedObject = getEmbeddedObject();
                    if (embeddedObject instanceof Number) {
                        return ((Number) embeddedObject).doubleValue();
                    }
                    break;
                case AnonymousClass1Y3.A01:
                    String text = getText();
                    if (text != null) {
                        String trim = text.trim();
                        if (trim.length() != 0) {
                            try {
                                return C29491gV.parseDouble(trim);
                            } catch (NumberFormatException unused) {
                                break;
                            }
                        }
                    }
                    break;
            }
        }
        return d;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public int getValueAsInt(int i) {
        C182811d r0 = this._currToken;
        if (r0 != null) {
            switch (C29601gg.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r0.ordinal()]) {
                case 5:
                case AnonymousClass1Y3.A02:
                    return getIntValue();
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    return 1;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case 8:
                    return 0;
                case Process.SIGKILL:
                    Object embeddedObject = getEmbeddedObject();
                    if (embeddedObject instanceof Number) {
                        return ((Number) embeddedObject).intValue();
                    }
                    break;
                case AnonymousClass1Y3.A01:
                    return C29491gV.parseAsInt(getText(), i);
            }
        }
        return i;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public long getValueAsLong(long j) {
        C182811d r0 = this._currToken;
        if (r0 != null) {
            switch (C29601gg.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r0.ordinal()]) {
                case 5:
                case AnonymousClass1Y3.A02:
                    return getLongValue();
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    return 1;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case 8:
                    return 0;
                case Process.SIGKILL:
                    Object embeddedObject = getEmbeddedObject();
                    if (embeddedObject instanceof Number) {
                        return ((Number) embeddedObject).longValue();
                    }
                    break;
                case AnonymousClass1Y3.A01:
                    return C29491gV.parseAsLong(getText(), j);
            }
        }
        return j;
    }

    public String getValueAsString(String str) {
        C182811d r1 = this._currToken;
        if (r1 == C182811d.VALUE_STRING || (r1 != null && r1 != C182811d.VALUE_NULL && r1.isScalarValue())) {
            return getText();
        }
        return str;
    }

    public boolean hasCurrentToken() {
        if (this._currToken != null) {
            return true;
        }
        return false;
    }

    public C28271eX skipChildren() {
        C182811d r1 = this._currToken;
        if (r1 != C182811d.START_OBJECT && r1 != C182811d.START_ARRAY) {
            return this;
        }
        int i = 1;
        while (true) {
            C182811d nextToken = nextToken();
            if (nextToken == null) {
                _handleEOF();
                return this;
            }
            int i2 = C29601gg.$SwitchMap$com$fasterxml$jackson$core$JsonToken[nextToken.ordinal()];
            if (i2 == 1 || i2 == 2) {
                i++;
            } else if ((i2 == 3 || i2 == 4) && i - 1 == 0) {
                return this;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        if (r6 >= 0) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        X.C10350jx._reportInvalidBase64(r14, r1, 0, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        if (r7 < r5) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        r3 = r7 + 1;
        r2 = r12.charAt(r7);
        r1 = r14.decodeBase64Char(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        if (r1 >= 0) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0031, code lost:
        X.C10350jx._reportInvalidBase64(r14, r2, 1, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        r10 = (r6 << 6) | r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0038, code lost:
        if (r3 < r5) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        r8 = r3 + 1;
        r2 = r12.charAt(r3);
        r1 = r14.decodeBase64Char(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0048, code lost:
        if (r1 >= 0) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004a, code lost:
        if (r1 == -2) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        X.C10350jx._reportInvalidBase64(r14, r2, 2, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004f, code lost:
        if (r8 < r5) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0052, code lost:
        r2 = r8 + 1;
        r6 = r12.charAt(r8);
        r3 = r14._paddingChar;
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005b, code lost:
        if (r6 != r3) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005d, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005e, code lost:
        if (r0 != false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0060, code lost:
        X.C10350jx._reportInvalidBase64(r14, r6, 3, X.AnonymousClass08S.A07("expected padding character '", r3, "'"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006b, code lost:
        r13.append(r10 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0071, code lost:
        r3 = (r10 << 6) | r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0074, code lost:
        if (r8 < r5) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0077, code lost:
        r2 = r8 + 1;
        r0 = r12.charAt(r8);
        r1 = r14.decodeBase64Char(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0081, code lost:
        if (r1 >= 0) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0083, code lost:
        if (r1 == -2) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0085, code lost:
        X.C10350jx._reportInvalidBase64(r14, r0, 3, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0088, code lost:
        r13.appendTwoBytes(r3 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x008f, code lost:
        r13.appendThreeBytes((r3 << 6) | r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0099, code lost:
        if (r14._usesPadding != false) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009b, code lost:
        r13.append(r10 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a0, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a1, code lost:
        r1 = new java.lang.IllegalArgumentException("Unexpected end-of-String in base64 content");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a9, code lost:
        r1 = new java.lang.IllegalArgumentException("Unexpected end-of-String in base64 content");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b3, code lost:
        if (r14._usesPadding != false) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b5, code lost:
        r13.appendTwoBytes(r3 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ba, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00bb, code lost:
        r1 = new java.lang.IllegalArgumentException("Unexpected end-of-String in base64 content");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c3, code lost:
        r1 = new java.lang.IllegalArgumentException("Unexpected end-of-String in base64 content");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ca, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        r6 = r14.decodeBase64Char(r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void _decodeBase64(java.lang.String r12, X.A21 r13, X.C10350jx r14) {
        /*
            r11 = this;
            int r5 = r12.length()     // Catch:{ IllegalArgumentException -> 0x00cb }
            r4 = 0
            r2 = 0
        L_0x0006:
            if (r2 >= r5) goto L_0x00d3
        L_0x0008:
            int r7 = r2 + 1
            char r1 = r12.charAt(r2)     // Catch:{ IllegalArgumentException -> 0x00cb }
            if (r7 >= r5) goto L_0x00d3
            r0 = 32
            if (r1 <= r0) goto L_0x0015
            goto L_0x0017
        L_0x0015:
            r2 = r7
            goto L_0x0008
        L_0x0017:
            int r6 = r14.decodeBase64Char(r1)     // Catch:{ IllegalArgumentException -> 0x00cb }
            r9 = 0
            if (r6 >= 0) goto L_0x0021
            X.C10350jx._reportInvalidBase64(r14, r1, r4, r9)     // Catch:{ IllegalArgumentException -> 0x00cb }
        L_0x0021:
            if (r7 < r5) goto L_0x0025
            goto L_0x00c3
        L_0x0025:
            int r3 = r7 + 1
            char r2 = r12.charAt(r7)     // Catch:{ IllegalArgumentException -> 0x00cb }
            int r1 = r14.decodeBase64Char(r2)     // Catch:{ IllegalArgumentException -> 0x00cb }
            if (r1 >= 0) goto L_0x0035
            r0 = 1
            X.C10350jx._reportInvalidBase64(r14, r2, r0, r9)     // Catch:{ IllegalArgumentException -> 0x00cb }
        L_0x0035:
            int r10 = r6 << 6
            r10 = r10 | r1
            if (r3 < r5) goto L_0x003b
            goto L_0x0097
        L_0x003b:
            int r8 = r3 + 1
            char r2 = r12.charAt(r3)     // Catch:{ IllegalArgumentException -> 0x00cb }
            int r1 = r14.decodeBase64Char(r2)     // Catch:{ IllegalArgumentException -> 0x00cb }
            r7 = 3
            r6 = -2
            r0 = 2
            if (r1 >= 0) goto L_0x0071
            if (r1 == r6) goto L_0x004f
            X.C10350jx._reportInvalidBase64(r14, r2, r0, r9)     // Catch:{ IllegalArgumentException -> 0x00cb }
        L_0x004f:
            if (r8 < r5) goto L_0x0052
            goto L_0x00a9
        L_0x0052:
            int r2 = r8 + 1
            char r6 = r12.charAt(r8)     // Catch:{ IllegalArgumentException -> 0x00cb }
            char r3 = r14._paddingChar     // Catch:{ IllegalArgumentException -> 0x00cb }
            r0 = 0
            if (r6 != r3) goto L_0x005e
            r0 = 1
        L_0x005e:
            if (r0 != 0) goto L_0x006b
            java.lang.String r1 = "expected padding character '"
            java.lang.String r0 = "'"
            java.lang.String r0 = X.AnonymousClass08S.A07(r1, r3, r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            X.C10350jx._reportInvalidBase64(r14, r6, r7, r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
        L_0x006b:
            int r0 = r10 >> 4
            r13.append(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            goto L_0x0006
        L_0x0071:
            int r3 = r10 << 6
            r3 = r3 | r1
            if (r8 < r5) goto L_0x0077
            goto L_0x00b1
        L_0x0077:
            int r2 = r8 + 1
            char r0 = r12.charAt(r8)     // Catch:{ IllegalArgumentException -> 0x00cb }
            int r1 = r14.decodeBase64Char(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            if (r1 >= 0) goto L_0x008f
            if (r1 == r6) goto L_0x0088
            X.C10350jx._reportInvalidBase64(r14, r0, r7, r9)     // Catch:{ IllegalArgumentException -> 0x00cb }
        L_0x0088:
            int r0 = r3 >> 2
            r13.appendTwoBytes(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            goto L_0x0006
        L_0x008f:
            int r0 = r3 << 6
            r0 = r0 | r1
            r13.appendThreeBytes(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            goto L_0x0006
        L_0x0097:
            boolean r0 = r14._usesPadding     // Catch:{ IllegalArgumentException -> 0x00cb }
            if (r0 != 0) goto L_0x00a1
            int r0 = r10 >> 4
            r13.append(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            return
        L_0x00a1:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x00cb }
            java.lang.String r0 = "Unexpected end-of-String in base64 content"
            r1.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            goto L_0x00ca
        L_0x00a9:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x00cb }
            java.lang.String r0 = "Unexpected end-of-String in base64 content"
            r1.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            goto L_0x00ca
        L_0x00b1:
            boolean r0 = r14._usesPadding     // Catch:{ IllegalArgumentException -> 0x00cb }
            if (r0 != 0) goto L_0x00bb
            int r0 = r3 >> 2
            r13.appendTwoBytes(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            return
        L_0x00bb:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x00cb }
            java.lang.String r0 = "Unexpected end-of-String in base64 content"
            r1.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
            goto L_0x00ca
        L_0x00c3:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IllegalArgumentException -> 0x00cb }
            java.lang.String r0 = "Unexpected end-of-String in base64 content"
            r1.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x00cb }
        L_0x00ca:
            throw r1     // Catch:{ IllegalArgumentException -> 0x00cb }
        L_0x00cb:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            r11._reportError(r0)
        L_0x00d3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28261eW._decodeBase64(java.lang.String, X.A21, X.0jx):void");
    }

    public final void _reportError(String str) {
        throw _constructError(str);
    }

    public C182811d nextValue() {
        C182811d nextToken = nextToken();
        if (nextToken == C182811d.FIELD_NAME) {
            return nextToken();
        }
        return nextToken;
    }

    public C11780nw version() {
        return C1926190k.versionFor(getClass());
    }

    public C28261eW() {
    }

    public C28261eW(int i) {
        super(i);
    }
}
