package X;

import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.1e8  reason: invalid class name and case insensitive filesystem */
public interface C28021e8 extends AutoCloseable {
    public static final C28021e8 A00 = new AnonymousClass7W8();

    C29621gi BLn();

    C17920zh BLo();

    ThreadSummary BLt();

    void close();
}
