package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import java.util.Set;

@UserScoped
/* renamed from: X.0tP  reason: invalid class name and case insensitive filesystem */
public final class C14480tP implements CallerContextable {
    private static C05540Zi A03 = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.cache.ArchiveThreadManager";
    public final BlueServiceOperationFactory A00;
    public final C189216c A01;
    public final Set A02 = new C05180Xy();

    public static final C14480tP A00(AnonymousClass1XY r4) {
        C14480tP r0;
        synchronized (C14480tP.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C14480tP((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (C14480tP) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    private C14480tP(AnonymousClass1XY r2) {
        this.A01 = C189216c.A02(r2);
        this.A00 = C30111hV.A00(r2);
    }
}
