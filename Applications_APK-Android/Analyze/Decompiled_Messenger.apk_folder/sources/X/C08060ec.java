package X;

import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.0ec  reason: invalid class name and case insensitive filesystem */
public final class C08060ec extends C07160cj implements Serializable {
    public final int bytes;
    public final MessageDigest prototype;
    public final boolean supportsClone;
    private final String toString;

    public String toString() {
        return this.toString;
    }

    public Object writeReplace() {
        return new C22743BAn(this.prototype.getAlgorithm(), this.bytes, this.toString);
    }

    public static MessageDigest A00(String str) {
        try {
            return MessageDigest.getInstance(str);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0016, code lost:
        if (r5 > r2) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C08060ec(java.lang.String r4, int r5, java.lang.String r6) {
        /*
            r3 = this;
            r3.<init>()
            com.google.common.base.Preconditions.checkNotNull(r6)
            r3.toString = r6
            java.security.MessageDigest r0 = A00(r4)
            r3.prototype = r0
            int r2 = r0.getDigestLength()
            r0 = 4
            if (r5 < r0) goto L_0x0018
            r1 = 1
            if (r5 <= r2) goto L_0x0019
        L_0x0018:
            r1 = 0
        L_0x0019:
            java.lang.String r0 = "bytes (%s) must be >= 4 and < %s"
            com.google.common.base.Preconditions.checkArgument(r1, r0, r5, r2)
            r3.bytes = r5
            java.security.MessageDigest r0 = r3.prototype
            r0.clone()     // Catch:{ CloneNotSupportedException -> 0x0026 }
            goto L_0x0028
        L_0x0026:
            r0 = 0
            goto L_0x0029
        L_0x0028:
            r0 = 1
        L_0x0029:
            r3.supportsClone = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08060ec.<init>(java.lang.String, int, java.lang.String):void");
    }

    public C08060ec(String str, String str2) {
        boolean z;
        MessageDigest A00 = A00(str);
        this.prototype = A00;
        this.bytes = A00.getDigestLength();
        Preconditions.checkNotNull(str2);
        this.toString = str2;
        try {
            this.prototype.clone();
            z = true;
        } catch (CloneNotSupportedException unused) {
            z = false;
        }
        this.supportsClone = z;
    }
}
