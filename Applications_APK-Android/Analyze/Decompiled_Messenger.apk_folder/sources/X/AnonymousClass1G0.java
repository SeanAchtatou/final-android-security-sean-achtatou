package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1G0  reason: invalid class name */
public final class AnonymousClass1G0 {
    public final C05270Yh A00;
    public final ListenableFuture A01;

    public static AnonymousClass1G0 A00(ListenableFuture listenableFuture, C05270Yh r2) {
        return new AnonymousClass1G0(listenableFuture, r2);
    }

    public void A01(boolean z) {
        this.A00.dispose();
        this.A01.cancel(z);
    }

    public AnonymousClass1G0(ListenableFuture listenableFuture, C05270Yh r2) {
        this.A01 = listenableFuture;
        this.A00 = r2;
    }
}
