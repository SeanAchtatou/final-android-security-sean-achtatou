package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Km  reason: invalid class name and case insensitive filesystem */
public final class C22241Km extends C17770zR {
    @Comparable(type = 3)
    public long A00;
    @Comparable(type = 3)
    public long A01;
    @Comparable(type = 13)
    public C25998CqM A02;
    public AnonymousClass0UN A03;
    @Comparable(type = 13)
    public C203989ja A04;
    @Comparable(type = 13)
    public C121605oW A05;
    @Comparable(type = 13)
    public C204019jd A06;
    @Comparable(type = 13)
    public CharSequence A07;
    @Comparable(type = 3)
    public boolean A08 = false;

    public C22241Km(Context context) {
        super("AccountRegPinRootComponent");
        this.A03 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
