package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;

/* renamed from: X.1FQ  reason: invalid class name */
public final class AnonymousClass1FQ implements AnonymousClass1FI {
    public static final AnonymousClass1FQ A00() {
        return new AnonymousClass1FQ();
    }

    public ImmutableMap get() {
        return ImmutableMap.of(new SubscribeTopic(TurboLoader.Locator.$const$string(94), 0), AnonymousClass1FP.ALWAYS);
    }
}
