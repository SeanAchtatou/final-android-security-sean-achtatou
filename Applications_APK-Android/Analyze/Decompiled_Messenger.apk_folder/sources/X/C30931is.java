package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1is  reason: invalid class name and case insensitive filesystem */
public final class C30931is {
    private static volatile C30931is A01;
    public AnonymousClass0UN A00;

    public static final C30931is A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C30931is.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C30931is(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v23, types: [X.1j9] */
    /* JADX WARN: Type inference failed for: r1v29, types: [X.1j9] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C31111jA A01(java.io.File r10, X.C31001iz r11) {
        /*
            r9 = this;
            int r1 = X.AnonymousClass1Y3.BBd
            X.0UN r0 = r9.A00
            r6 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1
            int r0 = r10.hashCode()
            r5 = 42991640(0x2900018, float:2.1158952E-37)
            r1.markerStart(r5, r0)
            int r1 = X.AnonymousClass1Y3.BBd
            X.0UN r0 = r9.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1
            int r0 = r10.hashCode()
            X.1Ew r2 = r1.withMarker(r5, r0)
            java.lang.String r1 = r10.toString()
            java.lang.String r0 = "path"
            r2.A08(r0, r1)
            java.lang.String r1 = r11.A03
            java.lang.String r0 = "name"
            r2.A08(r0, r1)
            r2.BK9()
            r2 = 1
            r4 = 2
            int r1 = X.AnonymousClass1Y3.AUL     // Catch:{ all -> 0x00f6 }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x00f6 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00f6 }
            X.1j1 r3 = (X.C31021j1) r3     // Catch:{ all -> 0x00f6 }
            int r1 = X.AnonymousClass1Y3.ACz     // Catch:{ all -> 0x00f6 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x00f6 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x00f6 }
            X.0bZ r2 = (X.C06480bZ) r2     // Catch:{ all -> 0x00f6 }
            X.1j2 r1 = new X.1j2     // Catch:{ all -> 0x00f6 }
            r1.<init>(r3, r10, r11)     // Catch:{ all -> 0x00f6 }
            r0 = -66671522(0xfffffffffc06ac5e, float:-2.7970552E36)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ all -> 0x00f6 }
            X.1j3 r3 = new X.1j3     // Catch:{ all -> 0x00f6 }
            int r2 = X.AnonymousClass1Y3.AN9     // Catch:{ all -> 0x00f6 }
            X.0UN r1 = r9.A00     // Catch:{ all -> 0x00f6 }
            r0 = 4
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x00f6 }
            com.facebook.storage.trash.FbTrashManager r0 = (com.facebook.storage.trash.FbTrashManager) r0     // Catch:{ all -> 0x00f6 }
            r3.<init>(r10, r0)     // Catch:{ all -> 0x00f6 }
            X.1j7 r8 = new X.1j7     // Catch:{ all -> 0x00f6 }
            r8.<init>(r3)     // Catch:{ all -> 0x00f6 }
            int r2 = X.AnonymousClass1Y3.ACz     // Catch:{ all -> 0x00f6 }
            X.0UN r1 = r9.A00     // Catch:{ all -> 0x00f6 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ all -> 0x00f6 }
            X.0bZ r2 = (X.C06480bZ) r2     // Catch:{ all -> 0x00f6 }
            X.0ly r1 = new X.0ly     // Catch:{ all -> 0x00f6 }
            r1.<init>(r8)     // Catch:{ all -> 0x00f6 }
            r0 = 526316242(0x1f5ef2d2, float:4.7211185E-20)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ all -> 0x00f6 }
            java.util.List r0 = r11.A04     // Catch:{ all -> 0x00f6 }
            if (r0 == 0) goto L_0x00d5
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00f6 }
            if (r0 != 0) goto L_0x00d5
            java.util.List r0 = r11.A04     // Catch:{ all -> 0x00f6 }
            if (r0 != 0) goto L_0x009d
            X.1j9 r1 = new X.1j9     // Catch:{ all -> 0x00f6 }
            java.util.List r0 = java.util.Collections.emptyList()     // Catch:{ all -> 0x00f6 }
            r1.<init>(r8, r0)     // Catch:{ all -> 0x00f6 }
            goto L_0x00d4
        L_0x009d:
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x00f6 }
            r3.<init>()     // Catch:{ all -> 0x00f6 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x00f6 }
        L_0x00a6:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x00f6 }
            if (r0 == 0) goto L_0x00cf
            java.lang.Object r1 = r7.next()     // Catch:{ all -> 0x00f6 }
            X.1iy r1 = (X.C30991iy) r1     // Catch:{ all -> 0x00f6 }
            boolean r0 = r1 instanceof X.C30981ix     // Catch:{ all -> 0x00f6 }
            if (r0 != 0) goto L_0x00c9
            r2 = 3
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x00f6 }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x00f6 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00f6 }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x00f6 }
            java.lang.String r1 = "Stash"
            java.lang.String r0 = "StashWithEvents can only be created with IStashEventListeners"
            r2.CGY(r1, r0)     // Catch:{ all -> 0x00f6 }
            goto L_0x00a6
        L_0x00c9:
            X.1ix r1 = (X.C30981ix) r1     // Catch:{ all -> 0x00f6 }
            r3.add(r1)     // Catch:{ all -> 0x00f6 }
            goto L_0x00a6
        L_0x00cf:
            X.1j9 r1 = new X.1j9     // Catch:{ all -> 0x00f6 }
            r1.<init>(r8, r3)     // Catch:{ all -> 0x00f6 }
        L_0x00d4:
            r8 = r1
        L_0x00d5:
            java.lang.String r7 = r11.A03     // Catch:{ all -> 0x00f6 }
            X.1jA r3 = new X.1jA     // Catch:{ all -> 0x00f6 }
            int r2 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x00f6 }
            X.0UN r1 = r9.A00     // Catch:{ all -> 0x00f6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r2, r1)     // Catch:{ all -> 0x00f6 }
            com.facebook.quicklog.QuickPerformanceLogger r0 = (com.facebook.quicklog.QuickPerformanceLogger) r0     // Catch:{ all -> 0x00f6 }
            r3.<init>(r7, r10, r8, r0)     // Catch:{ all -> 0x00f6 }
            X.0UN r0 = r9.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r0)
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1
            int r0 = r10.hashCode()
            r1.markerEnd(r5, r0, r4)
            return r3
        L_0x00f6:
            r3 = move-exception
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x010a }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x010a }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x010a }
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2     // Catch:{ all -> 0x010a }
            int r1 = r10.hashCode()     // Catch:{ all -> 0x010a }
            r0 = 3
            r2.markerEnd(r5, r1, r0)     // Catch:{ all -> 0x010a }
            throw r3     // Catch:{ all -> 0x010a }
        L_0x010a:
            r2 = move-exception
            int r1 = X.AnonymousClass1Y3.BBd
            X.0UN r0 = r9.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1
            int r0 = r10.hashCode()
            r1.markerEnd(r5, r0, r4)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30931is.A01(java.io.File, X.1iz):X.1jA");
    }

    private C30931is(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
