package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.service.model.FetchThreadListParams;

/* renamed from: X.0kf  reason: invalid class name and case insensitive filesystem */
public final class C10670kf implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new FetchThreadListParams(parcel);
    }

    public Object[] newArray(int i) {
        return new FetchThreadListParams[i];
    }
}
