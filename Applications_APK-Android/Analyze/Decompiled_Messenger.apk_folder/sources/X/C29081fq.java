package X;

import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.util.Collection;

/* renamed from: X.1fq  reason: invalid class name and case insensitive filesystem */
public final class C29081fq {
    public static void checkAndFixAccess(Member member) {
        AccessibleObject accessibleObject = (AccessibleObject) member;
        try {
            accessibleObject.setAccessible(true);
        } catch (SecurityException e) {
            if (!accessibleObject.isAccessible()) {
                Class<?> declaringClass = member.getDeclaringClass();
                throw new IllegalArgumentException("Can not access " + member + " (from class " + declaringClass.getName() + "; failed to set access: " + e.getMessage());
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object createInstance(java.lang.Class r5, boolean r6) {
        /*
            r0 = 0
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            java.lang.reflect.Constructor r1 = r5.getDeclaredConstructor(r0)     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            if (r6 == 0) goto L_0x000d
            checkAndFixAccess(r1)     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            goto L_0x003e
        L_0x000d:
            int r0 = r1.getModifiers()     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            boolean r0 = java.lang.reflect.Modifier.isPublic(r0)     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            if (r0 != 0) goto L_0x003e
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            java.lang.String r2 = "Default constructor for "
            java.lang.String r1 = r5.getName()     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            java.lang.String r0 = " is not accessible (non-public?): not allowed to try modify access via Reflection: can not instantiate type"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            r3.<init>(r0)     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
            throw r3     // Catch:{ NoSuchMethodException -> 0x003d, Exception -> 0x0029 }
        L_0x0029:
            r4 = move-exception
            java.lang.String r3 = "Failed to find default constructor of class "
            java.lang.String r2 = r5.getName()
            java.lang.String r1 = ", problem: "
            java.lang.String r0 = r4.getMessage()
            java.lang.String r0 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)
            unwrapAndThrowAsIAE(r4, r0)
        L_0x003d:
            r1 = 0
        L_0x003e:
            if (r1 == 0) goto L_0x005e
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0048 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ Exception -> 0x0048 }
            return r0
        L_0x0048:
            r4 = move-exception
            java.lang.String r3 = "Failed to instantiate class "
            java.lang.String r2 = r5.getName()
            java.lang.String r1 = ", problem: "
            java.lang.String r0 = r4.getMessage()
            java.lang.String r0 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)
            unwrapAndThrowAsIAE(r4, r0)
            r0 = 0
            return r0
        L_0x005e:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Class "
            java.lang.String r1 = r5.getName()
            java.lang.String r0 = " has no default (no arg) constructor"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29081fq.createInstance(java.lang.Class, boolean):java.lang.Object");
    }

    public static Class getOuterClass(Class cls) {
        try {
            if (cls.getEnclosingMethod() == null && !Modifier.isStatic(cls.getModifiers())) {
                return cls.getEnclosingClass();
            }
        } catch (NullPointerException | SecurityException unused) {
        }
        return null;
    }

    public static void _addSuperTypes(Class cls, Class cls2, Collection collection, boolean z) {
        if (cls != cls2 && cls != null && cls != Object.class) {
            if (z) {
                if (!collection.contains(cls)) {
                    collection.add(cls);
                } else {
                    return;
                }
            }
            for (Class<?> _addSuperTypes : cls.getInterfaces()) {
                _addSuperTypes(_addSuperTypes, cls2, collection, true);
            }
            _addSuperTypes(cls.getSuperclass(), cls2, collection, true);
        }
    }

    public static Class findClass(String str) {
        if (str.indexOf(46) < 0) {
            if ("int".equals(str)) {
                return Integer.TYPE;
            }
            if ("long".equals(str)) {
                return Long.TYPE;
            }
            if ("float".equals(str)) {
                return Float.TYPE;
            }
            if ("double".equals(str)) {
                return Double.TYPE;
            }
            if ("boolean".equals(str)) {
                return Boolean.TYPE;
            }
            if ("byte".equals(str)) {
                return Byte.TYPE;
            }
            if ("char".equals(str)) {
                return Character.TYPE;
            }
            if ("short".equals(str)) {
                return Short.TYPE;
            }
            if ("void".equals(str)) {
                return Void.TYPE;
            }
        }
        e = null;
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        if (contextClassLoader != null) {
            try {
                return Class.forName(str, true, contextClassLoader);
            } catch (Exception e) {
                e = e;
                while (e.getCause() != null) {
                    e = e.getCause();
                }
            }
        }
        try {
            return Class.forName(str);
        } catch (Exception e2) {
            e = e2;
            if (e == null) {
                while (e.getCause() != null) {
                    e = e.getCause();
                }
                e = e;
            }
            if (e instanceof RuntimeException) {
                throw e;
            }
            throw new ClassNotFoundException(e.getMessage(), e);
        }
    }

    public static boolean isJacksonStdImpl(Object obj) {
        if (obj != null) {
            Annotation annotation = obj.getClass().getAnnotation(JacksonStdImpl.class);
            boolean z = false;
            if (annotation != null) {
                z = true;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static Class wrapperType(Class cls) {
        if (cls == Integer.TYPE) {
            return Integer.class;
        }
        if (cls == Long.TYPE) {
            return Long.class;
        }
        if (cls == Boolean.TYPE) {
            return Boolean.class;
        }
        if (cls == Double.TYPE) {
            return Double.class;
        }
        if (cls == Float.TYPE) {
            return Float.class;
        }
        if (cls == Byte.TYPE) {
            return Byte.class;
        }
        if (cls == Short.TYPE) {
            return Short.class;
        }
        if (cls == Character.TYPE) {
            return Character.class;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0P("Class ", cls.getName(), " is not a primitive type"));
    }

    public static String canBeABeanType(Class cls) {
        if (cls.isAnnotation()) {
            return "annotation";
        }
        if (cls.isArray()) {
            return "array";
        }
        if (cls.isEnum()) {
            return "enum";
        }
        if (cls.isPrimitive()) {
            return "primitive";
        }
        return null;
    }

    public static void unwrapAndThrowAsIAE(Throwable th) {
        while (th.getCause() != null) {
            th = th.getCause();
        }
        String message = th.getMessage();
        if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else if (th instanceof Error) {
            throw ((Error) th);
        } else {
            throw new IllegalArgumentException(message, th);
        }
    }

    public static void unwrapAndThrowAsIAE(Throwable th, String str) {
        while (th.getCause() != null) {
            th = th.getCause();
        }
        if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else if (th instanceof Error) {
            throw ((Error) th);
        } else {
            throw new IllegalArgumentException(str, th);
        }
    }
}
