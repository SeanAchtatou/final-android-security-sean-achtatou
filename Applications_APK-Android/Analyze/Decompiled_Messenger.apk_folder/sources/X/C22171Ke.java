package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.NicknamesMap;
import com.facebook.messaging.model.threads.ThreadCustomization;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.ui.name.MessengerThreadNameViewData;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;
import java.util.Collection;

@UserScoped
/* renamed from: X.1Ke  reason: invalid class name and case insensitive filesystem */
public final class C22171Ke {
    private static C05540Zi A03;
    private final AnonymousClass0jE A00 = C05040Xk.A00();
    private final AnonymousClass16Z A01;
    private final C17270yd A02;

    public static MessengerThreadNameViewData A00(User user) {
        if (user == null) {
            return null;
        }
        String A09 = user.A09();
        return new MessengerThreadNameViewData(false, null, ImmutableList.of(A09), new ParticipantInfo(user.A0Q, A09, null, user.A0I), -1);
    }

    public static MessengerThreadNameViewData A01(ImmutableList immutableList) {
        return new MessengerThreadNameViewData(false, null, immutableList, null, -1);
    }

    public static final C22171Ke A02(AnonymousClass1XY r4) {
        C22171Ke r0;
        synchronized (C22171Ke.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C22171Ke((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (C22171Ke) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public MessengerThreadNameViewData A03(ThreadSummary threadSummary) {
        ThreadParticipant threadParticipant;
        long j;
        C005505z.A03("MessengerThreadNameViewDataFactory.getThreadNameViewData", 590693418);
        ParticipantInfo participantInfo = null;
        if (threadSummary == null) {
            C005505z.A00(-1722340207);
            return null;
        }
        try {
            if (ThreadKey.A0F(threadSummary.A0S)) {
                threadParticipant = C17270yd.A04(this.A02, threadSummary, C28711fF.TINCAN);
            } else {
                threadParticipant = C17270yd.A04(this.A02, threadSummary, C28711fF.ONE_TO_ONE);
            }
            if (threadParticipant != null) {
                participantInfo = threadParticipant.A04;
            }
            if (threadParticipant != null) {
                j = threadParticipant.A02;
            } else {
                j = -1;
            }
            return new MessengerThreadNameViewData(threadSummary.A07(), threadSummary.A0t, ImmutableList.copyOf((Collection) this.A02.A09(threadSummary)), participantInfo, j);
        } finally {
            C005505z.A00(-1802508269);
        }
    }

    public MessengerThreadNameViewData A04(ThreadSummary threadSummary) {
        ParticipantInfo participantInfo;
        ThreadCustomization threadCustomization;
        NicknamesMap nicknamesMap;
        if (threadSummary == null) {
            return null;
        }
        ThreadParticipant A04 = C17270yd.A04(this.A02, threadSummary, C28711fF.ONE_TO_ONE);
        if (!(A04 == null || (participantInfo = A04.A04) == null || (threadCustomization = threadSummary.A0e) == null || (nicknamesMap = threadCustomization.A00) == null)) {
            String A022 = nicknamesMap.A02(participantInfo.A00(), this.A00);
            if (A022 == null) {
                A022 = this.A01.A02(participantInfo);
            }
            if (A022 != null) {
                return new MessengerThreadNameViewData(false, null, ImmutableList.of(A022), participantInfo, -1);
            }
        }
        return A03(threadSummary);
    }

    private C22171Ke(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass16Z.A00(r2);
        this.A02 = C17270yd.A00(r2);
    }
}
