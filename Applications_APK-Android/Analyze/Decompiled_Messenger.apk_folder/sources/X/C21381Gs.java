package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1Gs  reason: invalid class name and case insensitive filesystem */
public final class C21381Gs extends Enum {
    private static final /* synthetic */ C21381Gs[] A00;
    public static final C21381Gs A01;
    public static final C21381Gs A02;
    public static final C21381Gs A03;
    public static final C21381Gs A04;
    public static final C21381Gs A05;
    public static final C21381Gs A06;
    public static final C21381Gs A07;
    public static final C21381Gs A08;
    public static final C21381Gs A09;
    public static final C21381Gs A0A;
    public static final C21381Gs A0B;
    public static final C21381Gs A0C;
    public static final C21381Gs A0D;
    public static final C21381Gs A0E;
    public static final C21381Gs A0F;
    public static final C21381Gs A0G;
    public static final C21381Gs A0H;
    public static final C21381Gs A0I;
    public static final C21381Gs A0J;
    public static final C21381Gs A0K;
    public static final C21381Gs A0L;
    public static final C21381Gs A0M;
    public static final C21381Gs A0N;
    public static final C21381Gs A0O;
    public static final C21381Gs A0P;
    public static final C21381Gs A0Q;
    public static final C21381Gs A0R;
    public static final C21381Gs A0S;
    public static final C21381Gs A0T;
    public static final C21381Gs A0U;
    public static final C21381Gs A0V;
    public static final C21381Gs A0W;
    public static final C21381Gs A0X;
    public static final C21381Gs A0Y;
    public final boolean canResizeBadgeIcon;
    public final boolean shouldDrawBackgroundBehindBadge;

    static {
        C21381Gs r0 = new C21381Gs("NONE", 0, false, false);
        A0L = r0;
        C21381Gs r02 = new C21381Gs("SOLID_BLUE", 1, true, true);
        A0S = r02;
        C21381Gs r03 = new C21381Gs("FACEBOOK", 2, true, true);
        A07 = r03;
        C21381Gs r04 = new C21381Gs(TurboLoader.Locator.$const$string(115), 3, true, true);
        A0H = r04;
        C21381Gs r05 = new C21381Gs("BIRTHDAY", 4, true, true);
        A03 = r05;
        C21381Gs r06 = new C21381Gs("VERIFIED", 5, false, false);
        A0V = r06;
        C21381Gs r07 = new C21381Gs("SMS", 6, true, true);
        A0R = r07;
        C21381Gs r08 = new C21381Gs("MESSENGER_AUDIO", 7, false, false);
        A0J = r08;
        C21381Gs r09 = new C21381Gs("MESSENGER_VIDEO", 8, false, false);
        A0K = r09;
        C21381Gs r010 = new C21381Gs("BROADCASTER", 9, false, false);
        A04 = r010;
        C21381Gs r15 = new C21381Gs("TINCAN", 10, true, true);
        A0T = r15;
        C21381Gs r14 = new C21381Gs("EVENT_REMINDER_GOING", 11, false, false);
        A06 = r14;
        C21381Gs r4 = new C21381Gs("EVENT_REMINDER_DECLINED", 12, false, false);
        A05 = r4;
        C21381Gs r42 = new C21381Gs("ACTIVE_NOW", 13, true, true);
        A01 = r42;
        C21381Gs r43 = new C21381Gs("RECENTLY_ACTIVE", 14, true, true);
        A0Q = r43;
        C21381Gs r44 = new C21381Gs("GAME", 15, false, false);
        A0C = r44;
        C21381Gs r45 = new C21381Gs("GAME_BIG", 16, false, false);
        A0D = r45;
        C21381Gs r46 = new C21381Gs("GEM", 17, false, false);
        A0E = r46;
        C21381Gs r47 = new C21381Gs("TROPHY", 18, false, false);
        A0U = r47;
        C21381Gs r48 = new C21381Gs("MESSENGER_APP", 19, true, true);
        A0I = r48;
        C21381Gs r49 = new C21381Gs("FACEBOOK_APP", 20, true, true);
        A08 = r49;
        C21381Gs r13 = new C21381Gs("INSTAGRAM_APP", 21, true, true);
        A0F = r13;
        C21381Gs r12 = new C21381Gs("PAYMENTS", 22, true, true);
        A0N = r12;
        C21381Gs r11 = new C21381Gs("PAYMENT_RECEIVED", 23, false, false);
        A0P = r11;
        C21381Gs r10 = new C21381Gs("PAYMENT_DECLINED", 24, false, false);
        A0O = r10;
        C21381Gs r9 = new C21381Gs("PARTIES", 25, true, true);
        A0M = r9;
        C21381Gs r1 = new C21381Gs("FB_EVENT_GOING", 26, false, false);
        A0A = r1;
        C21381Gs r8 = new C21381Gs("FB_EVENT_CANTGO", 27, false, false);
        A09 = r8;
        C21381Gs r7 = new C21381Gs("FB_EVENT_INTERESTED", 28, false, false);
        A0B = r7;
        C21381Gs r6 = new C21381Gs("ALOHA_HOME", 29, false, true);
        A02 = r6;
        C21381Gs r5 = new C21381Gs("WORK_MCC_EXTERNAL_USER", 30, true, true);
        A0Y = r5;
        C21381Gs r38 = new C21381Gs("WORK_DND_STATUS", 31, true, true);
        A0X = r38;
        C21381Gs r382 = new C21381Gs("WATCH_PARTY_HOST", 32, false, false);
        A0W = r382;
        C21381Gs r383 = new C21381Gs("KOALA_MODE", 33, true, true);
        A0G = r383;
        C21381Gs[] r410 = new C21381Gs[34];
        System.arraycopy(new C21381Gs[]{r0, r02, r03, r04, r05, r06, r07, r08, r09, r010, r15, r14, r4, r42, r43, r44, r45, r46, r47, r48, r49, r13, r12, r11, r10, r9, r1}, 0, r410, 0, 27);
        System.arraycopy(new C21381Gs[]{r8, r7, r6, r5, r38, r382, r383}, 0, r410, 27, 7);
        A00 = r410;
    }

    public static C21381Gs valueOf(String str) {
        return (C21381Gs) Enum.valueOf(C21381Gs.class, str);
    }

    public static C21381Gs[] values() {
        return (C21381Gs[]) A00.clone();
    }

    private C21381Gs(String str, int i, boolean z, boolean z2) {
        this.shouldDrawBackgroundBehindBadge = z;
        this.canResizeBadgeIcon = z2;
    }
}
