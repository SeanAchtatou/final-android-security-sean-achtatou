package X;

import android.os.Bundle;
import com.facebook.fbtrace.FbTraceNode;
import com.facebook.sync.analytics.FullRefreshReason;
import com.facebook.sync.service.SyncOperationParamsUtil$FullRefreshParams;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1mJ  reason: invalid class name and case insensitive filesystem */
public final class C32771mJ {
    private static volatile C32771mJ A01;
    private final AtomicLong A00 = new AtomicLong(System.currentTimeMillis());

    public static SyncOperationParamsUtil$FullRefreshParams A00(C11060ln r2) {
        Bundle bundle = r2.A00;
        FullRefreshReason fullRefreshReason = (FullRefreshReason) bundle.getParcelable("fullRefreshReason");
        Preconditions.checkNotNull(fullRefreshReason);
        return new SyncOperationParamsUtil$FullRefreshParams(fullRefreshReason, bundle.getString("syncTokenToReplace"));
    }

    public static final C32771mJ A01(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C32771mJ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C32771mJ();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public Bundle A02(FullRefreshReason fullRefreshReason, String str) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("fullRefreshReason", fullRefreshReason);
        bundle.putString("syncTokenToReplace", str);
        bundle.putLong("paramsId", this.A00.incrementAndGet());
        return bundle;
    }

    public Bundle A03(C162667fk r5) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(C99084oO.$const$string(443), r5);
        bundle.putLong("paramsId", this.A00.incrementAndGet());
        return bundle;
    }

    public Bundle A04(Serializable serializable, FbTraceNode fbTraceNode) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("syncPayload", serializable);
        bundle.putParcelable("fbTraceNode", fbTraceNode);
        bundle.putLong("paramsId", this.A00.incrementAndGet());
        return bundle;
    }
}
