package X;

import android.content.Context;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1oE  reason: invalid class name and case insensitive filesystem */
public final class C33841oE {
    private static volatile C33841oE A02;
    private AnonymousClass0UN A00;
    private final Map A01 = new AnonymousClass04a();

    public static final C33841oE A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C33841oE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C33841oE(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public boolean A01(String str) {
        if (this.A01.containsKey(str)) {
            return ((Boolean) this.A01.get(str)).booleanValue();
        }
        try {
            boolean hasSystemFeature = ((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, this.A00)).getPackageManager().hasSystemFeature(str);
            this.A01.put(str, Boolean.valueOf(hasSystemFeature));
            return hasSystemFeature;
        } catch (RuntimeException e) {
            C010708t.A0V("SystemFeatures", e, "Runtime Exception while checking for feature %s", str);
            return false;
        }
    }

    private C33841oE(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
