package X;

/* renamed from: X.0zt  reason: invalid class name and case insensitive filesystem */
public enum C17990zt {
    WHITELIST(0),
    PUBLIC(1);
    
    public final int dbValue;

    private C17990zt(int i) {
        this.dbValue = i;
    }

    public static C17990zt A00(int i) {
        for (C17990zt r1 : values()) {
            if (r1.dbValue == i) {
                return r1;
            }
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown dbValue of ", i));
    }
}
