package X;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1hC  reason: invalid class name and case insensitive filesystem */
public final class C29921hC extends AnonymousClass0UV {
    private static volatile A5C A00;
    private static volatile C29911hB A01;

    public static final A5C A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (A5C.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new A5C(A01(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C29911hB A01(AnonymousClass1XY r7) {
        C29911hB r2;
        if (A01 == null) {
            synchronized (C29911hB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        C29951hF A02 = C29931hD.A02(applicationInjector);
                        TelephonyManager telephonyManager = (TelephonyManager) A003.getSystemService("phone");
                        if (telephonyManager == null) {
                            r2 = null;
                        } else {
                            r2 = new C29911hB(telephonyManager, new C29971hH(false), new C30001hK(), A02);
                        }
                        A01 = r2;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
