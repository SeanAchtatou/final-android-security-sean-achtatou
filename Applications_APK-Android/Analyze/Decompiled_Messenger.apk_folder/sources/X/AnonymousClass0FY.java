package X;

import android.util.SparseIntArray;

/* renamed from: X.0FY  reason: invalid class name */
public final class AnonymousClass0FY extends C007907e {
    public C02560Fk[] A00;

    public AnonymousClass0FM A03() {
        return new AnonymousClass0FX();
    }

    public boolean A04(AnonymousClass0FM r13) {
        C02560Fk r6;
        boolean z;
        AnonymousClass0FX r132 = (AnonymousClass0FX) r13;
        C02740Gd.A00(r132, "Null value passed to getSnapshot!");
        int i = C03110Kb.A00;
        boolean z2 = false;
        for (int i2 = 0; i2 < i; i2++) {
            SparseIntArray sparseIntArray = r132.timeInStateS[i2];
            synchronized (this) {
                if (this.A00 == null) {
                    this.A00 = new C02560Fk[i];
                }
                C02560Fk[] r3 = this.A00;
                C02560Fk r0 = r3[i2];
                if (r0 == null) {
                    C02560Fk r2 = new C02560Fk(AnonymousClass08S.A0A("/sys/devices/system/cpu/cpu", i2, "/cpufreq/stats/time_in_state"), 512);
                    r2.A04();
                    r3[i2] = r2;
                } else {
                    r0.A04();
                }
                r6 = this.A00[i2];
            }
            synchronized (this) {
                sparseIntArray.clear();
                if (!r6.A01) {
                    z = false;
                } else {
                    while (r6.A08()) {
                        try {
                            long A03 = r6.A03();
                            r6.A06();
                            r6.A05();
                            sparseIntArray.put((int) A03, (int) (r6.A03() / C02750Ge.A00));
                        } catch (AnonymousClass0KX unused) {
                            z = false;
                        }
                    }
                    z = true;
                }
            }
            z2 |= z;
        }
        return z2;
    }
}
