package X;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.0wi  reason: invalid class name and case insensitive filesystem */
public final class C16230wi implements C16240wj {
    public final /* synthetic */ RecyclerView A00;

    public C16230wi(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    public void ANr(View view, int i) {
        this.A00.addView(view, i);
        RecyclerView recyclerView = this.A00;
        C33781o8 A05 = RecyclerView.A05(view);
        C20831Dz r0 = recyclerView.A0J;
        if (!(r0 == null || A05 == null)) {
            r0.A0B(A05);
        }
        List list = recyclerView.A0R;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                ((C24950CQi) recyclerView.A0R.get(size)).BS2(view);
            }
        }
    }

    public View Ah1(int i) {
        return this.A00.getChildAt(i);
    }

    public int Ah4() {
        return this.A00.getChildCount();
    }

    public int BCT(View view) {
        return this.A00.indexOfChild(view);
    }

    public void C27(int i) {
        View childAt = this.A00.getChildAt(i);
        if (childAt != null) {
            this.A00.A0u(childAt);
            childAt.clearAnimation();
        }
        this.A00.removeViewAt(i);
    }

    public void AP8(View view, int i, ViewGroup.LayoutParams layoutParams) {
        C33781o8 A05 = RecyclerView.A05(view);
        if (A05 != null) {
            if (A05.A0B() || A05.A0F()) {
                A05.A00 &= -257;
            } else {
                throw new IllegalArgumentException("Called attach on a child which is not detached: " + A05 + this.A00.A0d());
            }
        }
        this.A00.attachViewToParent(view, i, layoutParams);
    }

    public void AXE(int i) {
        C33781o8 A05;
        View Ah1 = Ah1(i);
        if (!(Ah1 == null || (A05 = RecyclerView.A05(Ah1)) == null)) {
            if (!A05.A0B() || A05.A0F()) {
                A05.A00 = 256 | A05.A00;
            } else {
                throw new IllegalArgumentException("called detach on an already detached child " + A05 + this.A00.A0d());
            }
        }
        this.A00.detachViewFromParent(i);
    }

    public C33781o8 Ah9(View view) {
        return RecyclerView.A05(view);
    }

    public void BXh(View view) {
        C33781o8 A05 = RecyclerView.A05(view);
        if (A05 != null) {
            RecyclerView recyclerView = this.A00;
            int i = A05.A03;
            if (i != -1) {
                A05.A06 = i;
            } else {
                A05.A06 = C15320v6.getImportantForAccessibility(A05.A0H);
            }
            boolean z = false;
            if (recyclerView.A0A > 0) {
                z = true;
            }
            if (z) {
                A05.A03 = 4;
                recyclerView.A13.add(A05);
                return;
            }
            C15320v6.setImportantForAccessibility(A05.A0H, 4);
        }
    }

    public void Bca(View view) {
        C33781o8 A05 = RecyclerView.A05(view);
        if (A05 != null) {
            RecyclerView recyclerView = this.A00;
            int i = A05.A06;
            boolean z = false;
            if (recyclerView.A0A > 0) {
                z = true;
            }
            if (z) {
                A05.A03 = i;
                recyclerView.A13.add(A05);
            } else {
                C15320v6.setImportantForAccessibility(A05.A0H, i);
            }
            A05.A06 = 0;
        }
    }

    public void C1V() {
        int Ah4 = Ah4();
        for (int i = 0; i < Ah4; i++) {
            View Ah1 = Ah1(i);
            this.A00.A0u(Ah1);
            Ah1.clearAnimation();
        }
        this.A00.removeAllViews();
    }
}
