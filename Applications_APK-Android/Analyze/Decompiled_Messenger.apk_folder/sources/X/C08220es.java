package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.0es  reason: invalid class name and case insensitive filesystem */
public final class C08220es extends Handler {
    private C26191b3 A00;

    public void handleMessage(Message message) {
        Object obj = message.obj;
        if (obj instanceof AnonymousClass0f7) {
            AnonymousClass0f7 r2 = (AnonymousClass0f7) obj;
            if (0 != 0) {
                try {
                    r2.A05(true);
                } catch (Exception e) {
                    this.A00.A01(r2, e);
                }
            } else {
                r2.A05(true);
            }
        }
    }

    public C08220es(Looper looper, C26191b3 r2) {
        super(looper);
        this.A00 = r2;
    }
}
