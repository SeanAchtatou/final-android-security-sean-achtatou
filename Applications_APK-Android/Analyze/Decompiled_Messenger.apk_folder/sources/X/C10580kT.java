package X;

import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0kT  reason: invalid class name and case insensitive filesystem */
public final class C10580kT extends AnonymousClass0UV {
    public static final C04310Tq A03(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AfE, r1);
    }

    public static final C04310Tq A04(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AjY, r1);
    }

    public static final ViewerContext A00(AnonymousClass1XY r0) {
        return AnonymousClass0XL.A01(r0).B9R();
    }

    public static final C05920aY A01(AnonymousClass1XY r0) {
        return AnonymousClass0XL.A01(r0);
    }

    public static final String A02(AnonymousClass1XY r0) {
        return AnonymousClass0XJ.A0G(r0);
    }
}
