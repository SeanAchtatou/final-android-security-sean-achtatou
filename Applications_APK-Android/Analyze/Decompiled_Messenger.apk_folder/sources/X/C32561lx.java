package X;

import android.net.Uri;
import com.facebook.common.stringformat.StringFormatUtil;
import com.google.common.io.Closeables;
import io.card.payment.BuildConfig;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import javax.inject.Singleton;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1lx  reason: invalid class name and case insensitive filesystem */
public final class C32561lx implements C08210er, C05460Za {
    private static volatile C32561lx A06;
    public long A00;
    public String A01 = "not inspected";
    public final AnonymousClass06B A02;
    public final Queue A03 = new LinkedList();
    private final AnonymousClass09P A04;
    private final C25051Yd A05;

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000b, code lost:
        if (r12.equals(r13) == false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(java.lang.String r12, java.lang.String r13, java.lang.String r14, java.lang.String r15) {
        /*
            r11 = this;
            r4 = r12
            r5 = r13
            if (r12 == 0) goto L_0x000d
            if (r13 == 0) goto L_0x000d
            boolean r1 = r12.equals(r13)
            r0 = 1
            if (r1 != 0) goto L_0x000e
        L_0x000d:
            r0 = 0
        L_0x000e:
            if (r0 != 0) goto L_0x006e
            java.lang.String r0 = "not inspected"
            if (r12 != 0) goto L_0x0015
            r4 = r0
        L_0x0015:
            if (r13 == 0) goto L_0x0018
            r0 = r13
        L_0x0018:
            if (r13 == 0) goto L_0x006f
            java.lang.String r0 = "orca_neue_main"
            boolean r0 = r13.equals(r0)
            if (r0 == 0) goto L_0x002d
            java.lang.String r1 = r11.A01
            java.lang.String r0 = "not inspected"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x002d
            r5 = r1
        L_0x002d:
            java.lang.String r0 = "not inspected"
            boolean r0 = r5.equals(r0)
            if (r0 != 0) goto L_0x003f
            java.lang.String r0 = "orca_neue_main"
            boolean r0 = r5.equals(r0)
            if (r0 != 0) goto L_0x003f
            r11.A01 = r5
        L_0x003f:
            java.util.Map r0 = X.C35071qd.A00
            java.lang.Object r9 = r0.get(r5)
            X.1qd r9 = (X.C35071qd) r9
            X.06B r0 = r11.A02
            long r2 = r0.now()
            long r0 = r11.A00
            long r7 = r2 - r0
            r11.A00 = r2
            X.1qe r1 = new X.1qe
            r6 = r14
            r10 = r15
            r1.<init>(r2, r4, r5, r6, r7, r9, r10)
            java.util.Queue r0 = r11.A03
            r0.add(r1)
            java.util.Queue r0 = r11.A03
            int r1 = r0.size()
            r0 = 50
            if (r1 <= r0) goto L_0x006e
            java.util.Queue r0 = r11.A03
            r0.remove()
        L_0x006e:
            return
        L_0x006f:
            r5 = r0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32561lx.A02(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }

    public String getName() {
        return "RecentNavigationTracker";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    public void prepareDataForWriting() {
    }

    public static final C32561lx A00(AnonymousClass1XY r6) {
        if (A06 == null) {
            synchronized (C32561lx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A06 = new C32561lx(AnonymousClass067.A02(), C04750Wa.A01(applicationInjector), AnonymousClass0WT.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static JSONObject A01(C32561lx r11, int i) {
        String str;
        JSONObject put;
        JSONObject jSONObject = new JSONObject();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(r11.A03);
        Collections.reverse(arrayList);
        Iterator it = arrayList.iterator();
        int i2 = 1;
        while (it.hasNext()) {
            C35081qe r3 = (C35081qe) it.next();
            if (i2 > i) {
                break;
            }
            String valueOf = String.valueOf(i2);
            if (r3 == null) {
                put = null;
            } else {
                C35071qd r0 = r3.A02;
                if (r0 != null) {
                    str = r0.toString();
                } else {
                    str = "not inspected";
                }
                JSONObject put2 = new JSONObject().put("recordTime", r3.A01).put("category", str).put("source", r3.A04).put("dest", r3.A05).put("operation", r3.A03);
                String str2 = r3.A06;
                if (str2 == null) {
                    str2 = BuildConfig.FLAVOR;
                }
                JSONObject put3 = put2.put("threadKey", str2);
                long j = r3.A00;
                put = put3.put("timeSpent", StringFormatUtil.formatStrLocaleSafe("%d s %d ms", Long.valueOf(j / 1000), Long.valueOf(j % 1000)));
            }
            jSONObject.put(valueOf, put);
            i2++;
        }
        return jSONObject;
    }

    public void clearUserData() {
        this.A03.clear();
        this.A01 = "not inspected";
    }

    public Map getExtraFileFromWorkerThread(File file) {
        PrintWriter printWriter;
        try {
            File file2 = new File(file, "recent_navigation_json.txt");
            printWriter = new PrintWriter(new FileOutputStream(file2));
            printWriter.write(A01(this, 50).toString());
            Uri fromFile = Uri.fromFile(file2);
            Closeables.A00(printWriter, false);
            HashMap hashMap = new HashMap();
            hashMap.put("recent_navigation_json.txt", fromFile.toString());
            return hashMap;
        } catch (Exception e) {
            this.A04.softReport("RecentNavigationTracker", e);
            return null;
        } catch (Throwable th) {
            Closeables.A00(printWriter, false);
            throw th;
        }
    }

    public boolean shouldSendAsync() {
        return this.A05.Aeo(2306124660286619942L, false);
    }

    private C32561lx(AnonymousClass06B r3, AnonymousClass09P r4, C25051Yd r5) {
        this.A02 = r3;
        this.A04 = r4;
        this.A00 = r3.now();
        this.A05 = r5;
    }
}
