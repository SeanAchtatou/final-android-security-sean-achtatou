package X;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentState;
import java.util.UUID;

/* renamed from: X.09q  reason: invalid class name and case insensitive filesystem */
public final class C012909q {
    public int A00 = -1;
    private final Fragment A01;
    private final C27781dk A02;

    public void A05() {
        C13060qW.A0I(3);
        Fragment fragment = this.A01;
        fragment.A1M(fragment.A01);
        C27781dk r3 = this.A02;
        Fragment fragment2 = this.A01;
        r3.A02(fragment2, fragment2.A01, false);
    }

    public void A06() {
        C13060qW.A0I(3);
        Fragment fragment = this.A01;
        if (!fragment.A07) {
            this.A02.A04(fragment, fragment.A01, false);
            Fragment fragment2 = this.A01;
            fragment2.A1N(fragment2.A01);
            C27781dk r2 = this.A02;
            Fragment fragment3 = this.A01;
            r2.A03(fragment3, fragment3.A01, false);
            return;
        }
        fragment.A1O(fragment.A01);
        this.A01.A0F = 1;
    }

    public void A08() {
        C13060qW.A0I(3);
        this.A01.A1E();
        this.A02.A09(this.A01, false);
    }

    public void A09() {
        C13060qW.A0I(3);
        Fragment fragment = this.A01;
        View view = fragment.A0I;
        if (view != null) {
            Bundle bundle = fragment.A01;
            SparseArray sparseArray = fragment.A0H;
            if (sparseArray != null) {
                view.restoreHierarchyState(sparseArray);
                fragment.A0H = null;
            }
            fragment.A0X = false;
            fragment.A1e(bundle);
            if (!fragment.A0X) {
                throw new C29891h9("Fragment " + fragment + " did not call through to super.onViewStateRestored()");
            } else if (fragment.A0I != null) {
                C29881h8 r0 = fragment.A0Q;
                r0.A00.A08(C14820uB.ON_CREATE);
            }
        }
        this.A01.A01 = null;
    }

    public void A0A() {
        C13060qW.A0I(3);
        this.A01.A1F();
        this.A02.A0A(this.A01, false);
        Fragment fragment = this.A01;
        fragment.A01 = null;
        fragment.A0H = null;
    }

    public void A0C() {
        C13060qW.A0I(3);
        this.A01.A1G();
        this.A02.A0B(this.A01, false);
    }

    public void A0D() {
        C13060qW.A0I(3);
        this.A01.A1H();
        this.A02.A0C(this.A01, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if (r8.A04(r5) != false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0013, code lost:
        if (r0 != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G(X.C13020qR r7, X.C27931dz r8) {
        /*
            r6 = this;
            r0 = 3
            X.C13060qW.A0I(r0)
            androidx.fragment.app.Fragment r5 = r6.A01
            boolean r0 = r5.A0f
            r3 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0015
            int r1 = r5.A0C
            r0 = 0
            if (r1 <= 0) goto L_0x0012
            r0 = 1
        L_0x0012:
            r4 = 1
            if (r0 == 0) goto L_0x0016
        L_0x0015:
            r4 = 0
        L_0x0016:
            if (r4 != 0) goto L_0x001f
            boolean r1 = r8.A04(r5)
            r0 = 0
            if (r1 == 0) goto L_0x0020
        L_0x001f:
            r0 = 1
        L_0x0020:
            if (r0 == 0) goto L_0x0079
            boolean r0 = r7 instanceof X.AnonymousClass0n8
            if (r0 == 0) goto L_0x006b
            boolean r3 = r8.A00
        L_0x0028:
            if (r4 != 0) goto L_0x002c
            if (r3 == 0) goto L_0x005e
        L_0x002c:
            androidx.fragment.app.Fragment r3 = r6.A01
            r0 = 3
            X.C13060qW.A0I(r0)
            java.util.HashMap r1 = r8.A01
            java.lang.String r0 = r3.A0V
            java.lang.Object r0 = r1.get(r0)
            X.1dz r0 = (X.C27931dz) r0
            if (r0 == 0) goto L_0x0048
            r0.A02()
            java.util.HashMap r1 = r8.A01
            java.lang.String r0 = r3.A0V
            r1.remove(r0)
        L_0x0048:
            java.util.HashMap r1 = r8.A03
            java.lang.String r0 = r3.A0V
            java.lang.Object r0 = r1.get(r0)
            X.1dx r0 = (X.C27911dx) r0
            if (r0 == 0) goto L_0x005e
            r0.A00()
            java.util.HashMap r1 = r8.A03
            java.lang.String r0 = r3.A0V
            r1.remove(r0)
        L_0x005e:
            androidx.fragment.app.Fragment r0 = r6.A01
            r0.A1C()
            X.1dk r1 = r6.A02
            androidx.fragment.app.Fragment r0 = r6.A01
            r1.A07(r0, r2)
            return
        L_0x006b:
            android.content.Context r1 = r7.A01
            boolean r0 = r1 instanceof android.app.Activity
            if (r0 == 0) goto L_0x0028
            android.app.Activity r1 = (android.app.Activity) r1
            boolean r0 = r1.isChangingConfigurations()
            r3 = r3 ^ r0
            goto L_0x0028
        L_0x0079:
            androidx.fragment.app.Fragment r0 = r6.A01
            r0.A0F = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C012909q.A0G(X.0qR, X.1dz):void");
    }

    public void A0H(C27931dz r7) {
        C13060qW.A0I(3);
        Fragment fragment = this.A01;
        fragment.A0F = -1;
        fragment.A0X = false;
        fragment.A1n();
        if (fragment.A0X) {
            C13060qW r1 = fragment.A0O;
            if (!r1.A0D) {
                r1.A0W();
                fragment.A0O = new C13050qV();
            }
            boolean z = false;
            this.A02.A08(this.A01, false);
            Fragment fragment2 = this.A01;
            fragment2.A0F = -1;
            fragment2.A0N = null;
            fragment2.A0L = null;
            fragment2.A0P = null;
            if (fragment2.A0f) {
                boolean z2 = false;
                if (fragment2.A0C > 0) {
                    z2 = true;
                }
                if (!z2) {
                    z = true;
                }
            }
            if (z || r7.A04(fragment2)) {
                C13060qW.A0I(3);
                Fragment fragment3 = this.A01;
                Fragment.A03(fragment3);
                fragment3.A0V = UUID.randomUUID().toString();
                fragment3.A0W = false;
                fragment3.A0f = false;
                fragment3.A0Z = false;
                fragment3.A0d = false;
                fragment3.A0g = false;
                fragment3.A0C = 0;
                fragment3.A0P = null;
                fragment3.A0O = new C13050qV();
                fragment3.A0N = null;
                fragment3.A0E = 0;
                fragment3.A0D = 0;
                fragment3.A0T = null;
                fragment3.A0b = false;
                fragment3.A0Y = false;
                return;
            }
            return;
        }
        throw new C29891h9("Fragment " + fragment + " did not call through to super.onDetach()");
    }

    private Bundle A00() {
        Bundle bundle = new Bundle();
        Fragment fragment = this.A01;
        fragment.A1u(bundle);
        fragment.A0R.A01(bundle);
        Parcelable A0M = fragment.A0O.A0M();
        if (A0M != null) {
            bundle.putParcelable("android:support:fragments", A0M);
        }
        this.A02.A05(this.A01, bundle, false);
        if (bundle.isEmpty()) {
            bundle = null;
        }
        if (this.A01.A0I != null) {
            A0B();
        }
        if (this.A01.A0H != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", this.A01.A0H);
        }
        if (!this.A01.A0i) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", this.A01.A0i);
        }
        return bundle;
    }

    public int A01() {
        int i = this.A00;
        Fragment fragment = this.A01;
        if (fragment.A0Z) {
            if (fragment.A0d) {
                i = Math.max(i, 1);
            } else {
                i = Math.min(i, fragment.A0F);
            }
        }
        if (!fragment.A0W) {
            i = Math.min(i, 1);
        }
        if (fragment.A0f) {
            boolean z = false;
            if (fragment.A0C > 0) {
                z = true;
            }
            if (z) {
                i = Math.min(i, 1);
            } else {
                i = Math.min(i, -1);
            }
        }
        if (fragment.A06 && fragment.A0F < 3) {
            i = Math.min(i, 2);
        }
        switch (fragment.A02.ordinal()) {
            case 2:
                return Math.min(i, 1);
            case 3:
                return Math.min(i, 3);
            case 4:
                return i;
            default:
                return Math.min(i, -1);
        }
    }

    public Fragment.SavedState A02() {
        Bundle A002;
        if (this.A01.A0F <= -1 || (A002 = A00()) == null) {
            return null;
        }
        return new Fragment.SavedState(A002);
    }

    public FragmentState A04() {
        Fragment fragment = this.A01;
        FragmentState fragmentState = new FragmentState(fragment);
        if (fragment.A0F <= -1 || fragmentState.A00 != null) {
            fragmentState.A00 = fragment.A01;
            return fragmentState;
        }
        Bundle A002 = A00();
        fragmentState.A00 = A002;
        if (this.A01.A0U != null) {
            if (A002 == null) {
                fragmentState.A00 = new Bundle();
            }
            fragmentState.A00.putString("android:target_state", this.A01.A0U);
            int i = this.A01.A00;
            if (i != 0) {
                fragmentState.A00.putInt("android:target_req_state", i);
            }
        }
        return fragmentState;
    }

    public void A07() {
        Fragment fragment = this.A01;
        if (fragment.A0Z && !fragment.A09) {
            C13060qW.A0I(3);
            fragment.A1Q(fragment.A1d(fragment.A01), null, this.A01.A01);
            View view = this.A01.A0I;
            if (view != null) {
                view.setSaveFromParentEnabled(false);
                Fragment fragment2 = this.A01;
                if (fragment2.A0b) {
                    fragment2.A0I.setVisibility(8);
                }
                Fragment fragment3 = this.A01;
                fragment3.A1v(fragment3.A0I, fragment3.A01);
                C27781dk r3 = this.A02;
                Fragment fragment4 = this.A01;
                r3.A06(fragment4, fragment4.A0I, fragment4.A01, false);
            }
        }
    }

    public void A0B() {
        View view = this.A01.A0I;
        if (view != null) {
            SparseArray sparseArray = new SparseArray();
            view.saveHierarchyState(sparseArray);
            if (sparseArray.size() > 0) {
                this.A01.A0H = sparseArray;
            }
        }
    }

    public void A0E(C27711dd r6) {
        String str;
        Fragment fragment = this.A01;
        if (!fragment.A0Z) {
            C13060qW.A0I(3);
            ViewGroup viewGroup = null;
            ViewGroup viewGroup2 = fragment.A0J;
            if (viewGroup2 != null) {
                viewGroup = viewGroup2;
            } else {
                int i = fragment.A0D;
                if (i != 0) {
                    if (i != -1) {
                        viewGroup = (ViewGroup) r6.A00(i);
                        if (viewGroup == null) {
                            Fragment fragment2 = this.A01;
                            if (!fragment2.A0g) {
                                try {
                                    str = fragment2.A12().getResourceName(this.A01.A0D);
                                } catch (Resources.NotFoundException unused) {
                                    str = "unknown";
                                }
                                StringBuilder sb = new StringBuilder("No view found for id 0x");
                                Fragment fragment3 = this.A01;
                                sb.append(Integer.toHexString(fragment3.A0D));
                                sb.append(" (");
                                sb.append(str);
                                sb.append(") for fragment ");
                                sb.append(fragment3);
                                throw new IllegalArgumentException(sb.toString());
                            }
                        }
                    } else {
                        throw new IllegalArgumentException("Cannot create fragment " + fragment + " for a container view with no id");
                    }
                }
            }
            Fragment fragment4 = this.A01;
            fragment4.A0J = viewGroup;
            fragment4.A1Q(fragment4.A1d(fragment4.A01), viewGroup, this.A01.A01);
            View view = this.A01.A0I;
            if (view != null) {
                boolean z = false;
                view.setSaveFromParentEnabled(false);
                Fragment fragment5 = this.A01;
                fragment5.A0I.setTag(2131298152, fragment5);
                if (viewGroup != null) {
                    viewGroup.addView(this.A01.A0I);
                }
                Fragment fragment6 = this.A01;
                if (fragment6.A0b) {
                    fragment6.A0I.setVisibility(8);
                }
                C15320v6.requestApplyInsets(this.A01.A0I);
                Fragment fragment7 = this.A01;
                fragment7.A1v(fragment7.A0I, fragment7.A01);
                C27781dk r3 = this.A02;
                Fragment fragment8 = this.A01;
                r3.A06(fragment8, fragment8.A0I, fragment8.A01, false);
                Fragment fragment9 = this.A01;
                if (fragment9.A0I.getVisibility() == 0 && this.A01.A0J != null) {
                    z = true;
                }
                fragment9.A08 = z;
            }
        }
    }

    public void A0F(C13020qR r6, C13060qW r7, Fragment fragment) {
        Fragment fragment2 = this.A01;
        fragment2.A0N = r6;
        fragment2.A0L = fragment;
        fragment2.A0P = r7;
        this.A02.A01(fragment2, r6.A01, false);
        Fragment fragment3 = this.A01;
        fragment3.A0O.A0v(fragment3.A0N, new C29901hA(fragment3), fragment3);
        fragment3.A0F = 0;
        fragment3.A0X = false;
        fragment3.A1s(fragment3.A0N.A01);
        if (fragment3.A0X) {
            Fragment fragment4 = this.A01;
            Fragment fragment5 = fragment4.A0L;
            if (fragment5 == null) {
                r6.A05(fragment4);
            } else {
                fragment5.A1w(fragment4);
            }
            this.A02.A00(this.A01, r6.A01, false);
            return;
        }
        throw new C29891h9("Fragment " + fragment3 + " did not call through to super.onAttach()");
    }

    public void A0I(ClassLoader classLoader) {
        Bundle bundle = this.A01.A01;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
            Fragment fragment = this.A01;
            fragment.A0H = fragment.A01.getSparseParcelableArray("android:view_state");
            Fragment fragment2 = this.A01;
            fragment2.A0U = fragment2.A01.getString("android:target_state");
            Fragment fragment3 = this.A01;
            if (fragment3.A0U != null) {
                fragment3.A00 = fragment3.A01.getInt("android:target_req_state", 0);
            }
            Fragment fragment4 = this.A01;
            Boolean bool = fragment4.A05;
            if (bool != null) {
                fragment4.A0i = bool.booleanValue();
                fragment4.A05 = null;
            } else {
                fragment4.A0i = fragment4.A01.getBoolean("android:user_visible_hint", true);
            }
            Fragment fragment5 = this.A01;
            if (!fragment5.A0i) {
                fragment5.A06 = true;
            }
        }
    }

    public Fragment A03() {
        return this.A01;
    }

    public C012909q(C27781dk r2, Fragment fragment) {
        this.A02 = r2;
        this.A01 = fragment;
    }

    public C012909q(C27781dk r3, Fragment fragment, FragmentState fragmentState) {
        String str;
        this.A02 = r3;
        this.A01 = fragment;
        fragment.A0H = null;
        fragment.A0C = 0;
        fragment.A0d = false;
        fragment.A0W = false;
        Fragment fragment2 = fragment.A0M;
        if (fragment2 != null) {
            str = fragment2.A0V;
        } else {
            str = null;
        }
        fragment.A0U = str;
        fragment.A0M = null;
        Bundle bundle = fragmentState.A00;
        if (bundle != null) {
            fragment.A01 = bundle;
        } else {
            fragment.A01 = new Bundle();
        }
    }

    public C012909q(C27781dk r4, ClassLoader classLoader, C27801dm r6, FragmentState fragmentState) {
        this.A02 = r4;
        this.A01 = r6.A01(classLoader, fragmentState.A05);
        Bundle bundle = fragmentState.A04;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
        }
        Fragment fragment = this.A01;
        fragment.A1P(fragmentState.A04);
        fragment.A0V = fragmentState.A07;
        fragment.A0Z = fragmentState.A09;
        fragment.A0g = true;
        fragment.A0E = fragmentState.A02;
        fragment.A0D = fragmentState.A01;
        fragment.A0T = fragmentState.A06;
        fragment.A0h = fragmentState.A0C;
        fragment.A0f = fragmentState.A0B;
        fragment.A0Y = fragmentState.A08;
        fragment.A0b = fragmentState.A0A;
        fragment.A02 = AnonymousClass1XL.values()[fragmentState.A03];
        Bundle bundle2 = fragmentState.A00;
        if (bundle2 != null) {
            this.A01.A01 = bundle2;
        } else {
            this.A01.A01 = new Bundle();
        }
        C13060qW.A0I(2);
    }
}
