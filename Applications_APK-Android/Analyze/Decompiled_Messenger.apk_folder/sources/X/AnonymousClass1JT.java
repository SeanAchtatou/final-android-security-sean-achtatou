package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.UserKey;
import com.google.common.base.Objects;

@UserScoped
/* renamed from: X.1JT  reason: invalid class name */
public final class AnonymousClass1JT implements C05460Za {
    private static C05540Zi A02;
    public AnonymousClass0UN A00;
    public UserKey A01 = null;

    public void clearUserData() {
        this.A01 = null;
    }

    public static final AnonymousClass1JT A00(AnonymousClass1XY r4) {
        AnonymousClass1JT r0;
        synchronized (AnonymousClass1JT.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass1JT((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass1JT) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public static boolean A01(ThreadSummary threadSummary) {
        C17930zi r1;
        if (threadSummary.A0O != C10950l8.A05 || (((r1 = threadSummary.A0i) != C17930zi.VOICE_CALL && r1 != C17930zi.VIDEO_CALL && r1 != C17930zi.RECENT_VOICE_CALL && r1 != C17930zi.RECENT_VIDEO_CALL) || !ThreadKey.A0B(threadSummary.A0S))) {
            return false;
        }
        return true;
    }

    public boolean A02(ThreadSummary threadSummary) {
        if (threadSummary.A0Q != null) {
            if (this.A01 == null) {
                this.A01 = (UserKey) AnonymousClass1XX.A03(AnonymousClass1Y3.B7T, this.A00);
            }
            if (!Objects.equal(this.A01, threadSummary.A0Q.A01)) {
                return false;
            }
            return true;
        }
        return false;
    }

    private AnonymousClass1JT(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
