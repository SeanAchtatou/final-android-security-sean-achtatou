package X;

/* renamed from: X.0k0  reason: invalid class name and case insensitive filesystem */
public enum C10380k0 {
    INTERN_FIELD_NAMES(true),
    CANONICALIZE_FIELD_NAMES(true);
    
    public final boolean _defaultState;

    private C10380k0(boolean z) {
        this._defaultState = z;
    }
}
