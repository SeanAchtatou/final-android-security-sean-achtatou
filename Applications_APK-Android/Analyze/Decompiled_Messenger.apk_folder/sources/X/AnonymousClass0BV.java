package X;

/* renamed from: X.0BV  reason: invalid class name */
public final class AnonymousClass0BV implements AnonymousClass0BU {
    public final C01570At A00;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r0.isConnected() == false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean CDi(java.util.Map r7) {
        /*
            r6 = this;
            X.0At r0 = r6.A00
            android.net.NetworkInfo r0 = r0.A03()
            if (r0 == 0) goto L_0x000f
            boolean r0 = r0.isConnected()
            r5 = 1
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            r5 = 0
        L_0x0010:
            if (r5 != 0) goto L_0x0023
            X.0At r0 = r6.A00
            android.net.NetworkInfo r4 = r0.A03()
            if (r7 == 0) goto L_0x0023
            java.lang.String r3 = "MqttNetworkManagerMonitor"
            if (r4 != 0) goto L_0x0024
            java.lang.String r0 = "no_info"
            r7.put(r3, r0)
        L_0x0023:
            return r5
        L_0x0024:
            int r0 = r4.getType()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            int r0 = r4.getSubtype()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            android.net.NetworkInfo$State r0 = r4.getState()
            java.lang.Object[] r2 = new java.lang.Object[]{r2, r1, r0}
            java.lang.String r1 = "%s_%s_%s"
            r0 = 0
            java.lang.String r0 = java.lang.String.format(r0, r1, r2)
            r7.put(r3, r0)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BV.CDi(java.util.Map):boolean");
    }

    public AnonymousClass0BV(C01570At r1) {
        this.A00 = r1;
    }
}
