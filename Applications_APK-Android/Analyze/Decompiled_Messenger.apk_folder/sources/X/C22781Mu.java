package X;

import android.graphics.Bitmap;
import android.os.Build;
import java.util.Map;

/* renamed from: X.1Mu  reason: invalid class name and case insensitive filesystem */
public class C22781Mu implements C22761Ms {
    public final C22761Ms A00;
    public final C22761Ms A01;
    private final C22761Ms A02 = new C30711iW(this);
    private final C30591iK A03;
    private final Map A04;

    public AnonymousClass1S5 A02(AnonymousClass1NY r6, C23541Px r7) {
        AnonymousClass1PS decodeFromEncodedImageWithColorSpace = this.A03.decodeFromEncodedImageWithColorSpace(r6, r7.A02, null, r7.A03);
        try {
            AnonymousClass8W1 r3 = r7.A04;
            if (r3 != null) {
                Bitmap bitmap = (Bitmap) decodeFromEncodedImageWithColorSpace.A0A();
                if (Build.VERSION.SDK_INT >= 12) {
                    bitmap.setHasAlpha(true);
                }
                r3.A00(bitmap);
            }
            C33271nJ r2 = C33271nJ.A03;
            AnonymousClass1NY.A05(r6);
            int i = r6.A02;
            AnonymousClass1NY.A05(r6);
            return new AnonymousClass1S5(decodeFromEncodedImageWithColorSpace, r2, i, r6.A00);
        } finally {
            decodeFromEncodedImageWithColorSpace.close();
        }
    }

    public AnonymousClass1S5 A03(AnonymousClass1NY r11, int i, C33271nJ r13, C23541Px r14) {
        AnonymousClass1PS decodeJPEGFromEncodedImageWithColorSpace = this.A03.decodeJPEGFromEncodedImageWithColorSpace(r11, r14.A02, null, i, r14.A03);
        try {
            AnonymousClass8W1 r3 = r14.A04;
            if (r3 != null) {
                Bitmap bitmap = (Bitmap) decodeJPEGFromEncodedImageWithColorSpace.A0A();
                if (Build.VERSION.SDK_INT >= 12) {
                    bitmap.setHasAlpha(true);
                }
                r3.A00(bitmap);
            }
            AnonymousClass1NY.A05(r11);
            int i2 = r11.A02;
            AnonymousClass1NY.A05(r11);
            return new AnonymousClass1S5(decodeJPEGFromEncodedImageWithColorSpace, r13, i2, r11.A00);
        } finally {
            decodeJPEGFromEncodedImageWithColorSpace.close();
        }
    }

    public AnonymousClass1S9 AWg(AnonymousClass1NY r3, int i, C33271nJ r5, C23541Px r6) {
        C22761Ms r0 = r6.A05;
        if (r0 == null) {
            AnonymousClass1NY.A05(r3);
            AnonymousClass1O3 r1 = r3.A07;
            if (r1 == null || r1 == AnonymousClass1O3.A02) {
                r1 = AnonymousClass1OG.A01(r3.A09());
                r3.A07 = r1;
            }
            Map map = this.A04;
            if (map == null || (r0 = (C22761Ms) map.get(r1)) == null) {
                r0 = this.A02;
            }
        }
        return r0.AWg(r3, i, r5, r6);
    }

    public C22781Mu(C22761Ms r2, C22761Ms r3, C30591iK r4, Map map) {
        this.A00 = r2;
        this.A01 = r3;
        this.A03 = r4;
        this.A04 = map;
    }
}
