package X;

import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

/* renamed from: X.1zS  reason: invalid class name and case insensitive filesystem */
public abstract class C39691zS {
    public static final C39691zS A00;

    public void A00(SQLiteDatabase sQLiteDatabase, boolean z) {
        if (!(this instanceof C70633az)) {
            sQLiteDatabase.setForeignKeyConstraintsEnabled(z);
        } else if (z) {
            C007406x.A00(1020634399);
            sQLiteDatabase.execSQL("PRAGMA foreign_keys = ON");
            C007406x.A00(206192310);
        } else {
            C007406x.A00(243189364);
            sQLiteDatabase.execSQL("PRAGMA foreign_keys = OFF");
            C007406x.A00(-2038376309);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            A00 = new C49942d7();
        } else {
            A00 = new C70633az();
        }
    }
}
