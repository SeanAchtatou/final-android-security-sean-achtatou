package X;

/* renamed from: X.08G  reason: invalid class name */
public final class AnonymousClass08G {
    public static Integer A01(int i) {
        if (i == 0) {
            return AnonymousClass07B.A00;
        }
        if (i == 1) {
            return AnonymousClass07B.A01;
        }
        if (i == 2) {
            return AnonymousClass07B.A0C;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown QOS level ", i));
    }

    public static int A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return 1;
            case 2:
                return 2;
            default:
                return 0;
        }
    }
}
