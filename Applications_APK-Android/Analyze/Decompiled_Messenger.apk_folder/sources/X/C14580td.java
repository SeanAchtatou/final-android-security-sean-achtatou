package X;

/* renamed from: X.0td  reason: invalid class name and case insensitive filesystem */
public final class C14580td {
    public static int[] A00 = new int[2];

    static {
        String[] strArr = {"overscroll_glow", "overscroll_edge"};
        try {
            Class<?> cls = Class.forName("com.android.internal.R$drawable");
            int i = 0;
            for (int i2 = 0; i2 < 2; i2++) {
                A00[i] = cls.getDeclaredField(strArr[i2]).getInt(null);
                i++;
            }
        } catch (Exception unused) {
        }
    }
}
