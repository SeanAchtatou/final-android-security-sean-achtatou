package X;

import java.util.Set;

/* renamed from: X.0Z0  reason: invalid class name */
public final class AnonymousClass0Z0 extends AnonymousClass0Wl {
    private AnonymousClass0UN A00;

    public String getSimpleName() {
        return "LazyCustomErrorDataInitializer";
    }

    public static final AnonymousClass0Z0 A00(AnonymousClass1XY r1) {
        return new AnonymousClass0Z0(r1);
    }

    private AnonymousClass0Z0(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public void init() {
        int A03 = C000700l.A03(-414435636);
        for (AnonymousClass0Z1 r3 : (Set) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B1V, this.A00)) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).Bz9(r3.Amj(), r3);
        }
        C000700l.A09(632258565, A03);
    }
}
