package X;

import android.app.job.JobParameters;
import android.app.job.JobServiceEngine;
import android.os.IBinder;

/* renamed from: X.0Hl  reason: invalid class name and case insensitive filesystem */
public final class C02990Hl extends JobServiceEngine implements C03000Hm {
    public JobParameters A00;
    public final AnonymousClass0E1 A01;
    public final Object A02 = new Object();

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        r2.getIntent().setExtrasClassLoader(r4.A01.getClassLoader());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        return new X.C03010Hn(r4, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000f, code lost:
        if (r2 == null) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0E7 AX2() {
        /*
            r4 = this;
            java.lang.Object r3 = r4.A02
            monitor-enter(r3)
            android.app.job.JobParameters r1 = r4.A00     // Catch:{ all -> 0x0025 }
            r0 = 0
            if (r1 != 0) goto L_0x000a
            monitor-exit(r3)     // Catch:{ all -> 0x0025 }
            return r0
        L_0x000a:
            android.app.job.JobWorkItem r2 = r1.dequeueWork()     // Catch:{ all -> 0x0025 }
            monitor-exit(r3)     // Catch:{ all -> 0x0025 }
            if (r2 == 0) goto L_0x0024
            android.content.Intent r1 = r2.getIntent()
            X.0E1 r0 = r4.A01
            java.lang.ClassLoader r0 = r0.getClassLoader()
            r1.setExtrasClassLoader(r0)
            X.0Hn r0 = new X.0Hn
            r0.<init>(r4, r2)
            return r0
        L_0x0024:
            return r0
        L_0x0025:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0025 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02990Hl.AX2():X.0E7");
    }

    public boolean onStartJob(JobParameters jobParameters) {
        this.A00 = jobParameters;
        this.A01.ensureProcessorRunningLocked(false);
        return true;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        boolean doStopCurrentWork = this.A01.doStopCurrentWork();
        synchronized (this.A02) {
            this.A00 = null;
        }
        return doStopCurrentWork;
    }

    public C02990Hl(AnonymousClass0E1 r2) {
        super(r2);
        this.A01 = r2;
    }

    public IBinder ATY() {
        return getBinder();
    }
}
