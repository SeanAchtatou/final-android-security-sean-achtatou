package X;

import com.facebook.common.dextricks.stats.ClassLoadingStats;

/* renamed from: X.08V  reason: invalid class name */
public final class AnonymousClass08V extends ClassLoadingStats {
    public void decrementDexFileQueries() {
    }

    public int getClassLoadsAttempted() {
        return 0;
    }

    public int getClassLoadsFailed() {
        return 0;
    }

    public int getDexFileQueries() {
        return 0;
    }

    public int getIncorrectDfaGuesses() {
        return 0;
    }

    public int getLocatorAssistedClassLoads() {
        return 0;
    }

    public int getTurboLoaderClassLocationFailures() {
        return 0;
    }

    public int getTurboLoaderClassLocationSuccesses() {
        return 0;
    }

    public int getTurboLoaderMapGenerationFailures() {
        return 0;
    }

    public int getTurboLoaderMapGenerationSuccesses() {
        return 0;
    }

    public void incrementClassLoadsAttempted() {
    }

    public void incrementClassLoadsFailed() {
    }

    public void incrementDexFileQueries(int i) {
    }

    public void incrementIncorrectDfaGuesses() {
    }

    public void incrementTurboLoaderMapGenerationFailures() {
    }

    public void incrementTurboLoaderMapGenerationSuccesses() {
    }
}
