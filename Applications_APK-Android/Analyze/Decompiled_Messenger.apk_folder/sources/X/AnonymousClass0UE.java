package X;

/* renamed from: X.0UE  reason: invalid class name */
public abstract class AnonymousClass0UE implements AnonymousClass1XW {
    public AnonymousClass1Xm mBinder;

    public void configure() {
    }

    public void bindAssistedProvider(Class cls) {
        this.mBinder.AQ3(cls);
    }

    public C30360Eul bindComponent(Class cls) {
        return this.mBinder.AQ4(cls);
    }

    public void bindScope(Class cls, C24891Xn r3) {
        this.mBinder.AQE(cls, r3);
    }

    public AnonymousClass1Xm getBinder() {
        return this.mBinder;
    }

    public void require(Class cls) {
        this.mBinder.C3N(cls);
    }

    public void assertBindingInstalled(C22916BKm bKm) {
        this.mBinder.AOu(bKm);
    }

    public void assertBindingInstalled(Class cls) {
        this.mBinder.AOv(cls);
    }

    public void assertBindingInstalled(Class cls, Class cls2) {
        this.mBinder.AOu(new C22916BKm(cls, C22916BKm.A00(cls2)));
    }

    public C30183ErA bind(C22916BKm bKm) {
        return this.mBinder.APt(bKm);
    }

    public C30187ErE bind(Class cls) {
        return this.mBinder.APs(cls);
    }

    public C30183ErA bindDefault(C22916BKm bKm) {
        return this.mBinder.AQ7(bKm);
    }

    public C30187ErE bindDefault(Class cls) {
        return this.mBinder.AQ6(cls);
    }

    public C38551xc bindMulti(C22916BKm bKm) {
        return this.mBinder.AQA(bKm);
    }

    public C38551xc bindMulti(Class cls) {
        return this.mBinder.AQB(cls);
    }

    public C38551xc bindMulti(Class cls, Class cls2) {
        return this.mBinder.AQC(cls, cls2);
    }

    public void declareMultiBinding(C22916BKm bKm) {
        this.mBinder.AWd(bKm);
    }

    public void declareMultiBinding(Class cls) {
        this.mBinder.AWe(cls);
    }

    public void declareMultiBinding(Class cls, Class cls2) {
        this.mBinder.AWf(cls, cls2);
    }
}
