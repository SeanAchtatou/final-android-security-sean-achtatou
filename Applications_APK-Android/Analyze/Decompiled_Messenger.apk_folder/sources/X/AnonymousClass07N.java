package X;

import android.content.Context;
import android.os.Build;
import android.os.DeadObjectException;
import android.os.health.HealthStats;
import android.os.health.SystemHealthManager;
import android.util.Pair;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.acra.constants.ReportField;
import com.facebook.acra.info.ExternalProcessInfo;
import io.card.payment.BuildConfig;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.07N  reason: invalid class name */
public final class AnonymousClass07N {
    public static final String A0U = "AppStateServiceReport";
    private static final Pattern A0V = Pattern.compile("\"process_id\":(\\d+)");
    public char A00;
    public char A01;
    public char A02;
    public char A03;
    public char A04;
    public char A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public long A0B;
    public long A0C;
    public long A0D;
    public long A0E;
    public long A0F;
    public long A0G;
    public long A0H;
    public AnonymousClass0K6 A0I;
    public File A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public Properties A0O;
    public Properties A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public byte[] A0T;

    public static Pair A00(Context context, String str) {
        HealthStats healthStats;
        long j;
        try {
            HealthStats takeMyUidSnapshot = ((SystemHealthManager) context.getSystemService(SystemHealthManager.class)).takeMyUidSnapshot();
            if (!takeMyUidSnapshot.hasStats(10014) || (healthStats = takeMyUidSnapshot.getStats(10014).get(str)) == null) {
                return null;
            }
            long j2 = 0;
            if (healthStats.hasMeasurement(30005)) {
                j = healthStats.getMeasurement(30005);
            } else {
                j = 0;
            }
            if (healthStats.hasMeasurement(30004)) {
                j2 = healthStats.getMeasurement(30004);
            }
            return new Pair(Long.valueOf(j), Long.valueOf(j2));
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                return null;
            }
            throw e;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:14|15|16|17|18) */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0033 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x003a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass0K3 A01(java.io.File r7, X.AnonymousClass0NH r8) {
        /*
            java.io.RandomAccessFile r6 = new java.io.RandomAccessFile     // Catch:{ 0Cz | IOException -> 0x003b }
            java.lang.String r0 = "rw"
            r6.<init>(r7, r0)     // Catch:{ 0Cz | IOException -> 0x003b }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ all -> 0x0034 }
            java.io.FileDescriptor r0 = r6.getFD()     // Catch:{ all -> 0x0034 }
            r5.<init>(r0)     // Catch:{ all -> 0x0034 }
            java.lang.String r4 = A02(r7)     // Catch:{ all -> 0x002d }
            long r2 = r7.lastModified()     // Catch:{ all -> 0x002d }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ all -> 0x002d }
            r0.<init>(r5)     // Catch:{ all -> 0x002d }
            char r1 = X.AnonymousClass07M.A00(r0)     // Catch:{ all -> 0x002d }
            X.0K3 r0 = new X.0K3     // Catch:{ all -> 0x002d }
            r0.<init>(r1, r4, r2)     // Catch:{ all -> 0x002d }
            r5.close()     // Catch:{ all -> 0x0034 }
            r6.close()     // Catch:{ 0Cz | IOException -> 0x003b }
            return r0
        L_0x002d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002f }
        L_0x002f:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x0033 }
        L_0x0033:
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            r6.close()     // Catch:{ all -> 0x003a }
        L_0x003a:
            throw r0     // Catch:{ 0Cz | IOException -> 0x003b }
        L_0x003b:
            r2 = move-exception
            java.lang.String r1 = X.AnonymousClass07N.A0U
            java.lang.String r0 = "Error parsing AppState native log file."
            X.C010708t.A0L(r1, r0, r2)
            if (r8 == 0) goto L_0x0048
            X.AnonymousClass0NH.A03(r8, r2, r7)
        L_0x0048:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07N.A01(java.io.File, X.0NH):X.0K3");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0033 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0033=Splitter:B:19:0x0033, B:10:0x0029=Splitter:B:10:0x0029} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Properties A04(java.io.File r5, boolean r6) {
        /*
            if (r5 == 0) goto L_0x003d
            java.util.Properties r4 = new java.util.Properties
            r4.<init>()
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0034 }
            r3.<init>(r5)     // Catch:{ IOException -> 0x0034 }
            if (r6 == 0) goto L_0x0026
            int r1 = r3.read()     // Catch:{ all -> 0x002d }
            r0 = 100
            if (r1 == r0) goto L_0x0026
            java.lang.String r2 = X.AnonymousClass07N.A0U     // Catch:{ all -> 0x002d }
            java.lang.String r1 = "Found partially-written properties file, ignoring contents: %s"
            java.lang.String r0 = r5.getName()     // Catch:{ all -> 0x002d }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x002d }
            X.C010708t.A0O(r2, r1, r0)     // Catch:{ all -> 0x002d }
            goto L_0x0029
        L_0x0026:
            r4.load(r3)     // Catch:{ all -> 0x002d }
        L_0x0029:
            r3.close()     // Catch:{ IOException -> 0x0034 }
            return r4
        L_0x002d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002f }
        L_0x002f:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0033 }
        L_0x0033:
            throw r0     // Catch:{ IOException -> 0x0034 }
        L_0x0034:
            r2 = move-exception
            java.lang.String r1 = X.AnonymousClass07N.A0U
            java.lang.String r0 = "Could not load properties"
            X.C010708t.A0L(r1, r0, r2)
            return r4
        L_0x003d:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07N.A04(java.io.File, boolean):java.util.Properties");
    }

    public ExternalProcessInfo A05() {
        String str;
        int lastIndexOf;
        long j = this.A0G;
        long currentTimeMillis = System.currentTimeMillis();
        Properties properties = this.A0P;
        Integer num = null;
        if (properties != null) {
            str = properties.getProperty("processId");
        } else {
            str = null;
        }
        String str2 = this.A0K;
        if (str == null && str2 != null) {
            Matcher matcher = A0V.matcher(str2);
            if (matcher.find()) {
                str = matcher.group(1);
            }
        }
        if (Build.VERSION.SDK_INT >= 19 && str != null) {
            num = Integer.valueOf(Integer.parseInt(str));
        }
        String str3 = "Recovering from UFAD on pid " + str + " " + (currentTimeMillis - j) + " msec after last write";
        ExternalProcessInfo externalProcessInfo = new ExternalProcessInfo();
        externalProcessInfo.pid = num;
        try {
            String A032 = A03(str2, "processName");
            if (A032 != null) {
                externalProcessInfo.setAcraField(ReportField.PROCESS_NAME, A032);
            }
            String A033 = A03(str2, "appVersionName");
            if (A033 != null) {
                externalProcessInfo.setAcraField(ReportField.APP_VERSION_NAME, A033);
            }
            String A034 = A03(str2, "appVersionCode");
            if (A034 != null) {
                externalProcessInfo.setAcraField(ReportField.APP_VERSION_CODE, A034);
            }
            String A035 = A03(str2, "processWallClockUptimeMs");
            if (A035 != null) {
                externalProcessInfo.setAcraField(ReportField.PROCESS_UPTIME, A035);
            }
            String A036 = A03(str2, "userId");
            if (A036 != null) {
                externalProcessInfo.setAcraField(ReportField.UID, A036);
            }
            String A037 = A03(str2, "granularExposures");
            if (A037 != null) {
                externalProcessInfo.setAcraField(ErrorReportingConstants.GRANULAR_EXPOSURES, A037);
            }
            String A038 = A03(str2, "navModule");
            if (A038 != null) {
                externalProcessInfo.setAcraField("navigation_module", A038);
            }
            String A039 = A03(str2, ErrorReportingConstants.ENDPOINT);
            if (A039 != null) {
                externalProcessInfo.setAcraField(ErrorReportingConstants.ENDPOINT, A039);
            }
            String str4 = this.A0N;
            if (str4 != null && (lastIndexOf = str4.lastIndexOf(95)) > -1) {
                externalProcessInfo.setAcraField(ReportField.ORIGINAL_SESSION_ID, str4.substring(lastIndexOf + 1));
            }
            externalProcessInfo.setAcraField("original_fad_data", str2);
            externalProcessInfo.setAcraField("fad_report_time", Long.toString(this.A0G));
            long j2 = this.A0B;
            if (j2 != Long.MAX_VALUE) {
                externalProcessInfo.setAcraField("health_stats_anr_diff", String.valueOf(j2));
            }
            long j3 = this.A0C;
            if (j3 != Long.MAX_VALUE) {
                externalProcessInfo.setAcraField("health_stats_crash_diff", String.valueOf(j3));
            }
            int i = this.A08;
            if (i > -1) {
                externalProcessInfo.setAcraField("nightwatch_status", String.valueOf(i));
            }
            int i2 = this.A07;
            if (i2 > -1) {
                externalProcessInfo.setAcraField("nightwatch_reason", String.valueOf(i2));
            }
            long j4 = this.A0F;
            if (j4 > 0) {
                externalProcessInfo.setAcraField("nightwatch_crash_time_ms", String.valueOf(j4));
            }
            externalProcessInfo.setAcraField("original_activity_state", String.valueOf(this.A00));
            externalProcessInfo.setAcraField("original_app_state_status", String.valueOf(this.A02));
            externalProcessInfo.setAcraField("original_anr_native_status", String.valueOf(this.A01));
            externalProcessInfo.setAcraField("original_native_status", String.valueOf(this.A04));
            externalProcessInfo.setAcraField("original_pending_stop_count", String.valueOf(this.A09));
            externalProcessInfo.setAcraField("original_next_pending_stop_code", String.valueOf(this.A06));
            externalProcessInfo.setAcraField("original_foreground_entity_name", String.valueOf(A06()));
            String A0310 = A03(str2, "activities");
            if (A0310 != null) {
                StringBuilder sb = new StringBuilder();
                int i3 = 0;
                int i4 = 0;
                do {
                    int indexOf = A0310.indexOf(123, i3);
                    if (indexOf == -1 || (i3 = A0310.indexOf((int) AnonymousClass1Y3.A0w, indexOf)) == -1) {
                        externalProcessInfo.setAcraField("activity_stack", sb.toString());
                        externalProcessInfo.setAcraField("activity_stack_size", AnonymousClass08S.A09(BuildConfig.FLAVOR, i4));
                    } else {
                        i4++;
                        String substring = A0310.substring(indexOf, i3 + 1);
                        sb.append(A03(substring, "name"));
                        sb.append("(");
                        sb.append(A03(substring, "state"));
                        sb.append(")\n");
                    }
                } while (i3 < A0310.length());
                externalProcessInfo.setAcraField("activity_stack", sb.toString());
                externalProcessInfo.setAcraField("activity_stack_size", AnonymousClass08S.A09(BuildConfig.FLAVOR, i4));
            }
        } catch (Throwable th) {
            str3 = AnonymousClass08S.A0P(str3, " + report failure: ", th.getMessage());
        }
        externalProcessInfo.mMessage = str3;
        AnonymousClass0K6 r0 = this.A0I;
        if (r0 != null) {
            List<String> unmodifiableList = Collections.unmodifiableList(r0.A00);
            if (!unmodifiableList.isEmpty()) {
                StringBuilder sb2 = new StringBuilder();
                for (String append : unmodifiableList) {
                    sb2.append(append);
                    sb2.append(" ");
                }
                externalProcessInfo.setAcraField("updated_libraries", sb2.toString().trim());
            }
        }
        return externalProcessInfo;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x0050 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A06() {
        /*
            r7 = this;
            java.io.File r2 = r7.A0J
            char r6 = r7.A03
            r0 = 32
            if (r6 != r0) goto L_0x000b
            java.lang.String r2 = "NONE"
            return r2
        L_0x000b:
            r0 = 33
            if (r6 != r0) goto L_0x0012
            java.lang.String r2 = "OTHERS"
            return r2
        L_0x0012:
            java.util.Properties r5 = new java.util.Properties
            r5.<init>()
            r4 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0051 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0051 }
            r5.load(r1)     // Catch:{ all -> 0x004a }
            r1.close()     // Catch:{ IOException -> 0x0051 }
            java.util.Set r0 = r5.stringPropertyNames()
            java.util.Iterator r3 = r0.iterator()
        L_0x002b:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0049
            java.lang.Object r2 = r3.next()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r1 = r5.getProperty(r2)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x002b
            r0 = 0
            char r0 = r1.charAt(r0)
            if (r6 != r0) goto L_0x002b
            return r2
        L_0x0049:
            return r4
        L_0x004a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004c }
        L_0x004c:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0050 }
        L_0x0050:
            throw r0     // Catch:{ IOException -> 0x0051 }
        L_0x0051:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07N.A06():java.lang.String");
    }

    public void A07() {
        String A002 = C013109s.A00(this.A0T, false);
        if (!A002.equals(this.A0L)) {
            throw new C02110Cz(AnonymousClass08S.A0P("Checksum does not match. Expected '", A002, "'"));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        if (X.C002101k.A09.mSymbol == r2) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A08() {
        /*
            r5 = this;
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_CREATED
            char r0 = r0.mLogSymbol
            char r3 = r5.A00
            r4 = 1
            if (r0 == r3) goto L_0x0039
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_STARTED
            char r0 = r0.mLogSymbol
            if (r0 == r3) goto L_0x0039
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_RESUMED
            char r0 = r0.mLogSymbol
            if (r0 == r3) goto L_0x0039
            X.01Y r0 = X.AnonymousClass01Y.IN_FOREGROUND
            char r0 = r0.mLogSymbol
            if (r0 == r3) goto L_0x0039
            X.01k r0 = X.C002101k.A0A
            char r0 = r0.mSymbol
            char r2 = r5.A05
            if (r0 == r2) goto L_0x0030
            X.01k r0 = X.C002101k.A0F
            char r0 = r0.mSymbol
            if (r0 == r2) goto L_0x0030
            X.01k r0 = X.C002101k.A09
            char r1 = r0.mSymbol
            r0 = 1
            if (r1 != r2) goto L_0x0031
        L_0x0030:
            r0 = 0
        L_0x0031:
            if (r0 == 0) goto L_0x003a
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_PAUSED
            char r0 = r0.mLogSymbol
            if (r0 != r3) goto L_0x003a
        L_0x0039:
            return r4
        L_0x003a:
            java.lang.String r1 = r5.A0K
            java.lang.String r0 = "\"fg_anr\":1"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x0039
            java.lang.String r0 = "\"Resumed\""
            boolean r0 = r1.contains(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07N.A08():boolean");
    }

    public static String A02(File file) {
        int lastIndexOf;
        String name = file.getName();
        Integer A002 = AnonymousClass01V.A00(name);
        if (A002 == null || (lastIndexOf = name.lastIndexOf(AnonymousClass01V.A01(A002))) <= 0) {
            return name;
        }
        return name.substring(0, lastIndexOf);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A03(java.lang.String r6, java.lang.String r7) {
        /*
            int r2 = r6.indexOf(r7)
            r5 = 0
            r4 = -1
            if (r2 == r4) goto L_0x0049
            r1 = 58
            int r0 = r7.length()
            int r2 = r2 + r0
            int r3 = r6.indexOf(r1, r2)
            if (r3 == r4) goto L_0x0049
            int r0 = r6.length()
            int r0 = r0 + -2
            if (r3 >= r0) goto L_0x0049
            r2 = 1
            int r3 = r3 + r2
            char r1 = r6.charAt(r3)
            r0 = 34
            if (r1 == r0) goto L_0x0031
            char r1 = r6.charAt(r3)
            r0 = 91
            if (r1 != r0) goto L_0x004a
            r0 = 93
        L_0x0031:
            int r3 = r3 + 1
            int r1 = r6.indexOf(r0, r3)
        L_0x0037:
            if (r1 == r4) goto L_0x0049
            r0 = 44
            int r0 = r6.indexOf(r0, r1)
            if (r0 != r4) goto L_0x004d
            r0 = 125(0x7d, float:1.75E-43)
            int r0 = r6.indexOf(r0, r1)
            if (r0 != r4) goto L_0x004d
        L_0x0049:
            return r5
        L_0x004a:
            r1 = r3
            r2 = 0
            goto L_0x0037
        L_0x004d:
            if (r2 == 0) goto L_0x0051
            int r0 = r0 + -1
        L_0x0051:
            java.lang.String r5 = r6.substring(r3, r0)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07N.A03(java.lang.String, java.lang.String):java.lang.String");
    }

    private AnonymousClass07N() {
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass07N(char r31, char r32, char r33, char r34, char r35, char r36, int r37, int r38, int r39, java.lang.String r40, java.lang.String r41, java.lang.String r42, long r43, long r45, long r47, long r49, boolean r51, byte[] r52, boolean r53, boolean r54, long r55, long r57, int r59, int r60, java.lang.String r61, long r62, java.util.Properties r64, java.util.Properties r65, java.io.File r66) {
        /*
            r30 = this;
            r2 = r30
            r19 = r47
            r17 = r45
            r15 = r43
            r14 = r42
            r13 = r41
            r12 = r40
            r11 = r39
            r10 = r38
            r9 = r37
            r21 = r49
            r4 = r32
            r3 = r31
            r5 = r33
            r27 = r64
            r29 = r66
            r28 = r65
            r26 = r54
            r25 = r53
            r8 = r36
            r24 = r52
            r7 = r35
            r23 = r51
            r6 = r34
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r17, r19, r21, r23, r24, r25, r26, r27, r28, r29)
            r0 = r55
            r2.A0C = r0
            r0 = r57
            r2.A0B = r0
            r0 = r59
            r2.A08 = r0
            r0 = r60
            r2.A07 = r0
            r0 = r61
            r2.A0M = r0
            r0 = r62
            r2.A0F = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07N.<init>(char, char, char, char, char, char, int, int, int, java.lang.String, java.lang.String, java.lang.String, long, long, long, long, boolean, byte[], boolean, boolean, long, long, int, int, java.lang.String, long, java.util.Properties, java.util.Properties, java.io.File):void");
    }

    private AnonymousClass07N(char c, char c2, char c3, char c4, char c5, char c6, int i, int i2, int i3, String str, String str2, String str3, long j, long j2, long j3, long j4, boolean z, byte[] bArr, boolean z2, boolean z3, Properties properties, Properties properties2, File file) {
        this.A05 = c;
        this.A02 = c2;
        this.A00 = c5;
        this.A03 = c6;
        this.A06 = i;
        this.A09 = i2;
        this.A0A = i3;
        this.A0L = str;
        this.A0K = str2;
        this.A0N = str3;
        this.A0G = j;
        this.A0E = j2;
        this.A0D = j3;
        this.A0H = j4;
        this.A0R = z;
        this.A0T = bArr;
        this.A0Q = z2;
        this.A0S = z3;
        this.A0P = properties;
        this.A0O = properties2;
        this.A04 = c3;
        this.A01 = c4;
        this.A0J = file;
        this.A0I = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01d8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Missing exception handler attribute for start block: B:122:0x01dc */
    /* JADX WARNING: Missing exception handler attribute for start block: B:75:0x014d */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:133:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:134:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fc  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:114:0x01d2=Splitter:B:114:0x01d2, B:122:0x01dc=Splitter:B:122:0x01dc} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass07N(android.content.Context r42, char r43, char r44, char r45, int r46, int r47, int r48, java.lang.String r49, java.lang.String r50, java.lang.String r51, long r52, long r54, long r56, long r58, boolean r60, byte[] r61, boolean r62, java.util.Properties r63, java.util.Properties r64, java.io.File r65) {
        /*
            r41 = this;
            r13 = r41
            X.01k r0 = X.C002101k.A0D
            char r0 = r0.mSymbol
            r36 = 0
            r37 = 0
            r32 = r58
            r30 = r56
            r28 = r54
            r19 = r45
            r20 = r46
            r38 = r63
            r21 = r47
            r39 = r64
            r40 = r65
            r22 = r48
            r18 = r44
            r35 = r61
            r14 = r43
            r34 = r60
            r23 = r49
            r24 = r50
            r25 = r51
            r26 = r52
            r15 = r14
            r16 = r0
            r17 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r28, r30, r32, r34, r35, r36, r37, r38, r39, r40)
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 24
            r3 = r42
            if (r1 < r0) goto L_0x00a1
            java.lang.String r2 = r13.A0K
            java.lang.String r0 = "anr_count"
            java.lang.String r1 = A03(r2, r0)
            java.lang.String r0 = "crash_count"
            java.lang.String r8 = A03(r2, r0)
            if (r1 != 0) goto L_0x0050
            if (r8 == 0) goto L_0x00a1
        L_0x0050:
            java.lang.String r0 = "processName"
            java.lang.String r0 = A03(r2, r0)
            if (r0 == 0) goto L_0x00a1
            android.util.Pair r2 = A00(r3, r0)
            if (r2 == 0) goto L_0x00a1
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r1 == 0) goto L_0x0079
            java.lang.Object r0 = r2.first
            java.lang.Long r0 = (java.lang.Long) r0
            long r6 = r0.longValue()
            long r0 = java.lang.Long.parseLong(r1)     // Catch:{ NumberFormatException -> 0x0073 }
            long r6 = r6 - r0
            goto L_0x007e
        L_0x0073:
            r6 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            goto L_0x007e
        L_0x0079:
            r6 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x007e:
            if (r8 == 0) goto L_0x0093
            java.lang.Object r0 = r2.second
            java.lang.Long r0 = (java.lang.Long) r0
            long r4 = r0.longValue()
            long r0 = java.lang.Long.parseLong(r8)     // Catch:{ NumberFormatException -> 0x008e }
            long r4 = r4 - r0
            goto L_0x0093
        L_0x008e:
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x0093:
            android.util.Pair r2 = new android.util.Pair
            java.lang.Long r1 = java.lang.Long.valueOf(r6)
            java.lang.Long r0 = java.lang.Long.valueOf(r4)
            r2.<init>(r1, r0)
            goto L_0x00a2
        L_0x00a1:
            r2 = 0
        L_0x00a2:
            if (r2 == 0) goto L_0x00b8
            java.lang.Object r0 = r2.first
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            r13.A0B = r0
            java.lang.Object r0 = r2.second
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            r13.A0C = r0
        L_0x00b8:
            r0 = -1
            r13.A08 = r0
            r13.A07 = r0
            r0 = 0
            r13.A0F = r0
            java.lang.String r2 = r13.A0N
            r0 = 95
            int r1 = r2.lastIndexOf(r0)
            r0 = -1
            if (r1 == r0) goto L_0x00fc
            r0 = 0
            java.lang.String r2 = r2.substring(r0, r1)
        L_0x00d1:
            if (r2 == 0) goto L_0x01e0
            r6 = 0
            java.lang.String r0 = "watcher"
            java.io.File r1 = r3.getDir(r0, r6)
            X.0K1 r0 = new X.0K1
            r0.<init>(r2)
            java.io.File[] r7 = r1.listFiles(r0)
            r4 = 0
            if (r7 == 0) goto L_0x019e
            int r5 = r7.length
            r10 = -9223372036854775808
            r9 = 0
            r3 = r4
        L_0x00eb:
            if (r9 >= r5) goto L_0x00fe
            r8 = r7[r9]
            long r1 = r8.lastModified()
            int r0 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x00f9
            r3 = r8
            r10 = r1
        L_0x00f9:
            int r9 = r9 + 1
            goto L_0x00eb
        L_0x00fc:
            r2 = 0
            goto L_0x00d1
        L_0x00fe:
            if (r3 == 0) goto L_0x019e
            java.lang.String r4 = r3.getCanonicalPath()     // Catch:{ IOException -> 0x0104 }
        L_0x0104:
            X.0Oa r2 = new X.0Oa
            r2.<init>()
            r12 = 0
            java.io.FileReader r9 = new java.io.FileReader     // Catch:{ IOException -> 0x014e }
            r9.<init>(r3)     // Catch:{ IOException -> 0x014e }
            r0 = 256(0x100, float:3.59E-43)
            char[] r10 = new char[r0]     // Catch:{ all -> 0x0145 }
            int r8 = r9.read(r10)     // Catch:{ all -> 0x0145 }
            if (r8 <= 0) goto L_0x013f
            java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x0145 }
            r1.<init>(r10, r6, r8)     // Catch:{ all -> 0x0145 }
            java.lang.String r0 = "\\s"
            java.lang.String[] r11 = r1.split(r0)     // Catch:{ all -> 0x0145 }
            int r1 = r11.length     // Catch:{ all -> 0x0145 }
            if (r1 <= 0) goto L_0x013f
            r10 = r11[r36]     // Catch:{ all -> 0x0145 }
            r0 = 1
            if (r1 <= r0) goto L_0x0139
            r8 = r11[r0]     // Catch:{ all -> 0x0136 }
            r0 = 2
            if (r1 <= r0) goto L_0x013c
            r1 = r11[r0]     // Catch:{ all -> 0x0134 }
            goto L_0x013d
        L_0x0134:
            r0 = move-exception
            goto L_0x0148
        L_0x0136:
            r0 = move-exception
            r8 = r12
            goto L_0x0148
        L_0x0139:
            r1 = r12
            r8 = r12
            goto L_0x013d
        L_0x013c:
            r1 = r12
        L_0x013d:
            r12 = r10
            goto L_0x0141
        L_0x013f:
            r1 = r12
            r8 = r12
        L_0x0141:
            r9.close()     // Catch:{ IOException -> 0x0153 }
            goto L_0x0153
        L_0x0145:
            r0 = move-exception
            r10 = r12
            r8 = r12
        L_0x0148:
            throw r0     // Catch:{ all -> 0x0149 }
        L_0x0149:
            r0 = move-exception
            r9.close()     // Catch:{ all -> 0x014d }
        L_0x014d:
            throw r0     // Catch:{ IOException -> 0x0151 }
        L_0x014e:
            r1 = r12
            r8 = r12
            goto L_0x0153
        L_0x0151:
            r1 = r12
            r12 = r10
        L_0x0153:
            if (r12 == 0) goto L_0x015b
            int r0 = java.lang.Integer.parseInt(r12)     // Catch:{ NumberFormatException -> 0x015b }
            r2.A01 = r0     // Catch:{ NumberFormatException -> 0x015b }
        L_0x015b:
            if (r8 == 0) goto L_0x0163
            int r0 = java.lang.Integer.parseInt(r8)     // Catch:{ NumberFormatException -> 0x0163 }
            r2.A00 = r0     // Catch:{ NumberFormatException -> 0x0163 }
        L_0x0163:
            if (r1 == 0) goto L_0x016b
            long r0 = java.lang.Long.parseLong(r1)     // Catch:{ NumberFormatException -> 0x016b }
            r2.A02 = r0     // Catch:{ NumberFormatException -> 0x016b }
        L_0x016b:
            int r0 = r2.A00
            r13.A07 = r0
            int r0 = r2.A01
            r13.A08 = r0
            long r0 = r2.A02
            r13.A0F = r0
            r10 = 0
        L_0x0178:
            if (r10 >= r5) goto L_0x01a0
            r9 = r7[r10]
            boolean r0 = r3.equals(r9)
            if (r0 != 0) goto L_0x0198
            java.io.File r8 = new java.io.File
            java.io.File r2 = r9.getParentFile()
            java.lang.String r1 = r9.getName()
            java.lang.String r0 = ".ru"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r8.<init>(r2, r0)
            r8.delete()
        L_0x0198:
            r9.delete()
            int r10 = r10 + 1
            goto L_0x0178
        L_0x019e:
            r0 = 0
            goto L_0x01a1
        L_0x01a0:
            r0 = 1
        L_0x01a1:
            if (r62 == 0) goto L_0x01a9
            if (r0 != 0) goto L_0x01a9
            r13.A08 = r6
            r13.A07 = r6
        L_0x01a9:
            if (r4 == 0) goto L_0x01e0
            java.io.File r5 = new java.io.File
            java.lang.String r0 = ".ru"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r4, r0)
            r5.<init>(r0)
            boolean r0 = r5.exists()
            if (r0 == 0) goto L_0x01e0
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ IOException -> 0x01dd }
            r4.<init>(r5)     // Catch:{ IOException -> 0x01dd }
            r0 = 256(0x100, float:3.59E-43)
            char[] r3 = new char[r0]     // Catch:{ all -> 0x01d6 }
            int r2 = r4.read(r3)     // Catch:{ all -> 0x01d6 }
            if (r2 <= 0) goto L_0x01d2
            java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x01d6 }
            r1.<init>(r3, r6, r2)     // Catch:{ all -> 0x01d6 }
            r13.A0M = r1     // Catch:{ all -> 0x01d6 }
        L_0x01d2:
            r4.close()     // Catch:{ IOException -> 0x01dd }
            goto L_0x01dd
        L_0x01d6:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x01d8 }
        L_0x01d8:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x01dc }
        L_0x01dc:
            throw r0     // Catch:{ IOException -> 0x01dd }
        L_0x01dd:
            r5.delete()
        L_0x01e0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07N.<init>(android.content.Context, char, char, char, int, int, int, java.lang.String, java.lang.String, java.lang.String, long, long, long, long, boolean, byte[], boolean, java.util.Properties, java.util.Properties, java.io.File):void");
    }
}
