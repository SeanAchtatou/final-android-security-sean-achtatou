package X;

import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.shapes.Shape;

/* renamed from: X.1M9  reason: invalid class name */
public interface AnonymousClass1M9 {
    Drawable Akn();

    boolean BGo();

    void C5v();

    void C5z();

    void C60(float f);

    void C6M(AnonymousClass1S6 r1);

    void C7T(Shape shape);

    void CBr(int i);

    void clear();

    void setColorFilter(int i, PorterDuff.Mode mode);

    void setColorFilter(ColorFilter colorFilter);
}
