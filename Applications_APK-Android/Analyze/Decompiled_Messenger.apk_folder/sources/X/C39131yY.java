package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1yY  reason: invalid class name and case insensitive filesystem */
public final class C39131yY implements AnonymousClass06U {
    public FbSharedPreferences A00;

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r7) {
        int A002 = AnonymousClass09Y.A00(696161889);
        FbSharedPreferences A003 = FbSharedPreferencesModule.A00(AnonymousClass1XX.get(context));
        this.A00 = A003;
        if (!A003.BFQ()) {
            AnonymousClass09Y.A01(1520535437, A002);
            return;
        }
        C30281hn edit = this.A00.edit();
        edit.putBoolean(C56232pa.A00, true);
        edit.commit();
        AnonymousClass09Y.A01(1499219647, A002);
    }
}
