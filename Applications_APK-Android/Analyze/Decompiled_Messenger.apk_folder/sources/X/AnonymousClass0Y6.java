package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.facebook.common.util.TriState;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.systrace.TraceDirect;
import com.google.common.base.Preconditions;
import java.lang.reflect.Array;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Y6  reason: invalid class name */
public final class AnonymousClass0Y6 implements AnonymousClass0WP, AnonymousClass0YZ, C25101Yi {
    public static final Class A0T = AnonymousClass0Y6.class;
    private static volatile AnonymousClass0Y6 A0U;
    public int A00 = 0;
    public int A01;
    public int A02 = 1;
    public long A03;
    public long A04;
    public AnonymousClass0UN A05;
    public Map A06;
    public PriorityQueue A07;
    public Set A08;
    public AtomicInteger A09 = new AtomicInteger(0);
    public boolean A0A;
    public boolean A0B;
    public boolean A0C = false;
    public boolean A0D;
    public boolean A0E = false;
    public boolean A0F;
    public boolean A0G;
    public AnonymousClass0XY[] A0H;
    public long[][] A0I;
    public final C05210Yb A0J;
    public final AtomicBoolean A0K = new AtomicBoolean(false);
    public final Condition A0L;
    public final Condition A0M;
    public final Condition A0N;
    public final ReentrantLock A0O;
    private final C25111Yj A0P = new C05200Ya();
    private final WeakHashMap A0Q;
    private final AtomicInteger A0R;
    public volatile Integer A0S = AnonymousClass07B.A00;

    private AnonymousClass0XX A01(String str, Runnable runnable, Callable callable, AnonymousClass0XV r19, ExecutorService executorService, int i) {
        C25111Yj r2;
        boolean z = true;
        boolean z2 = false;
        Runnable runnable2 = runnable;
        if (runnable != null) {
            z2 = true;
        }
        Callable callable2 = callable;
        if (callable == null) {
            z = false;
        }
        Preconditions.checkArgument(z ^ z2, "One of runnable or callable must be specified");
        this.A0O.lock();
        try {
            int incrementAndGet = this.A0R.incrementAndGet();
            if (((C25121Yk) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BTs, this.A05)).A02()) {
                r2 = (C180598Ya) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BDT, this.A05);
            } else {
                r2 = this.A0P;
            }
            String str2 = str;
            AnonymousClass0XV r7 = r19;
            AnonymousClass0XX r3 = new AnonymousClass0XX(r2.AVX(runnable2, callable2, incrementAndGet, str2, r7, "AppChoreographer(p%s)/%s"));
            AnonymousClass0XV r10 = r7;
            A05(new AnonymousClass0XY(r3, r10, executorService, str2, incrementAndGet, i));
            C05350Yp.A08(r3, new AnonymousClass0XT(this), C25141Ym.INSTANCE);
            if (A0C(r7)) {
                if (this.A0C) {
                    boolean z3 = false;
                    if (r19 != null && r7.ordinal() >= AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY.ordinal()) {
                        z3 = true;
                    }
                    if (z3) {
                        this.A0N.signalAll();
                    }
                }
                this.A0M.signalAll();
            }
            return r3;
        } finally {
            this.A0O.unlock();
        }
    }

    public static String A02(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "APPLICATION_INITIALIZING";
            case 2:
                return "APPLICATION_LOADING";
            case 3:
                return "APPLICATION_LOADED";
            default:
                return "START";
        }
    }

    public AnonymousClass0XX CIB(String str, Runnable runnable, AnonymousClass0XV r10, ExecutorService executorService) {
        return A01(str, runnable, null, r10, executorService, -1);
    }

    public static final AnonymousClass0Y6 A00(AnonymousClass1XY r4) {
        if (A0U == null) {
            synchronized (AnonymousClass0Y6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0U, r4);
                if (A002 != null) {
                    try {
                        A0U = new AnonymousClass0Y6(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0U;
    }

    private void A04() {
        if (!this.A0Q.isEmpty()) {
            long now = ((AnonymousClass06B) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AgK, this.A05)).now();
            Iterator it = this.A0Q.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (now - ((Long) entry.getValue()).longValue() >= ((C25121Yk) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BTs, this.A05)).A01()) {
                    C005505z.A03("DefaultAppChoreographer.pruneUiLoadingMap.timeOutUILoadingAsyncTask", 1792992669);
                    try {
                        entry.getKey();
                        it.remove();
                    } finally {
                        C005505z.A00(1399481994);
                    }
                }
            }
        }
    }

    private void A05(AnonymousClass0XY r5) {
        PriorityQueue priorityQueue = this.A07;
        if (priorityQueue != null) {
            priorityQueue.add(r5);
            return;
        }
        Map map = this.A06;
        if (map != null) {
            ExecutorService executorService = r5.A05;
            PriorityQueue priorityQueue2 = (PriorityQueue) map.get(executorService);
            if (priorityQueue2 == null) {
                priorityQueue2 = new PriorityQueue(100, this.A0J);
                this.A06.put(executorService, priorityQueue2);
            }
            priorityQueue2.add(r5);
            return;
        }
        throw new IllegalStateException("No queue to add tasks");
    }

    private void A06(AnonymousClass0XY r3) {
        PriorityQueue priorityQueue = this.A07;
        if (priorityQueue != null) {
            priorityQueue.remove(r3);
            return;
        }
        Map map = this.A06;
        if (map != null) {
            PriorityQueue priorityQueue2 = (PriorityQueue) map.get(r3.A05);
            priorityQueue2.remove(r3);
            if (priorityQueue2.isEmpty()) {
                this.A06.remove(r3.A05);
                return;
            }
            return;
        }
        throw new IllegalStateException("Cannot remove task because there is no queue");
    }

    public static void A07(AnonymousClass0Y6 r6) {
        if (r6.A0S == AnonymousClass07B.A0C && !r6.A0B()) {
            boolean z = true;
            if (!r6.A0G) {
                TriState triState = TriState.YES;
                int i = AnonymousClass1Y3.B5j;
                if (triState != ((AnonymousClass0Ud) AnonymousClass1XX.A02(5, i, r6.A05)).A0C() && ((AnonymousClass0Ud) AnonymousClass1XX.A02(5, i, r6.A05)).A07() <= LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT) {
                    z = false;
                }
            }
            if (z) {
                r6.A0A(AnonymousClass07B.A0N);
            } else {
                ((Handler) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amu, r6.A05)).sendEmptyMessageDelayed(0, 1000);
            }
        }
    }

    public static void A08(AnonymousClass0Y6 r1) {
        r1.A0M.signalAll();
        if (r1.A0C) {
            r1.A0N.signalAll();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01af, code lost:
        ((X.AnonymousClass09P) X.AnonymousClass1XX.A02(10, X.AnonymousClass1Y3.Amr, r6.A05)).Bz2("fb_task_description", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01c0, code lost:
        throw com.google.common.base.Throwables.propagate(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01c1, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01c3, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01c5, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        r1 = new java.lang.IllegalStateException("Cannot peek tasks because there is no queue");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00fa, code lost:
        if (r2 == false) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x012f, code lost:
        r6.A0A = false;
        r1 = r6.A08;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0133, code lost:
        if (r1 == null) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0135, code lost:
        r1.add(r7.A05);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x013a, code lost:
        r6.A06(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        r6.A0O.unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        r6.A0H[r20] = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        X.AnonymousClass07A.A02(r7.A05, r7.A03, -1730003143).get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r6.A0K.set(true);
        ((android.os.Handler) X.AnonymousClass1XX.A02(1, X.AnonymousClass1Y3.Amu, r6.A05)).sendEmptyMessage(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x018e, code lost:
        r1 = r6.A08;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0190, code lost:
        if (r1 == null) goto L_0x0197;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0192, code lost:
        r1.remove(r7.A05);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x019d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        X.C010708t.A0E(X.AnonymousClass0Y6.A0T, r3, "ExecutionException Error running appchoregrapher thread: %s", r18);
        r0 = r7.A04;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01ac, code lost:
        if (r0 != null) goto L_0x01ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01ae, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0201 A[Catch:{ all -> 0x023e }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0214 A[Catch:{ all -> 0x023e }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:122:0x01f4=Splitter:B:122:0x01f4, B:117:0x01e5=Splitter:B:117:0x01e5, B:134:0x022b=Splitter:B:134:0x022b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A09(X.AnonymousClass0Y6 r19, int r20, boolean r21) {
        /*
            java.lang.String r10 = "Null description"
            java.lang.String r8 = "fb_task_description"
            int r1 = X.AnonymousClass1Y3.APr
            r6 = r19
            X.0UN r0 = r6.A05
            r11 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r11, r1, r0)
            X.1Y6 r0 = (X.AnonymousClass1Y6) r0
            r0.AOx()
            java.lang.Thread r19 = java.lang.Thread.currentThread()
            java.lang.String r18 = r19.getName()
        L_0x001c:
            r9 = 10
            r17 = 0
            r16 = 1
            X.AnonymousClass05x.A00()     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
            java.util.concurrent.locks.ReentrantLock r0 = r6.A0O     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
            r0.lock()     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
        L_0x002a:
            boolean r0 = r6.A0F     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x006f
            java.util.PriorityQueue r0 = r6.A07     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x0064
            java.util.Set r0 = java.util.Collections.singleton(r0)     // Catch:{ all -> 0x01d7 }
        L_0x0036:
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x01d7 }
        L_0x003a:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x006d
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x01d7 }
            java.util.PriorityQueue r0 = (java.util.PriorityQueue) r0     // Catch:{ all -> 0x01d7 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x01d7 }
        L_0x004a:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x003a
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x01d7 }
            X.0XY r0 = (X.AnonymousClass0XY) r0     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x004a
            X.0XX r0 = r0.A03     // Catch:{ all -> 0x01d7 }
            boolean r0 = r0.isCancelled()     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x004a
            r1.remove()     // Catch:{ all -> 0x01d7 }
            goto L_0x004a
        L_0x0064:
            java.util.Map r0 = r6.A06     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x01cf
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x01d7 }
            goto L_0x0036
        L_0x006d:
            r6.A0F = r11     // Catch:{ all -> 0x01d7 }
        L_0x006f:
            java.util.PriorityQueue r0 = r6.A07     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x007b
            java.lang.Object r7 = r0.peek()     // Catch:{ all -> 0x01d7 }
            X.0XY r7 = (X.AnonymousClass0XY) r7     // Catch:{ all -> 0x01d7 }
            goto L_0x00fe
        L_0x007b:
            java.util.Map r0 = r6.A06     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x01c7
            java.util.Set r0 = r6.A08     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x01c7
            int r1 = r0.size()     // Catch:{ all -> 0x01d7 }
            int r0 = r6.A02     // Catch:{ all -> 0x01d7 }
            if (r0 > 0) goto L_0x008c
            r0 = 1
        L_0x008c:
            if (r1 >= r0) goto L_0x00fc
            long r4 = r6.A03     // Catch:{ all -> 0x01d7 }
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x01d7 }
            X.0UN r0 = r6.A05     // Catch:{ all -> 0x01d7 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01d7 }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x01d7 }
            long r14 = r0.now()     // Catch:{ all -> 0x01d7 }
            java.util.Map r0 = r6.A06     // Catch:{ all -> 0x01d7 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x01d7 }
            java.util.Iterator r13 = r0.iterator()     // Catch:{ all -> 0x01d7 }
            r7 = r17
        L_0x00ab:
            boolean r0 = r13.hasNext()     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x00f5
            java.lang.Object r2 = r13.next()     // Catch:{ all -> 0x01d7 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x01d7 }
            java.util.Set r1 = r6.A08     // Catch:{ all -> 0x01d7 }
            java.lang.Object r0 = r2.getKey()     // Catch:{ all -> 0x01d7 }
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x01d7 }
            if (r0 != 0) goto L_0x00ab
            java.lang.Object r0 = r2.getValue()     // Catch:{ all -> 0x01d7 }
            java.util.PriorityQueue r0 = (java.util.PriorityQueue) r0     // Catch:{ all -> 0x01d7 }
            java.lang.Object r12 = r0.peek()     // Catch:{ all -> 0x01d7 }
            X.0XY r12 = (X.AnonymousClass0XY) r12     // Catch:{ all -> 0x01d7 }
            if (r12 == 0) goto L_0x00ab
            int r1 = r12.A01     // Catch:{ all -> 0x01d7 }
            r0 = -1
            if (r1 == r0) goto L_0x00e9
            r2 = 0
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00e9
            long r2 = r6.A03     // Catch:{ all -> 0x01d7 }
            int r0 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1))
            if (r0 > 0) goto L_0x00e9
            long r0 = (long) r1     // Catch:{ all -> 0x01d7 }
            long r2 = r2 + r0
            int r0 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x00e9
            goto L_0x00ab
        L_0x00e9:
            if (r7 == 0) goto L_0x00f3
            X.0Yb r0 = r6.A0J     // Catch:{ all -> 0x01d7 }
            int r0 = r0.compare(r12, r7)     // Catch:{ all -> 0x01d7 }
            if (r0 >= 0) goto L_0x00ab
        L_0x00f3:
            r7 = r12
            goto L_0x00ab
        L_0x00f5:
            if (r21 == 0) goto L_0x00fe
            if (r7 != 0) goto L_0x0101
            r2 = 0
        L_0x00fa:
            if (r2 != 0) goto L_0x00fe
        L_0x00fc:
            r7 = r17
        L_0x00fe:
            if (r7 == 0) goto L_0x013e
            goto L_0x0114
        L_0x0101:
            X.0XV r0 = r7.A02     // Catch:{ all -> 0x01d7 }
            r2 = 0
            if (r0 == 0) goto L_0x00fa
            int r1 = r0.ordinal()     // Catch:{ all -> 0x01d7 }
            X.0XV r0 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY     // Catch:{ all -> 0x01d7 }
            int r0 = r0.ordinal()     // Catch:{ all -> 0x01d7 }
            if (r1 < r0) goto L_0x00fa
            r2 = 1
            goto L_0x00fa
        L_0x0114:
            X.0XX r0 = r7.A03     // Catch:{ all -> 0x01d7 }
            boolean r0 = r0.isCancelled()     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x0121
            r6.A06(r7)     // Catch:{ all -> 0x01d7 }
            goto L_0x002a
        L_0x0121:
            int r0 = r6.A01     // Catch:{ all -> 0x01d7 }
            int r0 = r0 + r16
            r6.A01 = r0     // Catch:{ all -> 0x01d7 }
            X.0XV r0 = r7.A02     // Catch:{ all -> 0x01d7 }
            boolean r0 = r6.A0C(r0)     // Catch:{ all -> 0x01d7 }
            if (r0 == 0) goto L_0x013e
            r6.A0A = r11     // Catch:{ all -> 0x01d7 }
            java.util.Set r1 = r6.A08     // Catch:{ all -> 0x01d7 }
            if (r1 == 0) goto L_0x013a
            java.util.concurrent.ExecutorService r0 = r7.A05     // Catch:{ all -> 0x01d7 }
            r1.add(r0)     // Catch:{ all -> 0x01d7 }
        L_0x013a:
            r6.A06(r7)     // Catch:{ all -> 0x01d7 }
            goto L_0x0164
        L_0x013e:
            r6.A0A = r11     // Catch:{ all -> 0x01d7 }
            java.util.concurrent.atomic.AtomicInteger r0 = r6.A09     // Catch:{ all -> 0x01d7 }
            r0.incrementAndGet()     // Catch:{ all -> 0x01d7 }
            java.util.concurrent.locks.Condition r0 = r6.A0L     // Catch:{ all -> 0x01d7 }
            r0.signalAll()     // Catch:{ all -> 0x01d7 }
            if (r21 != 0) goto L_0x014d
            goto L_0x0153
        L_0x014d:
            java.util.concurrent.locks.Condition r0 = r6.A0N     // Catch:{ all -> 0x01d7 }
            r0.await()     // Catch:{ all -> 0x01d7 }
            goto L_0x0158
        L_0x0153:
            java.util.concurrent.locks.Condition r0 = r6.A0M     // Catch:{ all -> 0x01d7 }
            r0.await()     // Catch:{ all -> 0x01d7 }
        L_0x0158:
            java.util.concurrent.atomic.AtomicInteger r0 = r6.A09     // Catch:{ all -> 0x01d7 }
            r0.decrementAndGet()     // Catch:{ all -> 0x01d7 }
            java.util.concurrent.locks.Condition r0 = r6.A0L     // Catch:{ all -> 0x01d7 }
            r0.signalAll()     // Catch:{ all -> 0x01d7 }
            goto L_0x002a
        L_0x0164:
            java.util.concurrent.locks.ReentrantLock r0 = r6.A0O     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
            r0.unlock()     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
            X.0XY[] r0 = r6.A0H     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            r0[r20] = r7     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            java.util.concurrent.ExecutorService r2 = r7.A05     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            X.0XX r1 = r7.A03     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            r0 = -1730003143(0xffffffff98e23f39, float:-5.848347E-24)
            java.util.concurrent.Future r0 = X.AnonymousClass07A.A02(r2, r1, r0)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            r0.get()     // Catch:{ ExecutionException -> 0x019d }
            java.util.concurrent.atomic.AtomicBoolean r1 = r6.A0K     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            r0 = 1
            r1.set(r0)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            int r2 = X.AnonymousClass1Y3.Amu     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            X.0UN r1 = r6.A05     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            android.os.Handler r1 = (android.os.Handler) r1     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            r1.sendEmptyMessage(r11)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            java.util.Set r1 = r6.A08
            if (r1 == 0) goto L_0x0197
            java.util.concurrent.ExecutorService r0 = r7.A05
            r1.remove(r0)
        L_0x0197:
            X.0XY[] r0 = r6.A0H
            r0[r20] = r17
            goto L_0x001c
        L_0x019d:
            r3 = move-exception
            java.lang.Class r2 = X.AnonymousClass0Y6.A0T     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            java.lang.String r1 = "ExecutionException Error running appchoregrapher thread: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r18}     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            X.C010708t.A0E(r2, r3, r1, r0)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            java.lang.String r0 = r7.A04     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            r2 = r10
            if (r0 == 0) goto L_0x01af
            r2 = r0
        L_0x01af:
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            X.0UN r0 = r6.A05     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            X.09P r0 = (X.AnonymousClass09P) r0     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            r0.Bz2(r8, r2)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            java.lang.RuntimeException r0 = com.google.common.base.Throwables.propagate(r3)     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
            throw r0     // Catch:{ InterruptedException -> 0x01c5, IncompatibleClassChangeError -> 0x01c3, RuntimeException -> 0x01c1 }
        L_0x01c1:
            r3 = move-exception
            goto L_0x01e5
        L_0x01c3:
            r2 = move-exception
            goto L_0x01f4
        L_0x01c5:
            r3 = move-exception
            goto L_0x022b
        L_0x01c7:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x01d7 }
            java.lang.String r0 = "Cannot peek tasks because there is no queue"
            r1.<init>(r0)     // Catch:{ all -> 0x01d7 }
            goto L_0x01d6
        L_0x01cf:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x01d7 }
            java.lang.String r0 = "Cannot get priority queues"
            r1.<init>(r0)     // Catch:{ all -> 0x01d7 }
        L_0x01d6:
            throw r1     // Catch:{ all -> 0x01d7 }
        L_0x01d7:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantLock r0 = r6.A0O     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
            r0.unlock()     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
            throw r1     // Catch:{ InterruptedException -> 0x0228, IncompatibleClassChangeError -> 0x01f1, RuntimeException -> 0x01e2, all -> 0x01de }
        L_0x01de:
            r2 = move-exception
            r7 = r17
            goto L_0x023f
        L_0x01e2:
            r3 = move-exception
            r7 = r17
        L_0x01e5:
            java.lang.Class r2 = X.AnonymousClass0Y6.A0T     // Catch:{ all -> 0x023e }
            java.lang.String r1 = "Error running appchoregrapher thread: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r18}     // Catch:{ all -> 0x023e }
            X.C010708t.A0E(r2, r3, r1, r0)     // Catch:{ all -> 0x023e }
            throw r3     // Catch:{ all -> 0x023e }
        L_0x01f1:
            r2 = move-exception
            r7 = r17
        L_0x01f4:
            java.lang.Class r3 = X.AnonymousClass0Y6.A0T     // Catch:{ all -> 0x023e }
            java.lang.String r1 = "IncompatibleClassChangeError Error running appchoregrapher thread: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r18}     // Catch:{ all -> 0x023e }
            X.C010708t.A0E(r3, r2, r1, r0)     // Catch:{ all -> 0x023e }
            if (r7 == 0) goto L_0x0214
            java.lang.String r0 = r7.A04     // Catch:{ all -> 0x023e }
            if (r0 == 0) goto L_0x0206
            r10 = r0
        L_0x0206:
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x023e }
            X.0UN r0 = r6.A05     // Catch:{ all -> 0x023e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x023e }
            X.09P r0 = (X.AnonymousClass09P) r0     // Catch:{ all -> 0x023e }
            r0.Bz2(r8, r10)     // Catch:{ all -> 0x023e }
            goto L_0x0223
        L_0x0214:
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x023e }
            X.0UN r0 = r6.A05     // Catch:{ all -> 0x023e }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x023e }
            X.09P r1 = (X.AnonymousClass09P) r1     // Catch:{ all -> 0x023e }
            java.lang.String r0 = "Null task"
            r1.Bz2(r8, r0)     // Catch:{ all -> 0x023e }
        L_0x0223:
            java.lang.RuntimeException r0 = com.google.common.base.Throwables.propagate(r2)     // Catch:{ all -> 0x023e }
            throw r0     // Catch:{ all -> 0x023e }
        L_0x0228:
            r3 = move-exception
            r7 = r17
        L_0x022b:
            java.lang.Class r2 = X.AnonymousClass0Y6.A0T     // Catch:{ all -> 0x023e }
            java.lang.String r1 = "InterruptedException Error running appchoregrapher thread: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r18}     // Catch:{ all -> 0x023e }
            X.C010708t.A0E(r2, r3, r1, r0)     // Catch:{ all -> 0x023e }
            r19.interrupt()     // Catch:{ all -> 0x023e }
            java.lang.RuntimeException r0 = com.google.common.base.Throwables.propagate(r3)     // Catch:{ all -> 0x023e }
            throw r0     // Catch:{ all -> 0x023e }
        L_0x023e:
            r2 = move-exception
        L_0x023f:
            java.util.Set r1 = r6.A08
            if (r1 == 0) goto L_0x024a
            if (r7 == 0) goto L_0x024a
            java.util.concurrent.ExecutorService r0 = r7.A05
            r1.remove(r0)
        L_0x024a:
            X.0XY[] r0 = r6.A0H
            r0[r20] = r17
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Y6.A09(X.0Y6, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0064, code lost:
        if (r9 == X.AnonymousClass07B.A01) goto L_0x0066;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0A(java.lang.Integer r9) {
        /*
            r8 = this;
            java.lang.String r7 = "AppChoreographer Stage"
            java.util.concurrent.locks.ReentrantLock r0 = r8.A0O
            r0.lock()
            int r6 = r8.hashCode()     // Catch:{ all -> 0x0077 }
            java.lang.String r5 = A02(r9)     // Catch:{ all -> 0x0077 }
            r1 = 8
            boolean r0 = X.AnonymousClass08Z.A05(r1)     // Catch:{ all -> 0x0077 }
            if (r0 == 0) goto L_0x001c
            r3 = 0
            com.facebook.systrace.TraceDirect.asyncTraceStageBegin(r7, r6, r3, r5)     // Catch:{ all -> 0x0077 }
        L_0x001c:
            java.lang.Integer r0 = r8.A0S     // Catch:{ all -> 0x0077 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x0077 }
            switch(r0) {
                case 0: goto L_0x0062;
                case 1: goto L_0x005d;
                case 2: goto L_0x004c;
                case 3: goto L_0x002b;
                default: goto L_0x0025;
            }     // Catch:{ all -> 0x0077 }
        L_0x0025:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0077 }
            r0.<init>()     // Catch:{ all -> 0x0077 }
            throw r0     // Catch:{ all -> 0x0077 }
        L_0x002b:
            r2 = 10
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0077 }
            X.0UN r0 = r8.A05     // Catch:{ all -> 0x0077 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0077 }
            X.09P r3 = (X.AnonymousClass09P) r3     // Catch:{ all -> 0x0077 }
            java.lang.String r2 = "DefaultAppChoreographer_Already_Loaded"
            java.lang.String r1 = "AppChoreographer already loaded. Requested stage = "
            if (r9 == 0) goto L_0x0049
            java.lang.String r0 = A02(r9)     // Catch:{ all -> 0x0077 }
        L_0x0041:
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0077 }
            r3.CGS(r2, r0)     // Catch:{ all -> 0x0077 }
            goto L_0x0071
        L_0x0049:
            java.lang.String r0 = "null"
            goto L_0x0041
        L_0x004c:
            java.lang.Integer r3 = X.AnonymousClass07B.A0N     // Catch:{ all -> 0x0077 }
            r0 = 0
            if (r9 != r3) goto L_0x0052
            r0 = 1
        L_0x0052:
            com.google.common.base.Preconditions.checkArgument(r0)     // Catch:{ all -> 0x0077 }
            int r0 = r8.hashCode()     // Catch:{ all -> 0x0077 }
            X.AnonymousClass08Z.A02(r1, r7, r0)     // Catch:{ all -> 0x0077 }
            goto L_0x006c
        L_0x005d:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x0077 }
            if (r9 != r0) goto L_0x0068
            goto L_0x0066
        L_0x0062:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0077 }
            if (r9 != r0) goto L_0x0068
        L_0x0066:
            r0 = 1
            goto L_0x0069
        L_0x0068:
            r0 = 0
        L_0x0069:
            com.google.common.base.Preconditions.checkArgument(r0)     // Catch:{ all -> 0x0077 }
        L_0x006c:
            r8.A0S = r9     // Catch:{ all -> 0x0077 }
            A08(r8)     // Catch:{ all -> 0x0077 }
        L_0x0071:
            java.util.concurrent.locks.ReentrantLock r0 = r8.A0O
            r0.unlock()
            return
        L_0x0077:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantLock r0 = r8.A0O
            r0.unlock()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Y6.A0A(java.lang.Integer):void");
    }

    private boolean A0B() {
        if (((C25121Yk) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BTs, this.A05)).A05()) {
            return this.A0D;
        }
        A04();
        return !this.A0Q.isEmpty();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        if (0 != 0) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0087, code lost:
        if (r3 < 0) goto L_0x0089;
     */
    /* JADX WARNING: Removed duplicated region for block: B:2:0x000b A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A0C(X.AnonymousClass0XV r9) {
        /*
            r8 = this;
            java.lang.Integer r0 = r8.A0S
            int r0 = r0.intValue()
            r7 = 0
            r6 = 1
            switch(r0) {
                case 1: goto L_0x000c;
                case 2: goto L_0x00b2;
                case 3: goto L_0x0011;
                default: goto L_0x000b;
            }
        L_0x000b:
            return r7
        L_0x000c:
            X.0XV r0 = X.AnonymousClass0XV.STARTUP_INITIALIZATION
            if (r9 != r0) goto L_0x000b
            return r6
        L_0x0011:
            int r1 = r9.ordinal()
            X.0XV r0 = X.AnonymousClass0XV.APPLICATION_LOADING
            int r0 = r0.ordinal()
            if (r1 <= r0) goto L_0x00be
            int r2 = X.AnonymousClass1Y3.BTs
            X.0UN r1 = r8.A05
            r0 = 12
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yk r0 = (X.C25121Yk) r0
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x00a2
            boolean r0 = r8.A0D
        L_0x0031:
            if (r0 != 0) goto L_0x000b
            boolean r0 = r8.A0B()
            if (r0 != 0) goto L_0x000b
            int r2 = X.AnonymousClass1Y3.BT7
            X.0UN r1 = r8.A05
            r0 = 7
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0yu r0 = (X.C17440yu) r0
            boolean r0 = r0.A01
            if (r0 != 0) goto L_0x004b
            r0 = 0
            if (r7 == 0) goto L_0x004c
        L_0x004b:
            r0 = 1
        L_0x004c:
            if (r0 != 0) goto L_0x0089
            int r1 = X.AnonymousClass1Y3.B5j
            X.0UN r0 = r8.A05
            r5 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0Ud r0 = (X.AnonymousClass0Ud) r0
            boolean r0 = r0.A0H()
            if (r0 == 0) goto L_0x0089
            r2 = 6
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r8.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            long r3 = r0.now()
            int r1 = X.AnonymousClass1Y3.B5j
            X.0UN r0 = r8.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0Ud r0 = (X.AnonymousClass0Ud) r0
            long r0 = r0.A0E
            long r3 = r3 - r0
            r1 = 60000(0xea60, double:2.9644E-319)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0089
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r1 = 0
            if (r0 >= 0) goto L_0x008a
        L_0x0089:
            r1 = 1
        L_0x008a:
            if (r1 == 0) goto L_0x000b
            X.0XV r0 = X.AnonymousClass0XV.APPLICATION_LOADED_HIGH_PRIORITY
            if (r9 == r0) goto L_0x00be
            boolean r0 = r8.A0A
            if (r0 != 0) goto L_0x00be
            int r2 = X.AnonymousClass1Y3.Amu
            X.0UN r1 = r8.A05
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            android.os.Handler r1 = (android.os.Handler) r1
            r1.sendEmptyMessage(r7)
            return r7
        L_0x00a2:
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Akn
            X.0UN r0 = r8.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Z4 r0 = (X.AnonymousClass1Z4) r0
            boolean r0 = r0.A05()
            goto L_0x0031
        L_0x00b2:
            int r1 = r9.ordinal()
            X.0XV r0 = X.AnonymousClass0XV.APPLICATION_LOADING
            int r0 = r0.ordinal()
            if (r1 > r0) goto L_0x000b
        L_0x00be:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Y6.A0C(X.0XV):boolean");
    }

    public void ANq(Object obj) {
        C005505z.A03("DefaultAppChoreographer.addUiLoadingAsyncTask", -580937971);
        this.A0O.lock();
        try {
            this.A0Q.put(obj, Long.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AgK, this.A05)).now()));
            this.A0G = true;
            A08(this);
        } finally {
            this.A0O.unlock();
            C005505z.A00(-151932020);
        }
    }

    public boolean BDj() {
        Integer num = this.A0S;
        Integer num2 = AnonymousClass07B.A0N;
        boolean z = true;
        if (num == num2) {
            return true;
        }
        this.A0O.lock();
        try {
            A07(this);
            if (this.A0S != num2) {
                z = false;
            }
            return z;
        } finally {
            this.A0O.unlock();
        }
    }

    public boolean BFP() {
        if (((AnonymousClass0Ud) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B5j, this.A05)).A0D > 0) {
            return true;
        }
        return false;
    }

    public boolean BHL() {
        if (((C25121Yk) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BTs, this.A05)).A05()) {
            return ((C28461eq) AnonymousClass1XX.A02(11, AnonymousClass1Y3.Aq4, this.A05)).A0A();
        }
        this.A0O.lock();
        try {
            A04();
            return !this.A0Q.isEmpty();
        } finally {
            this.A0O.unlock();
        }
    }

    public void BM0() {
        A0A(AnonymousClass07B.A0C);
    }

    public void start() {
        this.A0O.lock();
        try {
            AnonymousClass00K A012 = AnonymousClass00J.A01((Context) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A6S, this.A05));
            boolean z = A012.A1V;
            this.A0C = z;
            int i = A012.A0R;
            if (i <= 0) {
                i = 1;
            }
            this.A02 = i;
            this.A00 = A012.A0E;
            this.A0E = A012.A21;
            if (z) {
                this.A06 = new HashMap(8);
                if (i <= 0) {
                    i = 1;
                }
                this.A08 = Collections.synchronizedSet(new HashSet(i));
                PriorityQueue priorityQueue = this.A07;
                this.A07 = null;
                Iterator it = priorityQueue.iterator();
                while (it.hasNext()) {
                    A05((AnonymousClass0XY) it.next());
                }
            }
            int i2 = AnonymousClass1Y3.BTs;
            if (((C25121Yk) AnonymousClass1XX.A02(12, i2, this.A05)).A05() || ((C25121Yk) AnonymousClass1XX.A02(12, i2, this.A05)).A03()) {
                ((C28461eq) AnonymousClass1XX.A02(11, AnonymousClass1Y3.Aq4, this.A05)).A04(this);
            }
            if (!((C25121Yk) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BTs, this.A05)).A03()) {
                ((AnonymousClass1Z4) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Akn, this.A05)).A03(new AnonymousClass0YR(this));
            }
            A0A(AnonymousClass07B.A01);
            int i3 = this.A02;
            if (i3 <= 0) {
                i3 = 1;
            }
            this.A0H = new AnonymousClass0XY[i3];
            this.A0I = (long[][]) Array.newInstance(long.class, i3, 2);
            new Thread(new AnonymousClass0YV(this), "AppChoreographer-main").start();
            if (this.A0C) {
                for (int i4 = 1; i4 < i3; i4++) {
                    new Thread(new C70953bZ(this, i4), AnonymousClass08S.A09("AppChoreographer-idle-", i4)).start();
                }
            }
            AnonymousClass00S.A04((Handler) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amu, this.A05), new AnonymousClass0YW(this), -2081403738);
        } finally {
            this.A0O.unlock();
        }
    }

    private AnonymousClass0Y6(AnonymousClass1XY r8) {
        this.A05 = new AnonymousClass0UN(14, r8);
        C05210Yb r2 = new C05210Yb(this);
        this.A0J = r2;
        this.A07 = new PriorityQueue(100, r2);
        this.A0Q = new WeakHashMap();
        ReentrantLock reentrantLock = new ReentrantLock();
        this.A0O = reentrantLock;
        this.A0M = reentrantLock.newCondition();
        this.A0N = this.A0O.newCondition();
        this.A0L = this.A0O.newCondition();
        this.A0R = new AtomicInteger();
        AnonymousClass08Z.A01(8, "AppChoreographer Stage", hashCode());
        int hashCode = hashCode();
        String A022 = A02(AnonymousClass07B.A00);
        if (AnonymousClass08Z.A05(8)) {
            TraceDirect.asyncTraceStageBegin("AppChoreographer Stage", hashCode, 0, A022);
        }
    }

    private ExecutorService A03(Integer num) {
        int i;
        int i2;
        String str;
        int intValue = num.intValue();
        switch (intValue) {
            case 0:
                i = 9;
                i2 = AnonymousClass1Y3.ADs;
                break;
            case 1:
                i = 8;
                i2 = AnonymousClass1Y3.Aoc;
                break;
            default:
                if (num == null) {
                    str = "null";
                } else if (1 - intValue != 0) {
                    str = "UI";
                } else {
                    str = "BACKGROUND";
                }
                throw new IllegalStateException(AnonymousClass08S.A0J("Unknown thread type ", str));
        }
        return (ExecutorService) AnonymousClass1XX.A02(i, i2, this.A05);
    }

    public void BQF(boolean z, boolean z2) {
        C005505z.A06("DefaultAppChoreographer.onBusyStateChanged(isBusy = %b, isInitialState = %b)", Boolean.valueOf(z), Boolean.valueOf(z2), 414683707);
        this.A0O.lock();
        if (!z2 || z) {
            try {
                this.A0D = z;
                if (z) {
                    this.A0G = true;
                } else {
                    A07(this);
                }
                A08(this);
            } finally {
                this.A0O.unlock();
                C005505z.A00(-876212976);
            }
        } else {
            this.A0O.unlock();
            C005505z.A00(-1287841805);
        }
    }

    public void C25(Object obj) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, this.A05)).BxR(new C83943yV(this, obj));
            return;
        }
        C005505z.A03("DefaultAppChoreographer.removeUiLoadingAsyncTask", -494991589);
        this.A0O.lock();
        try {
            this.A0Q.remove(obj);
            A07(this);
            A08(this);
        } finally {
            this.A0O.unlock();
            C005505z.A00(-1164959079);
        }
    }

    public AnonymousClass0XX CIG(String str, Runnable runnable, AnonymousClass0XV r10, Integer num) {
        return A01(str, runnable, null, r10, A03(num), -1);
    }

    public AnonymousClass0XX CIH(String str, Callable callable, AnonymousClass0XV r10, Integer num) {
        return A01(str, null, callable, r10, A03(num), -1);
    }
}
