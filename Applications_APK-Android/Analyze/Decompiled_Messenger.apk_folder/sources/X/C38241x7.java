package X;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1x7  reason: invalid class name and case insensitive filesystem */
public final class C38241x7 {
    public int A00;
    public Integer A01 = AnonymousClass07B.A00;
    public final C30195ErM A02;
    public final AD1 A03;
    public final C197179Pd A04;
    public final C21347AAe A05;
    public final ABO A06;
    public final String A07;
    public final String A08;
    public final Collection A09;
    public final AtomicBoolean A0A;

    public void A00(Integer num) {
        synchronized (this.A01) {
            if (this.A01 == AnonymousClass07B.A00) {
                this.A01 = num;
                this.A06.A02();
            }
        }
    }

    public C38241x7(ABO abo, Collection collection, AtomicBoolean atomicBoolean, C197179Pd r5, C21347AAe aAe, int i, String str, String str2, C30195ErM erM, AD1 ad1) {
        this.A06 = abo;
        this.A09 = collection;
        this.A0A = atomicBoolean;
        this.A04 = r5;
        this.A05 = aAe;
        this.A00 = i;
        this.A07 = str;
        this.A08 = str2;
        this.A02 = erM;
        this.A03 = ad1;
    }
}
