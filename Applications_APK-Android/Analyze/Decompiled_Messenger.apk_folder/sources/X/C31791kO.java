package X;

import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1kO  reason: invalid class name and case insensitive filesystem */
public final class C31791kO {
    public AnonymousClass0UN A00;
    @LoggedInUser
    public C04310Tq A01;
    private AnonymousClass1YI A02;
    private FbSharedPreferences A03;

    public static final C31791kO A00(AnonymousClass1XY r1) {
        return new C31791kO(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0045, code lost:
        if (((com.facebook.contacts.upload.ContactsUploadRunner) X.AnonymousClass1XX.A02(0, r1, r6.A00)).A03().A03 == X.AnonymousClass97t.NOT_STARTED) goto L_0x0047;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r6 = this;
            com.facebook.prefs.shared.FbSharedPreferences r0 = r6.A03
            X.1Y8 r4 = X.C186814r.A00
            r2 = 0
            boolean r0 = r0.Aep(r4, r2)
            if (r0 != 0) goto L_0x0061
            X.0Tq r0 = r6.A01
            java.lang.Object r0 = r0.get()
            r5 = 0
            if (r0 == 0) goto L_0x004d
            X.0Tq r0 = r6.A01
            java.lang.Object r0 = r0.get()
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0
            java.lang.String r0 = r0.A0l
            boolean r3 = X.C06850cB.A0B(r0)
            r0 = 1
            r3 = r3 ^ r0
            int r1 = X.AnonymousClass1Y3.AXT
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.contacts.upload.ContactsUploadRunner r0 = (com.facebook.contacts.upload.ContactsUploadRunner) r0
            com.facebook.contacts.interfaces.model.ContactsUploadState r0 = r0.A03()
            if (r0 == 0) goto L_0x0047
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.contacts.upload.ContactsUploadRunner r0 = (com.facebook.contacts.upload.ContactsUploadRunner) r0
            com.facebook.contacts.interfaces.model.ContactsUploadState r0 = r0.A03()
            X.97t r2 = r0.A03
            X.97t r1 = X.AnonymousClass97t.NOT_STARTED
            r0 = 1
            if (r2 != r1) goto L_0x0048
        L_0x0047:
            r0 = 0
        L_0x0048:
            if (r3 != 0) goto L_0x004c
            if (r0 == 0) goto L_0x004d
        L_0x004c:
            r5 = 1
        L_0x004d:
            if (r5 == 0) goto L_0x0061
            com.facebook.prefs.shared.FbSharedPreferences r0 = r6.A03
            X.1hn r2 = r0.edit()
            r1 = 1
            r2.putBoolean(r4, r1)
            X.1Y8 r0 = X.C186814r.A01
            r2.putBoolean(r0, r1)
            r2.commit()
        L_0x0061:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31791kO.A01():void");
    }

    public boolean A02() {
        if (this.A03.Aep(C186814r.A01, false) && this.A02.AbO(261, false)) {
            return true;
        }
        C30281hn edit = this.A03.edit();
        edit.C1B(C186814r.A01);
        edit.commit();
        return false;
    }

    public C31791kO(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A03 = FbSharedPreferencesModule.A00(r3);
        this.A01 = AnonymousClass0WY.A01(r3);
        this.A02 = AnonymousClass0WA.A00(r3);
    }
}
