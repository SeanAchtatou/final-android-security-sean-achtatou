package X;

import com.facebook.acra.config.StartupBlockingConfig;

/* renamed from: X.1jJ  reason: invalid class name and case insensitive filesystem */
public final class C31201jJ {
    public long A00 = AnonymousClass06A.A00.now();
    public Object A01;

    public Object A00() {
        if (AnonymousClass06A.A00.now() - this.A00 < StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS) {
            return this.A01;
        }
        return null;
    }

    public C31201jJ(Object obj) {
        this.A01 = obj;
    }
}
