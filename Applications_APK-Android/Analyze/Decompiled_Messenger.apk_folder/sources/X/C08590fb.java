package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fb  reason: invalid class name and case insensitive filesystem */
public final class C08590fb implements C08560fY {
    private static volatile C08590fb A01;
    public AnonymousClass0UN A00;

    public String AWk() {
        return "network_detailed_info";
    }

    public static final C08590fb A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C08590fb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C08590fb(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public long BL4() {
        return C08350fD.A0B;
    }

    public void BjZ(PerformanceLoggingEvent performanceLoggingEvent) {
        AnonymousClass123 AvW;
        int i = AnonymousClass1Y3.Ar4;
        AnonymousClass0UN r1 = this.A00;
        boolean z = false;
        if (((C16810xo) AnonymousClass1XX.A02(0, i, r1)) == null) {
            z = false;
        } else if (((C25401Zm) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKr, this.A00)).BzU(((C25401Zm) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKr, r1)).B1q(6094849)) != Integer.MAX_VALUE) {
            z = true;
        }
        if (z && (AvW = ((C16810xo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ar4, this.A00)).AvW()) != null) {
            performanceLoggingEvent.A09("network_info_rtt_avg", AvW.A0J);
            performanceLoggingEvent.A08("network_info_rtt_stddev", AvW.A07);
            performanceLoggingEvent.A07("network_info_network_changed", AvW.A03);
            performanceLoggingEvent.A07("network_info_celltower_changed", AvW.A02);
            performanceLoggingEvent.A09("network_info_opened_conn", AvW.A0G);
            performanceLoggingEvent.A04("network_info_signal_level", AvW.A0B.intValue());
            performanceLoggingEvent.A04("network_info_signal_dbm", AvW.A0A.intValue());
            performanceLoggingEvent.A04("network_info_frequency_mhz", AvW.A08.intValue());
            performanceLoggingEvent.A04("network_info_link_speed_mbps", AvW.A09.intValue());
            performanceLoggingEvent.A07("network_info_app_backgrounded", AvW.A01);
            performanceLoggingEvent.A09("network_info_ms_since_launch", AvW.A0F);
            performanceLoggingEvent.A09("network_info_ms_since_init", AvW.A0E);
            performanceLoggingEvent.A07("network_info_may_have_network", AvW.A04);
            performanceLoggingEvent.A09("network_info_req_bw_ingress_avg", AvW.A0I);
            performanceLoggingEvent.A08("network_info_req_bw_ingress_std_dev", AvW.A06);
            performanceLoggingEvent.A09("network_info_req_bw_egress_avg", AvW.A0H);
            performanceLoggingEvent.A08("network_info_req_bw_egress_std_dev", AvW.A05);
            performanceLoggingEvent.A0B("network_info_latency_quality", AvW.A0L);
            performanceLoggingEvent.A0B("network_info_upload_bw_quality", AvW.A0M);
            performanceLoggingEvent.A0B("network_info_download_bw_quality", AvW.A0K);
            performanceLoggingEvent.A09("estimated_ttfb_ms", AvW.A0D);
            performanceLoggingEvent.A09("estimated_bandwidth_bps", AvW.A0C);
        }
    }

    private C08590fb(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A01;
    }
}
