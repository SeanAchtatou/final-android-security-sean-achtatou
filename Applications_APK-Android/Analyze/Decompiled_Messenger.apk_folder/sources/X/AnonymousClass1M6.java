package X;

import android.os.Build;
import android.text.TextUtils;
import android.view.View;

/* renamed from: X.1M6  reason: invalid class name */
public abstract class AnonymousClass1M6 {
    public final int A00;
    private final int A01;
    private final Class A02;

    public Object A01(View view) {
        boolean isAccessibilityHeading;
        if (this instanceof C29691gp) {
            isAccessibilityHeading = view.isAccessibilityHeading();
        } else if (this instanceof C15620va) {
            return view.getAccessibilityPaneTitle();
        } else {
            isAccessibilityHeading = view.isScreenReaderFocusable();
        }
        return Boolean.valueOf(isAccessibilityHeading);
    }

    public void A03(View view, Object obj) {
        if (this instanceof C29691gp) {
            view.setAccessibilityHeading(((Boolean) obj).booleanValue());
        } else if (!(this instanceof C15620va)) {
            view.setScreenReaderFocusable(((Boolean) obj).booleanValue());
        } else {
            view.setAccessibilityPaneTitle((CharSequence) obj);
        }
    }

    public Object A00(View view) {
        int i = Build.VERSION.SDK_INT;
        boolean z = false;
        if (i >= this.A00) {
            z = true;
        }
        if (z) {
            return A01(view);
        }
        boolean z2 = false;
        if (i >= 19) {
            z2 = true;
        }
        if (!z2) {
            return null;
        }
        Object tag = view.getTag(this.A01);
        if (this.A02.isInstance(tag)) {
            return tag;
        }
        return null;
    }

    public void A02(View view, Object obj) {
        int i = Build.VERSION.SDK_INT;
        boolean z = false;
        if (i >= this.A00) {
            z = true;
        }
        if (z) {
            A03(view, obj);
            return;
        }
        boolean z2 = false;
        if (i >= 19) {
            z2 = true;
        }
        if (z2 && A04(A00(view), obj)) {
            C15320v6.getOrCreateAccessibilityDelegateCompat(view);
            view.setTag(this.A01, obj);
            C15320v6.notifyViewAccessibilityStateChangedIfNeeded(view, 0);
        }
    }

    public boolean A04(Object obj, Object obj2) {
        boolean booleanValue;
        boolean booleanValue2;
        boolean equals;
        if (!(this instanceof C29691gp)) {
            if (this instanceof C15620va) {
                equals = TextUtils.equals((CharSequence) obj, (CharSequence) obj2);
            } else if (!(this instanceof AnonymousClass1L8)) {
                equals = obj2.equals(obj);
            }
            return !equals;
        }
        Boolean bool = (Boolean) obj;
        Boolean bool2 = (Boolean) obj2;
        boolean z = false;
        if (bool == null) {
            booleanValue = false;
        } else {
            booleanValue = bool.booleanValue();
        }
        if (bool2 == null) {
            booleanValue2 = false;
        } else {
            booleanValue2 = bool2.booleanValue();
        }
        if (booleanValue == booleanValue2) {
            z = true;
        }
        return !z;
    }

    public AnonymousClass1M6(int i, Class cls, int i2) {
        this.A01 = i;
        this.A02 = cls;
        this.A00 = i2;
    }
}
