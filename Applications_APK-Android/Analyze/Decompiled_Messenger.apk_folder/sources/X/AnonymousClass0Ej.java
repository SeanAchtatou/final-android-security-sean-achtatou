package X;

import android.content.Context;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0Ej  reason: invalid class name */
public final class AnonymousClass0Ej {
    public boolean A00 = false;
    public final Context A01;
    public final AnonymousClass0Ep A02;
    private final AnonymousClass0XQ A03;

    public synchronized void A04() {
        synchronized (this) {
            if (!this.A00) {
                A01();
            }
            this.A00 = true;
        }
    }

    private Set A00() {
        int size;
        HashSet hashSet = new HashSet();
        C05770aI A002 = this.A03.A00("AppModules::Uninstall");
        C05770aI.A01(A002);
        synchronized (A002.A02) {
            size = A002.A04.size();
        }
        if (size > 0) {
            AnonymousClass0GP.A00(this.A01);
            for (int i = 0; i < 10; i++) {
                String A022 = C02420Er.A02(i);
                if (A002.A0B(A022, false)) {
                    hashSet.add(A022);
                }
            }
        }
        return hashSet;
    }

    private void A02() {
        AnonymousClass16O A06 = this.A03.A00("AppModules::InitialInstallRequestTimestamp").A06();
        AnonymousClass16O.A04(A06);
        A06.A01 = true;
        A06.A05();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0091, code lost:
        if (r0 != false) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d6, code lost:
        if (r27.contains(r11.A01) == false) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0042, code lost:
        if (r14.groupCount() != 2) goto L_0x0044;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.AnonymousClass0Ej r27, java.util.Set r28, java.util.Set r29) {
        /*
            r8 = r27
            android.content.Context r0 = r8.A01
            X.AnonymousClass0GP.A00(r0)
            X.0Eq r26 = X.AnonymousClass0Eq.A00()
            r1 = 10
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r1)
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r1)
            X.0Q9 r11 = new X.0Q9
            r11.<init>()
            X.0Ep r0 = r8.A02
            java.io.File r10 = r0.A00
            java.lang.String[] r9 = r10.list()
            r27 = r29
            if (r9 == 0) goto L_0x00f2
            int r5 = r9.length
            r4 = 0
        L_0x002a:
            if (r4 >= r5) goto L_0x00f2
            r2 = r9[r4]
            java.util.regex.Pattern r0 = r11.A04
            java.util.regex.Matcher r14 = r0.matcher(r2)
            boolean r0 = r14.find()
            r13 = 2
            r3 = 0
            r15 = 1
            if (r0 == 0) goto L_0x0044
            int r12 = r14.groupCount()
            r0 = 1
            if (r12 == r13) goto L_0x0045
        L_0x0044:
            r0 = 0
        L_0x0045:
            r11.A03 = r0
            r12 = 0
            if (r0 == 0) goto L_0x00ef
            java.lang.String r0 = r14.group(r15)
        L_0x004e:
            r11.A01 = r0
            boolean r0 = r11.A03
            if (r0 == 0) goto L_0x0058
            java.lang.String r12 = r14.group(r13)
        L_0x0058:
            r11.A00 = r12
            java.lang.String r0 = "installed"
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x006a
            java.lang.String r0 = "usage_log"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006b
        L_0x006a:
            r3 = 1
        L_0x006b:
            r11.A02 = r3
            java.lang.String r3 = r11.A01
            r0 = r28
            boolean r0 = r0.contains(r3)
            if (r0 != 0) goto L_0x0093
            boolean r0 = r11.A02
            if (r0 != 0) goto L_0x00ed
            boolean r0 = r11.A03
            if (r0 == 0) goto L_0x008f
            java.lang.String r3 = r11.A00
            java.lang.String r0 = "0"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00ed
            boolean r0 = r11.A00()
            if (r0 != 0) goto L_0x00ed
        L_0x008f:
            r0 = 1
        L_0x0090:
            r13 = 0
            if (r0 == 0) goto L_0x0094
        L_0x0093:
            r13 = 1
        L_0x0094:
            if (r13 == 0) goto L_0x009e
            java.io.File r0 = new java.io.File
            r0.<init>(r10, r2)
            X.AnonymousClass0Ep.A01(r0)
        L_0x009e:
            java.lang.String r0 = r11.A01
            int r12 = X.AnonymousClass0GL.A00(r0)
            if (r12 < 0) goto L_0x00a9
            r6.set(r12)
        L_0x00a9:
            boolean r0 = r11.A00()
            if (r0 == 0) goto L_0x00e6
            X.0Ep r3 = r8.A02
            java.lang.String r2 = r11.A01
            java.lang.String r0 = r11.A00
            java.io.File r3 = r3.A02(r2, r0)
            java.lang.String r2 = r11.A01
            r0 = r27
            boolean r0 = r0.contains(r2)
            if (r0 == 0) goto L_0x00c6
            r3.delete()
        L_0x00c6:
            if (r13 != 0) goto L_0x00d8
            boolean r0 = r3.exists()
            if (r0 != 0) goto L_0x00ea
            java.lang.String r2 = r11.A01
            r0 = r27
            boolean r0 = r0.contains(r2)
            if (r0 != 0) goto L_0x00ea
        L_0x00d8:
            java.lang.Integer r3 = X.AnonymousClass07B.A0N
        L_0x00da:
            X.0Eq r2 = X.AnonymousClass0Eq.A00()
            java.lang.String r0 = r11.A01
            r2.A05(r0, r3)
            r7.set(r12)
        L_0x00e6:
            int r4 = r4 + 1
            goto L_0x002a
        L_0x00ea:
            java.lang.Integer r3 = X.AnonymousClass07B.A0C
            goto L_0x00da
        L_0x00ed:
            r0 = 0
            goto L_0x0090
        L_0x00ef:
            r0 = r12
            goto L_0x004e
        L_0x00f2:
            X.0XQ r2 = r8.A03
            java.lang.String r0 = "AppModules::PrevDownload"
            X.0aI r11 = r2.A00(r0)
            java.lang.String r25 = "key::PrevDownloadInit"
            r2 = 0
            r0 = r25
            boolean r24 = r11.A0B(r0, r2)
            X.16O r10 = r11.A06()
            X.0XQ r2 = r8.A03
            java.lang.String r0 = "AppModules::Uninstall"
            X.0aI r23 = r2.A00(r0)
            r8.A02()
            X.0XQ r2 = r8.A03
            java.lang.String r0 = "AppModules::InitialInstallRequestTs"
            X.0aI r22 = r2.A00(r0)
            X.16O r21 = r22.A06()
            X.0XQ r2 = r8.A03
            java.lang.String r0 = "AppModules::InstallLatency"
            X.0aI r9 = r2.A00(r0)
            X.16O r5 = r9.A06()
            long r19 = java.lang.System.currentTimeMillis()
            r4 = 0
            r18 = 0
            r17 = 0
            r3 = 0
        L_0x0134:
            if (r4 >= r1) goto L_0x01f8
            java.lang.String r2 = X.C02420Er.A02(r4)
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            boolean r0 = r7.get(r4)
            if (r0 != 0) goto L_0x0151
            android.content.Context r0 = r8.A01
            boolean r0 = X.AnonymousClass0GS.A01(r2, r0)
            if (r0 == 0) goto L_0x014c
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
        L_0x014c:
            r12 = r26
            r12.A04(r4, r1)
        L_0x0151:
            boolean r0 = r11.A0A(r2)
            r13 = 0
            r12 = r13
            if (r0 == 0) goto L_0x0162
            r0 = 0
            boolean r0 = r11.A0B(r2, r0)
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r0)
        L_0x0162:
            boolean r16 = r6.get(r4)
            java.lang.String r15 = "BackgroundInitializer"
            if (r16 == 0) goto L_0x017c
            if (r12 != 0) goto L_0x017c
            if (r24 == 0) goto L_0x017c
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.Object[] r14 = new java.lang.Object[]{r2, r0}
            java.lang.String r0 = "Module %s has download but prev download pref not set (hasPref=%b)"
            X.C010708t.A0P(r15, r0, r14)
        L_0x017c:
            r0 = r28
            boolean r0 = r0.contains(r2)
            r14 = 0
            if (r0 != 0) goto L_0x01f3
            r0 = r23
            boolean r0 = r0.A0B(r2, r14)
            if (r0 != 0) goto L_0x01f3
            if (r16 == 0) goto L_0x0194
            r0 = 1
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r0)
        L_0x0194:
            if (r13 == 0) goto L_0x01a4
            boolean r0 = r13.equals(r12)
            if (r0 != 0) goto L_0x01a4
            boolean r0 = r13.booleanValue()
            r10.A0D(r2, r0)
            r3 = 1
        L_0x01a4:
            r0 = r28
            boolean r0 = r0.contains(r2)
            if (r0 != 0) goto L_0x01c0
            boolean r0 = r7.get(r4)
            if (r0 != 0) goto L_0x01cc
            r0 = r27
            boolean r0 = r0.contains(r2)
            if (r0 != 0) goto L_0x01cc
            boolean r0 = r9.A0A(r2)
            if (r0 == 0) goto L_0x01cc
        L_0x01c0:
            r0 = r21
            r0.A08(r2)
            r5.A08(r2)
            r18 = 1
            r17 = 1
        L_0x01cc:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            if (r1 != r0) goto L_0x01ed
            boolean r0 = r9.A0A(r2)
            if (r0 != 0) goto L_0x01ed
            r0 = r22
            boolean r0 = r0.A0A(r2)
            if (r0 == 0) goto L_0x01ed
            r0 = 0
            r12 = r22
            long r12 = r12.A05(r2, r0)
            long r0 = r19 - r12
            r5.A0A(r2, r0)
            r17 = 1
        L_0x01ed:
            int r4 = r4 + 1
            r1 = 10
            goto L_0x0134
        L_0x01f3:
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r14)
            goto L_0x0194
        L_0x01f8:
            if (r18 == 0) goto L_0x01fd
            r21.A05()
        L_0x01fd:
            if (r17 == 0) goto L_0x0202
            r5.A05()
        L_0x0202:
            if (r24 != 0) goto L_0x020a
            r3 = 1
            r0 = r25
            r10.A0D(r0, r3)
        L_0x020a:
            if (r3 == 0) goto L_0x020f
            r10.A06()
        L_0x020f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Ej.A03(X.0Ej, java.util.Set, java.util.Set):void");
    }

    public AnonymousClass0Ej(Context context, AnonymousClass0XQ r3, AnonymousClass0Ep r4) {
        this.A01 = context;
        this.A03 = r3;
        this.A02 = r4;
    }

    private void A01() {
        Set<String> A002 = A00();
        Set A003 = AnonymousClass0Q4.A00(this.A01);
        A002.removeAll(A003);
        A03(this, A002, A003);
        if (!A002.isEmpty()) {
            AnonymousClass16O A06 = this.A03.A00("AppModules::Uninstall").A06();
            for (String A08 : A002) {
                A06.A08(A08);
            }
            A06.A06();
        }
    }
}
