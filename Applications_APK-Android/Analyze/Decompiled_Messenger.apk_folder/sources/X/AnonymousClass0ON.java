package X;

import android.content.Intent;
import android.text.TextUtils;
import com.facebook.rti.push.service.FbnsService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.0ON  reason: invalid class name */
public final class AnonymousClass0ON extends AnonymousClass0AR {
    public static final List A01 = new ArrayList(AnonymousClass0A9.A00);
    public final FbnsService A00;

    public AnonymousClass0ON(FbnsService fbnsService, C012309k r8, AnonymousClass0AS r9) {
        super(fbnsService, r8, r9, C01430Ae.A00(fbnsService.A0I()), AnonymousClass07B.A00);
        this.A00 = fbnsService;
    }

    public void A06(String str, Intent intent) {
        FbnsService fbnsService = this.A00;
        String str2 = intent.getPackage();
        AnonymousClass0RS r2 = fbnsService.A01;
        Map A002 = C01740Bl.A00("event_type", AnonymousClass0RT.A00(AnonymousClass07B.A0q));
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            A002.put("dpn", str2);
        }
        AnonymousClass0RS.A01(r2, "fbns_message_event", A002);
        fbnsService.A02.BIo(AnonymousClass08S.A0S("Redeliver Notif: notifId = ", str, "; target = ", str2));
    }

    public void A07(String str, String str2, AnonymousClass0SR r8) {
        FbnsService fbnsService = this.A00;
        FbnsService.A06(fbnsService, str, str2, r8);
        String r4 = r8.toString();
        AnonymousClass0RS r3 = fbnsService.A01;
        Map A002 = C01740Bl.A00("event_type", AnonymousClass0RT.A00(AnonymousClass07B.A0N));
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            A002.put("dpn", str2);
        }
        A002.put("result", r4);
        AnonymousClass0RS.A01(r3, "fbns_message_event", A002);
        fbnsService.A02.BIo(AnonymousClass08S.A0J("Error: Fail to deliver notifId = ", str));
    }

    public boolean A08(AnonymousClass0SE r5) {
        AnonymousClass0SR A012 = A01(this, r5.A00);
        if (A012.A00()) {
            A03().A01(r5.A01);
            A07(r5.A01, r5.A00.getPackage(), A012);
        } else if (!A012.A01()) {
            FbnsService.A06(this.A00, r5.A01, r5.A00.getPackage(), A012);
        }
        return A012.A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004e, code lost:
        if (X.AnonymousClass0A9.A01.contains(r5) != false) goto L_0x0050;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass0SR A01(X.AnonymousClass0ON r6, android.content.Intent r7) {
        /*
            java.lang.String r5 = r7.getPackage()
            java.lang.String r1 = r7.getAction()
            java.lang.String r0 = "com.facebook.rti.fbns.intent.RECEIVE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00aa
            X.09k r3 = r6.A05
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 == 0) goto L_0x0033
            X.0SR r4 = X.AnonymousClass0SR.A0A
        L_0x001a:
            X.0SR r0 = X.AnonymousClass0SR.A0D
            if (r4 == r0) goto L_0x007b
            com.facebook.rti.push.service.FbnsService r3 = r6.A00
            java.lang.String r2 = r4.name()
            X.0RS r1 = r3.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            r1.A04(r0, r2, r5)
            X.0OO r1 = r3.A02
            java.lang.String r0 = "Error: isTrusted() failed"
            r1.BIo(r0)
            return r4
        L_0x0033:
            android.content.Context r0 = r3.A00
            java.lang.String r0 = r0.getPackageName()
            boolean r0 = r5.equals(r0)
            if (r0 != 0) goto L_0x0078
            java.util.List r0 = X.AnonymousClass0A9.A00
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x0050
            java.util.List r0 = X.AnonymousClass0A9.A01
            boolean r1 = r0.contains(r5)
            r0 = 0
            if (r1 == 0) goto L_0x0051
        L_0x0050:
            r0 = 1
        L_0x0051:
            if (r0 != 0) goto L_0x0056
            X.0SR r4 = X.AnonymousClass0SR.A09
            goto L_0x001a
        L_0x0056:
            android.content.Context r2 = r3.A00
            r1 = 64
            X.07y r0 = r3.A01
            X.0AC r0 = X.AnonymousClass0AB.A00(r2, r5, r1, r0)
            java.lang.Integer r0 = r0.A02
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x006c;
                case 2: goto L_0x006f;
                case 3: goto L_0x0072;
                case 4: goto L_0x0069;
                case 5: goto L_0x0075;
                case 6: goto L_0x0078;
                default: goto L_0x0069;
            }
        L_0x0069:
            X.0SR r4 = X.AnonymousClass0SR.A08
            goto L_0x001a
        L_0x006c:
            X.0SR r4 = X.AnonymousClass0SR.A0B
            goto L_0x001a
        L_0x006f:
            X.0SR r4 = X.AnonymousClass0SR.A07
            goto L_0x001a
        L_0x0072:
            X.0SR r4 = X.AnonymousClass0SR.A0E
            goto L_0x001a
        L_0x0075:
            X.0SR r4 = X.AnonymousClass0SR.A0C
            goto L_0x001a
        L_0x0078:
            X.0SR r4 = X.AnonymousClass0SR.A0D
            goto L_0x001a
        L_0x007b:
            X.09k r2 = r6.A05
            r7.setPackage(r5)
            X.07y r1 = r2.A01
            android.content.Context r0 = r2.A00
            r2.A01(r7)
            boolean r0 = r1.A09(r0, r7)
            if (r0 == 0) goto L_0x00a7
            X.0SR r4 = X.AnonymousClass0SR.A02
        L_0x008f:
            boolean r0 = r4.A01()
            if (r0 != 0) goto L_0x00a6
            com.facebook.rti.push.service.FbnsService r3 = r6.A00
            X.0RS r2 = r3.A01
            java.lang.Integer r1 = X.AnonymousClass07B.A0p
            r0 = 0
            r2.A04(r1, r0, r5)
            X.0OO r1 = r3.A02
            java.lang.String r0 = "Error: secure-broadcast failed"
            r1.BIo(r0)
        L_0x00a6:
            return r4
        L_0x00a7:
            X.0SR r4 = X.AnonymousClass0SR.A01
            goto L_0x008f
        L_0x00aa:
            X.0SR r0 = X.AnonymousClass0SR.A04
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0ON.A01(X.0ON, android.content.Intent):X.0SR");
    }

    public long A05(String str, String str2, boolean z) {
        AnonymousClass0SR r2;
        long A05 = super.A05(str, str2, z);
        FbnsService fbnsService = this.A00;
        if (z) {
            r2 = AnonymousClass0SR.A05;
        } else {
            r2 = AnonymousClass0SR.A0F;
        }
        FbnsService.A06(fbnsService, str, str2, r2);
        AnonymousClass0RS r6 = fbnsService.A01;
        Map A002 = C01740Bl.A00("event_type", AnonymousClass0RT.A00(AnonymousClass07B.A0Y));
        if (!TextUtils.isEmpty(str)) {
            A002.put("event_extra_info", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            A002.put("dpn", str2);
        }
        A002.put("delivery_delay", String.valueOf(A05));
        AnonymousClass0RS.A01(r6, "fbns_message_event", A002);
        AnonymousClass0OO r4 = fbnsService.A02;
        r4.BIo("ACK from " + str2 + ": notifId = " + str + "; delay = " + A05);
        return A05;
    }
}
