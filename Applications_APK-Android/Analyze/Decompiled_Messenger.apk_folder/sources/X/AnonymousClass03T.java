package X;

import com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger;

/* renamed from: X.03T  reason: invalid class name */
public final class AnonymousClass03T implements AnonymousClass03a {
    public void Bse() {
        if (AnonymousClass08Z.A05(34359738368L)) {
            AnonymousClass08Z.A01(34359738368L, "CLASS_LOAD_TRACE", 0);
            ClassTracingLogger.sSystraceEnabled = true;
            ClassTracingLogger.initialize();
        }
    }

    public void Bsf() {
        if (AnonymousClass08Z.A05(34359738368L)) {
            ClassTracingLogger.sSystraceEnabled = false;
            ClassTracingLogger.initialize();
            AnonymousClass08Z.A02(34359738368L, "CLASS_LOAD_TRACE", 0);
        }
    }
}
