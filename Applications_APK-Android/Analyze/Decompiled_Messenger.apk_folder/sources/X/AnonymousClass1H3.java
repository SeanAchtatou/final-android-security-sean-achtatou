package X;

import android.net.Uri;
import com.facebook.user.model.UserKey;
import com.facebook.user.profilepic.PicSquare;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1H3  reason: invalid class name */
public final class AnonymousClass1H3 implements AnonymousClass1H4 {
    private final int A00;
    private final Uri A01;
    private final Uri A02;
    private final PicSquare A03;
    private final C21391Gt A04;
    private final C21381Gs A05;
    private final ImmutableList A06;
    private final ImmutableList A07;
    private final ImmutableList A08;
    private final boolean A09;

    public Uri AmV(int i, int i2, int i3) {
        UserKey userKey;
        if (i < 0 || i >= this.A07.size()) {
            return null;
        }
        if (this.A02 != null && i == 0) {
            return this.A01;
        }
        C21391Gt r1 = this.A04;
        if (r1 == null || (userKey = (UserKey) this.A07.get(i)) == null || !userKey.A08()) {
            return null;
        }
        return C21391Gt.A01(r1);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1H3 r5 = (AnonymousClass1H3) obj;
            if (this.A09 != r5.A09 || this.A00 != r5.A00 || !Objects.equal(this.A04, r5.A04) || !Objects.equal(this.A02, r5.A02) || !Objects.equal(this.A01, r5.A01) || !Objects.equal(this.A03, r5.A03) || !Objects.equal(this.A07, r5.A07) || this.A05 != r5.A05) {
                return false;
            }
            ImmutableList immutableList = this.A06;
            if (!Objects.equal(immutableList, immutableList) || !Objects.equal(this.A08, r5.A08)) {
                return false;
            }
        }
        return true;
    }

    public String Akc() {
        if (this.A06.isEmpty()) {
            return null;
        }
        return (String) this.A06.get(0);
    }

    public ImmutableList Akd() {
        return this.A06;
    }

    public int Aw7() {
        if (this.A09) {
            return 0;
        }
        if (this.A02 == null && this.A03 == null) {
            return this.A07.size();
        }
        return 1;
    }

    public C21381Gs B6D() {
        return this.A05;
    }

    public int B6P() {
        return this.A00;
    }

    public ImmutableList B8C() {
        return this.A07;
    }

    public boolean BHA() {
        if (this.A02 != null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int A042 = AnonymousClass07K.A04(this.A04, this.A02, this.A01, this.A03, Boolean.valueOf(this.A09));
        return ((A042 + 31) * 31) + AnonymousClass07K.A03(this.A07, Integer.valueOf(this.A05.ordinal()), this.A06, Integer.valueOf(this.A00));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        if (r4 != null) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1H3(X.C21391Gt r3, android.net.Uri r4, android.net.Uri r5, com.facebook.user.profilepic.PicSquare r6, boolean r7, com.google.common.collect.ImmutableList r8, X.C21381Gs r9, com.google.common.collect.ImmutableList r10, int r11) {
        /*
            r2 = this;
            r2.<init>()
            if (r3 != 0) goto L_0x0008
            r1 = 0
            if (r4 == 0) goto L_0x0009
        L_0x0008:
            r1 = 1
        L_0x0009:
            java.lang.String r0 = "userTileViewLogic or singleImageUri should not be NULL"
            com.google.common.base.Preconditions.checkState(r1, r0)
            r2.A04 = r3
            r2.A02 = r4
            r2.A01 = r5
            r2.A03 = r6
            r2.A09 = r7
            r2.A07 = r8
            r2.A05 = r9
            r2.A06 = r10
            r2.A00 = r11
            if (r3 == 0) goto L_0x0029
            com.google.common.collect.ImmutableList r0 = r3.A07(r8)
        L_0x0026:
            r2.A08 = r0
            return
        L_0x0029:
            r0 = 0
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1H3.<init>(X.1Gt, android.net.Uri, android.net.Uri, com.facebook.user.profilepic.PicSquare, boolean, com.google.common.collect.ImmutableList, X.1Gs, com.google.common.collect.ImmutableList, int):void");
    }

    public Uri ApX(int i, int i2, int i3) {
        C21391Gt r1;
        AnonymousClass1JY A032;
        boolean z = false;
        if (Aw7() > 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        Uri uri = this.A02;
        if (uri != null) {
            return uri;
        }
        Preconditions.checkNotNull(this.A04, "mUserTileViewLogic should not be NULL, if mSingleImageUri is NULL");
        PicSquare picSquare = this.A03;
        if (picSquare != null) {
            r1 = this.A04;
            A032 = AnonymousClass1JY.A06(picSquare);
        } else {
            r1 = this.A04;
            A032 = AnonymousClass1JY.A03((UserKey) this.A07.get(i));
        }
        return r1.A05(A032, i2, i3);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("mSingleImageUri", this.A02);
        stringHelper.add("mSingleImageFallbackUri", this.A01);
        stringHelper.add("mPicSquare", this.A03);
        stringHelper.add("mOnlyShowPlaceholder", this.A09);
        stringHelper.add("mTileUserKeys", this.A07);
        stringHelper.add("mTileBadge", this.A05);
        stringHelper.add("mDisplayNames", this.A06);
        stringHelper.add("mTintColor", this.A00);
        stringHelper.add("mUsersProfilePicState", this.A08);
        return stringHelper.toString();
    }
}
