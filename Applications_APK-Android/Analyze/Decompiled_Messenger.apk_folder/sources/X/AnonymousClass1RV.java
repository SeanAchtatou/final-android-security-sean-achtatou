package X;

import android.os.SystemClock;

/* renamed from: X.1RV  reason: invalid class name */
public final class AnonymousClass1RV implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.imagepipeline.producers.JobScheduler$1";
    public final /* synthetic */ AnonymousClass1RU A00;

    public AnonymousClass1RV(AnonymousClass1RU r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass1NY r4;
        int i;
        AnonymousClass1RU r5 = this.A00;
        long uptimeMillis = SystemClock.uptimeMillis();
        synchronized (r5) {
            r4 = r5.A03;
            i = r5.A00;
            r5.A03 = null;
            r5.A00 = 0;
            r5.A04 = AnonymousClass07B.A0C;
            r5.A01 = uptimeMillis;
        }
        try {
            if (AnonymousClass1RU.A02(r4, i)) {
                r5.A05.C47(r4, i);
            }
        } finally {
            AnonymousClass1NY.A04(r4);
            AnonymousClass1RU.A01(r5);
        }
    }
}
