package X;

import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.1qj  reason: invalid class name and case insensitive filesystem */
public final class C35131qj implements C04310Tq {
    public final /* synthetic */ C35091qf A00;

    public C35131qj(C35091qf r1) {
        this.A00 = r1;
    }

    public Object get() {
        return (ScheduledExecutorService) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADt, this.A00.A00);
    }
}
