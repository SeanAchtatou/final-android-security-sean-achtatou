package X;

import android.content.res.Resources;
import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.analytics.reliability.AggregatedReliabilityLogger;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.send.SendError;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1tT  reason: invalid class name and case insensitive filesystem */
public final class C36641tT implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.tincan.messenger.TincanMessengerErrorGenerator";
    public final BlueServiceOperationFactory A00;
    public final AnonymousClass18M A01;
    public final C36521tG A02;
    public final C30441i3 A03;
    public final C193917y A04;
    public final C04310Tq A05;
    private final Resources A06;
    private final AggregatedReliabilityLogger A07;
    private final C189216c A08;

    public static final C36641tT A00(AnonymousClass1XY r10) {
        return new C36641tT(C04490Ux.A0L(r10), C36521tG.A03(r10), C189216c.A02(r10), C30111hV.A00(r10), C193917y.A00(r10), C30441i3.A04(r10), AnonymousClass0VG.A00(AnonymousClass1Y3.BC1, r10), AnonymousClass18M.A00(r10), AggregatedReliabilityLogger.A00(r10));
    }

    public Message A01(Message message, String str) {
        C196899Nw r2 = new C196899Nw();
        r2.A02 = C36891u4.A0G;
        r2.A06 = str;
        r2.A01(Long.valueOf(message.A02));
        SendError sendError = new SendError(r2);
        AnonymousClass1TG A002 = Message.A00();
        A002.A03(message);
        AnonymousClass1V7 r22 = AnonymousClass1V7.A0A;
        A002.A0C = r22;
        A002.A0R = sendError;
        this.A02.A0N(message.A0q, r22);
        this.A02.A0O(message.A0q, sendError);
        this.A07.A06(message, "f");
        return A002.A00();
    }

    public void A02(ThreadKey threadKey, String str, Integer num, String str2) {
        C24971Xv it = this.A03.A0D(threadKey).A01.iterator();
        while (it.hasNext()) {
            this.A01.A07(false, ((Message) it.next()).A0q, 0, 0, num, str2, null);
        }
        ImmutableList immutableList = this.A03.A0D(threadKey).A01;
        for (int size = immutableList.size() - 1; size >= 0; size--) {
            Message message = (Message) immutableList.get(size);
            boolean z = false;
            if (message.A0D == AnonymousClass1V7.A0J) {
                z = true;
            }
            Preconditions.checkState(z);
            ((C14800u9) this.A05.get()).A01.A0Z(A01(message, str), true);
        }
        String string = this.A06.getString(2131821338);
        Bundle bundle = new Bundle();
        bundle.putParcelable("thread_key", threadKey);
        bundle.putString("message", string);
        this.A00.newInstance("TincanAdminMessage", bundle, 1, CallerContext.A04(C60952y6.class)).CGe();
        FetchThreadResult A012 = this.A04.A01(threadKey, 0);
        if (A012 != FetchThreadResult.A09) {
            ((C14800u9) this.A05.get()).A0H(A012);
        }
        this.A08.A0O("TincanMessengerErrorGenerator");
        this.A08.A0G(threadKey, "TincanMessengerErrorGenerator");
    }

    private C36641tT(Resources resources, C36521tG r2, C189216c r3, BlueServiceOperationFactory blueServiceOperationFactory, C193917y r5, C30441i3 r6, C04310Tq r7, AnonymousClass18M r8, AggregatedReliabilityLogger aggregatedReliabilityLogger) {
        this.A06 = resources;
        this.A02 = r2;
        this.A08 = r3;
        this.A00 = blueServiceOperationFactory;
        this.A04 = r5;
        this.A03 = r6;
        this.A05 = r7;
        this.A01 = r8;
        this.A07 = aggregatedReliabilityLogger;
    }
}
