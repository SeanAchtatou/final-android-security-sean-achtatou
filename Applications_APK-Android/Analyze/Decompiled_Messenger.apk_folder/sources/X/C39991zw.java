package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1zw  reason: invalid class name and case insensitive filesystem */
public final class C39991zw {
    private static volatile C39991zw A04;
    public final C04460Ut A00;
    public final C57362rr A01;
    public final C52622jP A02;
    public final C04310Tq A03;

    public static final C39991zw A00(AnonymousClass1XY r8) {
        if (A04 == null) {
            synchronized (C39991zw.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A04 = new C39991zw(AnonymousClass0VG.A00(AnonymousClass1Y3.ATE, applicationInjector), C57362rr.A00(applicationInjector), C04430Uq.A02(applicationInjector), C52622jP.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private C39991zw(C04310Tq r1, C57362rr r2, C04460Ut r3, C52622jP r4) {
        this.A03 = r1;
        this.A01 = r2;
        this.A00 = r3;
        this.A02 = r4;
    }
}
