package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.HashMap;

@UserScoped
/* renamed from: X.1Ex  reason: invalid class name and case insensitive filesystem */
public final class C21071Ex {
    private static C05540Zi A01;
    public final HashMap A00 = new HashMap();

    public static final C21071Ex A00(AnonymousClass1XY r3) {
        C21071Ex r0;
        synchronized (C21071Ex.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r3)) {
                    A01.A01();
                    A01.A00 = new C21071Ex();
                }
                C05540Zi r1 = A01;
                r0 = (C21071Ex) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }
}
