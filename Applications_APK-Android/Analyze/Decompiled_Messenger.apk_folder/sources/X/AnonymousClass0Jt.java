package X;

import android.os.Process;

/* renamed from: X.0Jt  reason: invalid class name */
public final class AnonymousClass0Jt extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.rti.common.concurrent.RtiThread";
    private final int A00;

    public void run() {
        Process.setThreadPriority(this.A00);
        super.run();
    }

    public AnonymousClass0Jt(Runnable runnable, String str, int i) {
        super(runnable, str);
        this.A00 = i;
    }
}
