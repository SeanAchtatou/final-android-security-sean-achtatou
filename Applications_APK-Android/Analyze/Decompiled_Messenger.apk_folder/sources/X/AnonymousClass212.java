package X;

import com.facebook.common.util.StringLocaleUtil;
import com.facebook.graphql.enums.GraphQLContactpointPurpose;
import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.util.List;

/* renamed from: X.212  reason: invalid class name */
public final class AnonymousClass212 implements C04810Wg {
    public final /* synthetic */ C181398bc A00;

    public AnonymousClass212(C181398bc r1) {
        this.A00 = r1;
    }

    public void BYh(Throwable th) {
        C010708t.A0I("ContactpointConsentsPrefetchConditionalWorker", th.getMessage() + th.getStackTrace());
        ((C07380dK) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BQ3, this.A00.A00)).A02(StringLocaleUtil.A00("%s.FAILURE", "tnc_sim_api_enforcement_v3.graphql"));
    }

    public void Bqf(Object obj) {
        C07380dK r2;
        Object[] objArr;
        String str;
        Object obj2;
        GraphQLResult graphQLResult = (GraphQLResult) obj;
        if (graphQLResult == null || (obj2 = graphQLResult.A03) == null) {
            r2 = (C07380dK) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BQ3, this.A00.A00);
            objArr = new Object[]{"tnc_sim_api_enforcement_v3.graphql"};
            str = "%s.NULL";
        } else {
            C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, ((C420127v) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A7e, this.A00.A00)).A00)).edit();
            edit.C24(C420127v.A01);
            edit.commit();
            for (GSTModelShape1S0000000 gSTModelShape1S0000000 : (List) obj2) {
                String A0P = gSTModelShape1S0000000.A0P(-1034364087);
                if (A0P != null) {
                    C24971Xv it = gSTModelShape1S0000000.A0M(-568245351, GSTModelShape1S0000000.class, 1996806902).iterator();
                    while (it.hasNext()) {
                        GSTModelShape1S0000000 gSTModelShape1S00000002 = (GSTModelShape1S0000000) it.next();
                        String valueOf = String.valueOf((GraphQLContactpointPurpose) gSTModelShape1S00000002.A0O(-220463842, GraphQLContactpointPurpose.A01));
                        boolean booleanValue = gSTModelShape1S00000002.getBooleanValue(-435737031);
                        AnonymousClass1Y7 A01 = C420127v.A01(A0P, valueOf);
                        C30281hn edit2 = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, ((C420127v) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A7e, this.A00.A00)).A00)).edit();
                        edit2.putBoolean(A01, booleanValue);
                        edit2.commit();
                        StringLocaleUtil.A00("%s: %s", A01, Boolean.valueOf(booleanValue));
                    }
                }
            }
            r2 = (C07380dK) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BQ3, this.A00.A00);
            objArr = new Object[]{"tnc_sim_api_enforcement_v3.graphql"};
            str = "%s.SUCCESS";
        }
        r2.A02(StringLocaleUtil.A00(str, objArr));
    }
}
