package X;

import com.facebook.graphql.enums.GraphQLThreadConnectivityStatus;
import com.facebook.messaging.model.threads.ThreadConnectivityData;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.1H9  reason: invalid class name */
public final class AnonymousClass1H9 {
    public static final AnonymousClass1H9 A00() {
        return new AnonymousClass1H9();
    }

    public static boolean A01(ThreadSummary threadSummary) {
        ThreadConnectivityData threadConnectivityData;
        if (threadSummary.A0O.A01() || ((threadConnectivityData = threadSummary.A0d) != null && GraphQLThreadConnectivityStatus.A01.equals(threadConnectivityData.A00()) && String.valueOf(threadSummary.A0S.A01).equals(threadConnectivityData.A03))) {
            return true;
        }
        return false;
    }
}
