package X;

/* renamed from: X.0UQ  reason: invalid class name */
public final class AnonymousClass0UQ extends AnonymousClass0UR {
    public final int A00;

    public static AnonymousClass0UQ A00(int i, AnonymousClass1XY r2) {
        return new AnonymousClass0UQ(i, r2);
    }

    private AnonymousClass0UQ(int i, AnonymousClass1XY r2) {
        super(r2);
        this.A00 = i;
    }
}
