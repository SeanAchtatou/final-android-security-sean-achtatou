package X;

import android.content.Context;
import android.content.res.Resources;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1vj  reason: invalid class name and case insensitive filesystem */
public final class C37471vj extends C20831Dz {
    public Resources A00;
    public C96324ia A01;
    public List A02;
    public boolean A03;
    public C88204Jb[] A04;
    public final Context A05;
    public final AnonymousClass1HK A06;
    public final AnonymousClass06B A07 = AnonymousClass067.A02();

    public static final C37471vj A00(AnonymousClass1XY r1) {
        return new C37471vj(r1);
    }

    public int ArU() {
        return this.A02.size();
    }

    public C37471vj(AnonymousClass1XY r2) {
        this.A05 = AnonymousClass1YA.A00(r2);
        this.A06 = AnonymousClass1HK.A00(r2);
        this.A00 = this.A05.getResources();
        this.A02 = new ArrayList();
        this.A04 = C88204Jb.values();
        this.A03 = true;
    }

    public static void A01(C37471vj r3, ArrayList arrayList) {
        if (!arrayList.isEmpty()) {
            r3.A02.add(new C12300p2(C88204Jb.AVAILABILITY_TIME_SLOT, new ArrayList(arrayList)));
            arrayList.clear();
        }
    }
}
