package X;

import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.user.model.User;
import java.util.Comparator;

/* renamed from: X.1IB  reason: invalid class name */
public final class AnonymousClass1IB implements Comparator {
    private final C14300t0 A00;

    public static final AnonymousClass1IB A00(AnonymousClass1XY r1) {
        return new AnonymousClass1IB(r1);
    }

    public int compare(Object obj, Object obj2) {
        float f;
        User A03 = this.A00.A03(((ParticipantInfo) obj).A01);
        User A032 = this.A00.A03(((ParticipantInfo) obj2).A01);
        float f2 = 0.0f;
        if (A03 != null) {
            f = A03.A04;
        } else {
            f = 0.0f;
        }
        if (A032 != null) {
            f2 = A032.A04;
        }
        return Float.compare(f2, f);
    }

    public AnonymousClass1IB(AnonymousClass1XY r2) {
        this.A00 = C14300t0.A00(r2);
    }
}
