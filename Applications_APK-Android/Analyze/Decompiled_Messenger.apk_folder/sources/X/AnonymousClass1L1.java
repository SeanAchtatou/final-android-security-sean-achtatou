package X;

import com.facebook.ui.emoji.model.Emoji;
import java.util.List;

/* renamed from: X.1L1  reason: invalid class name */
public abstract class AnonymousClass1L1 {
    public abstract Emoji A00(String str);

    public abstract String A01(String str);

    public abstract List A02();

    public abstract List A03();

    public boolean A04() {
        return false;
    }
}
