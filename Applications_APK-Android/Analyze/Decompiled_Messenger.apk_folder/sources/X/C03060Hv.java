package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.push.fbns.ipc.FbnsAIDLResult;

/* renamed from: X.0Hv  reason: invalid class name and case insensitive filesystem */
public final class C03060Hv implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new FbnsAIDLResult(parcel);
    }

    public Object[] newArray(int i) {
        return new FbnsAIDLResult[i];
    }
}
