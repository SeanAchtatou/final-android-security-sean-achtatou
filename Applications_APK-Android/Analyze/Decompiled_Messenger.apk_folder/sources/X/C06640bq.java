package X;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: X.0bq  reason: invalid class name and case insensitive filesystem */
public final class C06640bq {
    public static final List A01 = Collections.unmodifiableList(Arrays.asList("scheme", "authority", "path", "query"));
    public final Map A00;

    public C06640bq(Map map) {
        this.A00 = map;
    }
}
