package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0Se  reason: invalid class name and case insensitive filesystem */
public final class C04150Se extends Enum {
    private static final /* synthetic */ C04150Se[] A00;
    public static final C04150Se A01;

    static {
        C04150Se r2 = new C04150Se("UNUSED", 0);
        C04150Se r3 = new C04150Se("VOIP", 1);
        C04150Se r4 = new C04150Se("BACKGROUND_LOCATION", 2);
        C04150Se r5 = new C04150Se("VOIP_WEB", 3);
        C04150Se r6 = new C04150Se("MQTT_AGGRESSIVELY_NOTIFY", 4);
        C04150Se r7 = new C04150Se("VIDEO", 5);
        C04150Se r8 = new C04150Se("ONE_ON_ONE_OVER_MULTIWAY", 6);
        C04150Se r9 = new C04150Se("SHARED_SECRET", 7);
        A01 = r9;
        A00 = new C04150Se[]{r2, r3, r4, r5, r6, r7, r8, r9, new C04150Se("USER_AND_DEVICE_AUTH", 8)};
    }

    public static C04150Se valueOf(String str) {
        return (C04150Se) Enum.valueOf(C04150Se.class, str);
    }

    public static C04150Se[] values() {
        return (C04150Se[]) A00.clone();
    }

    private C04150Se(String str, int i) {
    }
}
