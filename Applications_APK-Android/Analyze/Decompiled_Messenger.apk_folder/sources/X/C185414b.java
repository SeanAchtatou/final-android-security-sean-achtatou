package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.14b  reason: invalid class name and case insensitive filesystem */
public interface C185414b {
    void AMt(C08900gA r1, AnonymousClass26K r2);

    void AMu(C08900gA r1, long j, String str);

    void AMv(C08900gA r1, String str);

    void AOH(C08900gA r1, long j, String str);

    void AOI(C08900gA r1, String str);

    void AOJ(C08900gA r1, long j, String str);

    void AOK(C08900gA r1, String str);

    void AOL(C08900gA r1, long j, String str, String str2);

    void AOM(C08900gA r1, String str, String str2);

    void AON(C08900gA r1, long j, String str, String str2, AnonymousClass26K r6);

    void AOO(C08900gA r1, String str, String str2, AnonymousClass26K r4);

    void AOP(C08900gA r1, String str, String str2, AnonymousClass26K r4, long j);

    void AOQ(C08900gA r1, long j, String str, String str2, AnonymousClass26K r6);

    void AOR(C08900gA r1, String str, String str2, AnonymousClass26K r4);

    void ARl(C08900gA r1);

    void ARm(C08900gA r1, long j);

    void AYS(C08900gA r1);

    void AYT(C08900gA r1, long j);

    ImmutableList Ans(C08900gA r1);

    long Ant(C08900gA r1);

    void CH1(C08900gA r1);

    void CH2(C08900gA r1, long j);

    void CH3(C08900gA r1);

    void CH4(C08900gA r1, long j);
}
