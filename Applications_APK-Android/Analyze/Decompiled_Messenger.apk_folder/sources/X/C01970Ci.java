package X;

/* renamed from: X.0Ci  reason: invalid class name and case insensitive filesystem */
public final class C01970Ci implements Comparable {
    public final long A00;
    public final C01900Cb A01;

    public int compareTo(Object obj) {
        return Long.valueOf(this.A00).compareTo(Long.valueOf(((C01970Ci) obj).A00));
    }

    public C01970Ci(C01900Cb r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }
}
