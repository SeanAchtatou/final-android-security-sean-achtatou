package X;

/* renamed from: X.0ue  reason: invalid class name and case insensitive filesystem */
public final class C15050ue {
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0035, code lost:
        if (((float) r5) < r7) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0044, code lost:
        if (r7 > ((float) r5)) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0026, code lost:
        if (java.lang.Math.abs(((float) r5) - r7) >= 0.5f) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(int r7, int r8, int r9) {
        /*
            int r6 = android.view.View.MeasureSpec.getMode(r8)
            int r5 = android.view.View.MeasureSpec.getSize(r8)
            int r4 = android.view.View.MeasureSpec.getMode(r7)
            int r3 = android.view.View.MeasureSpec.getSize(r7)
            if (r7 == r8) goto L_0x004a
            if (r4 != 0) goto L_0x0016
            if (r6 == 0) goto L_0x004a
        L_0x0016:
            float r7 = (float) r9
            r0 = 1073741824(0x40000000, float:2.0)
            if (r6 != r0) goto L_0x0028
            float r0 = (float) r5
            float r0 = r0 - r7
            float r1 = java.lang.Math.abs(r0)
            r0 = 1056964608(0x3f000000, float:0.5)
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            r0 = 1
            if (r1 < 0) goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            if (r0 != 0) goto L_0x004a
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r6 != r2) goto L_0x0037
            if (r4 != 0) goto L_0x0037
            float r0 = (float) r5
            int r1 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            r0 = 1
            if (r1 >= 0) goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            if (r0 != 0) goto L_0x004a
            if (r4 != r2) goto L_0x0046
            if (r6 != r2) goto L_0x0046
            if (r3 <= r5) goto L_0x0046
            float r0 = (float) r5
            int r0 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            r1 = 1
            if (r0 <= 0) goto L_0x0047
        L_0x0046:
            r1 = 0
        L_0x0047:
            r0 = 0
            if (r1 == 0) goto L_0x004b
        L_0x004a:
            r0 = 1
        L_0x004b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15050ue.A00(int, int, int):boolean");
    }
}
