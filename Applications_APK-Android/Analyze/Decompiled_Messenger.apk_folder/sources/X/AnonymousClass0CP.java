package X;

import java.util.ArrayList;

/* renamed from: X.0CP  reason: invalid class name */
public final class AnonymousClass0CP implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$CallbackHandler$1";
    public final /* synthetic */ AnonymousClass0CC A00;

    public AnonymousClass0CP(AnonymousClass0CC r1) {
        this.A00 = r1;
    }

    public void run() {
        ArrayList arrayList;
        this.A00.A02.A0I.onConnectSent();
        AnonymousClass0CC r1 = this.A00;
        if (r1.A01) {
            AnonymousClass0CC.A00(r1);
            AnonymousClass0CC r2 = this.A00;
            if (r2.A02.A0n == r2.A00) {
                r2.A02.A0O(null, null);
                r2.A02.A0I.BTc();
                AnonymousClass0AH r3 = r2.A02;
                AnonymousClass0C8 r22 = r2.A00;
                synchronized (r22) {
                    arrayList = new ArrayList();
                    arrayList.addAll(r22.A00);
                }
                r3.A0Z(arrayList);
            }
        }
    }
}
