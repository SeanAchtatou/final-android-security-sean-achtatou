package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.0xf  reason: invalid class name and case insensitive filesystem */
public final class C16720xf implements AnonymousClass0ZM {
    public final /* synthetic */ C16690xc A00;

    public C16720xf(C16690xc r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r6) {
        C16690xc r3 = this.A00;
        if (r3.A01.Aep(C26211b5.A06, false)) {
            r3.A00.A06(r3);
        } else {
            r3.A00.A05(r3);
        }
    }
}
