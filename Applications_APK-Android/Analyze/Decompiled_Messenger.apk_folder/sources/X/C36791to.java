package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.1to  reason: invalid class name and case insensitive filesystem */
public final class C36791to implements Iterator {
    public int A00 = 0;
    public Iterator A01;
    public final /* synthetic */ Iterable[] A02;

    public C36791to(Iterable[] iterableArr) {
        this.A02 = iterableArr;
    }

    public boolean hasNext() {
        int length = this.A02.length;
        while (this.A00 < length && ((r0 = this.A01) == null || !r0.hasNext())) {
            Iterable[] iterableArr = this.A02;
            int i = this.A00;
            this.A00 = i + 1;
            this.A01 = iterableArr[i].iterator();
        }
        Iterator it = this.A01;
        if (it == null || !it.hasNext()) {
            return false;
        }
        return true;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public Object next() {
        if (hasNext()) {
            return this.A01.next();
        }
        throw new NoSuchElementException();
    }
}
