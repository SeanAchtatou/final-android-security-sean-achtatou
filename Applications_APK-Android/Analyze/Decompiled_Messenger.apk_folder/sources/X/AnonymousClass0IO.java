package X;

import android.util.Pair;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0IO  reason: invalid class name */
public final class AnonymousClass0IO extends AnonymousClass0FM {
    private static final long serialVersionUID = 0;
    public HashMap threadCpuMap = new HashMap();

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        HashMap hashMap = this.threadCpuMap;
        HashMap hashMap2 = ((AnonymousClass0IO) obj).threadCpuMap;
        if (hashMap != null) {
            return hashMap.equals(hashMap2);
        }
        return hashMap2 == null;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r2) {
        this.threadCpuMap = ((AnonymousClass0IO) r2).threadCpuMap;
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r8, AnonymousClass0FM r9) {
        AnonymousClass0IO r82 = (AnonymousClass0IO) r8;
        AnonymousClass0IO r92 = (AnonymousClass0IO) r9;
        if (r92 == null) {
            r92 = new AnonymousClass0IO();
        }
        if (r82 == null) {
            r92.threadCpuMap = this.threadCpuMap;
        } else {
            r92.threadCpuMap.keySet().retainAll(this.threadCpuMap.keySet());
            for (Map.Entry entry : this.threadCpuMap.entrySet()) {
                int intValue = ((Integer) entry.getKey()).intValue();
                String str = (String) ((Pair) entry.getValue()).first;
                AnonymousClass0FV r3 = (AnonymousClass0FV) ((Pair) entry.getValue()).second;
                HashMap hashMap = r92.threadCpuMap;
                Integer valueOf = Integer.valueOf(intValue);
                if (!hashMap.containsKey(valueOf)) {
                    r92.threadCpuMap.put(valueOf, new Pair(str, new AnonymousClass0FV()));
                }
                AnonymousClass0FV r1 = (AnonymousClass0FV) ((Pair) r92.threadCpuMap.get(valueOf)).second;
                if (r82.threadCpuMap.containsKey(valueOf)) {
                    r3.A07((AnonymousClass0FV) ((Pair) r82.threadCpuMap.get(valueOf)).second, r1);
                } else {
                    r1.A0B(r3);
                }
            }
        }
        return r92;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r6, AnonymousClass0FM r7) {
        AnonymousClass0IO r62 = (AnonymousClass0IO) r6;
        AnonymousClass0IO r72 = (AnonymousClass0IO) r7;
        r72.threadCpuMap.keySet().retainAll(this.threadCpuMap.keySet());
        for (Map.Entry entry : this.threadCpuMap.entrySet()) {
            if (r72.threadCpuMap.containsKey(entry.getKey())) {
                ((AnonymousClass0FV) ((Pair) r72.threadCpuMap.get(Integer.valueOf(((Integer) entry.getKey()).intValue()))).second).A0B((AnonymousClass0FV) ((Pair) entry.getValue()).second);
            } else {
                r72.threadCpuMap.put(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry entry2 : r62.threadCpuMap.entrySet()) {
            int intValue = ((Integer) entry2.getKey()).intValue();
            HashMap hashMap = r72.threadCpuMap;
            Integer valueOf = Integer.valueOf(intValue);
            if (hashMap.containsKey(valueOf)) {
                AnonymousClass0FV r1 = (AnonymousClass0FV) ((Pair) r72.threadCpuMap.get(valueOf)).second;
                r1.A08((AnonymousClass0FV) ((Pair) entry2.getValue()).second, r1);
            } else {
                r72.threadCpuMap.put(entry2.getKey(), entry2.getValue());
            }
        }
        return r72;
    }

    public int hashCode() {
        HashMap hashMap = this.threadCpuMap;
        if (hashMap != null) {
            return hashMap.hashCode();
        }
        return 0;
    }

    public String toString() {
        return AnonymousClass08S.A0P("ThreadCpuMetrics{ ", this.threadCpuMap.toString(), " }");
    }
}
