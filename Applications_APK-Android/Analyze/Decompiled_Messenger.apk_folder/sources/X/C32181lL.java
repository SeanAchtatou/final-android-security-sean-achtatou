package X;

import android.content.Context;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;

/* renamed from: X.1lL  reason: invalid class name and case insensitive filesystem */
public final class C32181lL implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.NavigationLogger$5";
    public final /* synthetic */ AnonymousClass146 A00;
    public final /* synthetic */ boolean A01;

    public C32181lL(AnonymousClass146 r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    public void run() {
        USLEBaseShape0S0000000 A012 = AnonymousClass146.A01(this.A00, AnonymousClass07B.A0Y);
        AnonymousClass146 r1 = this.A00;
        if (r1.A06 == null && A012 != null) {
            r1.A06 = C188215g.A00().toString();
            A012.A0D("custom_session_id", this.A00.A06);
        }
        if (this.A01) {
            AnonymousClass146 r0 = this.A00;
            if (A012 != null) {
                A012.A06();
            }
            ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, r0.A03)).now();
        } else {
            AnonymousClass146.A05(this.A00, A012);
        }
        AnonymousClass146 r3 = this.A00;
        if (!r3.A09) {
            AnonymousClass08X.A04((Context) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BCt, this.A00.A03), "removeBgSessionId", ((AnonymousClass1YI) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AcD, r3.A03)).AbO(348, false) ? 1 : 0);
            this.A00.A09 = true;
        }
    }
}
