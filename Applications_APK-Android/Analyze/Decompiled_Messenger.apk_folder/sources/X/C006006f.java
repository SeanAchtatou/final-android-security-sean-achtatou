package X;

import io.card.payment.BuildConfig;
import java.util.Map;

/* renamed from: X.06f  reason: invalid class name and case insensitive filesystem */
public final class C006006f {
    public static Map A00;
    public static boolean A01;
    public static boolean A02;
    public static boolean A03;
    public static boolean A04;
    private static boolean A05;
    private static boolean A06;

    public static boolean A02(String str) {
        return "true".equals(A00(str, false));
    }

    static {
        if ("true".equalsIgnoreCase(AnonymousClass00I.A02("external_process_testing"))) {
            System.setProperty("IS_TESTING", AnonymousClass00I.A02("IS_TESTING"));
            System.setProperty("fb.running_e2e", AnonymousClass00I.A02("fb.running_e2e"));
            System.setProperty("is_mobileconfig_fetch_forced", AnonymousClass00I.A02("is_mobileconfig_fetch_forced"));
            System.setProperty("fb.e2e.e2e_username", AnonymousClass00I.A02("fb.e2e.e2e_username"));
            System.setProperty("fb.e2e.e2e_password", AnonymousClass00I.A02("fb.e2e.e2e_password"));
            System.setProperty("fb.e2e.main_test_user_id", AnonymousClass00I.A02("fb.e2e.main_test_user_id"));
            System.setProperty("fb.e2e.allow_write_prod", AnonymousClass00I.A02("fb.e2e.allow_write_prod"));
            System.setProperty("fb.e2e.sandbox_override", AnonymousClass00I.A02("fb.e2e.sandbox_override"));
            System.setProperty("fb.e2e.injected_feed", AnonymousClass00I.A02("fb.e2e.injected_feed"));
            System.setProperty("fb.http.dump_to_file", AnonymousClass00I.A02("fb.http.dump_to_file"));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if ("true".equals(java.lang.System.getProperty("fb.running_e2e")) != false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A01() {
        /*
            boolean r0 = X.C006006f.A05
            if (r0 != 0) goto L_0x002c
            java.lang.String r3 = "fb.running_e2e"
            java.lang.String r0 = X.AnonymousClass00I.A02(r3)
            java.lang.String r1 = "true"
            boolean r0 = r1.equals(r0)
            r2 = 1
            if (r0 != 0) goto L_0x001e
            java.lang.String r0 = java.lang.System.getProperty(r3)
            boolean r1 = r1.equals(r0)
            r0 = 0
            if (r1 == 0) goto L_0x001f
        L_0x001e:
            r0 = 1
        L_0x001f:
            X.C006006f.A06 = r0
            if (r0 == 0) goto L_0x002a
            java.lang.String r1 = "EndToEnd-Test"
            java.lang.String r0 = "Is running E2E test"
            android.util.Log.w(r1, r0)
        L_0x002a:
            X.C006006f.A05 = r2
        L_0x002c:
            boolean r0 = X.C006006f.A06
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C006006f.A01():boolean");
    }

    public static String A00(String str, boolean z) {
        if (A01()) {
            String property = System.getProperty(str);
            if (property != null && !property.equals(BuildConfig.FLAVOR)) {
                return property;
            }
            String property2 = System.getProperty(AnonymousClass08S.A0J("fb.e2e.", str));
            if (property2 != null && !property2.equals(BuildConfig.FLAVOR)) {
                return property2;
            }
            if (z) {
                String A022 = AnonymousClass00I.A02(str);
                if (A022 != null && !A022.equals(BuildConfig.FLAVOR)) {
                    return A022;
                }
                String A023 = AnonymousClass00I.A02(AnonymousClass08S.A0J("fb.e2e.", str));
                if (A023 == null || A023.equals(BuildConfig.FLAVOR)) {
                    return null;
                }
                return A023;
            }
        }
        return null;
    }
}
