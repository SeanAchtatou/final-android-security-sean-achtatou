package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0Bo  reason: invalid class name and case insensitive filesystem */
public final class C01770Bo extends Enum {
    private static final /* synthetic */ C01770Bo[] A00;
    public static final C01770Bo A01;
    public static final C01770Bo A02;
    public static final C01770Bo A03;
    public static final C01770Bo A04;
    public static final C01770Bo A05;
    public static final C01770Bo A06;
    public static final C01770Bo A07;
    public static final C01770Bo A08;
    public static final C01770Bo A09;
    public static final C01770Bo A0A;
    public static final C01770Bo A0B;
    public static final C01770Bo A0C;
    public static final C01770Bo A0D;
    public static final C01770Bo A0E;

    static {
        C01770Bo r14 = new C01770Bo("SERVICE_START", 0);
        A0E = r14;
        C01770Bo r13 = new C01770Bo("SERVICE_RESTART", 1);
        A0D = r13;
        C01770Bo r12 = new C01770Bo("PERSISTENT_KICK", 2);
        A0C = r12;
        C01770Bo r11 = new C01770Bo("CONNECTIVITY_CHANGED", 3);
        A04 = r11;
        C01770Bo r10 = new C01770Bo("CONFIG_CHANGED", 4);
        A02 = r10;
        C01770Bo r9 = new C01770Bo("EXPIRE_CONNECTION", 5);
        A07 = r9;
        C01770Bo r8 = new C01770Bo("CONNECT_NOW", 6);
        A05 = r8;
        C01770Bo r7 = new C01770Bo("CONNECTION_LOST", 7);
        A03 = r7;
        C01770Bo r6 = new C01770Bo("KEEPALIVE", 8);
        A0B = r6;
        C01770Bo r0 = new C01770Bo("APP_FOREGROUND", 9);
        C01770Bo r15 = new C01770Bo("FBNS_REGISTER", 10);
        A08 = r15;
        C01770Bo r5 = new C01770Bo("FBNS_REGISTER_RETRY", 11);
        A09 = r5;
        C01770Bo r4 = new C01770Bo("FBNS_UNREGISTER", 12);
        A0A = r4;
        C01770Bo r3 = new C01770Bo("CREDENTIALS_UPDATED", 13);
        A06 = r3;
        C01770Bo r18 = new C01770Bo("CLIENT_KICK", 14);
        C01770Bo r2 = new C01770Bo("AUTH_CREDENTIALS_CHANGE", 15);
        A01 = r2;
        C01770Bo r24 = r15;
        C01770Bo r25 = r5;
        C01770Bo r22 = r6;
        C01770Bo r23 = r0;
        C01770Bo r20 = r8;
        C01770Bo r21 = r7;
        C01770Bo r182 = r10;
        C01770Bo r19 = r9;
        C01770Bo r16 = r12;
        C01770Bo r17 = r11;
        C01770Bo r152 = r13;
        A00 = new C01770Bo[]{r14, r152, r16, r17, r182, r19, r20, r21, r22, r23, r24, r25, r4, r3, r18, r2};
    }

    public static C01770Bo valueOf(String str) {
        return (C01770Bo) Enum.valueOf(C01770Bo.class, str);
    }

    public static C01770Bo[] values() {
        return (C01770Bo[]) A00.clone();
    }

    private C01770Bo(String str, int i) {
    }
}
