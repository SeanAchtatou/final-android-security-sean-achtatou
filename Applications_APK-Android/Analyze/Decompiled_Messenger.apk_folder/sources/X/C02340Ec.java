package X;

import android.os.Process;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;
import com.facebook.systrace.TraceDirect;

/* renamed from: X.0Ec  reason: invalid class name and case insensitive filesystem */
public final class C02340Ec implements C02350Ed {
    public void AaI(long j, String str, C02980Hk r19) {
        if (AnonymousClass08Z.A05(j)) {
            C02980Hk r0 = r19;
            String[] strArr = r0.A01;
            int i = r0.A00;
            String str2 = str;
            if (TraceEvents.isEnabled(AnonymousClass00n.A08)) {
                int i2 = AnonymousClass00n.A08;
                int writeStandardEntry = Logger.writeStandardEntry(i2, 7, 22, 0, 0, -1591418627, 0, 0);
                Logger.writeBytesEntry(i2, 1, 83, writeStandardEntry, str2);
                for (int i3 = 1; i3 < i; i3 += 2) {
                    String str3 = strArr[i3 - 1];
                    String str4 = strArr[i3];
                    if (!(str3 == null || str4 == null)) {
                        int i4 = AnonymousClass00n.A08;
                        Logger.writeBytesEntry(i4, 1, 57, Logger.writeBytesEntry(i4, 1, 56, writeStandardEntry, str3), str4);
                    }
                }
            } else if (!AnonymousClass08Z.A05(j)) {
            } else {
                if (TraceDirect.checkNative()) {
                    TraceDirect.nativeBeginSectionWithArgs(str2, strArr, i);
                    return;
                }
                AnonymousClass09R r2 = new AnonymousClass09R('B');
                r2.A01(Process.myPid());
                r2.A03(str2);
                r2.A04(strArr, i);
                AnonymousClass0HL.A00(r2.toString());
            }
        }
    }
}
