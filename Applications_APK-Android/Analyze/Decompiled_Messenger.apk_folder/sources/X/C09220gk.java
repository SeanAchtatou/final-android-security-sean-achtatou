package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.profilo.core.TraceEvents;

/* renamed from: X.0gk  reason: invalid class name and case insensitive filesystem */
public final class C09220gk extends C09180gf {
    private boolean A00;
    private boolean A01;
    public final AnonymousClass1ZJ A02;

    public static void A00(C09220gk r22, C04270Tg r23, int i) {
        boolean z;
        C04270Tg r4 = r23;
        C09220gk r5 = r22;
        long A022 = r5.A02(r4.A02);
        long j = 0;
        if (A022 != 0 && r5.A03 != null) {
            long j2 = 1;
            int i2 = 0;
            if (!r5.A00 || (r5.A01 && !AnonymousClass08Z.A05(32))) {
                z = false;
            } else {
                z = true;
            }
            int i3 = 0;
            while (true) {
                AnonymousClass0Td[] r8 = r5.A03;
                if (i2 < r8.length) {
                    if ((A022 & j2) != j) {
                        int i4 = i;
                        if (z) {
                            try {
                                String name = r8[i2].getClass().getName();
                                Integer valueOf = Integer.valueOf(r4.A02);
                                Integer valueOf2 = Integer.valueOf(i4);
                                if (!TraceEvents.isEnabled(AnonymousClass00n.A08)) {
                                    AnonymousClass060.A01("QuickEventListenersList::notify [%s %d %d]", 3, name, valueOf, valueOf2, null, null);
                                } else {
                                    C005505z.A04(StringFormatUtil.formatStrLocaleSafe("QuickEventListenersList::notify [%s %d %d]", name, valueOf, valueOf2), 1038556350);
                                }
                            } catch (Throwable th) {
                                if (z) {
                                    C005505z.A00(1296463637);
                                }
                                throw th;
                            }
                        }
                        r4.A04 = i2;
                        if (i4 == 1) {
                            r5.A03[i2].BeT(r4);
                        } else if (i4 == 2) {
                            r5.A03[i2].BeU(r4);
                        } else if (i4 == 3) {
                            r5.A03[i2].BeR(r4);
                        } else if (i4 == 4) {
                            r5.A03[i2].BeM(r4);
                        } else {
                            throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown listenerMethod: ", i4));
                        }
                        i3++;
                        if (z) {
                            C005505z.A00(1062689059);
                        }
                    }
                    i2++;
                    j2 <<= 1;
                    j = 0;
                } else {
                    AnonymousClass1ZJ r0 = r5.A02;
                    if (r0 != null && i3 != 0) {
                        r0.A05.getAndAdd(i3);
                        return;
                    }
                    return;
                }
            }
        }
    }

    public C09220gk(AnonymousClass0Td[] r2, C39851zi r3, C12210op r4, AnonymousClass1ZJ r5) {
        super(r2, r3);
        this.A02 = r5;
        if (r4 != null) {
            this.A00 = r4.CDd();
            this.A01 = r4.CDn();
            return;
        }
        this.A00 = true;
    }
}
