package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0I5  reason: invalid class name */
public final class AnonymousClass0I5 implements FilenameFilter {
    public final /* synthetic */ String A00;

    public AnonymousClass0I5(String str) {
        this.A00 = str;
    }

    public boolean accept(File file, String str) {
        if (!str.startsWith("art_pgo_profile") || !str.endsWith(".prof") || str.equals(this.A00)) {
            return false;
        }
        return true;
    }
}
