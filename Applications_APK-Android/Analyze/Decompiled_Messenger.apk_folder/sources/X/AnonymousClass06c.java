package X;

/* renamed from: X.06c  reason: invalid class name */
public final class AnonymousClass06c {
    public int A00 = 16;
    public int A01 = 1024;
    public int A02 = 16;
    public C005906e A03;
    private long A04 = 60000;
    private AnonymousClass069 A05;
    private Class A06;
    private final AnonymousClass0N3 A07;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: boolean
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public X.AnonymousClass07J A00() {
        /*
            r11 = this;
            X.069 r10 = r11.A05
            if (r10 == 0) goto L_0x0029
            X.06e r9 = r11.A03
            if (r9 != 0) goto L_0x000f
            X.06d r9 = new X.06d
            java.lang.Class r0 = r11.A06
            r9.<init>(r0)
        L_0x000f:
            X.07J r2 = new X.07J
            java.lang.Class r3 = r11.A06
            int r4 = r11.A02
            int r5 = r11.A01
            int r6 = r11.A00
            long r7 = r11.A04
            r2.<init>(r3, r4, r5, r6, r7, r9, r10)
            X.0N3 r0 = r11.A07
            if (r0 == 0) goto L_0x0028
            java.lang.Class r1 = r11.A06
            r0 = 0
            r0.put(r1, r2)
        L_0x0028:
            return r2
        L_0x0029:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Must add a clock to the object pool builder"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass06c.A00():X.07J");
    }

    public AnonymousClass06c(AnonymousClass0N3 r3, Class cls, AnonymousClass069 r5) {
        this.A07 = r3;
        this.A06 = cls;
        this.A05 = r5;
    }
}
