package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1JF  reason: invalid class name */
public final class AnonymousClass1JF extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public BasicMontageThreadInfo A01;
    @Comparable(type = 13)
    public AnonymousClass1BO A02;
    @Comparable(type = 14)
    public AnonymousClass1RF A03 = new AnonymousClass1RF();
    @Comparable(type = 13)
    public MigColorScheme A04;
    @Comparable(type = 13)
    public AnonymousClass1H4 A05;

    public AnonymousClass1JF(Context context) {
        super("M4ThreadItemProfileImageComponent");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }

    public static void A00(AnonymousClass0p4 r3, boolean z) {
        if (r3.A04 != null) {
            r3.A0F(new C61322yh(0, Boolean.valueOf(z)), "updateState:M4ThreadItemProfileImageComponent.updatePressedState");
        }
    }

    public C17770zR A16() {
        AnonymousClass1JF r1 = (AnonymousClass1JF) super.A16();
        r1.A03 = new AnonymousClass1RF();
        return r1;
    }
}
