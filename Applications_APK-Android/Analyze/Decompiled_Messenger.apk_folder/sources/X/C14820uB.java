package X;

/* renamed from: X.0uB  reason: invalid class name and case insensitive filesystem */
public enum C14820uB {
    ON_CREATE,
    ON_START,
    ON_RESUME,
    ON_PAUSE,
    ON_STOP,
    ON_DESTROY,
    ON_ANY
}
