package X;

import com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000;
import com.facebook.graphql.calls.GraphQlCallInput;
import com.facebook.graphservice.GraphQLQueryBuilder;
import com.facebook.graphservice.asset.GraphServiceAsset;
import com.facebook.graphservice.interfaces.GraphQLQuery;
import com.facebook.graphservice.nativeutil.NativeMap;
import com.google.common.base.Preconditions;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0rE  reason: invalid class name and case insensitive filesystem */
public final class C13320rE {
    private static volatile C13320rE A01;
    private AnonymousClass0UN A00;

    public static final C13320rE A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C13320rE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C13320rE(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX INFO: finally extract failed */
    public GraphQLQuery A01(C10880l0 r14, C33431nZ r15) {
        String str;
        C005505z.A05("GS.newFromQueryString(%s)", r14.A06, 520970063);
        try {
            Class cls = r14.A04;
            boolean z = true;
            boolean z2 = false;
            if (cls != null) {
                z2 = true;
            }
            Preconditions.checkArgument(z2, "Query not enabled for GraphService (tree models): %s", r14.A06);
            if (r14.A03 == 0) {
                z = false;
            }
            Preconditions.checkArgument(z, "Query not enabled for GraphService (tree shape hash): %s", r14.A06);
            if (r14 instanceof C07450dZ) {
                str = C22298Ase.$const$string(56);
            } else {
                str = "Query";
            }
            C005505z.A05("GS.newFromQueryString(%s) - getParamsCopy", r14.A06, -1488150806);
            NativeMap nativeMap = new NativeMap();
            GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = r14.A00.A00;
            GraphQlCallInput.A02(gQLCallInputCInputShape0S0000000, gQLCallInputCInputShape0S0000000.A00, nativeMap);
            C005505z.A00(-525854738);
            GraphQLQuery result = new GraphQLQueryBuilder(r15, str, r14.A06, r14.A03, nativeMap, cls, r14.A02, (GraphServiceAsset) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5I, this.A00), false).getResult();
            C005505z.A00(-179786608);
            return result;
        } catch (Throwable th) {
            C005505z.A00(250498577);
            throw th;
        }
    }

    private C13320rE(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
