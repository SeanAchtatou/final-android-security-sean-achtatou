package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1C0  reason: invalid class name */
public final class AnonymousClass1C0 {
    public AnonymousClass0UN A00;
    public final Resources A01;

    public static final AnonymousClass1C0 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1C0(r1);
    }

    public static final AnonymousClass1C0 A01(AnonymousClass1XY r1) {
        return new AnonymousClass1C0(r1);
    }

    public static void A02(AnonymousClass1C0 r9, int i, C80043rl r11, Drawable drawable) {
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(i, PorterDuff.Mode.MULTIPLY);
        InsetDrawable insetDrawable = new InsetDrawable(drawable, 0, -r9.A01.getDimensionPixelSize(2132148265), 0, r9.A01.getDimensionPixelSize(2132148265));
        insetDrawable.setColorFilter(porterDuffColorFilter);
        r11.A0Q(insetDrawable);
        Drawable drawable2 = r9.A01.getDrawable(2132346717);
        drawable2.setColorFilter(porterDuffColorFilter);
        r11.A06.setImageDrawable(drawable2);
        Drawable drawable3 = r9.A01.getDrawable(2132346719);
        drawable3.setColorFilter(porterDuffColorFilter);
        r11.A07.setImageDrawable(drawable3);
    }

    public C80043rl A04(Context context) {
        C80043rl r2 = new C80043rl(context, 2132476973);
        MigColorScheme migColorScheme = (MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, this.A00);
        Drawable mutate = this.A01.getDrawable(2132345025).mutate();
        mutate.setColorFilter(migColorScheme.B2D(), PorterDuff.Mode.MULTIPLY);
        r2.A0Q(new InsetDrawable(mutate, 0, -this.A01.getDimensionPixelSize(2132148265), 0, this.A01.getDimensionPixelSize(2132148265)));
        Drawable mutate2 = this.A01.getDrawable(2132345024).mutate();
        mutate2.setColorFilter(migColorScheme.B2D(), PorterDuff.Mode.MULTIPLY);
        r2.A06.setImageDrawable(mutate2);
        Drawable mutate3 = this.A01.getDrawable(2132345026).mutate();
        mutate3.setColorFilter(migColorScheme.B2D(), PorterDuff.Mode.MULTIPLY);
        r2.A07.setImageDrawable(mutate3);
        r2.A0A.setTextColor(migColorScheme.AzL().AhV());
        r2.A09.setTextColor(migColorScheme.AzL().AhV());
        return r2;
    }

    private AnonymousClass1C0(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = C04490Ux.A0L(r3);
        C29001fi.A00(r3);
    }

    public C80043rl A03(Context context) {
        return A04(context);
    }
}
