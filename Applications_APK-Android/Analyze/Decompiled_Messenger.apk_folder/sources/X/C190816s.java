package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.16s  reason: invalid class name and case insensitive filesystem */
public final class C190816s extends AnonymousClass0W4 {
    private static final ImmutableList A00;
    private static final String A01 = AnonymousClass0W4.A05("contacts_indexed_data", "contacts_data_index", ImmutableList.of(C190916t.A02, C190916t.A01));
    private static final String A02;

    static {
        AnonymousClass0W6 r2 = C190916t.A02;
        AnonymousClass0W6 r1 = C190916t.A00;
        A00 = ImmutableList.of(r2, r1, C190916t.A01);
        A02 = AnonymousClass0W4.A05("contacts_indexed_data", "contacts_type_index", ImmutableList.of(r2, r1));
    }

    public C190816s() {
        super("contacts_indexed_data", A00);
    }

    public void A09(SQLiteDatabase sQLiteDatabase) {
        super.A09(sQLiteDatabase);
        String str = A02;
        C007406x.A00(847199386);
        sQLiteDatabase.execSQL(str);
        C007406x.A00(-1546394127);
        String str2 = A01;
        C007406x.A00(-2114632649);
        sQLiteDatabase.execSQL(str2);
        C007406x.A00(1899117632);
    }
}
