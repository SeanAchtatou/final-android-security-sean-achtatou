package X;

/* renamed from: X.0ZH  reason: invalid class name */
public class AnonymousClass0ZH implements C06380bP {
    public int A00;
    public final Object[] A01;

    public boolean C0w(Object obj) {
        int i;
        boolean z;
        int i2 = 0;
        while (true) {
            i = this.A00;
            if (i2 >= i) {
                z = false;
                break;
            } else if (this.A01[i2] == obj) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (!z) {
            Object[] objArr = this.A01;
            if (i >= objArr.length) {
                return false;
            }
            objArr[i] = obj;
            this.A00 = i + 1;
            return true;
        }
        throw new IllegalStateException("Already in the pool!");
    }

    public Object ALa() {
        int i = this.A00;
        if (i <= 0) {
            return null;
        }
        int i2 = i - 1;
        Object[] objArr = this.A01;
        Object obj = objArr[i2];
        objArr[i2] = null;
        this.A00 = i2;
        return obj;
    }

    public AnonymousClass0ZH(int i) {
        if (i > 0) {
            this.A01 = new Object[i];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }
}
