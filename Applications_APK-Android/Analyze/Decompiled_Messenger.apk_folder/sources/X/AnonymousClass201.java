package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.messages.montageattribution.EntityWithImage;

/* renamed from: X.201  reason: invalid class name */
public final class AnonymousClass201 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new EntityWithImage(parcel);
    }

    public Object[] newArray(int i) {
        return new EntityWithImage[i];
    }
}
