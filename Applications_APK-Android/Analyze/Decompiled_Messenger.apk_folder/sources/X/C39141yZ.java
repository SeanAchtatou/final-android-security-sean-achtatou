package X;

import android.content.Context;
import android.content.pm.PackageManager;
import com.facebook.inject.InjectorModule;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.voltron.fbdownloader.DefaultAppModuleManagerProvider;
import com.facebook.voltron.fbdownloader.DefaultModuleDownloadPreferencesProvider;
import com.facebook.voltron.fbdownloader.FbDownloaderExecutorServiceFactory;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;

@InjectorModule
/* renamed from: X.1yZ  reason: invalid class name and case insensitive filesystem */
public final class C39141yZ extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C71443cQ A01;
    private static volatile C43222Dn A02;
    private static volatile AnonymousClass37U A03;
    private static volatile C189718ru A04;
    private static volatile C71683co A05;
    private static volatile Boolean A06;

    public static final C71443cQ A00(AnonymousClass1XY r20) {
        if (A01 == null) {
            synchronized (C71443cQ.class) {
                AnonymousClass1XY r1 = r20;
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r1);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r1.getApplicationInjector();
                        Context A022 = AnonymousClass1YA.A02(applicationInjector);
                        C71453cR A003 = C71453cR.A00(applicationInjector);
                        C02380El A0A = AnonymousClass0Ei.A0A(applicationInjector);
                        C43222Dn A012 = A01(applicationInjector);
                        ExecutorService A0Z = AnonymousClass0UX.A0Z(applicationInjector);
                        AnonymousClass0XQ A004 = AnonymousClass0XP.A00(applicationInjector);
                        C71663cm r4 = new C71663cm(applicationInjector);
                        QuickPerformanceLogger A032 = AnonymousClass0ZD.A03(applicationInjector);
                        C71683co A052 = A05(applicationInjector);
                        AnonymousClass1YI A005 = AnonymousClass0WA.A00(applicationInjector);
                        AnonymousClass069 A033 = AnonymousClass067.A03(applicationInjector);
                        AnonymousClass0Ud A006 = AnonymousClass0Ud.A00(applicationInjector);
                        HashSet hashSet = new HashSet();
                        hashSet.add(r4);
                        hashSet.add(new AnonymousClass37a(A012, A0A, A003, A032, new C635737b(A006), 11337741, 11337743));
                        hashSet.add(A052);
                        A01 = new C71443cQ(A003, A0A, A012, A0Z, A004, A022, hashSet, A005.AbO(293, false), A033);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v7, types: [X.90G] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C43222Dn A01(X.AnonymousClass1XY r20) {
        /*
            X.2Dn r0 = X.C39141yZ.A02
            if (r0 != 0) goto L_0x00b8
            java.lang.Class<X.2Dn> r14 = X.C43222Dn.class
            monitor-enter(r14)
            X.2Dn r0 = X.C39141yZ.A02     // Catch:{ all -> 0x00b5 }
            r1 = r20
            X.0WD r13 = X.AnonymousClass0WD.A00(r0, r1)     // Catch:{ all -> 0x00b5 }
            if (r13 == 0) goto L_0x00b3
            X.1XY r0 = r1.getApplicationInjector()     // Catch:{ all -> 0x00ab }
            android.content.Context r6 = X.AnonymousClass1YA.A02(r0)     // Catch:{ all -> 0x00ab }
            X.2Du r9 = X.C43292Du.A00(r0)     // Catch:{ all -> 0x00ab }
            X.1YI r11 = X.AnonymousClass0WA.A00(r0)     // Catch:{ all -> 0x00ab }
            X.AnonymousClass0WT.A00(r0)     // Catch:{ all -> 0x00ab }
            X.0Ep r8 = X.AnonymousClass0Ei.A02(r0)     // Catch:{ all -> 0x00ab }
            X.0En r17 = X.AnonymousClass0Ei.A08(r0)     // Catch:{ all -> 0x00ab }
            X.37U r5 = A02(r0)     // Catch:{ all -> 0x00ab }
            com.facebook.quicklog.QuickPerformanceLogger r4 = X.AnonymousClass0ZD.A03(r0)     // Catch:{ all -> 0x00ab }
            java.util.concurrent.ExecutorService r3 = X.AnonymousClass0UX.A0a(r0)     // Catch:{ all -> 0x00ab }
            java.lang.Boolean r2 = A06(r0)     // Catch:{ all -> 0x00ab }
            X.0Ud r12 = X.AnonymousClass0Ud.A00(r0)     // Catch:{ all -> 0x00ab }
            r0 = 290(0x122, float:4.06E-43)
            r10 = 0
            boolean r1 = r11.AbO(r0, r10)     // Catch:{ all -> 0x00ab }
            boolean r0 = r2.booleanValue()     // Catch:{ all -> 0x00ab }
            if (r0 == 0) goto L_0x0055
            if (r1 == 0) goto L_0x0055
            X.90G r1 = new X.90G     // Catch:{ all -> 0x00ab }
            r1.<init>(r6)     // Catch:{ all -> 0x00ab }
            goto L_0x009f
        L_0x0055:
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x00ab }
            r7.<init>()     // Catch:{ all -> 0x00ab }
            if (r0 == 0) goto L_0x0080
            X.3cX r15 = new X.3cX     // Catch:{ all -> 0x00ab }
            X.37V r2 = new X.37V     // Catch:{ all -> 0x00ab }
            if (r12 != 0) goto L_0x0064
            r1 = 0
            goto L_0x0069
        L_0x0064:
            X.37W r1 = new X.37W     // Catch:{ all -> 0x00ab }
            r1.<init>(r12)     // Catch:{ all -> 0x00ab }
        L_0x0069:
            java.lang.String r0 = "GooglePlay"
            r2.<init>(r4, r0, r1)     // Catch:{ all -> 0x00ab }
            r0 = 289(0x121, float:4.05E-43)
            boolean r20 = r11.AbO(r0, r10)     // Catch:{ all -> 0x00ab }
            r16 = r6
            r18 = r8
            r19 = r2
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ all -> 0x00ab }
            r7.add(r15)     // Catch:{ all -> 0x00ab }
        L_0x0080:
            X.3cl r0 = new X.3cl     // Catch:{ all -> 0x00ab }
            r0.<init>(r6, r9, r8, r3)     // Catch:{ all -> 0x00ab }
            r7.add(r0)     // Catch:{ all -> 0x00ab }
            X.37Y r1 = new X.37Y     // Catch:{ all -> 0x00ab }
            int r0 = r7.size()     // Catch:{ all -> 0x00ab }
            X.2Dn[] r0 = new X.C43222Dn[r0]     // Catch:{ all -> 0x00ab }
            java.lang.Object[] r0 = r7.toArray(r0)     // Catch:{ all -> 0x00ab }
            X.2Dn[] r0 = (X.C43222Dn[]) r0     // Catch:{ all -> 0x00ab }
            r12 = 0
            r7 = r1
            r8 = r6
            r9 = r0
            r10 = r3
            r11 = r4
            r7.<init>(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x00ab }
        L_0x009f:
            r1.A08()     // Catch:{ all -> 0x00ab }
            java.lang.String r0 = r1.A08()     // Catch:{ all -> 0x00ab }
            r5.A00 = r0     // Catch:{ all -> 0x00ab }
            X.C39141yZ.A02 = r1     // Catch:{ all -> 0x00ab }
            goto L_0x00b0
        L_0x00ab:
            r0 = move-exception
            r13.A01()     // Catch:{ all -> 0x00b5 }
            throw r0     // Catch:{ all -> 0x00b5 }
        L_0x00b0:
            r13.A01()     // Catch:{ all -> 0x00b5 }
        L_0x00b3:
            monitor-exit(r14)     // Catch:{ all -> 0x00b5 }
            goto L_0x00b8
        L_0x00b5:
            r0 = move-exception
            monitor-exit(r14)     // Catch:{ all -> 0x00b5 }
            throw r0
        L_0x00b8:
            X.2Dn r0 = X.C39141yZ.A02
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39141yZ.A01(X.1XY):X.2Dn");
    }

    public static final AnonymousClass37U A02(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass37U.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass37U(AnonymousClass0ZD.A03(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C189718ru A03(AnonymousClass1XY r9) {
        if (A04 == null) {
            synchronized (C189718ru.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r9);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r9.getApplicationInjector();
                        A04 = new C189718ru(AnonymousClass0Ei.A0A(applicationInjector), C71453cR.A00(applicationInjector), C189668rp.A00(applicationInjector), C09450hO.A00(applicationInjector), AnonymousClass0WA.A00(applicationInjector), AnonymousClass0XP.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final C71683co A05(AnonymousClass1XY r11) {
        if (A05 == null) {
            synchronized (C71683co.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        A05 = new C71683co(DefaultModuleDownloadPreferencesProvider.class, FbDownloaderExecutorServiceFactory.class, DefaultAppModuleManagerProvider.class, A04(applicationInjector), AnonymousClass0WA.A00(applicationInjector).AbO(295, false), AnonymousClass0XP.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final Boolean A06(AnonymousClass1XY r8) {
        boolean z;
        if (A06 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        Context A022 = AnonymousClass1YA.A02(applicationInjector);
                        AnonymousClass1YI A003 = AnonymousClass0WA.A00(applicationInjector);
                        boolean z2 = false;
                        boolean AbO = A003.AbO(291, false);
                        if (!A003.AbO(AnonymousClass1Y3.A2K, false)) {
                            String installerPackageName = A022.getPackageManager().getInstallerPackageName(A022.getPackageName());
                            if (installerPackageName != null) {
                                z = true;
                                if (!"com.android.vending".equals(installerPackageName)) {
                                }
                            }
                            z = false;
                        } else {
                            try {
                                A022.getPackageManager().getPackageInfo("com.android.vending", 1);
                                z = true;
                            } catch (PackageManager.NameNotFoundException unused) {
                                z = false;
                            }
                        }
                        if (!AbO && z) {
                            z2 = true;
                        }
                        A06 = Boolean.valueOf(z2);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static final C71693cp A04(AnonymousClass1XY r5) {
        AnonymousClass1YI A002 = AnonymousClass0WA.A00(r5);
        C51432gf A003 = C56292pg.A00(r5);
        Context A004 = AnonymousClass1YA.A00(r5);
        AnonymousClass0XP.A00(r5);
        boolean AbO = A002.AbO(AnonymousClass1Y3.A2G, false);
        int i = 2;
        if (AbO) {
            i = 1;
        }
        return new C71693cp(A003, i, A004.getApplicationContext());
    }
}
