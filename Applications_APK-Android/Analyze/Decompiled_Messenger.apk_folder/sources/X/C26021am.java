package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1am  reason: invalid class name and case insensitive filesystem */
public final class C26021am extends C25941ae {
    private static volatile C26021am A00;

    private C26021am(AnonymousClass0US r3) {
        super(r3, C05690aA.A17, AnonymousClass07B.A01);
    }

    public static final C26021am A00(AnonymousClass1XY r5) {
        if (A00 == null) {
            synchronized (C26021am.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        A00 = new C26021am(AnonymousClass0VB.A00(AnonymousClass1Y3.AN5, r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
