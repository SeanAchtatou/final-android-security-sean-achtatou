package X;

/* renamed from: X.1bs  reason: invalid class name and case insensitive filesystem */
public abstract class C26701bs extends C10030jR implements C10040jS {
    private static final long serialVersionUID = -3581199092426900829L;
    public volatile transient String _canonicalName;

    public abstract String buildCanonicalName();

    public Object getTypeHandler() {
        return this._typeHandler;
    }

    public Object getValueHandler() {
        return this._valueHandler;
    }

    public String toCanonical() {
        String str = this._canonicalName;
        if (str == null) {
            return buildCanonicalName();
        }
        return str;
    }

    public void serialize(C11710np r2, C11260mU r3) {
        r2.writeString(toCanonical());
    }

    public void serializeWithType(C11710np r1, C11260mU r2, CY1 cy1) {
        cy1.writeTypePrefixForScalar(this, r1);
        serialize(r1, r2);
        cy1.writeTypeSuffixForScalar(this, r1);
    }

    public C26701bs(Class cls, int i, Object obj, Object obj2, boolean z) {
        super(cls, i, obj, obj2, z);
    }
}
