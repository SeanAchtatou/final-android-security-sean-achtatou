package X;

import com.facebook.perf.startupstatemachine.StartupStateMachine;

/* renamed from: X.14t  reason: invalid class name and case insensitive filesystem */
public final class C187014t {
    public AnonymousClass0UN A00;
    public final StartupStateMachine A01;

    public static final C187014t A00(AnonymousClass1XY r1) {
        return new C187014t(r1);
    }

    public C187014t(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = StartupStateMachine.A00(r3);
    }
}
