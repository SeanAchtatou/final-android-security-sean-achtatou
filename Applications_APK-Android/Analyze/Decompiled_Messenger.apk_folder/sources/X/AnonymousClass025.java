package X;

import android.os.StrictMode;
import java.io.File;
import java.util.Collection;

/* renamed from: X.025  reason: invalid class name */
public abstract class AnonymousClass025 {
    public abstract File A05(String str);

    public String A06(String str) {
        return null;
    }

    public void A07(Collection collection) {
    }

    public String[] A08(String str) {
        return null;
    }

    public abstract int A09(String str, int i, StrictMode.ThreadPolicy threadPolicy);

    public void A0A(int i) {
    }

    public String[] A0B() {
        return C002401o.A02();
    }

    public String toString() {
        return getClass().getName();
    }
}
