package X;

import javax.security.auth.x500.X500Principal;

/* renamed from: X.0Qi  reason: invalid class name and case insensitive filesystem */
public final class C03900Qi {
    public int A00;
    public int A01;
    public int A02;
    public char[] A03;
    public final int A04;
    public final String A05;

    public static char A00(C03900Qi r10) {
        int i;
        int i2 = r10.A02 + 1;
        r10.A02 = i2;
        int i3 = r10.A04;
        if (i2 != i3) {
            char[] cArr = r10.A03;
            char c = cArr[i2];
            if (c == ' ' || c == '%' || c == '\\' || c == '_' || c == '\"' || c == '#') {
                return c;
            }
            switch (c) {
                case '*':
                case '+':
                case AnonymousClass1Y3.A0I /*44*/:
                    return c;
                default:
                    switch (c) {
                        case ';':
                        case AnonymousClass1Y3.A0R /*60*/:
                        case '=':
                        case '>':
                            return c;
                        default:
                            int A012 = A01(r10, i2);
                            r10.A02 = i2 + 1;
                            if (A012 >= 128) {
                                if (A012 < 192 || A012 > 247) {
                                    return '?';
                                }
                                if (A012 <= 223) {
                                    A012 &= 31;
                                    i = 1;
                                } else if (A012 <= 239) {
                                    i = 2;
                                    A012 &= 15;
                                } else {
                                    i = 3;
                                    A012 &= 7;
                                }
                                for (int i4 = 0; i4 < i; i4++) {
                                    int i5 = r10.A02 + 1;
                                    r10.A02 = i5;
                                    if (i5 == i3 || cArr[i5] != '\\') {
                                        return '?';
                                    }
                                    int i6 = i5 + 1;
                                    r10.A02 = i6;
                                    int A013 = A01(r10, i6);
                                    r10.A02 = i6 + 1;
                                    if ((A013 & 192) != 128) {
                                        return '?';
                                    }
                                    A012 = (A012 << 6) + (A013 & 63);
                                }
                            }
                            return (char) A012;
                    }
            }
        } else {
            throw new IllegalStateException(AnonymousClass08S.A0J("Unexpected end of DN: ", r10.A05));
        }
    }

    public static int A01(C03900Qi r10, int i) {
        int i2;
        int i3;
        int i4 = i + 1;
        if (i4 < r10.A04) {
            char[] cArr = r10.A03;
            char c = cArr[i];
            if (c >= '0' && c <= '9') {
                i2 = c - '0';
            } else if (c >= 'a' && c <= 'f') {
                i2 = c - 'W';
            } else if (c >= 'A' && c <= 'F') {
                i2 = c - '7';
            }
            char c2 = cArr[i4];
            if (c2 >= '0' && c2 <= '9') {
                i3 = c2 - '0';
            } else if (c2 >= 'a' && c2 <= 'f') {
                i3 = c2 - 'W';
            } else if (c2 >= 'A' && c2 <= 'F') {
                i3 = c2 - '7';
            }
            return (i2 << 4) + i3;
        }
        throw new IllegalStateException(AnonymousClass08S.A0J("Malformed DN: ", r10.A05));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004f, code lost:
        if (r1 != r6) goto L_0x0051;
     */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0015 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02(X.C03900Qi r9) {
        /*
        L_0x0000:
            int r5 = r9.A02
            int r6 = r9.A04
            r4 = 32
            if (r5 >= r6) goto L_0x0013
            char[] r0 = r9.A03
            char r0 = r0[r5]
            if (r0 != r4) goto L_0x0013
            int r0 = r5 + 1
            r9.A02 = r0
            goto L_0x0000
        L_0x0013:
            if (r5 != r6) goto L_0x0017
            r0 = 0
            return r0
        L_0x0017:
            r9.A00 = r5
            int r0 = r5 + 1
            r9.A02 = r0
        L_0x001d:
            int r3 = r9.A02
            r8 = 61
            if (r3 >= r6) goto L_0x0030
            char[] r0 = r9.A03
            char r0 = r0[r3]
            if (r0 == r8) goto L_0x0030
            if (r0 == r4) goto L_0x0030
            int r0 = r3 + 1
            r9.A02 = r0
            goto L_0x001d
        L_0x0030:
            java.lang.String r7 = "Unexpected end of DN: "
            if (r3 >= r6) goto L_0x009f
            r9.A01 = r3
            char[] r2 = r9.A03
            char r0 = r2[r3]
            if (r0 != r4) goto L_0x0051
        L_0x003c:
            int r1 = r9.A02
            if (r1 >= r6) goto L_0x004b
            char r0 = r2[r1]
            if (r0 == r8) goto L_0x004b
            if (r0 != r4) goto L_0x004b
            int r0 = r1 + 1
            r9.A02 = r0
            goto L_0x003c
        L_0x004b:
            char r0 = r2[r1]
            if (r0 != r8) goto L_0x009f
            if (r1 == r6) goto L_0x009f
        L_0x0051:
            int r0 = r9.A02
            int r0 = r0 + 1
            r9.A02 = r0
        L_0x0057:
            int r1 = r9.A02
            if (r1 >= r6) goto L_0x0064
            char r0 = r2[r1]
            if (r0 != r4) goto L_0x0064
            int r0 = r1 + 1
            r9.A02 = r0
            goto L_0x0057
        L_0x0064:
            int r0 = r3 - r5
            r4 = 4
            if (r0 <= r4) goto L_0x0096
            int r0 = r5 + 3
            char r1 = r2[r0]
            r0 = 46
            if (r1 != r0) goto L_0x0096
            char r1 = r2[r5]
            r0 = 79
            if (r1 == r0) goto L_0x007b
            r0 = 111(0x6f, float:1.56E-43)
            if (r1 != r0) goto L_0x0096
        L_0x007b:
            int r0 = r5 + 1
            char r1 = r2[r0]
            r0 = 73
            if (r1 == r0) goto L_0x0087
            r0 = 105(0x69, float:1.47E-43)
            if (r1 != r0) goto L_0x0096
        L_0x0087:
            int r0 = r5 + 2
            char r1 = r2[r0]
            r0 = 68
            if (r1 == r0) goto L_0x0093
            r0 = 100
            if (r1 != r0) goto L_0x0096
        L_0x0093:
            int r5 = r5 + r4
            r9.A00 = r5
        L_0x0096:
            java.lang.String r1 = new java.lang.String
            int r0 = r9.A00
            int r3 = r3 - r0
            r1.<init>(r2, r0, r3)
            return r1
        L_0x009f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = r9.A05
            java.lang.String r0 = X.AnonymousClass08S.A0J(r7, r0)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03900Qi.A02(X.0Qi):java.lang.String");
    }

    public C03900Qi(X500Principal x500Principal) {
        String name = x500Principal.getName("RFC2253");
        this.A05 = name;
        this.A04 = name.length();
        this.A03 = name.toCharArray();
    }
}
