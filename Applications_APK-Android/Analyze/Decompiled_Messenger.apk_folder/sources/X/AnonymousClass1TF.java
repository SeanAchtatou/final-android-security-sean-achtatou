package X;

import com.facebook.common.callercontext.CallerContextable;
import com.facebook.messaging.montage.omnistore.cache.MontageCache;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1TF  reason: invalid class name */
public final class AnonymousClass1TF implements AnonymousClass1EM, CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.loader.OmnistoreMontageListLoader";
    public C20021Ap A00;
    public AnonymousClass0UN A01;
    public MontageCache A02;
    public boolean A03;
    public final C17350yl A04;
    public final AnonymousClass1TK A05;
    public final AnonymousClass1TH A06;
    public final C34041oY A07;
    public final AnonymousClass1G1 A08;
    public final AnonymousClass1TU A09;
    public final AtomicBoolean A0A = new AtomicBoolean(false);

    public void ARp() {
    }

    public static final AnonymousClass1TF A00(AnonymousClass1XY r1) {
        return new AnonymousClass1TF(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x013e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x013f, code lost:
        if (r3 != null) goto L_0x0141;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r8.A04.A0H() != false) goto L_0x0019;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:52:0x0144 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void CHB(java.lang.Object r9) {
        /*
            r8 = this;
            X.1TO r9 = (X.AnonymousClass1TO) r9
            java.lang.String r1 = "OmnistoreMontageListLoader.startLoad"
            r0 = 1732873176(0x67498bd8, float:9.517753E23)
            X.C005505z.A03(r1, r0)
            boolean r0 = r9.A01     // Catch:{ all -> 0x016c }
            r3 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0019
            X.0yl r0 = r8.A04     // Catch:{ all -> 0x016c }
            boolean r1 = r0.A0H()     // Catch:{ all -> 0x016c }
            r0 = 0
            if (r1 == 0) goto L_0x001a
        L_0x0019:
            r0 = 1
        L_0x001a:
            r8.A03 = r0     // Catch:{ all -> 0x016c }
            com.facebook.messaging.montage.omnistore.cache.MontageCache r0 = r8.A02     // Catch:{ all -> 0x016c }
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x016c }
            java.lang.Integer r0 = r0.A01     // Catch:{ all -> 0x016c }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x016c }
            if (r0 != 0) goto L_0x00c4
            int r1 = X.AnonymousClass1Y3.BMw     // Catch:{ all -> 0x016c }
            X.0UN r0 = r8.A01     // Catch:{ all -> 0x016c }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x016c }
            X.1Ei r1 = (X.C20921Ei) r1     // Catch:{ all -> 0x016c }
            java.lang.String r0 = "omni_try_init"
            r1.A08(r0)     // Catch:{ all -> 0x016c }
            int r1 = X.AnonymousClass1Y3.AQC     // Catch:{ all -> 0x016c }
            X.0UN r0 = r8.A01     // Catch:{ all -> 0x016c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x016c }
            X.18s r0 = (X.C195518s) r0     // Catch:{ all -> 0x016c }
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x016c }
            X.0UN r0 = r0.A00     // Catch:{ all -> 0x016c }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x016c }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ all -> 0x016c }
            r0 = 282501479138807(0x100ef005005f7, double:1.39574275741822E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x00a8
            X.1oY r6 = r8.A07     // Catch:{ all -> 0x016c }
            java.lang.String r1 = "MontageLoadingServiceImpl.tryToLoadAllBuckets"
            r0 = 68173664(0x4103f60, float:1.6956219E-36)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x016c }
            X.1TP r5 = new X.1TP     // Catch:{ all -> 0x00a0 }
            r5.<init>(r6)     // Catch:{ all -> 0x00a0 }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AQC     // Catch:{ all -> 0x00a0 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x00a0 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00a0 }
            X.18s r0 = (X.C195518s) r0     // Catch:{ all -> 0x00a0 }
            int r2 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x00a0 }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x00a0 }
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x00a0 }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ all -> 0x00a0 }
            r0 = 563976455783060(0x200ef004f0294, double:2.78641391865715E-309)
            long r2 = r2.At0(r0)     // Catch:{ all -> 0x00a0 }
            r4 = 0
            int r1 = X.AnonymousClass1Y3.AqV     // Catch:{ all -> 0x00a0 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x00a0 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x00a0 }
            X.1TQ r0 = (X.AnonymousClass1TQ) r0     // Catch:{ all -> 0x00a0 }
            X.0VL r1 = r0.A01(r2)     // Catch:{ all -> 0x00a0 }
            r0 = -666096114(0xffffffffd84c2e0e, float:-8.979927E14)
            X.AnonymousClass07A.A04(r1, r5, r0)     // Catch:{ all -> 0x00a0 }
            r0 = -1485290102(0xffffffffa778458a, float:-3.445461E-15)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x016c }
            goto L_0x00bd
        L_0x00a0:
            r1 = move-exception
            r0 = 1265214948(0x4b69a5e4, float:1.5312356E7)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x016c }
            throw r1     // Catch:{ all -> 0x016c }
        L_0x00a8:
            X.1G1 r0 = r8.A08     // Catch:{ all -> 0x016c }
            android.os.Bundle r4 = new android.os.Bundle     // Catch:{ all -> 0x016c }
            r4.<init>()     // Catch:{ all -> 0x016c }
            com.facebook.fbservice.ops.BlueServiceOperationFactory r3 = r0.A00     // Catch:{ all -> 0x016c }
            com.facebook.common.callercontext.CallerContext r2 = X.AnonymousClass1G1.A02     // Catch:{ all -> 0x016c }
            java.lang.String r1 = "load_montage_inventory"
            r0 = 1
            X.0lL r0 = r3.newInstance(r1, r4, r0, r2)     // Catch:{ all -> 0x016c }
            r0.CHd()     // Catch:{ all -> 0x016c }
        L_0x00bd:
            r0 = -1241817760(0xffffffffb5fb5d60, float:-1.8728133E-6)
            X.C005505z.A00(r0)
            return
        L_0x00c4:
            java.util.concurrent.atomic.AtomicBoolean r0 = r8.A0A     // Catch:{ all -> 0x016c }
            boolean r0 = r0.compareAndSet(r2, r3)     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x0165
            int r1 = X.AnonymousClass1Y3.BMw     // Catch:{ all -> 0x016c }
            X.0UN r0 = r8.A01     // Catch:{ all -> 0x016c }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x016c }
            X.1Ei r7 = (X.C20921Ei) r7     // Catch:{ all -> 0x016c }
            boolean r0 = X.C20921Ei.A03(r7)     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x00f7
            int[] r6 = X.C20921Ei.A08     // Catch:{ all -> 0x016c }
            int r5 = r6.length     // Catch:{ all -> 0x016c }
            r4 = 0
            r3 = 0
        L_0x00e1:
            if (r3 >= r5) goto L_0x00f7
            r2 = r6[r3]     // Catch:{ all -> 0x016c }
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x016c }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x016c }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x016c }
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1     // Catch:{ all -> 0x016c }
            java.lang.String r0 = "stories_load_start"
            r1.markerPoint(r2, r0)     // Catch:{ all -> 0x016c }
            int r3 = r3 + 1
            goto L_0x00e1
        L_0x00f7:
            X.1TK r0 = r8.A05     // Catch:{ all -> 0x016c }
            com.google.common.util.concurrent.ListenableFuture r4 = r0.A02()     // Catch:{ all -> 0x016c }
            boolean r0 = r8.A03     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x0145
            com.facebook.messaging.montage.omnistore.cache.MontageCache r5 = r8.A02     // Catch:{ all -> 0x016c }
            int r2 = X.AnonymousClass1Y3.BP2     // Catch:{ all -> 0x016c }
            X.0UN r1 = r5.A00     // Catch:{ all -> 0x016c }
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x016c }
            X.1HC r0 = (X.AnonymousClass1HC) r0     // Catch:{ all -> 0x016c }
            X.1HE r3 = r0.A01()     // Catch:{ all -> 0x016c }
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x013c }
            java.util.Collection r0 = com.facebook.messaging.montage.omnistore.cache.MontageCache.A02(r5)     // Catch:{ all -> 0x013c }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x013c }
        L_0x011e:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x013c }
            if (r0 == 0) goto L_0x0132
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x013c }
            X.1v1 r0 = (X.AnonymousClass1v1) r0     // Catch:{ all -> 0x013c }
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r0 = r0.A03     // Catch:{ all -> 0x013c }
            if (r0 == 0) goto L_0x011e
            r2.add(r0)     // Catch:{ all -> 0x013c }
            goto L_0x011e
        L_0x0132:
            com.google.common.collect.ImmutableList r0 = r2.build()     // Catch:{ all -> 0x013c }
            if (r3 == 0) goto L_0x014b
            r3.close()     // Catch:{ all -> 0x016c }
            goto L_0x014b
        L_0x013c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x013e }
        L_0x013e:
            r0 = move-exception
            if (r3 == 0) goto L_0x0144
            r3.close()     // Catch:{ all -> 0x0144 }
        L_0x0144:
            throw r0     // Catch:{ all -> 0x016c }
        L_0x0145:
            com.facebook.messaging.montage.omnistore.cache.MontageCache r0 = r8.A02     // Catch:{ all -> 0x016c }
            com.google.common.collect.ImmutableList r0 = r0.A07()     // Catch:{ all -> 0x016c }
        L_0x014b:
            com.google.common.util.concurrent.ListenableFuture r0 = X.C05350Yp.A03(r0)     // Catch:{ all -> 0x016c }
            X.2wx r3 = new X.2wx     // Catch:{ all -> 0x016c }
            r3.<init>(r8, r0, r4, r9)     // Catch:{ all -> 0x016c }
            com.google.common.util.concurrent.ListenableFuture[] r0 = new com.google.common.util.concurrent.ListenableFuture[]{r4, r0}     // Catch:{ all -> 0x016c }
            X.2wy r2 = new X.2wy     // Catch:{ all -> 0x016c }
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x016c }
            r0 = 0
            r2.<init>(r0, r1)     // Catch:{ all -> 0x016c }
            r2.A00(r3)     // Catch:{ all -> 0x016c }
        L_0x0165:
            r0 = -1985545908(0xffffffff89a6f94c, float:-4.0197484E-33)
            X.C005505z.A00(r0)
            return
        L_0x016c:
            r1 = move-exception
            r0 = 2128806856(0x7ee303c8, float:1.5087719E38)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TF.CHB(java.lang.Object):void");
    }

    private AnonymousClass1TF(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(3, r3);
        this.A02 = MontageCache.A01(r3);
        this.A06 = new AnonymousClass1TH(r3);
        this.A09 = AnonymousClass1TU.A05(r3);
        this.A08 = AnonymousClass1G1.A00(r3);
        this.A05 = new AnonymousClass1TK(r3);
        this.A07 = C34041oY.A00(r3);
        this.A04 = C17350yl.A00(r3);
    }

    public void C6Z(C20021Ap r1) {
        this.A00 = r1;
    }
}
