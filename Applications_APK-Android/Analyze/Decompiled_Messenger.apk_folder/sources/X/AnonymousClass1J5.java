package X;

import android.net.Uri;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.messages.Message;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.proxygen.TraceFieldType;
import java.util.BitSet;

/* renamed from: X.1J5  reason: invalid class name */
public final class AnonymousClass1J5 extends C17770zR {
    public static final MigColorScheme A05 = C17190yT.A00();
    @Comparable(type = 3)
    public int A00 = 0;
    @Comparable(type = 13)
    public Message A01;
    @Comparable(type = 13)
    public AnonymousClass1J0 A02;
    @Comparable(type = 13)
    public MigColorScheme A03 = A05;
    @Comparable(type = 13)
    public AnonymousClass1H4 A04;

    public AnonymousClass1J5() {
        super("M4MigThreadImage");
    }

    public static C17770zR A00(AnonymousClass0p4 r8, AnonymousClass1H4 r9, AnonymousClass1J0 r10, C22111Jy r11) {
        int A002 = C007106r.A00(r8.A09, (float) r10.A05);
        Uri ApX = r9.ApX(0, A002, A002);
        String[] strArr = {"profileConfig", TraceFieldType.Uri};
        BitSet bitSet = new BitSet(2);
        C32071l6 r4 = new C32071l6();
        C17770zR r1 = r8.A04;
        if (r1 != null) {
            r4.A07 = r1.A06;
        }
        bitSet.clear();
        r4.A01 = ApX;
        bitSet.set(1);
        r4.A03 = r10;
        bitSet.set(0);
        r4.A02 = r11;
        AnonymousClass11F.A0C(2, bitSet, strArr);
        return r4;
    }
}
