package X;

import android.view.View;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

/* renamed from: X.1KC  reason: invalid class name */
public final class AnonymousClass1KC extends C15220uv {
    public final /* synthetic */ AnonymousClass1I9 A00;

    public AnonymousClass1KC(AnonymousClass1I9 r1) {
        this.A00 = r1;
    }

    public void A0I(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        super.A0I(view, accessibilityNodeInfoCompat);
        C21651Ie.A02(accessibilityNodeInfoCompat, this.A00);
    }
}
