package X;

/* renamed from: X.1QL  reason: invalid class name */
public final class AnonymousClass1QL extends AnonymousClass1QM {
    public void A0A(Object obj, int i) {
        super.A0A(AnonymousClass1PS.A00((AnonymousClass1PS) obj), i);
    }

    public static AnonymousClass1QO A00(AnonymousClass1Q3 r2, AnonymousClass1QJ r3, AnonymousClass1MK r4) {
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("CloseableProducerToDataSourceAdapter#create");
        }
        AnonymousClass1QL r1 = new AnonymousClass1QL(r2, r3, r4);
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
        return r1;
    }

    public Object B1I() {
        return AnonymousClass1PS.A00((AnonymousClass1PS) super.B1I());
    }

    private AnonymousClass1QL(AnonymousClass1Q3 r1, AnonymousClass1QJ r2, AnonymousClass1MK r3) {
        super(r1, r2, r3);
    }
}
