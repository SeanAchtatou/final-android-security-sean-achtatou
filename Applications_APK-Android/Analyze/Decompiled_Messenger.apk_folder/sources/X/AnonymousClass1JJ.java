package X;

import com.facebook.litho.annotations.Comparable;
import java.util.BitSet;

/* renamed from: X.1JJ  reason: invalid class name */
public final class AnonymousClass1JJ extends C17770zR {
    @Comparable(type = 13)
    public A0T A00;
    @Comparable(type = 14)
    public AnonymousClass5EU A01 = new AnonymousClass5EU();
    @Comparable(type = 13)
    public Boolean A02;
    @Comparable(type = 13)
    public Boolean A03;
    @Comparable(type = 13)
    public String A04;
    @Comparable(type = 13)
    public String A05;
    @Comparable(type = 13)
    public String A06;
    @Comparable(type = 3)
    public boolean A07;
    @Comparable(type = 3)
    public boolean A08;

    public static C17770zR A00(AnonymousClass0p4 r8, AnonymousClass10N r9, C45722Nf r10, int i, boolean z, boolean z2) {
        String[] strArr = {"stateContainer"};
        BitSet bitSet = new BitSet(1);
        C17020yC r7 = new C17020yC();
        C17540z4 r5 = r8.A0B;
        C17770zR r1 = r8.A04;
        if (r1 != null) {
            r7.A07 = r1.A06;
        }
        bitSet.clear();
        r7.A03 = r10;
        bitSet.set(0);
        r7.A06 = z;
        r7.A05 = r5.A09(i);
        r7.A14().A0O(r8.A0C(i));
        r7.A02 = r9;
        r7.A14().BwM(AnonymousClass10G.A09, r5.A00((float) AnonymousClass5UN.SMALL.B3A()));
        r7.A14().AaB(0.0f);
        if (z2) {
            r7.A01 = C17780zS.A0E(AnonymousClass1JJ.class, r8, 96515278, new Object[]{r8});
        }
        AnonymousClass11F.A0C(1, bitSet, strArr);
        return r7;
    }

    public AnonymousClass1JJ() {
        super("AccountPasswordSetupComponent");
    }

    public static void A01(AnonymousClass0p4 r3, boolean z, A0T a0t) {
        if (z != a0t.A02()) {
            boolean z2 = !z;
            if (r3.A04 != null) {
                r3.A0G(new C61322yh(0, Boolean.valueOf(z2)), "updateState:AccountPasswordSetupComponent.setSaveButtonEnabled");
            }
        }
    }

    public C17770zR A16() {
        AnonymousClass1JJ r1 = (AnonymousClass1JJ) super.A16();
        r1.A01 = new AnonymousClass5EU();
        return r1;
    }
}
