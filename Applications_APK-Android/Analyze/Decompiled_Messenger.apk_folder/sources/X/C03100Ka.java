package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0Ka  reason: invalid class name and case insensitive filesystem */
public final class C03100Ka implements FilenameFilter {
    public boolean accept(File file, String str) {
        return str.matches("cpu\\d+");
    }
}
