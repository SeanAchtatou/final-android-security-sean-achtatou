package X;

import android.content.Context;
import android.content.Intent;
import android.content.ReceiverCallNotAllowedException;
import android.view.WindowManager;
import com.facebook.rtc.activities.RtcDialogActivity;
import com.facebook.rtc.logging.WebrtcLoggingHandler;

/* renamed from: X.22b  reason: invalid class name and case insensitive filesystem */
public final class C405622b {
    public AnonymousClass3At A00;
    private final WebrtcLoggingHandler A01;

    public static final C405622b A00(AnonymousClass1XY r1) {
        return new C405622b(r1);
    }

    private static void A01(Context context, String str, String str2) {
        C417626w.A06(new Intent(context, RtcDialogActivity.class).setAction("com.facebook.rtc.activities.intent.action.ACTION_DIALOG").putExtra("MESSAGE", str).putExtra(C22298Ase.$const$string(6), str2).setFlags(268435456), context);
    }

    public void A02() {
        AnonymousClass3At r0 = this.A00;
        if (r0 != null && r0.isShowing()) {
            try {
                this.A00.dismiss();
            } catch (IllegalArgumentException e) {
                this.A01.A0E(e.toString());
            }
        }
        this.A00 = null;
    }

    public void A03(Context context, String str, String str2, boolean z) {
        if (str == null) {
            str = context.getString(2131835006);
        }
        if (str2 == null) {
            str2 = context.getString(2131835011);
        }
        if (z) {
            A01(context, str, str2);
            return;
        }
        C13500rX r2 = new C13500rX(context);
        r2.A0D(str2);
        r2.A0C(str);
        r2.A05(context.getString(2131823866), new C119145kN(this));
        AnonymousClass3At A06 = r2.A06();
        this.A00 = A06;
        A06.setOnDismissListener(new C119135kM(this));
        boolean z2 = true;
        try {
            this.A00.show();
            z2 = false;
        } catch (ReceiverCallNotAllowedException | WindowManager.BadTokenException | SecurityException unused) {
        }
        if (z2) {
            this.A00 = null;
            A01(context, str, str2);
        }
    }

    public C405622b(AnonymousClass1XY r2) {
        this.A01 = WebrtcLoggingHandler.A01(r2);
    }
}
