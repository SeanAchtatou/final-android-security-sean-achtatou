package X;

/* renamed from: X.0YR  reason: invalid class name */
public final class AnonymousClass0YR implements C05130Xt {
    public final /* synthetic */ AnonymousClass0Y6 A00;

    public AnonymousClass0YR(AnonymousClass0Y6 r1) {
        this.A00 = r1;
    }

    public void Btw(boolean z) {
        AnonymousClass0Y6 r2 = this.A00;
        r2.A0O.lock();
        try {
            AnonymousClass0Y6.A08(r2);
        } finally {
            r2.A0O.unlock();
        }
    }
}
