package X;

import java.io.Writer;

/* renamed from: X.01I  reason: invalid class name */
public final class AnonymousClass01I implements AnonymousClass01J {
    public boolean ANg(Writer writer, AnonymousClass0HM r6) {
        if (r6 != null) {
            r6.onCodePoint$REDEX$yYf5PajrCI7(AnonymousClass07B.A0C);
        }
        AnonymousClass022 A00 = AnonymousClass02D.A00();
        writer.append((CharSequence) "\"addressSpacePeakKB\":");
        writer.append((CharSequence) Long.toString(A00.A01));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"addressSpaceCurrentKB\":");
        writer.append((CharSequence) Long.toString(A00.A00));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"rssPeakKB\":");
        writer.append((CharSequence) Long.toString(A00.A03));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"rssCurrentKB\":");
        writer.append((CharSequence) Long.toString(A00.A02));
        return true;
    }
}
