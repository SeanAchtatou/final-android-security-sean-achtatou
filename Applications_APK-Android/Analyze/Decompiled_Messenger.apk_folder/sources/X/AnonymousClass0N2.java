package X;

import java.io.Writer;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.0N2  reason: invalid class name */
public final class AnonymousClass0N2 implements AnonymousClass01J {
    public boolean ANg(Writer writer, AnonymousClass0HM r15) {
        int i;
        Boolean bool;
        boolean z;
        long nanoTime = System.nanoTime();
        try {
            AnonymousClass0DE r8 = new AnonymousClass0DE();
            Object newInstance = r8.A02.newInstance(new Object[0]);
            do {
                bool = (Boolean) r8.A04.invoke(null, newInstance);
                if (bool == null) {
                    break;
                }
            } while (!bool.booleanValue());
            synchronized (newInstance) {
                long nanoTime2 = System.nanoTime() + 100000000;
                while (!r8.A03.getBoolean(newInstance)) {
                    long nanoTime3 = System.nanoTime();
                    if (nanoTime3 >= nanoTime2) {
                        break;
                    }
                    long j = nanoTime2 - nanoTime3;
                    newInstance.wait(j / 1000000, (int) (j % 1000000));
                }
                z = r8.A03.getBoolean(newInstance);
            }
            if (z) {
                i = 1;
            } else {
                i = 2;
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | InterruptedException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            i = AnonymousClass0DE.A00(e);
        }
        long nanoTime4 = System.nanoTime();
        if (i == 1) {
            writer.append((CharSequence) "\"finalizer_latency_ms\":");
            writer.append((CharSequence) Long.toString((nanoTime4 - nanoTime) / 1000000));
            return true;
        } else if (i == 2) {
            writer.append((CharSequence) "\"finalizer_latency_error\":\"timeout\"");
            return true;
        } else {
            writer.append((CharSequence) "\"finalizer_latency_error\":\"other\"");
            return true;
        }
    }
}
