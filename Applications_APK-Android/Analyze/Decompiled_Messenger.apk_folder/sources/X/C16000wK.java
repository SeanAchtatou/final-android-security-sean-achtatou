package X;

/* renamed from: X.0wK  reason: invalid class name and case insensitive filesystem */
public final class C16000wK {
    public static boolean A00(C13060qW r1) {
        if (!(r1 instanceof C13050qV)) {
            return false;
        }
        return ((C13050qV) r1).A00;
    }

    public static boolean A01(C13060qW r1) {
        if (!(r1 instanceof C13050qV)) {
            return false;
        }
        return !((C13050qV) r1).A13();
    }
}
