package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.common.calltoaction.model.CTAPaymentInfo;

/* renamed from: X.1hS  reason: invalid class name and case insensitive filesystem */
public final class C30081hS implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new CTAPaymentInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new CTAPaymentInfo[i];
    }
}
