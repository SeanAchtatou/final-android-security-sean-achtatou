package X;

import android.content.Context;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1KX  reason: invalid class name */
public final class AnonymousClass1KX extends C17770zR {
    @Comparable(type = 0)
    public float A00;
    @Comparable(type = 0)
    public float A01;
    @Comparable(type = 3)
    public int A02;
    @Comparable(type = 3)
    public int A03;
    @Comparable(type = 3)
    public int A04;
    @Comparable(type = 3)
    public int A05;
    @Comparable(type = 13)
    public Path A06;
    @Comparable(type = 13)
    public Typeface A07;
    @Comparable(type = 13)
    public Drawable A08;
    public AnonymousClass0UN A09;
    @Comparable(type = 14)
    public AnonymousClass1KY A0A = new AnonymousClass1KY();
    @Comparable(type = 13)
    public AnonymousClass1JY A0B;
    @Comparable(type = 13)
    public AnonymousClass1KZ A0C;
    @Comparable(type = 13)
    public AnonymousClass1KU A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    @Comparable(type = 3)
    public boolean A0I;
    @Comparable(type = 3)
    public boolean A0J;
    @Comparable(type = 3)
    public boolean A0K;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1KX(Context context) {
        super("UserTileDrawableComponent");
        this.A09 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    public C17770zR A16() {
        AnonymousClass1KX r1 = (AnonymousClass1KX) super.A16();
        r1.A0E = null;
        r1.A0F = null;
        r1.A0G = null;
        r1.A0H = null;
        r1.A0A = new AnonymousClass1KY();
        return r1;
    }
}
