package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;

/* renamed from: X.1hW  reason: invalid class name and case insensitive filesystem */
public final class C30121hW implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new CallToAction(parcel);
    }

    public Object[] newArray(int i) {
        return new CallToAction[i];
    }
}
