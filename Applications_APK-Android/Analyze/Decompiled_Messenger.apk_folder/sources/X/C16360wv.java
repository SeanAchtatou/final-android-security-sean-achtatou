package X;

import android.animation.TimeInterpolator;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0wv  reason: invalid class name and case insensitive filesystem */
public class C16360wv extends C14030sU {
    public int A00;
    public ArrayList A01 = new ArrayList();
    public boolean A02 = true;
    public boolean A03 = false;
    private int A04 = 0;

    public C14030sU A07(int i, boolean z) {
        for (int i2 = 0; i2 < this.A01.size(); i2++) {
            ((C14030sU) this.A01.get(i2)).A07(i, z);
        }
        super.A07(i, z);
        return this;
    }

    public C14030sU A0B(View view) {
        for (int i = 0; i < this.A01.size(); i++) {
            ((C14030sU) this.A01.get(i)).A0B(view);
        }
        super.A0B(view);
        return this;
    }

    public C14030sU A0C(View view) {
        for (int i = 0; i < this.A01.size(); i++) {
            ((C14030sU) this.A01.get(i)).A0C(view);
        }
        super.A0C(view);
        return this;
    }

    public void A0d(int i) {
        for (int i2 = 0; i2 < this.A01.size(); i2++) {
            ((C14030sU) this.A01.get(i2)).A06(i);
        }
        super.A06(i);
    }

    public void A0e(int i) {
        if (i == 0) {
            this.A02 = true;
        } else if (i == 1) {
            this.A02 = false;
        } else {
            throw new AndroidRuntimeException(AnonymousClass08S.A09("Invalid parameter for TransitionSet ordering: ", i));
        }
    }

    public C14030sU A0A(TimeInterpolator timeInterpolator) {
        this.A04 |= 1;
        ArrayList arrayList = this.A01;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ((C14030sU) this.A01.get(i)).A0A(timeInterpolator);
            }
        }
        super.A0A(timeInterpolator);
        return this;
    }

    public void A0W(C97334ki r4) {
        if (A0a(r4.A00)) {
            Iterator it = this.A01.iterator();
            while (it.hasNext()) {
                C14030sU r1 = (C14030sU) it.next();
                if (r1.A0a(r4.A00)) {
                    r1.A0W(r4);
                    r4.A01.add(r1);
                }
            }
        }
    }

    public void A0X(C97334ki r4) {
        if (A0a(r4.A00)) {
            Iterator it = this.A01.iterator();
            while (it.hasNext()) {
                C14030sU r1 = (C14030sU) it.next();
                if (r1.A0a(r4.A00)) {
                    r1.A0X(r4);
                    r4.A01.add(r1);
                }
            }
        }
    }

    public void A0h(C14030sU r6) {
        this.A01.add(r6);
        r6.A07 = this;
        long j = this.A01;
        if (j >= 0) {
            r6.A08(j);
        }
        if ((this.A04 & 1) != 0) {
            r6.A0A(this.A03);
        }
        if ((this.A04 & 2) != 0) {
            r6.A0U(this.A06);
        }
        if ((this.A04 & 4) != 0) {
            r6.A0S(this.A04);
        }
        if ((this.A04 & 8) != 0) {
            r6.A0T(this.A05);
        }
    }

    public C14030sU A05() {
        C16360wv r3 = (C16360wv) super.clone();
        r3.A01 = new ArrayList();
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            r3.A0h(((C14030sU) this.A01.get(i)).clone());
        }
        return r3;
    }

    public C14030sU A0D(ViewGroup viewGroup) {
        super.A0D(viewGroup);
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0D(viewGroup);
        }
        return this;
    }

    public String A0I(String str) {
        String A0I = super.A0I(str);
        for (int i = 0; i < this.A01.size(); i++) {
            A0I = AnonymousClass08S.A0P(A0I, "\n", ((C14030sU) this.A01.get(i)).A0I(AnonymousClass08S.A0J(str, "  ")));
        }
        return A0I;
    }

    public void A0J() {
        super.A0J();
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0J();
        }
    }

    public void A0N(View view) {
        super.A0N(view);
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0N(view);
        }
    }

    public void A0O(View view) {
        super.A0O(view);
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0O(view);
        }
    }

    public void A0P(ViewGroup viewGroup) {
        super.A0P(viewGroup);
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0P(viewGroup);
        }
    }

    public void A0S(C28335Dt1 dt1) {
        super.A0S(dt1);
        this.A04 |= 4;
        for (int i = 0; i < this.A01.size(); i++) {
            ((C14030sU) this.A01.get(i)).A0S(dt1);
        }
    }

    public void A0T(C17130yN r4) {
        super.A0T(r4);
        this.A04 |= 8;
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0T(r4);
        }
    }

    public void A0U(C28304DsP dsP) {
        super.A0U(dsP);
        this.A04 |= 2;
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0U(dsP);
        }
    }

    public void A0V(C97334ki r4) {
        super.A0V(r4);
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0V(r4);
        }
    }

    public void A0Z(boolean z) {
        super.A0Z(z);
        int size = this.A01.size();
        for (int i = 0; i < size; i++) {
            ((C14030sU) this.A01.get(i)).A0Z(z);
        }
    }

    public void A0f(long j) {
        super.A08(j);
        if (this.A01 >= 0) {
            int size = this.A01.size();
            for (int i = 0; i < size; i++) {
                ((C14030sU) this.A01.get(i)).A08(j);
            }
        }
    }

    public /* bridge */ /* synthetic */ Object clone() {
        return clone();
    }

    public /* bridge */ /* synthetic */ C14030sU A06(int i) {
        A0d(i);
        return this;
    }

    public /* bridge */ /* synthetic */ C14030sU A08(long j) {
        A0f(j);
        return this;
    }

    public /* bridge */ /* synthetic */ C14030sU A09(long j) {
        super.A09(j);
        return this;
    }

    public C14030sU A0E(C17160yQ r1) {
        super.A0E(r1);
        return this;
    }

    public C14030sU A0F(C17160yQ r1) {
        super.A0F(r1);
        return this;
    }

    public void A0g(long j) {
        super.A09(j);
    }
}
