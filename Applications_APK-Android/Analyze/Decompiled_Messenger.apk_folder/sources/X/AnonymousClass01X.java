package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/* renamed from: X.01X  reason: invalid class name */
public final class AnonymousClass01X extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass01P A00;

    public AnonymousClass01X(AnonymousClass01P r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        String str;
        int A01 = C000700l.A01(-398200076);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            str = extras.getString("reason");
        } else {
            str = null;
        }
        AnonymousClass01P r2 = this.A00;
        if (!"recentapps".equals(str) || !r2.A0D.A02()) {
            AnonymousClass01P.A09(r2, null, AnonymousClass01Y.IN_BACKGROUND);
        }
        C000700l.A0D(intent, -539381793, A01);
    }
}
