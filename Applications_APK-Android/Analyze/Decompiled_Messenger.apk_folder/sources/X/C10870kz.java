package X;

import android.content.res.Resources;
import com.google.common.collect.ImmutableMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0kz  reason: invalid class name and case insensitive filesystem */
public final class C10870kz {
    private static volatile C10870kz A01;
    private final ImmutableMap A00;

    public static final C10870kz A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C10870kz.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C10870kz(C04490Ux.A0L(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public int A01(Integer num) {
        return ((Integer) this.A00.get(num)).intValue();
    }

    private C10870kz(Resources resources) {
        ImmutableMap.Builder builder = new ImmutableMap.Builder();
        builder.put(50, Integer.valueOf(C007106r.A03(resources, 50.0f)));
        builder.put(80, Integer.valueOf(C007106r.A03(resources, 80.0f)));
        builder.put(Integer.valueOf((int) AnonymousClass1Y3.A2Y), Integer.valueOf(C007106r.A03(resources, 320.0f)));
        this.A00 = builder.build();
    }
}
