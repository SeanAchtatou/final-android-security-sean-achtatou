package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0RT  reason: invalid class name */
public final class AnonymousClass0RT {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "DUPLICATED_NOTIFICATION";
            case 2:
                return "DELIVERYHELPER_FAILED";
            case 3:
                return "DISCARDED_NOTIFICATION";
            case 4:
                return "ACKNOWLEDGED_NOTIFICATION";
            case 5:
                return "FAIL_NULL_NOTIF_ID";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "FAIL_INVALID_RECEIVER";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "FAIL_UNTRUSTED_APP";
            case 8:
                return "FAIL_SECURE_BROADCAST";
            case Process.SIGKILL /*9*/:
                return "REDELIVER_NOTIFICATION";
            default:
                return "NOTIFICATION_RECEIVED";
        }
    }
}
