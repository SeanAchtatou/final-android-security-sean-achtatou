package X;

import java.util.Map;

/* renamed from: X.15P  reason: invalid class name */
public final class AnonymousClass15P implements AnonymousClass1LY {
    private final AnonymousClass15Q A00;

    public static final AnonymousClass15P A00(AnonymousClass1XY r1) {
        return new AnonymousClass15P(r1);
    }

    public void BgD(String str, String str2, Map map) {
        boolean equals = "native_newsfeed".equals(str);
        boolean equals2 = "native_newsfeed".equals(str2);
        if (!equals && equals2) {
            AnonymousClass15Q r4 = this.A00;
            AnonymousClass15R r3 = AnonymousClass15R.NEWSFEED;
            boolean A05 = AnonymousClass08Z.A05(32);
            if (A05) {
                C005505z.A03("FrameRateLimitEnable", -1010564007);
            }
            try {
                boolean z = false;
                if (r4.A03 == r3) {
                    z = true;
                }
                r4.A03 = r3;
                if (!r4.A04 || 0 != 0 || z) {
                    if (A05) {
                        C005505z.A00(-1368181432);
                    }
                } else if (A05) {
                    C005505z.A00(1012375594);
                }
            } catch (Throwable th) {
                th = th;
                if (A05) {
                    C005505z.A00(-999590026);
                }
                throw th;
            }
        } else if (equals && !equals2) {
            AnonymousClass15Q r42 = this.A00;
            AnonymousClass15R r32 = AnonymousClass15R.NEWSFEED;
            boolean A052 = AnonymousClass08Z.A05(32);
            if (A052) {
                C005505z.A03("FrameRateLimitDisable", 1900653754);
            }
            try {
                boolean z2 = false;
                if (r42.A03 == r32) {
                    z2 = true;
                }
                if (z2) {
                    r42.A03 = AnonymousClass15R.UNKNOWN;
                    if (!r42.A04 || 0 == 0) {
                        if (A052) {
                            C005505z.A00(-1056055364);
                        }
                    } else if (A052) {
                        C005505z.A00(-95340831);
                    }
                } else if (A052) {
                    C005505z.A00(1599440048);
                }
            } catch (Throwable th2) {
                th = th2;
                if (A052) {
                    C005505z.A00(1311410409);
                }
                throw th;
            }
        }
    }

    private AnonymousClass15P(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass15Q.A01(r2);
    }
}
