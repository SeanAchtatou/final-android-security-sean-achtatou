package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.05t  reason: invalid class name and case insensitive filesystem */
public final class C005205t implements FilenameFilter {
    public boolean accept(File file, String str) {
        if (!str.startsWith("override-") || !str.endsWith(".log")) {
            return false;
        }
        return true;
    }
}
