package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.graphservice.factory.GraphQLServiceFactory;
import com.facebook.graphservice.interfaces.TreeSerializer;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Set;

/* renamed from: X.1a4  reason: invalid class name and case insensitive filesystem */
public final class C25581a4 {
    public AnonymousClass0UN A00;
    public final C06140av A01;
    public final C09250go A02;
    private final String A03;
    private final String A04;
    private final String A05;

    public ImmutableList A00() {
        int i = AnonymousClass1Y3.BBd;
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerStart(5505220);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerTag(5505220, "all");
        Cursor query = this.A02.A06().query("units", new String[]{C06070an.A06.A00}, this.A01.A02(), this.A01.A04(), null, null, C06070an.A02.A00);
        try {
            ImmutableList.Builder builder = ImmutableList.builder();
            while (query.moveToNext()) {
                byte[] blob = query.getBlob(0);
                if (blob != null) {
                    builder.add((Object) ((C27161ck) ((TreeSerializer) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Aij, this.A00)).deserializeTreeFromByteBuffer(ByteBuffer.wrap(blob), C27161ck.class, 738428414)));
                } else {
                    throw new RuntimeException("Unable to load messenger inbox, no blob or tree_blob available.");
                }
            }
            ImmutableList build = builder.build();
            query.close();
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(5505220, 2);
            return build;
        } catch (IOException e) {
            C010708t.A0L("InboxUnitStoreReaderWriter", "Unable to deserialize inbox unit into tree", e);
            throw new RuntimeException(e);
        } catch (Throwable th) {
            query.close();
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(5505220, 2);
            throw th;
        }
    }

    public ImmutableMap A01(Set set) {
        SQLiteDatabase A06 = this.A02.A06();
        AnonymousClass0W6 r2 = C06070an.A01;
        C25601a6 A012 = C06160ax.A01(C06160ax.A04(r2.A00, set), this.A01);
        Cursor query = A06.query("units", new String[]{r2.A00, C06070an.A06.A00, C06070an.A02.A00, C06070an.A00.A00}, A012.A02(), A012.A04(), null, null, C06070an.A02.A00);
        try {
            ImmutableMap.Builder builder = ImmutableMap.builder();
            while (query.moveToNext()) {
                String string = query.getString(0);
                byte[] blob = query.getBlob(1);
                int i = query.getInt(2);
                boolean z = false;
                if (query.getInt(3) != 0) {
                    z = true;
                }
                if (blob != null) {
                    builder.put(string, new C29722Egy((C27161ck) ((TreeSerializer) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Aij, this.A00)).deserializeTreeFromByteBuffer(ByteBuffer.wrap(blob), C27161ck.class, 738428414), i, z));
                } else {
                    throw new RuntimeException("Unable to load messenger inbox, no blob or tree_blob available.");
                }
            }
            ImmutableMap build = builder.build();
            query.close();
            return build;
        } catch (IOException e) {
            C010708t.A0L("InboxUnitStoreReaderWriter", "Unable to deserialize inbox unit into tree", e);
            throw new RuntimeException(e);
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public void A02() {
        this.A02.A06().delete("units", this.A01.A02(), this.A01.A04());
    }

    public C25581a4(AnonymousClass1XY r4, C09250go r5, String str, String str2, String str3) {
        this.A00 = new AnonymousClass0UN(4, r4);
        this.A02 = r5;
        this.A05 = str;
        this.A03 = str2;
        this.A04 = str3;
        this.A01 = C06160ax.A01(C06160ax.A03(C06070an.A05.A00, str), C06160ax.A03(C06070an.A03.A00, str2), C06160ax.A03(C06070an.A04.A00, str3));
    }

    public void A03(C27161ck r10, int i, boolean z) {
        GraphQLMessengerInboxUnitType A0Q;
        String A0U = r10.A0U();
        if (A0U == null || (A0Q = r10.A0Q()) == null) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).CGS("InboxUnitStoreReaderWriter", "Trying to write incomplete unit to database.");
            return;
        }
        SQLiteDatabase A06 = this.A02.A06();
        ContentValues contentValues = new ContentValues();
        try {
            int i2 = AnonymousClass1Y3.Aij;
            AnonymousClass0UN r7 = this.A00;
            ByteBuffer serializeTreeToByteBuffer = ((TreeSerializer) AnonymousClass1XX.A02(2, i2, r7)).serializeTreeToByteBuffer(C27161ck.A01(r10, (GraphQLServiceFactory) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZv, r7)));
            byte[] bArr = new byte[serializeTreeToByteBuffer.remaining()];
            serializeTreeToByteBuffer.get(bArr);
            contentValues.put(C06070an.A01.A00, A0U);
            contentValues.put(C06070an.A02.A00, Integer.valueOf(i));
            contentValues.put(C06070an.A00.A00, Boolean.valueOf(z));
            contentValues.put(C06070an.A06.A00, bArr);
            contentValues.put(C06070an.A08.A00, Long.valueOf(r10.getTimeValue(815119367)));
            contentValues.put(C06070an.A07.A00, A0Q.name());
            contentValues.put(C06070an.A05.A00, this.A05);
            contentValues.put(C06070an.A03.A00, this.A03);
            contentValues.put(C06070an.A04.A00, this.A04);
            C007406x.A00(807634043);
            A06.replaceOrThrow("units", null, contentValues);
            C007406x.A00(344013642);
        } catch (IOException e) {
            C010708t.A0L("InboxUnitStoreReaderWriter", "Unable to serialize inbox unit tree to database", e);
        }
    }
}
