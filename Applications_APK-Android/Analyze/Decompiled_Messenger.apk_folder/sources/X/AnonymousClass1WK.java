package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1WK  reason: invalid class name */
public final class AnonymousClass1WK implements AnonymousClass1WL {
    public static AtomicReference A00 = new AtomicReference();

    public void BPH(boolean z) {
        synchronized (AnonymousClass1WN.A0A) {
            Iterator it = new ArrayList(AnonymousClass1WN.A0B.values()).iterator();
            while (it.hasNext()) {
                AnonymousClass1WN r1 = (AnonymousClass1WN) it.next();
                if (r1.A07.get()) {
                    for (C29266ESo onBackgroundStateChanged : r1.A05) {
                        onBackgroundStateChanged.onBackgroundStateChanged(z);
                    }
                }
            }
        }
    }
}
