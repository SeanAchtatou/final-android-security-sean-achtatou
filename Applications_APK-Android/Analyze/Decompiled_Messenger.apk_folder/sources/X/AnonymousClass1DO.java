package X;

import com.facebook.messaging.sharing.SingleRecipientShareLauncherActivity;
import java.util.concurrent.CancellationException;

/* renamed from: X.1DO  reason: invalid class name */
public final class AnonymousClass1DO extends C06020ai {
    public final /* synthetic */ SingleRecipientShareLauncherActivity A00;

    public AnonymousClass1DO(SingleRecipientShareLauncherActivity singleRecipientShareLauncherActivity) {
        this.A00 = singleRecipientShareLauncherActivity;
    }

    public void A03(CancellationException cancellationException) {
        SingleRecipientShareLauncherActivity singleRecipientShareLauncherActivity = this.A00;
        singleRecipientShareLauncherActivity.A0B = null;
        singleRecipientShareLauncherActivity.setResult(0);
        this.A00.finish();
    }
}
