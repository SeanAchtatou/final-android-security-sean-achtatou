package X;

import android.os.RemoteException;
import com.facebook.omnistore.OmnistoreMqtt;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Tj  reason: invalid class name and case insensitive filesystem */
public final class C24371Tj implements OmnistoreMqtt.Publisher {
    private static volatile C24371Tj A01;
    private final C29281gA A00;

    public void ensureConnection() {
    }

    public static final C24371Tj A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C24371Tj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C24371Tj(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void publishMessage(String str, byte[] bArr, OmnistoreMqtt.PublishCallback publishCallback) {
        C36991uR BvK = this.A00.BvK();
        try {
            if (BvK.A03(str, bArr, AnonymousClass07B.A01, new C57322rn(BvK, publishCallback)) == -1) {
                BvK.A06();
                publishCallback.onFailure();
            }
        } catch (RemoteException unused) {
            BvK.A06();
            publishCallback.onFailure();
        }
    }

    private C24371Tj(AnonymousClass1XY r2) {
        this.A00 = C24381Tk.A00(r2);
    }
}
