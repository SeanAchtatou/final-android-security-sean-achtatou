package X;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.cfg.PackageVersion;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0jJ  reason: invalid class name */
public class AnonymousClass0jJ extends C09980jK implements AnonymousClass0jN, Serializable {
    public static final C10140jc DEFAULT_ANNOTATION_INTROSPECTOR;
    public static final C10290jr DEFAULT_BASE;
    public static final C10060jU DEFAULT_INTROSPECTOR;
    public static final C10030jR JSON_NODE_TYPE = C10020jP.constructUnsafe(JsonNode.class);
    public static final C10160je STD_VISIBILITY_CHECKER;
    public static final C10210jj _defaultPrettyPrinter = new C10200ji();
    private static final long serialVersionUID = 1;
    public C10490kF _deserializationConfig;
    public AnonymousClass1c2 _deserializationContext;
    public final CYj _injectableValues;
    public final C10370jz _jsonFactory;
    public final HashMap _mixInAnnotations;
    public final ConcurrentHashMap _rootDeserializers;
    public final C10440k7 _rootNames;
    public C10450k8 _serializationConfig;
    public C26861cG _serializerFactory;
    public C11250mT _serializerProvider;
    public C10430k6 _subtypeResolver;
    public C10300js _typeFactory;

    public Object _readMapAndClose(C28271eX r9, C10030jR r10) {
        Object obj;
        C28271eX r3 = r9;
        try {
            C182811d _initForReading = _initForReading(r9);
            C10030jR r6 = r10;
            if (_initForReading == C182811d.VALUE_NULL) {
                obj = _findRootDeserializer(this._deserializationContext.createInstance(this._deserializationConfig, r9, this._injectableValues), r10).getNullValue();
            } else if (_initForReading == C182811d.END_ARRAY || _initForReading == C182811d.END_OBJECT) {
                obj = null;
            } else {
                C10490kF r5 = this._deserializationConfig;
                AnonymousClass1c2 createInstance = this._deserializationContext.createInstance(r5, r9, this._injectableValues);
                JsonDeserializer _findRootDeserializer = _findRootDeserializer(createInstance, r10);
                if (r5.useRootWrapping()) {
                    obj = _unwrapAndDeserialize(r3, createInstance, r5, r6, _findRootDeserializer);
                } else {
                    obj = _findRootDeserializer.deserialize(r9, createInstance);
                }
            }
            r9.clearCurrentToken();
            return obj;
        } finally {
            try {
                r9.close();
            } catch (IOException unused) {
            }
        }
    }

    public Object _readValue(C10490kF r8, C28271eX r9, C10030jR r10) {
        Object obj;
        C28271eX r2 = r9;
        C182811d _initForReading = _initForReading(r9);
        C10030jR r5 = r10;
        C10490kF r4 = r8;
        if (_initForReading == C182811d.VALUE_NULL) {
            obj = _findRootDeserializer(this._deserializationContext.createInstance(r8, r9, this._injectableValues), r10).getNullValue();
        } else if (_initForReading == C182811d.END_ARRAY || _initForReading == C182811d.END_OBJECT) {
            obj = null;
        } else {
            AnonymousClass1c2 createInstance = this._deserializationContext.createInstance(r8, r9, this._injectableValues);
            JsonDeserializer _findRootDeserializer = _findRootDeserializer(createInstance, r10);
            if (r8.useRootWrapping()) {
                obj = _unwrapAndDeserialize(r2, createInstance, r4, r5, _findRootDeserializer);
            } else {
                obj = _findRootDeserializer.deserialize(r9, createInstance);
            }
        }
        r9.clearCurrentToken();
        return obj;
    }

    static {
        C10050jT r2 = C10050jT.instance;
        DEFAULT_INTROSPECTOR = r2;
        C10130jb r3 = new C10130jb();
        DEFAULT_ANNOTATION_INTROSPECTOR = r3;
        C10150jd r4 = C10150jd.DEFAULT;
        STD_VISIBILITY_CHECKER = r4;
        DEFAULT_BASE = new C10290jr(r2, r3, r4, null, C10300js.instance, null, C10330jv.instance, null, Locale.getDefault(), TimeZone.getTimeZone("GMT"), C10340jw.MIME_NO_LINEFEEDS);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034 A[SYNTHETIC, Splitter:B:21:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0039 A[SYNTHETIC, Splitter:B:25:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void _configAndWriteValue(X.AnonymousClass0jJ r4, X.C11710np r5, java.lang.Object r6) {
        /*
            X.0k8 r3 = r4._serializationConfig
            X.0kE r0 = X.C10480kE.INDENT_OUTPUT
            boolean r0 = r3.isEnabled(r0)
            if (r0 == 0) goto L_0x000d
            r5.useDefaultPrettyPrinter()
        L_0x000d:
            X.0kE r0 = X.C10480kE.CLOSE_CLOSEABLE
            boolean r0 = r3.isEnabled(r0)
            if (r0 == 0) goto L_0x003d
            boolean r0 = r6 instanceof java.io.Closeable
            if (r0 == 0) goto L_0x003d
            r2 = r6
            java.io.Closeable r2 = (java.io.Closeable) r2
            r1 = 0
            X.0mT r0 = r4._serializerProvider(r3)     // Catch:{ all -> 0x002d }
            r0.serializeValue(r5, r6)     // Catch:{ all -> 0x002d }
            r5.close()     // Catch:{ all -> 0x002b }
            r2.close()     // Catch:{ all -> 0x0030 }
            return
        L_0x002b:
            r0 = move-exception
            goto L_0x0032
        L_0x002d:
            r0 = move-exception
            r1 = r5
            goto L_0x0032
        L_0x0030:
            r0 = move-exception
            r2 = r1
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0037:
            if (r2 == 0) goto L_0x0050
            r2.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x0050
        L_0x003d:
            r1 = 0
            X.0mT r0 = r4._serializerProvider(r3)     // Catch:{ all -> 0x004a }
            r0.serializeValue(r5, r6)     // Catch:{ all -> 0x004a }
            r1 = 1
            r5.close()     // Catch:{ all -> 0x004a }
            return
        L_0x004a:
            r0 = move-exception
            if (r1 != 0) goto L_0x0050
            r5.close()     // Catch:{ IOException -> 0x0050 }
        L_0x0050:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0jJ._configAndWriteValue(X.0jJ, X.0np, java.lang.Object):void");
    }

    private Object _unwrapAndDeserialize(C28271eX r6, C26791c3 r7, C10490kF r8, C10030jR r9, JsonDeserializer jsonDeserializer) {
        String str = r8._rootName;
        if (str == null) {
            str = this._rootNames.findRootName(r9._class, r8).getValue();
        }
        if (r6.getCurrentToken() != C182811d.START_OBJECT) {
            throw C37701w6.from(r6, "Current token not START_OBJECT (needed to unwrap root name '" + str + "'), but " + r6.getCurrentToken());
        } else if (r6.nextToken() == C182811d.FIELD_NAME) {
            String currentName = r6.getCurrentName();
            if (str.equals(currentName)) {
                r6.nextToken();
                Object deserialize = jsonDeserializer.deserialize(r6, r7);
                if (r6.nextToken() == C182811d.END_OBJECT) {
                    return deserialize;
                }
                throw C37701w6.from(r6, "Current token not END_OBJECT (to match wrapper object with root name '" + str + "'), but " + r6.getCurrentToken());
            }
            throw C37701w6.from(r6, "Root name '" + currentName + "' does not match expected ('" + str + "') for type " + r9);
        } else {
            throw C37701w6.from(r6, "Current token not FIELD_NAME (to contain expected root name '" + str + "'), but " + r6.getCurrentToken());
        }
    }

    public JsonDeserializer _findRootDeserializer(C26791c3 r4, C10030jR r5) {
        JsonDeserializer jsonDeserializer = (JsonDeserializer) this._rootDeserializers.get(r5);
        if (jsonDeserializer != null) {
            return jsonDeserializer;
        }
        JsonDeserializer findRootValueDeserializer = r4.findRootValueDeserializer(r5);
        if (findRootValueDeserializer != null) {
            this._rootDeserializers.put(r5, findRootValueDeserializer);
            return findRootValueDeserializer;
        }
        throw new C37701w6("Can not find a deserializer for type " + r5);
    }

    public C11250mT _serializerProvider(C10450k8 r3) {
        return this._serializerProvider.createInstance(r3, this._serializerFactory);
    }

    public Object convertValue(Object obj, Class cls) {
        C10450k8 r1;
        Object obj2;
        if (obj == null) {
            return null;
        }
        C10030jR _constructType = this._typeFactory._constructType(cls, null);
        Class<Object> cls2 = _constructType._class;
        if (cls2 != Object.class && !_constructType.hasGenericTypes() && cls2.isAssignableFrom(obj.getClass())) {
            return obj;
        }
        C11700no r5 = new C11700no(this);
        try {
            C10450k8 r3 = this._serializationConfig;
            C10480kE r0 = C10480kE.WRAP_ROOT_VALUE;
            int i = r3._serFeatures;
            int mask = (r0.getMask() ^ -1) & i;
            if (mask == i) {
                r1 = r3;
            } else {
                r1 = new C10450k8(r3, r3._mapperFeatures, mask);
            }
            _serializerProvider(r1).serializeValue(r5, obj);
            C28271eX asParser = r5.asParser();
            C10490kF r2 = this._deserializationConfig;
            C182811d _initForReading = _initForReading(asParser);
            if (_initForReading == C182811d.VALUE_NULL) {
                obj2 = _findRootDeserializer(this._deserializationContext.createInstance(r2, asParser, this._injectableValues), _constructType).getNullValue();
            } else if (_initForReading == C182811d.END_ARRAY || _initForReading == C182811d.END_OBJECT) {
                obj2 = null;
            } else {
                AnonymousClass1c2 createInstance = this._deserializationContext.createInstance(r2, asParser, this._injectableValues);
                obj2 = _findRootDeserializer(createInstance, _constructType).deserialize(asParser, createInstance);
            }
            asParser.close();
            return obj2;
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public ArrayNode createArrayNode() {
        return new ArrayNode(this._deserializationConfig._nodeFactory);
    }

    public ObjectNode createObjectNode() {
        return new ObjectNode(this._deserializationConfig._nodeFactory);
    }

    public C10370jz getFactory() {
        return this._jsonFactory;
    }

    public C10370jz getJsonFactory() {
        return this._jsonFactory;
    }

    public /* bridge */ /* synthetic */ Iterator readValues(C28271eX r11, Class cls) {
        C10030jR _constructType = this._typeFactory._constructType(cls, null);
        AnonymousClass1c2 createInstance = this._deserializationContext.createInstance(this._deserializationConfig, r11, this._injectableValues);
        return new CYa(_constructType, r11, createInstance, _findRootDeserializer(createInstance, _constructType), false, null);
    }

    public Object treeToValue(C10010jO r4, Class cls) {
        if (cls != Object.class) {
            try {
                if (cls.isAssignableFrom(r4.getClass())) {
                    return r4;
                }
            } catch (C37571vt e) {
                throw e;
            } catch (IOException e2) {
                throw new IllegalArgumentException(e2.getMessage(), e2);
            }
        }
        return readValue(new C25099CXo((JsonNode) r4, this), cls);
    }

    public JsonNode valueToTree(Object obj) {
        if (obj == null) {
            return null;
        }
        C11700no r0 = new C11700no(this);
        try {
            writeValue(r0, obj);
            C28271eX asParser = r0.asParser();
            JsonNode jsonNode = (JsonNode) readTree(asParser);
            asParser.close();
            return jsonNode;
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public C11780nw version() {
        return PackageVersion.VERSION;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0039 A[SYNTHETIC, Splitter:B:20:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeValue(X.C11710np r4, java.lang.Object r5) {
        /*
            r3 = this;
            X.0k8 r1 = r3._serializationConfig
            X.0kE r0 = X.C10480kE.INDENT_OUTPUT
            boolean r0 = r1.isEnabled(r0)
            if (r0 == 0) goto L_0x000d
            r4.useDefaultPrettyPrinter()
        L_0x000d:
            X.0kE r0 = X.C10480kE.CLOSE_CLOSEABLE
            boolean r0 = r1.isEnabled(r0)
            if (r0 == 0) goto L_0x003d
            boolean r0 = r5 instanceof java.io.Closeable
            if (r0 == 0) goto L_0x003d
            r2 = r5
            java.io.Closeable r2 = (java.io.Closeable) r2
            X.0mT r0 = r3._serializerProvider(r1)     // Catch:{ all -> 0x0033 }
            r0.serializeValue(r4, r5)     // Catch:{ all -> 0x0033 }
            X.0kE r0 = X.C10480kE.FLUSH_AFTER_WRITE_VALUE     // Catch:{ all -> 0x0033 }
            boolean r0 = r1.isEnabled(r0)     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x002e
            r4.flush()     // Catch:{ all -> 0x0033 }
        L_0x002e:
            r1 = 0
            r2.close()     // Catch:{ all -> 0x0035 }
            return
        L_0x0033:
            r0 = move-exception
            goto L_0x0037
        L_0x0035:
            r0 = move-exception
            r2 = r1
        L_0x0037:
            if (r2 == 0) goto L_0x003c
            r2.close()     // Catch:{ IOException -> 0x003c }
        L_0x003c:
            throw r0
        L_0x003d:
            X.0mT r0 = r3._serializerProvider(r1)
            r0.serializeValue(r4, r5)
            X.0kE r0 = X.C10480kE.FLUSH_AFTER_WRITE_VALUE
            boolean r0 = r1.isEnabled(r0)
            if (r0 == 0) goto L_0x004f
            r4.flush()
        L_0x004f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0jJ.writeValue(X.0np, java.lang.Object):void");
    }

    public byte[] writeValueAsBytes(Object obj) {
        A21 a21 = new A21(C10370jz._getBufferRecycler(), 500);
        try {
            _configAndWriteValue(this, this._jsonFactory.createGenerator(a21, C48392aM.UTF8), obj);
            byte[] byteArray = a21.toByteArray();
            a21.release();
            return byteArray;
        } catch (C37571vt e) {
            throw e;
        } catch (IOException e2) {
            throw C37701w6.fromUnexpectedIOE(e2);
        }
    }

    public String writeValueAsString(Object obj) {
        C48712av r2 = new C48712av(C10370jz._getBufferRecycler());
        try {
            _configAndWriteValue(this, this._jsonFactory.createGenerator(r2), obj);
            String contentsAsString = r2._buffer.contentsAsString();
            r2._buffer.releaseBuffers();
            return contentsAsString;
        } catch (C37571vt e) {
            throw e;
        } catch (IOException e2) {
            throw C37701w6.fromUnexpectedIOE(e2);
        }
    }

    public A24 writerWithDefaultPrettyPrinter() {
        return new A24(this, this._serializationConfig, null, _defaultPrettyPrinter);
    }

    private static C182811d _initForReading(C28271eX r1) {
        C182811d currentToken = r1.getCurrentToken();
        if (currentToken != null || (currentToken = r1.nextToken()) != null) {
            return currentToken;
        }
        throw C37701w6.from(r1, C22298Ase.$const$string(AnonymousClass1Y3.A19));
    }

    public AnonymousClass0jJ() {
        this(null, null, null);
    }

    public AnonymousClass0jJ(C10370jz r5, C11250mT r6, AnonymousClass1c2 r7) {
        this._mixInAnnotations = new HashMap();
        this._rootDeserializers = new ConcurrentHashMap(64, 0.6f, 2);
        if (r5 == null) {
            this._jsonFactory = new C36681tY(this);
        } else {
            this._jsonFactory = r5;
            if (r5.getCodec() == null) {
                r5._objectCodec = this;
            }
        }
        C26741bw r2 = new C26741bw();
        this._subtypeResolver = r2;
        this._rootNames = new C10440k7();
        this._typeFactory = C10300js.instance;
        C10290jr r3 = DEFAULT_BASE;
        this._serializationConfig = new C10450k8(r3, r2, this._mixInAnnotations);
        this._deserializationConfig = new C10490kF(r3, this._subtypeResolver, this._mixInAnnotations);
        this._serializerProvider = r6 == null ? new C10550kN() : r6;
        this._deserializationContext = r7 == null ? new C11400mr(AnonymousClass1c4.instance) : r7;
        this._serializerFactory = C26851cF.instance;
    }

    public C10010jO readTree(C28271eX r3) {
        C10490kF r1 = this._deserializationConfig;
        if (r3.getCurrentToken() == null && r3.nextToken() == null) {
            return null;
        }
        JsonNode jsonNode = (JsonNode) _readValue(r1, r3, JSON_NODE_TYPE);
        if (jsonNode == null) {
            return NullNode.instance;
        }
        return jsonNode;
    }

    public JsonNode readTree(String str) {
        JsonNode jsonNode = (JsonNode) _readMapAndClose(this._jsonFactory.createParser(str), JSON_NODE_TYPE);
        return jsonNode == null ? NullNode.instance : jsonNode;
    }

    public JsonNode readTree(byte[] bArr) {
        JsonNode jsonNode = (JsonNode) _readMapAndClose(this._jsonFactory.createParser(bArr), JSON_NODE_TYPE);
        return jsonNode == null ? NullNode.instance : jsonNode;
    }

    public Object readValue(C28271eX r2, C10030jR r3) {
        return _readValue(this._deserializationConfig, r2, r3);
    }

    public Object readValue(C28271eX r5, C28231eT r6) {
        return _readValue(this._deserializationConfig, r5, this._typeFactory._constructType(r6._type, null));
    }

    public Object readValue(C28271eX r4, Class cls) {
        return _readValue(this._deserializationConfig, r4, this._typeFactory._constructType(cls, null));
    }

    public Object readValue(String str, C28231eT r6) {
        return _readMapAndClose(this._jsonFactory.createParser(str), this._typeFactory._constructType(r6._type, null));
    }

    public Object readValue(String str, Class cls) {
        return _readMapAndClose(this._jsonFactory.createParser(str), this._typeFactory._constructType(cls, null));
    }
}
