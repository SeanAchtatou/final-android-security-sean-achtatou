package X;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.facebook.crudolib.sqliteproc.annotations.DefaultDataMigrator;
import com.facebook.crudolib.sqliteproc.annotations.DropAllTablesDataMigrator;
import com.facebook.crudolib.sqliteproc.annotations.DropTableDataMigrator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0r8  reason: invalid class name */
public final class AnonymousClass0r8 {
    public final C13610rj A00;
    public final C13620rk A01;
    public final C11340ml A02;
    public final boolean A03;

    private static void A06(SQLiteDatabase sQLiteDatabase, String str, BJH[] bjhArr) {
        for (BJH bjh : bjhArr) {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE ");
            if (bjh.A00) {
                sb.append("UNIQUE ");
            }
            sb.append("INDEX ");
            sb.append(str);
            String[] strArr = bjh.A01;
            for (String append : strArr) {
                sb.append("_");
                sb.append(append);
            }
            sb.append(" ON ");
            sb.append(str);
            sb.append("(");
            sb.append(strArr[0]);
            String[] strArr2 = bjh.A02;
            String str2 = strArr2[0];
            if (!str2.isEmpty()) {
                sb.append(" ");
                sb.append(str2);
            }
            for (int i = 1; i < r7; i++) {
                sb.append(',');
                sb.append(strArr[i]);
                String str3 = strArr2[i];
                if (!str3.isEmpty()) {
                    sb.append(" ");
                    sb.append(str3);
                }
            }
            sb.append(")");
            String sb2 = sb.toString();
            C007406x.A00(-1493524764);
            sQLiteDatabase.execSQL(sb2);
            C007406x.A00(-1741596280);
        }
    }

    private static void A09(String str, BJR[] bjrArr, Map map) {
        for (BJR bjr : bjrArr) {
            String str2 = bjr.A04;
            if (str2 != null) {
                Object obj = (Set) map.get(str2);
                if (obj == null) {
                    obj = new HashSet();
                    map.put(str2, obj);
                }
                obj.add(str);
            }
        }
    }

    private static void A00(SQLiteDatabase sQLiteDatabase, C22890BJj bJj, BJR[] bjrArr, BJH[] bjhArr) {
        String A0J = AnonymousClass08S.A0J("DROP TABLE IF EXISTS ", bJj.A01);
        C007406x.A00(-463443053);
        sQLiteDatabase.execSQL(A0J);
        C007406x.A00(-474708895);
        A05(sQLiteDatabase, bJj.A01, bjrArr, bjhArr);
    }

    private static void A01(SQLiteDatabase sQLiteDatabase, C22890BJj bJj, BJR[] bjrArr, BJH[] bjhArr, C11380mp r11) {
        StringBuilder sb = new StringBuilder();
        int length = bjrArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            BJR bjr = bjrArr[i];
            if (!bjr.A0C) {
                sb.append(bjr.A05);
                break;
            }
            i++;
        }
        while (true) {
            i++;
            if (i >= length) {
                break;
            }
            BJR bjr2 = bjrArr[i];
            if (!bjr2.A0C) {
                sb.append(", ");
                sb.append(bjr2.A05);
            }
        }
        String sb2 = sb.toString();
        if (TextUtils.isEmpty(sb2)) {
            A00(sQLiteDatabase, bJj, bjrArr, bjhArr);
            return;
        }
        AnonymousClass7W3.A00(sQLiteDatabase, "recreate_table_savepoint");
        SQLException e = null;
        try {
            String A0J = AnonymousClass08S.A0J("_temp_", bJj.A01);
            A05(sQLiteDatabase, A0J, bjrArr, bjhArr);
            A04(sQLiteDatabase, bJj.A01, A0J, sb2);
            String A0J2 = AnonymousClass08S.A0J("DROP TABLE ", bJj.A01);
            C007406x.A00(-2036823505);
            sQLiteDatabase.execSQL(A0J2);
            C007406x.A00(1172688247);
            A05(sQLiteDatabase, bJj.A01, bjrArr, bjhArr);
            A04(sQLiteDatabase, A0J, bJj.A01, sb2);
            String A0J3 = AnonymousClass08S.A0J("DROP TABLE ", A0J);
            C007406x.A00(875723827);
            sQLiteDatabase.execSQL(A0J3);
            C007406x.A00(-567561128);
        } catch (SQLException e2) {
            e = e2;
            AnonymousClass7W3.A02(sQLiteDatabase, "recreate_table_savepoint");
        } catch (Throwable th) {
            AnonymousClass7W3.A01(sQLiteDatabase, "recreate_table_savepoint");
            throw th;
        }
        AnonymousClass7W3.A01(sQLiteDatabase, "recreate_table_savepoint");
        if (e != null) {
            r11.BVe(AnonymousClass08S.A0P("Failed to migrate data for table ", bJj.A01, "."), e);
            A00(sQLiteDatabase, bJj, bjrArr, bjhArr);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002b A[Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A03(android.database.sqlite.SQLiteDatabase r5, java.lang.String r6, X.BT7 r7, X.C11380mp r8) {
        /*
            java.lang.String r2 = "Failed to instantiate "
            r8.BVW(r6)
            java.lang.Class r0 = java.lang.Class.forName(r6)     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            X.8yr r0 = (X.C192128yr) r0     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            java.lang.String r1 = "migrate_data_savepoint"
            X.AnonymousClass7W3.A00(r5, r1)     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            r0.BL5(r5, r7)     // Catch:{ 1wE -> 0x0020, SQLException -> 0x001b }
            X.AnonymousClass7W3.A01(r5, r1)     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            goto L_0x0028
        L_0x001b:
            r4 = move-exception
            X.AnonymousClass7W3.A02(r5, r1)     // Catch:{ all -> 0x003f }
            goto L_0x0024
        L_0x0020:
            r4 = move-exception
            X.AnonymousClass7W3.A02(r5, r1)     // Catch:{ all -> 0x003f }
        L_0x0024:
            X.AnonymousClass7W3.A01(r5, r1)     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            goto L_0x0029
        L_0x0028:
            r4 = 0
        L_0x0029:
            if (r4 == 0) goto L_0x0058
            java.lang.String r3 = "Failed to migrate data with "
            java.lang.Class r0 = r0.getClass()     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            java.lang.String r1 = r0.getSimpleName()     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            java.lang.String r0 = "."
            java.lang.String r0 = X.AnonymousClass08S.A0P(r3, r1, r0)     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            r8.BVe(r0, r4)     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            goto L_0x0058
        L_0x003f:
            r0 = move-exception
            X.AnonymousClass7W3.A01(r5, r1)     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
            throw r0     // Catch:{ IllegalAccessException -> 0x0048, InstantiationException -> 0x0044, ClassNotFoundException -> 0x004c }
        L_0x0044:
            r1 = move-exception
            java.lang.String r0 = " because class does not have empty constructor."
            goto L_0x004f
        L_0x0048:
            r1 = move-exception
            java.lang.String r0 = " because constructor is not accessible."
            goto L_0x004f
        L_0x004c:
            r1 = move-exception
            java.lang.String r0 = " because class was not found."
        L_0x004f:
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r6, r0)
            r8.BVe(r0, r1)
            r0 = 0
            goto L_0x005c
        L_0x0058:
            r0 = 0
            if (r4 != 0) goto L_0x005c
            r0 = 1
        L_0x005c:
            r8.BVV(r6, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0r8.A03(android.database.sqlite.SQLiteDatabase, java.lang.String, X.BT7, X.0mp):void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002b A[Catch:{ all -> 0x005c }] */
    public static void A05(android.database.sqlite.SQLiteDatabase r5, java.lang.String r6, X.BJR[] r7, X.BJH[] r8) {
        /*
            java.lang.String r1 = "createTableWithIndices"
            r0 = 1251039575(0x4a915957, float:4762795.5)
            X.AnonymousClass06K.A01(r1, r0)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x005c }
            r4.<init>()     // Catch:{ all -> 0x005c }
            java.lang.String r0 = "CREATE TABLE "
            r4.append(r0)     // Catch:{ all -> 0x005c }
            r4.append(r6)     // Catch:{ all -> 0x005c }
            java.lang.String r0 = " ("
            r4.append(r0)     // Catch:{ all -> 0x005c }
            int r3 = r7.length     // Catch:{ all -> 0x005c }
            r2 = 0
        L_0x001c:
            if (r2 >= r3) goto L_0x0027
            r1 = r7[r2]     // Catch:{ all -> 0x005c }
            boolean r0 = r1.A0C     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0036
            int r2 = r2 + 1
            goto L_0x001c
        L_0x0027:
            int r2 = r2 + 1
            if (r2 >= r3) goto L_0x003a
            r1 = r7[r2]     // Catch:{ all -> 0x005c }
            boolean r0 = r1.A0C     // Catch:{ all -> 0x005c }
            if (r0 != 0) goto L_0x0027
            java.lang.String r0 = ", "
            r4.append(r0)     // Catch:{ all -> 0x005c }
        L_0x0036:
            A0A(r4, r1)     // Catch:{ all -> 0x005c }
            goto L_0x0027
        L_0x003a:
            r0 = 41
            r4.append(r0)     // Catch:{ all -> 0x005c }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x005c }
            r0 = -1434718027(0xffffffffaa7bf0b5, float:-2.237679E-13)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x005c }
            r5.execSQL(r1)     // Catch:{ all -> 0x005c }
            r0 = 478050478(0x1c7e78ae, float:8.4197525E-22)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x005c }
            A06(r5, r6, r8)     // Catch:{ all -> 0x005c }
            r0 = -16535361(0xffffffffff03b0bf, float:-1.7504659E38)
            X.AnonymousClass06K.A00(r0)
            return
        L_0x005c:
            r1 = move-exception
            r0 = 1228390264(0x4937bf78, float:752631.5)
            X.AnonymousClass06K.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0r8.A05(android.database.sqlite.SQLiteDatabase, java.lang.String, X.BJR[], X.BJH[]):void");
    }

    private static void A08(String str, String str2, boolean z) {
        String A0S = AnonymousClass08S.A0S("[", str, "]: ", str2);
        if (!z) {
            C010708t.A0I("SchemaMigrator", A0S);
            return;
        }
        throw new UnsupportedOperationException(A0S);
    }

    private static void A0A(StringBuilder sb, BJR bjr) {
        sb.append(bjr.A05);
        sb.append(" ");
        sb.append(bjr.A09);
        sb.append(" ");
        String str = bjr.A01;
        if (str != null) {
            sb.append("DEFAULT ");
            sb.append(str);
            sb.append(" ");
        }
        if (!bjr.A0D) {
            sb.append("NOT NULL ");
        }
        if (bjr.A0E) {
            sb.append("PRIMARY KEY ");
        }
        if (bjr.A0B) {
            sb.append("AUTOINCREMENT ");
        }
        String str2 = bjr.A04;
        if (str2 != null || bjr.A03 != null) {
            sb.append("REFERENCES ");
            sb.append(str2);
            sb.append("(");
            sb.append(bjr.A03);
            sb.append(")");
            sb.append(" ON UPDATE ");
            sb.append(bjr.A07);
            sb.append(" ON DELETE ");
            sb.append(bjr.A06);
        }
    }

    public static BJR[] A0C(C13620rk r19, String str) {
        BK5 bk5 = new BK5(r19.A00(new BKA(str)));
        try {
            BJR[] bjrArr = new BJR[bk5.getCount()];
            int i = 0;
            while (bk5.moveToNext()) {
                String string = bk5.A01.getString(1);
                String string2 = bk5.A01.getString(2);
                String string3 = bk5.A01.getString(3);
                boolean z = false;
                if (bk5.A01.getInt(4) != 0) {
                    z = true;
                }
                boolean z2 = false;
                if (bk5.A01.getInt(5) != 0) {
                    z2 = true;
                }
                boolean z3 = false;
                if (bk5.A01.getInt(6) != 0) {
                    z3 = true;
                }
                boolean z4 = false;
                if (bk5.A01.getInt(7) != 0) {
                    z4 = true;
                }
                boolean z5 = false;
                if (bk5.A01.getInt(8) != 0) {
                    z5 = true;
                }
                bjrArr[i] = new BJR(string, string2, string3, z, z2, z3, null, z4, null, z5, null, bk5.A01.getString(9), bk5.A01.getString(10), bk5.A01.getString(11), bk5.A01.getString(12));
                i++;
            }
            return bjrArr;
        } finally {
            bk5.close();
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x0622  */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x06cd  */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x06d8  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x06dc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D(X.C11380mp r40) {
        /*
            r39 = this;
            r15 = r39
            X.0rk r1 = r15.A01
            java.lang.String r0 = "__database__"
            java.lang.String r1 = X.C28371eh.A00(r1, r0)
            X.0ml r0 = r15.A02
            X.1em r0 = r0.Ajf()
            java.lang.String r0 = r0.A00
            boolean r0 = r0.equals(r1)
            r0 = r0 ^ 1
            r38 = r40
            if (r0 != 0) goto L_0x0020
            r38.BVd()
            return
        L_0x0020:
            java.lang.String r17 = "SchemaMigrator"
            boolean r0 = r15.A03
            if (r0 == 0) goto L_0x004e
            r38.BVc()
        L_0x0029:
            java.util.ArrayList r23 = new java.util.ArrayList
            r23.<init>()
            android.util.SparseArray r14 = new android.util.SparseArray
            r14.<init>()
            X.0rk r0 = r15.A01
            java.util.Set r19 = X.C28371eh.A01(r0)
            java.util.HashSet r16 = new java.util.HashSet
            r1 = 4
            r0 = r16
            r0.<init>(r1)
            X.0rj r0 = r15.A00
            android.database.sqlite.SQLiteDatabase r10 = r0.Ab4()
            r0 = 1806125608(0x6ba74a28, float:4.044816E26)
            X.C007406x.A01(r10, r0)
            goto L_0x0052
        L_0x004e:
            r38.BVb()
            goto L_0x0029
        L_0x0052:
            r0 = 662(0x296, float:9.28E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)     // Catch:{ SQLiteException -> 0x0607 }
            r0 = 587199720(0x22fff4e8, float:6.9377193E-18)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0607 }
            r10.execSQL(r1)     // Catch:{ SQLiteException -> 0x0607 }
            r0 = -238554985(0xfffffffff1c7f097, float:-1.9801079E30)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0607 }
            X.0ml r0 = r15.A02     // Catch:{ SQLiteException -> 0x0607 }
            X.BJj[] r13 = r0.B5G()     // Catch:{ SQLiteException -> 0x0607 }
            int r0 = r13.length     // Catch:{ SQLiteException -> 0x0607 }
            r37 = r0
            r12 = 0
            r18 = 0
        L_0x0073:
            r0 = r37
            if (r12 >= r0) goto L_0x0467
            r11 = r13[r12]     // Catch:{ SQLiteException -> 0x0605 }
            X.0ml r0 = r15.A02     // Catch:{ SQLiteException -> 0x0605 }
            X.BJR[] r22 = r0.Ahc(r12)     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = r11.A01     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r19
            r0.remove(r1)     // Catch:{ SQLiteException -> 0x0605 }
            r21 = 1
            X.0rk r1 = r15.A01     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r0 = r11.A01     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = X.C28371eh.A00(r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
            if (r1 != 0) goto L_0x00c3
            java.lang.String r2 = r11.A01     // Catch:{ SQLiteException -> 0x0605 }
            X.0ml r0 = r15.A02     // Catch:{ SQLiteException -> 0x0605 }
            X.BJH[] r1 = r0.Apr(r12)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r22
            A05(r10, r2, r0, r1)     // Catch:{ SQLiteException -> 0x0605 }
            r1 = 4
            X.BT8 r2 = new X.BT8     // Catch:{ SQLiteException -> 0x0605 }
            r0 = 0
            r2.<init>(r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
        L_0x00a6:
            int r1 = r2.A00     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r21
            if (r1 == r0) goto L_0x00bc
            java.lang.String r1 = r11.A01     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r22
            X.C28371eh.A03(r10, r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r3 = r11.A01     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = r11.A00     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r0 = r11.A00     // Catch:{ SQLiteException -> 0x0605 }
            X.C28371eh.A02(r10, r3, r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
        L_0x00bc:
            int r1 = r2.A00     // Catch:{ SQLiteException -> 0x0605 }
            r0 = 5
            if (r1 != r0) goto L_0x0433
            goto L_0x0447
        L_0x00c3:
            java.lang.String r0 = r11.A00     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = r0.equals(r1)     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 != 0) goto L_0x042a
            X.0rk r0 = r15.A01     // Catch:{ SQLiteException -> 0x0605 }
            r36 = r0
            X.0ml r0 = r15.A02     // Catch:{ SQLiteException -> 0x0605 }
            r35 = r0
            java.lang.String r1 = "migrateTable"
            r0 = -314132230(0xffffffffed46b8fa, float:-3.8438534E27)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
            r9 = r16
            java.lang.String r0 = r11.A01     // Catch:{ all -> 0x045f }
            r1 = r36
            X.BJR[] r1 = A0C(r1, r0)     // Catch:{ all -> 0x045f }
            X.BJI r0 = new X.BJI     // Catch:{ all -> 0x045f }
            r2 = r22
            r0.<init>(r1, r2)     // Catch:{ all -> 0x045f }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            r0.A02 = r1     // Catch:{ all -> 0x045f }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            r0.A00 = r1     // Catch:{ all -> 0x045f }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            r0.A01 = r1     // Catch:{ all -> 0x045f }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            r0.A04 = r1     // Catch:{ all -> 0x045f }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            r0.A05 = r1     // Catch:{ all -> 0x045f }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            r0.A03 = r1     // Catch:{ all -> 0x045f }
            X.BJR[] r6 = r0.A09     // Catch:{ all -> 0x045f }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ all -> 0x045f }
            int r5 = r6.length     // Catch:{ all -> 0x045f }
            r4.<init>(r5)     // Catch:{ all -> 0x045f }
            r3 = 0
        L_0x011f:
            if (r3 >= r5) goto L_0x012b
            r2 = r6[r3]     // Catch:{ all -> 0x045f }
            java.lang.String r1 = r2.A05     // Catch:{ all -> 0x045f }
            r4.put(r1, r2)     // Catch:{ all -> 0x045f }
            int r3 = r3 + 1
            goto L_0x011f
        L_0x012b:
            X.BJR[] r3 = r0.A0A     // Catch:{ all -> 0x045f }
            int r1 = r3.length     // Catch:{ all -> 0x045f }
            r20 = r1
            r5 = 0
        L_0x0131:
            r8 = 1
            r1 = r20
            if (r5 >= r1) goto L_0x018a
            r6 = r3[r5]     // Catch:{ all -> 0x045f }
            java.lang.String r1 = r6.A05     // Catch:{ all -> 0x045f }
            java.lang.Object r2 = r4.remove(r1)     // Catch:{ all -> 0x045f }
            X.BJR r2 = (X.BJR) r2     // Catch:{ all -> 0x045f }
            if (r2 == 0) goto L_0x0180
            boolean r1 = r2.equals(r6)     // Catch:{ all -> 0x045f }
            if (r1 != 0) goto L_0x0187
            boolean r1 = r6.A0C     // Catch:{ all -> 0x045f }
            if (r1 != 0) goto L_0x0175
            boolean r1 = r2.A0C     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x0175
            java.util.ArrayList r1 = r0.A04     // Catch:{ all -> 0x045f }
            r1.add(r2)     // Catch:{ all -> 0x045f }
            java.lang.String r2 = r2.A02     // Catch:{ all -> 0x045f }
            java.lang.Class<com.facebook.crudolib.sqliteproc.annotations.DropAllTablesDataMigrator> r1 = com.facebook.crudolib.sqliteproc.annotations.DropAllTablesDataMigrator.class
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x045f }
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x0166
            r0.A07 = r8     // Catch:{ all -> 0x045f }
            goto L_0x0187
        L_0x0166:
            java.lang.Class<com.facebook.crudolib.sqliteproc.annotations.DropTableDataMigrator> r1 = com.facebook.crudolib.sqliteproc.annotations.DropTableDataMigrator.class
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x045f }
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x0187
            r0.A08 = r8     // Catch:{ all -> 0x045f }
            goto L_0x0187
        L_0x0175:
            java.util.ArrayList r7 = r0.A02     // Catch:{ all -> 0x045f }
            X.44l r1 = new X.44l     // Catch:{ all -> 0x045f }
            r1.<init>(r6, r2)     // Catch:{ all -> 0x045f }
            r7.add(r1)     // Catch:{ all -> 0x045f }
            goto L_0x0187
        L_0x0180:
            java.util.ArrayList r1 = r0.A05     // Catch:{ all -> 0x045f }
            java.lang.String r2 = r6.A05     // Catch:{ all -> 0x045f }
            r1.add(r2)     // Catch:{ all -> 0x045f }
        L_0x0187:
            int r5 = r5 + 1
            goto L_0x0131
        L_0x018a:
            java.util.Collection r1 = r4.values()     // Catch:{ all -> 0x045f }
            java.util.Iterator r6 = r1.iterator()     // Catch:{ all -> 0x045f }
        L_0x0192:
            boolean r1 = r6.hasNext()     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x01f1
            java.lang.Object r5 = r6.next()     // Catch:{ all -> 0x045f }
            X.BJR r5 = (X.BJR) r5     // Catch:{ all -> 0x045f }
            boolean r1 = r5.A0C     // Catch:{ all -> 0x045f }
            if (r1 != 0) goto L_0x0192
            boolean r1 = r5.A0A     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x01e9
            java.util.ArrayList r1 = r0.A00     // Catch:{ all -> 0x045f }
            r1.add(r5)     // Catch:{ all -> 0x045f }
            java.lang.String r1 = r5.A04     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x01c9
            java.lang.String r1 = r5.A03     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x01c9
            boolean r1 = r5.A0D     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x01bb
            java.lang.String r1 = r5.A01     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x01c9
        L_0x01bb:
            java.util.ArrayList r4 = r0.A03     // Catch:{ all -> 0x045f }
            android.util.Pair r3 = new android.util.Pair     // Catch:{ all -> 0x045f }
            java.lang.String r2 = r5.A05     // Catch:{ all -> 0x045f }
            java.lang.String r1 = "foreign_key_violation_added_column"
            r3.<init>(r2, r1)     // Catch:{ all -> 0x045f }
            r4.add(r3)     // Catch:{ all -> 0x045f }
        L_0x01c9:
            java.lang.String r2 = r5.A00     // Catch:{ all -> 0x045f }
            java.lang.Class<com.facebook.crudolib.sqliteproc.annotations.DropAllTablesDataMigrator> r1 = com.facebook.crudolib.sqliteproc.annotations.DropAllTablesDataMigrator.class
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x045f }
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x01da
            r0.A07 = r8     // Catch:{ all -> 0x045f }
            goto L_0x0192
        L_0x01da:
            java.lang.Class<com.facebook.crudolib.sqliteproc.annotations.DropTableDataMigrator> r1 = com.facebook.crudolib.sqliteproc.annotations.DropTableDataMigrator.class
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x045f }
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x045f }
            if (r1 == 0) goto L_0x0192
            r0.A08 = r8     // Catch:{ all -> 0x045f }
            goto L_0x0192
        L_0x01e9:
            java.util.ArrayList r1 = r0.A01     // Catch:{ all -> 0x045f }
            java.lang.String r2 = r5.A05     // Catch:{ all -> 0x045f }
            r1.add(r2)     // Catch:{ all -> 0x045f }
            goto L_0x0192
        L_0x01f1:
            r0.A06 = r8     // Catch:{ all -> 0x045f }
            if (r21 == 0) goto L_0x0457
            X.BT6 r7 = new X.BT6     // Catch:{ all -> 0x045f }
            X.BJR[] r1 = r0.A0A     // Catch:{ all -> 0x045f }
            r25 = r1
            X.BJR[] r1 = r0.A09     // Catch:{ all -> 0x045f }
            r26 = r1
            java.util.ArrayList r1 = r0.A02     // Catch:{ all -> 0x045f }
            r20 = r1
            java.util.ArrayList r6 = r0.A00     // Catch:{ all -> 0x045f }
            java.util.ArrayList r5 = r0.A01     // Catch:{ all -> 0x045f }
            java.util.ArrayList r4 = r0.A04     // Catch:{ all -> 0x045f }
            java.util.ArrayList r3 = r0.A05     // Catch:{ all -> 0x045f }
            java.util.ArrayList r2 = r0.A03     // Catch:{ all -> 0x045f }
            boolean r1 = r0.A07     // Catch:{ all -> 0x045f }
            boolean r0 = r0.A08     // Catch:{ all -> 0x045f }
            r24 = r7
            r27 = r20
            r28 = r6
            r29 = r5
            r30 = r4
            r31 = r3
            r32 = r2
            r33 = r1
            r34 = r0
            r24.<init>(r25, r26, r27, r28, r29, r30, r31, r32, r33, r34)     // Catch:{ all -> 0x045f }
            java.util.List r0 = r7.A02     // Catch:{ all -> 0x045f }
            r25 = r0
            java.util.List r0 = r7.A00     // Catch:{ all -> 0x045f }
            r28 = r0
            java.util.List r0 = r7.A05     // Catch:{ all -> 0x045f }
            r27 = r0
            java.util.List r6 = r7.A06     // Catch:{ all -> 0x045f }
            java.util.List r5 = r7.A01     // Catch:{ all -> 0x045f }
            java.util.List r0 = r7.A04     // Catch:{ all -> 0x045f }
            r24 = r0
            boolean r0 = r7.A08     // Catch:{ all -> 0x045f }
            r20 = r0
            boolean r4 = r7.A07     // Catch:{ all -> 0x045f }
            if (r4 == 0) goto L_0x0247
            java.lang.String r0 = "data_migration"
            r9.add(r0)     // Catch:{ all -> 0x045f }
        L_0x0247:
            r3 = 0
            boolean r0 = r6.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 != 0) goto L_0x026b
            java.lang.String r2 = r11.A01     // Catch:{ all -> 0x045f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "You must use @Deleted to remove columns: "
            r1.append(r0)     // Catch:{ all -> 0x045f }
            r1.append(r6)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x045f }
            A08(r2, r0, r3)     // Catch:{ all -> 0x045f }
            r4 = r4 | r21
            java.lang.String r0 = "removed_column_illegally"
            r9.add(r0)     // Catch:{ all -> 0x045f }
        L_0x026b:
            boolean r0 = r5.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 != 0) goto L_0x028e
            java.lang.String r2 = r11.A01     // Catch:{ all -> 0x045f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "You must use @Added to add columns: "
            r1.append(r0)     // Catch:{ all -> 0x045f }
            r1.append(r5)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x045f }
            A08(r2, r0, r3)     // Catch:{ all -> 0x045f }
            r4 = r4 | r21
            java.lang.String r0 = "added_column_illegally"
            r9.add(r0)     // Catch:{ all -> 0x045f }
        L_0x028e:
            boolean r0 = r25.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 != 0) goto L_0x02b3
            java.lang.String r1 = r11.A01     // Catch:{ all -> 0x045f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x045f }
            r2.<init>()     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "Modification of columns is not permitted, use @Deleted and a new column instead: "
            r2.append(r0)     // Catch:{ all -> 0x045f }
            r0 = r25
            r2.append(r0)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x045f }
            A08(r1, r0, r3)     // Catch:{ all -> 0x045f }
            r4 = r4 | r21
            java.lang.String r0 = "modified_column"
            r9.add(r0)     // Catch:{ all -> 0x045f }
        L_0x02b3:
            r0 = r24
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 != 0) goto L_0x02eb
            java.lang.String r1 = r11.A01     // Catch:{ all -> 0x045f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x045f }
            r2.<init>()     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "Detected other columns introducing illegal changes: "
            r2.append(r0)     // Catch:{ all -> 0x045f }
            r6 = r24
            r2.append(r6)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x045f }
            A08(r1, r0, r3)     // Catch:{ all -> 0x045f }
            r4 = r4 | r21
            java.util.Iterator r1 = r6.iterator()     // Catch:{ all -> 0x045f }
        L_0x02d9:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x045f }
            if (r0 == 0) goto L_0x02eb
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x045f }
            android.util.Pair r0 = (android.util.Pair) r0     // Catch:{ all -> 0x045f }
            java.lang.Object r0 = r0.second     // Catch:{ all -> 0x045f }
            r9.add(r0)     // Catch:{ all -> 0x045f }
            goto L_0x02d9
        L_0x02eb:
            if (r20 != 0) goto L_0x0406
            if (r4 != 0) goto L_0x0406
            r0 = r28
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 != 0) goto L_0x033a
            r5 = 0
            r0 = r28
            int r3 = r0.size()     // Catch:{ all -> 0x045f }
        L_0x02fe:
            if (r5 >= r3) goto L_0x033a
            r0 = r28
            java.lang.Object r2 = r0.get(r5)     // Catch:{ all -> 0x045f }
            X.BJR r2 = (X.BJR) r2     // Catch:{ all -> 0x045f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x045f }
            r1.<init>()     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "ALTER TABLE "
            r1.append(r0)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = r11.A01     // Catch:{ all -> 0x045f }
            r1.append(r0)     // Catch:{ all -> 0x045f }
            r0 = 32
            r1.append(r0)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "ADD COLUMN "
            r1.append(r0)     // Catch:{ all -> 0x045f }
            A0A(r1, r2)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x045f }
            r1 = -309648884(0xffffffffed8b220c, float:-5.382447E27)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x045f }
            r10.execSQL(r0)     // Catch:{ all -> 0x045f }
            r0 = -386407652(0xffffffffe8f7e31c, float:-9.3649116E24)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x045f }
            int r5 = r5 + 1
            goto L_0x02fe
        L_0x033a:
            r0 = r27
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 == 0) goto L_0x03e9
            java.lang.String r3 = r11.A00     // Catch:{ all -> 0x045f }
            java.lang.String r1 = r11.A01     // Catch:{ all -> 0x045f }
            X.BK7 r0 = new X.BK7     // Catch:{ all -> 0x045f }
            r0.<init>(r1)     // Catch:{ all -> 0x045f }
            r1 = r36
            android.database.Cursor r0 = r1.A00(r0)     // Catch:{ all -> 0x045f }
            X.BK6 r2 = new X.BK6     // Catch:{ all -> 0x045f }
            r2.<init>(r0)     // Catch:{ all -> 0x045f }
            boolean r0 = r2.moveToFirst()     // Catch:{ all -> 0x044d }
            if (r0 == 0) goto L_0x0366
            android.database.Cursor r1 = r2.A01     // Catch:{ all -> 0x044d }
            java.lang.String r0 = r1.getString(r8)     // Catch:{ all -> 0x044d }
            r2.close()     // Catch:{ all -> 0x045f }
            goto L_0x036a
        L_0x0366:
            r0 = 0
            r2.close()     // Catch:{ all -> 0x045f }
        L_0x036a:
            boolean r0 = r3.equals(r0)     // Catch:{ all -> 0x045f }
            if (r0 != 0) goto L_0x03e9
            java.lang.String r5 = "recreate_indices_savepoint"
            X.AnonymousClass7W3.A00(r10, r5)     // Catch:{ all -> 0x045f }
            r3 = 0
            X.0ml r0 = r15.A02     // Catch:{ SQLException -> 0x03d3 }
            X.BJH[] r20 = r0.Apr(r12)     // Catch:{ SQLException -> 0x03d3 }
            r24 = r10
            java.lang.String r1 = "recreateIndices"
            r0 = -1205995402(0xffffffffb81df876, float:-3.7663114E-5)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ SQLException -> 0x03d3 }
            java.lang.String r0 = r11.A01     // Catch:{ all -> 0x03cb }
            r6 = 0
            java.lang.String[] r1 = new java.lang.String[]{r0}     // Catch:{ all -> 0x03cb }
            java.lang.String r0 = "SELECT name FROM sqlite_master WHERE type == 'index' AND tbl_name == ?"
            android.database.Cursor r2 = r10.rawQuery(r0, r1)     // Catch:{ all -> 0x03cb }
        L_0x0393:
            boolean r0 = r2.moveToNext()     // Catch:{ all -> 0x03c6 }
            if (r0 == 0) goto L_0x03b3
            java.lang.String r1 = r2.getString(r6)     // Catch:{ all -> 0x03c6 }
            java.lang.String r0 = "DROP INDEX "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ all -> 0x03c6 }
            r1 = 392715864(0x17685e58, float:7.508232E-25)
            X.C007406x.A00(r1)     // Catch:{ all -> 0x03c6 }
            r10.execSQL(r0)     // Catch:{ all -> 0x03c6 }
            r0 = -94312165(0xfffffffffa60e91b, float:-2.919506E35)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x03c6 }
            goto L_0x0393
        L_0x03b3:
            r2.close()     // Catch:{ all -> 0x03cb }
            java.lang.String r0 = r11.A01     // Catch:{ all -> 0x03cb }
            r25 = r0
            r26 = r20
            A06(r24, r25, r26)     // Catch:{ all -> 0x03cb }
            r0 = 259119985(0xf71db71, float:1.192448E-29)
            X.AnonymousClass06K.A00(r0)     // Catch:{ SQLException -> 0x03d3 }
            goto L_0x03d7
        L_0x03c6:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x03cb }
            throw r0     // Catch:{ all -> 0x03cb }
        L_0x03cb:
            r1 = move-exception
            r0 = -1098610923(0xffffffffbe848715, float:-0.2588431)
            X.AnonymousClass06K.A00(r0)     // Catch:{ SQLException -> 0x03d3 }
            throw r1     // Catch:{ SQLException -> 0x03d3 }
        L_0x03d3:
            r3 = move-exception
            X.AnonymousClass7W3.A02(r10, r5)     // Catch:{ all -> 0x0452 }
        L_0x03d7:
            X.AnonymousClass7W3.A01(r10, r5)     // Catch:{ all -> 0x045f }
            if (r3 == 0) goto L_0x03e9
            java.lang.String r1 = "Error recreating indices, so dropping database"
            r0 = r38
            r0.BVe(r1, r3)     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "unique_constraint_failed_recreate_indices"
            r9.add(r0)     // Catch:{ all -> 0x045f }
            r4 = 1
        L_0x03e9:
            if (r4 != 0) goto L_0x0406
            boolean r0 = r27.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 == 0) goto L_0x03ff
            boolean r0 = r28.isEmpty()     // Catch:{ all -> 0x045f }
            if (r0 == 0) goto L_0x03ff
            r1 = 2
            X.BT8 r2 = new X.BT8     // Catch:{ all -> 0x045f }
            r0 = 0
            r2.<init>(r1, r0)     // Catch:{ all -> 0x045f }
            goto L_0x0422
        L_0x03ff:
            X.BT8 r2 = new X.BT8     // Catch:{ all -> 0x045f }
            r0 = 6
            r2.<init>(r0, r7)     // Catch:{ all -> 0x045f }
            goto L_0x0422
        L_0x0406:
            r1 = r35
            X.BJH[] r1 = r1.Apr(r12)     // Catch:{ all -> 0x045f }
            r0 = r22
            A00(r10, r11, r0, r1)     // Catch:{ all -> 0x045f }
            if (r4 == 0) goto L_0x041b
            r1 = 5
            X.BT8 r2 = new X.BT8     // Catch:{ all -> 0x045f }
            r0 = 0
            r2.<init>(r1, r0)     // Catch:{ all -> 0x045f }
            goto L_0x0422
        L_0x041b:
            r1 = 3
            X.BT8 r2 = new X.BT8     // Catch:{ all -> 0x045f }
            r0 = 0
            r2.<init>(r1, r0)     // Catch:{ all -> 0x045f }
        L_0x0422:
            r0 = 1158937850(0x4513fcfa, float:2367.811)
            X.AnonymousClass06K.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x00a6
        L_0x042a:
            r1 = 1
            X.BT8 r2 = new X.BT8     // Catch:{ SQLiteException -> 0x0605 }
            r0 = 0
            r2.<init>(r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x00a6
        L_0x0433:
            r0 = 3
            if (r1 != r0) goto L_0x043e
            java.lang.String r1 = r11.A01     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r23
            r0.add(r1)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x0449
        L_0x043e:
            r0 = 6
            if (r1 != r0) goto L_0x0449
            X.BT6 r0 = r2.A01     // Catch:{ SQLiteException -> 0x0605 }
            r14.put(r12, r0)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x0449
        L_0x0447:
            r18 = 1
        L_0x0449:
            int r12 = r12 + 1
            goto L_0x0073
        L_0x044d:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x045f }
            goto L_0x0456
        L_0x0452:
            r0 = move-exception
            X.AnonymousClass7W3.A01(r10, r5)     // Catch:{ all -> 0x045f }
        L_0x0456:
            throw r0     // Catch:{ all -> 0x045f }
        L_0x0457:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x045f }
            java.lang.String r0 = "Must call diff() before accessing results with getDiff()"
            r1.<init>(r0)     // Catch:{ all -> 0x045f }
            throw r1     // Catch:{ all -> 0x045f }
        L_0x045f:
            r1 = move-exception
            r0 = -1248369900(0xffffffffb5976314, float:-1.1279212E-6)
            X.AnonymousClass06K.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            throw r1     // Catch:{ SQLiteException -> 0x0605 }
        L_0x0467:
            if (r18 != 0) goto L_0x0577
            X.0ml r6 = r15.A02     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = r23.isEmpty()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 != 0) goto L_0x04e0
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ SQLiteException -> 0x0605 }
            r5.<init>()     // Catch:{ SQLiteException -> 0x0605 }
            X.BJj[] r4 = r6.B5G()     // Catch:{ SQLiteException -> 0x0605 }
            int r3 = r4.length     // Catch:{ SQLiteException -> 0x0605 }
            r2 = 0
        L_0x047c:
            if (r2 >= r3) goto L_0x048c
            r0 = r4[r2]     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = r0.A01     // Catch:{ SQLiteException -> 0x0605 }
            X.BJR[] r0 = r6.Ahc(r2)     // Catch:{ SQLiteException -> 0x0605 }
            A09(r1, r0, r5)     // Catch:{ SQLiteException -> 0x0605 }
            int r2 = r2 + 1
            goto L_0x047c
        L_0x048c:
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r23
            r4.<init>(r0)     // Catch:{ SQLiteException -> 0x0605 }
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x0605 }
            r6.<init>(r0)     // Catch:{ SQLiteException -> 0x0605 }
        L_0x0498:
            boolean r0 = r6.isEmpty()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 != 0) goto L_0x04e0
            r0 = 0
            java.lang.Object r1 = r6.remove(r0)     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = r5.containsKey(r1)     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x0498
            java.lang.Object r0 = r5.get(r1)     // Catch:{ SQLiteException -> 0x0605 }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ SQLiteException -> 0x0605 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ SQLiteException -> 0x0605 }
        L_0x04b5:
            boolean r0 = r7.hasNext()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x0498
            java.lang.Object r3 = r7.next()     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = "DELETE FROM "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r3)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = -1292425346(0xffffffffb2f7277e, float:-2.8772543E-8)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            r10.execSQL(r1)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = -337255030(0xffffffffebe5e58a, float:-5.5585596E26)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = r4.add(r3)     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x04b5
            r6.add(r3)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x04b5
        L_0x04e0:
            r9 = r38
            int r8 = r14.size()     // Catch:{ SQLiteException -> 0x0605 }
            r7 = 0
        L_0x04e7:
            if (r7 >= r8) goto L_0x054c
            int r6 = r14.keyAt(r7)     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.Object r5 = r14.get(r6)     // Catch:{ SQLiteException -> 0x0605 }
            X.BT6 r5 = (X.BT6) r5     // Catch:{ SQLiteException -> 0x0605 }
            java.util.List r0 = r5.A05     // Catch:{ SQLiteException -> 0x0605 }
            java.util.Iterator r12 = r0.iterator()     // Catch:{ SQLiteException -> 0x0605 }
        L_0x04f9:
            boolean r0 = r12.hasNext()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x051e
            java.lang.Object r11 = r12.next()     // Catch:{ SQLiteException -> 0x0605 }
            X.BJR r11 = (X.BJR) r11     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r4 = r11.A02     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = A0B(r4)     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x04f9
            X.BT7 r3 = new X.BT7     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r13[r6]     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r2 = r0.A01     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = r11.A05     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r0 = r11.A08     // Catch:{ SQLiteException -> 0x0605 }
            r3.<init>(r5, r2, r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
            A03(r10, r4, r3, r9)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x04f9
        L_0x051e:
            java.util.List r0 = r5.A00     // Catch:{ SQLiteException -> 0x0605 }
            java.util.Iterator r12 = r0.iterator()     // Catch:{ SQLiteException -> 0x0605 }
        L_0x0524:
            boolean r0 = r12.hasNext()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x0549
            java.lang.Object r11 = r12.next()     // Catch:{ SQLiteException -> 0x0605 }
            X.BJR r11 = (X.BJR) r11     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r4 = r11.A00     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = A0B(r4)     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x0524
            X.BT7 r3 = new X.BT7     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r13[r6]     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r2 = r0.A01     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = r11.A05     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r0 = r11.A08     // Catch:{ SQLiteException -> 0x0605 }
            r3.<init>(r5, r2, r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
            A03(r10, r4, r3, r9)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x0524
        L_0x0549:
            int r7 = r7 + 1
            goto L_0x04e7
        L_0x054c:
            int r5 = r14.size()     // Catch:{ SQLiteException -> 0x0605 }
            r4 = 0
        L_0x0551:
            if (r4 >= r5) goto L_0x05c4
            int r1 = r14.keyAt(r4)     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.Object r0 = r14.get(r1)     // Catch:{ SQLiteException -> 0x0605 }
            X.BT6 r0 = (X.BT6) r0     // Catch:{ SQLiteException -> 0x0605 }
            java.util.List r0 = r0.A05     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = r0.isEmpty()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 != 0) goto L_0x0574
            r3 = r13[r1]     // Catch:{ SQLiteException -> 0x0605 }
            X.0ml r0 = r15.A02     // Catch:{ SQLiteException -> 0x0605 }
            X.BJR[] r2 = r0.Ahc(r1)     // Catch:{ SQLiteException -> 0x0605 }
            X.BJH[] r1 = r0.Apr(r1)     // Catch:{ SQLiteException -> 0x0605 }
            A01(r10, r3, r2, r1, r9)     // Catch:{ SQLiteException -> 0x0605 }
        L_0x0574:
            int r4 = r4 + 1
            goto L_0x0551
        L_0x0577:
            int r5 = r14.size()     // Catch:{ SQLiteException -> 0x0605 }
            r4 = 0
        L_0x057c:
            if (r4 >= r5) goto L_0x05a4
            int r1 = r14.keyAt(r4)     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.Object r0 = r14.get(r1)     // Catch:{ SQLiteException -> 0x0605 }
            X.BT6 r0 = (X.BT6) r0     // Catch:{ SQLiteException -> 0x0605 }
            java.util.List r0 = r0.A05     // Catch:{ SQLiteException -> 0x0605 }
            boolean r0 = r0.isEmpty()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 != 0) goto L_0x05a1
            r3 = r13[r1]     // Catch:{ SQLiteException -> 0x0605 }
            X.0ml r0 = r15.A02     // Catch:{ SQLiteException -> 0x0605 }
            X.BJR[] r2 = r0.Ahc(r1)     // Catch:{ SQLiteException -> 0x0605 }
            X.BJH[] r1 = r0.Apr(r1)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = r38
            A01(r10, r3, r2, r1, r0)     // Catch:{ SQLiteException -> 0x0605 }
        L_0x05a1:
            int r4 = r4 + 1
            goto L_0x057c
        L_0x05a4:
            int r3 = r13.length     // Catch:{ SQLiteException -> 0x0605 }
            r2 = 0
        L_0x05a6:
            if (r2 >= r3) goto L_0x05c4
            r0 = r13[r2]     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = r0.A01     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r0 = "DELETE FROM "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = -1292425346(0xffffffffb2f7277e, float:-2.8772543E-8)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            r10.execSQL(r1)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = -337255030(0xffffffffebe5e58a, float:-5.5585596E26)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            int r2 = r2 + 1
            goto L_0x05a6
        L_0x05c4:
            X.0rj r0 = r15.A00     // Catch:{ SQLiteException -> 0x0605 }
            android.database.sqlite.SQLiteDatabase r3 = r0.Ab4()     // Catch:{ SQLiteException -> 0x0605 }
            X.0ml r0 = r15.A02     // Catch:{ SQLiteException -> 0x0605 }
            X.1em r0 = r0.Ajf()     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r2 = r0.A00     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r1 = "__database__"
            r0 = 0
            X.C28371eh.A02(r3, r1, r2, r0)     // Catch:{ SQLiteException -> 0x0605 }
            java.util.Iterator r3 = r19.iterator()     // Catch:{ SQLiteException -> 0x0605 }
        L_0x05dc:
            boolean r0 = r3.hasNext()     // Catch:{ SQLiteException -> 0x0605 }
            if (r0 == 0) goto L_0x0601
            java.lang.Object r2 = r3.next()     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ SQLiteException -> 0x0605 }
            java.lang.String r0 = "DROP TABLE IF EXISTS "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r2)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = -1831239079(0xffffffff92d98259, float:-1.37267585E-27)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            r10.execSQL(r1)     // Catch:{ SQLiteException -> 0x0605 }
            r0 = 377374831(0x167e486f, float:2.0540813E-25)
            X.C007406x.A00(r0)     // Catch:{ SQLiteException -> 0x0605 }
            A02(r10, r2)     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x05dc
        L_0x0601:
            r10.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x0605 }
            goto L_0x0619
        L_0x0605:
            r2 = move-exception
            goto L_0x060a
        L_0x0607:
            r2 = move-exception
            r18 = 0
        L_0x060a:
            java.lang.String r1 = "Error migrating database"
            r0 = r38
            r0.BVe(r1, r2)     // Catch:{ all -> 0x06e0 }
            r0 = 581579722(0x22aa33ca, float:4.6133426E-18)
            X.C007406x.A02(r10, r0)
            r0 = 1
            goto L_0x0620
        L_0x0619:
            r0 = 1497886630(0x5947efa6, float:3.51731354E15)
            X.C007406x.A02(r10, r0)
            r0 = 0
        L_0x0620:
            if (r0 == 0) goto L_0x06cb
            java.lang.String r1 = "Failed to migrate database, so using fallback that safely drops/recreates all tables."
            r0 = r17
            X.C010708t.A0K(r0, r1)
            X.0rj r9 = r15.A00
            X.0rk r5 = r15.A01
            X.0ml r8 = r15.A02
            android.database.sqlite.SQLiteDatabase r4 = r9.Ab4()
            r0 = -652041695(0xffffffffd922a221, float:-2.86107555E15)
            X.C007406x.A01(r4, r0)
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ all -> 0x06bb }
            r6.<init>()     // Catch:{ all -> 0x06bb }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ all -> 0x06bb }
            r3.<init>()     // Catch:{ all -> 0x06bb }
            java.util.Set r0 = X.C28371eh.A01(r5)     // Catch:{ all -> 0x06bb }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x06bb }
        L_0x064b:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x06bb }
            if (r0 == 0) goto L_0x065f
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x06bb }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x06bb }
            X.BJR[] r0 = A0C(r5, r1)     // Catch:{ all -> 0x06bb }
            A09(r1, r0, r3)     // Catch:{ all -> 0x06bb }
            goto L_0x064b
        L_0x065f:
            java.util.Set r0 = X.C28371eh.A01(r5)     // Catch:{ all -> 0x06bb }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x06bb }
        L_0x0667:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x06bb }
            if (r0 == 0) goto L_0x0677
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x06bb }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x06bb }
            A07(r9, r0, r6, r3)     // Catch:{ all -> 0x06bb }
            goto L_0x0667
        L_0x0677:
            X.BJj[] r7 = r8.B5G()     // Catch:{ all -> 0x06bb }
            r6 = 0
        L_0x067c:
            int r0 = r7.length     // Catch:{ all -> 0x06bb }
            if (r6 >= r0) goto L_0x06a7
            r5 = r7[r6]     // Catch:{ all -> 0x06bb }
            java.lang.String r3 = r5.A01     // Catch:{ all -> 0x06bb }
            X.BJH[] r2 = r8.Apr(r6)     // Catch:{ all -> 0x06bb }
            X.BJR[] r1 = r8.Ahc(r6)     // Catch:{ all -> 0x06bb }
            android.database.sqlite.SQLiteDatabase r0 = r9.Ab4()     // Catch:{ all -> 0x06bb }
            A05(r0, r3, r1, r2)     // Catch:{ all -> 0x06bb }
            android.database.sqlite.SQLiteDatabase r0 = r9.Ab4()     // Catch:{ all -> 0x06bb }
            X.C28371eh.A03(r0, r3, r1)     // Catch:{ all -> 0x06bb }
            android.database.sqlite.SQLiteDatabase r2 = r9.Ab4()     // Catch:{ all -> 0x06bb }
            java.lang.String r1 = r5.A00     // Catch:{ all -> 0x06bb }
            java.lang.String r0 = r5.A00     // Catch:{ all -> 0x06bb }
            X.C28371eh.A02(r2, r3, r1, r0)     // Catch:{ all -> 0x06bb }
            int r6 = r6 + 1
            goto L_0x067c
        L_0x06a7:
            android.database.sqlite.SQLiteDatabase r3 = r9.Ab4()     // Catch:{ all -> 0x06bb }
            X.1em r0 = r8.Ajf()     // Catch:{ all -> 0x06bb }
            java.lang.String r2 = r0.A00     // Catch:{ all -> 0x06bb }
            java.lang.String r1 = "__database__"
            r0 = 0
            X.C28371eh.A02(r3, r1, r2, r0)     // Catch:{ all -> 0x06bb }
            r4.setTransactionSuccessful()     // Catch:{ all -> 0x06bb }
            goto L_0x06c3
        L_0x06bb:
            r1 = move-exception
            r0 = 2063064401(0x7af7dd51, float:6.4349308E35)
            X.C007406x.A02(r4, r0)
            throw r1
        L_0x06c3:
            r0 = -1254626903(0xffffffffb537e9a9, float:-6.851283E-7)
            X.C007406x.A02(r4, r0)
            r18 = 1
        L_0x06cb:
            if (r18 == 0) goto L_0x06d4
            r1 = r16
            r0 = r38
            r0.BNi(r1)
        L_0x06d4:
            boolean r0 = r15.A03
            if (r0 == 0) goto L_0x06dc
            r38.BNj()
            return
        L_0x06dc:
            r38.BNk()
            return
        L_0x06e0:
            r1 = move-exception
            r0 = 800240147(0x2fb2b213, float:3.250454E-10)
            X.C007406x.A02(r10, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0r8.A0D(X.0mp):void");
    }

    public AnonymousClass0r8(C13610rj r2, C11340ml r3, boolean z) {
        this.A00 = r2;
        this.A02 = r3;
        this.A01 = new C13620rk(r2);
        this.A03 = z;
    }

    public static void A02(SQLiteDatabase sQLiteDatabase, String str) {
        sQLiteDatabase.delete("sqliteproc_schema", "table_name = ?", new String[]{str});
        sQLiteDatabase.delete("sqliteproc_metadata", "table_name = ?", new String[]{str});
    }

    private static void A04(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3) {
        String format = String.format("INSERT OR IGNORE INTO %s (%s) SELECT %s FROM %s", str2, str3, str3, str);
        C007406x.A00(787066303);
        sQLiteDatabase.execSQL(format);
        C007406x.A00(777775661);
    }

    private static void A07(C13610rj r3, String str, Set set, Map map) {
        if (!set.contains(str)) {
            if (map.containsKey(str)) {
                for (String A07 : (Set) map.get(str)) {
                    A07(r3, A07, set, map);
                }
            }
            SQLiteDatabase Ab4 = r3.Ab4();
            String A0J = AnonymousClass08S.A0J("DROP TABLE IF EXISTS ", str);
            C007406x.A00(1523472672);
            Ab4.execSQL(A0J);
            C007406x.A00(-2022094031);
            A02(r3.Ab4(), str);
            set.add(str);
        }
    }

    private static boolean A0B(String str) {
        if (TextUtils.isEmpty(str) || DefaultDataMigrator.class.getName().equals(str) || DropTableDataMigrator.class.getName().equals(str) || DropAllTablesDataMigrator.class.getName().equals(str)) {
            return false;
        }
        return true;
    }
}
