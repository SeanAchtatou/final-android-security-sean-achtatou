package X;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

/* renamed from: X.0vb  reason: invalid class name and case insensitive filesystem */
public final class C15630vb {
    public float A00 = 1.0f;
    public float A01 = 0.0f;
    public float A02;
    public float A03 = 0.0f;
    public float A04 = 0.0f;
    public float A05;
    public float A06;
    public float A07;
    public float A08 = 5.0f;
    public int A09 = 255;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public Path A0E;
    public boolean A0F;
    public int[] A0G;
    public final Paint A0H = new Paint();
    public final Paint A0I = new Paint();
    public final Paint A0J = new Paint();
    public final RectF A0K = new RectF();

    public C15630vb() {
        this.A0J.setStrokeCap(Paint.Cap.SQUARE);
        this.A0J.setAntiAlias(true);
        this.A0J.setStyle(Paint.Style.STROKE);
        this.A0H.setStyle(Paint.Style.FILL);
        this.A0H.setAntiAlias(true);
        this.A0I.setColor(0);
    }
}
