package X;

import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.notify.DirectMessageStorySeenNotification;
import com.facebook.messaging.notify.EventReminderNotification;
import com.facebook.messaging.notify.FailedToSendMessageNotification;
import com.facebook.messaging.notify.FriendInstallNotification;
import com.facebook.messaging.notify.JoinRequestNotification;
import com.facebook.messaging.notify.LoggedOutMessageNotification;
import com.facebook.messaging.notify.MessageReactionNotification;
import com.facebook.messaging.notify.MessageRequestNotification;
import com.facebook.messaging.notify.MessengerLivingRoomCreateNotification;
import com.facebook.messaging.notify.MessengerRoomInviteReminderNotification;
import com.facebook.messaging.notify.MissedCallNotification;
import com.facebook.messaging.notify.MontageMessageNotification;
import com.facebook.messaging.notify.MultipleAccountsNewMessagesNotification;
import com.facebook.messaging.notify.PageMessageNotification;
import com.facebook.messaging.notify.PaymentNotification;
import com.facebook.messaging.notify.SimpleMessageNotification;
import com.facebook.messaging.notify.StaleNotification;
import com.facebook.messaging.notify.UriNotification;
import com.facebook.messaging.notify.VideoChatLinkJoinAttemptNotification;
import com.facebook.messaging.notify.type.NewMessageNotification;
import java.util.ArrayList;

/* renamed from: X.16u  reason: invalid class name and case insensitive filesystem */
public interface C191016u {
    void ASY(String str);

    void ASm();

    void ASn(String str);

    void ASr(ArrayList arrayList);

    void ASx(ThreadKey threadKey, String str);

    void BM4(DirectMessageStorySeenNotification directMessageStorySeenNotification);

    void BM8(EventReminderNotification eventReminderNotification);

    void BM9(FailedToSendMessageNotification failedToSendMessageNotification);

    void BMA();

    void BMB(MontageMessageNotification montageMessageNotification);

    void BMC(FolderCounts folderCounts);

    void BME(SimpleMessageNotification simpleMessageNotification);

    void BMI(JoinRequestNotification joinRequestNotification);

    void BMK(LoggedOutMessageNotification loggedOutMessageNotification);

    void BML(MessageReactionNotification messageReactionNotification);

    void BMM(MessageRequestNotification messageRequestNotification);

    void BMN(MessengerLivingRoomCreateNotification messengerLivingRoomCreateNotification);

    void BMO(MessengerRoomInviteReminderNotification messengerRoomInviteReminderNotification);

    void BMP(StaleNotification staleNotification);

    void BMQ(UriNotification uriNotification);

    void BMR(MissedCallNotification missedCallNotification);

    void BMS(MontageMessageNotification montageMessageNotification);

    void BMT(MontageMessageNotification montageMessageNotification);

    void BMU(MontageMessageNotification montageMessageNotification);

    void BMV(MontageMessageNotification montageMessageNotification);

    void BMW(MultipleAccountsNewMessagesNotification multipleAccountsNewMessagesNotification);

    void BMX(FriendInstallNotification friendInstallNotification);

    void BMY(NewMessageNotification newMessageNotification);

    void BMb(PageMessageNotification pageMessageNotification);

    void BMc(PaymentNotification paymentNotification);

    void BMd(SimpleMessageNotification simpleMessageNotification);

    void BMg(VideoChatLinkJoinAttemptNotification videoChatLinkJoinAttemptNotification);
}
