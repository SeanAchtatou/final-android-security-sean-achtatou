package X;

import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0cC  reason: invalid class name and case insensitive filesystem */
public final class C06860cC extends AnonymousClass0Wl {
    private static volatile C06860cC A01;
    public final AnonymousClass0US A00;

    public String getSimpleName() {
        return "INeedInitForEventBusRegister";
    }

    public static final C06860cC A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (C06860cC.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        A01 = new C06860cC(AnonymousClass0UQ.A00(AnonymousClass1Y3.ARB, r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C06860cC(AnonymousClass0US r1) {
        this.A00 = r1;
    }

    /* JADX INFO: finally extract failed */
    public void init() {
        int A03 = C000700l.A03(86964101);
        C005505z.A03("INeedInitForEventBusRegister-RegisterEventBusSubscribers", -1268039904);
        try {
            C005505z.A03("INeedInitForEventBusRegister-RegisterEventBusSubscribers#construct", 449869580);
            Set<C1296465t> set = (Set) this.A00.get();
            C005505z.A00(1264703301);
            for (C1296465t A02 : set) {
                C06900cG r0 = null;
                r0.A02(A02);
            }
            C005505z.A00(1406397149);
            C000700l.A09(1215049678, A03);
        } catch (Throwable th) {
            C005505z.A00(2032162314);
            throw th;
        }
    }
}
