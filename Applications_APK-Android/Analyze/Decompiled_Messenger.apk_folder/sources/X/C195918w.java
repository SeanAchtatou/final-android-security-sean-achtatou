package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.18w  reason: invalid class name and case insensitive filesystem */
public final class C195918w extends AnonymousClass1A9 {
    private boolean A00 = true;
    public final /* synthetic */ ThreadListFragment A01;

    public C195918w(ThreadListFragment threadListFragment) {
        this.A01 = threadListFragment;
    }

    public void A07(RecyclerView recyclerView, int i) {
        if (this.A01.A0o.A00.Aem(2306128843582216079L)) {
            boolean z = false;
            if (i == 0) {
                z = true;
            }
            if (this.A00 != z) {
                this.A00 = z;
                if (z) {
                    AnonymousClass16N r2 = this.A01.A11;
                    int i2 = r2.A00 - 1;
                    r2.A00 = i2;
                    if (i2 < 0) {
                        C010708t.A06(AnonymousClass16N.A09, "[TL] Suspend count < 0");
                    }
                    AnonymousClass16N.A01(r2);
                } else {
                    this.A01.A11.A00++;
                }
            }
        }
        if (i == 0) {
            ((C08790fx) AnonymousClass1XX.A02(17, AnonymousClass1Y3.AsC, this.A01.A0O)).A08(5505081);
        } else if (i == 1) {
            C08790fx.A04((C08790fx) AnonymousClass1XX.A02(17, AnonymousClass1Y3.AsC, this.A01.A0O), 5505081);
        }
    }

    public void A08(RecyclerView recyclerView, int i, int i2) {
        C35201qq r0 = this.A01.A1A;
        if (r0 != null) {
            r0.A00.A00();
        }
    }
}
