package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: X.0HS  reason: invalid class name */
public final class AnonymousClass0HS {
    private ImmutableList A00;
    private Object A01;

    public synchronized ImmutableList A00() {
        if (this.A00 == null) {
            Object obj = this.A01;
            if (obj == null) {
                this.A00 = RegularImmutableList.A02;
            } else if (obj instanceof ArrayList) {
                this.A00 = ImmutableList.copyOf((Collection) ((ArrayList) obj));
            } else {
                this.A00 = ImmutableList.of(obj);
            }
        }
        return this.A00;
    }

    public synchronized void A01() {
        Object obj = this.A01;
        if (obj instanceof ArrayList) {
            ArrayList arrayList = (ArrayList) obj;
            if (!arrayList.isEmpty()) {
                arrayList.clear();
            }
        } else if (obj != null) {
            this.A01 = null;
        }
        this.A00 = null;
    }

    public synchronized void A02(Object obj) {
        if (obj != null) {
            if (!(obj instanceof ArrayList)) {
                Object obj2 = this.A01;
                if (obj2 == null) {
                    this.A01 = obj;
                } else if (obj2 instanceof ArrayList) {
                    ArrayList arrayList = (ArrayList) obj2;
                    if (!arrayList.contains(obj)) {
                        arrayList.add(obj);
                    }
                } else if (obj2 != obj) {
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(obj2);
                    arrayList2.add(obj);
                    this.A01 = arrayList2;
                }
                this.A00 = null;
            }
        }
        throw new IllegalArgumentException();
    }

    public synchronized void A03(Object obj) {
        ArrayList arrayList;
        int indexOf;
        Object obj2 = this.A01;
        if (obj2 != null) {
            if (obj2 == obj) {
                this.A01 = null;
            } else if ((obj2 instanceof ArrayList) && (indexOf = (arrayList = (ArrayList) obj2).indexOf(obj)) != -1) {
                arrayList.remove(indexOf);
            }
            this.A00 = null;
        }
    }
}
