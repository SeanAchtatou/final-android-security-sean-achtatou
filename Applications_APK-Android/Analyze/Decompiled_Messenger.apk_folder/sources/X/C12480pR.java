package X;

import com.facebook.content.ContentModule;
import com.facebook.content.SecureContextHelper;
import java.util.Collections;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0pR  reason: invalid class name and case insensitive filesystem */
public final class C12480pR {
    private static volatile C12480pR A09;
    public final C28131eJ A00;
    public final C28131eJ A01;
    public final C28131eJ A02;
    public final C28131eJ A03;
    public final C28131eJ A04;
    public final C28131eJ A05;
    public final C28131eJ A06;
    public final C28131eJ A07;
    public final C28131eJ A08;

    public static final C12480pR A01(AnonymousClass1XY r6) {
        if (A09 == null) {
            synchronized (C12480pR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A09 = new C12480pR(AnonymousClass0WT.A00(applicationInjector), C04920Ww.A00(applicationInjector), ContentModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    private C12480pR(C25051Yd r7, AnonymousClass09P r8, SecureContextHelper secureContextHelper) {
        C008807t A002 = C008807t.A00(r7.At0(566587790919380L));
        AnonymousClass21O r4 = new AnonymousClass21O(r8);
        this.A03 = new AnonymousClass15w(new C14070sZ(A002, r4), Collections.emptyList());
        new C28151eL();
        this.A07 = new AnonymousClass15w(new C13350rI(A002, r4, false), Collections.emptyList());
        this.A02 = new AnonymousClass15w(new C15890w9(A002, r4, new C28151eL(), false), Collections.emptyList());
        this.A01 = new AnonymousClass15w(new C28861fU(A002, r4, new C28151eL()), Collections.emptyList());
        new C28881fW(A002, r4, new C28151eL());
        this.A04 = secureContextHelper.AZR();
        this.A05 = secureContextHelper.BLv();
        this.A06 = secureContextHelper.BLw();
        this.A08 = new AnonymousClass15w(new C28881fW(C008807t.A00(r7.At0(566587790984917L)), r4, new C28151eL()), Collections.emptyList());
        this.A00 = new AnonymousClass15w(new C14130sf(C008807t.A00(r7.At0(566587791050454L)), new C52322it(r8)), Collections.emptyList());
    }

    public static final C12480pR A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
