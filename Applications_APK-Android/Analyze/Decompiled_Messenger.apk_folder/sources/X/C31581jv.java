package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1jv  reason: invalid class name and case insensitive filesystem */
public final class C31581jv extends AnonymousClass0UV {
    private static volatile AnonymousClass1WE A00;

    public static final AnonymousClass1WE A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass1WE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass1WE();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
