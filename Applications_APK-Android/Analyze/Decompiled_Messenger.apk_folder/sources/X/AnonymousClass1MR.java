package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;

/* renamed from: X.1MR  reason: invalid class name */
public final class AnonymousClass1MR extends Drawable implements Drawable.Callback, AnonymousClass1MS {
    public Drawable A00;
    private AnonymousClass1AN A01;
    private boolean A02;

    private void A00(boolean z, boolean z2) {
        Drawable drawable = this.A00;
        if (drawable != null && drawable.isVisible() != z) {
            try {
                this.A00.setVisible(z, z2);
            } catch (NullPointerException unused) {
            }
        }
    }

    public void A01() {
        if (this.A00 != null) {
            A00(false, false);
            this.A00.setCallback(null);
        }
        this.A00 = null;
        this.A01 = null;
        this.A02 = false;
    }

    public void A02(Drawable drawable, AnonymousClass1AN r5) {
        Drawable drawable2 = this.A00;
        if (drawable2 != drawable) {
            boolean z = false;
            if (drawable2 != null) {
                A00(false, false);
                this.A00.setCallback(null);
            }
            this.A00 = drawable;
            if (drawable != null) {
                A00(isVisible(), false);
                this.A00.setCallback(this);
            }
            this.A01 = r5;
            if ((r5 != null && r5.A00) || ((Build.VERSION.SDK_INT < 11 && (this.A00 instanceof ColorDrawable)) || (this.A00 instanceof InsetDrawable))) {
                z = true;
            }
            this.A02 = z;
            invalidateSelf();
        }
    }

    public boolean CE9(MotionEvent motionEvent) {
        Drawable drawable;
        if (Build.VERSION.SDK_INT < 21 || (drawable = this.A00) == null || !(drawable instanceof RippleDrawable) || motionEvent.getActionMasked() != 0 || !getBounds().contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
            return false;
        }
        return true;
    }

    public void draw(Canvas canvas) {
        if (this.A00 != null) {
            Rect bounds = getBounds();
            int save = canvas.save();
            canvas.translate((float) bounds.left, (float) bounds.top);
            if (this.A02) {
                canvas.clipRect(0, 0, bounds.width(), bounds.height());
            }
            AnonymousClass1AN r0 = this.A01;
            if (r0 != null) {
                canvas.concat(r0);
            }
            this.A00.draw(canvas);
            canvas.restoreToCount(save);
        }
    }

    public int getChangingConfigurations() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return -1;
        }
        return drawable.getChangingConfigurations();
    }

    public Drawable getCurrent() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return null;
        }
        return drawable.getCurrent();
    }

    public int getIntrinsicHeight() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return -1;
        }
        return drawable.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return -1;
        }
        return drawable.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return -1;
        }
        return drawable.getMinimumHeight();
    }

    public int getMinimumWidth() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return -1;
        }
        return drawable.getMinimumWidth();
    }

    public int getOpacity() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return -1;
        }
        return drawable.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        Drawable drawable = this.A00;
        if (drawable == null || !drawable.getPadding(rect)) {
            return false;
        }
        return true;
    }

    public int[] getState() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return null;
        }
        return drawable.getState();
    }

    public Region getTransparentRegion() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return null;
        }
        return drawable.getTransparentRegion();
    }

    public boolean isStateful() {
        Drawable drawable = this.A00;
        if (drawable == null || !drawable.isStateful()) {
            return false;
        }
        return true;
    }

    public boolean onLevelChange(int i) {
        Drawable drawable = this.A00;
        if (drawable == null || !drawable.setLevel(i)) {
            return false;
        }
        return true;
    }

    public void setAlpha(int i) {
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setAlpha(i);
        }
    }

    public void setChangingConfigurations(int i) {
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setChangingConfigurations(i);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        }
    }

    public void setDither(boolean z) {
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setDither(z);
        }
    }

    public void setFilterBitmap(boolean z) {
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setFilterBitmap(z);
        }
    }

    public boolean setState(int[] iArr) {
        Drawable drawable = this.A00;
        if (drawable == null || !drawable.setState(iArr)) {
            return false;
        }
        return true;
    }

    public boolean BsU(MotionEvent motionEvent, View view) {
        Rect bounds = getBounds();
        this.A00.setHotspot((float) (((int) motionEvent.getX()) - bounds.left), (float) (((int) motionEvent.getY()) - bounds.top));
        return false;
    }

    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        A00(z, z2);
        return visible;
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }
}
