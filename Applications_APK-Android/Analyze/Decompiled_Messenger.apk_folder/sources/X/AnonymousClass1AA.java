package X;

import android.view.View;
import java.util.List;

/* renamed from: X.1AA  reason: invalid class name */
public final class AnonymousClass1AA {
    public int A00;
    public boolean A01;
    public boolean A02 = true;
    public int A03;
    public int A04;
    public int A05 = 0;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public List A0A = null;

    public static void A00(AnonymousClass1AA r7, View view) {
        int A052;
        int size = r7.A0A.size();
        View view2 = null;
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < size; i2++) {
            View view3 = ((C33781o8) r7.A0A.get(i2)).A0H;
            AnonymousClass1R7 r0 = (AnonymousClass1R7) view3.getLayoutParams();
            if (view3 != view) {
                C33781o8 r1 = r0.mViewHolder;
                if (!r1.A0E() && (A052 = (r1.A05() - r7.A04) * r7.A06) >= 0 && A052 < i) {
                    view2 = view3;
                    if (A052 == 0) {
                        break;
                    }
                    i = A052;
                }
            }
        }
        if (view2 == null) {
            r7.A04 = -1;
        } else {
            r7.A04 = ((AnonymousClass1R7) view2.getLayoutParams()).mViewHolder.A05();
        }
    }

    public View A01(C15740vp r7) {
        List list = this.A0A;
        if (list != null) {
            int size = list.size();
            int i = 0;
            while (i < size) {
                View view = ((C33781o8) this.A0A.get(i)).A0H;
                C33781o8 r2 = ((AnonymousClass1R7) view.getLayoutParams()).mViewHolder;
                if (r2.A0E() || this.A04 != r2.A05()) {
                    i++;
                } else {
                    A00(this, view);
                    return view;
                }
            }
            return null;
        }
        View A042 = r7.A04(this.A04);
        this.A04 += this.A06;
        return A042;
    }
}
