package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

/* renamed from: X.0fL  reason: invalid class name and case insensitive filesystem */
public final class C08430fL extends Handler {
    private final WeakReference A00;

    public void handleMessage(Message message) {
        C08450fN r4 = (C08450fN) this.A00.get();
        if (r4 != null) {
            int i = message.what;
            if (i == 0) {
                C08450fN.A02(r4, message.arg1);
            } else if (i == 1) {
                C08450fN.A03(r4, message.arg1);
            } else if (i == 2) {
                BJ6 bj6 = (BJ6) message.obj;
                if (bj6 != null) {
                    int i2 = bj6.A00;
                    if (r4.A07.get(i2) == 0) {
                        return;
                    }
                    if (((AnonymousClass0Ud) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B5j, r4.A00)).A0G()) {
                        C08450fN.A03(r4, i2);
                    } else {
                        sendMessageDelayed(obtainMessage(2, bj6), ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r4.A00)).At0(564740954784680L));
                    }
                }
            } else {
                throw new RuntimeException(AnonymousClass08S.A09("Unexpected message for StallSessionHandler ", i));
            }
        }
    }

    public C08430fL(C08450fN r2) {
        super(Looper.getMainLooper());
        this.A00 = new WeakReference(r2);
    }
}
