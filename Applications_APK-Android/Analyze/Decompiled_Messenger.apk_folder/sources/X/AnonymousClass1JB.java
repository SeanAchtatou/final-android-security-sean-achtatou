package X;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;

/* renamed from: X.1JB  reason: invalid class name */
public abstract class AnonymousClass1JB {
    private static final Class A00 = AnonymousClass1JB.class;

    public static ImmutableSet A00(String str) {
        RegularImmutableSet regularImmutableSet = RegularImmutableSet.A05;
        try {
            if (!C06850cB.A0B(str)) {
                return C28361eg.A00(C24351Tg.A00(str));
            }
        } catch (Exception e) {
            C010708t.A0B(A00, "Error de-serializing enabled UI features - %s: %s", e.getMessage(), e);
        }
        return regularImmutableSet;
    }
}
