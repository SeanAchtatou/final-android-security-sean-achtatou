package X;

import com.facebook.profilo.logger.Logger;
import java.lang.Thread;

/* renamed from: X.0HY  reason: invalid class name */
public final class AnonymousClass0HY implements Thread.UncaughtExceptionHandler {
    public void uncaughtException(Thread thread, Throwable th) {
        C003904y r0 = Logger.sLoggerCallbacks;
        if (r0 != null) {
            r0.Bds(th);
        }
    }
}
