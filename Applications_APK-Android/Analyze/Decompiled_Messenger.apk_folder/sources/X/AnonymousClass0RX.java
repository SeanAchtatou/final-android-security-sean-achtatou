package X;

import com.facebook.common.dextricks.classid.ClassId;
import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.provider.class_load.ClassLoadProvider;

/* renamed from: X.0RX  reason: invalid class name */
public final class AnonymousClass0RX implements C03460Nv {
    public void ASK(String str, Class cls) {
        Logger.writeStandardEntry(ClassLoadProvider.A01, 7, 81, 0, 0, 0, 0, ClassId.getClassId(cls));
    }

    public void ASL(String str) {
        Logger.writeStandardEntry(ClassLoadProvider.A01, 7, 82, 0, 0, 0, 0, 0);
    }

    public void ASM(String str) {
        Logger.writeStandardEntry(ClassLoadProvider.A01, 7, 80, 0, 0, 0, 0, 0);
    }
}
