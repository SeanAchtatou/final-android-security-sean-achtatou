package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1I9  reason: invalid class name */
public final class AnonymousClass1I9 extends Enum {
    private static final /* synthetic */ AnonymousClass1I9[] A00;
    public static final AnonymousClass1I9 A01;
    public static final AnonymousClass1I9 A02;
    public static final AnonymousClass1I9 A03;
    public static final AnonymousClass1I9 A04;
    public static final AnonymousClass1I9 A05;
    public static final AnonymousClass1I9 A06;
    public static final AnonymousClass1I9 A07;
    public static final AnonymousClass1I9 A08;
    public static final AnonymousClass1I9 A09;
    public static final AnonymousClass1I9 A0A;
    public static final AnonymousClass1I9 A0B;
    public static final AnonymousClass1I9 A0C;
    public static final AnonymousClass1I9 A0D;
    public static final AnonymousClass1I9 A0E;
    public static final AnonymousClass1I9 A0F;
    public static final AnonymousClass1I9 A0G;
    public static final AnonymousClass1I9 A0H;
    public static final AnonymousClass1I9 A0I;
    public static final AnonymousClass1I9 A0J;
    public static final AnonymousClass1I9 A0K;
    public static final AnonymousClass1I9 A0L;
    public static final AnonymousClass1I9 A0M;
    public static final AnonymousClass1I9 A0N;
    public static final AnonymousClass1I9 A0O;
    public static final AnonymousClass1I9 A0P;
    public static final AnonymousClass1I9 A0Q;
    public static final AnonymousClass1I9 A0R;
    public static final AnonymousClass1I9 A0S;
    public static final AnonymousClass1I9 A0T;
    public static final AnonymousClass1I9 A0U;
    public static final AnonymousClass1I9 A0V;
    public final String mValue;

    static {
        AnonymousClass1I9 r0 = new AnonymousClass1I9("NONE", 0, null);
        A0H = r0;
        AnonymousClass1I9 r02 = new AnonymousClass1I9("BUTTON", 1, "android.widget.Button");
        A02 = r02;
        AnonymousClass1I9 r03 = new AnonymousClass1I9("CHECK_BOX", 2, "android.widget.CompoundButton");
        A04 = r03;
        AnonymousClass1I9 r04 = new AnonymousClass1I9("DROP_DOWN_LIST", 3, "android.widget.Spinner");
        A08 = r04;
        AnonymousClass1I9 r05 = new AnonymousClass1I9("EDIT_TEXT", 4, "android.widget.EditText");
        A09 = r05;
        AnonymousClass1I9 r06 = new AnonymousClass1I9("GRID", 5, "android.widget.GridView");
        A0A = r06;
        String $const$string = C99084oO.$const$string(2);
        AnonymousClass1I9 r10 = new AnonymousClass1I9("IMAGE", 6, $const$string);
        A0D = r10;
        AnonymousClass1I9 r3 = new AnonymousClass1I9("IMAGE_BUTTON", 7, $const$string);
        A0E = r3;
        AnonymousClass1I9 r07 = new AnonymousClass1I9("LIST", 8, "android.widget.AbsListView");
        A0G = r07;
        AnonymousClass1I9 r08 = new AnonymousClass1I9("PAGER", 9, C22298Ase.$const$string(178));
        A0J = r08;
        AnonymousClass1I9 r09 = new AnonymousClass1I9("RADIO_BUTTON", 10, C99084oO.$const$string(391));
        A0L = r09;
        AnonymousClass1I9 r010 = new AnonymousClass1I9("SEEK_CONTROL", 11, "android.widget.SeekBar");
        A0N = r010;
        AnonymousClass1I9 r011 = new AnonymousClass1I9("SWITCH", 12, "android.widget.Switch");
        A0P = r011;
        AnonymousClass1I9 r4 = new AnonymousClass1I9("TAB_BAR", 13, C99084oO.$const$string(AnonymousClass1Y3.A3B));
        A0Q = r4;
        AnonymousClass1I9 r42 = new AnonymousClass1I9(ECX.$const$string(116), 14, C99084oO.$const$string(AnonymousClass1Y3.A3C));
        A0U = r42;
        AnonymousClass1I9 r43 = new AnonymousClass1I9("VIEW_GROUP", 15, "android.view.ViewGroup");
        AnonymousClass1I9 r44 = new AnonymousClass1I9("WEB_VIEW", 16, "android.webkit.WebView");
        A0V = r44;
        AnonymousClass1I9 r45 = new AnonymousClass1I9("CHECKED_TEXT_VIEW", 17, "android.widget.CheckedTextView");
        A03 = r45;
        AnonymousClass1I9 r46 = new AnonymousClass1I9("PROGRESS_BAR", 18, "android.widget.ProgressBar");
        A0K = r46;
        AnonymousClass1I9 r15 = new AnonymousClass1I9("ACTION_BAR_TAB", 19, "android.app.ActionBar$Tab");
        A01 = r15;
        AnonymousClass1I9 r14 = new AnonymousClass1I9("DRAWER_LAYOUT", 20, "androidx.drawerlayout.widget.DrawerLayout");
        A07 = r14;
        AnonymousClass1I9 r13 = new AnonymousClass1I9("SLIDING_DRAWER", 21, "android.widget.SlidingDrawer");
        A0O = r13;
        AnonymousClass1I9 r12 = new AnonymousClass1I9("ICON_MENU", 22, "com.android.internal.view.menu.IconMenuView");
        A0C = r12;
        AnonymousClass1I9 r11 = new AnonymousClass1I9("TOAST", 23, "android.widget.Toast$TN");
        A0T = r11;
        AnonymousClass1I9 r2 = new AnonymousClass1I9("DATE_PICKER_DIALOG", 24, "android.app.DatePickerDialog");
        A06 = r2;
        AnonymousClass1I9 r1 = new AnonymousClass1I9("TIME_PICKER_DIALOG", 25, "android.app.TimePickerDialog");
        A0S = r1;
        AnonymousClass1I9 r012 = new AnonymousClass1I9("DATE_PICKER", 26, "android.widget.DatePicker");
        A05 = r012;
        AnonymousClass1I9 r9 = new AnonymousClass1I9("TIME_PICKER", 27, "android.widget.TimePicker");
        A0R = r9;
        AnonymousClass1I9 r8 = new AnonymousClass1I9("NUMBER_PICKER", 28, "android.widget.NumberPicker");
        A0I = r8;
        AnonymousClass1I9 r7 = new AnonymousClass1I9("SCROLL_VIEW", 29, "android.widget.ScrollView");
        A0M = r7;
        AnonymousClass1I9 r35 = new AnonymousClass1I9("HORIZONTAL_SCROLL_VIEW", 30, "android.widget.HorizontalScrollView");
        A0B = r35;
        AnonymousClass1I9 r352 = new AnonymousClass1I9("KEYBOARD_KEY", 31, "android.inputmethodservice.Keyboard$Key");
        A0F = r352;
        AnonymousClass1I9[] r47 = new AnonymousClass1I9[32];
        System.arraycopy(new AnonymousClass1I9[]{r0, r02, r03, r04, r05, r06, r10, r3, r07, r08, r09, r010, r011, r4, r42, r43, r44, r45, r46, r15, r14, r13, r12, r11, r2, r1, r012}, 0, r47, 0, 27);
        System.arraycopy(new AnonymousClass1I9[]{r9, r8, r7, r35, r352}, 0, r47, 27, 5);
        A00 = r47;
    }

    public static AnonymousClass1I9 valueOf(String str) {
        return (AnonymousClass1I9) Enum.valueOf(AnonymousClass1I9.class, str);
    }

    public static AnonymousClass1I9[] values() {
        return (AnonymousClass1I9[]) A00.clone();
    }

    private AnonymousClass1I9(String str, int i, String str2) {
        this.mValue = str2;
    }

    public static AnonymousClass1I9 A00(String str) {
        for (AnonymousClass1I9 r1 : values()) {
            String str2 = r1.mValue;
            if (str2 != null && str2.equals(str)) {
                return r1;
            }
        }
        return A0H;
    }
}
