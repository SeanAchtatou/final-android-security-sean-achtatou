package X;

import java.util.Map;

/* renamed from: X.1Ts  reason: invalid class name and case insensitive filesystem */
public final class C24461Ts {
    public final Map A00;
    public final boolean A01;
    public final boolean A02;

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002e, code lost:
        if (r1.getBoolean("enforce_scheme") == false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map A00(java.lang.String r12) {
        /*
            java.lang.String r10 = "enforce_scheme_and_authority"
            java.lang.String r9 = "enforce_scheme"
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x006f }
            r7.<init>(r12)     // Catch:{ JSONException -> 0x006f }
            java.util.Iterator r12 = r7.keys()     // Catch:{ JSONException -> 0x006f }
        L_0x0012:
            boolean r0 = r12.hasNext()     // Catch:{ JSONException -> 0x006f }
            if (r0 == 0) goto L_0x006f
            java.lang.Object r11 = r12.next()     // Catch:{ JSONException -> 0x006f }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ JSONException -> 0x006f }
            org.json.JSONObject r1 = r7.getJSONObject(r11)     // Catch:{ JSONException -> 0x006f }
            boolean r0 = r1.has(r9)     // Catch:{ JSONException -> 0x006f }
            r6 = 1
            if (r0 == 0) goto L_0x0030
            boolean r0 = r1.getBoolean(r9)     // Catch:{ JSONException -> 0x006f }
            r5 = 1
            if (r0 != 0) goto L_0x0031
        L_0x0030:
            r5 = 0
        L_0x0031:
            boolean r0 = r1.has(r10)     // Catch:{ JSONException -> 0x006f }
            if (r0 == 0) goto L_0x0064
            boolean r0 = r1.getBoolean(r10)     // Catch:{ JSONException -> 0x006f }
            if (r0 == 0) goto L_0x0064
        L_0x003d:
            java.lang.String r0 = "whitelist"
            org.json.JSONObject r4 = r1.getJSONObject(r0)     // Catch:{ JSONException -> 0x006f }
            java.util.Iterator r3 = r4.keys()     // Catch:{ JSONException -> 0x006f }
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ JSONException -> 0x006f }
            r2.<init>()     // Catch:{ JSONException -> 0x006f }
        L_0x004c:
            boolean r0 = r3.hasNext()     // Catch:{ JSONException -> 0x006f }
            if (r0 == 0) goto L_0x0066
            java.lang.Object r1 = r3.next()     // Catch:{ JSONException -> 0x006f }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ JSONException -> 0x006f }
            java.lang.String r0 = r4.getString(r1)     // Catch:{ JSONException -> 0x006f }
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r0)     // Catch:{ JSONException -> 0x006f }
            r2.put(r1, r0)     // Catch:{ JSONException -> 0x006f }
            goto L_0x004c
        L_0x0064:
            r6 = 0
            goto L_0x003d
        L_0x0066:
            X.1Ts r0 = new X.1Ts     // Catch:{ JSONException -> 0x006f }
            r0.<init>(r5, r6, r2)     // Catch:{ JSONException -> 0x006f }
            r8.put(r11, r0)     // Catch:{ JSONException -> 0x006f }
            goto L_0x0012
        L_0x006f:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24461Ts.A00(java.lang.String):java.util.Map");
    }

    private C24461Ts(boolean z, boolean z2, Map map) {
        this.A02 = z;
        this.A01 = z2;
        this.A00 = map;
    }
}
