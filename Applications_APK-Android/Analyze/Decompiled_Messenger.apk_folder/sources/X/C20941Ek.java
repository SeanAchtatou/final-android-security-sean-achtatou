package X;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1Ek  reason: invalid class name and case insensitive filesystem */
public final class C20941Ek {
    public final AnonymousClass06B A00 = AnonymousClass067.A02();
    public final C10970lA A01;
    public final C10960l9 A02;
    public final C20951El A03;
    public final FbSharedPreferences A04;
    private final Context A05;
    private final TelephonyManager A06;
    private final C21451Ha A07;
    private final C185814g A08;

    public static final C20941Ek A00(AnonymousClass1XY r1) {
        return new C20941Ek(r1);
    }

    public static boolean A01(C20941Ek r10, Integer num) {
        int i;
        int i2;
        if (r10.A04.Aep(C10990lD.A0F, false)) {
            return true;
        }
        if (!r10.A04.Aep(C10990lD.A0H, false) && r10.A03.A02()) {
            boolean A022 = r10.A08.A02();
            if (!r10.A04.BBh(C10990lD.A07)) {
                boolean A042 = AnonymousClass9NB.A04();
                boolean A082 = r10.A08.A00.A08("android.permission.READ_PHONE_STATE");
                if (!A042 || !A082) {
                    i = 0;
                } else {
                    try {
                        i = AnonymousClass9NB.A00(r10.A05);
                        try {
                            i2 = AnonymousClass9NB.A01(r10.A05);
                        } catch (SecurityException e) {
                            e = e;
                        }
                    } catch (SecurityException e2) {
                        e = e2;
                        i = 0;
                        C010708t.A0R("SmsTakeoverNuxController", e, "Unable to get device sim slot information");
                        i2 = 0;
                        C21451Ha r2 = r10.A07;
                        C11670nb A012 = C21451Ha.A01("sms_takeover_device_status");
                        A012.A0E("has_sms_contact_permissions", A022);
                        A012.A0E("has_phone_permissions", A082);
                        A012.A0E("has_multisim_api", A042);
                        A012.A09("active_sim_slots", i);
                        A012.A09("max_sim_slots", i2);
                        C21451Ha.A05(r2, A012);
                        C30281hn edit = r10.A04.edit();
                        edit.putBoolean(C10990lD.A07, true);
                        edit.commit();
                        if (!r10.A02.A0B()) {
                        }
                        return false;
                    }
                    C21451Ha r22 = r10.A07;
                    C11670nb A0122 = C21451Ha.A01("sms_takeover_device_status");
                    A0122.A0E("has_sms_contact_permissions", A022);
                    A0122.A0E("has_phone_permissions", A082);
                    A0122.A0E("has_multisim_api", A042);
                    A0122.A09("active_sim_slots", i);
                    A0122.A09("max_sim_slots", i2);
                    C21451Ha.A05(r22, A0122);
                    C30281hn edit2 = r10.A04.edit();
                    edit2.putBoolean(C10990lD.A07, true);
                    edit2.commit();
                }
                i2 = 0;
                C21451Ha r222 = r10.A07;
                C11670nb A01222 = C21451Ha.A01("sms_takeover_device_status");
                A01222.A0E("has_sms_contact_permissions", A022);
                A01222.A0E("has_phone_permissions", A082);
                A01222.A0E("has_multisim_api", A042);
                A01222.A09("active_sim_slots", i);
                A01222.A09("max_sim_slots", i2);
                C21451Ha.A05(r222, A01222);
                C30281hn edit22 = r10.A04.edit();
                edit22.putBoolean(C10990lD.A07, true);
                edit22.commit();
            }
            if (!r10.A02.A0B() || num == AnonymousClass07B.A00 || r10.A01.A00.AbO(264, false) || r10.A06.getSimState() == 1 || r10.A03.A01()) {
                return false;
            }
            return true;
        }
        return false;
    }

    private C20941Ek(AnonymousClass1XY r2) {
        this.A06 = C04490Ux.A0c(r2);
        this.A05 = AnonymousClass1YA.A00(r2);
        this.A04 = FbSharedPreferencesModule.A00(r2);
        this.A02 = C10960l9.A00(r2);
        this.A01 = C10970lA.A00(r2);
        this.A03 = new C20951El(r2);
        this.A07 = C21451Ha.A02(r2);
        this.A08 = C185814g.A00(r2);
    }
}
