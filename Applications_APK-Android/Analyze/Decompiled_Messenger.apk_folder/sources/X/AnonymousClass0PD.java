package X;

import java.io.File;
import java.util.Comparator;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0PD  reason: invalid class name */
public final class AnonymousClass0PD {
    public static final Comparator A02 = new AnonymousClass0PA();
    private static volatile AnonymousClass0PD A03;
    public AnonymousClass05m A00;
    private File A01;

    public static final AnonymousClass0PD A00(AnonymousClass1XY r3) {
        if (A03 == null) {
            synchronized (AnonymousClass0PD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A03 = new AnonymousClass0PD();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static File A01(AnonymousClass0PD r3) {
        C004505k A002;
        File file = r3.A01;
        if (file != null) {
            return file;
        }
        if (r3.A00 == null && (A002 = C004505k.A00()) != null) {
            r3.A00 = A002.A03;
        }
        AnonymousClass05m r0 = r3.A00;
        if (r0 != null) {
            File file2 = r0.A04;
            if (file2.isDirectory() || file2.mkdirs()) {
                r3.A01 = file2;
                return file2;
            }
        }
        return null;
    }
}
