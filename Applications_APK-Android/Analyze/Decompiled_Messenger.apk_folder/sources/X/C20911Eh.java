package X;

import android.os.SystemClock;
import java.util.Deque;

/* renamed from: X.1Eh  reason: invalid class name and case insensitive filesystem */
public final class C20911Eh implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.litho.widget.RecyclerBinder$14";
    public final /* synthetic */ Deque A00;
    public final /* synthetic */ boolean A01;

    public C20911Eh(Deque deque, boolean z) {
        this.A00 = deque;
        this.A01 = z;
    }

    public void run() {
        long uptimeMillis = SystemClock.uptimeMillis();
        while (!this.A00.isEmpty()) {
            ((C20841Ea) this.A00.pollFirst()).BVZ(this.A01, uptimeMillis);
        }
    }
}
