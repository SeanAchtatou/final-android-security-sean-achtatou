package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.orca.threadlist.inbox.InboxLoadMorePlaceholderItem;

/* renamed from: X.1GG  reason: invalid class name */
public final class AnonymousClass1GG implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxLoadMorePlaceholderItem(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxLoadMorePlaceholderItem[i];
    }
}
