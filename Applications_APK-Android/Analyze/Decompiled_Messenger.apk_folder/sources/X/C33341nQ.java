package X;

/* renamed from: X.1nQ  reason: invalid class name and case insensitive filesystem */
public final class C33341nQ implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.search.nux.InboxSearchBarAssistantTooltipInterstitialController$1";
    public final /* synthetic */ AnonymousClass0p4 A00;
    public final /* synthetic */ AnonymousClass1BB A01;

    public C33341nQ(AnonymousClass1BB r1, AnonymousClass0p4 r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void run() {
        AnonymousClass1BB r6 = this.A01;
        C20331Bz r5 = r6.A02;
        AnonymousClass0p4 r4 = this.A00;
        r5.A03(r4.A09, AnonymousClass1BB.A05, AnonymousClass1BB.class, new C24141Sl(r6, r4));
    }
}
