package X;

import com.facebook.messaging.montage.inboxunit.InboxMontageItem;
import java.util.Comparator;

/* renamed from: X.1ra  reason: invalid class name and case insensitive filesystem */
public final class C35661ra implements Comparator {
    public int compare(Object obj, Object obj2) {
        Boolean valueOf;
        Boolean valueOf2;
        InboxMontageItem inboxMontageItem = (InboxMontageItem) obj;
        InboxMontageItem inboxMontageItem2 = (InboxMontageItem) obj2;
        boolean z = inboxMontageItem.A06;
        if (z || inboxMontageItem2.A06) {
            valueOf = Boolean.valueOf(inboxMontageItem2.A06);
            valueOf2 = Boolean.valueOf(z);
        } else {
            valueOf = Boolean.valueOf(inboxMontageItem2.A02.A07);
            valueOf2 = Boolean.valueOf(inboxMontageItem.A02.A07);
        }
        return valueOf.compareTo(valueOf2);
    }
}
