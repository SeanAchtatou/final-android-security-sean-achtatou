package X;

import java.util.Iterator;
import java.util.List;

/* renamed from: X.1rO  reason: invalid class name and case insensitive filesystem */
public final class C35541rO implements Iterator {
    public int A00;
    private int A01;
    public final List A02;
    public final boolean A03;

    /* renamed from: A00 */
    public synchronized AnonymousClass1IK next() {
        if (!hasNext()) {
            return null;
        }
        AnonymousClass1IK r1 = (AnonymousClass1IK) this.A02.get(this.A00);
        if (this.A03) {
            this.A00--;
        } else {
            this.A00++;
        }
        this.A01--;
        return r1;
    }

    public void remove() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r2 >= r3.A02.size()) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasNext() {
        /*
            r3 = this;
        L_0x0000:
            int r0 = r3.A01
            if (r0 <= 0) goto L_0x0042
            int r2 = r3.A00
            if (r2 < 0) goto L_0x0011
            java.util.List r0 = r3.A02
            int r1 = r0.size()
            r0 = 1
            if (r2 < r1) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x0042
            java.util.List r1 = r3.A02
            int r0 = r3.A00
            java.lang.Object r1 = r1.get(r0)
            X.1IK r1 = (X.AnonymousClass1IK) r1
            X.1Ih r0 = r1.A03()
            boolean r0 = r0.C2H()
            if (r0 == 0) goto L_0x0030
            boolean r0 = r1.A09()
            if (r0 != 0) goto L_0x0030
            r0 = 1
            return r0
        L_0x0030:
            boolean r0 = r3.A03
            if (r0 == 0) goto L_0x003b
            int r0 = r3.A00
            int r0 = r0 + -1
            r3.A00 = r0
            goto L_0x0000
        L_0x003b:
            int r0 = r3.A00
            int r0 = r0 + 1
            r3.A00 = r0
            goto L_0x0000
        L_0x0042:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35541rO.hasNext():boolean");
    }

    public C35541rO(List list, int i, int i2, boolean z) {
        int i3;
        this.A02 = list;
        if (z) {
            i3 = i - 1;
        } else {
            i3 = i + 1;
        }
        this.A00 = i3;
        this.A01 = i2;
        this.A03 = z;
    }
}
