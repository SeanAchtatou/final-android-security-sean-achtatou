package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.19C  reason: invalid class name */
public final class AnonymousClass19C extends AnonymousClass0W1 {
    private static volatile AnonymousClass19C A02;
    private final AnonymousClass0US A00;
    private final AnonymousClass0US A01;

    private AnonymousClass19C(AnonymousClass0US r17, AnonymousClass0US r18) {
        super("tincan", 45, ImmutableList.of(new AnonymousClass19D(), new AnonymousClass19F(), new AnonymousClass19J(), new AnonymousClass19K(), new AnonymousClass19O(), new AnonymousClass19U(), new C196219a(), new C196519d(), new C196619e(), new C194818i(), new C197019i(), new AnonymousClass1C3(), new C30621iN()));
        this.A00 = r17;
        this.A01 = r18;
    }

    public static final AnonymousClass19C A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (AnonymousClass19C.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A02 = new AnonymousClass19C(C04750Wa.A03(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.A8c, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private void A01(SQLiteDatabase sQLiteDatabase) {
        C24971Xv it = this.A00.iterator();
        while (it.hasNext()) {
            String A002 = AnonymousClass0W4.A00(((AnonymousClass0W4) it.next()).A00);
            C007406x.A00(-2137844074);
            sQLiteDatabase.execSQL(A002);
            C007406x.A00(1011532650);
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r5v22, types: [java.lang.String, java.lang.String[]] */
    /* JADX WARN: Type inference failed for: r5v24 */
    /* JADX WARN: Type inference failed for: r5v28 */
    /* JADX WARN: Type inference failed for: r6v32, types: [java.lang.String[], android.database.Cursor] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:220|221|222|223|224|225|232) */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x03c4, code lost:
        if (r5 != false) goto L_0x06bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x08ab, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x08ac, code lost:
        if (r9 != null) goto L_0x08ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x08bd, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:0x08be, code lost:
        if (r6 != null) goto L_0x08c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x08cd, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x08ce, code lost:
        if (r7 != null) goto L_0x08d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x02ed, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        X.C010708t.A0E(X.C91394Xy.A04, r7, "Failed to move encrypted message attachments for thead %s", r12);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:223:0x0723 */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x02ed A[ExcHandler: 1wY | 1wZ | IOException | IllegalStateException (r7v23 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:77:0x0264] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.database.sqlite.SQLiteDatabase r21, int r22, int r23) {
        /*
            r20 = this;
            r1 = r22
            r3 = 23
            r0 = r21
            r2 = r20
            if (r1 >= r3) goto L_0x0011
            r2.A01(r0)
            r2.A04(r0)
            return
        L_0x0011:
            r8 = r23
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)
        L_0x0017:
            if (r1 >= r8) goto L_0x08fa
            X.0US r4 = r2.A01
            java.lang.Object r7 = r4.get()
            X.4Xz r7 = (X.C91404Xz) r7
            r6 = 23
            r5 = 0
            if (r1 < r6) goto L_0x0027
            r5 = 1
        L_0x0027:
            java.lang.String r4 = "currentVersion (%s) must be greater than %s for incremental upgrade: "
            com.google.common.base.Preconditions.checkArgument(r5, r4, r1, r6)
            switch(r1) {
                case 23: goto L_0x0035;
                case 24: goto L_0x0048;
                case 25: goto L_0x005b;
                case 26: goto L_0x0195;
                case 27: goto L_0x01de;
                case 28: goto L_0x035e;
                case 29: goto L_0x0376;
                case 30: goto L_0x0381;
                case 31: goto L_0x038c;
                case 32: goto L_0x03c8;
                case 33: goto L_0x04ab;
                case 34: goto L_0x04cb;
                case 35: goto L_0x04eb;
                case 36: goto L_0x04f6;
                case 37: goto L_0x06bf;
                case 38: goto L_0x06df;
                case 39: goto L_0x06fb;
                case 40: goto L_0x0706;
                case 41: goto L_0x0751;
                case 42: goto L_0x0751;
                case 43: goto L_0x078d;
                case 44: goto L_0x07b1;
                default: goto L_0x002f;
            }
        L_0x002f:
            r4 = 0
        L_0x0030:
            if (r4 == 0) goto L_0x08d9
            int r1 = r1 + 1
            goto L_0x0017
        L_0x0035:
            r4 = -687794809(0xffffffffd7011587, float:-1.41929459E14)
            X.C007406x.A00(r4)
            java.lang.String r4 = "ALTER TABLE messages ADD COLUMN facebook_hmac BLOB"
            r0.execSQL(r4)
            r4 = -802223494(0xffffffffd02f0a7a, float:-1.17467976E10)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x0048:
            r4 = -1278833143(0xffffffffb3c68e09, float:-9.2459295E-8)
            X.C007406x.A00(r4)
            java.lang.String r4 = "ALTER TABLE messages ADD COLUMN expired INTEGER"
            r0.execSQL(r4)
            r4 = 1111656763(0x4242893b, float:48.634014)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x005b:
            X.0US r4 = r7.A01
            java.lang.Object r7 = r4.get()
            X.9cE r7 = (X.C200569cE) r7
            r11 = 0
            java.lang.String r4 = "SELECT encrypted_value FROM properties WHERE key='local_identity_key'"
            android.database.Cursor r9 = r0.rawQuery(r4, r11)
            r13 = 0
            r15 = 0
        L_0x006c:
            boolean r4 = r9.moveToNext()     // Catch:{ all -> 0x08a9 }
            if (r4 == 0) goto L_0x00b6
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ all -> 0x08a9 }
            r5.<init>()     // Catch:{ all -> 0x08a9 }
            android.database.DatabaseUtils.cursorRowToContentValues(r9, r5)     // Catch:{ all -> 0x08a9 }
            java.lang.String r4 = "encrypted_value"
            byte[] r12 = r5.getAsByteArray(r4)     // Catch:{ all -> 0x08a9 }
            int r5 = X.AnonymousClass1Y3.Aes     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            X.0UN r4 = r7.A00     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            java.lang.Object r14 = X.AnonymousClass1XX.A02(r13, r5, r4)     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            X.9cF r14 = (X.C200579cF) r14     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            X.18A r5 = r14.A01     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            X.0Tq r4 = r14.A02     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            java.lang.Object r10 = r4.get()     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            com.facebook.crypto.keychain.KeyChain r10 = (com.facebook.crypto.keychain.KeyChain) r10     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            X.192 r6 = new X.192     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            X.18C r5 = r5.A00     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            java.lang.Integer r4 = X.AnonymousClass07B.A01     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            r6.<init>(r10, r5, r4)     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            X.18e r4 = r14.A00     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            r6.A00(r12, r4)     // Catch:{ 1wL -> 0x00ac, 1wY | 1wZ | IOException -> 0x00a3 }
            goto L_0x006c
        L_0x00a3:
            r6 = move-exception
            java.lang.Class r5 = X.C200569cE.A01     // Catch:{ all -> 0x08a9 }
            java.lang.String r4 = "Upgrade to v24 failed trying legacy crypto check."
            X.C010708t.A08(r5, r4, r6)     // Catch:{ all -> 0x08a9 }
            goto L_0x00b4
        L_0x00ac:
            r6 = move-exception
            java.lang.Class r5 = X.C200569cE.A01     // Catch:{ all -> 0x08a9 }
            java.lang.String r4 = "Inaccessible encrypted data found in db; clearing data."
            X.C010708t.A09(r5, r4, r6)     // Catch:{ all -> 0x08a9 }
        L_0x00b4:
            r15 = 1
            goto L_0x006c
        L_0x00b6:
            r9.close()
            if (r15 == 0) goto L_0x00c1
            X.C200569cE.A01(r7)
            r4 = 0
            goto L_0x0030
        L_0x00c1:
            X.0W6 r10 = new X.0W6
            java.lang.String r5 = "key"
            java.lang.String r4 = "TEXT"
            r10.<init>(r5, r4)
            X.0W6 r9 = new X.0W6
            java.lang.String r5 = "value"
            java.lang.String r4 = "BLOB"
            r9.<init>(r5, r4)
            com.google.common.collect.ImmutableList r12 = com.google.common.collect.ImmutableList.of(r10, r9)
            X.0al r5 = new X.0al
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r10)
            r5.<init>(r4)
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r5)
            java.lang.String r6 = "keychain"
            java.lang.String r5 = X.AnonymousClass0W4.A03(r6, r12, r4)
            r4 = -508864929(0xffffffffe1ab565f, float:-3.950771E20)
            X.C007406x.A00(r4)
            r0.execSQL(r5)
            r4 = -1218235019(0xffffffffb7633575, float:-1.3542701E-5)
            X.C007406x.A00(r4)
            int r5 = X.AnonymousClass1Y3.Aes
            X.0UN r4 = r7.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r13, r5, r4)
            X.9cF r4 = (X.C200579cF) r4
            X.0Tq r4 = r4.A02
            java.lang.Object r4 = r4.get()
            X.9cD r4 = (X.C200559cD) r4
            com.facebook.prefs.shared.FbSharedPreferences r12 = r4.A01
            X.1Y7 r4 = X.C200589cG.A01
            r5 = 0
            java.lang.String r12 = r12.B4F(r4, r11)
            if (r12 == 0) goto L_0x011a
            byte[] r5 = android.util.Base64.decode(r12, r13)
        L_0x011a:
            if (r5 == 0) goto L_0x0146
            X.19A r4 = X.AnonymousClass197.A03
            java.lang.String r13 = r4.A05()
        L_0x0122:
            android.content.ContentValues r12 = new android.content.ContentValues
            r12.<init>()
            java.lang.String r4 = r10.A00
            r12.put(r4, r13)
            java.lang.String r4 = r9.A00
            r12.put(r4, r5)
            r4 = -2054831049(0xffffffff8585c437, float:-1.2579338E-35)
            X.C007406x.A00(r4)
            r0.insert(r6, r11, r12)
            r4 = -675941017(0xffffffffd7b5f567, float:-4.00131199E14)
            X.C007406x.A00(r4)
            X.C200569cE.A01(r7)
            r4 = 1
            goto L_0x0030
        L_0x0146:
            int r5 = X.AnonymousClass1Y3.Aes     // Catch:{ 1wY -> 0x0187 }
            X.0UN r4 = r7.A00     // Catch:{ 1wY -> 0x0187 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r13, r5, r4)     // Catch:{ 1wY -> 0x0187 }
            X.9cF r4 = (X.C200579cF) r4     // Catch:{ 1wY -> 0x0187 }
            X.0Tq r4 = r4.A02     // Catch:{ 1wY -> 0x0187 }
            java.lang.Object r5 = r4.get()     // Catch:{ 1wY -> 0x0187 }
            X.9cD r5 = (X.C200559cD) r5     // Catch:{ 1wY -> 0x0187 }
            X.0Tq r4 = r5.A02     // Catch:{ 1wY -> 0x0187 }
            java.lang.Object r4 = r4.get()     // Catch:{ 1wY -> 0x0187 }
            com.facebook.user.model.User r4 = (com.facebook.user.model.User) r4     // Catch:{ 1wY -> 0x0187 }
            if (r4 != 0) goto L_0x0163
            goto L_0x0166
        L_0x0163:
            java.lang.String r4 = r4.A0j     // Catch:{ 1wY -> 0x0187 }
            goto L_0x0167
        L_0x0166:
            r4 = 0
        L_0x0167:
            if (r4 == 0) goto L_0x0174
            byte[] r5 = X.C200559cD.A01(r5, r4)     // Catch:{ 1wY -> 0x0187 }
            X.19A r4 = X.AnonymousClass197.A04     // Catch:{ 1wY -> 0x0187 }
            java.lang.String r13 = r4.A05()     // Catch:{ 1wY -> 0x0187 }
            goto L_0x0122
        L_0x0174:
            r4 = 292(0x124, float:4.09E-43)
            java.lang.String r5 = X.AnonymousClass80H.$const$string(r4)     // Catch:{ 1wY -> 0x0187 }
            java.lang.String r4 = "getRawUserMasterKey called when user not logged in"
            X.C010708t.A0I(r5, r4)     // Catch:{ 1wY -> 0x0187 }
            X.1wY r5 = new X.1wY     // Catch:{ 1wY -> 0x0187 }
            java.lang.String r4 = "Legacy key chain only available with logged-in user"
            r5.<init>(r4)     // Catch:{ 1wY -> 0x0187 }
            throw r5     // Catch:{ 1wY -> 0x0187 }
        L_0x0187:
            r6 = move-exception
            java.lang.Class r5 = X.C200569cE.A01
            java.lang.String r4 = "Upgrade to v26 failed trying to retrieve V2 master key."
            X.C010708t.A08(r5, r4, r6)
            X.C200569cE.A01(r7)
            r4 = 0
            goto L_0x0030
        L_0x0195:
            X.0US r4 = r7.A02
            java.lang.Object r7 = r4.get()
            X.22t r7 = (X.C407422t) r7
            X.0Tq r4 = r7.A01
            java.lang.Object r4 = r4.get()
            X.18F r4 = (X.AnonymousClass18F) r4
            byte[] r14 = X.AnonymousClass9UU.A01(r0, r4)
            if (r14 != 0) goto L_0x01b5
            java.lang.Class r5 = X.C407422t.A03
            java.lang.String r4 = "Failed to retrieve master key during v27 db upgrade"
            X.C010708t.A07(r5, r4)
            r4 = 0
            goto L_0x0030
        L_0x01b5:
            java.lang.String r11 = "pre_keys"
            java.lang.String r12 = "next_pkid"
            r13 = 16777214(0xfffffe, float:2.3509884E-38)
            r9 = r7
            r10 = r0
            X.C407422t.A02(r9, r10, r11, r12, r13, r14)     // Catch:{ 1wY | 1wZ | IOException -> 0x01d0 }
            java.lang.String r11 = "signed_pre_keys"
            java.lang.String r12 = "next_spkid"
            r13 = 2147483647(0x7fffffff, float:NaN)
            X.C407422t.A02(r9, r10, r11, r12, r13, r14)     // Catch:{ 1wY | 1wZ | IOException -> 0x01d0 }
            X.C407422t.A01(r7)     // Catch:{ 1wY | 1wZ | IOException -> 0x01d0 }
            goto L_0x06bc
        L_0x01d0:
            r6 = move-exception
            java.lang.Class r5 = X.C407422t.A03
            java.lang.String r4 = "Failed to migrate pre key id during v27 db upgrade"
            X.C010708t.A0A(r5, r4, r6)
            X.C407422t.A01(r7)
            r4 = 0
            goto L_0x0030
        L_0x01de:
            X.0US r4 = r7.A03
            java.lang.Object r10 = r4.get()
            X.4Xy r10 = (X.C91394Xy) r10
            X.0US r4 = r10.A01
            java.lang.Object r12 = r4.get()
            X.187 r12 = (X.AnonymousClass187) r12
            X.0Tq r4 = r10.A03
            java.lang.Object r4 = r4.get()
            X.18F r4 = (X.AnonymousClass18F) r4
            byte[] r9 = X.AnonymousClass9UU.A01(r0, r4)
            r11 = 0
            if (r9 != 0) goto L_0x020e
            java.lang.Class r5 = X.AnonymousClass9UU.A00
            java.lang.String r4 = "Failed to retrieve master key to generated thread to encryption key map"
            X.C010708t.A07(r5, r4)
        L_0x0204:
            if (r11 == 0) goto L_0x02fe
            r5 = 0
            java.lang.String r4 = "SELECT thread_key, encrypted_content FROM messages"
            android.database.Cursor r9 = r0.rawQuery(r4, r5)
            goto L_0x0240
        L_0x020e:
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.String r4 = "SELECT thread_key, encryption_key FROM threads"
            android.database.Cursor r6 = r0.rawQuery(r4, r11)
        L_0x0219:
            boolean r4 = r6.moveToNext()     // Catch:{ all -> 0x08bb }
            if (r4 == 0) goto L_0x023b
            android.content.ContentValues r11 = new android.content.ContentValues     // Catch:{ all -> 0x08bb }
            r11.<init>()     // Catch:{ all -> 0x08bb }
            android.database.DatabaseUtils.cursorRowToContentValues(r6, r11)     // Catch:{ all -> 0x08bb }
            java.lang.String r4 = "thread_key"
            java.lang.String r5 = r11.getAsString(r4)     // Catch:{ all -> 0x08bb }
            java.lang.String r4 = "encryption_key"
            byte[] r4 = r11.getAsByteArray(r4)     // Catch:{ all -> 0x08bb }
            byte[] r4 = r12.A05(r9, r4)     // Catch:{ 1wY | 1wZ | IOException -> 0x08b2 }
            r7.put(r5, r4)     // Catch:{ 1wY | 1wZ | IOException -> 0x08b2 }
            goto L_0x0219
        L_0x023b:
            r6.close()
            r11 = r7
            goto L_0x0204
        L_0x0240:
            boolean r4 = r9.moveToNext()     // Catch:{ all -> 0x08a9 }
            if (r4 == 0) goto L_0x02fb
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ all -> 0x08a9 }
            r5.<init>()     // Catch:{ all -> 0x08a9 }
            android.database.DatabaseUtils.cursorRowToContentValues(r9, r5)     // Catch:{ all -> 0x08a9 }
            java.lang.String r4 = "thread_key"
            java.lang.String r12 = r5.getAsString(r4)     // Catch:{ all -> 0x08a9 }
            java.lang.Object r6 = r11.get(r12)     // Catch:{ all -> 0x08a9 }
            byte[] r6 = (byte[]) r6     // Catch:{ all -> 0x08a9 }
            java.lang.String r4 = "encrypted_content"
            byte[] r5 = r5.getAsByteArray(r4)     // Catch:{ all -> 0x08a9 }
            if (r6 == 0) goto L_0x0240
            if (r5 == 0) goto L_0x0240
            X.0US r4 = r10.A01     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.Object r4 = r4.get()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.187 r4 = (X.AnonymousClass187) r4     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            byte[] r5 = r4.A05(r6, r5)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            if (r5 != 0) goto L_0x027a
            java.lang.Class r5 = X.C91394Xy.A04     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.String r4 = "Decrypted salamander is null."
            X.C010708t.A07(r5, r4)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            goto L_0x0240
        L_0x027a:
            com.facebook.messaging.model.threadkey.ThreadKey r14 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r12)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.0US r4 = r10.A02     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            r4.get()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.AwB r6 = X.C22030An2.A00(r5)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            if (r14 == 0) goto L_0x0240
            if (r6 == 0) goto L_0x0240
            X.AnN r5 = r6.type     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.AnN r4 = X.C22046AnN.ATTACHMENT_INFO_LIST     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            if (r5 != r4) goto L_0x0240
            X.Avy r4 = r6.body     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.util.List r4 = r4.A09()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.util.Iterator r16 = r4.iterator()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
        L_0x029b:
            boolean r4 = r16.hasNext()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            if (r4 == 0) goto L_0x0240
            java.lang.Object r4 = r16.next()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.AwN r4 = (X.AwN) r4     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.Long r4 = r4.download_fbid     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            long r4 = r4.longValue()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.String r15 = java.lang.Long.toString(r4)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.0US r4 = r10.A00     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.Object r4 = r4.get()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            android.content.Context r4 = (android.content.Context) r4     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            com.google.common.base.Preconditions.checkNotNull(r4)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            com.google.common.base.Preconditions.checkNotNull(r15)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.io.File r4 = X.AnonymousClass51R.A00(r4)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.io.File r5 = new java.io.File     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            r5.<init>(r4, r15)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.0US r4 = r10.A00     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.Object r4 = r4.get()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            android.content.Context r4 = (android.content.Context) r4     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.io.File r4 = X.AnonymousClass51R.A02(r4, r14, r15)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.AnonymousClass0q2.A05(r5, r4)     // Catch:{ IOException -> 0x02d8, 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed, 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            goto L_0x029b
        L_0x02d8:
            r13 = move-exception
            java.lang.Class r7 = X.C91394Xy.A04     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.String r6 = "Failed to move encrypted attachment with FBID %s from %s to %s"
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            java.lang.Object[] r4 = new java.lang.Object[]{r15, r5, r4}     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            X.C010708t.A0E(r7, r13, r6, r4)     // Catch:{ 1wY | 1wZ | IOException | IllegalStateException -> 0x02ed }
            goto L_0x029b
        L_0x02ed:
            r7 = move-exception
            java.lang.Class r6 = X.C91394Xy.A04     // Catch:{ all -> 0x08a9 }
            java.lang.Object[] r5 = new java.lang.Object[]{r12}     // Catch:{ all -> 0x08a9 }
            java.lang.String r4 = "Failed to move encrypted message attachments for thead %s"
            X.C010708t.A0E(r6, r7, r4, r5)     // Catch:{ all -> 0x08a9 }
            goto L_0x0240
        L_0x02fb:
            r9.close()
        L_0x02fe:
            r9 = 0
            X.0US r4 = r10.A00     // Catch:{ IOException -> 0x06b2 }
            java.lang.Object r4 = r4.get()     // Catch:{ IOException -> 0x06b2 }
            android.content.Context r4 = (android.content.Context) r4     // Catch:{ IOException -> 0x06b2 }
            java.io.File r13 = X.AnonymousClass51R.A00(r4)     // Catch:{ IOException -> 0x06b2 }
            if (r13 == 0) goto L_0x06bc
            java.io.File[] r4 = r13.listFiles()     // Catch:{ IOException -> 0x06b2 }
            if (r4 == 0) goto L_0x06bc
            java.io.File[] r4 = r13.listFiles()     // Catch:{ IOException -> 0x06b2 }
            int r4 = r4.length     // Catch:{ IOException -> 0x06b2 }
            if (r4 == 0) goto L_0x06bc
            java.io.File[] r11 = r13.listFiles()     // Catch:{ IOException -> 0x06b2 }
            int r10 = r11.length     // Catch:{ IOException -> 0x06b2 }
            r7 = 0
        L_0x0320:
            if (r7 >= r10) goto L_0x06bc
            r5 = r11[r7]     // Catch:{ IOException -> 0x06b2 }
            boolean r4 = r5.isDirectory()     // Catch:{ IOException -> 0x06b2 }
            if (r4 != 0) goto L_0x035b
            boolean r4 = r5.exists()     // Catch:{ IOException -> 0x06b2 }
            if (r4 != 0) goto L_0x0344
            java.lang.Class r12 = X.C91394Xy.A04     // Catch:{ IOException -> 0x06b2 }
            java.lang.String r6 = "Tried to delete file %s from root encrypted attachments directory %s but it does not exist"
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ IOException -> 0x06b2 }
            java.lang.String r4 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x06b2 }
            java.lang.Object[] r4 = new java.lang.Object[]{r5, r4}     // Catch:{ IOException -> 0x06b2 }
        L_0x0340:
            X.C010708t.A0B(r12, r6, r4)     // Catch:{ IOException -> 0x06b2 }
            goto L_0x035b
        L_0x0344:
            boolean r4 = r5.delete()     // Catch:{ IOException -> 0x06b2 }
            if (r4 != 0) goto L_0x035b
            java.lang.Class r12 = X.C91394Xy.A04     // Catch:{ IOException -> 0x06b2 }
            java.lang.String r6 = "Failed to delete file %s from root encrypted attachments directory %s"
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ IOException -> 0x06b2 }
            java.lang.String r4 = r13.getAbsolutePath()     // Catch:{ IOException -> 0x06b2 }
            java.lang.Object[] r4 = new java.lang.Object[]{r5, r4}     // Catch:{ IOException -> 0x06b2 }
            goto L_0x0340
        L_0x035b:
            int r7 = r7 + 1
            goto L_0x0320
        L_0x035e:
            java.lang.String r4 = "pending_sessions"
            java.lang.String r5 = X.AnonymousClass0W4.A00(r4)
            r4 = 1616834570(0x605ef00a, float:6.4257403E19)
            X.C007406x.A00(r4)
            r0.execSQL(r5)
            r4 = -600906364(0xffffffffdc2ee584, float:-1.96916004E17)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x0376:
            X.19e r4 = new X.19e
            r4.<init>()
            r4.A0A(r0)
            r4 = 1
            goto L_0x0030
        L_0x0381:
            X.18i r4 = new X.18i
            r4.<init>()
            r4.A0A(r0)
            r4 = 1
            goto L_0x0030
        L_0x038c:
            X.0US r4 = r7.A04
            java.lang.Object r4 = r4.get()
            X.4Mi r4 = (X.C88814Mi) r4
            X.0Tq r4 = r4.A00
            java.lang.Object r4 = r4.get()
            X.18F r4 = (X.AnonymousClass18F) r4
            byte[] r4 = X.AnonymousClass9UU.A01(r0, r4)
            if (r4 != 0) goto L_0x06bc
            r6 = 0
            java.lang.String r4 = "SELECT COUNT(*) FROM messages"
            android.database.Cursor r6 = r0.rawQuery(r4, r6)     // Catch:{ all -> 0x08c4 }
            boolean r4 = r6.moveToNext()     // Catch:{ all -> 0x08c4 }
            if (r4 == 0) goto L_0x03bd
            r5 = 0
            int r4 = r6.getInt(r5)     // Catch:{ all -> 0x08c4 }
            if (r4 <= 0) goto L_0x03b7
            r5 = 1
        L_0x03b7:
            if (r6 == 0) goto L_0x03c3
            r6.close()     // Catch:{ Exception -> 0x03c3 }
            goto L_0x03c3
        L_0x03bd:
            if (r6 == 0) goto L_0x03c2
            r6.close()     // Catch:{ Exception -> 0x03c2 }
        L_0x03c2:
            r5 = 1
        L_0x03c3:
            r4 = 0
            if (r5 == 0) goto L_0x0030
            goto L_0x06bc
        L_0x03c8:
            X.0US r4 = r7.A05
            java.lang.Object r9 = r4.get()
            X.4Xw r9 = (X.C91374Xw) r9
            X.0W6 r7 = X.AnonymousClass19Q.A03
            X.0W6 r6 = X.AnonymousClass19Q.A01
            java.lang.String r5 = "SELECT %s, %s FROM %s"
            java.lang.String r4 = "identity_keys"
            java.lang.String r5 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r5, r7, r6, r4)
            r4 = 0
            android.database.Cursor r7 = r0.rawQuery(r5, r4)
            int r4 = r7.getCount()     // Catch:{ all -> 0x08cb }
            if (r4 != 0) goto L_0x03ed
            r7.close()
            r4 = 1
            goto L_0x0030
        L_0x03ed:
            com.google.common.collect.ImmutableList r6 = X.AnonymousClass19O.A00     // Catch:{ all -> 0x08cb }
            java.lang.String r5 = "identity_keysTmp"
            r4 = 0
            java.lang.String r5 = X.AnonymousClass0W4.A03(r5, r6, r4)     // Catch:{ all -> 0x08cb }
            r4 = 903345249(0x35d7f461, float:1.6089872E-6)
            X.C007406x.A00(r4)     // Catch:{ all -> 0x08cb }
            r0.execSQL(r5)     // Catch:{ all -> 0x08cb }
            r4 = 1508205906(0x59e56552, float:8.0711466E15)
            X.C007406x.A00(r4)     // Catch:{ all -> 0x08cb }
        L_0x0405:
            boolean r4 = r7.moveToNext()     // Catch:{ all -> 0x08cb }
            if (r4 == 0) goto L_0x047f
            X.0W6 r4 = X.AnonymousClass19Q.A01     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            byte[] r6 = r4.A07(r7)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            X.0W6 r4 = X.AnonymousClass19Q.A03     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            java.lang.String r10 = r4.A05(r7)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            X.0Tq r4 = r9.A01     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            java.lang.Object r4 = r4.get()     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            X.18F r4 = (X.AnonymousClass18F) r4     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            byte[] r5 = X.AnonymousClass9UU.A01(r0, r4)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            if (r5 != 0) goto L_0x0426
            goto L_0x0433
        L_0x0426:
            X.0US r4 = r9.A00     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            java.lang.Object r4 = r4.get()     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            X.187 r4 = (X.AnonymousClass187) r4     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            byte[] r5 = r4.A06(r5, r6)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            goto L_0x0434
        L_0x0433:
            r5 = 0
        L_0x0434:
            if (r5 == 0) goto L_0x0405
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            r6.<init>()     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            X.0W6 r4 = X.AnonymousClass19Q.A03     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            java.lang.String r4 = r4.A00     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            r6.put(r4, r10)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            X.0W6 r4 = X.AnonymousClass19Q.A01     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            java.lang.String r4 = r4.A00     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            r6.put(r4, r5)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            r5 = 0
            r4 = 1793305144(0x6ae3aa38, float:1.37615E26)
            X.C007406x.A00(r4)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            java.lang.String r4 = "identity_keysTmp"
            r0.insert(r4, r5, r6)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            r4 = 745735488(0x2c730540, float:3.4535291E-12)
            X.C007406x.A00(r4)     // Catch:{ 1wY | 1wZ | IOException -> 0x045c }
            goto L_0x0405
        L_0x045c:
            r6 = move-exception
            java.lang.Class r5 = X.C91374Xw.A02     // Catch:{ all -> 0x08cb }
            java.lang.String r4 = "Failed to upgrade to v33"
            X.C010708t.A0A(r5, r4, r6)     // Catch:{ all -> 0x08cb }
            java.lang.String r4 = "identity_keysTmp"
            java.lang.String r5 = X.AnonymousClass0W4.A00(r4)     // Catch:{ all -> 0x08cb }
            r4 = -220307509(0xfffffffff2de5fcb, float:-8.809149E30)
            X.C007406x.A00(r4)     // Catch:{ all -> 0x08cb }
            r0.execSQL(r5)     // Catch:{ all -> 0x08cb }
            r4 = -2046555687(0xffffffff860409d9, float:-2.4833676E-35)
            X.C007406x.A00(r4)     // Catch:{ all -> 0x08cb }
            r7.close()
            r4 = 0
            goto L_0x0030
        L_0x047f:
            r7.close()
            java.lang.String r4 = "identity_keys"
            java.lang.String r5 = X.AnonymousClass0W4.A00(r4)
            r4 = 1592087881(0x5ee55549, float:8.2625973E18)
            X.C007406x.A00(r4)
            r0.execSQL(r5)
            r4 = -487640031(0xffffffffe2ef3421, float:-2.206264E21)
            X.C007406x.A00(r4)
            r4 = 1446105194(0x5631d06a, float:4.8877172E13)
            X.C007406x.A00(r4)
            java.lang.String r4 = "ALTER TABLE identity_keysTmp RENAME TO identity_keys"
            r0.execSQL(r4)
            r4 = -376450766(0xffffffffe98fd132, float:-2.1733036E25)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x04ab:
            java.lang.String r7 = "ALTER TABLE thread_devices ADD COLUMN "
            X.0W6 r4 = X.C194818i.A06
            java.lang.String r6 = r4.A00
            java.lang.String r5 = " "
            java.lang.String r4 = r4.A01
            java.lang.String r5 = X.AnonymousClass08S.A0S(r7, r6, r5, r4)
            r4 = 1649041963(0x624a622b, float:9.33329E20)
            X.C007406x.A00(r4)
            r0.execSQL(r5)
            r4 = -963583262(0xffffffffc690e2e2, float:-18545.441)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x04cb:
            java.lang.String r7 = "ALTER TABLE thread_devices ADD COLUMN "
            X.0W6 r4 = X.C194818i.A08
            java.lang.String r6 = r4.A00
            java.lang.String r5 = " "
            java.lang.String r4 = r4.A01
            java.lang.String r5 = X.AnonymousClass08S.A0S(r7, r6, r5, r4)
            r4 = 492205026(0x1d5673e2, float:2.8382574E-21)
            X.C007406x.A00(r4)
            r0.execSQL(r5)
            r4 = 603425312(0x23f78a20, float:2.6838292E-17)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x04eb:
            X.19i r4 = new X.19i
            r4.<init>()
            r4.A0A(r0)
            r4 = 1
            goto L_0x0030
        L_0x04f6:
            java.lang.String r7 = "ALTER TABLE thread_devices ADD COLUMN "
            X.0W6 r4 = X.C194818i.A07
            java.lang.String r6 = r4.A00
            java.lang.String r5 = " "
            java.lang.String r4 = r4.A01
            java.lang.String r5 = X.AnonymousClass08S.A0S(r7, r6, r5, r4)
            r4 = -395063084(0xffffffffe873d0d4, float:-4.605549E24)
            X.C007406x.A00(r4)
            r0.execSQL(r5)
            r4 = -165921977(0xfffffffff61c3b47, float:-7.921881E32)
            X.C007406x.A00(r4)
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            r6 = 0
            java.lang.String r11 = "thread_key"
            r5 = 1
            java.lang.String r10 = "other_user_device_id"
            r4 = 2
            java.lang.String r9 = "session_state"
            java.lang.String[] r11 = new java.lang.String[]{r11, r10, r9}
            java.lang.String r10 = "threads"
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            java.lang.String r16 = "last_read_timestamp_ms ASC"
            r17 = 0
            r9 = r0
            android.database.Cursor r11 = r9.query(r10, r11, r12, r13, r14, r15, r16, r17)
        L_0x0534:
            boolean r9 = r11.moveToNext()     // Catch:{ all -> 0x08d4 }
            if (r9 == 0) goto L_0x0584
            boolean r9 = r11.isNull(r6)     // Catch:{ all -> 0x08d4 }
            if (r9 != 0) goto L_0x0534
            boolean r9 = r11.isNull(r5)     // Catch:{ all -> 0x08d4 }
            if (r9 != 0) goto L_0x0534
            java.lang.String r13 = r11.getString(r6)     // Catch:{ all -> 0x08d4 }
            boolean r9 = r11.isNull(r4)     // Catch:{ all -> 0x08d4 }
            if (r9 == 0) goto L_0x057f
            byte[] r12 = X.C88824Mk.A01     // Catch:{ all -> 0x08d4 }
        L_0x0552:
            java.lang.String r9 = "::"
            int r15 = r13.indexOf(r9)     // Catch:{ all -> 0x08d4 }
            r14 = -1
            r9 = 0
            if (r15 == r14) goto L_0x055d
            r9 = 1
        L_0x055d:
            if (r9 == 0) goto L_0x0572
            android.util.Pair r14 = new android.util.Pair     // Catch:{ all -> 0x08d4 }
            java.lang.String r10 = r13.substring(r6, r15)     // Catch:{ all -> 0x08d4 }
            int r9 = r15 + 1
            java.lang.String r9 = r13.substring(r9)     // Catch:{ all -> 0x08d4 }
            r14.<init>(r10, r9)     // Catch:{ all -> 0x08d4 }
            r7.put(r14, r12)     // Catch:{ all -> 0x08d4 }
            goto L_0x0534
        L_0x0572:
            android.util.Pair r10 = new android.util.Pair     // Catch:{ all -> 0x08d4 }
            java.lang.String r9 = r11.getString(r5)     // Catch:{ all -> 0x08d4 }
            r10.<init>(r13, r9)     // Catch:{ all -> 0x08d4 }
            r7.put(r10, r12)     // Catch:{ all -> 0x08d4 }
            goto L_0x0534
        L_0x057f:
            byte[] r12 = r11.getBlob(r4)     // Catch:{ all -> 0x08d4 }
            goto L_0x0552
        L_0x0584:
            r11.close()
            java.util.Set r4 = r7.entrySet()
            java.util.Iterator r13 = r4.iterator()
        L_0x058f:
            boolean r4 = r13.hasNext()
            if (r4 == 0) goto L_0x0655
            java.lang.Object r12 = r13.next()
            java.util.Map$Entry r12 = (java.util.Map.Entry) r12
            java.lang.Object r4 = r12.getKey()
            android.util.Pair r4 = (android.util.Pair) r4
            java.lang.Object r11 = r4.first
            java.lang.String r11 = (java.lang.String) r11
            com.facebook.messaging.model.threadkey.ThreadKey r10 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r11)
            java.lang.Object r4 = r12.getKey()
            android.util.Pair r4 = (android.util.Pair) r4
            java.lang.Object r9 = r4.second
            java.lang.String r9 = (java.lang.String) r9
            X.0W6 r4 = X.C194818i.A0A
            java.lang.String r5 = r4.A00
            java.lang.String r4 = r10.A0J()
            X.0av r6 = X.C06160ax.A03(r5, r4)
            X.0W6 r4 = X.C194818i.A01
            java.lang.String r7 = r4.A00
            long r4 = r10.A01
            java.lang.String r4 = java.lang.Long.toString(r4)
            X.0av r5 = X.C06160ax.A03(r7, r4)
            X.0W6 r4 = X.C194818i.A05
            java.lang.String r4 = r4.A00
            X.0av r4 = X.C06160ax.A03(r4, r9)
            X.0av[] r4 = new X.C06140av[]{r6, r5, r4}
            X.1a6 r7 = X.C06160ax.A01(r4)
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.Object r5 = r12.getValue()
            byte[] r5 = (byte[]) r5
            int r4 = r5.length
            if (r4 != 0) goto L_0x064d
            X.0W6 r4 = X.C194818i.A07
            java.lang.String r4 = r4.A00
            r6.putNull(r4)
        L_0x05f2:
            java.lang.String r5 = r7.A02()
            java.lang.String[] r4 = r7.A04()
            java.lang.String r7 = "thread_devices"
            int r5 = r0.update(r7, r6, r5, r4)
            r4 = 1
            if (r5 == r4) goto L_0x058f
            if (r5 <= r4) goto L_0x0612
            java.lang.Class r6 = X.C88824Mk.A00
            java.lang.Object[] r5 = new java.lang.Object[]{r10, r9}
            java.lang.String r4 = "Multiple matches for %s %s"
            X.C010708t.A0C(r6, r4, r5)
            goto L_0x058f
        L_0x0612:
            X.0W6 r4 = X.C194818i.A0A
            java.lang.String r4 = r4.A00
            r6.put(r4, r11)
            X.0W6 r4 = X.C194818i.A01
            java.lang.String r11 = r4.A00
            long r4 = r10.A01
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r6.put(r11, r4)
            X.0W6 r4 = X.C194818i.A05
            java.lang.String r4 = r4.A00
            r6.put(r4, r9)
            r5 = 0
            r4 = 976389320(0x3a3284c8, float:6.809947E-4)
            X.C007406x.A00(r4)     // Catch:{ SQLException -> 0x063f }
            r0.insertOrThrow(r7, r5, r6)     // Catch:{ SQLException -> 0x063f }
            r4 = -1814035982(0xffffffff93e001f2, float:-5.6547468E-27)
            X.C007406x.A00(r4)     // Catch:{ SQLException -> 0x063f }
            goto L_0x058f
        L_0x063f:
            r7 = move-exception
            java.lang.Class r6 = X.C88824Mk.A00
            java.lang.Object[] r5 = new java.lang.Object[]{r10, r9}
            java.lang.String r4 = "Unable to create entry for %s %s"
            X.C010708t.A0F(r6, r7, r4, r5)
            goto L_0x058f
        L_0x064d:
            X.0W6 r4 = X.C194818i.A07
            java.lang.String r4 = r4.A00
            r6.put(r4, r5)
            goto L_0x05f2
        L_0x0655:
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            X.0W6 r4 = X.C194818i.A0A
            java.lang.String r4 = r4.A00
            r5 = 0
            java.lang.String[] r13 = new java.lang.String[]{r4}
            java.lang.String r12 = "threads"
            r14 = 0
            r15 = 0
            r16 = 0
            java.lang.String r18 = "last_read_timestamp_ms ASC"
            r19 = 0
            r11 = r0
            android.database.Cursor r11 = r11.query(r12, r13, r14, r15, r16, r17, r18, r19)
        L_0x0672:
            boolean r4 = r11.moveToNext()     // Catch:{ all -> 0x08d4 }
            if (r4 == 0) goto L_0x068d
            java.lang.String r10 = r11.getString(r5)     // Catch:{ all -> 0x08d4 }
            java.lang.String r4 = "::"
            int r9 = r10.indexOf(r4)     // Catch:{ all -> 0x08d4 }
            r7 = -1
            r4 = 0
            if (r9 == r7) goto L_0x0687
            r4 = 1
        L_0x0687:
            if (r4 == 0) goto L_0x0672
            r6.add(r10)     // Catch:{ all -> 0x08d4 }
            goto L_0x0672
        L_0x068d:
            r11.close()
            java.util.Iterator r6 = r6.iterator()
        L_0x0694:
            boolean r4 = r6.hasNext()
            if (r4 == 0) goto L_0x06bc
            java.lang.Object r5 = r6.next()
            java.lang.String r5 = (java.lang.String) r5
            java.lang.String r4 = "thread_key"
            X.0av r4 = X.C06160ax.A03(r4, r5)
            java.lang.String r5 = r4.A02()
            java.lang.String[] r4 = r4.A04()
            r0.delete(r12, r5, r4)
            goto L_0x0694
        L_0x06b2:
            r7 = move-exception
            java.lang.Class r6 = X.C91394Xy.A04
            java.lang.Object[] r5 = new java.lang.Object[r9]
            java.lang.String r4 = "Failed to delete remaining attachment files in root encrypted attachments directory"
            X.C010708t.A0E(r6, r7, r4, r5)
        L_0x06bc:
            r4 = 1
            goto L_0x0030
        L_0x06bf:
            java.lang.String r7 = "ALTER TABLE identity_keys ADD COLUMN "
            X.0W6 r4 = X.AnonymousClass19Q.A00
            java.lang.String r6 = r4.A00
            java.lang.String r5 = " "
            java.lang.String r4 = r4.A01
            java.lang.String r5 = X.AnonymousClass08S.A0S(r7, r6, r5, r4)
            r4 = 1897209761(0x71151fa1, float:7.3842406E29)
            X.C007406x.A00(r4)
            r0.execSQL(r5)
            r4 = 199496846(0xbe4148e, float:8.785333E-32)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x06df:
            X.1C3 r4 = new X.1C3
            r4.<init>()
            r4.A0A(r0)
            r4 = 1900366511(0x71454aaf, float:9.7694134E29)
            X.C007406x.A00(r4)
            java.lang.String r4 = "INSERT INTO thread_keys SELECT thread_key, encryption_key FROM threads"
            r0.execSQL(r4)
            r4 = 739769702(0x2c17fd66, float:2.1599055E-12)
            X.C007406x.A00(r4)
            r4 = 1
            goto L_0x0030
        L_0x06fb:
            X.1iN r4 = new X.1iN
            r4.<init>()
            r4.A0A(r0)
            r4 = 1
            goto L_0x0030
        L_0x0706:
            java.lang.String r7 = " "
            java.lang.String r6 = "ALTER TABLE retry_status ADD COLUMN "
            X.0W6 r4 = X.C30621iN.A03     // Catch:{ SQLiteException -> 0x0723 }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x0723 }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x0723 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x0723 }
            r4 = 1989213118(0x7690fbbe, float:1.470306E33)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0723 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x0723 }
            r4 = -864210702(0xffffffffcc7d30f2, float:-6.6372552E7)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0723 }
        L_0x0723:
            X.0W6 r4 = X.C30621iN.A01     // Catch:{ SQLiteException -> 0x073c }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x073c }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x073c }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x073c }
            r4 = 132401210(0x7e4483a, float:3.4348078E-34)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x073c }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x073c }
            r4 = 1879269858(0x700361e2, float:1.6264348E29)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x073c }
        L_0x073c:
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            X.0W6 r4 = X.C30621iN.A03
            java.lang.String r5 = r4.A00
            r4 = 3
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r6.put(r5, r4)
            r5 = 0
            java.lang.String r4 = "retry_status"
            goto L_0x0787
        L_0x0751:
            X.06B r4 = r7.A00
            long r9 = r4.now()
            java.lang.String r7 = "ALTER TABLE thread_devices ADD COLUMN "
            X.0W6 r4 = X.C194818i.A00     // Catch:{ SQLiteException -> 0x0774 }
            java.lang.String r6 = r4.A00     // Catch:{ SQLiteException -> 0x0774 }
            java.lang.String r5 = " "
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x0774 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r7, r6, r5, r4)     // Catch:{ SQLiteException -> 0x0774 }
            r4 = -866721478(0xffffffffcc56e13a, float:-5.6329448E7)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0774 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x0774 }
            r4 = 1872370030(0x6f9a196e, float:9.538287E28)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0774 }
        L_0x0774:
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            X.0W6 r4 = X.C194818i.A00
            java.lang.String r5 = r4.A00
            java.lang.Long r4 = java.lang.Long.valueOf(r9)
            r6.put(r5, r4)
            r5 = 0
            java.lang.String r4 = "thread_devices"
        L_0x0787:
            r0.update(r4, r6, r5, r5)
            r4 = 1
            goto L_0x0030
        L_0x078d:
            X.06B r4 = r7.A00
            r4.now()
            java.lang.String r7 = "ALTER TABLE threads ADD COLUMN "
            X.0W6 r4 = X.C193717w.A07     // Catch:{ SQLiteException -> 0x08a6 }
            java.lang.String r6 = r4.A00     // Catch:{ SQLiteException -> 0x08a6 }
            java.lang.String r5 = " "
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x08a6 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r7, r6, r5, r4)     // Catch:{ SQLiteException -> 0x08a6 }
            r4 = 498886943(0x1dbc691f, float:4.987188E-21)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x08a6 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x08a6 }
            r4 = -2053348539(0xffffffff859c6345, float:-1.4706635E-35)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x08a6 }
            goto L_0x08a6
        L_0x07b1:
            java.lang.String r7 = " "
            java.lang.String r6 = "ALTER TABLE identity_keys ADD COLUMN "
            X.0W6 r4 = X.AnonymousClass19Q.A02     // Catch:{ SQLiteException -> 0x07ce }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x07ce }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x07ce }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x07ce }
            r4 = 370615321(0x16172419, float:1.2209073E-25)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x07ce }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x07ce }
            r4 = 1025149058(0x3d1a8882, float:0.03772784)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x07ce }
        L_0x07ce:
            java.lang.String r6 = "ALTER TABLE properties ADD COLUMN "
            X.0W6 r4 = X.AnonymousClass19E.A02     // Catch:{ SQLiteException -> 0x07e9 }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x07e9 }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x07e9 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x07e9 }
            r4 = 218849562(0xd0b611a, float:4.2949564E-31)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x07e9 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x07e9 }
            r4 = -601770872(0xffffffffdc21b488, float:-1.8206387E17)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x07e9 }
        L_0x07e9:
            java.lang.String r6 = "ALTER TABLE pre_keys ADD COLUMN "
            X.0W6 r4 = X.AnonymousClass19X.A03     // Catch:{ SQLiteException -> 0x0804 }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x0804 }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x0804 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x0804 }
            r4 = -294089229(0xffffffffee788df3, float:-1.9230972E28)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0804 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x0804 }
            r4 = 1344995514(0x502b00ba, float:1.14758062E10)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0804 }
        L_0x0804:
            java.lang.String r6 = "ALTER TABLE signed_pre_keys ADD COLUMN "
            X.0W6 r4 = X.C196419c.A03     // Catch:{ SQLiteException -> 0x081f }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x081f }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x081f }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x081f }
            r4 = -823652010(0xffffffffcee81156, float:-1.94672512E9)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x081f }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x081f }
            r4 = 638116190(0x2608e15e, float:4.748991E-16)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x081f }
        L_0x081f:
            java.lang.String r6 = "ALTER TABLE other_devices ADD COLUMN "
            X.0W6 r4 = X.C197619o.A01     // Catch:{ SQLiteException -> 0x083a }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x083a }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x083a }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x083a }
            r4 = 504657070(0x1e1474ae, float:7.8591836E-21)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x083a }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x083a }
            r4 = -2058642560(0xffffffff854b9b80, float:-9.573575E-36)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x083a }
        L_0x083a:
            java.lang.String r6 = "ALTER TABLE thread_keys ADD COLUMN "
            X.0W6 r4 = X.C30611iM.A01     // Catch:{ SQLiteException -> 0x0855 }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x0855 }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x0855 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x0855 }
            r4 = 1077827456(0x403e5780, float:2.9740906)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0855 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x0855 }
            r4 = 653628104(0x26f592c8, float:1.7040075E-15)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0855 }
        L_0x0855:
            java.lang.String r6 = "ALTER TABLE threads ADD COLUMN "
            X.0W6 r4 = X.C193717w.A0E     // Catch:{ SQLiteException -> 0x0870 }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x0870 }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x0870 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x0870 }
            r4 = 971041422(0x39e0ea8e, float:4.2899366E-4)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0870 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x0870 }
            r4 = 1565691940(0x5d529024, float:9.4829167E17)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x0870 }
        L_0x0870:
            java.lang.String r6 = "ALTER TABLE thread_devices ADD COLUMN "
            X.0W6 r4 = X.C194818i.A09     // Catch:{ SQLiteException -> 0x088b }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x088b }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x088b }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x088b }
            r4 = -1678831063(0xffffffff9bef1229, float:-3.955097E-22)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x088b }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x088b }
            r4 = 233938281(0xdf19d69, float:1.4890667E-30)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x088b }
        L_0x088b:
            java.lang.String r6 = "ALTER TABLE messages ADD COLUMN "
            X.0W6 r4 = X.C195818v.A0D     // Catch:{ SQLiteException -> 0x08a6 }
            java.lang.String r5 = r4.A00     // Catch:{ SQLiteException -> 0x08a6 }
            java.lang.String r4 = r4.A01     // Catch:{ SQLiteException -> 0x08a6 }
            java.lang.String r5 = X.AnonymousClass08S.A0S(r6, r5, r7, r4)     // Catch:{ SQLiteException -> 0x08a6 }
            r4 = -1086225527(0xffffffffbf418389, float:-0.7559133)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x08a6 }
            r0.execSQL(r5)     // Catch:{ SQLiteException -> 0x08a6 }
            r4 = 1843516724(0x6de1d534, float:8.736484E27)
            X.C007406x.A00(r4)     // Catch:{ SQLiteException -> 0x08a6 }
        L_0x08a6:
            r4 = 1
            goto L_0x0030
        L_0x08a9:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x08ab }
        L_0x08ab:
            r0 = move-exception
            if (r9 == 0) goto L_0x08d3
            r9.close()     // Catch:{ all -> 0x08d3 }
            goto L_0x08d3
        L_0x08b2:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x08bb }
            java.lang.String r0 = "Failed to decrypt thread encryption key"
            r1.<init>(r0, r2)     // Catch:{ all -> 0x08bb }
            throw r1     // Catch:{ all -> 0x08bb }
        L_0x08bb:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x08bd }
        L_0x08bd:
            r0 = move-exception
            if (r6 == 0) goto L_0x08d3
            r6.close()     // Catch:{ all -> 0x08d3 }
            goto L_0x08d3
        L_0x08c4:
            r0 = move-exception
            if (r6 == 0) goto L_0x08d3
            r6.close()     // Catch:{  }
            goto L_0x08d3
        L_0x08cb:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x08cd }
        L_0x08cd:
            r0 = move-exception
            if (r7 == 0) goto L_0x08d3
            r7.close()     // Catch:{ all -> 0x08d3 }
        L_0x08d3:
            throw r0
        L_0x08d4:
            r0 = move-exception
            r11.close()
            throw r0
        L_0x08d9:
            r2.A01(r0)
            r2.A04(r0)
            r0 = 31
            if (r1 == r0) goto L_0x08fa
            X.0US r0 = r2.A00
            java.lang.Object r2 = r0.get()
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.String r0 = "Database not upgraded incrementally from %s to %s"
            java.lang.String r1 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1, r3)
            java.lang.String r0 = "tincan_upgrade"
            r2.CGS(r0, r1)
        L_0x08fa:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19C.A08(android.database.sqlite.SQLiteDatabase, int, int):void");
    }
}
