package X;

import android.os.Build;

/* renamed from: X.08T  reason: invalid class name */
public final class AnonymousClass08T {
    public static int A00(Throwable th) {
        if (Build.VERSION.SDK_INT < 21) {
            return AnonymousClass0MW.A00(th);
        }
        return C02130Db.A00(th);
    }

    public static String A01(int i) {
        if (Build.VERSION.SDK_INT < 21) {
            return AnonymousClass0MW.A01(i);
        }
        return C02130Db.A01(i);
    }
}
