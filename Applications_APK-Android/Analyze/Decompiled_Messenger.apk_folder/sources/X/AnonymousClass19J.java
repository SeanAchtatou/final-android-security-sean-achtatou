package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.19J  reason: invalid class name */
public final class AnonymousClass19J extends AnonymousClass0W4 {
    private static final C06060am A00 = new C06050al(ImmutableList.of(C195818v.A05));
    private static final ImmutableList A01;
    private static final String A02;
    private static final String A03;

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    static {
        AnonymousClass0W6 r5 = C195818v.A05;
        AnonymousClass0W6 r6 = C195818v.A0E;
        AnonymousClass0W6 r7 = C195818v.A02;
        AnonymousClass0W6 r8 = C195818v.A04;
        AnonymousClass0W6 r9 = C195818v.A09;
        AnonymousClass0W6 r10 = C195818v.A0F;
        AnonymousClass0W6 r11 = C195818v.A0G;
        AnonymousClass0W6 r12 = C195818v.A00;
        AnonymousClass0W6 r13 = C195818v.A06;
        AnonymousClass0W6 r14 = C195818v.A07;
        A01 = ImmutableList.of(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, C195818v.A01, C195818v.A08, C195818v.A0A, C195818v.A0B, C195818v.A0C, C195818v.A03, C195818v.A0D);
        String str = r6.A00;
        A03 = AnonymousClass0W4.A06("messages", "messages_timestamp_index", ImmutableList.of(str, r10 + " DESC"));
        A02 = AnonymousClass0W4.A05("messages", "messages_offline_threading_id_index", ImmutableList.of(r14));
    }

    public AnonymousClass19J() {
        super("messages", A01, A00);
    }

    public void A09(SQLiteDatabase sQLiteDatabase) {
        super.A09(sQLiteDatabase);
        String str = A03;
        C007406x.A00(1863609696);
        sQLiteDatabase.execSQL(str);
        C007406x.A00(-359866965);
        String str2 = A02;
        C007406x.A00(-751158);
        sQLiteDatabase.execSQL(str2);
        C007406x.A00(171613248);
    }
}
