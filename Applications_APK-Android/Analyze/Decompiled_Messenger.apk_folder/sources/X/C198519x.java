package X;

/* renamed from: X.19x  reason: invalid class name and case insensitive filesystem */
public final class C198519x implements C198619y {
    public final /* synthetic */ AnonymousClass1Ri A00;

    public C198519x(AnonymousClass1Ri r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a7, code lost:
        if (r9 > r2) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d9, code lost:
        if (r9 > r2) goto L_0x00db;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void CLg(int r18, int r19, int r20, int r21, int r22) {
        /*
            r17 = this;
            r2 = r17
            X.1Ri r1 = r2.A00
            r12 = r18
            r1.A01 = r12
            r11 = r19
            r1.A02 = r11
            X.1A7 r0 = r1.A0a
            r3 = 0
            r0.A02 = r3
            X.AnonymousClass1Ri.A0I(r1)
            X.1Ri r10 = r2.A00
            int r1 = r10.A04
            r0 = -1
            if (r1 == r0) goto L_0x0106
            if (r12 == r0) goto L_0x0106
            if (r11 == r0) goto L_0x0106
            int r0 = r19 - r18
            int r2 = java.lang.Math.max(r1, r0)
            float r1 = (float) r2
            float r0 = r10.A0Q
            float r1 = r1 * r0
            int r1 = (int) r1
            int r0 = r18 - r1
            int r9 = java.lang.Math.max(r3, r0)
            int r2 = r2 + r18
            int r2 = r2 + r1
            java.util.List r0 = r10.A0h
            int r0 = r0.size()
            int r0 = r0 + -1
            int r8 = java.lang.Math.min(r2, r0)
        L_0x003f:
            if (r9 > r8) goto L_0x0106
            java.util.List r0 = r10.A0h
            java.lang.Object r7 = r0.get(r9)
            X.1IK r7 = (X.AnonymousClass1IK) r7
            monitor-enter(r7)
            com.facebook.litho.ComponentTree r6 = r7.A01     // Catch:{ all -> 0x0103 }
            if (r6 == 0) goto L_0x00fb
            monitor-enter(r6)     // Catch:{ all -> 0x0103 }
            boolean r0 = com.facebook.litho.ComponentTree.A0C(r6)     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x0058
            X.15v r0 = r6.A09     // Catch:{ all -> 0x0100 }
            goto L_0x005a
        L_0x0058:
            X.15v r0 = r6.A08     // Catch:{ all -> 0x0100 }
        L_0x005a:
            if (r0 == 0) goto L_0x00fa
            X.1Iv r13 = r6.A0W     // Catch:{ all -> 0x0100 }
            X.38Y r5 = r0.A0I     // Catch:{ all -> 0x0100 }
            if (r5 == 0) goto L_0x00fa
            java.util.Map r0 = r5.A00     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x00fa
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x0100 }
            java.util.Iterator r16 = r0.iterator()     // Catch:{ all -> 0x0100 }
        L_0x006e:
            boolean r0 = r16.hasNext()     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x00fa
            java.lang.Object r1 = r16.next()     // Catch:{ all -> 0x0100 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0100 }
            java.util.Map r0 = r5.A00     // Catch:{ all -> 0x0100 }
            java.lang.Object r4 = r0.get(r1)     // Catch:{ all -> 0x0100 }
            X.38b r4 = (X.AnonymousClass38b) r4     // Catch:{ all -> 0x0100 }
            java.util.List r0 = r4.A02     // Catch:{ all -> 0x0100 }
            java.util.Iterator r15 = r0.iterator()     // Catch:{ all -> 0x0100 }
        L_0x0088:
            boolean r0 = r15.hasNext()     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x006e
            java.lang.Object r1 = r15.next()     // Catch:{ all -> 0x0100 }
            X.0zR r1 = (X.C17770zR) r1     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = r4.A01     // Catch:{ all -> 0x0100 }
            boolean r0 = r13.A00(r0, r1)     // Catch:{ all -> 0x0100 }
            if (r0 != 0) goto L_0x00c6
            X.EvT r0 = r4.A00     // Catch:{ all -> 0x0100 }
            int r2 = r0.A00     // Catch:{ all -> 0x0100 }
            int r0 = r18 - r2
            int r2 = r19 + r2
            if (r9 < r0) goto L_0x00a9
            r0 = 1
            if (r9 <= r2) goto L_0x00aa
        L_0x00a9:
            r0 = 0
        L_0x00aa:
            if (r0 == 0) goto L_0x00c6
            java.lang.String r0 = r4.A01     // Catch:{ all -> 0x0100 }
            r1.A0p(r0)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r4.A01     // Catch:{ all -> 0x0100 }
            r14 = 1
            java.lang.String r1 = r1.A06     // Catch:{ all -> 0x0100 }
            java.util.Map r2 = r13.A00     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = "_"
            java.lang.String r1 = X.AnonymousClass08S.A0P(r3, r0, r1)     // Catch:{ all -> 0x0100 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)     // Catch:{ all -> 0x0100 }
            r2.put(r1, r0)     // Catch:{ all -> 0x0100 }
            goto L_0x0088
        L_0x00c6:
            java.lang.String r0 = r4.A01     // Catch:{ all -> 0x0100 }
            boolean r0 = r13.A00(r0, r1)     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x0088
            X.EvT r0 = r4.A00     // Catch:{ all -> 0x0100 }
            int r2 = r0.A00     // Catch:{ all -> 0x0100 }
            int r0 = r18 - r2
            int r2 = r19 + r2
            if (r9 < r0) goto L_0x00db
            r0 = 1
            if (r9 <= r2) goto L_0x00dc
        L_0x00db:
            r0 = 0
        L_0x00dc:
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0088
            java.lang.String r0 = r4.A01     // Catch:{ all -> 0x0100 }
            r1.A0q(r0)     // Catch:{ all -> 0x0100 }
            java.lang.String r14 = r4.A01     // Catch:{ all -> 0x0100 }
            r3 = 2
            java.lang.String r1 = r1.A06     // Catch:{ all -> 0x0100 }
            java.util.Map r2 = r13.A00     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = "_"
            java.lang.String r1 = X.AnonymousClass08S.A0P(r14, r0, r1)     // Catch:{ all -> 0x0100 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0100 }
            r2.put(r1, r0)     // Catch:{ all -> 0x0100 }
            goto L_0x0088
        L_0x00fa:
            monitor-exit(r6)     // Catch:{ all -> 0x0103 }
        L_0x00fb:
            monitor-exit(r7)
            int r9 = r9 + 1
            goto L_0x003f
        L_0x0100:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0103 }
            throw r0     // Catch:{ all -> 0x0103 }
        L_0x0103:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0106:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C198519x.CLg(int, int, int, int, int):void");
    }
}
