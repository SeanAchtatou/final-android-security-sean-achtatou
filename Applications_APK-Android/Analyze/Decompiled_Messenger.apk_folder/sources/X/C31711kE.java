package X;

import android.os.Process;

/* renamed from: X.1kE  reason: invalid class name and case insensitive filesystem */
public final class C31711kE implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.http.executors.liger.nodi.HTTPThreadFactory$ThreadPriorityRunnable";
    private int A00;
    private final Runnable A01;

    public void run() {
        Process.setThreadPriority(this.A00);
        this.A01.run();
    }

    public C31711kE(Runnable runnable, int i) {
        this.A01 = runnable;
        this.A00 = i;
    }
}
