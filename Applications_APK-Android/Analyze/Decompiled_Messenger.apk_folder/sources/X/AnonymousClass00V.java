package X;

import com.facebook.common.dextricks.DexStore;

/* renamed from: X.00V  reason: invalid class name */
public final class AnonymousClass00V {
    public static AnonymousClass00X A00;
    private static final int[] A01 = {DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED};

    public static String A00(String str) {
        String[] strArr = {null};
        int[] iArr = A01;
        AnonymousClass00X r1 = A00;
        if (r1 != null) {
            r1.Bzf(str, iArr, strArr, null, null);
        }
        return strArr[0];
    }

    public static boolean A01(String str, int[] iArr, long[] jArr) {
        AnonymousClass00X r0 = A00;
        if (r0 == null) {
            return false;
        }
        return r0.Bzf(str, iArr, null, jArr, null);
    }

    static {
        AnonymousClass00X A002 = AnonymousClass00W.A00();
        if (A002 == null) {
            A002 = new C04120Sa();
        }
        A00 = A002;
    }

    public static boolean A02(String str, String[] strArr, long[] jArr) {
        return A00.Bzg(str, strArr, jArr);
    }
}
