package X;

import com.facebook.analytics.AnalyticsClientModule;
import com.facebook.inject.InjectorModule;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.quicklog.QuickPerformanceLoggerProvider;
import com.facebook.quicklog.xplat.QPLXplatInitializerImpl;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

@InjectorModule(requireModules = {C21245A3e.class})
/* renamed from: X.0ZD  reason: invalid class name */
public final class AnonymousClass0ZD extends AnonymousClass0UV {
    private static volatile C46942Sn A00;
    private static volatile C46972Ss A01;
    private static volatile C25401Zm A02;
    private static volatile QuickPerformanceLogger A03;
    private static volatile C12210op A04;
    private static volatile C25841aU A05;
    private static volatile C08150el A06;
    private static volatile AnonymousClass1J8 A07;
    private static volatile AnonymousClass0YL A08;
    private static volatile C06510bc A09;
    private static volatile AnonymousClass0ZE A0A;

    public static final C46942Sn A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C46942Sn.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass2So();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C46972Ss A01(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C46972Ss.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C46982St();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C25401Zm A02(AnonymousClass1XY r6) {
        C25401Zm r0;
        if (A02 == null) {
            synchronized (C25401Zm.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        C06510bc A0B = A0B(applicationInjector);
                        C12210op A052 = A05(applicationInjector);
                        AnonymousClass0UQ A003 = AnonymousClass0UQ.A00(AnonymousClass1Y3.AOJ, applicationInjector);
                        if (A052.CFF()) {
                            r0 = new EZW(A0B, A003);
                        } else {
                            r0 = A0B;
                        }
                        A02 = r0;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final QuickPerformanceLogger A04(AnonymousClass1XY r41) {
        ExecutorService executorService;
        C39851zi r24;
        C29543Ecw ecw;
        if (A03 == null) {
            synchronized (QuickPerformanceLogger.class) {
                AnonymousClass1XY r1 = r41;
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r1);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r1.getApplicationInjector();
                        AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.AWf, applicationInjector);
                        AnonymousClass09O A062 = AnonymousClass067.A06(applicationInjector);
                        AnonymousClass06B A022 = AnonymousClass067.A02();
                        AnonymousClass0Ud A004 = AnonymousClass0Ud.A00(applicationInjector);
                        AnonymousClass0VB A005 = AnonymousClass0VB.A00(AnonymousClass1Y3.B55, applicationInjector);
                        AnonymousClass0UQ A006 = AnonymousClass0UQ.A00(AnonymousClass1Y3.BSk, applicationInjector);
                        AnonymousClass0Zt A007 = AnonymousClass0Zt.A00(applicationInjector);
                        ScheduledExecutorService A0j = AnonymousClass0UX.A0j(applicationInjector);
                        C04460Ut A023 = C04430Uq.A02(applicationInjector);
                        AnonymousClass0X5<C26121aw> r8 = new AnonymousClass0X5<>(applicationInjector, AnonymousClass0X6.A2X);
                        AnonymousClass0X5 r2 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A2Y);
                        AnonymousClass0X5 r3 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A2V);
                        AnonymousClass0X5 r12 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A2W);
                        C12210op A052 = A05(applicationInjector);
                        C25401Zm A024 = A02(applicationInjector);
                        AnonymousClass09P A012 = C04750Wa.A01(applicationInjector);
                        C12230or A008 = C12230or.A00(applicationInjector);
                        C25811aR.A00(applicationInjector);
                        AnonymousClass1ZJ A009 = AnonymousClass1ZI.A00(applicationInjector);
                        boolean CFD = A052.CFD();
                        AnonymousClass0Td[] r22 = (AnonymousClass0Td[]) r2.toArray(new AnonymousClass0Td[r2.size()]);
                        C08230et[] r32 = (C08230et[]) r3.toArray(new C08230et[r3.size()]);
                        if (A052.CEU()) {
                            executorService = (ExecutorService) A006.get();
                        } else {
                            executorService = (ExecutorService) A005.get();
                        }
                        C08490fR r7 = new C08490fR(executorService, A0j);
                        C08500fS r0 = new C08500fS(A012);
                        if (A052.BFG()) {
                            AnonymousClass09O r25 = A062;
                            C25401Zm r27 = A024;
                            r24 = new C39851zi(r25, A052.Aog(), r27, new Random(), new C39861zj(r7, A003, A024, A022, A062, new Random(), r0, r32));
                        } else {
                            r24 = null;
                        }
                        C08230et[] r35 = r32;
                        C08520fU r252 = new C08520fU(A003, A024, A062, A022, A007, new C08530fV(A004), r7, r22, (C26121aw[]) r8.toArray(new C26121aw[r8.size()]), r35, (C08560fY[]) r12.toArray(new C08560fY[r12.size()]), A052, new C08510fT(), r24, CFD, A009);
                        if (CFD) {
                            ecw = new C29543Ecw(r252, A024, r35, r24, A052, new C29546Ecz(r252, A052.B3f(), r24, A052.C5V(), A052.BIl()));
                        }
                        QuickPerformanceLoggerProvider.mQuickPerformanceLogger = ecw;
                        if (A008.BA9() != null) {
                            if (QuickPerformanceLoggerProvider.getQPLInstance() != null) {
                                QPLXplatInitializerImpl.setupNativeQPLWithXAnalyticsHolder(A008.BA9());
                            } else {
                                throw new AnonymousClass4X1("Call QuickPerformanceLoggerProvider.setQuickPerformanceLogger(qplInstance) before calling QPLXplatInitializer.initialize(xAnalytixAnalyticsProvider)");
                            }
                        }
                        for (C26121aw r23 : r8) {
                            if (r23 instanceof C08150el) {
                                ((C08150el) r23).A00 = ecw;
                            }
                        }
                        C06600bl BMm = A023.BMm();
                        BMm.A02("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP", new C09630iD(ecw));
                        BMm.A00().A00();
                        C09600i8.A00 = ecw;
                        A03 = ecw;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C12210op A05(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (C12210op.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A04 = new C12220oq(AnonymousClass0UQ.A00(AnonymousClass1Y3.AcD, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AOJ, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AWK, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.BMV, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final C25841aU A07(AnonymousClass1XY r6) {
        if (A05 == null) {
            synchronized (C25841aU.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass0X5 r3 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A2Z);
                        new C25861aW();
                        A05 = new C25841aU(r3, A05(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.Abi, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final C08150el A08(AnonymousClass1XY r3) {
        if (A06 == null) {
            synchronized (C08150el.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r3);
                if (A002 != null) {
                    try {
                        A06 = C08150el.create(AnonymousClass0Zt.A00(r3.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static final AnonymousClass1J8 A09(AnonymousClass1XY r5) {
        if (A07 == null) {
            synchronized (AnonymousClass1J8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A07 = new AnonymousClass1J8(AnonymousClass1ZF.A02(applicationInjector), A05(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static final AnonymousClass0YL A0A(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (AnonymousClass0YL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new AnonymousClass0YL(AnonymousClass1YA.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public static final C06510bc A0B(AnonymousClass1XY r10) {
        if (A09 == null) {
            synchronized (C06510bc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r10);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r10.getApplicationInjector();
                        ExecutorService A0Z = AnonymousClass0UX.A0Z(applicationInjector);
                        AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.ACa, applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.BId, applicationInjector);
                        AnonymousClass0VB A005 = AnonymousClass0VB.A00(AnonymousClass1Y3.Anq, applicationInjector);
                        AnonymousClass0UQ A006 = AnonymousClass0UQ.A00(AnonymousClass1Y3.ArW, applicationInjector);
                        A09 = new C06510bc(A0Z, A003, A004, A005, new C07670dw(A006), AnonymousClass0W9.A01());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static final AnonymousClass0ZE A0C(AnonymousClass1XY r6) {
        if (A0A == null) {
            synchronized (AnonymousClass0ZE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0A = new AnonymousClass0ZE(AnonymousClass0UQ.A00(AnonymousClass1Y3.AmL, applicationInjector), A0B(applicationInjector), C04750Wa.A01(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static final C08180eo A0D() {
        return new C08180eo();
    }

    public static final QuickPerformanceLogger A03(AnonymousClass1XY r0) {
        return A04(r0);
    }

    public static final C72993fK A06(AnonymousClass1XY r4) {
        return new C72993fK(A07(r4), AnalyticsClientModule.A04(r4), C04750Wa.A01(r4));
    }
}
