package X;

import android.text.TextUtils;
import com.facebook.acra.LogCatCollector;
import com.facebook.proxygen.MQTTClientCallback;
import com.facebook.proxygen.MQTTClientError;
import io.card.payment.BuildConfig;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1sb  reason: invalid class name and case insensitive filesystem */
public final class C36171sb implements MQTTClientCallback {
    public AtomicBoolean A00 = new AtomicBoolean(false);
    public final /* synthetic */ C36151sZ A01;

    public void onConnectSuccess(byte[] bArr) {
        AnonymousClass0C5 r4;
        try {
            AnonymousClass0CQ A002 = AnonymousClass0CQ.A00(new String(bArr, LogCatCollector.UTF_8_ENCODING));
            this.A01.A00.A05(A002.A05);
            if (TextUtils.isEmpty(A002.A03) || TextUtils.isEmpty(A002.A04)) {
                r4 = AnonymousClass0C5.A01;
            } else {
                r4 = new AnonymousClass0C5(A002.A03, A002.A04, System.currentTimeMillis());
            }
            if (!this.A00.compareAndSet(false, true)) {
                this.A01.A00.A07(new Throwable(), "Whisle Client Error", "onConnectSuccess being called twice");
            }
            String str = A002.A01;
            if (str == null) {
                str = BuildConfig.FLAVOR;
            }
            String str2 = A002.A02;
            if (str2 == null) {
                str2 = BuildConfig.FLAVOR;
            }
            AnonymousClass0CR r2 = new AnonymousClass0CR(C01830Bu.A00(str, str2), r4);
            C36151sZ r1 = this.A01;
            r1.A01 = r1.A02;
            r1.A00.A03(AnonymousClass089.CONNECTED);
            this.A01.A00.A00();
            this.A01.A00.A02(r2);
        } catch (UnsupportedEncodingException e) {
            Arrays.toString(bArr);
            this.A01.A00.A02(new AnonymousClass0CR(AnonymousClass0SB.A01, e));
        }
    }

    public C36171sb(C36151sZ r3) {
        this.A01 = r3;
    }

    public void onConnectFailure(MQTTClientError mQTTClientError) {
        AnonymousClass0SB r0;
        this.A01.ASP();
        byte b = (byte) mQTTClientError.mConnAckCode;
        if (b == 4) {
            r0 = AnonymousClass0SB.A03;
        } else if (b == 5) {
            r0 = AnonymousClass0SB.A04;
        } else if (b == 17) {
            r0 = AnonymousClass0SB.A05;
        } else if (b != 19) {
            r0 = AnonymousClass0SB.A02;
        } else {
            r0 = AnonymousClass0SB.A06;
        }
        this.A01.A00.A02(new AnonymousClass0CR(r0, b));
        this.A01.A01 = null;
    }

    public void onConnectSent() {
        AnonymousClass0CB r1 = this.A01.A00;
        if (r1.A00.A0K) {
            r1.A03(AnonymousClass089.CONNECT_SENT);
            if (r1.A00.A0E.A0S) {
                r1.A03(AnonymousClass089.CONNECTED);
                r1.A00();
            }
            AnonymousClass0CC r3 = r1.A00.A0Y;
            if (r3 != null) {
                AnonymousClass00S.A04(r3.A02.A05, new AnonymousClass0CP(r3), -51077260);
            }
        }
    }

    public void onError(MQTTClientError mQTTClientError) {
        AnonymousClass0CB r2;
        AnonymousClass0CE A002;
        C010708t.A0P("WhistleClientCore", "onError=%s", mQTTClientError);
        C36151sZ r22 = this.A01;
        Throwable th = null;
        r22.A01 = null;
        MQTTClientError.MQTTErrorType mQTTErrorType = mQTTClientError.mErrType;
        if (mQTTErrorType.equals(MQTTClientError.MQTTErrorType.DISCONNECT)) {
            r2 = r22.A00;
            A002 = AnonymousClass0CE.A03;
        } else if (mQTTErrorType.equals(MQTTClientError.MQTTErrorType.STOPPED_BEFORE_MQTT_CONNECT)) {
            r2 = r22.A00;
            A002 = AnonymousClass0CE.A01;
        } else {
            th = C36151sZ.A01(mQTTClientError);
            r2 = this.A01.A00;
            A002 = AnonymousClass0CE.A00(th);
        }
        r2.A01(A002, AnonymousClass0CG.A02, th);
    }

    public void onPingRequest() {
        this.A01.A00.A04(C36151sZ.A00(AnonymousClass0CL.A05, 0));
    }

    public void onPingRequestSent() {
        this.A01.A00.A06(AnonymousClass0CL.A05.name(), BuildConfig.FLAVOR);
    }

    public void onPingResponse() {
        this.A01.A00.A04(C36151sZ.A00(AnonymousClass0CL.A06, 0));
    }

    public void onPublish(String str, byte[] bArr, int i, int i2) {
        this.A01.A00.A04(new C02040Cp(new C01990Ck(AnonymousClass0CL.A08, i), new AnonymousClass0G8(str, i2), bArr));
    }

    public void onPublishAck(int i) {
        this.A01.A00.A04(C36151sZ.A00(AnonymousClass0CL.A07, i));
    }

    public void onSubscribeAck(int i) {
        C010708t.A0I("WhistleClientCore", "SubAck msgId=%d, messageId");
        this.A01.A00.A04(C36151sZ.A00(AnonymousClass0CL.A09, i));
    }

    public void onSubscribeFailure(int i, MQTTClientError mQTTClientError) {
        C010708t.A0I("WhistleClientCore", "Subscribe should not be used");
        Throwable A012 = C36151sZ.A01(mQTTClientError);
        this.A01.A00.A01(AnonymousClass0CE.A01(A012), AnonymousClass0CG.A07, A012);
    }

    public void onUnsubscribeAck(int i) {
        this.A01.A00.A04(C36151sZ.A00(AnonymousClass0CL.A0B, i));
    }

    public void onPingRequestFailure(MQTTClientError mQTTClientError) {
        Throwable A012 = C36151sZ.A01(mQTTClientError);
        this.A01.A00.A01(AnonymousClass0CE.A01(A012), AnonymousClass0CG.A03, A012);
    }

    public void onPingResponseFailure(MQTTClientError mQTTClientError) {
        Throwable A012 = C36151sZ.A01(mQTTClientError);
        this.A01.A00.A01(AnonymousClass0CE.A01(A012), AnonymousClass0CG.A04, A012);
    }

    public void onPublishAckFailure(MQTTClientError mQTTClientError) {
        Throwable A012 = C36151sZ.A01(mQTTClientError);
        this.A01.A00.A01(AnonymousClass0CE.A01(A012), AnonymousClass0CG.A05, A012);
    }

    public void onPublishFailure(int i, MQTTClientError mQTTClientError) {
        Throwable A012 = C36151sZ.A01(mQTTClientError);
        this.A01.A00.A01(AnonymousClass0CE.A01(A012), AnonymousClass0CG.A06, A012);
    }

    public void onPublishSent(String str, int i) {
        String A012 = AnonymousClass0AI.A01(str);
        if (A012 != null) {
            str = A012;
        }
        this.A01.A00.A06(AnonymousClass0CL.A08.name(), str);
    }

    public void onUnsubscribeFailure(int i, MQTTClientError mQTTClientError) {
        Throwable A012 = C36151sZ.A01(mQTTClientError);
        this.A01.A00.A01(AnonymousClass0CE.A01(A012), AnonymousClass0CG.A08, A012);
    }
}
