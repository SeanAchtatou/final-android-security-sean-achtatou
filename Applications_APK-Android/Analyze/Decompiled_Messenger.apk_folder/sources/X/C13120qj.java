package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.facebook.messaging.connectionstab.contacts.AllContactsActivity;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.montage.composer.MontageComposerActivity;
import com.facebook.messaging.montage.composer.model.MontageComposerFragmentParams;
import com.facebook.messaging.omnipicker.datamodel.M4OmnipickerParam;
import com.facebook.messaging.peopletab.activity.PeopleTabActivity;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.facebook.messenger.neue.MessengerMePreferenceActivity;
import com.facebook.orca.threadlist.RecentThreadListActivity;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0qj  reason: invalid class name and case insensitive filesystem */
public final class C13120qj implements C32291lW {
    private AnonymousClass0UN A00;

    public static final C13120qj A00(AnonymousClass1XY r1) {
        return new C13120qj(r1);
    }

    public void BvP(Context context) {
        Intent intent;
        if (((Boolean) AnonymousClass1XX.A03(AnonymousClass1Y3.BAo, this.A00)).booleanValue()) {
            intent = ((C199309Zt) AnonymousClass1XX.A03(AnonymousClass1Y3.AJV, this.A00)).build();
        } else {
            intent = new Intent(context, PeopleTabActivity.class);
        }
        C417626w.A06(intent, context);
        C13400rN.A01((C13400rN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7C, this.A00), "add_contact_flow");
    }

    public void BvS(Context context, NavigationTrigger navigationTrigger, MontageComposerFragmentParams montageComposerFragmentParams) {
        ((C26558D1u) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BJ0, this.A00)).A03(true, true);
        C417626w.A06(MontageComposerActivity.A00(context, navigationTrigger, montageComposerFragmentParams), context);
        C13400rN.A01((C13400rN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7C, this.A00), "montage_tab");
    }

    public void BvV(Context context, ImmutableList immutableList) {
        ((C149356wK) AnonymousClass1XX.A03(AnonymousClass1Y3.BJi, this.A00)).A00.markerStart(5505182);
        ((C07380dK) AnonymousClass1XX.A03(AnonymousClass1Y3.BQ3, this.A00)).A03("android_omnipicker_launch");
        AnonymousClass1XX.A03(AnonymousClass1Y3.BEy, this.A00);
        C141606iq A002 = M4OmnipickerParam.A00(AnonymousClass07B.A01);
        A002.A0P = true;
        C417626w.A06(((C141396iR) AnonymousClass1XX.A03(AnonymousClass1Y3.AiD, this.A00)).A01(context, immutableList, new M4OmnipickerParam(A002)), context);
        C13400rN.A01((C13400rN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7C, this.A00), "composer");
    }

    public void Bvd(Context context, ThreadKey threadKey, C149266wB r6) {
        Intent intent = new Intent(context, RecentThreadListActivity.class);
        intent.putExtra(C99084oO.$const$string(AnonymousClass1Y3.A5q), threadKey);
        intent.putExtra(C99084oO.$const$string(764), r6);
        C417626w.A06(intent, context);
        C13400rN.A01((C13400rN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7C, this.A00), "filtered_threadlist");
    }

    public void Bvn(Context context, C13060qW r5, String str) {
        ((C118845jo) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BOP, this.A00)).A04(context, r5, str);
    }

    public void Bvo(Context context) {
        C417626w.A06(new Intent("android.intent.action.VIEW", Uri.parse(C52652jT.A09)), context);
        C13400rN.A01((C13400rN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7C, this.A00), AnonymousClass24B.$const$string(AnonymousClass1Y3.A25));
    }

    public void Bvw(Context context) {
        C417626w.A06(new Intent(context, AllContactsActivity.class), context);
        C13400rN.A01((C13400rN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7C, this.A00), "people");
    }

    public void Bw0(Context context) {
        C417626w.A06(new Intent(context, MessengerMePreferenceActivity.class), context);
        C13400rN.A01((C13400rN) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7C, this.A00), "app_settings");
    }

    public C13120qj(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
