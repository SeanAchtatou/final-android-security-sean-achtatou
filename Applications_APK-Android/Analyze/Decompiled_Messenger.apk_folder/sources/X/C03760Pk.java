package X;

import com.facebook.profilo.provider.systemcounters.SystemCounterThread;

/* renamed from: X.0Pk  reason: invalid class name and case insensitive filesystem */
public final class C03760Pk {
    static {
        AnonymousClass01q.A08("profilo_systemcounters");
    }

    public static void A00(int i) {
        SystemCounterThread.nativeAddToWhitelist(i);
    }
}
