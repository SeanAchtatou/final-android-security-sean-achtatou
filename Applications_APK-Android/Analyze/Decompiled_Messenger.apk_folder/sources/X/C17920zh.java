package X;

import android.net.Uri;
import com.facebook.common.util.TriState;
import com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType;
import com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason;
import com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType;
import com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.customthreads.model.ThreadThemeInfo;
import com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings;
import com.facebook.messaging.model.messages.MessageDraft;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.AdContextData;
import com.facebook.messaging.model.threads.AdsConversionsQPData;
import com.facebook.messaging.model.threads.GroupThreadData;
import com.facebook.messaging.model.threads.MarketplaceThreadData;
import com.facebook.messaging.model.threads.MontageThreadPreview;
import com.facebook.messaging.model.threads.NotificationSetting;
import com.facebook.messaging.model.threads.RelatedPageThreadData;
import com.facebook.messaging.model.threads.RequestAppointmentData;
import com.facebook.messaging.model.threads.ThreadBookingRequests;
import com.facebook.messaging.model.threads.ThreadConnectivityData;
import com.facebook.messaging.model.threads.ThreadCustomization;
import com.facebook.messaging.model.threads.ThreadMediaPreview;
import com.facebook.messaging.model.threads.ThreadPageMessageAssignedAdmin;
import com.facebook.messaging.model.threads.ThreadRtcCallInfoData;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.0zh  reason: invalid class name and case insensitive filesystem */
public final class C17920zh {
    public float A00;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08 = -1;
    public long A09;
    public long A0A;
    public long A0B;
    public Uri A0C;
    public Uri A0D;
    public TriState A0E;
    public GraphQLExtensibleMessageAdminTextType A0F;
    public GraphQLMessageThreadCannotReplyReason A0G;
    public GraphQLMessengerGroupThreadSubType A0H;
    public GraphQLMessengerXMAGroupingType A0I;
    public CallToAction A0J;
    public CallToAction A0K;
    public ThreadThemeInfo A0L;
    public GamesPushNotificationSettings A0M;
    public C10950l8 A0N;
    public MessageDraft A0O;
    public ParticipantInfo A0P;
    public ThreadKey A0Q;
    public ThreadKey A0R;
    public AdContextData A0S;
    public AdsConversionsQPData A0T;
    public GroupThreadData A0U;
    public MarketplaceThreadData A0V;
    public MontageThreadPreview A0W;
    public NotificationSetting A0X;
    public AnonymousClass105 A0Y;
    public RelatedPageThreadData A0Z;
    public RequestAppointmentData A0a;
    public ThreadBookingRequests A0b;
    public ThreadConnectivityData A0c;
    public ThreadCustomization A0d;
    public ThreadMediaPreview A0e;
    public ThreadPageMessageAssignedAdmin A0f;
    public ThreadRtcCallInfoData A0g;
    public C17930zi A0h;
    public AnimatedThreadActivityBannerDataModel A0i;
    public ImmutableList A0j;
    public ImmutableList A0k;
    public String A0l;
    public String A0m;
    public String A0n;
    public String A0o;
    public String A0p;
    public String A0q;
    public String A0r;
    public String A0s;
    public String A0t;
    public List A0u;
    public List A0v;
    public List A0w;
    public boolean A0x;
    public boolean A0y;
    public boolean A0z;
    public boolean A10;
    public boolean A11;
    public boolean A12;
    public boolean A13;
    public boolean A14;
    public boolean A15;
    public boolean A16;
    public boolean A17;

    public ThreadSummary A00() {
        if (this.A0d == null) {
            this.A0d = ThreadCustomization.A02;
        }
        if (this.A0L == null) {
            this.A0L = C28921fa.A00;
        }
        if (this.A0g == null) {
            this.A0g = ThreadRtcCallInfoData.A04;
        }
        if (this.A0E == null) {
            this.A0E = TriState.UNSET;
        }
        return new ThreadSummary(this);
    }

    public void A02(ThreadSummary threadSummary) {
        this.A0R = threadSummary.A0S;
        this.A08 = threadSummary.A09;
        this.A0p = threadSummary.A0t;
        this.A0k = threadSummary.A0m;
        this.A0j = threadSummary.A0k;
        this.A0A = threadSummary.A0B;
        this.A07 = threadSummary.A08;
        this.A06 = threadSummary.A07;
        this.A05 = threadSummary.A06;
        this.A02 = threadSummary.A03;
        this.A0B = threadSummary.A0C;
        this.A0v = threadSummary.A0n;
        this.A0s = threadSummary.A0w;
        this.A0P = threadSummary.A0Q;
        this.A0l = threadSummary.A0p;
        this.A0q = threadSummary.A0u;
        this.A0D = threadSummary.A0E;
        this.A0x = threadSummary.A0y;
        this.A0G = threadSummary.A0H;
        this.A15 = threadSummary.A16;
        this.A0z = threadSummary.A10;
        this.A0h = threadSummary.A0i;
        this.A0y = threadSummary.A0z;
        this.A0z = threadSummary.A12;
        this.A0N = threadSummary.A0O;
        this.A0O = threadSummary.A0P;
        this.A0X = threadSummary.A0Y;
        this.A0d = threadSummary.A0e;
        this.A13 = threadSummary.A14;
        this.A01 = threadSummary.A02;
        this.A0u = threadSummary.A0l;
        this.A0o = threadSummary.A0s;
        this.A0n = threadSummary.A0r;
        this.A0F = threadSummary.A0G;
        this.A0K = threadSummary.A0L;
        this.A00 = threadSummary.A01;
        this.A0g = threadSummary.A0h;
        this.A0m = threadSummary.A0q;
        this.A0E = threadSummary.A0F;
        this.A0e = threadSummary.A0f;
        this.A0b = threadSummary.A0c;
        this.A04 = threadSummary.A05;
        this.A0W = threadSummary.A0X;
        this.A0w = threadSummary.A0o;
        this.A0Q = threadSummary.A0R;
        this.A0U = threadSummary.A0V;
        this.A0V = threadSummary.A0W;
        this.A0S = threadSummary.A0T;
        this.A0T = threadSummary.A0U;
        this.A0Y = threadSummary.A0Z;
        this.A17 = threadSummary.A18;
        this.A0M = threadSummary.A0N;
        this.A0C = threadSummary.A0D;
        this.A0L = threadSummary.A0M;
        this.A12 = threadSummary.A13;
        this.A16 = threadSummary.A17;
        this.A09 = threadSummary.A0A;
        this.A0i = threadSummary.A0j;
        this.A0I = threadSummary.A0J;
        this.A0J = threadSummary.A0K;
        this.A0c = threadSummary.A0d;
        this.A0t = threadSummary.A0x;
        this.A0H = threadSummary.A0I;
        this.A14 = threadSummary.A15;
        this.A0a = threadSummary.A0b;
        this.A0Z = threadSummary.A0a;
        this.A0f = threadSummary.A0g;
        this.A03 = threadSummary.A04;
        this.A10 = threadSummary.A11;
    }

    public C17920zh() {
        ImmutableList immutableList = RegularImmutableList.A02;
        this.A0k = immutableList;
        this.A0j = immutableList;
        this.A05 = -1;
        this.A0v = immutableList;
        this.A0h = C17930zi.NONE;
        this.A0X = NotificationSetting.A06;
        this.A0u = immutableList;
        this.A0w = immutableList;
        this.A0U = new GroupThreadData(new C17980zq());
        this.A0Y = AnonymousClass105.A03;
        this.A0I = GraphQLMessengerXMAGroupingType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE;
    }

    public void A01(GroupThreadData groupThreadData) {
        C05520Zg.A02(groupThreadData);
        this.A0U = groupThreadData;
    }

    public void A03(List list) {
        C05520Zg.A02(list);
        this.A0k = ImmutableList.copyOf((Collection) list);
    }
}
