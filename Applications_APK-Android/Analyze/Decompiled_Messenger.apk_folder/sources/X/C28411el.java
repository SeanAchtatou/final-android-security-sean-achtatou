package X;

import java.io.Closeable;

/* renamed from: X.1el  reason: invalid class name and case insensitive filesystem */
public interface C28411el extends Closeable {
    void close();

    int getCount();

    boolean moveToFirst();

    boolean moveToNext();
}
