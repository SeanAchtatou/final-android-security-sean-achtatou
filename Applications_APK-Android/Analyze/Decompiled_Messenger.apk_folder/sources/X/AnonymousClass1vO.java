package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1vO  reason: invalid class name */
public final class AnonymousClass1vO extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C55672oT A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 13)
    public C21895Ajy A03;
    @Comparable(type = 3)
    public boolean A04;

    public AnonymousClass1vO(Context context) {
        super("M4ActiveStatusLayout");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }
}
