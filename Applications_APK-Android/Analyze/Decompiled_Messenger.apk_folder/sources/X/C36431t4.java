package X;

import android.os.Handler;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;

/* renamed from: X.1t4  reason: invalid class name and case insensitive filesystem */
public final class C36431t4 extends PhoneStateListener {
    public final /* synthetic */ AnonymousClass125 A00;

    public C36431t4(AnonymousClass125 r1) {
        this.A00 = r1;
    }

    public void onCellLocationChanged(CellLocation cellLocation) {
        AnonymousClass125 r3 = this.A00;
        r3.A00 = ((AnonymousClass069) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BBa, r3.A01)).now();
        if (((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, this.A00.A01)).Aem(283673999969684L)) {
            AnonymousClass00S.A04((Handler) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Abm, this.A00.A01), new C30133EqD(this), -132031477);
            return;
        }
        AnonymousClass125.A04(this.A00);
        this.A00.A06();
    }

    public void onServiceStateChanged(ServiceState serviceState) {
        if (((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, this.A00.A01)).Aem(283673999969684L)) {
            AnonymousClass00S.A04((Handler) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Abm, this.A00.A01), new C30132EqC(this), -521358619);
        } else {
            AnonymousClass125.A04(this.A00);
        }
    }
}
