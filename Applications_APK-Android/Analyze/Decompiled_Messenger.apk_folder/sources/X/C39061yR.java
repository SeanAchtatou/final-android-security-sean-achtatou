package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1yR  reason: invalid class name and case insensitive filesystem */
public final class C39061yR extends AnonymousClass0UV {
    private static volatile AnonymousClass996 A00;

    public static final Boolean A01() {
        return true;
    }

    public static final AnonymousClass996 A00(AnonymousClass1XY r10) {
        if (A00 == null) {
            synchronized (AnonymousClass996.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r10);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r10.getApplicationInjector();
                        A00 = new AnonymousClass996(AnonymousClass1YA.A00(applicationInjector), C06240bB.A01(applicationInjector), new AnonymousClass998(applicationInjector), C06240bB.A01(applicationInjector), C06240bB.A01(applicationInjector), AnonymousClass99I.A00(applicationInjector), new C45732Ng(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
