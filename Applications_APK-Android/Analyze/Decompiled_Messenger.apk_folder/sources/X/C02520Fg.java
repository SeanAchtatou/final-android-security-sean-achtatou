package X;

/* renamed from: X.0Fg  reason: invalid class name and case insensitive filesystem */
public final class C02520Fg extends AnonymousClass0FM {
    public long mobileBytesRx;
    public long mobileBytesTx;
    public long wifiBytesRx;
    public long wifiBytesTx;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C02520Fg r7 = (C02520Fg) obj;
            if (!(this.mobileBytesTx == r7.mobileBytesTx && this.mobileBytesRx == r7.mobileBytesRx && this.wifiBytesTx == r7.wifiBytesTx && this.wifiBytesRx == r7.wifiBytesRx)) {
                return false;
            }
        }
        return true;
    }

    private void A00(C02520Fg r3) {
        this.mobileBytesRx = r3.mobileBytesRx;
        this.mobileBytesTx = r3.mobileBytesTx;
        this.wifiBytesRx = r3.wifiBytesRx;
        this.wifiBytesTx = r3.wifiBytesTx;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((C02520Fg) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02520Fg r52 = (C02520Fg) r5;
        C02520Fg r62 = (C02520Fg) r6;
        if (r62 == null) {
            r62 = new C02520Fg();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.mobileBytesTx = this.mobileBytesTx - r52.mobileBytesTx;
        r62.mobileBytesRx = this.mobileBytesRx - r52.mobileBytesRx;
        r62.wifiBytesTx = this.wifiBytesTx - r52.wifiBytesTx;
        r62.wifiBytesRx = this.wifiBytesRx - r52.wifiBytesRx;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02520Fg r52 = (C02520Fg) r5;
        C02520Fg r62 = (C02520Fg) r6;
        if (r62 == null) {
            r62 = new C02520Fg();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.mobileBytesTx = this.mobileBytesTx + r52.mobileBytesTx;
        r62.mobileBytesRx = this.mobileBytesRx + r52.mobileBytesRx;
        r62.wifiBytesTx = this.wifiBytesTx + r52.wifiBytesTx;
        r62.wifiBytesRx = this.wifiBytesRx + r52.wifiBytesRx;
        return r62;
    }

    public int hashCode() {
        long j = this.mobileBytesTx;
        long j2 = this.mobileBytesRx;
        long j3 = this.wifiBytesTx;
        long j4 = this.wifiBytesRx;
        return (((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)));
    }

    public String toString() {
        return "NetworkMetrics{mobileBytesTx=" + this.mobileBytesTx + ", mobileBytesRx=" + this.mobileBytesRx + ", wifiBytesTx=" + this.wifiBytesTx + ", wifiBytesRx=" + this.wifiBytesRx + '}';
    }
}
