package X;

/* renamed from: X.1fg  reason: invalid class name and case insensitive filesystem */
public final class C28981fg extends C14610tg {
    public final Object _value;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Object obj2 = this._value;
        Object obj3 = ((C28981fg) obj)._value;
        if (obj2 == null) {
            return obj3 == null;
        }
        return obj2.equals(obj3);
    }

    public boolean asBoolean(boolean z) {
        Object obj = this._value;
        if (obj == null || !(obj instanceof Boolean)) {
            return z;
        }
        return ((Boolean) obj).booleanValue();
    }

    public double asDouble(double d) {
        Object obj = this._value;
        if (obj instanceof Number) {
            return ((Number) obj).doubleValue();
        }
        return d;
    }

    public int asInt(int i) {
        Object obj = this._value;
        if (obj instanceof Number) {
            return ((Number) obj).intValue();
        }
        return i;
    }

    public long asLong(long j) {
        Object obj = this._value;
        if (obj instanceof Number) {
            return ((Number) obj).longValue();
        }
        return j;
    }

    public String asText() {
        Object obj = this._value;
        if (obj == null) {
            return "null";
        }
        return obj.toString();
    }

    public C182811d asToken() {
        return C182811d.VALUE_EMBEDDED_OBJECT;
    }

    public byte[] binaryValue() {
        Object obj = this._value;
        if (obj instanceof byte[]) {
            return (byte[]) obj;
        }
        return super.binaryValue();
    }

    public C11980oL getNodeType() {
        return C11980oL.POJO;
    }

    public int hashCode() {
        return this._value.hashCode();
    }

    public final void serialize(C11710np r2, C11260mU r3) {
        Object obj = this._value;
        if (obj == null) {
            r3.defaultSerializeNull(r2);
        } else {
            r2.writeObject(obj);
        }
    }

    public String toString() {
        return String.valueOf(this._value);
    }

    public C28981fg(Object obj) {
        this._value = obj;
    }
}
