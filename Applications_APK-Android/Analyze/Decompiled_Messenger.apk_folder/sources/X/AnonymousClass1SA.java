package X;

import android.graphics.Bitmap;
import java.util.IdentityHashMap;
import java.util.Map;

/* renamed from: X.1SA  reason: invalid class name */
public final class AnonymousClass1SA {
    public static final Map A03 = new IdentityHashMap();
    public int A00 = 1;
    private Object A01;
    private final C22891Nf A02;

    public synchronized Object A01() {
        return this.A01;
    }

    public void A02() {
        int i;
        Object obj;
        synchronized (this) {
            A00(this);
            boolean z = false;
            if (this.A00 > 0) {
                z = true;
            }
            C05520Zg.A04(z);
            i = this.A00 - 1;
            this.A00 = i;
        }
        if (i == 0) {
            synchronized (this) {
                try {
                    obj = this.A01;
                    this.A01 = null;
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            this.A02.C0t(obj);
            Map map = A03;
            synchronized (map) {
                try {
                    Integer num = (Integer) map.get(obj);
                    if (num == null) {
                        AnonymousClass02w.A0F("SharedReference", "No entry in sLiveObjects for value of type %s", obj.getClass());
                    } else {
                        int intValue = num.intValue();
                        if (intValue == 1) {
                            map.remove(obj);
                        } else {
                            map.put(obj, Integer.valueOf(intValue - 1));
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x000f, code lost:
        if (r1 == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.AnonymousClass1SA r2) {
        /*
            if (r2 == 0) goto L_0x0011
            monitor-enter(r2)
            int r0 = r2.A00     // Catch:{ all -> 0x0009 }
            r1 = 0
            if (r0 <= 0) goto L_0x000d
            goto L_0x000c
        L_0x0009:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x000c:
            r1 = 1
        L_0x000d:
            monitor-exit(r2)
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x0015
            return
        L_0x0015:
            X.4X0 r0 = new X.4X0
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1SA.A00(X.1SA):void");
    }

    public AnonymousClass1SA(Object obj, C22891Nf r6) {
        C05520Zg.A02(obj);
        this.A01 = obj;
        C05520Zg.A02(r6);
        this.A02 = r6;
        if (!(AnonymousClass1PS.A04 == 3) || (!(obj instanceof Bitmap) && !(obj instanceof AnonymousClass1S5))) {
            Map map = A03;
            synchronized (map) {
                Integer num = (Integer) map.get(obj);
                if (num == null) {
                    map.put(obj, 1);
                } else {
                    map.put(obj, Integer.valueOf(num.intValue() + 1));
                }
            }
        }
    }
}
