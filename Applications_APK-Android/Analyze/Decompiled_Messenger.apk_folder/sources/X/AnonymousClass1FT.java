package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FT  reason: invalid class name */
public final class AnonymousClass1FT implements AnonymousClass1FI {
    private static volatile AnonymousClass1FT A00;

    public static final AnonymousClass1FT A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (AnonymousClass1FT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new AnonymousClass1FT(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public ImmutableMap get() {
        HashMap hashMap = new HashMap();
        hashMap.put(new SubscribeTopic("/pp", 0), AnonymousClass1FP.ALWAYS);
        return ImmutableMap.copyOf(hashMap);
    }

    private AnonymousClass1FT(AnonymousClass1XY r1) {
        AnonymousClass0UU.A05(r1);
    }
}
