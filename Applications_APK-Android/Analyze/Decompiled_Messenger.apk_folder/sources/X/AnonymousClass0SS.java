package X;

import java.net.Socket;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0SS  reason: invalid class name */
public abstract class AnonymousClass0SS {
    public final ExecutorService A00;

    public abstract Socket A00(Socket socket, String str, int i);

    public AnonymousClass0SS(ExecutorService executorService) {
        this.A00 = executorService;
    }
}
