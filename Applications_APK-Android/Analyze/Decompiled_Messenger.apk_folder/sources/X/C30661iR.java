package X;

import java.io.InputStream;

/* renamed from: X.1iR  reason: invalid class name and case insensitive filesystem */
public final class C30661iR {
    public final C23011Nw A00;
    public final C22921Ni A01;

    public AnonymousClass1SS A00(InputStream inputStream, int i) {
        AnonymousClass1S2 r1 = new AnonymousClass1S2(this.A01, i);
        try {
            this.A00.A00(inputStream, r1);
            return r1.A02();
        } finally {
            r1.close();
        }
    }

    public AnonymousClass1S3 A01() {
        C22921Ni r2 = this.A01;
        return new AnonymousClass1S2(r2, r2.A00[0]);
    }

    public C30661iR(C22921Ni r1, C23011Nw r2) {
        this.A01 = r1;
        this.A00 = r2;
    }
}
