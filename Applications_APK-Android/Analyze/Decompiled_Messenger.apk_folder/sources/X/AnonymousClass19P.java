package X;

import com.facebook.acra.LogCatCollector;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;

/* renamed from: X.19P  reason: invalid class name */
public final class AnonymousClass19P {
    public static final Charset A0A = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int[] A05;
    public int A06;
    public ByteBuffer A07;
    public boolean A08;
    public int[] A09;

    public void A04(int i) {
        A09(4, 0);
        ByteBuffer byteBuffer = this.A07;
        int i2 = this.A06 - 4;
        this.A06 = i2;
        byteBuffer.putInt(i2, (A02() - i) + 4);
    }

    public void A07(int i, byte b, int i2) {
        if (0 != 0 || b != i2) {
            A09(1, 0);
            ByteBuffer byteBuffer = this.A07;
            int i3 = this.A06 - 1;
            this.A06 = i3;
            byteBuffer.put(i3, b);
            this.A09[i] = A02();
        }
    }

    public void A08(int i, float f, double d) {
        if (0 != 0 || ((double) f) != d) {
            A09(4, 0);
            ByteBuffer byteBuffer = this.A07;
            int i2 = this.A06 - 4;
            this.A06 = i2;
            byteBuffer.putFloat(i2, f);
            this.A09[i] = A02();
        }
    }

    public void A0A(int i, int i2, int i3) {
        if (0 != 0 || i2 != i3) {
            A09(4, 0);
            ByteBuffer byteBuffer = this.A07;
            int i4 = this.A06 - 4;
            this.A06 = i4;
            byteBuffer.putInt(i4, i2);
            this.A09[i] = A02();
        }
    }

    public void A0B(int i, int i2, int i3) {
        if (0 != 0 || i2 != i3) {
            A04(i2);
            this.A09[i] = A02();
        }
    }

    public void A0D(int i, long j, long j2) {
        if (0 != 0 || j != j2) {
            A09(8, 0);
            ByteBuffer byteBuffer = this.A07;
            int i2 = this.A06 - 8;
            this.A06 = i2;
            byteBuffer.putLong(i2, j);
            this.A09[i] = A02();
        }
    }

    public void A0E(int i, boolean z, boolean z2) {
        if (0 != 0 || z != z2) {
            A09(1, 0);
            ByteBuffer byteBuffer = this.A07;
            int i2 = this.A06 - 1;
            this.A06 = i2;
            byteBuffer.put(i2, z ? (byte) 1 : 0);
            this.A09[i] = A02();
        }
    }

    public int A00() {
        int i;
        if (this.A09 == null || !this.A08) {
            throw new AssertionError("FlatBuffers: endObject called without startObject");
        }
        A09(4, 0);
        ByteBuffer byteBuffer = this.A07;
        int i2 = this.A06 - 4;
        this.A06 = i2;
        byteBuffer.putInt(i2, 0);
        int A022 = A02();
        for (int i3 = this.A04 - 1; i3 >= 0; i3--) {
            int i4 = this.A09[i3];
            int i5 = 0;
            if (i4 != 0) {
                i5 = A022 - i4;
            }
            short s = (short) i5;
            A09(2, 0);
            ByteBuffer byteBuffer2 = this.A07;
            int i6 = this.A06 - 2;
            this.A06 = i6;
            byteBuffer2.putShort(i6, s);
        }
        short s2 = (short) (A022 - this.A02);
        A09(2, 0);
        ByteBuffer byteBuffer3 = this.A07;
        int i7 = this.A06 - 2;
        this.A06 = i7;
        byteBuffer3.putShort(i7, s2);
        short s3 = (short) ((this.A04 + 2) << 1);
        A09(2, 0);
        ByteBuffer byteBuffer4 = this.A07;
        int i8 = this.A06 - 2;
        this.A06 = i8;
        byteBuffer4.putShort(i8, s3);
        int i9 = 0;
        loop1:
        while (true) {
            if (i9 >= this.A01) {
                i = 0;
                break;
            }
            int capacity = this.A07.capacity() - this.A05[i9];
            int i10 = this.A06;
            short s4 = this.A07.getShort(capacity);
            if (s4 == this.A07.getShort(i10)) {
                int i11 = 2;
                while (i11 < s4) {
                    if (this.A07.getShort(capacity + i11) == this.A07.getShort(i10 + i11)) {
                        i11 += 2;
                    }
                }
                i = this.A05[i9];
                break loop1;
            }
            i9++;
        }
        if (i != 0) {
            int capacity2 = this.A07.capacity() - A022;
            this.A06 = capacity2;
            this.A07.putInt(capacity2, i - A022);
        } else {
            int i12 = this.A01;
            int[] iArr = this.A05;
            if (i12 == iArr.length) {
                this.A05 = Arrays.copyOf(iArr, i12 << 1);
            }
            int[] iArr2 = this.A05;
            int i13 = this.A01;
            this.A01 = i13 + 1;
            iArr2[i13] = A02();
            ByteBuffer byteBuffer5 = this.A07;
            byteBuffer5.putInt(byteBuffer5.capacity() - A022, A02() - A022);
        }
        this.A08 = false;
        return A022;
    }

    public int A01() {
        int i = this.A03;
        ByteBuffer byteBuffer = this.A07;
        int i2 = this.A06 - 4;
        this.A06 = i2;
        byteBuffer.putInt(i2, i);
        return A02();
    }

    public int A02() {
        return this.A07.capacity() - this.A06;
    }

    public int A03(String str) {
        byte[] bytes = str.getBytes(A0A);
        A09(1, 0);
        ByteBuffer byteBuffer = this.A07;
        int i = this.A06 - 1;
        this.A06 = i;
        byteBuffer.put(i, (byte) 0);
        int length = bytes.length;
        A0C(1, length, 1);
        ByteBuffer byteBuffer2 = this.A07;
        int i2 = this.A06 - length;
        this.A06 = i2;
        byteBuffer2.position(i2);
        this.A07.put(bytes, 0, length);
        return A01();
    }

    public void A05(int i) {
        A09(this.A00, 4);
        A04(i);
        this.A07.position(this.A06);
    }

    public void A06(int i) {
        if (!this.A08) {
            int[] iArr = this.A09;
            if (iArr == null || iArr.length < i) {
                this.A09 = new int[i];
            }
            this.A04 = i;
            Arrays.fill(this.A09, 0, i, 0);
            this.A08 = true;
            this.A02 = A02();
            return;
        }
        throw new AssertionError(AnonymousClass80H.$const$string(91));
    }

    public void A09(int i, int i2) {
        if (i > this.A00) {
            this.A00 = i;
        }
        int capacity = ((((this.A07.capacity() - this.A06) + i2) ^ -1) + 1) & (i - 1);
        while (this.A06 < capacity + i + i2) {
            int capacity2 = this.A07.capacity();
            ByteBuffer byteBuffer = this.A07;
            int capacity3 = byteBuffer.capacity();
            if ((-1073741824 & capacity3) == 0) {
                int i3 = capacity3 << 1;
                byteBuffer.position(0);
                ByteBuffer allocate = ByteBuffer.allocate(i3);
                allocate.order(ByteOrder.LITTLE_ENDIAN);
                allocate.position(i3 - capacity3);
                allocate.put(byteBuffer);
                this.A07 = allocate;
                this.A06 += allocate.capacity() - capacity2;
            } else {
                throw new AssertionError("FlatBuffers: cannot grow buffer beyond 2 gigabytes.");
            }
        }
        for (int i4 = 0; i4 < capacity; i4++) {
            ByteBuffer byteBuffer2 = this.A07;
            int i5 = this.A06 - 1;
            this.A06 = i5;
            byteBuffer2.put(i5, (byte) 0);
        }
    }

    public void A0C(int i, int i2, int i3) {
        if (!this.A08) {
            this.A03 = i2;
            int i4 = i * i2;
            A09(4, i4);
            A09(i3, i4);
            return;
        }
        throw new AssertionError(AnonymousClass80H.$const$string(91));
    }

    public AnonymousClass19P() {
        this(1024);
    }

    public AnonymousClass19P(int i) {
        this.A00 = 1;
        this.A09 = null;
        this.A04 = 0;
        this.A08 = false;
        this.A05 = new int[16];
        this.A01 = 0;
        this.A03 = 0;
        i = i <= 0 ? 1 : i;
        this.A06 = i;
        ByteBuffer allocate = ByteBuffer.allocate(i);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        this.A07 = allocate;
    }
}
