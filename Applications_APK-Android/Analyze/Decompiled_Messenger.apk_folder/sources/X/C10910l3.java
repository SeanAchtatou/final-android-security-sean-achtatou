package X;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0l3  reason: invalid class name and case insensitive filesystem */
public final class C10910l3 implements ListenableFuture {
    private final ListenableFuture A00;

    public void addListener(Runnable runnable, Executor executor) {
        this.A00.addListener(runnable, executor);
    }

    public boolean cancel(boolean z) {
        return this.A00.cancel(z);
    }

    public boolean isCancelled() {
        return this.A00.isCancelled();
    }

    public boolean isDone() {
        return this.A00.isDone();
    }

    public C10910l3(ListenableFuture listenableFuture, C26931cN r2) {
        Preconditions.checkNotNull(listenableFuture);
        this.A00 = listenableFuture;
        Preconditions.checkNotNull(r2);
    }

    public Object get() {
        return this.A00.get();
    }

    public Object get(long j, TimeUnit timeUnit) {
        return this.A00.get(j, timeUnit);
    }
}
