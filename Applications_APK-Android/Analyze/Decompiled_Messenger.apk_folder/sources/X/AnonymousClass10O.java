package X;

/* renamed from: X.10O  reason: invalid class name */
public enum AnonymousClass10O {
    RED_WITH_TEXT,
    RED_WITH_TEXT_SMALL,
    ACTIVE_NOW,
    RED_DOT,
    GREEN_DOT,
    DO_NOT_DISTURB;

    public boolean A00() {
        switch (ordinal()) {
            case 3:
            case 4:
                return true;
            default:
                return false;
        }
    }
}
