package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.TextUtils;
import io.card.payment.BuildConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0QV  reason: invalid class name */
public final class AnonymousClass0QV implements C01400Ab {
    public static final AnonymousClass0BZ A0E = AnonymousClass0BZ.A00("MQTT_Analytics");
    public AnonymousClass0L0 A00;
    public final SharedPreferences A01;
    public final Handler A02;
    public final AnonymousClass0KU A03;
    public final C01690Bg A04;
    public final Runnable A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final Queue A09 = new ConcurrentLinkedQueue();
    public final AtomicBoolean A0A = new AtomicBoolean(false);
    private final C03280Ky A0B;
    private final AnonymousClass0O7 A0C;
    private final String A0D;

    public static void A00(AnonymousClass0QV r8) {
        if (!r8.A00.A07.isEmpty()) {
            C03280Ky r2 = r8.A0B;
            AnonymousClass0L0 r6 = r8.A00;
            if (!r2.A00.exists() && !r2.A00.mkdir()) {
                C010708t.A0I("AnalyticsStorage", "Unable to open analytics storage.");
            }
            File file = r2.A00;
            if (r6.A08 == null) {
                r6.A08 = UUID.randomUUID();
            }
            File file2 = new File(file, String.format(null, "%s_%d.batch", r6.A08.toString(), Integer.valueOf(r6.A00)));
            if (file2.exists() && !file2.delete()) {
                C010708t.A0P("AnalyticsStorage", "File %s was not deleted", file2);
            }
            r6.A01 = System.currentTimeMillis();
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF8");
                    try {
                        outputStreamWriter.write(r6.toString());
                    } catch (IOException e) {
                        C010708t.A0S("AnalyticsStorage", e, "failed to write session to file");
                    } catch (Throwable th) {
                        try {
                            outputStreamWriter.close();
                        } catch (IOException e2) {
                            C010708t.A0S("AnalyticsStorage", e2, "failed to close writer");
                        }
                        throw th;
                    }
                    try {
                        outputStreamWriter.close();
                    } catch (IOException e3) {
                        C010708t.A0S("AnalyticsStorage", e3, "failed to close writer");
                    }
                } catch (UnsupportedEncodingException e4) {
                    C010708t.A0S("AnalyticsStorage", e4, "UTF8 encoding is not supported");
                    try {
                        fileOutputStream.close();
                    } catch (IOException e5) {
                        C010708t.A0S("AnalyticsStorage", e5, "failed to close output stream");
                    }
                }
            } catch (FileNotFoundException e6) {
                C010708t.A0V("AnalyticsStorage", e6, "Batch file creation failed %s", file2);
            }
            AnonymousClass0L0 r1 = r8.A00;
            r1.A07.clear();
            r1.A00++;
        }
    }

    public static void A01(AnonymousClass0QV r3, Runnable runnable) {
        r3.A09.add(runnable);
        if (r3.A0A.compareAndSet(false, true)) {
            AnonymousClass07A.A04(A0E, r3.A05, -1778251650);
        }
    }

    public void C2i(C01750Bm r4) {
        AnonymousClass0O7 r1 = this.A0C;
        boolean z = false;
        if (r1.A01 || r1.A00.getBoolean(AnonymousClass0HA.A02.mPrefKey, false)) {
            z = true;
        }
        if (z) {
            A01(this, new AnonymousClass0KE(this, r4));
        }
    }

    public AnonymousClass0QV(Context context, String str, C01690Bg r10, AnonymousClass0O7 r11, SharedPreferences sharedPreferences, C01690Bg r13, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.A0D = str;
        this.A01 = sharedPreferences;
        this.A04 = r13;
        this.A08 = str4;
        this.A07 = str3;
        this.A06 = str7;
        this.A02 = new AnonymousClass0DU(this, context.getMainLooper());
        this.A05 = new AnonymousClass0KF(this);
        this.A0B = new C03280Ky(context.getApplicationContext(), this.A0D);
        this.A03 = new AnonymousClass0KU(context.getApplicationContext(), this.A0D, r10, str2, str5, str6);
        this.A0C = r11;
        if (this.A00 != null) {
            A00(this);
        }
        AnonymousClass0L0 r3 = new AnonymousClass0L0();
        r3.A04 = this.A07;
        r3.A05 = this.A08;
        String string = this.A01.getString("fb_uid", BuildConfig.FLAVOR);
        r3.A06 = TextUtils.isEmpty(string) ? "0" : string;
        r3.A03 = this.A06;
        r3.A02 = this.A04;
        this.A00 = r3;
    }
}
