package X;

import android.content.BroadcastReceiver;

/* renamed from: X.0BR  reason: invalid class name */
public final class AnonymousClass0BR extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0BO A00;

    public AnonymousClass0BR(AnonymousClass0BO r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0079, code lost:
        r11.A00.A0L.run();
        X.C000700l.A0D(r13, 126072304, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0086, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r12, android.content.Intent r13) {
        /*
            r11 = this;
            r0 = 1323576756(0x4ee42db4, float:1.91410022E9)
            int r2 = X.C000700l.A01(r0)
            java.lang.String r1 = r13.getAction()
            X.0BO r0 = r11.A00
            java.lang.String r0 = r0.A0H
            boolean r0 = X.AnonymousClass0A2.A00(r1, r0)
            if (r0 != 0) goto L_0x001c
            r0 = 1252561378(0x4aa891e2, float:5523697.0)
            X.C000700l.A0D(r13, r0, r2)
            return
        L_0x001c:
            X.0BO r3 = r11.A00
            monitor-enter(r3)
            r13.getAction()     // Catch:{ all -> 0x0087 }
            android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0087 }
            X.0BO r5 = r11.A00     // Catch:{ all -> 0x0087 }
            boolean r0 = r5.A0I     // Catch:{ all -> 0x0087 }
            if (r0 != 0) goto L_0x0034
            X.07y r4 = r5.A0E     // Catch:{ all -> 0x0087 }
            android.app.AlarmManager r1 = r5.A05     // Catch:{ all -> 0x0087 }
            android.app.PendingIntent r0 = r5.A06     // Catch:{ all -> 0x0087 }
            r4.A05(r1, r0)     // Catch:{ all -> 0x0087 }
        L_0x0034:
            X.0BO r4 = r11.A00     // Catch:{ all -> 0x0087 }
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0087 }
            long r0 = r4.A02     // Catch:{ all -> 0x0087 }
            long r8 = r8 + r0
            boolean r0 = r4.A03     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0055
            int r1 = r4.A04     // Catch:{ all -> 0x0087 }
            r0 = 23
            if (r1 < r0) goto L_0x0055
            boolean r0 = r4.A0I     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0055
            X.07y r5 = r4.A0E     // Catch:{ all -> 0x0087 }
            android.app.AlarmManager r6 = r4.A05     // Catch:{ all -> 0x0087 }
            r7 = 2
            android.app.PendingIntent r10 = r4.A08     // Catch:{ all -> 0x0087 }
            r5.A03(r6, r7, r8, r10)     // Catch:{ all -> 0x0087 }
        L_0x0055:
            X.0BO r7 = r11.A00     // Catch:{ all -> 0x0087 }
            long r0 = r7.A00     // Catch:{ all -> 0x0087 }
            r5 = 900000(0xdbba0, double:4.44659E-318)
            int r4 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r4 >= 0) goto L_0x0068
            monitor-exit(r3)     // Catch:{ all -> 0x0087 }
            r0 = -1985198(0xffffffffffe1b552, float:NaN)
            X.C000700l.A0D(r13, r0, r2)
            return
        L_0x0068:
            r7.A01 = r8     // Catch:{ all -> 0x0087 }
            boolean r0 = r7.A03     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0078
            boolean r0 = r7.A0I     // Catch:{ all -> 0x0087 }
            if (r0 != 0) goto L_0x0078
            r0 = 20000(0x4e20, double:9.8813E-320)
            long r8 = r8 + r0
            X.AnonymousClass0BO.A01(r7, r8)     // Catch:{ all -> 0x0087 }
        L_0x0078:
            monitor-exit(r3)     // Catch:{ all -> 0x0087 }
            X.0BO r0 = r11.A00
            java.lang.Runnable r0 = r0.A0L
            r0.run()
            r0 = 126072304(0x783b5f0, float:1.9817623E-34)
            X.C000700l.A0D(r13, r0, r2)
            return
        L_0x0087:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0087 }
            r0 = 997965508(0x3b7bbec4, float:0.0038413266)
            X.C000700l.A0D(r13, r0, r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BR.onReceive(android.content.Context, android.content.Intent):void");
    }
}
