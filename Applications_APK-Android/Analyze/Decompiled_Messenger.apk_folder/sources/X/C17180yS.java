package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0yS  reason: invalid class name and case insensitive filesystem */
public final class C17180yS<E> extends ArrayList<E> {
    public static C17180yS A00(Object... objArr) {
        C17180yS r1 = new C17180yS(objArr.length);
        Collections.addAll(r1, objArr);
        return r1;
    }

    private C17180yS(int i) {
        super(i);
    }

    public C17180yS(List list) {
        super(list);
    }
}
