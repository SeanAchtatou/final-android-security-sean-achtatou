package X;

import java.lang.ref.SoftReference;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.List;

/* renamed from: X.0zg  reason: invalid class name and case insensitive filesystem */
public final class C17910zg {
    public static final Class A01 = C17910zg.class;
    public SoftReference A00 = new SoftReference(null);

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0045, code lost:
        r1 = (javax.net.ssl.X509TrustManager) r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0055  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:17:0x0024=Splitter:B:17:0x0024, B:21:0x002e=Splitter:B:21:0x002e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[][] A01() {
        /*
            r8 = this;
            r7 = r8
            monitor-enter(r7)
            java.lang.ref.SoftReference r0 = r8.A00     // Catch:{ all -> 0x006e }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x006e }
            java.security.cert.X509Certificate[] r1 = (java.security.cert.X509Certificate[]) r1     // Catch:{ all -> 0x006e }
            if (r1 != 0) goto L_0x0064
            r6 = r8
            monitor-enter(r6)     // Catch:{ all -> 0x006e }
            r5 = 0
            java.lang.String r0 = javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm()     // Catch:{ NoSuchAlgorithmException -> 0x002c, KeyStoreException -> 0x0022 }
            javax.net.ssl.TrustManagerFactory r3 = javax.net.ssl.TrustManagerFactory.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x002c, KeyStoreException -> 0x0022 }
            r0 = r5
            java.security.KeyStore r0 = (java.security.KeyStore) r0     // Catch:{ NoSuchAlgorithmException -> 0x0020, KeyStoreException -> 0x001e }
            r3.init(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0020, KeyStoreException -> 0x001e }
            goto L_0x0035
        L_0x001e:
            r2 = move-exception
            goto L_0x0024
        L_0x0020:
            r2 = move-exception
            goto L_0x002e
        L_0x0022:
            r2 = move-exception
            r3 = r5
        L_0x0024:
            java.lang.Class r1 = X.C17910zg.A01     // Catch:{ all -> 0x0061 }
            java.lang.String r0 = "Failed to create TrustManagerFactory"
            X.C010708t.A08(r1, r0, r2)     // Catch:{ all -> 0x0061 }
            goto L_0x0035
        L_0x002c:
            r2 = move-exception
            r3 = r5
        L_0x002e:
            java.lang.Class r1 = X.C17910zg.A01     // Catch:{ all -> 0x0061 }
            java.lang.String r0 = "Failed to create TrustManagerFactory"
            X.C010708t.A08(r1, r0, r2)     // Catch:{ all -> 0x0061 }
        L_0x0035:
            if (r3 == 0) goto L_0x004d
            javax.net.ssl.TrustManager[] r4 = r3.getTrustManagers()     // Catch:{ all -> 0x0061 }
            int r3 = r4.length     // Catch:{ all -> 0x0061 }
            r2 = 0
        L_0x003d:
            if (r2 >= r3) goto L_0x004d
            r1 = r4[r2]     // Catch:{ all -> 0x0061 }
            boolean r0 = r1 instanceof javax.net.ssl.X509TrustManager     // Catch:{ all -> 0x0061 }
            if (r0 == 0) goto L_0x0048
            javax.net.ssl.X509TrustManager r1 = (javax.net.ssl.X509TrustManager) r1     // Catch:{ all -> 0x0061 }
            goto L_0x004b
        L_0x0048:
            int r2 = r2 + 1
            goto L_0x003d
        L_0x004b:
            monitor-exit(r6)     // Catch:{ all -> 0x006e }
            goto L_0x004f
        L_0x004d:
            monitor-exit(r6)     // Catch:{ all -> 0x006e }
            r1 = r5
        L_0x004f:
            if (r1 != 0) goto L_0x0055
            r0 = 0
            java.security.cert.X509Certificate[] r1 = new java.security.cert.X509Certificate[r0]     // Catch:{ all -> 0x006e }
            goto L_0x0064
        L_0x0055:
            java.security.cert.X509Certificate[] r1 = r1.getAcceptedIssuers()     // Catch:{ all -> 0x006e }
            java.lang.ref.SoftReference r0 = new java.lang.ref.SoftReference     // Catch:{ all -> 0x006e }
            r0.<init>(r1)     // Catch:{ all -> 0x006e }
            r8.A00 = r0     // Catch:{ all -> 0x006e }
            goto L_0x0064
        L_0x0061:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x006e }
            throw r0     // Catch:{ all -> 0x006e }
        L_0x0064:
            monitor-exit(r7)
            java.util.List r0 = java.util.Arrays.asList(r1)
            byte[][] r0 = A00(r0)
            return r0
        L_0x006e:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17910zg.A01():byte[][]");
    }

    public static byte[][] A00(List list) {
        byte[][] bArr = new byte[list.size()][];
        for (int i = 0; i < list.size(); i++) {
            try {
                bArr[i] = ((X509Certificate) list.get(i)).getEncoded();
            } catch (CertificateEncodingException e) {
                C010708t.A09(A01, "Failed to encode Root CA", e);
            }
        }
        return bArr;
    }
}
