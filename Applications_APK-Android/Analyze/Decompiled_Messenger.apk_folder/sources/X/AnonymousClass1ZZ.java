package X;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ZZ  reason: invalid class name */
public final class AnonymousClass1ZZ {
    private static volatile AnonymousClass1ZZ A02;
    public AnonymousClass0UN A00;
    private final Map A01 = new ConcurrentHashMap();

    public static C25281Za A00(AnonymousClass1ZZ r3, AnonymousClass1ZX r4, boolean z) {
        C25281Za r2 = (C25281Za) r3.A01.get(Integer.valueOf(r4.A02));
        if (r2 != null || !z) {
            return r2;
        }
        C25281Za r22 = new C25281Za();
        r3.A01.put(Integer.valueOf(r4.A02), r22);
        return r22;
    }

    public static final AnonymousClass1ZZ A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass1ZZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass1ZZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private AnonymousClass1ZZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
