package X;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.0j4  reason: invalid class name */
public final class AnonymousClass0j4 {
    public static Object[] A0F(Iterable iterable, Class cls) {
        return A0G(iterable, (Object[]) Array.newInstance(cls, 0));
    }

    public static Object A07(Iterable iterable, Object obj) {
        if (iterable instanceof Collection) {
            if (!((Collection) iterable).isEmpty()) {
                if (iterable instanceof List) {
                    List list = (List) iterable;
                    return list.get(list.size() - 1);
                }
            }
            return obj;
        }
        Iterator it = iterable.iterator();
        if (it.hasNext()) {
            do {
                obj = it.next();
            } while (it.hasNext());
        }
        return obj;
    }

    public static boolean A0B(Iterable iterable, Predicate predicate) {
        if (!(iterable instanceof RandomAccess) || !(iterable instanceof List)) {
            Iterator it = iterable.iterator();
            Preconditions.checkNotNull(predicate);
            boolean z = false;
            while (it.hasNext()) {
                if (predicate.apply(it.next())) {
                    it.remove();
                    z = true;
                }
            }
            return z;
        }
        List list = (List) iterable;
        Preconditions.checkNotNull(predicate);
        int i = 0;
        int i2 = 0;
        while (i < list.size()) {
            Object obj = list.get(i);
            if (!predicate.apply(obj)) {
                if (i > i2) {
                    try {
                        list.set(i2, obj);
                    } catch (UnsupportedOperationException unused) {
                        for (int size = list.size() - 1; size > i; size--) {
                            if (predicate.apply(list.get(size))) {
                                list.remove(size);
                            }
                        }
                        for (int i3 = i - 1; i3 >= i2; i3--) {
                            list.remove(i3);
                        }
                    } catch (IllegalArgumentException unused2) {
                        for (int size2 = list.size() - 1; size2 > i; size2--) {
                            if (predicate.apply(list.get(size2))) {
                                list.remove(size2);
                            }
                        }
                        for (int i4 = i - 1; i4 >= i2; i4--) {
                            list.remove(i4);
                        }
                    }
                }
                i2++;
            }
            i++;
        }
        list.subList(i2, list.size()).clear();
        if (i != i2) {
            return true;
        }
        return false;
    }

    public static boolean A0C(Iterable iterable, Iterable iterable2) {
        if (!(iterable instanceof Collection) || !(iterable2 instanceof Collection) || ((Collection) iterable).size() == ((Collection) iterable2).size()) {
            return C24931Xr.A0A(iterable.iterator(), iterable2.iterator());
        }
        return false;
    }

    public static boolean A0D(Collection collection, Iterable iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        Preconditions.checkNotNull(iterable);
        return C24931Xr.A09(collection, iterable.iterator());
    }

    public static Object[] A0E(Iterable iterable) {
        Collection A00;
        if (iterable instanceof Collection) {
            A00 = (Collection) iterable;
        } else {
            Iterator it = iterable.iterator();
            A00 = C04300To.A00();
            C24931Xr.A09(A00, it);
        }
        return A00.toArray();
    }

    public static Object[] A0G(Iterable iterable, Object[] objArr) {
        Collection A00;
        if (iterable instanceof Collection) {
            A00 = (Collection) iterable;
        } else {
            Iterator it = iterable.iterator();
            A00 = C04300To.A00();
            C24931Xr.A09(A00, it);
        }
        return A00.toArray(objArr);
    }

    public static Iterable A00(Iterable iterable, Function function) {
        Preconditions.checkNotNull(iterable);
        Preconditions.checkNotNull(function);
        return new AnonymousClass1CI(iterable, function);
    }

    public static Iterable A01(Iterable iterable, Predicate predicate) {
        Preconditions.checkNotNull(iterable);
        Preconditions.checkNotNull(predicate);
        return new C187268mo(iterable, predicate);
    }

    public static Object A02(Iterable iterable) {
        Iterator it = iterable.iterator();
        Object next = it.next();
        if (!it.hasNext()) {
            return next;
        }
        StringBuilder sb = new StringBuilder("expected one element but was: <");
        sb.append(next);
        for (int i = 0; i < 4 && it.hasNext(); i++) {
            sb.append(", ");
            sb.append(it.next());
        }
        if (it.hasNext()) {
            sb.append(", ...");
        }
        sb.append('>');
        throw new IllegalArgumentException(sb.toString());
    }

    public static Object A03(Iterable iterable, int i) {
        Preconditions.checkNotNull(iterable);
        if (iterable instanceof List) {
            return ((List) iterable).get(i);
        }
        Iterator it = iterable.iterator();
        if (i >= 0) {
            int A01 = C24931Xr.A01(it, i);
            if (it.hasNext()) {
                return it.next();
            }
            throw new IndexOutOfBoundsException(AnonymousClass08S.A0C("position (", i, ") must be less than the number of elements that remained (", A01, ")"));
        }
        throw new IndexOutOfBoundsException(AnonymousClass08S.A0A("position (", i, ") must not be negative"));
    }

    public static Object A04(Iterable iterable, int i, Object obj) {
        Preconditions.checkNotNull(iterable);
        if (i < 0) {
            throw new IndexOutOfBoundsException(AnonymousClass08S.A0A("position (", i, ") must not be negative"));
        } else if (iterable instanceof List) {
            List list = (List) iterable;
            if (i < list.size()) {
                return list.get(i);
            }
            return obj;
        } else {
            Iterator it = iterable.iterator();
            C24931Xr.A01(it, i);
            if (it.hasNext()) {
                return it.next();
            }
            return obj;
        }
    }

    public static Object A05(Iterable iterable, Predicate predicate, Object obj) {
        C24971Xv A05 = C24931Xr.A05(iterable.iterator(), predicate);
        if (A05.hasNext()) {
            return A05.next();
        }
        return obj;
    }

    public static Object A06(Iterable iterable, Object obj) {
        Iterator it = iterable.iterator();
        if (it.hasNext()) {
            return it.next();
        }
        return obj;
    }

    public static String A08(Iterable iterable) {
        StringBuilder sb = new StringBuilder("[");
        boolean z = true;
        for (Object append : iterable) {
            if (!z) {
                sb.append(", ");
            }
            z = false;
            sb.append(append);
        }
        sb.append(']');
        return sb.toString();
    }

    public static boolean A09(Iterable iterable, Predicate predicate) {
        Preconditions.checkNotNull(predicate);
        for (Object apply : iterable) {
            if (!predicate.apply(apply)) {
                return false;
            }
        }
        return true;
    }

    public static boolean A0A(Iterable iterable, Predicate predicate) {
        if (C24931Xr.A02(iterable.iterator(), predicate) != -1) {
            return true;
        }
        return false;
    }
}
