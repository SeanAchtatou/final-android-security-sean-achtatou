package X;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1W0  reason: invalid class name */
public final class AnonymousClass1W0 {
    public static final String[] A08 = {"cdma_base_station_id", "cdma_network_id", "cdma_system_id", "gsm_cid", "gsm_lac"};
    private static final String[] A09 = {"network_type", "phone_type", "sim_country_iso", AnonymousClass80H.$const$string(25), "network_country_iso", AnonymousClass80H.$const$string(22), "is_network_roaming", "signal_level", "signal_asu_level", "signal_dbm"};
    private static final String[] A0A;
    public AnonymousClass0UN A00;
    private long A01 = -1;
    private AtomicReference A02;
    private final long A03;
    private final long A04;
    private final long A05;
    private final C25051Yd A06;
    private final boolean A07;

    static {
        String[] strArr = new String[36];
        System.arraycopy(new String[]{"sim_operator_mcc_mnc", "has_icc_card", "cdma_base_station_id", "cdma_base_station_latitude", "cdma_base_station_longitude", "cdma_network_id", "cdma_system_id", "network_operator_mcc_mnc", "gsm_cid", "gsm_lac", "gsm_psc", "gsm_mcc", "gsm_mnc", "gsm_arfcn", "lte_ci", "lte_mcc", "lte_mnc", "lte_pci", "lte_tac", "lte_earfcn", "lte_bandwidth", "lte_cqi", "lte_rsrp", "lte_rsrq", "lte_rssnr", "wcdma_cid", "wcdma_mcc"}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{"wcdma_mnc", "wcdma_psc", "wcdma_lac", "wcdma_uarfcn", "signal_lte_timing_advance", "extra", "network_type_info", "network_generation", "network_params"}, 0, strArr, 27, 9);
        A0A = strArr;
    }

    public static final AnonymousClass1W0 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1W0(r1);
    }

    private void A01(Map map) {
        Map A0A2 = ((C31631k0) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AO4, this.A00)).A0A(this.A05);
        if (A0A2 != null) {
            for (String str : A09) {
                Object obj = A0A2.get(str);
                if (obj != null) {
                    map.put(str, obj.toString());
                }
            }
            for (String str2 : A0A) {
                Object obj2 = A0A2.get(str2);
                if (obj2 != null) {
                    map.put(str2, obj2.toString());
                }
            }
        }
        ((C31631k0) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AO4, this.A00)).A0C(map);
    }

    public void A02(Map map) {
        C188898pw A022;
        if (this.A04 > 0 && A04() && (A022 = ((AnonymousClass25O) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BPU, this.A00)).A02(this.A04)) != null) {
            map.put("device_lat", Double.toString(A022.A00.getLatitude()));
            map.put("device_long", Double.toString(A022.A00.getLongitude()));
            Double A032 = A022.A03();
            if (A032 != null) {
                map.put("device_altitude", Double.toString(A032.doubleValue()));
            }
            Float A042 = A022.A04();
            if (A042 != null) {
                map.put("device_acc", Float.toString(A042.floatValue()));
            }
            Float A052 = A022.A05();
            if (A052 != null) {
                map.put("device_altitude_acc", Float.toString(A052.floatValue()));
            }
        }
    }

    public void A03(Map map, int i) {
        if (map != null && A04()) {
            if (!this.A07 || this.A03 <= 0) {
                A01(map);
            } else {
                if (((AnonymousClass06B) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AgK, this.A00)).now() - this.A01 > this.A03) {
                    HashMap hashMap = new HashMap();
                    A01(hashMap);
                    this.A02.set(hashMap);
                    this.A01 = ((AnonymousClass06B) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AgK, this.A00)).now();
                }
                map.putAll((Map) this.A02.get());
            }
            if (i != -1) {
                boolean z = true;
                if (i == ((C31631k0) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AO4, this.A00)).A08()) {
                    z = false;
                }
                map.put("tower_changed", String.valueOf(z));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0030, code lost:
        if (((X.AnonymousClass25O) X.AnonymousClass1XX.A02(2, X.AnonymousClass1Y3.BPU, r8.A00)).A01.isProviderEnabled("gps") == false) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04() {
        /*
            r8 = this;
            r0 = 11
            java.lang.String r6 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            int r5 = X.AnonymousClass1Y3.AWJ
            X.0UN r0 = r8.A00
            r4 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r5, r0)
            X.0pP r0 = (X.C12460pP) r0
            r7 = 0
            if (r0 != 0) goto L_0x0015
            return r7
        L_0x0015:
            boolean r0 = r0.A08(r6)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            r2 = 2
            r3 = 1
            if (r0 == 0) goto L_0x0032
            int r1 = X.AnonymousClass1Y3.BPU     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            X.0UN r0 = r8.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            X.25O r0 = (X.AnonymousClass25O) r0     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            android.location.LocationManager r1 = r0.A01     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            java.lang.String r0 = "gps"
            boolean r1 = r1.isProviderEnabled(r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            r0 = 1
            if (r1 != 0) goto L_0x0033
        L_0x0032:
            r0 = 0
        L_0x0033:
            if (r0 == 0) goto L_0x0036
            return r3
        L_0x0036:
            X.0UN r0 = r8.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r5, r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            X.0pP r0 = (X.C12460pP) r0     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            boolean r0 = r0.A08(r6)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            if (r0 != 0) goto L_0x0058
            X.0UN r0 = r8.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r5, r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            X.0pP r1 = (X.C12460pP) r1     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            r0 = 10
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            boolean r0 = r1.A08(r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            if (r0 == 0) goto L_0x006d
        L_0x0058:
            int r1 = X.AnonymousClass1Y3.BPU     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            X.0UN r0 = r8.A00     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            X.25O r0 = (X.AnonymousClass25O) r0     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            android.location.LocationManager r1 = r0.A01     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            java.lang.String r0 = "network"
            boolean r0 = r1.isProviderEnabled(r0)     // Catch:{ IllegalArgumentException | SecurityException -> 0x006d }
            if (r0 == 0) goto L_0x006d
            r7 = 1
        L_0x006d:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1W0.A04():boolean");
    }

    public AnonymousClass1W0(AnonymousClass1XY r6) {
        this.A00 = new AnonymousClass0UN(7, r6);
        C25051Yd A002 = AnonymousClass0WT.A00(r6);
        this.A06 = A002;
        this.A04 = A002.At0(563521184465384L);
        this.A05 = this.A06.At0(563521184137700L);
        this.A07 = this.A06.Aeo(282046207624229L, false);
        this.A03 = this.A06.At1(563521184399847L, 2000);
        this.A02 = new AtomicReference();
    }
}
