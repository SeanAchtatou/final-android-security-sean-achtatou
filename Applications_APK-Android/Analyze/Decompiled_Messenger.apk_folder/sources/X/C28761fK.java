package X;

import android.util.Base64;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.messaging.analytics.reliability.AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.send.SendError;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.facebook.ui.media.attachments.source.MediaResourceCameraPosition;
import com.facebook.ui.media.attachments.source.MediaResourceSendSource;
import com.google.common.collect.ImmutableList;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1fK  reason: invalid class name and case insensitive filesystem */
public final class C28761fK {
    public static final AnonymousClass1Y7 A04 = ((AnonymousClass1Y7) C05690aA.A0m.A09("rich_media_reliability_serialized"));
    private static volatile C28761fK A05;
    public AnonymousClass0UN A00;
    public LinkedHashMap A01 = null;
    public final Set A02 = new HashSet();
    public final Set A03 = new HashSet();

    public static synchronized void A02(C28761fK r5) {
        synchronized (r5) {
            synchronized (r5) {
                if (r5.A01 != null) {
                    try {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                        objectOutputStream.writeObject(r5.A01);
                        objectOutputStream.flush();
                        String str = new String(Base64.encode(byteArrayOutputStream.toByteArray(), 0));
                        objectOutputStream.close();
                        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, r5.A00)).edit();
                        edit.BzC(A04, str);
                        edit.commit();
                    } catch (IOException e) {
                        ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, r5.A00)).softReport("rich_media_reliabilities_serialization_failed", e);
                        C30281hn edit2 = ((FbSharedPreferences) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B6q, r5.A00)).edit();
                        edit2.C1B(A04);
                        edit2.commit();
                    }
                }
            }
            return;
        }
        return;
    }

    private synchronized void A03(String str, AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo, SendError sendError) {
        String str2;
        C11670nb r3 = new C11670nb("rich_media_msg_send");
        r3.A0D("otd", str);
        r3.A0D("msgType", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.messageType);
        r3.A0D("threadType", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.threadType);
        r3.A0D("threadKey", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.threadKey);
        r3.A0D("entry_point", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.entryPoint);
        r3.A0D(C22298Ase.$const$string(AnonymousClass1Y3.A23), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mediaSource);
        r3.A0D("media_camera_position", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.cameraPosition);
        r3.A0D("media_camera_mode", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.cameraMode);
        r3.A09("mqttAttempts", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mqttAttempts);
        r3.A09("graphAttempts", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.graphAttempts);
        r3.A09("preparationAttempts", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.preparationAttempts);
        r3.A09("numOfFailure", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.numOfFailure);
        r3.A09("attachmentCount", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.numberOfSubAttachments);
        r3.A09("totalSize", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.sizeInBytesOfSubAttachments);
        r3.A09(AnonymousClass24B.$const$string(1274), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.sizeInBytesOriginally);
        r3.A0D("mediaType", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mediaType);
        r3.A0D("mimeType", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mimeType);
        r3.A0D("photoQualityOption", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.photoQualityOption);
        r3.A0D("outcome", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.outcome);
        if (sendError == null) {
            r3.A09("sendSuccess", 1);
            r3.A0A("sendLatency", ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A00)).now() - aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.startTimestamp);
        } else {
            r3.A09("sendSuccess", 0);
            r3.A0C("finalError", sendError);
            r3.A0A("failLatency", ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A00)).now() - aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.startTimestamp);
        }
        r3.A0A("startTime", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.startTimestamp);
        r3.A0A("duration", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mediaDurationMs);
        r3.A09(AnonymousClass24B.$const$string(AnonymousClass1Y3.AAR), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.originalHeight);
        r3.A09(AnonymousClass24B.$const$string(AnonymousClass1Y3.AAS), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.originalWidth);
        r3.A09("original_video_bitrate", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.originalVideoBitrate);
        r3.A09(AnonymousClass24B.$const$string(228), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.downsizedHeight);
        r3.A09(AnonymousClass24B.$const$string(AnonymousClass1Y3.A1n), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.downsizedWidth);
        r3.A09(AnonymousClass24B.$const$string(1445), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.transcodedBitrate);
        r3.A0D("sticker_id", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.stickerId);
        r3.A0E(AnonymousClass24B.$const$string(AnonymousClass1Y3.ABz), aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.isTwoPhase);
        r3.A0E("is_direct", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.isDirectMessage);
        r3.A0D(AnonymousClass24B.$const$string(AnonymousClass1Y3.A1r), C204779l5.A00(aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.exceptionInfo));
        r3.A0D("parent_msg_id", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.parentMessageId);
        if (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.isSendByServer) {
            str2 = "1";
        } else {
            str2 = "0";
        }
        r3.A0D("send_message_by_server", str2);
        r3.A0D("media_fbid", aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.mediaFbId);
        r3.A0D(AnonymousClass24B.$const$string(75), str);
        ((DeprecatedAnalyticsLogger) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AR0, this.A00)).A09(r3);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r1 = ((com.facebook.prefs.shared.FbSharedPreferences) X.AnonymousClass1XX.A02(3, X.AnonymousClass1Y3.B6q, r10.A00)).edit();
        r1.C1B(X.C28761fK.A04);
        r1.commit();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0080 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A04(X.C28761fK r10) {
        /*
            monitor-enter(r10)
            java.util.LinkedHashMap r0 = r10.A01     // Catch:{ all -> 0x00a3 }
            if (r0 != 0) goto L_0x009b
            monitor-enter(r10)     // Catch:{ all -> 0x00a3 }
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0097 }
            X.0UN r0 = r10.A00     // Catch:{ all -> 0x0097 }
            r5 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0097 }
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0     // Catch:{ all -> 0x0097 }
            boolean r0 = r0.BFQ()     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x009a
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            r10.A01 = r0     // Catch:{ all -> 0x0097 }
            X.0UN r0 = r10.A00     // Catch:{ all -> 0x0097 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0097 }
            com.facebook.prefs.shared.FbSharedPreferences r2 = (com.facebook.prefs.shared.FbSharedPreferences) r2     // Catch:{ all -> 0x0097 }
            X.1Y7 r1 = X.C28761fK.A04     // Catch:{ all -> 0x0097 }
            r0 = 0
            java.lang.String r0 = r2.B4F(r1, r0)     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x009a
            r7 = 0
            byte[] r0 = android.util.Base64.decode(r0, r7)     // Catch:{ Exception -> 0x0080 }
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0080 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0080 }
            java.io.ObjectInputStream r0 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0080 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0080 }
            java.lang.Object r0 = r0.readObject()     // Catch:{ Exception -> 0x0080 }
            java.util.LinkedHashMap r0 = (java.util.LinkedHashMap) r0     // Catch:{ Exception -> 0x0080 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ Exception -> 0x0080 }
            java.util.Iterator r9 = r0.iterator()     // Catch:{ Exception -> 0x0080 }
        L_0x004c:
            boolean r0 = r9.hasNext()     // Catch:{ Exception -> 0x0080 }
            if (r0 == 0) goto L_0x009a
            java.lang.Object r8 = r9.next()     // Catch:{ Exception -> 0x0080 }
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8     // Catch:{ Exception -> 0x0080 }
            java.lang.Object r6 = r8.getValue()     // Catch:{ Exception -> 0x0080 }
            com.facebook.messaging.analytics.reliability.AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo r6 = (com.facebook.messaging.analytics.reliability.AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) r6     // Catch:{ Exception -> 0x0080 }
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ Exception -> 0x0080 }
            X.0UN r0 = r10.A00     // Catch:{ Exception -> 0x0080 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ Exception -> 0x0080 }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ Exception -> 0x0080 }
            long r3 = r0.now()     // Catch:{ Exception -> 0x0080 }
            long r0 = r6.startTimestamp     // Catch:{ Exception -> 0x0080 }
            long r3 = r3 - r0
            r1 = 259200000(0xf731400, double:1.280618154E-315)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x004c
            java.util.LinkedHashMap r1 = r10.A01     // Catch:{ Exception -> 0x0080 }
            java.lang.Object r0 = r8.getKey()     // Catch:{ Exception -> 0x0080 }
            r1.put(r0, r6)     // Catch:{ Exception -> 0x0080 }
            goto L_0x004c
        L_0x0080:
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0097 }
            X.0UN r0 = r10.A00     // Catch:{ all -> 0x0097 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x0097 }
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0     // Catch:{ all -> 0x0097 }
            X.1hn r1 = r0.edit()     // Catch:{ all -> 0x0097 }
            X.1Y7 r0 = X.C28761fK.A04     // Catch:{ all -> 0x0097 }
            r1.C1B(r0)     // Catch:{ all -> 0x0097 }
            r1.commit()     // Catch:{ all -> 0x0097 }
            goto L_0x009a
        L_0x0097:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00a3 }
            throw r0     // Catch:{ all -> 0x00a3 }
        L_0x009a:
            monitor-exit(r10)     // Catch:{ all -> 0x00a3 }
        L_0x009b:
            java.util.LinkedHashMap r1 = r10.A01     // Catch:{ all -> 0x00a3 }
            r0 = 0
            if (r1 == 0) goto L_0x00a1
            r0 = 1
        L_0x00a1:
            monitor-exit(r10)
            return r0
        L_0x00a3:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28761fK.A04(X.1fK):boolean");
    }

    public synchronized void A08(Integer num, String str) {
        AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo;
        if (A04(this) && (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = (AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) this.A01.get(str)) != null) {
            if (num == AnonymousClass07B.A00) {
                aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.outcome = AnonymousClass53M.A00(AnonymousClass07B.A01);
            } else if (num == AnonymousClass07B.A01) {
                aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.outcome = AnonymousClass53M.A00(AnonymousClass07B.A0C);
            }
            A03(str, aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo, null);
            this.A01.remove(str);
            if (((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, this.A00)).Aem(282252365792520L)) {
                this.A02.add(str);
            }
            A02(this);
        }
    }

    public synchronized void A09(String str) {
        if (A04(this) && str != null) {
            AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = (AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) this.A01.get(str);
            if (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo == null) {
                this.A03.add(str);
            } else {
                aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.isTwoPhase = true;
                A02(this);
            }
        }
    }

    public synchronized void A0A(String str, SendError sendError) {
        AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo;
        Integer num;
        synchronized (this) {
            if (A04(this) && (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = (AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) this.A01.get(str)) != null) {
                if (sendError.A02.shouldNotBeRetried) {
                    num = AnonymousClass07B.A0Y;
                } else {
                    num = AnonymousClass07B.A0N;
                }
                aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.outcome = AnonymousClass53M.A00(num);
                aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.isDirectMessage = false;
                A03(str, aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo, sendError);
                aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.numOfFailure++;
                A02(this);
            }
        }
    }

    public synchronized void A0B(String str, MediaResource mediaResource) {
        AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo;
        if (!C06850cB.A0B(str) && A04(this) && mediaResource != null && (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = (AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) this.A01.get(str)) != null) {
            aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.sizeInBytesOfSubAttachments += (int) mediaResource.A06;
            A02(this);
        }
    }

    public synchronized void A0C(String str, Throwable th) {
        AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo;
        if (!C06850cB.A0B(str) && A04(this) && (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = (AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) this.A01.get(str)) != null) {
            aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.exceptionInfo = th.toString();
            A02(this);
        }
    }

    public synchronized void A0D(String str, boolean z) {
        AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo;
        if (!(!A04(this) || str == null || (aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = (AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) this.A01.get(str)) == null)) {
            aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.isSendByServer = z;
            A02(this);
        }
    }

    public static AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo A00(C28761fK r13, Message message) {
        String mediaResourceCameraPosition;
        String str;
        if (!C06850cB.A0A(message.A0z)) {
            return new AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo(((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, r13.A00)).now(), ((AnonymousClass18P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BPD, r13.A00)).A03(message), message.A0U, message.A0z);
        }
        if (!A05(message)) {
            return null;
        }
        long now = ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, r13.A00)).now();
        String A032 = ((AnonymousClass18P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BPD, r13.A00)).A03(message);
        ThreadKey threadKey = message.A0U;
        String A022 = ((MediaResource) message.A0b.get(0)).A02();
        MediaResourceSendSource mediaResourceSendSource = ((MediaResource) message.A0b.get(0)).A0R;
        StringBuilder sb = new StringBuilder();
        sb.append(mediaResourceSendSource.A00.analyticsName);
        String str2 = mediaResourceSendSource.A02;
        if (str2 != null) {
            sb.append("#");
            sb.append(str2);
        }
        String sb2 = sb.toString();
        String str3 = ((MediaResource) message.A0b.get(0)).A0R.A01.analyticsName;
        MediaResourceCameraPosition mediaResourceCameraPosition2 = ((MediaResource) message.A0b.get(0)).A0Q;
        if (mediaResourceCameraPosition2 == MediaResourceCameraPosition.A01) {
            mediaResourceCameraPosition = C64593Cm.UNKNOWN.analyticsName;
        } else {
            mediaResourceCameraPosition = mediaResourceCameraPosition2.toString();
        }
        MediaResource mediaResource = (MediaResource) message.A0b.get(0);
        if (C178298Kr.A00(mediaResource.A0D) != null) {
            str = "VIDEO_CALL";
        } else {
            str = mediaResource.A0V;
        }
        return new AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo(now, A032, threadKey, A022, sb2, str3, mediaResourceCameraPosition, str, message.A0b.size());
    }

    public static final C28761fK A01(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C28761fK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C28761fK(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static boolean A05(Message message) {
        ImmutableList immutableList = message.A0b;
        if (immutableList == null || immutableList.isEmpty() || message.A0b.contains(null)) {
            return false;
        }
        return true;
    }

    public synchronized void A07(Message message, String str) {
        AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo;
        if (A04(this)) {
            Message message2 = message;
            if (message != null) {
                if (((AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo) this.A01.get(message2.A0w)) == null) {
                    String str2 = str;
                    if (!C06850cB.A0A(message2.A0z)) {
                        aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = new AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo(((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A00)).now(), ((AnonymousClass18P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BPD, this.A00)).A03(message2), message2.A0U, message2.A0z);
                        aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo.sendSource = str2;
                    } else {
                        aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo = new AggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo(((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A00)).now(), ((AnonymousClass18P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BPD, this.A00)).A03(message2), message2.A0U, str2, C64563Cj.A0I.toString(), C64573Ck.PICK.toString(), 0);
                    }
                    this.A01.put(message2.A0w, aggregatedRichMediaReliabilityLogger$RichMediaReliabilityInfo);
                }
                A02(this);
            }
        }
    }

    private C28761fK(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if (r0 != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A06(com.facebook.messaging.model.messages.Message r1) {
        /*
            boolean r0 = A05(r1)
            if (r0 != 0) goto L_0x0028
            java.lang.String r0 = r1.A0z
            boolean r0 = X.C06850cB.A0A(r0)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x0028
            com.google.common.collect.ImmutableList r0 = r1.A0X
            if (r0 == 0) goto L_0x0024
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0024
            com.google.common.collect.ImmutableList r1 = r1.A0X
            r0 = 0
            boolean r0 = r1.contains(r0)
            r1 = 1
            if (r0 == 0) goto L_0x0025
        L_0x0024:
            r1 = 0
        L_0x0025:
            r0 = 0
            if (r1 == 0) goto L_0x0029
        L_0x0028:
            r0 = 1
        L_0x0029:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28761fK.A06(com.facebook.messaging.model.messages.Message):boolean");
    }
}
