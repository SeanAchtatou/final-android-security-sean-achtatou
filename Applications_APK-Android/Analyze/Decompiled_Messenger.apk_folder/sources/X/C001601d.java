package X;

import android.view.ViewTreeObserver;

/* renamed from: X.01d  reason: invalid class name and case insensitive filesystem */
public final class C001601d implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.base.app.SplashScreenActivity$Api16Utils$1$1";
    public final /* synthetic */ ViewTreeObserver.OnDrawListener A00;
    public final /* synthetic */ AnonymousClass08c A01;

    public C001601d(AnonymousClass08c r1, ViewTreeObserver.OnDrawListener onDrawListener) {
        this.A01 = r1;
        this.A00 = onDrawListener;
    }

    public void run() {
        this.A01.A01.removeOnDrawListener(this.A00);
    }
}
